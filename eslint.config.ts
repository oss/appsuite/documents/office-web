/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Linter } from "eslint";

import { utils, eslint } from "@open-xchange/linter-presets";

// constants ==================================================================

// uppercase function names that are allowed to be used without `new`
const UPPERCASE_FUNCTIONS = [
    "$.Deferred",
    "$.Event",
    "Baton",
    "CryptoJS.MD5",
];

// list of banned globals in browser modules, for core rule "no-restricted-globals"
const RESTRICTED_BROWSER_GLOBALS = [
    { name: "_", message: "Import Underscore library from '$/underscore'." },
    { name: "$", message: "Import JQuery library from '$/jquery'." },
];

// list of banned imports in browser modules, for core rule "no-restricted-imports"
const RESTRICTED_BROWSER_IMPORTS = [
    { name: "crypto-js", message: "Must be explicitly permitted to prevent accidental usage in security-relevant cases." },
    { name: "jquery", message: "Import JQuery library from '$/jquery'." },
    { name: "underscore", message: "Import Underscore library from '$/underscore'." },
    { name: "$/io.ox/core/notifications", message: "Use '$/io.ox/core/yell'." },
    { name: "$/io.ox/core/uuids", message: "Use 'uuid' from tk/algorithms." },
];

// list of banned properties in browser modules (JS and TS), for core rule "no-restricted-properties"
const RESTRICTED_BROWSER_PROPERTIES = [

    // I18N debug mode only implemented in Underscore mixin
    { object: "gt", property: "noI18n", message: "Use '_.noI18n' instead." },

    // banned Underscore plugins from core-ui
    { object: "_", property: "getArray", message: "Use 'ary.wrap' from tk/algorithms." },
    { object: "_", property: "deepClone", message: "Use 'json.deepClone' from tk/algorithms." },
    { object: "_", property: "escapeRegExp", message: "Use 're.escape' from tk/algorithms." },
    { object: "_", property: "url", message: "Use module '$/url' instead." },
];

// list of banned properties in browser modules (TS only), for core rule "no-restricted-properties"
const RESTRICTED_BROWSER_PROPERTIES_TS = [

    // banned built-in functions
    { object: "JSON", property: "parse", message: "Use 'json.tryParse' or 'json.safeParse' from tk/algorithms." },
    { object: "JSON", property: "stringify", message: "Use 'json.tryStringify' or 'json.safeStringify' from tk/algorithms." },

    // banned Underscore functions (better alternatives, e.g. better type support or safety)
    { object: "_", property: "isUndefined", message: "Compare to 'undefined' directly, or use 'is.undefined' from tk/algorithms." },
    { object: "_", property: "isNull", message: "Compare to 'null' directly, or use 'is.null' from tk/algorithms." },
    { object: "_", property: "isNumber", message: "Use 'is.number' from tk/algorithms." },
    { object: "_", property: "isFinite", message: "Use 'Number.isFinite' instead." },
    { object: "_", property: "isNaN", message: "Use 'Number.isNaN' instead." },
    { object: "_", property: "isString", message: "Use 'is.string' from tk/algorithms." },
    { object: "_", property: "isBoolean", message: "Use 'is.boolean' from tk/algorithms." },
    { object: "_", property: "isSymbol", message: "Use 'is.symbol' from tk/algorithms." },
    { object: "_", property: "isObject", message: "Use 'is.dict' or 'is.object' from tk/algorithms." },
    { object: "_", property: "isArray", message: "Use 'Array.isArray'; or 'is.array', 'is.readonlyArray', or 'is.nativeArray' from tk/algorithms." },
    { object: "_", property: "isFunction", message: "Use 'is.function' from tk/algorithms." },
    { object: "_", property: "isEmpty", message: "Use 'is.empty' from tk/algorithms." },
    { object: "_", property: "constant", message: "Use 'fun.const' from tk/algorithms; or '() => value'." },
    { object: "_", property: "once", message: "Use 'fun.once' from tk/algorithms." },
    { object: "_", property: "memoize", message: "Use 'fun.memoize' from tk/algorithms." },
    { object: "_", property: "times", message: "Use 'ary.sequence', 'ary.fill', or 'ary.generate' from tk/algorithms." },
    { object: "_", property: "keys", message: "Use 'Object.keys' instead." },
    { object: "_", property: "values", message: "Use 'dict.values' from tk/algorithms." },
    { object: "_", property: "extend", message: "Use 'Object.assign' or spread operator." },
    { object: "_", property: "extendOwn", message: "Use 'Object.assign' or spread operator." },
    { object: "_", property: "assign", message: "Use 'Object.assign' or spread operator." },
    { object: "_", property: "clone", message: "Use spread operator, or 'json.flatClone' from tk/algorithms." },
    { object: "_", property: "first", message: "Use '[0]' for single element, or 'Array::slice(0, n)' for sub arrays." },
    { object: "_", property: "head", message: "Use '[0]' for single element, or 'Array::slice(0, n)' for sub arrays." },
    { object: "_", property: "take", message: "Use '[0]' for single element, or 'Array::slice(0, n)' for sub arrays." },
    { object: "_", property: "initial", message: "Use 'ary.initial()' from tk/algorithms to remove the last element, or 'ary.initial(n)' to remove the last n elements." },
    { object: "_", property: "last", message: "Use 'ary.last()' from tk/algorithms for single element, or 'Array::slice(-n)' for sub arrays." },
    { object: "_", property: "rest", message: "Use 'Array::slice(n)'." },
    { object: "_", property: "tail", message: "Use 'Array::slice(n)'." },
    { object: "_", property: "drop", message: "Use 'Array::slice(n)'." },
    { object: "_", property: "range", message: "Use 'ary.sequence' from tk/algorithms." },

    // banned Underscore iterator functions due to https://github.com/jashkenas/underscore/issues/148 (and 659, 937, 1590, ...)
    { object: "_", property: "forEach", message: "Use 'Array::forEach' for arrays, and 'dict.forEach' from tk/algorithms for objects." },
    { object: "_", property: "each", message: "Use 'Array::forEach' for arrays, and 'dict.forEach' from tk/algorithms for objects." },
    { object: "_", property: "map", message: "Use 'Array::map' or 'Array.from' for arrays, and 'Array.from(dict.values())' from tk/algorithms for objects." },
    { object: "_", property: "collect", message: "Use 'Array::map' or 'Array.from' for arrays, and 'Array.from(dict.values())' from tk/algorithms for objects." },
    { object: "_", property: "reduce", message: "Use 'Array::reduce' for arrays, and 'dict.reduce' from tk/algorithms for objects." },
    { object: "_", property: "inject", message: "Use 'Array::reduce' for arrays, and 'dict.reduce' from tk/algorithms for objects." },
    { object: "_", property: "foldl", message: "Use 'Array::reduce' for arrays, and 'dict.reduce' from tk/algorithms for objects." },
    { object: "_", property: "reduceRight", message: "Use 'Array::reduceRight' for arrays, and 'dict.reduce' from tk/algorithms for objects." },
    { object: "_", property: "foldr", message: "Use 'Array::reduceRight' for arrays, and 'dict.reduce' from tk/algorithms for objects." },
    { object: "_", property: "find", message: "Use 'Array::find' for arrays, and 'dict.find' from tk/algorithms for objects." },
    { object: "_", property: "detect", message: "Use 'Array::find' for arrays, and 'dict.find' from tk/algorithms for objects." },
    { object: "_", property: "filter", message: "Use 'Array::filter' for arrays, and 'ary.filterFrom(dict.values())' from tk/algorithms for objects." },
    { object: "_", property: "select", message: "Use 'Array::filter' for arrays, and 'ary.filterFrom(dict.values())' from tk/algorithms for objects." },
    { object: "_", property: "reject", message: "Use 'Array::filter' for arrays, and 'ary.filterFrom(dict.values())' from tk/algorithms for objects." },
    { object: "_", property: "every", message: "Use 'Array::every' for arrays, and 'dict.every' from tk/algorithms for objects." },
    { object: "_", property: "all", message: "Use 'Array::every' for arrays, and 'dict.every' from tk/algorithms for objects." },
    { object: "_", property: "some", message: "Use 'Array::some' for arrays, and 'dict.some' from tk/algorithms for objects." },
    { object: "_", property: "any", message: "Use 'Array::some' for arrays, and 'dict.some' from tk/algorithms for objects." },
    { object: "_", property: "findIndex", message: "Use 'Array::findIndex', or 'ary.findIndex' from tk/algorithms for array-likes." },
    { object: "_", property: "findLastIndex", message: "Use 'ary.findIndex' from tk/algorithms." },
    { object: "_", property: "mapObject", message: "Use 'dict.mapDict' or 'dict.mapRecord' from tk/algorithms for objects." },
    { object: "_", property: "findKey", message: "Use 'dict.findKey' from tk/algorithms." },

    // banned Underscore plugins from core-ui
    { object: "_", property: "copy", message: "Use 'json.flatClone' or 'json.deepClone' from tk/algorithms." },

    // banned JQuery functions
    { object: "$", property: "Deferred", message: "Use 'jpromise.deferred' from tk/algorithms for better type safety." },
];

// all restricted items to be passed to the environment "env.browser"
const RESTRICTED_BROWSER_ITEMS = {
    globals: RESTRICTED_BROWSER_GLOBALS,
    imports: RESTRICTED_BROWSER_IMPORTS,
    properties: RESTRICTED_BROWSER_PROPERTIES,
};

// functions ==================================================================

const resolve = utils.resolver(import.meta.url);

// exports ====================================================================

export default [

    ...eslint.configure({

        ignores: [
            "src/io.ox/office/rt2/dist/*",
            "spec/mocks/upload/*",
            "e2e/{core,plugins,output}/*",
            "e2e/customRerun.js",
            "helm/**/*",
        ],

        license: resolve("license-header"),

        packages: {
            allowed: ["jquery", "moment", "underscore"],
        },

        rules: {
            "@typescript-eslint/no-unsafe-declaration-merging": "off",
            "jsdoc/no-undefined-types": "off",
            "jsdoc/require-jsdoc": "off",
        },
    }),

    // development/build scripts ----------------------------------------------

    // NodeJS environment for configuration files and plugins
    ...eslint.env.node({
        files: ["*.{js,ts}", "{vite,perf}/**/*.{js,ts}"],
    }),

    // TODO: workaround for failing project service in VSCode: with option
    // "projectService: true", all TS files from "vite" will not be linted
    // (but other TS files, e.g. from "e2e" work as expected...)
    ...eslint.env.tsconfig({
        files: ["*.ts", "vite/**/*.ts"],
        project: resolve("tsconfig.json"),
    }),

    // reconfigure rules for "perf" directory
    {
        files: ["perf/**/*.{js,ts}"],
        rules: {
            // TODO: fix documentation
            "jsdoc/require-param": "off",
            "jsdoc/require-returns": "off",
            "jsdoc/require-yields": "off",
        },
    },

    // application source from "src" directory --------------------------------

    ...eslint.env.browser({
        files: ["src/**/*.{js,ts}"],
        restricted: {
            ...RESTRICTED_BROWSER_ITEMS,
            overrides: [{
                files: ["src/**/*.ts"],
                properties: RESTRICTED_BROWSER_PROPERTIES_TS,
            }],
        },
        rules: {
            // uppercase symbols to be called as functions
            "new-cap": ["error", { capIsNewExceptions: UPPERCASE_FUNCTIONS }],
            // promise plugin
            "promise/always-return": "off",
            // TODO: fix documentation
            "jsdoc/check-types": "off",
            "jsdoc/no-defaults": "off",
            "jsdoc/require-param": "off",
            "jsdoc/require-returns": "off",
            "jsdoc/require-template": "off",
            "jsdoc/require-throws": "off",
            "jsdoc/require-yields": "off",
            "jsdoc/valid-types": "off",
        },
    }),

    // native ES decorators in JS files
    ...eslint.env.decorators({
        files: ["src/**/*.js"],
    }),

    // project structure and module imports
    ...eslint.env.project({
        files: ["src/**/*.{js,ts}"],
        alias: {
            "@": "src",
        },
        external: [
            // virtual module provided by @open-xchange/rollup-plugin-po2json
            "gettext",
            // virtual modules provided by @open-xchange/vite-plugin-icon-sprite
            "virtual:svg/docs-icons.svg",
            "virtual:png/docs-icons.css",
            "virtual:png/docs-icons-*.png",
            // Core-UI modules
            "$/{backbone,jquery,moment,ox,swiper,underscore,url}",
            "$/io.ox/{backbone,contacts,core,files,guard,mail,metrics,settings,switchboard}/**/*",
            "$/pe/openai/consent",
        ],
        hierarchy: {
            toolkit: {
                files: ["@/io.ox/office/tk/**/*", "@/io.ox/office/settings"],
                extends: "debug",
            },
            baseframework: {
                files: ["@/io.ox/office/{baseframework,settings,wopi}/**/*"],
                extends: "toolkit",
            },
            rt2: {
                files: ["@/io.ox/office/rt2/**/*"],
                extends: "baseframework",
            },
            editframework: {
                files: ["@/io.ox/office/{editframework,drawinglayer}/**/*"],
                extends: "rt2",
            },
            textframework: {
                files: ["@/io.ox/office/textframework/**/*"],
                extends: "editframework",
            },
            text: {
                files: ["@/io.ox/office/text/**/*"],
                extends: "textframework",
            },
            spreadsheet: {
                files: ["@/io.ox/office/spreadsheet/**/*"],
                extends: "textframework",
            },
            presentation: {
                files: ["@/io.ox/office/presentation/**/*"],
                extends: "textframework",
            },
            presenter: {
                files: ["@/io.ox/office/presenter/**/*"],
                extends: "editframework",
            },
            portal: {
                files: ["@/io.ox/office/portal/**/*"],
                extends: ["text", "spreadsheet", "presentation", "presenter"],
            },
            tab: {
                files: ["@/io.ox/office/tab/**/*"],
                extends: "portal",
            },
            debug: {
                files: ["@/io.ox/office/debug/**/*"],
                extends: "portal",
                optional: true,
            },
        },
    }),

    // TODO: workaround for failing project service in VSCode: with option
    // "projectService: true", all TS files from "src" will not be linted
    // (but other TS files, e.g. from "e2e" work as expected...)
    ...eslint.env.tsconfig({
        files: ["src/**/*.ts"],
        project: resolve("src/tsconfig.json"),
    }),

    // disable a few recommended rules in existing TS code
    {
        files: ["src/**/*.ts"],
        rules: {
            // this rule intentionally ignores callbacks with type declaration for "this" parameter
            "@typescript-eslint/no-invalid-this": "off",
            // rule triggers false-positives when using `JQuery.Promise`
            "@typescript-eslint/unbound-method": "off",
            // TODO: fix code
            "@typescript-eslint/no-deprecated": "off",
            "@typescript-eslint/no-unnecessary-condition": "off",
            // TODO: fix code style
            "@stylistic/comma-dangle": "off",
        },
    },

    // allow importing external modules in module type declarations
    {
        files: ["src/types/**/*.d.ts"],
        rules: {
            "no-restricted-imports": "off",
            "env-project/no-invalid-modules": "off",
        },
    },

    // disable a few recommended rules in existing JS code
    {
        files: ["src/**/*.js"],
        ignores: ["src/io.ox/office/wopi/**/*.js"],
        rules: {
            "no-else-return": "off",
            "no-invalid-this": "off",
            "no-lonely-if": "off",
            "no-shadow": "off",
            "no-unneeded-ternary": "off",
            "no-useless-assignment": "off",
            "no-var": "off",
            "prefer-arrow-callback": "off",
            "promise/catch-or-return": "off",
            "promise/no-nesting": "off",
            // TODO: fix documentation
            "jsdoc/require-param-description": "off",
            "jsdoc/require-returns-description": "off",
            // TODO: fix code style
            "@stylistic/comma-dangle": "off",
            "@stylistic/quotes": "off",
        },
    },

    // TODO: Spreadsheet throws error codes (instances of the `ErrorCode` enum) everywhere
    {
        files: ["src/io.ox/office/spreadsheet/**/*.ts"],
        rules: {
            "@typescript-eslint/only-throw-error": "off",
        },
    },

    // do not lint formatting in externally generated JSON files
    {
        files: [
            "src/io.ox/office/drawinglayer/view/presetGeometries.json",
            "src/io.ox/office/spreadsheet/model/formula/resource/help/*.json",
        ],
        rules: {
            "jsonc/comma-style": "off",
            "jsonc/indent": "off",
            "jsonc/key-spacing": "off",
            "@stylistic/comma-spacing": "off",
        },
    },

    // unit tests from "spec" directory ---------------------------------------

    ...eslint.env.jest({
        files: ["spec/**/*.{js,ts}"],
        jestDom: true,
        rules: {
            "jest/expect-expect": "off",    // too many false positives (using helper functions in tests)
            "jest/prefer-lowercase-title": "off",   // TODO
            "jest/valid-expect": ["error", { alwaysAwait: true, maxArgs: 2 }],
            "jest-dom/prefer-to-have-value": "off", // too many false positives
            // TODO: fix code style
            "@stylistic/comma-dangle": "off",
        },
    }),

    // add browser globals
    ...eslint.env.browser({
        files: ["spec/**/*.{js,ts}"],
        restricted: RESTRICTED_BROWSER_ITEMS,
        rules: {
            // uppercase symbols to be called as functions
            "new-cap": ["error", { capIsNewExceptions: UPPERCASE_FUNCTIONS }],
            "no-console": "off",
            "no-throw-literal": "off",
            // TODO: fix documentation
            "jsdoc/require-param": "off",
            "jsdoc/require-returns": "off",
            "jsdoc/require-yields": "off",
        },
    }),

    // native ES decorators in JS files
    ...eslint.env.decorators({
        files: ["spec/**/*.js"],
    }),

    // project structure and module imports
    ...eslint.env.project({
        files: ["spec/**/*.{js,ts}"],
        alias: {
            "@": "src",
            $: "spec/mocks",
            "~": "spec/helper",
        },
    }),

    // allow importing external modules in setup, plugins, and mock files
    {
        files: ["spec/setup.js", "spec/plugins/**/*.js", "spec/mocks/**/*.js", "spec/helper/**/*.js"],
        rules: {
            "no-restricted-imports": "off",
            "env-project/no-invalid-modules": "off",
        },
    },

    // disable a few recommended rules in plugin modules
    {
        files: ["spec/plugins/**/*.js"],
        rules: {
            "no-invalid-this": "off",
        },
    },

    // disable a few recommended rules in helper modules
    {
        files: ["spec/helper/**/*.js"],
        rules: {
            "jest/no-export": "off",
            "jest/no-standalone-expect": "off",
            "jest/prefer-hooks-on-top": "off",
            "jest/require-top-level-describe": "off",
        },
    },

    // disable a few recommended rules in spec files
    {
        files: ["spec/**/*_spec.js"],
        rules: {
            "no-array-constructor": "off",
            "no-new-wrappers": "off",
            "no-object-constructor": "off",
        },
    },

    // disable a few recommended rules in legacy JS code
    {
        files: ["spec/io.ox/office/**/*_spec.js"],
        rules: {
            "no-var": "off",
            "prefer-arrow-callback": "off",
            "prefer-promise-reject-errors": "off",
            "@stylistic/quotes": "off",
        },
    },

    // special settings for spreadsheet function tests
    {
        files: ["spec/io.ox/office/spreadsheet/model/formula/funcs/*_spec.*"],
        rules: {
            "new-cap": "off",                   // allow to use upper-case function names (for sheet functions) in tests
            "jest/no-identical-title": "off",   // rule does not recognize `FunctionModuleTester.testFunction()` scopes
        },
    },

    // E2E tests from "e2e" directory -----------------------------------------

    ...eslint.env.codecept({
        files: ["e2e/**/*.{js,ts}"],
    }),

    // NodeJS environment for all test scripts
    ...eslint.env.node({
        files: ["e2e/**/*.{js,ts}"],
        ignores: ["e2e/codecept.conf.js"],
    }),

    // configuration is in CommonJS
    ...eslint.env.node({
        files: ["e2e/codecept.conf.js"],
        sourceType: "commonjs",
    }),
] satisfies Linter.Config[];
