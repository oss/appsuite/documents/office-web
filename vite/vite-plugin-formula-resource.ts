/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { basename, extname } from "node:path";

import type { Plugin } from "vite";

import type { Opt, Dict } from "@open-xchange/vite-helper/utils";
import { isDict, dictEntries, wrapArray } from "@open-xchange/vite-helper/utils";
import { resolvePath, resolveConfigPath } from "@open-xchange/vite-helper/file";
import { type ModuleDescriptor, PluginHelper } from "@open-xchange/vite-helper";

// types ======================================================================

/**
 * Configuration for the plugin "vite-plugin-formula-resource".
 */
export interface FormulaResourcePluginOptions {

    /**
     * Path to root directory with all formula resource files to be consumed.
     * Must contain a "data" subdirectory with JSON and/or YAML files with a
     * language code as file stem. May contain additional "help" and "mapping"
     * subdirectories for function tooltip help resources.
     */
    srcPath: string;
}

// private types --------------------------------------------------------------

type FileFormatKeys = "ooxml" | "odf";

// type shape of a formula data resource for a specific locale
interface DataResource {
    RC: string;
    booleans: Dict<string>;
    errors: Dict<string>;
    regions: Dict<string>;
    functions: Dict<string | Record<FileFormatKeys, string>>;
    cellParams: Dict<string>;
}

// type shape of a function help entry in source JSON
interface HelpResourceEntry {
    description: string | null;
    params: ReadonlyArray<string | null>;
    paramshelp: ReadonlyArray<string | null>;
}

// type shape of a formula help resource for a specific locale
type HelpResource = Dict<HelpResourceEntry>;

// type shape of a formula help key mapping for a specific locale
type HelpKeyMapping = Dict<string | readonly string[]>;

// validation configuration for formula resource entries
interface ResourceValidation {
    dict: boolean;
    re: RegExp | null;
    upper: boolean;
    fileformat?: boolean;
    keys?: readonly string[];
}

// constants ==================================================================

// regular expression to validate resource keys
const KEY_RE = /^[A-Z][A-Z0-9]*(\.[A-Z0-9]+)*$/;
// character range for a valid leading character in a localized name
const LEADING_CHAR = "A-Z\xc0-\xd6\xd8-\xde\u0100-\u02af\u0386-\u052f\u1e00-\u1ffc";
// character class pattern for a valid character at any position in a localized name
const LEADING_CHAR_PATTERN = `[${LEADING_CHAR}]`;
// character class pattern for a valid character at any position in a localized name
const OTHER_CHAR_PATTERN = `[${LEADING_CHAR}0-9]`;
// pattern for a valid part of a localized name
const NAME_PATTERN = `${LEADING_CHAR_PATTERN}${OTHER_CHAR_PATTERN}*`;
// regular expression to validate error codes
const ERROR_RE = new RegExp(`^#[\xa1\xbf]?${NAME_PATTERN}\\.?([_/]${OTHER_CHAR_PATTERN}+\\.?)*[!?]?$`);
// regular expression to validate functions
const FUNCTION_RE = new RegExp(`^${NAME_PATTERN}([._]${OTHER_CHAR_PATTERN}+)*\\.?$`);
// regular expression to validate region names for table ranges
const REGION_RE = /^#.+$/;
// regular expression to validate boolean names
const BOOLEAN_RE = new RegExp(`^${NAME_PATTERN}$`);
// regular expression to validate the RC prefix characters
const RC_PREFIX_RE = /^[A-Z]{2}$/;

// a map with descriptors for all expected resource entries per language
const RESOURCE_VALIDATION: Record<keyof DataResource, ResourceValidation> = {
    RC:         { dict: false, re: RC_PREFIX_RE, upper: true },
    booleans:   { dict: true,  re: BOOLEAN_RE,   upper: true, keys: ["t", "f"] },
    errors:     { dict: true,  re: ERROR_RE,     upper: true },
    regions:    { dict: true,  re: REGION_RE,    upper: false },
    functions:  { dict: true,  re: FUNCTION_RE,  upper: true, fileformat: true },
    cellParams: { dict: true,  re: null,         upper: false },
};

// class FormulaPluginHelper ==================================================

class FormulaPluginHelper extends PluginHelper {

    // paths to JSON schema files
    readonly #dataSchemaPath = resolvePath("./schemas/formulares-data-schema.json", import.meta.url);
    readonly #helpSchemaPath = resolvePath("./schemas/formulares-help-schema.json", import.meta.url);
    readonly #mappingSchemaPath = resolvePath("./schemas/formulares-mapping-schema.json", import.meta.url);

    // constructor ------------------------------------------------------------

    constructor(options: FormulaResourcePluginOptions) {
        super({
            pluginIndex: import.meta.url,
            loggerPrefix: "fmla",
            logLevelEnvVar: "PLUGIN_FORMULA_RESOURCE_LOGLEVEL",
            cacheSrcFiles: options.srcPath + "/**/*.{json,yaml,yml}",
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Generates an ES source module for a formula resource of a specific
     * language.
     *
     * @param modulePath
     *  The path of the resource module. The file base name must be a lowercase
     *  language identifier, e.g. "path/to/de.json".
     *
     * @returns
     *  The source module exporting a JSON formula resource object.
     */
    async generateResourceModule(modulePath: string): Promise<ModuleDescriptor> {
        this.info("generating module %f", modulePath);

        // bare filename used for different subdirectories
        const fileName = basename(modulePath);

        // check file name (must be lowercase language code)
        const lang = basename(modulePath, extname(modulePath));
        this.ensure(/^[a-z]{2,3}$/.test(lang), "invalid file name for formula resource (language expected): %f", fileName);

        // try to resolve cached version of generated file
        return await this.generateModule(fileName, async () => {

            // regular expression for locale identifiers with region
            const LOCALE_RE = new RegExp(`^${lang}_[A-Z]{2,3}$`);

            // paths to help resource and key mapping files for the current data resource (may be JSON or YAML independent from data file)
            const helpPath = await resolveConfigPath(`../help/${lang}`, modulePath);
            const mappingPath = await resolveConfigPath(`../mapping/${lang}`, modulePath);

            // read and validate the resource file
            const dataDict = await this.#readResource(modulePath);
            this.ensure(lang in dataDict, "missing map for language '%s' in resource, parsing %f", lang, modulePath);

            // read and validate the help resource file
            const helpDict = await this.#readHelpResource(helpPath);

            // help resources may be missing completely
            const mappingDict = helpDict && await (async () => {

                // read and validate the key mapping file for help resources (required for existing help)
                const mappingDict = await this.#readHelpKeyMapping(mappingPath);

                // resolve references to other mapping files
                for (const [locale, mapping] of dictEntries(mappingDict)) {
                    if (!locale.startsWith("$") && (typeof mapping === "string")) {
                        const [path, anchor = ""] = mapping.split("#");
                        const linkedPath = resolvePath(path, mappingPath);
                        const linkedDict = await this.#readHelpKeyMapping(linkedPath);
                        const linkedMapping = anchor.startsWith("/") ? linkedDict[anchor.slice(1)] : undefined;
                        this.ensureDict(linkedMapping, "cannot resolve mapping link %f, parsing %f", mapping, mappingPath);
                        mappingDict[locale] = linkedMapping;
                    }
                }

                return mappingDict as Dict<HelpKeyMapping>;
            })();

            // process all locales in the map
            for (const [locale, data] of dictEntries(dataDict)) {

                // ignore meta entries (e.g. "$schema"), check for valid locale IDs
                if (locale.startsWith("$")) { delete dataDict[locale]; continue;  }
                this.ensure((locale === lang) || LOCALE_RE.test(locale), "invalid resource key '%s', expecting the language or a matching locale code", locale);

                // process the different resource entries (may be single strings,
                // e.g. for boolean names, or key/name maps, e.g. for function names)
                for (const [key1, entry] of dictEntries(data)) {
                    const logPath1 = `${modulePath}#/${locale}/${key1}`;
                    const settings = RESOURCE_VALIDATION[key1];

                    // process the key/name maps for error codes and function names
                    if (settings.dict) {
                        this.ensureDict(entry, "invalid resource entry, expecting a key/name map at %f", logPath1);

                        // check for duplicates in the translated labels
                        const usedValues = new Set<string>();

                        // check the validity of the unique resource keys
                        for (const [key2, value] of dictEntries(entry)) {
                            const logPath2 = `${logPath1}/${key2}`;
                            const validKey = settings.keys ? settings.keys.includes(key2) : KEY_RE.test(key2);
                            this.ensure(validKey, "invalid key %f", logPath2);

                            // entries may be dictionaries with different values for the file formats
                            if (settings.fileformat && isDict(value)) {
                                for (const format of ["ooxml", "odf"] as const) {
                                    this.#checkValue(settings, value[format], `${logPath2}/${format}`);
                                }
                            } else {
                                this.#checkValue(settings, value, logPath2);
                                this.ensure(!usedValues.has(value), "duplicate translated name '%s' at %f", value, logPath2);
                                usedValues.add(value);
                            }
                        }
                    } else {
                        // other entries must be simple strings
                        this.#checkValue(settings, entry, logPath1);
                    }
                }

                // process help resources if available
                const helpData = helpDict?.[locale];
                const logPath1 = `${helpPath}#/${locale}`;
                if (!helpData) {
                    this.warn("missing help resources at %f", logPath1);
                    continue;
                }

                // key mapping must exist for all help resources
                const mappingData = mappingDict?.[locale];
                const logPath2 = `${mappingPath}#/${locale}`;
                this.ensure(mappingData, "missing help key mapping at %f", logPath2);

                // swap keys and names, check for duplicates in the localized function names
                const keyMapping = new Map<string, string>();
                for (const [key, entry] of dictEntries(mappingData)) {
                    const logPath3 = `${logPath2}/${key}`;
                    for (const [index, name] of wrapArray(entry).entries()) {
                        this.ensure(!keyMapping.has(name), "duplicate function name '%s' at %f", name, `${logPath3}[${index}]`);
                        keyMapping.set(name, (index === 0) ? key : "");
                    }
                }

                // process the help entries of all functions
                interface HelpParamEntry { n: string; d: string }
                interface HelpEntry { d: string; p: HelpParamEntry[] }
                const newHelpData = Object.create(null) as Dict<HelpEntry>;
                const usedNames = new Map<string, string>();
                for (const [name, entry] of dictEntries(helpData)) {
                    const key = keyMapping.get(name) ?? name;
                    if (!key) { continue; } // help entries to be ignored
                    const logPath3 = `${logPath1}/${name}`;
                    this.ensure(key in data.functions, "help entry found for unknown function at %f", logPath3);
                    this.ensure(!usedNames.has(key), "duplicate help entry at %f, mapped by %f", logPath3, `${logPath2}/${key}`);
                    this.check(entry.description, "missing function description at %f", logPath3);
                    this.ensure(entry.params.length === entry.paramshelp.length, "parameter array length mismatch at %f", logPath3);
                    const params = entry.params.map((param, index) => {
                        const desc = entry.paramshelp[index];
                        this.check(param, "missing parameter name at %f", `${logPath3}/params[${index}]`);
                        this.check(desc, "missing description at %f", `${logPath3}/paramshelp[${index}]`);
                        return { n: param || "", d: desc || "" };
                    });
                    newHelpData[key] = { d: entry.description || "", p: params };
                    usedNames.set(key, name);
                }

                // insert the help map as property "help" into the resource data
                (data as unknown as Dict).help = newHelpData;
            }

            // serialize JSON data (Vite/Rollup will convert modules with ".json" extension by themselves)
            const dataMarkup = JSON.stringify(dataDict);
            this.info("module %f generated successfully", modulePath);
            return dataMarkup;
        });
    }

    // private methods --------------------------------------------------------

    // reads and validates a resource file
    async #readResource(path: string): Promise<Dict<DataResource>> {
        return await this.readConfig(path, { schema: this.#dataSchemaPath });
    }

    // reads and validates a help resource file
    async #readHelpResource(path: string): Promise<Opt<Dict<HelpResource>>> {
        return await this.readConfig(path, { schema: this.#helpSchemaPath, optional: true });
    }

    // reads and validates a key mapping file for help resources
    async #readHelpKeyMapping(path: string): Promise<Dict<string | HelpKeyMapping>> {
        return await this.readConfig<Dict<string | HelpKeyMapping>>(path, { schema: this.#mappingSchemaPath });
    }

    // checks the validity of the passed name, according to the regular expression for the passed map entry
    #checkValue(settings: ResourceValidation, value: unknown, path: string): asserts value is string {
        this.ensureStr(value, "invalid resource entry, expecting a string value at %f", path);
        this.ensure(!settings.re || settings.re.test(value), "invalid name '%s' for resource entry at %f", value, path);
        this.ensure(!settings.upper || (value === value.toUpperCase()), "invalid resource entry, expecting an uppercase name at %f", path);
    }
}

// plugin =====================================================================

/**
 * A plugin for Vite to generate localized text resources for spreadsheet
 * formulas, such as function names, error codes, and tooltip help texts for
 * sheet functions and their parameters.
 *
 * @param config
 *  The plugin configuration object.
 *
 * @returns
 *  The plugin instance.
 */
export default function formulaResources(config: FormulaResourcePluginOptions): Plugin {

    // resolved configuration options
    const { srcPath } = config;

    // helper instance for file system access, logging, etc.
    const helper = new FormulaPluginHelper(config);

    // glob pattern for main data files to be handled by the plugin
    const dataGlob = `${srcPath}/data/*.{json,yaml,yml}`;

    // create and return the plugin object
    return {
        name: "@open-xchange/vite-plugin-formula-resource",

        // initialize file system cache for generated modules
        async configResolved(viteConfig) {
            await helper.initializeCache(viteConfig);
        },

        // load the file contents, generate final JSON data
        async load(moduleId) {

            // match module identifier against glob pattern, reduce to path relative to CWD
            const modulePath = helper.matchModuleId(moduleId, dataGlob);
            if (!modulePath) { return; }

            // generate the ES source module for the formula resource
            return await helper.generateResourceModule(modulePath);
        },
    };
}
