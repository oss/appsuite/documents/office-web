/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { utils, stylelint } from "@open-xchange/linter-presets";

// functions ==================================================================

const resolve = utils.resolver(import.meta.url);

// exports ====================================================================

export default stylelint.configure({

    license: resolve("./license-header"),

    rules: {
        "no-duplicate-selectors": null, // too many hits in existing code
        "property-no-vendor-prefix": [true, { ignoreProperties: ["appearance", "user-select"] }], // TODO: still needed?
        "selector-class-pattern": null, // too many hits in existing code
        "selector-no-vendor-prefix": [true, { ignoreSelectors: ["::-moz-selection", /-full-?screen$/] }],
    },
});
