/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

const { process, performance, setTimeout } = globalThis;
const { round, floor, max } = Math;

// constants ==================================================================

const SP = " ".repeat(60);
const DASH = "-".repeat(60);

const TIME_UNITS = {
    ms: { factor: 1e3, name: "millisecond" },
    us: { factor: 1e6, name: "microsecond" },
    ns: { factor: 1e9, name: "nanosecond" },
};

// Node's high-resolution time
const hrtime = process?.hrtime;

const runnerCache = new Map/*<TestRunner>*/();

// private functions ==========================================================

/**
 * Returns the current timestamp handle, according to the measurement method of
 * the environment.
 *
 * @returns {number}
 *  The current timestamp handle.
 */
const now = hrtime || (performance ? performance.now.bind(performance) : Date.now);

/**
 * Returns the time elapsed since the passed timestamp, in nanoseconds, as
 * precise as possible.
 *
 * @param {number} t0
 *  A timestamp returned from the function `now()`.
 *
 * @returns {number}
 *  The elapsed time, in nanoseconds, as precise as possible.
 */
const duration = hrtime ?
    t0 => { const [s, n] = hrtime(t0); return s * 1e9 + n; } :
    t0 => round((now() - t0) * 1e6);

/**
 * Returns a promise that will fulfil after the specified delay.
 *
 * @param {number} ms
 *  The delay, in milliseconds.
 *
 * @returns {Promise<void>}
 *  A promise that will fulfil after the specified delay.
 */
function pause(ms) {
    return new Promise(resolve => { setTimeout(resolve, ms); });
}

/**
 * Pads the passed text with trailing space characters.
 */
function left(text, width, sep) {
    return (text + SP).slice(0, width) + (sep ? "|" : " ");
}

/**
 * Pads the passed text with space characters at both sides.
 */
function center(text, width, sep) {
    return (SP + text + SP).substr(SP.length - floor((width - text.length) / 2), width) + (sep ? "|" : " ");
}

/**
 * Pads the passed text with leading space characters.
 */
function right(text, width, sep) {
    return (SP + text).slice(-width) + (sep ? "|" : " ");
}

/**
 * Parses reduced Markdown mark-up into a simple syntax tree.
 *
 * @param {string} markup
 *  The Markdown mark-up to be parsed.
 *
 * @returns {ParagraphNode[]}
 *  An array of paragraph nodes describing the contents of each paragraph. Each
 *  paragraph node contains a `nodes` array property with text nodes. Each text
 *  node contains the properties `text` (string), `code` (boolean), and `url`
 *  (optional, text).
 */
function parseMD(markup) {
    return markup.split("\n\n").map(paragraph => {
        paragraph = paragraph.replace(/^\s+|\s+$/g, "").replace(/\s+/g, " ");
        const nodes = [];
        paragraph.split(/(`.*?`)/g).forEach((s1, i1) => {
            const code = !!(i1 & 1);
            s1 = code ? s1.slice(1, -1) : s1;
            s1.split(/(\[.*?\]\(.*?\))/g).forEach((s2, i2) => {
                let text = s2;
                let url = null;
                if (i2 & 1) {
                    const matches = s2.match(/^\[(.*?)\]\((.*?)\)$/);
                    text = matches[1];
                    url = matches[2];
                }
                nodes.push({ text, code, url });
            });
        });
        if (nodes[0].text.match(/^[-*] ./)) { nodes[0].text = `\u2022${nodes[0].text.slice(1)}`; }
        return { nodes };
    });
}

// class TestRunner ===========================================================

export class TestRunner {

    // static functions -------------------------------------------------------

    /**
     * Returns a cached instance, or creates a new instance if not available,
     * and calls the passed configuration callback function.
     *
     * @param {string} key
     *  The unique key of the performance test (used as cache key).
     *
     * @param {TestRunnerConfig} config
     *  The test runner configuration:
     *  - {string} title -- The title of the test, to be used in the UI (e.g.
     *    as headline or button label).
     *  - {string} [description] -- A detailed description of the test.
     *  - {(runner: TestRunner) => void} initialize -- A configuration function
     *    that will be called with the `TestRunner` instance, and in turn must
     *    invoke its methods to configure the test runner.
     */
    /*public*/ static create(key, config) {
        let runner = runnerCache.get(key);
        if (!runner) {
            runner = new TestRunner(config);
            runnerCache.set(key, runner);
        }
        return runner;
    }

    // constructor ------------------------------------------------------------

    /*public*/ constructor(config) {

        // initialize properties
        this._title = config.title;
        this._description = config.description;
        this._registry = [];
        this._argsName = "";
        this._runArgs = [];
        this._argsGen = arg => [arg];
        this._unit = "us";
        this._hrows = 1;
        this._cols = [];
        this._doc = null;
        this._cancel = false;

        // invoke callback for test configuration
        config.initialize(this);

        // collect settings for all table columns
        const pushCol = (name, width, options) => {
            const headers = name.split("|").map(n => Object.assign({ name: n, width }, options));
            this._cols.push(Object.assign({ headers, width }, options));
            this._hrows = max(this._hrows, headers.length);
        };
        pushCol(this._argsName, 10, { sep: true, rspan: true });
        for (const entry of this._registry) {
            pushCol(entry.name, 8, { sep: entry.sep });
        }

        // adjust settings for multiple header rows
        if (this._hrows > 1) {
            for (const col of this._cols) {
                while (col.headers.length < this._hrows) {
                    col.headers.unshift(col.rspan ? col.headers[0] : null);
                }
            }
            for (let c = 1, cols = this._cols.length; c < cols; c += 1) {
                const col = this._cols[c];
                for (let r = 0, rows = this._hrows; r < rows; r += 1) {
                    const header = col.headers[r];
                    if (!header) { continue; }
                    let c2 = c + 1;
                    while ((c2 < cols) && !this._cols[c2].headers[r]) {
                        header.width += this._cols[c2].width + 1;
                        header.sep = this._cols[c2].sep;
                        c2 += 1;
                    }
                    const cspan = c2 - c;
                    if (cspan > 1) { header.cspan = cspan; }
                }
            }
        }

        // text for separator line
        this._sep = this._cols.map(col => DASH.substr(-col.width) + (col.sep ? "+" : "-")).join("");
    }

    // public methods ---------------------------------------------------------

    /**
     * Sets the time unit to be shown in the result table. By default, the
     * results will be shown in microseconds.
     *
     * @param {"ms"|"us"|"ns"} unit
     *  The time unit to be used:
     *  - "ms" for milliseconds,
     *  - "us" for microseconds,
     *  - "ns" for nanoseconds.
     */
    /*public*/ setTimeUnit(unit) {
        this._unit = unit;
    }

    /**
     * Sets the arguments to be passed to the different test runs.
     *
     * @param {string} name
     *  The name of the test run arguments. Will appear as title of the leading
     *  column in the result table.
     *
     * @param {unknown[]} args
     *  The arguments for the test runs. Each argument in this array will be
     *  passed to the arguments generator function (if registered, see method
     *  `setArgumentsGenerator()`), and the resulting arguments will be passed
     *  to each registered test function. The measured times for all test runs
     *  will be collected and rendered later.
     */
    /*public*/ registerRuns(name, args) {
        this._argsName = name;
        this._runArgs = args;
    }

    /**
     * Registers a test function to be profiled.
     *
     * @param {string} name
     *  The name of the test. Will appear as column title in the result table.
     *
     * @param {(...args: unknown[]) => void} fn
     *  The test function to be profiled.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {(...args: unknown[]) => unknown[]} [options.initFn] -- A callback
     *    function that will be called before starting the test function. Can
     *    be used to individually convert the arguments created by the
     *    arguments generator function for the current test run. This code will
     *    not be profiled.
     *  - {boolean} [options.nosep=false] -- If set to `true`, the table column
     *    will not be separated with a vertical line to its right neighbor
     *    (console only).
     */
    /*public*/ registerTest(name, fn, options) {
        const sep = !(options && options.nosep);
        this._registry.push({ name, fn, init: options && options.initFn, sep });
    }

    /**
     * Sets a generator function for the arguments to be passed to the test
     * functions in each test run. The arguments will be generated once for a
     * test run, and will then be passed to all registered test functions in
     * that test run.
     *
     * If no generator function will be set, the default generator function
     * will forward the original argument of each test run to the test
     * functions unmodified.
     *
     * @param {(arg: unknown) => unknown[]} fn
     *  The new generator function. Receives the original argument of the test
     *  run, and must return an array of arguments to be passed to all
     *  registered test functions.
     */
    /*public*/ setArgumentsGenerator(fn) {
        this._argsGen = fn;
    }

    /**
     * Runs all registered test functions with all test arguments, and prints
     * all results to the global console.
     *
     * @param {HTMLElement} [outNode]
     *  If specified, a `<table>` element with the test results will be
     *  generated and inserted into this element. If omitted, the test results
     *  will be written to the global console only.
     *
     * @returns {Promise<void>}
     *  A promise that will fulfil when all tests have been executed.
     */
    /*public*/ async runTests(outNode) {

        // reset cancel token
        this._cancel = false;

        // print introduction to the console
        console.log("");
        console.log(`*** ${this._title} ***`);
        if (this._description) {
            console.log("");
            this._printMD(this._description);
        }

        // print table header to console
        console.log("");
        for (let row = 0, rows = this._hrows; row < rows; row += 1) {
            console.log(this._cols.map(col => {
                const header = col.headers[row];
                if (!header) { return; }
                const name = (col.rspan && row + 1 < rows) ? "" : header.name;
                return center(name, header.width, header.sep);
            }).join(""));
        }
        console.log(this._sep);

        // find the document root
        this._doc = outNode;
        while (this._doc && this._doc.nodeType !== 9) {
            this._doc = this._doc.parentNode;
        }

        // render introduction into DOM
        if (outNode) {
            this._element(outNode, "h4").textContent = this._title;
            if (this._description) {
                this._renderMD(outNode, this._description);
            }
        }

        // do not show the description multiple times
        delete this._description;

        // initialize the DOM <table> element
        let tbody = null;
        if (outNode) {
            const table = this._element(outNode, "table");
            const thead = this._element(table, "thead");
            for (let r = 0, rows = this._hrows; r < rows; r += 1) {
                const trow = this._element(thead, "tr");
                for (const col of this._cols) {
                    const header = col.headers[r];
                    if (!header || (col.rspan && r > 0)) { continue; }
                    const tcell = this._cell(trow, header.name, true);
                    if (col.rspan && rows > 1) { tcell.rowSpan = rows; }
                    if (header.cspan) { tcell.colSpan = header.cspan; }
                }
            }
            tbody = this._element(table, "tbody");
        }

        // execute all registered tests for every test argument
        for (const runArg of this._runArgs) {
            await this._runTest(runArg, tbody);
            if (this._cancel) { break; }
        }

        // final message according to cancel token
        const msg = this._cancel ? "Test canceled." :
            `Done. All results in ${TIME_UNITS[this._unit].name}s per call.`;

        // print table footer to console
        console.log(this._sep);
        console.log("");
        console.log(msg);

        // render final message into DOM
        if (outNode) {
            this._element(outNode, "p").textContent = msg;
        }
    }

    /**
     * Cancels the running tests. The promise returned by `runTests()` will
     * fulfil when the running tests have been actually canceled.
     */
    /*public*/ cancelTests() {
        this._cancel = true;
    }

    // private methods --------------------------------------------------------

    /**
     * Runs all registered test functions once with a test argument.
     *
     * @param {unknown} runArg
     *  The base argument for the current test run.
     *
     * @param {HTMLTableSectionElement} [tbody]
     *  The `<tbody>` element of the DOM table to be filled with a `<tr>`
     *  element containing the results of this test run.
     *
     * @returns {Promise<void>}
     *  A promise that will fulfil when all tests have been executed.
     */
    /*private*/ async _runTest(runArg, tbody) {

        // the text line to be printed
        let line = this._data(0, runArg);

        // the table row element
        const trow = tbody ? this._element(tbody, "tr") : null;
        if (trow) { this._cell(trow, runArg); }

        // generate the arguments to be passed to the test functions
        const runArgs = this._argsGen(runArg);

        // process all test functions
        for (let col = 0; col < this._registry.length; col += 1) {
            const entry = this._registry[col];
            const fnArgs = entry.init ? entry.init(...runArgs) : runArgs;
            const result = this._profile(() => entry.fn(...fnArgs));
            line += this._data(col + 1, result);
            if (trow) { this._cell(trow, result); }
            await pause(20);
            if (this._cancel) { return; }
        }

        console.log(line);
        await pause(50);
    }

    /**
     * Calls the passed function repeatedly for at least 50 milliseconds.
     *
     * @param {() => void} fn
     *  The function to be called repeatedly.
     *
     * @returns
     *  The arithmetic mean of the duration of a single run, in the configured
     *  time unit.
     */
    /*private*/ _profile(fn) {
        let d = 0, cycles = 0, limit = 1;
        const t0 = now();
        // keep running for at least 50ms
        while (d < 5e7) {
            for (let i = 0; i < limit; i += 1) { fn(); }
            d = duration(t0);
            cycles += limit;
            limit += 1;
        }
        // return arithmetic mean
        const quotient = 1e9 / TIME_UNITS[this._unit].factor;
        return round(d / cycles / quotient);
    }

    /**
     * Converts the passed value to a string, and pads it with spaces according
     * to the data type (numbers will be right-aligned).
     */
    /*private*/ _data(col, value) {
        const fn = (typeof value === "number") ? right : left;
        const entry = this._cols[col];
        return fn(String(value), entry.width, entry.sep);
    }

    /**
     * Creates a new DOM element.
     */
    /*private*/ _element(parent, tag, cls) {
        const el = this._doc.createElement(tag);
        if (cls) { el.className = cls; }
        parent.appendChild(el);
        return el;
    }

    /**
     * Creates a new `<td>` or `<th>` DOM element.
     */
    /*private*/ _cell(parent, value, head) {
        const cls = head ? "c" : (typeof value === "number") ? "r" : "l";
        const el = this._element(parent, head ? "th" : "td", cls);
        el.textContent = String(value);
        return el;
    }

    /**
     * Parses reduced Markdown mark-up to a plain-text string, and prints that
     * to the global console.
     *
     * @param {string} markup
     *  The Markdown mark-up to be parsed.
     */
    /*private*/ _printMD(markup) {
        parseMD(markup).forEach(paragraph => {
            const text = paragraph.nodes.map(node => node.url ? `${node.text} (${node.url})` : node.text).join("");
            console.log(text);
        });
    }

    /**
     * Renders reduced Markdown mark-up into a DOM element.
     *
     * @param {HTMLElement} parent
     *  The DOM element to be filled.
     *
     * @param {string} markup
     *  The Markdown mark-up to be parsed.
     */
    /*private*/ _renderMD(parent, markup) {
        parseMD(markup).forEach(paragraph => {
            const p = this._element(parent, "p");
            paragraph.nodes.forEach(node => {
                let el;
                if (node.url) {
                    el = this._element(p, "a");
                    el.href = node.url;
                } else {
                    el = this._element(p, "span");
                }
                el.textContent = node.text;
                if (node.code) { el.classList.add("m"); }
            });
        });
    }
}
