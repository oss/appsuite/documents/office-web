# Performance Tests

This directory is a playground for simple low-level performance tests that can run in a shell (with NodeJS), or in the browser. Each subdirectory contains tests for one specific topic.

## Shell

To run a performance test in the shell:

* Open a shell.
* Change to the `perf` directory (containing this README file).
* Type `node run <testname>` (for example: `node run array`).

Technically, the tests can be started from everywhere (for example: `node /abs/path/to/office-web/ui/perf/run array`).

## Browser

To run a performance test in the browser:

* Open the file `index.html` from the `perf` directory (containing this README file).
* Press the start button of a test.
