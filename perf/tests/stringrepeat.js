/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

export const title = "String.repeat";

export const description = `
This test measures the performance of \`String#repeat\` with different poly-fills.

* Table column "Native" tests the native string method.

* Table column "Recursive" tests an algorithm using exponentially growing string repetition.

* Table column "Linear" tests an algorithm using a simple linear loop.

* Table column "Array.join" tests the "new-Array-join" hack.
`;

export function initialize(runner) {

    // test runner configuration ----------------------------------------------

    // show results in nanoseconds
    runner.setTimeUnit("ns");

    // test runs: number of characters to be repeated
    runner.registerRuns("Count", [1, 2, 5, 10, 20, 50, 100, 200, 500, 1e3, 2e3, 5e3, 1e4, 2e4, 5e4, 1e5, 2e5, 5e5, 1e6]);

    // test column: native method `String#repeat`
    runner.registerTest("Native", count => "a".repeat(count));

    // test column: recursive repetition
    runner.registerTest("Recursive", count => {
        let result = "";
        while (count > 0) {
            if (count & 1) { result += "a"; }
            result += result;
            count >>= 1;
        }
        return result;
    });

    // test column: linear loop
    runner.registerTest("Linear", count => {
        let result = "";
        while (count > 0) {
            result += "a";
            count -= 1;
        }
        return result;
    });

    // test column: new-Array-join hack
    runner.registerTest("Array.join", count => new Array(count + 1).join("a"));
}
