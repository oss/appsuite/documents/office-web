/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

export const title = "String.fromCodePoint";

export const description = `
This test measures the performance of \`String.fromCodePoint\` with different polyfills.

* Table column "Native" tests the native string function.

* Table column "Polyfill" tests an algorithm that adds the encoded surrogate pairs directly to the result string.

* Table column "Extern" tests a public [NPM polyfill](https://github.com/mathiasbynens/String.fromCodePoint) that uses
a temporary array of numbers to buffer the encoded UTF-16 code units or surrogate pairs, and occasionally pushes the
buffer to the result string by calling \`String.fromCharCode.apply()\`.
`;

export function initialize(runner) {

    // imports ----------------------------------------------------------------

    const { fromCharCode } = String;
    const { floor, random } = Math;

    // test runner configuration ----------------------------------------------

    // test runs: number of code point values to be passed to the test functions
    runner.registerRuns("Length", [100, 300, 1000, 3000, 10000, 30000]);

    // test column: native function `String.fromCodePoint()`
    runner.registerTest("Native", String.fromCodePoint);

    // test column: own polyfill without array buffer
    runner.registerTest("Polyfill", (...codes) => {
        let text = "";
        for (let code of codes) {
            if ((code < 0) || (code > 0x10FFFF)) { throw new RangeError("invalid code point"); }
            if (code <= 0xFFFF) {
                text += fromCharCode(code);
            } else {
                code -= 0x10000;
                text += fromCharCode((code >> 10) + 0xD800, (code & 0x3FF) + 0xDC00);
            }
        }
        return text;
    });

    // test column: polyfill found in NPM with array buffer
    runner.registerTest("Extern", (...codes) => {
        let text = "";
        const buf = [];
        for (let code of codes) {
            if ((code < 0) || (code > 0x10FFFF)) { throw new RangeError("invalid code point"); }
            let len;
            if (code <= 0xFFFF) {
                len = buf.push(fromCharCode(code));
            } else {
                code -= 0x10000;
                len = buf.push(fromCharCode((code >> 10) + 0xD800, (code & 0x3FF) + 0xDC00));
            }
            if (len >= 0x3FFF) {
                text += fromCharCode(...buf);
                buf.length = 0;
            }
        }
        return text + fromCharCode(...buf);
    });

    // arguments generator: converts the number of code points (type `number`) to a sequence
    // of code points with a few large values resulting in surrogate pairs
    runner.setArgumentsGenerator(len => {
        const codes = [];
        for (let i = 0; i < len; i += 1) {
            let code = floor(random() * 65530) + 1;
            if (random() < 0.1) { code += 0x20000; }
            codes.push(code);
        }
        return codes;
    });
}
