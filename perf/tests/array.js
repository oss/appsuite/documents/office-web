/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

export const title = "Array insert/remove";

export const description = `
This test measures the performance of element insertions and removals in plain \`Array\`s and \`Uint32Array\`s with
different algorithms.

* For class \`Array\`, it compares \`Array#splice\` with \`Array#copyWithin\` and element relocations using a \`for\`
loop.

* For class \`Uint32Array\`, it compares \`Array#copyWithin\` with element relocations using a \`for\` loop. Because
typed arrays are fixed-size, insertion of elements may need to create a new instance.
`;

export function initialize(runner) {

    // imports ----------------------------------------------------------------

    const { max, ceil } = Math;

    // mix-in classes ---------------------------------------------------------

    /**
     * Mix-in class that defines the helper test interface (methods `_expand()`
     * and `_shrink()`).
     *
     * @param {Function} BaseClass
     *  The base array class to be extended. MUST contain the implementation
     *  method `_realloc()`.
     */
    function mixExt(BaseClass) {
        return class extends BaseClass {
            _expand() {
                if (this.a.length === this.l) { this._realloc(); }
            }
            _shrink() {
                if ((this.a.length >= 30) && (this.l * 3 < this.a.length)) { this._realloc(); }
            }
        };
    }

    /**
     * Mix-in class that defines the common test interface (methods `insert()`
     * and `remove()`) using the array method `copyWithin()` internally.
     *
     * @param {Function} BaseClass
     *  The base class to be extended. MUST contain the implementation methods
     *  `_expand()` and `_shrink()`.
     */
    function mixCopy(BaseClass) {
        return class extends BaseClass {
            insert(i, v) {
                this._expand();
                this.a.copyWithin(i + 1, i, this.l);
                this.a[i] = v;
                this.l += 1;
            }
            remove(i) {
                this.a.copyWithin(i, i + 1, this.l);
                this.l -= 1;
                this._shrink();
            }
        };
    }

    /**
     * Mix-in class that defines the common test interface (methods `insert()`
     * and `remove()`) using a for-loop for element shifting internally.
     *
     * @param {Function} BaseClass
     *  The base class to be extended. MUST contain the implementation methods
     *  `_expand()` and `_shrink()`.
     */
    function mixLoop(BaseClass) {
        return class extends BaseClass {
            insert(i, v) {
                this._expand();
                const a = this.a;
                for (let j = this.l; j > i; j -= 1) { a[j] = a[j - 1]; }
                a[i] = v;
                this.l += 1;
            }
            remove(i) {
                const a = this.a;
                for (let j = i + 1, l = this.l; j < l; j += 1) { a[j - 1] = a[j]; }
                this.l -= 1;
                this._shrink();
            }
        };
    }

    // class SpliceArray ------------------------------------------------------

    /**
     * An array class with common test interface (methods `insert()` and
     * `remove()`) using the array method `splice()` internally.
     */
    class SpliceArray {
        constructor(n) {
            this.a = new Array(n).fill(0);
        }
        insert(i, v) {
            this.a.splice(i, 0, v);
        }
        remove(i) {
            this.a.splice(i, 1);
        }
    }

    // class ExtArray ---------------------------------------------------------

    const ExtArray = mixExt(class {
        constructor(n) {
            this.a = new Array(n).fill(0);
            this.l = n;
        }
        _realloc() {
            this.a.length = ceil(this.l * 1.5);
        }
    });

    const CopyArray = mixCopy(ExtArray);
    const LoopArray = mixLoop(ExtArray);

    // class ExtArray32 -------------------------------------------------------

    const ExtArray32 = mixExt(class {
        constructor(n) {
            this.a = new Int32Array(max(n, 20));
            this.l = n;
        }
        _realloc() {
            const a = this.a;
            this.a = new Int32Array(ceil(this.l * 1.5));
            this.a.set(a.subarray(0, this.l));
        }
    });

    const CopyArray32 = mixCopy(ExtArray32);
    const LoopArray32 = mixLoop(ExtArray32);

    // test runner configuration ----------------------------------------------

    const TEST_SPECS = [
        { name: "Array|splice",        ArrayClass: SpliceArray },
        { name: "copyWithin",          ArrayClass: CopyArray },
        { name: "for-loop",            ArrayClass: LoopArray },
        { name: "I32Array|copyWithin", ArrayClass: CopyArray32 },
        { name: "for-loop",            ArrayClass: LoopArray32 },
    ];

    // test columns: each array test sonsists of an "insert" test and a "remove" test
    TEST_SPECS.forEach(({ name, ArrayClass }) => {

        // cached array instance (reuse filled array in "remove" test)
        let array = null;

        // test column: insert an element into the array
        runner.registerTest(`${name}|insert`, arr => arr.insert(42, 42), {
            initFn: len => { array = new ArrayClass(len); return [array]; },
            nosep: true,
        });

        // test column: remove an element from the array
        runner.registerTest("remove", arr => arr.remove(42), {
            initFn: () => [array],
        });
    });

    // test runs: different array sizes
    runner.registerRuns("Length", [1e3, 2e3, 5e3, 1e4, 2e4, 5e4, 1e5, 2e5, 5e5, 1e6, 2e6, 5e6, 1e7]);
}
