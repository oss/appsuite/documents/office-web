/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

export const title = "SubStringMatch";

export const description = `
This test measures the performance of matching an input string against a list of symbols. Every symbol may only match partially at the beginning.
`;

export function initialize(runner) {

    // test runner configuration ----------------------------------------------

    // show results in nanoseconds
    runner.setTimeUnit("ns");

    // test runs: the input string to be matched
    runner.registerRuns("Input", ["Jan", "February", "Mar", "april", "wrong", "Jun", "July", "Aug", "Septem", "Oct", "November", "Dec"]);

    // the symbols to match the input against
    const SYMBOLS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    const symbols = [];
    for (const symbol of SYMBOLS) {
        const l = symbol.length;
        for (let i = Math.min(3, l); i <= l; i += 1) {
            symbols.push(symbol.slice(0, i).toLowerCase());
        }
    }
    symbols.sort((s1, s2) => s2.length - s1.length);

    // test column: regular expression
    const regExp = new RegExp("^(?:" + symbols.join("|") + ")");
    runner.registerTest("RegExp", input => regExp.exec(input.toLowerCase()));

    // test column: explicit match
    runner.registerTest("Compare", input => symbols.some(symbol => input.startsWith(symbol)));
}
