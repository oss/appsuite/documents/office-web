# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [8.34.0] - 2025-01-15

## [8.33.0] - 2024-12-18

### Added

- Translation updates for version 8.32 [`6d0c6bc`](https://gitlab.open-xchange.com/documents/office-web/commit/6d0c6bc1019ca0db1fa2d308b498b761fb885e4b)



## [8.32.0] - 2024-11-20

### Fixed

- Fix for issue #66: User gets better error message when using an insecure image URL [`060abca`](https://gitlab.open-xchange.com/documents/office-web/commit/060abcaf7e22b9775a318ec2b36c273c5622d74a)


## [8.31.0] - 2024-10-23

### Added

- Add variable PUBLISH_HELM_CHART to publish helm chart if no helm changes exist; add variable to allow failures on deploy to force publish chart/image [`5957cba`](https://gitlab.open-xchange.com/documents/office-web/commit/5957cba42fb35fab27aae0ba4b24196d6e781815)

### Fixed

- [`DOCS-5325`](https://jira.open-xchange.com/browse/DOCS-5325): Close software keyboard less often, when comment node has the focus [`735b908`](https://gitlab.open-xchange.com/documents/office-web/commit/735b90841892dcf6d698767020932d3e791ad269)
- [`DOCS-5328`](https://jira.open-xchange.com/browse/DOCS-5328): Do not offer document viewing during download [`3ad2bef`](https://gitlab.open-xchange.com/documents/office-web/commit/3ad2bef14c22d3daf1403092a650b140d8496803)

## [8.30.0] - 2024-09-25

### Fixed

- [`DOCS-5319`](https://jira.open-xchange.com/browse/DOCS-5319): Ignore event.shiftKey, if it was set by Safari browser on iPhone [`506d0c8`](https://gitlab.open-xchange.com/documents/office-web/commit/506d0c832d2ed56b8c557ad46f1785dde3a261b9)
- [`DOCS-5323`](https://jira.open-xchange.com/browse/DOCS-5323): Improve scroll position update in IOS Safari browser [`1b7e75d`](https://gitlab.open-xchange.com/documents/office-web/commit/1b7e75d17df0ded11ace9384c6921aae59809f50)
- [`DOCS-5327`](https://jira.open-xchange.com/browse/DOCS-5327): Improve positioning of blue keyboard button on mobile devices [`2095afc`](https://gitlab.open-xchange.com/documents/office-web/commit/2095afc0870726e0d897cd2ffff2f50bcb23f494)
- #66: Allow insertion of images &gt; 10 MB into the document with URL [`c54aedd`](https://gitlab.open-xchange.com/documents/office-web/commit/c54aedd61a3b200c84e38ee54df99c8693ea4996)


## [8.29.0] - 2024-08-29

### Added

- Translation updates for version 8.28 [`5307fcb`](https://gitlab.open-xchange.com/documents/office-web/commit/5307fcbca710552a968dcf4e9b177c7a721b3dff)

### Fixed

- [`DOCS-5250`](https://jira.open-xchange.com/browse/DOCS-5250): Open app in new tab without settings dialog [`88fab30`](https://gitlab.open-xchange.com/documents/office-web/commit/88fab30724095715e828302e1c8ddcaea3b7ad7d)
- [`DOCS-5324`](https://jira.open-xchange.com/browse/DOCS-5324): Global CSS class in presentation mode to hide popups [`beb5eba`](https://gitlab.open-xchange.com/documents/office-web/commit/beb5ebab705b660080e07f150679a9b70cf1d304)


## [8.28.0] - 2024-07-31

### Added

### Changed

### Fixed

- [`DOCS-5312`](https://jira.open-xchange.com/browse/DOCS-5312): Script error after silent reload [`8761015`](https://gitlab.open-xchange.com/documents/office-web/commit/8761015b0d696aa76be9875027cc384c9cebc273)
- [`DOCS-5313`](https://jira.open-xchange.com/browse/DOCS-5313): Tabtoolbar needs to wait for app construction [`7f9b4f2`](https://gitlab.open-xchange.com/documents/office-web/commit/7f9b4f22872e574ec938f5c5f199976b2fe41560)


## [8.27.0] - 2024-07-05


## [8.26.0] - 2024-06-05

### Fixed

- [`DOCS-5295`](https://jira.open-xchange.com/browse/DOCS-5295): Formula recalc problems in complex sheets [`f03374c`](https://gitlab.open-xchange.com/documents/office-web/commit/f03374c77b358355310b359eccec162b88d0035e)


## [8.25.0] - 2024-05-08

### Fixed



## [8.24.0] - 2024-04-04

### Added

- [`DOCS-5273`](https://jira.open-xchange.com/browse/DOCS-5273) [`246a936`](https://gitlab.open-xchange.com/documents/office-web/commit/246a9361fcef4613fcbb8b58f022c8256e2e3ffc)
- [`DOCS-5278`](https://jira.open-xchange.com/browse/DOCS-5278): Update ui translations 8.23 [`7323ffd`](https://gitlab.open-xchange.com/documents/office-web/commit/7323ffd2cd354c22c0fc9bd24057a310076d9e29)

### Fixed

- [`DOCS-5265`](https://jira.open-xchange.com/browse/DOCS-5265): Clipboard does not work on main [`f303894`](https://gitlab.open-xchange.com/documents/office-web/commit/f30389480779f0fb118b7dcb163120d981115bbd)


## [8.23.0] - 2024-03-08

### Added

- Add: DOCS-5132: User can use AI to create or rephrase text [`7ab99f04`](https://gitlab.open-xchange.com/documents/office-web/commit/7ab99f044d63ed7b066c16b3d9efcf1466346793)

### Removed

### Fixed



## [8.22.0] - 2024-02-08

### Fixed

- [`DOCS-5183`](https://jira.open-xchange.com/browse/DOCS-5183): Fix for cursor travelling in Chrome browser [`43b1d34`](https://gitlab.open-xchange.com/documents/office-web/commit/43b1d3437545b42a41004f73c54d0300ea256f85)
- [`DOCS-5207`](https://jira.open-xchange.com/browse/DOCS-5207): Context menu in documents portals does not update the section name [`272ffa6`](https://gitlab.open-xchange.com/documents/office-web/commit/272ffa6610ca196ac2cc437287b471525266e496)
- [`DOCS-5234`](https://jira.open-xchange.com/browse/DOCS-5234): No more type error after keydown event on slide pane [`887ab3f`](https://gitlab.open-xchange.com/documents/office-web/commit/887ab3ff11dbb041ff182bfafb2bd7b20463dae2)

## [8.21.0] - 2024-01-10

### Fixed

- [`DOCS-5197`](https://jira.open-xchange.com/browse/DOCS-5197): Typo in Danish translation [`ab5cecc`](https://gitlab.open-xchange.com/documents/office-web/commit/ab5cecc9171d199707affb417c7a41dba3b04946)

## [8.20.0] - 2023-12-01

- [`DOCS-5143`](https://jira.open-xchange.com/browse/DOCS-5143): prefer locale settings for region [`6ad0ef`](https://gitlab.open-xchange.com/documents/office-web/commit/6ad0efe2e3122c466e74a9cc2cba0fd080d3a58a)

## [8.19.0] - 2023-10-27

### Fixed

- [`DOCS-5101`](https://jira.open-xchange.com/browse/DOCS-5101): Change role and position of a11y alert node [`a0c2316`](https://gitlab.open-xchange.com/documents/office-web/commit/a0c2316fde7f8098c8dafb688e6408942c009769)
- [`DOCS-5102`](https://jira.open-xchange.com/browse/DOCS-5102): Repair broken event handling in slide change [`9e363b4`](https://gitlab.open-xchange.com/documents/office-web/commit/9e363b4c32cb572426808f41384f1c65b6260d79)
- [`DOCS-5073`](https://jira.open-xchange.com/browse/DOCS-5073): Remove invalid height restriction for line and arrow shapes [`a58ae48`](https://gitlab.open-xchange.com/documents/office-web/commit/a58ae48337e2a77aff020704522892f5164f183d)

## [8.18.0] - 2023-09-27

### Fixed

- [`DOCS-5065`](https://jira.open-xchange.com/browse/DOCS-5065): Present icon missing in viewer when opened from Mail [`b17a909`](https://gitlab.open-xchange.com/documents/office-web/commit/b17a9092a92c5e80ff13533b81c7e3121b70cbf3)
- [`DOCS-5037`](https://jira.open-xchange.com/browse/DOCS-5037): Mobile: Missing theming CSS for document apps in dark mode [`6bbeb15`](https://gitlab.open-xchange.com/documents/office-web/commit/6bbeb1587fdf8af589d47336385d8e2f747951db)

## [8.17.0] - 2023-08-31

### Fixed

- [`DOCS-4813`](https://jira.open-xchange.com/browse/DOCS-4813): Type error when closing document very quickly [`d2fdead`](https://gitlab.open-xchange.com/documents/office-web/commit/d2fdeade96e18960feae719c3fe917e163affb8c)
- [`DOCS-5009`](https://jira.open-xchange.com/browse/DOCS-5009): OpenBlank tab did not work correctly [`c97b64a`](https://gitlab.open-xchange.com/documents/office-web/commit/c97b64afb90932317ef98d1a0628c3718b28291a)
- [`DOCS-5047`](https://jira.open-xchange.com/browse/DOCS-5047): Retain column/row when traversing over merged ranges [`7c5aaec`](https://gitlab.open-xchange.com/documents/office-web/commit/7c5aaecaae8b5f3b69a86618095d2431e18c3972)
- [`DOCS-5053`](https://jira.open-xchange.com/browse/DOCS-5053): PreserveFileName must no be unset by URL flag [`b3cb072`](https://gitlab.open-xchange.com/documents/office-web/commit/b3cb072ed3f3b9f0d88915b653a2b4694def8320)


## [8.16.0] - 2023-08-03

### Added

### Fixed

- [`DOCS-4926`](https://jira.open-xchange.com/browse/DOCS-4926): Allow only specified drawing types [`2dbad85`](https://gitlab.open-xchange.com/documents/office-web/commit/2dbad85f26ddc4f1fe3eada1622da71b56d73f3e)
- [`DOCS-4927`](https://jira.open-xchange.com/browse/DOCS-4927): Escaping imageData sent from external operation [`7ac6f3d`](https://gitlab.open-xchange.com/documents/office-web/commit/7ac6f3d783a20a9a3f755d91743829f482bda8ee)
- [`DOCS-4928`](https://jira.open-xchange.com/browse/DOCS-4928): Modify image node creation to avoid XSS in image source [`dd6e898`](https://gitlab.open-xchange.com/documents/office-web/commit/dd6e898239a2644395588bcaa44ab793b14cdfae)
- [`DOCS-4967`](https://jira.open-xchange.com/browse/DOCS-4967): Spreadsheet: no autofill if selected cell is hidden [`7d9c930`](https://gitlab.open-xchange.com/documents/office-web/commit/7d9c93081f949f01756f6b026eb652162ef5c5ea)
- [`DOCS-4974`](https://jira.open-xchange.com/browse/DOCS-4974): Handle standard number format with range specifier [`9c6cee4`](https://gitlab.open-xchange.com/documents/office-web/commit/9c6cee4185dbb0e5f46263d5300ebaf714c267bb)
- [`DOCS-4983`](https://jira.open-xchange.com/browse/DOCS-4983): Restore focus in toolbar after async action [`201f8a1`](https://gitlab.open-xchange.com/documents/office-web/commit/201f8a11aa9a4ff73726908a8c284b353112d31f)


## [8.15.0] - 2023-07-06

### Fixed

- [`DOCS-4848`](https://jira.open-xchange.com/browse/DOCS-4848): Remove listeners on quit app [`cae9f96`](https://gitlab.open-xchange.com/documents/office-web/commit/cae9f966411738ed1960bf4b8933c3c407fc977e)
- [`DOCS-4880`](https://jira.open-xchange.com/browse/DOCS-4880): No announcement update when closing document [`282d20a`](https://gitlab.open-xchange.com/documents/office-web/commit/282d20a9b2567e3902096a1f5d4c75370f63feb3)
- [`DOCS-4894`](https://jira.open-xchange.com/browse/DOCS-4894): Switching from documents to mail makes header disappear [`b73f56f`](https://gitlab.open-xchange.com/documents/office-web/commit/b73f56f6c015a8509bd50a780a1af08e53435428)
- [`DOCS-4898`](https://jira.open-xchange.com/browse/DOCS-4898): Improve vertical scroll handling in Firefox browser [`7ab3e53`](https://gitlab.open-xchange.com/documents/office-web/commit/7ab3e5336ad0492d93a7b0ad84cac1c3dcea44d5)
- [`DOCS-4901`](https://jira.open-xchange.com/browse/DOCS-4901): Correct active pane in frozen split mode [`45a8187`](https://gitlab.open-xchange.com/documents/office-web/commit/45a818754e6ff47d4ec0aa53b5817a4879dfb4e2)
- [`DOCS-4910`](https://jira.open-xchange.com/browse/DOCS-4910): Improve cursor travelling in drive templates [`5c58230`](https://gitlab.open-xchange.com/documents/office-web/commit/5c58230b9b0b7d15fa9bdeda49cf995bb174b533)
- [`DOCS-4911`](https://jira.open-xchange.com/browse/DOCS-4911): Type error when copying comment while sidepane is hidden [`196d06a`](https://gitlab.open-xchange.com/documents/office-web/commit/196d06a5e9cb28551537b3dbccb197fd28db1119)
- [`DOCS-4913`](https://jira.open-xchange.com/browse/DOCS-4913): User can change slide layout, even when the master slide is empty (ODP) [`2374513`](https://gitlab.open-xchange.com/documents/office-web/commit/2374513dbfdc7a3b6c7ed2128586f0a9e8b885d4)
- [`DOCS-4916`](https://jira.open-xchange.com/browse/DOCS-4916): Setting correct number when inserting slide number field in ODP [`6f718c3`](https://gitlab.open-xchange.com/documents/office-web/commit/6f718c30d5e59d5c514a9c658506d10ec165e5e2)
- [`DOCS-4921`](https://jira.open-xchange.com/browse/DOCS-4921): Do not show hidden columns when inserting columns [`e067dc7`](https://gitlab.open-xchange.com/documents/office-web/commit/e067dc733623456f594dfd4a662eba00c1076673)
- [`DOCS-4924`](https://jira.open-xchange.com/browse/DOCS-4924): Type error after formula count exceeds limit [`90dac21`](https://gitlab.open-xchange.com/documents/office-web/commit/90dac2199e35592693a7090af04c3e1bf5d23cd5)
- [`DOCS-4933`](https://jira.open-xchange.com/browse/DOCS-4933): Improve RE for parsing string literals in formula expressions [`0ddb95a`](https://gitlab.open-xchange.com/documents/office-web/commit/0ddb95ab76f1fb5d9dc3f92fc934e16929fea1db)
- Fix whitespace deletion [`5ee8c0a`](https://gitlab.open-xchange.com/documents/office-web/commit/5ee8c0aaf510c3d8b8dcd92fa07098637377217a, [`befa4f4`](https://gitlab.open-xchange.com/documents/office-web/commit/befa4f428da023f0790781e270cb5cb4be24dc03)

## [8.14.0] - 2023-06-07

### Fixed

- [`DOCS-2134`](https://jira.open-xchange.com/browse/DOCS-2134): No longer using non breakable spaces in clipboard operations [`ccbab93`](https://gitlab.open-xchange.com/documents/office-web/commit/ccbab93528d4e23ba83258729dba888e0d8bf253)
- [`DOCS-4827`](https://jira.open-xchange.com/browse/DOCS-4827): Missing error handling for ws connectionClose while loading the document [`6de73e9`](https://gitlab.open-xchange.com/documents/office-web/commit/6de73e958c05d32283b147e54b5b0221709ffd25)
- [`DOCS-4837`](https://jira.open-xchange.com/browse/DOCS-4837): Optimal column width for blank columns does not hide all columns [`ed74d59`](https://gitlab.open-xchange.com/documents/office-web/commit/ed74d59b87ce7322354de272bd1a094ce74f76b0)
- [`DOCS-4870`](https://jira.open-xchange.com/browse/DOCS-4870): Leaving the presentation mode must not switch the screen mode [`791a364`](https://gitlab.open-xchange.com/documents/office-web/commit/791a3649631f4e83aba281ea28729063578e6096)
- [`DOCS-4889`](https://jira.open-xchange.com/browse/DOCS-4889): Fix for drag to copy in Firefox browser on MacOS [`d61e95f`](https://gitlab.open-xchange.com/documents/office-web/commit/d61e95f9da5fecd2ba959b5d55871905a4241c75)


## [8.13.0] - 2023-05-04

### Fixed

- [`DOCS-4738`](https://jira.open-xchange.com/browse/DOCS-4738): Spreadsheet: standard style for controls in status bar [`0f4ce9c`](https://gitlab.open-xchange.com/documents/office-web/commit/0f4ce9ced6cdafc22be901067b3ab0766e3af9fb)
- [`DOCS-4825`](https://jira.open-xchange.com/browse/DOCS-4825): Creating exception rule for minimizing keys [`dcd09dd`](https://gitlab.open-xchange.com/documents/office-web/commit/dcd09dd62ea63b2031a2196098ac059c9dc1d439)
- [`DOCS-4830`](https://jira.open-xchange.com/browse/DOCS-4830): Transfering character attributes from empty paragraphs to the first... [`d4fc8c5`](https://gitlab.open-xchange.com/documents/office-web/commit/d4fc8c58ff87dcb0e485a1409f02594899a8d1c6)
- [`DOCS-4838`](https://jira.open-xchange.com/browse/DOCS-4838): Show correct text color in paragraph style preview [`b5a51e4`](https://gitlab.open-xchange.com/documents/office-web/commit/b5a51e48ad7bd4a45c3be73615bb32657767944c)
- [`DOCS-4852`](https://jira.open-xchange.com/browse/DOCS-4852): Footer placeholder drawings with content are not restored after deletion. [`9850a84`](https://gitlab.open-xchange.com/documents/office-web/commit/9850a84dbd4f86b8b66ea5327283abd04e4d83d4)


## [8.12.0] - 2023-04-06

### Fixed

- [`DOCS-4710`](https://jira.open-xchange.com/browse/DOCS-4710): Spreadsheet: accept broken sheet names in conditional formats (ODS) [`df25d79`](https://gitlab.open-xchange.com/documents/office-web/commit/df25d79240e240bbff864d7b84046f266f0bb270)
- [`DOCS-4751`](https://jira.open-xchange.com/browse/DOCS-4751): Firefox renders border images wrong, when there are no borders. [`4818afc`](https://gitlab.open-xchange.com/documents/office-web/commit/4818afc04d529a572f6b658ae9f94a13044245f0)
- [`DOCS-4765`](https://jira.open-xchange.com/browse/DOCS-4765): Generate correct CF operations when pasting clipboard [`01d2f39`](https://gitlab.open-xchange.com/documents/office-web/commit/01d2f39cb75249d8d5d5a5afae78669dc1c971a9)
- [`DOCS-4768`](https://jira.open-xchange.com/browse/DOCS-4768): Avoid superfluous scrollbar when default settings are shown [`6d58307`](https://gitlab.open-xchange.com/documents/office-web/commit/6d58307353990628876fe169176b78e7f67793e7)
- [`DOCS-4771`](https://jira.open-xchange.com/browse/DOCS-4771): Chrome Android: Fix for keyboard handling [`8852af4`](https://gitlab.open-xchange.com/documents/office-web/commit/8852af4ac4486c66a013ea531b1c9547f34082de)
- [`DOCS-4777`](https://jira.open-xchange.com/browse/DOCS-4777): Collaborator menu is shown, when leaving presentation mode... [`43f078b`](https://gitlab.open-xchange.com/documents/office-web/commit/43f078bd33b94322613788a363af97b713f55c20)
- [`DOCS-4781`](https://jira.open-xchange.com/browse/DOCS-4781): Right topbar can be reached again with Tab key travelling in multi tab mode [`f1b0e5a`](https://gitlab.open-xchange.com/documents/office-web/commit/f1b0e5a1738d6c26ed2146bacbad8f58a5bdd5d5)
- [`DOCS-4811`](https://jira.open-xchange.com/browse/DOCS-4811): Fixing cursor up and down travelling in Firefox in tables [`4b851e0`](https://gitlab.open-xchange.com/documents/office-web/commit/4b851e0c766aee0b378ae4089aaedbbe8455cce2)
- [`DOCS-4814`](https://jira.open-xchange.com/browse/DOCS-4814): Correct text color in footer dialog in OX Presentation [`4cb18da`](https://gitlab.open-xchange.com/documents/office-web/commit/4cb18dabaeebd6e19d8a6109b0dda3475d404de0)


## [8.11.0] - 2023-03-09

### Added

- added e2e test for [`DOCS-4749`](https://jira.open-xchange.com/browse/DOCS-4749) [`53097e7`](https://gitlab.open-xchange.com/documents/office-web/commit/53097e72361b7b31a5fc06b74e595f732e3149ad)

### Changed

- [`DOCS-4685`](https://jira.open-xchange.com/browse/DOCS-4685): tracking uses native DOM events, supports active/passive events [`f23e929`](https://gitlab.open-xchange.com/documents/office-web/commit/f23e92914e231e1284420c559314be3f0a54bc78)

### Fixed

- [`DOCS-4733`](https://jira.open-xchange.com/browse/DOCS-4733): Live update drawing changes must not be shown in side pane [`2817676`](https://gitlab.open-xchange.com/documents/office-web/commit/2817676a3c3a1e43d2693a8e9686c293cba2d4a6)
- [`DOCS-4708`](https://jira.open-xchange.com/browse/DOCS-4708): Spreadsheet: copy/paste ranges with cond. formatting [`ebe9a59`](https://gitlab.open-xchange.com/documents/office-web/commit/ebe9a59a39b90e01afe6fe454ed3d664e5f2fea1)
- [`DOCS-4739`](https://jira.open-xchange.com/browse/DOCS-4739): Spreadsheet: autofill of columns/rows misses values [`fd67571`](https://gitlab.open-xchange.com/documents/office-web/commit/fd675713d0f000d600e9aeec2ec25af37015dc8b)
- [`DOCS-4759`](https://jira.open-xchange.com/browse/DOCS-4759): Improve handling of slide pane thumbnails during loading large documents. [`6acddb9`](https://gitlab.open-xchange.com/documents/office-web/commit/6acddb94f88429f3034ab9f3bd01993052b753c3)
- [`DOCS-4714`](https://jira.open-xchange.com/browse/DOCS-4714): Safari cannot set browser selection into final empty text span [`b212bb3`](https://gitlab.open-xchange.com/documents/office-web/commit/b212bb3c986a5bf0c66b3c17824f43f94b8c85b8)
- [`DOCS-4745`](https://jira.open-xchange.com/browse/DOCS-4745): Fixing bug with texture filling [`9edf933`](https://gitlab.open-xchange.com/documents/office-web/commit/9edf9337997ef698b96f85d03a15aca513372935)
- [`DOCS-4744`](https://jira.open-xchange.com/browse/DOCS-4744): broken refresh after resizing col/row in frozen view [`5302865`](https://gitlab.open-xchange.com/documents/office-web/commit/5302865da80a570c389c610f1d09170e98bcb7d9)
- [`DOCS-4748`](https://jira.open-xchange.com/browse/DOCS-4748): Spreadsheet: do not render TAB characters [`6ef2dd2`](https://gitlab.open-xchange.com/documents/office-web/commit/6ef2dd292ec0b86ce3ed9f2bfa0080c2bb622fa8)
- [`DOCS-4742`](https://jira.open-xchange.com/browse/DOCS-4742): ignore invalid syntax in ODS files (matrix range without formula) [`fcd935f`](https://gitlab.open-xchange.com/documents/office-web/commit/fcd935f2fd5d7c98eba7317bedc493dbf09164a3)
- [`DOCS-4732`](https://jira.open-xchange.com/browse/DOCS-4732): Wrong text color in drawing format dialog [`a87dd60`](https://gitlab.open-xchange.com/documents/office-web/commit/a87dd60ec998ef11183cb8ef6622dc68c927badf)
- [`DOCS-4716`](https://jira.open-xchange.com/browse/DOCS-4716): No typeerror on Safari after pressing Enter at first position in table [`fd75fab`](https://gitlab.open-xchange.com/documents/office-web/commit/fd75faba49762808a3afaae4e85bbe0ce08255c3)
- [`DOCS-4684`](https://jira.open-xchange.com/browse/DOCS-4684): Type error when saving as encrypted [`bc2ecaf`](https://gitlab.open-xchange.com/documents/office-web/commit/bc2ecaf174317f4d9fdb99e18780ca6c7e300017)


## [8.10.0] - 2023-02-08

### Fixed

- [`DOCS-4654`](https://jira.open-xchange.com/browse/DOCS-4654): using system format for fields (date/time) without field instruction [`82d93ca`](https://gitlab.open-xchange.com/documents/office-web/commit/82d93ca368eb953a94b68c9fd0b67492fb7af890)
- [`DOCS-4697`](https://jira.open-xchange.com/browse/DOCS-4697): Collaborators popup not visible in presentation mode [`1f61ac2`](https://gitlab.open-xchange.com/documents/office-web/commit/1f61ac2472040580ec353cf613d459349c3c20ca)
- [`DOCS-4700`](https://jira.open-xchange.com/browse/DOCS-4700): Improve trigger of slide pane update after leaving presentation mode [`55499bd`](https://gitlab.open-xchange.com/documents/office-web/commit/55499bd9db160b3c7578f0bdfc9dcb1bae9b263d)
- [`DOCS-4652`](https://jira.open-xchange.com/browse/DOCS-4652): Safari browser cannot set selection before absolute paragraph drawing [`26d2548`](https://gitlab.open-xchange.com/documents/office-web/commit/26d2548272a2e2eb98afd21fa0049dfcc613f207)
- [`DOCS-4704`](https://jira.open-xchange.com/browse/DOCS-4704): Remove app/quit button from presenter toolbar [`f19f184`](https://gitlab.open-xchange.com/documents/office-web/commit/f19f184c583b82ad2c6111b5658c915ef4cdcb7c)
- [`DOCS-4692`](https://jira.open-xchange.com/browse/DOCS-4692): Leaving fullscreen mode in OX Presentation [`5af69cd`](https://gitlab.open-xchange.com/documents/office-web/commit/5af69cd8816df0bbfe4cfbf6329c8e1f7383a5dc)
- [`DOCS-4706`](https://jira.open-xchange.com/browse/DOCS-4706): Quickstart icons in Presenter toolbar [`73b7a05`](https://gitlab.open-xchange.com/documents/office-web/commit/73b7a0565bdecc4403b3b12d8158cb6164aa3e45)
- fix helm ox-common dependency [`8b18f14`](https://gitlab.open-xchange.com/documents/office-web/commit/8b18f14bfbe3fda3cc2cab027656b726ca6b19f7)
- [`DOCS-4653`](https://jira.open-xchange.com/browse/DOCS-4653): Increase z-index of comment layer over selection layers [`579bf76`](https://gitlab.open-xchange.com/documents/office-web/commit/579bf76fd1c4c1b40e861ce233ccda2e7920543b)
- [`DOCS-4676`](https://jira.open-xchange.com/browse/DOCS-4676): Avoid superfluous arrow-left when traversing backwards [`2bb7ff0`](https://gitlab.open-xchange.com/documents/office-web/commit/2bb7ff0a881dca51ca5c2722b76f8c6844191495)
- fixed e2e tests [`5da502d`](https://gitlab.open-xchange.com/documents/office-web/commit/5da502dff9dc872e8120ec414044630765c3feb1)
- fix spectral issue e2e dockerfile 'Ensure disabling recommended package in apt-get (--no-install-recommends)' [`be3571b`](https://gitlab.open-xchange.com/documents/office-web/commit/be3571b0afd5e198ae08c9a85310d2f551314da9)
- [`DOCS-4570`](https://jira.open-xchange.com/browse/DOCS-4570): Disabled the 'notifications' earlier to avoid TypeError [`7539843`](https://gitlab.open-xchange.com/documents/office-web/commit/753984376a50e00d20824b862a83479c6e5aba6a)
- fix spectral issue e2e dockerfile 'Ensure delete installations lists after installation by 'apt'' [`2e6f2a5`](https://gitlab.open-xchange.com/documents/office-web/commit/2e6f2a538d8243b7bdb7e72655a1da0fdf9da9ce)


## [8.9.0] - 2023-01-12

### Added

- changelog [`6215c06`](https://gitlab.open-xchange.com/documents/office-web/commit/6215c0645671690e663f438cf756137313e67dfc)


## [8.8.0]

[unreleased]: https://gitlab.open-xchange.com/documents/office-web/compare/8.33.0...main
[8.33.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.32.0...8.33.0
[8.32.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.31.0...8.32.0
[8.31.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.30.0...8.31.0
[8.30.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.29.0...8.30.0
[8.29.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.28.0...8.29.0
[8.28.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.27.0...8.28.0
[8.27.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.26.0...8.27.0
[8.26.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.25.0...8.26.0
[8.25.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.24.0...8.25.0
[8.24.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.23.0...8.24.0
[8.23.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.22.0...8.23.0
[8.22.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.21.0...8.22.0
[8.21.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.20.0...8.21.0
[8.20.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.19.0...8.20.0
[8.19.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.18.0...8.19.0
[8.18.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.17.0...8.18.0
[8.17.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.16.0...8.17.0
[8.16.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.15.0...8.16.0
[8.15.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.14.0...8.15.0
[8.14.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.13.0...8.14.0
[8.13.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.12.0...8.13.0
[8.12.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.11.0...8.12.0
[8.11.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.10.0...8.11.0
[8.10.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.9.0...8.10.0
[8.9.0]: https://gitlab.open-xchange.com/documents/office-web/compare/8.0.0-0...8.9.0
