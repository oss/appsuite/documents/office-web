/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// constants ==================================================================

const { EPSILON } = Number;

// tests ======================================================================

describe("module plugins/jest", () => {

    describe("method toBeInteger", () => {
        it("should pass for integers", () => {
            expect(1).toBeInteger();
            expect(-1).toBeInteger();
            expect(0).toBeInteger();
            expect(-0).toBeInteger();
            expect(1e6).toBeInteger();
        });
        it("should fail for other numbers", () => {
            expect(0.1).not.toBeInteger();
            expect(-0.9).not.toBeInteger();
            expect(Infinity).not.toBeInteger();
            expect(Number.NaN).not.toBeInteger();
        });
        it("should fail for other values", () => {
            expect("a").not.toBeInteger();
            expect(true).not.toBeInteger();
            expect("0").not.toBeInteger();
            expect([0]).not.toBeInteger();
        });
    });

    describe("method toBeIntegerInInterval", () => {
        it("should pass for integers in interval", () => {
            expect(-0).toBeIntegerInInterval(0, 2);
            expect(0).toBeIntegerInInterval(0, 2);
            expect(1).toBeIntegerInInterval(0, 2);
            expect(2).toBeIntegerInInterval(0, 2);
        });
        it("should fail for other numbers", () => {
            expect(-1).not.toBeIntegerInInterval(0, 2);
            expect(3).not.toBeIntegerInInterval(0, 2);
            expect(0.1).not.toBeIntegerInInterval(0, 2);
            expect(-0.9).not.toBeIntegerInInterval(0, 2);
            expect(Infinity).not.toBeIntegerInInterval(0, 2);
            expect(Number.NaN).not.toBeIntegerInInterval(0, 2);
        });
        it("should fail for other values", () => {
            expect("a").not.toBeIntegerInInterval(0, 2);
            expect(true).not.toBeIntegerInInterval(0, 2);
            expect("0").not.toBeIntegerInInterval(0, 2);
            expect([0]).not.toBeIntegerInInterval(0, 2);
        });
    });

    describe("method toBeAlmost", () => {
        it("should pass for numbers almost equal to the expected number", () => {
            expect(1).toBeAlmost(1);
            expect(1 + EPSILON).toBeAlmost(1);
            expect(1 - EPSILON).toBeAlmost(1);
            expect(1 + 2 * EPSILON).toBeAlmost(1);
            expect(1 - 2 * EPSILON).toBeAlmost(1);
            expect(-1).toBeAlmost(-1);
            expect(-1 + EPSILON).toBeAlmost(-1);
            expect(-1 - EPSILON).toBeAlmost(-1);
            expect(-1 + 2 * EPSILON).toBeAlmost(-1);
            expect(-1 - 2 * EPSILON).toBeAlmost(-1);
            expect(1024).toBeAlmost(1024);
            expect(1024 + 1024 * EPSILON).toBeAlmost(1024);
            expect(1024 - 1024 * EPSILON).toBeAlmost(1024);
            expect(1024 + 2048 * EPSILON).toBeAlmost(1024);
            expect(1024 - 2048 * EPSILON).toBeAlmost(1024);
            expect(-1024).toBeAlmost(-1024);
            expect(-1024 + 1024 * EPSILON).toBeAlmost(-1024);
            expect(-1024 - 1024 * EPSILON).toBeAlmost(-1024);
            expect(-1024 + 2048 * EPSILON).toBeAlmost(-1024);
            expect(-1024 - 2048 * EPSILON).toBeAlmost(-1024);
        });
        it("should pass for numbers almost equal to zero", () => {
            expect(0).toBeAlmost(0);
            expect(-0).toBeAlmost(0);
            expect(EPSILON).toBeAlmost(0);
            expect(2 * EPSILON).toBeAlmost(0);
            expect(-EPSILON).toBeAlmost(0);
            expect(-2 * EPSILON).toBeAlmost(0);
        });
        it("should fail for numbers not close to the expected numbers", () => {
            expect(-1).not.toBeAlmost(1);
            expect(1 + 3 * EPSILON).not.toBeAlmost(1);
            expect(1 - 3 * EPSILON).not.toBeAlmost(1);
            expect(1).not.toBeAlmost(-1);
            expect(-1 + 3 * EPSILON).not.toBeAlmost(-1);
            expect(-1 - 3 * EPSILON).not.toBeAlmost(-1);
            expect(-1024).not.toBeAlmost(1024);
            expect(1024 + 3072 * EPSILON).not.toBeAlmost(1024);
            expect(1024 - 3072 * EPSILON).not.toBeAlmost(1024);
            expect(1024).not.toBeAlmost(-1024);
            expect(-1024 + 3072 * EPSILON).not.toBeAlmost(-1024);
            expect(-1024 - 3072 * EPSILON).not.toBeAlmost(-1024);
        });
        it("should fail for numbers not close to zero", () => {
            expect(3 * EPSILON).not.toBeAlmost(0);
            expect(-3 * EPSILON).not.toBeAlmost(0);
        });
        it("should always fail for invalid values", () => {
            expect(() => expect("a").toBeAlmost(0)).toThrow();
            expect(() => expect("a").not.toBeAlmost(0)).toThrow();
        });
    });

    describe("method toBeInInterval", () => {
        it("should pass or fail", () => {
            expect(0).not.toBeInInterval(1, 2);
            expect(1).toBeInInterval(1, 2);
            expect(2).toBeInInterval(1, 2);
            expect(3).not.toBeInInterval(1, 2);
        });
        it("should always fail for invalid values", () => {
            expect(() => expect("a").toBeInInterval(1, 2)).toThrow();
            expect(() => expect("a").not.toBeInInterval(1, 2)).toThrow();
        });
    });

    describe("method toStringifyTo", () => {
        it("should pass", () => {
            expect(1).toStringifyTo("1");
            expect("a").toStringifyTo("a");
            expect(true).toStringifyTo("true");
            expect(null).toStringifyTo("null");
            expect(/abc/i).toStringifyTo("/abc/i");
            expect([1, 2, 3]).toStringifyTo("1,2,3");
            expect({ toString: () => "42" }).toStringifyTo("42");
        });
        it("should fail", () => {
            expect(1).not.toStringifyTo("a");
            expect("a").not.toStringifyTo("1");
            expect(true).not.toStringifyTo("false");
            expect(null).not.toStringifyTo("undefined");
            expect(/abc/i).not.toStringifyTo("/abc/");
            expect([1, 2, 3]).not.toStringifyTo("[1,2,3]");
            expect({ toString: () => { return "42"; } }).not.toStringifyTo("a");
        });
    });

    describe("method toThrowValue", () => {
        it("should pass or fail", () => {
            function throws42() { throw 42; }
            expect(throws42).toThrowValue(42);
            expect(throws42).not.toThrowValue(43);
            function fn() { }
            expect(() => expect(fn).toThrowValue(42)).toThrow();
            expect(fn).not.toThrowValue(42);
        });
        it("should always fail for invalid values", () => {
            expect(() => expect(42).toThrowValue(42)).toThrow();
            expect(() => expect(42).not.toThrowValue(42)).toThrow();
        });
    });

    describe("method toContainProps", () => {
        it("should pass or fail", () => {
            const arr = [];
            expect({ a: 42, b: arr }).toContainProps({});
            expect({ a: 42, b: arr }).toContainProps({ a: 42 });
            expect({ a: 42, b: arr }).toContainProps({ b: arr });
            expect({ a: 42, b: arr }).toContainProps({ a: 42, b: arr });
            expect({ a: 42, b: arr }).not.toContainProps({ a: 43 });
            expect({ a: 42, b: arr }).not.toContainProps({ b: [] });
            expect({ a: 42, b: arr }).not.toContainProps({ a: 42, b: arr, c: true });
        });
        it("should always fail for invalid values", () => {
            expect(() => expect("a").toContainProps({})).toThrow();
            expect(() => expect("a").not.toContainProps({})).toThrow();
        });
    });

    describe("method toContainPropsEqual", () => {
        it("should pass or fail", () => {
            expect({ a: 42, b: [1], c: { d: [1] } }).toContainPropsEqual({});
            expect({ a: 42, b: [1], c: { d: [1] } }).toContainPropsEqual({ a: 42 });
            expect({ a: 42, b: [1], c: { d: [1] } }).toContainPropsEqual({ b: [1] });
            expect({ a: 42, b: [1], c: { d: [1] } }).toContainPropsEqual({ a: 42, b: [1] });
            expect({ a: 42, b: [1], c: { d: [1] } }).toContainPropsEqual({ c: { d: [1] } });
            expect({ a: 42, b: [1], c: { d: [1] } }).not.toContainPropsEqual({ a: 43 });
            expect({ a: 42, b: [1], c: { d: [1] } }).not.toContainPropsEqual({ b: [2] });
            expect({ a: 42, b: [1], c: { d: [1] } }).not.toContainPropsEqual({ b: [1, 2] });
            expect({ a: 42, b: [1], c: { d: [1] } }).not.toContainPropsEqual({ c: [] });
            expect({ a: 42, b: [1], c: { d: [1] } }).not.toContainPropsEqual({ c: { d: [2] } });
            expect({ a: 42, b: [1], c: { d: [1] } }).not.toContainPropsEqual({ a: 42, b: [1], c: true });
        });
        it("should always fail for invalid values", () => {
            expect(() => expect("a").toContainPropsEqual({})).toThrow();
            expect(() => expect("a").not.toContainPropsEqual({})).toThrow();
        });
    });

    describe("method toRespondTo", () => {
        it("should pass or fail", () => {
            const obj = { a: 42, b() { } };
            expect(obj).toRespondTo("b");
            expect(obj).not.toRespondTo("a");
            expect(obj).not.toRespondTo("c");
            expect([]).toRespondTo("forEach"); // from prototype
        });
        it("should always fail for invalid values", () => {
            expect(() => expect(42).toRespondTo("a")).toThrow();
            expect(() => expect(42).not.toRespondTo("a")).toThrow();
        });
    });

    describe("method toBeClass", () => {
        it("should pass for class constructors", () => {
            expect(class ES6Class { }).toBeClass();
            expect(class { }).toBeClass();
        });
        it("should fail for built-in classes", () => {
            expect(Object).not.toBeClass();
            expect(Array).not.toBeClass();
            expect(Error).not.toBeClass();
        });
        it("should fail for other values", () => {
            function ES5Class() { }
            expect(ES5Class).not.toBeClass();
            expect({}).not.toBeClass();
            expect(42).not.toBeClass();
        });
    });

    describe("method toBeSubClassOf", () => {
        it("should pass for subclass constructors", () => {
            expect(Array).toBeSubClassOf(Object);
            expect(Error).toBeSubClassOf(Object);
            expect(TypeError).toBeSubClassOf(Error);
            expect(TypeError).toBeSubClassOf(Object);
        });
        it("should fail for other constructors", () => {
            expect(Array).not.toBeSubClassOf(Array);
            expect(Object).not.toBeSubClassOf(Array);
        });
        it("should always fail for invalid values", () => {
            expect(() => expect(42).toBeSubClassOf(Object)).toThrow();
            expect(() => expect(42).not.toBeSubClassOf(Object)).toThrow();
        });
    });

    describe("method toHaveMethod", () => {
        it("should pass or fail", () => {
            class Test { method1() { } static method2() { } }
            expect(Test).toHaveMethod("method1");
            expect(Test).not.toHaveMethod("method2");
        });
    });

    describe("method toHaveStaticMethod", () => {
        it("should pass or fail", () => {
            class Test { method1() { } static method2() { } }
            expect(Test).toHaveStaticMethod("method2");
            expect(Test).not.toHaveStaticMethod("method1");
        });
    });

    describe("method toBeIterator", () => {
        it("should pass or fail", () => {
            expect([].values()).toBeIterator();
            expect([]).not.toBeIterator();
            expect({ next() {} }).toBeIterator();
            expect({ next: 42 }).not.toBeIterator();
            expect(42).not.toBeIterator();
        });
    });

    describe("method toBeCssColor", () => {
        it("should pass or fail", () => {
            expect("#02468ace").toBeCssColor("#02468ACE");
            expect("#48c").toBeCssColor("#4488ccff");
            expect("#4488cc").toBeCssColor("#48cf");
            expect("rgb(128,255,0)").toBeCssColor("#80ff00");
            expect("#80ff0000").toBeCssColor("rgba(128 255 0 / 0%)");
            expect("hsl(200,50%,50%,50%)").toBeCssColor("hsla(200deg 50% 50% / 0.5)");
            expect("#02468ace").not.toBeCssColor("#02468acf");
        });
        it("should always fail for invalid values", () => {
            expect(() => expect(42).toBeCssColor("#000000")).toThrow();
            expect(() => expect(42).not.toBeCssColor("#000000")).toThrow();
        });
    });

    describe("method toHaveBeenCalledOn", () => {
        it("should pass or fail", () => {
            const ctx1 = {}, ctx2 = {};
            const spy1 = jest.fn(), spy2 = jest.fn();
            spy1.call(ctx1);
            spy1.call(ctx1);
            spy2.call(ctx1);
            spy2.call(ctx2);
            expect(spy1).toHaveBeenCalledOn(ctx1);
            expect(spy1).not.toHaveBeenCalledOn(ctx2);
            expect(spy2).toHaveBeenCalledOn(ctx1);
            expect(spy2).toHaveBeenCalledOn(ctx2);
        });
        it("should always fail for invalid values", () => {
            expect(() => expect(42).toHaveBeenCalledOn(null)).toThrow();
            expect(() => expect(42).not.toHaveBeenCalledOn(null)).toThrow();
        });
    });

    describe("method toHaveBeenAlwaysCalledOn", () => {
        it("should pass or fail", () => {
            const ctx1 = {}, ctx2 = {};
            const spy1 = jest.fn(), spy2 = jest.fn();
            spy1.call(ctx1);
            spy1.call(ctx1);
            spy2.call(ctx1);
            spy2.call(ctx2);
            expect(spy1).toHaveBeenAlwaysCalledOn(ctx1);
            expect(spy1).not.toHaveBeenAlwaysCalledOn(ctx2);
            expect(spy2).not.toHaveBeenAlwaysCalledOn(ctx1);
            expect(spy2).not.toHaveBeenAlwaysCalledOn(ctx2);
        });
        it("should always fail for invalid values", () => {
            expect(() => expect(42).toHaveBeenAlwaysCalledOn(null)).toThrow();
            expect(() => expect(42).not.toHaveBeenAlwaysCalledOn(null)).toThrow();
        });
    });

    describe("method toHaveAllBeenCalledTimes", () => {
        it("should pass or fail", () => {
            const spies = [jest.fn(), jest.fn(), jest.fn()];
            spies[0]();
            spies[1]();
            spies[1]();
            spies[1]();
            spies[2]();
            spies[2]();
            expect(spies).toHaveAllBeenCalledTimes(1, 3, 2);
            expect(spies).not.toHaveAllBeenCalledTimes(0, 3, 2);
            expect(spies).not.toHaveAllBeenCalledTimes(1, 2, 2);
            expect(spies).not.toHaveAllBeenCalledTimes(1, 3, 1);
        });
        it("should always fail for invalid values", () => {
            expect(() => expect(42).toHaveAllBeenCalledTimes(1, 2)).toThrow();
            expect(() => expect(42).not.toHaveAllBeenCalledTimes(1, 2)).toThrow();
            expect(() => expect(jest.fn()).toHaveAllBeenCalledTimes(1, 2)).toThrow();
            expect(() => expect(jest.fn()).not.toHaveAllBeenCalledTimes(1, 2)).toThrow();
            expect(() => expect([jest.fn()]).toHaveAllBeenCalledTimes(1, 2)).toThrow();
            expect(() => expect([jest.fn()]).not.toHaveAllBeenCalledTimes(1, 2)).toThrow();
        });
    });

    describe("method callOrder", () => {
        it("should pass for correct call order", () => {
            const spies = [jest.fn(), jest.fn(), jest.fn()];
            spies[1]();
            spies[2]();
            spies[0]();
            expect(spies).toHaveAllBeenCalledInOrder(1, 2, 0);
            expect(spies).not.toHaveAllBeenCalledInOrder(2, 0, 1);
        });
        it("should always fail for invalid values", () => {
            expect(() => expect(42).toHaveAllBeenCalledInOrder(1, 2)).toThrow();
            expect(() => expect(42).not.toHaveAllBeenCalledInOrder(1, 2)).toThrow();
            expect(() => expect(jest.fn()).toHaveAllBeenCalledInOrder(1, 2)).toThrow();
            expect(() => expect(jest.fn()).not.toHaveAllBeenCalledInOrder(1, 2)).toThrow();
            expect(() => expect([jest.fn()]).toHaveAllBeenCalledInOrder(1, 2)).toThrow();
            expect(() => expect([jest.fn()]).not.toHaveAllBeenCalledInOrder(1, 2)).toThrow();
        });
    });
});
