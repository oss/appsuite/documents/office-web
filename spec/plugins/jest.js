/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import parseColor from "parse-css-color";

// private functions ==========================================================

/**
 * Helper function for assertions that will fail regardless of the "not" flag
 * in the expectation context. Intended to be used for testing the mandatory
 * type of a received value.
 *
 * @param {jest.MatcherContext} context
 *  The matcher context with access to utilities.
 *
 * @param {string} matcher
 *  The name of the calling matcher function, for error output.
 *
 * @param {unknown} received
 *  The received value, for error output.
 *
 * @param {boolean} passed
 *  Whether the type check of the "received" value has passed.
 *
 * @param {string} message
 *  The error message particle to be inserted into the error output.
 *
 * @param {() => jest.ExpectationResult} callback
 *  The implementation callback to be invoked if the type check has passed (if
 *  "passed" is `true`).
 *
 * @returns {jest.ExpectationResult}
 *  A failing result with an appropriate error message, if "passed" is `false`,
 *  otherwise the return value of "callback".
 */
function assertReceived(context, matcher, received, passed, message, callback) {

    if (passed) { return callback.call(context); }

    const { matcherErrorMessage, matcherHint, printReceived, printWithType } = context.utils;

    const pass = context.isNot; // always fail, regardless of the "not" flag
    const msg = matcherErrorMessage(
        matcherHint(matcher, received, undefined, context),
        `${printReceived("received")} ${message}`,
        printWithType("Received", received, printReceived)
    );

    return { pass, message: () => msg };
}

/**
 * Helper function for assertions that expect a mock function as "received"
 * value.
 *
 * @param {jest.MatcherContext} context
 *  The matcher context with access to utilities.
 *
 * @param {string} matcher
 *  The name of the calling matcher function, for error messages.
 *
 * @param {unknown} received
 *  The received value to be checked.
 *
 * @param {() => jest.ExpectationResult} callback
 *  The implementation callback that will be invoked if "received " is a mock
 *  function as expected.
 *
 * @returns {jest.ExpectationResult}
 *  A failing result with an appropriate error message, if "received" is not a
 *  mock function, otherwise the return value of "callback".
 */
function assertReceivedIsMock(context, matcher, received, callback) {
    const passed = jest.isMockFunction(received);
    return assertReceived(context, matcher, received, passed, "value must be a mock function", callback);
}

/**
 * Helper function for assertions that expect an array of mock function as
 * "received" value.
 *
 * @param {jest.MatcherContext} context
 *  The matcher context with access to utilities.
 *
 * @param {string} matcher
 *  The name of the calling matcher function, for error messages.
 *
 * @param {unknown} received
 *  The received value to be checked.
 *
 * @param {number} count
 *  The number of mock functions being expected in `received`.
 *
 * @param {() => jest.ExpectationResult} callback
 *  The implementation callback that will be invoked if "received " is an array
 *  of mock functions as expected.
 *
 * @returns {jest.ExpectationResult}
 *  A failing result with an appropriate error message, if "received" is not an
 *  array of mock functions, otherwise the return value of "callback".
 */
function assertReceivedIsArrayOfMocks(context, matcher, received, count, callback) {
    const passed = Array.isArray(received) && (received.length === count) && received.every(jest.isMockFunction);
    return assertReceived(context, matcher, received, passed, "value must be an array of mock functions", callback);
}

/**
 * Parses the passed string for all kinds of CSS colors, and returns a
 * normalized version of the color ("#RRGGBBAA" for RGB colors and color names,
 * and level 4 syntax "hsla(deg s% l% / a%)" for HSL colors).
 *
 * @param {string} value
 *  The value to be converted.
 *
 * @returns {string | null}
 *  The normalized CSS color string, or `null` on error.
 */
function normalizeColor(value) {
    const result = parseColor(value);
    if (!result) { return null; }
    const { type, values: [v1, v2, v3], alpha } = result;
    const h2 = n => n.toString(16).padStart(2, "0");
    switch (type) {
        case "rgb": return "#" + h2(v1) + h2(v2) + h2(v3) + h2(Math.round(alpha * 255));
        case "hsl": return "hsl(" + v1 + "deg " + v2 + "% " + v3 + "% / " + Math.round(result.alpha * 100) + "%)";
    }
    return null;
}

// Jest extensions ============================================================

// numbers --------------------------------------------------------------------

/**
 * Asserts that the received value is a finite integer.
 *
 * @param {unknown} received
 *  The value to be tested.
 */
export function toBeInteger(received) {
    const { printReceived } = this.utils;
    const pass = Number.isFinite(received) && (Math.round(received) === received);
    const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to be an integer`;
    return { pass, message };
}

/**
 * Asserts that the received value is a finite integer in the specified closed
 * interval.
 *
 * @param {unknown} received
 *  The value to be tested.
 *
 * @param {number} lower
 *  The lower bound of the interval (inclusive).
 *
 * @param {number} upper
 *  The upper boundary of the interval (inclusive).
 */
export function toBeIntegerInInterval(received, lower, upper) {
    const { printReceived, printExpected } = this.utils;
    const pass = Number.isFinite(received) && (Math.round(received) === received) && (lower <= received) && (received <= upper);
    const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to be an integer in interval [${printExpected(lower)}..${printExpected(upper)}]`;
    return { pass, message };
}

/**
 * Asserts that a floating-point number is almost equal to an expected number,
 * with a relative error less than or equal to `2^-51` which is twice the
 * number of `Number.EPSILON`. If the number is expected to be zero, this
 * method will check the absolute value of the received number instead of the
 * relative error.
 *
 * @example
 *  expect(Math.sin(Math.PI/2)).toBe(1);        // fails
 *  expect(Math.sin(Math.PI)).toBe(0);          // fails
 *  expect(Math.sin(Math.PI/2)).toBeAlmost(1);  // passes
 *  expect(Math.sin(Math.PI)).toBeAlmost(0);    // passes
 *
 * @param {unknown} received
 *  The value to be tested. The assertion will always fail if this value is not
 *  a number (also with "not" flag).
 *
 * @param {number} expected
 *  The expected number that will be compared with the tested number.
 */
export function toBeAlmost(received, expected) {
    const passed = typeof received === "number";
    return assertReceived(this, "toBeAlmost", received, passed, "value must be a number", () => {
        const { printReceived, printExpected } = this.utils;
        // special case for expecting zero
        const error = expected ? Math.abs((received - expected) / expected) : Math.abs(received);
        const pass = Number.isFinite(received) && (error <= 2 * Number.EPSILON);
        const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to be almost ${printExpected(expected)}`;
        return { pass, message };
    });
}

/**
 * Asserts that a number is inside a closed interval.
 *
 * @param {unknown} received
 *  The value to be tested. The assertion will always fail if this value is not
 *  a number (also with "not" flag).
 *
 * @param {number} lower
 *  The lower bound of the interval (inclusive).
 *
 * @param {number} upper
 *  The upper boundary of the interval (inclusive).
 */
export function toBeInInterval(received, lower, upper) {
    const passed = typeof received === "number";
    return assertReceived(this, "toBeInInterval", received, passed, "value must be a number", () => {
        const { printReceived, printExpected } = this.utils;
        const pass = (lower <= received) && (received <= upper);
        const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to be in interval [${printExpected(lower)}..${printExpected(upper)}]`;
        return { pass, message };
    });
}

// strings --------------------------------------------------------------------

/**
 * Asserts whether the tested value, converted to a string, equals the
 * expectation.
 *
 * @example
 *  expect([1, 2]).toStringifyTo("1,2");
 *
 * @param {unknown} received
 *  The value to be tested.
 *
 * @param {string} expected
 *  The expected string that will be compared with the result of passing the
 *  tested value to the `String` constructor.
 */
export function toStringifyTo(received, expected) {
    const { printReceived, printExpected } = this.utils;
    const pass = String(received) === expected;
    const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to stringify to ${printExpected(expected)}`;
    return { pass, message };
}

// functions ------------------------------------------------------------------

/**
 * Asserts whether the tested function will throw an exception with the exact
 * value. The value does not need to be an instance of class `Error`.
 *
 * @example
 *  function throws() { throw 42; }
 *  expect(throws).toThrowValue(42);
 *
 * @param {unknown} received
 *  The value to be tested. The assertion will always fail if this value is not
 *  a function (also with "not" flag).
 *
 * @param {unknown} expected
 *  The value expected to be thrown by the tested function.
 */
export function toThrowValue(received, expected) {
    const passed = !!received && (typeof received === "function");
    return assertReceived(this, "toThrowValue", received, passed, "value must be a function", () => {
        const { printReceived, printExpected } = this.utils;
        try {
            received();
            const message = `expected ${printReceived(received)} to throw`;
            return { pass: false, message };
        } catch (result) {
            const pass = result === expected;
            const message = () => pass ? `expected ${printReceived(received)} not to throw ${printExpected(expected)}` : `expected ${printReceived(received)} to throw ${printExpected(expected)} but got ${printReceived(result)}`;
            return { pass, message };
        }
    });
}

// objects --------------------------------------------------------------------

/**
 * Asserts that an object contains all expected properties. Checks the property
 * values for _strict equality_.
 *
 * @param {unknown} received
 *  The value to be tested. The assertion will always fail if this value is not
 *  an object (also with "not" flag).
 *
 * @param {object} expected
 *  The properties expected to exist in the received object.
 */
export function toContainProps(received, expected) {
    const passed = !!received && (typeof received === "object");
    return assertReceived(this, "toContainProps", received, passed, "value must be an object", () => {
        const { printReceived, printExpected } = this.utils;
        const pass = Object.entries(expected).every(([key, value]) => (key in received) && Object.is(received[key], value));
        const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to have all properties of ${printExpected(expected)}`;
        return { pass, message };
    });
}

/**
 * Asserts that an object contains all expected properties. Checks the property
 * values for _deep equality_.
 *
 * @param {unknown} received
 *  The value to be tested. The assertion will always fail if this value is not
 *  an object (also with "not" flag).
 *
 * @param {object} expected
 *  The properties expected to exist in the received object.
 */
export function toContainPropsEqual(received, expected) {
    const passed = !!received && (typeof received === "object");
    return assertReceived(this, "toContainProps", received, passed, "value must be an object", () => {
        const { printReceived, printExpected } = this.utils;
        const pass = Object.entries(expected).every(([key, value]) => (key in received) && this.equals(received[key], value, this.customTesters));
        const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to have all properties of ${printExpected(expected)}`;
        return { pass, message };
    });
}

/**
 * Asserts that an object contains a function property.
 *
 * @param {unknown} received
 *  The value to be tested. The assertion will always fail if this value is not
 *  an object (also with "not" flag).
 *
 * @param {string} name
 *  The name of the expected function property.
 */
export function toRespondTo(received, name) {
    const passed = !!received && (typeof received === "object");
    return assertReceived(this, "toRespondTo", received, passed, "value must be an object", () => {
        const { printReceived, printExpected } = this.utils;
        const pass = typeof received[name] === "function";
        const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to respond to ${printExpected(`"${name}"`)}`;
        return { pass, message };
    });
}

// classes --------------------------------------------------------------------

/**
 * Asserts whether the tested function is an ES6 class constructor.
 *
 * @example
 *  function ES5Class() {}
 *  class ES6Class {}
 *  expect(ES5Class).not.toBeClass();
 *  expect(ES6Class).toBeClass();
 *
 * @param {unknown} received
 *  The value to be tested.
 */
export function toBeClass(received) {
    const { printReceived } = this.utils;
    const pass = (typeof received === "function") && received.toString().startsWith("class");
    const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to be a class`;
    return { pass, message };
}

/**
 * Asserts whether the tested function is the constructor of a subclass of the
 * specified base class.
 *
 * @example
 *  expect(TypeError).toBeSubClassOf(Error);
 *
 * @param {unknown} received
 *  The value to be tested. The assertion will always fail if this value is not
 *  a class constructor (also with "not" flag).
 *
 * @param {Function} expected
 *  The expected base class.
 */
export function toBeSubClassOf(received, expected) {
    const passed = typeof received === "function";
    return assertReceived(this, "toBeSubClassOf", received, passed, "value must be a class", () => {
        const { printReceived, printExpected } = this.utils;
        const pass = received.prototype instanceof expected;
        const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to be a subclass of ${printExpected(expected)}`;
        return { pass, message };
    });
}

/**
 * Asserts that a class contains an instance method.
 *
 * @param {unknown} received
 *  The value to be tested. The assertion will always fail if this value is not
 *  a class constructor (also with "not" flag).
 *
 * @param {string} name
 *  The name of the expected instance method.
 */
export function toHaveMethod(received, name) {
    const passed = typeof received === "function";
    return assertReceived(this, "toHaveMethod", received, passed, "value must be a class", () => {
        const { printReceived, printExpected } = this.utils;
        const pass = typeof received.prototype[name] === "function";
        const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to have the instance method ${printExpected(`"${name}"`)}`;
        return { pass, message };
    });
}

/**
 * Asserts that a class contains a static method.
 *
 * @param {unknown} received
 *  The value to be tested. The assertion will always fail if this value is not
 *  a class constructor (also with "not" flag).
 *
 * @param {string} name
 *  The name of the expected static method.
 */
export function toHaveStaticMethod(received, name) {
    const passed = typeof received === "function";
    return assertReceived(this, "toHaveStaticMethod", received, passed, "value must be a class", () => {
        const { printReceived, printExpected } = this.utils;
        const pass = typeof received[name] === "function";
        const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to have the static method ${printExpected(`"${name}"`)}`;
        return { pass, message };
    });
}

/**
 * Asserts that a value is an iterator object (it responds to "next").
 *
 * @param {unknown} received
 *  The value to be tested.
 */
export function toBeIterator(received) {
    const { printReceived } = this.utils;
    const pass = received && (typeof received === "object") && (typeof received.next === "function");
    const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to be an iterator`;
    return { pass, message };
}

// DOM ------------------------------------------------------------------------

/**
 * Asserts that the tested value is a CSS color with the same result as the
 * expected color.
 *
 * The tested value and the expected value will be normalized to a common color
 * representation format ("#rrggbbaa" for RGB colors and color names, and
 * "hsl(h s l / a)" for HSL colors). This will cause that "#rgb" syntax matches
 * with "rgb()" syntax, or level 3 function syntax "rgb(r,g,b,a)" matches with
 * level 4 syntax "rgb(r g b / a)".
 *
 * @example
 *  expect(document.body.style.color).toBeCssColor("#2277ff");
 *
 * @param {unknown} received
 *  The value to be tested. The assertion will always fail if this value is not
 *  a string (also with "not" flag).
 *
 * @param {string} expected
 *  The expected CSS color.
 */
export function toBeCssColor(received, expected) {
    const passed = typeof received === "string";
    return assertReceived(this, "toBeCssColor", received, passed, "value must be a string", () => {
        const { printReceived, printExpected } = this.utils;
        const value = normalizeColor(received);
        const pass = !!value && (value === normalizeColor(expected));
        const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to be the CSS color ${printExpected(expected)}`;
        return { pass, message };
    });
}

// Jest mocks -----------------------------------------------------------------

/**
 * Asserts that a mock function was called at least once with the specified
 * calling context.
 *
 * @param {jest.Mock} received
 *  The mock function to be tested. The assertion will always fail if this
 *  value is not a Jest mock function (also with "not" flag).
 *
 * @param {unknown} context
 *  The expected calling context.
 */
export function toHaveBeenCalledOn(received, context) {
    return assertReceivedIsMock(this, "toHaveBeenCalledOn", received, () => {
        const { printReceived, printExpected } = this.utils;
        const pass = received.mock.contexts.some(el => el === context);
        const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to have been called on ${printExpected(context)}`;
        return { pass, message };
    });
}

/**
 * Asserts that a mock function was always called with the specified calling
 * context.
 *
 * @param {jest.Mock} received
 *  The mock function to be tested. The assertion will always fail if this
 *  value is not a Jest mock function (also with "not" flag).
 *
 * @param {object} context
 *  The expected calling context.
 */
export function toHaveBeenAlwaysCalledOn(received, context) {
    return assertReceivedIsMock(this, "toHaveBeenAlwaysCalledOn", received, () => {
        const { printReceived, printExpected } = this.utils;
        const pass = received.mock.contexts.every(el => el === context);
        const message = () => `expected ${printReceived(received)}${pass ? " not" : ""} to have been always called on ${printExpected(context)}`;
        return { pass, message };
    });
}

/**
 * Asserts that the call counts of all mock functions match the specified call
 * counts.
 *
 * @example
 *  const spies = createJestFns(3);
 *  spies[1]();
 *  spies[0]();
 *  spies[1]();
 *  expect(spies).toHaveAllBeenCalledTimes(1, 2, 0);
 *
 * @param {jest.Mock[]} received
 *  An array of mock functions to be tested. The array length must match the
 *  number of remaining parameters ("expected" rest parameter).
 *
 * @param {number[]} expected
 *  The expected call counts for all mock functions.
 */
export function toHaveAllBeenCalledTimes(received, ...expected) {
    return assertReceivedIsArrayOfMocks(this, "toHaveAllBeenCalledTimes", received, expected.length, () => {
        received = received.map(fn => fn.mock.calls.length);
        const pass = this.equals(received, expected);
        const message = () => {
            const { printReceived, printExpected } = this.utils;
            const hint = this.utils.matcherHint("toHaveAllBeenCalledTimes", undefined, expected.join(", "), this);
            const msg = pass ? `Expected number of calls: not ${printExpected(expected)}` : `Expected number of calls: ${printExpected(expected)}\nReceived number of calls: ${printReceived(received)}`;
            return `${hint}\n\n${msg}`;
        };
        return { pass, message };
    });
}

/**
 * Asserts that the the call order of the mock functions matches the specified
 * call order.
 *
 * The following example with the arguments `(2, 0, 1)` expects that
 * - The third mock function (`spies[2]`) has been called first.
 * - The first mock function (`spies[0]`) has been called after.
 * - The second mock function (`spies[1]`) has been called last.
 *
 * @example
 *  const spies = createJestFns(3);
 *  spies[2]();
 *  spies[0]();
 *  spies[1]();
 *  expect(spies).toHaveAllBeenCalledInOrder(2, 0, 1);
 *
 * @param {jest.Mock[]} received
 *  An array of mock functions to be tested. The array length must match the
 *  number of remaining parameters ("expected" rest parameter).
 *
 * @param {number[]} expected
 *  The expected call order for all mock functions.
 */
export function toHaveAllBeenCalledInOrder(received, ...expected) {
    return assertReceivedIsArrayOfMocks(this, "toHaveAllBeenCalledInOrder", received, expected.length, () => {
        let pass = true;
        let lastOrder = 0;
        for (const idx of expected) {
            const minOrder = lastOrder;
            // find the minimum call index in the referenced mock that follows the call index of the preceding mock (in "lastOrder")
            const nextOrder = received[idx].mock.invocationCallOrder.reduce((accu, order) => (minOrder < order) ? Math.min(accu, order) : accu, Infinity);
            if (!Number.isFinite(nextOrder)) { pass = false; break; }
            lastOrder = nextOrder;
        }
        const message = () => {
            const { printReceived, printExpected } = this.utils;
            const hint = this.utils.matcherHint("toHaveAllBeenCalledInOrder", undefined, expected.join(", "), this);
            received = received.map((spy, idx) => ({ idx, min: Math.min(...spy.mock.invocationCallOrder) })).sort((e1, e2) => e1.min - e2.min).map(e => e.idx);
            const msg = pass ? `Expected calling order: not ${printExpected(expected)}` : `Expected calling order: ${printExpected(expected)}\nReceived calling order: ${printReceived(received)}`;
            return `${hint}\n\n${msg}`;
        };
        return { pass, message };
    });
}
