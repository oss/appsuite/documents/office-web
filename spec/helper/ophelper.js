/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";

import * as Op from "@/io.ox/office/textframework/utils/operations";

// public functions ===========================================================

/**
 * Converts a test value to an operation position.
 *
 * @param {Position|number|string} pos
 *  The position, as number array (a shallow copy of the array will be
 *  returned), as number (returned as one-element array), or as a
 *  space-separated string (e.g. "1 2 3").
 *
 * @returns {Position}
 *  The operation position, as array of numbers.
 */
export function parsePos(pos) {
    return is.number(pos) ? [pos] : is.string(pos) ? pos.split(" ").map(i => parseInt(i, 10)) : pos.slice(0);
}

/**
 * Creates a JSON document operation object.
 *
 * @param {string} name
 *  The name of the operation.
 *
 * @param {Dict[]} props
 *  One or more property objects that will be added to the operation.
 *
 * @returns {Operation}
 *  The JSON document operation with the specified name and properties.
 */
export function makeOp(name, ...props) {
    return Object.assign({ name }, ...props);
}

/**
 * Creates a "noOp" operation.
 *
 * @param {Dict[]} props
 *  One or more property objects that will be added to the operation.
 *
 * @returns {Operation}
 *  The "noOp" operation with the specified properties.
 */
export function noOp(...props) {
    return makeOp(Op.NOOP, ...props);
}

/**
 * Creates a "changeConfig" operation.
 *
 * @param {Dict} attrs
 *  The formatting attributes to be inserted into the operation.
 *
 * @returns {ChangeConfigOperation}
 *  The "changeConfig" operation.
 */
export function changeConfig(attrs) {
    return makeOp(Op.CHANGE_CONFIG, { attrs });
}

/**
 * Creates an "insertFont" operation.
 *
 * @param {string} fontName
 *  The font name.
 *
 * @returns {InsertFontOperation}
 *  The "insertFont" operation.
 */
export function insertFont(fontName) {
    return makeOp(Op.INSERT_FONT, { fontName, attrs: {} });
}

/**
 * Creates an "insertTheme" operation.
 *
 * @param {string} themeName
 *  The name of the theme.
 *
 * @returns {InsertThemeOperation}
 *  The "insertTheme" operation.
 */
export function insertTheme(themeName) {
    return makeOp(Op.INSERT_THEME, { themeName, colorScheme: {}, fontScheme: {} });
}

/**
 * Creates an "insertStyleSheet" operation.
 *
 * @returns {InsertStyleSheetOperation}
 *  The "insertStyleSheet" operation.
 */
export function insertStyle(type, styleId, attrs, props) {
    return makeOp(Op.INSERT_STYLESHEET, { type, styleId, attrs }, props);
}

/**
 * Creates a "deleteStyleSheet" operation.
 *
 * @returns {DeleteStyleSheetOperation}
 *  The "deleteStyleSheet" operation.
 */
export function deleteStyle(type, id) {
    return makeOp(Op.DELETE_STYLESHEET, { type, styleId: id });
}

/**
 * Creates a "changeStyleSheet" operation.
 *
 * @returns {ChangeStyleSheetOperation}
 *  The "changeStyleSheet" operation.
 */
export function changeStyle(type, styleId, attrs, props) {
    return makeOp(Op.CHANGE_STYLESHEET, { type, styleId, attrs }, props);
}

/**
 * Creates an "insertAutoStyle" operation.
 *
 * @param {string} [type]
 *  The style family of the auto-style. Can be omitted completely.
 *
 * @param {string} styleId
 *  The identifier of the auto-style.
 *
 * @param {Dict} [attrs]
 *  The formatting attributes carried by the auto-style. Can be omitted
 *  completely.
 *
 * @param {Dict|boolean} [props]
 *  Additional properties, or the boolean value of the `default` property.
 *
 * @returns {InsertAutoStyleOperation}
 *  The "insertAutoStyle" operation with the specified attributes.
 */
export function insertAutoStyle(type, styleId, attrs, props) {
    const opProps = {};
    if (is.string(styleId)) {
        opProps.type = type;
        opProps.styleId = styleId;
    } else {
        opProps.styleId = type;
        props = attrs;
        attrs = styleId;
    }
    if (is.boolean(attrs)) {
        props = attrs;
        attrs = null;
    }
    opProps.attrs = attrs || {};
    if (props === true) {
        opProps.default = true;
    } else {
        Object.assign(opProps, props);
    }
    return makeOp(Op.INSERT_AUTOSTYLE, opProps);
}

/**
 * Creates a "deleteAutoStyle" operation.
 *
 * @param {string} [type]
 *  The style family of the auto-style. Can be omitted completely.
 *
 * @param {string} styleId
 *  The identifier of the auto-style.
 *
 * @param {Dict} [props]
 *  Additional properties.
 *
 * @returns {DeleteAutoStyleOperation}
 *  The "deleteAutoStyle" operation with the specified attributes.
 */
export function deleteAutoStyle(type, styleId, props) {
    const opProps = {};
    if (is.string(styleId)) {
        opProps.type = type;
        opProps.styleId = styleId;
        Object.assign(opProps, props);
    } else {
        opProps.styleId = type;
        Object.assign(opProps, styleId);
    }
    return makeOp(Op.DELETE_AUTOSTYLE, opProps);
}

/**
 * Creates a "changeAutoStyle" operation.
 *
 * @param {string} [type]
 *  The style family of the auto-style. Can be omitted completely.
 *
 * @param {string} styleId
 *  The identifier of the auto-style.
 *
 * @param {Dict} [attrs]
 *  The formatting attributes carried by the auto-style. Can be omitted
 *  completely.
 *
 * @returns {ChangeAutoStyleOperation}
 *  The "changeAutoStyle" operation with the specified attributes.
 */
export function changeAutoStyle(type, styleId, attrs) {
    const props = {};
    if (is.string(styleId)) {
        props.type = type;
        props.styleId = styleId;
    } else {
        props.styleId = type;
        attrs = styleId;
    }
    props.attrs = is.dict(attrs) ? attrs : {};
    return makeOp(Op.CHANGE_AUTOSTYLE, props);
}

/**
 * Creates an operation with a "start" position property.
 */
export function posOp(name, pos, ...props) {
    return makeOp(name, { start: parsePos(pos) }, ...props);
}

/**
 * Creates an "insertDrawing" operation.
 */
export function insertDrawing(pos, type, attrs) {
    return posOp(Op.INSERT_DRAWING, pos, { type }, attrs ? { attrs } : null);
}

/**
 * Creates an "insertDrawing" operation for a drawing shape object.
 */
export function insertShape(pos, attrs) {
    return insertDrawing(pos, "shape", attrs);
}

/**
 * Creates an "insertDrawing" operation for a chart object.
 */
export function insertChart(pos, attrs) {
    return insertDrawing(pos, "chart", attrs);
}

/**
 * Creates a "deleteDrawing" operation.
 */
export function deleteDrawing(pos) {
    return posOp(Op.DELETE_DRAWING, pos);
}

/**
 * Creates a "changeDrawing" operation.
 */
export function changeDrawing(pos, attrs) {
    return posOp(Op.CHANGE_DRAWING, pos, { attrs });
}

/**
 * Creates a "moveDrawing" operation.
 */
export function moveDrawing(from, to) {
    return posOp(Op.MOVE_DRAWING, from, { to: parsePos(to) });
}

/**
 * Creates an "insertChartSeries" operation.
 */
export function insertChSeries(pos, series, attrs) {
    return posOp(Op.INSERT_CHART_SERIES, pos, { series, attrs });
}

/**
 * Creates a "deleteChartSeries" operation.
 */
export function deleteChSeries(pos, series) {
    return posOp(Op.DELETE_CHART_SERIES, pos, { series });
}

/**
 * Creates a "changeChartSeries" operation.
 */
export function changeChSeries(pos, series, attrs) {
    return posOp(Op.CHANGE_CHART_SERIES, pos, { series, attrs });
}

/**
 * Creates a "deleteChartAxis" operation.
 */
export function deleteChAxis(pos, axis) {
    return posOp(Op.DELETE_CHART_AXIS, pos, { axis });
}

/**
 * Creates a "changeChartAxis" operation.
 */
export function changeChAxis(pos, axis, attrs) {
    const props = { axis, attrs };
    // TODO DOCS-1783: move extra properties of "changeChartAxis" operation into "attrs"
    const props2 = attrs.axis ? { axPos: attrs.axis.axPos, crossAx: attrs.axis.crossAx } : null;
    if (attrs.axis && ("zAxis" in attrs.axis)) { props2.zAxis = attrs.axis.zAxis; }
    return posOp(Op.CHANGE_CHART_AXIS, pos, props, props2);
}

/**
 * Creates a "changeChartGrid" operation.
 */
export function changeChGrid(pos, axis, attrs) {
    return posOp(Op.CHANGE_CHART_GRID, pos, { axis, attrs });
}

/**
 * Creates a "changeChartTitle" operation.
 */
export function changeChTitle(pos, axis, attrs) {
    const props = {};
    if (is.number(axis)) { props.axis = axis; }
    if (is.dict(axis)) { props.attrs = axis; }
    if (is.dict(attrs)) { props.attrs = attrs; }
    return posOp(Op.CHANGE_CHART_TITLE, pos, props);
}

/**
 * Creates a "changeChartLegend" operation.
 */
export function changeChLegend(pos, attrs) {
    return posOp(Op.CHANGE_CHART_LEGEND, pos, { attrs });
}

/**
 * Creates an "insertText" operation for text contents.
 */
export function insertText(pos, text) {
    return posOp(Op.TEXT_INSERT, pos, { text });
}

/**
 * Creates an "insertTab" operation for text contents.
 */
export function insertTab(pos) {
    return posOp(Op.TAB_INSERT, pos);
}

/**
 * Creates an "insertHardBreak" operation for text contents.
 */
export function insertBreak(pos) {
    return posOp(Op.HARDBREAK_INSERT, pos);
}

/**
 * Creates an "insertParagraph" operation for text contents.
 */
export function insertPara(pos) {
    return posOp(Op.PARA_INSERT, pos);
}

/**
 * Creates a "splitParagraph" operation for text contents.
 */
export function splitPara(pos) {
    return posOp(Op.PARA_SPLIT, pos);
}

/**
 * Creates a "mergeParagraph" operation for text contents.
 */
export function mergePara(pos) {
    return posOp(Op.PARA_MERGE, pos);
}

/**
 * Creates a "setAttributes" operation for text contents.
 */
export function setAttrs(start, end, attrs) {
    const props = {};
    if (is.dict(end)) {
        props.attrs = end;
    } else {
        props.end = parsePos(end);
        props.attrs = attrs;
    }
    return posOp(Op.SET_ATTRIBUTES, start, props);
}

/**
 * Creates a "delete" operation for text contents.
 */
export function deleteOp(start, end) {
    return posOp(Op.DELETE, start, end ? { end: parsePos(end) } : null);
}

/**
 * Creates a synthetic "position" operation used in OT.
 */
export function positionOp(start) {
    return posOp(Op.POSITION, start);
}
