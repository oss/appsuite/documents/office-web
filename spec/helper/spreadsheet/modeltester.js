/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, dict, json } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/objects";

import { AttributedModel } from "@/io.ox/office/editframework/model/attributedmodel";
import { DrawingModel } from "@/io.ox/office/drawinglayer/model/drawingmodel";

import { Address, Range, RangeArray, isErrorCode } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { getModelValue, CellModel } from "@/io.ox/office/spreadsheet/model/cellmodel";
import { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";
import { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";
import { SharedFormulaModel } from "@/io.ox/office/spreadsheet/model/formula/sharedformulamodel";
import { NameModel } from "@/io.ox/office/spreadsheet/model/namemodel";
import { DVRuleModel } from "@/io.ox/office/spreadsheet/model/dvrulemodel";
import { CFRuleModel } from "@/io.ox/office/spreadsheet/model/cfrulemodel";
import { NoteModel } from "@/io.ox/office/spreadsheet/model/drawing/notemodel";
import { CommentModel } from "@/io.ox/office/spreadsheet/model/drawing/commentmodel";
import { SheetChartModel } from "@/io.ox/office/spreadsheet/model/drawing/chart/chartmodel";
import DataSeriesModel from "@/io.ox/office/spreadsheet/model/drawing/chart/dataseriesmodel";
import AxisModel from "@/io.ox/office/spreadsheet/model/drawing/chart/axismodel";
import TitleModel from "@/io.ox/office/spreadsheet/model/drawing/chart/titlemodel";

import { a, r, ia, ra } from "~/spreadsheet/sheethelper";

// private functions ==========================================================

// concatenates all string arguments with dots (unless a string starts with brackets)
function concatDescs(...args) {
    return args.flat().reduce((desc, arg) => {
        if (is.function(arg)) { arg = arg(); }
        if (desc && arg && (arg[0] !== "[")) { desc += "."; }
        return arg ? (desc + arg) : desc;
    }, "");
}

// convert error code objects to readable strings unlikely to equal real strings
function err2str(value) {
    return isErrorCode(value) ? ("\u200b" + value) : value;
}

// class Tester ===============================================================

/**
 * Base class of tester APIs used to wrap a specific model class.
 */
class Tester {
    constructor(parent, desc) {
        this._parent = parent;
        this._descs = [desc];
    }

    // public methods ---------------------------------------------------------

    /**
     * Executes the passed callback function with an extended description text.
     *
     * @param {string|()=>string} desc
     *  The description text to be appended to the base description text of
     *  this instance while the callback function is running.
     *
     * @param {()=>R} callback
     *  The callback function to be executed with an extended description text.
     *  Will be executed in the context of this instance.
     *
     * @returns {R}
     *  The return value of the callback function.
     */
    context(desc, callback) {
        this._descs.push(desc);
        const result = callback.call(this);
        this._descs.pop();
        return result;
    }

    /**
     * Returns the tester API of the parent model passed to the constructor.
     *
     * @returns {Tester}
     *  The tester API of the parent model, for chaining.
     */
    end() {
        return this._parent;
    }

    /**
     * Calls the global `expect()` function with a description text. The
     * description will be built from the description generator of the parent
     * tester API, the description generator passed to the constructor, and the
     * passed description text.
     *
     * @param {unknown} value
     *  The test value to be passed to `expect()`.
     *
     * @param {string} [desc]
     *  Additional text to be appended to the generated description.
     *
     * @returns {jest.CustomMatchers}
     *  The return value of the `expect()` call.
     */
    expect(value, desc) {
        desc = concatDescs(this._parent?._descs, this._descs, desc);
        // eslint-disable-next-line jest/valid-expect
        return expect(value, desc);
    }

    /**
     * Expects that the passed value is `undefined`.
     *
     * @param {unknown} value
     *  The test value to be passed to `expect()`.
     *
     * @param {string} [desc]
     *  Additional text to be appended to the generated description.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     */
    expectUndefined(value, desc) {
        // do not pass "value" to `expect` directly: on error, it will try to stringify the value, which will
        // lead to endless recursion for some model objects (e.g. due to back references to document model)
        this.expect((value instanceof DObject) ? { uid: value.uid } : value, desc).toBeUndefined();
        return this;
    }

    /**
     * Expects that the passed value is `null`.
     *
     * @param {unknown} value
     *  The test value to be passed to `expect()`.
     *
     * @param {string} [desc]
     *  Additional text to be appended to the generated description.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     */
    expectNull(value, desc) {
        // do not pass "value" to `expect` directly: on error, it will try to stringify the value, which will
        // lead to endless recursion for some model objects (e.g. due to back references to document model)
        this.expect((value instanceof DObject) ? { uid: value.uid } : value, desc).toBeNull();
        return this;
    }

    /**
     * Expects that the passed value is an instance of a specific class.
     *
     * @param {unknown} value
     *  The test value to be passed to `expect()`.
     *
     * @param {Function} Class
     *  The class constructor function.
     *
     * @param {string} [desc]
     *  Additional text to be appended to the generated description.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     */
    expectInstance(value, Class, desc) {
        this.expect(value, desc).toBeInstanceOf(Class);
        return this;
    }

    /**
     * Expects that an object contains a property.
     *
     * @param {object} obj
     *  The test object.
     *
     * @param {string} prop
     *  The name of the expected property.
     *
     * @param {unknown} value
     *  The expected value of the property.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     */
    expectProp(obj, prop, value) {
        this.expect(obj[prop], prop).toBe(value);
        return this;
    }

    /**
     * Expects that an attribute dictionary contains specific values.
     *
     * @param {Dict|AttributedModel} currAttrSet
     *  The model object with formatting attributes to be tested.
     *
     * @param {Dict} expAttrSet
     *  The expected (partial) attribute set.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     */
    expectAttrs(currAttrSet, expAttrSet) {
        this.context("attrs", () => {
            if (currAttrSet instanceof AttributedModel) {
                currAttrSet = currAttrSet.getMergedAttributeSet(true);
            }
            dict.forEach(expAttrSet, (attrs, family) => {
                this.context("[" + family + "]", () => {
                    const isAttrs = currAttrSet[family];
                    if (family === "styleId") {
                        this.expect(isAttrs).toBeString();
                        this.expect(isAttrs).toBe(attrs);
                    } else {
                        this.expect(isAttrs).toBeObject();
                        dict.forEach(attrs, (value, key) => {
                            const currStr = json.tryStringify(isAttrs[key], { sortKeys: true });
                            const expStr = json.tryStringify(value, { sortKeys: true });
                            this.expect(currStr, "[" + key + "]").toBe(expStr);
                        });
                    }
                });
            });
        });
        return this;
    }

    /**
     * Expects that the passed value is a cell range address.
     *
     * @param {unknown} value
     *  The value to be tested. If parameter `expected` is a string, this value
     *  is expected to be an instance of `Range` stringifying to the specified
     *  expectation. Otherwise, this value is expected to strictly equal `null`
     *  or `undefined`.
     *
     * @param {Nullable<string>} expected
     *  The expected value, either `null` or `undefined`, or the stringified
     *  range address.
     *
     * @param {string} [desc]
     *  Additional text to be appended to the generated description.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     */
    expectRange(value, expected, desc = "range") {
        this.context(desc, () => {
            if (is.string(expected)) {
                this.expectInstance(value, Range);
                this.expect(value.toOpStr()).toBe(r(expected).toOpStr());
            } else {
                this.expect(value).toBe(expected);
            }
        });
        return this;
    }

    /**
     * Expects that the passed value is a cell range list.
     *
     * @param {unknown} value
     *  The value to be tested. If parameter `expected` is a string, this value
     *  is expected to be an instance of `RangeArray` that will stringify to
     *  the specified expectation. Otherwise, this value is expected to
     *  strictly equal `null` or `undefined`.
     *
     * @param {Nullable<string>} expected
     *  The expected value, either `null` or `undefined`, or the stringified
     *  range addresses.
     *
     * @param {string|object} [options]
     *  Additional text to be appended to the generated description, or an
     *  object with optional parameters:
     *  - {string} [options.desc="ranges"]
     *    Additional text to be appended to the generated description.
     *  - {boolean} [options.ordered=false]
     *    If set to `true`, the ranges in `value` are expected to appear in the
     *    exact order as specified in `expected`. By default, the order of the
     *    ranges does not matter.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     */
    expectRanges(value, expected, options) {
        options = is.string(options) ? { desc: options } : (options || {});
        this.context(options.desc || "ranges", () => {
            if (is.string(expected)) {
                this.expectInstance(value, RangeArray);
                expected = ra(expected);
                if (!options.ordered) {
                    value = value.clone().sort();
                    expected.sort();
                }
                this.expect(value.toOpStr()).toBe(expected.toOpStr());
            } else {
                this.expect(value).toBe(expected);
            }
        });
        return this;
    }
}

// class CellModelTester ======================================================

/**
 * Tester API to for the class `CellModel`. An instance may also wrap an
 * undefined cell (a missing cell model instance).
 */
class CellModelTester extends Tester {

    constructor(sheetTester, address) {
        if (is.string(address)) { address = a(address); }
        super(sheetTester, "cell[" + address + "]");
        this.sheetModel = sheetTester.sheetModel;
        this.cellModel = this.sheetModel.cellCollection.getCellModel(address);
        this.address = address;
    }

    // public methods ---------------------------------------------------------

    /**
     * Expects that the cell is defined (the cell model exists).
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the cell model does not exist.
     */
    defined() {
        return this.expectInstance(this.cellModel, CellModel);
    }

    /**
     * Expects that the cell is undefined (the cell model does not exist).
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the cell model exists in the sheet.
     */
    undefined() {
        return this.expectNull(this.cellModel);
    }

    /**
     * Expects that the cell contains a specific value.
     *
     * @param {ScalarType|(value:ScalarType)=>boolean} value
     *  The expected value of the cell, or a predicate function that implements
     *  a custom test.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the cell does not contain the expected value.
     */
    value(value) {
        const currValue = getModelValue(this.cellModel);
        if (is.function(value)) {
            this.expect(currValue, "v").toSatisfy(value);
        } else {
            this.expect(err2str(currValue), "v").toBe(err2str(value));
        }
        return this;
    }

    /**
     * Expects that the cell is blank (regardless if the cell model exists).
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the cell contains a value except `null`.
     */
    blank() {
        return this.value(null);
    }

    /**
     * Expects that the cell contains a specific auto-style identifier, or
     * specific formatting attributes.
     *
     * @param {string|Dict} style
     *  The expected auto-style identifier of the cell, or the expected
     *  (partial) attribute set.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the cell does not contain the expected auto-style or formatting
     *  attributes.
     */
    style(style) {
        if (is.string(style)) {
            const id = this.sheetModel.cellCollection.getStyleId(this.address);
            this.expect(id, "s").toBe(style);
        } else {
            const attrs = this.sheetModel.cellCollection.getAttributeSet(this.address);
            this.expectAttrs(attrs, style);
        }
        return this;
    }

    /**
     * Expects that the cell contains a specific formula expression.
     *
     * @param {FormulaSpec} spec
     *  The expected formula expression of the cell. Can be a string to expect
     *  the formula expression, `null` to expect that there is no formula in
     *  the cell, or a descriptor with the following optional properties:
     *  - {string|null} f -- The expected formula expression, or null to expect
     *    that there is no formula in the cell.
     *  - {number|null|string} si -- The expected shared formula index. May be
     *    a number (exact expected index), or `null` to expect that the formula
     *    is not shared, or the address of a cell (in A1 notation) expected to
     *    have the same shared index, or the value "*" to expect that the cell
     *    contains a shared index regardless of its value.
     *  - {string|null} sr -- The expected bounding range of a shared formula,
     *    in A1 notation; or `null` to expect that there is no shared bounding
     *    range.
     *  - {string|null} mr -- The expected bounding range of a matrix formula,
     *    in A1 notation; or `null` to expect that there is no matrix range.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the cell does not contain the expected formula settings.
     */
    formula(spec) {
        // no formula expected: cell may be undefined
        if (spec === null) {
            if (this.cellModel) { this.expectProp(this.cellModel, "f", null); }
        } else {
            if (!is.dict(spec)) { spec = { f: spec }; }
            // otherwise, cell must be defined
            this.defined();
            if (spec.f !== undefined) {
                this.expectProp(this.cellModel, "f", spec.f);
            }
            const { si } = spec;
            if (si !== undefined) {
                this.context("shared.index", () => {
                    const sf = this.cellModel?.sf;
                    if (si === "*") {
                        this.expectInstance(sf, SharedFormulaModel);
                    } else if (is.string(si)) {
                        this.expectInstance(sf, SharedFormulaModel);
                        const otherModel = this.sheetModel.cellCollection.getCellModel(a(si));
                        this.expect(sf.index).toBe(otherModel?.sf?.index);
                    } else if (si === null) {
                        this.expectUndefined(sf);
                    } else {
                        this.expectInstance(sf, SharedFormulaModel);
                        this.expect(sf.index).toBe(si);
                    }
                });
            }
            const { sr } = spec;
            if (sr !== undefined) {
                this.context("shared.ref", () => {
                    const sf = this.cellModel?.sf;
                    if (sr === null) {
                        this.expect(this.cellModel.isSharedAnchor()).toBeFalse();
                    } else {
                        this.expectInstance(sf, SharedFormulaModel);
                        this.expect(this.cellModel.isSharedAnchor()).toBeTrue();
                        this.expectRange(sf.boundRange, sr);
                    }
                });
            }
            if (spec.mr !== undefined) {
                this.expectRange(this.cellModel.mr, spec.mr, "mr");
            }
        }
        return this;
    }

    /**
     * Expects that the cell does not contain a formula expression.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the cell contains a formula expression.
     */
    noFormula() {
        if (this.cellModel) {
            this.expectProp(this.cellModel, "f", null);
            this.expectProp(this.cellModel, "si", null);
            this.expectProp(this.cellModel, "sr", null);
            this.expectProp(this.cellModel, "mr", null);
        }
        return this;
    }
}

// class ChartModelTester =====================================================

/**
 * Tester API for the class `SheetChartModel`.
 */
class ChartModelTester extends Tester {

    constructor(sheetTester, chartModel) {
        super(sheetTester, () => `chart[${chartModel.getPosition().join()}]`);
        this.chartModel = chartModel;
    }

    // public methods ---------------------------------------------------------

    /**
     * Expects that a data series exists in this chart object.
     *
     * @param {number} series
     *  The index of a data series.
     *
     * @param {Dict} [attrs]
     *  The expected (partial) attribute set of the data series.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the chart object, or the data series does not exist.
     */
    series(series, attrs) {
        this.context(`series[${series}]`, () => {
            const seriesModel = this.chartModel.getSeriesModel(series);
            this.expectInstance(seriesModel, DataSeriesModel);
            this.expectAttrs(seriesModel, attrs);
        });
        return this;
    }

    /**
     * Expects that an axis exists in this chart object.
     *
     * @param {string} axis
     *  The identifier of an axis.
     *
     * @param {Dict} [attrs]
     *  The expected (partial) attribute set of the axis.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the chart object, or the axis does not exist.
     */
    axis(axis, attrs) {
        this.context(`axis[${axis}]`, () => {
            const axisModel = this.chartModel.getAxisModel(axis);
            this.expectInstance(axisModel, AxisModel);
            this.expectAttrs(axisModel, attrs);
        });
        return this;
    }

    /**
     * Expects that a title exists in this chart object.
     *
     * @param {string|null} axis
     *  The axis identifier of a chart title, or `null` for the main title.
     *
     * @param {Dict} [attrs]
     *  The expected (partial) attribute set of the chart title.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the chart object, or the chart title does not exist.
     */
    title(axis, attrs) {
        this.context((axis ? `axis[${axis}].` : "") + "title", () => {
            const titleModel = this.chartModel.getTitleModel(axis);
            this.expectInstance(titleModel, TitleModel);
            this.expectAttrs(titleModel, attrs);
        });
        return this;
    }
}

// class NameCollectionTester =================================================

/**
 * Shared tester API to be attached to a document or sheet model. Provides
 * methods for defined names which can be part of the document model
 * (global names), or the sheet model (sheet-local names).
 */
class NameCollectionTester extends Tester {

    constructor(parentTester, nameCollection, desc) {
        super(parentTester, desc);
        this.nameCollection = nameCollection;
    }

    // public methods ---------------------------------------------------------

    /**
     * Expects that a defined name exists.
     *
     * @param {string} label
     *  The label of a defined name.
     *
     * @param {string} [formula]
     *  The expected formula expression.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the defined name does not exist, or does not contain the specified
     *  settings.
     */
    defName(label, formula) {
        this.withName(label, nameModel => {
            this.expectInstance(nameModel, NameModel);
            if (formula !== undefined) {
                this.expect(nameModel.getFormula("op"), "formula").toBe(formula);
            }
        });
        return this;
    }

    /**
     * Expects that a defined name does not exist.
     *
     * @param {string} label
     *  The label of a defined name.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the specified name exists.
     */
    noDefName(label) {
        this.withName(label, this.expectUndefined);
        return this;
    }

    // private methods --------------------------------------------------------

    withName(label, callback) {
        return this.context(`name[${label}]`, () => {
            const nameModel = this.nameCollection.getNameModel(label);
            callback.call(this, nameModel);
            return nameModel;
        });
    }
}

// class SheetModelTester =====================================================

/**
 * Tester API for the class `SheetModel`.
 */
class SheetModelTester extends NameCollectionTester {

    constructor(docTester, sheetModel) {
        super(docTester, sheetModel.nameCollection, () => `sheet[${sheetModel.getIndex()}]`);
        this.sheetModel = sheetModel;
    }

    // public methods ---------------------------------------------------------

    /**
     * Expects that the sheet has a specific name.
     *
     * @param {string} name
     *  The expected name of the sheet.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the sheet does not have the expected name.
     */
    name(name) {
        this.expect(this.sheetModel.getName(), "name").toBe(name);
        return this;
    }

    /**
     * Expects that one or more columns in this sheet contain specific
     * settings.
     *
     * @param {number|string} index
     *  The zero-based index of a column; or an interval list in A1 notation.
     *
     * @param {string} [id]
     *  The expected auto-style identifier of all columns.
     *
     * @param {Dict} [attrs]
     *  The expected column and cell attributes.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If any of the columns does not contain the expected settings.
     */
    cols(index, id, attrs) {
        if (is.dict(id)) { attrs = id; id = undefined; }
        const intervals = ia(is.string(index) ? index : String(index + 1));
        for (const colDesc of this.sheetModel.colCollection.indexEntries(intervals)) {
            this.context(`cols[${colDesc.uniqueInterval.toOpStr(true)}]`, () => {
                if (id !== undefined) {
                    this.expectProp(colDesc, "style", id);
                }
                if (attrs !== undefined) {
                    const currAttrs = { column: colDesc.merged, ...colDesc.attributes };
                    this.expectAttrs(currAttrs, attrs);
                }
            });
        }
        return this;
    }

    /**
     * Expects that one or more rows in this sheet contain specific settings.
     *
     * @param {number|string} index
     *  The zero-based index of a row; or an interval list in A1 notation.
     *
     * @param {string} [id]
     *  The expected auto-style identifier of all rows.
     *
     * @param {Dict} [attrs]
     *  The expected row and cell attributes.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If any of the rows does not contain the expected settings.
     */
    rows(index, id, attrs) {
        if (is.dict(id)) { attrs = id; id = undefined; }
        const intervals = ia(is.string(index) ? index : String(index + 1));
        for (const rowDesc of this.sheetModel.rowCollection.indexEntries(intervals)) {
            this.context(`rows[${rowDesc.uniqueInterval.toOpStr(false)}]`, () => {
                if (id !== undefined) {
                    this.expectProp(rowDesc, "style", id);
                }
                if (attrs !== undefined) {
                    const currAttrs = { row: rowDesc.merged, ...rowDesc.attributes };
                    this.expectAttrs(currAttrs, attrs);
                }
            });
        }
        return this;
    }

    /**
     * Returns the tester API of the specified cell. The cell may also be
     * undefined (it does not contain a cell model).
     *
     * @param {string|Address} address
     *  The address of a cell, in A1 notation.
     *
     * @returns {CellModelTester}
     *  The tester API of the specified cell.
     */
    cell(address) {
        return new CellModelTester(this, address);
    }

    /**
     * Expects that multiple cells contain specific values.
     *
     * @param {string|Address|Range|Dict<ScalarType>} source
     *  The address of a cell (in A1 notation or as `Address`), or the address
     *  of a cell range (in A1 notation or as `Range`); or a dictionary mapping
     *  cell addresses to expected cell values.
     *
     * @param {CellMapping<ScalarType|((v:ScalarType)=>boolean)>} values
     *  The expected value of the cell; or a column vector (a one-dimensional
     *  array) with expected values in the column starting at the specified
     *  cell, or a matrix (a two-dimensional array) with expected values in the
     *  cell range starting at the specified cell. Either value can also be a
     *  predicate function to implement a custom test.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If any of the cells does not contain the expected values.
     */
    values(source, values) {
        return this.iterateCells(source, values, (cell, v) => cell.value(v));
    }

    /**
     * Expects that multiple cells contain specific auto-styles or formatting
     * attributes.
     *
     * @param {string|Address|Range|Dict<string|Dict>} source
     *  The address of a cell (in A1 notation or as `Address`), or the address
     *  of a cell range (in A1 notation or as `Range`); or a dictionary mapping
     *  cell addresses to expected cell auto-style identifiers or formatting
     *  attributes.
     *
     * @param {CellMapping<string|Dict>} styles
     *  The expected auto-style identifier or attribute set of the cell; or a
     *  column vector (a one-dimensional array) with expected auto-style
     *  identifiers or attribute sets in the column starting at the specified
     *  cell, or a matrix (a two-dimensional array) with expected auto-style
     *  identifiers or attribute sets in the cell range starting at the
     *  specified cell.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If any of the cells does not contain the expected auto-styles or
     *  formatting attributes.
     */
    styles(source, styles) {
        return this.iterateCells(source, styles, (cell, style) => {
            if (style !== null) { cell.style(style); }
        });
    }

    /**
     * Expects that multiple cells contain specific formula expressions.
     *
     * @param {string|Address|Range|Dict<FormulaSpec>} source
     *  The address of a cell (in A1 notation or as `Address`), or the address
     *  of a cell range (in A1 notation or as `Range`); or a dictionary mapping
     *  cell addresses to expected formula expressions.
     *
     * @param {CellMapping<FormulaSpec>} specs
     *  The expected formula expressions of the cell; or a column vector (a
     *  one-dimensional array) with expected formula expressions in the column
     *  starting at the specified cell, or a matrix (a two-dimensional array)
     *  with expected formula expressions in the cell range starting at the
     *  specified cell.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If any of the cells does not contain the expected formula expression.
     */
    formulas(source, specs) {
        return this.iterateCells(source, specs, (cell, spec) => cell.formula(spec));
    }

    /**
     * Expects that no cell model exists in a range of cells.
     *
     * @param {string} ranges
     *  The addresses of one or multiple cell ranges, in A1 notation.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If any of the cells contains a cell model.
     */
    noCells(ranges) {
        return this.iterateRanges(ranges, cell => cell.undefined());
    }

    /**
     * Expects that no cell model exists in a range of cells.
     *
     * @param {string} ranges
     *  The addresses of one or multiple cell ranges, in A1 notation.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If any of the cells contains a cell model.
     */
    blankCells(ranges) {
        return this.iterateRanges(ranges, cell => cell.blank());
    }

    /**
     * Expects that this sheet contains a specific shared formula.
     *
     * @param {number} index
     *  The index of the expected shared formula.
     *
     * @param {string} formula
     *  The formula expression used by the reference cell.
     *
     * @param {string} ranges
     *  The addresses of all member cells. Can be range addresses.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the shared formula does not exist, or does not contain the specified
     *  settings.
     */
    sharedFormula(index, formula, ranges) {
        this.withSharedFormula(index, sharedModel => {
            this.expectInstance(sharedModel, SharedFormulaModel);
            this.expect(sharedModel.getFormulaOp(sharedModel.anchorAddress), "formula").toBe(formula);
            ranges = ra(ranges).merge();
            this.expectRanges(RangeArray.mergeAddresses(sharedModel.addressSet), ranges.toOpStr(), "addresses");
            this.expectRange(sharedModel.boundRange, ranges.boundary().toOpStr(), "boundary");
        });
        return this;
    }

    /**
     * Expects that this sheet does not contain a specific shared formula.
     *
     * @param {number} index
     *  The index of a shared formula.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the shared formula exists.
     */
    noSharedFormula(index) {
        return this.withSharedFormula(index, this.expectUndefined);
    }

    /**
     * Expects that one or more ranges are merged.
     *
     * @param {string} ranges
     *  The ranges expected to be merged, in A1 range list notation.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If any of the specified ranges is not merged.
     */
    merged(ranges) {
        this.context("merged", () => {
            const collection = this.sheetModel.mergeCollection;
            for (const range of ra(ranges)) {
                const desc = `[${range}]`;
                this.expect(collection.isMergedRange(range), desc).toBeTrue();
            }
        });
        return this;
    }

    /**
     * Expects that one or more ranges do not overlap with any merged range.
     *
     * @param {string} ranges
     *  The ranges expected not to be merged, in A1 range list notation.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If any of the specified ranges overlaps with a merged range.
     */
    notMerged(ranges) {
        this.context("merged", () => {
            const collection = this.sheetModel.mergeCollection;
            for (const range of ra(ranges)) {
                const desc = `[${range}]`;
                this.expect(collection.coversAnyMergedRange(range), desc).toBeFalse();
            }
        });
        return this;
    }

    /**
     * Expects that one or more ranges contain a hyperlink.
     *
     * @param {string} url
     *  The expected URL.
     *
     * @param {string} ranges
     *  The ranges expected to contain a hyperlink, in A1 range list notation.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If any of the specified ranges does not contain the hyperlink.
     */
    hlink(url, ranges) {
        this.context(`hlink[${url}]`, () => {
            const collection = this.sheetModel.hlinkCollection;
            this.expectRanges(collection.getRangesForURL(url), ranges);
        });
        return this;
    }

    /**
     * Expects that one or more ranges do not overlap with any hyperlink.
     *
     * @param {string} ranges
     *  The ranges expected not to overlap with a hyperlink, in A1 range list
     *  notation.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If any of the specified ranges overlaps with a hyperlink.
     */
    noHlink(ranges) {
        this.context("nohlink", () => {
            const collection = this.sheetModel.hlinkCollection;
            for (const range of ra(ranges)) {
                const desc = `[${range}]`;
                this.expect(collection.coversAnyLinkRange(range), desc).toBeFalse();
            }
        });
        return this;
    }

    /**
     * Expects that a specific number of table ranges exist in the sheet.
     *
     * @param {number} count
     *  The expected number of table ranges.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the number of table ranges does not match.
     */
    tableCount(count) {
        this.context("tables.count", () => {
            this.expect(Array.from(this.sheetModel.tableCollection.yieldTableModels())).toHaveLength(count);
        });
        return this;
    }

    /**
     * Expects that a table range exists.
     *
     * @param {string} name
     *  The name of a table range.
     *
     * @param {string} [range]
     *  The expected range of the table, in A1 notation.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the table range does not exist, or does not contain the specified
     *  settings.
     */
    table(name, range) {
        this.withTable(name, tableModel => {
            this.expectInstance(tableModel, TableModel);
            if (range !== undefined) {
                this.expectRange(tableModel.getRange(), range);
            }
        });
        return this;
    }

    /**
     * Expects that a table range does not exist.
     *
     * @param {string} name
     *  The name of a table range.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the table range exists.
     */
    noTable(name) {
        return this.withTable(name, this.expectUndefined);
    }

    /**
     * Expects that an auto-filter range exists.
     *
     * @param {string} [range]
     *  The expected range of the auto-filter, in A1 notation.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the auto-filter range does not exist, or does not contain the
     *  specified settings.
     */
    autoFilter(range) {
        this.withAutoFilter(tableModel => {
            this.expectInstance(tableModel, TableModel);
            if (range !== undefined) {
                this.expectRange(tableModel.getRange(), range);
            }
        });
        return this;
    }

    /**
     * Expects that the auto-filter range does not exist.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the auto-filter range exists.
     */
    noAutoFilter() {
        return this.withAutoFilter(this.expectUndefined);
    }

    /**
     * Expects that a specific number of data validation rules exist in the
     * sheet.
     *
     * @param {number} count
     *  The expected number of data validation rules.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the number of data validation rules does not match.
     */
    dvRuleCount(count) {
        this.context("dvRules.count", () => {
            this.expect(Array.from(this.sheetModel.dvRuleCollection.yieldRuleModels())).toHaveLength(count);
        });
        return this;
    }

    /**
     * Expects that a data validation rule exists.
     *
     * @param {number} index
     *  The operation index of a data validation rule.
     *
     * @param {string} [ranges]
     *  The expected target ranges of the rule, in A1 range list notation.
     *
     * @param {Dict} [attrs]
     *  The expected (partial) attribute set of the rule.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the data validation rule does not exist, or does not contain the
     *  specified settings.
     */
    dvRule(index, ranges, attrs) {
        if (is.dict(ranges)) { attrs = ranges; ranges = undefined; }
        this.withDvRule(index, ruleModel => {
            this.expectInstance(ruleModel, DVRuleModel);
            if (ranges !== undefined) {
                this.expectRanges(ruleModel.getTargetRanges(), ranges);
            }
            this.expectAttrs(ruleModel, attrs);
        });
        return this;
    }

    /**
     * Expects that a data validation rule does not exist.
     *
     * @param {number} index
     *  The operation index of a data validation rule.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the specified rule exists.
     */
    noDvRule(index) {
        return this.withDvRule(index, this.expectUndefined);
    }

    /**
     * Expects that a specific number of conditional formatting rules exist in
     * the sheet.
     *
     * @param {number} count
     *  The expected number of conditional formatting rules.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the number of conditional formatting rules does not match.
     */
    cfRuleCount(count) {
        this.context("cfRules.count", () => {
            this.expect(Array.from(this.sheetModel.cfRuleCollection.yieldRuleModels())).toHaveLength(count);
        });
        return this;
    }

    /**
     * Expects that a conditional formatting rule exists.
     *
     * @param {string|number} id
     *  The operation identifier of a conditional formatting rule (OOXML), or
     *  the zero-based array index of the rule (ODF).
     *
     * @param {string} [ranges]
     *  The expected target ranges of the rule, in A1 range list notation.
     *
     * @param {Dict} [attrs]
     *  The expected (partial) attribute set of the rule.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the conditional formatting rule does not exist, or does not contain
     *  the specified settings.
     */
    cfRule(id, ranges, attrs) {
        if (is.dict(ranges)) { attrs = ranges; ranges = undefined; }
        this.withCfRule(id, ruleModel => {
            this.expectInstance(ruleModel, CFRuleModel);
            if (ranges !== undefined) {
                this.expectRanges(ruleModel.getTargetRanges(), ranges);
            }
            this.expectAttrs(ruleModel, attrs);
        });
        return this;
    }

    /**
     * Expects that a conditional formatting rule does not exist.
     *
     * @param {string|number} id
     *  The operation identifier of a conditional formatting rule (OOXML), or
     *  the zero-based array index of the rule (ODF).
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the specified rule exists.
     */
    noCfRule(id) {
        return this.withCfRule(id, this.expectUndefined);
    }

    /**
     * Expects that a specific number of drawing objects exist in the sheet.
     *
     * @param {number} count
     *  The expected number of drawing objects.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the number of drawing objects does not match.
     */
    drawingCount(count) {
        this.context("drawings.count", () => {
            this.expect(this.sheetModel.drawingCollection.getModelCount()).toBe(count);
        });
        return this;
    }

    /**
     * Expects that a drawing object exists.
     *
     * @param {number} pos
     *  The position of a drawing object.
     *
     * @param {DrawingType} [type]
     *  The expected type of the drawing object.
     *
     * @param {Dict} [attrs]
     *  The expected (partial) attribute set of the drawing object.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the drawing object does not exist.
     */
    drawing(pos, type, attrs) {
        if (is.dict(type)) { attrs = type; type = undefined; }
        this.withDrawing(pos, type, drawingModel => {
            this.expectInstance(drawingModel, DrawingModel);
            this.expectAttrs(drawingModel, attrs);
        });
        return this;
    }

    /**
     * Expects that a drawing object does not exist.
     *
     * @param {number} pos
     *  The position of a drawing object.
     *
     * @param {DrawingType} [type]
     *  The type of the drawing object not expected to exist. If not specified,
     *  no drawing object must exist at all. If specified, a drawing object
     *  with another type may exist at the specified position.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the drawing object exists.
     */
    noDrawing(pos, type) {
        return this.withDrawing(pos, type, this.expectUndefined);
    }

    /**
     * Expects that a chart object exists.
     *
     * @param {number} pos
     *  The position of a drawing object.
     *
     * @param {Dict} [attrs]
     *  The expected (partial) attribute set of the chart object.
     *
     * @returns {ChartModelTester}
     *  The tester API of the specified chart object.
     *
     * @throws
     *  If the chart object does not exist.
     */
    chart(pos, attrs) {
        return this.withDrawing(pos, "chart", chartModel => {
            this.expectInstance(chartModel, SheetChartModel);
            this.expectAttrs(chartModel, attrs);
            return new ChartModelTester(this, chartModel);
        });
    }

    /**
     * Expects that a chart object does not exist.
     *
     * @param {number} pos
     *  The position of a drawing object.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the chart object exists.
     */
    noChart(pos) {
        return this.noDrawing(pos, "chart");
    }

    /**
     * Expects that a specific number of cell notes exist in the sheet.
     *
     * @param {number} count
     *  The expected number of cell notes.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the number of cell notes does not match.
     */
    noteCount(count) {
        this.context("notes.count", () => {
            this.expect(this.sheetModel.noteCollection.getModelCount()).toBe(count);
        });
        return this;
    }

    /**
     * Expects that a cell note exists.
     *
     * @param {string|Address} address
     *  The address of an anchor cell.
     *
     * @param {string} [text]
     *  The expected text in the note shape.
     *
     * @param {Dict} [attrs]
     *  The expected (partial) attribute set of the note shape.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the cell note does not exist.
     */
    note(address, text, attrs) {
        if (is.dict(text)) { attrs = text; text = undefined; }
        this.withNote(address, noteModel => {
            this.expectInstance(noteModel, NoteModel);
            if (text !== undefined) {
                this.expect(noteModel.getText(), "text").toBe(text);
            }
            this.expectAttrs(noteModel, attrs);
        });
        return this;
    }

    /**
     * Expects that a cell note does not exist.
     *
     * @param {string|Address} address
     *  The address of an anchor cell.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the cell contains a note.
     */
    noNote(address) {
        return this.withNote(address, this.expectUndefined);
    }

    /**
     * Expects that a specific number of cell comments exist in the sheet.
     *
     * @param {number} count
     *  The expected number of cell comments.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the number of cell comments does not match.
     */
    commentCount(count) {
        this.context("comments.count", () => {
            this.expect(this.sheetModel.commentCollection.getCommentModels()).toHaveLength(count);
        });
        return this;
    }

    /**
     * Expects that a cell comment exists.
     *
     * @param {string|Address} address
     *  The address of an anchor cell.
     *
     * @param {number} index
     *  The index of the cell comment in its thread.
     *
     * @param {string} [text]
     *  The expected text in the cell comment.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the cell comment does not exist.
     */
    comment(address, index, text) {
        this.withComments(address, commentModels => {
            this.expect(commentModels).toBeArray();
            this.context(`[${index}]`, () => {
                const commentModel = commentModels[index];
                this.expectInstance(commentModel, CommentModel);
                if (text !== undefined) {
                    this.expect(commentModel.getText(), "text").toBe(text);
                }
            });
        });
        return this;
    }

    /**
     * Expects that a cell comment thread does not exist.
     *
     * @param {string|Address} address
     *  The address of an anchor cell.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the cell contains a comment thread.
     */
    noComment(address) {
        return this.withComments(address, this.expectUndefined);
    }

    // private methods --------------------------------------------------------

    iterateCells(source, values, callback) {

        // try to convert `source` to a range address
        let range;
        if (is.string(source)) {
            range = Range.parse(source);
        } else if (source instanceof Address) {
            range = new Range(source);
        } else if (source instanceof Range) {
            range = source;
        }

        // process dictionary of address/expected pairs
        if (!range) {
            dict.forEach(source, (value, key) => {
                callback(this.cell(key), value);
            });
            return this;
        }

        // process arrays of expected values
        if (is.array(values)) {
            const a1 = range.a1.clone();
            for (const vec of values) {
                if (is.array(vec)) {
                    for (const v of vec) {
                        callback(this.cell(a1), v);
                        a1.c += 1;
                    }
                    a1.c = range.a1.c;
                } else {
                    callback(this.cell(a1), vec);
                }
                a1.r += 1;
            }
            return this;
        }

        // process single expected value for all cells in the range
        for (const address of range.addresses()) {
            callback(this.cell(address), values);
        }
        return this;
    }

    iterateRanges(ranges, callback) {
        for (const address of ra(ranges).addresses()) {
            callback(this.cell(address));
        }
        return this;
    }

    withSharedFormula(index, callback) {
        return this.context(`shfmla[${index}]`, () => {
            return callback.call(this, this.sheetModel.cellCollection.sharedFormulas.get(index));
        });
    }

    withTable(name, callback) {
        return this.context(`table[${name}]`, () => {
            return callback.call(this, this.sheetModel.tableCollection.getTableModel(name));
        });
    }

    withAutoFilter(callback) {
        return this.context("autofilter", () => {
            return callback.call(this, this.sheetModel.tableCollection.getAutoFilterModel());
        });
    }

    withDvRule(index, callback) {
        return this.context(`dvrule[${index}]`, () => {
            return callback.call(this, this.sheetModel.dvRuleCollection.getRuleModel(index));
        });
    }

    withCfRule(id, callback) {
        return this.context(`cfrule[${id}]`, () => {
            return callback.call(this, this.sheetModel.cfRuleCollection.getRuleModel(String(id)));
        });
    }

    withDrawing(pos, type, callback) {
        return this.context((type || "drawing") + `[${pos}]`, () => {
            const drawingModel = this.sheetModel.drawingCollection.getModel([pos]);
            return callback.call(this, (drawingModel && type && (type !== drawingModel.drawingType)) ? undefined : drawingModel);
        });
    }

    withNote(address, callback) {
        if (is.string(address)) { address = a(address); }
        return this.context(`note[${address}]`, () => {
            return callback.call(this, this.sheetModel.noteCollection.getByAddress(address));
        });
    }

    withComments(address, callback) {
        if (is.string(address)) { address = a(address); }
        return this.context(`comments[${address}]`, () => {
            return callback.call(this, this.sheetModel.commentCollection.getByAddress(address));
        });
    }
}

// class SpreadsheetModelTester ===============================================

/**
 * Tester API for the class `SpreadsheetModel`.
 *
 * @example
 *  const docTester = new SpreadsheetModelTester(docModel);
 *  docTester.sheet(0, "Sheet1");
 */
export class SpreadsheetModelTester extends NameCollectionTester {

    constructor(docModel) {
        super(null, docModel.nameCollection, "doc");
        this.docModel = docModel;
    }

    // public methods ---------------------------------------------------------

    /**
     * Expects that the document contains a specific number of sheets.
     *
     * @param {number} count
     *  The expected number of sheet models.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the number of sheets does not match.
     */
    sheetCount(count) {
        this.context("sheets.count", () => {
            this.expect(this.docModel.getSheetCount()).toBe(count);
        });
        return this;
    }

    /**
     * Expects that a sheet model exists.
     *
     * @param {number} sheet
     *  The zero-based index of a sheet.
     *
     * @param {string} [name]
     *  If specified, the expected name of the sheet.
     *
     * @returns {SheetModelTester}
     *  The tester API of the specified sheet.
     *
     * @throws
     *  If the sheet model does not exist.
     */
    sheet(sheet, name) {
        return this.withSheet(sheet, sheetModel => {
            this.expectInstance(sheetModel, SheetModel);
            if (name !== undefined) {
                this.expect(sheetModel.getName(), "name").toBe(name);
            }
            return new SheetModelTester(this, sheetModel);
        });
    }

    /**
     * Expects that a sheet model does not exist.
     *
     * @param {number} sheet
     *  The zero-based index of a sheet.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the sheet model exists.
     */
    noSheet(sheet) {
        return this.withSheet(sheet, this.expectUndefined);
    }

    /**
     * Expects that a table range exists.
     *
     * @param {string} name
     *  The name of a table range.
     *
     * @param {number} [sheet]
     *  The expected index of the sheet containing the table range.
     *
     * @param {string} [range]
     *  The expected range of the table, in A1 notation.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the table range does not exist, or does not contain the specified
     *  settings.
     */
    table(name, sheet, range) {
        if (is.string(sheet)) { range = sheet; sheet = undefined; }
        this.withTable(name, tableModel => {
            this.expectInstance(tableModel, TableModel);
            if (sheet !== undefined) {
                this.expect(tableModel.sheetModel.getIndex(), "sheet").toBe(sheet);
            }
            if (range !== undefined) {
                this.expectRange(tableModel.getRange(), range);
            }
        });
        return this;
    }

    /**
     * Expects that a table range does not exist.
     *
     * @param {string} name
     *  The name of a table range.
     *
     * @returns {this}
     *  A reference to this instance, for chaining.
     *
     * @throws
     *  If the table range exists.
     */
    noTable(name) {
        return this.withTable(name, this.expectUndefined);
    }

    // private methods --------------------------------------------------------

    withSheet(index, callback) {
        return this.context(`sheet[${index}]`, () => {
            return callback.call(this, this.docModel.getSheetModel(index));
        });
    }

    withTable(name, callback) {
        return this.context(`table[${name}]`, () => {
            return callback.call(this, this.docModel.getTableModel(name));
        });
    }
}
