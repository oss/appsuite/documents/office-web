/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, str } from "@/io.ox/office/tk/algorithms";

import { AppType } from "@/io.ox/office/baseframework/utils/apputils";

import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { isErrorCode, mapKey, Address, Range } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";

import { registerMockApp, MockTextFrameworkView, MockEditController } from "~/edit/apphelper";
import * as op from "~/spreadsheet/sheethelper";
import { SpreadsheetModelTester } from "~/spreadsheet/modeltester";
import { FunctionModuleTester } from "~/spreadsheet/functiontester";

// re-exports =================================================================

export { defineModelTestWithUndo } from "~/edit/apphelper";

export * from "~/spreadsheet/sheethelper";

// constants ==================================================================

const DEFAULT_OPERATIONS = [
    op.changeConfig({ document: { cols: 16384, rows: 1048576, activeSheet: 0 } }),
    op.insertSheet(0, "Sheet1")
];

const OPERATION_ERRORCODES = {
    "#NULL": "#NULL!",
    "#DIV0": "#DIV/0!",
    "#VALUE": "#VALUE!",
    "#REF": "#REF!",
    "#NAME": "#NAME?",
    "#NUM": "#NUM!",
    "#NA": "#N/A",
    "#DATA": "#GETTING_DATA"
};

const SPREADSHEET_OPERATION_CONVERTERS = {};
SPREADSHEET_OPERATION_CONVERTERS[Op.CHANGE_CELLS] = operation => {
    if (!is.array(operation.contents)) { return; }
    const start = Address.parse(operation.start);
    const address = start.clone();
    const contents = {};
    for (let rowData of operation.contents) {
        if (is.array(rowData)) { rowData = { c: rowData }; }
        const rowRepeat = rowData.r || 1;
        if (is.array(rowData.c)) {
            for (let cellData of rowData.c) {
                cellData = (cellData === null) ? {} :
                    isErrorCode(cellData) ? { e: OPERATION_ERRORCODES[cellData.toString()] || "#N/A" } :
                    is.dict(cellData) ? { ...cellData } :
                    { v: cellData };
                const colRepeat = cellData.r || 1;
                delete cellData.r;
                if (!is.empty(cellData)) {
                    const range = Range.fromAddressAndSize(address, colRepeat, rowRepeat);
                    contents[range.toOpStr()] = cellData;
                }
                address.c += colRepeat;
            }
        }
        address.c = start.c;
        address.r += rowRepeat;
    }
    operation.contents = contents;
    delete operation.start;
};

// class MockSpreadsheetView ==================================================

class MockSpreadsheetView extends MockTextFrameworkView {
}

// class MockSpreadsheetController ============================================

class MockSpreadsheetController extends MockEditController {
}

// public functions ===========================================================

/**
 * Creates a test application with a real spreadsheet document model, and
 * applies the passed document operations in order to initialize the document
 * model.
 *
 * @param {FileFormatType} fileFormat
 *  The file format specifier.
 *
 * @param {Operation[]} [operations]
 *  The initial document operations to be applied after creating the
 *  application.
 *
 * @param {MockAppOptions} [options]
 *  Optional parameters.
 *
 * @returns {JPromise<SpreadsheetApp>}
 *  A promise that will fulfil with the application instance containing a mock
 *  spreadsheet application.
 */
export function createSpreadsheetApp(fileFormat, operations, options) {

    return registerMockApp({
        appType: AppType.SPREADSHEET,
        fileFormat,
        ResourceManagerClass: { path: "@/io.ox/office/spreadsheet/resource/resourcemanager",            symbol: "SpreadsheetResourceManager" },
        ModelClass:           { path: "@/io.ox/office/spreadsheet/model/spreadsheetmodel",              symbol: "SpreadsheetModel" },
        ViewClass:            { path: "@/io.ox/office/spreadsheet/view/spreadsheetview",                symbol: "SpreadsheetView",          mock: MockSpreadsheetView },
        ControllerClass:      { path: "@/io.ox/office/spreadsheet/controller/spreadsheetcontroller",    symbol: "SpreadsheetController",    mock: MockSpreadsheetController },
        OTManagerClass:       { path: "@/io.ox/office/spreadsheet/model/operation/otmanager",           symbol: "SpreadsheetOTManager" },
        operations: operations || DEFAULT_OPERATIONS,
        converters: SPREADSHEET_OPERATION_CONVERTERS,
        async finalizer(_docApp, docModel) {
            docModel.expect = new SpreadsheetModelTester(docModel);
            await docModel.postProcessImport();
        }
    }, options);
}

/**
 * Returns a new tester for a function implementation module.
 *
 * @param {Dict} funcModule
 *  The function module. Is expected to be a simple object with function
 *  descriptors, mapped by function resource keys.
 *
 * @param {Array<JPromise<SpreadsheetApp>>} appPromises
 *  One or more promises resolving with test application instances, e.g. for
 *  different file formats, as returned from `createSpreadsheetApp()`. The
 *  tester object will provide individual function resolvers for each
 *  application.
 *
 * @returns {FunctionModuleTester}
 *  A new tester for the specified function implementation module.
 */
export function createFunctionModuleTester(funcModule, ...appPromises) {
    return new FunctionModuleTester(funcModule, ...appPromises);
}

// class MockDocumentAccess ===================================================

/**
 * Simple mock implementation of the `IDocumentAccess` interface.
 *
 * @param {object} config
 *  The configuration of the document access object.
 *  - {number} config.cols -- number of columns in each sheet.
 *  - {number} config.rows -- number of rows in each sheet.
 *  - {string[]} config.sheets -- the exact names of all sheets.
 *  - {NameRef[]} [config.names] -- all defined names.
 *  - {TableRef[]} [config.tables] -- all table ranges.
 */
export class MockDocumentAccess {

    constructor(config) {
        this.addressFactory = new AddressFactory(config.cols, config.rows);
        this._sheets = config.sheets || [];
        this._names = config.names || [];
        this._tables = config.tables || [];
    }

    getSheetName(sheetIndex) {
        return this._sheets[sheetIndex] || null;
    }

    getSheetIndex(sheetName) {
        return this._sheets.findIndex(name => str.equalsICC(sheetName, name));
    }

    resolveName(sheet, label) {
        const key = mapKey(label);
        return this._names.find(ref => (sheet === ref.sheet) && (key === mapKey(ref.label)));
    }

    resolveTable(tableName) {
        const key = mapKey(tableName);
        return this._tables.find(ref => key === mapKey(ref.name));
    }

    resolveTableAt(sheet, address) {
        return this._tables.find(ref => (sheet === ref.sheeet) && ref.range.containsAddress(address));
    }
}
