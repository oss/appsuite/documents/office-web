/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, ary } from "@/io.ox/office/tk/algorithms";

import { MergeMode } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { DocumentSnapshot } from "@/io.ox/office/spreadsheet/model/modelsnapshot";
import { SpreadsheetOTManager } from "@/io.ox/office/spreadsheet/model/operation/otmanager";

import {
    createSpreadsheetApp, TestRunner, opSeries2,
    noOp, changeConfig, insertFont, insertTheme, insertStyle, deleteStyle, changeStyle,
    insertAutoStyle, deleteAutoStyle, changeAutoStyle,
    insertSheet, deleteSheet, moveSheet, copySheet, moveSheets, changeSheet,
    insertCols, deleteCols, changeCols, insertRows, deleteRows, changeRows,
    changeCells, moveCells, mergeCells, insertHlink, deleteHlink,
    insertName, deleteName, changeName, insertTable, deleteTable, changeTable, changeTableCol,
    insertDVRule, deleteDVRule, changeDVRule, insertCFRule, deleteCFRule, changeCFRule,
    insertNote, deleteNote, changeNote, moveNotes, insertComment, deleteComment, changeComment, moveComments,
    sheetSelection, insertShape, deleteDrawing, changeDrawing, moveDrawing,
    insertChSeries, deleteChSeries, changeChSeries, deleteChAxis, changeChAxis, changeChGrid, changeChTitle, changeChLegend,
    insertText, insertTab, insertBreak, insertPara, splitPara, mergePara, deleteOp, setAttrs, positionOp
} from "~/spreadsheet/apphelper";

// constants ==================================================================

export const MM_MERGE = MergeMode.MERGE;
export const MM_HORIZONTAL = MergeMode.HORIZONTAL;
export const MM_VERTICAL = MergeMode.VERTICAL;
export const MM_UNMERGE = MergeMode.UNMERGE;
export const MM_ALL = [MM_MERGE, MM_HORIZONTAL, MM_VERTICAL, MM_UNMERGE];
export const URL = "http://example.org";

// generic attribute sets
export const ATTRS = { f1: { a1: 10 } };
export const ATTRS2 = { f1: { a1: 20 } };

// independent attribute sets (will not be reduced)
export const ATTRS_I1 = { f1: { a1: 10 }, f2: { a1: 10 } };
export const ATTRS_I2 = { f2: { a2: 10 }, f3: { a1: 10 } };

// overlapping attribute sets (will be reduced)
export const ATTRS_O1 = { f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } };
export const ATTRS_O2 = { f3: { a1: 10 }, f2: { a1: 10, a2: 22, a4: 40 } };

// the reduced result of ATTRS_Ox
export const ATTRS_R1 = { f1: { a1: 10 }, f2: { a2: 20, a3: 30 } };
export const ATTRS_R2 = { f3: { a1: 10 }, f2: { a4: 40 } };

// attribute sets changing effective column width
export const COL_ATTRS = [{ column: { visible: false } }, { column: { width: 8 } }];

// attribute sets changing effective row height
export const ROW_ATTRS = [{ row: { visible: false } }, { row: { height: 10 } }, { row: { filtered: true } }];

// globally independent operations
export const GLOBAL_OPS = [noOp(), changeConfig({ f1: { p2: 42 } }), insertFont("f1"), insertTheme("t1")];

// generic style sheet operations
export const STYLESHEET_OPS = opSeries2([insertStyle, deleteStyle, changeStyle], "cell", "s1");

// generic auto-style operations
export const AUTOSTYLE_OPS = opSeries2([insertAutoStyle, deleteAutoStyle, changeAutoStyle], "a1");

// generic spreadsheet operations
export const SHEETCOLL_OPS = [insertSheet(5, "SheetA"), deleteSheet(5), moveSheet(5, 6), copySheet(5, 6, "SheetA"), moveSheets([0, 1, 2, 3, 4, 6, 5, 7, 8]), changeSheet(1, ATTRS)];

// the operations to be applied by the document model
export const OPERATIONS = [
    changeConfig({ document: { cols: 16384, rows: 1048576 } }),
    insertAutoStyle("a0", true),
    insertName("name", { formula: "1", ref: "A1" }),
    insertSheet(0, "Sheet1"),
    insertName("name", 0, { formula: "1", ref: "A1" }),
    insertSheet(1, "Sheet2"),
    changeCells(1, { A1: "Col1", B1: "Col2" }),
    insertName("name", 1, { formula: "1", ref: "A1" }),
    insertTable(1, "Table1", "A1:B4", { table: { headerRow: true, footerRow: true } }),
    insertTable(1, "TableX", "I1:J4", { table: { headerRow: true, footerRow: true } }),
    insertSheet(2, "Sheet3"),
    changeCells(2, { A1: "Col3", B1: "Col4", I1: "Col5", J1: "Col6" }),
    insertName("name", 2, { formula: "1", ref: "A1" }),
    insertTable(2, "Table2", "A1:B4", { table: { headerRow: true, footerRow: true } }),
    insertSheet(3, "Sheet4"),
    insertSheet(4, "Sheet5"),
    insertSheet(5, "Sheet6"),
    insertSheet(6, "Sheet7"),
    insertSheet(7, "Sheet8")
];

// functions ==================================================================

export function createTestRunner(fileFormat = "ooxml") {

    // initialize test documents
    const appPromise = createSpreadsheetApp(fileFormat, OPERATIONS);

    // initialize OT test runner
    let testRunner;
    beforeAll(async () => {
        const { docModel } = await appPromise;
        const otManager = new SpreadsheetOTManager(docModel);
        const userData = () => DocumentSnapshot.fromModel(docModel);
        testRunner = new TestRunner(otManager, { userData });
    });

    // return getter function for test runner
    return () => testRunner;
}

// generates column operations for multiple sheets and/or intervals
export function colOps(sheets, ...intervalArgs) {
    if (!intervalArgs.length) { intervalArgs = "B:C E:F"; }
    return [
        opSeries2(insertCols, sheets, intervalArgs, ATTRS),
        opSeries2(deleteCols, sheets, intervalArgs),
        opSeries2(changeCols, sheets, intervalArgs, ATTRS)
    ];
}

// generates row operations for multiple sheets and/or intervals
export function rowOps(sheets, ...intervalArgs) {
    if (!intervalArgs.length) { intervalArgs = "2:3 5:6"; }
    return [
        opSeries2(insertRows, sheets, intervalArgs, ATTRS),
        opSeries2(deleteRows, sheets, intervalArgs),
        opSeries2(changeRows, sheets, intervalArgs, ATTRS)
    ];
}

// generates cell operations for multiple sheets
export function cellOps(sheets) {
    return [
        opSeries2(changeCells, sheets, { A1: 42 }),
        opSeries2(moveCells, sheets, "A1:B2", "down"),
        opSeries2(mergeCells, sheets, "A1:B2")
    ];
}

// generates hyperlink operations for multiple sheets and/or ranges
export function hlinkOps(sheets, ...rangesArgs) {
    if (!rangesArgs.length) { rangesArgs = "A1:B2"; }
    return [
        opSeries2(insertHlink, sheets, rangesArgs, URL),
        opSeries2(deleteHlink, sheets, rangesArgs)
    ];
}

// generates defined name operations for multiple sheets
export function nameOps(sheets) {
    return [
        insertName("g"),
        opSeries2(insertName, "n", sheets),
        changeName("g"),
        opSeries2(changeName, "n", sheets),
        deleteName("g"),
        opSeries2(deleteName, "n", sheets)
    ];
}

// generates table range operations for multiple sheets
export function tableOps(sheets) {
    sheets = ary.wrap(sheets);
    return [
        sheets.map((sheet, index) => is.number(sheet) ? insertTable(sheet, "TX" + index, "A7:B9") : null).filter(Boolean),
        sheets.map((sheet, index) => is.number(sheet) ? changeTable(sheet, "TX" + index, "A7:B9") : null).filter(Boolean),
        sheets.map((sheet, index) => is.number(sheet) ? changeTableCol(sheet, "TX" + index, 1, { attrs: ATTRS }) : null).filter(Boolean),
        sheets.map((sheet, index) => is.number(sheet) ? deleteTable(sheet, "TX" + index) : null).filter(Boolean)
    ];
}

// generates data validation operations for multiple sheets and/or ranges
export function dvRuleOps(sheets, ...rangesArgs) {
    if (!rangesArgs.length) { rangesArgs = "A1:B2"; }
    return [
        opSeries2(insertDVRule, sheets, 0, rangesArgs, ATTRS),
        opSeries2(deleteDVRule, sheets, 0),
        opSeries2(changeDVRule, sheets, 0, rangesArgs, ATTRS)
    ];
}

// generates conditional formatting operations for multiple sheets
export function cfRuleOps(sheets, ...rangesArgs) {
    if (!rangesArgs.length) { rangesArgs = "A1:B2"; }
    return [
        opSeries2(insertCFRule, sheets, "R1", rangesArgs, ATTRS),
        opSeries2(deleteCFRule, sheets, "R1"),
        opSeries2(changeCFRule, sheets, "R1", rangesArgs, ATTRS)
    ];
}

// generates cell note operations for multiple sheets
export function noteOps(sheets) {
    return [
        opSeries2(insertNote, sheets, "A1", "abc"),
        opSeries2(deleteNote, sheets, "A1"),
        opSeries2(changeNote, sheets, "A1", "abc"),
        opSeries2(moveNotes,  sheets, "A1", "B2")
    ];
}

// generates cell comment operations for multiple sheets
export function commentOps(sheets) {
    return [
        opSeries2(insertComment, sheets, "C3", 0),
        opSeries2(deleteComment, sheets, "C3", 0),
        opSeries2(changeComment, sheets, "C3", 0),
        opSeries2(moveComments,  sheets, "C3", "D4")
    ];
}

// generates drawing operations for multiple sheets
export function drawingOps(sheets) {
    return [
        opSeries2(insertShape,   sheets, 0, ATTRS),
        opSeries2(deleteDrawing, sheets, 0),
        opSeries2(changeDrawing, sheets, 0, ATTRS),
        opSeries2(moveDrawing,   sheets, 0, 2),
        opSeries2(positionOp,    sheets, 0)
    ];
}

// generates chart operations for multiple sheets
export function chartOps(sheets) {
    return [
        opSeries2(insertChSeries, sheets, 0, 0, ATTRS),
        opSeries2(deleteChSeries, sheets, 0, 0),
        opSeries2(changeChSeries, sheets, 0, 0, ATTRS),
        opSeries2(deleteChAxis,   sheets, 0, 0),
        opSeries2(changeChAxis,   sheets, 0, 0, ATTRS),
        opSeries2(changeChGrid,   sheets, 0, 0, ATTRS),
        opSeries2(changeChTitle,  sheets, 0, 0, ATTRS),
        opSeries2(changeChTitle,  sheets, 0, ATTRS),
        opSeries2(changeChLegend, sheets, 0, ATTRS)
    ];
}

// generates drawing text operations for multiple sheets
export function drawingTextOps(sheets) {
    return [
        opSeries2(insertText,  sheets, "0 6 7", "abc"),
        opSeries2(insertTab,   sheets, "0 6 7"),
        opSeries2(insertBreak, sheets, "0 6 7"),
        opSeries2(deleteOp,    sheets, "0 6 7", "0 6 8"),
        opSeries2(setAttrs,    sheets, "0 6 7", "0 6 8", ATTRS),
        opSeries2(insertPara,  sheets, "0 6"),
        opSeries2(splitPara,   sheets, "0 6 7"),
        opSeries2(mergePara,   sheets, "0 6")
    ];
}

// generates selection operations for multiple sheets
export function selectOps(sheets) {
    return opSeries2(sheetSelection, sheets, "A1", 0, "A1", null, [[0, 1, 2]]);
}

// generates operations for multiple sheets (intended for sheet index transformations)
export function allSheetIndexOps(...args) {
    const sheetArgs = args.filter(is.number);
    const options = is.dict(ary.at(args, -1)) ? ary.at(args, -1) : {};
    const ops = [
        opSeries2(changeSheet, sheetArgs, ATTRS),
        colOps(sheetArgs),
        rowOps(sheetArgs),
        cellOps(sheetArgs),
        hlinkOps(sheetArgs),
        nameOps(sheetArgs),
        dvRuleOps(sheetArgs),
        cfRuleOps(sheetArgs),
        noteOps(sheetArgs),
        commentOps(sheetArgs),
        drawingOps(sheetArgs),
        chartOps(sheetArgs),
        drawingTextOps(sheetArgs),
        selectOps(sheetArgs)
    ];
    if (!options.skipTables) {
        ops.push(tableOps(args));
    }
    return ops;
}

// generates multiple "changeCells" operations with a value (focus on range addresses)
export function changeCellsOps(sheet, ...args) {
    const contentsArr = args.map(ranges => {
        const contents = {};
        for (const range of ranges.split(" ")) { contents[range] = 42; }
        return contents;
    });
    return opSeries2(changeCells, sheet, contentsArr);
}

// generates different note/comment operations (with cell "anchor" property)
export function cellAnchorOps(sheet, ...anchorArgs) {
    return [
        opSeries2([insertNote, deleteNote, changeNote], sheet, anchorArgs, "abc"),
        opSeries2([insertComment, deleteComment, changeComment], sheet, anchorArgs, 0)
    ];
}

// generates different drawing operations with "drawing.anchor" attribute
export function drawingAnchorOps(sheet, ...anchorArgs) {
    const attrsArgs = anchorArgs.map(anchor => ({ drawing: { anchor } }));
    const propsArgs = attrsArgs.map(attrs => ({ attrs }));
    return [
        opSeries2([insertNote, changeNote], sheet, "A1", "abc", attrsArgs),
        opSeries2([insertComment, changeComment], sheet, "A1", 0, propsArgs),
        opSeries2([insertShape, changeDrawing], sheet, 0, attrsArgs)
    ].flat();
}

// generates different drawing operations without "drawing.anchor" attribute
export function drawingNoAnchorOps(sheet) {
    return [
        opSeries2([insertNote, changeNote], sheet, "A1", "abc", ATTRS),
        opSeries2([insertShape, changeDrawing], sheet, 0, ATTRS)
    ];
}

// generates operations that change drawing objects (without movement)
export function changeDrawingOps(sheet, ...posArgs) {
    return [
        opSeries2(changeDrawing, sheet, posArgs, ATTRS),
        opSeries2(insertChSeries, sheet, posArgs, 0, ATTRS),
        opSeries2(deleteChSeries, sheet, posArgs, 0),
        opSeries2(changeChSeries, sheet, posArgs, 0, ATTRS),
        opSeries2(deleteChAxis, sheet, posArgs, 0),
        opSeries2(changeChAxis, sheet, posArgs, 0, ATTRS),
        opSeries2(changeChGrid, sheet, posArgs, 0, ATTRS),
        opSeries2(changeChTitle, sheet, posArgs, 0, ATTRS),
        opSeries2(changeChTitle, sheet, posArgs, ATTRS),
        opSeries2(changeChLegend, sheet, posArgs, ATTRS),
        opSeries2(insertText, sheet, posArgs, "abc"),
        opSeries2(insertTab, sheet, posArgs),
        opSeries2(insertBreak, sheet, posArgs),
        posArgs.map(pos => deleteOp(sheet, pos, pos)),
        posArgs.map(pos => setAttrs(sheet, pos, pos, ATTRS)),
        opSeries2(insertPara, sheet, posArgs),
        opSeries2(splitPara, sheet, posArgs),
        opSeries2(mergePara, sheet, posArgs),
        opSeries2(positionOp, sheet, posArgs)
    ];
}
