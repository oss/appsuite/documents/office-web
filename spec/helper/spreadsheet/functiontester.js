/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, ary } from "@/io.ox/office/tk/algorithms";

import { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";
import { Matrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import { FunctionSpecFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";
import { Operand } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import { FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";

import { a, isErrorCode } from "~/spreadsheet/sheethelper";

// private functions ==========================================================

/**
 * Returns a function that resolves the implementation of an operator or a
 * function used in sheet formulas.
 *
 * @param {SpreadsheetModel} docModel
 *  The document model used to resolve cell references passed to the operator
 *  or function implementation.
 *
 * @param {object} descriptor
 *  The raw descriptor of the operator or function, as provided by the
 *  implementation modules in "@/io.ox/office&/spreadsheet/model/formula/impl".
 *
 * @param {object} [options]
 *  Optional parameters. See method `FunctionModuleTester.testFunction()` for
 *  details.
 *
 * @returns {Function}
 *  A resolver function wrapping the passed document model, and operator
 *  descriptor. This function can be invoked in the same way as the wrapped
 *  operator or function (according to its parameter signature), and returns
 *  the unconverted result value. If the implementation throws a formula error
 *  code (an instance of the `ErrorCode` enum), this error code will be caught
 *  and returned. Other exceptions will be thrown.
 */
function createFunctionResolver(docModel, descriptor, options) {

    // create a formula context with the passed reference position
    const refSheet = (options && options.refSheet) || 0;
    const targetAddress = a((options && options.targetAddress) || "A1");
    const context = new FormulaContext(docModel, targetAddress, { refSheet });
    const fileFormat = docModel.docApp.getFileFormat();
    const toOperand = options && options.toOperand;

    // invokes the resolver function for the passed operator descriptor
    function invoke(...values) {
        try {

            // create and validate the function parameter values
            values.forEach((value, index) => {
                if (is.array(value) && !(value instanceof Range3DArray)) {
                    throw new Error("do not use plain arrays!");
                }
                if (value instanceof Matrix) {
                    values[index] = value.clone();
                }
            });

            // convert some values to Operand objects if specified
            if (toOperand === true) {
                values = values.map(value => new Operand(context, value));
            } else if (is.number(toOperand)) {
                if (toOperand < values.length) {
                    values[toOperand] = new Operand(context, values[toOperand]);
                }
            } else if (is.array(toOperand)) {
                toOperand.forEach(index => {
                    if (index < values.length) {
                        values[index] = new Operand(context, values[index]);
                    }
                });
            }

            // create an array of Operand objects for the operand resolver mock
            const operands = values.map(value => (value instanceof Operand) ? value : new Operand(context, value));

            // create a mocked operand resolver
            context.pushOperandResolver({
                size() { return operands.length; },
                get(i) { return operands[i]; },
                resolve(i) { return operands[i]; }
            }, "val");

            // invoke the function implementation, return the result
            let resolveFn = descriptor.resolve;
            if (is.dict(resolveFn)) {
                resolveFn = resolveFn[fileFormat];
            }
            return resolveFn.apply(context, values);

        } catch (error) {
            if (isErrorCode(error)) { return error; }
            throw error;
        }
    }

    // default behavior: convert Operand object to the result value
    function resolver(...args) {
        const result = invoke(...args);
        return (result instanceof Operand) ? result.getRawValue() : result;
    }

    // special behavior: return the value unmodified
    resolver.invoke = invoke;

    return resolver;
}

// class FunctionModuleTester =================================================

/**
 * A tester for a function implementation module.
 *
 * @param {Dict} funcModule
 *  The function module. Is expected to be a simple object with function
 *  descriptors, mapped by function resource keys.
 *
 * @param {Array<JPromise<BaseApplication>>} appPromises
 *  One or more promises resolving with test application instances, e.g. for
 *  different file formats. The tester will provide individual function
 *  resolvers for each application.
 */
export class FunctionModuleTester {

    constructor(funcModule, ...appPromises) {

        // test the existence of the passed module
        it("should exist", () => {
            expect(funcModule).toBeObject();
            expect(funcModule).toRespondTo("register");
        });

        this.funcSpecMap = new Map(new FunctionSpecFactory(funcModule));
        this.appPromises = appPromises;
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a test context for a single spreadsheet function implemented in
     * the implementation module passed to the constructor.
     *
     * @param {string} funcKey
     *  The resource key of the function to be tested.
     *
     * @param {object} [options]
     *  Optional parameters for the function resolvers:
     *  - {number} [options.refSheet=0]
     *    The zero-based index of the reference sheet used by the formula
     *    context (the position of the "simulated" formula cell evaluating a
     *    function). Defaults to the first sheet in the test document.
     *  - {string} [options.targetAddress="A1"]
     *    The name of the target reference address used by the formula context,
     *    in A1 notation (the position of the "simulated" formula cell
     *    evaluating a function).
     *  - {number|number[]|boolean} [options.toOperand=false]
     *    If set to a number, or an array of numbers, the specified function
     *    arguments (zero-based parameter indexes) that will be passed to the
     *    resolver functions will be converted to instances of the class
     *    `Operand` automatically. If set to `true`, all arguments will be
     *    converted to instances of `Operand`.
     *
     *  The options parameter can be omitted completely (the following callback
     *  can be passed as second parameter).
     *
     * @param {Function} callback
     *  The callback function that implemented the tests for the specified
     *  spreadsheet function. Receives one or more function resolvers as
     *  parameters, according to the number of application promises passed to
     *  the constructor. Each resolver is a JavaScript function wrapping the
     *  implementation of the specified spreadsheet function, and can be
     *  invoked in the same way as the wrapped function in a real spreadsheet
     *  formula, according to its parameter signature. It will return the
     *  unconverted result value. If the implementation throws a formula error
     *  code (an instance of `ErrorLiteral`), this error code will be caught
     *  and returned as value. Other exceptions will be thrown. Additionally,
     *  each resolver contains a method `invoke()` that returns the original
     *  value of the implementation (no conversion of instances of class
     *  `Operand` to the wrapped values).
     */
    testFunction(funcKey, options, callback) {

        // options can be omitted completely
        if (!callback) { callback = options; options = null; }

        // destructure own properties
        const { appPromises } = this;

        // create a text particle for the function, and a context for the `beforeAll()` block
        describe(`function ${funcKey}`, () => {

            // the function descriptor (add a few standard tests)
            const descriptor = this.funcSpecMap.get(funcKey);
            it("should exist", () => {
                expect(descriptor).toBeObject();
            });
            it("should be implemented", () => {
                expect(descriptor).toHaveProperty("resolve");
                if (!is.function(descriptor.resolve)) {
                    expect(descriptor.resolve).toBeObject();
                    expect(descriptor.resolve).toRespondTo("ooxml");
                    expect(descriptor.resolve).toRespondTo("odf");
                }
            });

            // create the function resolvers in a before block (this defers their creation
            // until the enclosing test block runs)
            let resolvers = null;
            beforeAll(async () => {
                const docApps = await Promise.all(appPromises);
                resolvers = docApps.map(docApp => createFunctionResolver(docApp.docModel, descriptor, options));
            });

            // immediately register the tests implemented in the callback function, but pass
            // placeholder callbacks that call the real function resolvers on their invocation
            // (resolvers will be initialized deferred in the `beforeAll()` block above)
            const resolverFns = ary.generate(appPromises.length, index => {
                function getResolver() { return resolvers[index]; }
                /** @this {unknown} */ function invokeResolver(...args) { return getResolver().apply(this, args); }
                invokeResolver.invoke = function (...args) { return getResolver().invoke.apply(this, args); };
                return invokeResolver;
            });

            callback(...resolverFns, descriptor);
        });
    }

    /**
     * Creates a simple test that the specified spreadsheet function has been
     * implemented by making it an alias of another function.
     *
     * @param {string} aliasFuncKey
     *  The resource key of the function to be tested.
     *
     * @param {string} implFuncKey
     *  The resource key of the function the descriptor of the specified alias
     *  function should refer to.
     */
    testAliasFunction(aliasFuncKey, implFuncKey) {

        // create a text particle for the function
        describe(`function ${aliasFuncKey}`, () => {

            const descriptor = this.funcSpecMap.get(aliasFuncKey);
            it("should exist", () => {
                expect(descriptor).toBeObject();
            });
            it("should be implemented", () => {
                expect(descriptor).toHaveProperty("resolve", implFuncKey);
            });
        });
    }
}
