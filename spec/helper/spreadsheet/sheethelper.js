/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { is, str } from "@/io.ox/office/tk/algorithms";

import * as Op from "@/io.ox/office/spreadsheet/utils/operations";
import { ErrorCode, isErrorCode, Interval, Address, Range, Range3D, IntervalArray, AddressArray, RangeArray, Range3DArray, CellAnchor, getBorderName } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { Matrix, NumberMatrix } from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import { CellRef } from "@/io.ox/office/spreadsheet/model/formula/utils/cellref";
import { DocRef, SheetRef, SheetRefs } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";

import { waitForEvent } from "~/asynchelper";
import * as OTHelper from "~/othelper";
import { parsePos, makeOp } from "~/othelper";

// re-exports =================================================================

export * from "~/othelper";

export { ErrorCode, isErrorCode };

// private functions ==========================================================

function sheetOp(name, sheet, ...props) {
    return makeOp(name, { sheet }, ...props);
}

function intervalListOp(name, sheet, intervals, style, attrs) {
    const props = { intervals };
    if (is.string(style)) { props.s = style; }
    if (is.dict(style)) { props.attrs = style; }
    if (is.dict(attrs)) { props.attrs = attrs; }
    return sheetOp(name, sheet, props);
}

function rangeListOp(name, sheet, ranges, ...props) {
    return sheetOp(name, sheet, { ranges }, ...props);
}

function nameOp(name, label, sheet, ...props) {
    const opProps = { label };
    if (is.number(sheet)) { opProps.sheet = sheet; }
    if (is.dict(sheet)) { Object.assign(opProps, sheet); }
    return makeOp(name, opProps, ...props);
}

function tableOp(name, sheet, table, ...props) {
    return sheetOp(name, sheet, table ? { table } : null, ...props);
}

function idOp(name, sheet, id, ...props) {
    return sheetOp(name, sheet, { id: String(id) }, ...props);
}

function anchorOp(name, sheet, anchor, ...props) {
    return sheetOp(name, sheet, { anchor }, ...props);
}

function commentOp(name, sheet, anchor, index, ...props) {
    return anchorOp(name, sheet, anchor, { index }, ...props);
}

function moveOp(name, sheet, from, to) {
    return sheetOp(name, sheet, { from, to });
}

function extendPos(sheet, pos) {
    return [sheet, ...parsePos(pos)];
}

// public functions ===========================================================

/**
 * Parses a column or row interval string in simple A1 notation (e.g. "B:C" or
 * "2:3") to an instance of class `Interval`.
 */
export function i(text) {
    return /^\d/.test(text) ? Interval.parseAsRows(text) : Interval.parseAsCols(text);
}

/**
 * Parses a cell address string in simple A1 notation (e.g. "B3") to an
 * instance of class `Address`.
 */
export const a = Address.parse;

/**
 * Parses a cell range address string in simple A1 notation (e.g. "B3:C4" or
 * "D5") to an instance of class `Range`.
 */
export const r = Range.parse;

/**
 * Parses a 3D cell range address string in simple A1 notation with integer
 * sheet indexes (e.g. "1:2!B3:C4" or "3!D5") to an instance of class
 * `Range3D`.
 */
export function r3d(text) {
    const matches = /^(\d+)(?::(\d+))?!(.*)$/.exec(text);
    const sheet1 = parseInt(matches[1], 10);
    const sheet2 = matches[2] ? parseInt(matches[2], 10) : sheet1;
    return Range3D.fromRange3D(r(matches[3]), sheet1, sheet2);
}

/**
 * Parses a space-separated list of column intervals or row intervals in simple
 * A1 notation (e.g. "B:C D:E 7:8") to an instance of class `IntervalArray`.
 */
export function ia(text) {
    return IntervalArray.map(str.splitTokens(text), i);
}

/**
 * Parses a space-separated list of cell addresses in simple A1 notation (e.g.
 * "B3 C4") to an instance of class `AddressArray`.
 */
export function aa(text) {
    return AddressArray.map(str.splitTokens(text), a);
}

/**
 * Parses a space-separated list of cell range addresses in simple A1 notation
 * (e.g. "B3:C4 D5") to an instance of class `RangeArray`.
 */
export function ra(text) {
    return RangeArray.map(str.splitTokens(text), r);
}

/**
 * Parses a space-separated list of 3D cell range addresses in simple A1
 * notation with integer sheet indexes (e.g. "1:2!B3:C4 3!D5") to an instance
 * of class `Range3DArray`.
 */
export function r3da(text) {
    return Range3DArray.map(str.splitTokens(text), r3d);
}

/**
 * Parses a cell anchor string to an instance of class CellAnchor.
 */
export const ca = CellAnchor.parseOpStr;

/**
 * Converts a date/time string to an instance of the `Date` class.
 *
 * @param {string} datetime
 *  The date-time string. May contain a date part, a time part, or both
 *  separated with a space character. The date part must be given in the format
 *  "YYYY-MM-DD". The time part must be given in one of the formats "hh:mm",
 *  "hh:mm:ss", or "hh:mm:ss.ttt".
 */
export function dt(datetime) {

    let dateMatches = datetime.match(/^(\d{4})-(\d{2})-(\d{2})/);
    if (dateMatches) {
        datetime = datetime.slice(dateMatches[0].length + 1);
    } else {
        dateMatches = ["", "1899", "12", "30"];
    }

    let timeMatches = datetime.match(/^(\d{2}):(\d{2})(?::(\d{2})(?:\.(\d+))?)?$/);
    if (!timeMatches) { timeMatches = ["", "00", "00"]; }

    return new Date(Date.UTC(
        parseInt(dateMatches[1], 10),
        parseInt(dateMatches[2], 10) - 1,
        parseInt(dateMatches[3], 10),
        parseInt(timeMatches[1], 10),
        parseInt(timeMatches[2], 10),
        timeMatches[3] ? parseInt(timeMatches[3], 10) : 0,
        timeMatches[4] ? parseInt((timeMatches[4] + "000").slice(0, 3), 10) : 0
    ));
}

/**
 * Converts a date/time string to a serial number in the 1900 date system.
 *
 * @param {string} datetime
 *  The date-time string. May contain a date part, a time part, or both
 *  separated with a space character. The date part must be given in the format
 *  "YYYY-MM-DD". The time part must be given in one of the formats "hh:mm",
 *  "hh:mm:ss", or "hh:mm:ss.ttt".
 */
export function dts(datetime) {
    return (dt(datetime).getTime() - Date.UTC(1899, 11, 30)) / 86400000;
}

/**
 * Creates a new instance of the class `Matrix` from the passed two-dimensional
 * array of scalar values.
 */
export function mat(arr) {
    return new Matrix(arr);
}

/**
 * Creates a new instance of the class `NumberMatrix` from the passed
 * two-dimensional array of numbers.
 */
export function nmat(arr) {
    return new NumberMatrix(arr);
}

/**
 * Creates a new instance of the class `Matrix` containing `Date` objects
 * converted from the passed two-dimensional array of strings. The dates will
 * be created with the function `dt()`.
 */
export function dtmat(arr) {
    return new Matrix(arr).transform(dt);
}

/**
 * Creates a `CellRef` instance from "$A$1" notation.
 *
 * @param {string} text
 *  The text representation of a cell reference, in "A1" notation, with
 *  optional dollar signs as absolute markers.
 */
export function cref(text) {
    const m = text.match(/^(\$)?([A-Z]+)(\$)?(\d+)$/i);
    const addr = a(m[2] + m[4]);
    return new CellRef(addr.c, addr.r, !!m[1], !!m[3]);
}

/**
 * Creates a `CellRefs` instance from $A$1:$A$1 notation.
 *
 * @param {string} text
 *  The text representation of up to two cell references, in "A1" notation,
 *  with optional dollar signs as absolute markers, as single cell, or as cell
 *  range with colon. The empty string will be converted to an empty object
 *  (representing a reference error).
 */
export function crefs(text) {
    const tokens = text.split(":");
    const refs = {};
    if (tokens[0]) { refs.r1 = cref(tokens[0]); }
    if (tokens[1]) { refs.r2 = cref(tokens[1]); }
    return refs;
}

/**
 * Creates a `SheetRef` instance from "$Sheet" notation.
 *
 * @param {string} text
 *  The text representation of a sheet reference, with optional dollar sign as
 *  absolute marker. If the string can be converted to an integer, it will be
 *  interpreted as sheet index, otherwise it will be used as literal unresolved
 *  sheet name.
 */
export function sref(text) {
    const m = text.match(/^(\$)?(.+)$/);
    const sheet = m[2].match(/^-?\d+$/) ? parseInt(m[2], 10) : m[2];
    return new SheetRef(sheet, !!m[1]);
}

/**
 * Creates a `SheetRefs` instance from "$Sheet1:$Sheet2" notation.
 *
 * @param {string | null} text
 *  The text representation of up to two sheet references, with optional dollar
 *  signs as absolute markers. If either of the strings can be converted to an
 *  integer, they will be interpreted as sheet index, otherwise they will be
 *  used as literal unresolved sheet name.
 *
 * @param {number} [index]
 *  The index of an external document.
 */
export function srefs(text, index) {
    const tokens = text?.split(":") ?? [];
    const refs = new SheetRefs();
    if (tokens[0]) { refs.r1 = sref(tokens[0]); }
    if (tokens[1]) { refs.r2 = sref(tokens[1]); }
    if (is.number(index)) { refs.doc = new DocRef(index); }
    return refs;
}

/**
 * Builds border attributes by character shortcuts.
 *
 * The function expects one or more pairs of parameters. The first value in the
 * pair is a string containing one or more one-letter border keys, and the
 * second value is a JSON border value to be assigned to the respective border
 * attributes.
 *
 * Example: `borders("tb", border1, "lr", border2)` results in an object with
 * the properties `borderTop` and `borderBottom` assigned to the value of
 * `border1`, and with properties `borderLeft` and `borderRight` assigned to
 * the value of `border2`.
 */
export function borders(...args) {
    const attrs = {};
    for (let idx = 0; idx < args.length; idx += 2) {
        for (const key of args[idx]) {
            attrs[getBorderName(key)] = args[idx + 1];
        }
    }
    return attrs;
}

/**
 * Creates a matcher predicate for the `toSatisfy` assertion that matches the
 * passed matrix values.
 *
 * @param {number[][]} expected
 *  The expected matrix values.
 *
 * @returns {Function}
 *  A matcher predicate that takes an instance of `Matrix`, and returns whether
 *  it matches the specified matrix values.
 */
export function matrixMatcher(expected) {
    return result => (result instanceof Matrix) && _.isEqual(expected, result.raw());
}

/**
 * Creates a matcher predicate for the `toSatisfy` assertion that matches the
 * passed index intervals but ignores any additional properties of the result
 * intervals.
 *
 * @param {string} expected
 *  The expected index intervals, as string (either as column intervals, or as
 *  row intervals). See `ia()` for details.
 *
 * @returns {Function}
 *  A matcher predicate that takes an instance of `IntervalArray`, and returns
 *  whether it matches the specified index intervals.
 */
export function orderedIntervalsMatcher(expected) {
    const expectedKey = ia(expected).toOpStrRows();
    return result => {
        const resultKey = is.array(result) ? IntervalArray.from(result).toOpStrRows() : null;
        if (expectedKey === resultKey) { return true; }
        console.error("orderedIntervalsMatcher: wrong result intervals");
        console.error("  exp=" + expected);
        console.error("  got=" + result);
    };
}

/**
 * Creates a matcher predicate for the `toSatisfy` assertion that matches the
 * passed index intervals regardless of their order in the result array.
 *
 * @param {string} expected
 *  The expected index intervals, as string (either as column intervals, or as
 *  row intervals). See `ia()` for details.
 *
 * @returns {Function}
 *  A matcher predicate that takes an instance of `IntervalArray`, and returns
 *  whether it matches the specified index intervals.
 */
export function unorderedIntervalsMatcher(expected) {
    const getKey = intervals => Array.from(intervals, int => int.toOpStrRows()).sort().join(" ");
    const expectedKey = getKey(ia(expected));
    return result => {
        const resultKey = is.array(result) ? getKey(result) : null;
        if (expectedKey === resultKey) { return true; }
        console.error("unorderedIntervalsMatcher: wrong result intervals");
        console.error("  exp=" + expected);
        console.error("  got=" + result);
    };
}

/**
 * Creates a matcher predicate for the `toSatisfy` assertion that matches the
 * passed cell addresses regardless of their order in the result array.
 *
 * @param {string} expected
 *  The expected cell addresses, as string. See `aa()` for details.
 *
 * @returns {Function}
 *  A matcher predicate that takes an instance of `AddressArray`, and returns
 *  whether it matches the specified cell addresses.
 */
export function unorderedAddressesMatcher(expected) {
    const getKey = addresses => Array.from(addresses, addr => addr.toOpStr()).sort().join(" ");
    const expectedKey = getKey(aa(expected));
    return result => {
        const resultKey = is.array(result) ? getKey(result) : null;
        if (expectedKey === resultKey) { return true; }
        console.error("unorderedAddressesMatcher: wrong result addresses");
        console.error("  exp=" + expected);
        console.error("  got=" + result);
    };
}

/**
 * Creates a matcher predicate for the `toSatisfy` assertion that matches the
 * passed cell range addresses regardless of their order in the result array.
 *
 * @param {string} expected
 *  The expected cell range addresses, as string. See `ra()` for details.
 *
 * @returns {Function}
 *  A matcher predicate that takes an instance of `RangeArray`, and returns
 *  whether it matches the specified cell range addresses.
 */
export function unorderedRangesMatcher(expected) {
    const getKey = ranges => Array.from(ranges, rng => rng.toOpStr()).sort().join(" ");
    const expectedKey = getKey(ra(expected));
    return result => {
        const resultKey = is.array(result) ? getKey(result) : null;
        if (expectedKey === resultKey) { return true; }
        console.error("unorderedRangesMatcher: wrong result ranges");
        console.error("  exp=" + expected);
        console.error("  got=" + result);
    };
}

/**
 * Creates a matcher predicate for the `toSatisfy` assertion that matches the
 * passed cell range addresses by merging them and the result ranges.
 *
 * @param {string} expected
 *  The expected cell range addresses, as string. See `r()` for details.
 *
 * @returns {Function}
 *  A matcher predicate that takes an instance of `RangeArray`, and returns
 *  whether the ranges cover the exact same cells as the passed ranges,
 *  regardless if and how the result range overlap each other.
 */
export function mergedRangesMatcher(expected) {
    expected = ra(expected).merge();
    return result => {
        const mergedResult = is.array(result) ? RangeArray.mergeRanges(result) : null;
        if (mergedResult && expected.deepEquals(mergedResult)) { return true; }
        console.error("mergedRangesMatcher: wrong result ranges");
        console.error("  exp=" + expected);
        console.error("  got=" + result);
    };
}

/**
 * Calls a BDD test for the two passed matrices.
 *
 * @param {unknown} matrix
 *  The value to be checked.
 *
 * @param {Matrix} expected
 *  The expected matrix literal.
 *
 * @param {number} digits
 *  The number of digits for tolerance.
 */
export function assertMatrixCloseTo(matrix, expected, digits) {

    expect(matrix).toBeInstanceOf(Matrix);
    expect(matrix.rows()).toBe(expected.rows());
    expect(matrix.cols()).toBe(expected.cols());

    // no usage of forEach, because it break the error stack!
    for (let row = 0; row < matrix.rows(); row += 1) {
        for (let col = 0; col < matrix.cols(); col += 1) {
            const currValue = matrix.get(row, col);
            const expectedValue = expected.get(row, col);
            if (is.number(expectedValue)) {
                expect(currValue).toBeCloseTo(expectedValue, digits);
            } else {
                expect(currValue).toBe(expectedValue);
            }
        }
    }
}

/**
 * Waits for a "recalc:end" of the dependency manager for at most 1 second.
 *
 * @param {SpreadsheetModel} docModel
 *  The spreadsheet document model.
 *
 * @returns {JPromise<DependencyCycle[]>}
 *  A promise that will fulfil when the update cycle of the dependency manager
 *  has finished, or that will reject for a timeout.
 */
export async function waitForRecalcEnd(docModel) {
    const [cycles] = await waitForEvent(docModel.dependencyManager, "recalc:end");
    return cycles;
}

// operation generators -------------------------------------------------------

/**
 * Creates an "insertNumberFormat" operation.
 */
export function insertNumFmt(id, code) {
    return makeOp(Op.INSERT_NUMBER_FORMAT, { id, code });
}

/**
 * Creates a "deleteNumberFormat" operation.
 */
export function deleteNumFmt(id) {
    return makeOp(Op.DELETE_NUMBER_FORMAT, { id });
}

/**
 * Creates an "insertSheet" operation.
 */
export function insertSheet(sheet, name, attrs) {
    return sheetOp(Op.INSERT_SHEET, sheet, { sheetName: name || "Sheet" }, attrs ? { attrs } : null);
}

/**
 * Creates a "deleteSheet" operation.
 */
export function deleteSheet(sheet) {
    return sheetOp(Op.DELETE_SHEET, sheet);
}

/**
 * Creates a "moveSheet" operation.
 */
export function moveSheet(from, to) {
    return sheetOp(Op.MOVE_SHEET, from, { to });
}

/**
 * Creates a "copySheet" operation.
 */
export function copySheet(from, to, name, tableNames) {
    return sheetOp(Op.COPY_SHEET, from, { to, sheetName: name || "Sheet" }, tableNames ? { tableNames } : null);
}

/**
 * Creates a "moveSheets" operation.
 */
export function moveSheets(sheets) {
    return makeOp(Op.MOVE_SHEETS, { sheets: sheets.slice() });
}

/**
 * Creates a "changeSheet" operation.
 */
export function changeSheet(sheet, name, attrs) {
    const props = {};
    if (is.string(name)) { props.sheetName = name; }
    if (is.dict(name)) { props.attrs = name; }
    if (is.dict(attrs)) { props.attrs = attrs; }
    return sheetOp(Op.CHANGE_SHEET, sheet, props);
}

/**
 * Creates an "insertColumns" operation.
 */
export function insertCols(sheet, intervals, style, attrs) {
    return intervalListOp(Op.INSERT_COLUMNS, sheet, intervals, style, attrs);
}

/**
 * Creates a "deleteColumns" operation.
 */
export function deleteCols(sheet, intervals) {
    return intervalListOp(Op.DELETE_COLUMNS, sheet, intervals);
}

/**
 * Creates a "changeColumns" operation.
 */
export function changeCols(sheet, intervals, style, attrs) {
    return intervalListOp(Op.CHANGE_COLUMNS, sheet, intervals, style, attrs);
}

/**
 * Creates an "insertRows" operation.
 */
export function insertRows(sheet, intervals, style, attrs) {
    return intervalListOp(Op.INSERT_ROWS, sheet, intervals, style, attrs);
}

/**
 * Creates a "deleteRows" operation.
 */
export function deleteRows(sheet, intervals) {
    return intervalListOp(Op.DELETE_ROWS, sheet, intervals);
}

/**
 * Creates a "changeRows" operation.
 */
export function changeRows(sheet, intervals, style, attrs) {
    return intervalListOp(Op.CHANGE_ROWS, sheet, intervals, style, attrs);
}

/**
 * Creates a "changeCells" operation.
 */
export function changeCells(sheet, start, contents) {
    const props = { contents: is.dict(start) ? start : contents };
    if (is.string(start)) { props.start = start; }
    return sheetOp(Op.CHANGE_CELLS, sheet, props);
}

/**
 * Creates a "moveCells" operation.
 */
export function moveCells(sheet, range, dir) {
    return sheetOp(Op.MOVE_CELLS, sheet, { range, dir });
}

/**
 * Creates a "mergeCells" operation.
 */
export function mergeCells(sheet, ranges, type) {
    return rangeListOp(Op.MERGE_CELLS, sheet, ranges, type ? { type } : null);
}

/**
 * Creates an "insertHyperlink" operation.
 */
export function insertHlink(sheet, ranges, url) {
    return rangeListOp(Op.INSERT_HYPERLINK, sheet, ranges, { url: url || "example.org" });
}

/**
 * Creates a "deleteHyperlink" operation.
 */
export function deleteHlink(sheet, ranges) {
    return rangeListOp(Op.DELETE_HYPERLINK, sheet, ranges);
}

/**
 * Creates an "insertName" operation.
 */
export function insertName(label, sheet, props) {
    return nameOp(Op.INSERT_NAME, label, sheet, props);
}

/**
 * Creates a "deleteName" operation.
 */
export function deleteName(label, sheet) {
    return nameOp(Op.DELETE_NAME, label, sheet);
}

/**
 * Creates a "changeName" operation.
 */
export function changeName(label, sheet, props) {
    return nameOp(Op.CHANGE_NAME, label, sheet, props);
}

/**
 * Creates an "insertTable" operation.
 */
export function insertTable(sheet, table, range, attrs) {
    return tableOp(Op.INSERT_TABLE, sheet, table, { range }, attrs ? { attrs } : null);
}

/**
 * Creates a "deleteTable" operation.
 */
export function deleteTable(sheet, table) {
    return tableOp(Op.DELETE_TABLE, sheet, table);
}

/**
 * Creates a "changeTable" operation.
 */
export function changeTable(sheet, table, range, props) {
    props = is.dict(range) ? { ...range } : { ...props };
    if (is.string(range)) { props.range = range; }
    return tableOp(Op.CHANGE_TABLE, sheet, table, props);
}

/**
 * Creates a "changeTableColumn" operation.
 */
export function changeTableCol(sheet, table, col, props) {
    return tableOp(Op.CHANGE_TABLE_COLUMN, sheet, table, { col }, props);
}

/**
 * Creates an "insertDVRule" operation.
 */
export function insertDVRule(sheet, index, ranges, props) {
    if (!is.number(index)) { props = ranges; ranges = index; index = undefined; }
    props = { ranges, ...props };
    if (is.number(index)) { props.index = index; }
    return sheetOp(Op.INSERT_DVRULE, sheet, props);
}

/**
 * Creates a "deleteDVRule" operation.
 */
export function deleteDVRule(sheet, index) {
    const props = {};
    props[is.number(index) ? "index" : "ranges"] = index;
    return sheetOp(Op.DELETE_DVRULE, sheet, props);
}

/**
 * Creates a "changeDVRule" operation.
 */
export function changeDVRule(sheet, index, ranges, props) {
    if (!is.number(index)) { props = ranges; ranges = index; index = undefined; }
    if (!is.string(ranges)) { props = ranges; ranges = undefined; }
    props = { ...props };
    if (is.number(index)) { props.index = index; }
    if (is.string(ranges)) { props.ranges = ranges; }
    return sheetOp(Op.CHANGE_DVRULE, sheet, props);
}

/**
 * Creates an "insertCFRule" operation.
 */
export function insertCFRule(sheet, id, ranges, props) {
    return idOp(Op.INSERT_CFRULE, sheet, id, { ranges }, props);
}

/**
 * Creates a "deleteCFRule" operation.
 */
export function deleteCFRule(sheet, id) {
    return idOp(Op.DELETE_CFRULE, sheet, id);
}

/**
 * Creates a "changeCFRule" operation.
 */
export function changeCFRule(sheet, id, ranges, props) {
    props = is.dict(ranges) ? { ...ranges } : { ...props };
    if (is.string(ranges)) { props.ranges = ranges; }
    return idOp(Op.CHANGE_CFRULE, sheet, id, props);
}

/**
 * Creates an "insertNote" operation.
 */
export function insertNote(sheet, anchor, text, attrs) {
    return anchorOp(Op.INSERT_NOTE, sheet, anchor, { text }, attrs ? { attrs } : null);
}

/**
 * Creates a "deleteNote" operation.
 */
export function deleteNote(sheet, anchor) {
    return anchorOp(Op.DELETE_NOTE, sheet, anchor);
}

/**
 * Creates a "changeNote" operation.
 */
export function changeNote(sheet, anchor, text, attrs) {
    const props = {};
    if (is.string(text)) { props.text = text; }
    if (is.dict(text)) { props.attrs = text; }
    if (is.dict(attrs)) { props.attrs = attrs; }
    return anchorOp(Op.CHANGE_NOTE, sheet, anchor, props);
}

/**
 * Creates a "moveNotes" operation.
 */
export function moveNotes(sheet, from, to) {
    return moveOp(Op.MOVE_NOTES, sheet, from, to);
}

/**
 * Creates an "insertComment" operation.
 */
export function insertComment(sheet, anchor, index, props) {
    return commentOp(Op.INSERT_COMMENT, sheet, anchor, index, props);
}

/**
 * Creates a "deleteComment" operation.
 */
export function deleteComment(sheet, anchor, index) {
    return commentOp(Op.DELETE_COMMENT, sheet, anchor, index);
}

/**
 * Creates an "changeComment" operation.
 */
export function changeComment(sheet, anchor, index, props) {
    return commentOp(Op.CHANGE_COMMENT, sheet, anchor, index, props);
}

/**
 * Creates a "moveComments" operation.
 */
export function moveComments(sheet, from, to) {
    return moveOp(Op.MOVE_COMMENTS, sheet, from, to);
}

/**
 * Creates an "insertDrawing" operation.
 */
export function insertDrawing(sheet, pos, type, attrs) {
    return OTHelper.insertDrawing(extendPos(sheet, pos), type, attrs);
}

/**
 * Creates an "insertShape" operation for a drawing shape object.
 */
export function insertShape(sheet, pos, attrs) {
    return OTHelper.insertShape(extendPos(sheet, pos), attrs);
}

/**
 * Creates an "insertChart" operation for a chart object.
 */
export function insertChart(sheet, pos, attrs) {
    return OTHelper.insertChart(extendPos(sheet, pos), attrs);
}

/**
 * Creates a "deleteDrawing" operation.
 */
export function deleteDrawing(sheet, pos) {
    return OTHelper.deleteDrawing(extendPos(sheet, pos));
}

/**
 * Creates a "changeDrawing" operation.
 */
export function changeDrawing(sheet, pos, attrs) {
    return OTHelper.changeDrawing(extendPos(sheet, pos), attrs);
}

/**
 * Creates a "moveDrawing" operation.
 */
export function moveDrawing(sheet, from, to) {
    return OTHelper.moveDrawing(extendPos(sheet, from), extendPos(sheet, to));
}
/**
 * Creates an "insertChartSeries" operation.
 */
export function insertChSeries(sheet, pos, series, attrs) {
    return OTHelper.insertChSeries(extendPos(sheet, pos), series, attrs);
}

/**
 * Creates a "deleteChartSeries" operation.
 */
export function deleteChSeries(sheet, pos, series) {
    return OTHelper.deleteChSeries(extendPos(sheet, pos), series);
}

/**
 * Creates a "changeChartSeries" operation.
 */
export function changeChSeries(sheet, pos, series, attrs) {
    return OTHelper.changeChSeries(extendPos(sheet, pos), series, attrs);
}

/**
 * Creates a "deleteChartAxis" operation.
 */
export function deleteChAxis(sheet, pos, axis) {
    return OTHelper.deleteChAxis(extendPos(sheet, pos), axis);
}

/**
 * Creates a "changeChartAxis" operation.
 */
export function changeChAxis(sheet, pos, axis, attrs) {
    return OTHelper.changeChAxis(extendPos(sheet, pos), axis, attrs);
}

/**
 * Creates a "changeChartGrid" operation.
 */
export function changeChGrid(sheet, pos, axis, attrs) {
    return OTHelper.changeChGrid(extendPos(sheet, pos), axis, attrs);
}

/**
 * Creates a "changeChartTitle" operation.
 */
export function changeChTitle(sheet, pos, axis, attrs) {
    return OTHelper.changeChTitle(extendPos(sheet, pos), axis, attrs);
}

/**
 * Creates a "changeChartLegend" operation.
 */
export function changeChLegend(sheet, pos, attrs) {
    return OTHelper.changeChLegend(extendPos(sheet, pos), attrs);
}

/**
 * Creates an "insertText" operation for text contents.
 */
export function insertText(sheet, pos, text) {
    return OTHelper.insertText(extendPos(sheet, pos), text);
}

/**
 * Creates an "insertTab" operation for text contents.
 */
export function insertTab(sheet, pos) {
    return OTHelper.insertTab(extendPos(sheet, pos));
}

/**
 * Creates an "insertHardBreak" operation for text contents.
 */
export function insertBreak(sheet, pos) {
    return OTHelper.insertBreak(extendPos(sheet, pos));
}

/**
 * Creates an "insertParagraph" operation for text contents.
 */
export function insertPara(sheet, pos) {
    return OTHelper.insertPara(extendPos(sheet, pos));
}

/**
 * Creates a "splitParagraph" operation for text contents.
 */
export function splitPara(sheet, pos) {
    return OTHelper.splitPara(extendPos(sheet, pos));
}

/**
 * Creates a "mergeParagraph" operation for text contents.
 */
export function mergePara(sheet, pos) {
    return OTHelper.mergePara(extendPos(sheet, pos));
}

/**
 * Creates a "setAttributes" operation for text contents.
 */
export function setAttrs(sheet, start, end, attrs) {
    if (is.dict(end) || (end === undefined)) { attrs = end; end = start; }
    return OTHelper.setAttrs(extendPos(sheet, start), extendPos(sheet, end), attrs);
}

/**
 * Creates a "delete" operation for text contents.
 */
export function deleteOp(sheet, start, end) {
    if (end === undefined) { end = start; }
    return OTHelper.deleteOp(extendPos(sheet, start), extendPos(sheet, end));
}

/**
 * Creates a synthetic "position" operation used in OT.
 */
export function positionOp(sheet, start) {
    return OTHelper.positionOp(extendPos(sheet, start));
}

/**
 * Creates a "sheetSelection" operation.
 */
export function sheetSelection(sheet, ranges, index, active, origin, drawings) {
    const props = { index, active };
    if (is.array(origin)) { drawings = origin; origin = null; }
    if (origin) { props.origin = origin; }
    if (drawings) { props.drawings = drawings.map(parsePos); }
    return rangeListOp(Op.SHEET_SELECTION, sheet, ranges, props);
}
