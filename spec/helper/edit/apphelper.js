/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { MockBaseView, MockBaseController, registerMockApp } from "~/apphelper";

// re-exports =================================================================

export * from "~/apphelper";

// class MockEditView =========================================================

export class MockEditView extends MockBaseView {
    isSearchActive() { return false; }
}

// class MockEditController ===================================================

export class MockEditController extends MockBaseController {
    enableUndoRedo() { }
}

// class MockTextFrameworkView ================================================

export class MockTextFrameworkView extends MockEditView {
    recalculateDocumentMargin() { }
    hideFieldFormatPopup() { }
    hideChangeTrackPopup() { }
    clearVisibleDrawingAnchor() { }
    setVisibleDrawingAnchor() { }
    scrollToChildNode() { }
}

// public functions ===========================================================

/**
 * Creates a test application with a real instance of an `EditModel`, and
 * applies the passed document operations in order to initialize the document
 * model.
 *
 * @param {FileFormatType} fileFormat
 *  The file format specifier.
 *
 * @param {Operation[]} [operations]
 *  The initial document operations to be applied after creating the
 *  application.
 *
 * @param {MockAppOptions} [options]
 *  Optional parameters.
 *
 * @returns {JPromise<MockEditApp>}
 *  A promise that will fulfil with the application instance containing a mock
 *  edit application.
 */
export function createEditApp(fileFormat, operations, options) {
    return registerMockApp({
        appType: "edit", // pseudo application type
        fileFormat,
        ResourceManagerClass: { path: "@/io.ox/office/editframework/resource/resourcemanager", symbol: "EditResourceManager" },
        ModelClass:           { path: "@/io.ox/office/editframework/model/editmodel",          symbol: "EditModel" },
        ViewClass:            { path: "@/io.ox/office/editframework/view/editview",            symbol: "EditView",          mock: MockEditView },
        ControllerClass:      { path: "@/io.ox/office/editframework/app/editcontroller",       symbol: "EditController",    mock: MockEditController },
        operations
    }, options);
}

/**
 * Defines test steps (`it` blocks) that run some code to modify the document
 * model, check its changed state, apply the generated undo operations, and
 * optionally apply and check the redo operations.
 *
 * @template DocAppT extends MockEditApp
 *
 * @param {Promise<DocAppT>} appPromise
 *  The application promise returned by one of the application generator
 *  functions.
 *
 * @param {(docModel: DocAppT["docModel"]) => MaybeAsync} implChangeFn
 *  The callback function that implements changing the document model.
 *
 * @param {(docModel: DocAppT["docModel"]) => MaybeAsync} expectChangedFn
 *  The callback function that implements testing the document model after it
 *  has been changed.
 *
 * @param {(docModel: DocAppT["docModel"]) => MaybeAsync} expectRestoreFn
 *  The callback function that implements testing the document model after the
 *  undo operations have been applied.
 *
 * @param {object} [options]
 *  Optional parameters:
 *  - {boolean} [options.redo=false]
 *    If set to `true`, this function will create two more test blocks: One
 *    block to apply the redo operations, and invoke the `expectChangedFn`
 *    function again; and another block to repeat applying and testing the undo
 *    operations.
 */
export function defineModelTestWithUndo(appPromise, implChangeFn, expectChangedFn, expectRestoreFn, options) {
    it("(Step 1: initial action)", async () => {
        const { docModel } = await appPromise;
        await implChangeFn(docModel);
        await expectChangedFn(docModel);
    });
    it("(Step 2: apply undo operations)", async () => {
        const { docModel } = await appPromise;
        await docModel.undoManager.undo();
        await expectRestoreFn(docModel);
    });
    if (options?.redo) {
        it("(Step 3: apply redo operations)", async () => {
            const { docModel } = await appPromise;
            await docModel.undoManager.redo();
            await expectChangedFn(docModel);
        });
        it("(Step 4: apply undo operations after redo)", async () => {
            const { docModel } = await appPromise;
            await docModel.undoManager.undo();
            await expectRestoreFn(docModel);
        });
    }
}
