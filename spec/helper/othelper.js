/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, ary, dict, json } from "@/io.ox/office/tk/algorithms";
import { OTError, isOperationRemoved } from "@/io.ox/office/editframework/utils/otutils";

// re-exports =================================================================

export * from "~/ophelper";

// private functions ==========================================================

function stringify(value) {
    return json.tryStringify(value, { sortKeys: true });
}

function compareOps(ops, expOps, arrName) {
    expect(ops, arrName).toBeArrayOfSize(expOps.length);
    for (const [index, op] of ops.entries()) {
        const exp = expOps[index];
        const prefix = `${arrName}[${index}]: ${op.name}`;
        dict.forEach(exp, (value, key) => {
            const prefix2 = `${prefix}["${key}"]`;
            expect(op, prefix2).toHaveProperty(key);
            expect(stringify(op[key]), prefix2).toBe(stringify(value));
        });
        dict.forEachKey(op, key => {
            const prefix2 = `${prefix}["${key}"]`;
            expect(exp, prefix2).toHaveProperty(key);
        });
    }
}

// public functions ===========================================================

/**
 * Creates an array of similar operations. It is possible to specify multiple
 * different arguments for an operation generator function (first example
 * below), or to call multiple operation generator functions with the same
 * arguments (second example below), or even both together (third example
 * below).
 *
 * Examples: Instead of
 *
 * - `[op1(A), op1(B1, B2, B3), op1()]`
 * - `[op1(A), op2(A), op3(A)]`
 * - `[op1(A), op1(B1, B2, B3), op1(), op2(A), op2(B1, B2, B3), op2()]`
 *
 * it is possible to write
 *
 * - `opSeries1(op1, A, [B1, B2, B3], [])`
 * - `opSeries1([op1, op2, op3], A)`
 * - `opSeries1([op1, op2], A, [B1, B2, B3], [])`
 *
 * @param {Function|Function[]} generators
 *  A function that will be called with the specified arguments, and must
 *  return a JSON operation object; or an array of such generator functions
 *  that will be used to create a concatenated array of different operations by
 *  combining all generators with all arguments.
 *
 * @param {unknown[]} callArgs
 *  The arguments to be passed to the generator functions. Each element of this
 *  array represents the arguments to be passed to one function call of a
 *  generator function. Multiple arguments for the generator must be passed as
 *  array. Use an empty array to call the generator without arguments. To pass
 *  an array as single argument, pack it into another (one-element) array.
 *
 * @returns {Operation[]}
 *  The JSON operations returned from the generator functions.
 */
export function opSeries1(generators, ...callArgs) {
    return ary.wrap(generators).reduce((operations, generator) => {
        callArgs.forEach(callArg => operations.push(generator(...ary.wrap(callArg))));
        return operations;
    }, []);
}

/**
 * Creates an array of similar operations with equal number of arguments. It is
 * possible to specify multiple alternative values for each argument, which
 * will all be combined with each other. Additionally, it is possible to call
 * multiple operation generator functions with all the combinations of the
 * arguments.
 *
 * Examples: Instead of
 *
 * - `[op1(A, B1, C), op1(A, B2, C), op1(A, B3, C)]`
 * - `[op1(A1, B1), op1(A1, B2), op1(A2, B1), op1(A2, B2)]`
 *
 * it is possible to write
 *
 * - `opSeries2(op1, A, [B1, B2, B3], C)`
 * - `opSeries2(op1, [A1, A2], [B1, B2])`
 *
 * @param {Function|Function[]} generators
 *  A function that will be called with the specified arguments, and must
 *  return a JSON operation object; or an array of such generator functions
 *  that will be used to create a concatenated array of different operations by
 *  combining all generators with all arguments.
 *
 * @param {unknown[]} genArgs
 *  The arguments to be passed to the generator functions. Each element of this
 *  array represents an argument of the generator functions (the first value in
 *  this array will always be passed as first argument to the generator
 *  functions, and so on). Each element can be an array which will cause to
 *  call the generator function repeatedly with each of its elements. To pass
 *  an array as single argument, pack it into another (one-element) array.
 *
 * @returns {Operation[]}
 *  The JSON operations returned from the generator functions.
 */
export function opSeries2(generators, ...genArgs) {
    genArgs = genArgs.map(ary.wrap);
    return ary.wrap(generators).reduce((operations, generator) => {
        const indexes = ary.fill(genArgs.length, 0);
        while (indexes[0] < genArgs[0].length) {
            const args = genArgs.map((genArg, i) => genArg[indexes[i]]);
            operations.push(generator(...args));
            for (let i = genArgs.length - 1; i >= 0; i -= 1) {
                indexes[i] += 1;
                if ((i === 0) || (indexes[i] < genArgs[i].length)) { break; }
                indexes[i] = 0;
            }
        }
        return operations;
    }, []);
}

/**
 * Expects that the passed JSON document operation contains the "removed"
 * marker for OT.
 *
 * @param {Operation} op
 *  The JSON document operation to be tested.
 */
export function expectRemoved(op) {
    expect(isOperationRemoved(op)).toBeTrue();
}

/**
 * Expects that the passed JSON document operation does not contain the
 * "removed" marker for OT.
 *
 * @param {Operation} op
 *  The JSON document operation to be tested.
 */
export function expectNotRemoved(op) {
    expect(isOperationRemoved(op)).toBeFalse();
}

/**
 * Comparing the actions operations of one expected action with a specified
 * collections of operations.
 *
 * @param {Action[]} expected
 *  A collection of actions in an array. The actions have the attribute
 *  'operations', that contains a collection of operations.
 *
 * @param {Action[]} result
 *  The calculated collection of actions. The actions have the attribute
 *  'operations', that contains a collection of operations.
 */
export function expectActions(expected, result) {
    expect(expected).toBeArrayOfSize(result.length);
    for (const [index, expectedAction] of expected.entries()) {
        expectAction([expectedAction], [result[index]]);
    }
}

/**
 * Comparing the operations of one expected action with a specified collections
 * of operations.
 *
 * @param {Action[]} expected
 *  One action in an array. The action has the attribute 'operations' that
 *  contains a collection of operations.
 *
 * @param {Operation[]} result
 *  The calculated collection of operations.
 */
export function expectAction(expected, result) {
    expect(expected).toBeArrayOfSize(result.length);
    expect(expected).toHaveLength(1); // not more than one action supported in unit tests
    expectOp(expected[0].operations, result[0].operations);
}

const ignorables = new Set(["osn", "opl"]);

/**
 * Comparing that two collections of operations are identical. Compared are all
 * attributes of an operation, except the ignorable attributes
 *
 * If the value of an attribute is an object, deepEqual is used to compare the
 * value.
 *
 * @param {Operation[]} expected
 *  The collection of expected operations.
 *
 * @param {Operation[]} result
 *  The calculated collection of operations.
 */
export function expectOp(expected, result) {

    expect(expected).toBeArrayOfSize(result.length);

    for (const [index, expectedOp] of expected.entries()) {

        const resultOp = result[index];

        for (const key in expectedOp) {
            if (!ignorables.has(key)) {
                expect(resultOp).toHaveProperty(key);
                expect(expectedOp[key]).toEqual(resultOp[key]);
            }
        }

        for (const key in resultOp) { // checking, that no further attributes exist
            if (!ignorables.has(key)) {
                expect(expectedOp).toHaveProperty(key);
            }
        }
    }
}

// class TestRunner ===========================================================

/**
 * A helper class that executes operation transformations, and tests the
 * resulting transformed operations against the specified expectations.
 *
 * @param {OTEngine} otManager
 *  The application OT manager used to run the transformations.
 *
 * @param {object} [config]
 *  Additional configuration for the test runner.
 *  - {object|()=>object|null} [config.userData]
 *    The user data to be passed to all transformations performed by this
 *    instance. Can be a generator function to create the user data
 *    dynamically. Can be overridden for each transformation test case by
 *    passing this option to the respective method of this class.
 */
export class TestRunner {

    constructor(otManager, config) {
        this.otManager = otManager;
        this.userData = config && config.userData;
    }

    // public methods ---------------------------------------------------------

    /**
     * Transforms the passed operations and compares the result with the
     * specified expected operations.
     *
     * @param {OperationSource} lclOps
     *  The local JSON document operations to be transformed. Creates a deep
     *  clone internally before transformations.
     *
     * @param {OperationSource} extOps
     *  The external JSON document operations to be transformed. Creates a deep
     *  clone internally before transformations.
     *
     * @param {OperationSource|null} [expLclOps]
     *  The expected local operations after all transformations have been
     *  executed (without removed operations). If set to `null` or omitted, the
     *  test will expect that the passed local operations will not be modified
     *  at all. Pass an empty array to expect that all operations have been
     *  removed.
     *
     * @param {OperationSource|null} [expExtOps]
     *  The expected external operations after all transformations have been
     *  executed (without removed operations). If set to `null` or omitted, the
     *  test will expect that the passed external operations will not be
     *  modified at all. Pass an empty array to expect that all operations have
     *  been removed.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.swapped=false]
     *    Whether to run the transformations in swapped mode by passing the
     *    option `swapped` to the `OTEngine` instance.
     *  - {object|()=>object|null} [options.userData]
     *    The user data to be passed to the `OTEngine` instance; or a generator
     *    function to create the user data dynamically.
     *  - {number|boolean} [options.warnings=0]
     *    The expected number of warnings to be issued by all transformations.
     *    If set to `false`, the number of warnings will not be checked. If set
     *    to `true`, expects one warning per executed transformation.
     */
    runTest(lclOps, extOps, expLclOps, expExtOps, options) {
        const origLclOps = ary.wrap(lclOps).flat(Infinity);
        const origExtOps = ary.wrap(extOps).flat(Infinity);
        const newLclOps = json.deepClone(origLclOps);
        let newExtOps = json.deepClone(origExtOps);
        const spy = jest.fn();
        try {
            this.otManager.on("warning", spy);
            newExtOps = this.otManager.transformOperations(newExtOps, [{ operations: newLclOps }], this._makeOptions(options));
            compareOps(newLclOps, expLclOps ? ary.wrap(expLclOps).flat(Infinity) : origLclOps, "lclOps");
            compareOps(newExtOps, expExtOps ? ary.wrap(expExtOps).flat(Infinity) : origExtOps, "extOps");
        } finally {
            this.otManager.off("warning", spy);
        }
        const warnings = options?.warnings;
        if (warnings !== false) {
            const expCount = (warnings === true) ? (origLclOps.length * origExtOps.length) : (warnings || 0);
            expect(spy).toHaveBeenCalledTimes(expCount);
        }
    }

    /**
     * Transforms the passed operations in both directions (as local
     * operations, and as external operations).
     *
     * @param {OperationSource} ops1
     *  The JSON document operations to be transformed as local operations, and
     *  as external operations. Creates a deep clone internally before
     *  transformations.
     *
     * @param {OperationSource} ops2
     *  The JSON document operations to be transformed as external operations,
     *  and as local operations. Creates a deep clone internally before
     *  transformations.
     *
     * @param {OperationSource|null} [exps1]
     *  The expected result after transforming `ops1`. If set to `null` or
     *  omitted, the test will expect that the operations will not be modified
     *  at all. Pass an empty array to expect that all operations have been
     *  removed.
     *
     * @param {OperationSource|null} [exps2]
     *  The expected result after transforming `ops2`. If set to `null` or
     *  omitted, the test will expect that the operations will not be modified
     *  at all. Pass an empty array to expect that all operations have been
     *  removed.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.swapped=false]
     *    Whether to run the transformations in swapped mode by passing the
     *    option `swapped` to the `OTEngine` instance.
     *  - {object|()=>object|null} [options.userData]
     *    The user data to be passed to the `OTEngine` instance; or a generator
     *    function to create the user data dynamically.
     *  - {number} [options.warnings=0]
     *    The expected number of warnings to be issued by all transformations
     *    in either direction.
     */
    runBidiTest(ops1, ops2, exps1, exps2, options) {
        this.runTest(ops1, ops2, exps1, exps2, options);
        this.runTest(ops2, ops1, exps2, exps1, options);
    }

    /**
     * Expects that transforming the passed operations issues the specified
     * number of warning messages.
     *
     * @param {OperationSource} lclOps
     *  The local JSON document operations to be transformed. Creates a deep
     *  clone internally before transformations.
     *
     * @param {OperationSource} extOps
     *  The external JSON document operations to be transformed. Creates a deep
     *  clone internally before transformations.
     *
     * @param {number} expCount
     *  The expected number of warnings.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.swapped=false]
     *    Whether to run the transformations in swapped mode by passing the
     *    option `swapped` to the `OTEngine` instance.
     *  - {object|()=>object|null} [options.userData]
     *    The user data to be passed to the `OTEngine` instance; or a generator
     *    function to create the user data dynamically.
     */
    expectWarnings(lclOps, extOps, expCount, options) {
        lclOps = json.deepClone(ary.wrap(lclOps).flat(Infinity));
        extOps = json.deepClone(ary.wrap(extOps).flat(Infinity));
        const spy = jest.fn();
        try {
            this.otManager.on("warning", spy);
            this.otManager.transformOperations(extOps, [{ operations: lclOps }], this._makeOptions(options));
        } finally {
            this.otManager.off("warning", spy);
        }
        expect(spy).toHaveBeenCalledTimes(expCount);
    }

    /**
     * Expects that transforming the passed operations logs the specified
     * number of warning messages in either direction.
     *
     * @param {OperationSource} ops1
     *  The JSON document operations to be transformed as local operations, and
     *  as external operations. Creates a deep clone internally before
     *  transformations.
     *
     * @param {OperationSource} ops2
     *  The JSON document operations to be transformed as external operations,
     *  and as local operations. Creates a deep clone internally before
     *  transformations.
     *
     * @param {number} count
     *  The expected number of warnings in either direction.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.swapped=false]
     *    Whether to run the transformations in swapped mode by passing the
     *    option `swapped` to the `OTEngine` instance.
     *  - {object|()=>object|null} [options.userData]
     *    The user data to be passed to the `OTEngine` instance; or a generator
     *    function to create the user data dynamically.
     */
    expectBidiWarnings(ops1, ops2, count, options) {
        this.expectWarnings(ops1, ops2, count, options);
        this.expectWarnings(ops2, ops1, count, options);
    }

    /**
     * Expects that trying to transform the passed operations fails (somewhere)
     * by throwing an `OTError`.
     *
     * @param {OperationSource} lclOps
     *  The local JSON document operations to be transformed. Creates a deep
     *  clone internally before transformations.
     *
     * @param {OperationSource} extOps
     *  The external JSON document operations to be transformed. Creates a deep
     *  clone internally before transformations.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.swapped=false]
     *    Whether to run the transformations in swapped mode by passing the
     *    option `swapped` to the `OTEngine` instance.
     *  - {object|()=>object|null} [options.userData]
     *    The user data to be passed to the `OTEngine` instance; or a generator
     *    function to create the user data dynamically.
     */
    expectError(lclOps, extOps, options) {
        lclOps = json.deepClone(ary.wrap(lclOps).flat(Infinity));
        extOps = json.deepClone(ary.wrap(extOps).flat(Infinity));
        const otManager = this.otManager;
        const transformFunc = () => otManager.transformOperations(extOps, [{ operations: lclOps }], this._makeOptions(options));
        expect(transformFunc).toThrow(OTError);
    }

    /**
     * Expects that trying to transform the passed operations fails in either
     * direction by throwing an `OTError`.
     *
     * @param {OperationSource} ops1
     *  The JSON document operations to be transformed as local operations, and
     *  as external operations. Creates a deep clone internally before the
     *  transformations.
     *
     * @param {OperationSource} ops2
     *  The JSON document operations to be transformed as external operations,
     *  and as local operations. Creates a deep clone internally before the
     *  transformations.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.swapped=false]
     *    Whether to run the transformations in swapped mode by passing the
     *    option `swapped` to the `OTEngine` instance.
     *  - {object|()=>object|null} [options.userData]
     *    The user data to be passed to the `OTEngine` instance; or a generator
     *    function to create the user data dynamically.
     */
    expectBidiError(ops1, ops2, options) {
        this.expectError(ops1, ops2, options);
        this.expectError(ops2, ops1, options);
    }

    /**
     * Transforms a set of external actions with a specified set of local actions.
     *
     * This is not a function required in OX Documents, because every single external
     * operation is converted against the set of local actions and then directly
     * applied to the DOM.
     *
     * This function is important for tests, to check valid transformation for a
     * larger set of external operations.
     *
     * Within this test the "action" structure of the local and especially the external
     * actions is retained. This is especially for the external operations not
     * necessary, because they are applied to the DOM, so an alternative solution
     * could also be, that only an array with all external operations is returned.
     * But retaining the "action" structure might simplify to recognize the transformation
     * of each single external action.
     *
     * @returns {Actions[]}
     *  A container with all modified external actions.
     */
    transformActions(externalActions, localActions) {
        externalActions.forEach(action => {
            action.operations = this.otManager.transformOperations(action.operations, localActions);
        });
        return externalActions;
    }

    // private methods --------------------------------------------------------

    _makeOptions(options) {
        const userData = (options && ("userData" in options)) ? options.userData : this.userData;
        return {
            swapped: options?.swapped,
            userData: is.function(userData) ? userData() : userData
        };
    }
}
