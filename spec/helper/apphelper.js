/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import ui from "$/io.ox/core/desktop";

import { is, fun, jpromise } from "@/io.ox/office/tk/algorithms";
import { DObject, BaseObject } from "@/io.ox/office/tk/objects";

import { AppType } from "@/io.ox/office/baseframework/utils/apputils";
import { PerformanceTracker } from "@/io.ox/office/baseframework/utils/performancetracker";
import { XAppAccess } from "@/io.ox/office/baseframework/app/xappaccess";

import { dbgFormatOp } from "@/io.ox/office/editframework/utils/otutils";

// private functions ==========================================================

// helper function that does nothing but returns the calling context
/** @this {unknown} */ function noop() { return this; }

async function loadClass(classConfig, loadRealClass) {
    if (loadRealClass) {
        const module = await import(classConfig.path);
        return module[classConfig.symbol];
    }
    return classConfig.mock;
}

// static initialization ======================================================

// fake the core root node
$("body").append('<div id="io-ox-core" class="abs">');

// class MockEvents ===========================================================

class MockEvents {
    __typeTag = "DocsEmitter";
    on() { return this; }
    one() { return this; }
    off() { return this; }
    trigger() { return this; }
    listenTo() { return this; }
    listenToAllEvents() { return this; }
}

// class MockBaseView =========================================================

/**
 * A simple mock view class for unit tests. Mimics the common public API of a
 * real `BaseView`.
 */
export class MockBaseView extends MockEvents {

    constructor(docApp, docModel) {
        super();

        this.docApp = docApp;
        this.docModel = docModel;

        this.rootNode = $("<div>");
        this.$winBodyNode = this.rootNode;
        this.$appPaneNode = this.rootNode;
        this.$contentRootNode = this.rootNode;
        this.$appContentNode = this.rootNode;

        this.toolPaneManager = { one: fun.undef, getActiveTabId: fun.undef, activateToolPane: fun.undef };
        this.collaboratorMenu = { isVisible: fun.true, toggle: fun.undef, show: fun.undef, hide: fun.undef };
    }

    getContentRootNode() { return this.$contentRootNode; }

    isVisible() { return true; }
    isBusy() { return false; }
    executeControllerItem() {}
    grabFocus() { }
    getZoomFactor() { return 1; }
    changeZoom() { }
    enterBusy() { }
    leaveBusy() { }
    updateBusyProgress() { }
    yell() { }
    refreshPaneLayout() { }
    initBeforeImport() { }
    showAfterImport() { }
    showQueryDialog() { return jpromise.resolve(); }
    destroy() {}

    createClipboardNode() {
        const clipboardNode = $('<div class="clipboard">');
        $("body").append(clipboardNode); // attaching clipboard to DOM because of browser selection handling
        return clipboardNode;
    }
}

// class MockBaseController ===================================================

/**
 * A simple test controller class for unit tests. Mimics the common public
 * API of a Documents controller instance.
 */
export class MockBaseController extends MockEvents {

    constructor(docApp, docModel, docView) {
        super();

        this.docApp = docApp;
        this.docModel = docModel;
        this.docView = docView;
    }

    registerItem() {}
    registerItems() {}

    isItemEnabled() { return false; }
    getItemValue() {}
    executeItem() {}
    update() {}

    destroy() {}
}

// public functions ===========================================================

/**
 * Creates a core application replacement for unit tests that behaves like the
 * application base objects used by the `ApplicationLauncher`, intended to be
 * extended with a real `BaseApplication` mix-in class or one of its
 * subclasses.
 *
 * @returns {App}
 *  The core application mock.
 */
export function createCoreApp(appType) {

    const coreApp = ui.createApp({ name: "io.ox/office/" + appType });
    const appWindow = ui.createWindow({ name: "io.ox/office/" + appType, search: false, chromeless: true });
    appWindow.isBusy = fun.false;

    coreApp.setWindow(appWindow);
    coreApp.setLauncher(fun.undef);
    coreApp.setState = fun.undef;
    coreApp.set("docsAppTypeTag", "docs");
    coreApp.set("docsAppType", appType);
    coreApp.set("docsLaunchTracker", new PerformanceTracker("LAUNCH", appType, "launch_application"));

    return coreApp;
}

/**
 * Generic implementation to create a test application instance. The creation
 * of the application will be placed in a `beforeAll()` block, and the
 * application will be destroyed in an `afterAll()` block. This results in
 * creation and destruction of the test application inside the surrounding
 * `describe()` block.
 *
 * @param {MockAppConfig} appConfig
 *  Application configuration, with the following properties:
 *  - {AppType} appConfig.appType
 *    The type identifier of the application.
 *  - {FileFormatType} appConfig.fileFormat
 *    The file format of the document represented by the application.
 *  - {MockClassSpec<BaseResourceManager>} appConfig.ResourceManagerClass
 *    Specifier for the constructor function for the document resource manager.
 *  - {MockClassSpec<BaseModel>} appConfig.ModelClass
 *    Specifier for the constructor function for the document model.
 *  - {MockClassSpec<BaseView>} appConfig.ViewClass
 *    Specifier for the constructor function for the document view.
 *  - {MockClassSpec<BaseController>} appConfig.ControllerClass
 *    Specifier for the constructor function for the document controller.
 *  - {MockClassSpec<OTEngine>} [appConfig.OTManagerClass]
 *    Specifier for the constructor function for the OT manager instance.
 *  - {Operation[]} [appConfig.operations]
 *    The initial document operations to be applied after creating the
 *    application.
 *  - {Dict<Function>} [appConfig.converters]
 *    App-specific operation converters, mapped by operation names, used to
 *    preprocess the operations passed in the option `operations`.
 *  - {(docApp, docView) => MaybeAsync} [appConfig.finalizer]
 *    Callback function that can be used to do further initialization after
 *    the application has been created. The resulting promise returned from
 *    `registerMockApp()` will be deferred until this callback settles.
 *
 * @param {MockAppOptions} [appOptions]
 *  Optional parameters:
 *  - {boolean} [appOptions.realView=false]
 *    If set to `true`, the real class from the "ViewClass" configuration will
 *    be loaded by the mock application instead of the mock view.
 *  - {boolean} [appOptions.realController=false]
 *    If set to `true`, the real class from the "ControllerClass" configuration
 *    will be loaded by the mock application instead of the mock controller.
 *
 * @returns {JPromise<MockApp>}
 *  A promise that will fulfil with the test application.
 */
export function registerMockApp(appConfig, appOptions) {

    const docApp = new BaseObject();
    const appDef = new $.Deferred();
    // eslint-disable-next-line no-undef
    let beforeAllTimeout = parseInt(process.env.UNITTEST_BEFORE_ALL_TIMEOUT, 10);
    beforeAllTimeout = (beforeAllTimeout > 0) ? beforeAllTimeout : undefined;

    // place the application creation code in a `before` block
    beforeAll(async () => {

        const coreApp = createCoreApp(appConfig.appType);
        const appWindow = coreApp.getWindow();
        const winRootNode = appWindow.nodes.outer;

        docApp.coreApp = coreApp;
        docApp.moduleName = coreApp.getName();
        const appType = docApp.appType = appConfig.appType;
        docApp.launchTracker = coreApp.get("docsLaunchTracker");

        const importDef = jpromise.deferred();

        let initHandlers = [];
        let inQuit = false;

        docApp.onInit = function (fn) {
            if (initHandlers) {
                initHandlers.push(fn);
            } else {
                fn();
            }
        };

        docApp.getWindow = () => appWindow;
        docApp.getWindowNode = () => coreApp.getWindowNode();

        docApp.importStartPromise = jpromise.deferred();
        docApp.importFinishPromise = importDef.promise();

        docApp.getModel = function () { return this.docModel; };
        docApp.getView = function () { return this.docView; };
        docApp.getController = function () { return this.docController; };

        docApp.isActive = fun.true;
        docApp.getRootNode = fun.const(winRootNode);
        docApp.isInQuit = () => inQuit;

        // provide (most of) the API of a real BaseObject instance (taken from baseapplication.js)
        const AppAccessContext = XAppAccess.mixin(DObject);
        docApp.docApp = docApp;
        for (const name of Object.getOwnPropertyNames(AppAccessContext.prototype)) {
            if (name !== "constructor") {
                const value = AppAccessContext.prototype[name];
                if (is.function(value)) { docApp[name] = value; }
            }
        }

        docApp.leaveBusyDuringImport = fun.const(importDef.promise());
        docApp.getFileFormat = fun.const(appConfig.fileFormat);
        docApp.isOOXML = fun.const(appConfig.fileFormat === "ooxml");
        docApp.isODF = fun.const(appConfig.fileFormat === "odf");
        docApp.getFullFileName = fun.const("testfile.xyz");
        docApp.getShortFileName = fun.const("testfile");
        docApp.getFileExtension = fun.const("xyz");
        docApp.getFilePath = fun.const(["path", "to"]);
        docApp.getFileDescriptor = fun.const({ id: "1", folder_id: "1" });
        docApp.registerVisibleStateHandler = noop;
        docApp.isLocalStorageSupported = fun.true;

        docApp.isEditable = fun.true;
        docApp.isInternalError = fun.false;
        docApp.setInternalError = () => { throw new Error("internal error"); };
        docApp.isLocallyModified = fun.true;
        docApp.isOperationsBlockActive = fun.false;
        docApp.stopOperationDistribution = callback => callback.call(docApp);
        docApp.setUserState = noop;
        docApp.sendLogMessage = noop;

        docApp.getUserSettingsValue = fun.null;
        docApp.destroyImageNodes = noop;
        docApp.addAuthors = noop;
        docApp.getAuthorColorIndex = fun.const(1);
        docApp.isMultiSelectionApplication = fun.const(appType !== AppType.TEXT);
        docApp.getActiveClients = fun.const([{ userId: 1, userName: "test1" }]);
        docApp.updateUserData = noop;
        docApp.showMainToolPaneAtStart = fun.true;
        docApp.getUserInfo = fun.const(jpromise.resolve({ guest: false, operationName: "User 1" }));
        docApp.getClientOperationName = fun.const("author");

        docApp.isTextApp = fun.const(appType === AppType.TEXT);
        docApp.isSpreadsheetApp = fun.const(appType === AppType.SPREADSHEET);
        docApp.isPresentationApp = fun.const(appType === AppType.PRESENTATION);

        docApp.getShortClientId = fun.const("1234");
        docApp.isRemovalOfInternalBlockerPossible = fun.true;
        docApp.checkStartOfExternalOperations = noop;
        docApp.getServerOSN = fun.const(42);
        docApp.getLaunchOption = fun.null;
        docApp.reloadDocument = fun.undef;
        docApp.isReloading = fun.false;
        docApp.isViewerMode = fun.false;
        docApp.setRootAttribute = fun.undef;

        const OTManagerSpec = appConfig.OTManagerClass;
        let OTManagerClass; // will be initialized in `docApp.setLauncher`
        docApp.isOTEnabled = fun.const(!!OTManagerSpec);

        // performance: create OT manager lazily on demand
        if (OTManagerSpec) {
            Object.defineProperty(docApp, "otManager", {
                get: () => (docApp._otManager ??= new OTManagerClass(docApp.docModel))
            });
        }

        docApp.createImageNode = function (url) {
            const def = new $.Deferred();
            const imgNode = $("<img>");
            const handlers = {
                load() { def.resolve(imgNode); },
                error() { def.reject(); }
            };
            imgNode.one(handlers).attr("src", url);
            return def.always(() => imgNode.off(handlers)).promise();
        };

        docApp.createDebouncedFor = fun.const(noop);

        docApp.quit = function () {
            inQuit = true;
            this.trigger("docs:destroying");
            this.docController.destroy();
            this.docView.destroy();
            this.docModel.destroy();
            this.destroy();
        };

        docApp.onInit(() => docApp.importStartPromise.resolve());

        coreApp.setLauncher(async () => {

            const ResourceManagerClass = await loadClass(appConfig.ResourceManagerClass, true);
            docApp.resourceManager = new ResourceManagerClass(docApp);
            await docApp.resourceManager.loadResources();

            const ModelClass = await loadClass(appConfig.ModelClass, true);
            const docModel = docApp.docModel = new ModelClass(docApp);

            const ViewClass = await loadClass(appConfig.ViewClass, appOptions?.realView);
            const docView = docApp.docView = new ViewClass(docApp, docModel);

            const ControllerClass = await loadClass(appConfig.ControllerClass, appOptions?.realController);
            docApp.docController = new ControllerClass(docApp, docModel, docView);

            if (OTManagerSpec) {
                OTManagerClass = await loadClass(OTManagerSpec, true);
            }

            for (const fn of initHandlers) { fn(); }
            initHandlers = undefined;

            await docApp.docView.initBeforeImport();

            let operations = appConfig.operations;
            if (is.array(operations)) {
                const converters = appConfig.converters;
                if (converters) {
                    operations = operations.map(operation => {
                        if (operation.name in converters) {
                            operation = { ...operation };
                            converters[operation.name](operation);
                        }
                        return operation;
                    });
                }
                try {
                    docModel.invokeOperationHandlers(operations);
                } catch (err) {
                    console.error(appType + " operation #" + err.index + " " + dbgFormatOp(err.operation) + " failed: " + err.message);
                    throw err;
                }
            }
            importDef.resolve();
        });

        try {

            // call the application launcher
            await coreApp.launch();

            // call the custom finalizer callback
            if (appConfig.finalizer) {
                await appConfig.finalizer(docApp, docApp.docModel);
            }

            // notify external listeners
            appDef.resolve(docApp);
        } catch (err) {
            appDef.reject(err);
        }
    }, beforeAllTimeout);

    afterAll(() => docApp?.quit());

    // return the promise that will fulfil with the application
    return appDef.promise();
}
