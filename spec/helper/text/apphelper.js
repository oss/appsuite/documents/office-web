/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { AppType } from "@/io.ox/office/baseframework/utils/apputils";

import { registerMockApp, MockTextFrameworkView, MockEditController } from "~/edit/apphelper";

// re-exports =================================================================

export { defineModelTestWithUndo } from "~/edit/apphelper";

// class MockTextView =========================================================

class MockTextView extends MockTextFrameworkView {

    constructor(docApp, docModel) {
        super(docApp, docModel);

        this.commentsPane = {
            toggle() { return this; },
            isVisible() { return true; },
            getThreadedCommentFrameById() { return null; }
        };
    }

    getDisplayDateString() { return "A"; }
    createTextCommentsPane() { return this.commentsPane; }
    updateCommentsIfRequired() { }
}

// class MockTextController ===================================================

class MockTextController extends MockEditController {
}

// public functions ===========================================================

/**
 * Creates a test application with a real text document model, and applies the
 * passed document operations in order to initialize the document model.
 *
 * @param {FileFormatType} fileFormat
 *  The file format specifier.
 *
 * @param {Operation[]} [operations]
 *  The initial document operations to be applied after creating the
 *  application.
 *
 * @param {MockAppOptions} [options]
 *  Optional parameters.
 *
 * @returns {JPromise<MockTextApp>}
 *  A promise that will fulfil with the application instance containing a mock
 *  text application.
 */
export function createTextApp(fileFormat, operations, options) {
    return registerMockApp({
        appType: AppType.TEXT,
        fileFormat,
        ResourceManagerClass: { path: "@/io.ox/office/text/resource/resourcemanager", symbol: "TextResourceManager" },
        ModelClass:           { path: "@/io.ox/office/text/model/docmodel",           symbol: "default" },
        ViewClass:            { path: "@/io.ox/office/text/view/view",                symbol: "default",    mock: MockTextView },
        ControllerClass:      { path: "@/io.ox/office/text/app/controller",           symbol: "default",    mock: MockTextController },
        OTManagerClass:       { path: "@/io.ox/office/text/components/ot/otmanager",  symbol: "default" },
        operations,
        async finalizer(_docApp, docModel) {
            docModel.setIgnoreMinMoveTime(true);
            await docModel.updateDocumentFormatting();
            await docModel.setDefaultPaperFormat();
            docModel.getSelection().setTextSelection([0, 0]);
        }
    }, options);
}
