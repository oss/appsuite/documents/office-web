/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "underscore";

// class FakeClock ============================================================

/**
 * Wrapper object for fake timers. Registers `beforeEach` and `afterEach` hooks
 * to register new fake timers for every test in the current scope.
 */
class FakeClock {

    constructor(now) {

        // each test has its own fake timers
        beforeEach(() => {
            jest.useFakeTimers({ now });
        });

        // cleanup after each test: drain timer and promise queue, restore system timers
        afterEach(async () => {
            await this.wait(10000);
            jest.useRealTimers();
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Advances the fake clock by the specified amount of milliseconds.
     */
    tick(ms) {
        jest.advanceTimersByTime(ms);
    }

    /**
     * Advances the fake clock by the specified amount of milliseconds, and
     * drains the microtask queue by awaiting a resolved promise.
     */
    async wait(ms) {
        await jest.advanceTimersByTimeAsync(ms);
    }
}

// public functions ===========================================================

/**
 * Creates and returns an array with spy functions.
 *
 * @param {number} count
 *  The number of spy functions to be created.
 *
 * @param {unknown[]} args
 *  Arguments to be passed to the spy function constructors.
 *
 * @returns {jest.Mock[]}
 *  An array with the specified number of spy functions.
 */
export function createJestFns(count, ...args) {
    return _.times(count, idx => jest.fn(args[idx]));
}

/**
 * Creates a wrapper that initializes a new fake clock for each test in the
 * scope this function was called in (via `beforeEach` and `afterEach`).
 *
 * @param {number|Date} [time]
 *  The initial system time to be used by the fake clock. By default, the fake
 *  clock will start at zero.
 *
 * @returns {FakeClock}
 *  The fake clock wrapper. MUST only be used inside tests ("it" blocks).
 */
export function useFakeClock(time = 0) {
    return new FakeClock(time);
}

/**
 * Registers a listener at an event emitter, executes the callback function,
 * and collects the event arguments of the first emitted event.
 *
 * The event listener will be removed automatically after the event has been
 * received, or the function has timed out.
 *
 * @param {AnyEventEmitter} emitter
 *  An event emitter (DOM emitter, JQuery collection, Backbone model, EventHub,
 *  EObject).
 *
 * @param {string} type
 *  The type of the event to listen to.
 *
 * @param {() => MaybeAsync} [fn]
 *  The callback function to be executed after starting to listen to the event.
 *
 * @returns {Promise<unknown[]>}
 *  A promise that will fulfil with the arguments passed to the event listener,
 *  or that will reject after two seconds without receiving an event.
 */
export async function waitForEvent(emitter, type, fn) {

    // create a new promise to be resolved by the event handler
    let resolveEvent, rejectEvent;
    const eventPromise = new Promise((resolve, reject) => {
        resolveEvent = resolve;
        rejectEvent = reject;
    });

    // callback function to unregister an event listener
    let off;

    // the event handler (resolves `eventPromise` with the event arguments)
    const handler = (...args) => {
        off();
        resolveEvent(args);
    };

    // timeout handler (rejects `eventPromise` with timeout error)
    const timeout = window.setTimeout(() => {
        off();
        rejectEvent(new Error("timeout"));
    }, 2000);

    // register the event handler according to emitter type (before running the callback!)
    if (typeof emitter.addEventListener === "function") {
        off = () => emitter.removeEventListener(type, handler);
        emitter.addEventListener(type, handler);
    } else if (typeof emitter.on === "function") {
        off = () => emitter.off(type, handler);
        emitter.on(type, handler);
    } else {
        throw new TypeError("unsupported event emitter");
    }

    // start the callback function, and wait for it (may be async by itself),
    // and wait for the event (may be emitted during callback, or some time later)
    try {
        await fn?.();
        return await eventPromise;
    } finally {
        window.clearTimeout(timeout);
    }
}

/**
 * Observes a native promise. Collects its state and result value.
 *
 * @param {Promise<RT>} promise
 *  The promise to be observed.
 *
 * @returns {PromiseObserver<RT>}
 *  An object with properties "promise", "state", and "value".
 */
export function observePromise(promise) {
    const observed = { promise, state: "pending", value: undefined };
    promise
        .then(value => Object.assign(observed, { state: "resolved", value }))
        .catch(value => Object.assign(observed, { state: "rejected", value }));
    return observed;
}
