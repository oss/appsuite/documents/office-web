/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { AppType } from "@/io.ox/office/baseframework/utils/apputils";

import { registerMockApp, MockTextFrameworkView, MockEditController } from "~/edit/apphelper";

// re-exports =================================================================

export { defineModelTestWithUndo } from "~/edit/apphelper";

// class MockPresentationView =================================================

class MockPresentationView extends MockTextFrameworkView {

    getSlidePane() {
        return {
            getSlidePaneContainer() {},
            getSlidePaneSelection() {},
            setSlidePaneSelection() {},
            setSidePaneWidthDuringLoad() {}
        };
    }

    enableVerticalScrollBar() { }
    handleSlideBackgroundVisibility() { }
    setVirtualFocusSlidepane() { }
    areSnaplinesEnabled() { return false; }
}

// class MockPresentationController ===========================================

class MockPresentationController extends MockEditController {
}

// public functions ===========================================================

/**
 * Creates a test application with a real presentation document model, and
 * applies the passed document operations in order to initialize the document
 * model.
 *
 * @param {FileFormatType} fileFormat
 *  The file format specifier.
 *
 * @param {Operation[]} [operations]
 *  The initial document operations to be applied after creating the
 *  application.
 *
 * @param {MockAppOptions} [options]
 *  Optional parameters.
 *
 * @returns {JPromise<MockPresentationApp>}
 *  A promise that will fulfil with the application instance containing a mock
 *  presentation application.
 */
export function createPresentationApp(fileFormat, operations, options) {
    return registerMockApp({
        appType: AppType.PRESENTATION,
        fileFormat,
        ResourceManagerClass: { path: "@/io.ox/office/presentation/resource/resourcemanager", symbol: "PresentationResourceManager" },
        ModelClass:           { path: "@/io.ox/office/presentation/model/docmodel",           symbol: "default" },
        ViewClass:            { path: "@/io.ox/office/presentation/view/view",                symbol: "default",    mock: MockPresentationView },
        ControllerClass:      { path: "@/io.ox/office/presentation/controller/controller",    symbol: "default",    mock: MockPresentationController },
        OTManagerClass:       { path: "@/io.ox/office/presentation/components/ot/otmanager",  symbol: "default" },
        operations,
        async finalizer(_docApp, docModel) {
            docModel.setIgnoreMinMoveTime(true);
            await docModel.updateDocumentFormatting();
        }
    }, options);
}
