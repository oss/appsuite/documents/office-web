/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import Backbone from '$/backbone';

import TemplateCollection from '@/io.ox/office/portal/collection/templatecollection';

// tests ======================================================================

describe('module portal/collection/templatecollection', function () {

    // class TemplateCollection -----------------------------------------------

    describe('class TemplateCollection', function () {

        it('should subclass Backbone.Collection', function () {
            expect(TemplateCollection).toBeSubClassOf(Backbone.Collection);
        });

        describe('method setData', function () {
            it('should exist', function () {
                expect(TemplateCollection).toHaveMethod('setData');
            });

            it('should fetch data', function () {
                var collection = new TemplateCollection();
                collection.setData([{ filename: 'one' }, { filename: 'two' }]);
                expect(collection).toHaveLength(2);
            });

            it('should return template', function () {
                var collection = new TemplateCollection();

                collection.setData([
                    { filename: 'one.docx', type: 'text' },
                    { filename: 'two.xlsx', type: 'spreadsheet' },
                    { filename: 'three.xlsx', type: 'text', source: 'definedSource' }
                ]);

                expect(collection).toHaveLength(3);
                expect(collection.getTemplates('text', 'all')).toBeArray();
                expect(collection.getTemplates('text', 'all')).toHaveLength(2);

                expect(collection.getTemplates('text', 'definedSource')).toBeArray();
                expect(collection.getTemplates('text', 'definedSource')).toHaveLength(1);

                expect(collection.getTemplates('text')).toBeArray();
                expect(collection.getTemplates('text')).toHaveLength(2);
            });
        });
    });
});
