/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import Backbone from "$/backbone";

import TemplateModel from '@/io.ox/office/portal/model/templatemodel';

// tests ======================================================================

describe('module portal/model/templatemodel', function () {

    // class TemplateModel ----------------------------------------------------

    describe('class TemplateModel', function () {

        it('should subclass Backbone.Model', function () {
            expect(TemplateModel).toBeSubClassOf(Backbone.Model);
        });

        describe('method get', function () {
            it('should exist', function () {
                expect(TemplateModel).toHaveMethod('get');
            });
            it('should return the filename', function () {
                var model = new TemplateModel({ filename: 'name.docx' });
                expect(model.get('filename')).toBe('name.docx');
            });
        });
    });
});
