/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import * as utils from '@/io.ox/office/portal/portalutils';

import { useFakeClock } from "~/asynchelper";

// tests ======================================================================

describe('module office/portal/portalutils', function () {

    // functions --------------------------------------------------------------

    describe('function createDocumentIcon', function () {
        it('should exist', function () {
            expect(utils.createDocumentIcon).toBeFunction();
        });
        it('should return the correct app-icon', function () {
            expect(utils.createDocumentIcon('invalid_')[0].dataset.iconId).toBe("file");
            expect(utils.createDocumentIcon('bla.docx')[0].dataset.iconId).toBe("file-earmark-text");
            expect(utils.createDocumentIcon('bla.xlsx')[0].dataset.iconId).toBe("spreadsheet");
            expect(utils.createDocumentIcon('bla.pptx')[0].dataset.iconId).toBe("presentation");
        });
    });

    describe('function removeFileExtension', function () {
        it('should exist', function () {
            expect(utils.removeFileExtension).toBeFunction();
        });
        it('should return the filename without extension', function () {
            expect(utils.removeFileExtension('nevermind')).toBe("nevermind");
            expect(utils.removeFileExtension('bla.docx')).toBe("bla");
            expect(utils.removeFileExtension('bla.bla.xlsx')).toBe("bla.bla");
        });
    });

    describe('function moveFocus', function () {
        it('should exist', function () {
            expect(utils.moveFocus).toBeFunction();
        });
        it('should set the focus and not return anything', function () {
            var firstNode = $('<div>'),
                secondNode = $('<div>'),
                thirdNode = $('<div>'),

                fourthNode = $('<div tabindex="1">'),
                fifthNode = $('<div tabindex="2">'),
                sixthNode = $('<div tabindex="3">');

            $('<div>').append(firstNode, secondNode, thirdNode);
            $('<div>').append(fourthNode, fifthNode, sixthNode);

            expect(utils.moveFocus()).toBeUndefined();
            expect(utils.moveFocus(secondNode)).toBeUndefined();
            expect(utils.moveFocus(secondNode, 'left')).toBeUndefined();
            expect(utils.moveFocus(secondNode, 'down')).toBeUndefined();
            expect(utils.moveFocus(secondNode, 'up')).toBeUndefined();

            expect(utils.moveFocus(fifthNode)).toBeUndefined();
            expect(utils.moveFocus(fifthNode, 'left')).toBeUndefined();
            expect(utils.moveFocus(fifthNode, 'down')).toBeUndefined();
            expect(utils.moveFocus(fifthNode, 'up')).toBeUndefined();
        });
    });

    describe('function keydownHandler', function () {
        it('should exist', function () {
            expect(utils.keydownHandler).toBeFunction();
        });
        it('should handle keydown events', function () {
            var spy = jest.fn(),
                ele = $('<a>').on('click', spy),

                e1      = new $.Event('click', { target: ele }),
                e2      = new $.Event('click', { target: ele, data: { grid: true } }),
                e3a     = new $.Event('click', { target: ele, keyCode: 13 }),
                e3b     = new $.Event('click', { target: ele, keyCode: 37 }),
                e3c     = new $.Event('click', { target: ele, keyCode: 38 }),
                e3c2    = new $.Event('click', { target: ele, keyCode: 38, data: { grid: true } }),
                e3d     = new $.Event('click', { target: ele, keyCode: 39 }),
                e3e     = new $.Event('click', { target: ele, keyCode: 40 }),
                e3e2    = new $.Event('click', { target: ele, keyCode: 40, data: { grid: true } });

            expect(utils.keydownHandler()).toBeUndefined();
            expect(utils.keydownHandler(e1)).toBeUndefined();
            expect(utils.keydownHandler(e2)).toBeUndefined();

            expect(utils.keydownHandler(e3a)).toBeUndefined();
            expect(spy).toHaveBeenCalledOnce();

            expect(utils.keydownHandler(e3b)).toBeUndefined();
            expect(utils.keydownHandler(e3c)).toBeUndefined();
            expect(utils.keydownHandler(e3c2)).toBeUndefined();
            expect(utils.keydownHandler(e3d)).toBeUndefined();
            expect(utils.keydownHandler(e3e)).toBeUndefined();
            expect(utils.keydownHandler(e3e2)).toBeUndefined();
        });
    });

    describe('function formatDate', function () {
        it('should exist', function () {
            expect(utils.formatDate).toBeFunction();
        });

        describe('time tests (now=2016-01-01 14:00)', function () {

            useFakeClock(new Date(2016, 0, 1, 14, 0, 0));

            it('should have a function "formatDate"', function () {
                expect(utils.formatDate).toBeFunction();
            });

            it('formatDate should return the correct formatted date', function () {
                var now             = new Date(),
                    yesterday       = new Date(2015, 11, 31, 14, 0, 0),
                    prevMonth       = new Date(2015, 11, 1, 14, 0, 0),
                    prevYear        = new Date(2015, 0, 1, 14, 0, 0);

                expect(utils.formatDate(now.getTime())).toBe("2:00 PM");
                expect(utils.formatDate(yesterday.getTime())).toBe("12/31/2015");
                expect(utils.formatDate(prevMonth.getTime())).toBe("12/01/2015");
                expect(utils.formatDate(prevYear.getTime())).toBe("01/01/2015");
            });
        });

        describe('time tests (now=2016-05-01 14:00)', function () {

            useFakeClock(new Date(2016, 4, 1, 14, 0, 0));

            it('formatDate should return the correct formatted date', function () {
                var now             = new Date(),
                    yesterday       = new Date(2016, 3, 30, 14, 0, 0),
                    prevMonth       = new Date(2016, 3, 1, 14, 0, 0),
                    prevYear        = new Date(2015, 4, 1, 14, 0, 0);

                expect(utils.formatDate(now.getTime())).toBe("2:00 PM");
                expect(utils.formatDate(yesterday.getTime())).toBe("30.Apr");
                expect(utils.formatDate(prevMonth.getTime())).toBe("01.Apr");
                expect(utils.formatDate(prevYear.getTime())).toBe("05/01/2015");
            });
        });

        describe('time tests (now=2016-05-31 14:00)', function () {

            useFakeClock(new Date(2016, 4, 31, 14, 0, 0));

            it('formatDate should return the correct formatted date', function () {
                var now             = new Date(),
                    yesterday       = new Date(2016, 4, 30, 14, 0, 0),
                    prevMonth       = new Date(2016, 3, 30, 14, 0, 0),
                    prevYear        = new Date(2015, 4, 31, 14, 0, 0);

                expect(utils.formatDate(now.getTime())).toBe("2:00 PM");
                expect(utils.formatDate(yesterday.getTime())).toBe("30.May");
                expect(utils.formatDate(prevMonth.getTime())).toBe("30.Apr");
                expect(utils.formatDate(prevYear.getTime())).toBe("05/31/2015");
            });
        });
    });
});
