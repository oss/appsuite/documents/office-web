/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import * as Position from '@/io.ox/office/textframework/utils/position';
import { CommentModel, CommentSettings } from '@/io.ox/office/text/model/comment/commentmodel';

import { createTextApp } from '~/text/apphelper';

// class CommentModel without filled comment collection ===================

describe('Text class CommentModel', function () {

    // private helpers ----------------------------------------------------

    var
        //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } }
        ],

        model = null,
        commentModel = null,
        commentSettings = null,

        commentId = 'cmt12340001',
        commentId_2 = 'cmt23450002',
        parentId = 'cmt5678',
        author = 'User A',
        userId = '1227',
        realUserId = 12,
        providerId = 'ox',
        date = '2020-08-20T09:50:00Z',
        text = 'Text in comment',
        mentions = { name: 'User B' },
        restorable = true,
        selectionState = { start: [1, 2], end: [1, 5] },
        target = 'target_1',
        newThread = false,
        done = false,
        placeHolderNode = null,

        newText = 'New text in the comment',
        newMentions = { name: 'User C' },
        newParentId = 'cmt5678',

        compareComment_1 = { getId: () => commentId },
        compareComment_2 = { getId: () => commentId_2 },

        createdJsonObject = {
            id: commentId,
            parentId,
            text,
            mentions,
            author,
            authorId: userId,
            authorProvider: providerId,
            date
        };

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // existence check ----------------------------------------------------

    it('should subclass ModelObject', function () {
        expect(CommentModel).toBeSubClassOf(ModelObject);
    });

    it('should contain class CommentSettings', function () {
        expect(CommentSettings).toBeClass();
    });

    // constructor --------------------------------------------------------

    describe('constructor for CommentSettings', function () {
        it('should create a CommentSettings class instance', function () {
            commentSettings = new CommentSettings(commentId, parentId, author, userId, providerId, date, text, mentions, restorable, selectionState, target, newThread, done, placeHolderNode);
            expect(commentSettings).toBeInstanceOf(CommentSettings);
        });
    });

    describe('constructor for CommentModel', function () {
        it('should create a CommentModel class instance', function () {
            commentModel = new CommentModel(model, commentSettings);
            expect(commentModel).toBeInstanceOf(CommentModel);
        });
    });

    // public methods -----------------------------------------------------

    describe('method getId', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getId');
        });
        it('should return the specified comment id', function () {
            expect(commentModel.getId()).toBe(commentId);
        });
    });

    describe('method getParentId', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getParentId');
        });
        it('should return the specified comment parent id', function () {
            expect(commentModel.getParentId()).toBe(parentId);
        });
    });

    describe('method setParentId', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('setParentId');
        });
        it('should set the specified comment parent id', function () {
            commentModel.setParentId(newParentId);
            expect(commentModel.getParentId()).toBe(newParentId);
            commentModel.setParentId(parentId);
            expect(commentModel.getParentId()).toBe(parentId);
        });
    });

    describe('method getText', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getText');
        });
        it('should return the specified comment text', function () {
            expect(commentModel.getText()).toBe(text);
        });
    });

    describe('method setText', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('setText');
        });
        it('should set the specified comment text', function () {
            commentModel.setText(newText);
            expect(commentModel.getText()).toBe(newText);
            commentModel.setText(text);
            expect(commentModel.getText()).toBe(text);
        });
    });

    describe('method getMentions', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getMentions');
        });
        it('should return the specified mentions object', function () {
            expect(commentModel.getMentions()).toEqual(mentions);
        });
    });

    describe('method setMentions', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getMentions');
        });
        it('should return the specified mentions object', function () {
            commentModel.setMentions(newMentions);
            expect(commentModel.getMentions()).toEqual(newMentions);
            commentModel.setMentions(mentions);
            expect(commentModel.getMentions()).toEqual(mentions);
        });
    });

    describe('method getDate', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getDate');
        });
        it('should return the specified date', function () {
            expect(commentModel.getDate()).toBe(date);
        });
    });

    describe('method getAuthor', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getAuthor');
        });
        it('should return the specified author', function () {
            expect(commentModel.getAuthor()).toBe(author);
        });
    });

    describe('method getAuthorProvider', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getAuthorProvider');
        });
        it('should return the specified author provider', function () {
            expect(commentModel.getAuthorProvider()).toBe(providerId);
        });
    });

    describe('method getAuthorId', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getAuthorId');
        });
        it('should return the specified author id', function () {
            expect(commentModel.getAuthorId()).toBe(userId);
        });
    });

    describe('method getInternalUserId', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getInternalUserId');
        });
        it('should return the calculated internal user id', function () {
            expect(commentModel.getInternalUserId()).toBe(realUserId);
        });
    });

    describe('method equals', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('equals');
        });
        it('should return that the comment model is equal with the specified comment model with the same comment id', function () {
            expect(commentModel.equals(compareComment_1)).toBeTrue();
        });
        it('should return that the comment model is equal with the specified comment model with a different comment id', function () {
            expect(commentModel.equals(compareComment_2)).toBeFalse();
        });
    });

    describe('method isReply', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('isReply');
        });
        it('should return that the comment model is a reply to another comment', function () {
            expect(commentModel.isReply()).toBeTrue();
        });
    });

    describe('method isRestorable', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('isReply');
        });
        it('should return that the comment model is a is restorable', function () {
            expect(commentModel.isRestorable()).toBeTrue();
        });
    });

    describe('method setRestorable', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('isReply');
        });
        it('should set a new state for isRestorable', function () {
            commentModel.setRestorable(false);
            expect(commentModel.isRestorable()).toBeFalse();
        });
    });

    describe('method isHidden', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('isHidden');
        });
        it('should return that the comment model is not hidden', function () {
            expect(commentModel.isHidden()).toBeFalse();
        });
    });

    describe('method setHiddenState', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('setHiddenState');
        });
        it('should set the specified hidden state', function () {
            commentModel.setHiddenState(true);
            expect(commentModel.isHidden()).toBeTrue();
            commentModel.setHiddenState(false);
            expect(commentModel.isHidden()).toBeFalse();
        });
    });

    describe('method isNewThread', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('isNewThread');
        });
        it('should return that the comment model is not a new thread, but a reply', function () {
            expect(commentModel.isNewThread()).toBeFalse();
        });
    });

    describe('method getSelectionState', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getSelectionState');
        });
        it('should return the specified selection state', function () {
            expect(commentModel.getSelectionState()).toEqual(selectionState);
        });
    });

    describe('method getCommentPlaceHolderNode', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getCommentPlaceHolderNode');
        });
        it('should return the not specified comment placeholder node', function () {
            expect(commentModel.getCommentPlaceHolderNode()).toBeNull();
        });
    });

    describe('method clone', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('clone');
        });
        it('should return a clone of the comment model instance', function () {
            var commentModelClone = commentModel.clone(model);
            expect(commentModelClone.getId()).toBe(commentId);
            expect(commentModelClone.getText()).toBe(text);
        });
    });

    describe('method getJSONSettings', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getJSONSettings');
        });
        it('should return a JSON object with data from the comment model instance', function () {
            expect(commentModel.getJSONSettings()).toEqual(createdJsonObject);
        });
    });

    describe('method isMarginal', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('isMarginal');
        });
        it('should return that the comment model belongs to a comment in header/footer', function () {
            expect(commentModel.isMarginal()).toBeTrue();
        });
    });

    describe('method getTarget', function () {
        it('should exist', function () {
            expect(commentModel).toRespondTo('getTarget');
        });
        it('should return the header/footer target of the comment model', function () {
            expect(commentModel.getTarget()).toBe(target);
        });
    });

});

// class CommentModel with filled comment collection ======================

describe('Text class CommentModel within a text document', function () {

    // private helpers ----------------------------------------------------

    var
        COMMENT_1_ID = 'cmt1234_1',
        COMMENT_2_ID = 'cmt1234_2',
        COMMENT_3_ID = 'cmt5678_3',

        COMMENT_1_DATE = '2020-08-20T09:05:00Z',
        COMMENT_2_DATE = '2020-08-20T09:06:00Z',
        COMMENT_3_DATE = '2020-08-20T09:07:00Z',

        COMMENT_1_TEXT = 'Text in comment 1',
        COMMENT_2_TEXT = 'Text in comment 2',
        COMMENT_3_TEXT = 'Text in comment 3',

        PARA_1_TEXT = 'This is the first paragraph!',
        PARA_2_TEXT = 'This is the second paragraph!',
        PARA_3_TEXT = 'This is the third paragraph!',
        PARA_4_TEXT = 'This is the fourth paragraph!',
        PARA_5_TEXT = 'This is the fifth paragraph!',

        PROVIDER_ID = 'ox',

        USER_ID_1 = '1234',
        USER_ID_2 = '5678',

        AUTHOR_1 = 'User A',
        AUTHOR_2 = 'User B',

        //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
            { name: 'insertParagraph', start: [0] },
            { name: 'insertText', start: [0, 0], text: PARA_1_TEXT },
            { name: 'insertParagraph', start: [1] },
            { name: 'insertText', start: [1, 0], text: PARA_2_TEXT },
            { name: 'insertParagraph', start: [2] },
            { name: 'insertText', start: [2, 0], text: PARA_3_TEXT },
            { name: 'insertParagraph', start: [3] },
            { name: 'insertText', start: [3, 0], text: PARA_4_TEXT },
            { name: 'insertParagraph', start: [4] },
            { name: 'insertText', start: [4, 0], text: PARA_5_TEXT },
            // inserting the first main comment
            { name: 'insertRange', start: [1, 12], position: 'start', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertRange', start: [1, 18], position: 'end', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertComment', id: COMMENT_1_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_1_DATE, start: [1, 19], text: COMMENT_1_TEXT },
            // inserting the first child comment
            { name: 'insertRange', start: [1, 13], position: 'start', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertRange', start: [1, 21], position: 'end', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertComment', id: COMMENT_2_ID, parentId: COMMENT_1_ID, author: AUTHOR_2, userId: USER_ID_2, providerId: PROVIDER_ID, date: COMMENT_2_DATE, start: [1, 22], text: COMMENT_2_TEXT },
            // inserting a second main comment
            { name: 'insertRange', start: [3, 12], position: 'start', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertRange', start: [3, 18], position: 'end', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertComment', id: COMMENT_3_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_3_DATE, start: [3, 19], text: COMMENT_3_TEXT }
        ],

        model = null,
        commentCollection = null,
        commentModel1 = null,
        commentModel2 = null,
        commentModel3 = null;

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        commentCollection = model.getCommentLayer().getCommentCollection();
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(CommentModel).toBeFunction();
    });

    // constructor --------------------------------------------------------

    describe('constructor for CommentModel', function () {
        it('should create a CommentModel class instance', function () {
            commentModel1 = commentCollection.getById(COMMENT_1_ID);
            commentModel2 = commentCollection.getById(COMMENT_2_ID);
            commentModel3 = commentCollection.getById(COMMENT_3_ID);
            expect(commentModel1).toBeInstanceOf(CommentModel);
            expect(commentModel2).toBeInstanceOf(CommentModel);
            expect(commentModel3).toBeInstanceOf(CommentModel);
        });
    });

    // public methods -----------------------------------------------------

    describe('method getId', function () {
        it('should return the specified comment id', function () {
            expect(commentModel1.getId()).toBe(COMMENT_1_ID);
            expect(commentModel2.getId()).toBe(COMMENT_2_ID);
            expect(commentModel3.getId()).toBe(COMMENT_3_ID);
        });
    });

    describe('method getParentId', function () {
        it('should return the specified comment parent id', function () {
            expect(commentModel1.getParentId()).toBe("");
            expect(commentModel2.getParentId()).toBe(COMMENT_1_ID);
            expect(commentModel3.getParentId()).toBe("");
        });
    });

    describe('method getText', function () {
        it('should return the specified comment text', function () {
            expect(commentModel1.getText()).toBe(COMMENT_1_TEXT);
            expect(commentModel2.getText()).toBe(COMMENT_2_TEXT);
            expect(commentModel3.getText()).toBe(COMMENT_3_TEXT);
        });
    });

    describe('method getMentions', function () {
        it('should return the specified mentions object', function () {
            expect(commentModel1.getMentions()).toBeNull();
            expect(commentModel2.getMentions()).toBeNull();
            expect(commentModel3.getMentions()).toBeNull();
        });
    });

    describe('method getDate', function () {
        it('should return the specified date', function () {
            expect(commentModel1.getDate()).toBe(COMMENT_1_DATE);
            expect(commentModel2.getDate()).toBe(COMMENT_2_DATE);
            expect(commentModel3.getDate()).toBe(COMMENT_3_DATE);
        });
    });

    describe('method getAuthor', function () {
        it('should return the specified author', function () {
            expect(commentModel1.getAuthor()).toBe(AUTHOR_1);
            expect(commentModel2.getAuthor()).toBe(AUTHOR_2);
            expect(commentModel3.getAuthor()).toBe(AUTHOR_1);
        });
    });

    describe('method getAuthorProvider', function () {
        it('should return the specified author provider', function () {
            expect(commentModel1.getAuthorProvider()).toBe(PROVIDER_ID);
            expect(commentModel2.getAuthorProvider()).toBe(PROVIDER_ID);
            expect(commentModel3.getAuthorProvider()).toBe(PROVIDER_ID);
        });
    });

    describe('method getAuthorId', function () {
        it('should return the specified author id', function () {
            expect(commentModel1.getAuthorId()).toBe(USER_ID_1);
            expect(commentModel2.getAuthorId()).toBe(USER_ID_2);
            expect(commentModel3.getAuthorId()).toBe(USER_ID_1);
        });
    });

    describe('method equals', function () {
        it('should return whether two comments are equal (have the same comment id)', function () {
            expect(commentModel1.equals(commentModel1)).toBeTrue();
            expect(commentModel1.equals(commentModel2)).toBeFalse();
            expect(commentModel1.equals(commentModel3)).toBeFalse();
            expect(commentModel2.equals(commentModel1)).toBeFalse();
            expect(commentModel2.equals(commentModel2)).toBeTrue();
            expect(commentModel2.equals(commentModel3)).toBeFalse();
            expect(commentModel3.equals(commentModel1)).toBeFalse();
            expect(commentModel3.equals(commentModel2)).toBeFalse();
            expect(commentModel3.equals(commentModel3)).toBeTrue();
        });
    });

    describe('method isReply', function () {
        it('should return that the comment model is a reply to another comment', function () {
            expect(commentModel1.isReply()).toBeFalse();
            expect(commentModel2.isReply()).toBeTrue();
            expect(commentModel3.isReply()).toBeFalse();
        });
    });

    describe('method isRestorable', function () {
        it('should return that the comment model is a is restorable', function () {
            expect(commentModel1.isRestorable()).toBeTrue();
            expect(commentModel2.isRestorable()).toBeTrue();
            expect(commentModel3.isRestorable()).toBeTrue();
        });
    });

    describe('method isHidden', function () {
        it('should return that the comment model is not hidden', function () {
            expect(commentModel1.isHidden()).toBeFalse();
            expect(commentModel2.isHidden()).toBeFalse();
            expect(commentModel3.isHidden()).toBeFalse();
        });
    });

    describe('method isMarginal', function () {
        it('should return that the comment model belongs to a comment in header/footer', function () {
            expect(commentModel1.isMarginal()).toBeFalse();
            expect(commentModel2.isMarginal()).toBeFalse();
            expect(commentModel3.isMarginal()).toBeFalse();
        });
    });

    describe('method getTarget', function () {
        it('should return the header/footer target of the comment model', function () {
            expect(commentModel1.getTarget()).toBe("");
            expect(commentModel2.getTarget()).toBe("");
            expect(commentModel3.getTarget()).toBe("");
        });
    });

});

// class CommentModel with temporary comment model ========================

describe('Text class CommentModel with temporary comment model', function () {

    // private helpers ----------------------------------------------------

    var
        COMMENT_1_ID = 'cmt1234_1',
        COMMENT_2_ID = 'cmt1234_2',
        COMMENT_3_ID = 'cmt5678_3',

        COMMENT_1_DATE = '2020-08-20T09:05:00Z',
        COMMENT_2_DATE = '2020-08-20T09:06:00Z',
        COMMENT_3_DATE = '2020-08-20T09:07:00Z',

        COMMENT_1_TEXT = 'Text in comment 1',
        COMMENT_2_TEXT = 'Text in comment 2',
        COMMENT_3_TEXT = 'Text in comment 3',

        PARA_1_TEXT = 'This is the first paragraph!',
        PARA_2_TEXT = 'This is the second paragraph!',
        PARA_3_TEXT = 'This is the third paragraph!',
        PARA_4_TEXT = 'This is the fourth paragraph!',
        PARA_5_TEXT = 'This is the fifth paragraph!',

        PROVIDER_ID = 'ox',

        USER_ID_1 = '1227',
        USER_ID_2 = '5678',

        AUTHOR_1 = 'User 1',
        AUTHOR_2 = 'User 2',

        selectionStartPosition = null,
        selectionEndPosition = null,

        //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
            { name: 'insertParagraph', start: [0] },
            { name: 'insertText', start: [0, 0], text: PARA_1_TEXT },
            { name: 'insertParagraph', start: [1] },
            { name: 'insertText', start: [1, 0], text: PARA_2_TEXT },
            { name: 'insertParagraph', start: [2] },
            { name: 'insertText', start: [2, 0], text: PARA_3_TEXT },
            { name: 'insertParagraph', start: [3] },
            { name: 'insertText', start: [3, 0], text: PARA_4_TEXT },
            { name: 'insertParagraph', start: [4] },
            { name: 'insertText', start: [4, 0], text: PARA_5_TEXT },
            // inserting the first main comment
            { name: 'insertRange', start: [1, 12], position: 'start', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertRange', start: [1, 18], position: 'end', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertComment', id: COMMENT_1_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_1_DATE, start: [1, 19], text: COMMENT_1_TEXT },
            // inserting the first child comment
            { name: 'insertRange', start: [1, 13], position: 'start', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertRange', start: [1, 21], position: 'end', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertComment', id: COMMENT_2_ID, parentId: COMMENT_1_ID, author: AUTHOR_2, userId: USER_ID_2, providerId: PROVIDER_ID, date: COMMENT_2_DATE, start: [1, 22], text: COMMENT_2_TEXT },
            // inserting a second main comment
            { name: 'insertRange', start: [3, 12], position: 'start', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertRange', start: [3, 18], position: 'end', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertComment', id: COMMENT_3_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_3_DATE, start: [3, 19], text: COMMENT_3_TEXT }
        ],

        model = null,
        commentCollection = null;

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        commentCollection = model.getCommentLayer().getCommentCollection();
    });

    // public methods -----------------------------------------------------

    describe('method commentCollection.startNewThread', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('startNewThread');
        });
        it('should start a new comment thread with a temporary comment model behind the first main comment', async function () {

            // setting a selection in the second paragraph behind the first main comment
            var secondParagraph = model.getNode().find('div.p')[1];
            var localPosition = 30; // the word paragraph should be around position 30
            var wordSelection = Position.getWordSelection(secondParagraph, localPosition);

            expect(wordSelection.text).toBe("paragraph");

            selectionStartPosition = [1, wordSelection.start];
            selectionEndPosition = [1, wordSelection.end];

            // setting a selection -> this determines the position in the sorted container of comment models
            model.getSelection().setTextSelection(selectionStartPosition, selectionEndPosition); // selecting the word 'paragraph' in the second paragraph

            expect(model.getSelection().getSelectedText()).toBe("paragraph");

            await commentCollection.startNewThread();
            expect(commentCollection.getCommentNumber()).toBe(4);
        });
    });

    describe('method commentCollection.getTemporaryModel', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getTemporaryModel');
        });
        let temporaryCommentModel = null;
        it('should return the temporary comment model', function () {
            temporaryCommentModel = commentCollection.getTemporaryModel();
            expect(temporaryCommentModel).toBeInstanceOf(CommentModel);
        });
        it('should return the ID of the temporary comment model', function () {
            expect(temporaryCommentModel.getId()).toBe(commentCollection.getTemporaryCommentId());
        });
        it('should return the specified comment parent id', function () {
            expect(temporaryCommentModel.getParentId()).toBe("");
        });
        it('should return the specified comment text', function () {
            expect(temporaryCommentModel.getText()).toBe("");
        });
        it('should return the specified mentions object', function () {
            expect(temporaryCommentModel.getMentions()).toBeNull();
        });
        it('should return the specified date', function () {
            expect(temporaryCommentModel.getDate()).toBe("");
        });
        it('should return the specified author', function () {
            expect(temporaryCommentModel.getAuthor()).toBe(AUTHOR_1);
        });
        it('should return the specified author provider', function () {
            expect(temporaryCommentModel.getAuthorProvider()).toBe(PROVIDER_ID);
        });
        it('should return the specified author id', function () {
            expect(temporaryCommentModel.getAuthorId()).toBe(USER_ID_1);
        });
        it('should return that the comment model is a reply to another comment', function () {
            expect(temporaryCommentModel.isReply()).toBeFalse();
        });
        it('should return that the comment model is a is restorable', function () {
            expect(temporaryCommentModel.isRestorable()).toBeTrue();
        });
        it('should return that the comment model is not hidden', function () {
            expect(temporaryCommentModel.isHidden()).toBeFalse();
        });
        it('should return that the comment model belongs to a comment in header/footer', function () {
            expect(temporaryCommentModel.isMarginal()).toBeFalse();
        });
        it('should return the header/footer target of the comment model', function () {
            expect(temporaryCommentModel.getTarget()).toBe("");
        });
        it('should return the not specified comment placeholder node', function () {
            expect(temporaryCommentModel.getCommentPlaceHolderNode()).toHaveLength(0); // always a jQuery object
        });
    });
});
