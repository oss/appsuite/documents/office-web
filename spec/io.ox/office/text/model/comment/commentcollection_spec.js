/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import * as Position from '@/io.ox/office/textframework/utils/position';
import CommentCollection from '@/io.ox/office/text/model/comment/commentcollection';

import { createTextApp } from '~/text/apphelper';

// class CommentCollection ================================================

describe('Text class CommentCollection', function () {

    // private helpers ----------------------------------------------------

    var
        COMMENT_1_ID = 'cmt12340001',
        COMMENT_2_ID = 'cmt12340002',
        COMMENT_3_ID = 'cmt56780003',
        COMMENT_4_ID = 'cmt12340004',

        COMMENT_1_DATE = '2020-08-20T09:05:00Z',
        COMMENT_2_DATE = '2020-08-20T09:06:00Z',
        COMMENT_3_DATE = '2020-08-20T09:07:00Z',

        COMMENT_INVALID_DATE = '2020-08-20T09:01:00Z',

        COMMENT_1_TEXT = 'Text in comment 1',
        COMMENT_2_TEXT = 'Text in comment 2',
        COMMENT_3_TEXT = 'Text in comment 3',
        COMMENT_4_TEXT = 'Text in comment 4',
        COMMENT_5_TEXT = 'Text in comment 5',
        COMMENT_6_TEXT = 'Text in comment 6',

        PARA_1_TEXT = 'This is the first paragraph!',
        PARA_2_TEXT = 'This is the second paragraph!',
        PARA_3_TEXT = 'This is the third paragraph!',
        PARA_4_TEXT = 'This is the fourth paragraph!',
        PARA_5_TEXT = 'This is the fifth paragraph!',

        COMMENT_2_TEXT_CHANGED = 'Text in the child comment 2',

        PROVIDER_ID = 'ox',

        USER_ID_1 = '1234',
        USER_ID_2 = '5678',

        AUTHOR_1 = 'User A',
        AUTHOR_2 = 'User B',

        AUTHOR_FILTER_1 = [AUTHOR_1], // only showing comments from AUTHOR_1

        //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
            { name: 'insertParagraph', start: [0] },
            { name: 'insertText', start: [0, 0], text: PARA_1_TEXT },
            { name: 'insertParagraph', start: [1] },
            { name: 'insertText', start: [1, 0], text: PARA_2_TEXT },
            { name: 'insertParagraph', start: [2] },
            { name: 'insertText', start: [2, 0], text: PARA_3_TEXT },
            { name: 'insertParagraph', start: [3] },
            { name: 'insertText', start: [3, 0], text: PARA_4_TEXT },
            { name: 'insertParagraph', start: [4] },
            { name: 'insertText', start: [4, 0], text: PARA_5_TEXT },
            // inserting the first main comment
            { name: 'insertRange', start: [1, 12], position: 'start', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertRange', start: [1, 18], position: 'end', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertComment', id: COMMENT_1_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_1_DATE, start: [1, 19], text: COMMENT_1_TEXT },
            // inserting the first child comment
            { name: 'insertRange', start: [1, 13], position: 'start', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertRange', start: [1, 21], position: 'end', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertComment', id: COMMENT_2_ID, parentId: COMMENT_1_ID, author: AUTHOR_2, userId: USER_ID_2, providerId: PROVIDER_ID, date: COMMENT_2_DATE, start: [1, 22], text: COMMENT_2_TEXT },
            // inserting a second main comment
            { name: 'insertRange', start: [3, 12], position: 'start', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertRange', start: [3, 18], position: 'end', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertComment', id: COMMENT_3_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_3_DATE, start: [3, 19], text: COMMENT_3_TEXT }
        ],

        model = null,
        commentCollection = null;

    createTextApp('ooxml', OPERATIONS, { realView: true }).done(function (app) {
        model = app.getModel();
        commentCollection = model.getCommentLayer().getCommentCollection();
    });

    // existence check ----------------------------------------------------

    it('should subclass ModelObject', function () {
        expect(CommentCollection).toBeSubClassOf(ModelObject);
    });

    // constructor --------------------------------------------------------
    describe('constructor', function () {
        it('should create a CommentCollection class instance', function () {
            expect(commentCollection).toBeInstanceOf(CommentCollection);
        });
    });

    // public methods -----------------------------------------------------

    describe('method isEmpty', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('isEmpty');
        });
        it('should return that the comment collection is not empty', function () {
            expect(commentCollection.isEmpty()).toBeFalse();
        });
    });

    describe('method getCommentNumber', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getCommentNumber');
        });
        it('should return that the comment collection contains two comments', function () {
            expect(commentCollection.getCommentNumber()).toBe(3);
        });
    });

    describe('method getVisibleCommentNumber', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getVisibleCommentNumber');
        });
        it('should return that the comment collection contains two visible comments', function () {
            expect(commentCollection.getVisibleCommentNumber()).toBe(3);
        });
    });

    describe('method getById', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getById');
        });
        it('should return that the comment with the first ID exists in the collection', function () {
            var commentModel = commentCollection.getById(COMMENT_1_ID);
            expect(commentModel).not.toBeNull();
            expect(commentModel.getId()).toBe(COMMENT_1_ID);
        });
        it('should return that the comment with the second ID exists in the collection', function () {
            var commentModel = commentCollection.getById(COMMENT_2_ID);
            expect(commentModel).not.toBeNull();
            expect(commentModel.getId()).toBe(COMMENT_2_ID);
        });
        it('should return that the comment with the third ID exists in the collection', function () {
            var commentModel = commentCollection.getById(COMMENT_3_ID);
            expect(commentModel).not.toBeNull();
            expect(commentModel.getId()).toBe(COMMENT_3_ID);
        });
        it('should return that the comment with the fourth ID does not exist in the collection', function () {
            var commentModel = commentCollection.getById(COMMENT_4_ID);
            expect(commentModel).toBeNull();
        });
    });

    describe('method getByIndex', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getByIndex');
        });
        it('should return that the main comment that was inserted in the second paragraph is the first in the collection', function () {
            var commentModel = commentCollection.getByIndex(0);
            expect(commentModel).not.toBeNull();
            expect(commentModel.getId()).toBe(COMMENT_1_ID);
        });
        it('should return that the child comment that was inserted in the second paragraph is the second in the collection', function () {
            var commentModel = commentCollection.getByIndex(1);
            expect(commentModel).not.toBeNull();
            expect(commentModel.getId()).toBe(COMMENT_2_ID);
        });
        it('should return that the main comment that was inserted in the fourth paragraph is the third in the collection', function () {
            var commentModel = commentCollection.getByIndex(2);
            expect(commentModel).not.toBeNull();
            expect(commentModel.getId()).toBe(COMMENT_3_ID);
        });
        it('should return null for an invalid index', function () {
            var commentModel = commentCollection.getByIndex(3);
            expect(commentModel).toBeNull();
        });
    });

    describe('method getByDateId', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getByDateId');
        });
        it('should return the comment model of that comment that was inserted at the specified time', function () {
            var commentModel = commentCollection.getByDateId(new Date(COMMENT_1_DATE).getTime());
            expect(commentModel).not.toBeNull();
            expect(commentModel.getId()).toBe(COMMENT_1_ID);
        });
        it('should return null for an invalid date', function () {
            var commentModel = commentCollection.getByDateId(new Date(COMMENT_INVALID_DATE).getTime());
            expect(commentModel).toBeNull();
        });
    });

    describe('method getThreadById', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getThreadById');
        });
        it('should return an array of comment model of the first main comment', function () {
            var commentThread = commentCollection.getThreadById(COMMENT_1_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(2);
            expect(commentThread[0].getId()).toBe(COMMENT_1_ID);
            expect(commentThread[1].getId()).toBe(COMMENT_2_ID);
        });
        it('should return an array with one comment model of the child comment', function () {
            var commentThread = commentCollection.getThreadById(COMMENT_2_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(1);
            expect(commentThread[0].getId()).toBe(COMMENT_2_ID);
        });
        it('should return an array with one comment model of the second main comment', function () {
            var commentThread = commentCollection.getThreadById(COMMENT_3_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(1);
            expect(commentThread[0].getId()).toBe(COMMENT_3_ID);
        });
        it('should return an empty array for an invalid comment Id', function () {
            var commentThread = commentCollection.getThreadById(COMMENT_4_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(0);
        });
    });

    describe('method getChildrenById', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getChildrenById');
        });
        it('should return an array with one comment model for the first main comment', function () {
            var commentThread = commentCollection.getChildrenById(COMMENT_1_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(1);
            expect(commentThread[0].getId()).toBe(COMMENT_2_ID);
        });
        it('should return an empty array for the child comment', function () {
            var commentThread = commentCollection.getChildrenById(COMMENT_2_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(0);
        });
        it('should return an array for the second main comment', function () {
            var commentThread = commentCollection.getChildrenById(COMMENT_3_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(0);
        });
        it('should return an empty array for an invalid comment Id', function () {
            var commentThread = commentCollection.getChildrenById(COMMENT_4_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(0);
        });
    });

    describe('method getNeighbourCommentModelById', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getNeighbourCommentModelById');
        });
        it('should return the child comment for the first main comment', function () {
            var commentModel = commentCollection.getNeighbourCommentModelById(COMMENT_1_ID).commentModel;
            expect(commentModel.getId()).toBe(COMMENT_2_ID);
        });
        it('should return the second main comment for the child comment', function () {
            var commentModel = commentCollection.getNeighbourCommentModelById(COMMENT_2_ID).commentModel;
            expect(commentModel.getId()).toBe(COMMENT_3_ID);
        });
        it('should return the first main comment for the second comment', function () {
            var commentModel = commentCollection.getNeighbourCommentModelById(COMMENT_3_ID).commentModel;
            expect(commentModel.getId()).toBe(COMMENT_1_ID);
        });
        it('should return the second main comment for the first main comment, if not next is specified', function () {
            var commentModel = commentCollection.getNeighbourCommentModelById(COMMENT_1_ID, { next: false }).commentModel;
            expect(commentModel.getId()).toBe(COMMENT_3_ID);
        });
        it('should return the firt main comment for the child comment, if not next is specified', function () {
            var commentModel = commentCollection.getNeighbourCommentModelById(COMMENT_2_ID, { next: false }).commentModel;
            expect(commentModel.getId()).toBe(COMMENT_1_ID);
        });
        it('should return the child comment for the second comment, if not next is specified', function () {
            var commentModel = commentCollection.getNeighbourCommentModelById(COMMENT_3_ID, { next: false }).commentModel;
            expect(commentModel.getId()).toBe(COMMENT_2_ID);
        });
        it('should return the second main comment for the first main comment, if only thread are searched', function () {
            var commentModel = commentCollection.getNeighbourCommentModelById(COMMENT_1_ID, { onlyThreads: true }).commentModel;
            expect(commentModel.getId()).toBe(COMMENT_3_ID);
        });
        it('should return the second main comment for the child comment, if only thread are searched', function () {
            var commentModel = commentCollection.getNeighbourCommentModelById(COMMENT_2_ID, { onlyThreads: true }).commentModel;
            expect(commentModel.getId()).toBe(COMMENT_3_ID);
        });
        it('should return the first main comment for the second main comment, if only thread are searched', function () {
            var commentModel = commentCollection.getNeighbourCommentModelById(COMMENT_3_ID, { onlyThreads: true }).commentModel;
            expect(commentModel.getId()).toBe(COMMENT_1_ID);
        });
        it('should return null for an invalid comment id', function () {
            var commentModel = commentCollection.getNeighbourCommentModelById(COMMENT_4_ID).commentModel;
            expect(commentModel).toBeNull();
        });
    });

    describe('method isTemporaryCommentId', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('isTemporaryCommentId');
        });
        it('should be false for the first main comment', function () {
            expect(commentCollection.isTemporaryCommentId(COMMENT_1_ID)).toBeFalse();
        });
        it('should be false for the child comment', function () {
            expect(commentCollection.isTemporaryCommentId(COMMENT_2_ID)).toBeFalse();
        });
        it('should be false for the second main comment', function () {
            expect(commentCollection.isTemporaryCommentId(COMMENT_1_ID)).toBeFalse();
        });
        it('should be true for the temporary comment id', function () {
            expect(commentCollection.isTemporaryCommentId(commentCollection.getTemporaryCommentId())).toBeTrue();
        });
    });

    describe('method getThreadIdForCommentId', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getThreadIdForCommentId');
        });
        it('should return the id of the first main comment for the first main comment', function () {
            expect(commentCollection.getThreadIdForCommentId(COMMENT_1_ID)).toBe(COMMENT_1_ID);
        });
        it('should return the id of the first main comment for the child comment', function () {
            expect(commentCollection.getThreadIdForCommentId(COMMENT_2_ID)).toBe(COMMENT_1_ID);
        });
        it('should return the id of the second main comment for the second main comment', function () {
            expect(commentCollection.getThreadIdForCommentId(COMMENT_3_ID)).toBe(COMMENT_3_ID);
        });
        it('should return an empty string for an invalid comment Id', function () {
            expect(commentCollection.getThreadIdForCommentId(COMMENT_4_ID)).toBe("");
        });
    });

    describe('method getCommentsAuthorList', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getCommentsAuthorList');
        });
        it('should have two authors in the list of comment authors', function () {
            var authorsList = commentCollection.getCommentsAuthorList();
            expect(authorsList).toBeArray();
            expect(authorsList).toHaveLength(2);
            expect(authorsList[0]).toBe(AUTHOR_1);
            expect(authorsList[1]).toBe(AUTHOR_2);
        });
    });

    describe('method getCommentAuthorCount', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getCommentAuthorCount');
        });
        it('should return 2 for the number of authors in the list of comment authors', function () {
            expect(commentCollection.getCommentAuthorCount()).toBe(2);
        });
    });

    describe('method hasReply', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('hasReply');
        });
        it('should be true for the first main comment', function () {
            expect(commentCollection.hasReply(commentCollection.getById(COMMENT_1_ID))).toBeTrue();
        });
        it('should be false for the child comment', function () {
            expect(commentCollection.hasReply(commentCollection.getById(COMMENT_2_ID))).toBeFalse();
        });
        it('should be false for the second main comment', function () {
            expect(commentCollection.hasReply(commentCollection.getById(COMMENT_3_ID))).toBeFalse();
        });
        it('should be false for an invalid comment id', function () {
            expect(commentCollection.hasReply(commentCollection.getById(COMMENT_4_ID))).toBeFalse();
        });
    });

    describe('method hasChild', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('hasChild');
        });
        it('should be true for the first main comment', function () {
            expect(commentCollection.hasChild(commentCollection.getById(COMMENT_1_ID))).toBeTrue();
        });
        it('should be false for the child comment', function () {
            expect(commentCollection.hasChild(commentCollection.getById(COMMENT_2_ID))).toBeFalse();
        });
        it('should be false for the second main comment', function () {
            expect(commentCollection.hasChild(commentCollection.getById(COMMENT_3_ID))).toBeFalse();
        });
        it('should be false for an invalid comment id', function () {
            expect(commentCollection.hasChild(commentCollection.getById(COMMENT_4_ID))).toBeFalse();
        });
    });

    describe('method hasVisibleChild', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('hasVisibleChild');
        });
        it('should be true for the first main comment', function () {
            expect(commentCollection.hasVisibleChild(commentCollection.getById(COMMENT_1_ID))).toBeTrue();
        });
        it('should be false for the child comment', function () {
            expect(commentCollection.hasVisibleChild(commentCollection.getById(COMMENT_2_ID))).toBeFalse();
        });
        it('should be false for the second main comment', function () {
            expect(commentCollection.hasVisibleChild(commentCollection.getById(COMMENT_3_ID))).toBeFalse();
        });
        it('should be false for an invalid comment id', function () {
            expect(commentCollection.hasVisibleChild(commentCollection.getById(COMMENT_4_ID))).toBeFalse();
        });
    });

    describe('method hasHiddenChild', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('hasHiddenChild');
        });
        it('should be false for the first main comment', function () {
            expect(commentCollection.hasHiddenChild(commentCollection.getById(COMMENT_1_ID))).toBeFalse();
        });
        it('should be false for the child comment', function () {
            expect(commentCollection.hasHiddenChild(commentCollection.getById(COMMENT_2_ID))).toBeFalse();
        });
        it('should be false for the second main comment', function () {
            expect(commentCollection.hasHiddenChild(commentCollection.getById(COMMENT_3_ID))).toBeFalse();
        });
        it('should be false for an invalid comment id', function () {
            expect(commentCollection.hasHiddenChild(commentCollection.getById(COMMENT_4_ID))).toBeFalse();
        });
    });

    describe('method containsMarginalComments', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('containsMarginalComments');
        });
        it('should be false for this collection', function () {
            expect(commentCollection.containsMarginalComments()).toBeFalse();
        });
    });

    describe('method containsTemporaryModel', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('containsTemporaryModel');
        });
        it('should be false for this collection', function () {
            expect(commentCollection.containsTemporaryModel()).toBeFalse();
        });
    });

    describe('method getTemporaryModel', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('getTemporaryModel');
        });
        it('should return null for this collection', function () {
            expect(commentCollection.getTemporaryModel()).toBeNull();
        });
    });

    describe('method applyAuthorFilter', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('applyAuthorFilter');
        });
        it('should still have 2 authors after applying an author filter', function () {
            commentCollection.applyAuthorFilter(AUTHOR_FILTER_1); // applying an author filter
            expect(commentCollection.getCommentAuthorCount()).toBe(2);
        });
        it('should still have a child at the first main comment', function () {
            expect(commentCollection.hasChild(commentCollection.getById(COMMENT_1_ID))).toBeTrue();
        });
        it('should no longer have a visible child at the first main comment', function () {
            expect(commentCollection.hasVisibleChild(commentCollection.getById(COMMENT_1_ID))).toBeFalse();
        });
        it('should show a hidden child at the first main comment', function () {
            expect(commentCollection.hasHiddenChild(commentCollection.getById(COMMENT_1_ID))).toBeTrue();
        });
        it('should return the second main comment for the first main comment, if onlyVisible is specified', function () {
            expect(commentCollection.getNeighbourCommentModelById(COMMENT_1_ID, { onlyVisible: true }).commentModel.getId()).toBe(COMMENT_3_ID);
        });
        it('should return the child comment for the first main comment, if onlyVisible is not specified', function () {
            expect(commentCollection.getNeighbourCommentModelById(COMMENT_1_ID).commentModel.getId()).toBe(COMMENT_2_ID);
        });
        it('should still have 2 authors after removing an author filter', function () {
            commentCollection.applyAuthorFilter(); // removing an author filter
            expect(commentCollection.getCommentAuthorCount()).toBe(2);
        });
        it('should still have a child at the first main comment after removing the author filter', function () {
            expect(commentCollection.hasChild(commentCollection.getById(COMMENT_1_ID))).toBeTrue();
        });
        it('should have a visible child at the first main comment after removing the author filter', function () {
            expect(commentCollection.hasVisibleChild(commentCollection.getById(COMMENT_1_ID))).toBeTrue();
        });
        it('should no longer have a hidden child at the first main comment after removing the author filter', function () {
            expect(commentCollection.hasHiddenChild(commentCollection.getById(COMMENT_1_ID))).toBeFalse();
        });
        it('should return the child comment for the first main comment after removing the author filter, if onlyVisible is specified', function () {
            expect(commentCollection.getNeighbourCommentModelById(COMMENT_1_ID, { onlyVisible: true }).commentModel.getId()).toBe(COMMENT_2_ID);
        });
        it('should return the child comment for the first main comment after removing the author filter, if onlyVisible is not specified', function () {
            expect(commentCollection.getNeighbourCommentModelById(COMMENT_1_ID).commentModel.getId()).toBe(COMMENT_2_ID);
        });
    });

    describe('method iterateCommentModels', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('iterateCommentModels');
        });
        it('should iterate over all three comments, if no restriction is specified', function () {
            var collector = [];
            commentCollection.iterateCommentModels(m => collector.push(m));
            expect(collector).toHaveLength(3);
            expect(collector[0].getId()).toBe(COMMENT_1_ID);
            expect(collector[1].getId()).toBe(COMMENT_2_ID);
            expect(collector[2].getId()).toBe(COMMENT_3_ID);
        });
        it('should iterate only over the two main comments, if specified', function () {
            var collector = [];
            commentCollection.iterateCommentModels(m => collector.push(m), null, { onlyThreads: true });
            expect(collector).toHaveLength(2);
            expect(collector[0].getId()).toBe(COMMENT_1_ID);
            expect(collector[1].getId()).toBe(COMMENT_3_ID);
        });
    });

    // modifying the commentCollection

    describe('method changeThread', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('changeThread');
        });
        it('should change the text of the specified comment model', function () {
            var commentModel = commentCollection.getById(COMMENT_2_ID);
            var commentInfo = { text: COMMENT_2_TEXT_CHANGED, mentions: undefined };
            expect(commentModel.getText()).toBe(COMMENT_2_TEXT);
            commentCollection.changeThread(commentModel, commentInfo);
            expect(commentModel.getText()).toBe(COMMENT_2_TEXT_CHANGED);
        });
        it('should undo the text change correctly', async function () {
            var commentModel = commentCollection.getById(COMMENT_2_ID);
            expect(commentModel.getText()).toBe(COMMENT_2_TEXT_CHANGED);
            await model.getUndoManager().undo();
            expect(commentModel.getText()).toBe(COMMENT_2_TEXT);
        });
    });

    describe('method insertThread', function () {

        it('should exist', function () {
            expect(commentCollection).toRespondTo('insertThread');
        });

        let COMMENT_ID_INSERT_EXPECTED_1;
        let COMMENT_ID_INSERT_EXPECTED_2;
        let COMMENT_ID_INSERT_EXPECTED_3;

        it('should add a further child comment to the first main comment', async function () {
            var commentInfo = { text: COMMENT_4_TEXT, mentions: undefined };
            const newCommentId = await commentCollection.insertThread(COMMENT_1_ID, commentInfo);
            COMMENT_ID_INSERT_EXPECTED_1 = newCommentId;
            expect(commentCollection.getCommentNumber()).toBe(4);
        });
        it('should have a new child comment with the expected ID', function () {
            var commentModel = commentCollection.getById(COMMENT_ID_INSERT_EXPECTED_1);
            expect(commentModel).not.toBeNull();
            expect(commentModel.getText()).toBe(COMMENT_4_TEXT);
        });
        it('should have two children at the first main comment', function () {
            var commentThread = commentCollection.getChildrenById(COMMENT_1_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(2);
            expect(commentThread[0].getId()).toBe(COMMENT_2_ID);
            expect(commentThread[1].getId()).toBe(COMMENT_ID_INSERT_EXPECTED_1);
        });
        it('should have inserted the new child comment at the third position in the comment model collector', function () {
            expect(commentCollection.getByIndex(0).getId()).toBe(COMMENT_1_ID);
            expect(commentCollection.getByIndex(1).getId()).toBe(COMMENT_2_ID);
            expect(commentCollection.getByIndex(2).getId()).toBe(COMMENT_ID_INSERT_EXPECTED_1);
            expect(commentCollection.getByIndex(3).getId()).toBe(COMMENT_3_ID);
            expect(commentCollection.getByIndex(4)).toBeNull();
        });

        it('should remove the inserted child comment via undo correctly', async function () {
            await model.getUndoManager().undo();
            expect(commentCollection.getCommentNumber()).toBe(3);
        });
        it('should have one child at the first main comment', function () {
            var commentThread = commentCollection.getChildrenById(COMMENT_1_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(1);
            expect(commentThread[0].getId()).toBe(COMMENT_2_ID);
        });
        it('should have removed the new child comment from the third position in the comment model collector', function () {
            expect(commentCollection.getByIndex(0).getId()).toBe(COMMENT_1_ID);
            expect(commentCollection.getByIndex(1).getId()).toBe(COMMENT_2_ID);
            expect(commentCollection.getByIndex(2).getId()).toBe(COMMENT_3_ID);
            expect(commentCollection.getByIndex(3)).toBeNull();
        });

        it('should add a new main comment behind the first main comment', async function () {

            var secondParagraph = model.getNode().find('div.p')[1];
            var localPosition = 30; // the word paragraph should be around position 30
            var wordSelection = Position.getWordSelection(secondParagraph, localPosition);

            expect(wordSelection.text).toBe("paragraph");

            // setting a selection -> this determines the position in the sorted container of comment models
            model.getSelection().setTextSelection([1, wordSelection.start], [1, wordSelection.end]); // selecting the word 'paragraph' in the second paragraph

            expect(model.getSelection().getSelectedText()).toBe("paragraph");

            var commentInfo = { text: COMMENT_5_TEXT, mentions: undefined };

            const newCommentId = await commentCollection.insertThread(null, commentInfo);
            COMMENT_ID_INSERT_EXPECTED_2 = newCommentId;
            expect(commentCollection.getCommentNumber()).toBe(4);
        });
        it('should have a new main comment with the expected ID', function () {
            var commentModel = commentCollection.getById(COMMENT_ID_INSERT_EXPECTED_2);
            expect(commentModel).not.toBeNull();
            expect(commentModel.getText()).toBe(COMMENT_5_TEXT);
        });
        it('should have still one child at the first main comment', function () {
            var commentThread = commentCollection.getChildrenById(COMMENT_1_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(1);
            expect(commentThread[0].getId()).toBe(COMMENT_2_ID);
        });
        it('should have inserted the new main comment at the third position in the comment model collector', function () {
            expect(commentCollection.getByIndex(0).getId()).toBe(COMMENT_1_ID);
            expect(commentCollection.getByIndex(1).getId()).toBe(COMMENT_2_ID);
            expect(commentCollection.getByIndex(2).getId()).toBe(COMMENT_ID_INSERT_EXPECTED_2);
            expect(commentCollection.getByIndex(3).getId()).toBe(COMMENT_3_ID);
            expect(commentCollection.getByIndex(4)).toBeNull();
        });

        it('should remove the inserted main comment via undo correctly', async function () {
            await model.getUndoManager().undo();
            expect(commentCollection.getCommentNumber()).toBe(3);
        });
        it('should have no longer a new main comment with the expected ID', function () {
            var commentModel = commentCollection.getById(COMMENT_ID_INSERT_EXPECTED_2);
            expect(commentModel).toBeNull();
        });
        it('should have no longer inserted the new main comment at the third position in the comment model collector', function () {
            expect(commentCollection.getByIndex(0).getId()).toBe(COMMENT_1_ID);
            expect(commentCollection.getByIndex(1).getId()).toBe(COMMENT_2_ID);
            expect(commentCollection.getByIndex(2).getId()).toBe(COMMENT_3_ID);
            expect(commentCollection.getByIndex(3)).toBeNull();
        });

        it('should add a new main comment before the first main comment', async function () {

            var secondParagraph = model.getNode().find('div.p')[1];
            var localPosition = 1; // the word 'This' should be around position 1
            var wordSelection = Position.getWordSelection(secondParagraph, localPosition);

            expect(wordSelection.text).toBe("This");

            // setting a selection -> this determines the position in the sorted container of comment models
            model.getSelection().setTextSelection([1, wordSelection.start], [1, wordSelection.end]); // selecting the word 'This' in the second paragraph

            expect(model.getSelection().getSelectedText()).toBe("This");

            var commentInfo = { text: COMMENT_6_TEXT, mentions: undefined };

            const newCommentId = await commentCollection.insertThread(null, commentInfo);
            COMMENT_ID_INSERT_EXPECTED_3 = newCommentId;
            expect(commentCollection.getCommentNumber()).toBe(4);
        });
        it('should also have a new main comment with the expected ID', function () {
            var commentModel = commentCollection.getById(COMMENT_ID_INSERT_EXPECTED_3);
            expect(commentModel).not.toBeNull();
            expect(commentModel.getText()).toBe(COMMENT_6_TEXT);
        });
        it('should have still one child at the first (now second) main comment', function () {
            var commentThread = commentCollection.getChildrenById(COMMENT_1_ID);
            expect(commentThread).toBeArray();
            expect(commentThread).toHaveLength(1);
            expect(commentThread[0].getId()).toBe(COMMENT_2_ID);
        });
        it('should have inserted the new main comment at the first position in the comment model collector', function () {
            expect(commentCollection.getByIndex(0).getId()).toBe(COMMENT_ID_INSERT_EXPECTED_3);
            expect(commentCollection.getByIndex(1).getId()).toBe(COMMENT_1_ID);
            expect(commentCollection.getByIndex(2).getId()).toBe(COMMENT_2_ID);
            expect(commentCollection.getByIndex(3).getId()).toBe(COMMENT_3_ID);
            expect(commentCollection.getByIndex(4)).toBeNull();
        });

        it('should remove the inserted main comment at first position via undo correctly', async function () {
            await model.getUndoManager().undo();
            expect(commentCollection.getCommentNumber()).toBe(3);
        });
        it('should have no longer a new main comment with the expected ID at the first position', function () {
            var commentModel = commentCollection.getById(COMMENT_ID_INSERT_EXPECTED_3);
            expect(commentModel).toBeNull();
        });
        it('should have no longer inserted the new main comment at the first position in the comment model collector', function () {
            expect(commentCollection.getByIndex(0).getId()).toBe(COMMENT_1_ID);
            expect(commentCollection.getByIndex(1).getId()).toBe(COMMENT_2_ID);
            expect(commentCollection.getByIndex(2).getId()).toBe(COMMENT_3_ID);
            expect(commentCollection.getByIndex(3)).toBeNull();
        });

    });

});

// using a new test scenario for the temporary comment model, because the changes started
// by startNewThread cannot be undone so easily. This means, that the DOM structure is
// modified irreversible.

describe('Text class CommentCollection with temporary comment model', function () {

    // private helpers ----------------------------------------------------

    var
        COMMENT_1_ID = 'cmt12340001',
        COMMENT_2_ID = 'cmt12340002',
        COMMENT_3_ID = 'cmt56780003',

        COMMENT_1_DATE = '2020-08-20T09:05:00Z',
        COMMENT_2_DATE = '2020-08-20T09:06:00Z',
        COMMENT_3_DATE = '2020-08-20T09:07:00Z',

        COMMENT_1_TEXT = 'Text in comment 1',
        COMMENT_2_TEXT = 'Text in comment 2',
        COMMENT_3_TEXT = 'Text in comment 3',

        PARA_1_TEXT = 'This is the first paragraph!',
        PARA_2_TEXT = 'This is the second paragraph!',
        PARA_3_TEXT = 'This is the third paragraph!',
        PARA_4_TEXT = 'This is the fourth paragraph!',
        PARA_5_TEXT = 'This is the fifth paragraph!',

        PROVIDER_ID = 'ox',

        USER_ID_1 = '1234',
        USER_ID_2 = '5678',

        AUTHOR_1 = 'User A',
        AUTHOR_2 = 'User B',

        selectionState = null,
        selectionStartPosition = null,
        selectionEndPosition = null,

        //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
            { name: 'insertParagraph', start: [0] },
            { name: 'insertText', start: [0, 0], text: PARA_1_TEXT },
            { name: 'insertParagraph', start: [1] },
            { name: 'insertText', start: [1, 0], text: PARA_2_TEXT },
            { name: 'insertParagraph', start: [2] },
            { name: 'insertText', start: [2, 0], text: PARA_3_TEXT },
            { name: 'insertParagraph', start: [3] },
            { name: 'insertText', start: [3, 0], text: PARA_4_TEXT },
            { name: 'insertParagraph', start: [4] },
            { name: 'insertText', start: [4, 0], text: PARA_5_TEXT },
            // inserting the first main comment
            { name: 'insertRange', start: [1, 12], position: 'start', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertRange', start: [1, 18], position: 'end', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertComment', id: COMMENT_1_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_1_DATE, start: [1, 19], text: COMMENT_1_TEXT },
            // inserting the first child comment
            { name: 'insertRange', start: [1, 13], position: 'start', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertRange', start: [1, 21], position: 'end', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertComment', id: COMMENT_2_ID, parentId: COMMENT_1_ID, author: AUTHOR_2, userId: USER_ID_2, providerId: PROVIDER_ID, date: COMMENT_2_DATE, start: [1, 22], text: COMMENT_2_TEXT },
            // inserting a second main comment
            { name: 'insertRange', start: [3, 12], position: 'start', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertRange', start: [3, 18], position: 'end', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertComment', id: COMMENT_3_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_3_DATE, start: [3, 19], text: COMMENT_3_TEXT }
        ],

        model = null,
        commentCollection = null;

    createTextApp('ooxml', OPERATIONS, { realView: true }).done(function (app) {
        model = app.getModel();
        commentCollection = model.getCommentLayer().getCommentCollection();
    });

    // existence check ----------------------------------------------------

    it('should subclass ModelObject', function () {
        expect(CommentCollection).toBeSubClassOf(ModelObject);
    });

    // constructor --------------------------------------------------------
    describe('constructor', function () {
        it('should create a CommentCollection class instance', function () {
            expect(commentCollection).toBeInstanceOf(CommentCollection);
        });
    });

    // public methods -----------------------------------------------------

    describe('method startNewThread', function () {
        it('should exist', function () {
            expect(commentCollection).toRespondTo('startNewThread');
        });
        it('should start a new comment thread with a temporary comment model behind the first main comment', async function () {

            // setting a selection in the second paragraph behind the first main comment
            var secondParagraph = model.getNode().find('div.p')[1];
            var localPosition = 30; // the word paragraph should be around position 30
            var wordSelection = Position.getWordSelection(secondParagraph, localPosition);

            expect(wordSelection.text).toBe("paragraph");

            selectionStartPosition = [1, wordSelection.start];
            selectionEndPosition = [1, wordSelection.end];

            // setting a selection -> this determines the position in the sorted container of comment models
            model.getSelection().setTextSelection(selectionStartPosition, selectionEndPosition); // selecting the word 'paragraph' in the second paragraph

            expect(model.getSelection().getSelectedText()).toBe("paragraph");

            await commentCollection.startNewThread();
            expect(commentCollection.getCommentNumber()).toBe(4);
        });

        describe('method getSavedSelectionState', function () {
            it('should exist', function () {
                expect(commentCollection).toRespondTo('getSavedSelectionState');
            });
            it('should return the saved selection state object', function () {
                selectionState = commentCollection.getSavedSelectionState();
                expect(selectionState).not.toBeNull();
            });
            it('should have valid postions in the saved selection state object', function () {
                expect(selectionState.start).toEqual(selectionStartPosition);
                expect(selectionState.end).toEqual(selectionEndPosition);
                expect(selectionState.isTextCursor).toBeFalse();
            });
        });

        describe('method containsTemporaryModel', function () {
            it('should exist', function () {
                expect(commentCollection).toRespondTo('containsTemporaryModel');
            });
            it('should return that the collection contains a temporary comment model', function () {
                expect(commentCollection.containsTemporaryModel()).toBeTrue();
            });
        });

        describe('method getById', function () {
            it('should exist', function () {
                expect(commentCollection).toRespondTo('getById');
            });
            it('should return the temporary comment model', function () {
                expect(commentCollection.getById(commentCollection.getTemporaryCommentId())).not.toBeNull();
            });
        });

        it('should have inserted the new temporary comment behind the first main comment', function () {
            expect(commentCollection.getByIndex(0).getId()).toBe(COMMENT_1_ID);
            expect(commentCollection.getByIndex(1).getId()).toBe(COMMENT_2_ID);
            expect(commentCollection.getByIndex(2).getId()).toBe(commentCollection.getTemporaryCommentId());
            expect(commentCollection.getByIndex(3).getId()).toBe(COMMENT_3_ID);
            expect(commentCollection.getByIndex(4)).toBeNull();
        });

        it('should modify the saved selection state if a previous paragraph is splitted', function () {

            model.splitParagraph([0, 5]); // split in the first paragraph

            selectionStartPosition[0] += 1;
            selectionEndPosition[0] += 1;

            selectionState = commentCollection.getSavedSelectionState();
            expect(selectionState).not.toBeNull();

            expect(selectionState.start).toEqual(selectionStartPosition);
            expect(selectionState.end).toEqual(selectionEndPosition);
            expect(selectionState.isTextCursor).toBeFalse();
        });
    });
});
