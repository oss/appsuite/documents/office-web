/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { transformOpColor, Color } from '@/io.ox/office/editframework/utils/color';
import { getElementStyleId } from '@/io.ox/office/editframework/utils/attributeutils';
import ListHandlerMixin from '@/io.ox/office/text/model/listhandlermixin';
import * as DOM from '@/io.ox/office/textframework/utils/dom';

import { waitForEvent } from '~/asynchelper';
import { createTextApp } from '~/text/apphelper';

// mix-in class ListHandlerMixin ==========================================

describe('OX Text mix-in class ListHandlerMixin', function () {

    // private helpers ----------------------------------------------------

    var model = null;
    var pageNode = null;
    var pageContentNode = null;
    var allParagraphs = null;
    var para0 = null;
    var para1 = null;
    var para2 = null;
    var para3 = null;
    var para4 = null;
    var para5 = null;
    var para6 = null;
    var para7 = null;
    var para8 = null;
    var para9 = null;
    var predefinedNumberingListStyleBrackets = 'L30003';
    var predefinedNumberingListStyleSmallLetter = 'L30011';
    var increaseListStartValue = 10;

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes',
            attrs: {
                document:  { defaultTabStop: 1270, zoom: { value: 100 } },
                page:      { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 },
                character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' },
                paragraph: { lineHeight: { type: 'percent', value: 119 }, marginBottom: 352 }
            }
        },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading1', styleName: 'heading 1',
            attrs: {
                character: { bold: true, fontName: 'Times New Roman', fontSize: 14, color: transformOpColor(Color.ACCENT1, { shade: 74902 }) },
                paragraph: { marginTop: 846, outlineLevel: 0, nextStyleId: 'Normal' }
            },
            parent: 'Normal',
            uiPriority: 9
        },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', text: 'ABC_0', start: [0, 0] },
        { name: 'insertParagraph', start: [1] },
        { name: 'insertText', start: [1, 0], text: 'ABC_1' },
        { name: 'insertParagraph', start: [2] },
        { name: 'insertText', start: [2, 0], text: 'ABC_2' },
        { name: 'insertDrawing', attrs: { fill: { type: 'solid', color: Color.ACCENT1 }, geometry: { presetShape: 'rect' }, line: { type: 'solid', style: 'solid', width: 26, color: transformOpColor(Color.ACCENT1, { shade: 50000 }) }, shape: { anchor: 'centered', anchorCentered: false, paddingLeft: 667, paddingRight: 667, paddingTop: 468, paddingBottom: 468, wordWrap: true, horzOverflow: 'overflow', vertOverflow: 'overflow' }, drawing: { name: 'rect', width: 6667, height: 4683, anchorHorBase: 'column', anchorHorAlign: 'offset', anchorVertBase: 'paragraph', anchorVertAlign: 'offset', inline: false, textWrapMode: 'none', anchorHorOffset: 1217, anchorVertOffset: -25268 } }, start: [2, 5], type: 'shape' },
        { name: 'insertParagraph', start: [2, 5, 0], attrs: { character: { color: Color.LIGHT1 }, paragraph: { alignment: 'center' } } },
        { name: 'insertText', text: 'ABC in Drawing', start: [2, 5, 0, 0] },
        { name: 'insertParagraph', start: [3] },
        { name: 'insertText', start: [3, 0], text: 'ABC_3' },
        { name: 'insertParagraph', start: [4] },
        { name: 'insertText', start: [4, 0], text: 'ABC_4' },
        { name: 'insertParagraph', start: [5] },
        { name: 'insertText', start: [5, 0], text: 'ABC_5' },
        { name: 'insertParagraph', start: [6] },
        { name: 'insertText', start: [6, 0], text: 'ABC_6' },
        { name: 'insertParagraph', start: [7] },
        { name: 'insertText', start: [7, 0], text: 'ABC_7' },
        { name: 'insertParagraph', start: [8] },
        { name: 'insertText', start: [8, 0], text: 'ABC_8' },
        { name: 'insertParagraph', start: [9] },
        { name: 'insertText', start: [9, 0], text: 'ABC_9' }
    ];

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(ListHandlerMixin).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('method getNode', function () {

        it('should exist', function () {
            expect(model).toRespondTo('getNode');
        });

        it('should exist also as getCurrentRootNode', function () {
            expect(model).toRespondTo('getCurrentRootNode');
        });

        it('should return the same node as getCurrentRootNode', function () {
            expect(model.getNode()[0]).toBe(model.getCurrentRootNode()[0]);
        });

        it('should have a correct prepared document with 10 paragraphs', function () {

            pageNode = model.getCurrentRootNode();
            pageContentNode = DOM.getPageContentNode(pageNode);
            allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);

            expect(allParagraphs).toHaveLength(10);

            para0 = $(allParagraphs[0]);
            para1 = $(allParagraphs[1]);
            para2 = $(allParagraphs[2]);
            para3 = $(allParagraphs[3]);
            para4 = $(allParagraphs[4]);
            para5 = $(allParagraphs[5]);
            para6 = $(allParagraphs[6]);
            para7 = $(allParagraphs[7]);
            para8 = $(allParagraphs[8]);
            para9 = $(allParagraphs[9]);

            expect(DOM.isParagraphNode(para0)).toBeTrue();
            expect(DOM.isParagraphNode(para1)).toBeTrue();
            expect(DOM.isParagraphNode(para2)).toBeTrue();
            expect(DOM.isParagraphNode(para3)).toBeTrue();
            expect(DOM.isParagraphNode(para4)).toBeTrue();
            expect(DOM.isParagraphNode(para5)).toBeTrue();
            expect(DOM.isParagraphNode(para6)).toBeTrue();
            expect(DOM.isParagraphNode(para7)).toBeTrue();
            expect(DOM.isParagraphNode(para8)).toBeTrue();
            expect(DOM.isParagraphNode(para9)).toBeTrue();
        });
    });

    describe('method DOM.isListParagraphNode', function () {
        it('should exist', function () {
            expect(DOM.isListParagraphNode).toBeFunction();
        });
        it('should contain no lists in the document', function () {
            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeFalse();
            expect(DOM.isListParagraphNode(para2)).toBeFalse();
            expect(DOM.isListParagraphNode(para3)).toBeFalse();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeFalse();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();
        });
    });

    describe('method createDefaultList', function () {

        it('should exist', function () {
            expect(model).toRespondTo('createDefaultList');
        });

        it('should set the default numbering list style to the selected paragraph', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([1, 1], [5, 1]);
                model.createDefaultList('numbering');
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeTrue();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4.");
            expect(para5.children().first().text()).toBe("5.");
        });

        it('should generate a new list, if new selection is behind non-list paragraph', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([7, 1], [8, 1]);
                model.createDefaultList('numbering');
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeTrue();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeTrue(); // !
            expect(DOM.isListParagraphNode(para8)).toBeTrue(); // !
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4.");
            expect(para5.children().first().text()).toBe("5.");
            expect(para7.children().first().text()).toBe("1."); // !
            expect(para8.children().first().text()).toBe("2."); // !
        });

    });

    describe('method setListStartValue', function () {

        it('should exist', function () {
            expect(model).toRespondTo('setListStartValue');
        });

        it('should set a new start value to the selected paragraph in the list', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([4, 1], [4, 2]);
                model.setListStartValue({ continue: false });
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeTrue();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeTrue();
            expect(DOM.isListParagraphNode(para8)).toBeTrue();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("1."); // !
            expect(para5.children().first().text()).toBe("2."); // !
            expect(para7.children().first().text()).toBe("1.");
            expect(para8.children().first().text()).toBe("2.");
        });

        it('should continue the list at the selected paragraph', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([7, 1], [7, 2]);
                model.setListStartValue({ continue: true });
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeTrue();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeTrue();
            expect(DOM.isListParagraphNode(para8)).toBeTrue();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("1.");
            expect(para5.children().first().text()).toBe("2.");
            expect(para7.children().first().text()).toBe("3."); // !
            expect(para8.children().first().text()).toBe("4."); // !
        });

        it('should do nothing, if first paragraph of list is selected and continue the list', function () {

            model.getSelection().setTextSelection([1, 1], [1, 2]);

            var operationCount = model.setListStartValue({ continue: true });

            expect(operationCount).toBe(0); // no operation generated
        });
    });

    describe('method createDefaultList #2', function () {

        it('should expand the numbering list with the paragraph directly following the selected numbering list paragraph', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([6, 1], [6, 2]);
                model.createDefaultList('numbering');
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeTrue();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue(); // !
            expect(DOM.isListParagraphNode(para7)).toBeTrue();
            expect(DOM.isListParagraphNode(para8)).toBeTrue();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("1.");
            expect(para5.children().first().text()).toBe("2.");
            expect(para6.children().first().text()).toBe("3."); // !
            expect(para7.children().first().text()).toBe("4."); // !
            expect(para8.children().first().text()).toBe("5."); // !
        });
    });

    describe('method removeListAttributes', function () {

        it('should exist', function () {
            expect(model).toRespondTo('removeListAttributes');
        });

        it('should remove the list attributes from selected paragraphs', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([4, 1], [8, 1]);
                model.removeListAttributes();
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeFalse();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
        });
    });

    describe('method createList', function () {

        it('should exist', function () {
            expect(model).toRespondTo('createList');
        });

        it('should set a list style to the selected paragraphs with increased start value', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([5, 0]);
                model.createList('numbering', { left: '', right: '.', listStartValue: increaseListStartValue, numberFormat: 'decimal', startPosition: [5, 0] });
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue(); // !
            expect(DOM.isListParagraphNode(para6)).toBeTrue(); // !
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
            expect(para5.children().first().text()).toBe(increaseListStartValue + '.'); // !
            expect(para6.children().first().text()).toBe((increaseListStartValue + 1) + '.');  // !
        });
    });

    describe('method setListStartValue #2', function () {

        it('should set the start value of the selecected list with increased start number to the first value', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([5, 1], [5, 2]);
                model.setListStartValue({ continue: false }); // using 'continue: false' -> setting first start value
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
            expect(para5.children().first().text()).toBe("1.");
            expect(para6.children().first().text()).toBe("2.");
        });

        it('should set the previous increased start value via undo', async function () {

            await waitForEvent(model, 'listformatting:done', () => model.getUndoManager().undo());

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
            expect(para5.children().first().text()).toBe(increaseListStartValue + '.');
            expect(para6.children().first().text()).toBe((increaseListStartValue + 1) + '.');
        });

        it('should set the start value of the selecected list with increased start number to the first value #2', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([5, 1], [5, 2]);
                model.setListStartValue({ continue: true }); // using 'continue: true' -> continuing previous list
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
            expect(para5.children().first().text()).toBe("4.");
            expect(para6.children().first().text()).toBe("5.");
        });
    });

    describe('method createSelectedListStyle', function () {

        it('should exist', function () {
            expect(model).toRespondTo('createSelectedListStyle');
        });

        it('should modify the list style, even if the list is splitted in two parts', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([1, 1]); // no range selected!
                model.createSelectedListStyle(predefinedNumberingListStyleBrackets, 0);
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("(1)");
            expect(para2.children().first().text()).toBe("(2)");
            expect(para3.children().first().text()).toBe("(3)");
            expect(para5.children().first().text()).toBe("(4)");
            expect(para6.children().first().text()).toBe("(5)");
        });
    });

    describe('method setAttribute', function () {

        it('should exist', function () {
            expect(model).toRespondTo('setAttribute');
        });

        it('should increase the list level of the selected paragraphs', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([2, 1], [3, 1]); // selecting two paragraphs
                model.setAttribute('paragraph', 'listLevel', 1); // increasing the list level of selected paragraphs
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("(1)");
            expect(para2.children().first().text()).toBe("a.");  // !
            expect(para3.children().first().text()).toBe("b.");  // !
            expect(para5.children().first().text()).toBe("(2)"); // !
            expect(para6.children().first().text()).toBe("(3)"); // !
        });
    });

    describe('method setListStartValue #3', function () {

        it('should set the start value of a paragraph with increase list level without changing paragraphs with lower list levels', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([3, 1]);
                model.setListStartValue({ continue: false }); // using 'continue: false' -> setting first start value
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("(1)");
            expect(para2.children().first().text()).toBe("a.");
            expect(para3.children().first().text()).toBe("a.");  // !
            expect(para5.children().first().text()).toBe("(2)");
            expect(para6.children().first().text()).toBe("(3)");
        });

        it('should continue a numbering list of a paragraph with increase list level without changing paragraphs with lower list levels', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([3, 1]);
                model.setListStartValue({ continue: true }); // using 'continue: true' -> continue previous list
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("(1)");
            expect(para2.children().first().text()).toBe("a.");
            expect(para3.children().first().text()).toBe("b.");  // !
            expect(para5.children().first().text()).toBe("(2)");
            expect(para6.children().first().text()).toBe("(3)");
        });
    });

    describe('method setAttribute #2', function () {

        it('should increase the list level of the selected paragraphs and modify the numbering of the affected list', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([3, 1]);
                model.setAttribute('paragraph', 'listLevel', 2); // increasing the list level of selected paragraph
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("(1)");
            expect(para2.children().first().text()).toBe("a.");
            expect(para3.children().first().text()).toBe("A.");  // !
            expect(para5.children().first().text()).toBe("(2)");
            expect(para6.children().first().text()).toBe("(3)");
        });

        it('should decrease the list level of the selected paragraphs and modify the numbering of the affected list with list level 1', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([3, 1]);
                model.setAttribute('paragraph', 'listLevel', 1); // decreasing the list level of selected paragraph
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("(1)");
            expect(para2.children().first().text()).toBe("a.");
            expect(para3.children().first().text()).toBe("b.");  // !
            expect(para5.children().first().text()).toBe("(2)");
            expect(para6.children().first().text()).toBe("(3)");
        });

        it('should decrease the list level of the selected paragraphs and modify the numbering of the affected list with list level 0', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([3, 1]);
                model.setAttribute('paragraph', 'listLevel', 0); // decreasing the list level of selected paragraph
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("(1)");
            expect(para2.children().first().text()).toBe("a.");
            expect(para3.children().first().text()).toBe("(2)"); // !
            expect(para5.children().first().text()).toBe("(3)"); // !
            expect(para6.children().first().text()).toBe("(4)"); // !
        });
    });

    describe('method removeListAttributes #2', function () {

        it('should remove all list attributes independent from list level or paragraph list style', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([0, 1], [9, 1]);
                model.removeListAttributes();
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeFalse();
            expect(DOM.isListParagraphNode(para2)).toBeFalse();
            expect(DOM.isListParagraphNode(para3)).toBeFalse();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeFalse();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();
        });
    });

    describe('method setAttributes', function () {

        it('should set paragraph style "Heading" to selected paragraphs', function () {

            var paraStyle = 'Heading1';

            model.getSelection().setTextSelection([0, 1]);
            model.setAttributes('paragraph', { styleId: paraStyle }, { clear: true });

            model.getSelection().setTextSelection([5, 1]);
            model.setAttributes('paragraph', { styleId: paraStyle }, { clear: true });

            expect(getElementStyleId(para0)).toBe(paraStyle);
            expect(getElementStyleId(para5)).toBe(paraStyle);
        });
    });

    describe('method createSelectedListStyle #2', function () {

        it('should set a specified list style to first heading paragraph and count correctly', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([0, 1]);
                model.createSelectedListStyle(predefinedNumberingListStyleBrackets, 0);
            });

            expect(DOM.isListParagraphNode(para0)).toBeTrue();
            expect(DOM.isListParagraphNode(para1)).toBeFalse();
            expect(DOM.isListParagraphNode(para2)).toBeFalse();
            expect(DOM.isListParagraphNode(para3)).toBeFalse();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeFalse();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para0.children().first().text()).toBe("(1)");
        });

        it('should set a specified list style to second heading paragraph and count correctly', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([5, 1]);
                model.createSelectedListStyle(predefinedNumberingListStyleBrackets, 0);
            });

            expect(DOM.isListParagraphNode(para0)).toBeTrue();
            expect(DOM.isListParagraphNode(para1)).toBeFalse();
            expect(DOM.isListParagraphNode(para2)).toBeFalse();
            expect(DOM.isListParagraphNode(para3)).toBeFalse();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para0.children().first().text()).toBe("(1)");
            expect(para5.children().first().text()).toBe("(2)");
        });

        it('should set a specified list style to non-headings paragraphs and count correctly, even if the same list style is used', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([1, 1], [3, 1]);
                model.createSelectedListStyle(predefinedNumberingListStyleBrackets, 0);
            });

            expect(DOM.isListParagraphNode(para0)).toBeTrue();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para0.children().first().text()).toBe("(1)");
            expect(para1.children().first().text()).toBe("(1)"); // ! same list style, but not continuing the list in heading
            expect(para2.children().first().text()).toBe("(2)");
            expect(para3.children().first().text()).toBe("(3)");
            expect(para5.children().first().text()).toBe("(2)");
        });

        it('should set a specified list style to non-headings paragraphs and count correctly', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([1, 1], [3, 1]);
                model.createSelectedListStyle(predefinedNumberingListStyleSmallLetter, 0);
            });

            expect(DOM.isListParagraphNode(para0)).toBeTrue();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();

            // checking state after formatting of list
            expect(para0.children().first().text()).toBe("(1)");
            expect(para1.children().first().text()).toBe("a.");
            expect(para2.children().first().text()).toBe("b.");
            expect(para3.children().first().text()).toBe("c.");
            expect(para5.children().first().text()).toBe("(2)");
        });

        it('should set a specified list style to second block of non-headings paragraphs and count correctly', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([7, 1], [9, 1]);
                model.createSelectedListStyle(predefinedNumberingListStyleSmallLetter, 0);
            });

            expect(DOM.isListParagraphNode(para0)).toBeTrue();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeTrue();
            expect(DOM.isListParagraphNode(para8)).toBeTrue();
            expect(DOM.isListParagraphNode(para9)).toBeTrue();

            // checking state after formatting of list
            expect(para0.children().first().text()).toBe("(1)");
            expect(para1.children().first().text()).toBe("a.");
            expect(para2.children().first().text()).toBe("b.");
            expect(para3.children().first().text()).toBe("c.");
            expect(para5.children().first().text()).toBe("(2)");
            expect(para7.children().first().text()).toBe("a.");
            expect(para8.children().first().text()).toBe("b.");
            expect(para9.children().first().text()).toBe("c.");
        });

        it('should resume an existing list independent from heading paragraphs', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([7, 1]);
                model.setListStartValue({ continue: true });
            });

            expect(DOM.isListParagraphNode(para0)).toBeTrue();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeTrue();
            expect(DOM.isListParagraphNode(para8)).toBeTrue();
            expect(DOM.isListParagraphNode(para9)).toBeTrue();

            // checking state after formatting of list
            expect(para0.children().first().text()).toBe("(1)");
            expect(para1.children().first().text()).toBe("a.");
            expect(para2.children().first().text()).toBe("b.");
            expect(para3.children().first().text()).toBe("c.");
            expect(para5.children().first().text()).toBe("(2)");
            expect(para7.children().first().text()).toBe("d.");
            expect(para8.children().first().text()).toBe("e.");
            expect(para9.children().first().text()).toBe("f.");
        });

        it('should reset start value of an existing list independent from heading paragraphs', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([2, 1]);
                model.setListStartValue({ continue: false });
            });

            expect(DOM.isListParagraphNode(para0)).toBeTrue();
            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeTrue();
            expect(DOM.isListParagraphNode(para8)).toBeTrue();
            expect(DOM.isListParagraphNode(para9)).toBeTrue();

            // checking state after formatting of list
            expect(para0.children().first().text()).toBe("(1)");
            expect(para1.children().first().text()).toBe("a.");
            expect(para2.children().first().text()).toBe("a.");
            expect(para3.children().first().text()).toBe("b.");
            expect(para5.children().first().text()).toBe("(2)");
            expect(para7.children().first().text()).toBe("c.");
            expect(para8.children().first().text()).toBe("d.");
            expect(para9.children().first().text()).toBe("e.");
        });
    });

    describe('method removeListAttributes #3', function () {

        it('should remove all list attributes independent from the paragraph styles', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([0, 1], [9, 1]);
                model.removeListAttributes();
            });

            expect(DOM.isListParagraphNode(para0)).toBeFalse();
            expect(DOM.isListParagraphNode(para1)).toBeFalse();
            expect(DOM.isListParagraphNode(para2)).toBeFalse();
            expect(DOM.isListParagraphNode(para3)).toBeFalse();
            expect(DOM.isListParagraphNode(para4)).toBeFalse();
            expect(DOM.isListParagraphNode(para5)).toBeFalse();
            expect(DOM.isListParagraphNode(para6)).toBeFalse();
            expect(DOM.isListParagraphNode(para7)).toBeFalse();
            expect(DOM.isListParagraphNode(para8)).toBeFalse();
            expect(DOM.isListParagraphNode(para9)).toBeFalse();
        });
    });
});
