/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { transformOpColor, opRgbColor, Color } from '@/io.ox/office/editframework/utils/color';
import { getCanvasNode, isGroupDrawingFrame, isGroupedDrawingFrame, isShapeDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';
import TextBaseModel from '@/io.ox/office/textframework/model/editor';
import TextModel from '@/io.ox/office/text/model/docmodel';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Position from '@/io.ox/office/textframework/utils/position';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';
import { handleParagraphIndex } from '@/io.ox/office/textframework/utils/textutils';

import { createTextApp } from '~/text/apphelper';

// class TextModel ========================================================

describe('Text class TextModel', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        firstParagraph = null,
        secondParagraph = null,
        secondParagraphLength = 241;

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes',
            attrs: {
                document:  { defaultTabStop: 1270, zoom: { value: 100 } },
                page:      { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 },
                character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' },
                paragraph: { lineHeight: { type: 'percent', value: 119 }, marginBottom: 352 }
            }
        },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading1', styleName: 'heading 1',
            attrs: {
                character: { bold: true, fontName: 'Times New Roman', fontSize: 14, color: transformOpColor(Color.ACCENT1, { shade: 74902 }) },
                paragraph: { marginTop: 846, outlineLevel: 0, nextStyleId: 'Normal' }
            },
            parent: 'Normal',
            uiPriority: 9
        },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', text: 'Hello World.', start: [0, 0] },

        { name: 'splitParagraph', start: [0, 12] },
        { name: 'insertText', start: [1, 0], text: [
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet',
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet',
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet.'
        ].join('') },
        { name: 'insertParagraph', start: [2] },
        { name: 'insertDrawing', attrs: { fill: { type: 'solid', color: Color.ACCENT1 }, geometry: { presetShape: 'rect' }, line: { type: 'solid', style: 'solid', width: 26, color: transformOpColor(Color.ACCENT1, { shade: 50000 }) }, shape: { anchor: 'centered', anchorCentered: false, paddingLeft: 667, paddingRight: 667, paddingTop: 468, paddingBottom: 468, wordWrap: true, horzOverflow: 'overflow', vertOverflow: 'overflow' }, drawing: { name: 'rect', width: 6667, height: 4683, anchorHorBase: 'column', anchorHorAlign: 'offset', anchorVertBase: 'paragraph', anchorVertAlign: 'offset', inline: false, textWrapMode: 'none', anchorHorOffset: 1217, anchorVertOffset: -25268 } }, start: [2, 0], type: 'shape' },
        { name: 'insertParagraph', start: [2, 0, 0], attrs: { character: { color: Color.LIGHT1 }, paragraph: { alignment: 'center' } } },
        { name: 'insertText', text: 'Hello World again', start: [2, 0, 0, 0] }
    ];

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        var node = model.getNode();
        firstParagraph = $(Position.getParagraphElement(node, [0]));
        secondParagraph = $(Position.getParagraphElement(node, [1]));
    });

    // existence check ----------------------------------------------------

    it('should subclass TextBaseModel', function () {
        expect(TextModel).toBeSubClassOf(TextBaseModel);
    });

    describe('method insertShapeWithDefaultBox', function () {

        it('should exist', function () {
            expect(model).toRespondTo('insertShapeWithDefaultBox');
        });

        it('should insert a shape with default size at the specified position', function () {

            var paraChildren = firstParagraph.children();

            expect(paraChildren).toHaveLength(1);
            expect(firstParagraph.text()).toBe("Hello World.");

            model.getSelection().setTextSelection([0, 6], [0, 7]); // Selecting the 'W' of world

            model.insertShapeWithDefaultBox('rect');

            paraChildren = firstParagraph.children();
            expect(paraChildren).toHaveLength(3);

            // checking the new two text spans
            expect($(paraChildren[0]).text()).toBe("Hello ");
            expect($(paraChildren[2]).text()).toBe("World.");

            // checking the new inserted drawing
            // -> inserted at the start point of the selection
            const drawingNode = $(paraChildren[1]);

            // checking the assigned classes
            expect(drawingNode.hasClass('drawing')).toBeTrue();
            expect(drawingNode.hasClass('inline')).toBeTrue();
            expect(drawingNode.hasClass('selected')).toBeTrue();
            expect(drawingNode.hasClass('movable')).toBeTrue();
            expect(drawingNode.hasClass('resizable')).toBeTrue();

            // checking the drawing type
            expect(isShapeDrawingFrame(paraChildren[1])).toBeTrue();

            // checking the default size
            expect(drawingNode.data().attributes.drawing.width).toBe(model.getDefaultShapeSize().width);
            expect(drawingNode.data().attributes.drawing.height).toBe(model.getDefaultShapeSize().height);
        });

    });

    describe('method insertShape', function () {

        it('should exist', function () {
            expect(model).toRespondTo('insertShape');
        });

        // test if shapes are inserted with the correct fill style
        it('Text: should insert a shape with fill', function () {
            var paraChildren = firstParagraph.children();
            // check inital state
            expect(paraChildren).toHaveLength(3);

            // insert a shape with fill
            model.insertShape('ellipse', { width: 200, height: 200 });
            paraChildren = firstParagraph.children();
            expect(paraChildren).toHaveLength(5);

            // checking the new inserted drawing
            // -> inserted at the start point of the selection
            var drawingNode = $(paraChildren[1]);

            expect(getExplicitAttributes(drawingNode, 'fill', true).type).toBe("solid");
            expect(getExplicitAttributes(drawingNode, 'drawing', true).name).toBe("ellipse");
        });

        it('Text: should insert a shape with no fill', function () {
            var paraChildren = firstParagraph.children();
            // check inital state
            expect(paraChildren).toHaveLength(5);

            // insert a shape with no fill
            model.insertShape('leftBrace', { width: 200, height: 200 });
            paraChildren = firstParagraph.children();
            expect(paraChildren).toHaveLength(7);

            // checking the new inserted drawing
            // -> inserted at the start point of the selection
            var drawingNode = $(paraChildren[1]);

            expect(getExplicitAttributes(drawingNode, 'fill', true)).toEqual({ type: 'none' });
            expect(getExplicitAttributes(drawingNode, 'drawing', true).name).toBe("leftBrace");
        });
    });

    // checking the selection after undo
    describe('Check, if selection is set correctly after undo', function () {

        it('Checks selection after undo and redo of setAttributes operation', async function () {

            var selection = model.getSelection();

            expect(secondParagraph.children()).toHaveLength(1);
            expect(Position.getParagraphNodeLength(secondParagraph)).toBe(secondParagraphLength);

            selection.setTextSelection([1, 6], [1, 11]); // Selecting 'ipsum'

            // marking word as 'bold'
            model.setAttribute('character', 'bold', true);

            // checking number of children in paragraph
            expect(secondParagraph.children()).toHaveLength(3);
            expect(Position.getParagraphNodeLength(secondParagraph)).toBe(secondParagraphLength);

            let boldSpan = $(secondParagraph.children()[1]);
            expect(boldSpan.css('font-weight')).toBe("bold");

            // setting the cursor to another position
            selection.setTextSelection([1, 2]);

            expect(selection.isTextCursor()).toBeTrue();
            expect(selection.getStartPosition()[0]).toBe(1);
            expect(selection.getStartPosition()[1]).toBe(2);
            expect(selection.getEndPosition()[0]).toBe(1);
            expect(selection.getEndPosition()[1]).toBe(2);

            await model.getUndoManager().undo();

            expect(secondParagraph.children()).toHaveLength(1); // only one span left
            expect(Position.getParagraphNodeLength(secondParagraph)).toBe(secondParagraphLength);

            // the old selection must be restored after undo
            expect(selection.isTextCursor()).toBeFalse();
            expect(selection.getStartPosition()[0]).toBe(1);
            expect(selection.getStartPosition()[1]).toBe(6);
            expect(selection.getEndPosition()[0]).toBe(1);
            expect(selection.getEndPosition()[1]).toBe(11);

            // setting the cursor to another position
            selection.setTextSelection([1, 2]);

            expect(selection.isTextCursor()).toBeTrue();
            expect(selection.getStartPosition()[0]).toBe(1);
            expect(selection.getStartPosition()[1]).toBe(2);
            expect(selection.getEndPosition()[0]).toBe(1);
            expect(selection.getEndPosition()[1]).toBe(2);

            // also after redo the selection must be restored correctly
            await model.getUndoManager().redo();

            expect(secondParagraph.children()).toHaveLength(3); // three spans
            expect(Position.getParagraphNodeLength(secondParagraph)).toBe(secondParagraphLength);

            boldSpan = $(secondParagraph.children()[1]);
            expect(boldSpan.css('font-weight')).toBe("bold");

            // the old selection must be restored after redo
            expect(selection.isTextCursor()).toBeFalse();
            expect(selection.getStartPosition()[0]).toBe(1);
            expect(selection.getStartPosition()[1]).toBe(6);
            expect(selection.getEndPosition()[0]).toBe(1);
            expect(selection.getEndPosition()[1]).toBe(11);

            // and set to old state again
            await model.getUndoManager().undo();

            expect(secondParagraph.children()).toHaveLength(1); // one span
            expect(Position.getParagraphNodeLength(secondParagraph)).toBe(secondParagraphLength);
        });

        it('Checks selection after undo of delete operation', async function () {

            var selection = model.getSelection();

            expect(secondParagraph.children()).toHaveLength(1);
            expect(Position.getParagraphNodeLength(secondParagraph)).toBe(secondParagraphLength);

            // selecting one word
            expect(selection.isTextCursor()).toBeFalse();
            expect(selection.getStartPosition()[0]).toBe(1);
            expect(selection.getStartPosition()[1]).toBe(6);
            expect(selection.getEndPosition()[0]).toBe(1);
            expect(selection.getEndPosition()[1]).toBe(11);

            await model.deleteSelected({ deleteKey: true });

            expect(secondParagraph.children()).toHaveLength(1); // still one span
            expect(Position.getParagraphNodeLength(secondParagraph)).toBe(secondParagraphLength - 5); // word removed

            expect(selection.isTextCursor()).toBeTrue(); // text cursor at start of delete range
            expect(selection.getStartPosition()[0]).toBe(1);
            expect(selection.getStartPosition()[1]).toBe(6);
            expect(selection.getEndPosition()[0]).toBe(1);
            expect(selection.getEndPosition()[1]).toBe(6);

            // setting the cursor to another position
            selection.setTextSelection([1, 2]);

            expect(selection.isTextCursor()).toBeTrue(); // text cursor at start of delete range
            expect(selection.getStartPosition()[0]).toBe(1);
            expect(selection.getStartPosition()[1]).toBe(2);
            expect(selection.getEndPosition()[0]).toBe(1);
            expect(selection.getEndPosition()[1]).toBe(2);

            // inserting bringing drawing1 to the front again
            await model.getUndoManager().undo();

            expect(secondParagraph.children()).toHaveLength(1); // only one span after inserting the text again
            expect(Position.getParagraphNodeLength(secondParagraph)).toBe(secondParagraphLength);

            // the old selection must be restored after undo
            expect(selection.isTextCursor()).toBeFalse();
            expect(selection.getStartPosition()[0]).toBe(1);
            expect(selection.getStartPosition()[1]).toBe(6);
            expect(selection.getEndPosition()[0]).toBe(1);
            expect(selection.getEndPosition()[1]).toBe(11);
        });

        describe('method getVerticalAlignmentMode', function () {
            it('should exist', function () {
                expect(model).toRespondTo('getVerticalAlignmentMode');
            });
            it('should get the vertical alignment mode of the text inside the selected drawing', function () {
                model.getSelection().setTextSelection([2, 0], [2, 1]);
                expect(model.getSelection().getSelectedDrawing()).toHaveLength(1);
                expect(model.getSelection().getSelectedDrawing().text()).toBe("Hello World again");
                expect(model.getVerticalAlignmentMode()).toBe("centered");
            });
        });

        describe('method setVerticalAlignmentMode', function () {
            it('should exist', function () {
                expect(model).toRespondTo('setVerticalAlignmentMode');
            });
            it('should set the vertical alignment mode of the text inside the selected drawing', function () {
                model.getSelection().setTextSelection([2, 0], [2, 1]);
                expect(model.getSelection().getSelectedDrawing().text()).toBe("Hello World again");
                model.setVerticalAlignmentMode('top');
                expect(model.getVerticalAlignmentMode()).toBe("top");
                model.setVerticalAlignmentMode('centered');
                expect(model.getVerticalAlignmentMode()).toBe("centered");
                model.setVerticalAlignmentMode('bottom');
                expect(model.getVerticalAlignmentMode()).toBe("bottom");
            });
        });
    });

});

// test for ODF files

describe('Text class TextModel in ODF', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        firstParagraph = null,
        drawings = null,
        drawing = null,
        canvasNode = null,
        drawingAttrs = null,
        drawing1Name = 'Rect1',
        drawing2Name = 'Heart1',
        drawingWidth = 0,
        drawingHeight = 0,
        canvasWidth = 0,
        canvasHeight = 0,
        drawing1Text = 'Rect',
        drawing2Text = 'Heart',
        drawing1WidthHmm = 2000,
        drawing1HeightHmm = 3700,
        drawing2WidthHmm = 4000,
        drawing2HeightHmm = 5000,
        drawing1WidthPx = 76,
        drawing1HeightPx = 140,
        canvas1WidthPx = drawing1WidthPx + 2,
        canvas1HeightPx = drawing1HeightPx + 2,
        drawing2WidthPx = 151,
        drawing2HeightPx = 189,
        canvas2WidthPx = drawing2WidthPx + 2,
        canvas2HeightPx = drawing2HeightPx + 2,
        tolerance = 2,

        // Info: The width of the drawing is adapted to the page width. This is set in this case to 21 cm minus 4 cm for the margins.
        //       Therefore there is a call in pagestyles.js: page.css({ width: '170mm' })
        //       The jQuery call in a unit test 'page.width()' then returns simply '170' which is interpreted in the normal code as
        //       pixel value. Therefore in a unit test the page width is reduced to '170 px' and the drawing width is automatically
        //       reduced to this value, if it is larger.

        // the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes',
                attrs: {
                    document:  { defaultTabStop: 1270, fileFormat: 'odf', zoom: { value: 100 } },
                    page:      { width: 21001, height: 29700, marginLeft: 2000, marginTop: 2000, marginRight: 2000, marginBottom: 2000 }
                }
            },
            { name: 'insertStyleSheet', type: 'drawing', styleId: '_default', default: true, attrs: { character: { fontName: 'Liberation·Serif', color: Color.AUTO, fontNameAsian: 'SimSun', fontNameComplex: 'Lucida·Sans', language: 'de-DE', fontSize: 12, fontSizeAsian: 10.5, fontSizeComplex: 12 }, line: { color: opRgbColor('3465A4'), style: 'solid', type: 'solid' }, fill: { color: opRgbColor('729fcf'), type: 'solid' } } },
            { name: 'insertStyleSheet', type: 'paragraph', styleId: '_default', default: true, attrs: { character: { fontName: 'Liberation·Serif', color: Color.AUTO, fontNameAsian: 'SimSun', fontNameComplex: 'Lucida·Sans', language: 'de-DE', fontSize: 12, fontSizeAsian: 10.5, fontSizeComplex: 12 } } },
            { name: 'insertParagraph', start: [0], attrs: { styleId: 'Standard' } },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { shape: { paddingBottom: 200, paddingRight: 600, paddingTop: 200, paddingLeft: 600 }, line: { width: 26, style: 'solid', type: 'solid' }, drawing: { name: drawing1Name, anchorHorOffset: 2302, anchorVertOffset: 1270, anchorHorBase: 'column', top: 1200, inline: false, left: 2300, anchorHorAlign: 'offset', anchorVertAlign: 'offset', width: drawing1WidthHmm, anchorVertBase: 'paragraph', height: drawing1HeightHmm }, geometry: { presetShape: 'rect', hostData: 'H4sIAAAAAAAAAJWRwQrCMAyGX6X0PqbzJlbBs/oA3sKWrYW20Sxu06d3bCooU5BSKPn5viR0VTC0\r\nS4wWYo5FUiEFFL6qoRwcM3Fiid2NooA3ugRfo36LG2Rx+Wco2EkCjFAbPVMztSkX/c20qptq2Ths\r\nt9SNSX+ezPWERhN1wSeMuTzKr+lOINbo/cDsBuPLO7wydVQHvV6N1PkC4iiOjgihV5fPTiVxuHgw\r\n2lPVukJsmvVc+gb+8swnPBZdZeVPUfZV9Jdm8W2vCUs6/eXrO8ipjHsLAgAA\r\n' }, fill: { type: 'solid' } } },
            { name: 'insertParagraph', start: [0, 0, 0], attrs: { paragraph: { alignment: 'center' } } },
            { name: 'insertText', start: [0, 0, 0, 0], text: drawing1Text },
            { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { shape: { paddingBottom: 300, paddingRight: 500, paddingTop: 300, paddingLeft: 500 }, line: { width: 26, style: 'solid', type: 'solid' }, drawing: { name: drawing2Name, anchorHorOffset: 10001, anchorVertOffset: 4286, anchorHorBase: 'column', top: 4200, inline: false, left: 10000, anchorHorAlign: 'offset', anchorVertAlign: 'offset', width: drawing2WidthHmm, anchorVertBase: 'paragraph', height: drawing2HeightHmm }, geometry: { presetShape: 'heart', hostData: 'H4sIAAAAAAAAAJXU3WqDMBgG4FuRHLYEE/9aS62wHW8XsLNQowmo6WKqdle/+FeoM4MgiZD4PQkv\r\nH54zSboTrRmprzSDBRUVVfLhjMsVl1JIyITkP6JWpExATsqGgpftlkrFr+tNRXsFiaSkSUCax06a\r\nY2+Y0DBh4DRtcWo57d5EnwDkjM9S+rjRBAjRVyVklEg1rz9veSOKJeBDS97kvutXqMdRj2g5atzz\r\n9eQvK8H8yVz15XyCy3miv+9EcVFPB9Wk0hfIl/vkQlb3kiSgFEXHM8V2QewGR13rvhT/Z2GjhZGt\r\n5Zks17Ny/LUzJLNHMM2RYwUFRgjbQeEmNDAQWUGRAUK20GEja0Z5wZTrW0HHNYTGgA52AcXmNnIj\r\nu4Y0d3doS2019xTSzrOMCW819xx4YCf9ae+ntOG42//Byy/XmZsDIAUAAA==\r\n' }, fill: { type: 'solid' } } },
            { name: 'insertParagraph', start: [0, 1, 0], attrs: { paragraph: { alignment: 'center' } } },
            { name: 'insertText', start: [0, 1, 0, 0], text: drawing2Text }
        ];

    createTextApp('odf', OPERATIONS).done(function (app) {
        model = app.getModel();
        var node = model.getNode();
        firstParagraph  = $(Position.getParagraphElement(node, [0]));
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(TextModel).toBeFunction();
    });

    describe('Checking the size of the Canvas nodes inside the drawing in ODF text files', function () {

        it('should contain two drawings in the first paragraph', function () {
            drawings = firstParagraph.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(2);
        });

        it('drawings in first paragraph should have the correct name', function () {
            drawingAttrs = getExplicitAttributes(drawings[0], 'drawing', true);
            expect(drawingAttrs.name).toBe(drawing1Name);
            drawingAttrs = getExplicitAttributes(drawings[1], 'drawing', true);
            expect(drawingAttrs.name).toBe(drawing2Name);
        });

        it('drawings in first paragraph should have the correct text content', function () {
            expect($(drawings[0]).text()).toBe(drawing1Text);
            expect($(drawings[1]).text()).toBe(drawing2Text);
        });

        it('drawings should have the specified width and height in hmm', function () {
            drawingAttrs = getExplicitAttributes(drawings[0], 'drawing', true);
            expect(drawingAttrs.width).toBe(drawing1WidthHmm);
            expect(drawingAttrs.height).toBe(drawing1HeightHmm);
            drawingAttrs = getExplicitAttributes(drawings[1], 'drawing', true);
            expect(drawingAttrs.width).toBe(drawing2WidthHmm);
            expect(drawingAttrs.height).toBe(drawing2HeightHmm);
        });

        it('first drawing without line ends should have only a slightly increased canvas', function () {

            // first drawing in first paragraph: the rectangle shape
            drawing = $(drawings[0]);

            canvasNode = getCanvasNode(drawing);
            expect(canvasNode).toHaveLength(1);

            drawingWidth = drawing.width();
            drawingHeight = drawing.height();
            canvasWidth = canvasNode.width();
            canvasHeight = canvasNode.height();

            expect(drawingWidth).toBeInInterval(drawing1WidthPx - tolerance, drawing1WidthPx + tolerance);
            expect(drawingHeight).toBeInInterval(drawing1HeightPx - tolerance, drawing1HeightPx + tolerance);
            expect(canvasWidth).toBeInInterval(canvas1WidthPx - tolerance, canvas1WidthPx + tolerance);
            expect(canvasHeight).toBeInInterval(canvas1HeightPx - tolerance, canvas1HeightPx + tolerance);
        });

        it('second drawing without line ends should have only a slightly increased canvas', function () {

            // second drawing in first paragraph: the heart shape
            drawing = $(drawings[1]);
            canvasNode = getCanvasNode(drawing);
            expect(canvasNode).toHaveLength(1);

            drawingWidth = drawing.width();
            drawingHeight = drawing.height();
            canvasWidth = canvasNode.width();
            canvasHeight = canvasNode.height();

            expect(drawingWidth).toBeInInterval(drawing2WidthPx - tolerance, drawing2WidthPx + tolerance);
            expect(drawingHeight).toBeInInterval(drawing2HeightPx - tolerance, drawing2HeightPx + tolerance);
            expect(canvasWidth).toBeInInterval(canvas2WidthPx - tolerance, canvas2WidthPx + tolerance);
            expect(canvasHeight).toBeInInterval(canvas2HeightPx - tolerance, canvas2HeightPx + tolerance);
        });

    });

    describe('method getVerticalAlignmentMode', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getVerticalAlignmentMode');
        });
        it('should get the vertical alignment mode of the text inside the selected drawing', function () {
            model.getSelection().setTextSelection([0, 0], [0, 1]);
            expect(model.getSelection().getSelectedDrawing()).toHaveLength(1);
            expect(model.getSelection().getSelectedDrawing().text()).toBe(drawing1Text);
            expect(model.getVerticalAlignmentMode()).toBe("top");
        });
    });

    describe('method setVerticalAlignmentMode', function () {
        it('should exist', function () {
            expect(model).toRespondTo('setVerticalAlignmentMode');
        });
        it('should set the vertical alignment mode of the text inside the selected drawing', function () {
            model.getSelection().setTextSelection([0, 0], [0, 1]);
            expect(model.getSelection().getSelectedDrawing().text()).toBe(drawing1Text);
            model.setVerticalAlignmentMode('centered');
            expect(model.getVerticalAlignmentMode()).toBe("centered");
            model.setVerticalAlignmentMode('bottom');
            expect(model.getVerticalAlignmentMode()).toBe("bottom");
            model.setVerticalAlignmentMode('top');
            expect(model.getVerticalAlignmentMode()).toBe("top");
        });
    });
});

// test for grouped shapes in ODF files

describe('Grouped Shapes in class TextModel in ODF', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        firstParagraph = null,
        groupDrawing = null,
        groupedDrawings = null,
        groupedDrawing1 = null,
        groupedDrawing2 = null,
        drawing1Text = 'Hello',
        drawing2Text = 'World',
        groupDrawingHeightPx = 382,
        groupDrawingWidthPx = 135,
        groupedDrawing1HeightPx = 200,
        groupedDrawing1WidthPx = 78,
        groupedDrawing2HeightPx = 182,
        groupedDrawing2WidthPx = 80,
        groupedDrawing1TopOffsetPx = 0,
        groupedDrawing1LeftOffsetPx = 0,
        groupedDrawing2TopOffsetPx = 200,
        groupedDrawing2LeftOffsetPx = 56,
        groupedDrawing1Color = '46B030',
        groupedDrawing2Color = '92D050',
        tolerance = 2,

        // Info: The width of the drawing is adapted to the page width. This is set in this case to 21 cm minus 4 cm for the margins.
        //       Therefore there is a call in pagestyles.js: page.css({ width: '170mm' })
        //       The jQuery call in a unit test 'page.width()' then returns simply '170' which is interpreted in the normal code as
        //       pixel value. Therefore in a unit test the page width is reduced to '170 px' and the drawing width is automatically
        //       reduced to this value, if it is larger.

        // the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes',
                attrs: {
                    document:  { defaultTabStop: 1270, fileFormat: 'odf', zoom: { value: 100 } },
                    page:      { width: 21001, height: 29700, marginLeft: 2000, marginTop: 2000, marginRight: 2000, marginBottom: 2000 }
                }
            },
            { name: 'insertStyleSheet', type: 'drawing', styleId: '_default', default: true, attrs: { character: { fontName: 'Liberation·Serif', color: Color.AUTO, fontNameAsian: 'SimSun', fontNameComplex: 'Lucida·Sans', language: 'de-DE', fontSize: 12, fontSizeAsian: 10.5, fontSizeComplex: 12 }, line: { color: opRgbColor('3465a4'), style: 'solid', type: 'solid' }, fill: { color: opRgbColor('729fcf'), type: 'solid' } } },
            { name: 'insertStyleSheet', type: 'paragraph', styleId: '_default', default: true, attrs: { character: { fontName: 'Liberation·Serif', color: Color.AUTO, fontNameAsian: 'SimSun', fontNameComplex: 'Lucida·Sans', language: 'de-DE', fontSize: 12, fontSizeAsian: 10.5, fontSizeComplex: 12 } } },
            { name: 'insertParagraph', start: [0], attrs: { styleId: 'Standard' } },

            // inserting a ellipse group
            { name: 'insertDrawing', start: [0, 0], type: 'group', attrs: { drawing: { anchorVertOffset: 64, anchorHorBase: 'column', anchorHorOffset: 72, height: 10103, inline: false, childTop: 64, childHeight: 10103, width: 3560, childLeft: 72, childWidth: 3560, anchorLayerOrder: 0, anchorVertBase: 'paragraph' } } },
            // inserting a rectangle into the group
            { name: 'insertDrawing', start: [0, 0, 0], type: 'shape', attrs: { fill: { color: opRgbColor(groupedDrawing1Color), type: 'solid' }, line: { style: 'solid', color: opRgbColor('3465A4'), type: 'solid' }, shape: { autoResizeHeight: false, wordWrap: false, anchor: 'centered' }, drawing: { height: 5292, width: 2068, left: 72, top: 64 }, geometry: { hostData: 'H4sIAAAAAAAAAG2QTQ6CMBCFrzLpnjgVg4RQFhzAnRt3jUxKE4QGKj+3F9qiJrp5yXydN286edXL\r\nKaO2lu2dqkhR9yDbLzCMKhs1TWU3C4aAcOQJBmXgTKp5UmQ63dpBMI4pbm0xT2IvCJ65gqen+ByA\r\nnxSI00DcqyebKcRYmm0ke5Jrymf6l33vWwwJRk2jzUCBvf9lpK0Fu+4ZP7qumSDc4MKK/PD/JMUL\r\n+7XQTCsBAAA=\r\n', presetShape: 'ellipse' } } },
            { name: 'insertParagraph', start: [0, 0, 0, 0], attrs: { paragraph: { marginBottom: 0, alignment: 'left', marginTop: 0 } } },
            { name: 'insertText', start: [0, 0, 0, 0, 0], text: drawing1Text },
            { name: 'setAttributes', attrs: { character: { color: Color.WHITE, underline: false, italicComplex: false, strike: 'none', letterSpacing: 'normal', bold: false, italic: false, caps: 'none', fontName: 'Calibri2', boldComplex: false, vertAlign: 'baseline', fontSize: 11, fontSizeAsian: 11, boldAsian: false, fontSizeComplex: 11, italicAsian: false } }, start: [0, 0, 0, 0, 0], end: [0, 0, 0, 0, 4] },

            // inserting an drawing into the group
            { name: 'insertDrawing', start: [0, 0, 1], type: 'shape', attrs: { fill: { color: opRgbColor(groupedDrawing2Color), type: 'solid' }, line: { style: 'solid', color: opRgbColor('3465a4'), type: 'solid' }, shape: { autoResizeHeight: false, wordWrap: false, anchor: 'centered' }, drawing: { height: 4811, width: 2070, left: 1562, top: 5356 }, geometry: { hostData: 'H4sIAAAAAAAAALNJKUost0rNy0jMS05N0U1Pzc9NLSmqVCguS7cqy0wtd8qvsFUyUDBQMDI0M4CS\r\nSgpgTSWVBam2SkWpySWJeek5qVBRuFEFiSUZtkq+CiDNPlDtKMYoIGgDhSgFPyU7G33srrEDADU3\r\nw26mAAAA\r\n', presetShape: 'rect' } } },

            { name: 'insertParagraph', start: [0, 0, 1, 0], attrs: { paragraph: { lineHeight: { type: 'percent', value: 100 }, marginBottom: 0, alignment: 'center', marginTop: 0 } } },
            { name: 'insertText', start: [0, 0, 1, 0, 0], text: drawing2Text },
            { name: 'setAttributes', attrs: { character: { color: Color.WHITE, underline: false, italicComplex: false, strike: 'none', letterSpacing: 'normal', bold: false, italic: false, caps: 'none', fontName: 'Calibri2', boldComplex: false, vertAlign: 'baseline', fontSize: 11, fontSizeAsian: 11, boldAsian: false, fontSizeComplex: 11, italicAsian: false } }, start: [0, 0, 1, 0, 0], end: [0, 0, 1, 0, 4] },
            { name: 'setAttributes', attrs: { drawing: { anchorVertOffset: 64, anchorHorBase: 'column', anchorHorOffset: 72, height: 10103, inline: false, childTop: 64, childHeight: 10103, width: 3560, childLeft: 72, childWidth: 3560, anchorLayerOrder: 0, anchorVertBase: 'paragraph' } }, start: [0, 0] }
        ];

    createTextApp('odf', OPERATIONS).done(function (app) {
        model = app.getModel();
        var node = model.getNode();
        firstParagraph  = $(Position.getParagraphElement(node, [0]));
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(TextModel).toBeFunction();
    });

    describe('checking group drawing and grouped drawings', function () {

        it('should contain one group drawing in the first paragraph', function () {
            groupDrawing = firstParagraph.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(groupDrawing).toHaveLength(1);
            expect(isGroupDrawingFrame(groupDrawing)).toBeTrue();
        });

        it('should contain two grouped drawings in the group drawing', function () {
            groupedDrawings = groupDrawing.find(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(groupedDrawings).toHaveLength(2);
            groupedDrawing1 = $(groupedDrawings[0]);
            groupedDrawing2 = $(groupedDrawings[1]);
            expect(isGroupedDrawingFrame(groupedDrawings[0])).toBeTrue();
            expect(isGroupedDrawingFrame(groupedDrawings[1])).toBeTrue();
            expect(groupedDrawing1.text()).toBe(drawing1Text);
            expect(groupedDrawing2.text()).toBe(drawing2Text);
        });

        // checking the size of the group drawing
        it('should have the correct size for the group drawing', function () {
            expect(groupDrawing.width()).toBeInInterval(groupDrawingWidthPx - tolerance, groupDrawingWidthPx + tolerance);
            expect(groupDrawing.height()).toBeInInterval(groupDrawingHeightPx - tolerance, groupDrawingHeightPx + tolerance);
        });

        it('should have the correct size for the grouped drawings', function () {
            expect(groupedDrawing1.height()).toBeInInterval(groupedDrawing1HeightPx - tolerance, groupedDrawing1HeightPx + tolerance);
            expect(groupedDrawing1.width()).toBeInInterval(groupedDrawing1WidthPx - tolerance, groupedDrawing1WidthPx + tolerance);
            expect(groupedDrawing2.height()).toBeInInterval(groupedDrawing2HeightPx - tolerance, groupedDrawing2HeightPx + tolerance);
            expect(groupedDrawing2.width()).toBeInInterval(groupedDrawing2WidthPx - tolerance, groupedDrawing2WidthPx + tolerance);
        });

        it('should have the correct vertical offset for the grouped drawings', function () {
            expect(parseInt(groupedDrawing1.css('top'), 10)).toBeInInterval(groupedDrawing1TopOffsetPx - tolerance, groupedDrawing1TopOffsetPx + tolerance);
            expect(parseInt(groupedDrawing2.css('top'), 10)).toBeInInterval(groupedDrawing2TopOffsetPx - tolerance, groupedDrawing2TopOffsetPx + tolerance);
        });

        it('should have the correct horizontal offset for the grouped drawings', function () {
            expect(parseInt(groupedDrawing1.css('left'), 10)).toBeInInterval(groupedDrawing1LeftOffsetPx - tolerance, groupedDrawing1LeftOffsetPx + tolerance);
            expect(parseInt(groupedDrawing2.css('left'), 10)).toBeInInterval(groupedDrawing2LeftOffsetPx - tolerance, groupedDrawing2LeftOffsetPx + tolerance);
        });

        it('should have the correct background color set as explicit attributes', function () {
            var attrs1 = getExplicitAttributes(groupedDrawing1, 'fill', true);
            var attrs2 = getExplicitAttributes(groupedDrawing2, 'fill', true);
            expect(attrs1.color.value).toBe(groupedDrawing1Color);
            expect(attrs2.color.value).toBe(groupedDrawing2Color);
        });

    });

});

// tests for z-index of paragraphs in documents with drawings with attribute 'anchorBehindDoc set to 'true' during loading

describe('Text class TextModel (z-index) of paragraphs for documents containing drawing(s) behind the text during loading', function () {

    // private helpers ----------------------------------------------------

    var model = null;
    var selection = null;
    var pageNode = null;
    var pageContentNode = null;
    var para1 = null;
    var para2 = null;
    var para3 = null;
    var para4 = null;
    var para5 = null;

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes',
            attrs: {
                document:  { defaultTabStop: 1270, zoom: { value: 100 } },
                page:      { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 },
                character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' },
                paragraph: { lineHeight: { type: 'percent', value: 119 }, marginBottom: 352 }
            }
        },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading1', styleName: 'heading 1',
            attrs: {
                character: { bold: true, fontName: 'Times New Roman', fontSize: 14, color: transformOpColor(Color.ACCENT1, { shade: 74902 }) },
                paragraph: { marginTop: 846, outlineLevel: 0, nextStyleId: 'Normal' }
            },
            parent: 'Normal',
            uiPriority: 9
        },

        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', start: [0, 0], text: 'abcde' },

        { name: 'insertParagraph', start: [1] },

        { name: 'insertDrawing', start: [1, 0], type: 'shape' },
        { name: 'insertParagraph', start: [1, 0, 0] },
        { name: 'insertText', start: [1, 0, 0, 0], text: 'paragraph one in shape 1' },
        { name: 'insertParagraph', start: [1, 0, 1] },
        { name: 'insertText', start: [1, 0, 1, 0], text: 'paragraph two in shape 1' },
        // this drawing is in front of the text in the document
        { name: 'setAttributes', attrs: { geometry: { presetShape: 'rect', avList: {} }, line: { color: transformOpColor(Color.ACCENT1, { shade: 50000 }), width: 26, type: 'solid' }, fill: { color: Color.ACCENT1, type: 'solid' }, shape: { anchor: 'centered', anchorCentered: false, paddingLeft: 466, paddingRight: 466, paddingTop: 164, paddingBottom: 164, wordWrap: true, horzOverflow: 'overflow', vert: 'horz', vertOverflow: 'overflow' }, drawing: { id: '4000008', left: 0, top: 0, width: 4657, height: 1640, inline: false, anchorHorBase: 'column', anchorHorAlign: 'offset', anchorHorOffset: 900, anchorVertBase: 'paragraph', anchorVertAlign: 'offset', anchorVertOffset: 344, textWrapMode: 'none', name: 'tf1' }, character: { color: Color.DARK1 } }, start: [1, 0] },

        { name: 'insertText', start: [1, 1], text: 'fghij' },

        { name: 'insertParagraph', start: [2] },
        { name: 'insertText', start: [2, 0], text: 'klmno' },

        { name: 'insertParagraph', start: [3] },
        { name: 'insertDrawing', start: [3, 0], type: 'shape' },
        { name: 'insertParagraph', start: [3, 0, 0] },
        { name: 'insertText', start: [3, 0, 0, 0], text: 'paragraph one in shape 2' },
        // this drawing is behind the text in the document
        { name: 'setAttributes', attrs: { geometry: { presetShape: 'rect', avList: {} }, line: { color: transformOpColor(Color.ACCENT1, { shade: 50000 }), width: 26, type: 'solid' }, fill: { color: Color.ACCENT1, type: 'solid' }, shape: { anchor: 'centered', anchorCentered: false, paddingLeft: 466, paddingRight: 466, paddingTop: 164, paddingBottom: 164, wordWrap: true, horzOverflow: 'overflow', vert: 'horz', vertOverflow: 'overflow' }, drawing: { id: '4000008', left: 0, top: 0, width: 4657, height: 1640, inline: false, anchorHorBase: 'column', anchorHorAlign: 'offset', anchorHorOffset: 900, anchorVertBase: 'paragraph', anchorVertAlign: 'offset', anchorVertOffset: 344, anchorBehindDoc: true, textWrapMode: 'none', name: 'tf2' }, character: { color: Color.DARK1 } }, start: [3, 0] },
        { name: 'insertText', start: [3, 1], text: 'pqrst' },

        { name: 'insertParagraph', start: [4] },
        { name: 'insertText', start: [4, 0], text: 'uvwxy' }
    ];

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(TextModel).toBeFunction();
    });

    it('should exist Utils helper function', function () {
        expect(handleParagraphIndex).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('method isIncreasedDocContent', function () {

        it('should exist', function () {
            expect(model).toRespondTo('isIncreasedDocContent');
        });

        it('should indicate an increased content for paragraphs and tables', function () {
            expect(model.isIncreasedDocContent()).toBeTrue();
        });
    });

    describe('method handleParagraphIndex', function () {

        it('should exist', function () {
            expect(model).toRespondTo('getNode');
        });

        it('should exist also as getCurrentRootNode', function () {
            expect(model).toRespondTo('getCurrentRootNode');
        });

        it('should return the same node as getCurrentRootNode', function () {
            expect(model.getNode()[0]).toBe(model.getCurrentRootNode()[0]);
        });

        it('should have a correct prepared document with 4 paragraphs', function () {

            selection = model.getSelection();
            pageNode = model.getCurrentRootNode();
            pageContentNode = DOM.getPageContentNode(pageNode);
            const allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);

            expect(allParagraphs).toHaveLength(5);

            para1 = $(allParagraphs[0]);
            para2 = $(allParagraphs[1]);
            para3 = $(allParagraphs[2]);
            para4 = $(allParagraphs[3]);
            para5 = $(allParagraphs[4]);

            expect(DOM.isParagraphNode(para1)).toBeTrue();
            expect(DOM.isParagraphNode(para2)).toBeTrue();
            expect(DOM.isParagraphNode(para3)).toBeTrue();
            expect(DOM.isParagraphNode(para4)).toBeTrue();
            expect(DOM.isParagraphNode(para5)).toBeTrue();

            expect(para1.text()).toHaveLength(5);
            expect(para1.text()).toBe("abcde");
            expect(para1.children()).toHaveLength(1); // only 1 text span

            expect(para5.text()).toHaveLength(5);
            expect(para5.text()).toBe("uvwxy");
            expect(para5.children()).toHaveLength(1); // only 1 text span

        });

        it('should have the correct z-index at each paragraph', function () {

            expect(para2.children('.drawing')).toHaveLength(1);

            expect(para1.hasClass('incZIndex')).toBeFalse();
            expect(para2.hasClass('incZIndex')).toBeTrue(); // increased paragraph because of drawing on top of text
            expect(para3.hasClass('incZIndex')).toBeFalse();
            expect(para4.hasClass('incZIndex')).toBeFalse();
            expect(para5.hasClass('incZIndex')).toBeFalse();
        });

        it('should remove the increased z-index when deleting the drawing and restore it after undo', async function () {

            var startPos = [1, 0];
            var endPos = [1, 1];

            expect(para1.text()).toHaveLength(5);
            expect(para1.children()).toHaveLength(1); // only 1 text span

            selection.setTextSelection(startPos, endPos);

            expect(selection.isTextCursor()).toBeFalse();

            await model.deleteSelected({ deleteKey: true }); // triggered by pressing 'delete' or 'backspace'

            let allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(5);

            expect(para2.children('.drawing')).toHaveLength(0); // no more drawing in paragraph

            // checking increased paragraph is no longer increased
            expect(para1.hasClass('incZIndex')).toBeFalse();
            expect(para2.hasClass('incZIndex')).toBeFalse(); // no longer increased paragraph because of drawing
            expect(para3.hasClass('incZIndex')).toBeFalse();
            expect(para4.hasClass('incZIndex')).toBeFalse();
            expect(para5.hasClass('incZIndex')).toBeFalse();

            // undo has to insert the drawing again
            await model.getUndoManager().undo();

            allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(5); // still 5 paragraphs

            expect(para2.children('.drawing')).toHaveLength(1); // the drawing is inserted again into the second paragraph

            // checking that the paragraph is increased again
            expect(para1.hasClass('incZIndex')).toBeFalse();
            expect(para2.hasClass('incZIndex')).toBeTrue(); // increased paragraph because of drawing
            expect(para3.hasClass('incZIndex')).toBeFalse();
            expect(para4.hasClass('incZIndex')).toBeFalse();
            expect(para5.hasClass('incZIndex')).toBeFalse();

            // the old selection must be restored after undo
            expect(selection.getStartPosition()).toEqual(startPos);
            expect(selection.getEndPosition()).toEqual(endPos);
            expect(selection.isTextCursor()).toBeFalse();
        });

        it('should double increase the z-index when inserting a further shape in the following paragraph and reduce it again after undo', async function () {

            var startPos = [2, 0];

            expect(para3.children('.drawing')).toHaveLength(0); // no drawing in third paragraph

            model.insertShape('rect', { height: 1000, width: 1000 }, { absolute: true, paragraphOffset: { left: 1000, top: 300 }, position: startPos });

            expect(para3.children('.drawing')).toHaveLength(1); // the drawing is synchronously inserted into the third paragraph

            // checking that the paragraphs are correctly increased
            expect(para1.hasClass('incZIndex')).toBeFalse();

            expect(para2.hasClass('doubleIncZIndex')).toBeTrue(); // double increase
            expect(para2.hasClass('incZIndex')).toBeFalse(); // no more simple increase

            expect(para3.hasClass('incZIndex')).toBeTrue(); // simple increase

            expect(para4.hasClass('incZIndex')).toBeFalse();
            expect(para5.hasClass('incZIndex')).toBeFalse();

            // undo has to insert the drawing again
            await model.getUndoManager().undo();

            expect(para3.children('.drawing')).toHaveLength(0); // no more drawing in third paragraph
            expect(para2.children('.drawing')).toHaveLength(1); // the only drawing is inserted into the second paragraph

            const allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(5); // still 5 paragraphs

            // // checking that the third paragraph is no longer increased and the second paragraph only simply increased
            expect(para1.hasClass('incZIndex')).toBeFalse();
            expect(para2.hasClass('doubleIncZIndex')).toBeFalse(); // no more double increase
            expect(para2.hasClass('incZIndex')).toBeTrue(); // simple increased paragraph because of drawing
            expect(para3.hasClass('incZIndex')).toBeFalse();
            expect(para4.hasClass('incZIndex')).toBeFalse();
            expect(para5.hasClass('incZIndex')).toBeFalse();
        });

        it('should reduce the z-index compared to other paragraphs, when drawing is shifted into the background and increased again after undo', async function () {

            var startPos = [1, 0];
            var endPos = [1, 1];

            selection.setTextSelection(startPos, endPos);

            expect(selection.isDrawingSelection()).toBeTrue();
            expect(selection.isTextCursor()).toBeFalse();

            expect(para2.hasClass('incZIndex')).toBeTrue(); // simple increased paragraph because of drawing

            model.toggleDrawingToTextMode();

            expect(para2.hasClass('incZIndex')).toBeFalse(); // no more increased paragraph because of drawing
            expect(para2.hasClass('decZIndex')).toBeFalse(); // decreasing paragraph is also not required. Following paragraphs anyhow overwrite it

            // undo has to insert the drawing again
            await model.getUndoManager().undo();

            expect(para2.hasClass('incZIndex')).toBeTrue(); // the paragraph is increased again because of the drawing
            expect(para2.hasClass('decZIndex')).toBeFalse(); // decreasing paragraph would be wrong
        });
    });
});

// tests for z-index of paragraphs in documents with drawings with attribute 'anchorBehindDoc set to 'true' during loading
// -> this scenario is identical to the previous tests, but in this case the first drawing is anchored behind the text. In this simpler
//    case it is in the loading phase not required, to update all already formatted drawings, after the pagecontent node got the flag
//    "increased-content".

describe('Text class TextModel (z-index) of paragraphs for documents containing a first drawing behind the text during loading', function () {

    // private helpers ----------------------------------------------------

    var model = null;
    var pageNode = null;
    var pageContentNode = null;
    var para1 = null;
    var para2 = null;
    var para3 = null;
    var para4 = null;
    var para5 = null;

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes',
            attrs: {
                document:  { defaultTabStop: 1270, zoom: { value: 100 } },
                page:      { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 },
                character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' },
                paragraph: { lineHeight: { type: 'percent', value: 119 }, marginBottom: 352 }
            }
        },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading1', styleName: 'heading 1',
            attrs: {
                character: { bold: true, fontName: 'Times New Roman', fontSize: 14, color: transformOpColor(Color.ACCENT1, { shade: 74902 }) },
                paragraph: { marginTop: 846, outlineLevel: 0, nextStyleId: 'Normal' }
            },
            parent: 'Normal',
            uiPriority: 9
        },

        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', start: [0, 0], text: 'abcde' },

        { name: 'insertParagraph', start: [1] },

        { name: 'insertDrawing', start: [1, 0], type: 'shape' },
        { name: 'insertParagraph', start: [1, 0, 0] },
        { name: 'insertText', start: [1, 0, 0, 0], text: 'paragraph one in shape 1' },
        { name: 'insertParagraph', start: [1, 0, 1] },
        { name: 'insertText', start: [1, 0, 1, 0], text: 'paragraph two in shape 1' },
        // this drawing is behind the text in the document
        { name: 'setAttributes', attrs: { geometry: { presetShape: 'rect', avList: {} }, line: { color: transformOpColor(Color.ACCENT1, { shade: 50000 }), width: 26, type: 'solid' }, fill: { color: Color.ACCENT1, type: 'solid' }, shape: { anchor: 'centered', anchorCentered: false, paddingLeft: 466, paddingRight: 466, paddingTop: 164, paddingBottom: 164, wordWrap: true, horzOverflow: 'overflow', vert: 'horz', vertOverflow: 'overflow' }, drawing: { id: '4000008', left: 0, top: 0, width: 4657, height: 1640, inline: false, anchorHorBase: 'column', anchorHorAlign: 'offset', anchorHorOffset: 900, anchorVertBase: 'paragraph', anchorVertAlign: 'offset', anchorVertOffset: 344, anchorBehindDoc: true, textWrapMode: 'none', name: 'tf1' }, character: { color: Color.DARK1 } }, start: [1, 0] },

        { name: 'insertText', start: [1, 1], text: 'fghij' },

        { name: 'insertParagraph', start: [2] },
        { name: 'insertText', start: [2, 0], text: 'klmno' },

        { name: 'insertParagraph', start: [3] },
        { name: 'insertDrawing', start: [3, 0], type: 'shape' },
        { name: 'insertParagraph', start: [3, 0, 0] },
        { name: 'insertText', start: [3, 0, 0, 0], text: 'paragraph one in shape 2' },
        // this drawing is in front of the text in the document
        { name: 'setAttributes', attrs: { geometry: { presetShape: 'rect', avList: {} }, line: { color: transformOpColor(Color.ACCENT1, { shade: 50000 }), width: 26, type: 'solid' }, fill: { color: Color.ACCENT1, type: 'solid' }, shape: { anchor: 'centered', anchorCentered: false, paddingLeft: 466, paddingRight: 466, paddingTop: 164, paddingBottom: 164, wordWrap: true, horzOverflow: 'overflow', vert: 'horz', vertOverflow: 'overflow' }, drawing: { id: '4000008', left: 0, top: 0, width: 4657, height: 1640, inline: false, anchorHorBase: 'column', anchorHorAlign: 'offset', anchorHorOffset: 900, anchorVertBase: 'paragraph', anchorVertAlign: 'offset', anchorVertOffset: 344, textWrapMode: 'none', name: 'tf2' }, character: { color: Color.DARK1 } }, start: [3, 0] },
        { name: 'insertText', start: [3, 1], text: 'pqrst' },

        { name: 'insertParagraph', start: [4] },
        { name: 'insertText', start: [4, 0], text: 'uvwxy' }
    ];

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(TextModel).toBeFunction();
    });

    it('should exist Utils helper function', function () {
        expect(handleParagraphIndex).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('method isIncreasedDocContent', function () {

        it('should exist', function () {
            expect(model).toRespondTo('isIncreasedDocContent');
        });

        it('should indicate an increased content for paragraphs and tables', function () {
            expect(model.isIncreasedDocContent()).toBeTrue();
        });
    });

    describe('method handleParagraphIndex', function () {

        it('should exist', function () {
            expect(model).toRespondTo('getNode');
        });

        it('should exist also as getCurrentRootNode', function () {
            expect(model).toRespondTo('getCurrentRootNode');
        });

        it('should return the same node as getCurrentRootNode', function () {
            expect(model.getNode()[0]).toBe(model.getCurrentRootNode()[0]);
        });

        it('should have a correct prepared document with 4 paragraphs', function () {

            pageNode = model.getCurrentRootNode();
            pageContentNode = DOM.getPageContentNode(pageNode);
            const allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);

            expect(allParagraphs).toHaveLength(5);

            para1 = $(allParagraphs[0]);
            para2 = $(allParagraphs[1]);
            para3 = $(allParagraphs[2]);
            para4 = $(allParagraphs[3]);
            para5 = $(allParagraphs[4]);

            expect(DOM.isParagraphNode(para1)).toBeTrue();
            expect(DOM.isParagraphNode(para2)).toBeTrue();
            expect(DOM.isParagraphNode(para3)).toBeTrue();
            expect(DOM.isParagraphNode(para4)).toBeTrue();
            expect(DOM.isParagraphNode(para5)).toBeTrue();

            expect(para1.text()).toHaveLength(5);
            expect(para1.text()).toBe("abcde");
            expect(para1.children()).toHaveLength(1); // only 1 text span

            expect(para5.text()).toHaveLength(5);
            expect(para5.text()).toBe("uvwxy");
            expect(para5.children()).toHaveLength(1); // only 1 text span

        });

        it('should have the correct z-index at each paragraph', function () {

            expect(para2.children('.drawing')).toHaveLength(1);

            expect(para1.hasClass('incZIndex')).toBeFalse();
            expect(para2.hasClass('incZIndex')).toBeFalse();
            expect(para3.hasClass('incZIndex')).toBeFalse();
            expect(para4.hasClass('incZIndex')).toBeTrue(); // increased paragraph because of drawing on top of text
            expect(para5.hasClass('incZIndex')).toBeFalse();
        });

    });

});

// tests for z-index of paragraphs in documents without drawings with attribute 'anchorBehindDoc set to 'true'

describe('Text class TextModel (z-index) of paragraphs for documents containing no drawing(s) behind the text during loading', function () {

    // private helpers ----------------------------------------------------

    var model = null;
    var selection = null;
    var pageNode = null;
    var pageContentNode = null;
    var para1 = null;
    var para2 = null;
    var para3 = null;
    var para4 = null;

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes',
            attrs: {
                document:  { defaultTabStop: 1270, zoom: { value: 100 } },
                page:      { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 },
                character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' },
                paragraph: { lineHeight: { type: 'percent', value: 119 }, marginBottom: 352 }
            }
        },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading1', styleName: 'heading 1',
            attrs: {
                character: { bold: true, fontName: 'Times New Roman', fontSize: 14, color: transformOpColor(Color.ACCENT1, { shade: 74902 }) },
                paragraph: { marginTop: 846, outlineLevel: 0, nextStyleId: 'Normal' }
            },
            parent: 'Normal',
            uiPriority: 9
        },

        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', start: [0, 0], text: 'abcde' },

        { name: 'insertParagraph', start: [1] },

        { name: 'insertDrawing', start: [1, 0], type: 'shape' },
        { name: 'insertParagraph', start: [1, 0, 0] },
        { name: 'insertText', start: [1, 0, 0, 0], text: 'paragraph one in shape' },
        { name: 'insertParagraph', start: [1, 0, 1] },
        { name: 'insertText', start: [1, 0, 1, 0], text: 'paragraph two in shape' },
        // drawing is in front of the text
        { name: 'setAttributes', attrs: { geometry: { presetShape: 'rect', avList: {} }, line: { color: transformOpColor(Color.ACCENT1, { shade: 50000 }), width: 26, type: 'solid' }, fill: { color: Color.ACCENT1, type: 'solid' }, shape: { anchor: 'centered', anchorCentered: false, paddingLeft: 466, paddingRight: 466, paddingTop: 164, paddingBottom: 164, wordWrap: true, horzOverflow: 'overflow', vert: 'horz', vertOverflow: 'overflow' }, drawing: { id: '4000008', left: 0, top: 0, width: 4657, height: 1640, inline: false, anchorHorBase: 'column', anchorHorAlign: 'offset', anchorHorOffset: 900, anchorVertBase: 'paragraph', anchorVertAlign: 'offset', anchorVertOffset: 344, textWrapMode: 'none', name: 'tf1' }, character: { color: Color.DARK1 } }, start: [1, 0] },

        { name: 'insertText', start: [1, 1], text: 'fghij' },

        { name: 'insertParagraph', start: [2] },
        { name: 'insertText', start: [2, 0], text: 'klmno' },

        { name: 'insertParagraph', start: [3] },
        { name: 'insertText', start: [3, 0], text: 'pqrst' }
    ];

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(TextModel).toBeFunction();
    });

    it('should exist Utils helper function', function () {
        expect(handleParagraphIndex).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('method isIncreasedDocContent', function () {

        it('should exist', function () {
            expect(model).toRespondTo('isIncreasedDocContent');
        });

        it('should indicate an increased content for paragraphs and tables', function () {
            expect(model.isIncreasedDocContent()).toBeFalse();
        });
    });

    describe('method handleParagraphIndex', function () {

        it('should exist', function () {
            expect(model).toRespondTo('getNode');
        });

        it('should exist also as getCurrentRootNode', function () {
            expect(model).toRespondTo('getCurrentRootNode');
        });

        it('should return the same node as getCurrentRootNode', function () {
            expect(model.getNode()[0]).toBe(model.getCurrentRootNode()[0]);
        });

        it('should have a correct prepared document with 4 paragraphs', function () {

            selection = model.getSelection();
            pageNode = model.getCurrentRootNode();
            pageContentNode = DOM.getPageContentNode(pageNode);
            const allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);

            expect(allParagraphs).toHaveLength(4);

            para1 = $(allParagraphs[0]);
            para2 = $(allParagraphs[1]);
            para3 = $(allParagraphs[2]);
            para4 = $(allParagraphs[3]);

            expect(DOM.isParagraphNode(para1)).toBeTrue();
            expect(DOM.isParagraphNode(para2)).toBeTrue();
            expect(DOM.isParagraphNode(para3)).toBeTrue();
            expect(DOM.isParagraphNode(para4)).toBeTrue();

            expect(para1.text()).toHaveLength(5);
            expect(para1.text()).toBe("abcde");
            expect(para1.children()).toHaveLength(1); // only 1 text span

            expect(para4.text()).toHaveLength(5);
            expect(para4.text()).toBe("pqrst");
            expect(para4.children()).toHaveLength(1); // only 1 text span

        });

        it('should have no increased z-index at any paragraph', function () {

            expect(para2.children('.drawing')).toHaveLength(1);

            expect(para1.hasClass('incZIndex')).toBeFalse();
            expect(para2.hasClass('incZIndex')).toBeFalse(); // the paragraph is not increased, although it contains a drawing
            expect(para3.hasClass('incZIndex')).toBeFalse();
            expect(para4.hasClass('incZIndex')).toBeFalse();
        });

        it('should not contain an increased z-index when deleting the drawing and restore it after undo', async function () {

            var startPos = [1, 0];
            var endPos = [1, 1];

            expect(para1.text()).toHaveLength(5);
            expect(para1.children()).toHaveLength(1); // only 1 text span

            selection.setTextSelection(startPos, endPos);

            expect(selection.isTextCursor()).toBeFalse();

            await model.deleteSelected({ deleteKey: true }); // triggered by pressing 'delete' or 'backspace'

            let allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(4); // only one remaining paragraph before the table (and two following paragraphs remain)

            expect(para2.children('.drawing')).toHaveLength(0); // no more drawing in paragraph

            // checking increased paragraph is no longer increased
            expect(para1.hasClass('incZIndex')).toBeFalse();
            expect(para2.hasClass('incZIndex')).toBeFalse(); // no longer increased paragraph because of drawing
            expect(para3.hasClass('incZIndex')).toBeFalse();
            expect(para4.hasClass('incZIndex')).toBeFalse();

            // undo has to insert the drawing again
            await model.getUndoManager().undo();

            allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(4); // still 4 paragraphs

            expect(para2.children('.drawing')).toHaveLength(1); // the drawing is inserted again into the second paragraph

            // checking that the paragraph is increased again
            expect(para1.hasClass('incZIndex')).toBeFalse();
            expect(para2.hasClass('incZIndex')).toBeFalse(); // still not increased
            expect(para3.hasClass('incZIndex')).toBeFalse();
            expect(para4.hasClass('incZIndex')).toBeFalse();

            // the old selection must be restored after undo
            expect(selection.getStartPosition()).toEqual(startPos);
            expect(selection.getEndPosition()).toEqual(endPos);
            expect(selection.isTextCursor()).toBeFalse();
        });

        it('should not introduce the z-index when inserting a further shape in the following paragraph and removing it again after undo', async function () {

            var startPos = [2, 0];

            expect(para3.children('.drawing')).toHaveLength(0); // no drawing in third paragraph

            model.insertShape('rect', { height: 1000, width: 1000 }, { absolute: true, paragraphOffset: { left: 1000, top: 300 }, position: startPos });

            expect(para3.children('.drawing')).toHaveLength(1); // the drawing is synchronously inserted into the third paragraph

            // checking that the paragraphs are correctly increased
            expect(para1.hasClass('incZIndex')).toBeFalse();
            expect(para2.hasClass('incZIndex')).toBeFalse(); // no increase
            expect(para3.hasClass('incZIndex')).toBeFalse();
            expect(para4.hasClass('incZIndex')).toBeFalse();

            // undo has to insert the drawing again
            await model.getUndoManager().undo();

            expect(para3.children('.drawing')).toHaveLength(0); // no more drawing in third paragraph
            expect(para2.children('.drawing')).toHaveLength(1); // the only drawing is inserted into the second paragraph

            const allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(4); // still 4 paragraphs

            // // checking that the third paragraph is no longer increased and the second paragraph only simply increased
            expect(para1.hasClass('incZIndex')).toBeFalse();
            expect(para2.hasClass('incZIndex')).toBeFalse(); // still no increase
            expect(para3.hasClass('incZIndex')).toBeFalse();
            expect(para4.hasClass('incZIndex')).toBeFalse();
        });

        it('should reduce the z-index compared to other paragraphs, when drawing is shifted into the background and increased again after undo', async function () {

            var startPos = [1, 0];
            var endPos = [1, 1];

            selection.setTextSelection(startPos, endPos);

            expect(selection.isDrawingSelection()).toBeTrue();
            expect(selection.isTextCursor()).toBeFalse();

            expect(model.isIncreasedDocContent()).toBeFalse(); // no class marker at the page node

            expect(para2.hasClass('incZIndex')).toBeFalse(); // no increased paragraph because of drawing

            model.toggleDrawingToTextMode();

            expect(model.isIncreasedDocContent()).toBeTrue(); // now there is the class marker at the page node

            expect(para2.hasClass('decZIndex')).toBeFalse(); // decreasing paragraph is not required. Following paragraphs anyhow overwrite it

            // undo has to insert the drawing again
            await model.getUndoManager().undo();

            expect(model.isIncreasedDocContent()).toBeTrue(); // the class marker is still at the page node (Info: This might be removed in the future)

            expect(para2.hasClass('incZIndex')).toBeTrue(); // the paragraph is now increased again because of the drawing (Info: This might be removed in the future)
            expect(para2.hasClass('decZIndex')).toBeFalse(); // decreasing paragraph would be wrong
        });
    });
});
