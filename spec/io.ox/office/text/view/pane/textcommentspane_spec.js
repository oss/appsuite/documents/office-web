/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import CommentFrame from '@/io.ox/office/editframework/view/commentframe';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Position from '@/io.ox/office/textframework/utils/position';
import { TextCommentsPane } from '@/io.ox/office/text/view/pane/textcommentspane';

import { createTextApp } from '~/text/apphelper';

// class TextCommentsPane with no comments in document ====================

describe('Text class TextCommentsPane without comments in document', function () {

    // private helpers ----------------------------------------------------

    var

        COMMENT_1_ID = 'cmt12340001',

        //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } }
        ],

        textCommentsPane = null,
        commentsPaneNode = null;

    createTextApp('ooxml', OPERATIONS, { realView: true }).done(function (docApp) {
        textCommentsPane = docApp.docView.commentsPane;
    });

    // existence check ----------------------------------------------------

    it('should subclass ModelObject', function () {
        expect(TextCommentsPane).toBeSubClassOf(ModelObject);
    });

    // constructor --------------------------------------------------------
    describe('constructor', function () {
        it('should create a TextCommentsPane class instance', function () {
            expect(textCommentsPane).toBeInstanceOf(TextCommentsPane);
        });
    });

    // public methods -----------------------------------------------------

    describe('method isVisible', function () {
        it('should exist', function () {
            expect(TextCommentsPane).toHaveMethod('isVisible');
        });
        it('should return that the comments pane is visible', function () {
            expect(textCommentsPane.isVisible()).toBeFalse();
        });
    });

    describe('method showsComments', function () {
        it('should exist', function () {
            expect(TextCommentsPane).toHaveMethod('showsComments');
        });
        it('should return that the comments are not shown', function () {
            expect(textCommentsPane.showsComments()).toBeFalse();
        });
    });

    describe('method getNode', function () {
        it('should exist', function () {
            expect(TextCommentsPane).toHaveMethod('getNode');
        });
        it('should return the comments pane node', function () {
            commentsPaneNode = textCommentsPane.getNode();
            expect(commentsPaneNode.hasClass('commentlayer')).toBeTrue();
        });
    });

    describe('method getHeader', function () {
        it('should exist', function () {
            expect(TextCommentsPane).toHaveMethod('getHeader');
        });
        it('should return the header node inside the comments pane node', function () {
            var headerNode = textCommentsPane.getHeader();
            expect(headerNode.hasClass('header')).toBeTrue();
        });
    });

    describe('method getCommentsContainer', function () {
        it('should exist', function () {
            expect(TextCommentsPane).toHaveMethod('getCommentsContainer');
        });
        it('should return the header node inside the comments pane node', function () {
            var commentsNode = textCommentsPane.getCommentsContainer();
            expect(commentsNode.hasClass('comments-container')).toBeTrue();
        });
    });

    describe('method commentFrames', function () {
        it('should exist', function () {
            expect(TextCommentsPane).toHaveMethod('commentFrames');
        });
        it('should return an empty iterator', function () {
            var iter = textCommentsPane.commentFrames();
            expect(iter).toBeIterator();
            expect(Array.from(iter)).toEqual([]);
        });
    });

    describe('method getThreadedCommentFrameById', function () {
        it('should exist', function () {
            expect(TextCommentsPane).toHaveMethod('getThreadedCommentFrameById');
        });
        it('should not return a comment thread by a not existing comment id', function () {
            var commentFrame = textCommentsPane.getThreadedCommentFrameById(COMMENT_1_ID);
            expect(commentFrame).toBeUndefined();
        });
    });
});

// class TextCommentsPane with comments in document =======================

describe('Text class TextCommentsPane with comments in document', function () {

    // private helpers ----------------------------------------------------

    var
        COMMENT_1_ID = 'cmt12340001',
        COMMENT_2_ID = 'cmt12340002',
        COMMENT_3_ID = 'cmt56780003',
        COMMENT_4_ID = 'cmt56780004',
        COMMENT_5_ID = null,
        COMMENT_6_ID = null,

        COMMENT_1_DATE = '2020-08-20T09:05:00Z',
        COMMENT_2_DATE = '2020-08-20T09:06:00Z',
        COMMENT_3_DATE = '2020-08-20T09:07:00Z',

        COMMENT_1_TEXT = 'Text in comment 1',
        COMMENT_2_TEXT = 'Text in comment 2',
        COMMENT_3_TEXT = 'Text in comment 3',
        COMMENT_4_TEXT = 'Text in comment 4',

        COMMENT_2_TEXT_CHANGED = 'Text changed in comment 2',
        COMMENT_3_TEXT_REPLY = 'I am a further reply',

        PARA_1_TEXT = 'This is the first paragraph!',
        PARA_2_TEXT = 'This is the second paragraph!',
        PARA_3_TEXT = 'This is the third paragraph!',
        PARA_4_TEXT = 'This is the fourth paragraph!',
        PARA_5_TEXT = 'This is the fifth paragraph!',

        PROVIDER_ID = 'ox',

        USER_ID_1 = '1234',
        USER_ID_2 = '5678',

        AUTHOR_1 = 'User A',
        AUTHOR_2 = 'User B',

        HIDE_BUTTON_START_TEXT = 'View 1 more',

        //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
            { name: 'insertParagraph', start: [0] },
            { name: 'insertText', start: [0, 0], text: PARA_1_TEXT },
            { name: 'insertParagraph', start: [1] },
            { name: 'insertText', start: [1, 0], text: PARA_2_TEXT },
            { name: 'insertParagraph', start: [2] },
            { name: 'insertText', start: [2, 0], text: PARA_3_TEXT },
            { name: 'insertParagraph', start: [3] },
            { name: 'insertText', start: [3, 0], text: PARA_4_TEXT },
            { name: 'insertParagraph', start: [4] },
            { name: 'insertText', start: [4, 0], text: PARA_5_TEXT },
            // inserting the first main comment
            { name: 'insertRange', start: [1, 12], position: 'start', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertRange', start: [1, 18], position: 'end', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertComment', id: COMMENT_1_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_1_DATE, start: [1, 19], text: COMMENT_1_TEXT },
            // inserting the first child comment
            { name: 'insertRange', start: [1, 13], position: 'start', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertRange', start: [1, 21], position: 'end', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertComment', id: COMMENT_2_ID, parentId: COMMENT_1_ID, author: AUTHOR_2, userId: USER_ID_2, providerId: PROVIDER_ID, date: COMMENT_2_DATE, start: [1, 22], text: COMMENT_2_TEXT },
            // inserting a second main comment
            { name: 'insertRange', start: [3, 12], position: 'start', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertRange', start: [3, 18], position: 'end', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertComment', id: COMMENT_3_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_3_DATE, start: [3, 19], text: COMMENT_3_TEXT }
        ],

        textCommentsPane = null,
        commentsPaneNode = null,
        model = null,
        commentLayer = null,
        commentCollection = null,
        threadNode = null,
        threadNode_1 = null,
        threadNode_2 = null,
        endPosition = null;

    createTextApp('ooxml', OPERATIONS, { realView: true }).done(function (docApp) {
        textCommentsPane = docApp.docView.commentsPane;
        model = docApp.docModel;
        commentLayer = model.getCommentLayer();
        commentCollection = commentLayer.getCommentCollection();
    });

    function expectCommentIds(...expectedIds) {
        var iterator = textCommentsPane.commentFrames();
        var commentIds = Array.from(iterator, commentFrame => commentFrame.getThreadComment().getId());
        expect(commentIds).toEqual(expectedIds);
    }

    // public methods -----------------------------------------------------

    describe('method isVisible', function () {
        it('should return that the comments pane is visible', function () {
            expect(textCommentsPane.isVisible()).toBeFalse(); // should be true -> problem with height()
        });
    });

    describe('method showsComments', function () {
        it('should return that the comments are not shown', function () {
            expect(textCommentsPane.showsComments()).toBeTrue();
        });
    });

    describe('method getNode', function () {
        it('should return the comments pane node', function () {
            commentsPaneNode = textCommentsPane.getNode();
            expect(commentsPaneNode.hasClass('commentlayer')).toBeTrue();
        });
    });

    describe('method commentFrames', function () {
        it('should return a non-empty iterator', function () {
            expectCommentIds(COMMENT_1_ID, COMMENT_3_ID);
        });
    });

    describe('method getThreadedCommentFrameById', function () {
        var commentFrame = null;
        var threadComment = null;
        it('should return a comment thread by the id of the first comment in the thread', function () {
            commentFrame = textCommentsPane.getThreadedCommentFrameById(COMMENT_1_ID);
            expect(commentFrame).toBeInstanceOf(CommentFrame);
        });
        it('should return a comment thread by id that has the first comment in the thread as specified threadComment', function () {
            threadComment = commentFrame.getThreadComment();
            expect(threadComment.getId()).toBe(COMMENT_1_ID);
        });
        it('should not return a comment thread by the id of the first child comment of the thread', function () {
            commentFrame = textCommentsPane.getThreadedCommentFrameById(COMMENT_2_ID);
            expect(commentFrame).toBeUndefined();
        });
        it('should not return a comment thread by the id of the second main comment', function () {
            commentFrame = textCommentsPane.getThreadedCommentFrameById(COMMENT_3_ID);
            expect(commentFrame).toBeInstanceOf(CommentFrame);
        });
        it('should return a comment thread by id that has the first comment in the second thread as specified threadComment', function () {
            threadComment = commentFrame.getThreadComment();
            expect(threadComment.getId()).toBe(COMMENT_3_ID);
        });
        it('should not return a comment thread by a not existing comment id', function () {
            commentFrame = textCommentsPane.getThreadedCommentFrameById(COMMENT_4_ID);
            expect(commentFrame).toBeUndefined();
        });
    });

    // checking the selection
    describe('method commentCollection.selectComment', function () {

        it('should select the second thread node correctly', function () {
            var commentFrame = textCommentsPane.getThreadedCommentFrameById(COMMENT_3_ID);
            threadNode = commentFrame.getNode();
            expect(threadNode.hasClass('show-all')).toBeFalse();
            expect(threadNode.hasClass('selected')).toBeFalse();

            commentCollection.selectComment(commentCollection.getById(COMMENT_3_ID)); // selecting the second main comment

            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_3_ID);
            expect(threadNode.hasClass('show-all')).toBeTrue();
            expect(threadNode.hasClass('selected')).toBeTrue();
        });

        it('should select the first thread node correctly', function () {
            var commentFrame = textCommentsPane.getThreadedCommentFrameById(COMMENT_1_ID);
            threadNode = commentFrame.getNode();
            expect(threadNode.hasClass('show-all')).toBeFalse();
            expect(threadNode.hasClass('selected')).toBeFalse();

            commentCollection.selectComment(commentCollection.getById(COMMENT_2_ID)); // selecting the child of the first main comment

            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_2_ID);
            expect(threadNode.hasClass('show-all')).toBeTrue();
            expect(threadNode.hasClass('selected')).toBeTrue();
        });

        it('should deselect the second thread node', function () {
            var commentFrame = textCommentsPane.getThreadedCommentFrameById(COMMENT_3_ID);
            threadNode = commentFrame.getNode();
            expect(threadNode.hasClass('show-all')).toBeFalse();
            expect(threadNode.hasClass('selected')).toBeFalse();
        });
    });

    describe('method commentCollection.deselectComment', function () {

        it('should deselect the first thread node correctly', function () {
            var commentFrame = textCommentsPane.getThreadedCommentFrameById(COMMENT_1_ID);
            threadNode = commentFrame.getNode();
            expect(threadNode.hasClass('show-all')).toBeTrue();
            expect(threadNode.hasClass('selected')).toBeTrue();
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_2_ID);

            commentCollection.deselectComment(); // deselecting the second main comment

            expect(commentCollection.getSelectedCommentId()).toBeNull();
            expect(threadNode.hasClass('show-all')).toBeFalse();
            expect(threadNode.hasClass('selected')).toBeFalse();
        });

    });

    // checking the author filter
    describe('method commentLayer.setAuthorFilter', function () {

        it('should only hide the child node, if the second user is filtered', function () {

            threadNode_1 = textCommentsPane.getThreadedCommentFrameById(COMMENT_1_ID).getNode();
            threadNode_2 = textCommentsPane.getThreadedCommentFrameById(COMMENT_3_ID).getNode();

            expect(threadNode_1.hasClass('active-author-filter')).toBeFalse(); // no filter at thread
            expect(threadNode_2.hasClass('active-author-filter')).toBeFalse(); // no filter at thread
            expect(threadNode_1.hasClass('isFilteredThread')).toBeFalse(); // the complete thread is not filtered
            expect(threadNode_2.hasClass('isFilteredThread')).toBeFalse(); // the complete thread is not filtered

            expect(threadNode_1.children('.comment')).toHaveLength(2); // 2 comments in the first thread
            expect(threadNode_2.children('.comment')).toHaveLength(1); // 1 comment in the second thread
            expect(threadNode_1.children('.isFilteredComment')).toHaveLength(0); // 0 filtered comments in the first thread
            expect(threadNode_2.children('.isFilteredComment')).toHaveLength(0); // 0 filtered comments in the second thread

            commentLayer.setAuthorFilter([AUTHOR_1]); // applying an author filter -> show only comments from AUTHOR_1

            expect(threadNode_1.hasClass('active-author-filter')).toBeTrue(); // active filter at thread
            expect(threadNode_2.hasClass('active-author-filter')).toBeTrue(); // active filter marker also at second thread
            expect(threadNode_1.hasClass('isFilteredThread')).toBeFalse(); // the complete thread is not filtered
            expect(threadNode_2.hasClass('isFilteredThread')).toBeFalse(); // the complete thread is not filtered

            expect(threadNode_1.children('.comment')).toHaveLength(2); // still 2 comment in the first thread
            expect(threadNode_2.children('.comment')).toHaveLength(1); // 1 comment in the second thread
            expect(threadNode_1.children('.isFilteredComment')).toHaveLength(1); // 1 filtered comment in the first thread
            expect(threadNode_2.children('.isFilteredComment')).toHaveLength(0); // 0 filtered comments in the second thread
        });

        it('should show the hidden child node after removing the filter', function () {

            expect(threadNode_1.hasClass('active-author-filter')).toBeTrue();
            expect(threadNode_2.hasClass('active-author-filter')).toBeTrue();
            expect(threadNode_1.hasClass('isFilteredThread')).toBeFalse();
            expect(threadNode_2.hasClass('isFilteredThread')).toBeFalse();

            expect(threadNode_1.children('.comment')).toHaveLength(2);
            expect(threadNode_2.children('.comment')).toHaveLength(1);
            expect(threadNode_1.children('.isFilteredComment')).toHaveLength(1);
            expect(threadNode_2.children('.isFilteredComment')).toHaveLength(0);

            commentLayer.setAuthorFilter(); // removing the author filter

            expect(threadNode_1.hasClass('active-author-filter')).toBeFalse();
            expect(threadNode_2.hasClass('active-author-filter')).toBeFalse();
            expect(threadNode_1.hasClass('isFilteredThread')).toBeFalse();
            expect(threadNode_2.hasClass('isFilteredThread')).toBeFalse();

            expect(threadNode_1.children('.comment')).toHaveLength(2);
            expect(threadNode_2.children('.comment')).toHaveLength(1);
            expect(threadNode_1.children('.isFilteredComment')).toHaveLength(0);
            expect(threadNode_2.children('.isFilteredComment')).toHaveLength(0);
        });

        it('should hide two comments, if the first user is filtered', function () {

            expect(threadNode_1.hasClass('active-author-filter')).toBeFalse();
            expect(threadNode_2.hasClass('active-author-filter')).toBeFalse();
            expect(threadNode_1.hasClass('isFilteredThread')).toBeFalse();
            expect(threadNode_2.hasClass('isFilteredThread')).toBeFalse();

            expect(threadNode_1.children('.comment')).toHaveLength(2);
            expect(threadNode_2.children('.comment')).toHaveLength(1);
            expect(threadNode_1.children('.isFilteredComment')).toHaveLength(0);
            expect(threadNode_2.children('.isFilteredComment')).toHaveLength(0);

            commentLayer.setAuthorFilter([AUTHOR_2]); // applying an author filter -> show only comments from AUTHOR_2

            expect(threadNode_1.hasClass('active-author-filter')).toBeTrue();
            expect(threadNode_2.hasClass('active-author-filter')).toBeTrue();
            expect(threadNode_1.hasClass('isFilteredThread')).toBeFalse();
            expect(threadNode_2.hasClass('isFilteredThread')).toBeTrue();

            expect(threadNode_1.children('.comment')).toHaveLength(2);
            expect(threadNode_2.children('.comment')).toHaveLength(1);
            expect(threadNode_1.children('.isFilteredComment')).toHaveLength(1);
            expect(threadNode_2.children('.isFilteredComment')).toHaveLength(1);
        });

        it('should show all comments again after removing the filter', function () {

            expect(threadNode_1.hasClass('active-author-filter')).toBeTrue();
            expect(threadNode_2.hasClass('active-author-filter')).toBeTrue();
            expect(threadNode_1.hasClass('isFilteredThread')).toBeFalse();
            expect(threadNode_2.hasClass('isFilteredThread')).toBeTrue();

            expect(threadNode_1.children('.comment')).toHaveLength(2);
            expect(threadNode_2.children('.comment')).toHaveLength(1);
            expect(threadNode_1.children('.isFilteredComment')).toHaveLength(1);
            expect(threadNode_2.children('.isFilteredComment')).toHaveLength(1);

            commentLayer.setAuthorFilter(); // removing the author filter

            expect(threadNode_1.hasClass('active-author-filter')).toBeFalse();
            expect(threadNode_2.hasClass('active-author-filter')).toBeFalse();
            expect(threadNode_1.hasClass('isFilteredThread')).toBeFalse();
            expect(threadNode_2.hasClass('isFilteredThread')).toBeFalse();

            expect(threadNode_1.children('.comment')).toHaveLength(2);
            expect(threadNode_2.children('.comment')).toHaveLength(1);
            expect(threadNode_1.children('.isFilteredComment')).toHaveLength(0);
            expect(threadNode_2.children('.isFilteredComment')).toHaveLength(0);
        });
    });

    // changing a comment
    describe('method commentCollection.changeThread', function () {

        it('should change the text content of a comment', function () {

            expect(threadNode_1.children('.comment')).toHaveLength(2); // 2 comments in the first thread
            var childComment = threadNode_1.children('.comment')[1];

            expect($(childComment).find('.text').text()).toBe(COMMENT_2_TEXT);

            var commentInfo = { text: COMMENT_2_TEXT_CHANGED, mentions: undefined };
            var commentModel = commentCollection.getById(COMMENT_2_ID);
            commentCollection.changeThread(commentModel, commentInfo);

            expect($(childComment).find('.text').text()).toBe(COMMENT_2_TEXT_CHANGED);
        });

        it('should undo the text change correctly', async function () {

            expect(threadNode_1.children('.comment')).toHaveLength(2); // 2 comments in the first thread
            let childComment = threadNode_1.children('.comment')[1];
            expect($(childComment).find('.text').text()).toBe(COMMENT_2_TEXT_CHANGED);

            await model.getUndoManager().undo();
            expect(threadNode_1.children('.comment')).toHaveLength(2); // 2 comments in the first thread
            childComment = threadNode_1.children('.comment')[1];
            expect($(childComment).find('.text').text()).toBe(COMMENT_2_TEXT);
        });
    });

    // reply to an existing comment
    describe('method commentCollection.insertThread for a comment reply', function () {

        it('should not contain a hide button, if there is only one child comment', function () {
            var hideButton = threadNode_1.children('.hide-button');
            expect(hideButton).toHaveLength(0);
        });

        it('should insert a new reply to the first comment thread', async function () {

            expect(threadNode_1.children('.comment')).toHaveLength(2); // 2 comments in the first thread

            var commentInfo = { text: COMMENT_3_TEXT_REPLY, mentions: undefined };

            await commentCollection.insertThread(COMMENT_1_ID, commentInfo); // inserting a reply to the first comment
            expect(threadNode_1.children('.comment')).toHaveLength(3); // 3 comments in the first thread
            var childComment = threadNode_1.children('.comment')[2];
            expect($(childComment).find('.text').text()).toBe(COMMENT_3_TEXT_REPLY);
        });

        it('should contain a hide button, if there is more than one child comment', function () {
            var hideButton = threadNode_1.children('.hide-button');
            expect(hideButton).toHaveLength(1);
            expect(hideButton.text().slice(0, 11)).toBe(HIDE_BUTTON_START_TEXT); // only checking start because of 'reply' vs. 'replies'
        });

        it('should remove the new reply via undo correctly', async function () {

            expect(threadNode_1.children('.comment')).toHaveLength(3);
            var childComment = threadNode_1.children('.comment')[2];
            expect($(childComment).find('.text').text()).toBe(COMMENT_3_TEXT_REPLY);

            await model.getUndoManager().undo();
            expect(threadNode_1.children('.comment')).toHaveLength(2); // 2 comments in the first thread
        });

        it('should no longer contain a hide button, if there is only one child comment', function () {
            var hideButton = threadNode_1.children('.hide-button');
            expect(hideButton).toHaveLength(0);
        });
    });

    // inserting a new comment thread
    describe('method commentCollection.insertThread for a new comment', function () {

        it('should have 2 comment threads before inserting the new comment thread', function () {
            expectCommentIds(COMMENT_1_ID, COMMENT_3_ID);
        });

        it('should insert a new comment in a new comment thread with one comment', async function () {

            var secondParagraph = model.getNode().find('div.p')[1];
            var localPosition = 30; // the word paragraph should be around position 30
            var wordSelection = Position.getWordSelection(secondParagraph, localPosition);

            expect(wordSelection.text).toBe("paragraph");

            // setting a selection -> this determines the position in the sorted container of comment models
            model.getSelection().setTextSelection([1, wordSelection.start], [1, wordSelection.end]); // selecting the word 'paragraph' in the second paragraph

            expect(model.getSelection().getSelectedText()).toBe("paragraph");
            expect(commentCollection.getCommentNumber()).toBe(3);

            var commentInfo = { text: COMMENT_4_TEXT, mentions: undefined };
            COMMENT_5_ID = await commentCollection.insertThread(null, commentInfo);
            expect(commentCollection.getCommentNumber()).toBe(4);
        });

        it('should have 3 comment threads after inserting the new thread', function () {
            expectCommentIds(COMMENT_1_ID, COMMENT_3_ID, COMMENT_5_ID);
        });

        it('should have the specified text in the new comment', function () {

            threadNode = textCommentsPane.getThreadedCommentFrameById(COMMENT_5_ID).getNode();

            expect(threadNode.children('.comment')).toHaveLength(1);
            var childComment = threadNode.children('.comment')[0];
            expect($(childComment).find('.text').text()).toBe(COMMENT_4_TEXT);
        });

        it('should remove the new reply via undo correctly', async function () {
            expectCommentIds(COMMENT_1_ID, COMMENT_3_ID, COMMENT_5_ID);
            await model.getUndoManager().undo();
            expectCommentIds(COMMENT_1_ID, COMMENT_3_ID);
        });
    });

    // deleting a comment thread
    describe('method delete for deleting a comment a new comment', function () {

        it('should have 2 comment threads before inserting the new comment thread', function () {
            expectCommentIds(COMMENT_1_ID, COMMENT_3_ID);
        });

        it('should insert a new comment in a new comment thread with one comment', async function () {

            var secondParagraph = model.getNode().find('div.p')[1];
            var localPosition = 30; // the word paragraph should be around position 30
            var wordSelection = Position.getWordSelection(secondParagraph, localPosition);
            endPosition = [1, wordSelection.end];

            expect(wordSelection.text).toBe("paragraph");

            // setting a selection -> this determines the position in the sorted container of comment models
            model.getSelection().setTextSelection([1, wordSelection.start], endPosition); // selecting the word 'paragraph' in the second paragraph

            expect(model.getSelection().getSelectedText()).toBe("paragraph");
            expect(commentCollection.getCommentNumber()).toBe(3);

            var commentInfo = { text: COMMENT_4_TEXT, mentions: undefined };
            COMMENT_6_ID = await commentCollection.insertThread(null, commentInfo);
            expect(commentCollection.getCommentNumber()).toBe(4);
        });

        it('should have 3 comment threads after inserting the new thread', function () {
            expectCommentIds(COMMENT_1_ID, COMMENT_3_ID, COMMENT_6_ID);
        });

        it('should have a range marker end node at the specified end position', function () {
            var domPoint = Position.getDOMPosition(model.getNode(), Position.increaseLastIndex(endPosition), true); // increasing because of selection and range start marker
            expect(DOM.isRangeMarkerEndNode(domPoint.node)).toBeTrue();
        });

        it('should have a comment place holder node directly behind the specified end position', function () {
            var commentPosition = Position.increaseLastIndex(endPosition, 2);
            var domPoint = Position.getDOMPosition(model.getNode(), commentPosition, true);
            expect(DOM.isCommentPlaceHolderNode(domPoint.node)).toBeTrue();
        });

        it('should have 1 comment place holder node in the DOM belonging to the comment', function () {
            var rangeMarkerSelector = '.commentplaceholder[data-container-id=' + COMMENT_6_ID + ']';
            var rangeMarkerNodes = model.getNode().find(rangeMarkerSelector);

            expect(rangeMarkerNodes).toHaveLength(1);
        });

        it('should have 2 range marker nodes in the DOM belonging to the comment', function () {
            var rangeMarkerSelector = '.rangemarker[data-range-id=' + COMMENT_6_ID + ']';
            var rangeMarkerNodes = model.getNode().find(rangeMarkerSelector);

            expect(rangeMarkerNodes).toHaveLength(2);
        });

        it('should remove the new comment when the position of the comment placeholder node is removed', async function () {

            var commentPosition = Position.increaseLastIndex(endPosition, 2);

            // setting a selection that includes the comment placeholder node
            model.getSelection().setTextSelection(Position.increaseLastIndex(commentPosition, -2), Position.increaseLastIndex(commentPosition, 2));
            expect(commentCollection.getCommentNumber()).toBe(4);

            await model.deleteSelected();
            expect(commentCollection.getCommentNumber()).toBe(3);
        });

        it('should have 2 comment threads after deleting the new inserted comment thread', function () {
            expectCommentIds(COMMENT_1_ID, COMMENT_3_ID);
        });

        it('should have 0 comment place holder node in the DOM belonging to the comment after deleting the new inserted comment thread', function () {
            var rangeMarkerSelector = '.commentplaceholder[data-container-id=' + COMMENT_6_ID + ']';
            var rangeMarkerNodes = model.getNode().find(rangeMarkerSelector);

            expect(rangeMarkerNodes).toHaveLength(0);
        });

        it('should have 0 range marker nodes in the DOM belonging to the comment after deleting the new inserted comment thread', function () {
            var rangeMarkerSelector = '.rangemarker[data-range-id=' + COMMENT_6_ID + ']';
            var rangeMarkerNodes = model.getNode().find(rangeMarkerSelector);

            expect(rangeMarkerNodes).toHaveLength(0);
        });
    });
});

// startNewThread for temporary comment
// -> using a new test scenario, because the temporary node cannot be removed in unit test environment

describe('Text class TextCommentsPane with new comments in document', function () {

    // private helpers ----------------------------------------------------

    var
        COMMENT_1_ID = 'cmt12340001',
        COMMENT_2_ID = 'cmt12340002',
        COMMENT_3_ID = 'cmt56780003',

        COMMENT_1_DATE = '2020-08-20T09:05:00Z',
        COMMENT_2_DATE = '2020-08-20T09:06:00Z',
        COMMENT_3_DATE = '2020-08-20T09:07:00Z',

        COMMENT_1_TEXT = 'Text in comment 1',
        COMMENT_2_TEXT = 'Text in comment 2',
        COMMENT_3_TEXT = 'Text in comment 3',

        PARA_1_TEXT = 'This is the first paragraph!',
        PARA_2_TEXT = 'This is the second paragraph!',
        PARA_3_TEXT = 'This is the third paragraph!',
        PARA_4_TEXT = 'This is the fourth paragraph!',
        PARA_5_TEXT = 'This is the fifth paragraph!',

        PROVIDER_ID = 'ox',

        USER_ID_1 = '1234',
        USER_ID_2 = '5678',

        AUTHOR_1 = 'User A',
        AUTHOR_2 = 'User B',

        //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
            { name: 'insertParagraph', start: [0] },
            { name: 'insertText', start: [0, 0], text: PARA_1_TEXT },
            { name: 'insertParagraph', start: [1] },
            { name: 'insertText', start: [1, 0], text: PARA_2_TEXT },
            { name: 'insertParagraph', start: [2] },
            { name: 'insertText', start: [2, 0], text: PARA_3_TEXT },
            { name: 'insertParagraph', start: [3] },
            { name: 'insertText', start: [3, 0], text: PARA_4_TEXT },
            { name: 'insertParagraph', start: [4] },
            { name: 'insertText', start: [4, 0], text: PARA_5_TEXT },
            // inserting the first main comment
            { name: 'insertRange', start: [1, 12], position: 'start', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertRange', start: [1, 18], position: 'end', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertComment', id: COMMENT_1_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_1_DATE, start: [1, 19], text: COMMENT_1_TEXT },
            // inserting the first child comment
            { name: 'insertRange', start: [1, 13], position: 'start', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertRange', start: [1, 21], position: 'end', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertComment', id: COMMENT_2_ID, parentId: COMMENT_1_ID, author: AUTHOR_2, userId: USER_ID_2, providerId: PROVIDER_ID, date: COMMENT_2_DATE, start: [1, 22], text: COMMENT_2_TEXT },
            // inserting a second main comment
            { name: 'insertRange', start: [3, 12], position: 'start', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertRange', start: [3, 18], position: 'end', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertComment', id: COMMENT_3_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_3_DATE, start: [3, 19], text: COMMENT_3_TEXT }
        ],

        textCommentsPane = null,
        model = null,
        commentLayer = null,
        commentCollection = null,
        commentFrame = null,
        temporaryThreadNode = null;

    createTextApp('ooxml', OPERATIONS, { realView: true }).done(function (docApp) {
        textCommentsPane = docApp.docView.commentsPane;
        model = docApp.docModel;
        commentLayer = model.getCommentLayer();
        commentCollection = commentLayer.getCommentCollection();
    });

    function expectCommentIds(...expectedIds) {
        var iterator = textCommentsPane.commentFrames();
        var commentIds = Array.from(iterator, frame => frame.getThreadComment().getId());
        expect(commentIds).toEqual(expectedIds);
    }

    // public methods -----------------------------------------------------

    // inserting a new comment thread
    describe('method commentCollection.startNewThread', function () {

        it('should have 2 comment threads before inserting the new comment thread', function () {
            expectCommentIds(COMMENT_1_ID, COMMENT_3_ID);
        });

        it('should have 2 comment thread nodes in the comments node before inserting the new comment thread', function () {
            var commentsNode = textCommentsPane.getCommentsContainer();
            var commentThreadNodes = commentsNode.children('.thread');
            expect(commentThreadNodes).toHaveLength(2);
            expect($(commentThreadNodes[0]).attr('data-comment-id')).toBe(COMMENT_1_ID);
            expect($(commentThreadNodes[1]).attr('data-comment-id')).toBe(COMMENT_3_ID);
        });

        it('should return that no comment editor is active', function () {
            expect(commentLayer.isCommentEditorActive()).toBeFalse();
        });

        it('should start a new comment thread with a temporary comment model behind the first main comment', async function () {

            // setting a selection in the second paragraph behind the first main comment
            var secondParagraph = model.getNode().find('div.p')[1];
            var localPosition = 30; // the word paragraph should be around position 30
            var wordSelection = Position.getWordSelection(secondParagraph, localPosition);

            expect(wordSelection.text).toBe("paragraph");

            var selectionStartPosition = [1, wordSelection.start];
            var selectionEndPosition = [1, wordSelection.end];

            // setting a selection -> this determines the position in the sorted container of comment models
            model.getSelection().setTextSelection(selectionStartPosition, selectionEndPosition); // selecting the word 'paragraph' in the second paragraph

            expect(model.getSelection().getSelectedText()).toBe("paragraph");
            expect(commentCollection.getTemporaryModel()).toBeNull();
            expect(commentCollection.getSelectedCommentId()).toBeNull();

            await commentCollection.startNewThread();
            expect(commentCollection.getCommentNumber()).toBe(4);
            expect(commentCollection.getTemporaryModel()).not.toBeNull();
            expect(commentCollection.getSelectedCommentId()).toBe(commentCollection.getTemporaryCommentId());
        });

        it('should still iterate over 2 comment threads after inserting the new thread', function () {
            expectCommentIds(COMMENT_1_ID, COMMENT_3_ID);
        });

        it('should have 3 comment thread nodes in the comments node after inserting the new comment thread', function () {
            var commentsNode = textCommentsPane.getCommentsContainer();
            var commentThreadNodes = commentsNode.children('.thread');
            expect(commentThreadNodes).toHaveLength(3);
            expect($(commentThreadNodes[0]).attr('data-comment-id')).toBe(COMMENT_1_ID);
            expect($(commentThreadNodes[1]).attr('data-comment-id')).toBe(commentCollection.getTemporaryCommentId());
            expect($(commentThreadNodes[2]).attr('data-comment-id')).toBe(COMMENT_3_ID);
        });

        it('should find a comment thread by the temporary comment id', function () {
            commentFrame = textCommentsPane.getThreadedCommentFrameById(commentCollection.getTemporaryCommentId());
            expect(commentFrame).toBeDefined();
            expect(commentFrame).toBeInstanceOf(CommentFrame);
            temporaryThreadNode = commentFrame.getNode();
            expect(temporaryThreadNode.hasClass('selected')).toBeTrue();

        });

        it('should create a selected temporary comment thread node', function () {
            temporaryThreadNode = commentFrame.getNode();
            expect(temporaryThreadNode.hasClass('selected')).toBeTrue();
        });

        it('should contain an invisible text node as child', function () {
            var textNode = temporaryThreadNode.find('.text');
            expect(textNode).toHaveLength(1);
            expect(textNode.css('display')).toBe("none");
        });

        it('should contain an active comment editor as child', function () {
            var commentEditor = temporaryThreadNode.find('.commentEditor');
            expect(commentEditor).toHaveLength(1);
        });

        it('should return that a comment editor is active', function () {
            expect(commentLayer.isCommentEditorActive()).toBeTrue();
        });
    });
});
