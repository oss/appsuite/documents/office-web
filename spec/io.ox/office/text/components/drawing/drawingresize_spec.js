/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { transformOpColor, Color } from '@/io.ox/office/editframework/utils/color';
import { drawDrawingSelection, updateOverlaySelection } from '@/io.ox/office/text/components/drawing/drawingresize';

import { createTextApp } from '~/text/apphelper';

// class DrawingResize ====================================================

describe('Text class DrawingResize', function () {

    // private helpers ----------------------------------------------------
    var model = null;
    var selection = null;

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes',
            attrs: {
                document:  { defaultTabStop: 1270, zoom: { value: 100 } },
                page:      { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 },
                character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' },
                paragraph: { lineHeight: { type: 'percent', value: 119 }, marginBottom: 352 }
            }
        },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading1', styleName: 'heading 1',
            attrs: {
                character: { bold: true, fontName: 'Times New Roman', fontSize: 14, color: transformOpColor(Color.ACCENT1, { shade: 74902 }) },
                paragraph: { marginTop: 846, outlineLevel: 0, nextStyleId: 'Normal' }
            },
            parent: 'Normal',
            uiPriority: 9
        },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', text: 'Hello World.', start: [0, 0] },
        { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { drawing: { name: 'TextBox 1', width: 5000, height: 1000 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'none' } } },
        { name: 'setAttributes', start: [0, 0], attrs: { drawing: { inline: false, anchorHorBase: 'column', anchorVertBase: 'paragraph', anchorHorAlign: 'right', anchorHorOffset: 0, anchorVertOffset: 0 } } },
        { name: 'insertParagraph', start: [0, 0, 0], attrs: { character: { color: Color.LIGHT1 }, paragraph: { alignment: 'center' } } }
    ];

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        selection = model.getSelection();
    });

    // public methods -----------------------------------------------------

    describe('method drawDrawingSelection', function () {

        it('should have a function "drawDrawingSelection"', function () {
            expect(drawDrawingSelection).toBeFunction();
        });

        it('should have a function "updateOverlaySelection"', function () {
            expect(updateOverlaySelection).toBeFunction();
        });

        it('should generate a selection inside the selection overlay node', function () {

            selection.setTextSelection([0, 0], [0, 1]);

            var selectionOverlayNode = model.getNode().children('.drawingselection-overlay');

            // there must be one (and only one) overlay node for the selection
            expect(selectionOverlayNode).toHaveLength(1);

            // there must be one drawing selection inside this overlay node
            var drawingSelection = selectionOverlayNode.children();
            expect(drawingSelection).toHaveLength(1);

            var firstDrawing = selection.getSelectedDrawing();
            var selectionDrawing = firstDrawing.data('selection');

            // one drawing selected with one registered drawing selection
            expect(firstDrawing).toHaveLength(1);
            expect(selectionDrawing).toHaveLength(1);

            // the one existing drawing selection must be registered at the drawing in the document
            expect(selectionDrawing[0]).toBe(drawingSelection[0]);
        });

        it('should remove the selection in the selection overlay node when drawing is inline', function () {

            selection.setTextSelection([0, 0], [0, 1]);

            var selectionOverlayNode = model.getNode().children('.drawingselection-overlay');

            // there must be one (and only one) overlay node for the selection
            expect(selectionOverlayNode).toHaveLength(1);

            // switching the selected drawing to be inline
            model.anchorDrawingTo('inline');

            var firstDrawing = selection.getSelectedDrawing();
            expect(firstDrawing).toHaveLength(1);

            // there must be no selection inside the overlay node for 'inline' drawings
            expect(selectionOverlayNode.children()).toHaveLength(0);

        });

        it('should insert the selection into the selection overlay node when drawing is aligned with page', function () {

            selection.setTextSelection([0, 0], [0, 1]);

            var selectionOverlayNode = model.getNode().children('.drawingselection-overlay');

            // there must be one (and only one) overlay node for the selection
            expect(selectionOverlayNode).toHaveLength(1);

            // switching the selected drawing to be aligned with page
            model.anchorDrawingTo('page');

            // there must be one drawing selection inside this overlay node
            var drawingSelection = selectionOverlayNode.children();
            expect(drawingSelection).toHaveLength(1);

            var firstDrawing = selection.getSelectedDrawing();
            var selectionDrawing = firstDrawing.data('selection');

            // one drawing selected with one registered drawing selection
            expect(firstDrawing).toHaveLength(1);
            expect(selectionDrawing).toHaveLength(1);

            // the one existing drawing selection must be registered at the drawing in the document
            expect(selectionDrawing[0]).toBe(drawingSelection[0]);
        });

        it('should insert the selection into the selection overlay node when drawing is aligned with paragraph', function () {

            selection.setTextSelection([0, 0], [0, 1]);

            var selectionOverlayNode = model.getNode().children('.drawingselection-overlay');

            // there must be one (and only one) overlay node for the selection
            expect(selectionOverlayNode).toHaveLength(1);

            // switching the selected drawing to be aligned with paragraph
            model.anchorDrawingTo('paragraph');

            // there must be one drawing selection inside this overlay node
            var drawingSelection = selectionOverlayNode.children();
            expect(drawingSelection).toHaveLength(1);

            var firstDrawing = selection.getSelectedDrawing();
            var selectionDrawing = firstDrawing.data('selection');

            // one drawing selected with one registered drawing selection
            expect(firstDrawing).toHaveLength(1);
            expect(selectionDrawing).toHaveLength(1);

            // the one existing drawing selection must be registered at the drawing in the document
            expect(selectionDrawing[0]).toBe(drawingSelection[0]);
        });

        it('should remove the drawing selection from selection overlay if there is a cursor selection', function () {

            selection.setTextSelection([0, 0], [0, 1]); // -> text selection

            var selectionOverlayNode = model.getNode().children('.drawingselection-overlay');

            // there must be one (and only one) overlay node for the selection
            expect(selectionOverlayNode).toHaveLength(1);

            // there must be one drawing selection inside this overlay node
            expect(selectionOverlayNode.children()).toHaveLength(1);

            // setting text selection
            selection.setTextSelection([0, 0], [0, 0]);

            // no more drawing selection
            expect(model.getSelection().isAnyDrawingSelection()).toBeFalse();

            // there must be no drawing selection inside this overlay node
            expect(selectionOverlayNode.children()).toHaveLength(0);
        });

        it('should remove the drawing selection from selection overlay if the drawing is deleted', async function () {

            selection.setTextSelection([0, 0], [0, 1]); // -> selecting the drawing

            var selectionOverlayNode = model.getNode().children('.drawingselection-overlay');

            // there must be one (and only one) overlay node for the selection
            expect(selectionOverlayNode).toHaveLength(1);

            // switching the selected drawing to be aligned with paragraph
            model.anchorDrawingTo('paragraph');

            // there must be one drawing selection inside this overlay node
            var drawingSelection = selectionOverlayNode.children();
            expect(drawingSelection).toHaveLength(1);

            var selectedDrawing = selection.getSelectedDrawing();
            var selectionDrawing = selectedDrawing.data('selection');

            // one drawing selected with one registered drawing selection
            expect(selectedDrawing).toHaveLength(1);
            expect(selectionDrawing).toHaveLength(1);

            // the one existing drawing selection must be registered at the drawing in the document
            expect(selectionDrawing[0]).toBe(drawingSelection[0]);

            // deleting the selected drawing
            await model.deleteSelected({ deleteKey: true });

            // there must be no more drawing selection inside the overlay node
            expect(selectionOverlayNode.children()).toHaveLength(0);
        });

        it('should restore the drawing selection in the selection overlay after undoing the deleting of the drawing', async function () {

            selection.setTextSelection([0, 0], [0, 0]); // -> text selection

            var selectionOverlayNode = model.getNode().children('.drawingselection-overlay');

            // there must be one (and only one) overlay node for the selection
            expect(selectionOverlayNode).toHaveLength(1);

            // no more drawing selection
            expect(model.getSelection().isAnyDrawingSelection()).toBeFalse();

            // there must be no drawing selection inside this overlay node
            expect(selectionOverlayNode.children()).toHaveLength(0);

            // restoring the drawing and the drawing selection via undo
            await model.getUndoManager().undo();

            // there must be a drawing selection after undo of deleting a drawing
            expect(model.getSelection().isAnyDrawingSelection()).toBeTrue();

            // there must be one drawing selection inside this overlay node
            var drawingSelection = selectionOverlayNode.children();
            expect(drawingSelection).toHaveLength(1);

            var selectedDrawing = selection.getSelectedDrawing();
            var selectionDrawing = selectedDrawing.data('selection');

            // one drawing selected with one registered drawing selection
            expect(selectedDrawing).toHaveLength(1);
            expect(selectionDrawing).toHaveLength(1);

            // the one existing drawing selection must be registered at the drawing in the document
            expect(selectionDrawing[0]).toBe(drawingSelection[0]);
        });
    });
});
