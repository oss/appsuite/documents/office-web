/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { createTextApp } from '~/text/apphelper';
import CommentCollection from '@/io.ox/office/text/model/comment/commentcollection';
import CommentLayer from '@/io.ox/office/text/components/comment/commentlayer';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Position from '@/io.ox/office/textframework/utils/position';

// class CommentLayer =====================================================

describe('Text class CommentLayer without comments in the document', function () {

    // private helpers ----------------------------------------------------

    var
        //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } }
        ],

        COMMENT_1_TEXT = 'Text in comment 1',
        COMMENT_1_TEXT_CHANGED = 'Changed text in comment 1',
        COMMENT_2_TEXT = 'Text in comment 2',
        COMMENT_3_TEXT = 'Text in comment 3',

        NEW_COMMENT_1_ID = null,
        NEW_COMMENT_2_ID = null,
        NEW_COMMENT_3_ID = null,

        firstParagraph = null,
        commentPlaceHolderNode = null,
        model = null,
        commentLayer = null,
        commentCollection = null,
        commentModel_1 = null,
        commentModel_2 = null,
        commentModel_3 = null;

    createTextApp('ooxml', OPERATIONS, { realView: true }).done(function (app) {
        model = app.getModel();
        commentLayer = model.getCommentLayer();
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(CommentLayer).toBeFunction();
    });

    // constructor --------------------------------------------------------
    describe('constructor', function () {
        it('should create a CommentLayer class instance', function () {
            expect(commentLayer).toBeInstanceOf(CommentLayer);
        });
    });

    // public methods -----------------------------------------------------

    describe('method DOM.isImplicitParagraphNode', function () {
        it('should exist', function () {
            expect(DOM.isImplicitParagraphNode).toBeFunction();
        });
        it('should be true for the first paragraph in the document', function () {
            firstParagraph = Position.getParagraphElement(model.getNode(), [0]);
            expect(DOM.isImplicitParagraphNode(firstParagraph)).toBeTrue();
        });
    });

    describe('method isEmpty', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('isEmpty');
        });
        it('should return that the collection is empty', function () {
            expect(commentLayer.isEmpty()).toBeTrue();
        });
    });

    describe('method getCommentNumber', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('getCommentNumber');
        });
        it('should return that there is no comment in the collection', function () {
            expect(commentLayer.getCommentNumber()).toBe(0);
        });
    });

    describe('method getCommentCollection', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('getCommentCollection');
        });
        it('should return an empty array', function () {
            commentCollection = commentLayer.getCommentCollection();
            expect(commentCollection).toBeInstanceOf(CommentCollection);
            expect(commentCollection.isEmpty()).toBeTrue();
        });
    });

    describe('method getDisplayMode', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('getDisplayMode');
        });
        it('should return the hidden display mode', function () {
            expect(commentLayer.getDisplayMode()).toBe(commentLayer.getHiddenDisplayMode());
        });
    });

    describe('method isHiddenDisplayModeActive', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('isHiddenDisplayModeActive');
        });
        it('should return that the hidden display mode is active', function () {
            expect(commentLayer.isHiddenDisplayModeActive()).toBeTrue();
        });
    });

    describe('method isCommentEditorActive', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('isCommentEditorActive');
        });
        it('should return that the comment editor is not active', function () {
            expect(commentLayer.isCommentEditorActive()).toBeFalse();
        });
    });

    describe('method getCommentAuthor', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('getCommentAuthor');
        });
        it('should return the user operation name that is used in comment operations', function () {
            expect(commentLayer.getCommentAuthor()).toBe("author");
        });
    });

    describe('method getCommentAuthorUid', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('getCommentAuthorUid');
        });
        it('should return the user operation name that is used in comment operations', function () {
            expect(commentLayer.getCommentAuthorUid()).toBe("1227");
        });
    });

    describe('method getCommentAuthorCount', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('getCommentAuthorCount');
        });
        it('should return that the comment author count is zero', function () {
            expect(commentLayer.getCommentAuthorCount()).toBe(0);
        });
    });

    describe('method getOrCreateCommentLayerNode', function () {
        var commentLayerNode = null;
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('getOrCreateCommentLayerNode');
        });
        it('should return a jQueryfied comment layer node', function () {
            commentLayerNode = commentLayer.getOrCreateCommentLayerNode();
            expect(commentLayerNode).toBeInstanceOf($);
        });
        it('should return exactly one comment layer node', function () {
            expect(commentLayerNode).toHaveLength(1);
        });
        it('should return the comment layer node that is recognized as comment layer node', function () {
            expect(DOM.isCommentLayerNode(commentLayerNode)).toBeTrue();
        });
    });

    describe('method DOM.hasCommentPlaceHolderNode', function () {
        it('should exist', function () {
            expect(DOM.hasCommentPlaceHolderNode).toBeFunction();
        });
        it('should return no comment placeholder node after loading an empty document', function () {
            expect(DOM.hasCommentPlaceHolderNode(firstParagraph)).toBeFalse();
        });
    });

    describe('method getListOfUnfilteredAuthors', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('getListOfUnfilteredAuthors');
        });
        it('should return an empty list of unfiltered authors', function () {
            var unfilteredAuthorsList = commentLayer.getListOfUnfilteredAuthors();
            expect(unfilteredAuthorsList).toHaveLength(0);
        });
    });

    describe('method handleMissingRangeMarkersForPasting', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('handleMissingRangeMarkersForPasting');
        });
        it('should replace a single insertRange operation by insertText operation', function () {
            var OPS = [
                { name: 'insertRange', position: 'start', type: 'comment', id: 'cmt_1' }
            ];
            var modifiedOps = commentLayer.handleMissingRangeMarkersForPasting(OPS);
            expect(modifiedOps).toHaveLength(1);
            expect(modifiedOps[0].name).toBe("insertText");
        });
        it('should not replace a full set of insertRange operation by insertText operation', function () {
            var OPS = [
                { name: 'insertRange', position: 'start', type: 'comment', id: 'cmt_1' },
                { name: 'insertRange', position: 'end', type: 'comment', id: 'cmt_1' }
            ];
            var modifiedOps = commentLayer.handleMissingRangeMarkersForPasting(OPS);
            expect(modifiedOps).toHaveLength(2);
            expect(modifiedOps[0].name).toBe("insertRange");
            expect(modifiedOps[1].name).toBe("insertRange");
        });
        it('should replace insertRange operations by insertText operation if the comment id is different', function () {
            var OPS = [
                { name: 'insertRange', position: 'start', type: 'comment', id: 'cmt_1' },
                { name: 'insertRange', position: 'end', type: 'comment', id: 'cmt_2' }
            ];
            var modifiedOps = commentLayer.handleMissingRangeMarkersForPasting(OPS);
            expect(modifiedOps).toHaveLength(2);
            expect(modifiedOps[0].name).toBe("insertText");
            expect(modifiedOps[1].name).toBe("insertText");
        });
        it('should replace some insertRange operations by insertText operation if the comment id is different', function () {
            var OPS = [
                { name: 'insertRange', position: 'start', type: 'comment', id: 'cmt_1' },
                { name: 'insertRange', position: 'end', type: 'comment', id: 'cmt_2' },
                { name: 'insertRange', position: 'start', type: 'comment', id: 'cmt_3' },
                { name: 'insertRange', position: 'end', type: 'comment', id: 'cmt_1' }
            ];
            var modifiedOps = commentLayer.handleMissingRangeMarkersForPasting(OPS);
            expect(modifiedOps).toHaveLength(4);
            expect(modifiedOps[0].name).toBe("insertRange");
            expect(modifiedOps[1].name).toBe("insertText");
            expect(modifiedOps[2].name).toBe("insertText");
            expect(modifiedOps[3].name).toBe("insertRange");
        });
        it('should replace a single insertRange operation by insertText operation and generate a delete operation', function () {
            var OPS = [
                { name: 'insertRange', position: 'start', type: 'comment', id: 'cmt_1' }
            ];
            var generator =  model.createOperationGenerator();
            var modifiedOps = commentLayer.handleMissingRangeMarkersForPasting(OPS, true, generator);
            expect(modifiedOps).toHaveLength(1);
            expect(modifiedOps[0].name).toBe("insertText");
            var deleteOps = generator.getOperations();
            expect(deleteOps).toHaveLength(1);
            expect(deleteOps[0].name).toBe("delete");
        });
        it('should not replace a full set of insertRange operation by insertText operation and not generate delete operations', function () {
            var OPS = [
                { name: 'insertRange', position: 'start', type: 'comment', id: 'cmt_1' },
                { name: 'insertRange', position: 'end', type: 'comment', id: 'cmt_1' }
            ];
            var generator =  model.createOperationGenerator();
            var modifiedOps = commentLayer.handleMissingRangeMarkersForPasting(OPS, true, generator);
            expect(modifiedOps).toHaveLength(2);
            expect(modifiedOps[0].name).toBe("insertRange");
            expect(modifiedOps[1].name).toBe("insertRange");
            var deleteOps = generator.getOperations();
            expect(deleteOps).toHaveLength(0);
        });
        it('should replace insertRange operations by insertText operation if the comment id is different and generate two delete operations', function () {
            var OPS = [
                { name: 'insertRange', position: 'start', type: 'comment', id: 'cmt_1' },
                { name: 'insertRange', position: 'end', type: 'comment', id: 'cmt_2' }
            ];
            var generator =  model.createOperationGenerator();
            var modifiedOps = commentLayer.handleMissingRangeMarkersForPasting(OPS, true, generator);
            expect(modifiedOps).toHaveLength(2);
            expect(modifiedOps[0].name).toBe("insertText");
            expect(modifiedOps[1].name).toBe("insertText");
            var deleteOps = generator.getOperations();
            expect(deleteOps).toHaveLength(2);
            expect(deleteOps[0].name).toBe("delete");
            expect(deleteOps[1].name).toBe("delete");
        });
        it('should replace some insertRange operations by insertText operation if the comment id is different and generate two delete operations', function () {
            var OPS = [
                { name: 'insertRange', position: 'start', type: 'comment', id: 'cmt_1' },
                { name: 'insertRange', position: 'end', type: 'comment', id: 'cmt_2' },
                { name: 'insertRange', position: 'start', type: 'comment', id: 'cmt_3' },
                { name: 'insertRange', position: 'end', type: 'comment', id: 'cmt_1' }
            ];
            var generator =  model.createOperationGenerator();
            var modifiedOps = commentLayer.handleMissingRangeMarkersForPasting(OPS, true, generator);
            expect(modifiedOps).toHaveLength(4);
            expect(modifiedOps[0].name).toBe("insertRange");
            expect(modifiedOps[1].name).toBe("insertText");
            expect(modifiedOps[2].name).toBe("insertText");
            expect(modifiedOps[3].name).toBe("insertRange");
            var deleteOps = generator.getOperations();
            expect(deleteOps).toHaveLength(2);
            expect(deleteOps[0].name).toBe("delete");
            expect(deleteOps[1].name).toBe("delete");
        });
    });

    // inserting the first commment

    describe('method insertComment', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('insertComment');
        });
        it('should insert a new comment into the implicit paragraph', function () {
            NEW_COMMENT_1_ID = commentLayer.insertComment(commentCollection.getTemporaryCommentId(), 'User A', { text: COMMENT_1_TEXT, mentions: undefined });
            firstParagraph = Position.getParagraphElement(model.getNode(), [0]);
            expect(DOM.isImplicitParagraphNode(firstParagraph)).toBeFalse();
            expect(DOM.hasCommentPlaceHolderNode(firstParagraph)).toBeTrue();
        });
    });

    describe('method commentCollection.getById after inserting a comment', function () {
        it('should return the comment model of the new inserted comment with correct text', function () {
            commentModel_1 = commentCollection.getById(NEW_COMMENT_1_ID);
            expect(commentModel_1.getText()).toBe(COMMENT_1_TEXT);
        });
    });

    describe('method DOM.getTargetContainerId after inserting a comment', function () {
        it('should exist', function () {
            expect(DOM.getTargetContainerId).toBeFunction();
        });
        it('should return the correct ID for the place holder node saved in the comment model', function () {
            commentPlaceHolderNode = commentModel_1.getCommentPlaceHolderNode();
            expect(DOM.getTargetContainerId(commentPlaceHolderNode)).toBe(NEW_COMMENT_1_ID);
        });
        it('should return the correct ID for the place holder node found in the paragraph node', function () {
            commentPlaceHolderNode = $(firstParagraph).find('.commentplaceholder');
            expect(commentPlaceHolderNode).toHaveLength(1);
            expect(DOM.getTargetContainerId(commentPlaceHolderNode)).toBe(NEW_COMMENT_1_ID);
        });
    });

    describe('method getDisplayMode after inserting a comment', function () {
        it('should return the default display mode', function () {
            expect(commentLayer.getDisplayMode()).toBe(commentLayer.getDefaultDisplayMode());
        });
    });

    describe('method isHiddenDisplayModeActive after inserting a comment', function () {
        it('should return that the hidden display mode is not active', function () {
            expect(commentLayer.isHiddenDisplayModeActive()).toBeFalse();
        });
    });

    describe('method isEmpty after inserting a comment', function () {
        it('should return that the collection is not empty', function () {
            expect(commentLayer.isEmpty()).toBeFalse();
        });
    });

    describe('method getCommentNumber after inserting a comment', function () {
        it('should return that there is one comment in the collection', function () {
            expect(commentLayer.getCommentNumber()).toBe(1);
        });
    });

    describe('method getCommentAuthorCount after inserting a comment', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('getCommentAuthorCount');
        });
        it('should return that the comment author count is one', function () {
            expect(commentLayer.getCommentAuthorCount()).toBe(1);
        });
    });

    describe('method getListOfUnfilteredAuthors after inserting a comment', function () {
        it('should return a list of unfiltered authors with one author', function () {
            var unfilteredAuthorsList = commentLayer.getListOfUnfilteredAuthors();
            expect(unfilteredAuthorsList).toHaveLength(1);
        });
    });

    describe('checking the DOM structure after inserting a comment"', function () {
        it('should contain one comment node with the specified comment text', function () {
            var commentLayerNode = commentLayer.getOrCreateCommentLayerNode();
            var commentsNode = commentLayerNode.children('.comments-container');
            expect(commentsNode).toHaveLength(1);
            var commentThreads = commentsNode.children('.thread');
            expect(commentThreads).toHaveLength(1);
            var commentNodes = commentThreads.children('.comment');
            expect(commentNodes).toHaveLength(1);
            expect(commentNodes.find('.text').text()).toBe(COMMENT_1_TEXT);
        });
    });

    // changing the one and only comment

    describe('method changeComment', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('changeComment');
        });
        it('should change the one comment in the document', function () {
            commentLayer.changeComment(commentModel_1, { text: COMMENT_1_TEXT_CHANGED, mentions: undefined });
            expect(commentModel_1.getText()).toBe(COMMENT_1_TEXT_CHANGED);
        });
    });

    describe('checking the DOM structure after changing a comment"', function () {
        it('should contain one comment node with the changed comment text', function () {
            var commentLayerNode = commentLayer.getOrCreateCommentLayerNode();
            var commentsNode = commentLayerNode.children('.comments-container');
            expect(commentsNode).toHaveLength(1);
            var commentThreads = commentsNode.children('.thread');
            expect(commentThreads).toHaveLength(1);
            var commentNodes = commentThreads.children('.comment');
            expect(commentNodes).toHaveLength(1);
            expect(commentNodes.find('.text').text()).toBe(COMMENT_1_TEXT_CHANGED);
        });
    });

    // inserting the second commment as reply to the first comment

    describe('method insertComment for inserting a child comment', function () {
        it('should insert a new comment as reply to the first comment', function () {
            NEW_COMMENT_2_ID = commentLayer.insertComment(NEW_COMMENT_1_ID, 'User A', { text: COMMENT_2_TEXT, mentions: undefined });
            expect(DOM.hasCommentPlaceHolderNode(firstParagraph)).toBeTrue();
        });
    });

    describe('method commentCollection.getById after inserting a child comment', function () {
        it('should return the comment model of the new inserted comment with correct text', function () {
            commentModel_2 = commentCollection.getById(NEW_COMMENT_2_ID);
            expect(commentModel_2.getText()).toBe(COMMENT_2_TEXT);
        });
        it('should return the id of the first comment as parent id', function () {
            expect(commentModel_2.getParentId()).toBe(NEW_COMMENT_1_ID);
        });
    });

    describe('method DOM.getTargetContainerId after inserting a child comment', function () {
        it('should return the correct ID for the place holder node saved in the second comment model', function () {
            commentPlaceHolderNode = commentModel_2.getCommentPlaceHolderNode();
            expect(DOM.getTargetContainerId(commentPlaceHolderNode)).toBe(NEW_COMMENT_2_ID);
        });
        it('should return the correct ID for the second place holder node found in the paragraph node', function () {
            commentPlaceHolderNode = $(firstParagraph).find('.commentplaceholder');
            expect(commentPlaceHolderNode).toHaveLength(2);
            expect(DOM.getTargetContainerId(commentPlaceHolderNode.get(0))).toBe(NEW_COMMENT_1_ID);
            expect(DOM.getTargetContainerId(commentPlaceHolderNode.get(1))).toBe(NEW_COMMENT_2_ID);
        });
    });

    describe('method getCommentNumber after inserting a child comment', function () {
        it('should return that there are two comments in the collection', function () {
            expect(commentLayer.getCommentNumber()).toBe(2);
        });
    });

    describe('checking the DOM structure after inserting a child comment"', function () {
        it('should contain one comment node with the specified comment text', function () {
            var commentLayerNode = commentLayer.getOrCreateCommentLayerNode();
            var commentsNode = commentLayerNode.children('.comments-container');
            expect(commentsNode).toHaveLength(1);
            var commentThreads = commentsNode.children('.thread');
            expect(commentThreads).toHaveLength(1);
            var commentNodes = commentThreads.children('.comment');
            expect(commentNodes).toHaveLength(2);
            expect($(commentNodes.get(0)).find('.text').text()).toBe(COMMENT_1_TEXT_CHANGED);
            expect($(commentNodes.get(1)).find('.text').text()).toBe(COMMENT_2_TEXT);
        });
    });

    // inserting a third commment that is a second main comment

    describe('method insertComment for inserting a second main comment', function () {
        it('should insert a new comment that is the second main comment', function () {
            NEW_COMMENT_3_ID = commentLayer.insertComment(commentCollection.getTemporaryCommentId(), 'User A', { text: COMMENT_3_TEXT, mentions: undefined });
            expect(DOM.hasCommentPlaceHolderNode(firstParagraph)).toBeTrue();
        });
    });

    describe('method commentCollection.getById after inserting a second main comment', function () {
        it('should return the comment model of the new inserted main comment with correct text', function () {
            commentModel_3 = commentCollection.getById(NEW_COMMENT_3_ID);
            expect(commentModel_3.getText()).toBe(COMMENT_3_TEXT);
        });
        it('should return that there is no parent specified for the new comment', function () {
            expect(commentModel_3.getParentId()).toBe("");
        });
    });

    describe('method DOM.getTargetContainerId after inserting a second main comment', function () {
        it('should return the correct ID for the place holder node saved in the third comment model', function () {
            commentPlaceHolderNode = commentModel_3.getCommentPlaceHolderNode();
            expect(DOM.getTargetContainerId(commentPlaceHolderNode)).toBe(NEW_COMMENT_3_ID);
        });
        it('should return the correct ID for the third place holder node found in the paragraph node', function () {
            commentPlaceHolderNode = $(firstParagraph).find('.commentplaceholder');
            expect(commentPlaceHolderNode).toHaveLength(3);
            expect(DOM.getTargetContainerId(commentPlaceHolderNode.get(0))).toBe(NEW_COMMENT_1_ID);
            expect(DOM.getTargetContainerId(commentPlaceHolderNode.get(1))).toBe(NEW_COMMENT_3_ID);
            expect(DOM.getTargetContainerId(commentPlaceHolderNode.get(2))).toBe(NEW_COMMENT_2_ID);
        });
    });

    describe('method getCommentNumber after inserting a second main comment', function () {
        it('should return that there are three comments in the collection', function () {
            expect(commentLayer.getCommentNumber()).toBe(3);
        });
    });

    // deleting the second main comment

    describe('method deleteComment', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('deleteComment');
        });
        it('should delete the second main comment and its child', async function () {
            await commentLayer.deleteComment(commentModel_3);
            expect(commentLayer.getCommentNumber()).toBe(2);
        });
    });

    describe('method commentCollection.getById after deleting the second main comment', function () {
        it('should return the comment model of the new inserted main comment with correct text', function () {
            expect(commentCollection.getById(NEW_COMMENT_3_ID)).toBeNull();
        });
    });

    describe('method DOM.getTargetContainerId after deleting the second main comment', function () {
        it('should return that there is no more placeholder node in the paragraph', function () {
            commentPlaceHolderNode = $(firstParagraph).find('.commentplaceholder');
            expect(commentPlaceHolderNode).toHaveLength(2);
            expect(DOM.getTargetContainerId(commentPlaceHolderNode.get(0))).toBe(NEW_COMMENT_1_ID);
            expect(DOM.getTargetContainerId(commentPlaceHolderNode.get(1))).toBe(NEW_COMMENT_2_ID);
        });
    });

    // restoring the second main comment via undo

    describe('method insertComment for restoring the second main comment', function () {
        it('should restore the second main comment via undo', async function () {
            await model.getUndoManager().undo();
            expect(commentLayer.getCommentNumber()).toBe(3);
        });
    });

    describe('method commentCollection.getById after restoring the second main comment', function () {
        it('should return the comment model of the restored main comment with correct text', function () {
            commentModel_3 = commentCollection.getById(NEW_COMMENT_3_ID);
            expect(commentModel_3.getText()).toBe(COMMENT_3_TEXT);
        });
        it('should return that there is no parent specified for the new comment', function () {
            expect(commentModel_3.getParentId()).toBe("");
        });
    });

    describe('method DOM.getTargetContainerId after restoring the second main comment', function () {
        it('should return the correct ID for the place holder node saved in the third comment model', function () {
            commentPlaceHolderNode = commentModel_3.getCommentPlaceHolderNode();
            expect(DOM.getTargetContainerId(commentPlaceHolderNode)).toBe(NEW_COMMENT_3_ID);
        });
        it('should return the correct ID for the third place holder node found in the paragraph node', function () {
            commentPlaceHolderNode = $(firstParagraph).find('.commentplaceholder');
            expect(commentPlaceHolderNode).toHaveLength(3);
            expect(DOM.getTargetContainerId(commentPlaceHolderNode.get(0))).toBe(NEW_COMMENT_1_ID);
            expect(DOM.getTargetContainerId(commentPlaceHolderNode.get(1))).toBe(NEW_COMMENT_3_ID);
            expect(DOM.getTargetContainerId(commentPlaceHolderNode.get(2))).toBe(NEW_COMMENT_2_ID);
        });
    });

});

describe('Text class CommentLayer with comments in the document', function () {

    // private helpers ----------------------------------------------------

    var
        COMMENT_1_ID = 'cmt1234_1',
        COMMENT_2_ID = 'cmt1234_2',
        COMMENT_3_ID = 'cmt5678_3',

        COMMENT_1_DATE = '2020-08-20T09:05:00Z',
        COMMENT_2_DATE = '2020-08-20T09:06:00Z',
        COMMENT_3_DATE = '2020-08-20T09:07:00Z',

        COMMENT_1_TEXT = 'Text in comment 1',
        COMMENT_2_TEXT = 'Text in comment 2',
        COMMENT_3_TEXT = 'Text in comment 3',

        PARA_1_TEXT = 'This is the first paragraph!',
        PARA_2_TEXT = 'This is the second paragraph!',
        PARA_3_TEXT = 'This is the third paragraph!',
        PARA_4_TEXT = 'This is the fourth paragraph!',
        PARA_5_TEXT = 'This is the fifth paragraph!',

        PROVIDER_ID = 'ox',

        USER_ID_1 = '1234',
        USER_ID_2 = '5678',

        AUTHOR_1 = 'User A',
        AUTHOR_2 = 'User B',

        //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
            { name: 'insertParagraph', start: [0] },
            { name: 'insertText', start: [0, 0], text: PARA_1_TEXT },
            { name: 'insertParagraph', start: [1] },
            { name: 'insertText', start: [1, 0], text: PARA_2_TEXT },
            { name: 'insertParagraph', start: [2] },
            { name: 'insertText', start: [2, 0], text: PARA_3_TEXT },
            { name: 'insertParagraph', start: [3] },
            { name: 'insertText', start: [3, 0], text: PARA_4_TEXT },
            { name: 'insertParagraph', start: [4] },
            { name: 'insertText', start: [4, 0], text: PARA_5_TEXT },
            // inserting the first main comment
            { name: 'insertRange', start: [1, 12], position: 'start', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertRange', start: [1, 18], position: 'end', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertComment', id: COMMENT_1_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_1_DATE, start: [1, 19], text: COMMENT_1_TEXT },
            // inserting the first child comment
            { name: 'insertRange', start: [1, 13], position: 'start', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertRange', start: [1, 21], position: 'end', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertComment', id: COMMENT_2_ID, parentId: COMMENT_1_ID, author: AUTHOR_2, userId: USER_ID_2, providerId: PROVIDER_ID, date: COMMENT_2_DATE, start: [1, 22], text: COMMENT_2_TEXT },
            // inserting a second main comment
            { name: 'insertRange', start: [3, 12], position: 'start', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertRange', start: [3, 18], position: 'end', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertComment', id: COMMENT_3_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_3_DATE, start: [3, 19], text: COMMENT_3_TEXT }
        ],

        para_1 = null,
        para_2 = null,
        para_3 = null,
        para_4 = null,
        para_5 = null,
        model = null,
        commentLayer = null,
        commentCollection = null;

    createTextApp('ooxml', OPERATIONS, { realView: true }).done(function (app) {
        model = app.getModel();
        commentLayer = model.getCommentLayer();
        commentCollection = commentLayer.getCommentCollection();
    });

    // public methods -----------------------------------------------------

    describe('method DOM.isImplicitParagraphNode', function () {
        it('should be false for the first and the second paragraph in the document', function () {
            para_1 = Position.getParagraphElement(model.getNode(), [0]);
            para_2 = Position.getParagraphElement(model.getNode(), [1]);
            para_3 = Position.getParagraphElement(model.getNode(), [2]);
            para_4 = Position.getParagraphElement(model.getNode(), [3]);
            para_5 = Position.getParagraphElement(model.getNode(), [4]);
            expect(DOM.isImplicitParagraphNode(para_1)).toBeFalse();
            expect(DOM.isImplicitParagraphNode(para_2)).toBeFalse();
        });
    });

    describe('method isEmpty', function () {
        it('should return that the collection is not empty', function () {
            expect(commentLayer.isEmpty()).toBeFalse();
        });
    });

    describe('method getCommentNumber', function () {
        it('should return that there are 3 comment in the collection', function () {
            expect(commentLayer.getCommentNumber()).toBe(3);
        });
    });

    describe('method getCommentCollection', function () {
        it('should return a non-empty array', function () {
            commentCollection = commentLayer.getCommentCollection();
            expect(commentCollection).toBeInstanceOf(CommentCollection);
            expect(commentCollection.isEmpty()).toBeFalse();
        });
    });

    describe('method getDisplayMode', function () {
        it('should return the hidden display mode', function () {
            expect(commentLayer.getDisplayMode()).toBe(commentLayer.getDefaultDisplayMode());
        });
    });

    describe('method isHiddenDisplayModeActive', function () {
        it('should return that the hidden display mode is not active', function () {
            expect(commentLayer.isHiddenDisplayModeActive()).toBeFalse();
        });
    });

    describe('method isCommentEditorActive', function () {
        it('should return that the comment editor is not active', function () {
            expect(commentLayer.isCommentEditorActive()).toBeFalse();
        });
    });

    describe('method getCommentAuthorCount', function () {
        it('should return that the comment author count is two', function () {
            expect(commentLayer.getCommentAuthorCount()).toBe(2);
        });
    });

    describe('method getListOfUnfilteredAuthors', function () {
        it('should return a list with two unfiltered authors', function () {
            var unfilteredAuthorsList = commentLayer.getListOfUnfilteredAuthors();
            expect(unfilteredAuthorsList).toHaveLength(2);
            expect(unfilteredAuthorsList[0]).toBe(AUTHOR_1);
            expect(unfilteredAuthorsList[1]).toBe(AUTHOR_2);
        });
    });

    describe('method DOM.hasCommentPlaceHolderNode', function () {
        it('should return that there are comment placeholder nodes in the second paragraph after loading the document', function () {
            expect(DOM.hasCommentPlaceHolderNode(para_1)).toBeFalse();
            expect(DOM.hasCommentPlaceHolderNode(para_2)).toBeTrue();
            expect(DOM.hasCommentPlaceHolderNode(para_3)).toBeFalse();
            expect(DOM.hasCommentPlaceHolderNode(para_4)).toBeTrue();
            expect(DOM.hasCommentPlaceHolderNode(para_5)).toBeFalse();
        });
    });

    describe('method setAuthorFilter', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('setAuthorFilter');
        });
        it('should set an author filter', function () {
            commentLayer.setAuthorFilter([AUTHOR_1]);
            var unfilteredAuthorsList = commentLayer.getListOfUnfilteredAuthors();
            expect(unfilteredAuthorsList).toHaveLength(1);
            expect(unfilteredAuthorsList[0]).toBe(AUTHOR_1);
        });
        it('should set another author to the filter', function () {
            commentLayer.setAuthorFilter([AUTHOR_2]);
            var unfilteredAuthorsList = commentLayer.getListOfUnfilteredAuthors();
            expect(unfilteredAuthorsList).toHaveLength(1);
            expect(unfilteredAuthorsList[0]).toBe(AUTHOR_2);
        });
    });

    describe('method removeAuthorFilter', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('removeAuthorFilter');
        });
        it('should remove an existing author filter', function () {
            commentLayer.removeAuthorFilter([AUTHOR_2]);
            var unfilteredAuthorsList = commentLayer.getListOfUnfilteredAuthors();
            expect(unfilteredAuthorsList).toHaveLength(2);
            expect(unfilteredAuthorsList[0]).toBe(AUTHOR_1);
            expect(unfilteredAuthorsList[1]).toBe(AUTHOR_2);
        });
    });

});

describe('Text class CommentLayer with several comments in the document', function () {

    // private helpers ----------------------------------------------------

    var
        COMMENT_1_ID = 'cmt1234_1',
        COMMENT_2_ID = 'cmt1234_2',
        COMMENT_3_ID = 'cmt5678_3',
        COMMENT_4_ID = 'cmt5678_4',
        COMMENT_5_ID = 'cmt5678_5',
        COMMENT_6_ID = 'cmt5678_6',
        COMMENT_7_ID = 'cmt5678_7',

        COMMENT_1_DATE = '2020-08-20T09:05:00Z',
        COMMENT_2_DATE = '2020-08-20T09:06:00Z',
        COMMENT_3_DATE = '2020-08-20T09:07:00Z',
        COMMENT_4_DATE = '2020-08-20T09:08:00Z',
        COMMENT_5_DATE = '2020-08-20T09:09:00Z',
        COMMENT_6_DATE = '2020-08-20T09:10:00Z',
        COMMENT_7_DATE = '2020-08-20T09:11:00Z',

        COMMENT_1_TEXT = 'Text in comment 1',
        COMMENT_2_TEXT = 'Text in comment 2',
        COMMENT_3_TEXT = 'Text in comment 3',
        COMMENT_4_TEXT = 'Text in comment 4',
        COMMENT_5_TEXT = 'Text in comment 5',
        COMMENT_6_TEXT = 'Text in comment 6',
        COMMENT_7_TEXT = 'Text in comment 7',

        PARA_1_TEXT = 'This is the first paragraph!',
        PARA_2_TEXT = 'This is the second paragraph!',
        PARA_3_TEXT = 'This is the third paragraph!',
        PARA_4_TEXT = 'This is the fourth paragraph!',
        PARA_5_TEXT = 'This is the fifth paragraph!',

        PROVIDER_ID = 'ox',

        USER_ID_1 = '1234',
        USER_ID_2 = '5678',

        AUTHOR_1 = 'User A',
        AUTHOR_2 = 'User B',

        //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
            { name: 'insertParagraph', start: [0] },
            { name: 'insertText', start: [0, 0], text: PARA_1_TEXT },
            { name: 'insertParagraph', start: [1] },
            { name: 'insertText', start: [1, 0], text: PARA_2_TEXT },
            { name: 'insertParagraph', start: [2] },
            { name: 'insertText', start: [2, 0], text: PARA_3_TEXT },
            { name: 'insertParagraph', start: [3] },
            { name: 'insertText', start: [3, 0], text: PARA_4_TEXT },
            { name: 'insertParagraph', start: [4] },
            { name: 'insertText', start: [4, 0], text: PARA_5_TEXT },
            // inserting the first main comment
            { name: 'insertRange', start: [0, 12], position: 'start', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertRange', start: [0, 18], position: 'end', type: 'comment', id: COMMENT_1_ID },
            { name: 'insertComment', id: COMMENT_1_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_1_DATE, start: [0, 19], text: COMMENT_1_TEXT },
            // inserting the first child comment
            { name: 'insertRange', start: [0, 13], position: 'start', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertRange', start: [0, 21], position: 'end', type: 'comment', id: COMMENT_2_ID },
            { name: 'insertComment', id: COMMENT_2_ID, parentId: COMMENT_1_ID, author: AUTHOR_2, userId: USER_ID_2, providerId: PROVIDER_ID, date: COMMENT_2_DATE, start: [0, 22], text: COMMENT_2_TEXT },
            // inserting a second main comment
            { name: 'insertRange', start: [1, 12], position: 'start', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertRange', start: [1, 18], position: 'end', type: 'comment', id: COMMENT_3_ID },
            { name: 'insertComment', id: COMMENT_3_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_3_DATE, start: [1, 19], text: COMMENT_3_TEXT },
            // inserting a third main comment
            { name: 'insertRange', start: [2, 12], position: 'start', type: 'comment', id: COMMENT_4_ID },
            { name: 'insertRange', start: [2, 18], position: 'end', type: 'comment', id: COMMENT_4_ID },
            { name: 'insertComment', id: COMMENT_4_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_4_DATE, start: [2, 19], text: COMMENT_4_TEXT },
            // inserting the second child comment
            { name: 'insertRange', start: [2, 13], position: 'start', type: 'comment', id: COMMENT_5_ID },
            { name: 'insertRange', start: [2, 21], position: 'end', type: 'comment', id: COMMENT_5_ID },
            { name: 'insertComment', id: COMMENT_5_ID, parentId: COMMENT_4_ID, author: AUTHOR_2, userId: USER_ID_2, providerId: PROVIDER_ID, date: COMMENT_5_DATE, start: [2, 22], text: COMMENT_5_TEXT },
            // inserting a fourth main comment
            { name: 'insertRange', start: [3, 12], position: 'start', type: 'comment', id: COMMENT_6_ID },
            { name: 'insertRange', start: [3, 18], position: 'end', type: 'comment', id: COMMENT_6_ID },
            { name: 'insertComment', id: COMMENT_6_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_6_DATE, start: [3, 19], text: COMMENT_6_TEXT },
            // inserting a fifth main comment
            { name: 'insertRange', start: [4, 12], position: 'start', type: 'comment', id: COMMENT_7_ID },
            { name: 'insertRange', start: [4, 18], position: 'end', type: 'comment', id: COMMENT_7_ID },
            { name: 'insertComment', id: COMMENT_7_ID, author: AUTHOR_1, userId: USER_ID_1, providerId: PROVIDER_ID, date: COMMENT_7_DATE, start: [4, 19], text: COMMENT_7_TEXT }
        ],

        para_1 = null,
        para_2 = null,
        para_3 = null,
        para_4 = null,
        para_5 = null,
        model = null,
        commentLayer = null,
        commentCollection = null,
        commentsNode = null,
        bubbleLayerNode = null,
        rangeMarkerOverlayNode = null,
        commentModel_3 = null;

    createTextApp('ooxml', OPERATIONS, { realView: true }).done(function (app) {
        model = app.getModel();
        commentLayer = model.getCommentLayer();
        commentCollection = commentLayer.getCommentCollection();
        commentsNode = model.getNode().find('>.commentlayer>.comments-container');
        bubbleLayerNode = model.getNode().children('.commentbubblelayer');
        rangeMarkerOverlayNode = model.getNode().children('.rangemarkeroverlay');
    });

    // public methods -----------------------------------------------------

    describe('method DOM.isImplicitParagraphNode', function () {
        it('should be false for the first and the second paragraph in the document', function () {
            para_1 = Position.getParagraphElement(model.getNode(), [0]);
            para_2 = Position.getParagraphElement(model.getNode(), [1]);
            para_3 = Position.getParagraphElement(model.getNode(), [2]);
            para_4 = Position.getParagraphElement(model.getNode(), [3]);
            para_5 = Position.getParagraphElement(model.getNode(), [4]);
            expect(DOM.isImplicitParagraphNode(para_1)).toBeFalse();
            expect(DOM.isImplicitParagraphNode(para_2)).toBeFalse();
            expect(DOM.isImplicitParagraphNode(para_3)).toBeFalse();
            expect(DOM.isImplicitParagraphNode(para_4)).toBeFalse();
            expect(DOM.isImplicitParagraphNode(para_5)).toBeFalse();
        });
    });

    describe('method isEmpty', function () {
        it('should return that the collection is not empty', function () {
            expect(commentLayer.isEmpty()).toBeFalse();
        });
    });

    describe('method getCommentNumber', function () {
        it('should return that there are 7 comment in the collection', function () {
            expect(commentLayer.getCommentNumber()).toBe(7);
        });
    });

    describe('method getCommentCollection', function () {
        it('should return a non-empty array', function () {
            commentCollection = commentLayer.getCommentCollection();
            expect(commentCollection).toBeInstanceOf(CommentCollection);
            expect(commentCollection.isEmpty()).toBeFalse();
        });
    });

    describe('method isCommentEditorActive', function () {
        it('should return that the comment editor is not active', function () {
            expect(commentLayer.isCommentEditorActive()).toBeFalse();
        });
    });

    describe('method getCommentAuthorCount', function () {
        it('should return that the comment author count is two', function () {
            expect(commentLayer.getCommentAuthorCount()).toBe(2);
        });
    });

    describe('method getListOfUnfilteredAuthors', function () {
        it('should return a list with two unfiltered authors', function () {
            var unfilteredAuthorsList = commentLayer.getListOfUnfilteredAuthors();
            expect(unfilteredAuthorsList).toHaveLength(2);
            expect(unfilteredAuthorsList[0]).toBe(AUTHOR_1);
            expect(unfilteredAuthorsList[1]).toBe(AUTHOR_2);
        });
    });

    describe('method DOM.hasCommentPlaceHolderNode', function () {
        it('should return that there are comment placeholder nodes in all paragraphs after loading the document', function () {
            expect(DOM.hasCommentPlaceHolderNode(para_1)).toBeTrue();
            expect(DOM.hasCommentPlaceHolderNode(para_2)).toBeTrue();
            expect(DOM.hasCommentPlaceHolderNode(para_3)).toBeTrue();
            expect(DOM.hasCommentPlaceHolderNode(para_4)).toBeTrue();
            expect(DOM.hasCommentPlaceHolderNode(para_5)).toBeTrue();
        });
    });

    describe('method selectNextComment', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('selectNextComment');
        });
        it('should select the first main comment after pressing next', function () {
            expect(commentCollection.getSelectedCommentId()).toBeNull();
            commentLayer.selectNextComment();
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_1_ID);
        });
        it('should select the second main comment after pressing next', function () {
            commentLayer.selectNextComment();
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_3_ID);
        });
        it('should select the third main comment after pressing next', function () {
            commentLayer.selectNextComment();
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_4_ID);
        });
        it('should select the fourth main comment after pressing next', function () {
            commentLayer.selectNextComment();
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_6_ID);
        });
        it('should select the fifth main comment after pressing next', function () {
            commentLayer.selectNextComment();
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_7_ID);
        });
        it('should select the first main comment again after pressing next', function () {
            commentLayer.selectNextComment();
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_1_ID);
        });
        it('should select the fifth main comment again after pressing previous', function () {
            commentLayer.selectNextComment({ next: false });
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_7_ID);
        });
        it('should select the fourth main comment again after pressing previous', function () {
            commentLayer.selectNextComment({ next: false });
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_6_ID);
        });
        it('should select the third main comment again after pressing previous', function () {
            commentLayer.selectNextComment({ next: false });
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_4_ID);
        });
        it('should select the second main comment again after pressing previous', function () {
            commentLayer.selectNextComment({ next: false });
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_3_ID);
        });
        it('should select the first main comment again after pressing previous', function () {
            commentLayer.selectNextComment({ next: false });
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_1_ID);
        });
        it('should select the last main comment again after pressing previous', function () {
            commentLayer.selectNextComment({ next: false });
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_7_ID);
        });
        it('should select the first main comment again after pressing next once more', function () {
            commentLayer.selectNextComment();
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_1_ID);
        });
        it('should have no comment selection after deselecting the comment', function () {
            commentCollection.deselectComment();
            expect(commentCollection.getSelectedCommentId()).toBeNull();
        });
        it('should select the last main comment again after pressing previous without selected comment', function () {
            commentLayer.selectNextComment({ next: false });
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_7_ID);
        });
        it('should have no comment selection after deselecting the comment again', function () {
            commentCollection.deselectComment();
            expect(commentCollection.getSelectedCommentId()).toBeNull();
        });
    });

    // tests for the display mode

    describe('method getDisplayMode', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('getDisplayMode');
        });
        it('should return the default display mode is active', function () {
            expect(commentLayer.getDisplayMode()).toBe(commentLayer.getDefaultDisplayMode());
        });
    });

    describe('method isBubbleMode', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('isBubbleMode');
        });
    });

    describe('method isHiddenDisplayModeActive', function () {
        it('should return that the hidden display mode is not active', function () {
            expect(commentLayer.isHiddenDisplayModeActive()).toBeFalse();
        });
    });

    describe('method switchCommentDisplayMode', function () {
        it('should exist', function () {
            expect(CommentLayer).toHaveMethod('switchCommentDisplayMode');
        });

        // switching to the "selected" mode
        it('should switch to selected mode for the comment layer', function () {
            commentLayer.switchCommentDisplayMode(CommentLayer.DISPLAY_MODE.SELECTED);
            expect(commentLayer.getDisplayMode()).toBe(CommentLayer.DISPLAY_MODE.SELECTED);
        });
        it('should have five threads in the comments node inside the comment layer', function () {
            expect(commentsNode).toHaveLength(1);
            expect(commentsNode.children()).toHaveLength(5);
            expect(commentsNode.children('.thread')).toHaveLength(5);
        });
        it('should have seven comments in the comments node inside the comment layer', function () {
            expect(commentsNode.find('.comment')).toHaveLength(7);
        });
        it('should have no children in bubble layer node after switching to "selected" mode', function () {
            expect(bubbleLayerNode).toHaveLength(1);
            expect(bubbleLayerNode.children()).toHaveLength(0);
        });
        it('should show that the bubble mode is not active after switching to "selected" mode', function () {
            expect(commentLayer.isBubbleMode()).toBeFalse();
        });
        it('should show that the hidden mode is not active after switching to "selected" mode', function () {
            expect(commentLayer.isHiddenDisplayModeActive()).toBeFalse();
        });
        it('should have no content in rangemarker overlay node after switching to "selected" mode', function () {
            expect(rangeMarkerOverlayNode).toHaveLength(1);
            expect(rangeMarkerOverlayNode.children()).toHaveLength(0); // no comment was selected before
        });
        it('should have content in rangemarker overlay node after selecting a comment in the "selected" mode', function () {
            commentModel_3 = commentCollection.getById(COMMENT_3_ID);
            expect(commentModel_3.getText()).toBe(COMMENT_3_TEXT);
            commentCollection.selectComment(commentModel_3);
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_3_ID);
            expect(rangeMarkerOverlayNode.children().length).toBeGreaterThan(1);
            expect(rangeMarkerOverlayNode.children('.isRangeMarker').length).toBeGreaterThan(0); // highlighting the node
            expect(rangeMarkerOverlayNode.children('.isCommentConnectionLine').length).toBeGreaterThan(0); // line markers
        });
        it('should have no more content in rangemarker overlay node after deselecting the comment in the "selected" mode', function () {
            commentCollection.deselectComment();
            expect(commentCollection.getSelectedCommentId()).toBeNull();
            expect(rangeMarkerOverlayNode.children()).toHaveLength(0);
        });

        // switching to the "bubble" mode
        it('should switch to bubble mode for the comment layer', function () {
            commentLayer.switchCommentDisplayMode(CommentLayer.DISPLAY_MODE.BUBBLES);
            expect(commentLayer.getDisplayMode()).toBe(CommentLayer.DISPLAY_MODE.BUBBLES);
        });
        it('should still have five threads in the comments node inside the comment layer after switching to "bubble" mode', function () {
            expect(commentsNode.children()).toHaveLength(5);
            expect(commentsNode.children('.thread')).toHaveLength(5);
        });
        it('should still have seven comments in the comments node inside the comment layer after switching to "bubble" mode', function () {
            expect(commentsNode.find('.comment')).toHaveLength(7);
        });
        it('should have 5 children in bubble layer node after switching to "bubble" mode', function () {
            expect(bubbleLayerNode.children()).toHaveLength(5);
        });
        it('should show that the bubble mode is active after switching to "bubble" mode', function () {
            expect(commentLayer.isBubbleMode()).toBeTrue();
        });
        it('should show that the hidden mode is not active after switching to "bubble" mode', function () {
            expect(commentLayer.isHiddenDisplayModeActive()).toBeFalse();
        });
        it('should have no content in rangemarker overlay node after switching to "bubble" mode', function () {
            expect(rangeMarkerOverlayNode.children()).toHaveLength(0);
        });
        it('should have content in rangemarker overlay node after selecting a comment in the "bubble" mode', function () {
            commentCollection.selectComment(commentModel_3);
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_3_ID);
            expect(rangeMarkerOverlayNode.children().length).toBeGreaterThan(0);
            expect(rangeMarkerOverlayNode.children('.isRangeMarker').length).toBeGreaterThan(0); // highlighting the node
            expect(rangeMarkerOverlayNode.children('.isCommentConnectionLine')).toHaveLength(0); // no line markers in bubble mode
        });
        it('should have no more content in rangemarker overlay node after deselecting the comment in the "bubble" mode', function () {
            commentCollection.deselectComment();
            expect(commentCollection.getSelectedCommentId()).toBeNull();
            expect(rangeMarkerOverlayNode.children()).toHaveLength(0);
        });

        // switching to the "none" mode
        it('should switch to "none" mode for the comment layer', function () {
            commentLayer.switchCommentDisplayMode(CommentLayer.DISPLAY_MODE.NONE);
            expect(commentLayer.getDisplayMode()).toBe(CommentLayer.DISPLAY_MODE.NONE);
        });
        it('should still have five threads in the comments node inside the comment layer after switching to "none" mode', function () {
            expect(commentsNode.children()).toHaveLength(5);
            expect(commentsNode.children('.thread')).toHaveLength(5);
        });
        it('should still have seven comments in the comments node inside the comment layer after switching to "none" mode', function () {
            expect(commentsNode.find('.comment')).toHaveLength(7);
        });
        it('should have 0 children in bubble layer node after switching to "none" mode', function () {
            expect(bubbleLayerNode.children()).toHaveLength(0);
        });
        it('should show that the bubble mode is not active after switching to "none" mode', function () {
            expect(commentLayer.isBubbleMode()).toBeFalse();
        });
        it('should show that the hidden mode is not active after switching to "none" mode', function () {
            expect(commentLayer.isHiddenDisplayModeActive()).toBeFalse();
        });
        it('should have no content in rangemarker overlay node after switching to "none" mode', function () {
            expect(rangeMarkerOverlayNode.children()).toHaveLength(0);
        });

        // switching to the "all" mode
        it('should switch to "all" mode for the comment layer', function () {
            commentLayer.switchCommentDisplayMode(CommentLayer.DISPLAY_MODE.ALL);
            expect(commentLayer.getDisplayMode()).toBe(CommentLayer.DISPLAY_MODE.ALL);
        });
        it('should still have five threads in the comments node inside the comment layer after switching to "all" mode', function () {
            expect(commentsNode.children()).toHaveLength(5);
            expect(commentsNode.children('.thread')).toHaveLength(5);
        });
        it('should still have seven comments in the comments node inside the comment layer after switching to "all" mode', function () {
            expect(commentsNode.find('.comment')).toHaveLength(7);
        });
        it('should have 0 children in bubble layer node after switching to "all" mode', function () {
            expect(bubbleLayerNode.children()).toHaveLength(0);
        });
        it('should show that the bubble mode is not active after switching to "all" mode', function () {
            expect(commentLayer.isBubbleMode()).toBeFalse();
        });
        it('should show that the hidden mode is not active after switching to "all" mode', function () {
            expect(commentLayer.isHiddenDisplayModeActive()).toBeFalse();
        });
        it('should have much content in rangemarker overlay node after switching to "all" mode even if no comment is selected', function () {
            expect(commentCollection.getSelectedCommentId()).toBeNull();
            expect(rangeMarkerOverlayNode.children().length).toBeGreaterThan(9);
            expect(rangeMarkerOverlayNode.children('.isRangeMarker')).toHaveLength(5);
            expect(rangeMarkerOverlayNode.children('.isCommentConnectionLine').length).toBeGreaterThan(4);
        });
        it('should also have content in rangemarker overlay node after selecting a comment in the "all" mode', function () {
            commentCollection.selectComment(commentModel_3);
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_3_ID);
            expect(rangeMarkerOverlayNode.children().length).toBeGreaterThan(9);
            expect(rangeMarkerOverlayNode.children('.isRangeMarker').length).toBeGreaterThan(4);
            expect(rangeMarkerOverlayNode.children('.isCommentConnectionLine').length).toBeGreaterThan(4);
        });
        it('should still have content in rangemarker overlay node after deselecting the comment in the "all" mode', function () {
            commentCollection.deselectComment();
            expect(commentCollection.getSelectedCommentId()).toBeNull();
            expect(rangeMarkerOverlayNode.children().length).toBeGreaterThan(9);
            expect(rangeMarkerOverlayNode.children('.isRangeMarker')).toHaveLength(5);
            expect(rangeMarkerOverlayNode.children('.isCommentConnectionLine').length).toBeGreaterThan(4);
        });

        // switching to the "hidden" mode
        it('should switch to "hidden" mode for the comment layer', function () {
            commentLayer.switchCommentDisplayMode(CommentLayer.DISPLAY_MODE.HIDDEN);
            expect(commentLayer.getDisplayMode()).toBe(CommentLayer.DISPLAY_MODE.HIDDEN);
        });
        it('should still have five threads in the comments node inside the comment layer after switching to "hidden" mode', function () {
            expect(commentsNode.children()).toHaveLength(5);
            expect(commentsNode.children('.thread')).toHaveLength(5);
        });
        it('should still have seven comments in the comments node inside the comment layer after switching to "hidden" mode', function () {
            expect(commentsNode.find('.comment')).toHaveLength(7);
        });
        it('should have 0 children in bubble layer node after switching to "hidden" mode', function () {
            expect(bubbleLayerNode.children()).toHaveLength(0);
        });
        it('should show that the bubble mode is not active after switching to "hidden" mode', function () {
            expect(commentLayer.isBubbleMode()).toBeFalse();
        });
        it('should show that the hidden mode is active after switching to "hidden" mode', function () {
            expect(commentLayer.isHiddenDisplayModeActive()).toBeTrue();
        });
        it('should have no more much content in rangemarker overlay node after switching to "hidden" mode', function () {
            expect(rangeMarkerOverlayNode.children()).toHaveLength(0);
        });

        // switching back to the "selected" mode
        it('should switch back to selected mode for the comment layer', function () {
            commentLayer.switchCommentDisplayMode(CommentLayer.DISPLAY_MODE.SELECTED);
            expect(commentLayer.getDisplayMode()).toBe(CommentLayer.DISPLAY_MODE.SELECTED);
        });
        it('should still have five threads in the comments node inside the comment layer after switching to "selected" mode again', function () {
            expect(commentsNode).toHaveLength(1);
            expect(commentsNode.children()).toHaveLength(5);
            expect(commentsNode.children('.thread')).toHaveLength(5);
        });
        it('should still have seven comments in the comments node inside the comment layer after switching to "selected" mode again', function () {
            expect(commentsNode.find('.comment')).toHaveLength(7);
        });
        it('should have no children in bubble layer node after switching to "selected" mode again', function () {
            expect(bubbleLayerNode).toHaveLength(1);
            expect(bubbleLayerNode.children()).toHaveLength(0);
        });
        it('should show that the bubble mode is not active after switching to "selected" mode again', function () {
            expect(commentLayer.isBubbleMode()).toBeFalse();
        });
        it('should show that the hidden mode is not active after switching to "selected" mode again', function () {
            expect(commentLayer.isHiddenDisplayModeActive()).toBeFalse();
        });
        it('should have no content in rangemarker overlay node after switching to "selected" mode again', function () {
            expect(rangeMarkerOverlayNode).toHaveLength(1);
            expect(rangeMarkerOverlayNode.children()).toHaveLength(0); // no comment was selected before
        });
        it('should have content in rangemarker overlay node after selecting a comment in the "selected" mode after switching back', function () {
            commentModel_3 = commentCollection.getById(COMMENT_3_ID);
            expect(commentModel_3.getText()).toBe(COMMENT_3_TEXT);
            commentCollection.selectComment(commentModel_3);
            expect(commentCollection.getSelectedCommentId()).toBe(COMMENT_3_ID);
            expect(rangeMarkerOverlayNode.children().length).toBeGreaterThan(1);
            expect(rangeMarkerOverlayNode.children('.isRangeMarker').length).toBeGreaterThan(0);
            expect(rangeMarkerOverlayNode.children('.isCommentConnectionLine').length).toBeGreaterThan(0);
        });
        it('should have no more content in rangemarker overlay node after deselecting the comment in the "selected" mode after switching back', function () {
            commentCollection.deselectComment();
            expect(commentCollection.getSelectedCommentId()).toBeNull();
            expect(rangeMarkerOverlayNode.children()).toHaveLength(0);
        });
    });
});
