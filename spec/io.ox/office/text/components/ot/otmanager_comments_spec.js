/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import OTManagerText from '@/io.ox/office/text/components/ot/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerText = new OTManagerText(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // handling of insertComment operation
        // { name: 'insertComment', start: [0, 1], text: '2222', date: '2020-01-07T09:37:43Z', pos: [529, 529], author: 'def', initials: 'd', providerId: 'ox', userId: '14', parent: 0, is_new_comment: true }

        it('should calculate valid transformed insertComment operation after external insertText operation before comment', function () {
            oneOperation = { name: 'insertText', start: [3, 1], text: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerText.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [3, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 1], text: '123', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertComment operation after external insertText operation behind comment', function () {
            oneOperation = { name: 'insertText', start: [3, 7], text: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerText.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 8], text: '123', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertComment operation after external insertText operation at the same position', function () {
            oneOperation = { name: 'insertText', start: [3, 7], text: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerText.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [3, 10], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 7], text: '123', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertComment operation after internal insertText operation before comment', function () {
            oneOperation = { name: 'insertComment', start: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 1], text: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerText.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [3, 1], text: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [3, 8], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertComment operation after internal insertText operation behind comment', function () {
            oneOperation = { name: 'insertComment', start: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 7], text: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerText.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [3, 8], text: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertComment operation after intenal insertText operation at the same position', function () {
            oneOperation = { name: 'insertComment', start: [3, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 7], text: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerText.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [3, 8], text: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [3, 7], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertComment operation after external splitParagraph operation before comment', function () {
            oneOperation = { name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerText.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [4, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertComment operation after external splitParagraph operation behind comment', function () {
            oneOperation = { name: 'splitParagraph', start: [3, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerText.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitParagraph', start: [3, 8], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertComment operation after internal splitParagraph operation before comment', function () {
            oneOperation = { name: 'insertComment', start: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerText.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [4, 4], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertComment operation after internal splitParagraph operation behind comment', function () {
            oneOperation = { name: 'insertComment', start: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerText.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }], transformedOps);
        });
    });
});
