/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // setAttributes and local move operation (handleSetAttrsMove)
        // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
        it('should calculate valid transformed setAttributes operation after local move operation', function () {

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 3]);
            expect(oneOperation.end).toEqual([3, 7]);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]);
            expect(oneOperation.end).toEqual([2, 9]);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4]);
            expect(oneOperation.end).toEqual([2, 9]);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 8]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 6], end: [2, 6], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4]);
            expect(oneOperation.end).toEqual([2, 7]);
            expect(localActions[0].operations[0].start).toEqual([2, 6]);
            expect(localActions[0].operations[0].end).toEqual([2, 6]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 11], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4]);
            expect(oneOperation.end).toEqual([2, 7]);
            expect(localActions[0].operations[0].start).toEqual([2, 5]);
            expect(localActions[0].operations[0].end).toEqual([2, 5]);
            expect(localActions[0].operations[0].to).toEqual([2, 11]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4]);
            expect(oneOperation.end).toEqual([2, 8]);
            expect(localActions[0].operations[0].start).toEqual([2, 5]);
            expect(localActions[0].operations[0].end).toEqual([2, 5]);
            expect(localActions[0].operations[0].to).toEqual([2, 7]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4]);
            expect(oneOperation.end).toEqual([2, 7]);
            expect(localActions[0].operations[0].start).toEqual([2, 5]);
            expect(localActions[0].operations[0].end).toEqual([2, 5]);
            expect(localActions[0].operations[0].to).toEqual([2, 8]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 3]);
            expect(oneOperation.end).toEqual([2, 8]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 7]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4]);
            expect(oneOperation.end).toEqual([2, 8]);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 10], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 3]);
            expect(oneOperation.end).toEqual([2, 7]);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 10]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 10], end: [2, 10], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]);
            expect(oneOperation.end).toEqual([2, 9]);
            expect(localActions[0].operations[0].start).toEqual([2, 10]);
            expect(localActions[0].operations[0].end).toEqual([2, 10]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4]);
            expect(oneOperation.end).toEqual([2, 8]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);
            expect(localActions[0].operations[0].end).toEqual([1, 10]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 3, 3]);
            expect(oneOperation.end).toEqual([2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2, 1, 0], end: [2, 2, 1, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3]);
            expect(oneOperation.end).toEqual([2, 4, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 3]);
            expect(oneOperation.end).toEqual([2, 5, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);
            expect(localActions[0].operations[0].end).toEqual([1, 10]);
            expect(localActions[0].operations[0].to).toEqual([2, 4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3]);
            expect(oneOperation.end).toEqual([2, 4, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);
            expect(localActions[0].operations[0].end).toEqual([1, 10]);
            expect(localActions[0].operations[0].to).toEqual([2, 5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 3, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3]);
            expect(oneOperation.end).toEqual([2, 4, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);
            expect(localActions[0].operations[0].end).toEqual([1, 10]);
            expect(localActions[0].operations[0].to).toEqual([2, 3, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3]);
            expect(oneOperation.end).toEqual([2, 4, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);
            expect(localActions[0].operations[0].end).toEqual([1, 10]);
            expect(localActions[0].operations[0].to).toEqual([2, 4, 3, 0]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3, 3]);
            expect(oneOperation.end).toEqual([2, 4, 3, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);
            expect(localActions[0].operations[0].end).toEqual([1, 10]);
            expect(localActions[0].operations[0].to).toEqual([2, 4, 3, 0]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3, 3]);
            expect(oneOperation.end).toEqual([2, 4, 3, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);
            expect(localActions[0].operations[0].end).toEqual([1, 10]);
            expect(localActions[0].operations[0].to).toEqual([2, 4, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3, 2]);
            expect(oneOperation.end).toEqual([2, 4, 3, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);
            expect(localActions[0].operations[0].end).toEqual([1, 10]);
            expect(localActions[0].operations[0].to).toEqual([2, 4, 3, 4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3, 2]);
            expect(oneOperation.end).toEqual([2, 4, 3, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);
            expect(localActions[0].operations[0].end).toEqual([1, 10]);
            expect(localActions[0].operations[0].to).toEqual([2, 4, 3, 6]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3, 2]);
            expect(oneOperation.end).toEqual([2, 4, 3, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);
            expect(localActions[0].operations[0].end).toEqual([1, 10]);
            expect(localActions[0].operations[0].to).toEqual([2, 4, 3, 7]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(oneOperation.end).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[0].to).toEqual([1, 5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 0], end: [2, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 0]);
            expect(oneOperation.end).toEqual([1, 5, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[0].to).toEqual([1, 5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 2, 1], end: [2, 4, 2, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 1]);
            expect(oneOperation.end).toEqual([1, 5, 2, 6]);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[0].to).toEqual([1, 5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 2, 1], end: [2, 4, 2, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 3], end: [2, 3], to: [1, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 3, 2, 1]);
            expect(oneOperation.end).toEqual([2, 3, 2, 6]);
            expect(localActions[0].operations[0].start).toEqual([2, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 3]);
            expect(localActions[0].operations[0].to).toEqual([1, 5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 3, 2, 2]);
            expect(oneOperation.end).toEqual([1, 5, 3, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[0].to).toEqual([1, 5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 3], end: [2, 3], to: [1, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 3, 3, 2, 2]);
            expect(oneOperation.end).toEqual([2, 3, 3, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 3]);
            expect(localActions[0].operations[0].to).toEqual([1, 5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3, 2, 2]);
            expect(oneOperation.end).toEqual([2, 4, 3, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 3]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4, 3, 2], end: [2, 4, 3, 2], to: [1, 10], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10, 2]);
            expect(oneOperation.end).toEqual([1, 10, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 3, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 4, 3, 2]);
            expect(localActions[0].operations[0].to).toEqual([1, 10]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4, 3, 2], end: [2, 4, 3, 2], to: [1, 10], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10]);
            expect(oneOperation.end).toEqual([1, 10]);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 3, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 4, 3, 2]);
            expect(localActions[0].operations[0].to).toEqual([1, 10]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4, 3, 1], end: [2, 4, 3, 1], to: [1, 10], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3, 1, 2]);
            expect(oneOperation.end).toEqual([2, 4, 3, 1, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 3, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 4, 3, 1]);
            expect(localActions[0].operations[0].to).toEqual([1, 10]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 3, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3]);
            expect(oneOperation.end).toEqual([2, 4, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 5]);
            expect(localActions[0].operations[0].to).toEqual([2, 3, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 3]);
            expect(oneOperation.end).toEqual([2, 5, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 5]);
            expect(localActions[0].operations[0].to).toEqual([2, 3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 3]);
            expect(oneOperation.end).toEqual([2, 5, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 5]);
            expect(localActions[0].operations[0].to).toEqual([2, 4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 3]);
            expect(oneOperation.end).toEqual([2, 4, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 5]);
            expect(localActions[0].operations[0].to).toEqual([2, 5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'setAttributes', start: [2], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.end).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[0].to).toEqual([1, 5]);
            expect(localActions[0].operations).toHaveLength(1);
        });
    });

    // setAttributes and local external operation (handleSetAttrsMove)
    // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
    it('should calculate valid transformed setAttributes operation after external move operation', function () {

        oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 8], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 7], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }], transformedOps);
    });

    // updateComplexField and local move (handleSetAttrsMove)
    it('should calculate valid transformed updateComplexField operation after local move operation', function () {

        oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
        localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

        oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
        localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

        oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2, 1, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' };
        localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [2, 0, 1, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

        oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2, 1, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' };
        localActions = [{ operations: [{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1, 1, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

        oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
        localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

        oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
        localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

        oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
        localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
    });

    // updateComplexField and external move (handleSetAttrsMove)
    it('should calculate valid transformed updateComplexField operation after external move operation', function () {

        oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2, 1, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [2, 0, 1, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2, 1, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1, 1, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }], transformedOps);
    });

});
