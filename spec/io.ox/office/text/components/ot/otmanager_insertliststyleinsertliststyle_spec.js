/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertListStyle and local insertListStyle operation (handleInsertListStyleInsertListStyle)
        // { name: 'insertListStyle', listStyleId: 'L1', listDefinition: { ... } }
        it('should calculate valid transformed insertListStyle operation after local insertListStyle operation', function () {

            oneOperation = { name: 'insertListStyle', listStyleId: 'L1', listDefinition: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertListStyle', listStyleId: 'L2', listDefinition: '456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertListStyle', listStyleId: 'L2', listDefinition: '456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertListStyle', listStyleId: 'L1', listDefinition: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertListStyle', listStyleId: 'L1', listDefinition: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertListStyle', listStyleId: 'L1', listDefinition: '456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertListStyle', listStyleId: 'L1', listDefinition: '456', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });
    });
});
