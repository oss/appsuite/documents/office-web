/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertHeaderFooter and local insertHeaderFooter operation (handleInsertHeaderInsertHeader)
        // { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }
        it('should calculate valid transformed insertHeaderFooter operation after local insertHeaderFooter operation', function () {

            oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'footer_default', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'footer_default', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }], transformedOps);

            // same type but different ID -> is this possible?
            oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [
                { name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 },
                { name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default', opl: 1, osn: 1 }
            ] }], localActions);
            expectOp([], transformedOps);

            // same ID but different type -> is this possible?
            oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'footer_first', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [
                { name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 },
                { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }
            ] }], localActions);
            expectOp([], transformedOps);
        });

        it('should calculate valid transformed insertHeaderFooter operations after local deleteHeaderFooter operation', function () {

            // oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 };
            // localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }];
            // transformedOps = otManager.transformOperation(oneOperation, localActions);
            // expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }], localActions);
            // expectOp([], transformedOps);

            oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed deleteHeaderFooter operations after local insertHeaderFooter operation', function () {

            // oneOperation = { name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 };
            // localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }] }];
            // transformedOps = otManager.transformOperation(oneOperation, localActions);
            // expectAction([{ operations: [] }], localActions);
            // expectOp([{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteHeaderFooter', id: 'OX_rId101', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'OX_rId101', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertText operations after local deleteHeaderFooter operation', function () {

            oneOperation = { name: 'insertText', target: 'OX_rId100', text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertText', target: 'OX_rId101', text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', target: 'OX_rId101', text: 'ooo', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertText operations after external deleteHeaderFooter operation', function () {

            oneOperation = { name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', target: 'OX_rId100', text: 'ooo', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteHeaderFooter', id: 'OX_rId101', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', target: 'OX_rId100', text: 'ooo', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', target: 'OX_rId100', text: 'ooo', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'OX_rId101', opl: 1, osn: 1 }], transformedOps);
        });
    });

});
