/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // updateComplexField and local delete (handleSetAttrsDelete)
        it('should calculate valid transformed updateComplexField operation after local delete operation', function () {

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 6], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 5], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 6], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 4, 1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 3, 1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [4, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [4, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        // updateComplexField and external delete (handleSetAttrsDelete)
        it('should calculate valid transformed updateComplexField operation after external delete operation', function () {

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 6], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 5], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 6], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 4, 1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 3, 1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [4, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [4, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3], opl: 1, osn: 1 }], transformedOps);
        });
    });
});
