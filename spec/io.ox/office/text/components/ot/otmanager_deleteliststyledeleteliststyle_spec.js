/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // deleteListStyle and local deleteListStyle operation (handleDeleteListStyleDeleteListStyle)
        // { name: 'deleteListStyle', listStyleId: 'L1' }
        it('should calculate valid transformed deleteListStyle operation after local deleteListStyle operation', function () {

            oneOperation = { name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);
        });
    });
});
