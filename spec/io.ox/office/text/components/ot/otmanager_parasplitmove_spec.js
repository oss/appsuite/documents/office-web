/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        // splitParagraph and local move operation (handleParaSplitMove)
        // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
        it('should calculate valid transformed splitParagraph operation after local move operation', function () {

            oneOperation = { name: 'splitParagraph', start: [0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 2]);

            oneOperation = { name: 'splitParagraph', start: [1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 2]);

            oneOperation = { name: 'splitParagraph', start: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3]);

            oneOperation = { name: 'splitParagraph', start: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 2]);

            oneOperation = { name: 'splitParagraph', start: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0]);

            oneOperation = { name: 'splitParagraph', start: [4, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 0]);

            oneOperation = { name: 'splitParagraph', start: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0]);

            oneOperation = { name: 'splitParagraph', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0]);

            oneOperation = { name: 'splitParagraph', start: [4, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 0]);

            oneOperation = { name: 'splitParagraph', start: [2, 2, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 2, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 2, 4]);

            oneOperation = { name: 'splitParagraph', start: [3, 2, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 2, 4]);

            oneOperation = { name: 'splitParagraph', start: [3, 2, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2, 4]);

            oneOperation = { name: 'splitParagraph', start: [3, 2, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2, 4]);

            oneOperation = { name: 'splitParagraph', start: [3, 2, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2, 4]);

            oneOperation = { name: 'splitParagraph', start: [0, 2, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 3, 2, 4]);

            oneOperation = { name: 'splitParagraph', start: [2, 6, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 6, 2, 4]);

            oneOperation = { name: 'splitParagraph', start: [2, 6, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 8]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 5, 2, 4]);

            oneOperation = { name: 'splitParagraph', start: [2, 6, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 8]);
            expect(localActions[0].operations[0].end).toEqual([2, 8]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 7, 2, 4]);

            oneOperation = { name: 'splitParagraph', start: [2, 6, 2, 3, 2, 3, 4, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 8]);
            expect(localActions[0].operations[0].end).toEqual([2, 8]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 7, 2, 3, 2, 3, 4, 4]);

            oneOperation = { name: 'splitParagraph', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 0], end: [3, 2, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 2, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 2, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0]);

            oneOperation = { name: 'splitParagraph', start: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 0], end: [3, 2, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 0], end: [3, 2, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 0], end: [3, 2, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3]);

            oneOperation = { name: 'splitParagraph', start: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5]);

            oneOperation = { name: 'splitParagraph', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0]);

            oneOperation = { name: 'splitParagraph', start: [4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 1]);

            oneOperation = { name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);

            oneOperation = { name: 'splitParagraph', start: [4, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 0]);

            oneOperation = { name: 'splitParagraph', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 2]);
            expect(localActions[0].operations[0].end).toEqual([4, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0]);

            oneOperation = { name: 'splitParagraph', start: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4]);

            oneOperation = { name: 'splitParagraph', start: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5]);

            oneOperation = { name: 'splitParagraph', start: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([5, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4]);

            oneOperation = { name: 'splitParagraph', start: [5, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 0]);

            oneOperation = { name: 'splitParagraph', start: [5, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [5, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([6, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 0]);

            oneOperation = { name: 'splitParagraph', start: [3, 1, 1, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 0, 8], end: [3, 1, 1, 0, 8], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 3]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 0, 5]);

            oneOperation = { name: 'splitParagraph', start: [3, 1, 1, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 2, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 0, 5]);

            oneOperation = { name: 'splitParagraph', start: [3, 1, 1, 1, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 1, 4]);

            oneOperation = { name: 'splitParagraph', start: [3, 1, 1, 2, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 2, 5]);

            oneOperation = { name: 'splitParagraph', start: [3, 1, 2, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 0, 5]);

            oneOperation = { name: 'splitParagraph', start: [3, 1, 2, 1, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 1, 6]);

            oneOperation = { name: 'splitParagraph', start: [3, 1, 2, 2, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 2, 5]);

            oneOperation = { name: 'splitParagraph', start: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3]);

            oneOperation = { name: 'splitParagraph', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([4, 1, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0]);

            oneOperation = { name: 'splitParagraph', start: [2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([4, 1, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3]);

            oneOperation = { name: 'splitParagraph', start: [3, 1, 2, 0, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 0, 0]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 3, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0, 1, 0, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3, 2, 0], end: [3, 3, 2, 0], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 2, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 3, 2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 1, 0, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 1, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 1, 0, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0, 0, 0, 1, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 4, 1, 1, 0, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 1, 1, 0, 1, 2]);

            oneOperation = { name: 'splitParagraph', start: [3, 3, 0, 1, 0, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 1, 0, 1, 2]);
        });

        // splitTable and local move operation (handleParaSplitMove)
        // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
        it('should calculate valid transformed splitTable operation after local move operation', function () {

            oneOperation = { name: 'splitTable', start: [0, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 3]);

            oneOperation = { name: 'splitTable', start: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3]);

            oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0, 1, 1, 1], end: [3, 0, 1, 1, 1], to: [2, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([4, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3]);

            oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0, 1, 1, 1], end: [3, 0, 1, 1, 1], to: [2, 2, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([4, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([2, 2, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3]);

            oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0, 1, 1, 1], end: [3, 0, 1, 1, 1], to: [2, 3, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([4, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3]);

            oneOperation = { name: 'splitTable', start: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3]);

            oneOperation = { name: 'splitTable', start: [4, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 3]);

            oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0, 1, 1, 1], end: [2, 0, 1, 1, 1], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([5, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3]);

            oneOperation = { name: 'splitTable', start: [2, 2, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 3]);

            oneOperation = { name: 'splitTable', start: [3, 2, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 2, 3]);

            oneOperation = { name: 'splitTable', start: [3, 2, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 2, 3]);

            oneOperation = { name: 'splitTable', start: [3, 2, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2, 3]);

            oneOperation = { name: 'splitTable', start: [3, 2, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2, 3]);

            oneOperation = { name: 'splitTable', start: [3, 2, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2, 3]);

            oneOperation = { name: 'splitTable', start: [0, 2, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 3, 2, 3]);

            oneOperation = { name: 'splitTable', start: [2, 6, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 6, 2, 3]);

            oneOperation = { name: 'splitTable', start: [2, 6, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 8]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 5, 2, 3]);

            oneOperation = { name: 'splitTable', start: [2, 6, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 8]);
            expect(localActions[0].operations[0].end).toEqual([2, 8]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 7, 2, 3]);

            oneOperation = { name: 'splitTable', start: [2, 6, 2, 3, 2, 3, 4, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 8]);
            expect(localActions[0].operations[0].end).toEqual([2, 8]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 7, 2, 3, 2, 3, 4, 3]);

            oneOperation = { name: 'splitTable', start: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2]);

            oneOperation = { name: 'splitTable', start: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3]);

            oneOperation = { name: 'splitTable', start: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 2, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2]);

            oneOperation = { name: 'splitTable', start: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 2, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 2, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3]);

            oneOperation = { name: 'splitTable', start: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 4, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3]);

            oneOperation = { name: 'splitTable', start: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3]);

            oneOperation = { name: 'splitTable', start: [5, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1]);

            oneOperation = { name: 'splitTable', start: [5, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [6, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([7, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1]);

            oneOperation = { name: 'splitTable', start: [3, 1, 1, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0, 1, 1, 1], end: [3, 1, 1, 1, 0, 1, 1, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 1, 2]);

            oneOperation = { name: 'splitTable', start: [3, 1, 1, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 2, 1, 1, 1], end: [3, 1, 1, 1, 2, 1, 1, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 2, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 2, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 1, 2]);

            oneOperation = { name: 'splitTable', start: [3, 1, 1, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 2, 0, 1, 1, 1], end: [3, 1, 1, 2, 0, 1, 1, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 3, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 3, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 1, 2]);

            oneOperation = { name: 'splitTable', start: [3, 1, 1, 1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 3, 0, 1, 1, 1], end: [3, 1, 1, 3, 0, 1, 1, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 4, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 4, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 1, 3]);

            oneOperation = { name: 'splitTable', start: [3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([4, 0, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);

            oneOperation = { name: 'splitTable', start: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2]);

            oneOperation = { name: 'splitTable', start: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [4, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([5, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3]);

            oneOperation = { name: 'splitTable', start: [3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [5, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([6, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);

            oneOperation = { name: 'splitTable', start: [3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 0, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 0, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 3, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0, 1, 0, 0, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 1, 0, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 1, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 0, 1, 0, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0, 0, 0, 1, 2]);

            oneOperation = { name: 'splitTable', start: [3, 4, 1, 1, 0, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 1, 1, 0, 1, 2]);

            oneOperation = { name: 'splitTable', start: [3, 3, 0, 1, 0, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 1, 0, 1, 2]);
        });
    });

});
