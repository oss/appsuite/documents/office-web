/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertText and local deleteHeaderFooter operation
        it('should calculate valid transformed insertText operation after local deleteHeaderFooter operation', function () {

            oneOperation = { name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [0, 2], text: 'a', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [0, 2], text: 'a', opl: 1, osn: 1 }], transformedOps);
        });

        // insertParagraph and local deleteHeaderFooter operation
        it('should calculate valid transformed insertParagraph operation after local deleteHeaderFooter operation', function () {

            oneOperation = { name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }], transformedOps);
        });

        // deleteHeaderFooter and local insertText
        it('should calculate valid transformed insertText operation after external deleteHeaderFooter operation', function () {

            oneOperation = { name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 2], text: 'a', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [0, 2], text: 'a', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }], transformedOps);
        });

        // deleteHeaderFooter and local insertParagraph
        it('should calculate valid transformed insertParagraph operation after external deleteHeaderFooter operation', function () {

            oneOperation = { name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }], transformedOps);
        });

    });

});
