/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    // var expectActions = Utils.expectActions;
    var expectAction = Utils.expectAction;
    var expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertText and local move operation (handleMoveInsertChar)
        // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
        it('should calculate valid transformed insertText operation after local move operation', function () {

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3]);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 8]);
            expect(localActions[0].operations[0].end).toEqual([3, 8]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4]);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 7]);
            expect(localActions[0].operations[0].end).toEqual([3, 7]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4]);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 10]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3]);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 2, 7, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4]);

            oneOperation = { name: 'insertText', start: [3, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 5, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 5, 1, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5, 7, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);

            oneOperation = { name: 'insertText', start: [2, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 2, 7, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1]);

            oneOperation = { name: 'insertText', start: [4, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 2, 7, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 1]);

            oneOperation = { name: 'insertText', start: [3, 1, 1, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 2, 7, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 0]);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5]);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 5], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 9]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4]);

            oneOperation = { name: 'insertText', start: [3, 4, 1, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 1, 2]);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [4, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([4, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4]);

            oneOperation = { name: 'insertText', start: [3, 1, 1, 2, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 1, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 1, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 2, 3]);

            oneOperation = { name: 'insertText', start: [3, 1, 1, 2, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 1, 2, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 1, 2, 8]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 2, 2]);

            oneOperation = { name: 'insertText', start: [3, 1, 1, 2, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 2, 2, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 2, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 2, 2]);

            oneOperation = { name: 'insertText', start: [3, 1, 1, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 1, 2]);

            oneOperation = { name: 'insertText', start: [3, 1, 1, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 2]);

            oneOperation = { name: 'insertText', start: [2, 2, 1, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 1, 2]);

            oneOperation = { name: 'insertText', start: [2, 2, 1, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3, 0, 3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 3, 0, 3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 3, 0, 1, 2]);

            oneOperation = { name: 'insertText', start: [2, 2, 1, 1, 2, 1, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2, 1, 1, 2], end: [2, 2, 1, 1, 2], to: [3, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 1, 1, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 1, 0]);

            oneOperation = { name: 'insertText', start: [2, 2, 1, 1, 2, 1, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2, 1, 1, 3], end: [2, 2, 1, 1, 3], to: [3, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1, 1, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 1, 1, 3]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 2, 1, 1, 2, 1, 0]);

            oneOperation = { name: 'insertText', start: [2, 2, 0, 1, 2, 1, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2, 1, 1, 2], end: [2, 2, 1, 1, 2], to: [3, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 1, 1, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 2, 0, 1, 2, 1, 0]);

            oneOperation = { name: 'insertText', start: [3, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3, 1, 2, 0, 2, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 6, 1, 2, 0, 2, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);

            oneOperation = { name: 'insertText', start: [3, 1, 1, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3, 1, 2, 0, 2, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 3, 1, 2, 0, 2, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 0]);
        });

        // move and local insertText operation (handleMoveInsertChar)
        // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
        it('should calculate valid transformed move operation after local insertText operation', function () {

            oneOperation = { name: 'move', start: [2, 2], end: [2, 2], to: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [3, 5], text: 'ooo', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 2], end: [2, 2], to: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 2], end: [2, 2], to: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 5], opl: 1, osn: 1 }], transformedOps);
        });
    });

});
