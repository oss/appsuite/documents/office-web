/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // move and local move operation (handleMoveMove)
        // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
        it('should calculate valid transformed move operation after local move operation', function () {

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [5, 0, 1, 0], end: [5, 0, 1, 0], to: [4, 0, 1, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 0, 1, 0], end: [5, 0, 1, 0], to: [4, 0, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [4, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0, 1, 1, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 1, 1, 0]); // setting the source to the dest of the external operation
            expect(localActions[0].operations[0].end).toEqual([3, 0, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0); // external operation must not be applied

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]); // setting the source to the dest of the external operation
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0); // external operation must not be applied

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]); // setting the source to the dest of the external operation
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0); // external operation must not be applied

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 0]);
            expect(oneOperation.end).toEqual([5, 0]);
            expect(oneOperation.to).toEqual([3, 2]);

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 0]);
            expect(oneOperation.end).toEqual([5, 0]);
            expect(oneOperation.to).toEqual([3, 7]);

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 2], end: [5, 2], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1]);
            expect(localActions[0].operations[0].end).toEqual([5, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 0]);
            expect(oneOperation.end).toEqual([5, 0]);
            expect(oneOperation.to).toEqual([3, 7]);

            oneOperation = { name: 'move', start: [5, 4], end: [5, 4], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 2], end: [5, 2], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 2]);
            expect(localActions[0].operations[0].end).toEqual([5, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 3]);
            expect(oneOperation.end).toEqual([5, 3]);
            expect(oneOperation.to).toEqual([3, 7]);

            oneOperation = { name: 'move', start: [5, 4], end: [5, 4], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 0]);
            expect(localActions[0].operations[0].to).toEqual([5, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 4]);
            expect(oneOperation.end).toEqual([5, 4]);
            expect(oneOperation.to).toEqual([3, 6]);

            oneOperation = { name: 'move', start: [5, 4], end: [5, 4], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 0]);
            expect(localActions[0].operations[0].to).toEqual([5, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 3]);
            expect(oneOperation.end).toEqual([5, 3]);
            expect(oneOperation.to).toEqual([3, 6]);

            oneOperation = { name: 'move', start: [5, 4, 1, 0], end: [5, 4, 1, 0], to: [3, 6], opl: 1, osn: 1 }; // only the inner drawing is moved
            localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 0]);
            expect(localActions[0].operations[0].to).toEqual([5, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 3, 1, 0]);
            expect(oneOperation.end).toEqual([5, 3, 1, 0]);
            expect(oneOperation.to).toEqual([3, 6]);

            oneOperation = { name: 'move', start: [5, 4, 1, 0], end: [5, 4, 1, 0], to: [3, 1, 1, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 0]);
            expect(localActions[0].operations[0].end).toEqual([5, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 3, 1, 0]);
            expect(oneOperation.end).toEqual([5, 3, 1, 0]);
            expect(oneOperation.to).toEqual([3, 1, 1, 0]);

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 0]);
            expect(oneOperation.end).toEqual([5, 0]);
            expect(oneOperation.to).toEqual([3, 0]);

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 0]);
            expect(oneOperation.end).toEqual([5, 0]);
            expect(oneOperation.to).toEqual([3, 0]);

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [5, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1]);
            expect(localActions[0].operations[0].to).toEqual([5, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1]);
            expect(oneOperation.end).toEqual([5, 1]);
            expect(oneOperation.to).toEqual([3, 0]);

            oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 0]);
            expect(oneOperation.end).toEqual([5, 0]);
            expect(oneOperation.to).toEqual([2, 1]);
        });
    });

});
