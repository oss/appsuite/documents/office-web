/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        // insertParagraph and local move operation (handleMoveinsertParagraph)
        // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
        it('should calculate valid transformed insertParagraph operation after local move operation', function () {

            oneOperation = { name: 'mergeParagraph', start: [0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([1, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 5]);
            expect(localActions[0].operations[0].end).toEqual([2, 5]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.paralength).toBe(6);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(4);

            oneOperation = { name: 'mergeParagraph', start: [4], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.paralength).toBe(4);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(6);

            oneOperation = { name: 'mergeParagraph', start: [4], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [2, 2, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 2, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 2, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 2, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 2, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 2, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [0, 2, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 3, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [2, 6, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 6, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [2, 6, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 8]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 5, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [2, 6, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 8]);
            expect(localActions[0].operations[0].end).toEqual([2, 8]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 7, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [2, 6, 2, 3, 2, 3, 4], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 8]);
            expect(localActions[0].operations[0].end).toEqual([2, 8]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 7, 2, 3, 2, 3, 4]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 0], end: [3, 2, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(5); // ! The outer drawing at [3, 2] remains, the inner drawing is moved

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(4); // ! The drawing at [3, 2] is moved away

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(5); // the drawing at position [3, 0] was already there before the move

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(5); // the drawing at position [3, 1] was already there before the move

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(4);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [5], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [5], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [5, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([5, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5]);
            expect(oneOperation.paralength).toBe(6);

            oneOperation = { name: 'mergeParagraph', start: [3, 1, 1, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 0, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 0, 5]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 1, 1, 1], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 1]);
            expect(oneOperation.paralength).toBe(4);

            oneOperation = { name: 'mergeParagraph', start: [3, 1, 1, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], paralength: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 1, 2, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 0, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 1]);
            expect(oneOperation.paralength).toBe(6);

            oneOperation = { name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 2]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(5); // the drawing at [3, 1] was already there before

            oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([2, 6, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.paralength).toBe(4);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 3, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0, 1, 0, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3, 2, 0], end: [3, 3, 2, 0], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 2, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 3, 2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0, 1, 0, 1], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 1]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0, 1, 0, 1], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0, 0, 0, 1]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 1, 1, 0, 1], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 1, 1, 0, 1]);
            expect(oneOperation.paralength).toBe(5);

            oneOperation = { name: 'mergeParagraph', start: [3, 3, 0, 1, 0, 1], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 1, 0, 1]);
            expect(oneOperation.paralength).toBe(5);
        });

        // mergeTable and local move operation (handleMoveInsertParagraph)
        // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
        it('should calculate valid transformed mergeTable operation after local move operation', function () {

            oneOperation = { name: 'mergeTable', start: [0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([1, 5, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0, 1, 1, 1], end: [3, 0, 1, 1, 1], to: [2, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 5, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [4], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0, 1, 1, 1], end: [2, 0, 1, 1, 1], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [2, 2, 2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 2, 2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 2, 2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 2, 2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 2, 2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 2, 2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [0, 2, 2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 3, 2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [2, 6, 2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 6, 2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [2, 6, 2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 8]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 5, 2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [2, 6, 2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 8]);
            expect(localActions[0].operations[0].end).toEqual([2, 8]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 7, 2]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [2, 6, 2, 3, 2, 3, 4], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 8]);
            expect(localActions[0].operations[0].end).toEqual([2, 8]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 7, 2, 3, 2, 3, 4]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 4, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 7, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 4, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [5], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [5], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [6, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([5, 6, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0, 1, 1, 1], end: [3, 1, 1, 1, 0, 1, 1, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 1]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 2, 0, 1, 1, 1], end: [3, 1, 1, 2, 0, 1, 1, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 5, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 5, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 1]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 3, 0, 1, 1, 1], end: [3, 1, 1, 3, 0, 1, 1, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 2, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 2, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 1]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [4, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 6, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [5, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([4, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 3, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0, 1, 0, 0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0, 1, 0, 1], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 1]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 0, 1, 0, 1], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0, 0, 0, 1]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 4, 1, 1, 0, 1], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 1, 1, 0, 1]);
            expect(oneOperation.rowcount).toBe(5);

            oneOperation = { name: 'mergeTable', start: [3, 3, 0, 1, 0, 1], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 1, 0, 1]);
            expect(oneOperation.rowcount).toBe(5);
        });
    });

});
