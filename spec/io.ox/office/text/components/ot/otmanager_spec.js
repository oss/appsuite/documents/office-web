/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager } from '@/io.ox/office/textframework/model/otmanager';
import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var otManager = new TextOTManager();

    // the test runner
    var TestRunner = Utils.TestRunner;
    var testRunner = new TestRunner(otManager);

    // shortcuts for the utils functions
    var expectActions = Utils.expectActions;

    // existence check ----------------------------------------------------

    it('should subclass TextBaseOTManager', function () {
        expect(TextOTManager).toBeSubClassOf(TextBaseOTManager);
    });

    // public methods -----------------------------------------------------

    describe('method hasAliasName', function () {
        it('should contain operations for alias "insertChar"', function () {
            expect(otManager.hasAliasName({ name: 'insertText' }, 'insertChar')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'insertDrawing' }, 'insertChar')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'insertComment' }, 'insertChar')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'insertComplexField' }, 'insertChar')).toBeTrue();
        });
    });

    describe('method transformPosition', function () {

        it('should change position after move operation, if a drawing is moved away before the selection', function () {
            var pos = [1, 5];
            var actions = [{ operations: [{ name: 'move', start: [1, 4], end: [1, 4], to: [2, 0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([1, 4]);
        });

        it('should change position after move operation, if a drawing is inserted before the selection', function () {
            var pos = [1, 5];
            var actions = [{ operations: [{ name: 'move', start: [0, 4], end: [0, 4], to: [1, 0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([1, 6]);
        });

        it('should not change position after move operation, if a drawing is moved away behind the selection', function () {
            var pos = [1, 5];
            var actions = [{ operations: [{ name: 'move', start: [1, 6], end: [1, 6], to: [2, 0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([1, 5]);
        });

        it('should not change position after move operation, if a drawing is inserted behind the selection', function () {
            var pos = [1, 5];
            var actions = [{ operations: [{ name: 'move', start: [0, 4], end: [0, 4], to: [1, 6], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([1, 5]);
        });

        it('should not change position after move operation, if a drawing is moved away and inserted before the selection', function () {
            var pos = [1, 5];
            var actions = [{ operations: [{ name: 'move', start: [1, 4], end: [1, 4], to: [1, 0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([1, 5]);
        });

        it('should change position inside table after move operation, if a drawing is moved away before the selection', function () {
            var pos = [1, 5, 3, 2, 1];
            var actions = [{ operations: [{ name: 'move', start: [1, 4], end: [1, 4], to: [2, 0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([1, 4, 3, 2, 1]);
        });

        it('should change position inside drawing after move operation, if the drawing with the selection is moved', function () {
            var pos = [1, 5, 3, 2];
            var actions = [{ operations: [{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 2], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([2, 2, 3, 2]);
        });

        it('should change drawing position after move operation, if the drawing itself is moved', function () {
            var pos = [1, 5];
            var actions = [{ operations: [{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 2], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([2, 2]);
        });

        it('should change position inside a drawing after move operation, if the drawing itself is moved out of a table', function () {
            var pos = [1, 1, 1, 2, 2, 3, 3];
            var actions = [{ operations: [{ name: 'move', start: [1, 1, 1, 2, 2], end: [1, 1, 1, 2, 2], to: [5, 0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([5, 0, 3, 3]);
        });

        it('should change position inside a drawing after move operation, if the drawing itself is moved into a table', function () {
            var pos = [5, 0, 3, 3];
            var actions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [1, 1, 1, 2, 2], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([1, 1, 1, 2, 2, 3, 3]);
        });

        // deleteHeaderFooter and local position (handleDeleteTarget)
        it('should calculate valid transformed position after external deleteHeaderFooter operation, if position is not in header/footer', function () {
            var pos = [2, 4];
            var actions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([2, 4]);
        });

        it('should calculate valid transformed position after external deleteHeaderFooter operation, if position is inside deleted header/footer', function () {
            var pos = [2, 4];
            var target = 'hf1';
            var actions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions, target);
            expect(pos).toBeUndefined();
        });

        it('should calculate valid transformed position after external deleteHeaderFooter operation, if position is in different header/footer', function () {
            var pos = [2, 4];
            var target = 'hf2';
            var actions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions, target);
            expect(pos).toEqual([2, 4]);
        });

        // insertHeaderFooter and local position
        it('should calculate valid transformed position after external insertHeaderFooter operation', function () {
            var pos = [2, 4];
            var actions = [{ operations: [{ name: 'insertHeaderFooter', id: 'hf1', type: 'header_default', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([2, 4]);
        });

        it('should calculate valid transformed position after external insertComment operation, if position is in same paragraph behind comment', function () {
            var pos = [2, 4];
            var actions = [{ operations: [{ name: 'insertComment', id: 'cmt2', author: 'abc', start: [2, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([2, 5]);
        });

        it('should calculate valid transformed position after external insertComment operation, if position is in same paragraph before comment', function () {
            var pos = [2, 4];
            var actions = [{ operations: [{ name: 'insertComment', id: 'cmt2', author: 'abc', start: [2, 6], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([2, 4]);
        });

        it('should calculate valid transformed position after external insertComment operation, if position is in same paragraph at comment position', function () {
            var pos = [2, 4];
            var actions = [{ operations: [{ name: 'insertComment', id: 'cmt2', author: 'abc', start: [2, 4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([2, 5]);
        });

        it('should calculate valid transformed position after external insertComment operation, if position is in different paragraph', function () {
            var pos = [2, 4];
            var actions = [{ operations: [{ name: 'insertComment', id: 'cmt2', author: 'abc', start: [1, 2], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions);
            expect(pos).toEqual([2, 4]);
        });

        it('should calculate valid transformed position after external insertComment operation, if position is in has different target', function () {
            var pos = [2, 4];
            var target = 'cmt1';
            var actions = [{ operations: [{ name: 'insertComment', id: 'cmt2', author: 'abc', start: [2, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, actions, target);
            expect(pos).toEqual([2, 4]);
        });
    });

    // checking that no text handler is missing
    describe('method transformOperation() handles all operations', function () {

        it('should transform the specified external action with the specified local actions', function () {

            var externalActions = [
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertListStyle', listStyleId: 'L1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [1, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [1, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComment', start: [1, 4], userId: '1234', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComplexField', start: [1, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateComplexField', start: [1, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [1, 7], type: 'shape', opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [1, 7], end: [1, 7], to: [1, 8], opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [1], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [1, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitTable', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [2], end: [2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeTable', start: [1], rowcount: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHeaderFooter', id: 'hf1', type: 'header_default', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }
            ];
            var localActions = [
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [3, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [3, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComment', start: [3, 4], userId: '1234', id: 'cmt2', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComplexField', start: [3, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateComplexField', start: [3, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [3, 7], type: 'shape', opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [3, 7], end: [3, 7], to: [3, 8], opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [3], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [3, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitTable', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeTable', start: [3], rowcount: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHeaderFooter', id: 'hf2', type: 'footer_first', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteHeaderFooter', id: 'hf2', opl: 1, osn: 1 }] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [3, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [3, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComment', start: [3, 4], userId: '1234', id: 'cmt2', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComplexField', start: [3, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateComplexField', start: [3, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [3, 7], type: 'shape', opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [3, 7], end: [3, 7], to: [3, 8], opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [3], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [3, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitTable', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeTable', start: [3], rowcount: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHeaderFooter', id: 'hf2', type: 'footer_first', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteHeaderFooter', id: 'hf2', opl: 1, osn: 1 }] }
            ], localActions);
            expectActions([
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertListStyle', listStyleId: 'L1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [1, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [1, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComment', start: [1, 4], userId: '1234', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComplexField', start: [1, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateComplexField', start: [1, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [1, 7], type: 'shape', opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [1, 7], end: [1, 7], to: [1, 8], opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [1], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [1, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitTable', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [2], end: [2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeTable', start: [1], rowcount: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHeaderFooter', id: 'hf1', type: 'header_default', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }
            ], transformedActions);
        });

    });

    describe('method transformOperation', function () {

        it('should transform all specified external actions with the specified local actions and text only comment operation', function () {

            var externalActions = [
                { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertRange', start: [5, 1, 4, 0, 0], position: 'start', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                    { name: 'insertRange', start: [5, 1, 4, 0, 2], position: 'end', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                    { name: 'insertComment', start: [5, 1, 4, 0, 3], text: '123', id: 'cmt6', author: 'me', date: '1.1.1', userId: 101, opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 0] }] }
            ];
            var localActions = [
                { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [4, 1], count: 3, opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                    { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                ] },
                { operations: [
                    { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                    { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                ] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [
                    { name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                    { name: 'mergeTable', start: [6], rowcount: 6, opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'insertText', start: [6, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [6, 1], count: 3, opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                    { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                ] },
                { operations: [
                    { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                    { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                ] }
            ], localActions);
            expectActions([
                { operations: [{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [7, 10, 5, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertRange', start: [7, 10, 5, 0, 0], position: 'start', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                    { name: 'insertRange', start: [7, 10, 5, 0, 2], position: 'end', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                    { name: 'insertComment', start: [7, 10, 5, 0, 3], text: '123', id: 'cmt6', author: 'me', date: '1.1.1', userId: 101, opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [4, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [5, 0] }] }
            ], transformedActions);
        });

        it('should transform all specified external actions with the specified local actions and text only drawing operation', function () {

            var externalActions = [
                { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertBookmark', start: [5, 1, 4, 0, 0], position: 'start', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                    { name: 'insertDrawing', start: [5, 1, 4, 0, 3], type: 'shape', opl: 1, osn: 1 },
                    { name: 'insertParagraph', start: [5, 1, 4, 0, 3, 0], opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [5, 1, 4, 0, 3, 0], attrs: { paragraph: { marginBottom: 0 } }, opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 0] }] }
            ];
            var localActions = [
                { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [4, 1], count: 3, opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                    { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                ] },
                { operations: [
                    { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                    { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                ] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [
                    { name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                    { name: 'mergeTable', start: [6], rowcount: 6, opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'insertText', start: [6, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [6, 1], count: 3, opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                    { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                ] },
                { operations: [
                    { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                    { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                ] }
            ], localActions);
            expectActions([
                { operations: [{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [7, 10, 5, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertBookmark', start: [7, 10, 5, 0, 0], position: 'start', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                    { name: 'insertDrawing', start: [7, 10, 5, 0, 3], type: 'shape', opl: 1, osn: 1 },
                    { name: 'insertParagraph', start: [7, 10, 5, 0, 3, 0], opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [7, 10, 5, 0, 3, 0], attrs: { paragraph: { marginBottom: 0 } }, opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [4, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [5, 0] }] }
            ], transformedActions);
        });

    });
});
