/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import OTManager from '@/io.ox/office/text/components/ot/otmanager';

// class OTManager ================================================

describe('Text class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // updateComplexField and local insertRows (handleSetAttrsInsertRows)
        it('should calculate valid transformed updateComplexField operation after local insertRows operation', function () {

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 8, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 11, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 5, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
        });

        // updateComplexField and external insertRows (handleSetAttrsInsertRows)
        it('should calculate valid transformed updateComplexField operation after external insertRows operation', function () {

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 8, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 11, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 5, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }], transformedOps);
        });

        // updateComplexField and local insertCells (handleSetAttrsInsertRows)
        it('should calculate valid transformed updateComplexField operation after local insertCells operation', function () {

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 7, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
        });

        // updateComplexField and external insertCells (handleSetAttrsInsertRows)
        it('should calculate valid transformed updateComplexField operation after external insertCells operation', function () {

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 7, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }], transformedOps);
        });
    });

});
