/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import OTManager from '@/io.ox/office/text/components/ot/otmanager';

// class OTManager ================================================

describe('Text class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // deleteColumns and external updateComplexField (handleSetAttrsDeleteColumns)
        it('should calculate valid transformed updateComplexField operation after local deleteColumns operation', function () {

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 4, 4, 2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{  name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 4, 1, 2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 }] }], localActions);
            expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 4, 1, 2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 }] }], localActions);
            expectOp([], transformedOps);
        });

        // deleteColumns and local updateComplexField (handleSetAttrsDeleteColumns)
        it('should calculate valid transformed updateComplexField operation after external deleteColumns operation', function () {

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 4, 4, 2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 }], transformedOps);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 4, 1, 2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 4, 1, 2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
            expectOp([{ name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 }], transformedOps);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 }], transformedOps);
        });

    });

});
