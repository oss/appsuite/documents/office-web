/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import OTManager from '@/io.ox/office/text/components/ot/otmanager';

// class OTManager ================================================

describe('Text class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // changeComment and local changeComment
        it('should calculate valid transformed changeComment operation after local changeComment operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content A' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content A' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content A' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content A' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content A' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content A' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content B' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content B' }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content A' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf2', text: 'Mofied comment content B' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf2', text: 'Mofied comment content B' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content A' }], transformedOps);
        });

        // changeComment and local delete
        it('should calculate valid transformed changeComment operation after local delete operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 3], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 3] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2] }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 5], end: [1, 9] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 5], end: [1, 9] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 3] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 3, 2], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 4] }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0], end: [0] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0], end: [0] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [0, 4, 2], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 4, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [1] }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 4, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 3] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 2, 4, 2], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content', target: 'hf1' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content', target: 'hf2' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content', target: 'hf2' }], transformedOps);
        });

        // changeComment and external delete
        it('should calculate valid transformed changeComment operation after external delete operation', function () {

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 2] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [0, 2] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 3] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 3] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 5], end: [1, 9] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 5], end: [1, 9] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 3] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 3, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 3] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 4] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 4] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0], end: [0] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [0, 4, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [0], end: [0] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [1] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 4, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1], end: [1] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 3] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 4, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 2, 4, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 3] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf1', text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }], transformedOps);
        });

        // changeComment and local deleteHeaderFooter
        it('should calculate valid transformed changeComment operation after local deleteHeaderFooter operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf1', text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' }], transformedOps);
        });

        // changeComment and external deleteHeaderFooter
        it('should calculate valid transformed changeComment operation after external deleteHeaderFooter operation', function () {

            oneOperation = { name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf1', text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }], transformedOps);
        });

        // changeComment and local insertText
        it('should calculate valid transformed changeComment operation after local insertText operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 3], text: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [0, 3], text: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertText', start: [1, 4], text: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [1, 4], text: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertText', start: [1, 5], text: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [1, 5], text: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertText', start: [1, 3], text: '123', target: 'hf1', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [1, 3], text: '123', target: 'hf1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7, 2, 3], text: 'Mofied comment content' }], transformedOps);
        });

        // changeComment and external insertText
        it('should calculate valid transformed changeComment operation after external insertText operation', function () {

            oneOperation = { name: 'insertText', start: [0, 3], text: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertText', start: [0, 3], text: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [1, 4], text: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertText', start: [1, 4], text: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [1, 5], text: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertText', start: [1, 5], text: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [1, 3], text: '123', target: 'hf1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertText', start: [1, 3], text: '123', target: 'hf1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }], transformedOps);
        });

        // changeComment and local insertParagraph
        it('should calculate valid transformed changeComment operation after local insertParagraph operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [1], target: 'hf1', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [1], target: 'hf1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);
        });

        // changeComment and external insertParagraph
        it('should calculate valid transformed changeComment operation after external insertParagraph operation', function () {

            oneOperation = { name: 'insertParagraph', start: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [1], target: 'hf1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [1], target: 'hf1', opl: 1, osn: 1 }], transformedOps);
        });

        // changeComment and local insertTable
        it('should calculate valid transformed changeComment operation after local insertTable operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertTable', start: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertTable', start: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertTable', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertTable', start: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertTable', start: [1], target: 'hf1', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertTable', start: [1], target: 'hf1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);
        });

        // changeComment and external insertTable
        it('should calculate valid transformed changeComment operation after external insertTable operation', function () {

            oneOperation = { name: 'insertTable', start: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertTable', start: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertTable', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertTable', start: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertTable', start: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertTable', start: [1], target: 'hf1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertTable', start: [1], target: 'hf1', opl: 1, osn: 1 }], transformedOps);
        });

        // changeComment and local mergeParagraph
        it('should calculate valid transformed changeComment operation after local mergeParagraph operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 1], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 4, 5], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [1, 4, 4, 5], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 4, 2], paralength: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeParagraph', start: [1, 4, 2], paralength: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);
        });

        // changeComment and external mergeParagraph
        it('should calculate valid transformed changeComment operation after external mergeParagraph operation', function () {

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 4, 5], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 4, 5], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [1, 4, 4], paralength: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'mergeParagraph', start: [1, 4, 4], paralength: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2, opl: 1, osn: 1 }], transformedOps);
        });

        // changeComment and local mergeTable
        it('should calculate valid transformed changeComment operation after local mergeTable operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 0, 1, 1], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [1, 1, 0, 1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);
        });

        // changeComment and external mergeTable
        it('should calculate valid transformed changeComment operation after external mergeTable operation', function () {

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 0, 1, 1], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [1, 1, 0, 1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }], transformedOps);
        });

        // changeComment and local splitParagraph
        it('should calculate valid transformed changeComment operation after local splitParagraph operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);
        });

        // changeComment and external splitParagraph
        it('should calculate valid transformed changeComment operation after external splitParagraph operation', function () {

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] }], transformedOps);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] }], transformedOps);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] }], transformedOps);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2], target: 'hf1' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }], transformedOps);
        });

        // changeComment and local splitTable
        it('should calculate valid transformed changeComment operation after local splitTable operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0, 0, 1, 1], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }], transformedOps);
        });

        // changeComment and external splitTable
        it('should calculate valid transformed changeComment operation after external splitTable operation', function () {

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }], transformedOps);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 6] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }], transformedOps);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0, 0, 1, 1], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }], transformedOps);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2], target: 'hf1' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }], transformedOps);
        });

        // changeComment and local insertRows
        it('should calculate valid transformed changeComment operation after local insertRows operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 8, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 11, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 5, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'hf1' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'hf1' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);
        });

        // changeComment and external insertRows
        it('should calculate valid transformed changeComment operation after external insertRows operation', function () {

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 8, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 11, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 5, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'hf1' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'hf1' }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }], transformedOps);
        });

        // changeComment and local insertCells
        it('should calculate valid transformed changeComment operation after local insertCells operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 7, 2, 3], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'hf1' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'hf1' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);
        });

        // changeComment and external insertCells
        it('should calculate valid transformed changeComment operation after external insertCells operation', function () {

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 7, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'hf1' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'hf1' }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }], transformedOps);
        });

        // changeComment and local insertColumn
        it('should calculate valid transformed changeComment operation after local insertColumn operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 4, 2, 2], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], text: 'Mofied comment content' }], transformedOps);
        });

        // changeComment and external insertColumn
        it('should calculate valid transformed changeComment operation after external insertColumn operation', function () {

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 4, 2, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }], transformedOps);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }], transformedOps);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }], transformedOps);
        });

        // changeComment and local move
        it('should calculate valid transformed changeComment operation after local move operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2, 1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0, 1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2, 1, 4], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1, 1, 4], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }], transformedOps);
        });

        // changeComment and external move
        it('should calculate valid transformed changeComment operation after external move operation', function () {

            oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2, 1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0, 1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2, 1, 4], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1, 1, 4], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }], transformedOps);
        });

        // changeComment and local setAttributes
        it('should calculate valid transformed changeComment operation after local setAttributes operation', function () {

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1], end: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 1], end: [3, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }], transformedOps);

            oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }], transformedOps);
        });

        // changeComment and external setAttributes
        it('should calculate valid transformed changeComment operation after external setAttributes operation', function () {

            oneOperation = { name: 'setAttributes', start: [3], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 1], end: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 1], end: [3, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);
        });

    });

});
