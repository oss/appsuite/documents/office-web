/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import OTManager from '@/io.ox/office/text/components/ot/otmanager';

// class OTManager ================================================

describe('Text class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        // updateComplexField and local insert (handleSetAttrsInsertChar)
        it('should calculate valid transformed updateComplexField operation after internal insertText operation', function () {

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);

            oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 0], instruction: 'DATE·\\@ "DD-MMM-YY"' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 0]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
        });

        // updateComplexField and external insert (handleSetAttrsInsertChar)
        it('should calculate valid transformed updateComplexField operation after external insertText operation', function () {

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' };
            localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 0], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 0]);
        });
    });
});
