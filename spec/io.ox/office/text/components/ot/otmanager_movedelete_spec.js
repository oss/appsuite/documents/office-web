/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // delete and local move operation (handleMoveDelete)
        // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
        it('should calculate valid transformed delete operation after local move operation without generating a new delete operation', function () {

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2, 8], end: [2, 9], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2, 9], end: [2, 10], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [4, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([4, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 2]);
            expect(transformedOps[0].end).toEqual([2, 3]);

            oneOperation = { name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 3]);
            expect(transformedOps[0].end).toEqual([2, 4]);

            oneOperation = { name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 1]);
            expect(transformedOps[0].end).toEqual([2, 2]);

            oneOperation = { name: 'delete', start: [2, 6], end: [2, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 6]);
            expect(transformedOps[0].end).toEqual([2, 7]);

            oneOperation = { name: 'delete', start: [2, 1, 0, 4], end: [2, 1, 0, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 0, 0, 4]);
            expect(transformedOps[0].end).toEqual([2, 0, 0, 6]);

            oneOperation = { name: 'delete', start: [2, 1, 0, 4], end: [2, 1, 0, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 2, 2, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([3, 2, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 2, 1, 0, 0, 4]);
            expect(transformedOps[0].end).toEqual([3, 2, 2, 1, 0, 0, 6]);

            oneOperation = { name: 'delete', start: [2, 1, 0, 4], end: [2, 1, 0, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [0, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 0, 0, 4]);
            expect(transformedOps[0].end).toEqual([0, 0, 0, 6]);

            oneOperation = { name: 'delete', start: [2, 1, 0], end: [2, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [0, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 0, 0]);
            expect(transformedOps[0].end).toEqual([0, 0, 2]);

            oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 1], end: [4, 1], to: [0, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2]);
            expect(transformedOps[0].end).toEqual([3]);

            oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 1]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2]);
            expect(transformedOps[0].end).toEqual([3]);

            oneOperation = { name: 'delete', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].start).toEqual([1]);
            expect(transformedOps[0].end).toEqual([3]);
            expect(transformedOps[1].name).toBe("delete");
            expect(transformedOps[1].start).toEqual([3, 0]);
            expect(transformedOps[1].end).toEqual([3, 0]);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].start).toEqual([1]);
            expect(transformedOps[0].end).toBeUndefined();
            expect(transformedOps[1].name).toBe("delete");
            expect(transformedOps[1].start).toEqual([5, 0]);
            expect(transformedOps[1].end).toEqual([5, 0]);

            oneOperation = { name: 'delete', start: [7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 1]);
            expect(localActions[0].operations[0].to).toEqual([6, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([7]);
            expect(transformedOps[0].end).toBeUndefined();

            oneOperation = { name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 1, 2, 2, 2], end: [1, 1, 2, 2, 2], to: [6, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].start).toEqual([1, 1]);
            expect(transformedOps[0].end).toEqual([1, 2]);
            expect(transformedOps[1].name).toBe("delete");
            expect(transformedOps[1].start).toEqual([6, 0]);
            expect(transformedOps[1].end).toEqual([6, 0]);

            oneOperation = { name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 2, 0, 0], end: [1, 2, 0, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].start).toEqual([1, 1]);
            expect(transformedOps[0].end).toEqual([1, 2]);
            expect(transformedOps[1].name).toBe("delete");
            expect(transformedOps[1].start).toEqual([2, 0]);
            expect(transformedOps[1].end).toEqual([2, 0]);

            oneOperation = { name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 0, 0]);
            expect(localActions[0].operations[0].end).toEqual([1, 1, 0, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1, 1]);
            expect(transformedOps[0].end).toEqual([1, 2]);

            oneOperation = { name: 'delete', start: [1], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1]);
            expect(transformedOps[0].end).toEqual([2]);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [1, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1]);
            expect(transformedOps[0].end).toBeUndefined();

            oneOperation = { name: 'delete', start: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].start).toEqual([1, 3]);
            expect(transformedOps[0].end).toBeUndefined();
            expect(transformedOps[1].name).toBe("delete");
            expect(transformedOps[1].start).toEqual([2, 0]);
            expect(transformedOps[1].end).toEqual([2, 0]);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3, 0, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 0], end: [1, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [1, 3, 0, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [4, 0], end: [4, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [7, 3], end: [7, 3], to: [1, 3, 0, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].name).toBe("delete");
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1]);
            expect(transformedOps[0].end).toEqual([4]);

            oneOperation = { name: 'delete', start: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0, 0, 0], end: [2, 0, 0, 0], to: [1, 3, 0, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].start).toEqual([2, 0]);
            expect(transformedOps[0].end).toBeUndefined();
            expect(transformedOps[1].name).toBe("delete");
            expect(transformedOps[1].start).toEqual([1, 3, 0, 0]);
            expect(transformedOps[1].end).toEqual([1, 3, 0, 0]);

            oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2]);
            expect(transformedOps[0].end).toBeUndefined();

            oneOperation = { name: 'delete', start: [2], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2]);
            expect(transformedOps[0].end).toEqual([2]);

            oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0, 0, 1, 4], end: [2, 0, 0, 1, 4], to: [2, 0, 1, 1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2]);
            expect(transformedOps[0].end).toBeUndefined();

            oneOperation = { name: 'delete', start: [1, 0], opl: 1, osn: 1 }; // this might be a character
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([1, 2]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1, 0]);
            expect(transformedOps[0].end).toBeUndefined();

            oneOperation = { name: 'delete', start: [1, 2], opl: 1, osn: 1 }; // this might be a character
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([1, 2]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1, 2]);
            expect(transformedOps[0].end).toBeUndefined();

            oneOperation = { name: 'delete', start: [1, 3], opl: 1, osn: 1 }; // this might be a character at exactly the same position
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([1, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1, 4]);
            expect(transformedOps[0].end).toBeUndefined();

            oneOperation = { name: 'delete', start: [1, 4], opl: 1, osn: 1 }; // this might be a character
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([1, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1, 5]);
            expect(transformedOps[0].end).toBeUndefined();

            oneOperation = { name: 'delete', start: [1, 5], opl: 1, osn: 1 }; // this might be a character
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([1, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1, 6]);
            expect(transformedOps[0].end).toBeUndefined();

            // deleting the destination paragraph
            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 }; // deleting the complete paragraph
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].name).toBe("delete");
            expect(localActions[0].operations[0].start).toEqual([1, 0]); // deleting the drawing that is now on position [1, 0] on server side, ignoring move operation
            expect(localActions[0].operations[0].end).toEqual([1, 0]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1]);
            expect(transformedOps[0].end).toBeUndefined();

            // deleting the destination range
            oneOperation = { name: 'delete', start: [1, 1], end: [1, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].name).toBe("delete");
            expect(localActions[0].operations[0].start).toEqual([2, 0]); // deleting the drawing that is still on position [2, 0] on server side, ignoring move operation
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1, 1]);
            expect(transformedOps[0].end).toEqual([1, 8]);

            // deleting the source range
            oneOperation = { name: 'delete', start: [1, 1], end: [1, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].start).toEqual([1, 1]);
            expect(transformedOps[0].end).toEqual([1, 6]);
            expect(transformedOps[1].name).toBe("delete");
            expect(transformedOps[1].start).toEqual([2, 0]); // deleting the drawing that is locally on position [2, 0]
            expect(transformedOps[1].end).toEqual([2, 0]);

            // deleting the source drawing
            oneOperation = { name: 'delete', start: [1, 3], end: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(1); // should be 1
            expect(transformedOps[0].name).toBe("delete"); // a new delete operation must be generated to remove the locally moved drawing
            expect(transformedOps[0].start).toEqual([2, 0]);
            expect(transformedOps[0].end).toEqual([2, 0]);
        });

        // delete and external move operation (handleMoveDelete)
        // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
        it('should calculate valid transformed delete operation after external move operation without generating a new delete operation', function () {

            oneOperation = { name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 8], end: [2, 9], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 9], end: [2, 10], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 }], transformedOps);
        });
    });
});
