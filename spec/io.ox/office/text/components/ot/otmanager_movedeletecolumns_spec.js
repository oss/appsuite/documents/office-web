/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import TextOTManager from '@/io.ox/office/text/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Text class TextOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new TextOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // deleteColumns and local move operation (handleMoveDeleteColumns)
        // { name: move, start: [2, 4], end: [2, 4], to: [1, 2, 2, 1, 0] }
        // { name: deleteColumns, start: [2], startGrid: 2, endGrid: 3 }
        it('should calculate valid transformed deleteColumns operation after local move operation without generating a new delete operation', function () {

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 4, 1, 0], end: [3, 2, 4, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 2, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 2, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 4, 1, 0], end: [3, 2, 4, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 3, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 3, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 4, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 3, 1, 0], end: [3, 2, 3, 1, 0], to: [3, 2, 4, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 4, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 2, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 4, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 2, 3, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 4, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2]);

            oneOperation = { name: 'deleteColumns', start: [3, 2, 1, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 3, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 3, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 1, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 1, 1, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 1, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 1, 2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 1, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([3, 8]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 3, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3, 1, 0], end: [3, 3, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 3, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 0, 1, 0, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 0, 0]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0, 1, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 0, 1]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 0, 1, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0, 0, 0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 0, 0, 0, 0, 1]);

            oneOperation = { name: 'deleteColumns', start: [3, 4, 1, 1, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 1, 1, 0, 1]);

            oneOperation = { name: 'deleteColumns', start: [3, 3, 0, 1, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 4, 0, 1, 0]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 0, 1, 0, 1]);
        });

        it('should calculate valid transformed deleteColumns operation after local move operation and generate a new delete operation', function () {

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [3, 3, 3, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].start).toEqual([3]);
            expect(transformedOps[1].name).toBe("delete");
            expect(transformedOps[1].start).toEqual([2, 0]);
            expect(transformedOps[1].end).toEqual([2, 0]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2, 2, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].name).toBe("delete");
            expect(localActions[0].operations[0].start).toEqual([2, 0]); // sending the delete operation to the server instead of the move operation
            expect(localActions[0].operations[0].end).toEqual([2, 0]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);
        });

        it('should calculate valid transformed move operation after local deleteColumns operation and generate a new delete operation', function () {

            oneOperation = { name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [3, 3, 3, 1, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(transformedOps).toHaveLength(0); // the only external operation was removed, no delete operation required

            oneOperation = { name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[1].name).toBe("delete"); // sending additional delete operation to the server
            expect(localActions[0].operations[1].start).toEqual([2, 0]);
            expect(localActions[0].operations[1].end).toEqual([2, 0]);
            expect(transformedOps).toHaveLength(0); // the only external operation was removed, no delete operation required

            oneOperation = { name: 'move', start: [2, 0], end: [2, 0], to: [3, 2, 2, 1, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].name).toBe("delete"); // applying locally a delete operation instead of the merge operation
            expect(transformedOps[0].start).toEqual([2, 0]);
            expect(transformedOps[0].end).toEqual([2, 0]);
        });

    });
});
