/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as operations from "@/io.ox/office/drawinglayer/utils/operations";

// tests ======================================================================

describe("module drawinglayer/utils/operations", function () {

    it("constants should exist", function () {
        expect(operations.INSERT_DRAWING).toBe("insertDrawing");
        expect(operations.DELETE_DRAWING).toBe("deleteDrawing");
        expect(operations.CHANGE_DRAWING).toBe("changeDrawing");
        expect(operations.MOVE_DRAWING).toBe("moveDrawing");
        expect(operations.GROUP).toBe("group");
        expect(operations.UNGROUP).toBe("ungroup");
        expect(operations.INSERT_CHART_SERIES).toBe("insertChartSeries");
        expect(operations.DELETE_CHART_SERIES).toBe("deleteChartSeries");
        expect(operations.CHANGE_CHART_SERIES).toBe("changeChartSeries");
        expect(operations.DELETE_CHART_AXIS).toBe("deleteChartAxis");
        expect(operations.CHANGE_CHART_AXIS).toBe("changeChartAxis");
        expect(operations.CHANGE_CHART_GRID).toBe("changeChartGrid");
        expect(operations.CHANGE_CHART_TITLE).toBe("changeChartTitle");
        expect(operations.CHANGE_CHART_LEGEND).toBe("changeChartLegend");
    });
});
