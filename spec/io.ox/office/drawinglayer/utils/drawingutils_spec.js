/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { coord } from '@/io.ox/office/tk/algorithms';
import { Rectangle } from '@/io.ox/office/tk/dom';
import * as utils from '@/io.ox/office/drawinglayer/utils/drawingutils';

const p = coord.point;

// functions ==================================================================

function r(l, t, w, h) {
    return new Rectangle(l, t, w, h);
}

// tests ======================================================================

describe('module drawinglayer/utils/drawingutils', function () {

    // types ------------------------------------------------------------------

    describe('enum DrawingType', function () {
        it('should exist', function () {
            expect(utils.DrawingType).toBeObject();
        });
    });

    // functions --------------------------------------------------------------

    describe('method createDefaultSizeForShapeId', function () {
        it('should exist', function () {
            expect(utils.createDefaultSizeForShapeId).toBeFunction();
        });
        it('should create a width and height for a given presetShapeId that fits in the provided dimensions', function () {
            expect(utils.createDefaultSizeForShapeId('ellipse', 100, 100)).toEqual({ width: 100, height: 100 });
            expect(utils.createDefaultSizeForShapeId('leftRightArrow', 100, 100)).toEqual({ width: 100, height: 50 });
            expect(utils.createDefaultSizeForShapeId('upDownArrow', 100, 100)).toEqual({ width: 50, height: 100 });
            // test shape with a format that is not supported (e.g. scale-vertical-double) - > use square
            expect(utils.createDefaultSizeForShapeId('bentConnector3_2', 100, 100)).toEqual({ width: 100, height: 100 });
        });
    });

    describe('method getTrackingRectangle', function () {
        it('should exist', function () {
            expect(utils.getTrackingRectangle).toBeFunction();
        });
        it('should return a frame rectangle for normal tracking mode', function () {
            var options = { modifiers: { shiftKey: false, ctrlKey: false, altKey: false } };
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 200), options).equals(r(200, 200, 5, 5))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 250), options).equals(r(200, 200, 21, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 250), options).equals(r(200, 200, 5, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 250), options).equals(r(180, 200, 21, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 200), options).equals(r(180, 200, 21, 5))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 150), options).equals(r(180, 150, 21, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 150), options).equals(r(200, 150, 5, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 150), options).equals(r(200, 150, 21, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 200), options).equals(r(200, 200, 21, 5))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 150)).equals(r(200, 150, 21, 51))).toBeTrue();
            options.boundRect = r(180, 190, 41, 21);
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 200), options).equals(r(200, 200, 5, 5))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(210, 210), options).equals(r(200, 200, 11, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 220), options).equals(r(200, 200, 21, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 230), options).equals(r(200, 200, 21, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(190, 190), options).equals(r(190, 190, 11, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 180), options).equals(r(180, 190, 21, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 170), options).equals(r(180, 190, 21, 11))).toBeTrue();
        });
        it('should return a frame rectangle for square tracking mode', function () {
            var options = { modifiers: { shiftKey: true, ctrlKey: false, altKey: false } };
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 200), options).equals(r(200, 200, 5, 5))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 250), options).equals(r(200, 200, 51, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(250, 220), options).equals(r(200, 200, 51, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 250), options).equals(r(200, 200, 51, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 250), options).equals(r(150, 200, 51, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(150, 220), options).equals(r(150, 200, 51, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 200), options).equals(r(180, 200, 21, 21))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 150), options).equals(r(150, 150, 51, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(150, 180), options).equals(r(150, 150, 51, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 150), options).equals(r(200, 150, 51, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 150), options).equals(r(200, 150, 51, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(250, 180), options).equals(r(200, 150, 51, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 200), options).equals(r(200, 200, 21, 21))).toBeTrue();
            options.boundRect = r(180, 190, 41, 21);
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 200), options).equals(r(200, 200, 5, 5))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 230), options).equals(r(200, 200, 11, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 170), options).equals(r(200, 190, 11, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 170), options).equals(r(190, 190, 11, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 230), options).equals(r(190, 200, 11, 11))).toBeTrue();
        });
        it('should return a frame rectangle for center tracking mode', function () {
            var options = { modifiers: { shiftKey: false, ctrlKey: true, altKey: true } };
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 200), options).equals(r(198, 198, 5, 5))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 250), options).equals(r(180, 150, 41, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 250), options).equals(r(198, 150, 5, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 250), options).equals(r(180, 150, 41, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 200), options).equals(r(180, 198, 41, 5))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 150), options).equals(r(180, 150, 41, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 150), options).equals(r(198, 150, 5, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 150), options).equals(r(180, 150, 41, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 200), options).equals(r(180, 198, 41, 5))).toBeTrue();
            options.boundRect = r(180, 190, 51, 31);
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 230), options).equals(r(180, 190, 41, 21))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 170), options).equals(r(180, 190, 41, 21))).toBeTrue();
            options.boundRect = r(170, 180, 51, 31);
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 230), options).equals(r(180, 190, 41, 21))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 170), options).equals(r(180, 190, 41, 21))).toBeTrue();
            options.boundRect = r(190, 180, 31, 51);
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 230), options).equals(r(190, 180, 21, 41))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 170), options).equals(r(190, 180, 21, 41))).toBeTrue();
            options.boundRect = r(180, 170, 31, 51);
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 230), options).equals(r(190, 180, 21, 41))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 170), options).equals(r(190, 180, 21, 41))).toBeTrue();
        });
        it('should return a frame rectangle for square/center tracking mode', function () {
            var options = { modifiers: { shiftKey: true, ctrlKey: true, altKey: true } };
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 200), options).equals(r(198, 198, 5, 5))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 250), options).equals(r(150, 150, 101, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(250, 220), options).equals(r(150, 150, 101, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 250), options).equals(r(150, 150, 101, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 250), options).equals(r(150, 150, 101, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(150, 220), options).equals(r(150, 150, 101, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 200), options).equals(r(180, 180, 41, 41))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 150), options).equals(r(150, 150, 101, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(150, 180), options).equals(r(150, 150, 101, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 150), options).equals(r(150, 150, 101, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 150), options).equals(r(150, 150, 101, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(250, 180), options).equals(r(150, 150, 101, 101))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 200), options).equals(r(180, 180, 41, 41))).toBeTrue();
            options.boundRect = r(180, 190, 51, 31);
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 230), options).equals(r(190, 190, 21, 21))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 170), options).equals(r(190, 190, 21, 21))).toBeTrue();
            options.boundRect = r(170, 180, 51, 31);
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 230), options).equals(r(190, 190, 21, 21))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 170), options).equals(r(190, 190, 21, 21))).toBeTrue();
            options.boundRect = r(190, 180, 31, 51);
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 230), options).equals(r(190, 190, 21, 21))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 170), options).equals(r(190, 190, 21, 21))).toBeTrue();
            options.boundRect = r(180, 170, 31, 51);
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 230), options).equals(r(190, 190, 21, 21))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 170), options).equals(r(190, 190, 21, 21))).toBeTrue();
        });
        it('should return a frame rectangle for two-point mode', function () {
            var options = { modifiers: { shiftKey: false, ctrlKey: false, altKey: false }, twoPointMode: true };
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 200), options).equals(r(200, 200, 1, 1))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 250), options).equals(r(200, 200, 21, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 250), options).equals(r(200, 200, 1, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 250), options).equals(r(180, 200, 21, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 200), options).equals(r(180, 200, 21, 1))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 150), options).equals(r(180, 150, 21, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 150), options).equals(r(200, 150, 1, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 150), options).equals(r(200, 150, 21, 51))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 200), options).equals(r(200, 200, 21, 1))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 150)).equals(r(200, 150, 21, 51))).toBeTrue();
            options.boundRect = r(180, 190, 41, 21);
            expect(utils.getTrackingRectangle(p(200, 200), p(200, 200), options).equals(r(200, 200, 1, 1))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(210, 210), options).equals(r(200, 200, 11, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(220, 220), options).equals(r(200, 200, 21, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(230, 230), options).equals(r(200, 200, 21, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(190, 190), options).equals(r(190, 190, 11, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(180, 180), options).equals(r(180, 190, 21, 11))).toBeTrue();
            expect(utils.getTrackingRectangle(p(200, 200), p(170, 170), options).equals(r(180, 190, 21, 11))).toBeTrue();
        });
        it('should add custom properties to the resulting rectangle', function () {
            var rect1 = utils.getTrackingRectangle(p(200, 200), p(230, 240));
            expect(rect1).toHaveProperty('origShift', 50);
            expect(rect1).toHaveProperty('reverseX', false);
            expect(rect1).toHaveProperty('reverseY', false);
            var rect2 = utils.getTrackingRectangle(p(200, 200), p(170, 240));
            expect(rect2).toHaveProperty('origShift', 50);
            expect(rect2).toHaveProperty('reverseX', true);
            expect(rect2).toHaveProperty('reverseY', false);
            var rect3 = utils.getTrackingRectangle(p(200, 200), p(170, 160));
            expect(rect3).toHaveProperty('origShift', 50);
            expect(rect3).toHaveProperty('reverseX', true);
            expect(rect3).toHaveProperty('reverseY', true);
            var rect4 = utils.getTrackingRectangle(p(200, 200), p(230, 160));
            expect(rect4).toHaveProperty('origShift', 50);
            expect(rect4).toHaveProperty('reverseX', false);
            expect(rect4).toHaveProperty('reverseY', true);
            var rect5 = utils.getTrackingRectangle(p(200, 200), p(200, 200));
            expect(rect5).toHaveProperty('origShift', 0);
            expect(rect5).toHaveProperty('reverseX', false);
            expect(rect5).toHaveProperty('reverseY', false);
        });
    });
});
