/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as utils from '@/io.ox/office/drawinglayer/utils/imageutils';

import { createSpreadsheetApp } from '~/spreadsheet/apphelper';

// tests ======================================================================

describe('module drawinglayer/utils/imageutils', function () {

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { cols: 5, rows: 10 } } },
        { name: 'insertAutoStyle', styleId: 'a0', attrs: {}, default: true },
        { name: 'insertSheet', sheet: 0, sheetName: 'Sheet1' }
    ];

    // initialize test document
    var app;
    createSpreadsheetApp('ooxml', OPERATIONS).done(function (application) {
        app = application;
    });

    // functions --------------------------------------------------------------

    describe('function calculateBitmapSettings', function () {

        it('should exist', function () {
            expect(utils.calculateBitmapSettings).toBeFunction();
        });

        it('should return correct bitmap values for Spreadsheet app', function () {
            // image size w:20 cm in hmm
            var frameSize = 20000;
            var leadingCrop = -60;
            var trailingCrop = -40;
            var bitmapSettings = utils.calculateBitmapSettings(app, frameSize, leadingCrop, trailingCrop);
            expect(bitmapSettings).toBeObject();
            expect(bitmapSettings.offset).toBe('30%');
            expect(bitmapSettings.size).toBe('50%');

            //image size h: 10cm
            frameSize = 10000;
            leadingCrop = -70;
            trailingCrop = -30;
            bitmapSettings = utils.calculateBitmapSettings(app, frameSize, leadingCrop, trailingCrop);
            expect(bitmapSettings).toBeObject();
            expect(bitmapSettings.offset).toBe('35%');
            expect(bitmapSettings.size).toBe('50%');
        });

    });
});
