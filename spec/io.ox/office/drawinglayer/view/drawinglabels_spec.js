/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as drawinglabels from "@/io.ox/office/drawinglayer/view/drawinglabels";

// constants ==================================================================

const TYPES = ["chart", "image", "shape", "textframe", "diagram"];

// tests ======================================================================

describe("module drawinglayer/view/drawinglabels", function () {

    describe("function getDrawingTypeIcon", function () {
        const { getDrawingTypeIcon } = drawinglabels;
        it("should exist", function () {
            expect(getDrawingTypeIcon).toBeFunction();
        });
        it("should return the icon for the specified type", function () {
            for (const type of TYPES) {
                expect(getDrawingTypeIcon(type)).toBeString();
            }
        });
        it("should return an icon for unknown types", function () {
            const defIcon = getDrawingTypeIcon(null);
            expect(defIcon).toBeString();
            expect(getDrawingTypeIcon("__something_invalid__")).toBe(defIcon);
            expect(getDrawingTypeIcon()).toBe(defIcon);
        });
    });

    describe("function getDrawingTypeLabel", function () {
        const { getDrawingTypeLabel } = drawinglabels;
        it("should exist", function () {
            expect(getDrawingTypeLabel).toBeFunction();
        });
        it("should return the label for the specified type", function () {
            for (const type of TYPES) {
                expect(getDrawingTypeLabel(type)).toBeString();
            }
        });
        it("should return a label for unknown types", function () {
            const defLabel = getDrawingTypeLabel(null);
            expect(defLabel).toBeString();
            expect(getDrawingTypeLabel("__something_invalid__")).toBe(defLabel);
            expect(getDrawingTypeLabel()).toBe(defLabel);
        });
    });
});
