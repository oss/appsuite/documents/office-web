/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import * as drawingframe from '@/io.ox/office/drawinglayer/view/drawingframe';

// tests ======================================================================

describe('module drawinglayer/view/drawingframe', function () {

    // functions --------------------------------------------------------------

    describe('function createDrawingFrame', function () {
        it('should exist', function () {
            expect(drawingframe.createDrawingFrame).toBeFunction();
        });
        it('should generate a jQuery object with one drawing node', function () {
            const drawingNode = drawingframe.createDrawingFrame('image');
            expect(drawingNode).toBeInstanceOf($);
            expect(drawingNode).toHaveLength(1);
        });
    });

    describe('function isDrawingFrame', function () {
        it('should exist', function () {
            expect(drawingframe.isDrawingFrame).toBeFunction();
        });
        it('should specify the generated drawing node as drawing frame', function () {
            const drawingNode = drawingframe.createDrawingFrame('image');
            expect(drawingframe.isDrawingFrame(drawingNode)).toBeTrue();
            expect(drawingframe.isDrawingFrame(null)).toBeFalse();
        });
    });

    describe('function getDrawingType', function () {
        it('should exist', function () {
            expect(drawingframe.getDrawingType).toBeFunction();
        });
        it('should return the type of the generated drawing node', function () {
            const drawingNode = drawingframe.createDrawingFrame('image');
            expect(drawingframe.getDrawingType(drawingNode)).toBe("image");
            expect(drawingframe.getDrawingType(null)).toBeUndefined();
        });
    });
});
