/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as drawinglinestylepicker from "@/io.ox/office/drawinglayer/view/control/drawinglinestylepicker";
import * as drawingarrowstylepicker from "@/io.ox/office/drawinglayer/view/control/drawingarrowstylepicker";
import * as drawingalignmentpicker from "@/io.ox/office/drawinglayer/view/control/drawingalignmentpicker";
import * as drawingarrangementpicker from "@/io.ox/office/drawinglayer/view/control/drawingarrangementpicker";
import * as shapetypepicker from "@/io.ox/office/drawinglayer/view/control/shapetypepicker";
import * as charttypepicker from "@/io.ox/office/drawinglayer/view/control/charttypepicker";
import * as chartcolorsetpicker from "@/io.ox/office/drawinglayer/view/control/chartcolorsetpicker";
import * as chartstylesetpicker from "@/io.ox/office/drawinglayer/view/control/chartstylesetpicker";
import * as chartlegendpicker from "@/io.ox/office/drawinglayer/view/control/chartlegendpicker";

import * as drawingcontrols from '@/io.ox/office/drawinglayer/view/drawingcontrols';

// tests ======================================================================

describe('module drawinglayer/view/drawingcontrols', function () {

    it("re-exports should exist", function () {
        expect(drawingcontrols).toContainProps(drawinglinestylepicker);
        expect(drawingcontrols).toContainProps(drawingarrowstylepicker);
        expect(drawingcontrols).toContainProps(drawingalignmentpicker);
        expect(drawingcontrols).toContainProps(drawingarrangementpicker);
        expect(drawingcontrols).toContainProps(shapetypepicker);
        expect(drawingcontrols).toContainProps(charttypepicker);
        expect(drawingcontrols).toContainProps(chartcolorsetpicker);
        expect(drawingcontrols).toContainProps(chartstylesetpicker);
        expect(drawingcontrols).toContainProps(chartlegendpicker);
    });
});
