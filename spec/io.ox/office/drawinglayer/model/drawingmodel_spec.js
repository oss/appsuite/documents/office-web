/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { AttributedModel } from '@/io.ox/office/editframework/model/attributedmodel';
import { DrawingModel, ShapeModel, ConnectorModel, ImageModel, GroupModel } from '@/io.ox/office/drawinglayer/model/drawingmodel';

// tests ======================================================================

describe('module drawinglayer/model/drawingmodel', function () {

    // class DrawingModel -----------------------------------------------------

    describe('class DrawingModel', function () {
        it('should subclass AttributedModel', function () {
            expect(DrawingModel).toBeSubClassOf(AttributedModel);
        });
    });

    // class ShapeModel -------------------------------------------------------

    describe('class ShapeModel', function () {
        it('should subclass DrawingModel', function () {
            expect(ShapeModel).toBeSubClassOf(DrawingModel);
        });
    });

    // class ConnectorModel ---------------------------------------------------

    describe('class ConnectorModel', function () {
        it('should subclass DrawingModel', function () {
            expect(ConnectorModel).toBeSubClassOf(DrawingModel);
        });
    });

    // class ImageModel -------------------------------------------------------

    describe('class ImageModel', function () {
        it('should subclass DrawingModel', function () {
            expect(ImageModel).toBeSubClassOf(DrawingModel);
        });
    });

    // class GroupModel -------------------------------------------------------

    describe('class GroupModel', function () {
        it('should subclass DrawingModel', function () {
            expect(GroupModel).toBeSubClassOf(DrawingModel);
        });
    });
});
