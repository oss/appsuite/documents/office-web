/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as basecontrols from "@/io.ox/office/baseframework/view/basecontrols";
import * as drawingcontrols from "@/io.ox/office/drawinglayer/view/drawingcontrols";

import * as toolpanebuttongroup from "@/io.ox/office/editframework/view/control/toolpanebuttongroup";
import * as toolpanepicker from "@/io.ox/office/editframework/view/control/toolpanepicker";
import * as toolpanecombinedmenu from "@/io.ox/office/editframework/view/control/toolpanecombinedmenu";
import * as applicationstatuslabel from "@/io.ox/office/editframework/view/control/applicationstatuslabel";
import * as newdocumentbutton from "@/io.ox/office/editframework/view/control/newdocumentbutton";
import * as acquireeditbutton from "@/io.ox/office/editframework/view/control/acquireeditbutton";
import * as filenamefield from "@/io.ox/office/editframework/view/control/filenamefield";
import * as colorpicker from "@/io.ox/office/editframework/view/control/colorpicker";
import * as borderflagspicker from "@/io.ox/office/editframework/view/control/borderflagspicker";
import * as borderstylepicker from "@/io.ox/office/editframework/view/control/borderstylepicker";
import * as borderwidthpicker from "@/io.ox/office/editframework/view/control/borderwidthpicker";
import * as fontfamilypicker from "@/io.ox/office/editframework/view/control/fontfamilypicker";
import * as fontsizepicker from "@/io.ox/office/editframework/view/control/fontsizepicker";
import * as stylesheetpicker from "@/io.ox/office/editframework/view/control/stylesheetpicker";
import * as languagepicker from "@/io.ox/office/editframework/view/control/languagepicker";
import * as tablesizepicker from "@/io.ox/office/editframework/view/control/tablesizepicker";
import * as tablestylepicker from "@/io.ox/office/editframework/view/control/tablestylepicker";

import * as editcontrols from '@/io.ox/office/editframework/view/editcontrols';

// tests ======================================================================

describe('module editframework/view/editcontrols', function () {

    it("re-exports should exist", function () {
        expect(editcontrols).toContainProps(basecontrols);
        expect(editcontrols).toContainProps(drawingcontrols);
        expect(editcontrols).toContainProps(toolpanebuttongroup);
        expect(editcontrols).toContainProps(toolpanepicker);
        expect(editcontrols).toContainProps(toolpanecombinedmenu);
        expect(editcontrols).toContainProps(applicationstatuslabel);
        expect(editcontrols).toContainProps(newdocumentbutton);
        expect(editcontrols).toContainProps(acquireeditbutton);
        expect(editcontrols).toContainProps(filenamefield);
        expect(editcontrols).toContainProps(colorpicker);
        expect(editcontrols).toContainProps(borderflagspicker);
        expect(editcontrols).toContainProps(borderstylepicker);
        expect(editcontrols).toContainProps(borderwidthpicker);
        expect(editcontrols).toContainProps(fontfamilypicker);
        expect(editcontrols).toContainProps(fontsizepicker);
        expect(editcontrols).toContainProps(stylesheetpicker);
        expect(editcontrols).toContainProps(languagepicker);
        expect(editcontrols).toContainProps(tablesizepicker);
        expect(editcontrols).toContainProps(tablestylepicker);
    });
});
