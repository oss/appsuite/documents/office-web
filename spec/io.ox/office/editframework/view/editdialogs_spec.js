/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as tkdialogs from "@/io.ox/office/tk/dialogs";
import * as drawingdialogs from "@/io.ox/office/drawinglayer/view/drawingdialogs";

import SaveAsFileDialog from "@/io.ox/office/editframework/view/dialog/saveasfiledialog";
import SaveAsTemplateDialog from "@/io.ox/office/editframework/view/dialog/saveastemplatedialog";
import ClipboardNoticeDialog from "@/io.ox/office/editframework/view/dialog/clipboardnoticedialog";
import UnsavedCommentsDialog from "@/io.ox/office/editframework/view/dialog/unsavedcommentsdialog";
import HyperlinkDialog from "@/io.ox/office/editframework/view/dialog/hyperlinkdialog";

import * as editdialogs from "@/io.ox/office/editframework/view/editdialogs";

// tests ======================================================================

describe("module editframework/view/editdialogs", function () {

    it("re-exports should exist", function () {
        expect(editdialogs).toContainProps(tkdialogs);
        expect(editdialogs).toContainProps(drawingdialogs);
        expect(editdialogs.SaveAsFileDialog).toBe(SaveAsFileDialog);
        expect(editdialogs.SaveAsTemplateDialog).toBe(SaveAsTemplateDialog);
        expect(editdialogs.ClipboardNoticeDialog).toBe(ClipboardNoticeDialog);
        expect(editdialogs.UnsavedCommentsDialog).toBe(UnsavedCommentsDialog);
        expect(editdialogs.HyperlinkDialog).toBe(HyperlinkDialog);
    });
});
