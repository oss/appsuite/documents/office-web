/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Color } from '@/io.ox/office/editframework/utils/color';
import {
    NO_BORDER, HAIR_WIDTH_HMM, THIN_WIDTH_HMM, MEDIUM_WIDTH_HMM, THICK_WIDTH_HMM,
    getCssBorderStyle, getBorderPattern, getCssBorderAttributes, getCssBorder, isVisibleBorder, equalBorders
} from '@/io.ox/office/editframework/utils/border';

import _ from '$/underscore';

// constants ==================================================================

const border = { style: 'solid', width: 1, color: Color.AUTO };
const noBorder = { style: 'none', width: 1, color: Color.AUTO };
const doubleBorder = { style: 'double', width: 73, color: Color.AUTO };
const dashedBorder = { style: 'dashed', width: 23, color: Color.AUTO };
const hairBorder = { style: 'dashed', width: 1, color: Color.AUTO };
const theme = { getSchemeColor() { return '000000'; } };

// tests ======================================================================

describe('module editframework/utils/border', function () {

    // constants --------------------------------------------------------------

    describe('constant NO_BORDER', function () {
        it('should exist', function () {
            expect(NO_BORDER).toBeObject();
        });
    });

    describe('constant HAIR_WIDTH_HMM', function () {
        it('should exist', function () {
            expect(HAIR_WIDTH_HMM).toBeNumber();
        });
    });

    describe('constant THIN_WIDTH_HMM', function () {
        it('should exist', function () {
            expect(THIN_WIDTH_HMM).toBeNumber();
        });
    });

    describe('constant MEDIUM_WIDTH_HMM', function () {
        it('should exist', function () {
            expect(MEDIUM_WIDTH_HMM).toBeNumber();
        });
    });

    describe('constant THICK_WIDTH_HMM', function () {
        it('should exist', function () {
            expect(THICK_WIDTH_HMM).toBeNumber();
        });
    });

    // functions --------------------------------------------------------------

    describe('function getCssBorderStyle', function () {
        it('should exist', function () {
            expect(getCssBorderStyle).toBeFunction();
        });
        it('should return correct CSS border style', function () {
            expect(getCssBorderStyle('none')).toBe("none");
            expect(getCssBorderStyle('solid')).toBe("solid");
            expect(getCssBorderStyle('double')).toBe("double");
            expect(getCssBorderStyle('triple')).toBe("double");
            expect(getCssBorderStyle('dashed')).toBe("dashed");
            expect(getCssBorderStyle('dashSmallGap')).toBe("dashed");
            expect(getCssBorderStyle('dotted')).toBe("dotted");
            expect(getCssBorderStyle('dotDash')).toBe("dotted");
            expect(getCssBorderStyle('dotDotDash')).toBe("dotted");
            expect(getCssBorderStyle('dashDotStroked')).toBe("dotted");
            expect(getCssBorderStyle('__invalid__')).toBe("solid");
        });
    });

    describe('function getBorderPattern', function () {
        it('should exist', function () {
            expect(getBorderPattern).toBeFunction();
        });
        it('should return pattern for dotted lines', function () {
            expect(getBorderPattern('dotted', 1)).toEqual([2, 2]);
            expect(getBorderPattern('dotted', 23)).toEqual([24, 24]);
        });
        it('should return pattern for dashed lines', function () {
            expect(getBorderPattern('dashed', 1)).toEqual([6, 2]);
        });
        it('should return null for unknown lines', function () {
            expect(getBorderPattern('unknown', 1)).toBeNull();
        });
    });

    describe('function getCssBorderAttributes', function () {
        it('should exist', function () {
            expect(getCssBorderAttributes).toBeFunction();
        });
        it('should return a map with CSS border attributes', function () {
            expect(getCssBorderAttributes(border, theme)).toEqual({ style: 'solid', width: 1, color: '#000000' });
            expect(getCssBorderAttributes(border, theme, { preview: true })).toEqual({ style: 'solid', width: 1, color: '#000000' });
            expect(getCssBorderAttributes(noBorder, theme)).toEqual({ style: 'none', width: 0, color: '#000000' });
            expect(getCssBorderAttributes(doubleBorder, theme)).toEqual({ style: 'double', width: 3, color: '#000000' });
            expect(getCssBorderAttributes(doubleBorder, theme, { preview: true })).toEqual({ style: 'double', width: 3, color: '#000000' });
            expect(getCssBorderAttributes(dashedBorder, theme)).toEqual({ style: 'dashed', width: 1, color: '#000000' });
        });
        it('should return a map with CSS border attributes for a hair line', function () {
            const attrs = getCssBorderAttributes(hairBorder, theme, { transparentHair: true });
            expect(attrs).toHaveProperty('style', 'dashed');
            expect(attrs).toHaveProperty('width', 1);
            expect(attrs).toHaveProperty('color', 'rgba(0,0,0,0.5)');
        });
    });

    describe('function getCssBorder', function () {
        it('should exist', function () {
            expect(getCssBorder).toBeFunction();
        });
        it('should return the value for a CSS border attribute', function () {
            expect(getCssBorder(border, theme)).toBe("solid 1px #000000");
            expect(getCssBorder(noBorder, theme)).toBe("none");
            expect(getCssBorder(noBorder, theme, { clearNone: true })).toBe("");
        });
    });

    describe('function isVisibleBorder', function () {
        it('should exist', function () {
            expect(isVisibleBorder).toBeFunction();
        });
        it('should return whether the border is visible', function () {
            expect(isVisibleBorder(null)).toBeFalse();
            expect(isVisibleBorder({})).toBeFalse();
            expect(isVisibleBorder(noBorder)).toBeFalse();
            expect(isVisibleBorder(border)).toBeTrue();
            expect(isVisibleBorder(dashedBorder)).toBeTrue();
            expect(isVisibleBorder({ style: 'solid', width: 0 })).toBeFalse();
        });
    });

    describe('function equalBorders', function () {
        it('should exist', function () {
            expect(equalBorders).toBeFunction();
        });
        it('should return whether the borders are equal', function () {
            expect(equalBorders(border, _.clone(border))).toBeTrue();
            expect(equalBorders(border, noBorder)).toBeFalse();
            expect(equalBorders(border, {})).toBeFalse();
            expect(equalBorders({}, {})).toBeTrue();
            expect(equalBorders(noBorder, noBorder)).toBeTrue();
        });
        it('should accept null values', function () {
            expect(equalBorders(null, border)).toBeFalse();
            expect(equalBorders(border, null)).toBeFalse();
            expect(equalBorders(null, noBorder)).toBeTrue();
            expect(equalBorders(noBorder, null)).toBeTrue();
            expect(equalBorders(null, null)).toBeTrue();
        });
    });
});
