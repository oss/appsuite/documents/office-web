/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { opPresetColor, Color } from '@/io.ox/office/editframework/utils/color';
import { createPatternDataURL, createCanvasPattern } from '@/io.ox/office/editframework/utils/pattern';

// private functions ==========================================================

// converts the passed byte array to a 32-bit array
function to32(a) {
    var result = [];
    for (var i = 0; i < a.length; i += 4) {
        result.push(0x1000000 * a[i] + 0x10000 * a[i + 1] + 0x100 * a[i + 2] + a[i + 3]);
    }
    return result;
}

// tests ======================================================================

describe('module editframework/utils/pattern', function () {

    describe('function createPatternDataURL', function () {
        it('should exist', function () {
            expect(createPatternDataURL).toBeFunction();
        });
        // ES6-LATER: better mocking for canvas code
        it.skip('should create the specified pattern', () => new Promise(resolve => {
            var url = createPatternDataURL('lgGrid', Color.WHITE, Color.RED);
            expect(url).toBeString();
            var image = document.createElement('img');
            image.onload = function () {
                expect(image.width).toBe(8);
                expect(image.height).toBe(8);
                var canvas = document.createElement('canvas');
                canvas.width = canvas.height = 8;
                var context = canvas.getContext('2d');
                context.drawImage(image, 0, 0);
                expect(to32(context.getImageData(0, 0, 8, 8).data)).toEqual([
                    0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff,
                    0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
                    0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
                    0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
                    0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
                    0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
                    0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
                    0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
                ]);
                resolve();
            };
            image.src = url;
        }));
        it('should return the same pattern for equivalent settings', function () {
            var url1 = createPatternDataURL('pct20', Color.WHITE, Color.RED);
            var url2 = createPatternDataURL('pct20', opPresetColor('white'), opPresetColor('red'));
            expect(url1).toBe(url2);
        });
    });

    describe('function createCanvasPattern', function () {
        it('should exist', function () {
            expect(createCanvasPattern).toBeFunction();
        });
        // ES6-LATER: better mocking for canvas code
        it.skip('should create the specified pattern', function () {
            var canvas = document.createElement('canvas');
            canvas.width = canvas.height = 8;
            var context = canvas.getContext('2d');
            var pattern = createCanvasPattern('smGrid', Color.WHITE, Color.RED);
            expect(pattern).toBeInstanceOf(window.CanvasPattern);
            context.fillStyle = pattern;
            context.fillRect(0, 0, 8, 8);
            expect(to32(context.getImageData(0, 0, 8, 8).data)).toEqual([
                0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff,
                0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff,
                0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff,
                0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff,
                0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff,
                0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff,
                0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff,
                0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff0000ff, 0xffffffff, 0xffffffff, 0xffffffff
            ]);
        });
        it('should return the same pattern for equivalent settings', function () {
            var pattern1 = createCanvasPattern('pct50', Color.WHITE, Color.RED);
            var pattern2 = createCanvasPattern('pct50', opPresetColor('white'), opPresetColor('red'));
            expect(pattern1).toBe(pattern2);
        });
    });
});
