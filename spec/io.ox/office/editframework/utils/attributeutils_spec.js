/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { cloneAttributeSet, hasAttribute, matchesAttributesSet, insertAttribute, deleteAttribute, getExplicitAttributes,
    getExplicitAttributeSet, getElementStyleId, hasEqualElementAttributes } from '@/io.ox/office/editframework/utils/attributeutils';

import $ from '$/jquery';

// constants ==================================================================

const node1 = $('<div>').data('attributes', { family: { attr: true }, styleId: 'style' });
const node2 = $('<div>').data('attributes', { family: { attr: true }, styleId: 'style' });
const node3 = $('<div>').data('attributes', { family: { attr: true } });
const node4 = $('<div>');

// tests ======================================================================

describe('module editframework/utils/attributeutils', function () {

    // functions --------------------------------------------------------------

    describe('function cloneAttributeSet', function () {
        it('should exist', function () {
            expect(cloneAttributeSet).toBeFunction();
        });
        const attrs1 = { a: 42, b: [1], c: { d: 1 } };
        const attrs2 = { a: 43, b: [2], c: { d: 2 } };
        const attrSet = { f1: attrs1, f2: attrs2 };
        it('should return a clone with the same attribute values', function () {
            const clone = cloneAttributeSet(attrSet);
            expect(clone).toEqual(attrSet);
            expect(clone.f1.a).toBe(attrSet.f1.a);
            expect(clone.f1.b).toBe(attrSet.f1.b);
            expect(clone.f2.c).toBe(attrSet.f2.c);
        });
        it('should return a clone with cloned attribute values', function () {
            const clone = cloneAttributeSet(attrSet, true);
            expect(clone).toEqual(attrSet);
            expect(clone.f1.b).not.toBe(attrSet.f1.b);
            expect(clone.f2.c).not.toBe(attrSet.f2.c);
        });
    });

    describe('function hasAttribute', function () {
        it('should exist', function () {
            expect(hasAttribute).toBeFunction();
        });
        const attrSet = { family: { attr: true } };
        it('should return true for existing attribute', function () {
            expect(hasAttribute(attrSet, 'family', 'attr')).toBeTrue();
        });
        it('should return false for missing attribute', function () {
            expect(hasAttribute(attrSet, 'family', 'missing')).toBeFalse();
            expect(hasAttribute(attrSet, 'missing', 'attr')).toBeFalse();
        });
    });

    describe('function matchesAttributesSet', function () {
        it('should exist', function () {
            expect(matchesAttributesSet).toBeFunction();
        });
    });

    describe('function insertAttribute', function () {
        it('should exist', function () {
            expect(insertAttribute).toBeFunction();
        });
        const attrSet = { family1: { attr1: true } };
        it('should insert a new attribute', function () {
            expect('attr2' in attrSet.family1).toBeFalse();
            insertAttribute(attrSet, 'family1', 'attr2', 42);
            expect(attrSet.family1).toEqual({ attr1: true, attr2: 42 });
        });
        it('should overwrite an attribute', function () {
            insertAttribute(attrSet, 'family1', 'attr1', 'abc');
            expect(attrSet.family1).toEqual({ attr1: 'abc', attr2: 42 });
        });
        it('should not overwrite an attribute if specified', function () {
            insertAttribute(attrSet, 'family1', 'attr1', 'this not', true);
            insertAttribute(attrSet, 'family1', 'attr3', 'but this', true);
            expect(attrSet.family1).toEqual({ attr1: 'abc', attr2: 42, attr3: 'but this' });
        });
        it('should insert a new family', function () {
            expect('family2' in attrSet).toBeFalse();
            insertAttribute(attrSet, 'family2', 'attr1', true);
            expect(attrSet.family2).toEqual({ attr1: true });
        });
    });

    describe('function deleteAttribute', function () {
        it('should exist', function () {
            expect(deleteAttribute).toBeFunction();
        });
        const attrSet = { family1: { attr1: true, attr2: 42 } };
        it('should delete an existing attribute', function () {
            deleteAttribute(attrSet, 'family1', 'attr2');
            expect(attrSet.family1).toEqual({ attr1: true });
        });
        it('should ignore missing attribute', function () {
            deleteAttribute(attrSet, 'family1', 'attr2');
            expect(attrSet.family1).toEqual({ attr1: true });
        });
        it('should ignore missing family', function () {
            deleteAttribute(attrSet, 'family2', 'attr1');
            expect(attrSet.family1).toEqual({ attr1: true });
        });
        it('should delete empty family', function () {
            deleteAttribute(attrSet, 'family1', 'attr1');
            expect(attrSet).toEqual({});
        });
    });

    describe('function getExplicitAttributes', function () {
        it('should exist', function () {
            expect(getExplicitAttributes).toBeFunction();
        });
        it('should return the attributes of a family', function () {
            const original = node1.data('attributes').family;
            const attrs1 = getExplicitAttributes(node1, 'family');
            expect(attrs1).toEqual(original);
            expect(attrs1).not.toBe(original);
            const attrs2 = getExplicitAttributes(node1, 'family', true);
            expect(attrs2).toBe(original);
        });
        it('should return empty object for missing family', function () {
            expect(getExplicitAttributes(node1, 'missing')).toEqual({});
            expect(getExplicitAttributes(node4, 'missing')).toEqual({});
        });
    });

    describe('function getExplicitAttributeSet', function () {
        it('should exist', function () {
            expect(getExplicitAttributeSet).toBeFunction();
        });
        it('should return the attributes of a family', function () {
            const original = node1.data('attributes');
            const attrSet1 = getExplicitAttributeSet(node1);
            expect(attrSet1).toEqual(original);
            expect(attrSet1).not.toBe(original);
            const attrSet2 = getExplicitAttributeSet(node1, true);
            expect(attrSet2).toBe(original);
        });
        it('should return empty object for missing family', function () {
            expect(getExplicitAttributeSet(node4)).toEqual({});
        });
    });

    describe('function getElementStyleId', function () {
        it('should exist', function () {
            expect(getElementStyleId).toBeFunction();
        });
        it('should return existing style identifier', function () {
            expect(getElementStyleId(node1)).toBe("style");
        });
        it('should return null for missing style identifier', function () {
            expect(getElementStyleId(node3)).toBeNull();
            expect(getElementStyleId(node4)).toBeNull();
        });
    });

    describe('function hasEqualElementAttributes', function () {
        it('should exist', function () {
            expect(hasEqualElementAttributes).toBeFunction();
        });
        it('should compare the element attribute sets deeply', function () {
            expect(hasEqualElementAttributes(node1, node2)).toBeTrue();
            expect(hasEqualElementAttributes(node1, node3)).toBeFalse();
        });
    });
});
