/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { RGBModel, HSLModel } from '@/io.ox/office/tk/dom';
import * as mod from '@/io.ox/office/editframework/utils/color';
import { Color } from '@/io.ox/office/editframework/utils/color';

// constants ==================================================================

// the default color scheme used in OX Documents
const COLOR_SCHEME = {
    accent1: '4f81bd',
    accent2: 'c0504d',
    accent3: '9bbb59',
    accent4: '8064a2',
    accent5: '4bacc6',
    accent6: 'f79646',
    text1: '000000',
    text2: '1f497d',
    light1: 'ffffff',
    light2: 'eeece1',
    dark1: '000000',
    dark2: '1f497d',
    background1: 'ffffff',
    background2: 'eeece1',
    hyperlink: '0000ff',
    followedHyperlink: '800080'
};

// mock the getSchemeColor() method of an Office design theme
const theme = { getSchemeColor(name) { return COLOR_SCHEME[name] || null; } };

// tests ======================================================================

describe('module editframework/utils/color', function () {

    // verifies a color channel in the passed color model (interval [0;1])
    function verifyChannel(colorModel, channelName, exp) {
        expect(colorModel[channelName]).toBeCloseTo(exp, 2);
    }

    // checks the 'rgb' property of the passed color descriptor
    function verifyRGB(colorDesc, expR, expG, expB, expA) {
        expect(colorDesc.rgb).toBeInstanceOf(RGBModel);
        verifyChannel(colorDesc.rgb, 'r', expR);
        verifyChannel(colorDesc.rgb, 'g', expG);
        verifyChannel(colorDesc.rgb, 'b', expB);
        verifyChannel(colorDesc.rgb, 'a', expA);
    }

    // checks the 'hsl' property of the passed color descriptor
    function verifyHSL(colorDesc, expH, expS, expL, expA) {
        expect(colorDesc.hsl).toBeInstanceOf(HSLModel);
        verifyChannel(colorDesc.hsl, 'h', expH);
        verifyChannel(colorDesc.hsl, 's', expS);
        verifyChannel(colorDesc.hsl, 'l', expL);
        verifyChannel(colorDesc.hsl, 'a', expA);
    }

    // constructs a color, and verifies the resolved color descriptor (no fall-backs, no transformations)
    function verifyColor(type, value, expR, expG, expB, expH, expS, expL, expA, expY, expHex, expCSS) {
        const colorDesc = Color.parseJSON({ type, value }).resolve('', theme);
        verifyRGB(colorDesc, expR, expG, expB, expA);
        verifyHSL(colorDesc, expH, expS, expL, expA);
        verifyChannel(colorDesc, 'a', expA);
        verifyChannel(colorDesc, 'y', expY);
        expect(colorDesc).toHaveProperty('hex', expHex.toUpperCase());
        expect(colorDesc).toHaveProperty('css', expCSS);
    }

    // constructs a color with transformations, and verifies the resolved color descriptor
    function verifyTransformedColor(css, type, value, expR, expG, expB, expH, expS, expL, expA, expY, expHex, expCSS) {
        const colorDesc = Color.parseCSS(css).transform(type, value).resolve('', theme);
        verifyRGB(colorDesc, expR, expG, expB, expA);
        verifyHSL(colorDesc, expH, expS, expL, expA);
        verifyChannel(colorDesc, 'a', expA);
        verifyChannel(colorDesc, 'y', expY);
        expect(colorDesc).toHaveProperty('hex', expHex.toUpperCase());
        expect(colorDesc).toHaveProperty('css', expCSS);
    }

    function verifyRgbTransformation(css, type, value, expR, expG, expB) {
        const colorDesc = Color.parseCSS(css).transform(type, value).resolve('', theme);
        verifyRGB(colorDesc, expR, expG, expB, 1);
        verifyChannel(colorDesc, 'a', 1);
    }

    // functions --------------------------------------------------------------

    const { transformOpColor } = mod;
    describe('function transformOpColor', function () {
        it('should exist', function () {
            expect(transformOpColor).toBeFunction();
        });
        it('should create JSON color transformations', function () {
            expect(transformOpColor(Color.BLACK, { alpha: 10000 })).toEqual({ type: 'rgb', value: '000000', transformations: [{ type: 'alpha', value: 10000 }] });
            expect(transformOpColor(Color.BLACK, { alpha: 10000 }, { comp: null })).toEqual({ type: 'rgb', value: '000000', transformations: [{ type: 'alpha', value: 10000 }, { type: 'comp' }] });
        });
        it('should clone color without transformations', function () {
            const opColor = transformOpColor(Color.BLACK);
            expect(opColor).toEqual(Color.BLACK);
            expect(opColor).not.toBe(Color.BLACK);
        });
        it('should clone color existing transformations', function () {
            const color1 = { type: 'rgb', value: 'ffffff', transformations: [{ comp: null }] };
            const color2 = transformOpColor(color1);
            expect(color2).toEqual(color1);
            expect(color2).not.toBe(color1);
            expect(color2.transformations).toEqual(color1.transformations);
            expect(color2.transformations).not.toBe(color1.transformations);
        });
    });

    const { opRgbColor } = mod;
    describe('function opRgbColor', function () {
        it('should exist', function () {
            expect(opRgbColor).toBeFunction();
        });
        it('should create JSON colors', function () {
            expect(opRgbColor('456789')).toEqual({ type: 'rgb', value: '456789' });
            expect(opRgbColor('456789', { alpha: 10000 })).toEqual({ type: 'rgb', value: '456789', transformations: [{ type: 'alpha', value: 10000 }] });
            expect(opRgbColor('456789', { alpha: 10000 }, { comp: null })).toEqual({ type: 'rgb', value: '456789', transformations: [{ type: 'alpha', value: 10000 }, { type: 'comp' }] });
        });
    });

    const { opHslColor } = mod;
    describe('function opHslColor', function () {
        it('should exist', function () {
            expect(opHslColor).toBeFunction();
        });
        it('should create JSON colors', function () {
            expect(opHslColor(0, 5000, 10000)).toEqual({ type: 'hsl', value: { h: 0, s: 5000, l: 10000 } });
            expect(opHslColor(0, 5000, 10000, { alpha: 10000 })).toEqual({ type: 'hsl', value: { h: 0, s: 5000, l: 10000 }, transformations: [{ type: 'alpha', value: 10000 }] });
            expect(opHslColor(0, 5000, 10000, { alpha: 10000 }, { comp: null })).toEqual({ type: 'hsl', value: { h: 0, s: 5000, l: 10000 }, transformations: [{ type: 'alpha', value: 10000 }, { type: 'comp' }] });
        });
    });

    const { opPresetColor } = mod;
    describe('function opPresetColor', function () {
        it('should exist', function () {
            expect(opPresetColor).toBeFunction();
        });
        it('should create JSON colors', function () {
            expect(opPresetColor('red')).toEqual({ type: 'preset', value: 'red' });
            expect(opPresetColor('red', { alpha: 10000 })).toEqual({ type: 'preset', value: 'red', transformations: [{ type: 'alpha', value: 10000 }] });
            expect(opPresetColor('red', { alpha: 10000 }, { comp: null })).toEqual({ type: 'preset', value: 'red', transformations: [{ type: 'alpha', value: 10000 }, { type: 'comp' }] });
        });
    });

    const { opSystemColor } = mod;
    describe('function opSystemColor', function () {
        it('should exist', function () {
            expect(opSystemColor).toBeFunction();
        });
        it('should create JSON colors', function () {
            expect(opSystemColor('text')).toEqual({ type: 'system', value: 'text' });
            expect(opSystemColor('text', { alpha: 10000 })).toEqual({ type: 'system', value: 'text', transformations: [{ type: 'alpha', value: 10000 }] });
            expect(opSystemColor('text', { alpha: 10000 }, { comp: null })).toEqual({ type: 'system', value: 'text', transformations: [{ type: 'alpha', value: 10000 }, { type: 'comp' }] });
        });
    });

    const { opSchemeColor } = mod;
    describe('function opSchemeColor', function () {
        it('should exist', function () {
            expect(opSchemeColor).toBeFunction();
        });
        it('should create JSON colors', function () {
            expect(opSchemeColor('accent2')).toEqual({ type: 'scheme', value: 'accent2' });
            expect(opSchemeColor('accent2', { alpha: 10000 })).toEqual({ type: 'scheme', value: 'accent2', transformations: [{ type: 'alpha', value: 10000 }] });
            expect(opSchemeColor('accent2', { alpha: 10000 }, { comp: null })).toEqual({ type: 'scheme', value: 'accent2', transformations: [{ type: 'alpha', value: 10000 }, { type: 'comp' }] });
        });
    });

    // class Color ------------------------------------------------------------

    describe('class Color', function () {

        it('should exist', function () {
            expect(Color).toBeClass();
        });

        function checkColorConstant(name, expected) {
            describe(`constant ${name}`, function () {
                it('should exist', function () {
                    expect(Color[name]).toEqual(expected);
                });
                it('should be frozen', function () {
                    expect(() => { Color[name].type = 'hsl'; }).toThrow();
                });
            });
        }

        checkColorConstant('AUTO', { type: 'auto' });
        checkColorConstant('BLACK', { type: 'rgb', value: '000000' });
        checkColorConstant('WHITE', { type: 'rgb', value: 'FFFFFF' });
        checkColorConstant('RED', { type: 'rgb', value: 'FF0000' });
        checkColorConstant('YELLOW', { type: 'rgb', value: 'FFFF00' });
        checkColorConstant('GREEN', { type: 'rgb', value: '00FF00' });
        checkColorConstant('CYAN', { type: 'rgb', value: '00FFFF' });
        checkColorConstant('BLUE', { type: 'rgb', value: '0000FF' });
        checkColorConstant('VIOLET', { type: 'rgb', value: 'FF00FF' });
        checkColorConstant('DARK1', { type: 'scheme', value: 'dark1' });
        checkColorConstant('DARK2', { type: 'scheme', value: 'dark2' });
        checkColorConstant('LIGHT1', { type: 'scheme', value: 'light1' });
        checkColorConstant('LIGHT2', { type: 'scheme', value: 'light2' });
        checkColorConstant('TEXT1', { type: 'scheme', value: 'text1' });
        checkColorConstant('TEXT2', { type: 'scheme', value: 'text2' });
        checkColorConstant('BACK1', { type: 'scheme', value: 'background1' });
        checkColorConstant('BACK2', { type: 'scheme', value: 'background2' });
        checkColorConstant('ACCENT1', { type: 'scheme', value: 'accent1' });
        checkColorConstant('ACCENT2', { type: 'scheme', value: 'accent2' });
        checkColorConstant('ACCENT3', { type: 'scheme', value: 'accent3' });
        checkColorConstant('ACCENT4', { type: 'scheme', value: 'accent4' });
        checkColorConstant('ACCENT5', { type: 'scheme', value: 'accent5' });
        checkColorConstant('ACCENT6', { type: 'scheme', value: 'accent6' });
        checkColorConstant('HYPERLINK', { type: 'scheme', value: 'hyperlink' });
        checkColorConstant('PLACEHOLDER', { type: 'scheme', value: 'phClr' });

        describe('static function parseJSON', function () {
            it('should exist', function () {
                expect(Color).toHaveStaticMethod('parseJSON');
            });
            it('should parse auto color', function () {
                const color = Color.parseJSON(Color.AUTO);
                expect(color).toBeInstanceOf(Color);
                expect(color.toJSON()).toEqual(Color.AUTO);
            });
            it('should parse RGB color', function () {
                const json = opRgbColor('4080C0');
                expect(Color.parseJSON(json).toJSON()).toEqual(json);
            });
            it('should parse HSL color', function () {
                const json = opHslColor(0, 5000, 10000);
                expect(Color.parseJSON(json).toJSON()).toEqual(json);
            });
            it('should parse transformations', function () {
                const json = opRgbColor('4080C0', { tint: 25000 });
                expect(Color.parseJSON(json).toJSON()).toEqual(json);
            });
            it('should parse fallback RGB', function () {
                const json = { ...Color.ACCENT1, fallbackValue: '4080C0' };
                expect(Color.parseJSON(json).toJSON()).toEqual(json);
            });
            it('should fall back to auto', function () {
                expect(Color.parseJSON({}).toJSON()).toEqual(Color.AUTO);
                expect(Color.parseJSON(null).toJSON()).toEqual(Color.AUTO);
            });
        });

        describe('static function parseCSS', function () {
            it('should exist', function () {
                expect(Color).toHaveStaticMethod('parseCSS');
            });
            it('should return null for missing or empty string', function () {
                expect(Color.parseCSS(null)).toBeNull();
                expect(Color.parseCSS('')).toBeNull();
            });
            it('should parse "transparent" keyword', function () {
                const color = Color.parseCSS('transparent');
                expect(color).toBeInstanceOf(Color);
                expect(color.toJSON()).toEqual(opRgbColor('000000', { alpha: 0 }));
            });
            it('should return automatic color for "transparent" keyword in fill mode', function () {
                expect(Color.parseCSS('transparent', true).toJSON()).toEqual(Color.AUTO);
            });
            it('should parse color keywords', function () {
                expect(Color.parseCSS('black').toJSON()).toEqual(opPresetColor('black'));
                expect(Color.parseCSS('white').toJSON()).toEqual(opPresetColor('white'));
                expect(Color.parseCSS('red').toJSON()).toEqual(opPresetColor('red'));
                expect(Color.parseCSS('green').toJSON()).toEqual(opPresetColor('green'));
                expect(Color.parseCSS('blue').toJSON()).toEqual(opPresetColor('blue'));
                expect(Color.parseCSS('wrong_color_name')).toBeNull();
            });
            it('should parse hexadecimal RGB color', function () {
                expect(Color.parseCSS('#4080c0').toJSON()).toEqual(opRgbColor('4080C0'));
                expect(Color.parseCSS('#4080C0').toJSON()).toEqual(opRgbColor('4080C0'));
                expect(Color.parseCSS('#48c').toJSON()).toEqual(opRgbColor('4488CC'));
            });
            it('should parse hexadecimal RGB color with alpha channel', function () {
                expect(Color.parseCSS('#4080c0cc').toJSON()).toEqual(opRgbColor('4080C0', { alpha: 80000 }));
                expect(Color.parseCSS('#4080C0CC').toJSON()).toEqual(opRgbColor('4080C0', { alpha: 80000 }));
                expect(Color.parseCSS('#4080C0FF').toJSON()).toEqual(opRgbColor('4080C0'));
                expect(Color.parseCSS('#4080C000').toJSON()).toEqual(opRgbColor('4080C0', { alpha: 0 }));
                expect(Color.parseCSS('#48cc').toJSON()).toEqual(opRgbColor('4488CC', { alpha: 80000 }));
                expect(Color.parseCSS('#48cf').toJSON()).toEqual(opRgbColor('4488CC'));
                expect(Color.parseCSS('#48c0').toJSON()).toEqual(opRgbColor('4488CC', { alpha: 0 }));
            });
            it('should reject invalid hexadecimal RGB color', function () {
                expect(Color.parseCSS('#')).toBeNull();
                expect(Color.parseCSS('#4')).toBeNull();
                expect(Color.parseCSS('#48')).toBeNull();
                expect(Color.parseCSS('#4080c')).toBeNull();
                expect(Color.parseCSS('#4080c04')).toBeNull();
                expect(Color.parseCSS('#4080c0400')).toBeNull();
                expect(Color.parseCSS('#4080cg')).toBeNull();
            });
            it('should parse function-style RGB(A) color', function () {
                expect(Color.parseCSS('rgb(64,128,192)').toJSON()).toEqual(opRgbColor('4080C0'));
                expect(Color.parseCSS('rgb( 0 , 127 , 255 )').toJSON()).toEqual(opRgbColor('007FFF'));
                expect(Color.parseCSS('rgb(64,128,192,1)').toJSON()).toEqual(opRgbColor('4080C0'));
                expect(Color.parseCSS('rgb( 64 , 128 , 192 , 0.42 )').toJSON()).toEqual(opRgbColor('4080C0', { alpha: 42000 }));
                expect(Color.parseCSS('rgb(64,128,192,0)').toJSON()).toEqual(opRgbColor('4080C0', { alpha: 0 }));
                expect(Color.parseCSS('rgb(64,128,192,0)', true).toJSON()).toEqual(Color.AUTO);
                expect(Color.parseCSS('rgba(64,128,192)').toJSON()).toEqual(opRgbColor('4080C0'));
                expect(Color.parseCSS('rgba( 0 , 127 , 255 )').toJSON()).toEqual(opRgbColor('007FFF'));
                expect(Color.parseCSS('rgba(64,128,192,1)').toJSON()).toEqual(opRgbColor('4080C0'));
                expect(Color.parseCSS('rgba( 64 , 128 , 192 , 0.42 )').toJSON()).toEqual(opRgbColor('4080C0', { alpha: 42000 }));
                expect(Color.parseCSS('rgba(64,128,192,0)').toJSON()).toEqual(opRgbColor('4080C0', { alpha: 0 }));
                expect(Color.parseCSS('rgba(64,128,192,0)', true).toJSON()).toEqual(Color.AUTO);
                expect(Color.parseCSS('rgb(64,128)')).toBeNull();
                expect(Color.parseCSS('rgb(64,128,1000)').toJSON()).toEqual(opRgbColor('4080FF'));
                expect(Color.parseCSS('rgb(64,128,ff)')).toBeNull();
                expect(Color.parseCSS('rgb(64,128,192,1,1)')).toBeNull();
                expect(Color.parseCSS('rgb(64,128,192,2)')).toBeNull();
                expect(Color.parseCSS('rgba(64,128,ff)')).toBeNull();
                expect(Color.parseCSS('rgba(64,128,192,1,1)')).toBeNull();
                expect(Color.parseCSS('rgba(64,128,192,2)')).toBeNull();
            });
            it('should parse function-style HSL(A) color', function () {
                expect(Color.parseCSS('hsl(60,100%,50%)').toJSON()).toEqual(opHslColor(3600000, 100000, 50000));
                expect(Color.parseCSS('hsl( 0 , 0% , 0% )').toJSON()).toEqual(opHslColor(0, 0, 0));
                expect(Color.parseCSS('hsl(60,100%,50%,1)').toJSON()).toEqual(opHslColor(3600000, 100000, 50000));
                expect(Color.parseCSS('hsl( 60 , 100% , 50% , 0.42 )').toJSON()).toEqual(opHslColor(3600000, 100000, 50000, { alpha: 42000 }));
                expect(Color.parseCSS('hsl(60,100%,50%,0)').toJSON()).toEqual(opHslColor(3600000, 100000, 50000, { alpha: 0 }));
                expect(Color.parseCSS('hsl(60,100%,50%,0)', true).toJSON()).toEqual(Color.AUTO);
                expect(Color.parseCSS('hsla(60,100%,50%)').toJSON()).toEqual(opHslColor(3600000, 100000, 50000));
                expect(Color.parseCSS('hsla( 0 , 0% , 0% )').toJSON()).toEqual(opHslColor(0, 0, 0));
                expect(Color.parseCSS('hsla(60,100%,50%,1)').toJSON()).toEqual(opHslColor(3600000, 100000, 50000));
                expect(Color.parseCSS('hsla( 60 , 100% , 50% , 0.42 )').toJSON()).toEqual(opHslColor(3600000, 100000, 50000, { alpha: 42000 }));
                expect(Color.parseCSS('hsla(60,100%,50%,0)').toJSON()).toEqual(opHslColor(3600000, 100000, 50000, { alpha: 0 }));
                expect(Color.parseCSS('hsla(60,100%,50%,0)', true).toJSON()).toEqual(Color.AUTO);
                expect(Color.parseCSS('hsl(60,100%)')).toBeNull();
                expect(Color.parseCSS('hsl(60,100%,50)')).toBeNull();
                expect(Color.parseCSS('hsl(60%,100%,50%)')).toBeNull();
                expect(Color.parseCSS('hsl(420,200%,50%)').toJSON()).toEqual(opHslColor(3600000, 100000, 50000));
                expect(Color.parseCSS('hsl(60,100%,ff%)')).toBeNull();
                expect(Color.parseCSS('hsl(60,100%,50%,1,1)')).toBeNull();
                expect(Color.parseCSS('hsl(60,100%,50%,2)')).toBeNull();
                expect(Color.parseCSS('hsla(60,100%,ff%)')).toBeNull();
                expect(Color.parseCSS('hsla(60,100%,50%,1,1)')).toBeNull();
                expect(Color.parseCSS('hsla(60,100%,50%,2)')).toBeNull();
            });
        });

        describe('method isAuto', function () {
            it('should exist', function () {
                expect(Color).toHaveMethod('isAuto');
            });
            const color = Color.auto();
            it('should return true for automatic color', function () {
                expect(color.isAuto()).toBeTrue();
            });
            it('should return false for all other colors', function () {
                expect(Color.fromRgb(0).isAuto()).toBeFalse();
                expect(Color.fromHsl(0, 0, 0).isAuto()).toBeFalse();
                expect(Color.fromCrgb(0, 0, 0).isAuto()).toBeFalse();
                expect(Color.fromPreset('black').isAuto()).toBeFalse();
                expect(Color.fromSystem('menu').isAuto()).toBeFalse();
                expect(Color.fromScheme('accent1').isAuto()).toBeFalse();
            });
        });

        describe('method equals', function () {
            it('should exist', function () {
                expect(Color).toHaveMethod('equals');
            });
            it('should return true for equal colors', function () {
                expect(Color.auto().equals(Color.auto())).toBeTrue();
                expect(Color.fromRgb('4080c0').equals(Color.fromRgb('4080C0'))).toBeTrue();
                expect(Color.fromHsl(25000, 50000, 75000).equals(Color.fromHsl(25000, 50000, 75000))).toBeTrue();
                expect(Color.fromCrgb(25000, 50000, 75000).equals(Color.fromCrgb(25000, 50000, 75000))).toBeTrue();
                expect(Color.fromPreset('black').equals(Color.fromPreset('Black'))).toBeFalse();
                expect(Color.fromPreset('black').equals(Color.fromPreset('black'))).toBeTrue();
                expect(Color.fromSystem('menu').equals(Color.fromSystem('Menu'))).toBeFalse();
                expect(Color.fromSystem('menu').equals(Color.fromSystem('menu'))).toBeTrue();
                expect(Color.fromScheme('accent1').equals(Color.fromScheme('Accent1'))).toBeFalse();
                expect(Color.fromScheme('accent1').equals(Color.fromScheme('accent1'))).toBeTrue();
                expect(Color.fromRgb('4080c0', { gray: null }).equals(Color.fromRgb('4080C0').transform('gray'))).toBeTrue();
            });
            it('should return false for different color types', function () {
                expect(Color.auto().equals(Color.fromRgb('4080c0'))).toBeFalse();
                expect(Color.fromRgb('4080c0').equals(Color.fromHsl(25000, 50000, 75000))).toBeFalse();
                expect(Color.fromSystem('menu').equals(Color.fromPreset('black'))).toBeFalse();
            });
            it('should return false for different color values', function () {
                expect(Color.fromRgb('4080c0').equals(Color.fromRgb('4080c1'))).toBeFalse();
                expect(Color.fromHsl(25000, 50000, 75000).equals(Color.fromHsl(25001, 50000, 75000))).toBeFalse();
                expect(Color.fromCrgb(25000, 50000, 75000).equals(Color.fromCrgb(25001, 50000, 75000))).toBeFalse();
                expect(Color.fromPreset('black').equals(Color.fromPreset('white'))).toBeFalse();
                expect(Color.fromSystem('menu').equals(Color.fromSystem('note'))).toBeFalse();
                expect(Color.fromScheme('accent1').equals(Color.fromScheme('accent2'))).toBeFalse();
            });
            it('should return false for different color transformations', function () {
                expect(Color.fromRgb('4080c0').equals(Color.fromRgb('4080c0', { gray: null }))).toBeFalse();
                expect(Color.fromRgb('4080c0', { gray: null }).equals(Color.fromRgb('4080c0', { inv: null }))).toBeFalse();
                expect(Color.fromRgb('4080c0', { gray: null }).equals(Color.fromRgb('4080c0', { gray: null }, { inv: null }))).toBeFalse();
                expect(Color.fromRgb('4080c0', { gray: null }, { inv: null }).equals(Color.fromRgb('4080c0', { inv: null }, { gray: null }))).toBeFalse();
                expect(Color.fromRgb('4080c0', { red: 25000 }).equals(Color.fromRgb('4080c0', { green: 25000 }))).toBeFalse();
                expect(Color.fromRgb('4080c0', { red: 25000 }).equals(Color.fromRgb('4080c0', { red: 25001 }))).toBeFalse();
            });
        });

        describe('method clone', function () {
            it('should exist', function () {
                expect(Color).toHaveMethod('clone');
            });
            it('should create a clone', function () {
                expect(Color.auto().clone()).toBeInstanceOf(Color);
                const color = Color.fromRgb('4080c0', { gray: null });
                const clone = color.clone();
                expect(clone).toBeInstanceOf(Color);
                expect(clone).not.toBe(color);
                expect(clone.equals(color)).toBeTrue();
            });
        });

        describe('method transform', function () {
            it('should exist', function () {
                expect(Color).toHaveMethod('transform');
            });
            it('should store transformations passed as value', function () {
                const color = Color.auto();
                expect(color.transform('alpha', 50000)).toBe(color);
                color.transform('gray');
                expect(color.toJSON()).toEqual(transformOpColor(Color.AUTO, { alpha: 50000 }, { gray: null }));
            });
        });

        describe('method resolve', function () {
            it('should exist', function () {
                expect(Color).toHaveMethod('resolve');
            });
            it('should use preset fall-back values for automatic color', function () {
                expect(Color.auto().resolve('text')).toHaveProperty('css', '#000000');
                expect(Color.auto().resolve('line')).toHaveProperty('css', '#000000');
                expect(Color.auto().resolve('fill')).toHaveProperty('css', 'transparent');
            });
            it('should use fallback color for automatic color', function () {
                const desc = Color.auto().resolve(Color.fromRgb('4080c0'));
                expect(desc).toHaveProperty('css', '#4080c0');
            });
            it('should return color descriptor for scheme colors', function () {
                verifyColor('scheme', 'accent1', 0.310, 0.506, 0.741, 212.727, 0.455, 0.525, 1, 0.481, '4f81bd', '#4f81bd');
                verifyColor('scheme', 'accent2', 0.753, 0.314, 0.302, 1.565, 0.477, 0.527, 1, 0.406, 'c0504d', '#c0504d');
                verifyColor('scheme', 'accent3', 0.608, 0.733, 0.349, 79.592, 0.419, 0.541, 1, 0.679, '9bbb59', '#9bbb59');
                verifyColor('scheme', 'accent4', 0.502, 0.392, 0.635, 267.097, 0.25, 0.514, 1, 0.433, '8064a2', '#8064a2');
                verifyColor('scheme', 'accent5', 0.294, 0.675, 0.776, 192.683, 0.519, 0.535, 1, 0.601, '4bacc6', '#4bacc6');
                verifyColor('scheme', 'accent6', 0.969, 0.588, 0.275, 27.119, 0.917, 0.622, 1, 0.646, 'f79646', '#f79646');
            });
            it('should return color descriptor for preset colors', function () {
                verifyColor('preset', 'black', 0, 0, 0, 0, 0, 0, 1, 0, '000000', '#000000');
                verifyColor('preset', 'Black', 0, 0, 0, 0, 0, 0, 1, 0, '000000', '#000000');
                verifyColor('preset', 'blue', 0, 0, 1, 240, 1, 0.5, 1, 0.072, '0000ff', '#0000ff');
                verifyColor('preset', 'brown', 0.647, 0.165, 0.165, 0, 0.594, 0.406, 1, 0.267, 'a52a2a', '#a52a2a');
                verifyColor('preset', 'cyan', 0, 1, 1, 180, 1, 0.5, 1, 0.787, '00ffff', '#00ffff');
                verifyColor('preset', 'gray', 0.502, 0.502, 0.502, 0, 0, 0.502, 1, 0.502, '808080', '#808080');
                verifyColor('preset', 'green', 0, 0.502, 0, 120, 1, 0.251, 1, 0.359, '008000', '#008000');
                verifyColor('preset', 'lime', 0, 1, 0, 120, 1, 0.5, 1, 0.715, '00ff00', '#00ff00');
                verifyColor('preset', 'magenta', 1, 0, 1, 300, 1, 0.5, 1, 0.285, 'ff00ff', '#ff00ff');
                verifyColor('preset', 'orange', 1, 0.647, 0, 38.824, 1, 0.5, 1, 0.675, 'ffa500', '#ffa500');
                verifyColor('preset', 'pink', 1, 0.753, 0.796, 349.524, 1, 0.876, 1, 0.809, 'ffc0cb', '#ffc0cb');
                verifyColor('preset', 'red', 1, 0, 0, 0, 1, 0.5, 1, 0.213, 'ff0000', '#ff0000');
                verifyColor('preset', 'turquoise', 0.251, 0.878, 0.816, 174, 0.721, 0.565, 1, 0.741, '40e0d0', '#40e0d0');
                verifyColor('preset', 'white', 1, 1, 1, 0, 0, 1, 1, 1, 'ffffff', '#ffffff');
                verifyColor('preset', 'yellow', 1, 1, 0, 60, 1, 0.5, 1, 0.928, 'ffff00', '#ffff00');
            });
            it('should return color descriptor for RGB colors', function () {
                verifyColor('rgb', '000000', 0, 0, 0, 0, 0, 0, 1, 0, '000000', '#000000');
                verifyColor('rgb', 'FFFFFF', 1, 1, 1, 0, 0, 1, 1, 1, 'ffffff', '#ffffff');
                verifyColor('rgb', 'FF0000', 1, 0, 0, 0, 1, 0.5, 1, 0.213, 'ff0000', '#ff0000');
                verifyColor('rgb', 'FFFF00', 1, 1, 0, 60, 1, 0.5, 1, 0.928, 'ffff00', '#ffff00');
                verifyColor('rgb', '00FF00', 0, 1, 0, 120, 1, 0.5, 1, 0.715, '00ff00', '#00ff00');
                verifyColor('rgb', '00FFFF', 0, 1, 1, 180, 1, 0.5, 1, 0.787, '00ffff', '#00ffff');
                verifyColor('rgb', '0000FF', 0, 0, 1, 240, 1, 0.5, 1, 0.072, '0000ff', '#0000ff');
                verifyColor('rgb', 'FF00FF', 1, 0, 1, 300, 1, 0.5, 1, 0.285, 'ff00ff', '#ff00ff');
                verifyColor('rgb', '808080', 0.502, 0.502, 0.502, 0, 0, 0.502, 1, 0.502, '808080', '#808080');
                verifyColor('rgb', '4080c0', 0.251, 0.502, 0.753, 210, 0.504, 0.502, 1, 0.467, '4080c0', '#4080c0');
            });
            it('should return color descriptor for CRGB colors', function () {
                verifyColor('crgb', { r: 0, g: 0, b: 0 }, 0, 0, 0, 0, 0, 0, 1, 0, '000000', '#000000');
                verifyColor('crgb', { r: 25000, g: 25000, b: 25000 }, 0.543, 0.543, 0.543, 0, 0, 0.543, 1, 0.543, '8a8a8a', '#8a8a8a');
                verifyColor('crgb', { r: 50000, g: 50000, b: 50000 }, 0.737, 0.737, 0.737, 0, 0, 0.737, 1, 0.737, 'bcbcbc', '#bcbcbc');
                verifyColor('crgb', { r: 75000, g: 75000, b: 75000 }, 0.881, 0.881, 0.881, 0, 0, 0.881, 1, 0.881, 'e1e1e1', '#e1e1e1');
                verifyColor('crgb', { r: 100000, g: 100000, b: 100000 }, 1, 1, 1, 0, 0, 1, 1, 1, 'ffffff', '#ffffff');
                verifyColor('crgb', { r: 25000, g: 50000, b: 75000 }, 0.543, 0.737, 0.881, 205.58, 0.587, 0.712, 1, 0.706, '8abce1', '#8abce1');
            });
            it('should return color descriptor for HSL colors', function () {
                verifyColor('hsl', { h: 0, s: 0, l: 0 }, 0, 0, 0, 0, 0, 0, 1, 0, '000000', 'hsl(0,0%,0%)');
                verifyColor('hsl', { h: 5400000, s: 0, l: 0 }, 0, 0, 0, 90, 0, 0, 1, 0, '000000', 'hsl(90,0%,0%)');
                verifyColor('hsl', { h: 10800000, s: 0, l: 0 }, 0, 0, 0, 180, 0, 0, 1, 0, '000000', 'hsl(180,0%,0%)');
                verifyColor('hsl', { h: 16200000, s: 0, l: 0 }, 0, 0, 0, 270, 0, 0, 1, 0, '000000', 'hsl(270,0%,0%)');
                verifyColor('hsl', { h: 0, s: 25000, l: 0 }, 0, 0, 0, 0, 0.25, 0, 1, 0, '000000', 'hsl(0,25%,0%)');
                verifyColor('hsl', { h: 5400000, s: 50000, l: 0 }, 0, 0, 0, 90, 0.5, 0, 1, 0, '000000', 'hsl(90,50%,0%)');
                verifyColor('hsl', { h: 10800000, s: 75000, l: 0 }, 0, 0, 0, 180, 0.75, 0, 1, 0, '000000', 'hsl(180,75%,0%)');
                verifyColor('hsl', { h: 16200000, s: 100000, l: 0 }, 0, 0, 0, 270, 1, 0, 1, 0, '000000', 'hsl(270,100%,0%)');
                verifyColor('hsl', { h: 0, s: 0, l: 25000 }, 0.25, 0.25, 0.25, 0, 0, 0.25, 1, 0.25, '404040', 'hsl(0,0%,25%)');
                verifyColor('hsl', { h: 5400000, s: 0, l: 50000 }, 0.5, 0.5, 0.5, 90, 0, 0.5, 1, 0.5, '808080', 'hsl(90,0%,50%)');
                verifyColor('hsl', { h: 10800000, s: 0, l: 75000 }, 0.75, 0.75, 0.75, 180, 0, 0.75, 1, 0.75, 'bfbfbf', 'hsl(180,0%,75%)');
                verifyColor('hsl', { h: 16200000, s: 0, l: 100000 }, 1, 1, 1, 270, 0, 1, 1, 1, 'ffffff', 'hsl(270,0%,100%)');
                verifyColor('hsl', { h: 0, s: 50000, l: 100000 }, 1, 1, 1, 0, 0.5, 1, 1, 1, 'ffffff', 'hsl(0,50%,100%)');
                verifyColor('hsl', { h: 10800000, s: 100000, l: 100000 }, 1, 1, 1, 180, 1, 1, 1, 1, 'ffffff', 'hsl(180,100%,100%)');
                verifyColor('hsl', { h: 600000, s: 20000, l: 20000 }, 0.24, 0.173, 0.16, 10, 0.2, 0.2, 1, 0.187, '3d2c29', 'hsl(10,20%,20%)');
                verifyColor('hsl', { h: 3000000, s: 20000, l: 50000 }, 0.6, 0.567, 0.4, 50, 0.2, 0.5, 1, 0.562, '999166', 'hsl(50,20%,50%)');
                verifyColor('hsl', { h: 5400000, s: 20000, l: 80000 }, 0.8, 0.84, 0.76, 90, 0.2, 0.8, 1, 0.826, 'ccd6c2', 'hsl(90,20%,80%)');
                verifyColor('hsl', { h: 7800000, s: 50000, l: 20000 }, 0.1, 0.3, 0.133, 130, 0.5, 0.2, 1, 0.245, '1a4d22', 'hsl(130,50%,20%)');
                verifyColor('hsl', { h: 10200000, s: 50000, l: 50000 }, 0.25, 0.75, 0.667, 170, 0.5, 0.5, 1, 0.638, '40bfaa', 'hsl(170,50%,50%)');
                verifyColor('hsl', { h: 12600000, s: 50000, l: 80000 }, 0.7, 0.8, 0.9, 210, 0.5, 0.8, 1, 0.786, 'b3cce6', 'hsl(210,50%,80%)');
                verifyColor('hsl', { h: 15000000, s: 80000, l: 20000 }, 0.093, 0.04, 0.36, 250, 0.8, 0.2, 1, 0.074, '180a5c', 'hsl(250,80%,20%)');
                verifyColor('hsl', { h: 17400000, s: 80000, l: 50000 }, 0.767, 0.1, 0.9, 290, 0.8, 0.5, 1, 0.299, 'c41ae6', 'hsl(290,80%,50%)');
                verifyColor('hsl', { h: 19800000, s: 80000, l: 80000 }, 0.96, 0.64, 0.8, 330, 0.8, 0.8, 1, 0.72, 'f5a3cc', 'hsl(330,80%,80%)');
            });
            it('should return "dark" flag', function () {
                expect(Color.fromPreset('black').resolve('')).toHaveProperty('dark', true);
                expect(Color.fromPreset('white').resolve('')).toHaveProperty('dark', false);
            });
            it('should resolve color with "alpha" transformation', function () {
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alpha', 0, 1, 0, 0, 0, 1, 0.5, 0, 0.213, 'ff0000', 'transparent');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alpha', 25000, 1, 0, 0, 0, 1, 0.5, 0.25, 0.213, 'ff0000', 'rgba(255,0,0,0.25)');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alpha', 50000, 1, 0, 0, 0, 1, 0.5, 0.5, 0.213, 'ff0000', 'rgba(255,0,0,0.5)');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alpha', 75000, 1, 0, 0, 0, 1, 0.5, 0.75, 0.213, 'ff0000', 'rgba(255,0,0,0.75)');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alpha', 100000, 1, 0, 0, 0, 1, 0.5, 1, 0.213, 'ff0000', '#ff0000');
            });
            it('should resolve color with "alphaMod" transformation', function () {
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaMod', 0, 1, 0, 0, 0, 1, 0.5, 0, 0.213, 'ff0000', 'transparent');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaMod', 25000, 1, 0, 0, 0, 1, 0.5, 0.125, 0.213, 'ff0000', 'rgba(255,0,0,0.125)');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaMod', 50000, 1, 0, 0, 0, 1, 0.5, 0.25, 0.213, 'ff0000', 'rgba(255,0,0,0.25)');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaMod', 75000, 1, 0, 0, 0, 1, 0.5, 0.375, 0.213, 'ff0000', 'rgba(255,0,0,0.375)');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaMod', 100000, 1, 0, 0, 0, 1, 0.5, 0.5, 0.213, 'ff0000', 'rgba(255,0,0,0.5)');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaMod', 150000, 1, 0, 0, 0, 1, 0.5, 0.75, 0.213, 'ff0000', 'rgba(255,0,0,0.75)');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaMod', 200000, 1, 0, 0, 0, 1, 0.5, 1, 0.213, 'ff0000', '#ff0000');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaMod', 300000, 1, 0, 0, 0, 1, 0.5, 1, 0.213, 'ff0000', '#ff0000');
            });
            it('should resolve color with "alphaOff" transformation', function () {
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaOff', -75000, 1, 0, 0, 0, 1, 0.5, 0, 0.213, 'ff0000', 'transparent');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaOff', -50000, 1, 0, 0, 0, 1, 0.5, 0, 0.213, 'ff0000', 'transparent');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaOff', -25000, 1, 0, 0, 0, 1, 0.5, 0.25, 0.213, 'ff0000', 'rgba(255,0,0,0.25)');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaOff', 0, 1, 0, 0, 0, 1, 0.5, 0.5, 0.213, 'ff0000', 'rgba(255,0,0,0.5)');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaOff', 25000, 1, 0, 0, 0, 1, 0.5, 0.75, 0.213, 'ff0000', 'rgba(255,0,0,0.75)');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaOff', 50000, 1, 0, 0, 0, 1, 0.5, 1, 0.213, 'ff0000', '#ff0000');
                verifyTransformedColor('rgba(255,0,0,0.5)', 'alphaOff', 750000, 1, 0, 0, 0, 1, 0.5, 1, 0.213, 'ff0000', '#ff0000');
            });
            it('should resolve color with "red" transformation', function () {
                verifyRgbTransformation('#666666', 'red', 0, 0, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'red', 20000, 0.492, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'red', 40000, 0.668, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'red', 60000, 0.798, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'red', 80000, 0.906, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'red', 100000, 1, 0.4, 0.4);
            });
            it('should resolve color with "redMod" transformation', function () {
                verifyRgbTransformation('#666666', 'redMod', 0, 0, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redMod', 50000, 0.295, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redMod', 100000, 0.4, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redMod', 150000, 0.478, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redMod', 200000, 0.543, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redMod', 300000, 0.649, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redMod', 500000, 0.813, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redMod', 900000, 1, 0.4, 0.4);
            });
            it('should resolve color with "redOff" transformation', function () {
                verifyRgbTransformation('#666666', 'redOff', -20000, 0, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redOff', -10000, 0.197, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redOff', 0, 0.4, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redOff', 10000, 0.518, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redOff', 40000, 0.753, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redOff', 70000, 0.919, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'redOff', 90000, 1, 0.4, 0.4);
            });
            it('should resolve color with "green" transformation', function () {
                verifyRgbTransformation('#666666', 'green', 0, 0.4, 0, 0.4);
                verifyRgbTransformation('#666666', 'green', 20000, 0.4, 0.492, 0.4);
                verifyRgbTransformation('#666666', 'green', 40000, 0.4, 0.668, 0.4);
                verifyRgbTransformation('#666666', 'green', 60000, 0.4, 0.798, 0.4);
                verifyRgbTransformation('#666666', 'green', 80000, 0.4, 0.906, 0.4);
                verifyRgbTransformation('#666666', 'green', 100000, 0.4, 1, 0.4);
            });
            it('should resolve color with "greenMod" transformation', function () {
                verifyRgbTransformation('#666666', 'greenMod', 0, 0.4, 0, 0.4);
                verifyRgbTransformation('#666666', 'greenMod', 50000, 0.4, 0.295, 0.4);
                verifyRgbTransformation('#666666', 'greenMod', 100000, 0.4, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'greenMod', 150000, 0.4, 0.478, 0.4);
                verifyRgbTransformation('#666666', 'greenMod', 200000, 0.4, 0.543, 0.4);
                verifyRgbTransformation('#666666', 'greenMod', 300000, 0.4, 0.649, 0.4);
                verifyRgbTransformation('#666666', 'greenMod', 500000, 0.4, 0.813, 0.4);
                verifyRgbTransformation('#666666', 'greenMod', 900000, 0.4, 1, 0.4);
            });
            it('should resolve color with "greenOff" transformation', function () {
                verifyRgbTransformation('#666666', 'greenOff', -20000, 0.4, 0, 0.4);
                verifyRgbTransformation('#666666', 'greenOff', -10000, 0.4, 0.197, 0.4);
                verifyRgbTransformation('#666666', 'greenOff', 0, 0.4, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'greenOff', 10000, 0.4, 0.518, 0.4);
                verifyRgbTransformation('#666666', 'greenOff', 40000, 0.4, 0.753, 0.4);
                verifyRgbTransformation('#666666', 'greenOff', 70000, 0.4, 0.919, 0.4);
                verifyRgbTransformation('#666666', 'greenOff', 90000, 0.4, 1, 0.4);
            });
            it('should resolve color with "blue" transformation', function () {
                verifyRgbTransformation('#666666', 'blue', 0, 0.4, 0.4, 0);
                verifyRgbTransformation('#666666', 'blue', 20000, 0.4, 0.4, 0.492);
                verifyRgbTransformation('#666666', 'blue', 40000, 0.4, 0.4, 0.668);
                verifyRgbTransformation('#666666', 'blue', 60000, 0.4, 0.4, 0.798);
                verifyRgbTransformation('#666666', 'blue', 80000, 0.4, 0.4, 0.906);
                verifyRgbTransformation('#666666', 'blue', 100000, 0.4, 0.4, 1);
            });
            it('should resolve color with "blueMod" transformation', function () {
                verifyRgbTransformation('#666666', 'blueMod', 0, 0.4, 0.4, 0);
                verifyRgbTransformation('#666666', 'blueMod', 50000, 0.4, 0.4, 0.295);
                verifyRgbTransformation('#666666', 'blueMod', 100000, 0.4, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'blueMod', 150000, 0.4, 0.4, 0.478);
                verifyRgbTransformation('#666666', 'blueMod', 200000, 0.4, 0.4, 0.543);
                verifyRgbTransformation('#666666', 'blueMod', 300000, 0.4, 0.4, 0.649);
                verifyRgbTransformation('#666666', 'blueMod', 500000, 0.4, 0.4, 0.813);
                verifyRgbTransformation('#666666', 'blueMod', 900000, 0.4, 0.4, 1);
            });
            it('should resolve color with "blueOff" transformation', function () {
                verifyRgbTransformation('#666666', 'blueOff', -20000, 0.4, 0.4, 0);
                verifyRgbTransformation('#666666', 'blueOff', -10000, 0.4, 0.4, 0.197);
                verifyRgbTransformation('#666666', 'blueOff', 0, 0.4, 0.4, 0.4);
                verifyRgbTransformation('#666666', 'blueOff', 10000, 0.4, 0.4, 0.518);
                verifyRgbTransformation('#666666', 'blueOff', 40000, 0.4, 0.4, 0.753);
                verifyRgbTransformation('#666666', 'blueOff', 70000, 0.4, 0.4, 0.919);
                verifyRgbTransformation('#666666', 'blueOff', 90000, 0.4, 0.4, 1);
            });
            it('should resolve color with "hue" transformation', function () {
                verifyTransformedColor('hsl(120,40%,40%)', 'hue', 0, 0.56, 0.24, 0.24, 0, 0.4, 0.4, 1, 0.308, '8f3d3d', 'hsl(0,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hue', 3600000, 0.56, 0.56, 0.24, 60, 0.4, 0.4, 1, 0.537, '8f8f3d', 'hsl(60,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hue', 7200000, 0.24, 0.56, 0.24, 120, 0.4, 0.4, 1, 0.469, '3d8f3d', 'hsl(120,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hue', 10800000, 0.24, 0.56, 0.56, 180, 0.4, 0.4, 1, 0.492, '3d8f8f', 'hsl(180,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hue', 14400000, 0.24, 0.24, 0.56, 240, 0.4, 0.4, 1, 0.263, '3d3d8f', 'hsl(240,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hue', 18000000, 0.56, 0.24, 0.56, 300, 0.4, 0.4, 1, 0.331, '8f3d8f', 'hsl(300,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hue', 21600000, 0.56, 0.24, 0.24, 0, 0.4, 0.4, 1, 0.308, '8f3d3d', 'hsl(0,40%,40%)');
            });
            it('should resolve color with "hueMod" transformation', function () {
                verifyTransformedColor('hsl(120,40%,40%)', 'hueMod', 0, 0.56, 0.24, 0.24, 0, 0.4, 0.4, 1, 0.308, '8f3d3d', 'hsl(0,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueMod', 50000, 0.56, 0.56, 0.24, 60, 0.4, 0.4, 1, 0.537, '8f8f3d', 'hsl(60,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueMod', 100000, 0.24, 0.56, 0.24, 120, 0.4, 0.4, 1, 0.469, '3d8f3d', 'hsl(120,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueMod', 150000, 0.24, 0.56, 0.56, 180, 0.4, 0.4, 1, 0.492, '3d8f8f', 'hsl(180,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueMod', 200000, 0.24, 0.24, 0.56, 240, 0.4, 0.4, 1, 0.263, '3d3d8f', 'hsl(240,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueMod', 250000, 0.56, 0.24, 0.56, 300, 0.4, 0.4, 1, 0.331, '8f3d8f', 'hsl(300,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueMod', 300000, 0.56, 0.24, 0.24, 0, 0.4, 0.4, 1, 0.308, '8f3d3d', 'hsl(0,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueMod', 350000, 0.56, 0.24, 0.24, 0, 0.4, 0.4, 1, 0.308, '8f3d3d', 'hsl(0,40%,40%)');
            });
            it('should resolve color with "hueOff" transformation', function () {
                verifyTransformedColor('hsl(120,40%,40%)', 'hueOff', -10800000, 0.56, 0.24, 0.24, 0, 0.4, 0.4, 1, 0.308, '8f3d3d', 'hsl(0,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueOff', -7200000, 0.56, 0.24, 0.24, 0, 0.4, 0.4, 1, 0.308, '8f3d3d', 'hsl(0,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueOff', -3600000, 0.56, 0.56, 0.24, 60, 0.4, 0.4, 1, 0.537, '8f8f3d', 'hsl(60,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueOff', 0, 0.24, 0.56, 0.24, 120, 0.4, 0.4, 1, 0.469, '3d8f3d', 'hsl(120,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueOff', 3600000, 0.24, 0.56, 0.56, 180, 0.4, 0.4, 1, 0.492, '3d8f8f', 'hsl(180,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueOff', 7200000, 0.24, 0.24, 0.56, 240, 0.4, 0.4, 1, 0.263, '3d3d8f', 'hsl(240,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueOff', 10800000, 0.56, 0.24, 0.56, 300, 0.4, 0.4, 1, 0.331, '8f3d8f', 'hsl(300,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueOff', 14400000, 0.56, 0.24, 0.24, 0, 0.4, 0.4, 1, 0.308, '8f3d3d', 'hsl(0,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'hueOff', 18000000, 0.56, 0.24, 0.24, 0, 0.4, 0.4, 1, 0.308, '8f3d3d', 'hsl(0,40%,40%)');
            });
            it('should resolve color with "sat" transformation', function () {
                verifyTransformedColor('hsl(120,40%,40%)', 'sat', 0, 0.4, 0.4, 0.4, 120, 0, 0.4, 1, 0.4, '666666', 'hsl(120,0%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'sat', 20000, 0.32, 0.48, 0.32, 120, 0.2, 0.4, 1, 0.434, '527a52', 'hsl(120,20%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'sat', 40000, 0.24, 0.56, 0.24, 120, 0.4, 0.4, 1, 0.469, '3d8f3d', 'hsl(120,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'sat', 60000, 0.16, 0.64, 0.16, 120, 0.6, 0.4, 1, 0.503, '29a329', 'hsl(120,60%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'sat', 80000, 0.08, 0.72, 0.08, 120, 0.8, 0.4, 1, 0.538, '14b814', 'hsl(120,80%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'sat', 100000, 0, 0.8, 0, 120, 1, 0.4, 1, 0.572, '00cc00', 'hsl(120,100%,40%)');
            });
            it('should resolve color with "satMod" transformation', function () {
                verifyTransformedColor('hsl(120,40%,40%)', 'satMod', 0, 0.4, 0.4, 0.4, 120, 0, 0.4, 1, 0.4, '666666', 'hsl(120,0%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satMod', 50000, 0.32, 0.48, 0.32, 120, 0.2, 0.4, 1, 0.434, '527a52', 'hsl(120,20%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satMod', 100000, 0.24, 0.56, 0.24, 120, 0.4, 0.4, 1, 0.469, '3d8f3d', 'hsl(120,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satMod', 150000, 0.16, 0.64, 0.16, 120, 0.6, 0.4, 1, 0.503, '29a329', 'hsl(120,60%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satMod', 200000, 0.08, 0.72, 0.08, 120, 0.8, 0.4, 1, 0.538, '14b814', 'hsl(120,80%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satMod', 250000, 0, 0.8, 0, 120, 1, 0.4, 1, 0.572, '00cc00', 'hsl(120,100%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satMod', 300000, 0, 0.8, 0, 120, 1, 0.4, 1, 0.572, '00cc00', 'hsl(120,100%,40%)');
            });
            it('should resolve color with "satOff" transformation', function () {
                verifyTransformedColor('hsl(120,40%,40%)', 'satOff', -60000, 0.4, 0.4, 0.4, 120, 0, 0.4, 1, 0.4, '666666', 'hsl(120,0%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satOff', -40000, 0.4, 0.4, 0.4, 120, 0, 0.4, 1, 0.4, '666666', 'hsl(120,0%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satOff', -20000, 0.32, 0.48, 0.32, 120, 0.2, 0.4, 1, 0.434, '527a52', 'hsl(120,20%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satOff', 0, 0.24, 0.56, 0.24, 120, 0.4, 0.4, 1, 0.469, '3d8f3d', 'hsl(120,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satOff', 20000, 0.16, 0.64, 0.16, 120, 0.6, 0.4, 1, 0.503, '29a329', 'hsl(120,60%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satOff', 40000, 0.08, 0.72, 0.08, 120, 0.8, 0.4, 1, 0.538, '14b814', 'hsl(120,80%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satOff', 60000, 0, 0.8, 0, 120, 1, 0.4, 1, 0.572, '00cc00', 'hsl(120,100%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'satOff', 80000, 0, 0.8, 0, 120, 1, 0.4, 1, 0.572, '00cc00', 'hsl(120,100%,40%)');
            });
            it('should resolve color with "lum" transformation', function () {
                verifyTransformedColor('hsl(120,40%,40%)', 'lum', 0, 0, 0, 0, 120, 0.4, 0, 1, 0, '000000', 'hsl(120,40%,0%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lum', 20000, 0.12, 0.28, 0.12, 120, 0.4, 0.2, 1, 0.234, '1f471f', 'hsl(120,40%,20%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lum', 40000, 0.24, 0.56, 0.24, 120, 0.4, 0.4, 1, 0.469, '3d8f3d', 'hsl(120,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lum', 60000, 0.44, 0.76, 0.44, 120, 0.4, 0.6, 1, 0.669, '70c270', 'hsl(120,40%,60%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lum', 80000, 0.72, 0.88, 0.72, 120, 0.4, 0.8, 1, 0.834, 'b8e0b8', 'hsl(120,40%,80%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lum', 100000, 1, 1, 1, 120, 0.4, 1, 1, 1, 'ffffff', 'hsl(120,40%,100%)');
            });
            it('should resolve color with "lumMod" transformation', function () {
                verifyTransformedColor('hsl(120,40%,40%)', 'lumMod', 0, 0, 0, 0, 120, 0.4, 0, 1, 0, '000000', 'hsl(120,40%,0%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumMod', 50000, 0.12, 0.28, 0.12, 120, 0.4, 0.2, 1, 0.234, '1f471f', 'hsl(120,40%,20%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumMod', 100000, 0.24, 0.56, 0.24, 120, 0.4, 0.4, 1, 0.469, '3d8f3d', 'hsl(120,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumMod', 150000, 0.44, 0.76, 0.44, 120, 0.4, 0.6, 1, 0.669, '70c270', 'hsl(120,40%,60%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumMod', 200000, 0.72, 0.88, 0.72, 120, 0.4, 0.8, 1, 0.834, 'b8e0b8', 'hsl(120,40%,80%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumMod', 250000, 1, 1, 1, 120, 0.4, 1, 1, 1, 'ffffff', 'hsl(120,40%,100%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumMod', 300000, 1, 1, 1, 120, 0.4, 1, 1, 1, 'ffffff', 'hsl(120,40%,100%)');
            });
            it('should resolve color with "lumOff" transformation', function () {
                verifyTransformedColor('hsl(120,40%,40%)', 'lumOff', -60000, 0, 0, 0, 120, 0.4, 0, 1, 0, '000000', 'hsl(120,40%,0%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumOff', -40000, 0, 0, 0, 120, 0.4, 0, 1, 0, '000000', 'hsl(120,40%,0%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumOff', -20000, 0.12, 0.28, 0.12, 120, 0.4, 0.2, 1, 0.234, '1f471f', 'hsl(120,40%,20%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumOff', 0, 0.24, 0.56, 0.24, 120, 0.4, 0.4, 1, 0.469, '3d8f3d', 'hsl(120,40%,40%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumOff', 20000, 0.44, 0.76, 0.44, 120, 0.4, 0.6, 1, 0.669, '70c270', 'hsl(120,40%,60%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumOff', 40000, 0.72, 0.88, 0.72, 120, 0.4, 0.8, 1, 0.834, 'b8e0b8', 'hsl(120,40%,80%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumOff', 60000, 1, 1, 1, 120, 0.4, 1, 1, 1, 'ffffff', 'hsl(120,40%,100%)');
                verifyTransformedColor('hsl(120,40%,40%)', 'lumOff', 80000, 1, 1, 1, 120, 0.4, 1, 1, 1, 'ffffff', 'hsl(120,40%,100%)');
            });
            it('should resolve color with "shade" transformation', function () {
                verifyRgbTransformation('#336699', 'shade', 0, 0, 0, 0);
                verifyRgbTransformation('#336699', 'shade', 20000, 0.098, 0.197, 0.295);
                verifyRgbTransformation('#336699', 'shade', 40000, 0.134, 0.267, 0.401);
                verifyRgbTransformation('#336699', 'shade', 60000, 0.160, 0.319, 0.479);
                verifyRgbTransformation('#336699', 'shade', 80000, 0.181, 0.363, 0.544);
                verifyRgbTransformation('#336699', 'shade', 100000, 0.2, 0.4, 0.6);
            });
            it('should resolve color with "tint" transformation', function () {
                verifyRgbTransformation('#336699', 'tint', 0, 1, 1, 1);
                verifyRgbTransformation('#336699', 'tint', 20000, 0.909, 0.919, 0.937);
                verifyRgbTransformation('#336699', 'tint', 40000, 0.805, 0.827, 0.868);
                verifyRgbTransformation('#336699', 'tint', 60000, 0.679, 0.720, 0.792);
                verifyRgbTransformation('#336699', 'tint', 80000, 0.514, 0.588, 0.704);
                verifyRgbTransformation('#336699', 'tint', 100000, 0.2, 0.4, 0.6);
            });
            it('should resolve color with "gamma" transformation', function () {
                verifyTransformedColor('rgb(200,100,50)', 'gamma', null, 0.899, 0.662, 0.488, 25.455, 0.669, 0.693, 1, 0.7, 'e5a97c', '#e5a97c');
                verifyTransformedColor('hsl(135,40%,40%)', 'gamma', null, 0.533, 0.775, 0.605, 137.917, 0.349, 0.654, 1, 0.711, '88c69a', '#88c69a');
            });
            it('should resolve color with "invGamma" transformation', function () {
                verifyTransformedColor('rgb(200,100,50)', 'invGamma', null, 0.576, 0.119, 0.025, 10.304, 0.918, 0.3, 1, 0.21, '931e06', '#931e06');
                verifyTransformedColor('hsl(135,40%,40%)', 'invGamma', null, 0.039, 0.268, 0.075, 129.46, 0.745, 0.154, 1, 0.206, '0a4413', '#0a4413');
            });
            it('should resolve color with "comp" transformation', function () {
                verifyTransformedColor('rgb(200,100,50)', 'comp', null, 0.196, 0.588, 0.784, 200, 0.6, 0.49, 1, 0.519, '3296c8', 'hsl(200,60%,49%)');
                verifyTransformedColor('hsl(135,40%,40%)', 'comp', null, 0.56, 0.24, 0.48, 315, 0.4, 0.4, 1, 0.325, '8f3d7a', 'hsl(315,40%,40%)');
            });
            it('should resolve color with "inv" transformation', function () {
                verifyRgbTransformation('#336699', 'inv', null, 0.989, 0.943, 0.847);
                verifyRgbTransformation('#00f0ff', 'inv', null, 1, 0.405, 0);
            });
            it('should resolve color with "gray" transformation', function () {
                verifyTransformedColor('rgb(200,100,50)', 'gray', null, 0.461, 0.461, 0.461, 0, 0, 0.461, 1, 0.461, '767676', '#767676');
                verifyTransformedColor('hsl(135,40%,40%)', 'gray', null, 0.475, 0.475, 0.475, 0, 0, 0.475, 1, 0.475, '797979', '#797979');
            });
            it('should resolve multiple transformations in order', function () {
                expect(Color.fromRgb('00ff00').transform('red', 100000).transform('gray').resolve('').css).toBe("#ededed");
                expect(Color.fromRgb('00ff00').transform('gray').transform('red', 100000).resolve('').css).toBe("#ffb6b6");
            });
        });

        describe('method resolveAlpha', function () {
            it('should exist', function () {
                expect(Color).toHaveMethod('resolveAlpha');
            });
            it('should resolve auto color', function () {
                expect(Color.auto().resolveAlpha()).toBe(1);
            });
        });

        describe('method resolveText', function () {
            it('should exist', function () {
                expect(Color).toHaveMethod('resolveText');
            });
        });

        describe('method resolveMixed', function () {
            it('should exist', function () {
                expect(Color).toHaveMethod('resolveMixed');
            });
            it('should resolve a mixed color', function () {
                const color1 = Color.parseCSS('#135');
                const color2 = Color.parseCSS('#db9');
                verifyRGB(color1.resolveMixed(color2, -0.25, '', theme),  1 / 15,  3 / 15, 5 / 15, 1);
                verifyRGB(color1.resolveMixed(color2,  0.00, '', theme),  1 / 15,  3 / 15, 5 / 15, 1);
                verifyRGB(color1.resolveMixed(color2,  0.25, '', theme),  4 / 15,  5 / 15, 6 / 15, 1);
                verifyRGB(color1.resolveMixed(color2,  0.50, '', theme),  7 / 15,  7 / 15, 7 / 15, 1);
                verifyRGB(color1.resolveMixed(color2,  0.75, '', theme), 10 / 15,  9 / 15, 8 / 15, 1);
                verifyRGB(color1.resolveMixed(color2,  1.00, '', theme), 13 / 15, 11 / 15, 9 / 15, 1);
                verifyRGB(color1.resolveMixed(color2,  1.25, '', theme), 13 / 15, 11 / 15, 9 / 15, 1);
            });
        });

        describe('method toOpStr', function () {
            it('should exist', function () {
                expect(Color).toHaveMethod('toOpStr');
            });
        });

        describe('method toJSON', function () {
            it('should exist', function () {
                expect(Color).toHaveMethod('toJSON');
            });
            it('should return the JSON representation of auto color', function () {
                expect(Color.auto().toJSON()).toEqual(Color.AUTO);
            });
            it('should return the JSON representation of RGB color', function () {
                expect(Color.fromRgb('4080C0').toJSON()).toEqual(opRgbColor('4080C0'));
            });
            it('should return the JSON representation of HSL color', function () {
                expect(Color.fromHsl(0, 5000, 10000).toJSON()).toEqual(opHslColor(0, 5000, 10000));
            });
            it('should return the JSON representation of color with transformations', function () {
                expect(Color.fromRgb('4080C0', { tint: 25000 }).toJSON()).toEqual(opRgbColor('4080C0', { tint: 25000 }));
            });
            it('should return the JSON representation of color with fallback RGB', function () {
                expect(Color.parseJSON({ ...Color.ACCENT1, fallbackValue: '4080c0' }).toJSON()).toEqual({ ...Color.ACCENT1, fallbackValue: '4080C0' });
            });
        });
    });
});
