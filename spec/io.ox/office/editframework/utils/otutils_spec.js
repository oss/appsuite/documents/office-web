/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { makeOp, expectRemoved } from '~/othelper';
import { OTError, getOTKey, isOperationRemoved, setOperationRemoved, copyProperties, reduceProperties, transformAttribute,
    reduceAttributeSets, removeEmptyAttributes, reduceOperationAttributes, transformIndexInsert, transformIndexDelete, transformIndexMove,
    transformIndexSort, transformIndexInsertInsert, transformIndexInsertDelete, transformIndexInsertMove, transformIndexInsertCopy,
    transformIndexInsertSort, transformIndexDeleteDelete, transformIndexDeleteMove, transformIndexDeleteCopy, transformIndexDeleteSort,
    transformIndexMoveMove, transformIndexMoveSort, transformIndexMoveCopy, transformIndexCopyCopy, transformIndexCopySort,
    transformIndexSortSort, transformPositionInsert, transformPositionDelete, transformPositionMove } from '@/io.ox/office/editframework/utils/otutils';

// private functions ==========================================================

// shortcut to create a `OTMoveResult` structure
// if `fromIdx` is `undefined`, the result will be `undefined` too
function moveRes(fromIdx, toIdx) {
    return (fromIdx === undefined) ? undefined : { fromIdx, toIdx };
}

// shortcut to create a `OTShiftShiftResult` structure
function shiftShiftRes(lclIdx, extIdx) {
    return { lclIdx, extIdx };
}

// shortcut to create a `OTShiftMoveResult` structure
// if `fromIdx` is `undefined`, the property `moveRes` will be `undefined` too
function shiftMoveRes(shiftIdx, fromIdx, toIdx) {
    return { shiftIdx, moveRes: moveRes(fromIdx, toIdx) };
}

// shortcut to create a `OTShiftCopyResult` structure
function shiftCopyRes(shiftIdx, fromIdx, toIdx) {
    return { shiftIdx, moveRes: moveRes(fromIdx, toIdx), delToIdx: undefined };
}

// shortcut to create a `OTShiftCopyResult` structure with numeric `delToIdx` property
function shiftCopyDelRes(shiftIdx, delToIdx) {
    return { shiftIdx, moveRes: undefined, delToIdx };
}

// shortcut to create a `OTShiftSortResult` structure
// if `sortVec` is `undefined`, the property `sortVec` will be `undefined` too
function shiftSortRes(shiftIdx, sortVec) {
    return { shiftIdx, sortVec };
}

// shortcut to create a `OTMoveSortResult` structure with undefined `moveRes` property
// if `sortVec` is `undefined`, the property `sortVec` will be `undefined` too
function moveSortRes(sortVec) {
    return { moveRes: undefined, sortVec };
}

// shortcut to create a `OTMoveSortResult` structure for "copy" operation
// if `sortVec` is `undefined`, the property `sortVec` will be `undefined` too
function copySortRes(fromIdx, toIdx, sortVec) {
    return { moveRes: moveRes(fromIdx, toIdx), sortVec };
}

function decodeBase5(arrIdx) {
    return [Math.floor(arrIdx / 125) + 1, (Math.floor(arrIdx / 25) % 5) + 1, (Math.floor(arrIdx / 5) % 5) + 1, (arrIdx % 5) + 1];
}

function decodeCopy(text, offset) {
    return moveRes(parseInt(text[offset], 10), parseInt(text[offset + 1], 10));
}

function decodeMove(text, offset) {
    var result = decodeCopy(text, offset);
    return (result.fromIdx === result.toIdx) ? undefined : result;
}

function decodeMoveMove(text) {
    return { lclRes: decodeMove(text, 0), extRes: decodeMove(text, 2) };
}

function decodeMoveCopy(text) {
    return { moveRes: decodeMove(text, 0), copyRes: decodeCopy(text, 2) };
}

function decodeCopyCopy(text) {
    return { lclRes: decodeCopy(text, 0), extRes: decodeCopy(text, 2) };
}

// tests ======================================================================

describe('module editframework/utils/otutils', function () {

    // class OTError ----------------------------------------------------------

    describe('class OTError', function () {
        it('should subclass Error', function () {
            expect(OTError).toBeSubClassOf(Error);
        });
    });

    // functions --------------------------------------------------------------

    describe('function getOTKey', function () {
        it('should exist', function () {
            expect(getOTKey).toBeFunction();
        });
        it('should return same key for same operations', function () {
            var key1 = getOTKey('op1', 'op2');
            var key2 = getOTKey('op1', 'op2');
            expect(key1).toBe(key2);
        });
        it('should return different keys for different operations', function () {
            var key = getOTKey('op1', 'op2');
            expect(key).not.toBe(getOTKey('op1', 'op1'));
            expect(key).not.toBe(getOTKey('op2', 'op1'));
            expect(key).not.toBe(getOTKey('op2', 'op2'));
        });
    });

    describe('function isOperationRemoved', function () {
        it('should exist', function () {
            expect(isOperationRemoved).toBeFunction();
        });
    });

    describe('function setOperationRemoved', function () {
        it('should exist', function () {
            expect(setOperationRemoved).toBeFunction();
        });
        it('should mark operations in-place', function () {
            var op1 = makeOp('op1');
            var op2 = makeOp('op2');
            expect(isOperationRemoved(op1)).toBeFalse();
            expect(isOperationRemoved(op2)).toBeFalse();
            expect(setOperationRemoved(op1, op2)).toBeUndefined();
            expect(isOperationRemoved(op1)).toBeTrue();
            expect(isOperationRemoved(op2)).toBeTrue();
        });
    });

    describe('function copyProperties', function () {
        it('should exist', function () {
            expect(copyProperties).toBeFunction();
        });
        it('should copy existing properties', function () {
            var tgtDict = { a: 1, b: 2, c: 3 };
            var srcDict = { b: 4, c: 5, d: 6 };
            copyProperties(tgtDict, srcDict, 'b', 'd');
            expect(tgtDict).toEqual({ a: 1, b: 4, c: 3, d: 6 });
            expect(srcDict).toEqual({ b: 4, c: 5, d: 6 });
        });
        it('should delete non-existing properties', function () {
            var tgtDict = { a: 1, b: 2, c: 3 };
            var srcDict = { b: 4, c: 5, d: 6 };
            copyProperties(tgtDict, srcDict, 'a', 'e');
            expect(tgtDict).toEqual({ b: 2, c: 3 });
            expect(srcDict).toEqual({ b: 4, c: 5, d: 6 });
        });
    });

    describe('function reduceProperties', function () {
        it('should exist', function () {
            expect(reduceProperties).toBeFunction();
        });
        it('should return false if nothing will be reduced (keep-equal mode)', function () {
            var lclDict = { p1: 10, p2: 20, p3: 30 };
            var extDict = { p1: 11, p2: 20, p4: 40 };
            expect(reduceProperties(lclDict, extDict, 'p0')).toBeFalse();
            expect(reduceProperties(lclDict, extDict, 'p3')).toBeFalse();
            expect(reduceProperties(lclDict, extDict, 'p4')).toBeFalse();
            expect(lclDict).toEqual({ p1: 10, p2: 20, p3: 30 });
            expect(extDict).toEqual({ p1: 11, p2: 20, p4: 40 });
        });
        it('should return false if nothing will be reduced (delete-equal mode)', function () {
            var lclDict = { p1: 10, p2: 20, p3: 30 };
            var extDict = { p1: 11, p2: 20, p4: 40 };
            expect(reduceProperties(lclDict, extDict, 'p0', { deleteEqual: true })).toBeFalse();
            expect(reduceProperties(lclDict, extDict, 'p3', { deleteEqual: true })).toBeFalse();
            expect(reduceProperties(lclDict, extDict, 'p4', { deleteEqual: true })).toBeFalse();
            expect(lclDict).toEqual({ p1: 10, p2: 20, p3: 30 });
            expect(extDict).toEqual({ p1: 11, p2: 20, p4: 40 });
        });
        it('should reduce redundant property (keep-equal mode)', function () {
            var lclDict = { p1: 10, p2: 20, p3: 30 };
            var extDict = { p1: 11, p2: 20, p4: 40 };
            expect(reduceProperties(lclDict, extDict, 'p1')).toBeTrue();
            expect(lclDict).toEqual({ p1: 10, p2: 20, p3: 30 });
            expect(extDict).toEqual({ p2: 20, p4: 40 });
            expect(reduceProperties(lclDict, extDict, 'p2')).toBeFalse();
            expect(lclDict).toEqual({ p1: 10, p2: 20, p3: 30 });
            expect(extDict).toEqual({ p2: 20, p4: 40 });
        });
        it('should reduce redundant property (delete-equal mode)', function () {
            var lclDict = { p1: 10, p2: 20, p3: 30 };
            var extDict = { p1: 11, p2: 20, p4: 40 };
            expect(reduceProperties(lclDict, extDict, 'p1', { deleteEqual: true })).toBeTrue();
            expect(lclDict).toEqual({ p1: 10, p2: 20, p3: 30 });
            expect(extDict).toEqual({ p2: 20, p4: 40 });
            expect(reduceProperties(lclDict, extDict, 'p2', { deleteEqual: true })).toBeTrue();
            expect(lclDict).toEqual({ p1: 10, p3: 30 });
            expect(extDict).toEqual({ p4: 40 });
        });
        it('should compare values deeply', function () {
            var lclDict = { p1: { a: 10 }, p2: { b: 20 }, p3: 30 };
            var extDict = { p1: { a: 11 }, p2: { b: 20 }, p4: 40 };
            expect(reduceProperties(lclDict, extDict, 'p1', { deleteEqual: true })).toBeTrue();
            expect(lclDict).toEqual({ p1: { a: 10 }, p2: { b: 20 }, p3: 30 });
            expect(extDict).toEqual({ p2: { b: 20 }, p4: 40 });
            expect(reduceProperties(lclDict, extDict, 'p2', { deleteEqual: true })).toBeTrue();
            expect(lclDict).toEqual({ p1: { a: 10 }, p3: 30 });
            expect(extDict).toEqual({ p4: 40 });
        });
        it('should reduce multiple properties', function () {
            var lclDict = { p1: 10, p2: 20, p3: 30 };
            var extDict = { p1: 11, p2: 20, p4: 40 };
            expect(reduceProperties(lclDict, extDict, ['p1', 'p2', 'p3'], { deleteEqual: true })).toBeTrue();
            expect(lclDict).toEqual({ p1: 10, p3: 30 });
            expect(extDict).toEqual({ p4: 40 });
        });
    });

    describe('function transformAttribute', function () {
        it('should exist', function () {
            expect(transformAttribute).toBeFunction();
        });
        it('should transform an existing attribute', function () {
            var attrOp = { name: 'op1', attrs: { f1: { a1: 10 } } };
            transformAttribute(attrOp, 'f1', 'a1', function () { return 42; });
            expect(attrOp.attrs).toEqual({ f1: { a1: 42 } });
            transformAttribute(attrOp, 'f1', 'a1', function () { return undefined; });
            expect(attrOp.attrs).toEqual({ f1: { a1: 42 } });
        });
        it('should skip missing or invalid attributes', function () {
            var attrOp = { name: 'op1', attrs: { f1: { a1: 10 }, f2: 10 } };
            transformAttribute(attrOp, 'f1', 'a2', function () { return 42; });
            expect(attrOp.attrs).toEqual({ f1: { a1: 10 }, f2: 10 });
            transformAttribute(attrOp, 'f2', 'a1', function () { return 42; });
            expect(attrOp.attrs).toEqual({ f1: { a1: 10 }, f2: 10 });
            transformAttribute(attrOp, 'f3', 'a1', function () { return 42; });
            var otherOp = { name: 'op2' };
            transformAttribute(otherOp, 'f1', 'a1', function () { return 42; });
            expect(otherOp).toEqual({ name: 'op2' });
        });
    });

    describe('function reduceAttributeSets', function () {
        it('should exist', function () {
            expect(reduceAttributeSets).toBeFunction();
        });
        it('should return false if nothing will be reduced (keep-equal mode)', function () {
            var lclAttrSet = { f1: { a1: 10 }, f2: { a1: 10 } };
            var extAttrSet = { f3: { a1: 10 }, f2: { a2: 20 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet)).toBeFalse();
            expect(lclAttrSet).toEqual({ f1: { a1: 10 }, f2: { a1: 10 } });
            expect(extAttrSet).toEqual({ f3: { a1: 10 }, f2: { a2: 20 } });
        });
        it('should return false if nothing will be reduced (delete-equal mode)', function () {
            var lclAttrSet = { f1: { a1: 10 }, f2: { a1: 10 } };
            var extAttrSet = { f3: { a1: 10 }, f2: { a2: 20 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet, { deleteEqual: true })).toBeFalse();
            expect(lclAttrSet).toEqual({ f1: { a1: 10 }, f2: { a1: 10 } });
            expect(extAttrSet).toEqual({ f3: { a1: 10 }, f2: { a2: 20 } });
        });
        it('should reduce all redundant attributes (keep-equal mode)', function () {
            var lclAttrSet = { f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } };
            var extAttrSet = { f3: { a1: 10 }, f2: { a1: 10, a2: 22, a4: 30 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet)).toBeTrue();
            expect(lclAttrSet).toEqual({ f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } });
            expect(extAttrSet).toEqual({ f3: { a1: 10 }, f2: { a1: 10, a4: 30 } });
        });
        it('should reduce all redundant attributes (delete-equal mode)', function () {
            var lclAttrSet = { f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } };
            var extAttrSet = { f3: { a1: 10 }, f2: { a1: 10, a2: 22, a4: 30 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet, { deleteEqual: true })).toBeTrue();
            expect(lclAttrSet).toEqual({ f1: { a1: 10 }, f2: { a2: 20, a3: 30 } });
            expect(extAttrSet).toEqual({ f3: { a1: 10 }, f2: { a4: 30 } });
        });
        it('should delete empty attribute families (keep-equal mode)', function () {
            var lclAttrSet = { f1: { a1: 10, a2: 20 }, f2: { a1: 10 } };
            var extAttrSet = { f1: { a1: 11 }, f2: { a1: 11, a2: 20 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet)).toBeTrue();
            expect(lclAttrSet).toEqual({ f1: { a1: 10, a2: 20 }, f2: { a1: 10 } });
            expect(extAttrSet).toEqual({ f2: { a2: 20 } });
        });
        it('should delete empty attribute families (delete-equal mode)', function () {
            var lclAttrSet = { f1: { a1: 10, a2: 20 }, f2: { a1: 10 } };
            var extAttrSet = { f1: { a1: 10 }, f2: { a1: 10, a2: 20 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet, { deleteEqual: true })).toBeTrue();
            expect(lclAttrSet).toEqual({ f1: { a2: 20 } });
            expect(extAttrSet).toEqual({ f2: { a2: 20 } });
        });
        it('should handle equal style sheet identifier (keep-equal mode)', function () {
            var lclAttrSet = { style: 's1', f1: { a1: 10 } };
            var extAttrSet = { style: 's1', f2: { a1: 10 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet)).toBeFalse();
            expect(lclAttrSet).toEqual({ style: 's1', f1: { a1: 10 } });
            expect(extAttrSet).toEqual({ style: 's1', f2: { a1: 10 } });
        });
        it('should handle equal style sheet identifier (delete-equal mode)', function () {
            var lclAttrSet = { style: 's1', f1: { a1: 10 } };
            var extAttrSet = { style: 's1', f2: { a1: 10 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet, { deleteEqual: true })).toBeTrue();
            expect(lclAttrSet).toEqual({ f1: { a1: 10 } });
            expect(extAttrSet).toEqual({ f2: { a1: 10 } });
        });
        it('should handle different style sheet identifier (keep-equal mode)', function () {
            var lclAttrSet = { style: 's1', f1: { a1: 10 } };
            var extAttrSet = { style: 's2', f2: { a1: 10 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet)).toBeTrue();
            expect(lclAttrSet).toEqual({ style: 's1', f1: { a1: 10 } });
            expect(extAttrSet).toEqual({ f2: { a1: 10 } });
        });
        it('should handle different style sheet identifier (delete-equal mode)', function () {
            var lclAttrSet = { style: 's1', f1: { a1: 10 } };
            var extAttrSet = { style: 's2', f2: { a1: 10 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet, { deleteEqual: true })).toBeTrue();
            expect(lclAttrSet).toEqual({ style: 's1', f1: { a1: 10 } });
            expect(extAttrSet).toEqual({ f2: { a1: 10 } });
        });
        it('should keep local style sheet identifier', function () {
            var lclAttrSet = { style: 's1', f1: { a1: 10 } };
            var extAttrSet = { f2: { a1: 10 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet)).toBeFalse();
            expect(lclAttrSet).toEqual({ style: 's1', f1: { a1: 10 } });
            expect(extAttrSet).toEqual({ f2: { a1: 10 } });
        });
        it('should keep external style sheet identifier', function () {
            var lclAttrSet = { f1: { a1: 10 } };
            var extAttrSet = { style: 's1', f2: { a1: 10 } };
            expect(reduceAttributeSets(lclAttrSet, extAttrSet)).toBeFalse();
            expect(lclAttrSet).toEqual({ f1: { a1: 10 } });
            expect(extAttrSet).toEqual({ style: 's1', f2: { a1: 10 } });
        });
    });

    describe('function removeEmptyAttributes', function () {
        it('should exist', function () {
            expect(removeEmptyAttributes).toBeFunction();
        });
        it('should delete the empty attribute set', function () {
            var op1 = { name: 'test', attrs: { f1: { a1: 10 } } };
            removeEmptyAttributes(op1);
            expect(op1).toEqual({ name: 'test', attrs: { f1: { a1: 10 } } });
            var op2 = { name: 'test', attrs: {} };
            removeEmptyAttributes(op2);
            expect(op2).toEqual({ name: 'test' });
            var op3 = { name: 'test' };
            removeEmptyAttributes(op3);
            expect(op3).toEqual({ name: 'test' });
        });
        it('should remove the empty operation', function () {
            var op1 = { name: 'test', attrs: { f1: { a1: 10 } } };
            removeEmptyAttributes(op1, { removeEmptyOp: true });
            expect(op1).toEqual({ name: 'test', attrs: { f1: { a1: 10 } } });
            var op2 = { name: 'test', attrs: {} };
            removeEmptyAttributes(op2, { removeEmptyOp: true });
            expectRemoved(op2);
            var op3 = { name: 'test' };
            removeEmptyAttributes(op3, { removeEmptyOp: true });
            expectRemoved(op3);
        });
    });

    describe('function reduceOperationAttributes', function () {
        it('should exist', function () {
            expect(reduceOperationAttributes).toBeFunction();
        });
        it('should return false if nothing will be reduced (keep-equal mode)', function () {
            var lclOp = { name: 'test', attrs: { f1: { a1: 10 }, f2: { a1: 10 } } };
            var extOp = { name: 'test', attrs: { f3: { a1: 10 }, f2: { a2: 20 } } };
            expect(reduceOperationAttributes(lclOp, extOp)).toBeFalse();
            expect(lclOp.attrs).toEqual({ f1: { a1: 10 }, f2: { a1: 10 } });
            expect(extOp.attrs).toEqual({ f3: { a1: 10 }, f2: { a2: 20 } });
        });
        it('should return false if nothing will be reduced (delete-equal mode)', function () {
            var lclOp = { name: 'test', attrs: { f1: { a1: 10 }, f2: { a1: 10 } } };
            var extOp = { name: 'test', attrs: { f3: { a1: 10 }, f2: { a2: 20 } } };
            expect(reduceOperationAttributes(lclOp, extOp, { deleteEqual: true })).toBeFalse();
            expect(lclOp.attrs).toEqual({ f1: { a1: 10 }, f2: { a1: 10 } });
            expect(extOp.attrs).toEqual({ f3: { a1: 10 }, f2: { a2: 20 } });
        });
        it('should reduce all redundant attributes (keep-equal mode)', function () {
            var lclOp = { name: 'test', attrs: { f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } } };
            var extOp = { name: 'test', attrs: { f3: { a1: 10 }, f2: { a1: 10, a2: 22, a4: 30 } } };
            expect(reduceOperationAttributes(lclOp, extOp)).toBeTrue();
            expect(lclOp.attrs).toEqual({ f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } });
            expect(extOp.attrs).toEqual({ f3: { a1: 10 }, f2: { a1: 10, a4: 30 } });
        });
        it('should reduce all redundant attributes (delete-equal mode)', function () {
            var lclOp = { name: 'test', attrs: { f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } } };
            var extOp = { name: 'test', attrs: { f3: { a1: 10 }, f2: { a1: 10, a2: 22, a4: 30 } } };
            expect(reduceOperationAttributes(lclOp, extOp, { deleteEqual: true })).toBeTrue();
            expect(lclOp.attrs).toEqual({ f1: { a1: 10 }, f2: { a2: 20, a3: 30 } });
            expect(extOp.attrs).toEqual({ f3: { a1: 10 }, f2: { a4: 30 } });
        });
        it('should delete the empty attribute set (keep-equal mode)', function () {
            var lclOp = { name: 'test', attrs: { f1: { a1: 10 } } };
            var extOp = { name: 'test', attrs: { f1: { a1: 11 } } };
            expect(reduceOperationAttributes(lclOp, extOp)).toBeTrue();
            expect(lclOp).toEqual({ name: 'test', attrs: { f1: { a1: 10 } } });
            expect(extOp).toEqual({ name: 'test' });
        });
        it('should delete the empty attribute set (delete-equal mode)', function () {
            var lclOp = { name: 'test', attrs: { f1: { a1: 10 } } };
            var extOp = { name: 'test', attrs: { f1: { a1: 10 } } };
            expect(reduceOperationAttributes(lclOp, extOp, { deleteEqual: true })).toBeTrue();
            expect(lclOp).toEqual({ name: 'test' });
            expect(extOp).toEqual({ name: 'test' });
        });
        it('should remove the empty operation (keep-equal mode)', function () {
            var lclOp = { name: 'test', attrs: { f1: { a1: 10 } } };
            var extOp = { name: 'test', attrs: { f1: { a1: 11 } } };
            expect(reduceOperationAttributes(lclOp, extOp, { removeEmptyOp: true })).toBeTrue();
            expect(lclOp).toEqual({ name: 'test', attrs: { f1: { a1: 10 } } });
            expectRemoved(extOp);
        });
        it('should remove the empty operations (delete-equal mode)', function () {
            var lclOp = { name: 'test', attrs: { f1: { a1: 10 } } };
            var extOp = { name: 'test', attrs: { f1: { a1: 10 } } };
            expect(reduceOperationAttributes(lclOp, extOp, { deleteEqual: true, removeEmptyOp: true })).toBeTrue();
            expectRemoved(lclOp);
            expectRemoved(extOp);
        });
    });

    describe('function transformIndexInsert', function () {
        it('should exist', function () {
            expect(transformIndexInsert).toBeFunction();
        });
        it('should transform the index', function () {
            expect(transformIndexInsert(0, 2, 1)).toBe(0);
            expect(transformIndexInsert(1, 2, 1)).toBe(1);
            expect(transformIndexInsert(2, 2, 1)).toBe(3);
            expect(transformIndexInsert(3, 2, 1)).toBe(4);
            expect(transformIndexInsert(4, 2, 1)).toBe(5);
        });
        it('should transform the index for multiple inserted elements', function () {
            expect(transformIndexInsert(0, 2, 3)).toBe(0);
            expect(transformIndexInsert(1, 2, 3)).toBe(1);
            expect(transformIndexInsert(2, 2, 3)).toBe(5);
            expect(transformIndexInsert(3, 2, 3)).toBe(6);
            expect(transformIndexInsert(4, 2, 3)).toBe(7);
        });
    });

    describe('function transformIndexDelete', function () {
        it('should exist', function () {
            expect(transformIndexDelete).toBeFunction();
        });
        it('should transform the index', function () {
            expect(transformIndexDelete(0, 2, 1)).toBe(0);
            expect(transformIndexDelete(1, 2, 1)).toBe(1);
            expect(transformIndexDelete(2, 2, 1)).toBeUndefined();
            expect(transformIndexDelete(3, 2, 1)).toBe(2);
            expect(transformIndexDelete(4, 2, 1)).toBe(3);
        });
        it('should transform the index for multiple deleted elements', function () {
            expect(transformIndexDelete(0, 2, 3)).toBe(0);
            expect(transformIndexDelete(1, 2, 3)).toBe(1);
            expect(transformIndexDelete(2, 2, 3)).toBeUndefined();
            expect(transformIndexDelete(3, 2, 3)).toBeUndefined();
            expect(transformIndexDelete(4, 2, 3)).toBeUndefined();
            expect(transformIndexDelete(5, 2, 3)).toBe(2);
            expect(transformIndexDelete(6, 2, 3)).toBe(3);
        });
        it('should keep the index valid with "keepDel" flag', function () {
            expect(transformIndexDelete(2, 2, 1, true)).toBe(2);
            expect(transformIndexDelete(2, 2, 3, true)).toBe(2);
            expect(transformIndexDelete(3, 2, 3, true)).toBe(2);
            expect(transformIndexDelete(4, 2, 3, true)).toBe(2);
        });
    });

    describe('function transformIndexMove', function () {
        it('should exist', function () {
            expect(transformIndexMove).toBeFunction();
        });
        it('should transform the index when moving forwards', function () {
            expect(transformIndexMove(0, 2, 1, 4)).toBe(0);
            expect(transformIndexMove(1, 2, 1, 4)).toBe(1);
            expect(transformIndexMove(2, 2, 1, 4)).toBe(4);
            expect(transformIndexMove(3, 2, 1, 4)).toBe(2);
            expect(transformIndexMove(4, 2, 1, 4)).toBe(3);
            expect(transformIndexMove(5, 2, 1, 4)).toBe(5);
            expect(transformIndexMove(6, 2, 1, 4)).toBe(6);
        });
        it('should transform the index when moving multiple elements forwards', function () {
            expect(transformIndexMove(0, 2, 3, 4)).toBe(0);
            expect(transformIndexMove(1, 2, 3, 4)).toBe(1);
            expect(transformIndexMove(2, 2, 3, 4)).toBe(4);
            expect(transformIndexMove(3, 2, 3, 4)).toBe(5);
            expect(transformIndexMove(4, 2, 3, 4)).toBe(6);
            expect(transformIndexMove(5, 2, 3, 4)).toBe(2);
            expect(transformIndexMove(6, 2, 3, 4)).toBe(3);
            expect(transformIndexMove(7, 2, 3, 4)).toBe(7);
            expect(transformIndexMove(8, 2, 3, 4)).toBe(8);
        });
        it('should transform the index when moving backwards', function () {
            expect(transformIndexMove(0, 4, 1, 2)).toBe(0);
            expect(transformIndexMove(1, 4, 1, 2)).toBe(1);
            expect(transformIndexMove(2, 4, 1, 2)).toBe(3);
            expect(transformIndexMove(3, 4, 1, 2)).toBe(4);
            expect(transformIndexMove(4, 4, 1, 2)).toBe(2);
            expect(transformIndexMove(5, 4, 1, 2)).toBe(5);
            expect(transformIndexMove(6, 4, 1, 2)).toBe(6);
        });
        it('should transform the index when moving multiple elements backwards', function () {
            expect(transformIndexMove(0, 4, 3, 2)).toBe(0);
            expect(transformIndexMove(1, 4, 3, 2)).toBe(1);
            expect(transformIndexMove(2, 4, 3, 2)).toBe(5);
            expect(transformIndexMove(3, 4, 3, 2)).toBe(6);
            expect(transformIndexMove(4, 4, 3, 2)).toBe(2);
            expect(transformIndexMove(5, 4, 3, 2)).toBe(3);
            expect(transformIndexMove(6, 4, 3, 2)).toBe(4);
            expect(transformIndexMove(7, 4, 3, 2)).toBe(7);
            expect(transformIndexMove(8, 4, 3, 2)).toBe(8);
        });
    });

    describe('function transformIndexSort', function () {
        it('should exist', function () {
            expect(transformIndexSort).toBeFunction();
        });
        it('should transform the index', function () {
            var VECTOR = [0, 4, 1, 3, 5, 2, 6];
            expect(transformIndexSort(0, VECTOR)).toBe(0);
            expect(transformIndexSort(1, VECTOR)).toBe(2);
            expect(transformIndexSort(2, VECTOR)).toBe(5);
            expect(transformIndexSort(3, VECTOR)).toBe(3);
            expect(transformIndexSort(4, VECTOR)).toBe(1);
            expect(transformIndexSort(5, VECTOR)).toBe(4);
            expect(transformIndexSort(6, VECTOR)).toBe(6);
            expect(transformIndexSort(7, VECTOR)).toBe(7);
        });
    });

    describe('function transformIndexInsertInsert', function () {
        it('should exist', function () {
            expect(transformIndexInsertInsert).toBeFunction();
        });
        it('should transform the indexes', function () {
            expect(transformIndexInsertInsert(1, 3)).toEqual(shiftShiftRes(1, 4));
            expect(transformIndexInsertInsert(2, 3)).toEqual(shiftShiftRes(2, 4));
            expect(transformIndexInsertInsert(3, 3)).toEqual(shiftShiftRes(4, 3));
            expect(transformIndexInsertInsert(4, 3)).toEqual(shiftShiftRes(5, 3));
            expect(transformIndexInsertInsert(5, 3)).toEqual(shiftShiftRes(6, 3));
        });
    });

    describe('function transformIndexInsertDelete', function () {
        it('should exist', function () {
            expect(transformIndexInsertDelete).toBeFunction();
        });
        it('should transform the indexes', function () {
            expect(transformIndexInsertDelete(1, 3)).toEqual(shiftShiftRes(1, 4));
            expect(transformIndexInsertDelete(2, 3)).toEqual(shiftShiftRes(2, 4));
            expect(transformIndexInsertDelete(3, 3)).toEqual(shiftShiftRes(3, 4));
            expect(transformIndexInsertDelete(4, 3)).toEqual(shiftShiftRes(3, 3));
            expect(transformIndexInsertDelete(5, 3)).toEqual(shiftShiftRes(4, 3));
        });
    });

    describe('function transformIndexInsertMove', function () {
        it('should exist', function () {
            expect(transformIndexInsertMove).toBeFunction();
        });
        it('should handle index moved to end', function () {
            expect(transformIndexInsertMove(1, 2, 4)).toEqual(shiftMoveRes(1, 3, 5));
            expect(transformIndexInsertMove(2, 2, 4)).toEqual(shiftMoveRes(2, 3, 5));
            expect(transformIndexInsertMove(3, 2, 4)).toEqual(shiftMoveRes(2, 2, 5));
            expect(transformIndexInsertMove(4, 2, 4)).toEqual(shiftMoveRes(3, 2, 5));
            expect(transformIndexInsertMove(5, 2, 4)).toEqual(shiftMoveRes(5, 2, 4));
            expect(transformIndexInsertMove(6, 2, 4)).toEqual(shiftMoveRes(6, 2, 4));
            expect(transformIndexInsertMove(1, 2, 3)).toEqual(shiftMoveRes(1, 3, 4));
            expect(transformIndexInsertMove(2, 2, 3)).toEqual(shiftMoveRes(2, 3, 4));
            expect(transformIndexInsertMove(3, 2, 3)).toEqual(shiftMoveRes(2, 2, 4));
            expect(transformIndexInsertMove(4, 2, 3)).toEqual(shiftMoveRes(4, 2, 3));
            expect(transformIndexInsertMove(5, 2, 3)).toEqual(shiftMoveRes(5, 2, 3));
        });
        it('should handle index moved to front', function () {
            expect(transformIndexInsertMove(1, 4, 2)).toEqual(shiftMoveRes(1, 5, 3));
            expect(transformIndexInsertMove(2, 4, 2)).toEqual(shiftMoveRes(2, 5, 3));
            expect(transformIndexInsertMove(3, 4, 2)).toEqual(shiftMoveRes(4, 5, 2));
            expect(transformIndexInsertMove(4, 4, 2)).toEqual(shiftMoveRes(5, 5, 2));
            expect(transformIndexInsertMove(5, 4, 2)).toEqual(shiftMoveRes(5, 4, 2));
            expect(transformIndexInsertMove(6, 4, 2)).toEqual(shiftMoveRes(6, 4, 2));
            expect(transformIndexInsertMove(1, 3, 2)).toEqual(shiftMoveRes(1, 4, 3));
            expect(transformIndexInsertMove(2, 3, 2)).toEqual(shiftMoveRes(2, 4, 3));
            expect(transformIndexInsertMove(3, 3, 2)).toEqual(shiftMoveRes(4, 4, 2));
            expect(transformIndexInsertMove(4, 3, 2)).toEqual(shiftMoveRes(4, 3, 2));
            expect(transformIndexInsertMove(5, 3, 2)).toEqual(shiftMoveRes(5, 3, 2));
        });
        it('should handle no-op move', function () {
            expect(transformIndexInsertMove(1, 3, 3)).toEqual(shiftMoveRes(1));
            expect(transformIndexInsertMove(2, 3, 3)).toEqual(shiftMoveRes(2));
            expect(transformIndexInsertMove(3, 3, 3)).toEqual(shiftMoveRes(3));
            expect(transformIndexInsertMove(4, 3, 3)).toEqual(shiftMoveRes(4));
            expect(transformIndexInsertMove(5, 3, 3)).toEqual(shiftMoveRes(5));
        });
    });

    describe('function transformIndexInsertCopy', function () {
        it('should exist', function () {
            expect(transformIndexInsertCopy).toBeFunction();
        });
        it('should handle index copied to end', function () {
            expect(transformIndexInsertCopy(2, 2, 4)).toEqual(shiftMoveRes(2, 3, 5));
            expect(transformIndexInsertCopy(1, 2, 4)).toEqual(shiftMoveRes(1, 3, 5));
            expect(transformIndexInsertCopy(3, 2, 4)).toEqual(shiftMoveRes(3, 2, 5));
            expect(transformIndexInsertCopy(4, 2, 4)).toEqual(shiftMoveRes(4, 2, 5));
            expect(transformIndexInsertCopy(5, 2, 4)).toEqual(shiftMoveRes(6, 2, 4));
        });
        it('should handle index copied to front', function () {
            expect(transformIndexInsertCopy(1, 4, 2)).toEqual(shiftMoveRes(1, 5, 3));
            expect(transformIndexInsertCopy(2, 4, 2)).toEqual(shiftMoveRes(2, 5, 3));
            expect(transformIndexInsertCopy(3, 4, 2)).toEqual(shiftMoveRes(4, 5, 2));
            expect(transformIndexInsertCopy(4, 4, 2)).toEqual(shiftMoveRes(5, 5, 2));
            expect(transformIndexInsertCopy(5, 4, 2)).toEqual(shiftMoveRes(6, 4, 2));
        });
        it('should handle index copied to itself', function () {
            expect(transformIndexInsertCopy(1, 3, 3)).toEqual(shiftMoveRes(1, 4, 4));
            expect(transformIndexInsertCopy(2, 3, 3)).toEqual(shiftMoveRes(2, 4, 4));
            expect(transformIndexInsertCopy(3, 3, 3)).toEqual(shiftMoveRes(3, 4, 4));
            expect(transformIndexInsertCopy(4, 3, 3)).toEqual(shiftMoveRes(5, 3, 3));
            expect(transformIndexInsertCopy(5, 3, 3)).toEqual(shiftMoveRes(6, 3, 3));
        });
    });

    describe('function transformIndexInsertSort', function () {
        it('should exist', function () {
            expect(transformIndexInsertSort).toBeFunction();
        });
        it('should transform the sort vector', function () {
            var VECTOR = [0, 6, 3, 4, 2, 1, 5];
            expect(transformIndexInsertSort(0, VECTOR)).toEqual(shiftSortRes(0, [0, 1, 7, 4, 5, 3, 2, 6]));
            expect(transformIndexInsertSort(1, VECTOR)).toEqual(shiftSortRes(1, [0, 1, 7, 4, 5, 3, 2, 6]));
            expect(transformIndexInsertSort(2, VECTOR)).toEqual(shiftSortRes(2, [0, 7, 2, 4, 5, 3, 1, 6]));
            expect(transformIndexInsertSort(3, VECTOR)).toEqual(shiftSortRes(3, [0, 7, 4, 3, 5, 2, 1, 6]));
            expect(transformIndexInsertSort(4, VECTOR)).toEqual(shiftSortRes(4, [0, 7, 3, 5, 4, 2, 1, 6]));
            expect(transformIndexInsertSort(5, VECTOR)).toEqual(shiftSortRes(5, [0, 7, 3, 4, 2, 5, 1, 6]));
            expect(transformIndexInsertSort(6, VECTOR)).toEqual(shiftSortRes(6, [0, 7, 3, 4, 2, 1, 6, 5]));
            expect(transformIndexInsertSort(7, VECTOR)).toEqual(shiftSortRes(7, [0, 6, 3, 4, 2, 1, 5, 7]));
        });
        it('should detect no-op sort vector', function () {
            expect(transformIndexInsertSort(1, [0, 1, 2, 3, 4])).toEqual(shiftSortRes(1));
        });
    });

    describe('function transformIndexDeleteDelete', function () {
        it('should exist', function () {
            expect(transformIndexDeleteDelete).toBeFunction();
        });
        it('should transform the indexes', function () {
            expect(transformIndexDeleteDelete(1, 3)).toEqual(shiftShiftRes(1, 2));
            expect(transformIndexDeleteDelete(2, 3)).toEqual(shiftShiftRes(2, 2));
            expect(transformIndexDeleteDelete(3, 3)).toBeUndefined();
            expect(transformIndexDeleteDelete(4, 3)).toEqual(shiftShiftRes(3, 3));
            expect(transformIndexDeleteDelete(5, 3)).toEqual(shiftShiftRes(4, 3));
        });
    });

    describe('function transformIndexDeleteMove', function () {
        it('should exist', function () {
            expect(transformIndexDeleteMove).toBeFunction();
        });
        it('should handle index moved to end', function () {
            expect(transformIndexDeleteMove(1, 2, 4)).toEqual(shiftMoveRes(1, 1, 3));
            expect(transformIndexDeleteMove(2, 2, 4)).toEqual(shiftMoveRes(4));
            expect(transformIndexDeleteMove(3, 2, 4)).toEqual(shiftMoveRes(2, 2, 3));
            expect(transformIndexDeleteMove(4, 2, 4)).toEqual(shiftMoveRes(3, 2, 3));
            expect(transformIndexDeleteMove(5, 2, 4)).toEqual(shiftMoveRes(5, 2, 4));
            expect(transformIndexDeleteMove(1, 2, 3)).toEqual(shiftMoveRes(1, 1, 2));
            expect(transformIndexDeleteMove(2, 2, 3)).toEqual(shiftMoveRes(3));
            expect(transformIndexDeleteMove(3, 2, 3)).toEqual(shiftMoveRes(2));
            expect(transformIndexDeleteMove(4, 2, 3)).toEqual(shiftMoveRes(4, 2, 3));
        });
        it('should handle index moved to front', function () {
            expect(transformIndexDeleteMove(1, 4, 2)).toEqual(shiftMoveRes(1, 3, 1));
            expect(transformIndexDeleteMove(2, 4, 2)).toEqual(shiftMoveRes(3, 3, 2));
            expect(transformIndexDeleteMove(3, 4, 2)).toEqual(shiftMoveRes(4, 3, 2));
            expect(transformIndexDeleteMove(4, 4, 2)).toEqual(shiftMoveRes(2));
            expect(transformIndexDeleteMove(5, 4, 2)).toEqual(shiftMoveRes(5, 4, 2));
            expect(transformIndexDeleteMove(1, 3, 2)).toEqual(shiftMoveRes(1, 2, 1));
            expect(transformIndexDeleteMove(2, 3, 2)).toEqual(shiftMoveRes(3));
            expect(transformIndexDeleteMove(3, 3, 2)).toEqual(shiftMoveRes(2));
            expect(transformIndexDeleteMove(4, 3, 2)).toEqual(shiftMoveRes(4, 3, 2));
        });
        it('should handle no-op move', function () {
            expect(transformIndexDeleteMove(1, 3, 3)).toEqual(shiftMoveRes(1));
            expect(transformIndexDeleteMove(2, 3, 3)).toEqual(shiftMoveRes(2));
            expect(transformIndexDeleteMove(3, 3, 3)).toEqual(shiftMoveRes(3));
            expect(transformIndexDeleteMove(4, 3, 3)).toEqual(shiftMoveRes(4));
            expect(transformIndexDeleteMove(5, 3, 3)).toEqual(shiftMoveRes(5));
        });
    });

    describe('function transformIndexDeleteCopy', function () {
        it('should exist', function () {
            expect(transformIndexDeleteCopy).toBeFunction();
        });
        it('should handle index copied to end', function () {
            expect(transformIndexDeleteCopy(1, 2, 4)).toEqual(shiftCopyRes(1, 1, 3));
            expect(transformIndexDeleteCopy(2, 2, 4)).toEqual(shiftCopyDelRes(2, 3));
            expect(transformIndexDeleteCopy(3, 2, 4)).toEqual(shiftCopyRes(3, 2, 3));
            expect(transformIndexDeleteCopy(4, 2, 4)).toEqual(shiftCopyRes(5, 2, 4));
            expect(transformIndexDeleteCopy(5, 2, 4)).toEqual(shiftCopyRes(6, 2, 4));
            expect(transformIndexDeleteCopy(1, 2, 3)).toEqual(shiftCopyRes(1, 1, 2));
            expect(transformIndexDeleteCopy(2, 2, 3)).toEqual(shiftCopyDelRes(2, 2));
            expect(transformIndexDeleteCopy(3, 2, 3)).toEqual(shiftCopyRes(4, 2, 3));
            expect(transformIndexDeleteCopy(4, 2, 3)).toEqual(shiftCopyRes(5, 2, 3));
        });
        it('should handle index copied to front', function () {
            expect(transformIndexDeleteCopy(1, 4, 2)).toEqual(shiftCopyRes(1, 3, 1));
            expect(transformIndexDeleteCopy(2, 4, 2)).toEqual(shiftCopyRes(3, 3, 2));
            expect(transformIndexDeleteCopy(3, 4, 2)).toEqual(shiftCopyRes(4, 3, 2));
            expect(transformIndexDeleteCopy(4, 4, 2)).toEqual(shiftCopyDelRes(5, 2));
            expect(transformIndexDeleteCopy(5, 4, 2)).toEqual(shiftCopyRes(6, 4, 2));
            expect(transformIndexDeleteCopy(1, 3, 2)).toEqual(shiftCopyRes(1, 2, 1));
            expect(transformIndexDeleteCopy(2, 3, 2)).toEqual(shiftCopyRes(3, 2, 2));
            expect(transformIndexDeleteCopy(3, 3, 2)).toEqual(shiftCopyDelRes(4, 2));
            expect(transformIndexDeleteCopy(4, 3, 2)).toEqual(shiftCopyRes(5, 3, 2));
        });
        it('should handle index copied to itself', function () {
            expect(transformIndexDeleteCopy(1, 3, 3)).toEqual(shiftCopyRes(1, 2, 2));
            expect(transformIndexDeleteCopy(2, 3, 3)).toEqual(shiftCopyRes(2, 2, 2));
            expect(transformIndexDeleteCopy(3, 3, 3)).toEqual(shiftCopyDelRes(4, 3));
            expect(transformIndexDeleteCopy(4, 3, 3)).toEqual(shiftCopyRes(5, 3, 3));
            expect(transformIndexDeleteCopy(5, 3, 3)).toEqual(shiftCopyRes(6, 3, 3));
        });
    });

    describe('function transformIndexDeleteSort', function () {
        it('should exist', function () {
            expect(transformIndexDeleteSort).toBeFunction();
        });
        it('should transform the sort vector', function () {
            var VECTOR = [0, 6, 3, 4, 2, 1, 5];
            expect(transformIndexDeleteSort(0, VECTOR)).toEqual(shiftSortRes(0, [5, 2, 3, 1, 0, 4]));
            expect(transformIndexDeleteSort(1, VECTOR)).toEqual(shiftSortRes(5, [0, 5, 2, 3, 1, 4]));
            expect(transformIndexDeleteSort(2, VECTOR)).toEqual(shiftSortRes(4, [0, 5, 2, 3, 1, 4]));
            expect(transformIndexDeleteSort(3, VECTOR)).toEqual(shiftSortRes(2, [0, 5, 3, 2, 1, 4]));
            expect(transformIndexDeleteSort(4, VECTOR)).toEqual(shiftSortRes(3, [0, 5, 3, 2, 1, 4]));
            expect(transformIndexDeleteSort(5, VECTOR)).toEqual(shiftSortRes(6, [0, 5, 3, 4, 2, 1]));
            expect(transformIndexDeleteSort(6, VECTOR)).toEqual(shiftSortRes(1, [0, 3, 4, 2, 1, 5]));
        });
        it('should detect no-op sort vector', function () {
            expect(transformIndexDeleteSort(1, [0, 2, 1, 3, 4])).toEqual(shiftSortRes(2));
        });
    });

    describe('function transformIndexMoveMove', function () {
        it('should exist', function () {
            expect(transformIndexMoveMove).toBeFunction();
        });
        // array index encodes the source indexes in one-based BASE-5 (lclFrom, lclTo, extFrom, extTo)
        // array element contains the expected transformed indexes (digits in same order as above)
        var EXPECT_MATRIX = [
            '1111', '2111', '3111', '4111', '5111', '2221', '1122', '1123', '1124', '1125', '2231', '1132', '1133', '1134', '1135', '2241', '1142', '1143', '1144', '1145', '2251', '1152', '1153', '1154', '1155',
            '1222', '2222', '3222', '4222', '5222', '2211', '1211', '1113', '1114', '1115', '2331', '1331', '1233', '1234', '1235', '2341', '1341', '1243', '1244', '1245', '2351', '1351', '1253', '1254', '1255',
            '1333', '2333', '3333', '4333', '5333', '2311', '1311', '1213', '1214', '1215', '2321', '1321', '1322', '1224', '1225', '2441', '1441', '1442', '1344', '1345', '2451', '1451', '1452', '1354', '1355',
            '1444', '2444', '3444', '4444', '5444', '2411', '1411', '1412', '1314', '1315', '2421', '1421', '1422', '1324', '1325', '2431', '1431', '1432', '1433', '1335', '2551', '1551', '1552', '1553', '1455',
            '1555', '2555', '3555', '4555', '5555', '2511', '1511', '1512', '1513', '1415', '2521', '1521', '1522', '1523', '1425', '2531', '1531', '1532', '1533', '1435', '2541', '1541', '1542', '1543', '1544',
            '2122', '1122', '1123', '1124', '1125', '1111', '2111', '3111', '4111', '5111', '3231', '3133', '2133', '2134', '2135', '3241', '3143', '2143', '2144', '2145', '3251', '3153', '2153', '2154', '2155',
            '2211', '1112', '1113', '1114', '1115', '1222', '2222', '3222', '4222', '5222', '3331', '3332', '2233', '2234', '2235', '3341', '3342', '2243', '2244', '2245', '3351', '3352', '2253', '2254', '2255',
            '2311', '1311', '1213', '1214', '1215', '1333', '2333', '3333', '4333', '5333', '3321', '3322', '2322', '2224', '2225', '3441', '3442', '2442', '2344', '2345', '3451', '3452', '2452', '2354', '2355',
            '2411', '1411', '1412', '1314', '1315', '1444', '2444', '3444', '4444', '5444', '3421', '3422', '2422', '2324', '2325', '3431', '3432', '2432', '2433', '2335', '3551', '3552', '2552', '2553', '2455',
            '2511', '1511', '1512', '1513', '1415', '1555', '2555', '3555', '4555', '5555', '3521', '3522', '2522', '2523', '2425', '3531', '3532', '2532', '2533', '2435', '3541', '3542', '2542', '2543', '2544',
            '3122', '3123', '2123', '2124', '2125', '3231', '3133', '2133', '2134', '2135', '1111', '2111', '3111', '4111', '5111', '4241', '4143', '4144', '3144', '3145', '4251', '4153', '4154', '3154', '3155',
            '3211', '3113', '2113', '2114', '2115', '3331', '3233', '2233', '2234', '2235', '1222', '2222', '3222', '4222', '5222', '4341', '4342', '4244', '3244', '3245', '4351', '4352', '4254', '3254', '3255',
            '3311', '3312', '2213', '2214', '2215', '3321', '3322', '2223', '2224', '2225', '1333', '2333', '3333', '4333', '5333', '4441', '4442', '4443', '3344', '3345', '4451', '4452', '4453', '3354', '3355',
            '3411', '3412', '2412', '2314', '2315', '3421', '3422', '2422', '2324', '2325', '1444', '2444', '3444', '4444', '5444', '4431', '4432', '4433', '3433', '3335', '4551', '4552', '4553', '3553', '3455',
            '3511', '3512', '2512', '2513', '2415', '3521', '3522', '2522', '2523', '2425', '1555', '2555', '3555', '4555', '5555', '4531', '4532', '4533', '3533', '3435', '4541', '4542', '4543', '3543', '3544',
            '4122', '4123', '4124', '3124', '3125', '4231', '4133', '4134', '3134', '3135', '4241', '4143', '4144', '3144', '3145', '1111', '2111', '3111', '4111', '5111', '5251', '5153', '5154', '5155', '4155',
            '4211', '4113', '4114', '3114', '3115', '4331', '4233', '4234', '3234', '3235', '4341', '4342', '4244', '3244', '3245', '1222', '2222', '3222', '4222', '5222', '5351', '5352', '5254', '5255', '4255',
            '4311', '4312', '4214', '3214', '3215', '4321', '4322', '4224', '3224', '3225', '4441', '4442', '4344', '3344', '3345', '1333', '2333', '3333', '4333', '5333', '5451', '5452', '5453', '5355', '4355',
            '4411', '4412', '4413', '3314', '3315', '4421', '4422', '4423', '3324', '3325', '4431', '4432', '4433', '3334', '3335', '1444', '2444', '3444', '4444', '5444', '5551', '5552', '5553', '5554', '4455',
            '4511', '4512', '4513', '3513', '3415', '4521', '4522', '4523', '3523', '3425', '4531', '4532', '4533', '3533', '3435', '1555', '2555', '3555', '4555', '5555', '5541', '5542', '5543', '5544', '4544',
            '5122', '5123', '5124', '5125', '4125', '5231', '5133', '5134', '5135', '4135', '5241', '5143', '5144', '5145', '4145', '5251', '5153', '5154', '5155', '4155', '1111', '2111', '3111', '4111', '5111',
            '5211', '5113', '5114', '5115', '4115', '5331', '5233', '5234', '5235', '4235', '5341', '5342', '5244', '5245', '4245', '5351', '5352', '5254', '5255', '4255', '1222', '2222', '3222', '4222', '5222',
            '5311', '5312', '5214', '5215', '4215', '5321', '5322', '5224', '5225', '4225', '5441', '5442', '5344', '5345', '4345', '5451', '5452', '5453', '5355', '4355', '1333', '2333', '3333', '4333', '5333',
            '5411', '5412', '5413', '5315', '4315', '5421', '5422', '5423', '5325', '4325', '5431', '5432', '5433', '5335', '4335', '5551', '5552', '5553', '5455', '4455', '1444', '2444', '3444', '4444', '5444',
            '5511', '5512', '5513', '5514', '4415', '5521', '5522', '5523', '5524', '4425', '5531', '5532', '5533', '5534', '4435', '5541', '5542', '5543', '5544', '4445', '1555', '2555', '3555', '4555', '5555'
        ];
        it('should transform the array indexes', function () {
            EXPECT_MATRIX.forEach(function (value, index) {
                var src = decodeBase5(index);
                var exp = decodeMoveMove(value);
                expect(JSON.stringify(transformIndexMoveMove(...src))).toEqual(JSON.stringify(exp));
            });
        });
    });

    describe('function transformIndexMoveCopy', function () {
        it('should exist', function () {
            expect(transformIndexMoveCopy).toBeFunction();
        });
        // array index encodes the source indexes in BASE-5 (moveFrom, moveTo, copyFrom, copyTo)
        // array element contains the expected transformed indexes (digits in same order as above)
        var EXPECT_MATRIX = [
            '2211', '1112', '1113', '1114', '1115', '2221', '1122', '1123', '1124', '1125', '2231', '1132', '1133', '1134', '1135', '2241', '1142', '1143', '1144', '1145', '2251', '1152', '1153', '1154', '1155',
            '2321', '1321', '1223', '1224', '1225', '2311', '1311', '1213', '1214', '1215', '2331', '1331', '1233', '1234', '1235', '2341', '1341', '1243', '1244', '1245', '2351', '1351', '1253', '1254', '1255',
            '2431', '1431', '1432', '1334', '1335', '2411', '1411', '1412', '1314', '1315', '2421', '1421', '1422', '1324', '1325', '2441', '1441', '1442', '1344', '1345', '2451', '1451', '1452', '1354', '1355',
            '2541', '1541', '1542', '1543', '1445', '2511', '1511', '1512', '1513', '1415', '2521', '1521', '1522', '1523', '1425', '2531', '1531', '1532', '1533', '1435', '2551', '1551', '1552', '1553', '1455',
            '2651', '1651', '1652', '1653', '1654', '2611', '1611', '1612', '1613', '1614', '2621', '1621', '1622', '1623', '1624', '2631', '1631', '1632', '1633', '1634', '2641', '1641', '1642', '1643', '1644',
            '3221', '3123', '2123', '2124', '2125', '3211', '3113', '2113', '2114', '2115', '3231', '3133', '2133', '2134', '2135', '3241', '3143', '2143', '2144', '2145', '3251', '3153', '2153', '2154', '2155',
            '3311', '3312', '2213', '2214', '2215', '3321', '3322', '2223', '2224', '2225', '3331', '3332', '2233', '2234', '2235', '3341', '3342', '2243', '2244', '2245', '3351', '3352', '2253', '2254', '2255',
            '3411', '3412', '2412', '2314', '2315', '3431', '3432', '2432', '2334', '2335', '3421', '3422', '2422', '2324', '2325', '3441', '3442', '2442', '2344', '2345', '3451', '3452', '2452', '2354', '2355',
            '3511', '3512', '2512', '2513', '2415', '3541', '3542', '2542', '2543', '2445', '3521', '3522', '2522', '2523', '2425', '3531', '3532', '2532', '2533', '2435', '3551', '3552', '2552', '2553', '2455',
            '3611', '3612', '2612', '2613', '2614', '3651', '3652', '2652', '2653', '2654', '3621', '3622', '2622', '2623', '2624', '3631', '3632', '2632', '2633', '2634', '3641', '3642', '2642', '2643', '2644',
            '4221', '4123', '4124', '3124', '3125', '4231', '4133', '4134', '3134', '3135', '4211', '4113', '4114', '3114', '3115', '4241', '4143', '4144', '3144', '3145', '4251', '4153', '4154', '3154', '3155',
            '4311', '4312', '4214', '3214', '3215', '4331', '4332', '4234', '3234', '3235', '4321', '4322', '4224', '3224', '3225', '4341', '4342', '4244', '3244', '3245', '4351', '4352', '4254', '3254', '3255',
            '4411', '4412', '4413', '3314', '3315', '4421', '4422', '4423', '3324', '3325', '4431', '4432', '4433', '3334', '3335', '4441', '4442', '4443', '3344', '3345', '4451', '4452', '4453', '3354', '3355',
            '4511', '4512', '4513', '3513', '3415', '4521', '4522', '4523', '3523', '3425', '4541', '4542', '4543', '3543', '3445', '4531', '4532', '4533', '3533', '3435', '4551', '4552', '4553', '3553', '3455',
            '4611', '4612', '4613', '3613', '3614', '4621', '4622', '4623', '3623', '3624', '4651', '4652', '4653', '3653', '3654', '4631', '4632', '4633', '3633', '3634', '4641', '4642', '4643', '3643', '3644',
            '5221', '5123', '5124', '5125', '4125', '5231', '5133', '5134', '5135', '4135', '5241', '5143', '5144', '5145', '4145', '5211', '5113', '5114', '5115', '4115', '5251', '5153', '5154', '5155', '4155',
            '5311', '5312', '5214', '5215', '4215', '5331', '5332', '5234', '5235', '4235', '5341', '5342', '5244', '5245', '4245', '5321', '5322', '5224', '5225', '4225', '5351', '5352', '5254', '5255', '4255',
            '5411', '5412', '5413', '5315', '4315', '5421', '5422', '5423', '5325', '4325', '5441', '5442', '5443', '5345', '4345', '5431', '5432', '5433', '5335', '4335', '5451', '5452', '5453', '5355', '4355',
            '5511', '5512', '5513', '5514', '4415', '5521', '5522', '5523', '5524', '4425', '5531', '5532', '5533', '5534', '4435', '5541', '5542', '5543', '5544', '4445', '5551', '5552', '5553', '5554', '4455',
            '5611', '5612', '5613', '5614', '4614', '5621', '5622', '5623', '5624', '4624', '5631', '5632', '5633', '5634', '4634', '5651', '5652', '5653', '5654', '4654', '5641', '5642', '5643', '5644', '4644',
            '6221', '6123', '6124', '6125', '6126', '6231', '6133', '6134', '6135', '6136', '6241', '6143', '6144', '6145', '6146', '6251', '6153', '6154', '6155', '6156', '6211', '6113', '6114', '6115', '6116',
            '6311', '6312', '6214', '6215', '6216', '6331', '6332', '6234', '6235', '6236', '6341', '6342', '6244', '6245', '6246', '6351', '6352', '6254', '6255', '6256', '6321', '6322', '6224', '6225', '6226',
            '6411', '6412', '6413', '6315', '6316', '6421', '6422', '6423', '6325', '6326', '6441', '6442', '6443', '6345', '6346', '6451', '6452', '6453', '6355', '6356', '6431', '6432', '6433', '6335', '6336',
            '6511', '6512', '6513', '6514', '6416', '6521', '6522', '6523', '6524', '6426', '6531', '6532', '6533', '6534', '6436', '6551', '6552', '6553', '6554', '6456', '6541', '6542', '6543', '6544', '6446',
            '6611', '6612', '6613', '6614', '6615', '6621', '6622', '6623', '6624', '6625', '6631', '6632', '6633', '6634', '6635', '6641', '6642', '6643', '6644', '6645', '6651', '6652', '6653', '6654', '6655'
        ];
        it('should transform the array indexes', function () {
            EXPECT_MATRIX.forEach(function (value, index) {
                var src = decodeBase5(index);
                var exp = decodeMoveCopy(value);
                expect(JSON.stringify(transformIndexMoveCopy(...src))).toEqual(JSON.stringify(exp));
            });
        });
    });

    describe('function transformIndexMoveSort', function () {
        it('should exist', function () {
            expect(transformIndexMoveSort).toBeFunction();
        });
        it('should transform the sort vector', function () {
            var VECTOR = [0, 6, 3, 4, 2, 1, 5];
            expect(transformIndexMoveSort(1, 1, VECTOR)).toEqual(moveSortRes([0, 6, 3, 4, 2, 1, 5]));
            expect(transformIndexMoveSort(1, 2, VECTOR)).toEqual(moveSortRes([0, 6, 3, 4, 1, 2, 5]));
            expect(transformIndexMoveSort(1, 3, VECTOR)).toEqual(moveSortRes([0, 6, 2, 4, 1, 3, 5]));
            expect(transformIndexMoveSort(1, 4, VECTOR)).toEqual(moveSortRes([0, 6, 2, 3, 1, 4, 5]));
            expect(transformIndexMoveSort(1, 5, VECTOR)).toEqual(moveSortRes([0, 6, 2, 3, 1, 5, 4]));
            expect(transformIndexMoveSort(2, 1, VECTOR)).toEqual(moveSortRes([0, 6, 3, 4, 1, 2, 5]));
            expect(transformIndexMoveSort(2, 2, VECTOR)).toEqual(moveSortRes([0, 6, 3, 4, 2, 1, 5]));
            expect(transformIndexMoveSort(2, 3, VECTOR)).toEqual(moveSortRes([0, 6, 2, 4, 3, 1, 5]));
            expect(transformIndexMoveSort(2, 4, VECTOR)).toEqual(moveSortRes([0, 6, 2, 3, 4, 1, 5]));
            expect(transformIndexMoveSort(2, 5, VECTOR)).toEqual(moveSortRes([0, 6, 2, 3, 5, 1, 4]));
            expect(transformIndexMoveSort(3, 1, VECTOR)).toEqual(moveSortRes([0, 6, 1, 4, 3, 2, 5]));
            expect(transformIndexMoveSort(3, 2, VECTOR)).toEqual(moveSortRes([0, 6, 2, 4, 3, 1, 5]));
            expect(transformIndexMoveSort(3, 3, VECTOR)).toEqual(moveSortRes([0, 6, 3, 4, 2, 1, 5]));
            expect(transformIndexMoveSort(3, 4, VECTOR)).toEqual(moveSortRes([0, 6, 4, 3, 2, 1, 5]));
            expect(transformIndexMoveSort(3, 5, VECTOR)).toEqual(moveSortRes([0, 6, 5, 3, 2, 1, 4]));
            expect(transformIndexMoveSort(4, 1, VECTOR)).toEqual(moveSortRes([0, 6, 4, 1, 3, 2, 5]));
            expect(transformIndexMoveSort(4, 2, VECTOR)).toEqual(moveSortRes([0, 6, 4, 2, 3, 1, 5]));
            expect(transformIndexMoveSort(4, 3, VECTOR)).toEqual(moveSortRes([0, 6, 4, 3, 2, 1, 5]));
            expect(transformIndexMoveSort(4, 4, VECTOR)).toEqual(moveSortRes([0, 6, 3, 4, 2, 1, 5]));
            expect(transformIndexMoveSort(4, 5, VECTOR)).toEqual(moveSortRes([0, 6, 3, 5, 2, 1, 4]));
            expect(transformIndexMoveSort(5, 1, VECTOR)).toEqual(moveSortRes([0, 6, 4, 5, 3, 2, 1]));
            expect(transformIndexMoveSort(5, 2, VECTOR)).toEqual(moveSortRes([0, 6, 4, 5, 3, 1, 2]));
            expect(transformIndexMoveSort(5, 3, VECTOR)).toEqual(moveSortRes([0, 6, 4, 5, 2, 1, 3]));
            expect(transformIndexMoveSort(5, 4, VECTOR)).toEqual(moveSortRes([0, 6, 3, 5, 2, 1, 4]));
            expect(transformIndexMoveSort(5, 5, VECTOR)).toEqual(moveSortRes([0, 6, 3, 4, 2, 1, 5]));
        });
        it('should detect no-op sort vector', function () {
            expect(transformIndexMoveSort(1, 2, [0, 2, 1, 3, 4])).toEqual(moveSortRes());
        });
    });

    describe('function transformIndexCopyCopy', function () {
        it('should exist', function () {
            expect(transformIndexCopyCopy).toBeFunction();
        });
        // array index encodes the source indexes in BASE-5 (lclFrom, lclTo, extFrom, extTo)
        // array element contains the expected transformed indexes (digits in same order as above)
        var EXPECT_MATRIX = [
            '2221', '1123', '1124', '1125', '1126', '2231', '1133', '1134', '1135', '1136', '2241', '1143', '1144', '1145', '1146', '2251', '1153', '1154', '1155', '1156', '2261', '1163', '1164', '1165', '1166',
            '2311', '1312', '1214', '1215', '1216', '2331', '1332', '1234', '1235', '1236', '2341', '1342', '1244', '1245', '1246', '2351', '1352', '1254', '1255', '1256', '2361', '1362', '1264', '1265', '1266',
            '2411', '1412', '1413', '1315', '1316', '2421', '1422', '1423', '1325', '1326', '2441', '1442', '1443', '1345', '1346', '2451', '1452', '1453', '1355', '1356', '2461', '1462', '1463', '1365', '1366',
            '2511', '1512', '1513', '1514', '1416', '2521', '1522', '1523', '1524', '1426', '2531', '1532', '1533', '1534', '1436', '2551', '1552', '1553', '1554', '1456', '2561', '1562', '1563', '1564', '1466',
            '2611', '1612', '1613', '1614', '1615', '2621', '1622', '1623', '1624', '1625', '2631', '1632', '1633', '1634', '1635', '2641', '1642', '1643', '1644', '1645', '2661', '1662', '1663', '1664', '1665',
            '3221', '3123', '2124', '2125', '2126', '3231', '3133', '2134', '2135', '2136', '3241', '3143', '2144', '2145', '2146', '3251', '3153', '2154', '2155', '2156', '3261', '3163', '2164', '2165', '2166',
            '3311', '3312', '2214', '2215', '2216', '3331', '3332', '2234', '2235', '2236', '3341', '3342', '2244', '2245', '2246', '3351', '3352', '2254', '2255', '2256', '3361', '3362', '2264', '2265', '2266',
            '3411', '3412', '2413', '2315', '2316', '3421', '3422', '2423', '2325', '2326', '3441', '3442', '2443', '2345', '2346', '3451', '3452', '2453', '2355', '2356', '3461', '3462', '2463', '2365', '2366',
            '3511', '3512', '2513', '2514', '2416', '3521', '3522', '2523', '2524', '2426', '3531', '3532', '2533', '2534', '2436', '3551', '3552', '2553', '2554', '2456', '3561', '3562', '2563', '2564', '2466',
            '3611', '3612', '2613', '2614', '2615', '3621', '3622', '2623', '2624', '2625', '3631', '3632', '2633', '2634', '2635', '3641', '3642', '2643', '2644', '2645', '3661', '3662', '2663', '2664', '2665',
            '4221', '4123', '4124', '3125', '3126', '4231', '4133', '4134', '3135', '3136', '4241', '4143', '4144', '3145', '3146', '4251', '4153', '4154', '3155', '3156', '4261', '4163', '4164', '3165', '3166',
            '4311', '4312', '4214', '3215', '3216', '4331', '4332', '4234', '3235', '3236', '4341', '4342', '4244', '3245', '3246', '4351', '4352', '4254', '3255', '3256', '4361', '4362', '4264', '3265', '3266',
            '4411', '4412', '4413', '3315', '3316', '4421', '4422', '4423', '3325', '3326', '4441', '4442', '4443', '3345', '3346', '4451', '4452', '4453', '3355', '3356', '4461', '4462', '4463', '3365', '3366',
            '4511', '4512', '4513', '3514', '3416', '4521', '4522', '4523', '3524', '3426', '4531', '4532', '4533', '3534', '3436', '4551', '4552', '4553', '3554', '3456', '4561', '4562', '4563', '3564', '3466',
            '4611', '4612', '4613', '3614', '3615', '4621', '4622', '4623', '3624', '3625', '4631', '4632', '4633', '3634', '3635', '4641', '4642', '4643', '3644', '3645', '4661', '4662', '4663', '3664', '3665',
            '5221', '5123', '5124', '5125', '4126', '5231', '5133', '5134', '5135', '4136', '5241', '5143', '5144', '5145', '4146', '5251', '5153', '5154', '5155', '4156', '5261', '5163', '5164', '5165', '4166',
            '5311', '5312', '5214', '5215', '4216', '5331', '5332', '5234', '5235', '4236', '5341', '5342', '5244', '5245', '4246', '5351', '5352', '5254', '5255', '4256', '5361', '5362', '5264', '5265', '4266',
            '5411', '5412', '5413', '5315', '4316', '5421', '5422', '5423', '5325', '4326', '5441', '5442', '5443', '5345', '4346', '5451', '5452', '5453', '5355', '4356', '5461', '5462', '5463', '5365', '4366',
            '5511', '5512', '5513', '5514', '4416', '5521', '5522', '5523', '5524', '4426', '5531', '5532', '5533', '5534', '4436', '5551', '5552', '5553', '5554', '4456', '5561', '5562', '5563', '5564', '4466',
            '5611', '5612', '5613', '5614', '4615', '5621', '5622', '5623', '5624', '4625', '5631', '5632', '5633', '5634', '4635', '5641', '5642', '5643', '5644', '4645', '5661', '5662', '5663', '5664', '4665',
            '6221', '6123', '6124', '6125', '6126', '6231', '6133', '6134', '6135', '6136', '6241', '6143', '6144', '6145', '6146', '6251', '6153', '6154', '6155', '6156', '6261', '6163', '6164', '6165', '6166',
            '6311', '6312', '6214', '6215', '6216', '6331', '6332', '6234', '6235', '6236', '6341', '6342', '6244', '6245', '6246', '6351', '6352', '6254', '6255', '6256', '6361', '6362', '6264', '6265', '6266',
            '6411', '6412', '6413', '6315', '6316', '6421', '6422', '6423', '6325', '6326', '6441', '6442', '6443', '6345', '6346', '6451', '6452', '6453', '6355', '6356', '6461', '6462', '6463', '6365', '6366',
            '6511', '6512', '6513', '6514', '6416', '6521', '6522', '6523', '6524', '6426', '6531', '6532', '6533', '6534', '6436', '6551', '6552', '6553', '6554', '6456', '6561', '6562', '6563', '6564', '6466',
            '6611', '6612', '6613', '6614', '6615', '6621', '6622', '6623', '6624', '6625', '6631', '6632', '6633', '6634', '6635', '6641', '6642', '6643', '6644', '6645', '6661', '6662', '6663', '6664', '6665'
        ];
        it('should transform the array indexes', function () {
            EXPECT_MATRIX.forEach(function (value, index) {
                var src = decodeBase5(index);
                var exp = decodeCopyCopy(value);
                expect(JSON.stringify(transformIndexCopyCopy(...src))).toEqual(JSON.stringify(exp));
            });
        });
    });

    describe('function transformIndexCopySort', function () {
        it('should exist', function () {
            expect(transformIndexCopySort).toBeFunction();
        });
        it('should transform the array indexes, and the sort vector', function () {
            var VECTOR = [0, 6, 3, 4, 2, 1, 5];
            expect(transformIndexCopySort(1, 1, VECTOR)).toEqual(copySortRes(5, 1, [0, 1, 7, 4, 5, 3, 2, 6]));
            expect(transformIndexCopySort(1, 2, VECTOR)).toEqual(copySortRes(5, 2, [0, 7, 2, 4, 5, 3, 1, 6]));
            expect(transformIndexCopySort(1, 3, VECTOR)).toEqual(copySortRes(5, 3, [0, 7, 4, 3, 5, 2, 1, 6]));
            expect(transformIndexCopySort(1, 4, VECTOR)).toEqual(copySortRes(5, 4, [0, 7, 3, 5, 4, 2, 1, 6]));
            expect(transformIndexCopySort(1, 5, VECTOR)).toEqual(copySortRes(5, 5, [0, 7, 3, 4, 2, 5, 1, 6]));
            expect(transformIndexCopySort(2, 1, VECTOR)).toEqual(copySortRes(4, 1, [0, 1, 7, 4, 5, 3, 2, 6]));
            expect(transformIndexCopySort(2, 2, VECTOR)).toEqual(copySortRes(4, 2, [0, 7, 2, 4, 5, 3, 1, 6]));
            expect(transformIndexCopySort(2, 3, VECTOR)).toEqual(copySortRes(4, 3, [0, 7, 4, 3, 5, 2, 1, 6]));
            expect(transformIndexCopySort(2, 4, VECTOR)).toEqual(copySortRes(4, 4, [0, 7, 3, 5, 4, 2, 1, 6]));
            expect(transformIndexCopySort(2, 5, VECTOR)).toEqual(copySortRes(4, 5, [0, 7, 3, 4, 2, 5, 1, 6]));
            expect(transformIndexCopySort(3, 1, VECTOR)).toEqual(copySortRes(2, 1, [0, 1, 7, 4, 5, 3, 2, 6]));
            expect(transformIndexCopySort(3, 2, VECTOR)).toEqual(copySortRes(2, 2, [0, 7, 2, 4, 5, 3, 1, 6]));
            expect(transformIndexCopySort(3, 3, VECTOR)).toEqual(copySortRes(2, 3, [0, 7, 4, 3, 5, 2, 1, 6]));
            expect(transformIndexCopySort(3, 4, VECTOR)).toEqual(copySortRes(2, 4, [0, 7, 3, 5, 4, 2, 1, 6]));
            expect(transformIndexCopySort(3, 5, VECTOR)).toEqual(copySortRes(2, 5, [0, 7, 3, 4, 2, 5, 1, 6]));
            expect(transformIndexCopySort(4, 1, VECTOR)).toEqual(copySortRes(3, 1, [0, 1, 7, 4, 5, 3, 2, 6]));
            expect(transformIndexCopySort(4, 2, VECTOR)).toEqual(copySortRes(3, 2, [0, 7, 2, 4, 5, 3, 1, 6]));
            expect(transformIndexCopySort(4, 3, VECTOR)).toEqual(copySortRes(3, 3, [0, 7, 4, 3, 5, 2, 1, 6]));
            expect(transformIndexCopySort(4, 4, VECTOR)).toEqual(copySortRes(3, 4, [0, 7, 3, 5, 4, 2, 1, 6]));
            expect(transformIndexCopySort(4, 5, VECTOR)).toEqual(copySortRes(3, 5, [0, 7, 3, 4, 2, 5, 1, 6]));
            expect(transformIndexCopySort(5, 1, VECTOR)).toEqual(copySortRes(6, 1, [0, 1, 7, 4, 5, 3, 2, 6]));
            expect(transformIndexCopySort(5, 2, VECTOR)).toEqual(copySortRes(6, 2, [0, 7, 2, 4, 5, 3, 1, 6]));
            expect(transformIndexCopySort(5, 3, VECTOR)).toEqual(copySortRes(6, 3, [0, 7, 4, 3, 5, 2, 1, 6]));
            expect(transformIndexCopySort(5, 4, VECTOR)).toEqual(copySortRes(6, 4, [0, 7, 3, 5, 4, 2, 1, 6]));
            expect(transformIndexCopySort(5, 5, VECTOR)).toEqual(copySortRes(6, 5, [0, 7, 3, 4, 2, 5, 1, 6]));
        });
        it('should detect no-op sort vector', function () {
            expect(transformIndexCopySort(1, 2, [0, 1, 2, 3, 4])).toEqual(copySortRes(1, 2));
        });
    });

    describe('function transformIndexSortSort', function () {
        it('should exist', function () {
            expect(transformIndexSortSort).toBeFunction();
        });
        it('should transform local sort vectors', function () {
            var VECTOR1 = [0, 6, 3, 4, 2, 1, 5];
            var VECTOR2 = [3, 2, 6, 0, 4, 5, 1];
            expect(transformIndexSortSort(VECTOR1, VECTOR2)).toEqual({ lclSortVec: [3, 2, 0, 4, 1, 6, 5], extSortVec: undefined });
            expect(transformIndexSortSort(VECTOR2, VECTOR1)).toEqual({ lclSortVec: [2, 4, 1, 0, 3, 6, 5], extSortVec: undefined });
        });
        it('should detect local no-op sort vector', function () {
            var VECTOR = [0, 6, 3, 4, 2, 1, 5];
            expect(transformIndexSortSort(VECTOR, VECTOR)).toEqual({ lclSortVec: undefined, extSortVec: undefined });
        });
    });

    describe('function transformPositionInsert', function () {
        it('should exist', function () {
            expect(transformPositionInsert).toBeFunction();
        });
        it('should modify positions in-place', function () {
            var pos = [1, 1];
            expect(transformPositionInsert(pos, [0])).toBe(pos);
        });
        it('should shift related positions (level-1 siblings)', function () {
            expect(transformPositionInsert([0], [1], 1)).toEqual([0]);
            expect(transformPositionInsert([1], [1], 1)).toEqual([2]);
            expect(transformPositionInsert([2], [1], 1)).toEqual([3]);
        });
        it('should shift related positions with size (level-1 siblings)', function () {
            expect(transformPositionInsert([0], [1], 2)).toEqual([0]);
            expect(transformPositionInsert([1], [1], 2)).toEqual([3]);
            expect(transformPositionInsert([2], [1], 2)).toEqual([4]);
        });
        it('should shift related positions (descendants)', function () {
            expect(transformPositionInsert([0, 5], [1], 1)).toEqual([0, 5]);
            expect(transformPositionInsert([1, 5], [1], 1)).toEqual([2, 5]);
            expect(transformPositionInsert([2, 5], [1], 1)).toEqual([3, 5]);
        });
        it('should shift related positions (level-2 siblings)', function () {
            expect(transformPositionInsert([5, 0], [5, 1], 1)).toEqual([5, 0]);
            expect(transformPositionInsert([5, 1], [5, 1], 1)).toEqual([5, 2]);
            expect(transformPositionInsert([5, 2], [5, 1], 1)).toEqual([5, 3]);
        });
        it('should not shift unrelated positions (parents)', function () {
            expect(transformPositionInsert([0], [1, 5], 1)).toEqual([0]);
            expect(transformPositionInsert([1], [1, 5], 1)).toEqual([1]);
            expect(transformPositionInsert([2], [1, 5], 1)).toEqual([2]);
        });
        it('should not shift unrelated positions (non-siblings)', function () {
            expect(transformPositionInsert([5, 0], [1, 5], 1)).toEqual([5, 0]);
            expect(transformPositionInsert([5, 1], [1, 5], 1)).toEqual([5, 1]);
            expect(transformPositionInsert([5, 2], [1, 5], 1)).toEqual([5, 2]);
        });
    });

    describe('function transformPositionDelete', function () {
        it('should exist', function () {
            expect(transformPositionDelete).toBeFunction();
        });
        it('should modify positions in-place', function () {
            var pos = [1, 1];
            expect(transformPositionDelete(pos, [0])).toBe(pos);
        });
        it('should shift related positions (level-1 siblings)', function () {
            expect(transformPositionDelete([0], [1], 1)).toEqual([0]);
            expect(transformPositionDelete([1], [1], 1)).toBeUndefined();
            expect(transformPositionDelete([2], [1], 1)).toEqual([1]);
        });
        it('should shift related positions with size (level-1 siblings)', function () {
            expect(transformPositionDelete([0], [1], 2)).toEqual([0]);
            expect(transformPositionDelete([1], [1], 2)).toBeUndefined();
            expect(transformPositionDelete([2], [1], 2)).toBeUndefined();
            expect(transformPositionDelete([3], [1], 2)).toEqual([1]);
        });
        it('should shift related positions (descendants)', function () {
            expect(transformPositionDelete([0, 5], [1], 1)).toEqual([0, 5]);
            expect(transformPositionDelete([1, 5], [1], 1)).toBeUndefined();
            expect(transformPositionDelete([2, 5], [1], 1)).toEqual([1, 5]);
        });
        it('should shift related positions (level-2 siblings)', function () {
            expect(transformPositionDelete([5, 0], [5, 1], 1)).toEqual([5, 0]);
            expect(transformPositionDelete([5, 1], [5, 1], 1)).toBeUndefined();
            expect(transformPositionDelete([5, 2], [5, 1], 1)).toEqual([5, 1]);
        });
        it('should not shift unrelated positions (parents)', function () {
            expect(transformPositionDelete([0], [1, 5], 1)).toEqual([0]);
            expect(transformPositionDelete([1], [1, 5], 1)).toEqual([1]);
            expect(transformPositionDelete([2], [1, 5], 1)).toEqual([2]);
        });
        it('should not shift unrelated positions (non-siblings)', function () {
            expect(transformPositionDelete([5, 0], [1, 5], 1)).toEqual([5, 0]);
            expect(transformPositionDelete([5, 1], [1, 5], 1)).toEqual([5, 1]);
            expect(transformPositionDelete([5, 2], [1, 5], 1)).toEqual([5, 2]);
        });
    });

    describe('function transformPositionMove', function () {
        it('should exist', function () {
            expect(transformPositionMove).toBeFunction();
        });
        it('should modify positions in-place', function () {
            var pos = [1, 1];
            expect(transformPositionMove(pos, [0], 1, 1)).toBe(pos);
        });
        it('should shift related positions (level-1 siblings)', function () {
            expect(transformPositionMove([0], [1], 1, 3)).toEqual([0]);
            expect(transformPositionMove([1], [1], 1, 3)).toEqual([3]);
            expect(transformPositionMove([2], [1], 1, 3)).toEqual([1]);
            expect(transformPositionMove([3], [1], 1, 3)).toEqual([2]);
            expect(transformPositionMove([4], [1], 1, 3)).toEqual([4]);
        });
        it('should shift related positions with size (level-1 siblings)', function () {
            expect(transformPositionMove([0], [1], 2, 3)).toEqual([0]);
            expect(transformPositionMove([1], [1], 2, 3)).toEqual([3]);
            expect(transformPositionMove([2], [1], 2, 3)).toEqual([4]);
            expect(transformPositionMove([3], [1], 2, 3)).toEqual([1]);
            expect(transformPositionMove([4], [1], 2, 3)).toEqual([2]);
            expect(transformPositionMove([5], [1], 2, 3)).toEqual([5]);
        });
        it('should shift related positions (descendants)', function () {
            expect(transformPositionMove([0, 5], [1], 1, 3)).toEqual([0, 5]);
            expect(transformPositionMove([1, 5], [1], 1, 3)).toEqual([3, 5]);
            expect(transformPositionMove([2, 5], [1], 1, 3)).toEqual([1, 5]);
            expect(transformPositionMove([3, 5], [1], 1, 3)).toEqual([2, 5]);
            expect(transformPositionMove([4, 5], [1], 1, 3)).toEqual([4, 5]);
        });
        it('should shift related positions (level-2 siblings)', function () {
            expect(transformPositionMove([5, 0], [5, 1], 1, 3)).toEqual([5, 0]);
            expect(transformPositionMove([5, 1], [5, 1], 1, 3)).toEqual([5, 3]);
            expect(transformPositionMove([5, 2], [5, 1], 1, 3)).toEqual([5, 1]);
            expect(transformPositionMove([5, 3], [5, 1], 1, 3)).toEqual([5, 2]);
            expect(transformPositionMove([5, 4], [5, 1], 1, 3)).toEqual([5, 4]);
        });
        it('should not shift unrelated positions (parents)', function () {
            expect(transformPositionMove([0], [1, 5], 1, 3)).toEqual([0]);
            expect(transformPositionMove([1], [1, 5], 1, 3)).toEqual([1]);
            expect(transformPositionMove([2], [1, 5], 1, 3)).toEqual([2]);
            expect(transformPositionMove([3], [1, 5], 1, 3)).toEqual([3]);
            expect(transformPositionMove([4], [1, 5], 1, 3)).toEqual([4]);
        });
        it('should not shift unrelated positions (non-siblings)', function () {
            expect(transformPositionMove([5, 0], [1, 5], 1, 3)).toEqual([5, 0]);
            expect(transformPositionMove([5, 1], [1, 5], 1, 3)).toEqual([5, 1]);
            expect(transformPositionMove([5, 2], [1, 5], 1, 3)).toEqual([5, 2]);
            expect(transformPositionMove([5, 3], [1, 5], 1, 3)).toEqual([5, 3]);
            expect(transformPositionMove([5, 4], [1, 5], 1, 3)).toEqual([5, 4]);
        });
    });
});
