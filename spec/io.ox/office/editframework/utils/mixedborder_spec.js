/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { opRgbColor, Color } from '@/io.ox/office/editframework/utils/color';
import * as mixedborder from '@/io.ox/office/editframework/utils/mixedborder';

// constants ==================================================================

const AUTO = Color.AUTO;
const SOLID = { style: 'solid', width: 1, color: AUTO };
const DOTTED = { style: 'dotted', width: 1, color: AUTO };
const THICK = { style: 'solid', width: 10, color: AUTO };
const NONE = { style: 'none' };
const MIXED = { style: 'solid', width: 10, color: opRgbColor('C00000'), mixed: true };

// tests ======================================================================

describe('module editframework/utils/mixedborder', function () {

    // functions --------------------------------------------------------------

    describe('function isVisibleBorder', function () {
        const { isVisibleBorder } = mixedborder;
        it('should exist', function () {
            expect(isVisibleBorder).toBeFunction();
        });
        it('should return true for visible borders', function () {
            expect(isVisibleBorder(SOLID)).toBeTrue();
            expect(isVisibleBorder(DOTTED)).toBeTrue();
            expect(isVisibleBorder(THICK)).toBeTrue();
            expect(isVisibleBorder({ mixed: false, ...SOLID })).toBeTrue();
            expect(isVisibleBorder({ mixed: false, ...DOTTED })).toBeTrue();
            expect(isVisibleBorder({ mixed: false, ...THICK })).toBeTrue();
        });
        it('should return false for invisible borders', function () {
            expect(isVisibleBorder(NONE)).toBeFalse();
            expect(isVisibleBorder({ mixed: false, ...NONE })).toBeFalse();
            expect(isVisibleBorder({ style: 'solid', width: 0, color: AUTO })).toBeFalse();
            expect(isVisibleBorder(null)).toBeFalse();
        });
        it('should return true for visible ambiguous borders', function () {
            expect(isVisibleBorder({ mixed: false, style: null, width: 1, color: AUTO })).toBeTrue();
            expect(isVisibleBorder({ mixed: false, style: 'solid', width: null, color: AUTO })).toBeTrue();
            expect(isVisibleBorder({ mixed: false, style: 'solid', width: 1, color: null })).toBeTrue();
            expect(isVisibleBorder({ mixed: false, style: null, width: null, color: null })).toBeTrue();
        });
        it('should return true for mixed borders', function () {
            expect(isVisibleBorder({ mixed: true, ...SOLID })).toBeTrue();
            expect(isVisibleBorder({ mixed: true, ...DOTTED })).toBeTrue();
            expect(isVisibleBorder({ mixed: true, ...THICK })).toBeTrue();
        });
        it('should return true for mixed ambiguous borders', function () {
            expect(isVisibleBorder({ mixed: true, style: null, width: 1, color: AUTO })).toBeTrue();
            expect(isVisibleBorder({ mixed: true, style: 'solid', width: null, color: AUTO })).toBeTrue();
            expect(isVisibleBorder({ mixed: true, style: 'solid', width: 1, color: null })).toBeTrue();
            expect(isVisibleBorder({ mixed: true, style: null, width: null, color: null })).toBeTrue();
        });
    });

    describe('function hasAmbiguousProperties', function () {
        const { hasAmbiguousProperties } = mixedborder;
        it('should exist', function () {
            expect(hasAmbiguousProperties).toBeFunction();
        });
        it('should return true for ambiguous properties', function () {
            expect(hasAmbiguousProperties({ style: null, width: 1, color: AUTO })).toBeTrue();
            expect(hasAmbiguousProperties({ style: 'solid', width: null, color: AUTO })).toBeTrue();
            expect(hasAmbiguousProperties({ style: 'solid', width: 1, color: null })).toBeTrue();
        });
        it('should return false for simple borders', function () {
            expect(hasAmbiguousProperties(NONE)).toBeFalse();
            expect(hasAmbiguousProperties(SOLID)).toBeFalse();
            expect(hasAmbiguousProperties(DOTTED)).toBeFalse();
            expect(hasAmbiguousProperties(THICK)).toBeFalse();
            expect(hasAmbiguousProperties(null)).toBeFalse();
        });
        it('should ignore the mixed flag', function () {
            expect(hasAmbiguousProperties(MIXED)).toBeFalse();
        });
    });

    describe('function isFullyAmbiguousBorder', function () {
        const { isFullyAmbiguousBorder } = mixedborder;
        it('should exist', function () {
            expect(isFullyAmbiguousBorder).toBeFunction();
        });
        it('should return true for an ambiguous mixed border', function () {
            expect(isFullyAmbiguousBorder({ mixed: true, style: null, width: null, color: null })).toBeTrue();
        });
        it('should return false for simple borders', function () {
            expect(isFullyAmbiguousBorder(NONE)).toBeFalse();
            expect(isFullyAmbiguousBorder(SOLID)).toBeFalse();
            expect(isFullyAmbiguousBorder(DOTTED)).toBeFalse();
            expect(isFullyAmbiguousBorder(THICK)).toBeFalse();
            expect(isFullyAmbiguousBorder(null)).toBeFalse();
        });
        it('should return false for all other mixed borders', function () {
            expect(isFullyAmbiguousBorder({ mixed: false, style: null, width: null, color: null })).toBeFalse();
            expect(isFullyAmbiguousBorder({ style: null, width: null, color: null })).toBeFalse();
            expect(isFullyAmbiguousBorder({ mixed: true, style: 'solid', width: null, color: null })).toBeFalse();
            expect(isFullyAmbiguousBorder({ mixed: true, style: null, width: 0, color: null })).toBeFalse();
            expect(isFullyAmbiguousBorder({ mixed: true, style: null, width: null, color: AUTO })).toBeFalse();
        });
    });

    describe('function mixBorders', function () {
        const { mixBorders } = mixedborder;
        it('should exist', function () {
            expect(mixBorders).toBeFunction();
        });
        it('should return equal borders unmodified', function () {
            expect(mixBorders(SOLID)).toEqual({ mixed: false, ...SOLID });
            expect(mixBorders(SOLID, SOLID)).toEqual({ mixed: false, ...SOLID });
            expect(mixBorders(SOLID, SOLID, SOLID)).toEqual({ mixed: false, ...SOLID });
            expect(mixBorders(DOTTED, DOTTED)).toEqual({ mixed: false, ...DOTTED });
            expect(mixBorders(THICK, THICK)).toEqual({ mixed: false, ...THICK });
            expect(mixBorders(NONE, NONE)).toEqual({ mixed: false, ...NONE });
        });
        it('should mix visible borders', function () {
            expect(mixBorders(SOLID, DOTTED)).toEqual({ mixed: false, style: null, width: 1, color: AUTO });
            expect(mixBorders(SOLID, THICK)).toEqual({ mixed: false, style: 'solid', width: null, color: AUTO });
            expect(mixBorders(DOTTED, THICK)).toEqual({ mixed: false, style: null, width: null, color: AUTO });
            expect(mixBorders(SOLID, DOTTED, THICK)).toEqual({ mixed: false, style: null, width: null, color: AUTO });
        });
        it('should mix visible and invisible borders', function () {
            expect(mixBorders(SOLID, NONE)).toEqual({ mixed: true, ...SOLID });
            expect(mixBorders(NONE, SOLID)).toEqual({ mixed: true, ...SOLID });
            expect(mixBorders(DOTTED, NONE)).toEqual({ mixed: true, ...DOTTED });
            expect(mixBorders(NONE, DOTTED)).toEqual({ mixed: true, ...DOTTED });
            expect(mixBorders(THICK, NONE)).toEqual({ mixed: true, ...THICK });
            expect(mixBorders(NONE, THICK)).toEqual({ mixed: true, ...THICK });
            expect(mixBorders(SOLID, DOTTED, NONE)).toEqual({ mixed: true, style: null, width: 1, color: AUTO });
            expect(mixBorders(SOLID, NONE, DOTTED)).toEqual({ mixed: true, style: null, width: 1, color: AUTO });
            expect(mixBorders(NONE, SOLID, DOTTED)).toEqual({ mixed: true, style: null, width: 1, color: AUTO });
            expect(mixBorders(SOLID, THICK, NONE)).toEqual({ mixed: true, style: 'solid', width: null, color: AUTO });
            expect(mixBorders(NONE, SOLID, THICK)).toEqual({ mixed: true, style: 'solid', width: null, color: AUTO });
            expect(mixBorders(DOTTED, THICK, NONE)).toEqual({ mixed: true, style: null, width: null, color: AUTO });
            expect(mixBorders(NONE, DOTTED, THICK)).toEqual({ mixed: true, style: null, width: null, color: AUTO });
            expect(mixBorders(SOLID, DOTTED, THICK, NONE)).toEqual({ mixed: true, style: null, width: null, color: AUTO });
            expect(mixBorders(NONE, SOLID, DOTTED, THICK)).toEqual({ mixed: true, style: null, width: null, color: AUTO });
        });
        it('should mix already mixed borders', function () {
            expect(mixBorders(NONE, MIXED)).toEqual(MIXED);
            expect(mixBorders(MIXED, NONE)).toEqual(MIXED);
            expect(mixBorders(SOLID, { mixed: true, ...SOLID })).toEqual({ mixed: true, ...SOLID });
            expect(mixBorders({ mixed: true, ...SOLID }, SOLID)).toEqual({ mixed: true, ...SOLID });
            expect(mixBorders(NONE, SOLID, DOTTED, MIXED)).toEqual({ mixed: true, style: null, width: null, color: null });
            expect(mixBorders(MIXED, SOLID, DOTTED, NONE)).toEqual({ mixed: true, style: null, width: null, color: null });
        });
        it('should accept an array as parameter', function () {
            expect(mixBorders([SOLID])).toEqual({ mixed: false, ...SOLID });
            expect(mixBorders([SOLID, NONE])).toEqual({ mixed: true, ...SOLID });
            expect(mixBorders([SOLID, DOTTED], THICK, NONE)).toEqual({ mixed: true, style: null, width: null, color: AUTO });
            expect(mixBorders(SOLID, [DOTTED, THICK], NONE)).toEqual({ mixed: true, style: null, width: null, color: AUTO });
            expect(mixBorders(SOLID, DOTTED, [THICK, NONE])).toEqual({ mixed: true, style: null, width: null, color: AUTO });
            expect(mixBorders([SOLID, DOTTED], [THICK, NONE])).toEqual({ mixed: true, style: null, width: null, color: AUTO });
        });
    });

    describe('function getBorderFlags', function () {
        const { getBorderFlags } = mixedborder;
        it('should exist', function () {
            expect(getBorderFlags).toBeFunction();
        });
        it('should return the border flags of a single border', function () {
            expect(getBorderFlags({ borderLeft: SOLID })).toEqual({ l: true });
            expect(getBorderFlags({ borderRight: DOTTED })).toEqual({ r: true });
            expect(getBorderFlags({ borderTop: THICK })).toEqual({ t: true });
            expect(getBorderFlags({ borderBottom: NONE })).toEqual({ b: false });
            expect(getBorderFlags({ borderInsideHor: MIXED })).toEqual({ h: null });
            expect(getBorderFlags({ borderInsideVert: MIXED })).toEqual({ v: null });
        });
        it('should ignore other attributes', function () {
            expect(getBorderFlags({ fillColor: AUTO })).toEqual({});
            expect(getBorderFlags({ borderInside: SOLID })).toEqual({});
        });
        it('should return the border flags of all borders', function () {
            expect(getBorderFlags({ borderLeft: SOLID, borderRight: DOTTED, borderTop: THICK, borderBottom: NONE, borderInsideHor: NONE, borderInsideVert: MIXED })).toEqual({ l: true, r: true, t: true, b: false, h: false, v: null });
        });
        it('should return the border flags of a single border (paragraph mode)', function () {
            expect(getBorderFlags({ borderLeft: SOLID }, { paragraph: true })).toEqual({ l: true });
            expect(getBorderFlags({ borderRight: DOTTED }, { paragraph: true })).toEqual({ r: true });
            expect(getBorderFlags({ borderTop: THICK }, { paragraph: true })).toEqual({ t: true });
            expect(getBorderFlags({ borderBottom: NONE }, { paragraph: true })).toEqual({ b: false });
            expect(getBorderFlags({ borderInside: MIXED }, { paragraph: true })).toEqual({ h: null });
        });
        it('should return the border flags of all borders (paragraph mode)', function () {
            expect(getBorderFlags({ borderLeft: SOLID, borderRight: DOTTED, borderTop: THICK, borderBottom: NONE, borderInside: MIXED }, { paragraph: true })).toEqual({ l: true, r: true, t: true, b: false, h: null });
        });
        it('should ignore other attributes (paragraph mode)', function () {
            expect(getBorderFlags({ fillColor: AUTO }, { paragraph: true })).toEqual({});
            expect(getBorderFlags({ borderInsideHor: SOLID }, { paragraph: true })).toEqual({});
            expect(getBorderFlags({ borderInsideVert: SOLID }, { paragraph: true })).toEqual({});
        });
    });

    describe('function getBorderAttributes', function () {
        const { getBorderAttributes } = mixedborder;
        it('should exist', function () {
            expect(getBorderAttributes).toBeFunction();
        });
        it('should return the border attributes of a single border', function () {
            expect(getBorderAttributes({ l: true }, { borderLeft: DOTTED }, SOLID)).toEqual({ borderLeft: DOTTED });
            expect(getBorderAttributes({ r: false }, { borderRight: SOLID }, SOLID)).toEqual({ borderRight: NONE });
            expect(getBorderAttributes({ t: true }, { borderTop: NONE }, SOLID)).toEqual({ borderTop: SOLID });
            expect(getBorderAttributes({ b: false }, { borderBottom: NONE }, SOLID)).toEqual({ borderBottom: NONE });
            expect(getBorderAttributes({ h: true }, { borderInsideHor: SOLID }, DOTTED)).toEqual({ borderInsideHor: SOLID });
            expect(getBorderAttributes({ v: true }, { borderInsideVert: NONE }, DOTTED)).toEqual({ borderInsideVert: DOTTED });
        });
        it('should return the border attributes of all borders', function () {
            expect(getBorderAttributes({ l: true, r: false, t: true, b: false, h: true, v: true }, {
                borderLeft: DOTTED,
                borderRight: SOLID,
                borderTop: NONE,
                borderBottom: NONE,
                borderInsideHor: THICK,
                borderInsideVert: NONE
            }, SOLID)).toEqual({
                borderLeft: DOTTED,
                borderRight: NONE,
                borderTop: SOLID,
                borderBottom: NONE,
                borderInsideHor: THICK,
                borderInsideVert: SOLID
            });
        });
        it('should remove other attributes', function () {
            expect(getBorderAttributes({ l: true }, { borderLeft: SOLID, borderRight: SOLID, borderInsideHor: SOLID, borderInsideVert: SOLID, borderInside: SOLID, fillColor: AUTO }, SOLID)).toEqual({ borderLeft: SOLID });
        });
        it('should return the border attributes of a single border (paragraph mode)', function () {
            expect(getBorderAttributes({ l: true }, { borderLeft: DOTTED }, SOLID, { paragraph: true })).toEqual({ borderLeft: DOTTED });
            expect(getBorderAttributes({ r: false }, { borderRight: SOLID }, SOLID, { paragraph: true })).toEqual({ borderRight: NONE });
            expect(getBorderAttributes({ t: true }, { borderTop: NONE }, SOLID, { paragraph: true })).toEqual({ borderTop: SOLID });
            expect(getBorderAttributes({ b: false }, { borderBottom: NONE }, SOLID, { paragraph: true })).toEqual({ borderBottom: NONE });
            expect(getBorderAttributes({ h: true }, { borderInside: SOLID }, DOTTED, { paragraph: true })).toEqual({ borderInside: SOLID });
        });
        it('should return the border attributes of all borders (paragraph mode)', function () {
            expect(getBorderAttributes({ l: true, r: false, t: true, b: false, h: true }, {
                borderLeft: DOTTED,
                borderRight: SOLID,
                borderTop: NONE,
                borderBottom: NONE,
                borderInside: THICK
            }, SOLID, { paragraph: true })).toEqual({
                borderLeft: DOTTED,
                borderRight: NONE,
                borderTop: SOLID,
                borderBottom: NONE,
                borderInside: THICK
            });
        });
        it('should remove other attributes (paragraph mode)', function () {
            expect(getBorderAttributes({ l: true }, { borderLeft: SOLID, borderRight: SOLID, borderInsideHor: SOLID, borderInsideVert: SOLID, borderInside: SOLID, fillColor: AUTO }, SOLID, { paragraph: true })).toEqual({ borderLeft: SOLID });
        });
    });
});
