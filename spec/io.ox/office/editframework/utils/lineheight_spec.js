/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as lineheight from "@/io.ox/office/editframework/utils/lineheight";

// tests ======================================================================

describe("module editframework/utils/lineheight", function () {

    // enum LineHeightType ----------------------------------------------------

    describe("enum LineHeightType", function () {
        const { LineHeightType } = lineheight;

        it("should exist", function () {
            expect(LineHeightType).toBeObject();
            expect(LineHeightType).toHaveProperty("NORMAL", "normal");
            expect(LineHeightType).toHaveProperty("PERCENT", "percent");
            expect(LineHeightType).toHaveProperty("FIXED", "fixed");
            expect(LineHeightType).toHaveProperty("LEADING", "leading");
            expect(LineHeightType).toHaveProperty("AT_LEAST", "atLeast");
        });
    });

    // namespace LineHeight ---------------------------------------------------

    describe("namespace LineHeight", function () {
        const { LineHeight } = lineheight;

        it("should exist", function () {
            expect(LineHeight).toBeObject();
        });

        it("should contain static constants", function () {
            expect(LineHeight.NORMAL).toEqual({ type: "normal" });
            expect(LineHeight.SINGLE).toEqual({ type: "percent", value: 100 });
            expect(LineHeight._115).toEqual({ type: "percent", value: 115 });
            expect(LineHeight.ONE_HALF).toEqual({ type: "percent", value: 150 });
            expect(LineHeight.DOUBLE).toEqual({ type: "percent", value: 200 });
        });

        describe("function serialize", function () {
            it("should exist", function () {
                expect(LineHeight).toRespondTo("serialize");
            });
            it("should return strings", function () {
                expect(LineHeight.serialize({ type: "normal" })).toBe("normal");
                expect(LineHeight.serialize({ type: "percent", value: 42 })).toBe("42%");
                expect(LineHeight.serialize({ type: "fixed", value: 42 })).toBe("42hmm");
                expect(LineHeight.serialize({ type: "leading", value: 42 })).toBe("leading:42hmm");
                expect(LineHeight.serialize({ type: "atLeast", value: 42 })).toBe("atLeast:42hmm");
            });
        });

        describe("function parse", function () {
            it("should exist", function () {
                expect(LineHeight).toRespondTo("parse");
            });
            it("should parse JSON data", function () {
                expect(LineHeight.parse({ type: "normal" })).toEqual({ type: "normal" });
                expect(LineHeight.parse({ type: "normal", value: 200 })).toEqual({ type: "normal" });
                expect(LineHeight.parse({ type: "percent", value: 42 })).toEqual({ type: "percent", value: 42 });
                expect(LineHeight.parse({ type: "percent", value: "abc" })).toEqual({ type: "percent", value: 0 });
                expect(LineHeight.parse({ type: "percent" })).toEqual({ type: "percent", value: 0 });
                expect(LineHeight.parse({ type: "fixed", value: 42 })).toEqual({ type: "fixed", value: 42 });
                expect(LineHeight.parse({ type: "leading", value: 42 })).toEqual({ type: "leading", value: 42 });
                expect(LineHeight.parse({ type: "atLeast", value: 42 })).toEqual({ type: "atLeast", value: 42 });
                expect(LineHeight.parse({ type: "_invalid_", value: 42 })).toEqual({ type: "normal" });
                expect(LineHeight.parse({ type: 42, value: 42 })).toEqual({ type: "normal" });
                expect(LineHeight.parse(42)).toEqual({ type: "normal" });
            });
        });
    });
});
