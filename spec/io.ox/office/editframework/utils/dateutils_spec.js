/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as dateutils from '@/io.ox/office/editframework/utils/dateutils';

// tests ======================================================================

describe('module editframework/utils/dateutils', function () {

    // functions --------------------------------------------------------------

    describe('method getMSFormatIsoDateString', function () {
        it('should exist', function () {
            expect(dateutils.getMSFormatIsoDateString).toBeFunction();
        });
        it('should return a special date format', function () {
            expect(dateutils.getMSFormatIsoDateString()).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:00Z$/);
        });
    });

    describe('method getWeekDayFormat', function () {
        it('should exist', function () {
            expect(dateutils.getWeekDayFormat).toBeFunction();
        });
        it('should create an week-day format', function () {
            expect(dateutils.getWeekDayFormat()).toBe('dddd');
        });
    });
});
