/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { opRgbColor, opPresetColor, Color } from '@/io.ox/office/editframework/utils/color';
import module from '@/io.ox/office/editframework/utils/gradient';

// class Gradient ============================================================

describe('EditFramework module Gradient', function () {
    var
        fullDescriptor        = {
            type: 'linear',
            rotation: 66.666,
            isRotateWithShape: true,
            isScaled: true,
            colorStops: [{
                position: 0.525,
                color: opRgbColor('FFC000')
            }, {
                position: 0.875,
                color: opPresetColor('red')
            }]
        },
        feasibleDescriptor    = {
            isRotateWithShape:  true,
            color: opPresetColor('green'),
            color2: opRgbColor('CCFF00')
        },
        emptyDescriptor       = {
            isRotateWithShape:  true
        },

        mockedGradient = {
            getType() { return ''; },
            getRotation() { return 0; },
            isRotateWithShape() { return true; },
            isScaled() { return true; },
            getColorStops() { return []; }
        },
        disfunctionalGradient = {
            getRotation:        mockedGradient.getRotation,
            isRotateWithShape:  mockedGradient.isRotateWithShape,
            isScaled:           mockedGradient.isScaled,
            getColorStops:      mockedGradient.getColorStops
        },

        minimalLinearGradient       = module.create(module.LINEAR),
        emptyDescriptorGradient     = module.create(emptyDescriptor),

        fullDescriptorGradient      = module.create(fullDescriptor),
        feasibleDescriptorGradient  = module.create(feasibleDescriptor),

        expectedValueOfTheEmptyDescriptorGradient     = {
            type:               'linear',
            rotation:           0,
            isRotateWithShape:  true,
            isScaled:           false,
            colorStops:         [
                { position: 0, color: Color.AUTO },
                { position: 1, color: Color.AUTO }
            ]
        },
        expectedValueOfTheFeasibleDescriptorGradient  = JSON.parse(JSON.stringify(expectedValueOfTheEmptyDescriptorGradient)); // data structure clone

    expectedValueOfTheFeasibleDescriptorGradient.colorStops[0].color = feasibleDescriptor.color;
    expectedValueOfTheFeasibleDescriptorGradient.colorStops[1].color = feasibleDescriptor.color2;

    // constants ----------------------------------------------------------

    describe('constant LINEAR', function () {

        it('should exist', function () {
            expect(module.LINEAR).toEqual({ type: 'linear', isRotateWithShape:  true });
        });
    });

    // factory method -----------------------------------------------------

    describe('factory method "create"', function () {

        it('should exist', function () {
            expect(module).toRespondTo('create');
        });

        it('should return the NULL value when arguments are omitted', function () {
            expect(module.create()).toBeNull();
        });
        it('should return the NULL value when passing the NULL value as sole argument', function () {
            expect(module.create(null)).toBeNull();
        });
        it('should return the NULL value when passing any non "Object" object as sole argument', function () {
            expect(module.create(RegExp())).toBeNull();
            expect(module.create([])).toBeNull();
        });
        it('should create a gradient when passing at least a plain empty "Object" object as sole descriptor argument', function () {
            expect(module.isGradient(emptyDescriptorGradient)).toBeTrue();
        });
        it('should create a gradient when passing e.g. the minimal descriptor object for linear gradient types as sole argument', function () {
            expect(module.isGradient(minimalLinearGradient)).toBeTrue();
        });
    });

    // module methods -----------------------------------------------------

    describe('static method "isGradient"', function () {

        it('should exist', function () {
            expect(module).toRespondTo('isGradient');
        });

        it('should return false for every object that does not match the "Gradient" signature', function () {
            expect(module.isGradient(module.create())).toBeFalse();
            expect(module.isGradient(module.create(null))).toBeFalse();

            expect(module.isGradient(module.create(RegExp()))).toBeFalse();
            expect(module.isGradient(module.create([]))).toBeFalse();

            expect(module.isGradient(disfunctionalGradient)).toBeFalse();
        });
        it('should return true for every object that does match the "Gradient" signature', function () {
            //expect(Gradient.isGradient(mockedGradient)).toBeTrue(); // @TODO - make it pass again

            expect(module.isGradient(emptyDescriptorGradient)).toBeTrue();
            expect(module.isGradient(minimalLinearGradient)).toBeTrue();

            expect(module.isGradient(fullDescriptorGradient)).toBeTrue();
            expect(module.isGradient(feasibleDescriptorGradient)).toBeTrue();
        });
    });

    describe('static method "isEqualDescriptors"', function () {

        it('should exist', function () {
            expect(module).toRespondTo('isEqualDescriptors');
        });

        it('should return true in case of both passed arguments create each a gradient object which then again do equal each other', function () {
            expect(module.isEqualDescriptors(emptyDescriptor, emptyDescriptor)).toBeTrue();
            expect(module.isEqualDescriptors(module.LINEAR, module.LINEAR)).toBeTrue();

            expect(module.isEqualDescriptors(emptyDescriptor, module.LINEAR)).toBeTrue();

            expect(module.isEqualDescriptors(emptyDescriptor, fullDescriptor)).toBeFalse();
            expect(module.isEqualDescriptors(emptyDescriptor, feasibleDescriptor)).toBeFalse();
            expect(module.isEqualDescriptors(feasibleDescriptor, fullDescriptor)).toBeFalse();
        });
    });

    // instance methods ---------------------------------------------------

    describe('method equals', function () {

        it('should exist', function () {
            expect(emptyDescriptorGradient).toRespondTo('equals');
            expect(minimalLinearGradient).toRespondTo('equals');
        });

        it('should return true for equal gradient types', function () {
            expect(minimalLinearGradient.equals(emptyDescriptorGradient)).toBeTrue();
            expect(emptyDescriptorGradient.equals(minimalLinearGradient)).toBeTrue();

            expect(fullDescriptorGradient.equals(minimalLinearGradient)).toBeFalse();
            expect(feasibleDescriptorGradient.equals(minimalLinearGradient)).toBeFalse();
            expect(feasibleDescriptorGradient.equals(fullDescriptorGradient)).toBeFalse();

            // same reference comparison
            expect(fullDescriptorGradient.equals(fullDescriptorGradient)).toBeTrue();
        });
    });

    describe('method clone', function () {

        it('should exist', function () {
            expect(emptyDescriptorGradient).toRespondTo('equals');
            expect(minimalLinearGradient).toRespondTo('equals');
        });

        it('should return a clone that is not just a reference to the operated gradient', function () {
            expect(minimalLinearGradient.clone()).not.toBe(minimalLinearGradient);
        });
        it('should return a clone that is a gradient type itself which equals the operated gradient type', function () {
            expect(module.isGradient(minimalLinearGradient.clone())).toBeTrue();
            expect(minimalLinearGradient.clone().equals(minimalLinearGradient)).toBeTrue();

            expect(module.isGradient(fullDescriptorGradient.clone())).toBeTrue();
            expect(fullDescriptorGradient.clone().equals(fullDescriptorGradient)).toBeTrue();

            expect(module.isGradient(feasibleDescriptorGradient.clone())).toBeTrue();
            expect(feasibleDescriptorGradient.clone().equals(feasibleDescriptorGradient)).toBeTrue();

            expect(fullDescriptorGradient.clone().equals(emptyDescriptorGradient)).toBeFalse();
            expect(feasibleDescriptorGradient.clone().equals(emptyDescriptorGradient)).toBeFalse();

            expect(fullDescriptorGradient.clone().equals(feasibleDescriptorGradient)).toBeFalse();
            expect(feasibleDescriptorGradient.clone().equals(fullDescriptorGradient)).toBeFalse();
        });
    });

    describe('method valueOf', function () {

        it('should exist', function () {
            expect(emptyDescriptorGradient).toRespondTo('valueOf');
            expect(feasibleDescriptorGradient).toRespondTo('valueOf');

            expect(fullDescriptorGradient).toRespondTo('valueOf');
        });

        it('should return a complete standard descriptor representation of its operated gradient type.', function () {
            // complete data transformation round-trip but mocked.
            expect(emptyDescriptorGradient.valueOf()).toEqual(expectedValueOfTheEmptyDescriptorGradient);
            expect(feasibleDescriptorGradient.valueOf()).toEqual(expectedValueOfTheFeasibleDescriptorGradient);

            // complete data transformation round-trip and real.
            //expect(fullDescriptorGradient.valueOf()).toEqual(fullDescriptor);
        });
    });

    describe('method toString', function () {

        it('should exist', function () {
            expect(emptyDescriptorGradient).toRespondTo('toString');
            expect(feasibleDescriptorGradient).toRespondTo('toString');

            expect(fullDescriptorGradient).toRespondTo('toString');
        });

        it.skip('should return the JSON-style stringified version of a complete standard descriptor representation of its operated gradient type.', function () {
            var
                //stringifiedEmptyDescriptor,   = '{"type":"linear","rotation":0,"isRotateWithShape":false,"isScaled":false,"colorStops":[{"position":0,"color":{"type":"auto"}},{"position":1,"color":{"type":"auto"}}]}',
                //stringifiedFeasibleDescriptor = '{"type":"linear","rotation":0,"isRotateWithShape":false,"isScaled":false,"colorStops":[{"position":0,"color":{"type":"preset","value":"6"}},{"position":1,"color":{"type":"rgb","value":"ccff00"}}]}',
                //stringifiedFullDescriptor,    = '{"type":"linear","rotation":66.666,"isRotateWithShape":true,"isScaled":true,"colorStops":[{"position":0.525,"color":{"type":"rgb","value":"ffc000"}},{"position":0.875,"color":{"type":"preset","value":4}}]}',

                stringifiedEmptyDescriptor    = JSON.stringify(expectedValueOfTheEmptyDescriptorGradient),
                stringifiedFeasibleDescriptor = JSON.stringify(expectedValueOfTheFeasibleDescriptorGradient);

            //stringifiedFullDescriptor     = JSON.stringify(fullDescriptor);

            // complete data transformation round-trip but mocked.
            expect(emptyDescriptorGradient.toString()).toEqual(stringifiedEmptyDescriptor);
            expect(feasibleDescriptorGradient.toString()).toEqual(stringifiedFeasibleDescriptor);

            // complete data transformation round-trip and real.
            //expect(fullDescriptorGradient.toString()).toEqual(stringifiedFullDescriptor);
        });
    });
});
