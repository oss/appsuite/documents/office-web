/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { createPresentationApp } from '~/presentation/apphelper';
import { Canvas } from '@/io.ox/office/tk/canvas';
import * as Texture from '@/io.ox/office/editframework/utils/texture';

// private functions ==========================================================

// converts the passed byte array to a 32-bit array
function to32(a) {
    var result = [];
    for (var i = 0; i < a.length; i += 4) {
        result.push(0x1000000 * a[i] + 0x10000 * a[i + 1] + 0x100 * a[i + 2] + a[i + 3]);
    }
    return result;
}

// tests ======================================================================

describe('module editframework/utils/texture', function () {

    var docModel;
    createPresentationApp('ooxml').done(function (docApp) {
        docModel = docApp.docModel;
    });

    var dummyImageUrl = null;
    var canvas = new Canvas({ location: { width: 250, height: 250 } });

    // for DEBUG
    //$('body').append(canvas.$el.css({ zIndex: 99, position: 'fixed', left: '500px' }));

    // ES6-LATER: better mocking for canvas code
    describe.skip('Texture.getTextureFill()', function () {

        describe('as bitmap', function () {

            beforeAll(async function () {
                var helpCanvas = document.createElement('canvas');
                helpCanvas.width = helpCanvas.height = 100;
                var helpCtx = helpCanvas.getContext('2d');

                helpCtx.fillStyle = '#0000ff';
                helpCtx.fillRect(0, 0, 50, 50);
                helpCtx.fillRect(50, 50, 100, 100);

                dummyImageUrl = helpCanvas.toDataURL();
                var fillAttrs = { bitmap: { tiling: { rectAlignment: 'topLeft', flipMode: 'none' }, imageUrl: dummyImageUrl } };

                await canvas.render(async context => {
                    const textureFill = await Texture.getTextureFill(docModel, context, fillAttrs, 250, 250, null, { isRotateWithShape: true });
                    context.setFillStyle(textureFill).drawRect(0, 0, 250, 250, 'fill');
                });
            });

            it('should return correct texture', function () {
                expect(to32(canvas.getImageData(0, 0, 5, 5).data)).toEqual([
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff
                ]);
                expect(to32(canvas.getImageData(51, 0, 5, 5).data)).toEqual([
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000
                ]);
                expect(to32(canvas.getImageData(51, 51, 5, 5).data)).toEqual([
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff
                ]);
                expect(to32(canvas.getImageData(0, 51, 5, 5).data)).toEqual([
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000
                ]);
                expect(to32(canvas.getImageData(100, 100, 5, 5).data)).toEqual([
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff
                ]);
                expect(to32(canvas.getImageData(51, 0, 5, 5).data)).toEqual([
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000
                ]);
                expect(to32(canvas.getImageData(151, 151, 5, 5).data)).toEqual([
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff
                ]);
                expect(to32(canvas.getImageData(100, 151, 5, 5).data)).toEqual([
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000
                ]);
            });
        });

        describe('as image', function () {

            beforeAll(async function () {
                var helpCanvas = document.createElement('canvas');
                helpCanvas.width = helpCanvas.height = 100;
                var helpCtx = helpCanvas.getContext('2d');

                helpCtx.fillStyle = '#0000ff';
                helpCtx.fillRect(0, 0, 50, 50);
                helpCtx.fillRect(50, 50, 100, 100);

                dummyImageUrl = helpCanvas.toDataURL();
                var fillAttrs = { bitmap: { imageUrl: dummyImageUrl } };

                await canvas.render(async context => {
                    const textureFill = await Texture.getTextureFill(docModel, context, fillAttrs, 200, 200, null, { isRotateWithShape: true });
                    context.clearRect(0, 0, 250, 250);
                    context.setFillStyle(textureFill).drawRect(0, 0, 200, 200, 'fill');
                });
            });

            it('should return correct image', function () {
                expect(to32(canvas.getImageData(0, 0, 5, 5).data)).toEqual([
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff
                ]);
                expect(to32(canvas.getImageData(51, 51, 5, 5).data)).toEqual([
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff
                ]);
                expect(to32(canvas.getImageData(101, 0, 5, 5).data)).toEqual([
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000
                ]);
                expect(to32(canvas.getImageData(0, 101, 5, 5).data)).toEqual([
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000,
                    0x0000000, 0x0000000, 0x0000000, 0x0000000, 0x0000000
                ]);
                expect(to32(canvas.getImageData(101, 101, 5, 5).data)).toEqual([
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff
                ]);
                expect(to32(canvas.getImageData(151, 151, 5, 5).data)).toEqual([
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff,
                    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff
                ]);
            });
        });
    });
});
