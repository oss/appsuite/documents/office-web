/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as drawingops from "@/io.ox/office/drawinglayer/utils/operations";
import * as operations from "@/io.ox/office/editframework/utils/operations";

// tests ======================================================================

describe("module editframework/utils/operations", function () {

    it("re-exports should exist", function () {
        expect(operations).toContainProps(drawingops);
    });

    it("constants should exist", () => {
        expect(operations.NOOP).toBe("noOp");
        expect(operations.CHANGE_CONFIG).toBe("setDocumentAttributes");
        expect(operations.INSERT_THEME).toBe("insertTheme");
        expect(operations.INSERT_FONT).toBe("insertFontDescription");
        expect(operations.INSERT_STYLESHEET).toBe("insertStyleSheet");
        expect(operations.DELETE_STYLESHEET).toBe("deleteStyleSheet");
        expect(operations.CHANGE_STYLESHEET).toBe("changeStyleSheet");
        expect(operations.INSERT_AUTOSTYLE).toBe("insertAutoStyle");
        expect(operations.CHANGE_AUTOSTYLE).toBe("changeAutoStyle");
        expect(operations.DELETE_AUTOSTYLE).toBe("deleteAutoStyle");
        expect(operations.POSITION).toBe("position");
    });

    describe("function isPosition", function () {
        const { isPosition } = operations;
        it("should exist", function () {
            expect(isPosition).toBeFunction();
        });
        it("should return true for valid positions", function () {
            expect(isPosition([0])).toBeTrue();
            expect(isPosition([1, 2, 3, 4])).toBeTrue();
        });
        it("should return false for invalid positions", function () {
            expect(isPosition([])).toBeFalse();
            expect(isPosition([-1])).toBeFalse();
            expect(isPosition([0, 1, 2, -1])).toBeFalse();
            expect(isPosition([1.5])).toBeFalse();
            expect(isPosition([0, 1, 2, 1.5])).toBeFalse();
            expect(isPosition(["a"])).toBeFalse();
            expect(isPosition([0, 1, 2, "a"])).toBeFalse();
            expect(isPosition({})).toBeFalse();
            expect(isPosition(1)).toBeFalse();
            expect(isPosition("a")).toBeFalse();
        });
    });

    describe("function clonePosition", function () {
        const { clonePosition } = operations;
        it("should exist", function () {
            expect(clonePosition).toBeFunction();
        });
        it("should return a clone of the position", function () {
            const pos1 = [1, 2, 3];
            const pos2 = clonePosition(pos1);
            expect(pos1).not.toBe(pos2);
            expect(pos1).toEqual(pos2);
        });
    });

    describe("function clonePositions", function () {
        const { clonePositions } = operations;
        it("should exist", function () {
            expect(clonePositions).toBeFunction();
        });
        it("should return a clone of the position", function () {
            const pos1 = [[1, 2, 3], [4, 5]];
            const pos2 = clonePositions(pos1);
            expect(pos1).not.toBe(pos2);
            expect(pos1).toEqual(pos2);
        });
    });

    describe("function equalPositions", function () {
        const { equalPositions } = operations;
        it("should exist", function () {
            expect(equalPositions).toBeFunction();
        });
        it("should recognize equal positions", function () {
            expect(equalPositions([1], [1])).toBeTrue();
            expect(equalPositions([1, 2, 3, 4], [1, 2, 3, 4])).toBeTrue();
        });
        it("should recognize different positions", function () {
            expect(equalPositions([1], [2])).toBeFalse();
            expect(equalPositions([2], [1])).toBeFalse();
            expect(equalPositions([1, 2, 3, 4], [1, 2, 4, 3])).toBeFalse();
            expect(equalPositions([1, 2, 4, 3], [1, 2, 3, 4])).toBeFalse();
        });
        it("should compare positions of different length", function () {
            expect(equalPositions([1, 2, 3], [1, 2, 3, 4])).toBeFalse();
            expect(equalPositions([1, 2, 3, 4], [1, 2, 3])).toBeFalse();
        });
        it("should use specified maximum length", function () {
            expect(equalPositions([1, 2, 3], [1, 2, 3, 4], 3)).toBeTrue();
            expect(equalPositions([1, 2, 3, 4], [1, 2, 4, 3], 2)).toBeTrue();
            expect(equalPositions([1, 2, 3, 4], [2, 1, 3, 4], 0)).toBeTrue();
        });
    });

    describe("function comparePositions", function () {
        const { comparePositions } = operations;
        it("should exist", function () {
            expect(comparePositions).toBeFunction();
        });
        it("should recognize equal arrays", function () {
            expect(comparePositions([1], [1])).toBe(0);
            expect(comparePositions([1, 2, 3, 4], [1, 2, 3, 4])).toBe(0);
        });
        it("should recognize different arrays", function () {
            expect(comparePositions([1], [2])).toBe(-1);
            expect(comparePositions([2], [1])).toBe(1);
            expect(comparePositions([1, 2, 3, 4], [1, 2, 4, 3])).toBe(-1);
            expect(comparePositions([1, 2, 4, 3], [1, 2, 3, 4])).toBe(1);
        });
        it("should compare arrays of different size", function () {
            expect(comparePositions([1, 2, 3], [1, 2, 3, 4])).toBe(-1);
            expect(comparePositions([1, 2, 3, 4], [1, 2, 3])).toBe(1);
        });
        it("should use specified maximum length", function () {
            expect(comparePositions([1, 2, 3], [1, 2, 3, 4], 3)).toBe(0);
            expect(comparePositions([1, 2, 3, 4], [1, 2, 4, 3], 2)).toBe(0);
            expect(comparePositions([1, 2, 3, 4], [2, 1, 3, 4], 0)).toBe(0);
        });
    });

    describe("function makeOp", function () {
        const { makeOp } = operations;
        it("should exist", function () {
            expect(makeOp).toBeFunction();
        });
        it("should create a JSON operation", function () {
            expect(makeOp("noOp")).toEqual({ name: "noOp" });
            expect(makeOp("noOp", { start: [1], flag: true })).toEqual({ name: "noOp", start: [1], flag: true });
        });
    });
});
