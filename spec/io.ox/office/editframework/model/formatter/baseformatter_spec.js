/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import moment from "$/moment";

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { EObject } from "@/io.ox/office/tk/objects";
import { SEC_PER_DAY, MSEC_PER_DAY } from "@/io.ox/office/tk/utils/dateutils";
import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";
import { generateCurrencyCode } from "@/io.ox/office/editframework/model/formatter/currencycodes";
import { ParsedFormat, BaseFormatter, FormatCategory, OP_STD_TOKEN } from "@/io.ox/office/editframework/model/formatter/baseformatter";

const OOX = FileFormatType.OOX;
const ODF = FileFormatType.ODF;

// constants ==================================================================

const CSS_COLORS = {
    black: "#000000",
    blue: "#0000FF",
    green: "#00FF00",
    cyan: "#00FFFF",
    red: "#FF0000",
    magenta: "#FF00FF",
    yellow: "#FFFF00",
    white: "#FFFFFF"
};

const SHORT_DATE = LOCALE_DATA.shortDate;
const SHORT_TIME = LOCALE_DATA.shortTime;
const LONG_TIME = LOCALE_DATA.longTime;

// private functions ==========================================================

function expectCategory(parsedFormat, expCategory) {
    expect(parsedFormat).toHaveProperty("category", expCategory);
}

function expectResult(result, descr, expected) {
    const text = Array.isArray(expected) ? expected[0] : expected;
    const key = Array.isArray(expected) ? expected[1] : undefined;
    expect(result, descr).toHaveProperty("text", text);
    if (key) {
        expect(result, descr).toHaveProperty("color");
        expect(result.color, descr).toHaveProperty("key", key);
        expect(result.color, descr).toHaveProperty("css", CSS_COLORS[key]);
    } else {
        expect(result, descr).not.toHaveProperty("color");
    }
}

// class FormatTester =========================================================

function FormatTester() {
    this.ooxFormatter = new BaseFormatter(OOX);
    this.odfFormatter = new BaseFormatter(ODF);
}

FormatTester.prototype.test = function (formatCode, testValue, expValueOox, expValueOdf) {
    const descr = `formatCode="${formatCode}" value=${testValue}`;
    if (expValueOdf === undefined) { expValueOdf = expValueOox; }
    expectResult(this.ooxFormatter.formatValue(formatCode, testValue), `fileFormat=OOX ${descr}`, expValueOox);
    expectResult(this.odfFormatter.formatValue(formatCode, testValue), `fileFormat=ODF ${descr}`, expValueOdf);
};

// class SeriesTester =========================================================

function SeriesTester(...testValues) {
    this.formatter = new BaseFormatter(OOX);
    this.testValues = testValues;
}

SeriesTester.prototype.test = function (formatCode, ...expValues) {
    const parsedFormat = this.formatter.getParsedFormat(formatCode);
    const descr = `formatCode="${formatCode}"`;
    for (const [i, v] of this.testValues.entries()) {
        expectResult(this.formatter.formatValue(parsedFormat, v), `${descr} value=${v}`, expValues[i]);
    }
};

// tests ======================================================================

describe("module editframework/model/formatter/baseformatter", () => {

    // types ------------------------------------------------------------------

    describe("enum FormatCategory", () => {
        it("should exist", () => {
            expect(FormatCategory).toBeObject();
        });
        it("should contain category entries", () => {
            expect(FormatCategory.STANDARD).toBeString();
            expect(FormatCategory.NUMBER).toBeString();
            expect(FormatCategory.SCIENTIFIC).toBeString();
            expect(FormatCategory.PERCENT).toBeString();
            expect(FormatCategory.FRACTION).toBeString();
            expect(FormatCategory.CURRENCY).toBeString();
            expect(FormatCategory.ACCOUNTING).toBeString();
            expect(FormatCategory.DATE).toBeString();
            expect(FormatCategory.TIME).toBeString();
            expect(FormatCategory.DATETIME).toBeString();
            expect(FormatCategory.TEXT).toBeString();
            expect(FormatCategory.CUSTOM).toBeString();
            expect(FormatCategory.ERROR).toBeString();
        });
    });

    // constants --------------------------------------------------------------

    describe("constant OP_STD_TOKEN", () => {
        it("should exist", () => {
            expect(OP_STD_TOKEN).toBe("General");
        });
    });

    // class ParsedFormat -----------------------------------------------------

    describe("class ParsedFormat", () => {
        it("should exist", () => {
            expect(ParsedFormat).toBeClass();
        });
    });

    // class BaseFormatter ----------------------------------------------------

    describe("class BaseFormatter", () => {

        it("should subclass EObject", () => {
            expect(BaseFormatter).toBeSubClassOf(EObject);
        });

        describe("method configure", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("configure");
            });
        });

        describe("method isValidDate", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("isValidDate");
            });
        });

        describe("method convertNumberToDate", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("convertNumberToDate");
            });
        });

        describe("method convertDateToNumber", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("convertDateToNumber");
            });
        });

        describe("method convertNowToNumber", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("convertNowToNumber");
            });
        });

        describe("method autoFormatNumber", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("autoFormatNumber");
            });
        });

        describe("method getParsedFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getParsedFormat");
            });

            const formatterOOX = new BaseFormatter(OOX);
            const formatterODF = new BaseFormatter(ODF);

            it("should return ParsedFormat instances", () => {
                const parsedFormat = formatterOOX.getParsedFormat("0");
                expect(parsedFormat).toBeInstanceOf(ParsedFormat);
                expect(formatterOOX.getParsedFormat(parsedFormat)).toBe(parsedFormat);
            });

            it("should cache the parsed format codes", () => {
                const parsedFormatOP = formatterOOX.getParsedFormat("0");
                const parsedFormatUI = formatterOOX.getParsedFormat("0", { uiGrammar: true });
                expect(parsedFormatOP).not.toBe(parsedFormatUI);
                expect(formatterOOX.getParsedFormat("0")).toBe(parsedFormatOP);
                expect(formatterOOX.getParsedFormat("0", { uiGrammar: true })).toBe(parsedFormatUI);
                expect(formatterOOX.getParsedFormat("00")).not.toBe(parsedFormatOP);
            });

            describe("should recognize", () => {
                it("STANDARD category", () => {
                    expectCategory(formatterOOX.getParsedFormat("General"), FormatCategory.STANDARD);
                    expectCategory(formatterOOX.getParsedFormat("Standard", { uiGrammar: true }), FormatCategory.STANDARD);
                    expectCategory(formatterODF.getParsedFormat("General"), FormatCategory.STANDARD);
                    expectCategory(formatterODF.getParsedFormat("Standard", { uiGrammar: true }), FormatCategory.STANDARD);
                    // bug 46211: empty format code falls back to standard format
                    expectCategory(formatterOOX.getParsedFormat(""), FormatCategory.STANDARD);
                    expectCategory(formatterOOX.getParsedFormat("", { uiGrammar: true }), FormatCategory.STANDARD);
                    expectCategory(formatterODF.getParsedFormat(""), FormatCategory.STANDARD);
                    expectCategory(formatterODF.getParsedFormat("", { uiGrammar: true }), FormatCategory.STANDARD);
                });
                it("NUMBER category", () => {
                    expectCategory(formatterOOX.getParsedFormat("0"), FormatCategory.NUMBER);
                    expectCategory(formatterOOX.getParsedFormat("0.00"), FormatCategory.NUMBER);
                    expectCategory(formatterOOX.getParsedFormat("#,##0"), FormatCategory.NUMBER);
                    expectCategory(formatterOOX.getParsedFormat("#,##0.00"), FormatCategory.NUMBER);
                    expectCategory(formatterOOX.getParsedFormat("#,##0.00;[Red]-#,##0.00"), FormatCategory.NUMBER);
                    expectCategory(formatterOOX.getParsedFormat("0;@"), FormatCategory.NUMBER);
                });
                it("SCIENTIFIC category", () => {
                    expectCategory(formatterOOX.getParsedFormat("0E+00"), FormatCategory.SCIENTIFIC);
                    expectCategory(formatterOOX.getParsedFormat("0.00E+00"), FormatCategory.SCIENTIFIC);
                    expectCategory(formatterOOX.getParsedFormat("0.00E-00"), FormatCategory.SCIENTIFIC);
                    expectCategory(formatterOOX.getParsedFormat("#,##0E+00"), FormatCategory.SCIENTIFIC);
                    expectCategory(formatterOOX.getParsedFormat("#,##0.00E+00"), FormatCategory.SCIENTIFIC);
                    expectCategory(formatterOOX.getParsedFormat("#,##0.00E+00;[Red]-#,##0.00E+00"), FormatCategory.SCIENTIFIC);
                    expectCategory(formatterOOX.getParsedFormat("0E+00;@"), FormatCategory.SCIENTIFIC);
                });
                it("PERCENT category", () => {
                    expectCategory(formatterOOX.getParsedFormat("0%"), FormatCategory.PERCENT);
                    expectCategory(formatterOOX.getParsedFormat("0.00%"), FormatCategory.PERCENT);
                    expectCategory(formatterOOX.getParsedFormat("0.00%;[Red]-0.00%"), FormatCategory.PERCENT);
                    expectCategory(formatterOOX.getParsedFormat("0%;@"), FormatCategory.PERCENT);
                });
                it("FRACTION category", () => {
                    expectCategory(formatterOOX.getParsedFormat("# ?/?"), FormatCategory.FRACTION);
                    expectCategory(formatterOOX.getParsedFormat("# ??/??"), FormatCategory.FRACTION);
                    expectCategory(formatterOOX.getParsedFormat("# ??/10"), FormatCategory.FRACTION);
                    expectCategory(formatterOOX.getParsedFormat("# ??/16"), FormatCategory.FRACTION);
                });
                it("CURRENCY category", () => {
                    expectCategory(formatterOOX.getParsedFormat("$#,##0.00"), FormatCategory.CURRENCY);
                    expectCategory(formatterOOX.getParsedFormat("#,##0.00 \u20ac"), FormatCategory.CURRENCY);
                    expectCategory(formatterOOX.getParsedFormat("[$$]#,##0.00"), FormatCategory.CURRENCY);
                    expectCategory(formatterOOX.getParsedFormat("[$$-]#,##0.00"), FormatCategory.CURRENCY);
                    expectCategory(formatterOOX.getParsedFormat("[$$-409]#,##0.00"), FormatCategory.CURRENCY);
                    expectCategory(formatterOOX.getParsedFormat("#,##0.00 [$\u20ac]"), FormatCategory.CURRENCY);
                    expectCategory(formatterOOX.getParsedFormat("#,##0.00 [$\u20ac-]"), FormatCategory.CURRENCY);
                    expectCategory(formatterOOX.getParsedFormat("#,##0.00 [$\u20ac-407]"), FormatCategory.CURRENCY);
                });
                it("ACCOUNTING category", () => {
                    expectCategory(formatterOOX.getParsedFormat("_-* $#,##0.00_-"), FormatCategory.ACCOUNTING);
                    expectCategory(formatterOOX.getParsedFormat("_-* $#,##0.00_-;-* $#,##0.00_-"), FormatCategory.ACCOUNTING);
                });
                it("DATE category", () => {
                    expectCategory(formatterOOX.getParsedFormat("MM/DD/YYYY"), FormatCategory.DATE);
                    expectCategory(formatterOOX.getParsedFormat("D. MMM. YYYY"), FormatCategory.DATE);
                    expectCategory(formatterOOX.getParsedFormat("MM"), FormatCategory.DATE);
                    expectCategory(formatterOOX.getParsedFormat("MMM"), FormatCategory.DATE);
                    expectCategory(formatterOOX.getParsedFormat("mm"), FormatCategory.DATE);
                    expectCategory(formatterOOX.getParsedFormat("mmm"), FormatCategory.DATE);
                });
                it("TIME category", () => {
                    expectCategory(formatterOOX.getParsedFormat("hh:mm:ss"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("hh:mm:ss AM/PM"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("hh:mm:ss a/p"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("[hh]:mm:ss"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("mm:ss.00"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("hh:MM"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("MM:ss"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("hh:MM:ss"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("mm:ss"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("hh:mm"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("MM:ss"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("hh:MM"), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("hh/mmm", { uiGrammar: true }), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("mmm/ss", { uiGrammar: true }), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("mm", { uiGrammar: true }), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("mmm", { uiGrammar: true }), FormatCategory.TIME);
                    expectCategory(formatterOOX.getParsedFormat("A/P"), FormatCategory.TIME);
                });
                it("DATETIME category", () => {
                    expectCategory(formatterOOX.getParsedFormat("MM/DD/YYYY hh:mm:ss"), FormatCategory.DATETIME);
                    expectCategory(formatterOOX.getParsedFormat("DD. MMM. hh:mm AM/PM"), FormatCategory.DATETIME);
                    expectCategory(formatterOOX.getParsedFormat("hh/MMM"), FormatCategory.DATETIME);
                    expectCategory(formatterOOX.getParsedFormat("MMM/ss"), FormatCategory.DATETIME);
                    expectCategory(formatterOOX.getParsedFormat("hh/mmm"), FormatCategory.DATETIME);
                    expectCategory(formatterOOX.getParsedFormat("mmm/ss"), FormatCategory.DATETIME);
                    expectCategory(formatterOOX.getParsedFormat("DD. AM/PM"), FormatCategory.DATETIME);
                });
                it("TEXT category", () => {
                    expectCategory(formatterOOX.getParsedFormat("@"), FormatCategory.TEXT);
                    expectCategory(formatterOOX.getParsedFormat("[Red]@"), FormatCategory.TEXT);
                });
                it("CUSTOM category", () => {
                    expectCategory(formatterOOX.getParsedFormat("-General"), FormatCategory.CUSTOM);
                    expectCategory(formatterOOX.getParsedFormat("General;-General"), FormatCategory.CUSTOM);
                    expectCategory(formatterOOX.getParsedFormat("General;-0"), FormatCategory.CUSTOM);
                    expectCategory(formatterOOX.getParsedFormat("0;0E+0"), FormatCategory.CUSTOM);
                    expectCategory(formatterOOX.getParsedFormat("0;0%"), FormatCategory.CUSTOM);
                    expectCategory(formatterOOX.getParsedFormat("0E+0%"), FormatCategory.CUSTOM);
                    expectCategory(formatterOOX.getParsedFormat("$0%"), FormatCategory.CUSTOM);
                    expectCategory(formatterOOX.getParsedFormat(";"), FormatCategory.CUSTOM);
                });
            });

            it("should parse format codes in UI grammar", () => {
                expectCategory(formatterOOX.getParsedFormat("YYYY-DD"), FormatCategory.DATE);
                expectCategory(formatterOOX.getParsedFormat("JJJJ-TT"), FormatCategory.CUSTOM);
                expectCategory(formatterOOX.getParsedFormat("YYYY-DD", { uiGrammar: true }), FormatCategory.CUSTOM);
                expectCategory(formatterOOX.getParsedFormat("JJJJ-TT", { uiGrammar: true }), FormatCategory.DATE);
            });
            it("should parse special ODF date tokens", () => {
                expectCategory(formatterOOX.getParsedFormat("QQ"), FormatCategory.CUSTOM);
                expectCategory(formatterOOX.getParsedFormat("WW"), FormatCategory.CUSTOM);
                expectCategory(formatterOOX.getParsedFormat("NN"), FormatCategory.ERROR);
                expectCategory(formatterODF.getParsedFormat("QQ"), FormatCategory.DATE);
                expectCategory(formatterODF.getParsedFormat("WW"), FormatCategory.DATE);
                expectCategory(formatterODF.getParsedFormat("NN"), FormatCategory.DATE);
            });

            it("should fail to parse illegal codes", () => {
                expectCategory(formatterOOX.getParsedFormat("General General"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("General 0"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("General ."), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("General E+0"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("General /"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("General Y"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("General s"), FormatCategory.ERROR);

                expectCategory(formatterOOX.getParsedFormat("0 E+0 E+0"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("E+0"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("0 E+"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("0 E +0"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("0 E+0 /"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("0 E+0 Y"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("0 E+0 s"), FormatCategory.ERROR);

                expectCategory(formatterOOX.getParsedFormat("0 / 0 / 0"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("/ 0"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("0 /"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("0.0 / 0"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("0 E+0 / 0"), FormatCategory.ERROR);

                expectCategory(formatterOOX.getParsedFormat("Y 0"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("Y ,#"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("Y E+"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("Y %"), FormatCategory.ERROR);

                expectCategory(formatterOOX.getParsedFormat("@ General"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("@ 0"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("@ ."), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("@ E+0"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("@ %"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("@ /"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("@ Y"), FormatCategory.ERROR);
                expectCategory(formatterOOX.getParsedFormat("@ s"), FormatCategory.ERROR);
            });
        });

        describe("method getDecimalFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getDecimalFormat");
            });
        });

        describe("method getCurrencyFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getCurrencyFormat");
            });
        });

        describe("method getPercentFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getPercentFormat");
            });
        });

        describe("method getScientificFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getScientificFormat");
            });
        });

        describe("method getFractionFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getFractionFormat");
            });
        });

        describe("method getSystemDateFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getSystemDateFormat");
            });
        });

        describe("method getSystemTimeFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getSystemTimeFormat");
            });
        });

        describe("method getSystemDateTimeFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getSystemDateTimeFormat");
            });
        });

        describe("method getFixedDateFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getFixedDateFormat");
            });
        });

        describe("method getTimeFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getTimeFormat");
            });
        });

        describe("method getFixedDateTimeFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getFixedDateTimeFormat");
            });
        });

        describe("method getCategoryFormat", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getCategoryFormat");
            });
        });

        describe("method getInitialFormatCodes", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("getInitialFormatCodes");
            });
        });

        describe("method resolveFormatId", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("resolveFormatId");
            });
        });

        describe("method formatValue", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("formatValue");
            });

            describe("should format numbers", () => {
                it("with digit code '0'", () => {
                    const tester = new SeriesTester(0, 1.234567, 12.34567,  123.4567,   1234.567,   12345.67,    123456.7,     1234567);
                    tester.test("-0-",     "-0-",     "-1-",     "-12-",    "-123-",    "-1235-",   "-12346-",   "-123457-",   "-1234567-");
                    tester.test("-00-",    "-00-",    "-01-",    "-12-",    "-123-",    "-1235-",   "-12346-",   "-123457-",   "-1234567-");
                    tester.test("-000-",   "-000-",   "-001-",   "-012-",   "-123-",    "-1235-",   "-12346-",   "-123457-",   "-1234567-");
                    tester.test("-0000-",  "-0000-",  "-0001-",  "-0012-",  "-0123-",   "-1235-",   "-12346-",   "-123457-",   "-1234567-");
                    tester.test("-00000-", "-00000-", "-00001-", "-00012-", "-00123-",  "-01235-",  "-12346-",   "-123457-",   "-1234567-");
                    tester.test("-0+0~0-", "-0+0~0-", "-0+0~1-", "-0+1~2-", "-1+2~3-",  "-12+3~5-", "-123+4~6-", "-1234+5~7-", "-12345+6~7-");
                    tester.test("-08090-", "-08090-", "-08091-", "-08192-", "-18293-",  "-128395-", "-1238496-", "-12348597-", "-123458697-");
                    tester.test("-8090-",  "-8090-",  "-8091-",  "-8192-",  "-81293-",  "-812395-", "-8123496-", "-81234597-", "-812345697-");
                });
                it("with decimal separator", () => {
                    const tester = new SeriesTester(0,    1,           0.1,         0.12,        0.123,       0.00567,     1.234,       1.987);
                    tester.test("-0.-",      "-0,-",      "-1,-",      "-0,-",      "-0,-",      "-0,-",      "-0,-",      "-1,-",      "-2,-");
                    tester.test("-0.0-",     "-0,0-",     "-1,0-",     "-0,1-",     "-0,1-",     "-0,1-",     "-0,0-",     "-1,2-",     "-2,0-");
                    tester.test("-0.00-",    "-0,00-",    "-1,00-",    "-0,10-",    "-0,12-",    "-0,12-",    "-0,01-",    "-1,23-",    "-1,99-");
                    tester.test("-0.000-",   "-0,000-",   "-1,000-",   "-0,100-",   "-0,120-",   "-0,123-",   "-0,006-",   "-1,234-",   "-1,987-");
                    tester.test("-0.0000-",  "-0,0000-",  "-1,0000-",  "-0,1000-",  "-0,1200-",  "-0,1230-",  "-0,0057-",  "-1,2340-",  "-1,9870-");
                    tester.test("-0+.~0+0-", "-0+,~0+0-", "-1+,~0+0-", "-0+,~1+0-", "-0+,~1+2-", "-0+,~1+2-", "-0+,~0+1-", "-1+,~2+3-", "-1+,~9+9-");
                    tester.test("-04.5060-", "-04,5060-", "-14,5060-", "-04,5160-", "-04,5162-", "-04,5162-", "-04,5061-", "-14,5263-", "-14,5969-");
                    tester.test("-0.0.0.0-", "-0,0,0,0-", "-1,0,0,0-", "-0,1,0,0-", "-0,1,2,0-", "-0,1,2,3-", "-0,0,0,6-", "-1,2,3,4-", "-1,9,8,7-");
                });
                it("with group separator", () => {
                    const tester = new SeriesTester(0,         1.234567,    12.34567,    123.4567,    1234.567,     12345.67,      123456.7,       1234567);
                    tester.test("-0,0-",          "-00-",      "-01-",      "-12-",      "-123-",     "-1.235-",    "-12.346-",    "-123.457-",    "-1.234.567-");
                    tester.test("-0,00-",         "-000-",     "-001-",     "-012-",     "-123-",     "-1.235-",    "-12.346-",    "-123.457-",    "-1.234.567-");
                    tester.test("-0,000-",        "-0.000-",   "-0.001-",   "-0.012-",   "-0.123-",   "-1.235-",    "-12.346-",    "-123.457-",    "-1.234.567-");
                    tester.test("-0,000.0-",      "-0.000,0-", "-0.001,2-", "-0.012,3-", "-0.123,5-", "-1.234,6-",  "-12.345,7-",  "-123.456,7-",  "-1.234.567,0-");
                    tester.test("-0,0,,0-",       "-000-",     "-001-",     "-012-",     "-123-",     "-1.235-",    "-12.346-",    "-123.457-",    "-1.234.567-");
                    tester.test("-0,0.-",         "-00,-",     "-01,-",     "-12,-",     "-123,-",    "-1.235,-",   "-12.346,-",   "-123.457,-",   "-1.234.567,-");
                    tester.test("-0,0.0-",        "-00,0-",    "-01,2-",    "-12,3-",    "-123,5-",   "-1.234,6-",  "-12.345,7-",  "-123.456,7-",  "-1.234.567,0-");
                    tester.test("-0,0.0,0-",      "-00,00-",   "-01,23-",   "-12,35-",   "-123,46-",  "-1.234,57-", "-12.345,67-", "-123.456,70-", "-1.234.567,00-");
                    tester.test("-0,-",           "-0-",       "-0-",       "-0-",       "-0-",       "-1-",        "-12-",        "-123-",        "-1235-");
                    tester.test("-0,.0-",         "-0,0-",     "-0,0-",     "-0,0-",     "-0,1-",     "-1,2-",      "-12,3-",      "-123,5-",      "-1234,6-");
                    tester.test("-0,0,.0-",       "-00,0-",    "-00,0-",    "-00,0-",    "-00,1-",    "-01,2-",     "-12,3-",      "-123,5-",      "-1.234,6-");
                    tester.test("-0,,-",          "-0-",       "-0-",       "-0-",       "-0-",       "-0-",        "-0-",         "-0-",          "-1-");
                    tester.test("-0,,.0-",        "-0,0-",     "-0,0-",     "-0,0-",     "-0,0-",     "-0,0-",      "-0,0-",       "-0,1-",        "-1,2-");
                    tester.test("-0.0,-",         "-0,0-",     "-0,0-",     "-0,0-",     "-0,1-",     "-1,2-",      "-12,3-",      "-123,5-",      "-1234,6-");
                    tester.test("-0,.0,-",        "-0,0-",     "-0,0-",     "-0,0-",     "-0,0-",     "-0,0-",      "-0,0-",       "-0,1-",        "-1,2-");
                    tester.test("-0,.0,0,-",      "-0,00-",    "-0,00-",    "-0,00-",    "-0,00-",    "-0,00-",     "-0,01-",      "-0,12-",       "-1,23-");
                    tester.test("-0\\,0\\.0\\,-", "-0,0.0,-",  "-0,0.1,-",  "-0,1.2,-",  "-1,2.3,-",  "-12,3.5,-",  "-123,4.6,-",  "-1234,5.7,-",  "-12345,6.7,-");
                });
                it("with digit code '#'", () => {
                    const tester = new SeriesTester(0, 0.12,      1.234567,    12.34567,     123.4567,      1234.567,       12345.67,       123456.7);
                    tester.test("-#-",        "--",    "--",      "-1-",       "-12-",       "-123-",       "-1235-",       "-12346-",      "-123457-");
                    tester.test("-##-",       "--",    "--",      "-1-",       "-12-",       "-123-",       "-1235-",       "-12346-",      "-123457-");
                    tester.test("-###-",      "--",    "--",      "-1-",       "-12-",       "-123-",       "-1235-",       "-12346-",      "-123457-");
                    tester.test("-##0-",      "-0-",   "-0-",     "-1-",       "-12-",       "-123-",       "-1235-",       "-12346-",      "-123457-");
                    tester.test("-#0#-",      "-0-",   "-0-",     "-01-",      "-12-",       "-123-",       "-1235-",       "-12346-",      "-123457-");
                    tester.test("-0##-",      "-0-",   "-0-",     "-01-",      "-012-",      "-123-",       "-1235-",       "-12346-",      "-123457-");
                    tester.test("-#+#~#-",    "-+~-",  "-+~-",    "-+~1-",     "-+1~2-",     "-1+2~3-",     "-12+3~5-",     "-123+4~6-",    "-1234+5~7-");
                    tester.test("-#+#~0-",    "-+~0-", "-+~0-",   "-+~1-",     "-+1~2-",     "-1+2~3-",     "-12+3~5-",     "-123+4~6-",    "-1234+5~7-");
                    tester.test("-#+0~#-",    "-+0~-", "-+0~-",   "-+0~1-",    "-+1~2-",     "-1+2~3-",     "-12+3~5-",     "-123+4~6-",    "-1234+5~7-");
                    tester.test("-0+#~#-",    "-0+~-", "-0+~-",   "-0+~1-",    "-0+1~2-",    "-1+2~3-",     "-12+3~5-",     "-123+4~6-",    "-1234+5~7-");
                    tester.test("-#8#9#-",    "-89-",  "-89-",    "-891-",     "-8192-",     "-18293-",     "-128395-",     "-1238496-",    "-12348597-");
                    tester.test("-0.#-",      "-0,-",  "-0,1-",   "-1,2-",     "-12,3-",     "-123,5-",     "-1234,6-",     "-12345,7-",    "-123456,7-");
                    tester.test("-.#-",       "-,-",   "-,1-",    "-1,2-",     "-12,3-",     "-123,5-",     "-1234,6-",     "-12345,7-",    "-123456,7-");
                    tester.test("-#.#-",      "-,-",   "-,1-",    "-1,2-",     "-12,3-",     "-123,5-",     "-1234,6-",     "-12345,7-",    "-123456,7-");
                    tester.test("-#.##-",     "-,-",   "-,12-",   "-1,23-",    "-12,35-",    "-123,46-",    "-1234,57-",    "-12345,67-",   "-123456,7-");
                    tester.test("-#.###-",    "-,-",   "-,12-",   "-1,235-",   "-12,346-",   "-123,457-",   "-1234,567-",   "-12345,67-",   "-123456,7-");
                    tester.test("-#.0##-",    "-,0-",  "-,12-",   "-1,235-",   "-12,346-",   "-123,457-",   "-1234,567-",   "-12345,67-",   "-123456,7-");
                    tester.test("-#.00#-",    "-,00-", "-,12-",   "-1,235-",   "-12,346-",   "-123,457-",   "-1234,567-",   "-12345,67-",   "-123456,70-");
                    tester.test("-#.##0-",    "-,0-",  "-,120-",  "-1,235-",   "-12,346-",   "-123,457-",   "-1234,567-",   "-12345,670-",  "-123456,70-");
                    tester.test("-#.#+#~#-",  "-,+~-", "-,1+2~-", "-1,2+3~5-", "-12,3+4~6-", "-123,4+5~7-", "-1234,5+6~7-", "-12345,6+7~-", "-123456,7+~-");
                    tester.test("-#.#8#9#-",  "-,89-", "-,1829-", "-1,28395-", "-12,38496-", "-123,48597-", "-1234,58697-", "-12345,6879-", "-123456,789-");
                    tester.test("-#,###.#-",  "-,-",   "-,1-",    "-1,2-",     "-12,3-",     "-123,5-",     "-1.234,6-",    "-12.345,7-",  "-123.456,7-");
                    tester.test("-#,#.#,#-",  "-,-",   "-,12-",   "-1,23-",    "-12,35-",    "-123,46-",    "-1.234,57-",   "-12.345,67-",  "-123.456,7-");
                    tester.test("-#,#,.#,#-", "-,-",   "-,-",     "-,-",       "-,01-",      "-,12-",       "-1,23-",       "-12,35-",      "-123,46-");
                    tester.test("-#,#.#,#,-", "-,-",   "-,-",     "-,-",       "-,01-",      "-,12-",       "-1,23-",       "-12,35-",      "-123,46-");
                });
                it("with digit code '?'", () => {
                    const tester = new SeriesTester(0,     0.12,        1.234567,    12.34567,     123.4567,      1234.567,       12345.67,        123456.7);
                    tester.test("-?-",        "- -",       "- -",       "-1-",       "-12-",       "-123-",       "-1235-",       "-12346-",       "-123457-");
                    tester.test("-??-",       "-  -",      "-  -",      "- 1-",      "-12-",       "-123-",       "-1235-",       "-12346-",       "-123457-");
                    tester.test("-???-",      "-   -",     "-   -",     "-  1-",     "- 12-",      "-123-",       "-1235-",       "-12346-",       "-123457-");
                    tester.test("-??0-",      "-  0-",     "-  0-",     "-  1-",     "- 12-",      "-123-",       "-1235-",       "-12346-",       "-123457-");
                    tester.test("-?0?-",      "- 0 -",     "- 0 -",     "- 01-",     "- 12-",      "-123-",       "-1235-",       "-12346-",       "-123457-");
                    tester.test("-0??-",      "-0  -",     "-0  -",     "-0 1-",     "-012-",      "-123-",       "-1235-",       "-12346-",       "-123457-");
                    tester.test("-?+?~?-",    "- + ~ -",   "- + ~ -",   "- + ~1-",   "- +1~2-",    "-1+2~3-",     "-12+3~5-",     "-123+4~6-",     "-1234+5~7-");
                    tester.test("-?+?~0-",    "- + ~0-",   "- + ~0-",   "- + ~1-",   "- +1~2-",    "-1+2~3-",     "-12+3~5-",     "-123+4~6-",     "-1234+5~7-");
                    tester.test("-?+0~?-",    "- +0~ -",   "- +0~ -",   "- +0~1-",   "- +1~2-",    "-1+2~3-",     "-12+3~5-",     "-123+4~6-",     "-1234+5~7-");
                    tester.test("-0+?~?-",    "-0+ ~ -",   "-0+ ~ -",   "-0+ ~1-",   "-0+1~2-",    "-1+2~3-",     "-12+3~5-",     "-123+4~6-",     "-1234+5~7-");
                    tester.test("-?8?9?-",    "- 8 9 -",   "- 8 9 -",   "- 8 91-",   "- 8192-",    "-18293-",     "-128395-",     "-1238496-",     "-12348597-");
                    tester.test("-?.?-",      "- , -",     "- ,1-",     "-1,2-",     "-12,3-",     "-123,5-",     "-1234,6-",     "-12345,7-",     "-123456,7-");
                    tester.test("-?.?#-",     "- , -",     "- ,12-",    "-1,23-",    "-12,35-",    "-123,46-",    "-1234,57-",    "-12345,67-",    "-123456,7-");
                    tester.test("-?.??-",     "- ,  -",    "- ,12-",    "-1,23-",    "-12,35-",    "-123,46-",    "-1234,57-",    "-12345,67-",    "-123456,7 -");
                    tester.test("-?.???-",    "- ,   -",   "- ,12 -",   "-1,235-",   "-12,346-",   "-123,457-",   "-1234,567-",   "-12345,67 -",   "-123456,7  -");
                    tester.test("-?.0??-",    "- ,0  -",   "- ,12 -",   "-1,235-",   "-12,346-",   "-123,457-",   "-1234,567-",   "-12345,67 -",   "-123456,7  -");
                    tester.test("-?.00?-",    "- ,00 -",   "- ,12 -",   "-1,235-",   "-12,346-",   "-123,457-",   "-1234,567-",   "-12345,67 -",   "-123456,70 -");
                    tester.test("-?.??0-",    "- ,  0-",   "- ,120-",   "-1,235-",   "-12,346-",   "-123,457-",   "-1234,567-",   "-12345,670-",   "-123456,7 0-");
                    tester.test("-?.?+?~?-",  "- , + ~ -", "- ,1+2~ -", "-1,2+3~5-", "-12,3+4~6-", "-123,4+5~7-", "-1234,5+6~7-", "-12345,6+7~ -", "-123456,7+ ~ -");
                    tester.test("-?.?8?9?-",  "- , 8 9 -", "- ,1829 -", "-1,28395-", "-12,38496-", "-123,48597-", "-1234,58697-", "-12345,6879 -", "-123456,78 9 -");
                    tester.test("-?,???.?-",  "-     , -", "-     ,1-", "-    1,2-", "-   12,3-",  "-  123,5-",   "-1.234,6-",    "-12.345,7-",    "-123.456,7-");
                    tester.test("-?,?.?,?-",  "-  ,  -",   "-  ,12-",   "- 1,23-",   "-12,35-",    "-123,46-",    "-1.234,57-",   "-12.345,67-",   "-123.456,7 -");
                    tester.test("-?,?,.?,?-", "-  ,  -",   "-  ,  -",   "-  ,  -",   "-  ,01-",    "-  ,12-",     "- 1,23-",      "-12,35-",       "-123,46-");
                    tester.test("-?,?.?,?,-", "-  ,  -",   "-  ,  -",   "-  ,  -",   "-  ,01-",    "-  ,12-",     "- 1,23-",      "-12,35-",       "-123,46-");
                });
                it("with scientific format", () => {
                    const tester = new SeriesTester(0,          1,              12.34567,       1.234567e13,    1.234567e125,    0.1234567,      1.234567e-13,   1.234567e-125);
                    tester.test("-0E+0-",       "-0E+0-",       "-1E+0-",       "-1E+1-",       "-1E+13-",      "-1E+125-",      "-1E-1-",       "-1E-13-",      "-1E-125-");
                    tester.test("-0E-0-",       "-0E0-",        "-1E0-",        "-1E1-",        "-1E13-",       "-1E125-",       "-1E-1-",       "-1E-13-",      "-1E-125-");
                    tester.test("-00E+00-",     "-00E+00-",     "-01E+00-",     "-12E+00-",     "-12E+12-",     "-12E+124-",     "-12E-02-",     "-12E-14-",     "-12E-126-");
                    tester.test("-000E+000-",   "-000E+000-",   "-001E+000-",   "-012E+000-",   "-012E+012-",   "-123E+123-",    "-123E-003-",   "-123E-015-",   "-012E-126-");
                    tester.test("-0000E+0000-", "-0000E+0000-", "-0001E+0000-", "-0012E+0000-", "-0012E+0012-", "-0012E+0124-",  "-1235E-0004-", "-1235E-0016-", "-1235E-0128-");
                    tester.test("-#E+#-",       "-0E+0-",       "-1E+0-",       "-1E+1-",       "-1E+13-",      "-1E+125-",      "-1E-1-",       "-1E-13-",      "-1E-125-");
                    tester.test("-##E+##-",     "-0E+0-",       "-1E+0-",       "-12E+0-",      "-12E+12-",     "-12E+124-",     "-12E-2-",      "-12E-14-",     "-12E-126-");
                    tester.test("-###E+###-",   "-0E+0-",       "-1E+0-",       "-12E+0-",      "-12E+12-",     "-123E+123-",    "-123E-3-",     "-123E-15-",    "-12E-126-");
                    tester.test("-####E+####-", "-0E+0-",       "-1E+0-",       "-12E+0-",      "-12E+12-",     "-12E+124-",     "-1235E-4-",    "-1235E-16-",   "-1235E-128-");
                    tester.test("-?E+?-",       "-0E+0-",       "-1E+0-",       "-1E+1-",       "-1E+13-",      "-1E+125-",      "-1E-1-",       "-1E-13-",      "-1E-125-");
                    tester.test("-??E+??-",     "- 0E+ 0-",     "- 1E+ 0-",     "-12E+ 0-",     "-12E+12-",     "-12E+124-",     "-12E- 2-",     "-12E-14-",     "-12E-126-");
                    tester.test("-???E+???-",   "-  0E+  0-",   "-  1E+  0-",   "- 12E+  0-",   "- 12E+ 12-",   "-123E+123-",    "-123E-  3-",   "-123E- 15-",   "- 12E-126-");
                    tester.test("-????E+????-", "-   0E+   0-", "-   1E+   0-", "-  12E+   0-", "-  12E+  12-", "-  12E+ 124-",  "-1235E-   4-", "-1235E-  16-", "-1235E- 128-");
                    tester.test("-0.0E+0-",     "-0,0E+0-",     "-1,0E+0-",     "-1,2E+1-",     "-1,2E+13-",    "-1,2E+125-",    "-1,2E-1-",     "-1,2E-13-",    "-1,2E-125-");
                    tester.test("-#.##E+0-",    "-0,E+0-",      "-1,E+0-",      "-1,23E+1-",    "-1,23E+13-",   "-1,23E+125-",   "-1,23E-1-",    "-1,23E-13-",   "-1,23E-125-");
                    tester.test("-??.???E+0-",  "- 0,   E+0-",  "- 1,   E+0-",  "-12,346E+0-",  "-12,346E+12-", "-12,346E+124-", "-12,346E-2-",  "-12,346E-14-", "-12,346E-126-");
                    tester.test("-0+E+~0-",     "-0+E~+0-",     "-1+E~+0-",     "-1+E~+1-",     "-1+E~+13-",    "-1+E~+125-",    "-1+E~-1-",     "-1+E~-13-",    "-1+E~-125-");
                    tester.test("-0+E-~0-",     "-0+E~0-",      "-1+E~0-",      "-1+E~1-",      "-1+E~13-",     "-1+E~125-",     "-1+E~-1-",     "-1+E~-13-",    "-1+E~-125-");
                    tester.test("-0E+0.0-",     "-0E+0,0-",     "-1E+0,0-",     "-1E+1,0-",     "-1E+13,0-",    "-1E+125,0-",    "-1E-1,0-",     "-1E-13,0-",    "-1E-125,0-");
                    tester.test("-0E+0.#-",     "-0E+0,-",      "-1E+0,-",      "-1E+1,-",      "-1E+13,-",     "-1E+125,-",     "-1E-1,-",      "-1E-13,-",     "-1E-125,-");
                    tester.test("-0E+0.?-",     "-0E+0, -",     "-1E+0, -",     "-1E+1, -",     "-1E+13, -",    "-1E+125, -",    "-1E-1, -",     "-1E-13, -",    "-1E-125, -");
                    tester.test("-0,000E+0-",   "-0.000E+0-",   "-0.001E+0-",   "-0.012E+0-",   "-0.012E+12-",  "-0.012E+124-",  "-1.235E-4-",   "-1.235E-16-",  "-1.235E-128-");
                    tester.test("-0,,.0E+0-",   "-0,0E+0-",     "-1,0E+0-",     "-1,2E+1-",     "-1,2E+13-",    "-1,2E+125-",    "-1,2E-1-",     "-1,2E-13-",    "-1,2E-125-");
                });
                it("with percent format", () => {
                    const tester = new SeriesTester(0,       1,             0.01234567,   0.1234567,    1.234567,      12.34567,       123.4567,        1234.567);
                    tester.test("-0+%-",       "-0+%-",      "-100+%-",     "-1+%-",      "-12+%-",     "-123+%-",     "-1235+%-",     "-12346+%-",     "-123457+%-");
                    tester.test("-0.0+%-",     "-0,0+%-",    "-100,0+%-",   "-1,2+%-",    "-12,3+%-",   "-123,5+%-",   "-1234,6+%-",   "-12345,7+%-",   "-123456,7+%-");
                    tester.test("-0,0.0~%-",   "-00,0~%-",   "-100,0~%-",   "-01,2~%-",   "-12,3~%-",   "-123,5~%-",   "-1.234,6~%-",  "-12.345,7~%-",  "-123.456,7~%-");
                    tester.test("-0,0.0,~%-",  "-00,0~%-",   "-00,1~%-",    "-00,0~%-",   "-00,0~%-",   "-00,1~%-",    "-01,2~%-",     "-12,3~%-",      "-123,5~%-");
                    tester.test("-0.0E+0~%-",  "-0,0E+0~%-", "-1,0E+0~%-",  "-1,2E-2~%-", "-1,2E-1~%-", "-1,2E+0~%-",  "-1,2E+1~%-",   "-1,2E+2~%-",    "-1,2E+3~%-");
                    tester.test("-%+#~%-",     "-%+~%-",     "-%+10000~%-", "-%+123~%-",  "-%+1235~%-", "-%+12346~%-", "-%+123457~%-", "-%+1234567~%-", "-%+12345670~%-");
                    tester.test("-%%%+0,,.0-", "-%%%+0,0-",  "-%%%+1,0-",   "-%%%+0,0-",  "-%%%+0,1-",  "-%%%+1,2-",   "-%%%+12,3-",   "-%%%+123,5-",   "-%%%+1234,6-");
                });
                it("with numeral systems", () => {
                    const tester = new FormatTester();
                    // difference format code tokens
                    tester.test("[$-,200]General", 123, "١٢٣");
                    tester.test("[$-,200]General", 1.23e45, "١,٢٣E+٤٥");
                    tester.test("[$-,200]0#?", 123, "١٢٣");
                    tester.test("[$-,200]0.0#?", 0.123, "٠,١٢٣");
                    tester.test("[$-,200]0.00E+00.00", 1.23e123, "١,٢٣E+١٢٣,٠٠");
                    tester.test("[$-,200]0 00/00", 1 + 23 / 45, "١ ٢٣/٤٥");
                    tester.test("[$-,200]YYYY-MM-DD", 32142, "١٩٨٧-١٢-٣١");
                    tester.test("[$-,200]hh:mm:ss.0", 45296.7 / 86400, "١٢:٣٤:٥٦,٧");
                    // all supported numeral systems
                    tester.test("[$-,0000]General", 1234567890, "1234567890");
                    tester.test("[$-,0100]General", 1234567890, "1234567890");
                    tester.test("[$-,0200]General", 1234567890, "١٢٣٤٥٦٧٨٩٠");
                    tester.test("[$-,0300]General", 1234567890, "۱۲۳۴۵۶۷۸۹۰");
                    tester.test("[$-,0400]General", 1234567890, "१२३४५६७८९०");
                    tester.test("[$-,0500]General", 1234567890, "১২৩৪৫৬৭৮৯০");
                    tester.test("[$-,0600]General", 1234567890, "੧੨੩੪੫੬੭੮੯੦");
                    tester.test("[$-,0700]General", 1234567890, "૧૨૩૪૫૬૭૮૯૦");
                    tester.test("[$-,0800]General", 1234567890, "୧୨୩୪୫୬୭୮୯୦");
                    tester.test("[$-,0900]General", 1234567890, "௧௨௩௪௫௬௭௮௯௦");
                    tester.test("[$-,0A00]General", 1234567890, "౧౨౩౪౫౬౭౮౯౦");
                    tester.test("[$-,0B00]General", 1234567890, "೧೨೩೪೫೬೭೮೯೦");
                    tester.test("[$-,0C00]General", 1234567890, "൧൨൩൪൫൬൭൮൯൦");
                    tester.test("[$-,0D00]General", 1234567890, "๑๒๓๔๕๖๗๘๙๐");
                    tester.test("[$-,0E00]General", 1234567890, "໑໒໓໔໕໖໗໘໙໐");
                    tester.test("[$-,0F00]General", 1234567890, "༡༢༣༤༥༦༧༨༩༠");
                    tester.test("[$-,1000]General", 1234567890, "၁၂၃၄၅၆၇၈၉၀");
                    tester.test("[$-,1100]General", 1234567890, "፩፪፫፬፭፮፯፰፱0");
                    tester.test("[$-,1200]General", 1234567890, "១២៣៤៥៦៧៨៩០");
                    tester.test("[$-,1300]General", 1234567890, "᠑᠒᠓᠔᠕᠖᠗᠘᠙᠐");
                    tester.test("[$-,1400]General", 1234567890, "1234567890");
                    tester.test("[$-,1500]General", 1234567890, "1234567890");
                    tester.test("[$-,1600]General", 1234567890, "1234567890");
                    tester.test("[$-,1700]General", 1234567890, "1234567890");
                    tester.test("[$-,1800]General", 1234567890, "1234567890");
                    tester.test("[$-,1900]General", 1234567890, "1234567890");
                    tester.test("[$-,1A00]General", 1234567890, "1234567890");
                    tester.test("[$-,1B00]General", 1234567890, "一二三四五六七八九〇");
                    tester.test("[$-,1C00]General", 1234567890, "壱弐参四伍六七八九〇");
                    tester.test("[$-,1D00]General", 1234567890, "１２３４５６７８９０");
                    tester.test("[$-,1E00]General", 1234567890, "一二三四五六七八九○");
                    tester.test("[$-,1F00]General", 1234567890, "壹贰叁肆伍陆柒捌玖零");
                    tester.test("[$-,2000]General", 1234567890, "１２３４５６７８９０");
                    tester.test("[$-,2100]General", 1234567890, "一二三四五六七八九○");
                    tester.test("[$-,2200]General", 1234567890, "壹貳參肆伍陸柒捌玖零");
                    tester.test("[$-,2300]General", 1234567890, "１２３４５６７８９０");
                    tester.test("[$-,2400]General", 1234567890, "一二三四五六七八九０");
                    tester.test("[$-,2500]General", 1234567890, "壹貳參四伍六七八九零");
                    tester.test("[$-,2600]General", 1234567890, "１２３４５６７８９０");
                    tester.test("[$-,2700]General", 1234567890, "일이삼사오육칠팔구영");
                    // DBNum token (OOX only)
                    tester.test("[DBNum1][$-en-US]0", 1234567890, "1234567890");
                    tester.test("[DBNum2][$-en-US]0", 1234567890, "1234567890");
                    tester.test("[DBNum3][$-en-US]0", 1234567890, "1234567890");
                    tester.test("[DBNum4][$-en-US]0", 1234567890, "1234567890");
                    tester.test("[DBNum1][$-ja-JP]0", 1234567890, "一二三四五六七八九〇", "1234567890");
                    tester.test("[DBNum2][$-ja-JP]0", 1234567890, "壱弐参四伍六七八九〇", "1234567890");
                    tester.test("[DBNum3][$-ja-JP]0", 1234567890, "１２３４５６７８９０", "1234567890");
                    tester.test("[DBNum4][$-ja-JP]0", 1234567890, "1234567890");
                    tester.test("[DBNum1][$-zh-CN]0", 1234567890, "一二三四五六七八九○", "1234567890");
                    tester.test("[DBNum2][$-zh-CN]0", 1234567890, "壹贰叁肆伍陆柒捌玖零", "1234567890");
                    tester.test("[DBNum3][$-zh-CN]0", 1234567890, "１２３４５６７８９０", "1234567890");
                    tester.test("[DBNum4][$-zh-CN]0", 1234567890, "1234567890");
                    tester.test("[DBNum1][$-zh-TW]0", 1234567890, "一二三四五六七八九○", "1234567890");
                    tester.test("[DBNum2][$-zh-TW]0", 1234567890, "壹貳參肆伍陸柒捌玖零", "1234567890");
                    tester.test("[DBNum3][$-zh-TW]0", 1234567890, "１２３４５６７８９０", "1234567890");
                    tester.test("[DBNum4][$-zh-TW]0", 1234567890, "1234567890");
                    tester.test("[DBNum1][$-ko-KR]0", 1234567890, "一二三四五六七八九０", "1234567890");
                    tester.test("[DBNum2][$-ko-KR]0", 1234567890, "壹貳參四伍六七八九零", "1234567890");
                    tester.test("[DBNum3][$-ko-KR]0", 1234567890, "１２３４５６７８９０", "1234567890");
                    tester.test("[DBNum4][$-ko-KR]0", 1234567890, "일이삼사오육칠팔구영", "1234567890");
                    // use script identifier for simplified/ traditional)
                    tester.test("[DBNum2][$-zh-Hans]0", 1234567890, "壹贰叁肆伍陆柒捌玖零", "1234567890");
                    tester.test("[DBNum2][$-zh-Hant]0", 1234567890, "壹貳參肆伍陸柒捌玖零", "1234567890");
                    // ignore regions (except for Chinese traditional)
                    tester.test("[DBNum2][$-ja-XX]0", 1234567890, "壱弐参四伍六七八九〇", "1234567890");
                    tester.test("[DBNum2][$-ja]0", 1234567890, "壱弐参四伍六七八九〇", "1234567890");
                    tester.test("[DBNum2][$-zh-XX]0", 1234567890, "壹贰叁肆伍陆柒捌玖零", "1234567890");
                    tester.test("[DBNum2][$-zh]0", 1234567890, "壹贰叁肆伍陆柒捌玖零", "1234567890");
                    tester.test("[DBNum2][$-ko-XX]0", 1234567890, "壹貳參四伍六七八九零", "1234567890");
                    tester.test("[DBNum2][$-ko]0", 1234567890, "壹貳參四伍六七八九零", "1234567890");
                    // numeral system ID overrides DBNum token
                    tester.test("[DBNum2][$-ja-JP,200]0", 1234567890, "١٢٣٤٥٦٧٨٩٠");
                });
            });

            // bug 41434: round up to next power of 10
            it("should round up to next power of 10", () => {
                const tester = new FormatTester();
                tester.test("0.00", 9.99999999, "10,00");
                tester.test("000E+0", 999.9, "001E+3");
            });

            describe("should format fractions", () => {
                it("without integral part", () => {
                    const tester = new SeriesTester(0,    0.01,        1 / 9,       0.5,         6 / 7,       0.99,        1,           Math.E,       500 / 9);
                    tester.test("-0/0-",     "-0/1-",     "-0/1-",     "-1/9-",     "-1/2-",     "-6/7-",     "-1/1-",     "-1/1-",     "-19/7-",     "-500/9-");
                    tester.test("-#/#-",     "-0/1-",     "-0/1-",     "-1/9-",     "-1/2-",     "-6/7-",     "-1/1-",     "-1/1-",     "-19/7-",     "-500/9-");
                    tester.test("-?/?-",     "-0/1-",     "-0/1-",     "-1/9-",     "-1/2-",     "-6/7-",     "-1/1-",     "-1/1-",     "-19/7-",     "-500/9-");
                    tester.test("-0/2-",     "-0/2-",     "-0/2-",     "-0/2-",     "-1/2-",     "-2/2-",     "-2/2-",     "-2/2-",     "-5/2-",      "-111/2-");
                    tester.test("-#/3-",     "-0/3-",     "-0/3-",     "-0/3-",     "-2/3-",     "-3/3-",     "-3/3-",     "-3/3-",     "-8/3-",      "-167/3-");
                    tester.test("-?/5-",     "-0/5-",     "-0/5-",     "-1/5-",     "-3/5-",     "-4/5-",     "-5/5-",     "-5/5-",     "-14/5-",     "-278/5-");
                    tester.test("-00/00-",   "-00/01-",   "-01/99-",   "-01/09-",   "-01/02-",   "-06/07-",   "-98/99-",   "-01/01-",   "-193/71-",   "-500/09-");
                    tester.test("-##/##-",   "-0/1-",     "-1/99-",    "-1/9-",     "-1/2-",     "-6/7-",     "-98/99-",   "-1/1-",     "-193/71-",   "-500/9-");
                    tester.test("-??/??-",   "- 0/1 -",   "- 1/99-",   "- 1/9 -",   "- 1/2 -",   "- 6/7 -",   "-98/99-",   "- 1/1 -",   "-193/71-",   "-500/9 -");
                    tester.test("-00/10-",   "-00/10-",   "-00/10-",   "-01/10-",   "-05/10-",   "-09/10-",   "-10/10-",   "-10/10-",   "-27/10-",    "-556/10-");
                    tester.test("-##/33-",   "-0/33-",    "-0/33-",    "-4/33-",    "-17/33-",   "-28/33-",   "-33/33-",   "-33/33-",   "-90/33-",    "-1833/33-");
                    tester.test("-??/50-",   "- 0/50-",   "- 1/50-",   "- 6/50-",   "-25/50-",   "-43/50-",   "-50/50-",   "-50/50-",   "-136/50-",   "-2778/50-");
                    tester.test("-000/000-", "-000/001-", "-001/100-", "-001/009-", "-001/002-", "-006/007-", "-099/100-", "-001/001-", "-1457/536-", "-500/009-");
                    tester.test("-###/###-", "-0/1-",     "-1/100-",   "-1/9-",     "-1/2-",     "-6/7-",     "-99/100-",  "-1/1-",     "-1457/536-", "-500/9-");
                    tester.test("-???/???-", "-  0/1  -", "-  1/100-", "-  1/9  -", "-  1/2  -", "-  6/7  -", "- 99/100-", "-  1/1  -", "-1457/536-", "-500/9  -");
                    tester.test("-000/100-", "-000/100-", "-001/100-", "-011/100-", "-050/100-", "-086/100-", "-099/100-", "-100/100-", "-272/100-",  "-5556/100-");
                    tester.test("-###/333-", "-0/333-",   "-3/333-",   "-37/333-",  "-167/333-", "-285/333-", "-330/333-", "-333/333-", "-905/333-",  "-18500/333-");
                    tester.test("-???/500-", "-  0/500-", "-  5/500-", "- 56/500-", "-250/500-", "-429/500-", "-495/500-", "-500/500-", "-1359/500-", "-27778/500-");
                });
                it("with integral part (single letter)", () => {
                    const tester = new SeriesTester(0, 0.01,     1 / 9,     0.5,       6 / 7,     0.99,      1,         Math.E,    500 / 9);
                    tester.test("-0+0/0-", "-0+0/1-", "-0+0/1-", "-0+1/9-", "-0+1/2-", "-0+6/7-", "-1+0/1-", "-1+0/1-", "-2+5/7-", "-55+5/9-");
                    tester.test("-0+#/0-", "-0-",     "-0-",     "-0+1/9-", "-0+1/2-", "-0+6/7-", "-1-",     "-1-",     "-2+5/7-", "-55+5/9-");
                    tester.test("-0+?/0-", "-0    -", "-0    -", "-0+1/9-", "-0+1/2-", "-0+6/7-", "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-0+0/#-", "-0+0/1-", "-0+0/1-", "-0+1/9-", "-0+1/2-", "-0+6/7-", "-1+0/1-", "-1+0/1-", "-2+5/7-", "-55+5/9-");
                    tester.test("-0+#/#-", "-0-",     "-0-",     "-0+1/9-", "-0+1/2-", "-0+6/7-", "-1-",     "-1-",     "-2+5/7-", "-55+5/9-");
                    tester.test("-0+?/#-", "-0    -", "-0    -", "-0+1/9-", "-0+1/2-", "-0+6/7-", "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-0+0/?-", "-0+0/1-", "-0+0/1-", "-0+1/9-", "-0+1/2-", "-0+6/7-", "-1+0/1-", "-1+0/1-", "-2+5/7-", "-55+5/9-");
                    tester.test("-0+#/?-", "-0    -", "-0    -", "-0+1/9-", "-0+1/2-", "-0+6/7-", "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-0+?/?-", "-0    -", "-0    -", "-0+1/9-", "-0+1/2-", "-0+6/7-", "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-#+0/0-", "-0/1-",   "-0/1-",   "-1/9-",   "-1/2-",   "-6/7-",   "-1+0/1-", "-1+0/1-", "-2+5/7-", "-55+5/9-");
                    tester.test("-#+#/0-", "-0-",     "-0-",     "-1/9-",   "-1/2-",   "-6/7-",   "-1-",     "-1-",     "-2+5/7-", "-55+5/9-");
                    tester.test("-#+?/0-", "-0    -", "-0    -", "- 1/9-",  "- 1/2-",  "- 6/7-",  "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-#+0/#-", "-0/1-",   "-0/1-",   "-1/9-",   "-1/2-",   "-6/7-",   "-1+0/1-", "-1+0/1-", "-2+5/7-", "-55+5/9-");
                    tester.test("-#+#/#-", "-0-",     "-0-",     "-1/9-",   "-1/2-",   "-6/7-",   "-1-",     "-1-",     "-2+5/7-", "-55+5/9-");
                    tester.test("-#+?/#-", "-0    -", "-0    -", "- 1/9-",  "- 1/2-",  "- 6/7-",  "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-#+0/?-", "-0/1-",   "-0/1-",   "-1/9-",   "-1/2-",   "-6/7-",   "-1+0/1-", "-1+0/1-", "-2+5/7-", "-55+5/9-");
                    tester.test("-#+#/?-", "-0    -", "-0    -", "-1/9-",   "-1/2-",   "-6/7-",   "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-#+?/?-", "-0    -", "-0    -", "- 1/9-",  "- 1/2-",  "- 6/7-",  "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-?+0/0-", "-0+0/1-", "-0+0/1-", "-  1/9-", "-  1/2-", "-  6/7-", "-1+0/1-", "-1+0/1-", "-2+5/7-", "-55+5/9-");
                    tester.test("-?+#/0-", "-0    -", "-0    -", "-  1/9-", "-  1/2-", "-  6/7-", "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-?+?/0-", "-0    -", "-0    -", "-  1/9-", "-  1/2-", "-  6/7-", "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-?+0/#-", "-0+0/1-", "-0+0/1-", "-  1/9-", "-  1/2-", "-  6/7-", "-1+0/1-", "-1+0/1-", "-2+5/7-", "-55+5/9-");
                    tester.test("-?+#/#-", "-0    -", "-0    -", "-  1/9-", "-  1/2-", "-  6/7-", "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-?+?/#-", "-0    -", "-0    -", "-  1/9-", "-  1/2-", "-  6/7-", "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-?+0/?-", "-0+0/1-", "-0+0/1-", "-  1/9-", "-  1/2-", "-  6/7-", "-1+0/1-", "-1+0/1-", "-2+5/7-", "-55+5/9-");
                    tester.test("-?+#/?-", "-0    -", "-0    -", "-  1/9-", "-  1/2-", "-  6/7-", "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-?+?/?-", "-0    -", "-0    -", "-  1/9-", "-  1/2-", "-  6/7-", "-1    -", "-1    -", "-2+5/7-", "-55+5/9-");
                    tester.test("-0+0/2-", "-0+0/2-", "-0+0/2-", "-0+0/2-", "-0+1/2-", "-1+0/2-", "-1+0/2-", "-1+0/2-", "-2+1/2-", "-55+1/2-");
                    tester.test("-0+#/3-", "-0-",     "-0-",     "-0-",     "-0+2/3-", "-1-",     "-1-",     "-1-",     "-2+2/3-", "-55+2/3-");
                    tester.test("-0+?/5-", "-0    -", "-0    -", "-0+1/5-", "-0+3/5-", "-0+4/5-", "-1    -", "-1    -", "-2+4/5-", "-55+3/5-");
                });
                it("with integral part (multiple letters)", () => {
                    const tester = new SeriesTester(0,            0.01,            1 / 9,           0.5,             6 / 7,           0.99,            1,               Math.E,          500 / 9);
                    tester.test("-00+00/00-",    "-00+00/01-",    "-00+01/99-",    "-00+01/09-",    "-00+01/02-",    "-00+06/07-",    "-00+98/99-",    "-01+00/01-",    "-02+51/71-",    "-55+05/09-");
                    tester.test("-##+##/##-",    "-0-",           "-1/99-",        "-1/9-",         "-1/2-",         "-6/7-",         "-98/99-",       "-1-",           "-2+51/71-",     "-55+5/9-");
                    tester.test("-??+??/??-",    "- 0      -",    "-    1/99-",    "-    1/9 -",    "-    1/2 -",    "-    6/7 -",    "-   98/99-",    "- 1      -",    "- 2+51/71-",    "-55+ 5/9 -");
                    tester.test("-00+00/10-",    "-00+00/10-",    "-00+00/10-",    "-00+01/10-",    "-00+05/10-",    "-00+09/10-",    "-01+00/10-",    "-01+00/10-",    "-02+07/10-",    "-55+06/10-");
                    tester.test("-##+##/33-",    "-0-",           "-0-",           "-4/33-",        "-17/33-",       "-28/33-",       "-1-",           "-1-",           "-2+24/33-",      "-55+18/33-");
                    tester.test("-??+??/50-",    "- 0      -",    "-    1/50-",    "-    6/50-",    "-   25/50-",    "-   43/50-",    "- 1      -",    "- 1      -",    "- 2+36/50-",    "-55+28/50-");
                    tester.test("-000+000/000-", "-000+000/001-", "-000+001/100-", "-000+001/009-", "-000+001/002-", "-000+006/007-", "-000+099/100-", "-001+000/001-", "-002+385/536-", "-055+005/009-");
                    tester.test("-###+###/###-", "-0-",           "-1/100-",       "-1/9-",         "-1/2-",         "-6/7-",         "-99/100-",      "-1-",           "-2+385/536-",   "-55+5/9-");
                    tester.test("-???+???/???-", "-  0        -", "-      1/100-", "-      1/9  -", "-      1/2  -", "-      6/7  -", "-     99/100-", "-  1        -", "-  2+385/536-", "- 55+  5/9  -");
                    tester.test("-000+000/100-", "-000+000/100-", "-000+001/100-", "-000+011/100-", "-000+050/100-", "-000+086/100-", "-000+099/100-", "-001+000/100-", "-002+072/100-", "-055+056/100-");
                    tester.test("-###+###/333-", "-0-",           "-3/333-",       "-37/333-",      "-167/333-",     "-285/333-",     "-330/333-",     "-1-",           "-2+239/333-",    "-55+185/333-");
                    tester.test("-???+???/500-", "-  0        -", "-      5/500-", "-     56/500-", "-    250/500-", "-    429/500-", "-    495/500-", "-  1        -", "-  2+359/500-", "- 55+278/500-");
                });
            });

            describe("should format dates and times", () => {

                it("(dates)", () => {
                    const tester = new SeriesTester(31782,                     35864,                  36656,                 40010,                    40438,                      44492,                    48574);
                    tester.test("-D.M.Y-",             "-5.1.87-",             "-10.3.98-",            "-10.5.00-",           "-16.7.09-",              "-17.9.10-",                "-23.10.21-",             "-26.12.32-");
                    tester.test("-DD.MM.YY-",          "-05.01.87-",           "-10.03.98-",           "-10.05.00-",          "-16.07.09-",             "-17.09.10-",               "-23.10.21-",             "-26.12.32-");
                    tester.test("-DDD.MMM.YYY-",       "-Mo.Jan.1987-",        "-Di.Mrz.1998-",        "-Mi.Mai.2000-",       "-Do.Jul.2009-",          "-Fr.Sep.2010-",            "-Sa.Okt.2021-",          "-So.Dez.2032-");
                    tester.test("-DDDD.MMMM.YYYY-",    "-Montag.Januar.1987-", "-Dienstag.März.1998-", "-Mittwoch.Mai.2000-", "-Donnerstag.Juli.2009-", "-Freitag.September.2010-", "-Samstag.Oktober.2021-", "-Sonntag.Dezember.2032-");
                    tester.test("-DDDDD.MMMMM.YYYYY-", "-Montag.J.1987-",      "-Dienstag.M.1998-",    "-Mittwoch.M.2000-",   "-Donnerstag.J.2009-",    "-Freitag.S.2010-",         "-Samstag.O.2021-",       "-Sonntag.D.2032-");
                    tester.test("-a aa aaa aaaa-",     "-a aa Mo Montag-",     "-a aa Di Dienstag-",   "-a aa Mi Mittwoch-",  "-a aa Do Donnerstag-",   "-a aa Fr Freitag-",        "-a aa Sa Samstag-",      "-a aa So Sonntag-");
                    tester.test("-b bb bbb bbbb-",     "-30 30 2530 2530-",    "-41 41 2541 2541-",    "-43 43 2543 2543-",   "-52 52 2552 2552-",      "-53 53 2553 2553-",        "-64 64 2564 2564-",      "-75 75 2575 2575-");
                });

                it("(leap year bug and invalid dates)", () => {
                    const tester = new FormatTester();
                    tester.test("YYYY-MM-DD DDD",       -1, null,            "1899-12-29 Fr");
                    tester.test("YYYY-MM-DD DDD",        0, "1900-01-00 Sa", "1899-12-30 Sa");
                    tester.test("YYYY-MM-DD DDD",        1, "1900-01-01 So", "1899-12-31 So");
                    tester.test("YYYY-MM-DD DDD",        2, "1900-01-02 Mo", "1900-01-01 Mo");
                    tester.test("YYYY-MM-DD DDD",       59, "1900-02-28 Di", "1900-02-27 Di");
                    tester.test("YYYY-MM-DD DDD",       60, "1900-02-29 Mi", "1900-02-28 Mi");
                    tester.test("YYYY-MM-DD DDD",       61, "1900-03-01 Do", "1900-03-01 Do");
                    tester.test("YYYY-MM-DD DDD",  2958465, "9999-12-31 Fr", "9999-12-31 Fr");
                    tester.test("YYYY-MM-DD DDD",  2958466, null,            "10000-01-01 Sa");
                    tester.test("YYYY-MM-DD DDD", 11274306, null,            "32767-12-31 So");
                    tester.test("YYYY-MM-DD DDD", 11274307, null,            null);
                });

                it("(localized dates)", () => {
                    const tester = new FormatTester();
                    tester.test("[$-409]MMMM DDDD",   0, "January Saturday", "December Saturday");
                    tester.test("[$-en-us]MMMM DDDD", 0, "January Saturday", "December Saturday");
                    tester.test("[$-407]MMMM DDDD",   0, "Januar Samstag",   "Dezember Samstag");
                    tester.test("[$-de-de]MMMM DDDD", 0, "Januar Samstag",   "Dezember Samstag");
                    tester.test("[$-40C]MMMM DDDD",   0, "janvier samedi",   "décembre samedi");
                    tester.test("[$-fr-fr]MMMM DDDD", 0, "janvier samedi",   "décembre samedi");
                });

                it("(dates with Gengo calendar)", () => {
                    const tester = new SeriesTester(0,            4594,             4595,             9855,             9856,             32515,            32516,            43585,            43586);
                    tester.test("-e-",          "-1900-",         "-1912-",         "-1912-",         "-1926-",         "-1926-",         "-1989-",         "-1989-",         "-2019-",         "-2019-");
                    tester.test("-ee-",         "-1900-",         "-1912-",         "-1912-",         "-1926-",         "-1926-",         "-1989-",         "-1989-",         "-2019-",         "-2019-");
                    tester.test("-g-",          "--",             "--",             "--",             "--",             "--",             "--",             "--",             "--",             "--");
                    tester.test("-gg-",         "--",             "--",             "--",             "--",             "--",             "--",             "--",             "--",             "--");
                    tester.test("-ggg-",        "--",             "--",             "--",             "--",             "--",             "--",             "--",             "--",             "--");
                    tester.test("[$-411]-e-",   "-33-",           "-45-",           "-1-",            "-15-",           "-1-",            "-64-",           "-1-",            "-31-",           "-1-");
                    tester.test("[$-411]-ee-",  "-33-",           "-45-",           "-01-",           "-15-",           "-01-",           "-64-",           "-01-",           "-31-",           "-01-");
                    tester.test("[$-411]-g-",   "-M-",            "-M-",            "-T-",            "-T-",            "-S-",            "-S-",            "-H-",            "-H-",            "-R-");
                    tester.test("[$-411]-gg-",  "-\u660E-",       "-\u660E-",       "-\u5927-",       "-\u5927-",       "-\u662D-",       "-\u662D-",       "-\u5E73-",       "-\u5E73-",       "-\u4EE4-");
                    tester.test("[$-411]-ggg-", "-\u660E\u6CBB-", "-\u660E\u6CBB-", "-\u5927\u6B63-", "-\u5927\u6B63-", "-\u662D\u548C-", "-\u662D\u548C-", "-\u5E73\u6210-", "-\u5E73\u6210-", "-\u4EE4\u548C-");
                });

                it("(times)", () => {
                    const S = 1 / SEC_PER_DAY;
                    const tester = new SeriesTester(0,         3599 * S,     3600 * S,     22028 * S,    43199 * S,    43200 * S,    46799 * S,    46800 * S,    67014 * S,    86399 * S);
                    tester.test("-h:m:s-",       "-0:0:0-",    "-0:59:59-",  "-1:0:0-",    "-6:7:8-",    "-11:59:59-", "-12:0:0-",   "-12:59:59-", "-13:0:0-",   "-18:36:54-", "-23:59:59-");
                    tester.test("-hh:mm:ss-",    "-00:00:00-", "-00:59:59-", "-01:00:00-", "-06:07:08-", "-11:59:59-", "-12:00:00-", "-12:59:59-", "-13:00:00-", "-18:36:54-", "-23:59:59-");
                    tester.test("-hh:mm AM/PM-", "-12:00 AM-", "-12:59 AM-", "-01:00 AM-", "-06:07 AM-", "-11:59 AM-", "-12:00 PM-", "-12:59 PM-", "-01:00 PM-", "-06:36 PM-", "-11:59 PM-");
                    tester.test("-hh:mm am/pm-", "-12:00 AM-", "-12:59 AM-", "-01:00 AM-", "-06:07 AM-", "-11:59 AM-", "-12:00 PM-", "-12:59 PM-", "-01:00 PM-", "-06:36 PM-", "-11:59 PM-");
                    tester.test("-hh:mm aM/Pm-", "-12:00 AM-", "-12:59 AM-", "-01:00 AM-", "-06:07 AM-", "-11:59 AM-", "-12:00 PM-", "-12:59 PM-", "-01:00 PM-", "-06:36 PM-", "-11:59 PM-");
                    tester.test("-hh:mm A/P-",   "-12:00 A-",  "-12:59 A-",  "-01:00 A-",  "-06:07 A-",  "-11:59 A-",  "-12:00 P-",  "-12:59 P-",  "-01:00 P-",  "-06:36 P-",  "-11:59 P-");
                    tester.test("-hh:mm a/p-",   "-12:00 a-",  "-12:59 a-",  "-01:00 a-",  "-06:07 a-",  "-11:59 a-",  "-12:00 p-",  "-12:59 p-",  "-01:00 p-",  "-06:36 p-",  "-11:59 p-");
                    tester.test("-A/p+hha/Pmm-", "-A+12a00-",  "-A+12a59-",  "-A+01a00-",  "-A+06a07-",  "-A+11a59-",  "-p+12P00-",  "-p+12P59-",  "-p+01P00-",  "-p+06P36-",  "-p+11P59-");
                    tester.test("-h:M-",         "-0:0-",      "-0:59-",     "-1:0-",      "-6:7-",      "-11:59-",    "-12:0-",     "-12:59-",    "-13:0-",     "-18:36-",    "-23:59-");
                    tester.test("-M:s-",         "-0:0-",      "-59:59-",    "-0:0-",      "-7:8-",      "-59:59-",    "-0:0-",      "-59:59-",    "-0:0-",      "-36:54-",    "-59:59-");
                });

                it("(times with localized AM/PM)", () => {
                    const tester = new SeriesTester(0,               0.5);
                    tester.test("[$-af-ZA]hh:mm AM/PM", "12:00 vm.", "12:00 nm.");
                    tester.test("[$-af-ZA]hh:mm A/P",   "12:00 A",   "12:00 P");
                });

                it("(times with fractional seconds)", () => {
                    const MS = 1 / MSEC_PER_DAY;
                    const tester = new SeriesTester(0,    0.5 * MS,    MS,          1.5 * MS,    2 * MS,      123 * MS,    789 * MS,    2999 * MS,   2999.5 * MS);
                    tester.test("-m:s.000-", "-0:0,000-", "-0:0,001-", "-0:0,001-", "-0:0,002-", "-0:0,002-", "-0:0,123-", "-0:0,789-", "-0:2,999-", "-0:3,000-");
                    tester.test("-m:s.00-",  "-0:0,00-",  "-0:0,00-",  "-0:0,00-",  "-0:0,00-",  "-0:0,00-",  "-0:0,12-",  "-0:0,79-",  "-0:3,00-",  "-0:3,00-");
                    tester.test("-m:s+.0-",  "-0:0+,0-",  "-0:0+,0-",  "-0:0+,0-",  "-0:0+,0-",  "-0:0+,0-",  "-0:0+,1-",  "-0:0+,8-",  "-0:3+,0-",  "-0:3+,0-");
                    tester.test("-m:s-",     "-0:0-",     "-0:0-",     "-0:0-",     "-0:0-",     "-0:0-",     "-0:0-",     "-0:1-",     "-0:3-",     "-0:3-");
                });

                // bug 48011: special LCIDs for system date and time formats
                it("(system formats for special LCIDs)", () => {
                    const tester = new FormatTester();
                    const date = new Date(Date.UTC(2001, 3, 2, 6, 0, 0));
                    tester.test("[$-F800]M/D/Y", date, "Montag, 2. April 2001");
                    tester.test("[$-F800]0.0",   date, "Montag, 2. April 2001");
                    tester.test("[$-F800]",      date, "Montag, 2. April 2001");
                    tester.test("[$-x-sysdate]", date, "Montag, 2. April 2001");
                    tester.test("[$-X-SysDate]", date, "Montag, 2. April 2001");
                    tester.test("[$-F400]h:m:s", date, "06:00:00");
                    tester.test("[$-F400]0.0",   date, "06:00:00");
                    tester.test("[$-F400]",      date, "06:00:00");
                    tester.test("[$-x-systime]", date, "06:00:00");
                    tester.test("[$-X-SysTime]", date, "06:00:00");
                });

                // special ODF format codes
                it("(ODF date format tokens)", () => {
                    const tester = new FormatTester();
                    // weekday
                    tester.test("N",     36617, null, "N");
                    tester.test("NN",    36617, null, "Sa");
                    tester.test("NNN",   36617, null, "Samstag");
                    tester.test("NNNN",  36617, null, "Samstag, ");
                    tester.test("NNNNN", 36617, null, "Samstag, N");
                    tester.test("n",     36617, null, "n");
                    tester.test("nn",    36617, null, "Sa");
                    tester.test("nnn",   36617, null, "Samstag");
                    tester.test("nnnn",  36617, null, "Samstag, ");
                    tester.test("nnnnn", 36617, null, "Samstag, n");
                    // weeknumber
                    tester.test("W",   36547, "W",   "W");
                    tester.test("WW",  36547, "WW",  "3");
                    tester.test("WWW", 36547, "WWW", "3W");
                    tester.test("w",   36547, "w",   "w");
                    tester.test("ww",  36547, "ww",  "3");
                    tester.test("www", 36547, "www", "3w");
                    // quarter
                    tester.test("Q",   36617, "Q",   "Q2");
                    tester.test("QQ",  36617, "QQ",  "2. Quartal");
                    tester.test("QQQ", 36617, "QQQ", "2. QuartalQ2");
                    tester.test("q",   36617, "q",   "Q2");
                    tester.test("qq",  36617, "qq",  "2. Quartal");
                    tester.test("qqq", 36617, "qqq", "2. QuartalQ2");
                });
            });

            describe("should format text", () => {
                const tester = new SeriesTester(null, "", "txt");
                it("(with literal text)", () => {
                    tester.test("@",      null, "",    "txt");
                    tester.test("-@-",    null, "--",  "-txt-");
                    tester.test("-@+@-",  null, "-+-", "-txt+txt-");
                    tester.test(";;;@",   null, "",    "txt");
                    tester.test(";;;-@-", null, "--",  "-txt-");
                    tester.test(";;;--",  null, "--",  "--");
                    tester.test(";;;",    null, "",    "");
                });
                it("(with blind text)", () => {
                    tester.test("-_W@-",  null, "- -", "- txt-");
                    tester.test("-@_i-",  null, "- -", "-txt -");
                    tester.test("-@_@@-", null, "- -", "-txt txt-");
                });
            });

            describe("should format sections", () => {
                const tester = new SeriesTester(3, 2, 1, 0, -1, -2, -3, "txt");
                it("without intervals", () => {
                    tester.test("\\A",              "A", "A", "A", "A", "-A", "-A", "-A", "txt");
                    tester.test("\\A;\\B",          "A", "A", "A", "A", "B",  "B",  "B",  "txt");
                    tester.test("\\A;\\B;\\C",      "A", "A", "A", "C", "B",  "B",  "B",  "txt");
                    tester.test("\\D@",             "3", "2", "1", "0", "-1", "-2", "-3", "Dtxt");
                    tester.test("\\A;\\D@",         "A", "A", "A", "A", "-A", "-A", "-A", "Dtxt");
                    tester.test("\\A;\\B;\\D@",     "A", "A", "A", "A", "B",  "B",  "B",  "Dtxt");
                    tester.test("\\A;\\B;\\C;\\D@", "A", "A", "A", "C", "B",  "B",  "B",  "Dtxt");
                });
                it("with single section with interval", () => {
                    tester.test("[=2]\\A",          "3",  "A",  "1",  "0",  "-1",  "-2",  "-3",  "txt");
                    tester.test("[=0]\\A",          "3",  "2",  "1",  "A",  "-1",  "-2",  "-3",  "txt");
                    tester.test("[=-2]\\A",         "3",  "2",  "1",  "0",  "-1",  "A",   "-3",  "txt");
                    tester.test("[<>2]\\A",         "A",  "2",  "A",  "A",  "-A",  "-A",  "-A",  "txt");
                    tester.test("[<>0]\\A",         "A",  "A",  "A",  "0",  "-A",  "-A",  "-A",  "txt");
                    tester.test("[<>-2]\\A",        "A",  "A",  "A",  "A",  "-A",  "2",   "-A",  "txt");
                    tester.test("[>1]\\A",          "A",  "A",  "1",  "0",  "-1",  "-2",  "-3",  "txt");
                    tester.test("[<-1]\\A",         "3",  "2",  "1",  "0",  "-1",  "A",   "A",   "txt");
                    tester.test("[>0]\\A",          "A",  "A",  "A",  "0",  "-1",  "-2",  "-3",  "txt");
                    tester.test("[<0]\\A",          "3",  "2",  "1",  "0",  "A",   "A",   "A",   "txt");
                    tester.test("[>=-1]\\A",        "A",  "A",  "A",  "A",  "-A",  "2",   "3",   "txt");
                    tester.test("[<=1]\\A",         "3",  "2",  "A",  "A",  "-A",  "-A",  "-A",  "txt");
                    tester.test("[>=0]\\A",         "A",  "A",  "A",  "A",  "1",   "2",   "3",   "txt");
                    tester.test("[<=0]\\A",         "3",  "2",  "1",  "A",  "-A",  "-A",  "-A",  "txt");
                    tester.test("[<=1]General",     null, null, "1",  "0",  "-1",  "-2",  "-3",  "txt");
                    tester.test("[>=-1]General",    "3",  "2",  "1",  "0",  "-1",  null,  null,  "txt");
                    tester.test("[<=1]General\\A",  "3",  "2",  "1A", "0A", "-1A", "-2A", "-3A", "txt");
                    tester.test("[>=-1]General\\A", "3A", "2A", "1A", "0A", "-1A", "2",   "3",   "txt");
                });
                it("with two sections (leading interval)", () => {
                    tester.test("[=2]\\A;\\B",   "B", "A", "B", "B", "-B", "-B", "-B", "txt");
                    tester.test("[=0]\\A;\\B",   "B", "B", "B", "A", "-B", "-B", "-B", "txt");
                    tester.test("[=-2]\\A;\\B",  "B", "B", "B", "B", "-B", "A",  "-B", "txt");
                    tester.test("[<>2]\\A;\\B",  "A", "B", "A", "A", "-A", "-A", "-A", "txt");
                    tester.test("[<>0]\\A;\\B",  "A", "A", "A", "B", "-A", "-A", "-A", "txt");
                    tester.test("[<>-2]\\A;\\B", "A", "A", "A", "A", "-A", "B",  "-A", "txt");
                    tester.test("[>1]\\A;\\B",   "A", "A", "B", "B", "-B", "-B", "-B", "txt");
                    tester.test("[<-1]\\A;\\B",  "B", "B", "B", "B", "-B", "A",  "A",  "txt");
                    tester.test("[>0]\\A;\\B",   "A", "A", "A", "B", "-B", "-B", "-B", "txt");
                    tester.test("[<0]\\A;\\B",   "B", "B", "B", "B", "A",  "A",  "A",  "txt");
                    tester.test("[>=-1]\\A;\\B", "A", "A", "A", "A", "-A", "B",  "B",  "txt");
                    tester.test("[<=1]\\A;\\B",  "B", "B", "A", "A", "-A", "-A", "-A", "txt");
                    tester.test("[>=0]\\A;\\B",  "A", "A", "A", "A", "B",  "B",  "B",  "txt");
                    tester.test("[<=0]\\A;\\B",  "B", "B", "B", "A", "-A", "-A", "-A", "txt");
                });
                it("with two sections (trailing interval)", () => {
                    tester.test("\\A;[=2]\\B",   "A", "A", "A", null, null, null, null, "txt");
                    tester.test("\\A;[=0]\\B",   "A", "A", "A", "B",  null, null, null, "txt");
                    tester.test("\\A;[=-2]\\B",  "A", "A", "A", null, null, "B",  null, "txt");
                    tester.test("\\A;[<>2]\\B",  "A", "A", "A", "B",  "-B", "-B", "-B", "txt");
                    tester.test("\\A;[<>0]\\B",  "A", "A", "A", null, "-B", "-B", "-B", "txt");
                    tester.test("\\A;[<>-2]\\B", "A", "A", "A", "B",  "-B", null, "-B", "txt");
                    tester.test("\\A;[>1]\\B",   "A", "A", "A", null, null, null, null, "txt");
                    tester.test("\\A;[<-1]\\B",  "A", "A", "A", null, null, "B",  "B",  "txt");
                    tester.test("\\A;[>0]\\B",   "A", "A", "A", null, null, null, null, "txt");
                    tester.test("\\A;[<0]\\B",   "A", "A", "A", null, "B",  "B",  "B",  "txt");
                    tester.test("\\A;[>=-1]\\B", "A", "A", "A", "B",  "-B", null, null, "txt");
                    tester.test("\\A;[<=1]\\B",  "A", "A", "A", "B",  "-B", "-B", "-B", "txt");
                    tester.test("\\A;[>=0]\\B",  "A", "A", "A", "B",  null, null, null, "txt");
                    tester.test("\\A;[<=0]\\B",  "A", "A", "A", "B",  "-B", "-B", "-B", "txt");
                });
                it("with two sections (two intervals)", () => {
                    tester.test("[=2]\\A;[=-2]\\B",   null, "A",  null, null, null, "B",  null, "txt");
                    tester.test("[=-2]\\A;[=2]\\B",   null, "B",  null, null, null, "A",  null, "txt");
                    tester.test("[=0]\\A;[=0]\\B",    null, null, null, "A",  null, null, null, "txt");
                    tester.test("[>1]\\A;[<-1]\\B",   "A",  "A",  null, null, null, "B",  "B",  "txt");
                    tester.test("[<-1]\\A;[>1]\\B",   "B",  "B",  null, null, null, "A",  "A",  "txt");
                    tester.test("[>0]\\A;[<0]\\B",    "A",  "A",  "A",  null, "B",  "B",  "B",  "txt");
                    tester.test("[<0]\\A;[>0]\\B",    "B",  "B",  "B",  null, "A",  "A",  "A",  "txt");
                    tester.test("[>=-1]\\A;[<=1]\\B", "A",  "A",  "A",  "A",  "-A", "-B", "-B", "txt");
                    tester.test("[<=1]\\A;[>=-1]\\B", "B",  "B",  "A",  "A",  "-A", "-A", "-A", "txt");
                    tester.test("[>=0]\\A;[<=0]\\B",  "A",  "A",  "A",  "A",  "-B", "-B", "-B", "txt");
                    tester.test("[<=0]\\A;[>=0]\\B",  "B",  "B",  "B",  "A",  "-A", "-A", "-A", "txt");
                });
                it("with two empty sections", () => {
                    tester.test("\\A;",       "A", "A", "A", "A",  "",   "",   "",   "txt");
                    tester.test("[>1]\\A;",   "A", "A", "",  "",   "-",  "-",  "-",  "txt");
                    tester.test("[>0]\\A;",   "A", "A", "A", "",   "-",  "-",  "-",  "txt");
                    tester.test("[>=-1]\\A;", "A", "A", "A", "A",  "-A", "",   "",   "txt");
                    tester.test(";\\B",       "",  "",  "",  "",   "B",  "B",  "B",  "txt");
                    tester.test(";[<=1]\\B",  "",  "",  "",  "B",  "-B", "-B", "-B", "txt");
                    tester.test(";[<0]\\B",   "",  "",  "",  null, "B",  "B",  "B",  "txt");
                    tester.test(";[>1]\\B",   "",  "",  "",  null, null, null, null, "txt");
                    tester.test(";",          "",  "",  "",  "",   "",   "",   "",   "txt");
                });
                it("with three sections (first interval)", () => {
                    tester.test("[=2]\\A;\\B;\\C",   "C", "A", "C", "C", "B",  "B",  "B",  "txt");
                    tester.test("[=0]\\A;\\B;\\C",   "C", "C", "C", "A", "B",  "B",  "B",  "txt");
                    tester.test("[=-2]\\A;\\B;\\C",  "C", "C", "C", "C", "B",  "A",  "B",  "txt");
                    tester.test("[<>2]\\A;\\B;\\C",  "A", "C", "A", "A", "-A", "-A", "-A", "txt");
                    tester.test("[<>0]\\A;\\B;\\C",  "A", "A", "A", "C", "-A", "-A", "-A", "txt");
                    tester.test("[<>-2]\\A;\\B;\\C", "A", "A", "A", "A", "-A", "B",  "-A", "txt");
                    tester.test("[>1]\\A;\\B;\\C",   "A", "A", "C", "C", "B",  "B",  "B",  "txt");
                    tester.test("[<-1]\\A;\\B;\\C",  "C", "C", "C", "C", "B",  "A",  "A",  "txt");
                    tester.test("[>0]\\A;\\B;\\C",   "A", "A", "A", "C", "B",  "B",  "B",  "txt");
                    tester.test("[<0]\\A;\\B;\\C",   "C", "C", "C", "C", "A",  "A",  "A",  "txt");
                    tester.test("[>=-1]\\A;\\B;\\C", "A", "A", "A", "A", "-A", "B",  "B",  "txt");
                    tester.test("[<=1]\\A;\\B;\\C",  "C", "C", "A", "A", "-A", "-A", "-A", "txt");
                    tester.test("[>=0]\\A;\\B;\\C",  "A", "A", "A", "A", "B",  "B",  "B",  "txt");
                    tester.test("[<=0]\\A;\\B;\\C",  "C", "C", "C", "A", "-A", "-A", "-A", "txt");
                });
                it("with three sections (second interval)", () => {
                    tester.test("\\A;[=2]\\B;\\C",   "A", "A", "A", "C", "-C", "-C", "-C", "txt");
                    tester.test("\\A;[=0]\\B;\\C",   "A", "A", "A", "B", "-C", "-C", "-C", "txt");
                    tester.test("\\A;[=-2]\\B;\\C",  "A", "A", "A", "C", "-C", "B",  "-C", "txt");
                    tester.test("\\A;[<>2]\\B;\\C",  "A", "A", "A", "B", "-B", "-B", "-B", "txt");
                    tester.test("\\A;[<>0]\\B;\\C",  "A", "A", "A", "C", "-B", "-B", "-B", "txt");
                    tester.test("\\A;[<>-2]\\B;\\C", "A", "A", "A", "B", "-B", "-C", "-B", "txt");
                    tester.test("\\A;[>1]\\B;\\C",   "A", "A", "A", "C", "-C", "-C", "-C", "txt");
                    tester.test("\\A;[<-1]\\B;\\C",  "A", "A", "A", "C", "-C", "B",  "B",  "txt");
                    tester.test("\\A;[>0]\\B;\\C",   "A", "A", "A", "C", "-C", "-C", "-C", "txt");
                    tester.test("\\A;[<0]\\B;\\C",   "A", "A", "A", "C", "B",  "B",  "B",  "txt");
                    tester.test("\\A;[>=-1]\\B;\\C", "A", "A", "A", "B", "-B", "-C", "-C", "txt");
                    tester.test("\\A;[<=1]\\B;\\C",  "A", "A", "A", "B", "-B", "-B", "-B", "txt");
                    tester.test("\\A;[>=0]\\B;\\C",  "A", "A", "A", "B", "-C", "-C", "-C", "txt");
                    tester.test("\\A;[<=0]\\B;\\C",  "A", "A", "A", "B", "-B", "-B", "-B", "txt");
                });
                it("with three sections (two intervals)", () => {
                    tester.test("[=2]\\A;[=-2]\\B;\\C",   "C", "A", "C", "C", "-C", "B",  "-C", "txt");
                    tester.test("[=-2]\\A;[=2]\\B;\\C",   "C", "B", "C", "C", "-C", "A",  "-C", "txt");
                    tester.test("[=0]\\A;[=0]\\B;\\C",    "C", "C", "C", "A", "-C", "-C", "-C", "txt");
                    tester.test("[>1]\\A;[<-1]\\B;\\C",   "A", "A", "C", "C", "-C", "B",  "B",  "txt");
                    tester.test("[<-1]\\A;[>1]\\B;\\C",   "B", "B", "C", "C", "-C", "A",  "A",  "txt");
                    tester.test("[>0]\\A;[<0]\\B;\\C",    "A", "A", "A", "C", "B",  "B",  "B",  "txt");
                    tester.test("[<0]\\A;[>0]\\B;\\C",    "B", "B", "B", "C", "A",  "A",  "A",  "txt");
                    tester.test("[>=-1]\\A;[<=1]\\B;\\C", "A", "A", "A", "A", "-A", "-B", "-B", "txt");
                    tester.test("[<=1]\\A;[>=-1]\\B;\\C", "B", "B", "A", "A", "-A", "-A", "-A", "txt");
                    tester.test("[>=0]\\A;[<=0]\\B;\\C",  "A", "A", "A", "A", "-B", "-B", "-B", "txt");
                    tester.test("[<=0]\\A;[>=0]\\B;\\C",  "B", "B", "B", "A", "-A", "-A", "-A", "txt");
                });
                it("with three sections (one empty)", () => {
                    tester.test("\\A;\\B;",           "A", "A", "A", "",  "B",  "B",  "B",  "txt");
                    tester.test("[>1]\\A;\\B;",       "A", "A", "",  "",  "B",  "B",  "B",  "txt");
                    tester.test("\\A;[<-1]\\B;",      "A", "A", "A", "",  "-",  "B",  "B",  "txt");
                    tester.test("[>1]\\A;[<-1]\\B;",  "A", "A", "",  "",  "-",  "B",  "B",  "txt");
                    tester.test("[>1]\\A;[>=-1]\\B;", "A", "A", "B", "B", "-B", "-",  "-",  "txt");
                    tester.test("\\A;;\\C",           "A", "A", "A", "C", "",   "",   "",   "txt");
                    tester.test("[>1]\\A;;\\C",       "A", "A", "C", "C", "",   "",   "",   "txt");
                    tester.test("[>=-1]\\A;;\\C",     "A", "A", "A", "A", "-A", "",   "",   "txt");
                    tester.test(";\\B;\\C",           "",  "",  "",  "C", "B",  "B",  "B",  "txt");
                    tester.test(";[<=1]\\B;\\C",      "",  "",  "",  "B", "-B", "-B", "-B", "txt");
                    tester.test(";[<0]\\B;\\C",       "",  "",  "",  "C", "B",  "B",  "B",  "txt");
                    tester.test(";[>1]\\B;\\C",       "",  "",  "",  "C", "-C", "-C", "-C", "txt");
                });
                it("with three sections (two empty)", () => {
                    tester.test("\\A;;",       "A", "A", "A", "",  "",   "",   "",   "txt");
                    tester.test("[>1]\\A;;",   "A", "A", "",  "",  "",   "",   "",   "txt");
                    tester.test("[>0]\\A;;",   "A", "A", "A", "",  "",   "",   "",   "txt");
                    tester.test("[>=-1]\\A;;", "A", "A", "A", "A", "-A", "",   "",   "txt");
                    tester.test(";\\B;",       "",  "",  "",  "",  "B",  "B",  "B",  "txt");
                    tester.test(";[<=1]\\B;",  "",  "",  "",  "B", "-B", "-B", "-B", "txt");
                    tester.test(";[<0]\\B;",   "",  "",  "",  "",  "B",  "B",  "B",  "txt");
                    tester.test(";[>1]\\B;",   "",  "",  "",  "",  "-",  "-",  "-",  "txt");
                    tester.test(";;\\C",       "",  "",  "",  "C", "",   "",   "",   "txt");
                });
                it("with three empty sections", () => {
                    tester.test(";;", "", "", "", "", "", "", "", "txt");
                });
            });

            describe("should format colors", () => {
                const tester = new SeriesTester(1, -1, 0, "txt");
                it("(one section)", () => {
                    tester.test("0",          ["1", undefined], ["-1", undefined], ["0", undefined], ["txt", undefined]);
                    tester.test("[Black]0",   ["1", "black"],   ["-1", "black"],   ["0", "black"],   ["txt", undefined]);
                    tester.test("[Blue]0",    ["1", "blue"],    ["-1", "blue"],    ["0", "blue"],    ["txt", undefined]);
                    tester.test("[Green]0",   ["1", "green"],   ["-1", "green"],   ["0", "green"],   ["txt", undefined]);
                    tester.test("[Cyan]0",    ["1", "cyan"],    ["-1", "cyan"],    ["0", "cyan"],    ["txt", undefined]);
                    tester.test("[Red]0",     ["1", "red"],     ["-1", "red"],     ["0", "red"],     ["txt", undefined]);
                    tester.test("[Magenta]0", ["1", "magenta"], ["-1", "magenta"], ["0", "magenta"], ["txt", undefined]);
                    tester.test("[Yellow]0",  ["1", "yellow"],  ["-1", "yellow"],  ["0", "yellow"],  ["txt", undefined]);
                    tester.test("[White]0",   ["1", "white"],   ["-1", "white"],   ["0", "white"],   ["txt", undefined]);
                });
                it("(two sections)", () => {
                    tester.test("[Blue]0;-0",      ["1", "blue"],    ["-1", undefined], ["0", "blue"],    ["txt", undefined]);
                    tester.test("0;[Red]-0",       ["1", undefined], ["-1", "red"],     ["0", undefined], ["txt", undefined]);
                    tester.test("[Blue]0;[Red]-0", ["1", "blue"],    ["-1", "red"],     ["0", "blue"],    ["txt", undefined]);
                });
                it("(three sections)", () => {
                    tester.test("[Blue]0;-0;0",             ["1", "blue"],    ["-1", undefined], ["0", undefined], ["txt", undefined]);
                    tester.test("0;[Red]-0;0",              ["1", undefined], ["-1", "red"],     ["0", undefined], ["txt", undefined]);
                    tester.test("[Blue]0;[Red]-0;0",        ["1", "blue"],    ["-1", "red"],     ["0", undefined], ["txt", undefined]);
                    tester.test("0;-0;[Green]0",            ["1", undefined], ["-1", undefined], ["0", "green"],   ["txt", undefined]);
                    tester.test("[Blue]0;-0;[Green]0",      ["1", "blue"],    ["-1", undefined], ["0", "green"],   ["txt", undefined]);
                    tester.test("0;[Red]-0;[Green]0",       ["1", undefined], ["-1", "red"],     ["0", "green"],   ["txt", undefined]);
                    tester.test("[Blue]0;[Red]-0;[Green]0", ["1", "blue"],    ["-1", "red"],     ["0", "green"],   ["txt", undefined]);
                });
                it("(text section)", () => {
                    tester.test("[Cyan]@",              ["1", undefined], ["-1", undefined], ["0", undefined], ["txt", "cyan"]);
                    tester.test("[Red]General",         ["1", "red"],     ["-1", "red"],     ["0", "red"],     ["txt", "red"]);
                    tester.test("[Red][>0]General",     ["1", "red"],     null,              null,             ["txt", "red"]);
                    tester.test("[Red]General;-0",      ["1", "red"],     ["-1", undefined], ["0", "red"],     ["txt", "red"]);
                    tester.test("[Red]General;-0;0",    ["1", "red"],     ["-1", undefined], ["0", undefined], ["txt", "red"]);
                    tester.test("[Red]General;@",       ["1", "red"],     ["-1", "red"],     ["0", "red"],     ["txt", undefined]);
                    tester.test("[Red]General;[Cyan]@", ["1", "red"],     ["-1", "red"],     ["0", "red"],     ["txt", "cyan"]);
                });
            });
        });

        describe("method formatNow", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("formatNow");
            });
        });

        describe("method parseFormattedDate", () => {
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("parseFormattedDate");
            });
        });

        describe("method parseFormattedValue", () => {
            const formatter = new BaseFormatter(OOX);
            it("should exist", () => {
                expect(BaseFormatter).toHaveMethod("parseFormattedValue");
            });
            it("should parse text", () => {
                expect(formatter.parseFormattedValue("text")).toEqual({ value: "text", format: 0 });
                expect(formatter.parseFormattedValue("")).toEqual({ value: "", format: 0 });
                expect(formatter.parseFormattedValue("  ")).toEqual({ value: "  ", format: 0 });
            });
            it("should parse decimal numbers without group separator", () => {
                expect(formatter.parseFormattedValue("1234")).toEqual({ value: 1234, format: 0 });
                expect(formatter.parseFormattedValue("+1234")).toEqual({ value: 1234, format: 0 });
                expect(formatter.parseFormattedValue("-1234")).toEqual({ value: -1234, format: 0 });
                expect(formatter.parseFormattedValue("-1234,5")).toEqual({ value: -1234.5, format: 0 });
                expect(formatter.parseFormattedValue(" - 001,00 ")).toEqual({ value: -1, format: 0 });
                expect(formatter.parseFormattedValue("(1234,5)")).toEqual({ value: -1234.5, format: 0 });
                expect(formatter.parseFormattedValue(" ( 001,00 ) ")).toEqual({ value: -1, format: 0 });
                expect(formatter.parseFormattedValue(",5")).toEqual({ value: 0.5, format: 0 });
                expect(formatter.parseFormattedValue("-1,")).toEqual({ value: -1, format: 0 });
                // invalid strings
                expect(formatter.parseFormattedValue("1a")).toEqual({ value: "1a", format: 0 });
                expect(formatter.parseFormattedValue("1, 5")).toEqual({ value: "1, 5", format: 0 });
                expect(formatter.parseFormattedValue("1 ,5")).toEqual({ value: "1 ,5", format: 0 });
                expect(formatter.parseFormattedValue("1-")).toEqual({ value: "1-", format: 0 });
                expect(formatter.parseFormattedValue("+(1)")).toEqual({ value: "+(1)", format: 0 });
                expect(formatter.parseFormattedValue("(-1)")).toEqual({ value: "(-1)", format: 0 });
                expect(formatter.parseFormattedValue("-,")).toEqual({ value: "-,", format: 0 });
            });
            it("should parse decimal numbers with group separator", () => {
                expect(formatter.parseFormattedValue("1.234")).toEqual({ value: 1234, format: 3 });
                expect(formatter.parseFormattedValue("+1.234.567")).toEqual({ value: 1234567, format: 3 });
                expect(formatter.parseFormattedValue("-123.4567")).toEqual({ value: -1234567, format: 3 });
                expect(formatter.parseFormattedValue("-1.234,5")).toEqual({ value: -1234.5, format: 4 });
                expect(formatter.parseFormattedValue("(1.234)")).toEqual({ value: -1234, format: 3 });
                expect(formatter.parseFormattedValue("(1.234,5)")).toEqual({ value: -1234.5, format: 4 });
                // invalid strings
                expect(formatter.parseFormattedValue("1,234.5")).toEqual({ value: "1,234.5", format: 0 });
                expect(formatter.parseFormattedValue("12.34,5")).toEqual({ value: "12.34,5", format: 0 });
            });
            it("should parse scientific notation", () => {
                expect(formatter.parseFormattedValue("42,5e-03")).toEqual({ value: 0.0425, format: 11 });
                expect(formatter.parseFormattedValue("-42,5E+03")).toEqual({ value: -42500, format: 11 });
                expect(formatter.parseFormattedValue("(42,5e3)")).toEqual({ value: -42500, format: 11 });
                // invalid strings
                expect(formatter.parseFormattedValue("42,5 e-03")).toEqual({ value: "42,5 e-03", format: 0 });
                expect(formatter.parseFormattedValue("42,5e -03")).toEqual({ value: "42,5e -03", format: 0 });
                expect(formatter.parseFormattedValue("42,5e")).toEqual({ value: "42,5e", format: 0 });
                expect(formatter.parseFormattedValue("42,5e3,0")).toEqual({ value: "42,5e3,0", format: 0 });
                expect(formatter.parseFormattedValue("42,5e1000")).toEqual({ value: "42,5e1000", format: 0 });
                expect(formatter.parseFormattedValue("42,5e0.001")).toEqual({ value: "42,5e0.001", format: 0 });
            });
            it("should parse fractional notation", () => {
                expect(formatter.parseFormattedValue("1.234 2/5")).toEqual({ value: 1234.4, format: 12 });
                expect(formatter.parseFormattedValue("1.234 4/10")).toEqual({ value: 1234.4, format: 13 });
                expect(formatter.parseFormattedValue(" - 00 02/5 ")).toEqual({ value: -0.4, format: 12 });
                expect(formatter.parseFormattedValue(" ( 0 2/05 ) ")).toEqual({ value: -0.4, format: 12 });
                // invalid strings
                expect(formatter.parseFormattedValue("12.34 2/5")).toEqual({ value: "12.34 2/5", format: 0 });
                expect(formatter.parseFormattedValue("1  2/5")).toEqual({ value: "1  2/5", format: 0 });
                expect(formatter.parseFormattedValue("1 2 /5")).toEqual({ value: "1 2 /5", format: 0 });
                expect(formatter.parseFormattedValue("1 2/ 5")).toEqual({ value: "1 2/ 5", format: 0 });
                expect(formatter.parseFormattedValue("1 2/0")).toEqual({ value: "1 2/0", format: 0 });
            });
            it("should parse percent notation", () => {
                expect(formatter.parseFormattedValue("123%")).toEqual({ value: 1.23, format: 9 });
                expect(formatter.parseFormattedValue("+123,45 %")).toEqual({ value: 1.2345, format: 10 });
                expect(formatter.parseFormattedValue("% -123 2/5")).toEqual({ value: -1.234, format: 10 });
                expect(formatter.parseFormattedValue("-%123e2")).toEqual({ value: -123, format: 10 });
                expect(formatter.parseFormattedValue("(1 ) %")).toEqual({ value: -0.01, format: 9 });
                expect(formatter.parseFormattedValue(" ( % 0 2/05 ) ")).toEqual({ value: -0.004, format: 10 });
                // invalid strings
                expect(formatter.parseFormattedValue("123%%")).toEqual({ value: "123%%", format: 0 });
                expect(formatter.parseFormattedValue("%123%")).toEqual({ value: "%123%", format: 0 });
                expect(formatter.parseFormattedValue("%(1%)")).toEqual({ value: "%(1%)", format: 0 });
            });
            it("should parse currency symbol", () => {
                const EUR_INT_CODE = generateCurrencyCode({ locale: "de-DE", patternType: "currency", negativeRed: true, groupSep: true, fracDigits: 0 });
                const EUR_DEC_CODE = generateCurrencyCode({ locale: "de-DE", patternType: "currency", negativeRed: true, groupSep: true });
                expect(formatter.parseFormattedValue("1€")).toEqual({ value: 1, format: EUR_INT_CODE });
                expect(formatter.parseFormattedValue(" € - 1,0 ")).toEqual({ value: -1, format: EUR_DEC_CODE });
                expect(formatter.parseFormattedValue(" - € 1,0 ")).toEqual({ value: -1, format: EUR_DEC_CODE });
                expect(formatter.parseFormattedValue(" - 1,0 € ")).toEqual({ value: -1, format: EUR_DEC_CODE });
                expect(formatter.parseFormattedValue("-1 2/5€")).toEqual({ value: -1.4, format: EUR_DEC_CODE });
                expect(formatter.parseFormattedValue("€1,23E+2")).toEqual({ value: 123, format: EUR_DEC_CODE });
                expect(formatter.parseFormattedValue("1EUR")).toEqual({ value: 1, format: EUR_INT_CODE });
                expect(formatter.parseFormattedValue("EUR1")).toEqual({ value: 1, format: EUR_INT_CODE });
                const USD_INT_CODE = generateCurrencyCode({ locale: "en-US", patternType: "currency", negativeRed: true, groupSep: true, fracDigits: 0 });
                const USD_DEC_CODE = generateCurrencyCode({ locale: "en-US", patternType: "currency", negativeRed: true, groupSep: true });
                expect(formatter.parseFormattedValue("$(1)")).toEqual({ value: -1, format: USD_INT_CODE });
                expect(formatter.parseFormattedValue(" $ ( 1,0 ) ")).toEqual({ value: -1, format: USD_DEC_CODE });
                expect(formatter.parseFormattedValue(" ( $ 1,0 ) ")).toEqual({ value: -1, format: USD_DEC_CODE });
                expect(formatter.parseFormattedValue(" ( 1,0 $ ) ")).toEqual({ value: -1, format: USD_DEC_CODE });
                expect(formatter.parseFormattedValue(" ( 1,0 ) $ ")).toEqual({ value: -1, format: USD_DEC_CODE });
                expect(formatter.parseFormattedValue("USD1")).toEqual({ value: 1, format: USD_INT_CODE });
                const GBP_INT_CODE = generateCurrencyCode({ locale: "en-GB", patternType: "currency", negativeRed: true, groupSep: true, fracDigits: 0 });
                expect(formatter.parseFormattedValue("£1")).toEqual({ value: 1, format: GBP_INT_CODE });
                expect(formatter.parseFormattedValue("1GBP")).toEqual({ value: 1, format: GBP_INT_CODE });
                // invalid strings
                expect(formatter.parseFormattedValue("1%€")).toEqual({ value: "1%€", format: 0 });
                expect(formatter.parseFormattedValue("-€1,0$")).toEqual({ value: "-€1,0$", format: 0 });
            });

            it("should be able to detect long date strings", () => {
                const formattedValue = formatter.parseFormattedValue("2016-12-31");
                expect(formattedValue.format).toBe(14);
            });
            it("should be able to detect short date strings", () => {
                const formattedValue = formatter.parseFormattedValue(moment().format(SHORT_DATE));
                expect(formattedValue.format).toBe(14);
            });

            const time_1 = moment().format(SHORT_TIME),
                time_2 = moment().format(LONG_TIME),
                time_3 = "01:30 am",
                time_4 = "3 pm",
                time_5 = "3 p",
                time_6 = "12 am",
                time_7 = "12 a";

            it("should be able to detect short time strings", () => {
                const text = time_1,
                    formattedValue = formatter.parseFormattedValue(text);
                expect(formattedValue.format).toBe(20);
            });
            it("should be able to detect long time strings", () => {
                const text = time_2,
                    formattedValue = formatter.parseFormattedValue(text);
                expect(formattedValue.format).toBe(21);
            });
            it("should be able to detect am/pm time strings", () => {
                const formattedValue3 = formatter.parseFormattedValue(time_3),
                    formattedValue4 = formatter.parseFormattedValue(time_4),
                    formattedValue5 = formatter.parseFormattedValue(time_5),
                    formattedValue6 = formatter.parseFormattedValue(time_6),
                    formattedValue7 = formatter.parseFormattedValue(time_7);
                expect(formattedValue3.format).toBe(18);
                expect(formattedValue4.format).toBe(18);
                expect(formattedValue5.format).toBe(18);
                expect(formattedValue6.format).toBe(18);
                expect(formattedValue7.format).toBe(18);
            });

            const date_time_1 = moment().format(SHORT_DATE + " " + SHORT_TIME);

            it("should be able to detect date/time strings", () => {
                const text = date_time_1,
                    formattedValue = formatter.parseFormattedValue(text);
                expect(formattedValue.format).toBe(22);
            });
        });
    });
});
