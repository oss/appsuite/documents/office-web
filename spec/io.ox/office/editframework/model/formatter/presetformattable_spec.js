/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { DObject } from "@/io.ox/office/tk/objects";
import { getDecimalId, getCurrencyId, getPercentId, getScientificId, getTimeId, PresetFormatTable } from "@/io.ox/office/editframework/model/formatter/presetformattable";

// tests ======================================================================

describe("module editframework/model/formatter/presetformattable", () => {

    // public functions -------------------------------------------------------

    describe("function getDecimalId", () => {
        it("should exist", () => {
            expect(getDecimalId).toBeFunction();
        });
        it("should return the identifiers of decimal number formats", () => {
            expect(getDecimalId({ int: true })).toBe(1);
            expect(getDecimalId()).toBe(2);
            expect(getDecimalId({ int: true, group: true })).toBe(3);
            expect(getDecimalId({ group: true })).toBe(4);
        });
    });

    describe("function getCurrencyId", () => {
        it("should exist", () => {
            expect(getCurrencyId).toBeFunction();
        });
        it("should return the identifiers of currency formats", () => {
            expect(getCurrencyId({ int: true })).toBe(5);
            expect(getCurrencyId({ int: true, red: true })).toBe(6);
            expect(getCurrencyId()).toBe(7);
            expect(getCurrencyId({ red: true })).toBe(8);
            expect(getCurrencyId({ int: true, blind: true })).toBe(37);
            expect(getCurrencyId({ int: true, red: true, blind: true })).toBe(38);
            expect(getCurrencyId({ blind: true })).toBe(39);
            expect(getCurrencyId({ red: true, blind: true })).toBe(40);
        });
    });

    describe("function getPercentId", () => {
        it("should exist", () => {
            expect(getPercentId).toBeFunction();
        });
        it("should return the identifiers of percentage formats", () => {
            expect(getPercentId({ int: true })).toBe(9);
            expect(getPercentId()).toBe(10);
        });
    });

    describe("function getScientificId", () => {
        it("should exist", () => {
            expect(getScientificId).toBeFunction();
        });
        it("should return the identifiers of percentage formats", () => {
            expect(getScientificId()).toBe(11);
            expect(getScientificId({ short: true })).toBe(48);
        });
    });

    describe("function getTimeId", () => {
        it("should exist", () => {
            expect(getTimeId).toBeFunction();
        });
        it("should return the identifiers of time formats", () => {
            expect(getTimeId()).toBe(18);
            expect(getTimeId({ seconds: true })).toBe(19);
            expect(getTimeId({ hours24: true })).toBe(20);
            expect(getTimeId({ hours24: true, seconds: true })).toBe(21);
        });
    });

    // class PresetFormatTable ------------------------------------------------

    describe("class PresetFormatTable", () => {

        it("should subclass DOject", () => {
            expect(PresetFormatTable).toBeSubClassOf(DObject);
        });

        describe("method getFormatCode", () => {
            it("should exist", () => {
                expect(PresetFormatTable).toHaveMethod("getFormatCode");
            });
            it("should return the preset format codes", () => {
                var presetTable = new PresetFormatTable(LOCALE_DATA);
                expect(presetTable.getFormatCode(0)).toBe("General");
                expect(presetTable.getFormatCode(1)).toBe("0");
                expect(presetTable.getFormatCode(2)).toBe("0.00");
                expect(presetTable.getFormatCode(3)).toBe("#,##0");
                expect(presetTable.getFormatCode(4)).toBe("#,##0.00");
                expect(presetTable.getFormatCode(5)).toBe('#,##0 "€";-#,##0 "€"');
                expect(presetTable.getFormatCode(6)).toBe('#,##0 "€";[Red]-#,##0 "€"');
                expect(presetTable.getFormatCode(7)).toBe('#,##0.00 "€";-#,##0.00 "€"');
                expect(presetTable.getFormatCode(8)).toBe('#,##0.00 "€";[Red]-#,##0.00 "€"');
                expect(presetTable.getFormatCode(9)).toBe("0%");
                expect(presetTable.getFormatCode(10)).toBe("0.00%");
                expect(presetTable.getFormatCode(11)).toBe("0.00E+00");
                expect(presetTable.getFormatCode(12)).toBe("# ?/?");
                expect(presetTable.getFormatCode(13)).toBe("# ??/??");
                expect(presetTable.getFormatCode(14)).toBe("DD.MM.YYYY");
                expect(presetTable.getFormatCode(15)).toBe("DD. MMM YY");
                expect(presetTable.getFormatCode(16)).toBe("DD. MMM");
                expect(presetTable.getFormatCode(17)).toBe("MMM YY");
                expect(presetTable.getFormatCode(18)).toBe("h:mm AM/PM");
                expect(presetTable.getFormatCode(19)).toBe("h:mm:ss AM/PM");
                expect(presetTable.getFormatCode(20)).toBe("hh:mm");
                expect(presetTable.getFormatCode(21)).toBe("hh:mm:ss");
                expect(presetTable.getFormatCode(22)).toBe("DD.MM.YYYY hh:mm");
                expect(presetTable.getFormatCode(37)).toBe("#,##0 _€;-#,##0 _€");
                expect(presetTable.getFormatCode(38)).toBe("#,##0 _€;[Red]-#,##0 _€");
                expect(presetTable.getFormatCode(39)).toBe("#,##0.00 _€;-#,##0.00 _€");
                expect(presetTable.getFormatCode(40)).toBe("#,##0.00 _€;[Red]-#,##0.00 _€");
                expect(presetTable.getFormatCode(41)).toBe('_-* #,##0 _€_-;-* #,##0 _€_-;_-* "-" _€_-;_-@_-');
                expect(presetTable.getFormatCode(42)).toBe('_-* #,##0 "€"_-;-* #,##0 "€"_-;_-* "-" "€"_-;_-@_-');
                expect(presetTable.getFormatCode(43)).toBe('_-* #,##0.00 _€_-;-* #,##0.00 _€_-;_-* "-"?? _€_-;_-@_-');
                expect(presetTable.getFormatCode(44)).toBe('_-* #,##0.00 "€"_-;-* #,##0.00 "€"_-;_-* "-"?? "€"_-;_-@_-');
                expect(presetTable.getFormatCode(45)).toBe("mm:ss");
                expect(presetTable.getFormatCode(46)).toBe("[h]:mm:ss");
                expect(presetTable.getFormatCode(47)).toBe("mm:ss.0");
                expect(presetTable.getFormatCode(48)).toBe("0.0E+0");
                expect(presetTable.getFormatCode(49)).toBe("@");
            });
        });
    });
});
