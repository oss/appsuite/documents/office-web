/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { DObject } from "@/io.ox/office/tk/objects";
import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";
import { FormatCodeParser } from "@/io.ox/office/editframework/model/formatter/codeparser";

// constants ==================================================================

// private functions ==========================================================

function testDecimalPlaceTransformation(formatCode, formatCodeDecreased, formatCodeIncreased) {
    const codeParser = FormatCodeParser.get();
    const parsedFormat = codeParser.parse(FileFormatType.OOX, formatCode);
    expect(codeParser.transform(parsedFormat)).toBe(formatCode);
    expect(codeParser.transform(parsedFormat, { increaseDecimalPlaces: false })).toBe(formatCodeDecreased);
    expect(codeParser.transform(parsedFormat, { increaseDecimalPlaces: true })).toBe(formatCodeIncreased);
}

// class BaseFormatter ----------------------------------------------------

// tests ======================================================================

describe("module editframework/model/formatter/codeparser", () => {

    describe("class FormatCodeParser", () => {

        it("should subclass DObject", () => {
            expect(FormatCodeParser).toBeSubClassOf(DObject);
        });

        describe("method parse", () => {
            it("should exist", () => {
                expect(FormatCodeParser).toHaveMethod("parse");
            });
        });

        describe("method isTransformable", () => {
            it("should exist", () => {
                expect(FormatCodeParser).toHaveMethod("isTransformable");
            });
        });

        describe("method transform", () => {
            it("should exist", () => {
                expect(FormatCodeParser).toHaveMethod("transform");
            });
            it("should transform decimal places", () => {
                testDecimalPlaceTransformation("General", "General", "0.0");
                testDecimalPlaceTransformation("0", "0", "0.0");
                testDecimalPlaceTransformation("0.0", "0", "0.00");
                testDecimalPlaceTransformation("0.00", "0.0", "0.000");
                testDecimalPlaceTransformation("#,##0", "#,##0", "#,##0.0");
                testDecimalPlaceTransformation("#,##0.00", "#,##0.0", "#,##0.000");
                testDecimalPlaceTransformation("#,##0_);(#,##0)", "#,##0_);(#,##0)", "#,##0.0_);(#,##0.0)");
                testDecimalPlaceTransformation("#,##0_);[red](#,##0)", "#,##0_);[red](#,##0)", "#,##0.0_);[red](#,##0.0)");
                testDecimalPlaceTransformation('#,##0.00_);(#,##0.00)', '#,##0.0_);(#,##0.0)', '#,##0.000_);(#,##0.000)');
                testDecimalPlaceTransformation('#,##0.00_);[Red](#,##0.00)', '#,##0.0_);[Red](#,##0.0)', '#,##0.000_);[Red](#,##0.000)');
                testDecimalPlaceTransformation('$#,##0_);($#,##0)', '$#,##0_);($#,##0)', '$#,##0.0_);($#,##0.0)');
                testDecimalPlaceTransformation('$#,##0_);[Red]($#,##0)', '$#,##0_);[Red]($#,##0)', '$#,##0.0_);[Red]($#,##0.0)');
                testDecimalPlaceTransformation('$#,##0.00_);($#,##0.00)', '$#,##0.0_);($#,##0.0)', '$#,##0.000_);($#,##0.000)');
                testDecimalPlaceTransformation('$#,##0.00_);[Red]($#,##0.00)', '$#,##0.0_);[Red]($#,##0.0)', '$#,##0.000_);[Red]($#,##0.000)');
                testDecimalPlaceTransformation('0%', '0%', '0.0%');
                testDecimalPlaceTransformation('0.00%', '0.0%', '0.000%');
                testDecimalPlaceTransformation('0.00E+00', '0.0E+00', '0.000E+00');
                testDecimalPlaceTransformation('##0.0E+0', '##0E+0', '##0.00E+0');
                testDecimalPlaceTransformation('# ?/?', '# ?/?', '# ?/?');
                testDecimalPlaceTransformation('# ??/??', '# ??/??', '# ??/??');
                testDecimalPlaceTransformation('m/d/yyyy', 'm/d/yyyy', 'm/d/yyyy');
                testDecimalPlaceTransformation('d-mmm-yy', 'd-mmm-yy', 'd-mmm-yy');
                testDecimalPlaceTransformation('d-mmm', 'd-mmm', 'd-mmm');
                testDecimalPlaceTransformation('mmm-yy', 'mmm-yy', 'mmm-yy');
                testDecimalPlaceTransformation('h:mm AM/PM', 'h:mm AM/PM', 'h:mm AM/PM');
                testDecimalPlaceTransformation('h:mm:ss AM/PM', 'h:mm:ss AM/PM', 'h:mm:ss AM/PM');
                testDecimalPlaceTransformation('h:mm', 'h:mm', 'h:mm');
                testDecimalPlaceTransformation('h:mm:ss', 'h:mm:ss', 'h:mm:ss');
                testDecimalPlaceTransformation('m/d/yyyy h:mm', 'm/d/yyyy h:mm', 'm/d/yyyy h:mm');
                testDecimalPlaceTransformation('mm:ss', 'mm:ss', 'mm:ss');
                testDecimalPlaceTransformation('mm:ss.0', 'mm:ss.0', 'mm:ss.0');
                testDecimalPlaceTransformation('@', '@', '@');
                testDecimalPlaceTransformation('[h]:mm:ss', '[h]:mm:ss', '[h]:mm:ss');
                testDecimalPlaceTransformation('_($* #,##0_);_($* (#,##0);_($* "-"_);_(@_)', '_($* #,##0_);_($* (#,##0);_($* "-"_);_(@_)', '_($* #,##0.0_);_($* (#,##0.0);_($* "-"_);_(@_)');
                testDecimalPlaceTransformation('_(* #,##0_);_(* (#,##0);_(* "-"_);_(@_)', '_(* #,##0_);_(* (#,##0);_(* "-"_);_(@_)', '_(* #,##0.0_);_(* (#,##0.0);_(* "-"_);_(@_)');
                testDecimalPlaceTransformation('_($* #,##0.00_);_($* (#,##0.00);_($* "-"??_);_(@_)', '_($* #,##0.0_);_($* (#,##0.0);_($* "-"??_);_(@_)', '_($* #,##0.000_);_($* (#,##0.000);_($* "-"??.0_);_(@_)');
                testDecimalPlaceTransformation('_(* #,##0.00_);_(* (#,##0.00);_(* "-"??_);_(@_)', '_(* #,##0.0_);_(* (#,##0.0);_(* "-"??_);_(@_)', '_(* #,##0.000_);_(* (#,##0.000);_(* "-"??.0_);_(@_)');
                testDecimalPlaceTransformation('_([$ZAR] * #,##0.00_);_([$ZAR] * (#,##0.00);_([$ZAR] * "-"??_);_(@_)', '_([$ZAR] * #,##0.0_);_([$ZAR] * (#,##0.0);_([$ZAR] * "-"??_);_(@_)', '_([$ZAR] * #,##0.000_);_([$ZAR] * (#,##0.000);_([$ZAR] * "-"??.0_);_(@_)');
                testDecimalPlaceTransformation('[=1]0" day";0" days"', '[=1]0" day";0" days"', '[=1]0.0" day";0.0" days"');
                testDecimalPlaceTransformation('[=1]0" day";0.0" days"', '[=1]0" day";0" days"', '[=1]0.0" day";0.00" days"');
                testDecimalPlaceTransformation('[Red][<100]0;[Blue][>=100]0', '[Red][<100]0;[Blue][>=100]0', '[Red][<100]0.0;[Blue][>=100]0.0');
                testDecimalPlaceTransformation('[Red][<100]0.0;[Blue][>=100]0.0', '[Red][<100]0;[Blue][>=100]0', '[Red][<100]0.00;[Blue][>=100]0.00');
                testDecimalPlaceTransformation("#.##", "#.#", "#.###");
                testDecimalPlaceTransformation("#.??", "#.?", "#.???");
                testDecimalPlaceTransformation( // test with max decimal places (127)
                    "0.0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                    "0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                    "0.0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
                testDecimalPlaceTransformation("0.0;-0.0", "0;-0", "0.00;-0.00");
                testDecimalPlaceTransformation("0.0;-0.0;#.#", "0;-0;#", "0.00;-0.00;#.##");
                testDecimalPlaceTransformation("0.0;-0.0;#.#;@", "0;-0;#;@", "0.00;-0.00;#.##;@");
                testDecimalPlaceTransformation(";-0.0", ";-0", ";-0.00");
                testDecimalPlaceTransformation("0.0;", "0;", "0.00;");
                testDecimalPlaceTransformation(";-0.0;@", ";-0;@", ";-0.00;@");
                testDecimalPlaceTransformation("-0.0;;@", "-0;;@", "-0.00;;@");
                testDecimalPlaceTransformation(";", ";", ";");
                testDecimalPlaceTransformation(";;", ";;", ";;");
                testDecimalPlaceTransformation(";;@", ";;@", ";;@");
                testDecimalPlaceTransformation("General;[Red]General", "General;[Red]General", "0.0;[Red]0.0");
            });
        });
    });
});
