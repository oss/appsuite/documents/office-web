/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Font } from "@/io.ox/office/tk/dom";
import { getSignificantDigits, autoFormatNumber } from "@/io.ox/office/editframework/model/formatter/autoformat";

// tests ======================================================================

describe("module editframework/model/formatter/autoformat", () => {

    // functions --------------------------------------------------------------

    describe("function getSignificantDigits", () => {
        it("should exist", () => {
            expect(getSignificantDigits).toBeFunction();
        });
        it("should return the significant digits", () => {

            expect(getSignificantDigits(1.2345,  9, 7)).toEqual({ mant: 1.2345, expn:  9, digits: "12345" });
            expect(getSignificantDigits(1.2345,  1, 7)).toEqual({ mant: 1.2345, expn:  1, digits: "12345" });
            expect(getSignificantDigits(1.2345,  0, 7)).toEqual({ mant: 1.2345, expn:  0, digits: "12345" });
            expect(getSignificantDigits(1.2345, -1, 7)).toEqual({ mant: 1.2345, expn: -1, digits: "12345" });
            expect(getSignificantDigits(1.2345, -9, 7)).toEqual({ mant: 1.2345, expn: -9, digits: "12345" });

            expect(getSignificantDigits(1.0203, 3,  6)).toEqual({ mant: 1.0203, expn: 3,         digits: "10203" });
            expect(getSignificantDigits(1.0203, 3,  5)).toEqual({ mant: 1.0203, expn: 3,         digits: "10203" });
            expect(getSignificantDigits(1.0203, 3,  4)).toEqual({ mant: 1.02,   expn: 3,         digits: "102" });
            expect(getSignificantDigits(1.0203, 3,  3)).toEqual({ mant: 1.02,   expn: 3,         digits: "102" });
            expect(getSignificantDigits(1.0203, 3,  2)).toEqual({ mant: 1,      expn: 3,         digits: "1" });
            expect(getSignificantDigits(1.0203, 3,  1)).toEqual({ mant: 1,      expn: 3,         digits: "1" });
            expect(getSignificantDigits(1.0203, 3,  0)).toEqual({ mant: 0,      expn: -Infinity, digits: "" });
            expect(getSignificantDigits(1.0203, 3, -1)).toEqual({ mant: 0,      expn: -Infinity, digits: "" });
            expect(getSignificantDigits(1.0203, 3, -2)).toEqual({ mant: 0,      expn: -Infinity, digits: "" });

            expect(getSignificantDigits(7.0605, 3,  6)).toEqual({ mant: 7.0605, expn: 3,         digits: "70605" });
            expect(getSignificantDigits(7.0605, 3,  5)).toEqual({ mant: 7.0605, expn: 3,         digits: "70605" });
            expect(getSignificantDigits(7.0605, 3,  4)).toEqual({ mant: 7.061,  expn: 3,         digits: "7061" });
            expect(getSignificantDigits(7.0605, 3,  3)).toEqual({ mant: 7.06,   expn: 3,         digits: "706" });
            expect(getSignificantDigits(7.0605, 3,  2)).toEqual({ mant: 7.1,    expn: 3,         digits: "71" });
            expect(getSignificantDigits(7.0605, 3,  1)).toEqual({ mant: 7,      expn: 3,         digits: "7" });
            expect(getSignificantDigits(7.0605, 3,  0)).toEqual({ mant: 1,      expn: 4,         digits: "1" });
            expect(getSignificantDigits(7.0605, 3, -1)).toEqual({ mant: 0,      expn: -Infinity, digits: "" });
            expect(getSignificantDigits(7.0605, 3, -2)).toEqual({ mant: 0,      expn: -Infinity, digits: "" });

            expect(getSignificantDigits(9.999, 3,  5)).toEqual({ mant: 9.999, expn: 3,         digits: "9999" });
            expect(getSignificantDigits(9.999, 3,  4)).toEqual({ mant: 9.999, expn: 3,         digits: "9999" });
            expect(getSignificantDigits(9.999, 3,  3)).toEqual({ mant: 1,     expn: 4,         digits: "1" });
            expect(getSignificantDigits(9.999, 3,  2)).toEqual({ mant: 1,     expn: 4,         digits: "1" });
            expect(getSignificantDigits(9.999, 3,  1)).toEqual({ mant: 1,     expn: 4,         digits: "1" });
            expect(getSignificantDigits(9.999, 3,  0)).toEqual({ mant: 1,     expn: 4,         digits: "1" });
            expect(getSignificantDigits(9.999, 3, -1)).toEqual({ mant: 0,     expn: -Infinity, digits: "" });
            expect(getSignificantDigits(9.999, 3, -2)).toEqual({ mant: 0,     expn: -Infinity, digits: "" });

            expect(getSignificantDigits(0, -Infinity,  2)).toEqual({ mant: 0, expn: -Infinity, digits: "" });
            expect(getSignificantDigits(0, -Infinity,  0)).toEqual({ mant: 0, expn: -Infinity, digits: "" });
            expect(getSignificantDigits(0, -Infinity, -2)).toEqual({ mant: 0, expn: -Infinity, digits: "" });

            // internal correction of mantissa needed to workaround rounding errors in `Number.toFixed()`
            expect(getSignificantDigits(1.2345, 0, 4)).toEqual({ mant: 1.235, expn: 0, digits: "1235" });
        });
    });

    describe("function autoFormatNumber", () => {

        it("should exist", () => {
            expect(autoFormatNumber).toBeFunction();
        });

        function expectText(result, exp) {
            expect(result).toHaveProperty("text");
            if (exp instanceof RegExp) {
                expect(result.text).toMatch(exp);
            } else {
                expect(result.text).toBe(exp);
            }
        }

        describe("(to length)", () => {

            function testAutoFormat(num, options, expText) {
                const result = autoFormatNumber(num, options);
                expectText(result, expText);
                expect(result).toHaveProperty("width", 0);
            }

            it("should format numbers to decimal notation", () => {
                testAutoFormat(0, { stdLen: 7 }, "0");
                testAutoFormat(1, { stdLen: 7 }, "1");
                testAutoFormat(-1, { stdLen: 7 }, "-1");
                testAutoFormat(12, { stdLen: 7 }, "12");
                testAutoFormat(-12, { stdLen: 7 }, "-12");
                testAutoFormat(123, { stdLen: 7 }, "123");
                testAutoFormat(-123, { stdLen: 7 }, "-123");
                testAutoFormat(1230, { stdLen: 7 }, "1230");
                testAutoFormat(-1230, { stdLen: 7 }, "-1230");
                testAutoFormat(12300, { stdLen: 7 }, "12300");
                testAutoFormat(-12300, { stdLen: 7 }, "-12300");
                testAutoFormat(123000, { stdLen: 7 }, "123000");
                testAutoFormat(-123000, { stdLen: 7 }, "-123000");
                testAutoFormat(1230000, { stdLen: 7 }, "1230000");
                testAutoFormat(-1230000, { stdLen: 7 }, "-1230000");
                testAutoFormat(1e10, { stdLen: 20 }, "10000000000");
                testAutoFormat(-1e10, { stdLen: 20 }, "-10000000000");
                testAutoFormat(9.87654321e307, { stdLen: 310 }, /^9876543210{299}$/);
                testAutoFormat(-9.87654321e307, { stdLen: 310 }, /^-9876543210{299}$/);
                testAutoFormat(12345.6, { stdLen: 7 }, "12345,6");
                testAutoFormat(-12345.6, { stdLen: 7 }, "-12345,6");
                testAutoFormat(1234.56, { stdLen: 7 }, "1234,56");
                testAutoFormat(-1234.56, { stdLen: 7 }, "-1234,56");
                testAutoFormat(123.456, { stdLen: 7 }, "123,456");
                testAutoFormat(-123.456, { stdLen: 7 }, "-123,456");
                testAutoFormat(12.3456, { stdLen: 7 }, "12,3456");
                testAutoFormat(-12.3456, { stdLen: 7 }, "-12,3456");
                testAutoFormat(1.23456, { stdLen: 7 }, "1,23456");
                testAutoFormat(-1.23456, { stdLen: 7 }, "-1,23456");
                testAutoFormat(0.123, { stdLen: 7 }, "0,123");
                testAutoFormat(-0.123, { stdLen: 7 }, "-0,123");
                testAutoFormat(0.0123, { stdLen: 7 }, "0,0123");
                testAutoFormat(-0.0123, { stdLen: 7 }, "-0,0123");
                testAutoFormat(0.00123, { stdLen: 7 }, "0,00123");
                testAutoFormat(-0.00123, { stdLen: 7 }, "-0,00123");
            });
            it("should round fractional part in decimal notation", () => {
                testAutoFormat(0.12345, { stdLen: 6 }, "0,1235");
                testAutoFormat(-0.12345, { stdLen: 6 }, "-0,1235");
                testAutoFormat(0.12345, { stdLen: 5 }, "0,123");
                testAutoFormat(-0.12345, { stdLen: 5 }, "-0,123");
                testAutoFormat(0.12345, { stdLen: 4 }, "0,12");
                testAutoFormat(-0.12345, { stdLen: 4 }, "-0,12");
                testAutoFormat(0.12345, { stdLen: 3 }, "0,1");
                testAutoFormat(-0.12345, { stdLen: 3 }, "-0,1");
                testAutoFormat(0.12345, { stdLen: 2 }, "0");
                testAutoFormat(-0.12345, { stdLen: 2 }, "-0");
                testAutoFormat(0.12345, { stdLen: 1 }, "0");
                testAutoFormat(-0.12345, { stdLen: 1 }, "-0");
                testAutoFormat(123.456, { stdLen: 6 }, "123,46");
                testAutoFormat(-123.456, { stdLen: 6 }, "-123,46");
                testAutoFormat(123.456, { stdLen: 5 }, "123,5");
                testAutoFormat(-123.456, { stdLen: 5 }, "-123,5");
                testAutoFormat(1234.56, { stdLen: 5 }, "1235");
                testAutoFormat(-1234.56, { stdLen: 5 }, "-1235");
                testAutoFormat(1234.56, { stdLen: 4 }, "1235");
                testAutoFormat(-1234.56, { stdLen: 4 }, "-1235");
                testAutoFormat(9999.99, { stdLen: 6 }, "10000");
                testAutoFormat(-9999.99, { stdLen: 6 }, "-10000");
                testAutoFormat(8.9999, { stdLen: 1 }, "9");
                testAutoFormat(-8.9999, { stdLen: 1 }, "-9");
                testAutoFormat(0.9999, { stdLen: 1 }, "1");
                testAutoFormat(-0.9999, { stdLen: 1 }, "-1");
                testAutoFormat(0.09999, { stdLen: 1 }, "0");
                testAutoFormat(-0.09999, { stdLen: 1 }, "-0");
            });
            it("should format numbers to scientific notation", () => {
                testAutoFormat(3450000000, { stdLen: 10 }, "3450000000");
                testAutoFormat(-3450000000, { stdLen: 10 }, "-3450000000");
                testAutoFormat(3450000000, { stdLen: 9 }, "3,45E+09");
                testAutoFormat(-3450000000, { stdLen: 9 }, "-3,45E+09");
                testAutoFormat(3450000000, { stdLen: 8 }, "3,45E+09");
                testAutoFormat(-3450000000, { stdLen: 8 }, "-3,45E+09");
                testAutoFormat(3450000000, { stdLen: 7 }, "3,5E+09");
                testAutoFormat(-3450000000, { stdLen: 7 }, "-3,5E+09");
                testAutoFormat(3450000000, { stdLen: 6 }, "3E+09");
                testAutoFormat(-3450000000, { stdLen: 6 }, "-3E+09");
                testAutoFormat(3450000000, { stdLen: 5 }, "3E+09");
                testAutoFormat(-3450000000, { stdLen: 5 }, "-3E+09");
                testAutoFormat(9876000000, { stdLen: 5 }, "1E+10");
                testAutoFormat(-9876000000, { stdLen: 5 }, "-1E+10");
                testAutoFormat(0.00003456, { stdLen: 10 }, "0,00003456");
                testAutoFormat(-0.00003456, { stdLen: 10 }, "-0,00003456");
                testAutoFormat(0.00003456, { stdLen: 9 }, "3,456E-05");
                testAutoFormat(-0.00003456, { stdLen: 9 }, "-3,456E-05");
                testAutoFormat(0.00003456, { stdLen: 8 }, "3,46E-05");
                testAutoFormat(-0.00003456, { stdLen: 8 }, "-3,46E-05");
                testAutoFormat(0.00003456, { stdLen: 7 }, "3,5E-05");
                testAutoFormat(-0.00003456, { stdLen: 7 }, "-3,5E-05");
                testAutoFormat(0.00003456, { stdLen: 6 }, "3E-05");
                testAutoFormat(-0.00003456, { stdLen: 6 }, "-3E-05");
                testAutoFormat(0.00003456, { stdLen: 5 }, "3E-05");
                testAutoFormat(-0.00003456, { stdLen: 5 }, "-3E-05");
                testAutoFormat(0.00003456, { stdLen: 4 }, "0");
                testAutoFormat(-0.00003456, { stdLen: 4 }, "-0");
                testAutoFormat(0.00003456, { stdLen: 1 }, "0");
                testAutoFormat(-0.00003456, { stdLen: 1 }, "-0");
                testAutoFormat(1e-100, { stdLen: 5 }, "0");
                testAutoFormat(-1e-100, { stdLen: 5 }, "-0");
            });
            it("should be able to round up to an exponent with more digits", () => {
                testAutoFormat(9.99e99, { stdLen: 8 }, "9,99E+99");
                testAutoFormat(9.99e99, { stdLen: 7 }, "1E+100");
            });
            it("should prefer decimal notation over scientific notation", () => {
                testAutoFormat(0.001, { stdLen: 5 }, "0,001");
                testAutoFormat(10000, { stdLen: 5 }, "10000");
            });
            it("should fail for large integral parts", () => {
                testAutoFormat(12345, { stdLen: 4 }, null);
                testAutoFormat(1234, { stdLen: 3 }, null);
                testAutoFormat(123, { stdLen: 2 }, null);
                testAutoFormat(12, { stdLen: 1 }, null);
                testAutoFormat(999.5, { stdLen: 3 }, null);
                testAutoFormat(99.5, { stdLen: 2 }, null);
                testAutoFormat(9.5, { stdLen: 1 }, null);
                testAutoFormat(9.49999999999, { stdLen: 1 }, "9");
            });
            it("should format decimal notation for all powers of 10", () => {
                for (let i = 1; i <= 307; i += 1) {
                    testAutoFormat(Number("1e+" + i), { stdLen: 310 }, new RegExp("^10{" + i + "}$"));
                    testAutoFormat(Number("1e-" + i), { stdLen: 310 }, new RegExp("^0,0{" + (i - 1) + "}1$"));
                }
            });
            it("should format scientific notation for all powers of 10", () => {
                for (let i = 21; i <= 307; i += 1) {
                    testAutoFormat(Number("1e+" + i), { stdLen: 21 }, new RegExp("^1E\\+" + i + "$"));
                    testAutoFormat(Number("1e-" + i), { stdLen: 21 }, new RegExp("^1E-" + i + "$"));
                }
            });
            it("should format with a custom exponent length", () => {
                testAutoFormat(1e9, { stdLen: 8, expLen: 1 }, "1E+9");
                testAutoFormat(1e9, { stdLen: 8, expLen: 2 }, "1E+09");
                testAutoFormat(1e9, { stdLen: 8, expLen: 3 }, "1E+009");
                testAutoFormat(1e9, { stdLen: 8, expLen: 4 }, "1E+0009");
                testAutoFormat(1e19, { stdLen: 8, expLen: 1 }, "1E+19");
                testAutoFormat(1e19, { stdLen: 8, expLen: 2 }, "1E+19");
                testAutoFormat(1e19, { stdLen: 8, expLen: 3 }, "1E+019");
                testAutoFormat(1e19, { stdLen: 8, expLen: 4 }, "1E+0019");
                testAutoFormat(1e199, { stdLen: 8, expLen: 1 }, "1E+199");
                testAutoFormat(1e199, { stdLen: 8, expLen: 2 }, "1E+199");
                testAutoFormat(1e199, { stdLen: 8, expLen: 3 }, "1E+199");
                testAutoFormat(1e199, { stdLen: 8, expLen: 4 }, "1E+0199");
            });
            it("should use scientific notation for specific edge cases", () => {
                testAutoFormat(5e-100, { stdLen: 6 }, "5E-100");
                testAutoFormat(5e-100, { stdLen: 5 }, "1E-99");
                testAutoFormat(5e-10, { stdLen: 5 }, "5E-10");
                testAutoFormat(5e-10, { stdLen: 4 }, "0");
                testAutoFormat(5e-10, { stdLen: 4, expLen: 1 }, "1E-9");
                testAutoFormat(5e-10, { stdLen: 3, expLen: 1 }, "0");
            });
            it("should use default length", () => {
                testAutoFormat(12345678901, undefined, "12345678901");
                testAutoFormat(12345678901, { stdLen: 11 }, "12345678901");
                testAutoFormat(12345678901, { stdLen: 10 }, "1,2346E+10");
            });
            it("should use custom decimal separator", () => {
                testAutoFormat(1.5, { dec: "." }, "1.5");
                testAutoFormat(1.5, { dec: "," }, "1,5");
                testAutoFormat(1.5, { dec: "|" }, "1|5");
            });
            it("should use custom exponent sign", () => {
                testAutoFormat(1e+99, { expSign: "exp" }, "1exp+99");
                testAutoFormat(1e-99, { expSign: "exp" }, "1exp-99");
            });
        });

        describe("(to width)", () => {

            const font = new Font("times", 10, false, false);

            function testAutoFormat(num, width, options, expText, expWidth) {
                const result = autoFormatNumber(num, { renderFont: font, maxWidth: width, ...options });
                expectText(result, expText);
                expect(result.width).toBeNumber();
                if (expWidth) { expect(result.width).toBe(expWidth); }
            }

            it("should format positive numbers in decimal notation and round to pixel width", () => {
                const w1 = font.getTextWidth("0,12345"), w2 = font.getTextWidth("0,1235"), w3 = font.getTextWidth("0,123");
                const w4 = font.getTextWidth("0,12"), w5 = font.getTextWidth("0,1"), w6 = font.getTextWidth("0");
                const n = 0.12345;
                testAutoFormat(0, w1,       undefined,     "0",       w6);
                testAutoFormat(n, w1,       undefined,     "0,12345", w1);
                testAutoFormat(n, w1 - 0.1, undefined,     "0,1235",  w2);
                testAutoFormat(n, w2,       undefined,     "0,1235");
                testAutoFormat(n, w2 - 0.1, undefined,     "0,123");
                testAutoFormat(n, w3,       undefined,     "0,123");
                testAutoFormat(n, w3 - 0.1, undefined,     "0,12");
                testAutoFormat(n, w4,       undefined,     "0,12");
                testAutoFormat(n, w4 - 0.1, undefined,     "0,1");
                testAutoFormat(n, w5,       undefined,     "0,1");
                testAutoFormat(n, w5 - 0.1, undefined,     "0");
                testAutoFormat(n, w6,       undefined,     "0");
                testAutoFormat(n, w6 - 0.1, undefined,     null);
                testAutoFormat(n, w1,       { stdLen: 5 }, "0,123");
                testAutoFormat(n, w2,       { stdLen: 5 }, "0,123");
                testAutoFormat(n, w3,       { stdLen: 5 }, "0,123");
                testAutoFormat(n, w4,       { stdLen: 5 }, "0,12");
                testAutoFormat(n, w5,       { stdLen: 5 }, "0,1");
                testAutoFormat(n, w6,       { stdLen: 5 }, "0");
            });
            it("should format negative numbers in decimal notation and round to pixel width", () => {
                const w1 = font.getTextWidth("-0,12345"), w2 = font.getTextWidth("-0,1235"), w3 = font.getTextWidth("-0,123");
                const w4 = font.getTextWidth("-0,12"), w5 = font.getTextWidth("-0,1"), w6 = font.getTextWidth("-0");
                const n = -0.12345;
                testAutoFormat(n, w1,       undefined,     "-0,12345", w1);
                testAutoFormat(n, w1 - 0.1, undefined,     "-0,1235",  w2);
                testAutoFormat(n, w2,       undefined,     "-0,1235");
                testAutoFormat(n, w2 - 0.1, undefined,     "-0,123");
                testAutoFormat(n, w3,       undefined,     "-0,123");
                testAutoFormat(n, w3 - 0.1, undefined,     "-0,12");
                testAutoFormat(n, w4,       undefined,     "-0,12");
                testAutoFormat(n, w4 - 0.1, undefined,     "-0,1");
                testAutoFormat(n, w5,       undefined,     "-0,1");
                testAutoFormat(n, w5 - 0.1, undefined,     "-0");
                testAutoFormat(n, w6,       undefined,     "-0");
                testAutoFormat(n, w6 - 0.1, undefined,     null);
                testAutoFormat(n, w1,       { stdLen: 5 }, "-0,123");
                testAutoFormat(n, w2,       { stdLen: 5 }, "-0,123");
                testAutoFormat(n, w3,       { stdLen: 5 }, "-0,123");
                testAutoFormat(n, w4,       { stdLen: 5 }, "-0,12");
                testAutoFormat(n, w5,       { stdLen: 5 }, "-0,1");
                testAutoFormat(n, w6,       { stdLen: 5 }, "-0");
            });
            it("should format positive numbers in scientific notation and round to pixel width", () => {
                const w1 = font.getTextWidth("0,00000345"), w2 = font.getTextWidth("3,45E-06");
                const w3 = font.getTextWidth("3,5E-06"), w4 = font.getTextWidth("3E-06"), w5 = font.getTextWidth("0");
                const n = 0.00000345;
                testAutoFormat(n, w1,       undefined, "0,00000345", w1);
                testAutoFormat(n, w1 - 0.1, undefined, "3,45E-06", w2);
                testAutoFormat(n, w2,       undefined, "3,45E-06");
                testAutoFormat(n, w2 - 0.1, undefined, "3,5E-06");
                testAutoFormat(n, w3,       undefined, "3,5E-06");
                testAutoFormat(n, w3 - 0.1, undefined, "3E-06");
                testAutoFormat(n, w4,       undefined, "3E-06");
                testAutoFormat(n, w4 - 0.1, undefined, "0", w5);
                testAutoFormat(n, w5,       undefined, "0");
                testAutoFormat(n, w5 - 0.1, undefined, null);
            });
            it("should format negative numbers in scientific notation and round to pixel width", () => {
                const w1 = font.getTextWidth("-0,00000345"), w2 = font.getTextWidth("-3,45E-06");
                const w3 = font.getTextWidth("-3,5E-06"), w4 = font.getTextWidth("-3E-06"), w5 = font.getTextWidth("-0");
                const n = -0.00000345;
                testAutoFormat(n, w1,       undefined, "-0,00000345", w1);
                testAutoFormat(n, w1 - 0.1, undefined, "-3,45E-06", w2);
                testAutoFormat(n, w2,       undefined, "-3,45E-06");
                testAutoFormat(n, w2 - 0.1, undefined, "-3,5E-06");
                testAutoFormat(n, w3,       undefined, "-3,5E-06");
                testAutoFormat(n, w3 - 0.1, undefined, "-3E-06");
                testAutoFormat(n, w4,       undefined, "-3E-06");
                testAutoFormat(n, w4 - 0.1, undefined, "-0");
                testAutoFormat(n, w5,       undefined, "-0");
                testAutoFormat(n, w5 - 0.1, undefined, null);
            });
            it("should prefer scientific notation for large numbers not fitting into the pixel width", () => {
                const w1 = font.getTextWidth("10000000000"), w2 = font.getTextWidth("1E+10");
                testAutoFormat(1e10, w1,       undefined, "10000000000");
                testAutoFormat(1e10, w1 - 0.1, undefined, "1E+10");
                testAutoFormat(1e10, w2,       undefined, "1E+10");
                testAutoFormat(1e10, w2 - 0.1, undefined, null);
            });
            it("should use scientific notation for specific edge cases", () => {
                testAutoFormat(5e-100, font.getTextWidth("5E-100"),      undefined,     "5E-100");
                testAutoFormat(5e-100, font.getTextWidth("1E-99"),       undefined,     "1E-99");
                testAutoFormat(5e-100, font.getTextWidth("1E-99") - 0.1, undefined,     "0");
                testAutoFormat(5e-10,  font.getTextWidth("1E-9"),        { expLen: 1 }, "1E-9");
                testAutoFormat(5e-10,  font.getTextWidth("1E-9"),        undefined,     "0");
            });
            it("should use custom decimal separator", () => {
                testAutoFormat(1.5, 100, { dec: "." }, "1.5");
                testAutoFormat(1.5, 100, { dec: "," }, "1,5");
                testAutoFormat(1.5, 100, { dec: "|" }, "1|5");
            });
            it("should use custom exponent sign", () => {
                testAutoFormat(1e+99, 100, { expSign: "exp" }, "1exp+99");
                testAutoFormat(1e-99, 100, { expSign: "exp" }, "1exp-99");
            });
        });
    });
});
