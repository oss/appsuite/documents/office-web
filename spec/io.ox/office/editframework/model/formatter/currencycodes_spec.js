/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as currencycodes from "@/io.ox/office/editframework/model/formatter/currencycodes";

// tests ======================================================================

describe("module editframework/model/formatter/currencycodes", () => {

    // functions --------------------------------------------------------------

    describe("function generateCurrencyCode", () => {
        const { generateCurrencyCode } = currencycodes;
        it("should exist", () => {
            expect(generateCurrencyCode).toBeFunction();
        });
        it("should return default format code", () => {
            expect(generateCurrencyCode()).toBe('0.00 "€"');
        });
        it("should return format codes with integer length", () => {
            expect(generateCurrencyCode({ intDigits: 0 })).toBe('#.00 "€"');
            expect(generateCurrencyCode({ intDigits: 1 })).toBe('0.00 "€"');
            expect(generateCurrencyCode({ intDigits: 2 })).toBe('00.00 "€"');
            expect(generateCurrencyCode({ intDigits: 3 })).toBe('000.00 "€"');
            expect(generateCurrencyCode({ intDigits: 4 })).toBe('0000.00 "€"');
            expect(generateCurrencyCode({ intDigits: 5 })).toBe('00000.00 "€"');
            expect(generateCurrencyCode({ intDigits: 10 })).toBe('0000000000.00 "€"');
        });
        it("should return format codes with grouping separator", () => {
            expect(generateCurrencyCode({ groupSep: true, intDigits: 0 })).toBe('#,###.00 "€"');
            expect(generateCurrencyCode({ groupSep: true, intDigits: 1 })).toBe('#,##0.00 "€"');
            expect(generateCurrencyCode({ groupSep: true, intDigits: 2 })).toBe('#,#00.00 "€"');
            expect(generateCurrencyCode({ groupSep: true, intDigits: 3 })).toBe('#,000.00 "€"');
            expect(generateCurrencyCode({ groupSep: true, intDigits: 4 })).toBe('0,000.00 "€"');
            expect(generateCurrencyCode({ groupSep: true, intDigits: 5 })).toBe('00,000.00 "€"');
            expect(generateCurrencyCode({ groupSep: true, intDigits: 10 })).toBe('0,000,000,000.00 "€"');
        });
        it("should return format codes with fraction length", () => {
            expect(generateCurrencyCode({ fracDigits: 0 })).toBe('0 "€"');
            expect(generateCurrencyCode({ fracDigits: 1 })).toBe('0.0 "€"');
            expect(generateCurrencyCode({ fracDigits: 2 })).toBe('0.00 "€"');
            expect(generateCurrencyCode({ fracDigits: 3 })).toBe('0.000 "€"');
            expect(generateCurrencyCode({ fracDigits: 10 })).toBe('0.0000000000 "€"');
        });
        it("should return format codes with red modifier", () => {
            expect(generateCurrencyCode({ negativeRed: true })).toBe('0.00 "€";[Red]-0.00 "€"');
        });
        it("should return format codes with number modifier", () => {
            expect(generateCurrencyCode({ digitModifier: "t" })).toBe('t0.00 "€";t-0.00 "€"');
            expect(generateCurrencyCode({ digitModifier: "t", negativeRed: true })).toBe('t0.00 "€";[Red]t-0.00 "€"');
        });
        it("should return format codes with specific currency symbols", () => {
            const currencyData = { symbol: "@", iso: "@@@" };
            expect(generateCurrencyCode({ currencyData })).toBe('0.00 "@"');
            expect(generateCurrencyCode({ isoCurrency: true })).toBe('0.00 "EUR"');
            expect(generateCurrencyCode({ currencyData, isoCurrency: true })).toBe('0.00 "@@@"');
            expect(generateCurrencyCode({ symbolModifier: "blind" })).toBe('0.00 _€');
            expect(generateCurrencyCode({ symbolModifier: "blind", currencyData })).toBe('0.00 _@');
            expect(generateCurrencyCode({ symbolModifier: "blind", isoCurrency: true })).toBe('0.00 _E_U_R');
            expect(generateCurrencyCode({ symbolModifier: "blind", currencyData, isoCurrency: true })).toBe('0.00 _@_@_@');
            expect(generateCurrencyCode({ symbolModifier: "locale" })).toBe('0.00 [$€-de-DE]');
            expect(generateCurrencyCode({ symbolModifier: "locale", currencyData })).toBe('0.00 [$@-de-DE]');
            expect(generateCurrencyCode({ symbolModifier: "locale", isoCurrency: true })).toBe('0.00 [$EUR-de-DE]');
            expect(generateCurrencyCode({ symbolModifier: "locale", currencyData, isoCurrency: true })).toBe('0.00 [$@@@-de-DE]');
        });
        it("should return format codes for symbol position and spacing", () => {
            expect(generateCurrencyCode({ trailingSymbol: false })).toBe('"€" 0.00');
            expect(generateCurrencyCode({ trailingSymbol: false, negativeRed: true })).toBe('"€" 0.00;[Red]-"€" 0.00');
            expect(generateCurrencyCode({ spaceSeparator: false })).toBe('0.00"€"');
            expect(generateCurrencyCode({ spaceSeparator: false, negativeRed: true })).toBe('0.00"€";[Red]-0.00"€"');
        });
        it("should return format codes for pattern types", () => {
            expect(generateCurrencyCode({ patternType: "currency" })).toBe('0.00 "€";-0.00 "€"');
            expect(generateCurrencyCode({ patternType: "currency", symbolModifier: "blind", negativeRed: true, fracDigits: 0 })).toBe('0 _€;[Red]-0 _€');
            expect(generateCurrencyCode({ patternType: "accounting" })).toBe('_-* 0.00 "€"_-;-* 0.00 "€"_-;_-* "-"?? "€"_-;_-@_-');
            expect(generateCurrencyCode({ patternType: "accounting", symbolModifier: "blind", negativeRed: true, fracDigits: 0 })).toBe('_-* 0 _€_-;[Red]-* 0 _€_-;_-* "-" _€_-;_-@_-');
        });
        it("should return format codes for negative modes", () => {
            expect(generateCurrencyCode({ patternType: "currency", negativeMode: "leading" })).toBe('0.00 "€";-0.00 "€"');
            expect(generateCurrencyCode({ patternType: "currency", negativeMode: "between" })).toBe('0.00_- "€";0.00- "€"');
            expect(generateCurrencyCode({ patternType: "currency", negativeMode: "trailing" })).toBe('0.00 "€"_-;0.00 "€"-');
            expect(generateCurrencyCode({ patternType: "currency", negativeMode: "parentheses" })).toBe('0.00 "€"_);(0.00 "€")');
            expect(generateCurrencyCode({ patternType: "accounting", negativeMode: "leading" })).toBe('_-* 0.00 "€"_-;-* 0.00 "€"_-;_-* "-"?? "€"_-;_-@_-');
            expect(generateCurrencyCode({ patternType: "accounting", negativeMode: "between" })).toBe('_ * 0.00_- "€"_ ;_ * 0.00- "€"_ ;_ * "-"??_- "€"_ ;_ @_ ');
            expect(generateCurrencyCode({ patternType: "accounting", negativeMode: "trailing" })).toBe('_-* 0.00 "€"_-;_-* 0.00 "€"-;_-* "-"?? "€"_-;_-@_-');
            expect(generateCurrencyCode({ patternType: "accounting", negativeMode: "parentheses" })).toBe('_ * 0.00_) "€"_ ;_ * (0.00) "€"_ ;_ * "-"??_) "€"_ ;_ @_ ');
        });
        it("should return format codes with custom locale", () => {
            expect(generateCurrencyCode({ locale: "de-AT" })).toBe('"€" 0.00');
            expect(generateCurrencyCode({ locale: "de-AT", negativeRed: true })).toBe('"€" 0.00;[Red]-"€" 0.00');
            expect(generateCurrencyCode({ locale: "de-AT", patternType: "currency" })).toBe('"€" 0.00;-"€" 0.00');
            expect(generateCurrencyCode({ locale: "de-AT", patternType: "accounting" })).toBe('_-"€" * 0.00_-;-"€" * 0.00_-;_-"€" * "-"??_-;_-@_-');
            expect(generateCurrencyCode({ locale: "de-AT", symbolModifier: "locale" })).toBe("[$€-de-AT] 0.00");
            expect(generateCurrencyCode({ locale: "en-US" })).toBe('"$"0.00');
            expect(generateCurrencyCode({ locale: "en-US", negativeRed: true })).toBe('"$"0.00;[Red]-"$"0.00');
            expect(generateCurrencyCode({ locale: "en-US", patternType: "currency" })).toBe('"$"0.00_);("$"0.00)');
            expect(generateCurrencyCode({ locale: "en-US", patternType: "accounting" })).toBe('_("$"* 0.00_);_("$"* (0.00);_("$"* "-"??_);_(@_)');
            expect(generateCurrencyCode({ locale: "en-US", symbolModifier: "locale" })).toBe("[$$-en-US]0.00");
            expect(generateCurrencyCode({ locale: "fr-CA" })).toBe('0.00 "$"');
            expect(generateCurrencyCode({ locale: "fr-CA", negativeRed: true })).toBe('0.00 "$";[Red]-0.00 "$"');
            expect(generateCurrencyCode({ locale: "fr-CA", patternType: "currency" })).toBe('0.00 "$"_);(0.00 "$")');
            expect(generateCurrencyCode({ locale: "fr-CA", patternType: "accounting" })).toBe('_ * 0.00_) "$"_ ;_ * (0.00) "$"_ ;_ * "-"??_) "$"_ ;_ @_ ');
            expect(generateCurrencyCode({ locale: "fr-CA", symbolModifier: "locale" })).toBe("0.00 [$$-fr-CA]");
        });
    });
});
