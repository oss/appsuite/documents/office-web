/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { FormatCategory } from "@/io.ox/office/editframework/model/formatter/parsedsection";
import { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";

// tests ======================================================================

describe("module editframework/model/formatter/parsedformat", () => {

    // class ParsedFormat -----------------------------------------------------

    describe("class ParsedFormat", () => {
        it("should exist", () => {
            expect(ParsedFormat).toBeClass();
        });

        const stdFormat = new ParsedFormat("", FormatCategory.STANDARD);
        const pctFormat1 = new ParsedFormat("", FormatCategory.PERCENT);
        const pctFormat2 = new ParsedFormat("", FormatCategory.PERCENT);
        const dateFormat = new ParsedFormat("", FormatCategory.DATE);
        const timeFormat = new ParsedFormat("", FormatCategory.TIME);
        const dateTimeFormat = new ParsedFormat("", FormatCategory.DATETIME);
        const textFormat = new ParsedFormat("", FormatCategory.TEXT);
        const errFormat = new ParsedFormat("", FormatCategory.ERROR);

        describe("method getSection", () => {
            it("should exist", () => {
                expect(ParsedFormat).toHaveMethod("getSection");
            });
        });

        describe("method isError", () => {
            it("should exist", () => {
                expect(ParsedFormat).toHaveMethod("isError");
            });
            it("should return true for error formats only", () => {
                expect(stdFormat.isError()).toBeFalse();
                expect(pctFormat1.isError()).toBeFalse();
                expect(pctFormat2.isError()).toBeFalse();
                expect(dateFormat.isError()).toBeFalse();
                expect(timeFormat.isError()).toBeFalse();
                expect(dateTimeFormat.isError()).toBeFalse();
                expect(textFormat.isError()).toBeFalse();
                expect(errFormat.isError()).toBeTrue();
            });
        });

        describe("method isStandard", () => {
            it("should exist", () => {
                expect(ParsedFormat).toHaveMethod("isStandard");
            });
            it("should return true for standard formats only", () => {
                expect(stdFormat.isStandard()).toBeTrue();
                expect(pctFormat1.isStandard()).toBeFalse();
                expect(pctFormat2.isStandard()).toBeFalse();
                expect(dateFormat.isStandard()).toBeFalse();
                expect(timeFormat.isStandard()).toBeFalse();
                expect(dateTimeFormat.isStandard()).toBeFalse();
                expect(textFormat.isStandard()).toBeFalse();
                expect(errFormat.isStandard()).toBeFalse();
            });
        });

        describe("method isAnyDate", () => {
            it("should exist", () => {
                expect(ParsedFormat).toHaveMethod("isAnyDate");
            });
            it("should return true for date and date/time formats only", () => {
                expect(stdFormat.isAnyDate()).toBeFalse();
                expect(pctFormat1.isAnyDate()).toBeFalse();
                expect(pctFormat2.isAnyDate()).toBeFalse();
                expect(dateFormat.isAnyDate()).toBeTrue();
                expect(timeFormat.isAnyDate()).toBeFalse();
                expect(dateTimeFormat.isAnyDate()).toBeTrue();
                expect(textFormat.isAnyDate()).toBeFalse();
                expect(errFormat.isAnyDate()).toBeFalse();
            });
        });

        describe("method isAnyTime", () => {
            it("should exist", () => {
                expect(ParsedFormat).toHaveMethod("isAnyTime");
            });
            it("should return true for time and date/time formats only", () => {
                expect(stdFormat.isAnyTime()).toBeFalse();
                expect(pctFormat1.isAnyTime()).toBeFalse();
                expect(pctFormat2.isAnyTime()).toBeFalse();
                expect(dateFormat.isAnyTime()).toBeFalse();
                expect(timeFormat.isAnyTime()).toBeTrue();
                expect(dateTimeFormat.isAnyTime()).toBeTrue();
                expect(textFormat.isAnyTime()).toBeFalse();
                expect(errFormat.isAnyTime()).toBeFalse();
            });
        });

        describe("method isAnyDateTime", () => {
            it("should exist", () => {
                expect(ParsedFormat).toHaveMethod("isAnyDateTime");
            });
            it("should return true for date and time formats only", () => {
                expect(stdFormat.isAnyDateTime()).toBeFalse();
                expect(pctFormat1.isAnyDateTime()).toBeFalse();
                expect(pctFormat2.isAnyDateTime()).toBeFalse();
                expect(dateFormat.isAnyDateTime()).toBeTrue();
                expect(timeFormat.isAnyDateTime()).toBeTrue();
                expect(dateTimeFormat.isAnyDateTime()).toBeTrue();
                expect(textFormat.isAnyDateTime()).toBeFalse();
                expect(errFormat.isAnyDateTime()).toBeFalse();
            });
        });

        describe("method isText", () => {
            it("should exist", () => {
                expect(ParsedFormat).toHaveMethod("isText");
            });
            it("should return true for standard formats only", () => {
                expect(stdFormat.isText()).toBeFalse();
                expect(pctFormat1.isText()).toBeFalse();
                expect(pctFormat2.isText()).toBeFalse();
                expect(dateFormat.isText()).toBeFalse();
                expect(timeFormat.isText()).toBeFalse();
                expect(dateTimeFormat.isText()).toBeFalse();
                expect(textFormat.isText()).toBeTrue();
                expect(errFormat.isText()).toBeFalse();
            });
        });

        describe("method hasEqualCategory", () => {
            it("should exist", () => {
                expect(ParsedFormat).toHaveMethod("hasEqualCategory");
            });
            it("should return true for equal categories only", () => {
                expect(stdFormat.hasEqualCategory(stdFormat)).toBeTrue();
                expect(stdFormat.hasEqualCategory(pctFormat1)).toBeFalse();
                expect(stdFormat.hasEqualCategory(pctFormat2)).toBeFalse();
                expect(stdFormat.hasEqualCategory(dateFormat)).toBeFalse();
                expect(stdFormat.hasEqualCategory(timeFormat)).toBeFalse();
                expect(stdFormat.hasEqualCategory(dateTimeFormat)).toBeFalse();
                expect(pctFormat1.hasEqualCategory(pctFormat2)).toBeTrue();
                expect(dateFormat.hasEqualCategory(timeFormat)).toBeFalse();
                expect(timeFormat.hasEqualCategory(dateFormat)).toBeFalse();
                expect(dateFormat.hasEqualCategory(dateTimeFormat)).toBeFalse();
                expect(timeFormat.hasEqualCategory(dateTimeFormat)).toBeFalse();
                expect(errFormat.hasEqualCategory(dateTimeFormat)).toBeFalse();
            });
            it("should match any date/time categories", () => {
                expect(stdFormat.hasEqualCategory(dateFormat, { anyDateTime: true })).toBeFalse();
                expect(dateFormat.hasEqualCategory(timeFormat, { anyDateTime: true })).toBeTrue();
                expect(timeFormat.hasEqualCategory(dateFormat, { anyDateTime: true })).toBeTrue();
                expect(dateFormat.hasEqualCategory(dateTimeFormat, { anyDateTime: true })).toBeTrue();
                expect(timeFormat.hasEqualCategory(dateTimeFormat, { anyDateTime: true })).toBeTrue();
            });
        });
    });
});
