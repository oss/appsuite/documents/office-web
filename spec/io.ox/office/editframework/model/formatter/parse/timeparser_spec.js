/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TimeParser } from "@/io.ox/office/editframework/model/formatter/parse/timeparser";

// functions ==================================================================

function utc(h, m, s, ms) {
    return new Date(Date.UTC(1970, 0, 1, h, m, s, ms || 0));
}

// tests ======================================================================

describe("module editframework/model/formatter/parse/timeparser", () => {

    // class TimeParser -------------------------------------------------------

    describe("class TimeParser", () => {
        it("should exist", () => {
            expect(TimeParser).toBeClass();
        });

        describe("static function parseTime", () => {
            it("should exist", () => {
                expect(TimeParser).toHaveStaticMethod("parseTime");
            });
            it("should parse times in format h:m:s", () => {
                expect(TimeParser.parseTime("0:0:0abc")).toEqual({ text: "0:0:0", remaining: "abc", time: utc(0, 0, 0), serial: 0, hours12: false, seconds: true, fraction: false });
                expect(TimeParser.parseTime("23:59:59abc")).toEqual({ text: "23:59:59", remaining: "abc", time: utc(23, 59, 59), serial: 86399 / 86400, hours12: false, seconds: true, fraction: false });
                expect(TimeParser.parseTime("480:00:00abc")).toEqual({ text: "480:00:00", remaining: "abc", time: utc(480, 0, 0), serial: 20, hours12: false, seconds: true, fraction: false });
                expect(TimeParser.parseTime("12:00:00,125abc")).toEqual({ text: "12:00:00,125", remaining: "abc", time: utc(12, 0, 0, 125), serial: 0.5 + 0.125 / 86400, hours12: false, seconds: true, fraction: true });
                expect(TimeParser.parseTime("12 : 00 : 00 , 5abc")).toEqual({ text: "12 : 00 : 00 , 5", remaining: "abc", time: utc(12, 0, 0, 500), serial: 0.5 + 0.5 / 86400, hours12: false, seconds: true, fraction: true });
                expect(TimeParser.parseTime("12:0:0 AM.abc")).toEqual({ text: "12:0:0 AM.", remaining: "abc", time: utc(0, 0, 0), serial: 0, hours12: true, seconds: true, fraction: false });
                expect(TimeParser.parseTime("12:0:0 pm.abc")).toEqual({ text: "12:0:0 pm.", remaining: "abc", time: utc(12, 0, 0), serial: 0.5, hours12: true, seconds: true, fraction: false });
                expect(TimeParser.parseTime("1:0:0 abc")).toEqual({ text: "1:0:0 a", remaining: "bc", time: utc(1, 0, 0), serial: 1 / 24, hours12: true, seconds: true, fraction: false });
                expect(TimeParser.parseTime("6:0:0 Pmabc")).toEqual({ text: "6:0:0 Pm", remaining: "abc", time: utc(18, 0, 0), serial: 0.75, hours12: true, seconds: true, fraction: false });
                expect(TimeParser.parseTime("13:0:0 AM.abc")).toEqual({ text: "13:0:0", remaining: " AM.abc", time: utc(13, 0, 0), serial: 13 / 24, hours12: false, seconds: true, fraction: false });
                expect(TimeParser.parseTime("0:0:0abc", { complete: true })).toBeUndefined();
                // custom separator
                expect(TimeParser.parseTime("12:00:00#125abc", { decSep: "#" })).toEqual({ text: "12:00:00#125", remaining: "abc", time: utc(12, 0, 0, 125), serial: 0.5 + 0.125 / 86400, hours12: false, seconds: true, fraction: true });
                // invalid input
                expect(TimeParser.parseTime("10000:0:0", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("0:60:0", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("0:0:60", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("0:010:0", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("0:0:010", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("0:0:0 AM", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("13:0:0 AM", { complete: true })).toBeUndefined();
            });
            it("should parse times in format h:m", () => {
                expect(TimeParser.parseTime("0:0abc")).toEqual({ text: "0:0", remaining: "abc", time: utc(0, 0, 0), serial: 0, hours12: false, seconds: false, fraction: false });
                expect(TimeParser.parseTime("23:59abc")).toEqual({ text: "23:59", remaining: "abc", time: utc(23, 59, 0), serial: 86340 / 86400, hours12: false, seconds: false, fraction: false });
                expect(TimeParser.parseTime("480:00abc")).toEqual({ text: "480:00", remaining: "abc", time: utc(480, 0, 0), serial: 20, hours12: false, seconds: false, fraction: false });
                expect(TimeParser.parseTime("12 : 00abc")).toEqual({ text: "12 : 00", remaining: "abc", time: utc(12, 0, 0), serial: 0.5, hours12: false, seconds: false, fraction: false });
                expect(TimeParser.parseTime("12:0 AM.abc")).toEqual({ text: "12:0 AM.", remaining: "abc", time: utc(0, 0, 0), serial: 0, hours12: true, seconds: false, fraction: false });
                expect(TimeParser.parseTime("12:0 pm.abc")).toEqual({ text: "12:0 pm.", remaining: "abc", time: utc(12, 0, 0), serial: 0.5, hours12: true, seconds: false, fraction: false });
                expect(TimeParser.parseTime("1:0 abc")).toEqual({ text: "1:0 a", remaining: "bc", time: utc(1, 0, 0), serial: 1 / 24, hours12: true, seconds: false, fraction: false });
                expect(TimeParser.parseTime("6:0 Pmabc")).toEqual({ text: "6:0 Pm", remaining: "abc", time: utc(18, 0, 0), serial: 0.75, hours12: true, seconds: false, fraction: false });
                expect(TimeParser.parseTime("13:0 AM.abc")).toEqual({ text: "13:0", remaining: " AM.abc", time: utc(13, 0, 0), serial: 13 / 24, hours12: false, seconds: false, fraction: false });
                expect(TimeParser.parseTime("0:0abc", { complete: true })).toBeUndefined();
                // invalid input
                expect(TimeParser.parseTime("10000:0", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("0:60", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("0:010", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("0:0 AM", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("13:0 AM", { complete: true })).toBeUndefined();
            });
            it("should parse times in format h AM/PM", () => {
                expect(TimeParser.parseTime("12 AM.abc")).toEqual({ text: "12 AM.", remaining: "abc", time: utc(0, 0, 0), serial: 0, hours12: true, seconds: false, fraction: false });
                expect(TimeParser.parseTime("12 pm.abc")).toEqual({ text: "12 pm.", remaining: "abc", time: utc(12, 0, 0), serial: 0.5, hours12: true, seconds: false, fraction: false });
                expect(TimeParser.parseTime("1 abc")).toEqual({ text: "1 a", remaining: "bc", time: utc(1, 0, 0), serial: 1 / 24, hours12: true, seconds: false, fraction: false });
                expect(TimeParser.parseTime("6 Pmabc")).toEqual({ text: "6 Pm", remaining: "abc", time: utc(18, 0, 0), serial: 0.75, hours12: true, seconds: false, fraction: false });
                expect(TimeParser.parseTime("12 AM.bc", { complete: true })).toBeUndefined();
                // invalid input
                expect(TimeParser.parseTime("0 AM", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("13 AM", { complete: true })).toBeUndefined();
            });
            it("should parse times in format m:s.0", () => {
                expect(TimeParser.parseTime("0:0,0abc")).toEqual({ text: "0:0,0", remaining: "abc", time: utc(0, 0, 0), serial: 0, hours12: false, seconds: true, fraction: true });
                expect(TimeParser.parseTime("59:59,5000abc")).toEqual({ text: "59:59,5000", remaining: "abc", time: utc(0, 59, 59, 500), serial: 3599.5 / 86400, hours12: false, seconds: true, fraction: true });
                expect(TimeParser.parseTime("9999:59,9abc")).toEqual({ text: "9999:59,9", remaining: "abc", time: utc(0, 9999, 59, 900), serial: 599999.9 / 86400, hours12: false, seconds: true, fraction: true });
                expect(TimeParser.parseTime("30 : 0 , 5abc")).toEqual({ text: "30 : 0 , 5", remaining: "abc", time: utc(0, 30, 0, 500), serial: 1800.5 / 86400, hours12: false, seconds: true, fraction: true });
                expect(TimeParser.parseTime("0:0,0abc", { complete: true })).toBeUndefined();
                // invalid input
                expect(TimeParser.parseTime("10000:00,0", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("0:60,0", { complete: true })).toBeUndefined();
                expect(TimeParser.parseTime("0:60,", { complete: true })).toBeUndefined();
            });
        });
    });
});
