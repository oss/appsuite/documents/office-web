/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { DateParser } from "@/io.ox/office/editframework/model/formatter/parse/dateparser";

// constants ==================================================================

const YEAR = new Date().getFullYear();

// private functions ==========================================================

function utc(y, m, d) {
    return new Date(Date.UTC(y, m, d));
}

// tests ======================================================================

describe("module editframework/model/formatter/parse/dateparser", () => {

    // class DateParser -------------------------------------------------------

    describe("class DateParser", () => {
        it("should exist", () => {
            expect(DateParser).toBeClass();
        });

        describe("static function parseDate", () => {
            it("should exist", () => {
                expect(DateParser).toHaveStaticMethod("parseDate");
            });
            it("should parse date in native format D-M-Y", () => {
                expect(DateParser.parseDate("2.4.1999abc")).toEqual({ text: "2.4.1999", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("02.04.99abc")).toEqual({ text: "02.04.99", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("2/4/1999abc")).toEqual({ text: "2/4/1999", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("02/04/99abc")).toEqual({ text: "02/04/99", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("2-4-1999abc")).toEqual({ text: "2-4-1999", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("02-04-99abc")).toEqual({ text: "02-04-99", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("2 . 4 / 1999abc")).toEqual({ text: "2 . 4 / 1999", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("2.4.6abc")).toEqual({ text: "2.4.6", remaining: "abc", date: utc(2006, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("2.4.1999abc", { complete: true })).toBeUndefined();
                // invalid input
                expect(DateParser.parseDate("0-3-1999", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("32-3-1999", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1-0-1999", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1-13-1999", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("31-4-1999", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("29-2-1999", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1-3-999", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1-3-10000", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1 3 1999", { complete: true })).toBeUndefined();
            });
            it("should parse date in ISO format Y-M-D", () => {
                expect(DateParser.parseDate("1999.4.2abc")).toEqual({ text: "1999.4.2", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("1999-04-02abc")).toEqual({ text: "1999-04-02", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                // invalid input
                expect(DateParser.parseDate("1999-3-0", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1999-3-32", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1999-0-1", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1999-13-1", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1999-4-31", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1999-2-29", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("999-3-1", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("10000-3-1", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1999 3 1", { complete: true })).toBeUndefined();
            });
            it("should parse date with month name in native format D-M-Y", () => {
                expect(DateParser.parseDate("2.Apr.1999abc")).toEqual({ text: "2.Apr.1999", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("02-APRI-1999abc")).toEqual({ text: "02-APRI-1999", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("02/april/99abc")).toEqual({ text: "02/april/99", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("02. Apr. 1999abc")).toEqual({ text: "02. Apr. 1999", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("02 Apr1999abc")).toEqual({ text: "02 Apr1999", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("2apr-1999abc")).toEqual({ text: "2apr-1999", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("2Apr6abc")).toEqual({ text: "2Apr6", remaining: "abc", date: utc(2006, 3, 2), hasYear: true, hasDay: true });
                // invalid input
                expect(DateParser.parseDate("2.Ap.1999", { complete: true })).toBeUndefined();
            });
            it("should parse date with month name in ISO format Y-M-D", () => {
                expect(DateParser.parseDate("1999.Apr.2abc")).toEqual({ text: "1999.Apr.2", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("1999-APRI-02abc")).toEqual({ text: "1999-APRI-02", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("99/april/02abc")).toEqual({ text: "99/april/02", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("1999. Apr. 02abc")).toEqual({ text: "1999. Apr. 02", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("1999 Apr02abc")).toEqual({ text: "1999 Apr02", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                expect(DateParser.parseDate("1999apr-2abc")).toEqual({ text: "1999apr-2", remaining: "abc", date: utc(1999, 3, 2), hasYear: true, hasDay: true });
                // invalid input
                expect(DateParser.parseDate("1999.Ap.2", { complete: true })).toBeUndefined();
            });
            it("should parse date without year in native format D-M", () => {
                expect(DateParser.parseDate("2.4abc")).toEqual({ text: "2.4", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("02.04.abc")).toEqual({ text: "02.04.", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("2/4abc")).toEqual({ text: "2/4", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("02/04abc")).toEqual({ text: "02/04", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("2-4abc")).toEqual({ text: "2-4", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("02-04abc")).toEqual({ text: "02-04", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("2 . 4abc")).toEqual({ text: "2 . 4", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("2.Aprabc")).toEqual({ text: "2.Apr", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("2-APRIabc")).toEqual({ text: "2-APRI", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("2/aprilabc")).toEqual({ text: "2/april", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("2. Apr.abc")).toEqual({ text: "2. Apr.", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("2Aprabc")).toEqual({ text: "2Apr", remaining: "abc", date: utc(YEAR, 3, 2), hasYear: false, hasDay: true });
                expect(DateParser.parseDate("2.4abc", { complete: true })).toBeUndefined();
                // invalid input
                expect(DateParser.parseDate("0-3", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("32-3", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1-0", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1-13", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("31-4", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("2-Ap", { complete: true })).toBeUndefined();
            });
            it("should parse month/year in native format M-Y", () => {
                expect(DateParser.parseDate("4.1999abc")).toEqual({ text: "4.1999", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("04/1999abc")).toEqual({ text: "04/1999", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("4-1999abc")).toEqual({ text: "4-1999", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("4 / 1999abc")).toEqual({ text: "4 / 1999", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("Apr.1999abc")).toEqual({ text: "Apr.1999", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("APRI/1999abc")).toEqual({ text: "APRI/1999", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("april-1999abc")).toEqual({ text: "april-1999", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("Apr 1999abc")).toEqual({ text: "Apr 1999", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("Apr1999abc")).toEqual({ text: "Apr1999", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("4.1999abc", { complete: true })).toBeUndefined();
                // invalid input
                expect(DateParser.parseDate("0-1999", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("13-1999", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("3-999", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("3-10000", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("3 1999", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("Ap-1999", { complete: true })).toBeUndefined();
            });
            it("should parse year/month in ISO format Y-M", () => {
                expect(DateParser.parseDate("1999.4abc")).toEqual({ text: "1999.4", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("1999/04abc")).toEqual({ text: "1999/04", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("1999-4abc")).toEqual({ text: "1999-4", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("1999 / 4abc")).toEqual({ text: "1999 / 4", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("1999.Aprabc")).toEqual({ text: "1999.Apr", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("1999/APRIabc")).toEqual({ text: "1999/APRI", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("1999-aprilabc")).toEqual({ text: "1999-april", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("1999 Aprabc")).toEqual({ text: "1999 Apr", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("1999Aprabc")).toEqual({ text: "1999Apr", remaining: "abc", date: utc(1999, 3, 1), hasYear: true, hasDay: false });
                expect(DateParser.parseDate("1999.4abc", { complete: true })).toBeUndefined();
                // invalid input
                expect(DateParser.parseDate("1999-0", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1999-13", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("999-3", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("10000-3", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1999 3", { complete: true })).toBeUndefined();
                expect(DateParser.parseDate("1999-Ap", { complete: true })).toBeUndefined();
            });
        });
    });
});
