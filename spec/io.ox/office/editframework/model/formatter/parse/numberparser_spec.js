/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NumberParser } from "@/io.ox/office/editframework/model/formatter/parse/numberparser";

// tests ======================================================================

describe("module editframework/model/formatter/parse/numberparser", () => {

    // class NumberParser -----------------------------------------------------

    describe("class NumberParser", () => {
        it("should exist", () => {
            expect(NumberParser).toBeClass();
        });

        describe("static function parseNumber", () => {
            it("should exist", () => {
                expect(NumberParser).toHaveStaticMethod("parseNumber");
            });
            it("should parse numbers using local decimal separator", () => {
                expect(NumberParser.parseNumber("42abc")).toEqual({ number: 42, text: "42", negative: false, sign: "", dec: false, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("042,abc")).toEqual({ number: 42, text: "042,", negative: false, sign: "", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("42,5abc")).toEqual({ number: 42.5, text: "42,5", negative: false, sign: "", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("0,5abc")).toEqual({ number: 0.5, text: "0,5", negative: false, sign: "", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber(",50abc")).toEqual({ number: 0.5, text: ",50", negative: false, sign: "", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber(",5,5")).toEqual({ number: 0.5, text: ",5", negative: false, sign: "", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: ",5" });
                expect(NumberParser.parseNumber("42,5E3abc")).toEqual({ number: 42500, text: "42,5E3", negative: false, sign: "", dec: true, scientific: true, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("42,5e0abc")).toEqual({ number: 42.5, text: "42,5e0", negative: false, sign: "", dec: true, scientific: true, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("42,5e+03abc")).toEqual({ number: 42500, text: "42,5e+03", negative: false, sign: "", dec: true, scientific: true, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("42,5e-03abc")).toEqual({ number: 0.0425, text: "42,5e-03", negative: false, sign: "", dec: true, scientific: true, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("42,5e3,5")).toEqual({ number: 42500, text: "42,5e3", negative: false, sign: "", dec: true, scientific: true, grouped: false, fraction: undefined, remaining: ",5" });
                expect(NumberParser.parseNumber("42e3,5")).toEqual({ number: 42000, text: "42e3", negative: false, sign: "", dec: false, scientific: true, grouped: false, fraction: undefined, remaining: ",5" });
                expect(NumberParser.parseNumber("42.5")).toEqual({ number: 42, text: "42", negative: false, sign: "", dec: false, scientific: false, grouped: false, fraction: undefined, remaining: ".5" });
                expect(NumberParser.parseNumber("42abc", { complete: true })).toBeUndefined();
                expect(NumberParser.parseNumber("42abc", { parseType: "fractional" })).toBeUndefined();
            });
            it("should reject invalid text", () => {
                expect(NumberParser.parseNumber("abc")).toBeUndefined();
                expect(NumberParser.parseNumber(",")).toBeUndefined();
                expect(NumberParser.parseNumber("e3")).toBeUndefined();
                expect(NumberParser.parseNumber(",e3")).toBeUndefined();
            });
            it("should handle leading sign", () => {
                expect(NumberParser.parseNumber("+42abc")).toEqual({ number: 42, text: "+42", negative: false, sign: "+", dec: false, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("+0,5abc")).toEqual({ number: 0.5, text: "+0,5", negative: false, sign: "+", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("+00,abc")).toEqual({ number: 0, text: "+00,", negative: false, sign: "+", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("+,5abc")).toEqual({ number: 0.5, text: "+,5", negative: false, sign: "+", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("+,0abc")).toEqual({ number: 0, text: "+,0", negative: false, sign: "+", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("-42abc")).toEqual({ number: -42, text: "-42", negative: true, sign: "-", dec: false, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("-0,5abc")).toEqual({ number: -0.5, text: "-0,5", negative: true, sign: "-", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("-00,abc")).toEqual({ number: -0, text: "-00,", negative: true, sign: "-", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("-,5abc")).toEqual({ number: -0.5, text: "-,5", negative: true, sign: "-", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("-,0abc")).toEqual({ number: -0, text: "-,0", negative: true, sign: "-", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("+0abc")).toEqual({ number: 0, text: "+0", negative: false, sign: "+", dec: false, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("-0abc")).toEqual({ number: -0, text: "-0", negative: true, sign: "-", dec: false, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("+42abc", { complete: true })).toBeUndefined();
            });
            it("should use custom decimal separator", () => {
                expect(NumberParser.parseNumber("42#5abc")).toEqual({ number: 42, text: "42", negative: false, sign: "", dec: false, scientific: false, grouped: false, fraction: undefined, remaining: "#5abc" });
                expect(NumberParser.parseNumber("42#5abc", { decSep: "#" })).toEqual({ number: 42.5, text: "42#5", negative: false, sign: "", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("42,5abc", { decSep: "#" })).toEqual({ number: 42, text: "42", negative: false, sign: "", dec: false, scientific: false, grouped: false, fraction: undefined, remaining: ",5abc" });
                expect(NumberParser.parseNumber("+42#5abc", { decSep: "#" })).toEqual({ number: 42.5, text: "+42#5", negative: false, sign: "+", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("-42#5abc", { decSep: "#" })).toEqual({ number: -42.5, text: "-42#5", negative: true, sign: "-", dec: true, scientific: false, grouped: false, fraction: undefined, remaining: "abc" });
            });
            it("should handle group separator", () => {
                expect(NumberParser.parseNumber("42.123,5abc")).toEqual({ number: 42, text: "42", negative: false, sign: "", dec: false, scientific: false, grouped: false, fraction: undefined, remaining: ".123,5abc" });
                expect(NumberParser.parseNumber("42.123,5abc", { groupSep: true })).toEqual({ number: 42123.5, text: "42.123,5", negative: false, sign: "", dec: true, scientific: false, grouped: true, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("1.042.123,5abc", { groupSep: true })).toEqual({ number: 1042123.5, text: "1.042.123,5", negative: false, sign: "", dec: true, scientific: false, grouped: true, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("1042.123,5abc", { groupSep: true })).toEqual({ number: 1042123.5, text: "1042.123,5", negative: false, sign: "", dec: true, scientific: false, grouped: true, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("104.2123,5abc", { groupSep: true })).toEqual({ number: 1042123.5, text: "104.2123,5", negative: false, sign: "", dec: true, scientific: false, grouped: true, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("42.123,5e+01abc", { groupSep: true })).toEqual({ number: 421235, text: "42.123,5e+01", negative: false, sign: "", dec: true, scientific: true, grouped: true, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("42.12,5abc", { groupSep: true })).toEqual({ number: 42, text: "42", negative: false, sign: "", dec: false, scientific: false, grouped: false, fraction: undefined, remaining: ".12,5abc" });
                expect(NumberParser.parseNumber("42'123,5abc", { groupSep: "'" })).toEqual({ number: 42123.5, text: "42'123,5", negative: false, sign: "", dec: true, scientific: false, grouped: true, fraction: undefined, remaining: "abc" });
                expect(NumberParser.parseNumber("42.123,5abc", { groupSep: true, complete: true })).toBeUndefined();
            });
            it("should parse fractional notation", () => {
                expect(NumberParser.parseNumber("1 2/5abc")).toEqual({ number: 1.4, text: "1 2/5", negative: false, sign: "", grouped: false, dec: false, scientific: false, fraction: [1, 2, 5], remaining: "abc" });
                expect(NumberParser.parseNumber("042 042/210abc")).toEqual({ number: 42.2, text: "042 042/210", negative: false, sign: "", grouped: false, dec: false, scientific: false, fraction: [42, 42, 210], remaining: "abc" });
                expect(NumberParser.parseNumber("1,1 2/5abc")).toEqual({ number: 1.1, text: "1,1", negative: false, sign: "", grouped: false, dec: true, scientific: false, fraction: undefined, remaining: " 2/5abc" });
                expect(NumberParser.parseNumber("1e3 2/5abc")).toEqual({ number: 1000, text: "1e3", negative: false, sign: "", grouped: false, dec: false, scientific: true, fraction: undefined, remaining: " 2/5abc" });
                expect(NumberParser.parseNumber("1 2,1/5abc")).toEqual({ number: 1, text: "1", negative: false, sign: "", grouped: false, dec: false, scientific: false, fraction: undefined, remaining: " 2,1/5abc" });
                expect(NumberParser.parseNumber("1 2e1/5abc")).toEqual({ number: 1, text: "1", negative: false, sign: "", grouped: false, dec: false, scientific: false, fraction: undefined, remaining: " 2e1/5abc" });
                expect(NumberParser.parseNumber("1  2/5abc")).toEqual({ number: 1, text: "1", negative: false, sign: "", grouped: false, dec: false, scientific: false, fraction: undefined, remaining: "  2/5abc" });
                expect(NumberParser.parseNumber("1 2/0abc")).toBeUndefined(); // valid syntax, but invalid division by zero
                expect(NumberParser.parseNumber("1 2/5abc", { complete: true })).toBeUndefined();
                expect(NumberParser.parseNumber("1 2/5abc", { parseType: "decimal" })).toEqual({ number: 1, text: "1", negative: false, sign: "", grouped: false, dec: false, scientific: false, fraction: undefined, remaining: " 2/5abc" });
            });
            it("should handle leading sign in fractional notation", () => {
                expect(NumberParser.parseNumber("+1 2/5abc")).toEqual({ number: 1.4, text: "+1 2/5", negative: false, sign: "+", grouped: false, dec: false, scientific: false, fraction: [1, 2, 5], remaining: "abc" });
                expect(NumberParser.parseNumber("-0 2/5abc")).toEqual({ number: -0.4, text: "-0 2/5", negative: true, sign: "-", grouped: false, dec: false, scientific: false, fraction: [0, 2, 5], remaining: "abc" });
                expect(NumberParser.parseNumber("+1 2/5abc", { complete: true })).toBeUndefined();
            });
            it("should handle group separator in fractional notation", () => {
                expect(NumberParser.parseNumber("42.123 2/5abc")).toEqual({ number: 42, text: "42", negative: false, sign: "", grouped: false, dec: false, scientific: false, fraction: undefined, remaining: ".123 2/5abc" });
                expect(NumberParser.parseNumber("42.123 2/5abc", { groupSep: true })).toEqual({ number: 42123.4, text: "42.123 2/5", negative: false, sign: "", grouped: true, dec: false, scientific: false, fraction: [42123, 2, 5], remaining: "abc" });
                expect(NumberParser.parseNumber("104.2123 2/5abc", { groupSep: true })).toEqual({ number: 1042123.4, text: "104.2123 2/5", negative: false, sign: "", grouped: true, dec: false, scientific: false, fraction: [1042123, 2, 5], remaining: "abc" });
                expect(NumberParser.parseNumber("42.12 2/5abc", { groupSep: true })).toEqual({ number: 42, text: "42", negative: false, sign: "", grouped: false, dec: false, scientific: false, fraction: undefined, remaining: ".12 2/5abc" });
                expect(NumberParser.parseNumber("42'123 2/5abc", { groupSep: "'" })).toEqual({ number: 42123.4, text: "42'123 2/5", negative: false, sign: "", grouped: true, dec: false, scientific: false, fraction: [42123, 2, 5], remaining: "abc" });
                expect(NumberParser.parseNumber("42.123 2/5abc", { groupSep: true, complete: true })).toBeUndefined();
            });
        });
    });
});
