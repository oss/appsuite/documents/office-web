/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { OperationError } from '@/io.ox/office/editframework/utils/operationerror';
import { OperationContext } from '@/io.ox/office/editframework/model/operation/context';

// tests ======================================================================

describe('module editframework/model/operation/context', function () {

    // a fake document model
    var docModel = {};
    // the test operation
    var operation = { name: 'test', bool0: false, bool1: true, num: 2.5, nan: Number.NaN, str: 'a', estr: '', enum: 'e1', nul: null, obj: { i: 1 }, arr: [3.5], earr: [], pos1: [0, 1, 2], pos2: [-1], pos3: ['a'] };
    // the tested operation context
    var context = new OperationContext(docModel, operation, false);
    // a default object value
    var defObj = { a: 42 };
    // a default array value
    var defArr = [42];
    // an enumeration class
    var MyEnum = { E1: 'e1', E2: 'e2', E3: 'e3' };

    // class OperationError ---------------------------------------------------

    describe('class OperationError', function () {

        it('should exist', function () {
            expect(OperationError).toBeSubClassOf(Error);
        });

        describe('constructor', function () {
            it('should create an error instance', function () {
                var op = { name: 'op' };
                var error1 = new OperationError('bad1', { operation: op });
                expect(error1).toHaveProperty('name', 'OperationError');
                expect(error1).toHaveProperty('message', 'bad1');
                expect(error1).toHaveProperty('cause', 'operation');
                expect(error1).toHaveProperty('operation', op);
                var error2 = new OperationError('bad2', { cause: 'recursive' });
                expect(error2).toHaveProperty('name', 'OperationError');
                expect(error2).toHaveProperty('message', 'bad2');
                expect(error2).toHaveProperty('cause', 'recursive');
                expect(error2).toHaveProperty('operation', undefined);
            });
        });
    });

    // class OperationContext -------------------------------------------------

    describe('class OperationContext', function () {

        function expectToThrowBad(fn) {
            expect(fn).toThrow(OperationError);
            expect(fn).toThrow('bad');
        }

        it('should exist', function () {
            expect(OperationContext).toBeClass();
        });

        describe('constructor', function () {
            it('should create a context', function () {
                expect(context).toHaveProperty('docModel', docModel);
                expect(context).toHaveProperty('operation', operation);
                expect(context).toHaveProperty('external', false);
                expect(new OperationContext(docModel, operation, true)).toHaveProperty('external', true);
            });
        });

        describe('method error', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('error');
            });
            it('should throw', function () {
                expectToThrowBad(() => context.error('bad'));
            });
        });

        describe('method ensure', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('ensure');
            });
            it('should do nothing on truthy condition', function () {
                expect(() => context.ensure(true, 'bad')).not.toThrow();
                expect(() => context.ensure(1, 'bad')).not.toThrow();
                expect(() => context.ensure('a', 'bad')).not.toThrow();
                expect(() => context.ensure({}, 'bad')).not.toThrow();
                expect(() => context.ensure([], 'bad')).not.toThrow();
            });
            it('should throw on falsy condition', function () {
                expectToThrowBad(() => context.ensure(false, 'bad'));
                expectToThrowBad(() => context.ensure(0, 'bad'));
                expectToThrowBad(() => context.ensure('', 'bad'));
                expectToThrowBad(() => context.ensure(null, 'bad'));
            });
        });

        describe('method has', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('has');
            });
            it('should return true for existing properties', function () {
                expect(context.has('bool0')).toBeTrue();
                expect(context.has('bool1')).toBeTrue();
                expect(context.has('num')).toBeTrue();
                expect(context.has('nan')).toBeTrue();
                expect(context.has('str')).toBeTrue();
                expect(context.has('estr')).toBeTrue();
                expect(context.has('nul')).toBeTrue();
                expect(context.has('obj')).toBeTrue();
                expect(context.has('arr')).toBeTrue();
                expect(context.has('earr')).toBeTrue();
            });
            it('should return false for missing properties', function () {
                expect(context.has('wrong')).toBeFalse();
            });
        });

        describe('method getBool', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('getBool');
            });
            it('should return existing properties', function () {
                expect(context.getBool('bool0')).toBe(operation.bool0);
                expect(context.getBool('bool1')).toBe(operation.bool1);
            });
            it('should throw on missing properties', function () {
                expect(() => context.getBool('wrong')).toThrow(OperationError);
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.getBool('num')).toThrow(OperationError);
                expect(() => context.getBool('nan')).toThrow(OperationError);
                expect(() => context.getBool('str')).toThrow(OperationError);
                expect(() => context.getBool('estr')).toThrow(OperationError);
                expect(() => context.getBool('nul')).toThrow(OperationError);
                expect(() => context.getBool('obj')).toThrow(OperationError);
                expect(() => context.getBool('arr')).toThrow(OperationError);
                expect(() => context.getBool('earr')).toThrow(OperationError);
            });
        });

        describe('method optBool', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('optBool');
            });
            it('should return existing properties', function () {
                expect(context.optBool('bool0', true)).toBe(operation.bool0);
                expect(context.optBool('bool1', true)).toBe(operation.bool1);
            });
            it('should return default value for missing properties', function () {
                expect(context.optBool('wrong', true)).toBeTrue();
                expect(context.optBool('wrong', false)).toBeFalse();
                expect(context.optBool('wrong')).toBeFalse();
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.optBool('num')).toThrow(OperationError);
                expect(() => context.optBool('nan')).toThrow(OperationError);
                expect(() => context.optBool('str')).toThrow(OperationError);
                expect(() => context.optBool('estr')).toThrow(OperationError);
                expect(() => context.optBool('nul')).toThrow(OperationError);
                expect(() => context.optBool('obj')).toThrow(OperationError);
                expect(() => context.optBool('arr')).toThrow(OperationError);
                expect(() => context.optBool('earr')).toThrow(OperationError);
            });
        });

        describe('method getNum', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('getNum');
            });
            it('should return existing properties', function () {
                expect(context.getNum('num')).toBe(operation.num);
            });
            it('should throw on missing properties', function () {
                expect(() => context.getNum('wrong')).toThrow(OperationError);
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.getNum('bool0')).toThrow(OperationError);
                expect(() => context.getNum('bool1')).toThrow(OperationError);
                expect(() => context.getNum('nan')).toThrow(OperationError);
                expect(() => context.getNum('str')).toThrow(OperationError);
                expect(() => context.getNum('estr')).toThrow(OperationError);
                expect(() => context.getNum('nul')).toThrow(OperationError);
                expect(() => context.getNum('obj')).toThrow(OperationError);
                expect(() => context.getNum('arr')).toThrow(OperationError);
                expect(() => context.getNum('earr')).toThrow(OperationError);
            });
        });

        describe('method optNum', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('optNum');
            });
            it('should return existing properties', function () {
                expect(context.optNum('num', 42)).toBe(operation.num);
            });
            it('should return default value for missing properties', function () {
                expect(context.optNum('wrong', 42)).toBe(42);
                expect(context.optNum('wrong')).toBe(0);
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.optNum('bool0')).toThrow(OperationError);
                expect(() => context.optNum('bool1')).toThrow(OperationError);
                expect(() => context.optNum('nan')).toThrow(OperationError);
                expect(() => context.optNum('str')).toThrow(OperationError);
                expect(() => context.optNum('estr')).toThrow(OperationError);
                expect(() => context.optNum('nul')).toThrow(OperationError);
                expect(() => context.optNum('obj')).toThrow(OperationError);
                expect(() => context.optNum('arr')).toThrow(OperationError);
                expect(() => context.optNum('earr')).toThrow(OperationError);
            });
        });

        describe('method getInt', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('getInt');
            });
            it('should return existing properties', function () {
                expect(context.getInt('num')).toBe(Math.floor(operation.num));
            });
            it('should throw on missing properties', function () {
                expect(() => context.getInt('wrong')).toThrow(OperationError);
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.getInt('bool0')).toThrow(OperationError);
                expect(() => context.getInt('bool1')).toThrow(OperationError);
                expect(() => context.getInt('nan')).toThrow(OperationError);
                expect(() => context.getInt('str')).toThrow(OperationError);
                expect(() => context.getInt('estr')).toThrow(OperationError);
                expect(() => context.getInt('nul')).toThrow(OperationError);
                expect(() => context.getInt('obj')).toThrow(OperationError);
                expect(() => context.getInt('arr')).toThrow(OperationError);
                expect(() => context.getInt('earr')).toThrow(OperationError);
            });
        });

        describe('method optInt', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('optInt');
            });
            it('should return existing properties', function () {
                expect(context.optInt('num', 42)).toBe(Math.floor(operation.num));
            });
            it('should return default value for missing properties', function () {
                expect(context.optInt('wrong', 42)).toBe(42);
                expect(context.optInt('wrong', 42.5)).toBe(42);
                expect(context.optInt('wrong')).toBe(0);
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.optInt('bool0')).toThrow(OperationError);
                expect(() => context.optInt('bool1')).toThrow(OperationError);
                expect(() => context.optInt('nan')).toThrow(OperationError);
                expect(() => context.optInt('str')).toThrow(OperationError);
                expect(() => context.optInt('estr')).toThrow(OperationError);
                expect(() => context.optInt('nul')).toThrow(OperationError);
                expect(() => context.optInt('obj')).toThrow(OperationError);
                expect(() => context.optInt('arr')).toThrow(OperationError);
                expect(() => context.optInt('earr')).toThrow(OperationError);
            });
        });

        describe('method getStr', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('getStr');
            });
            it('should return existing properties', function () {
                expect(context.getStr('str')).toBe(operation.str);
                expect(context.getStr('str', { empty: true })).toBe(operation.str);
            });
            it('should throw on missing properties', function () {
                expect(() => context.getStr('wrong')).toThrow(OperationError);
            });
            it('should handle empty strings', function () {
                expect(() => context.getStr('estr')).toThrow(OperationError);
                expect(context.getStr('estr', { empty: true })).toBe("");
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.getStr('bool0')).toThrow(OperationError);
                expect(() => context.getStr('bool1')).toThrow(OperationError);
                expect(() => context.getStr('num')).toThrow(OperationError);
                expect(() => context.getStr('nan')).toThrow(OperationError);
                expect(() => context.getStr('nul')).toThrow(OperationError);
                expect(() => context.getStr('obj')).toThrow(OperationError);
                expect(() => context.getStr('arr')).toThrow(OperationError);
                expect(() => context.getStr('earr')).toThrow(OperationError);
            });
        });

        describe('method optStr', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('optStr');
            });
            it('should return existing properties', function () {
                expect(context.optStr('str', '42')).toBe(operation.str);
                expect(context.optStr('str', '42', { empty: true })).toBe(operation.str);
            });
            it('should return default value for missing properties', function () {
                expect(context.optStr('wrong', '42')).toBe("42");
                expect(context.optStr('wrong')).toBe("");
            });
            it('should handle empty strings', function () {
                expect(() => context.optStr('estr')).toThrow(OperationError);
                expect(() => context.optStr('estr', '42')).toThrow(OperationError);
                expect(context.optStr('estr', { empty: true })).toBe("");
                expect(context.optStr('estr', '42', { empty: true })).toBe("");
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.optStr('bool0')).toThrow(OperationError);
                expect(() => context.optStr('bool1')).toThrow(OperationError);
                expect(() => context.optStr('num')).toThrow(OperationError);
                expect(() => context.optStr('nan')).toThrow(OperationError);
                expect(() => context.optStr('nul')).toThrow(OperationError);
                expect(() => context.optStr('obj')).toThrow(OperationError);
                expect(() => context.optStr('arr')).toThrow(OperationError);
                expect(() => context.optStr('earr')).toThrow(OperationError);
            });
        });

        describe('method getPrim', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('getPrim');
            });
            it('should return existing properties', function () {
                expect(context.getPrim('bool0')).toBe(operation.bool0);
                expect(context.getPrim('bool1')).toBe(operation.bool1);
                expect(context.getPrim('num')).toBe(operation.num);
                expect(context.getPrim('str')).toBe(operation.str);
                expect(context.getPrim('str', { empty: true })).toBe(operation.str);
                expect(context.getPrim('nul')).toBe(operation.nul);
            });
            it('should throw on missing properties', function () {
                expect(() => context.getPrim('wrong')).toThrow(OperationError);
            });
            it('should handle empty strings', function () {
                expect(() => context.getPrim('estr')).toThrow(OperationError);
                expect(context.getPrim('estr', { empty: true })).toBe("");
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.getPrim('nan')).toThrow(OperationError);
                expect(() => context.getPrim('obj')).toThrow(OperationError);
                expect(() => context.getPrim('arr')).toThrow(OperationError);
                expect(() => context.getPrim('earr')).toThrow(OperationError);
            });
        });

        describe('method optPrim', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('optPrim');
            });
            it('should return existing properties', function () {
                expect(context.optPrim('bool0')).toBe(operation.bool0);
                expect(context.optPrim('bool1')).toBe(operation.bool1);
                expect(context.optPrim('num')).toBe(operation.num);
                expect(context.optPrim('str')).toBe(operation.str);
                expect(context.optPrim('str', { empty: true })).toBe(operation.str);
                expect(context.optPrim('nul')).toBe(operation.nul);
            });
            it('should return default value for missing properties', function () {
                expect(context.optPrim('wrong')).toBeNull();
            });
            it('should handle empty strings', function () {
                expect(() => context.optPrim('estr')).toThrow(OperationError);
                expect(context.optPrim('estr', { empty: true })).toBe("");
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.optPrim('nan')).toThrow(OperationError);
                expect(() => context.optPrim('obj')).toThrow(OperationError);
                expect(() => context.optPrim('arr')).toThrow(OperationError);
                expect(() => context.optPrim('earr')).toThrow(OperationError);
            });
        });

        describe('method getEnum', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('getEnum');
            });
            it('should return existing properties', function () {
                expect(context.getEnum('enum', MyEnum)).toBe(MyEnum.E1);
            });
            it('should throw on missing properties', function () {
                expect(() => context.getEnum('wrong', MyEnum)).toThrow(OperationError);
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.getEnum('bool0', MyEnum)).toThrow(OperationError);
                expect(() => context.getEnum('bool1', MyEnum)).toThrow(OperationError);
                expect(() => context.getEnum('num', MyEnum)).toThrow(OperationError);
                expect(() => context.getEnum('nan', MyEnum)).toThrow(OperationError);
                expect(() => context.getEnum('str', MyEnum)).toThrow(OperationError);
                expect(() => context.getEnum('estr', MyEnum)).toThrow(OperationError);
                expect(() => context.getEnum('nul', MyEnum)).toThrow(OperationError);
                expect(() => context.getEnum('obj', MyEnum)).toThrow(OperationError);
                expect(() => context.getEnum('arr', MyEnum)).toThrow(OperationError);
                expect(() => context.getEnum('earr', MyEnum)).toThrow(OperationError);
            });
        });

        describe('method optEnum', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('optEnum');
            });
            it('should return existing properties', function () {
                expect(context.optEnum('enum', MyEnum, MyEnum.E2)).toBe(MyEnum.E1);
            });
            it('should return default value for missing properties', function () {
                expect(context.optEnum('wrong', MyEnum, MyEnum.E2)).toBe(MyEnum.E2);
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.optEnum('bool0', MyEnum, MyEnum.E2)).toThrow(OperationError);
                expect(() => context.optEnum('bool1', MyEnum, MyEnum.E2)).toThrow(OperationError);
                expect(() => context.optEnum('num', MyEnum, MyEnum.E2)).toThrow(OperationError);
                expect(() => context.optEnum('nan', MyEnum, MyEnum.E2)).toThrow(OperationError);
                expect(() => context.optEnum('str', MyEnum, MyEnum.E2)).toThrow(OperationError);
                expect(() => context.optEnum('estr', MyEnum, MyEnum.E2)).toThrow(OperationError);
                expect(() => context.optEnum('nul', MyEnum, MyEnum.E2)).toThrow(OperationError);
                expect(() => context.optEnum('obj', MyEnum, MyEnum.E2)).toThrow(OperationError);
                expect(() => context.optEnum('arr', MyEnum, MyEnum.E2)).toThrow(OperationError);
                expect(() => context.optEnum('earr', MyEnum, MyEnum.E2)).toThrow(OperationError);
            });
        });

        describe('method getDict', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('getDict');
            });
            it('should return existing properties', function () {
                expect(context.getDict('obj')).toBe(operation.obj);
            });
            it('should throw on missing properties', function () {
                expect(() => context.getDict('wrong')).toThrow(OperationError);
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.getDict('bool0')).toThrow(OperationError);
                expect(() => context.getDict('bool1')).toThrow(OperationError);
                expect(() => context.getDict('num')).toThrow(OperationError);
                expect(() => context.getDict('nan')).toThrow(OperationError);
                expect(() => context.getDict('str')).toThrow(OperationError);
                expect(() => context.getDict('estr')).toThrow(OperationError);
                expect(() => context.getDict('nul')).toThrow(OperationError);
                expect(() => context.getDict('arr')).toThrow(OperationError);
                expect(() => context.getDict('earr')).toThrow(OperationError);
            });
        });

        describe('method optDict', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('optDict');
            });
            it('should return existing properties', function () {
                expect(context.optDict('obj', defObj)).toBe(operation.obj);
            });
            it('should return default value for missing properties', function () {
                expect(context.optDict('wrong', defObj)).toBe(defObj);
                expect(context.optDict('wrong')).toBeUndefined();
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.optDict('bool0')).toThrow(OperationError);
                expect(() => context.optDict('bool1')).toThrow(OperationError);
                expect(() => context.optDict('num')).toThrow(OperationError);
                expect(() => context.optDict('nan')).toThrow(OperationError);
                expect(() => context.optDict('str')).toThrow(OperationError);
                expect(() => context.optDict('estr')).toThrow(OperationError);
                expect(() => context.optDict('nul')).toThrow(OperationError);
                expect(() => context.optDict('arr')).toThrow(OperationError);
                expect(() => context.optDict('earr')).toThrow(OperationError);
            });
        });

        describe('method getArr', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('getArr');
            });
            it('should return existing properties', function () {
                expect(context.getArr('arr')).toBe(operation.arr);
                expect(context.getArr('earr')).toBe(operation.earr);
            });
            it('should throw on missing properties', function () {
                expect(() => context.getArr('wrong')).toThrow(OperationError);
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.getArr('bool0')).toThrow(OperationError);
                expect(() => context.getArr('bool1')).toThrow(OperationError);
                expect(() => context.getArr('num')).toThrow(OperationError);
                expect(() => context.getArr('nan')).toThrow(OperationError);
                expect(() => context.getArr('str')).toThrow(OperationError);
                expect(() => context.getArr('estr')).toThrow(OperationError);
                expect(() => context.getArr('nul')).toThrow(OperationError);
                expect(() => context.getArr('obj')).toThrow(OperationError);
            });
        });

        describe('method optArr', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('optArr');
            });
            it('should return existing properties', function () {
                expect(context.optArr('arr', defArr)).toBe(operation.arr);
                expect(context.optArr('earr', defArr)).toBe(operation.earr);
            });
            it('should return default value for missing properties', function () {
                expect(context.optArr('wrong', defArr)).toBe(defArr);
                expect(context.optArr('wrong')).toBeUndefined();
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.optArr('bool0')).toThrow(OperationError);
                expect(() => context.optArr('bool1')).toThrow(OperationError);
                expect(() => context.optArr('num')).toThrow(OperationError);
                expect(() => context.optArr('nan')).toThrow(OperationError);
                expect(() => context.optArr('str')).toThrow(OperationError);
                expect(() => context.optArr('estr')).toThrow(OperationError);
                expect(() => context.optArr('nul')).toThrow(OperationError);
                expect(() => context.optArr('obj')).toThrow(OperationError);
            });
        });

        describe('method getPos', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('getPos');
            });
            it('should return existing properties', function () {
                expect(context.getPos('pos1')).toBe(operation.pos1);
            });
            it('should throw on missing properties', function () {
                expect(() => context.getPos('wrong')).toThrow(OperationError);
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.getPos('bool0')).toThrow(OperationError);
                expect(() => context.getPos('bool1')).toThrow(OperationError);
                expect(() => context.getPos('num')).toThrow(OperationError);
                expect(() => context.getPos('nan')).toThrow(OperationError);
                expect(() => context.getPos('str')).toThrow(OperationError);
                expect(() => context.getPos('estr')).toThrow(OperationError);
                expect(() => context.getPos('nul')).toThrow(OperationError);
                expect(() => context.getPos('obj')).toThrow(OperationError);
                expect(() => context.getPos('arr')).toThrow(OperationError);
                expect(() => context.getPos('earr')).toThrow(OperationError);
                expect(() => context.getPos('pos2')).toThrow(OperationError);
                expect(() => context.getPos('pos3')).toThrow(OperationError);
            });
        });

        describe('method optPos', function () {
            it('should exist', function () {
                expect(OperationContext).toHaveMethod('optPos');
            });
            it('should return existing properties', function () {
                expect(context.optPos('pos1')).toBe(operation.pos1);
            });
            it('should return null for missing properties', function () {
                expect(context.optPos('wrong')).toBeNull();
            });
            it('should throw on existing properties with wrong type', function () {
                expect(() => context.optPos('bool0')).toThrow(OperationError);
                expect(() => context.optPos('bool1')).toThrow(OperationError);
                expect(() => context.optPos('num')).toThrow(OperationError);
                expect(() => context.optPos('nan')).toThrow(OperationError);
                expect(() => context.optPos('str')).toThrow(OperationError);
                expect(() => context.optPos('estr')).toThrow(OperationError);
                expect(() => context.optPos('nul')).toThrow(OperationError);
                expect(() => context.optPos('obj')).toThrow(OperationError);
                expect(() => context.optPos('arr')).toThrow(OperationError);
                expect(() => context.optPos('earr')).toThrow(OperationError);
                expect(() => context.optPos('pos2')).toThrow(OperationError);
                expect(() => context.optPos('pos3')).toThrow(OperationError);
            });
        });
    });
});
