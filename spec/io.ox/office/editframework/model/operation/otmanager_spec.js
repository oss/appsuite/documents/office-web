/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { opSeries1, opSeries2, noOp, changeConfig, insertFont, insertTheme, insertStyle, deleteStyle, changeStyle,
    insertAutoStyle, deleteAutoStyle, changeAutoStyle, insertChSeries, deleteChSeries, changeChSeries, deleteChAxis,
    changeChAxis, changeChGrid, changeChTitle, changeChLegend, TestRunner } from '~/othelper';
import { OTEngine } from '@/io.ox/office/editframework/model/operation/otengine';
import { BaseOTManager, OT_DELETE_TARGET_ALIAS } from '@/io.ox/office/editframework/model/operation/otmanager';

// tests ==================================================================

describe('module editframework/model/operation/otmanager', function () {

    // generic attribute set
    var ATTRS = { f1: { a1: 10 } };

    // independent attribute sets (will not be reduced)
    var ATTRS_I1 = { f1: { a1: 10 }, f2: { a1: 10 } };
    var ATTRS_I2 = { f2: { a2: 10 }, f3: { a1: 10 } };

    // overlapping attribute sets (will be reduced)
    var ATTRS_O1 = { f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } };
    var ATTRS_O2 = { f3: { a1: 10 }, f2: { a1: 10, a2: 22, a4: 40 } };

    // the reduced result of ATTRS_Ox
    var ATTRS_R1 = { f1: { a1: 10 }, f2: { a2: 20, a3: 30 } };
    var ATTRS_R2 = { f3: { a1: 10 }, f2: { a4: 40 } };

    // globally independent operations
    var GLOBAL_OPS = [changeConfig(ATTRS), insertFont('f1'), insertTheme('t1')];

    // generic style sheet operations
    var STYLESHEET_OPS = opSeries2([insertStyle, deleteStyle, changeStyle], 't1', 's1', ATTRS);

    // generic auto-style operations
    var AUTOSTYLE_OPS = opSeries2([insertAutoStyle, deleteAutoStyle, changeAutoStyle], 'a1', ATTRS);

    // generic chart operations
    var CHART_SERIES_OPS = opSeries2([insertChSeries, deleteChSeries, changeChSeries], 1, 0, ATTRS);
    var CHART_AXIS_OPS = [deleteChAxis(1, 0), changeChAxis(1, 0, ATTRS)];
    var CHART_COMP_OPS = [opSeries2([changeChGrid, changeChTitle], 1, 0, ATTRS), changeChTitle(1, ATTRS), changeChLegend(1, ATTRS)];
    var CHART_OPS = [CHART_SERIES_OPS, CHART_AXIS_OPS, CHART_COMP_OPS];

    // constants ==========================================================

    describe('constant OT_DELETE_TARGET_ALIAS', function () {
        it('should exist', function () {
            expect(OT_DELETE_TARGET_ALIAS).toMatch(/./);
        });
    });

    // class BaseOTManager ================================================

    describe('class BaseOTManager', function () {

        it('should subclass OTEngine', function () {
            expect(BaseOTManager).toBeSubClassOf(OTEngine);
        });

        describe('method transformPosition', function () {
            it('should exist', function () {
                expect(BaseOTManager).toHaveMethod('transformPosition');
            });
        });
    });

    // transformations ====================================================

    var testRunner = null;
    beforeAll(function () {
        var otManager = new BaseOTManager({
            defaultAutoStyleType: 't1',
            indexedAutoStylePrefixes: { t1: 'a' }
        });
        testRunner = new TestRunner(otManager);
    });

    // noOp ---------------------------------------------------------------

    describe('"noOp" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runTest(noOp(), noOp());
            testRunner.runBidiTest(noOp(), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS, CHART_OPS]);
        });
    });

    // changeConfig -------------------------------------------------------

    describe('"changeConfig" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(changeConfig({}), [insertFont('f1'), insertTheme('t1'), STYLESHEET_OPS, AUTOSTYLE_OPS, CHART_OPS]);
        });
    });

    describe('"changeConfig" and "changeConfig"', function () {
        it('should not process independent attributes', function () {
            testRunner.runTest(changeConfig(ATTRS_I1), changeConfig(ATTRS_I2));
        });
        it('should reduce attribute sets', function () {
            testRunner.runTest(
                changeConfig({ f1: { a1: 10, a2: 20, a3: 30 } }), changeConfig({ f1: { a1: 10, a2: 22, a4: 40 } }),
                changeConfig({ f1: { a2: 20, a3: 30 } }),         changeConfig({ f1: { a4: 40 } })
            );
        });
        it('should remove entire operation if nothing will change', function () {
            testRunner.runTest(changeConfig({ f1: { a1: 10 } }), changeConfig({ f1: { a1: 20 } }), null, []);
            testRunner.runTest(changeConfig({ f1: { a1: 10 } }), changeConfig({ f1: { a1: 10 } }), [],   []);
        });
    });

    // insertFont ---------------------------------------------------------

    describe('"insertFont" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runTest(insertFont('f1'), insertFont('f1'));
            testRunner.runBidiTest(insertFont('f1'), [insertTheme('t1'), STYLESHEET_OPS, AUTOSTYLE_OPS, CHART_OPS]);
        });
    });

    // insertTheme ---------------------------------------------------------

    describe('"insertTheme" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runTest(insertTheme('t1'), insertTheme('t1'));
            testRunner.runBidiTest(insertTheme('t1'), [STYLESHEET_OPS, AUTOSTYLE_OPS, CHART_OPS]);
        });
    });

    // insertStyleSheet ---------------------------------------------------

    describe('"insertStyleSheet" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(insertStyle('t1', 's1', ATTRS), [AUTOSTYLE_OPS, CHART_OPS]);
        });
    });

    describe('"insertStyleSheet" and "insertStyleSheet"', function () {
        it('should not process different style sheets', function () {
            testRunner.runTest(insertStyle('t1', 's1', ATTRS), opSeries1(insertStyle, ['t1', 's2', ATTRS], ['t2', 's1', ATTRS], ['t2', 's2', ATTRS]));
        });
        it('should set external operation to "removed" state', function () {
            testRunner.runTest(insertStyle('t1', 's1', ATTRS), insertStyle('t1', 's1', ATTRS), null, []);
        });
    });

    describe('"insertStyleSheet" and "deleteStyleSheet"', function () {
        it('should not process different style sheets', function () {
            testRunner.runBidiTest(insertStyle('t1', 's1', ATTRS), opSeries1(deleteStyle, ['t1', 's2'], ['t2', 's1'], ['t2', 's2']));
        });
        it('should set insert operation to "removed" state', function () {
            testRunner.runBidiTest(insertStyle('t1', 's1', ATTRS), deleteStyle('t1', 's1'), []);
        });
    });

    describe('"insertStyleSheet" and "changeStyleSheet"', function () {
        it('should not process different style sheets', function () {
            testRunner.runTest(insertStyle('t1', 's1', ATTRS), opSeries1(changeStyle, ['t1', 's2', ATTRS], ['t2', 's1', ATTRS], ['t2', 's2', ATTRS]));
        });
        it('should set change operation to "removed" state', function () {
            testRunner.runBidiTest(insertStyle('t1', 's1', ATTRS), changeStyle('t1', 's1', ATTRS), null, []);
        });
    });

    // deleteStyleSheet ---------------------------------------------------

    describe('"deleteStyleSheet" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(deleteStyle('t1', 's1'), [AUTOSTYLE_OPS, CHART_OPS]);
        });
    });

    describe('"deleteStyleSheet" and "deleteStyleSheet"', function () {
        it('should not process different style sheets', function () {
            testRunner.runTest(deleteStyle('t1', 's1'), opSeries1(deleteStyle, ['t1', 's2'], ['t2', 's1'], ['t2', 's2']));
        });
        it('should set both operations to "removed" state', function () {
            testRunner.runTest(deleteStyle('t1', 's1'), deleteStyle('t1', 's1'), [], []);
        });
    });

    describe('"deleteStyleSheet" and "changeStyleSheet"', function () {
        it('should not process different style sheets', function () {
            testRunner.runBidiTest(deleteStyle('t1', 's1'), opSeries1(changeStyle, ['t1', 's2', ATTRS], ['t2', 's1', ATTRS], ['t2', 's2', ATTRS]));
        });
        it('should set change operation to "removed" state', function () {
            testRunner.runBidiTest(deleteStyle('t1', 's1'), changeStyle('t1', 's1', ATTRS), null, []);
        });
    });

    // changeStyleSheet ---------------------------------------------------

    describe('"changeStyleSheet" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(changeStyle('t1', 's1', ATTRS), [AUTOSTYLE_OPS, CHART_OPS]);
        });
    });

    describe('"changeStyleSheet" and "changeStyleSheet"', function () {
        it('should not process different style sheets', function () {
            testRunner.runTest(changeStyle('t1', 's1', ATTRS), opSeries1(changeStyle, ['t1', 's2', ATTRS], ['t2', 's1', ATTRS], ['t2', 's2', ATTRS]));
        });
        it('should not process operations for independent attributes', function () {
            testRunner.runTest(changeStyle('t1', 's1', ATTRS_I1), changeStyle('t1', 's1', ATTRS_I2));
        });
        it('should reduce attribute sets', function () {
            testRunner.runTest(
                changeStyle('t1', 's1', ATTRS_O1), changeStyle('t1', 's1', ATTRS_O2),
                changeStyle('t1', 's1', ATTRS_R1), changeStyle('t1', 's1', ATTRS_R2)
            );
        });
        it('should reduce style sheet name, and parent style', function () {
            testRunner.runTest(
                changeStyle('t1', 's1', ATTRS_I1, { styleName: 'n1', parent: 'p1' }), changeStyle('t1', 's1', ATTRS_I2, { styleName: 'n1', parent: 'p1' }),
                changeStyle('t1', 's1', ATTRS_I1),                                    changeStyle('t1', 's1', ATTRS_I2)
            );
            testRunner.runTest(
                changeStyle('t1', 's1', ATTRS_I1, { styleName: 'n1', parent: 'p1' }), changeStyle('t1', 's1', ATTRS_I2, { styleName: 'n2', parent: 'p2' }),
                changeStyle('t1', 's1', ATTRS_I1, { styleName: 'n1', parent: 'p1' }), changeStyle('t1', 's1', ATTRS_I2)
            );
            testRunner.runTest(
                changeStyle('t1', 's1', ATTRS_I1, { styleName: 'n1', parent: 'p1' }), changeStyle('t1', 's1', ATTRS_I2),
                changeStyle('t1', 's1', ATTRS_I1, { styleName: 'n1', parent: 'p1' }), changeStyle('t1', 's1', ATTRS_I2)
            );
            testRunner.runTest(
                changeStyle('t1', 's1', ATTRS_I1), changeStyle('t1', 's1', ATTRS_I2, { styleName: 'n2', parent: 'p2' }),
                changeStyle('t1', 's1', ATTRS_I1), changeStyle('t1', 's1', ATTRS_I2, { styleName: 'n2', parent: 'p2' })
            );
        });
        it('should remove entire operation if nothing will change', function () {
            testRunner.runTest(
                changeStyle('t1', 's1', ATTRS_I1, { styleName: 'n1', parent: 'p1' }),
                changeStyle('t1', 's1', { f1: { a1: 20 } }, { styleName: 'n2', parent: 'p2' }),
                null, []
            );
            testRunner.runTest(
                changeStyle('t1', 's1', ATTRS, { styleName: 'n1', parent: 'p1' }),
                changeStyle('t1', 's1', ATTRS, { styleName: 'n1', parent: 'p1' }),
                [], []
            );
        });
    });

    // insertAutoStyle ----------------------------------------------------

    describe('"insertAutoStyle" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(insertAutoStyle('a1', ATTRS), CHART_OPS);
        });
    });

    describe('"insertAutoStyle" and "insertAutoStyle"', function () {
        var OTIS = { otIndexShift: true };
        it('should not process style sheets of different types', function () {
            testRunner.runTest(opSeries1(insertAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]), opSeries2(insertAutoStyle, 't2', ['a1', 'a2'], ATTRS));
        });
        it('should shift style identifiers in indexed mode', function () {
            testRunner.runTest(insertAutoStyle('a2', ATTRS), insertAutoStyle('a1', ATTRS), insertAutoStyle('a3', ATTRS),       insertAutoStyle('a1', ATTRS, OTIS));
            testRunner.runTest(insertAutoStyle('a2', ATTRS), insertAutoStyle('a2', ATTRS), insertAutoStyle('a3', ATTRS),       insertAutoStyle('a2', ATTRS, OTIS));
            testRunner.runTest(insertAutoStyle('a2', ATTRS), insertAutoStyle('a3', ATTRS), insertAutoStyle('a2', ATTRS, OTIS), insertAutoStyle('a4', ATTRS));
            testRunner.runTest(insertAutoStyle('a2', ATTRS), insertAutoStyle('a4', ATTRS), insertAutoStyle('a2', ATTRS, OTIS), insertAutoStyle('a5', ATTRS));
            // with/without type
            testRunner.runTest(insertAutoStyle('a2', ATTRS),       insertAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('a3', ATTRS),       insertAutoStyle('t1', 'a2', ATTRS, OTIS));
            testRunner.runTest(insertAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('a2', ATTRS),       insertAutoStyle('t1', 'a3', ATTRS), insertAutoStyle('a2', ATTRS, OTIS));
            testRunner.runTest(insertAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('t1', 'a3', ATTRS), insertAutoStyle('t1', 'a2', ATTRS, OTIS));
        });
        it('should shift style identifiers in indexed mode multiple times', function () {
            testRunner.runTest(
                opSeries2(insertAutoStyle, ['a2', 'a3', 'a4'], ATTRS), opSeries2(insertAutoStyle, ['a2', 'a3', 'a4'], ATTRS),
                opSeries2(insertAutoStyle, ['a5', 'a6', 'a7'], ATTRS), opSeries2(insertAutoStyle, ['a2', 'a3', 'a4'], ATTRS, OTIS)
            );
        });
        it('should fail for invalid style identifiers in indexed mode', function () {
            testRunner.expectError(insertAutoStyle('a2', ATTRS), insertAutoStyle('b2', ATTRS));
            testRunner.expectError(insertAutoStyle('b2', ATTRS), insertAutoStyle('a2', ATTRS));
            testRunner.expectError(insertAutoStyle('b2', ATTRS), insertAutoStyle('b2', ATTRS));
        });
        it('should not shift style identifiers in freestyle mode', function () {
            testRunner.runTest(insertAutoStyle('t2', 'a2', ATTRS), insertAutoStyle('t2', 'a1', ATTRS));
            testRunner.runTest(insertAutoStyle('t2', 'a2', ATTRS), insertAutoStyle('t2', 'a3', ATTRS));
        });
        it('should fail to insert auto-style twice in freestyle mode', function () {
            testRunner.expectError(insertAutoStyle('t2', 'a2', ATTRS), insertAutoStyle('t2', 'a2', ATTRS));
        });
    });

    describe('"insertAutoStyle" and "deleteAutoStyle"', function () {
        var OTIS = { otIndexShift: true };
        it('should not process style sheets of different types', function () {
            testRunner.runBidiTest(opSeries1(insertAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]), opSeries2(deleteAutoStyle, 't2', ['a1', 'a2']));
        });
        it('should shift style identifiers in indexed mode', function () {
            testRunner.runBidiTest(insertAutoStyle('a3', ATTRS), deleteAutoStyle('a1'), insertAutoStyle('a2', ATTRS),       deleteAutoStyle('a1', OTIS));
            testRunner.runBidiTest(insertAutoStyle('a3', ATTRS), deleteAutoStyle('a2'), insertAutoStyle('a2', ATTRS),       deleteAutoStyle('a2', OTIS));
            testRunner.runBidiTest(insertAutoStyle('a3', ATTRS), deleteAutoStyle('a3'), insertAutoStyle('a3', ATTRS, OTIS), deleteAutoStyle('a4'));
            testRunner.runBidiTest(insertAutoStyle('a3', ATTRS), deleteAutoStyle('a4'), insertAutoStyle('a3', ATTRS, OTIS), deleteAutoStyle('a5'));
            testRunner.runBidiTest(insertAutoStyle('a3', ATTRS), deleteAutoStyle('a5'), insertAutoStyle('a3', ATTRS, OTIS), deleteAutoStyle('a6'));
            // with/without type
            testRunner.runBidiTest(insertAutoStyle('a3', ATTRS),       deleteAutoStyle('t1', 'a1'), insertAutoStyle('a2', ATTRS),       deleteAutoStyle('t1', 'a1', OTIS));
            testRunner.runBidiTest(insertAutoStyle('t1', 'a3', ATTRS), deleteAutoStyle('a1'),       insertAutoStyle('t1', 'a2', ATTRS), deleteAutoStyle('a1', OTIS));
            testRunner.runBidiTest(insertAutoStyle('t1', 'a3', ATTRS), deleteAutoStyle('t1', 'a1'), insertAutoStyle('t1', 'a2', ATTRS), deleteAutoStyle('t1', 'a1', OTIS));
        });
        it('should fail for invalid style identifiers in indexed mode', function () {
            testRunner.expectBidiError(insertAutoStyle('a2', ATTRS), deleteAutoStyle('b2'));
            testRunner.expectBidiError(insertAutoStyle('b2', ATTRS), deleteAutoStyle('a2'));
            testRunner.expectBidiError(insertAutoStyle('b2', ATTRS), deleteAutoStyle('b2'));
        });
        it('should not shift style identifiers in freestyle mode', function () {
            testRunner.runBidiTest(insertAutoStyle('t2', 'a3', ATTRS), deleteAutoStyle('t2', 'a2'));
            testRunner.runBidiTest(insertAutoStyle('t2', 'a3', ATTRS), deleteAutoStyle('t2', 'a4'));
        });
        it('should fail to insert and delete same auto-style in freestyle mode', function () {
            testRunner.expectBidiError(insertAutoStyle('t2', 'a3', ATTRS), deleteAutoStyle('t2', 'a3'));
        });
    });

    describe('"insertAutoStyle" and "changeAutoStyle"', function () {
        var OTIS = { otIndexShift: true };
        it('should not process style sheets of different types', function () {
            testRunner.runBidiTest(opSeries1(insertAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]), opSeries2(changeAutoStyle, 't2', ['a1', 'a2'], ATTRS));
        });
        it('should shift style identifiers in indexed mode', function () {
            testRunner.runBidiTest(insertAutoStyle('a2', ATTRS), changeAutoStyle('a0', ATTRS), insertAutoStyle('a2', ATTRS),       changeAutoStyle('a0', ATTRS));
            testRunner.runBidiTest(insertAutoStyle('a2', ATTRS), changeAutoStyle('a1', ATTRS), insertAutoStyle('a2', ATTRS),       changeAutoStyle('a1', ATTRS));
            testRunner.runBidiTest(insertAutoStyle('a2', ATTRS), changeAutoStyle('a2', ATTRS), insertAutoStyle('a2', ATTRS, OTIS), changeAutoStyle('a3', ATTRS));
            testRunner.runBidiTest(insertAutoStyle('a2', ATTRS), changeAutoStyle('a3', ATTRS), insertAutoStyle('a2', ATTRS, OTIS), changeAutoStyle('a4', ATTRS));
            // with/without type
            testRunner.runBidiTest(insertAutoStyle('a2', ATTRS),       changeAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('a2', ATTRS, OTIS),       changeAutoStyle('t1', 'a3', ATTRS));
            testRunner.runBidiTest(insertAutoStyle('t1', 'a2', ATTRS), changeAutoStyle('a2', ATTRS),       insertAutoStyle('t1', 'a2', ATTRS, OTIS), changeAutoStyle('a3', ATTRS));
            testRunner.runBidiTest(insertAutoStyle('t1', 'a2', ATTRS), changeAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('t1', 'a2', ATTRS, OTIS), changeAutoStyle('t1', 'a3', ATTRS));
        });
        it('should fail for invalid style identifiers in indexed mode', function () {
            testRunner.expectBidiError(insertAutoStyle('a2', ATTRS), changeAutoStyle('b2', ATTRS));
            testRunner.expectBidiError(insertAutoStyle('b2', ATTRS), changeAutoStyle('a2', ATTRS));
            testRunner.expectBidiError(insertAutoStyle('b2', ATTRS), changeAutoStyle('b2', ATTRS));
        });
        it('should not shift style identifiers in freestyle mode', function () {
            testRunner.runBidiTest(insertAutoStyle('t2', 'a2', ATTRS), changeAutoStyle('t2', 'a1', ATTRS));
            testRunner.runBidiTest(insertAutoStyle('t2', 'a2', ATTRS), changeAutoStyle('t2', 'a3', ATTRS));
        });
        it('should fail to insert existing auto-style in freestyle mode', function () {
            testRunner.expectBidiError(insertAutoStyle('t2', 'a2', ATTRS), changeAutoStyle('t2', 'a2', ATTRS));
        });
    });

    // deleteAutoStyle ----------------------------------------------------

    describe('"deleteAutoStyle" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(deleteAutoStyle('a1'), CHART_OPS);
        });
    });

    describe('"deleteAutoStyle" and "deleteAutoStyle"', function () {
        var OTIS = { otIndexShift: true };
        it('should not process style sheets of different types', function () {
            testRunner.runTest(opSeries1(deleteAutoStyle, 'a1', ['t1', 'a1']), opSeries2(deleteAutoStyle, 't2', ['a1', 'a2']));
        });
        it('should shift style identifiers in indexed mode', function () {
            testRunner.runTest(deleteAutoStyle('a3'), deleteAutoStyle('a1'), deleteAutoStyle('a2'),       deleteAutoStyle('a1', OTIS));
            testRunner.runTest(deleteAutoStyle('a3'), deleteAutoStyle('a2'), deleteAutoStyle('a2'),       deleteAutoStyle('a2', OTIS));
            testRunner.runTest(deleteAutoStyle('a3'), deleteAutoStyle('a3'), [],                          []);
            testRunner.runTest(deleteAutoStyle('a3'), deleteAutoStyle('a4'), deleteAutoStyle('a3', OTIS), deleteAutoStyle('a3'));
            testRunner.runTest(deleteAutoStyle('a3'), deleteAutoStyle('a5'), deleteAutoStyle('a3', OTIS), deleteAutoStyle('a4'));
            // with/without type
            testRunner.runTest(deleteAutoStyle('a3'),       deleteAutoStyle('t1', 'a1'), deleteAutoStyle('a2'),       deleteAutoStyle('t1', 'a1', OTIS));
            testRunner.runTest(deleteAutoStyle('t1', 'a3'), deleteAutoStyle('a1'),       deleteAutoStyle('t1', 'a2'), deleteAutoStyle('a1', OTIS));
            testRunner.runTest(deleteAutoStyle('t1', 'a3'), deleteAutoStyle('t1', 'a1'), deleteAutoStyle('t1', 'a2'), deleteAutoStyle('t1', 'a1', OTIS));
        });
        it('should fail for invalid style identifiers in indexed mode', function () {
            testRunner.expectError(deleteAutoStyle('a2'), deleteAutoStyle('b2'));
            testRunner.expectError(deleteAutoStyle('b2'), deleteAutoStyle('a2'));
            testRunner.expectError(deleteAutoStyle('b2'), deleteAutoStyle('b1'));
        });
        it('should not shift style identifiers in freestyle mode', function () {
            testRunner.runTest(deleteAutoStyle('t2', 'a3'), deleteAutoStyle('t2', 'a1'));
            testRunner.runTest(deleteAutoStyle('t2', 'a3'), deleteAutoStyle('t2', 'a2'));
            testRunner.runTest(deleteAutoStyle('t2', 'a3'), deleteAutoStyle('t2', 'a3'), [], []);
            testRunner.runTest(deleteAutoStyle('t2', 'a3'), deleteAutoStyle('t2', 'a4'));
            testRunner.runTest(deleteAutoStyle('t2', 'a3'), deleteAutoStyle('t2', 'a5'));
        });
    });

    describe('"deleteAutoStyle" and "changeAutoStyle"', function () {
        var OTIS = { otIndexShift: true };
        it('should not process style sheets of different types', function () {
            testRunner.runBidiTest(opSeries1(deleteAutoStyle, 'a1', ['t1', 'a1']), opSeries2(changeAutoStyle, 't2', ['a1', 'a2'], ATTRS));
        });
        it('should shift style identifiers in indexed mode', function () {
            testRunner.runBidiTest(deleteAutoStyle('a3'), changeAutoStyle('a1', ATTRS), deleteAutoStyle('a3'),       changeAutoStyle('a1', ATTRS));
            testRunner.runBidiTest(deleteAutoStyle('a3'), changeAutoStyle('a2', ATTRS), deleteAutoStyle('a3'),       changeAutoStyle('a2', ATTRS));
            testRunner.runBidiTest(deleteAutoStyle('a3'), changeAutoStyle('a3', ATTRS), null,                        []);
            testRunner.runBidiTest(deleteAutoStyle('a3'), changeAutoStyle('a4', ATTRS), deleteAutoStyle('a3', OTIS), changeAutoStyle('a3', ATTRS));
            testRunner.runBidiTest(deleteAutoStyle('a3'), changeAutoStyle('a5', ATTRS), deleteAutoStyle('a3', OTIS), changeAutoStyle('a4', ATTRS));
            // with/without type
            testRunner.runBidiTest(deleteAutoStyle('a3'),       changeAutoStyle('t1', 'a5', ATTRS), deleteAutoStyle('a3', OTIS),       changeAutoStyle('t1', 'a4', ATTRS));
            testRunner.runBidiTest(deleteAutoStyle('t1', 'a3'), changeAutoStyle('a5', ATTRS),       deleteAutoStyle('t1', 'a3', OTIS), changeAutoStyle('a4', ATTRS));
            testRunner.runBidiTest(deleteAutoStyle('t1', 'a3'), changeAutoStyle('t1', 'a5', ATTRS), deleteAutoStyle('t1', 'a3', OTIS), changeAutoStyle('t1', 'a4', ATTRS));
        });
        it('should fail for invalid style identifiers in indexed mode', function () {
            testRunner.expectBidiError(deleteAutoStyle('a2'), changeAutoStyle('b2', ATTRS));
            testRunner.expectBidiError(deleteAutoStyle('b2'), changeAutoStyle('a2', ATTRS));
            testRunner.expectBidiError(deleteAutoStyle('b2'), changeAutoStyle('b3', ATTRS));
        });
        it('should not shift style identifiers in freestyle mode', function () {
            testRunner.runBidiTest(deleteAutoStyle('t2', 'a3'), changeAutoStyle('t2', 'a1', ATTRS));
            testRunner.runBidiTest(deleteAutoStyle('t2', 'a3'), changeAutoStyle('t2', 'a2', ATTRS));
            testRunner.runBidiTest(deleteAutoStyle('t2', 'a3'), changeAutoStyle('t2', 'a3', ATTRS), null, []);
            testRunner.runBidiTest(deleteAutoStyle('t2', 'a3'), changeAutoStyle('t2', 'a4', ATTRS));
            testRunner.runBidiTest(deleteAutoStyle('t2', 'a3'), changeAutoStyle('t2', 'a5', ATTRS));
        });
    });

    // changeAutoStyle ----------------------------------------------------

    describe('"changeAutoStyle" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(changeAutoStyle('a1', ATTRS), CHART_OPS);
        });
    });

    describe('"changeAutoStyle" and "changeAutoStyle"', function () {
        it('should not process different styles', function () {
            testRunner.runTest(
                opSeries1(changeAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]),
                opSeries1(changeAutoStyle, ['a2', ATTRS], ['t1', 'a2', ATTRS], ['t2', 'a1', ATTRS], ['t2', 'a2', ATTRS])
            );
        });
        it('should not process operations for independent attributes', function () {
            testRunner.runTest(changeAutoStyle('t1', 'a1', ATTRS_I1), changeAutoStyle('t1', 'a1', ATTRS_I2));
        });
        it('should reduce attribute sets', function () {
            testRunner.runTest(
                changeAutoStyle('t1', 'a1', ATTRS_O1), changeAutoStyle('t1', 'a1', ATTRS_O2),
                changeAutoStyle('t1', 'a1', ATTRS_R1), changeAutoStyle('t1', 'a1', ATTRS_R2)
            );
        });
        it('should set equal operations to "removed" state', function () {
            testRunner.runTest(
                opSeries1(changeAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]),
                opSeries1(changeAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]),
                [], []
            );
            testRunner.runBidiTest(changeAutoStyle('a1', {}), changeAutoStyle('a1', ATTRS), [], null);
        });
    });

    // insertChartSeries --------------------------------------------------

    describe('"insertChartSeries" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(insertChSeries(1, 0, ATTRS), [CHART_AXIS_OPS, CHART_COMP_OPS]);
        });
    });

    describe('"insertChartSeries" and "insertChartSeries"', function () {
        it('should not process different charts', function () {
            testRunner.runBidiTest(opSeries2(insertChSeries, 1, [1, 2], ATTRS), opSeries2(insertChSeries, 2, [1, 2], ATTRS));
        });
        it('should shift series index', function () {
            testRunner.runTest(insertChSeries(1, 2, ATTRS), insertChSeries(1, 0, ATTRS), insertChSeries(1, 3, ATTRS), insertChSeries(1, 0, ATTRS));
            testRunner.runTest(insertChSeries(1, 2, ATTRS), insertChSeries(1, 1, ATTRS), insertChSeries(1, 3, ATTRS), insertChSeries(1, 1, ATTRS));
            testRunner.runTest(insertChSeries(1, 2, ATTRS), insertChSeries(1, 2, ATTRS), insertChSeries(1, 3, ATTRS), insertChSeries(1, 2, ATTRS));
            testRunner.runTest(insertChSeries(1, 2, ATTRS), insertChSeries(1, 3, ATTRS), insertChSeries(1, 2, ATTRS), insertChSeries(1, 4, ATTRS));
            testRunner.runTest(insertChSeries(1, 2, ATTRS), insertChSeries(1, 4, ATTRS), insertChSeries(1, 2, ATTRS), insertChSeries(1, 5, ATTRS));
        });
    });

    describe('"insertChartSeries" and "deleteChartSeries"', function () {
        it('should not process different charts', function () {
            testRunner.runBidiTest(opSeries2(insertChSeries, 1, [1, 2], ATTRS), opSeries2(deleteChSeries, 2, [1, 2]));
        });
        it('should handle inserted/deleted series without collision', function () {
            testRunner.runBidiTest(insertChSeries(1, 0, ATTRS), deleteChSeries(1, 2), insertChSeries(1, 0, ATTRS), deleteChSeries(1, 3));
            testRunner.runBidiTest(insertChSeries(1, 1, ATTRS), deleteChSeries(1, 2), insertChSeries(1, 1, ATTRS), deleteChSeries(1, 3));
            testRunner.runBidiTest(insertChSeries(1, 2, ATTRS), deleteChSeries(1, 2), insertChSeries(1, 2, ATTRS), deleteChSeries(1, 3));
            testRunner.runBidiTest(insertChSeries(1, 3, ATTRS), deleteChSeries(1, 2), insertChSeries(1, 2, ATTRS), deleteChSeries(1, 2));
            testRunner.runBidiTest(insertChSeries(1, 4, ATTRS), deleteChSeries(1, 2), insertChSeries(1, 3, ATTRS), deleteChSeries(1, 2));
        });
    });

    describe('"insertChartSeries" and "changeChartSeries"', function () {
        it('should not process different charts', function () {
            testRunner.runBidiTest(opSeries2(insertChSeries, 1, [1, 2], ATTRS), opSeries2(changeChSeries, 2, [1, 2], ATTRS));
        });
        it('should shift series index in change operation', function () {
            testRunner.runBidiTest(insertChSeries(1, 0, ATTRS), changeChSeries(1, 2, ATTRS), null, changeChSeries(1, 3, ATTRS));
            testRunner.runBidiTest(insertChSeries(1, 1, ATTRS), changeChSeries(1, 2, ATTRS), null, changeChSeries(1, 3, ATTRS));
            testRunner.runBidiTest(insertChSeries(1, 2, ATTRS), changeChSeries(1, 2, ATTRS), null, changeChSeries(1, 3, ATTRS));
            testRunner.runBidiTest(insertChSeries(1, 3, ATTRS), changeChSeries(1, 2, ATTRS));
            testRunner.runBidiTest(insertChSeries(1, 4, ATTRS), changeChSeries(1, 2, ATTRS));
        });
    });

    // deleteChartSeries --------------------------------------------------

    describe('"deleteChartSeries" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(deleteChSeries(1, 0), [CHART_AXIS_OPS, CHART_COMP_OPS]);
        });
    });

    describe('"deleteChartSeries" and "deleteChartSeries"', function () {
        it('should not process different charts', function () {
            testRunner.runBidiTest(opSeries2(deleteChSeries, 1, [1, 2]), opSeries2(deleteChSeries, 2, [1, 2]));
        });
        it('should shift series indexes', function () {
            testRunner.runTest(deleteChSeries(1, 0), deleteChSeries(1, 2), deleteChSeries(1, 0), deleteChSeries(1, 1));
            testRunner.runTest(deleteChSeries(1, 1), deleteChSeries(1, 2), deleteChSeries(1, 1), deleteChSeries(1, 1));
            testRunner.runTest(deleteChSeries(1, 2), deleteChSeries(1, 2), [],                   []);
            testRunner.runTest(deleteChSeries(1, 3), deleteChSeries(1, 2), deleteChSeries(1, 2), deleteChSeries(1, 2));
            testRunner.runTest(deleteChSeries(1, 4), deleteChSeries(1, 2), deleteChSeries(1, 3), deleteChSeries(1, 2));
        });
    });

    describe('"deleteChartSeries" and "changeChartSeries"', function () {
        it('should not process different charts', function () {
            testRunner.runBidiTest(opSeries2(deleteChSeries, 1, [1, 2]), opSeries2(changeChSeries, 2, [1, 2], ATTRS));
        });
        it('should shift series indexes in change operation', function () {
            testRunner.runTest(deleteChSeries(1, 2), changeChSeries(1, 0, ATTRS), null, changeChSeries(1, 0, ATTRS));
            testRunner.runTest(deleteChSeries(1, 2), changeChSeries(1, 1, ATTRS), null, changeChSeries(1, 1, ATTRS));
            testRunner.runTest(deleteChSeries(1, 2), changeChSeries(1, 2, ATTRS), null, []);
            testRunner.runTest(deleteChSeries(1, 2), changeChSeries(1, 3, ATTRS), null, changeChSeries(1, 2, ATTRS));
            testRunner.runTest(deleteChSeries(1, 2), changeChSeries(1, 4, ATTRS), null, changeChSeries(1, 3, ATTRS));
        });
    });

    // changeChartSeries --------------------------------------------------

    describe('"changeChartSeries" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(changeChSeries(1, 0, ATTRS), [CHART_AXIS_OPS, CHART_COMP_OPS]);
        });
    });

    describe('"changeChartSeries" and "changeChartSeries"', function () {
        it('should not process different charts', function () {
            testRunner.runTest(changeChSeries(1, 2, ATTRS), changeChSeries(2, 2, ATTRS));
        });
        it('should not process operations for independent attributes', function () {
            testRunner.runTest(changeChSeries(1, 2, ATTRS_I1), changeChSeries(1, 2, ATTRS_I2));
        });
        it('should reduce attribute sets', function () {
            testRunner.runTest(
                changeChSeries(1, 2, ATTRS_O1), changeChSeries(1, 2, ATTRS_O2),
                changeChSeries(1, 2, ATTRS_R1), changeChSeries(1, 2, ATTRS_R2)
            );
        });
        it('should set equal operations to "removed" state', function () {
            testRunner.runTest(changeChSeries(1, 2, ATTRS), changeChSeries(1, 2, ATTRS), [], []);
            testRunner.runBidiTest(changeChSeries(1, 2, {}), changeChSeries(1, 2, ATTRS), [], null);
        });
    });

    // deleteChartAxis ----------------------------------------------------

    describe('"deleteChartAxis" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChLegend(1, ATTRS));
        });
    });

    describe('"deleteChartAxis" and "deleteChartAxis"', function () {
        it('should not process different charts', function () {
            testRunner.runTest(deleteChAxis(1, 0), deleteChAxis(2, 0));
        });
        it('should not process different axes', function () {
            testRunner.runTest(deleteChAxis(1, 0), deleteChAxis(1, 1));
        });
        it('should set equal operations to "removed" state', function () {
            testRunner.runTest(deleteChAxis(1, 0), deleteChAxis(1, 0), [], []);
        });
    });

    describe('"deleteChartAxis" and "changeChartAxis"', function () {
        it('should not process different charts', function () {
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChAxis(2, 0, ATTRS));
        });
        it('should not process different axes', function () {
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChAxis(1, 1, ATTRS));
        });
        it('should set change operation to "removed" state', function () {
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChAxis(1, 0, ATTRS), null, []);
        });
    });

    describe('"deleteChartAxis" and "changeChartGrid"', function () {
        it('should not process different charts', function () {
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChGrid(2, 0, ATTRS));
        });
        it('should not process different axes', function () {
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChGrid(1, 1, ATTRS));
        });
        it('should set change operation to "removed" state', function () {
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChGrid(1, 0, ATTRS), null, []);
        });
    });

    describe('"deleteChartAxis" and "changeChartTitle"', function () {
        it('should not process different charts', function () {
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChTitle(2, 0, ATTRS));
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChTitle(2, ATTRS));
        });
        it('should not process different axes', function () {
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChTitle(1, 1, ATTRS));
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChTitle(1, ATTRS));
        });
        it('should set change operation to "removed" state', function () {
            testRunner.runBidiTest(deleteChAxis(1, 0), changeChTitle(1, 0, ATTRS), null, []);
        });
    });

    // changeChartAxis ----------------------------------------------------

    describe('"changeChartAxis" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(changeChAxis(1, 0, ATTRS), CHART_COMP_OPS);
        });
    });

    describe('"changeChartAxis" and "changeChartAxis"', function () {
        it('should not process different charts', function () {
            testRunner.runTest(changeChAxis(1, 0, ATTRS), changeChAxis(2, 0, ATTRS));
        });
        it('should not process different axes', function () {
            testRunner.runTest(changeChAxis(1, 0, ATTRS), changeChAxis(1, 1, ATTRS));
        });
        it('should not process operations for independent attributes', function () {
            testRunner.runTest(changeChAxis(1, 0, ATTRS_I1), changeChAxis(1, 0, ATTRS_I2));
        });
        it('should reduce attribute sets', function () {
            testRunner.runTest(
                changeChAxis(1, 0, ATTRS_O1), changeChAxis(1, 0, ATTRS_O2),
                changeChAxis(1, 0, ATTRS_R1), changeChAxis(1, 0, ATTRS_R2)
            );
        });
        it('should set equal operations to "removed" state', function () {
            testRunner.runTest(changeChAxis(1, 0, ATTRS), changeChAxis(1, 0, ATTRS), [], []);
            testRunner.runBidiTest(changeChAxis(1, 0, {}), changeChAxis(1, 0, ATTRS), [], null);
        });
    });

    // changeChartGrid ----------------------------------------------------

    describe('"changeChartGrid" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(changeChGrid(1, 0, ATTRS), [changeChTitle(1, 0, ATTRS), changeChLegend(1, ATTRS)]);
        });
    });

    describe('"changeChartGrid" and "changeChartGrid"', function () {
        it('should not process different charts', function () {
            testRunner.runTest(changeChGrid(1, 0, ATTRS), changeChGrid(2, 0, ATTRS));
        });
        it('should not process different axes', function () {
            testRunner.runTest(changeChGrid(1, 0, ATTRS), changeChGrid(1, 1, ATTRS));
        });
        it('should not process operations for independent attributes', function () {
            testRunner.runTest(changeChGrid(1, 0, ATTRS_I1), changeChGrid(1, 0, ATTRS_I2));
        });
        it('should reduce attribute sets', function () {
            testRunner.runTest(
                changeChGrid(1, 0, ATTRS_O1), changeChGrid(1, 0, ATTRS_O2),
                changeChGrid(1, 0, ATTRS_R1), changeChGrid(1, 0, ATTRS_R2)
            );
        });
        it('should set equal operations to "removed" state', function () {
            testRunner.runTest(changeChGrid(1, 0, ATTRS), changeChGrid(1, 0, ATTRS), [], []);
            testRunner.runBidiTest(changeChGrid(1, 0, {}), changeChGrid(1, 0, ATTRS), [], null);
        });
    });

    // changeChartTitle ---------------------------------------------------

    describe('"changeChartTitle" and independent operations', function () {
        it('should skip the transformations', function () {
            testRunner.runBidiTest(changeChTitle(1, 0, ATTRS), changeChLegend(1, ATTRS));
        });
    });

    describe('"changeChartTitle" and "changeChartTitle"', function () {
        it('should not process different charts', function () {
            testRunner.runTest(changeChTitle(1, ATTRS), changeChTitle(2, ATTRS));
        });
        it('should not process different axes', function () {
            testRunner.runBidiTest(changeChTitle(1, ATTRS), changeChTitle(1, 0, ATTRS));
            testRunner.runTest(changeChTitle(1, 0, ATTRS), changeChTitle(1, 1, ATTRS));
        });
        it('should not process operations for independent attributes', function () {
            testRunner.runTest(changeChTitle(1, ATTRS_I1), changeChTitle(1, ATTRS_I2));
        });
        it('should reduce attribute sets', function () {
            testRunner.runTest(
                changeChTitle(1, ATTRS_O1), changeChTitle(1, ATTRS_O2),
                changeChTitle(1, ATTRS_R1), changeChTitle(1, ATTRS_R2)
            );
        });
        it('should set equal operations to "removed" state', function () {
            testRunner.runTest(changeChTitle(1, ATTRS), changeChTitle(1, ATTRS), [], []);
            testRunner.runTest(changeChTitle(1, 0, ATTRS), changeChTitle(1, 0, ATTRS), [], []);
            testRunner.runBidiTest(changeChTitle(1, {}), changeChTitle(1, ATTRS), [], null);
        });
    });

    // changeChartLegend --------------------------------------------------

    describe('"changeChartLegend" and "changeChartLegend"', function () {
        it('should not process different charts', function () {
            testRunner.runTest(changeChLegend(1, ATTRS), changeChLegend(2, ATTRS));
        });
        it('should not process operations for independent attributes', function () {
            testRunner.runTest(changeChLegend(1, ATTRS_I1), changeChLegend(1, ATTRS_I2));
        });
        it('should reduce attribute sets', function () {
            testRunner.runTest(
                changeChLegend(1, ATTRS_O1), changeChLegend(1, ATTRS_O2),
                changeChLegend(1, ATTRS_R1), changeChLegend(1, ATTRS_R2)
            );
        });
        it('should set equal operations to "removed" state', function () {
            testRunner.runTest(changeChLegend(1, ATTRS), changeChLegend(1, ATTRS), [], []);
            testRunner.runBidiTest(changeChLegend(1, {}), changeChLegend(1, ATTRS), [], null);
        });
    });
});
