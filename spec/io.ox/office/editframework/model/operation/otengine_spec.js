/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from '$/underscore';

import { OTEngine } from '@/io.ox/office/editframework/model/operation/otengine';

import { makeOp } from '~/othelper';

// tests ======================================================================

describe('module editframework/model/operation/otengine', function () {

    // expects that a transformation handler exists in the engine
    function expectHandler(otEngine, opName1, opName2) {
        expect(otEngine.hasRawHandler(opName1, opName2)).toBeTrue();
    }

    // expects that NO transformation handler exists in the engine
    function expectNoHandler(otEngine, opName1, opName2) {
        expect(otEngine.hasRawHandler(opName1, opName2)).toBeFalse();
    }

    // class OTEngine =========================================================

    describe('class OTEngine', function () {

        it('should exist', function () {
            expect(OTEngine).toBeFunction();
        });

        // protected methods --------------------------------------------------

        describe('method registerTransformation', function () {
            it('should exist', function () {
                expect(OTEngine).toHaveMethod('registerTransformation');
            });
            it('should take a transformation handler', function () {
                var otEngine = new OTEngine();
                otEngine.registerTransformation('op1', 'op2', _.noop);
                expectHandler(otEngine, 'op1', 'op2');
                expectNoHandler(otEngine, 'op2', 'op1');
                otEngine.registerTransformation('op2', 'op3', null);
                expectHandler(otEngine, 'op2', 'op3');
                expectNoHandler(otEngine, 'op3', 'op2');
                otEngine.registerTransformation('op3', 'op4', 'error');
                expectHandler(otEngine, 'op3', 'op4');
                expectNoHandler(otEngine, 'op4', 'op3');
            });
            it('should fail for double registration', function () {
                var otEngine = new OTEngine();
                otEngine.registerTransformation('op1', 'op2', _.noop);
                expect(() => otEngine.registerTransformation('op1', 'op2', null)).toThrow();
            });
            it('should fail for empty operation names', function () {
                var otEngine = new OTEngine();
                expect(() => otEngine.registerTransformation('op1', '', null)).toThrow();
                expectNoHandler(otEngine, 'op1', '');
                expect(() => otEngine.registerTransformation('', 'op2', null)).toThrow();
                expectNoHandler(otEngine, '', 'op2');
            });
        });

        describe('method registerSelfTransformation', function () {
            it('should exist', function () {
                expect(OTEngine).toHaveMethod('registerSelfTransformation');
            });
            it('should take a transformation handler', function () {
                var otEngine = new OTEngine();
                otEngine.registerSelfTransformation('op1', _.noop);
                expectHandler(otEngine, 'op1', 'op1');
                otEngine.registerSelfTransformation('op2', null);
                expectHandler(otEngine, 'op2', 'op2');
                otEngine.registerSelfTransformation('op3', 'error');
                expectHandler(otEngine, 'op3', 'op3');
            });
            it('should fail for double registration', function () {
                var otEngine = new OTEngine();
                otEngine.registerTransformation('op1', 'op1', _.noop);
                expect(() => otEngine.registerSelfTransformation('op1', null)).toThrow();
            });
        });

        describe('method registerBidiTransformation', function () {
            it('should exist', function () {
                expect(OTEngine).toHaveMethod('registerBidiTransformation');
            });
            it('should take a transformation handler', function () {
                var otEngine = new OTEngine();
                otEngine.registerBidiTransformation('op1', 'op2', _.noop);
                expectHandler(otEngine, 'op1', 'op2');
                expectHandler(otEngine, 'op2', 'op1');
                otEngine.registerBidiTransformation('op2', 'op3', null);
                expectHandler(otEngine, 'op2', 'op3');
                expectHandler(otEngine, 'op3', 'op2');
                otEngine.registerBidiTransformation('op3', 'op4', 'error');
                expectHandler(otEngine, 'op3', 'op4');
                expectHandler(otEngine, 'op4', 'op3');
            });
            it('should fail for double registration', function () {
                var otEngine = new OTEngine();
                otEngine.registerTransformation('op1', 'op2', _.noop);
                otEngine.registerBidiTransformation('op2', 'op3', _.noop);
                expect(() => otEngine.registerBidiTransformation('op2', 'op1', null)).toThrow();
                expect(() => otEngine.registerBidiTransformation('op3', 'op2', null)).toThrow();
                expect(() => otEngine.registerBidiTransformation('op1', 'op1', null)).toThrow();
            });
        });

        describe('method registerTransformations', function () {
            it('should exist', function () {
                expect(OTEngine).toHaveMethod('registerTransformations');
            });
            it('should register all transformation handlers', function () {
                var otEngine = new OTEngine();
                otEngine.registerTransformations({
                    op1: {
                        op2: _.noop
                    },
                    op2: {
                        op1: null,
                        op2: _.noop
                    }
                });
                expectHandler(otEngine, 'op1', 'op2');
                expectHandler(otEngine, 'op2', 'op1');
                expectHandler(otEngine, 'op2', 'op2');
                expectNoHandler(otEngine, 'op1', 'op1');
            });
        });

        describe('method registerIgnoreNames', function () {
            it('should exist', function () {
                expect(OTEngine).toHaveMethod('registerIgnoreNames');
            });
        });

        describe('method registerAliasName', function () {
            it('should exist', function () {
                expect(OTEngine).toHaveMethod('registerAliasName');
            });
        });

        // public methods -----------------------------------------------------

        describe('method hasAliasName', function () {
            it('should exist', function () {
                expect(OTEngine).toHaveMethod('hasAliasName');
            });
            it('should return whether the alias exists', function () {
                var otEngine = new OTEngine();
                otEngine.registerAliasName('alias1', 'op1', 'op2');
                otEngine.registerAliasName('alias2', 'op1');
                expect(otEngine.hasAliasName(makeOp('op1'), 'alias1')).toBeTrue();
                expect(otEngine.hasAliasName(makeOp('op2'), 'alias1')).toBeTrue();
                expect(otEngine.hasAliasName(makeOp('op3'), 'alias1')).toBeFalse();
                expect(otEngine.hasAliasName(makeOp('op1'), 'alias2')).toBeTrue();
                expect(otEngine.hasAliasName(makeOp('op2'), 'alias2')).toBeFalse();
                expect(otEngine.hasAliasName(makeOp('op3'), 'alias2')).toBeFalse();
                expect(otEngine.hasAliasName(makeOp('op1'), 'alias3')).toBeFalse();
                expect(otEngine.hasAliasName(makeOp('op2'), 'alias3')).toBeFalse();
                expect(otEngine.hasAliasName(makeOp('op3'), 'alias3')).toBeFalse();
            });
            it('should support alias cascades', function () {
                var otEngine = new OTEngine();
                otEngine.registerAliasName('alias1', 'op1');
                otEngine.registerAliasName('alias2', 'op3');
                otEngine.registerAliasName('alias1', 'op2');
                otEngine.registerAliasName('alias2', 'alias1');
                expect(otEngine.hasAliasName(makeOp('op1'), 'alias1')).toBeTrue();
                expect(otEngine.hasAliasName(makeOp('op2'), 'alias1')).toBeTrue();
                expect(otEngine.hasAliasName(makeOp('op3'), 'alias1')).toBeFalse();
                expect(otEngine.hasAliasName(makeOp('op1'), 'alias2')).toBeTrue();
                expect(otEngine.hasAliasName(makeOp('op2'), 'alias2')).toBeTrue();
                expect(otEngine.hasAliasName(makeOp('op3'), 'alias2')).toBeTrue();
            });
        });

        describe('method hasRawHandler', function () {
            it('should exist', function () {
                expect(OTEngine).toHaveMethod('hasRawHandler');
            });
        });

        describe('method transformOperation', function () {
            it('should exist', function () {
                expect(OTEngine).toHaveMethod('transformOperation');
            });
        });

        describe('method transformOperations', function () {
            it('should exist', function () {
                expect(OTEngine).toHaveMethod('transformOperations');
            });
        });
    });
});
