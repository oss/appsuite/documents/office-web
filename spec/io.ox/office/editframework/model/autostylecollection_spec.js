/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';

import { OperationError } from '@/io.ox/office/editframework/utils/operationerror';
import { OperationContext } from '@/io.ox/office/editframework/model/operation/context';
import { AttributedModel } from '@/io.ox/office/editframework/model/attributedmodel';
import { AutoStyleModel, AutoStyleCollection } from '@/io.ox/office/editframework/model/autostylecollection';
import { StyleCollection } from '@/io.ox/office/editframework/model/stylecollection';

import { createEditApp } from '~/edit/apphelper';

// tests ======================================================================

describe('module editframework/model/autostylecollection', function () {

    // dummy attribute definitions for three families
    var A_ATTRS = { a1: { def: 'a1' }, a2: { def: 'a2' } };
    var B_ATTRS = { b1: { def: 'b1' }, b2: { def: 'b2' } };
    var C_ATTRS = { c1: { def: 'c1' }, c2: { def: 'c2' } };

    // initialize an edit application with style sheets and auto-styles
    var docModel = null;
    var styleSheets1 = null, autoStyles1 = null;
    var styleSheets2 = null, autoStyles2 = null;
    createEditApp('ooxml').done(function (docApp) {
        docModel = docApp.docModel;
        docModel.defAttrPool.registerAttrFamily('A', A_ATTRS);
        docModel.defAttrPool.registerAttrFamily('B', B_ATTRS);
        docModel.defAttrPool.registerAttrFamily('C', C_ATTRS);
        styleSheets1 = docModel.addStyleCollection(new StyleCollection(docModel, 'A', { families: ['B', 'C'] }));
        styleSheets2 = docModel.addStyleCollection(new StyleCollection(docModel, 'B', { families: ['A', 'C'] }));
        autoStyles1 = docModel.addAutoStyleCollection(new AutoStyleCollection(docModel, 'A', { standardPrefix: 'as', indexedMode: true }));
        autoStyles2 = docModel.addAutoStyleCollection(new AutoStyleCollection(docModel, 'B'));
    });

    // creates an operation context with the passed operation properties
    function op(properties) {
        return new OperationContext(docModel, properties, false);
    }

    // class AutoStyleModel ---------------------------------------------------

    describe('class AutoStyleModel', function () {
        it('should subclass AttributedModel', function () {
            expect(AutoStyleModel).toBeSubClassOf(AttributedModel);
        });
    });

    // class AutoStyleCollection ----------------------------------------------

    describe('class AutoStyleCollection', function () {

        it('should subclass ModelObject', function () {
            expect(AutoStyleCollection).toBeSubClassOf(ModelObject);
        });

        describe('property styleFamily', function () {
            it('should exist', function () {
                expect(autoStyles1.styleFamily).toBe("A");
                expect(autoStyles2.styleFamily).toBe("B");
            });
        });

        describe('property styleCollection', function () {
            it('should exist', function () {
                expect(autoStyles1.styleSheets).toBe(styleSheets1);
                expect(autoStyles2.styleSheets).toBe(styleSheets2);
            });
        });

        describe('method getEffectiveStyleId', function () {
            it('should exist', function () {
                expect(AutoStyleCollection).toHaveMethod('getEffectiveStyleId');
            });
            it('should return the initial default auto-style identifier', function () {
                expect(autoStyles1.getEffectiveStyleId('a1')).toBe("a1");
                expect(autoStyles1.getEffectiveStyleId('')).toBe("");
            });
        });

        describe('method getDefaultStyleId', function () {
            it('should exist', function () {
                expect(AutoStyleCollection).toHaveMethod('getDefaultStyleId');
            });
            it('should return the initial default auto-style identifier', function () {
                expect(autoStyles1.getDefaultStyleId()).toBe("");
                expect(autoStyles2.getDefaultStyleId()).toBe("");
            });
        });

        describe('method isDefaultStyleId', function () {
            it('should exist', function () {
                expect(AutoStyleCollection).toHaveMethod('isDefaultStyleId');
            });
            it('should return true for the initial default auto-style identifier', function () {
                expect(autoStyles1.isDefaultStyleId('')).toBeTrue();
                expect(autoStyles1.isDefaultStyleId('as0')).toBeFalse();
                expect(autoStyles2.isDefaultStyleId('')).toBeTrue();
                expect(autoStyles2.isDefaultStyleId('a0')).toBeFalse();
            });
        });

        describe('method areEqualStyleIds', function () {
            it('should exist', function () {
                expect(AutoStyleCollection).toHaveMethod('areEqualStyleIds');
            });
            it('should return true for the initial default auto-style identifier', function () {
                expect(autoStyles1.areEqualStyleIds('', '')).toBeTrue();
                expect(autoStyles1.areEqualStyleIds('', 'as1')).toBeFalse();
                expect(autoStyles1.areEqualStyleIds('as1', 'as1')).toBeTrue();
                expect(autoStyles1.areEqualStyleIds('as1', 'as2')).toBeFalse();
            });
        });

        describe('method getAutoStyle', function () {
            it('should exist', function () {
                expect(AutoStyleCollection).toHaveMethod('getAutoStyle');
            });
            it('should return null for unknown auto-style', function () {
                expect(autoStyles1.getAutoStyle('__unknown__')).toBeUndefined();
            });
        });

        describe('method getMergedAttributeSet', function () {
            it('should exist', function () {
                expect(AutoStyleCollection).toHaveMethod('getMergedAttributeSet');
            });
            it('should return the attribute defaults for missing auto-styles', function () {
                expect(autoStyles1.getMergedAttributeSet('as1')).toEqual({ styleId: '', A: { a1: 'a1', a2: 'a2' }, B: { b1: 'b1', b2: 'b2' }, C: { c1: 'c1', c2: 'c2' } });
            });
        });

        describe('method getExplicitAttributeSet', function () {
            it('should exist', function () {
                expect(AutoStyleCollection).toHaveMethod('getExplicitAttributeSet');
            });
            it('should return an empty object for missing auto-styles', function () {
                expect(autoStyles1.getExplicitAttributeSet('as1')).toEqual({});
            });
        });

        describe('method getDefaultAttributeSet', function () {
            it('should exist', function () {
                expect(AutoStyleCollection).toHaveMethod('getDefaultAttributeSet');
            });
            it('should return the attribute defaults for missing auto-styles', function () {
                expect(autoStyles1.getDefaultAttributeSet()).toEqual({ styleId: '', A: { a1: 'a1', a2: 'a2' }, B: { b1: 'b1', b2: 'b2' }, C: { c1: 'c1', c2: 'c2' } });
            });
        });

        describe('method applyInsertAutoStyleOperation', function () {
            it('should exist', function () {
                expect(AutoStyleCollection).toHaveMethod('applyInsertAutoStyleOperation');
            });
            describe('in indexed mode', function () {
                it('should insert the default auto-style', function () {
                    expect(autoStyles1.getAutoStyle('as0')).toBeUndefined();
                    expect(autoStyles1.getDefaultStyleId()).toBe("");
                    autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as0', attrs: {}, default: true }));
                    expect(autoStyles1.getAutoStyle('as0')).toBeInstanceOf(AutoStyleModel);
                    expect(autoStyles1.getEffectiveStyleId('')).toBe("as0");
                    expect(autoStyles1.getDefaultStyleId()).toBe("as0");
                    expect(autoStyles1.isDefaultStyleId('as0')).toBeTrue();
                    expect(autoStyles1.isDefaultStyleId('')).toBeTrue();
                    expect(autoStyles1.areEqualStyleIds('', 'as0')).toBeTrue();
                });
                it('should fail to insert another default auto-style', function () {
                    expect(() => autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as1', attrs: {}, default: true }))).toThrow(OperationError);
                    expect(autoStyles1.getAutoStyle('as1')).toBeUndefined();
                    expect(autoStyles1.getDefaultStyleId()).toBe("as0");
                });
                it('should insert another auto-style', function () {
                    expect(autoStyles1.getAutoStyle('as1')).toBeUndefined();
                    autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as1', attrs: {} }));
                    expect(autoStyles1.getAutoStyle('as1')).toBeInstanceOf(AutoStyleModel);
                });
                it('should fail to insert an auto-style with wrong identifier', function () {
                    expect(() => autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as1', attrs: {} }))).toThrow(OperationError);
                    expect(() => autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as3', attrs: {} }))).toThrow(OperationError);
                    expect(autoStyles1.getAutoStyle('as3')).toBeUndefined();
                    expect(() => autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as-1', attrs: {} }))).toThrow(OperationError);
                    expect(autoStyles1.getAutoStyle('as-1')).toBeUndefined();
                    expect(() => autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'a2', attrs: {} }))).toThrow(OperationError);
                    expect(autoStyles1.getAutoStyle('a2')).toBeUndefined();
                    expect(autoStyles1.getAutoStyle('as2')).toBeUndefined();
                    autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as2', attrs: {} }));
                    expect(autoStyles1.getAutoStyle('as2')).toBeInstanceOf(AutoStyleModel);
                });
                it('should insert the attributes for an auto-style', function () {
                    autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as3', attrs: { styleId: 'style', A: { a1: 1 }, B: { b2: 2, wrong: 42 }, WRONG: { a1: 1 } } }));
                    expect(autoStyles1.getAutoStyle('as3')).toBeInstanceOf(AutoStyleModel);
                    expect(autoStyles1.getMergedAttributeSet('as3')).toEqual({ styleId: 'style', A: { a1: 1, a2: 'a2' }, B: { b1: 'b1', b2: 2 }, C: { c1: 'c1', c2: 'c2' } });
                    expect(autoStyles1.getExplicitAttributeSet('as3')).toEqual({ styleId: 'style', A: { a1: 1 }, B: { b2: 2 } });
                });
                it('should shift auto-style indexes in OT mode', function () {
                    var autoStyle1 = autoStyles1.getAutoStyle('as1');
                    var autoStyle2 = autoStyles1.getAutoStyle('as2');
                    var autoStyle3 = autoStyles1.getAutoStyle('as3');
                    autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as2', attrs: {}, otIndexShift: true }));
                    expect(autoStyles1.getAutoStyle('as2')).toBeInstanceOf(AutoStyleModel);
                    expect(autoStyles1.getAutoStyle('as1')).toBe(autoStyle1);
                    expect(autoStyles1.getAutoStyle('as2')).not.toBe(autoStyle2);
                    expect(autoStyles1.getAutoStyle('as3')).toBe(autoStyle2);
                    expect(autoStyles1.getAutoStyle('as4')).toBe(autoStyle3);
                });
                it('should fail to insert invalid auto-style indexes in OT mode', function () {
                    expect(() => autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as0', attrs: {}, otIndexShift: true }))).toThrow(OperationError);
                    expect(() => autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as-1', attrs: {}, otIndexShift: true }))).toThrow(OperationError);
                    expect(() => autoStyles1.applyInsertAutoStyleOperation(op({ styleId: 'as6', attrs: {}, otIndexShift: true }))).toThrow(OperationError);
                });
            });
            describe('in freestyle mode', function () {
                it('should insert an auto-style', function () {
                    expect(autoStyles2.getAutoStyle('as3')).toBeUndefined();
                    autoStyles2.applyInsertAutoStyleOperation(op({ styleId: 'as3', attrs: {} }));
                    expect(autoStyles2.getAutoStyle('as3')).toBeInstanceOf(AutoStyleModel);
                });
                it('should insert the default auto-style', function () {
                    expect(autoStyles2.getAutoStyle('as1')).toBeUndefined();
                    autoStyles2.applyInsertAutoStyleOperation(op({ styleId: 'as1', attrs: {}, default: true }));
                    expect(autoStyles2.getAutoStyle('as1')).toBeInstanceOf(AutoStyleModel);
                    expect(autoStyles2.getEffectiveStyleId('')).toBe("as1");
                    expect(autoStyles2.getDefaultStyleId()).toBe("as1");
                });
                it('should fail to insert another default auto-style', function () {
                    expect(() => autoStyles2.applyInsertAutoStyleOperation(op({ styleId: 'as2', attrs: {}, default: true }))).toThrow(OperationError);
                    expect(autoStyles2.getAutoStyle('as2')).toBeUndefined();
                    expect(autoStyles2.getDefaultStyleId()).toBe("as1");
                });
                it('should fail to insert an auto-style with wrong identifier', function () {
                    expect(() => autoStyles2.applyInsertAutoStyleOperation(op({ styleId: 'as1', attrs: {} }))).toThrow(OperationError);
                });
            });
        });

        describe('method applyChangeAutoStyleOperation', function () {
            it('should exist', function () {
                expect(AutoStyleCollection).toHaveMethod('applyChangeAutoStyleOperation');
            });
        });

        describe('method applyDeleteAutoStyleOperation', function () {
            it('should exist', function () {
                expect(AutoStyleCollection).toHaveMethod('applyDeleteAutoStyleOperation');
            });
        });
    });
});
