/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { EObject } from "@/io.ox/office/tk/objects";
import { AttributeFamilyPool } from "@/io.ox/office/editframework/model/attributefamilypool";
import { AttributePool } from "@/io.ox/office/editframework/model/attributepool";

// constants ==================================================================

const EXP_DEFAULTS_1 = { name1: 42, name2: "value1", name4: 0 };
const EXP_DEFAULTS_2 = { name1: true, name3: [42] };

const EXP_NULL_1 = { name1: null, name4: null };
const EXP_NULL_2 = { name1: null };

// tests ======================================================================

describe("module editframework/model/attributepool", function () {

    const attrPool = new AttributePool();

    // class AttributePool ----------------------------------------------------

    describe("class AttributePool", function () {

        it("should subclass EObject", function () {
            expect(AttributePool).toBeSubClassOf(EObject);
        });

        describe("method hasAttrEntry", function () {
            it("should exist", function () {
                expect(AttributePool).toHaveMethod("hasAttrEntry");
            });
            it("should return false for the empty pool", function () {
                expect(attrPool.hasAttrEntry("family1", "name1")).toBeFalse();
            });
        });

        describe("method registerAttrFamily", function () {
            it("should exist", function () {
                expect(AttributePool).toHaveMethod("registerAttrFamily");
            });
            it("should register new attributes", function () {
                attrPool.registerAttrFamily("family1", {
                    name1: { def: 42 },
                    name2: { def: "value1", scope: "runtime" },
                    name4: { def: 0 }
                });
                attrPool.registerAttrFamily("family2", {
                    name1: { def: true },
                    name3: { def: [42], scope: "runtime" }
                });
                expect(attrPool.hasAttrEntry("family1", "name1")).toBeTrue();
                expect(attrPool.hasAttrEntry("family1", "name2")).toBeTrue();
                expect(attrPool.hasAttrEntry("family1", "name3")).toBeFalse();
                expect(attrPool.hasAttrEntry("family1", "name4")).toBeTrue();
                expect(attrPool.hasAttrEntry("family2", "name1")).toBeTrue();
                expect(attrPool.hasAttrEntry("family2", "name2")).toBeFalse();
                expect(attrPool.hasAttrEntry("family2", "name3")).toBeTrue();
                expect(attrPool.hasAttrEntry("family2", "name4")).toBeFalse();
                expect(attrPool.hasAttrEntry("family3", "name1")).toBeFalse();
            });
        });

        describe("method getFamilyPool", function () {
            it("should exist", function () {
                expect(AttributePool).toHaveMethod("getFamilyPool");
            });
            it("should return family pool", function () {
                expect(attrPool.getFamilyPool("family1")).toBeInstanceOf(AttributeFamilyPool);
                expect(attrPool.getFamilyPool("family2")).toBeInstanceOf(AttributeFamilyPool);
                expect(attrPool.getFamilyPool("family3")).toBeUndefined();
            });
        });

        describe("method getDefaultValue", function () {
            it("should exist", function () {
                expect(AttributePool).toHaveMethod("getDefaultValue");
            });
            it("should return the default value", function () {
                expect(attrPool.getDefaultValue("family1", "name1")).toBe(42);
                expect(attrPool.getDefaultValue("family1", "name2")).toBe("value1");
                expect(attrPool.getDefaultValue("family1", "name3")).toBeUndefined();
                expect(attrPool.getDefaultValue("family1", "name4")).toBe(0);
                expect(attrPool.getDefaultValue("family2", "name1")).toBeTrue();
                expect(attrPool.getDefaultValue("family2", "name2")).toBeUndefined();
                expect(attrPool.getDefaultValue("family2", "name3")).toEqual([42]);
                expect(attrPool.getDefaultValue("family3", "name1")).toBeUndefined();
            });
        });

        describe("method getDefaultValues", function () {
            it("should exist", function () {
                expect(AttributePool).toHaveMethod("getDefaultValues");
            });
            it("should return the default values", function () {
                expect(attrPool.getDefaultValues("family1")).toEqual(EXP_DEFAULTS_1);
                expect(attrPool.getDefaultValues("family2")).toEqual(EXP_DEFAULTS_2);
                expect(attrPool.getDefaultValues("family3")).toBeUndefined();
            });
        });

        describe("method getDefaultValueSet", function () {
            it("should exist", function () {
                expect(AttributePool).toHaveMethod("getDefaultValueSet");
            });
            it("should return the default values for an array of families", function () {
                expect(attrPool.getDefaultValueSet(["family1"])).toEqual({ family1: EXP_DEFAULTS_1 });
                expect(attrPool.getDefaultValueSet(new Set(["family1", "family2"]))).toEqual({ family1: EXP_DEFAULTS_1, family2: EXP_DEFAULTS_2 });
            });
        });

        describe("method buildNullValues", function () {
            it("should exist", function () {
                expect(AttributePool).toHaveMethod("buildNullValues");
            });
            it("should return the null values", function () {
                expect(attrPool.buildNullValues("family1")).toEqual(EXP_NULL_1);
                expect(attrPool.buildNullValues("family2")).toEqual(EXP_NULL_2);
                expect(attrPool.buildNullValues("family3")).toBeUndefined();
            });
        });

        describe("method buildNullValueSet", function () {
            it("should exist", function () {
                expect(AttributePool).toHaveMethod("buildNullValueSet");
            });
            it("should return the null values for an array of families", function () {
                expect(attrPool.buildNullValueSet(["family1"])).toEqual({ family1: EXP_NULL_1 });
                expect(attrPool.buildNullValueSet(new Set(["family1", "family2"]))).toEqual({ family1: EXP_NULL_1, family2: EXP_NULL_2 });
            });
        });

        describe("method yieldAttrBags", function () {
            it("should exist", function () {
                expect(AttributePool).toHaveMethod("yieldAttrBags");
            });
        });

        describe("method extendAttrSet", function () {
            it("should exist", function () {
                expect(AttributePool).toHaveMethod("extendAttrSet");
            });
        });
    });
});
