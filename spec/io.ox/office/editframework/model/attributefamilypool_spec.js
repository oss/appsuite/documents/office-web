/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { to, fun } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/objects";
import { AttributeScope, AttributePoolEntry, AttributeFamilyPool } from "@/io.ox/office/editframework/model/attributefamilypool";

// tests ======================================================================

describe("module editframework/model/attributefamilypool", function () {

    // types ------------------------------------------------------------------

    describe("enum AttributeScope", function () {
        it("should exist", function () {
            expect(AttributeScope).toBeObject();
            expect(AttributeScope).toHaveProperty("STYLE", "style");
            expect(AttributeScope).toHaveProperty("ELEMENT", "element");
            expect(AttributeScope).toHaveProperty("RUNTIME", "runtime");
        });
    });

    // class AttributePoolEntry -----------------------------------------------

    describe("class AttributePoolEntry", function () {

        it("should exist", function () {
            expect(AttributePoolEntry).toBeClass();
        });

        describe("constructor", function () {
            it("should initialize default configuration options", function () {
                const config = { def: 42 };
                const entry = new AttributePoolEntry("name1", config);
                expect(entry.name).toBe("name1");
                expect(entry.config).toBe(config);
                expect(entry.parse).toBeFunction();
                expect(entry.parse(1)).toBe(1);
                expect(entry.parse("a")).toBeUndefined();
                expect(entry.merge).toBeUndefined();
                expect(entry.undo).toEqual([]);
                expect(entry.serializable).toBeTrue();
            });
            it("should initialize custom configuration options", function () {
                const config = { def: "abc", scope: AttributeScope.RUNTIME, parse: to.enum, merge: fun.pluck1st, undo: "name1" };
                const entry = new AttributePoolEntry("name2", config);
                expect(entry.name).toBe("name2");
                expect(entry.config).toBe(config);
                expect(entry.parse).toBe(to.enum);
                expect(entry.merge).toBe(fun.pluck1st);
                expect(entry.undo).toEqual(["name1"]);
                expect(entry.serializable).toBeFalse();
            });
        });
    });

    // class AttributeFamilyPool ----------------------------------------------

    describe("class AttributeFamilyPool", function () {

        it("should subclass DObject", function () {
            expect(AttributeFamilyPool).toBeSubClassOf(DObject);
        });

        const pool = new AttributeFamilyPool();

        describe("method hasEntry", function () {
            it("should exist", function () {
                expect(AttributeFamilyPool).toHaveMethod("hasEntry");
            });
            it("should return false for the empty pool", function () {
                expect(pool.hasEntry("name1")).toBeFalse();
            });
        });

        describe("method registerAttrs", function () {
            it("should exist", function () {
                expect(AttributeFamilyPool).toHaveMethod("registerAttrs");
            });
            it("should register new attributes", function () {
                pool.registerAttrs({
                    name1: { def: 42 },
                    name2: { def: "value1", scope: AttributeScope.RUNTIME },
                    name4: { def: 0 }
                });
                expect(pool.hasEntry("name1")).toBeTrue();
                expect(pool.hasEntry("name2")).toBeTrue();
                expect(pool.hasEntry("name3")).toBeFalse();
                expect(pool.hasEntry("name4")).toBeTrue();
            });
        });

        describe("method getEntry", function () {
            it("should exist", function () {
                expect(AttributeFamilyPool).toHaveMethod("getEntry");
            });
            it("should return pool entries", function () {
                expect(pool.getEntry("name1")).toBeInstanceOf(AttributePoolEntry);
                expect(pool.getEntry("name1").name).toBe("name1");
                expect(pool.getEntry("name2")).toBeInstanceOf(AttributePoolEntry);
                expect(pool.getEntry("name2").name).toBe("name2");
                expect(pool.getEntry("name3")).toBeUndefined();
                expect(pool.getEntry("name4")).toBeInstanceOf(AttributePoolEntry);
                expect(pool.getEntry("name4").name).toBe("name4");
            });
        });

        describe("method yieldEntries", function () {
            it("should exist", function () {
                expect(AttributeFamilyPool).toHaveMethod("yieldEntries");
            });
            it("should return pool entries", function () {
                const iter = pool.yieldEntries();
                expect(iter).toBeIterator();
                expect(Array.from(iter)).toEqual([
                    pool.getEntry("name1"),
                    pool.getEntry("name2"),
                    pool.getEntry("name4")
                ]);
            });
        });

        describe("method getDefault", function () {
            it("should exist", function () {
                expect(AttributeFamilyPool).toHaveMethod("getDefault");
            });
            it("should return the default value", function () {
                expect(pool.getDefault("name1")).toBe(42);
                expect(pool.getDefault("name2")).toBe("value1");
                expect(pool.getDefault("name3")).toBeUndefined();
                expect(pool.getDefault("name4")).toBe(0);
            });
        });

        describe("method getDefaults", function () {
            it("should exist", function () {
                expect(AttributeFamilyPool).toHaveMethod("getDefaults");
            });
            it("should return the default values", function () {
                expect(pool.getDefaults()).toEqual({ name1: 42, name2: "value1", name4: 0 });
            });
        });

        describe("method buildNullValues", function () {
            it("should exist", function () {
                expect(AttributeFamilyPool).toHaveMethod("buildNullValues");
            });
            it("should return the null values", function () {
                expect(pool.buildNullValues()).toEqual({ name1: null, name4: null });
            });
        });

        describe("method changeDefaults", function () {
            it("should exist", function () {
                expect(AttributeFamilyPool).toHaveMethod("changeDefaults");
            });
        });

        describe("method yieldAttrs", function () {
            it("should exist", function () {
                expect(AttributeFamilyPool).toHaveMethod("yieldAttrs");
            });
            it("should return registered attributes", function () {
                const iter = pool.yieldAttrs({ name1: 1, name2: 2, name3: 3, name4: 4 });
                expect(iter).toBeIterator();
                expect(Array.from(iter)).toEqual([
                    { name: "name1", value: 1, def: 42, entry: pool.getEntry("name1") },
                    { name: "name2", value: 2, def: "value1", entry: pool.getEntry("name2") },
                    { name: "name4", value: 4, def: 0, entry: pool.getEntry("name4") }
                ]);
            });
        });

        describe("method extendAttrs", function () {
            it("should exist", function () {
                expect(AttributeFamilyPool).toHaveMethod("extendAttrs");
            });
        });

        describe("method parseDict", function () {
            it("should exist", function () {
                expect(AttributeFamilyPool).toHaveMethod("parseDict");
            });
            it("should parse any dictionaries", function () {
                expect(pool.parseDict({})).toEqual({});
                expect(pool.parseDict({ name1: 1, name2: "a", name3: "x", name4: "y" })).toEqual({ name1: 1 });
            });
        });
    });
});
