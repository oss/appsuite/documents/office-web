/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import { StyleCollection } from '@/io.ox/office/editframework/model/stylecollection';

import { createEditApp } from '~/edit/apphelper';

// tests ======================================================================

describe('module editframework/model/stylecollection', function () {

    // dummy attribute definitions for three families
    var A_ATTRS = { attrA1: { def: 'a1' }, attrA2: { def: 'a2' } };
    var B_ATTRS = { attrA1: { def: 'b1' }, attrA2: { def: 'b2' } };
    var C_ATTRS = { attrA1: { def: 'b1' }, attrA2: { def: 'c2' } };

    // initialize an edit application with style sheets
    var docModel;
    createEditApp('ooxml').done(function (app) {
        docModel = app.getModel();
        docModel.defAttrPool.registerAttrFamily('A', A_ATTRS);
        docModel.defAttrPool.registerAttrFamily('B', B_ATTRS);
        docModel.defAttrPool.registerAttrFamily('C', C_ATTRS);
        // styleSheets = docModel.addStyleCollection(new StyleCollection(docModel, 'A', { families: ['B', 'C'] }));
    });

    // class StyleCollection --------------------------------------------------

    describe('class StyleCollection', function () {

        it('should subclass ModelObject', function () {
            expect(StyleCollection).toBeSubClassOf(ModelObject);
        });

        describe('method applyInsertStyleSheetOperation', function () {
            it('should exist', function () {
                expect(StyleCollection).toHaveMethod('applyInsertStyleSheetOperation');
            });
        });

        describe('method applyDeleteStyleSheetOperation', function () {
            it('should exist', function () {
                expect(StyleCollection).toHaveMethod('applyDeleteStyleSheetOperation');
            });
        });

        describe('method applyChangeStyleSheetOperation', function () {
            it('should exist', function () {
                expect(StyleCollection).toHaveMethod('applyChangeStyleSheetOperation');
            });
        });
    });
});
