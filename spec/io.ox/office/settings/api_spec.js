/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as api_ from '@/io.ox/office/settings/api';
import { UserDictionary } from '@/io.ox/office/settings/model/userdictionary';
import { SpellingSettings } from '@/io.ox/office/settings/model/spellingsettings';
import { TemplateFolders } from '@/io.ox/office/settings/model/templatefolders';

// tests ======================================================================

describe('module settings/api', function () {

    // constants --------------------------------------------------------------

    describe('constant SPELLING_SETTINGS_KEY', function () {
        const { SPELLING_SETTINGS_KEY } = api_;
        it('should exist', function () {
            expect(SPELLING_SETTINGS_KEY).toBeString();
        });
    });

    describe('constant THEME_SETTINGS_KEY', function () {
        const { THEME_SETTINGS_KEY } = api_;
        it('should exist', function () {
            expect(THEME_SETTINGS_KEY).toBeString();
        });
    });

    // singletons -------------------------------------------------------------

    describe('singleton spellingSettings', function () {
        const { spellingSettings } = api_;
        it('should exist', function () {
            expect(spellingSettings).toBeInstanceOf(SpellingSettings);
        });
    });

    describe('singleton userDictionary', function () {
        const { userDictionary } = api_;
        it('should exist', function () {
            expect(userDictionary).toBeInstanceOf(UserDictionary);
        });
    });

    describe('singleton contextTemplateFolders', function () {
        const { contextTemplateFolders } = api_;
        it('should exist', function () {
            expect(contextTemplateFolders).toBeInstanceOf(TemplateFolders);
        });
    });

    describe('singleton userTemplateFolders', function () {
        const { userTemplateFolders } = api_;
        it('should exist', function () {
            expect(userTemplateFolders).toBeInstanceOf(TemplateFolders);
        });
    });

    // public functions -------------------------------------------------------

    describe('function useEditorThemeColors', function () {
        const { useEditorThemeColors } = api_;
        it('should exist', function () {
            expect(useEditorThemeColors).toBeFunction();
        });
    });

    describe('function fetchContextTemplateFolders', function () {
        const { fetchContextTemplateFolders } = api_;
        it('should exist', function () {
            expect(fetchContextTemplateFolders).toBeFunction();
        });
    });
});
