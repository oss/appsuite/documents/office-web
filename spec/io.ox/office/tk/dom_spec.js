/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as device from "@/io.ox/office/tk/dom/device";
import * as domutils from "@/io.ox/office/tk/dom/domutils";
import * as iconutils from "@/io.ox/office/tk/dom/iconutils";
import * as focus from "@/io.ox/office/tk/dom/focus";
import * as keycodes from "@/io.ox/office/tk/dom/keycodes";
import * as labels from "@/io.ox/office/tk/dom/labels";
import * as formutils from "@/io.ox/office/tk/dom/formutils";
import * as colorutils from "@/io.ox/office/tk/dom/colorutils";
import * as font from "@/io.ox/office/tk/dom/font";
import * as rectangle from "@/io.ox/office/tk/dom/rectangle";
import * as symbols from "@/io.ox/office/tk/dom/symbols";
import * as fullscreen from "@/io.ox/office/tk/dom/fullscreen";

import * as dom from "@/io.ox/office/tk/dom";

// tests ======================================================================

describe("module tk/dom", () => {

    // re-exports -------------------------------------------------------------

    it("re-exports should exist", () => {
        expect(dom).toContainProps(device);
        expect(dom).toContainProps(domutils);
        expect(dom).toContainProps(iconutils);
        expect(dom).toContainProps(focus);
        expect(dom).toContainProps(keycodes);
        expect(dom).toContainProps(labels);
        expect(dom).toContainProps(formutils);
        expect(dom).toContainProps(colorutils);
        expect(dom).toContainProps(font);
        expect(dom).toContainProps(rectangle);
        expect(dom).toContainProps(symbols);
        expect(dom).toContainProps(fullscreen);
    });
});
