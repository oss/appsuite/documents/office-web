/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as aborterror from "@/io.ox/office/tk/object/aborterror";
import * as dobject from "@/io.ox/office/tk/object/dobject";
import * as eobject from "@/io.ox/office/tk/object/eobject";
import * as baseobject from "@/io.ox/office/tk/object/baseobject";
import * as decorators from "@/io.ox/office/tk/object/decorators";

import * as objects from "@/io.ox/office/tk/objects";

// tests ======================================================================

describe("module tk/objects", () => {

    // re-exports -------------------------------------------------------------

    it("re-exports should exist", () => {
        expect(objects).toContainProps(aborterror);
        expect(objects).toContainProps(dobject);
        expect(objects).toContainProps(eobject);
        expect(objects).toContainProps(baseobject);
        expect(objects).toContainProps(decorators);
    });
});
