/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as driveutils from "@/io.ox/office/tk/utils/driveutils";

// tests ======================================================================

describe("module tk/utils/driveutils", () => {

    // constants --------------------------------------------------------------

    describe("constant GUARD_EXT", () => {
        it("should exist", () => {
            expect(driveutils.GUARD_EXT).toBe(".pgp");
        });
    });

    // functions --------------------------------------------------------------

    describe("method hasGuardExt", () => {
        it("should exist", () => {
            expect(driveutils.hasGuardExt).toBeFunction();
        });
        it("should return whether a file name has the PGP extension", () => {
            expect(driveutils.hasGuardExt("")).toBeFalse();
            expect(driveutils.hasGuardExt("txt")).toBeFalse();
            expect(driveutils.hasGuardExt("pgp")).toBeFalse();
            expect(driveutils.hasGuardExt(".txt")).toBeFalse();
            expect(driveutils.hasGuardExt(".pgp")).toBeFalse();
            expect(driveutils.hasGuardExt("example.")).toBeFalse();
            expect(driveutils.hasGuardExt("example.txt")).toBeFalse();
            expect(driveutils.hasGuardExt("example.pgp")).toBeTrue();
            expect(driveutils.hasGuardExt("example.txt.pgp")).toBeTrue();
            expect(driveutils.hasGuardExt("example.pgp.txt")).toBeFalse();
        });
        it("should ignore case", () => {
            expect(driveutils.hasGuardExt("example.PGP")).toBeTrue();
            expect(driveutils.hasGuardExt("example.TXT.PGP")).toBeTrue();
        });
    });

    describe("method removeGuardExt", () => {
        it("should exist", () => {
            expect(driveutils.removeGuardExt).toBeFunction();
        });
        it("should return whether a file name has the PGP extension", () => {
            expect(driveutils.removeGuardExt("")).toBe("");
            expect(driveutils.removeGuardExt("txt")).toBe("txt");
            expect(driveutils.removeGuardExt("pgp")).toBe("pgp");
            expect(driveutils.removeGuardExt(".txt")).toBe(".txt");
            expect(driveutils.removeGuardExt(".pgp")).toBe(".pgp");
            expect(driveutils.removeGuardExt("example.")).toBe("example.");
            expect(driveutils.removeGuardExt("example.txt")).toBe("example.txt");
            expect(driveutils.removeGuardExt("example.pgp")).toBe("example");
            expect(driveutils.removeGuardExt("example.txt.pgp")).toBe("example.txt");
            expect(driveutils.removeGuardExt("example.pgp.txt")).toBe("example.pgp.txt");
        });
        it("should ignore case", () => {
            expect(driveutils.removeGuardExt("example.PGP")).toBe("example");
            expect(driveutils.removeGuardExt("example.TXT.PGP")).toBe("example.TXT");
        });
    });

    describe("method getFileExtension", () => {
        it("should exist", () => {
            expect(driveutils.getFileExtension).toBeFunction();
        });
        it("should return the file extension", () => {
            expect(driveutils.getFileExtension("")).toBe("");
            expect(driveutils.getFileExtension("txt")).toBe("");
            expect(driveutils.getFileExtension("pgp")).toBe("");
            expect(driveutils.getFileExtension(".txt")).toBe("txt");
            expect(driveutils.getFileExtension(".pgp")).toBe("pgp");
            expect(driveutils.getFileExtension("example.")).toBe("");
            expect(driveutils.getFileExtension("example.txt")).toBe("txt");
            expect(driveutils.getFileExtension("example.pgp")).toBe("pgp");
            expect(driveutils.getFileExtension("example.txt.pgp")).toBe("txt");
            expect(driveutils.getFileExtension("example.pgp.txt")).toBe("txt");
        });
        it("should ignore case", () => {
            expect(driveutils.getFileExtension("example.TXT")).toBe("txt");
            expect(driveutils.getFileExtension("example.TXT.PGP")).toBe("txt");
        });
    });

    describe("method replaceFileExtension", () => {
        it("should exist", () => {
            expect(driveutils.replaceFileExtension).toBeFunction();
        });
        it("should replace the file extension", () => {
            expect(driveutils.replaceFileExtension("example.txt", "dat")).toBe("example.dat");
            expect(driveutils.replaceFileExtension("example.txt.pgp", "dat")).toBe("example.dat.pgp");
            expect(driveutils.replaceFileExtension("example.TXT.PGP", "dat")).toBe("example.dat.pgp");
        });
    });

    describe("method getFileBaseName", () => {
        it("should exist", () => {
            expect(driveutils.getFileBaseName).toBeFunction();
        });
        it("should return the file base name", () => {
            expect(driveutils.getFileBaseName("")).toBe("");
            expect(driveutils.getFileBaseName("txt")).toBe("txt");
            expect(driveutils.getFileBaseName("pgp")).toBe("pgp");
            expect(driveutils.getFileBaseName(".txt")).toBe("");
            expect(driveutils.getFileBaseName(".pgp")).toBe("");
            expect(driveutils.getFileBaseName("example.")).toBe("example.");
            expect(driveutils.getFileBaseName("example.txt")).toBe("example");
            expect(driveutils.getFileBaseName("example.pgp")).toBe("example");
            expect(driveutils.getFileBaseName("example.txt.pgp")).toBe("example");
            expect(driveutils.getFileBaseName("example.pgp.txt")).toBe("example.pgp");
        });
        it("should ignore case", () => {
            expect(driveutils.getFileBaseName("example.TXT")).toBe("example");
            expect(driveutils.getFileBaseName("example.TXT.PGP")).toBe("example");
        });
    });
});
