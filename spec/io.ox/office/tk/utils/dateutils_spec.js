/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as dateutils from "@/io.ox/office/tk/utils/dateutils";

// tests ======================================================================

describe("module tk/utils/dateutils", () => {

    // constants --------------------------------------------------------------

    describe("constant SEC_PER_DAY", () => {
        const { SEC_PER_DAY } = dateutils;
        it("should exist", () => {
            expect(SEC_PER_DAY).toBe(24 * 60 * 60);
        });
    });

    describe("constant SEC_PER_WEEK", () => {
        const { SEC_PER_WEEK } = dateutils;
        it("should exist", () => {
            expect(SEC_PER_WEEK).toBe(7 * 24 * 60 * 60);
        });
    });

    describe("constant MSEC_PER_DAY", () => {
        const { MSEC_PER_DAY } = dateutils;
        it("should exist", () => {
            expect(MSEC_PER_DAY).toBe(24 * 60 * 60 * 1000);
        });
    });

    describe("constant MSEC_PER_WEEK", () => {
        const { MSEC_PER_WEEK } = dateutils;
        it("should exist", () => {
            expect(MSEC_PER_WEEK).toBe(7 * 24 * 60 * 60 * 1000);
        });
    });

    // public functions -------------------------------------------------------

    describe("function isLeapYear", () => {
        const { isLeapYear } = dateutils;
        it("should exist", () => {
            expect(isLeapYear).toBeFunction();
        });
        it("should return the correct result", () => {
            expect(isLeapYear(1500)).toBeFalse();
            expect(isLeapYear(1600)).toBeTrue();
            expect(isLeapYear(1700)).toBeFalse();
            expect(isLeapYear(1800)).toBeFalse();
            expect(isLeapYear(1900)).toBeFalse();
            expect(isLeapYear(1995)).toBeFalse();
            expect(isLeapYear(1996)).toBeTrue();
            expect(isLeapYear(1997)).toBeFalse();
            expect(isLeapYear(1998)).toBeFalse();
            expect(isLeapYear(1999)).toBeFalse();
            expect(isLeapYear(2000)).toBeTrue();
            expect(isLeapYear(2001)).toBeFalse();
            expect(isLeapYear(2100)).toBeFalse();
            expect(isLeapYear(2200)).toBeFalse();
            expect(isLeapYear(2300)).toBeFalse();
            expect(isLeapYear(2400)).toBeTrue();
            expect(isLeapYear(2500)).toBeFalse();
        });
    });

    describe("function getDaysInYear", () => {
        const { getDaysInYear } = dateutils;
        it("should exist", () => {
            expect(getDaysInYear).toBeFunction();
        });
        it("should return the correct result", () => {
            expect(getDaysInYear(1500)).toBe(365);
            expect(getDaysInYear(1600)).toBe(366);
            expect(getDaysInYear(1700)).toBe(365);
            expect(getDaysInYear(1800)).toBe(365);
            expect(getDaysInYear(1900)).toBe(365);
            expect(getDaysInYear(1995)).toBe(365);
            expect(getDaysInYear(1996)).toBe(366);
            expect(getDaysInYear(1997)).toBe(365);
            expect(getDaysInYear(1998)).toBe(365);
            expect(getDaysInYear(1999)).toBe(365);
            expect(getDaysInYear(2000)).toBe(366);
            expect(getDaysInYear(2001)).toBe(365);
            expect(getDaysInYear(2100)).toBe(365);
            expect(getDaysInYear(2200)).toBe(365);
            expect(getDaysInYear(2300)).toBe(365);
            expect(getDaysInYear(2400)).toBe(366);
            expect(getDaysInYear(2500)).toBe(365);
        });
    });

    describe("function getDaysInFebruary", () => {
        const { getDaysInFebruary } = dateutils;
        it("should exist", () => {
            expect(getDaysInFebruary).toBeFunction();
        });
        it("should return the correct result", () => {
            expect(getDaysInFebruary(1500)).toBe(28);
            expect(getDaysInFebruary(1600)).toBe(29);
            expect(getDaysInFebruary(1700)).toBe(28);
            expect(getDaysInFebruary(1800)).toBe(28);
            expect(getDaysInFebruary(1900)).toBe(28);
            expect(getDaysInFebruary(1995)).toBe(28);
            expect(getDaysInFebruary(1996)).toBe(29);
            expect(getDaysInFebruary(1997)).toBe(28);
            expect(getDaysInFebruary(1998)).toBe(28);
            expect(getDaysInFebruary(1999)).toBe(28);
            expect(getDaysInFebruary(2000)).toBe(29);
            expect(getDaysInFebruary(2001)).toBe(28);
            expect(getDaysInFebruary(2100)).toBe(28);
            expect(getDaysInFebruary(2200)).toBe(28);
            expect(getDaysInFebruary(2300)).toBe(28);
            expect(getDaysInFebruary(2400)).toBe(29);
            expect(getDaysInFebruary(2500)).toBe(28);
        });
    });

    describe("function getDaysInMonth", () => {
        const { getDaysInMonth } = dateutils;
        it("should exist", () => {
            expect(getDaysInMonth).toBeFunction();
        });
        it("should return the correct result", () => {
            expect(getDaysInMonth(1999, 0)).toBe(31);
            expect(getDaysInMonth(1999, 1)).toBe(28);
            expect(getDaysInMonth(1999, 2)).toBe(31);
            expect(getDaysInMonth(1999, 3)).toBe(30);
            expect(getDaysInMonth(1999, 4)).toBe(31);
            expect(getDaysInMonth(1999, 5)).toBe(30);
            expect(getDaysInMonth(1999, 6)).toBe(31);
            expect(getDaysInMonth(1999, 7)).toBe(31);
            expect(getDaysInMonth(1999, 8)).toBe(30);
            expect(getDaysInMonth(1999, 9)).toBe(31);
            expect(getDaysInMonth(1999, 10)).toBe(30);
            expect(getDaysInMonth(1999, 11)).toBe(31);
            expect(getDaysInMonth(2000, 1)).toBe(29);
        });
    });

    describe("function expandYear", () => {
        const { expandYear } = dateutils;
        it("should exist", () => {
            expect(expandYear).toBeFunction();
        });
        it("should return full years as passed", () => {
            expect(expandYear(100)).toBe(100);
            expect(expandYear(199)).toBe(199);
            expect(expandYear(1990)).toBe(1990);
            expect(expandYear(2000)).toBe(2000);
            expect(expandYear(2010)).toBe(2010);
            expect(expandYear(2020)).toBe(2020);
            expect(expandYear(2030)).toBe(2030);
            expect(expandYear(2040)).toBe(2040);
            expect(expandYear(2050)).toBe(2050);
            expect(expandYear(2060)).toBe(2060);
            expect(expandYear(2070)).toBe(2070);
            expect(expandYear(2080)).toBe(2080);
            expect(expandYear(2090)).toBe(2090);
            expect(expandYear(2100)).toBe(2100);
        });
        it("should expand short years", () => {
            const year = new Date().getFullYear();
            for (let y = year - 79; y <= year + 20; y += 1) {
                expect(expandYear(y % 100)).toBe(y);
            }
        });
        it("should expand short years with custom threshold", () => {
            const year = new Date().getFullYear();
            // threshold=50
            for (let y1 = year - 49; y1 <= year + 50; y1 += 1) {
                expect(expandYear(y1 % 100, 50)).toBe(y1);
            }
            // threshold=0 (all years in the past)
            for (let y2 = year - 99; y2 <= year; y2 += 1) {
                expect(expandYear(y2 % 100, 0)).toBe(y2);
            }
            // threshold=99 (all years in the future)
            for (let y3 = year; y3 <= year + 99; y3 += 1) {
                expect(expandYear(y3 % 100, 99)).toBe(y3);
            }
        });
    });

    const { splitUTCDate } = dateutils;
    describe("function splitUTCDate", () => {
        it("should exist", () => {
            expect(splitUTCDate).toBeFunction();
        });
        it("should return the UTC date components", () => {
            expect(splitUTCDate(new Date(Date.UTC(2015, 11, 31)))).toEqual({ Y: 2015, M: 11, D: 31 });
            expect(splitUTCDate(new Date(Date.UTC(2015, 11, 31, 23, 59, 59, 999)))).toEqual({ Y: 2015, M: 11, D: 31 });
        });
    });

    const { splitLocalDate } = dateutils;
    describe("function splitLocalDate", () => {
        it("should exist", () => {
            expect(splitLocalDate).toBeFunction();
        });
        it("should return the local date components", () => {
            expect(splitLocalDate(new Date(2015, 11, 31))).toEqual({ Y: 2015, M: 11, D: 31 });
            expect(splitLocalDate(new Date(2015, 11, 31, 23, 59, 59, 999))).toEqual({ Y: 2015, M: 11, D: 31 });
        });
    });

    const { splitUTCDateTime } = dateutils;
    describe("function splitUTCDateTime", () => {
        it("should exist", () => {
            expect(splitUTCDateTime).toBeFunction();
        });
        it("should return the UTC date/time components", () => {
            expect(splitUTCDateTime(new Date(Date.UTC(2015, 11, 31)))).toEqual({ Y: 2015, M: 11, D: 31, h: 0, m: 0, s: 0, ms: 0 });
            expect(splitUTCDateTime(new Date(Date.UTC(2015, 11, 31, 23, 59, 59, 999)))).toEqual({ Y: 2015, M: 11, D: 31, h: 23, m: 59, s: 59, ms: 999 });
        });
    });

    const { splitLocalDateTime } = dateutils;
    describe("function splitLocalDateTime", () => {
        it("should exist", () => {
            expect(splitLocalDateTime).toBeFunction();
        });
        it("should return the local date/time components", () => {
            expect(splitLocalDateTime(new Date(2015, 11, 31))).toEqual({ Y: 2015, M: 11, D: 31, h: 0, m: 0, s: 0, ms: 0 });
            expect(splitLocalDateTime(new Date(2015, 11, 31, 23, 59, 59, 999))).toEqual({ Y: 2015, M: 11, D: 31, h: 23, m: 59, s: 59, ms: 999 });
        });
    });

    const { makeUTCDate } = dateutils;
    describe("function makeUTCDate", () => {
        it("should exist", () => {
            expect(makeUTCDate).toBeFunction();
        });
        it("should return the UTC date", () => {
            expect(makeUTCDate({ Y: 2015, M: 11, D: 31 }).getTime()).toBe(new Date(Date.UTC(2015, 11, 31)).getTime());
        });
    });

    const { makeLocalDate } = dateutils;
    describe("function makeLocalDate", () => {
        it("should exist", () => {
            expect(makeLocalDate).toBeFunction();
        });
        it("should return the UTC date", () => {
            expect(makeLocalDate({ Y: 2015, M: 11, D: 31 }).getTime()).toBe(new Date(2015, 11, 31).getTime());
        });
    });

    const { makeUTCDateTime } = dateutils;
    describe("function makeUTCDateTime", () => {
        it("should exist", () => {
            expect(makeUTCDateTime).toBeFunction();
        });
        it("should return the UTC date/time", () => {
            expect(makeUTCDateTime({ Y: 2015, M: 11, D: 31, h: 0, m: 0, s: 0, ms: 0 }).getTime()).toBe(new Date(Date.UTC(2015, 11, 31)).getTime());
            expect(makeUTCDateTime({ Y: 2015, M: 11, D: 31, h: 23, m: 59, s: 59, ms: 999 }).getTime()).toBe(new Date(Date.UTC(2015, 11, 31, 23, 59, 59, 999)).getTime());
        });
    });

    const { makeLocalDateTime } = dateutils;
    describe("function makeLocalDateTime", () => {
        it("should exist", () => {
            expect(makeLocalDateTime).toBeFunction();
        });
        it("should return the UTC date/time", () => {
            expect(makeLocalDateTime({ Y: 2015, M: 11, D: 31, h: 0, m: 0, s: 0, ms: 0 }).getTime()).toBe(new Date(2015, 11, 31).getTime());
            expect(makeLocalDateTime({ Y: 2015, M: 11, D: 31, h: 23, m: 59, s: 59, ms: 999 }).getTime()).toBe(new Date(2015, 11, 31, 23, 59, 59, 999).getTime());
        });
    });

    describe("function getLocalTodayAsUTC", () => {
        const { getLocalTodayAsUTC } = dateutils;
        it("should exist", () => {
            expect(getLocalTodayAsUTC).toBeFunction();
        });
        it("should return the UTC date for local today", () => {
            const today1 = makeUTCDate(splitLocalDate(new Date()));
            const today2 = getLocalTodayAsUTC();
            const today3 = makeUTCDate(splitLocalDate(new Date()));
            expect(today2).toBeDate();
            expect((today1 <= today2) && (today2 <= today3)).toBeTrue();
        });
    });

    describe("function getUTCTodayAsLocal", () => {
        const { getUTCTodayAsLocal } = dateutils;
        it("should exist", () => {
            expect(getUTCTodayAsLocal).toBeFunction();
        });
        it("should return the local date for UTC today", () => {
            const today1 = makeLocalDate(splitUTCDate(new Date()));
            const today2 = getUTCTodayAsLocal();
            const today3 = makeLocalDate(splitUTCDate(new Date()));
            expect(today2).toBeDate();
            expect((today1 <= today2) && (today2 <= today3)).toBeTrue();
        });
    });

    describe("function getLocalNowAsUTC", () => {
        const { getLocalNowAsUTC } = dateutils;
        it("should exist", () => {
            expect(getLocalNowAsUTC).toBeFunction();
        });
        it("should return the UTC date for local now", () => {
            const now1 = makeUTCDateTime(splitLocalDateTime(new Date()));
            const now2 = getLocalNowAsUTC();
            const now3 = makeUTCDateTime(splitLocalDateTime(new Date()));
            expect(now2).toBeDate();
            expect((now1 <= now2) && (now2 <= now3)).toBeTrue();
        });
    });

    describe("function getUTCNowAsLocal", () => {
        const { getUTCNowAsLocal } = dateutils;
        it("should exist", () => {
            expect(getUTCNowAsLocal).toBeFunction();
        });
        it("should return the local date for UTC now", () => {
            const now1 = makeLocalDateTime(splitUTCDateTime(new Date()));
            const now2 = getUTCNowAsLocal();
            const now3 = makeLocalDateTime(splitUTCDateTime(new Date()));
            expect(now2).toBeDate();
            expect((now1 <= now2) && (now2 <= now3)).toBeTrue();
        });
    });
});
