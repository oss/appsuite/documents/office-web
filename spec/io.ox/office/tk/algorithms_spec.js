/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as is from "@/io.ox/office/tk/algorithms/is";
import * as to from "@/io.ox/office/tk/algorithms/to";
import * as math from "@/io.ox/office/tk/algorithms/math";
import * as coord from "@/io.ox/office/tk/algorithms/coord";
import * as str from "@/io.ox/office/tk/algorithms/string";
import * as unicode from "@/io.ox/office/tk/algorithms/unicode";
import * as re from "@/io.ox/office/tk/algorithms/regexp";
import * as fmt from "@/io.ox/office/tk/algorithms/format";
import * as fun from "@/io.ox/office/tk/algorithms/function";
import * as hash from "@/io.ox/office/tk/algorithms/hash";
import * as itr from "@/io.ox/office/tk/algorithms/iterable";
import * as ary from "@/io.ox/office/tk/algorithms/array";
import * as map from "@/io.ox/office/tk/algorithms/map";
import * as set from "@/io.ox/office/tk/algorithms/set";
import * as dict from "@/io.ox/office/tk/algorithms/dict";
import * as pick from "@/io.ox/office/tk/algorithms/pick";
import * as json from "@/io.ox/office/tk/algorithms/json";
import * as uuid from "@/io.ox/office/tk/algorithms/uuid";
import * as proto from "@/io.ox/office/tk/algorithms/prototype";
import * as jpromise from "@/io.ox/office/tk/algorithms/jpromise";
import * as debug from "@/io.ox/office/tk/algorithms/debug";

import * as algorithms from "@/io.ox/office/tk/algorithms";

// tests ======================================================================

describe("module tk/algorithms", function () {

    // re-exports -------------------------------------------------------------

    it("re-exports should exist", function () {
        expect(algorithms).toHaveProperty("is", is);
        expect(algorithms).toHaveProperty("to", to);
        expect(algorithms).toHaveProperty("math", math);
        expect(algorithms).toHaveProperty("coord", coord);
        expect(algorithms).toHaveProperty("str", str);
        expect(algorithms).toHaveProperty("unicode", unicode);
        expect(algorithms).toHaveProperty("re", re);
        expect(algorithms).toHaveProperty("fmt", fmt);
        expect(algorithms).toHaveProperty("fun", fun);
        expect(algorithms).toHaveProperty("hash", hash);
        expect(algorithms).toHaveProperty("itr", itr);
        expect(algorithms).toHaveProperty("ary", ary);
        expect(algorithms).toHaveProperty("map", map);
        expect(algorithms).toHaveProperty("set", set);
        expect(algorithms).toHaveProperty("dict", dict);
        expect(algorithms).toHaveProperty("pick", pick);
        expect(algorithms).toHaveProperty("json", json);
        expect(algorithms).toHaveProperty("uuid", uuid);
        expect(algorithms).toHaveProperty("proto", proto);
        expect(algorithms).toHaveProperty("jpromise", jpromise);
        expect(algorithms).toHaveProperty("debug", debug);
    });
});
