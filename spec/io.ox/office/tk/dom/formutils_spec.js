/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as formutils from "@/io.ox/office/tk/dom/formutils";

// tests ======================================================================

describe("module tk/dom/formutils", () => {

    // public functions -------------------------------------------------------

    describe("function enableElement", () => {
        const { enableElement } = formutils;
        it("should exist", () => {
            expect(enableElement).toBeFunction();
        });
        it("should enable and disable element", () => {
            const elem = document.createElement("button");
            enableElement(elem, true);
            expect(elem.disabled).toBeFalse();
            expect(elem).not.toHaveAttribute("aria-disabled");
            enableElement(elem, false);
            expect(elem.disabled).toBeTrue();
            expect(elem).toHaveAttribute("aria-disabled", "true");
            enableElement(elem, true);
            expect(elem.disabled).toBeFalse();
            expect(elem).not.toHaveAttribute("aria-disabled");
        });
    });

    describe("function serializeControlValue", () => {
        const { serializeControlValue } = formutils;
        it("should exist", () => {
            expect(serializeControlValue).toBeFunction();
        });
    });

    describe("function createFieldSet", () => {
        const { createFieldSet } = formutils;
        it("should exist", () => {
            expect(createFieldSet).toBeFunction();
        });
        it("should create a fieldset element", () => {
            expect(createFieldSet()).toBeInstanceOf(HTMLFieldSetElement);
        });
        it("should add CSS classes", () => {
            const elem = createFieldSet("a b");
            expect(elem).toHaveClass("a b", { exact: true });
            expect(elem.childNodes).toHaveLength(0);
        });
        it("should add id, classes, and styles", () => {
            const elem = createFieldSet({ id: "id", classes: "a b", style: { display: "none" }, indent: true });
            expect(elem.id).toBe("id");
            expect(elem).toHaveClass("a b indent", { exact: true });
            expect(elem).toHaveStyle({ display: "none" });
        });
        it("should add legend", () => {
            const elem = createFieldSet({ legend: "legend" });
            expect(elem.firstChild).toBeInstanceOf(HTMLLegendElement);
            expect(elem.firstChild).toHaveTextContent("legend");
        });
    });

    describe("function createLabel", () => {
        const { createLabel } = formutils;
        it("should exist", () => {
            expect(createLabel).toBeFunction();
        });
    });

    describe("function createCaret", () => {
        const { createCaret } = formutils;
        it("should exist", () => {
            expect(createCaret).toBeFunction();
        });
    });

    describe("function createButton", () => {
        const { createButton } = formutils;
        it("should exist", () => {
            expect(createButton).toBeFunction();
        });
    });

    describe("function createDeleteButton", () => {
        const { createDeleteButton } = formutils;
        it("should exist", () => {
            expect(createDeleteButton).toBeFunction();
        });
    });

    describe("function createInput", () => {
        const { createInput } = formutils;
        it("should exist", () => {
            expect(createInput).toBeFunction();
        });
        it("should create an imput element", () => {
            const elem = createInput();
            expect(elem).toBeInstanceOf(HTMLInputElement);
            expect(elem.type).toBe("text");
            expect(elem.spellcheck).toBeFalse();
            expect(elem.autocomplete).toBe("off");
        });
        it("should add id, classes, and styles", () => {
            const elem = createInput({ id: "id", classes: "a b", style: { display: "none" }, type: "number", disabled: true, placeholder: "placeholder", maxLength: 42 });
            expect(elem.childNodes).toHaveLength(0);
            expect(elem.id).toBe("id");
            expect(elem).toHaveClass("a b", { exact: true });
            expect(elem).toHaveStyle({ display: "none" });
            expect(elem.type).toBe("number");
            expect(elem.disabled).toBeTrue();
            expect(elem.placeholder).toBe("placeholder");
            expect(elem.maxLength).toBe(42);
            expect(elem.spellcheck).toBeFalse();
            expect(elem.autocomplete).toBe("off");
        });
    });

    describe("function createTextArea", () => {
        const { createTextArea } = formutils;
        it("should exist", () => {
            expect(createTextArea).toBeFunction();
        });
        it("should create a textarea element", () => {
            const elem = createTextArea();
            expect(elem).toBeInstanceOf(HTMLTextAreaElement);
            expect(elem.spellcheck).toBeFalse();
            expect(elem.autocomplete).toBe("off");
        });
        it("should add id, classes, and styles", () => {
            const elem = createTextArea({ id: "id", classes: "a b", style: { display: "none" }, disabled: true, placeholder: "placeholder", maxLength: 42 });
            expect(elem.childNodes).toHaveLength(0);
            expect(elem.id).toBe("id");
            expect(elem).toHaveClass("a b", { exact: true });
            expect(elem).toHaveStyle({ display: "none" });
            expect(elem.disabled).toBeTrue();
            expect(elem.placeholder).toBe("placeholder");
            expect(elem.maxLength).toBe(42);
            expect(elem.spellcheck).toBeFalse();
            expect(elem.autocomplete).toBe("off");
        });
    });

    describe("function isTextInputElement", () => {
        const { isTextInputElement } = formutils;
        it("should exist", () => {
            expect(isTextInputElement).toBeFunction();
        });
        it("should return true for input elements", () => {
            expect(isTextInputElement(document.createElement("input"))).toBeTrue();
            expect(isTextInputElement(document.createElement("textarea"))).toBeTrue();
        });
        it("should return false for other values", () => {
            expect(isTextInputElement(document.createElement("div"))).toBeFalse();
            expect(isTextInputElement(document.createElement("span"))).toBeFalse();
            expect(isTextInputElement(document)).toBeFalse();
            expect(isTextInputElement(null)).toBeFalse();
        });
    });

    describe("function getInputSelection", () => {
        const { getInputSelection } = formutils;
        it("should exist", () => {
            expect(getInputSelection).toBeFunction();
        });
        it("should return input selection", () => {
            const elem = document.createElement("input");
            elem.value = "abc";
            elem.setSelectionRange(1, 2);
            expect(getInputSelection(elem)).toEqual({ start: 1, end: 2 });
            elem.setSelectionRange(0, 0);
            expect(getInputSelection(elem)).toEqual({ start: 0, end: 0 });
        });
    });

    describe("function isCursorSelection", () => {
        const { isCursorSelection } = formutils;
        it("should exist", () => {
            expect(isCursorSelection).toBeFunction();
        });
        it("should return cursor state", () => {
            const elem = document.createElement("input");
            elem.value = "abc";
            elem.setSelectionRange(1, 2);
            expect(isCursorSelection(elem)).toBeFalse();
            elem.setSelectionRange(0, 0);
            expect(isCursorSelection(elem)).toBeTrue();
        });
    });

    describe("function setInputSelection", () => {
        const { setInputSelection } = formutils;
        it("should exist", () => {
            expect(setInputSelection).toBeFunction();
        });
        it("should change selection", () => {
            const elem = document.createElement("input");
            elem.value = "abc";
            setInputSelection(elem, 1, 2);
            expect(elem.selectionStart).toBe(1);
            expect(elem.selectionEnd).toBe(2);
            setInputSelection(elem, 0);
            expect(elem.selectionStart).toBe(0);
            expect(elem.selectionEnd).toBe(0);
            setInputSelection(elem, { start: 1, end: 2 });
            expect(elem.selectionStart).toBe(1);
            expect(elem.selectionEnd).toBe(2);
            setInputSelection(elem);
            expect(elem.selectionStart).toBe(0);
            expect(elem.selectionEnd).toBe(3);
        });
    });

    describe("function replaceInputSelection", () => {
        const { replaceInputSelection } = formutils;
        it("should exist", () => {
            expect(replaceInputSelection).toBeFunction();
        });
        it("should replace selection", () => {
            const elem = document.createElement("input");
            elem.value = "abc";
            elem.selectionStart = 1;
            elem.selectionEnd = 2;
            replaceInputSelection(elem, "xy");
            expect(elem.value).toBe("axyc");
            expect(elem.selectionStart).toBe(3);
            expect(elem.selectionEnd).toBe(3);
            replaceInputSelection(elem, "z", 0);
            expect(elem.value).toBe("axyzc");
            expect(elem.selectionStart).toBe(3);
            expect(elem.selectionEnd).toBe(3);
            replaceInputSelection(elem, "123", -1);
            expect(elem.value).toBe("axy123zc");
            expect(elem.selectionStart).toBe(5);
            expect(elem.selectionEnd).toBe(5);
            elem.selectionStart = 1;
            elem.selectionEnd = 4;
            replaceInputSelection(elem);
            expect(elem.value).toBe("a23zc");
            expect(elem.selectionStart).toBe(1);
            expect(elem.selectionEnd).toBe(1);
        });
    });
});
