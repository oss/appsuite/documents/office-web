/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as keycodes from "@/io.ox/office/tk/dom/keycodes";

// tests ======================================================================

describe("module tk/dom/keycodes", () => {

    const { KeyCode } = keycodes;

    function checkKeyCodes(keyCodes) {
        keyCodes.split(" ").forEach(function (key) {
            expect(KeyCode[key]).toBeInteger();
        });
    }

    // constants --------------------------------------------------------------

    describe("enum KeyCode", () => {
        it("should exist", () => {
            expect(KeyCode).toBeObject();
        });
    });

    describe("constants for control keys", () => {
        it("should exist", () => {
            checkKeyCodes("BACKSPACE TAB ENTER BREAK ESCAPE SPACE PAGE_UP PAGE_DOWN END HOME LEFT_ARROW UP_ARROW RIGHT_ARROW DOWN_ARROW PRINT INSERT DELETE");
        });
    });

    describe("constants for modifier keys", () => {
        it("should exist", () => {
            checkKeyCodes("SHIFT CONTROL ALT CAPS_LOCK LEFT_WINDOWS LEFT_COMMAND RIGHT_WINDOWS SELECT RIGHT_COMMAND NUM_LOCK SCROLL_LOCK");
        });
    });

    describe("constants for alpha-numeric keys", () => {
        it("should exist", () => {
            checkKeyCodes("0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z");
        });
    });

    describe("constants for numeric pad keys", () => {
        it("should exist", () => {
            checkKeyCodes("NUM_0 NUM_1 NUM_2 NUM_3 NUM_4 NUM_5 NUM_6 NUM_7 NUM_8 NUM_9 NUM_MULTIPLY NUM_PLUS NUM_MINUS NUM_POINT NUM_DIVIDE");
        });
    });

    describe("constants for function keys", () => {
        it("should exist", () => {
            checkKeyCodes("F1 F2 F3 F4 F5 F6 F7 F8 F9 F10 F11 F12");
        });
    });

    describe("constants for special Mozilla key codes", () => {
        it("should exist", () => {
            checkKeyCodes("MOZ_SEMICOLON MOZ_OPEN_ANGLE MOZ_EQUAL_SIGN MOZ_COMMAND");
        });
    });

    describe("constants for IME key codes", () => {
        it("should exist", () => {
            checkKeyCodes("IME_INPUT");
        });
    });

    // public functions -------------------------------------------------------

    describe("function hasModifierKeys", () => {
        const { hasModifierKeys } = keycodes;
        it("should exist", () => {
            expect(hasModifierKeys).toBeFunction();
        });
        it("should return the correct state", () => {
            expect(hasModifierKeys({})).toBeFalse();
            expect(hasModifierKeys({ shiftKey: false })).toBeFalse();
            expect(hasModifierKeys({ altKey: false })).toBeFalse();
            expect(hasModifierKeys({ ctrlKey: false })).toBeFalse();
            expect(hasModifierKeys({ metaKey: false })).toBeFalse();
            expect(hasModifierKeys({ shiftKey: true })).toBeTrue();
            expect(hasModifierKeys({ altKey: true })).toBeTrue();
            expect(hasModifierKeys({ ctrlKey: true })).toBeTrue();
            expect(hasModifierKeys({ metaKey: true })).toBeTrue();
            expect(hasModifierKeys({ shiftKey: false, altKey: false, ctrlKey: false, metaKey: false })).toBeFalse();
            expect(hasModifierKeys({ shiftKey: true, altKey: false, ctrlKey: false, metaKey: false })).toBeTrue();
            expect(hasModifierKeys({ shiftKey: false, altKey: true, ctrlKey: false, metaKey: false })).toBeTrue();
            expect(hasModifierKeys({ shiftKey: false, altKey: false, ctrlKey: true, metaKey: false })).toBeTrue();
            expect(hasModifierKeys({ shiftKey: false, altKey: false, ctrlKey: false, metaKey: true })).toBeTrue();
            expect(hasModifierKeys({ shiftKey: true, altKey: true, ctrlKey: true, metaKey: true })).toBeTrue();
        });
    });

    describe("function matchModifierKeys", () => {
        const { matchModifierKeys } = keycodes;
        it("should exist", () => {
            expect(matchModifierKeys).toBeFunction();
        });
        it("should detect SHIFT key", () => {
            expect(matchModifierKeys({})).toBeTrue();
            expect(matchModifierKeys({ shiftKey: false })).toBeTrue();
            expect(matchModifierKeys({ shiftKey: true })).toBeFalse();
            expect(matchModifierKeys({},                  { shift: false })).toBeTrue();
            expect(matchModifierKeys({ shiftKey: false }, { shift: false })).toBeTrue();
            expect(matchModifierKeys({ shiftKey: true },  { shift: false })).toBeFalse();
            expect(matchModifierKeys({},                  { shift: true })).toBeFalse();
            expect(matchModifierKeys({ shiftKey: false }, { shift: true })).toBeFalse();
            expect(matchModifierKeys({ shiftKey: true },  { shift: true })).toBeTrue();
            expect(matchModifierKeys({},                  { shift: null })).toBeTrue();
            expect(matchModifierKeys({ shiftKey: false }, { shift: null })).toBeTrue();
            expect(matchModifierKeys({ shiftKey: true },  { shift: null })).toBeTrue();
        });
        it("should detect ALT key", () => {
            expect(matchModifierKeys({})).toBeTrue();
            expect(matchModifierKeys({ altKey: false })).toBeTrue();
            expect(matchModifierKeys({ altKey: true })).toBeFalse();
            expect(matchModifierKeys({},                { alt: false })).toBeTrue();
            expect(matchModifierKeys({ altKey: false }, { alt: false })).toBeTrue();
            expect(matchModifierKeys({ altKey: true },  { alt: false })).toBeFalse();
            expect(matchModifierKeys({},                { alt: true })).toBeFalse();
            expect(matchModifierKeys({ altKey: false }, { alt: true })).toBeFalse();
            expect(matchModifierKeys({ altKey: true },  { alt: true })).toBeTrue();
            expect(matchModifierKeys({},                { alt: null })).toBeTrue();
            expect(matchModifierKeys({ altKey: false }, { alt: null })).toBeTrue();
            expect(matchModifierKeys({ altKey: true },  { alt: null })).toBeTrue();
        });
        it("should detect CTRL key", () => {
            expect(matchModifierKeys({})).toBeTrue();
            expect(matchModifierKeys({ ctrlKey: false })).toBeTrue();
            expect(matchModifierKeys({ ctrlKey: true })).toBeFalse();
            expect(matchModifierKeys({},                 { ctrl: false })).toBeTrue();
            expect(matchModifierKeys({ ctrlKey: false }, { ctrl: false })).toBeTrue();
            expect(matchModifierKeys({ ctrlKey: true },  { ctrl: false })).toBeFalse();
            expect(matchModifierKeys({},                 { ctrl: true })).toBeFalse();
            expect(matchModifierKeys({ ctrlKey: false }, { ctrl: true })).toBeFalse();
            expect(matchModifierKeys({ ctrlKey: true },  { ctrl: true })).toBeTrue();
            expect(matchModifierKeys({},                 { ctrl: null })).toBeTrue();
            expect(matchModifierKeys({ ctrlKey: false }, { ctrl: null })).toBeTrue();
            expect(matchModifierKeys({ ctrlKey: true },  { ctrl: null })).toBeTrue();
        });
        it("should detect META key", () => {
            expect(matchModifierKeys({})).toBeTrue();
            expect(matchModifierKeys({ metaKey: false })).toBeTrue();
            expect(matchModifierKeys({ metaKey: true })).toBeFalse();
            expect(matchModifierKeys({},                 { meta: false })).toBeTrue();
            expect(matchModifierKeys({ metaKey: false }, { meta: false })).toBeTrue();
            expect(matchModifierKeys({ metaKey: true },  { meta: false })).toBeFalse();
            expect(matchModifierKeys({},                 { meta: true })).toBeFalse();
            expect(matchModifierKeys({ metaKey: false }, { meta: true })).toBeFalse();
            expect(matchModifierKeys({ metaKey: true },  { meta: true })).toBeTrue();
            expect(matchModifierKeys({},                 { meta: null })).toBeTrue();
            expect(matchModifierKeys({ metaKey: false }, { meta: null })).toBeTrue();
            expect(matchModifierKeys({ metaKey: true },  { meta: null })).toBeTrue();
        });
        it("should detect ALT or META key", () => {
            expect(matchModifierKeys({}, { altOrMeta: true })).toBeFalse();
            expect(matchModifierKeys({ shiftKey: true }, { altOrMeta: true })).toBeFalse();
            expect(matchModifierKeys({ altKey: true }, { altOrMeta: true })).toBeTrue();
            expect(matchModifierKeys({ ctrlKey: true }, { altOrMeta: true })).toBeFalse();
            expect(matchModifierKeys({ metaKey: true }, { altOrMeta: true })).toBeTrue();
            expect(matchModifierKeys({ altKey: true, metaKey: true }, { altOrMeta: true })).toBeFalse();
        });
        it("should detect CTRL or META key", () => {
            expect(matchModifierKeys({}, { ctrlOrMeta: true })).toBeFalse();
            expect(matchModifierKeys({ shiftKey: true }, { ctrlOrMeta: true })).toBeFalse();
            expect(matchModifierKeys({ altKey: true }, { ctrlOrMeta: true })).toBeFalse();
            expect(matchModifierKeys({ ctrlKey: true }, { ctrlOrMeta: true })).toBeTrue();
            expect(matchModifierKeys({ metaKey: true }, { ctrlOrMeta: true })).toBeTrue();
            expect(matchModifierKeys({ ctrlKey: true, metaKey: true }, { ctrlOrMeta: true })).toBeFalse();
        });
    });

    describe("function hasKeyCode", () => {
        const { hasKeyCode } = keycodes;
        it("should exist", () => {
            expect(hasKeyCode).toBeFunction();
        });
        it("should match key code as number", () => {
            expect(hasKeyCode({ keyCode: KeyCode.TAB }, 9)).toBeTrue();
            expect(hasKeyCode({ keyCode: KeyCode["9"] }, 57)).toBeTrue();
            expect(hasKeyCode({ keyCode: KeyCode.TAB }, 57)).toBeFalse();
        });
        it("should match key code as string", () => {
            expect(hasKeyCode({ keyCode: KeyCode.TAB }, "TAB")).toBeTrue();
            expect(hasKeyCode({ keyCode: KeyCode["9"] }, "9")).toBeTrue();
            expect(hasKeyCode({ keyCode: KeyCode.TAB }, "9")).toBeFalse();
        });
    });

    describe("function isEscapeKey", () => {
        const { isEscapeKey } = keycodes;
        it("should exist", () => {
            expect(isEscapeKey).toBeFunction();
        });
        it("should match ESCAPE key", () => {
            expect(isEscapeKey({ keyCode: KeyCode.ESCAPE })).toBeTrue();
        });
        it("should not match other keys", () => {
            expect(isEscapeKey({ keyCode: KeyCode.ESCAPE - 1 })).toBeFalse();
            expect(isEscapeKey({ keyCode: KeyCode.ESCAPE + 1 })).toBeFalse();
        });
    });

    describe("function isFunctionKey", () => {
        const { isFunctionKey } = keycodes;
        it("should exist", () => {
            expect(isFunctionKey).toBeFunction();
        });
        it("should match function keys", () => {
            for (let keyCode = KeyCode.F1; keyCode <= KeyCode.F12; keyCode += 1) {
                expect(isFunctionKey({ keyCode })).toBeTrue();
            }
        });
        it("should not match other keys", () => {
            expect(isFunctionKey({ keyCode: KeyCode.F1 - 1 })).toBeFalse();
            expect(isFunctionKey({ keyCode: KeyCode.F12 + 1 })).toBeFalse();
        });
    });

    describe("function isIMEInputKey", () => {
        const { isIMEInputKey } = keycodes;
        it("should exist", () => {
            expect(isIMEInputKey).toBeFunction();
        });
        it("should match IME key", () => {
            expect(isIMEInputKey({ keyCode: KeyCode.IME_INPUT })).toBeTrue();
        });
        it("should not match other keys", () => {
            expect(isIMEInputKey({ keyCode: KeyCode.IME_INPUT - 1 })).toBeFalse();
            expect(isIMEInputKey({ keyCode: KeyCode.IME_INPUT + 1 })).toBeFalse();
        });
    });

    describe("function matchKeyCode", () => {
        const { matchKeyCode } = keycodes;
        it("should exist", () => {
            expect(matchKeyCode).toBeFunction();
        });
        it("should match key code as number", () => {
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, 9)).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode["9"] }, 57)).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, 57)).toBeFalse();
        });
        it("should match key code as string", () => {
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, "TAB")).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode["9"] }, "9")).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, "9")).toBeFalse();
        });
        it("should match modifier keys", () => {
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, "TAB", { shift: true })).toBeFalse();
            expect(matchKeyCode({ keyCode: KeyCode.TAB, shiftKey: true }, "TAB", { shift: true })).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, "TAB", { shift: null })).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB, shiftKey: true }, "TAB", { shift: null })).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, "TAB", { alt: true })).toBeFalse();
            expect(matchKeyCode({ keyCode: KeyCode.TAB, altKey: true }, "TAB", { alt: true })).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, "TAB", { alt: null })).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB, altKey: true }, "TAB", { alt: null })).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, "TAB", { ctrl: true })).toBeFalse();
            expect(matchKeyCode({ keyCode: KeyCode.TAB, ctrlKey: true }, "TAB", { ctrl: true })).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, "TAB", { ctrl: null })).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB, ctrlKey: true }, "TAB", { ctrl: null })).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, "TAB", { meta: true })).toBeFalse();
            expect(matchKeyCode({ keyCode: KeyCode.TAB, metaKey: true }, "TAB", { meta: true })).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB }, "TAB", { meta: null })).toBeTrue();
            expect(matchKeyCode({ keyCode: KeyCode.TAB, metaKey: true }, "TAB", { meta: null })).toBeTrue();
        });
    });

    describe("function isCopyKeyEvent", () => {
        const { isCopyKeyEvent } = keycodes;
        it("should exist", () => {
            expect(isCopyKeyEvent).toBeFunction();
        });
        it("should match copy events", () => {
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.C, ctrlKey: true })).toBeTrue();
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.C, metaKey: true })).toBeTrue();
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT, ctrlKey: true })).toBeTrue();
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT, metaKey: true })).toBeTrue();
        });
        it("should not match other events", () => {
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.C })).toBeFalse();
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.X })).toBeFalse();
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.V })).toBeFalse();
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT })).toBeFalse();
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.DELETE })).toBeFalse();
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.X, ctrlKey: true })).toBeFalse();
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.DELETE, shiftKey: true })).toBeFalse();
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.V, ctrlKey: true })).toBeFalse();
            expect(isCopyKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT, shiftKey: true })).toBeFalse();
        });
    });

    describe("function isCutKeyEvent", () => {
        const { isCutKeyEvent } = keycodes;
        it("should exist", () => {
            expect(isCutKeyEvent).toBeFunction();
        });
        it("should match cut events", () => {
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.X, ctrlKey: true })).toBeTrue();
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.X, metaKey: true })).toBeTrue();
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.DELETE, shiftKey: true })).toBeTrue();
        });
        it("should not match other events", () => {
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.C })).toBeFalse();
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.X })).toBeFalse();
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.V })).toBeFalse();
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.DELETE })).toBeFalse();
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT })).toBeFalse();
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.C, ctrlKey: true })).toBeFalse();
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT, ctrlKey: true })).toBeFalse();
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.V, ctrlKey: true })).toBeFalse();
            expect(isCutKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT, shiftKey: true })).toBeFalse();
        });
    });

    describe("function isPasteKeyEvent", () => {
        const { isPasteKeyEvent } = keycodes;
        it("should exist", () => {
            expect(isPasteKeyEvent).toBeFunction();
        });
        it("should match paste events", () => {
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.V, ctrlKey: true })).toBeTrue();
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.V, metaKey: true })).toBeTrue();
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT, shiftKey: true })).toBeTrue();
        });
        it("should not match other events", () => {
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.C })).toBeFalse();
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.X })).toBeFalse();
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.V })).toBeFalse();
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.DELETE })).toBeFalse();
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT })).toBeFalse();
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.C, ctrlKey: true })).toBeFalse();
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT, ctrlKey: true })).toBeFalse();
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.X, ctrlKey: true })).toBeFalse();
            expect(isPasteKeyEvent({ type: "keydown", keyCode: KeyCode.DELETE, shiftKey: true })).toBeFalse();
        });
    });

    describe("function isClipboardKeyEvent", () => {
        const { isClipboardKeyEvent } = keycodes;
        it("should exist", () => {
            expect(isClipboardKeyEvent).toBeFunction();
        });
        it("should match copy events", () => {
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.V, ctrlKey: true })).toBeTrue();
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.V, metaKey: true })).toBeTrue();
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT, shiftKey: true })).toBeTrue();
        });
        it("should match cut events", () => {
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.X, ctrlKey: true })).toBeTrue();
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.X, metaKey: true })).toBeTrue();
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.DELETE, shiftKey: true })).toBeTrue();
        });
        it("should match paste events", () => {
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.V, ctrlKey: true })).toBeTrue();
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.V, metaKey: true })).toBeTrue();
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT, shiftKey: true })).toBeTrue();
        });
        it("should not match other events", () => {
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.C })).toBeFalse();
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.X })).toBeFalse();
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.V })).toBeFalse();
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.DELETE })).toBeFalse();
            expect(isClipboardKeyEvent({ type: "keydown", keyCode: KeyCode.INSERT })).toBeFalse();
        });
    });
});
