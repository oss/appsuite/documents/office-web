/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import * as font from "@/io.ox/office/tk/dom/font";

// tests ======================================================================

describe("module tk/dom/font", () => {

    // constants --------------------------------------------------------------

    const { AUTO_BIDI_SUPPORT } = font;
    describe("constant AUTO_BIDI_SUPPORT", () => {
        it("should exist", () => {
            expect(AUTO_BIDI_SUPPORT).toBeBoolean();
        });
    });

    // public functions -------------------------------------------------------

    describe("function isRightToLeft", () => {
        const { isRightToLeft } = font;
        const LTR_TEXT = "abc";
        const RTL_TEXT = "\u05d0\u05d1\u05d2";
        it("should exist", () => {
            expect(isRightToLeft).toBeFunction();
        });
        it("should return false for western text", () => {
            expect(isRightToLeft(LTR_TEXT)).toBeFalse();
            expect(isRightToLeft("  " + LTR_TEXT + "  ")).toBeFalse();
        });
        // ES6-LATER: this test does not work with JSDOM (needs DOM layout support)
        it.skip("should return true for complex text", () => {
            expect(isRightToLeft(RTL_TEXT)).toBeTrue();
            expect(isRightToLeft("  " + RTL_TEXT + "  ")).toBeTrue();
        });
        // ES6-LATER: this test does not work with JSDOM (needs DOM layout support)
        it.skip("should return correct direction for mixed text", () => {
            expect(isRightToLeft(LTR_TEXT + " " + RTL_TEXT)).toBeFalse();
            expect(isRightToLeft(RTL_TEXT + " " + LTR_TEXT)).toBeTrue();
        });
        it("should return false for undetermined text", () => {
            expect(isRightToLeft("")).toBeFalse();
            expect(isRightToLeft("  ")).toBeFalse();
            expect(isRightToLeft(".")).toBeFalse();
            expect(isRightToLeft("123")).toBeFalse();
        });
    });

    // class Font -------------------------------------------------------------

    describe("class Font", () => {

        const { Font } = font;
        it("should exist", () => {
            expect(Font).toBeClass();
        });

        describe("constructor", () => {
            it("should take all parameters", () => {
                const fontDesc = new Font("times", 10, false, true);
                expect(fontDesc).toHaveProperty("family", "times");
                expect(fontDesc).toHaveProperty("size", 10);
                expect(fontDesc).toHaveProperty("bold", false);
                expect(fontDesc).toHaveProperty("italic", true);
            });
            it("should convert font name to lower-case", () => {
                const fontDesc = new Font("TiMeS", 10.25, false, true);
                expect(fontDesc).toHaveProperty("family", "times");
            });
            it("should round font size", () => {
                const fontDesc = new Font("times", 10.25, false, true);
                expect(fontDesc).toHaveProperty("size", 10.3);
            });
            it("should accept missing flags", () => {
                const fontDesc = new Font("times", 10);
                expect(fontDesc).toHaveProperty("bold", false);
                expect(fontDesc).toHaveProperty("italic", false);
            });
        });

        describe("property key", () => {
            it("should return a string", () => {
                const fontDesc = new Font("times", 10, false, true);
                expect(fontDesc.key).toBeString();
            });
            it("should return different keys for different font names", () => {
                const fontDesc1 = new Font("times1", 10, false, true),
                    fontDesc2 = new Font("times2", 10, false, true);
                expect(fontDesc1.key).not.toBe(fontDesc2.key);
            });
            it("should return different keys for different font sizes", () => {
                const fontDesc1 = new Font("times", 10, false, true),
                    fontDesc2 = new Font("times", 11, false, true);
                expect(fontDesc1.key).not.toBe(fontDesc2.key);
            });
            it("should return different keys for different bold flags", () => {
                const fontDesc1 = new Font("times", 10, false, true),
                    fontDesc2 = new Font("times", 10, true, true);
                expect(fontDesc1.key).not.toBe(fontDesc2.key);
            });
            it("should return different keys for different italic flags", () => {
                const fontDesc1 = new Font("times", 10, false, true),
                    fontDesc2 = new Font("times", 10, false, false);
                expect(fontDesc1.key).not.toBe(fontDesc2.key);
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(Font).toHaveMethod("clone");
            });
            it("should return a clone", () => {
                const fontDesc1 = new Font("times", 10, false, true);
                const fontDesc2 = fontDesc1.clone();
                expect(fontDesc2).not.toBe(fontDesc1);
                expect(fontDesc2).toEqual(fontDesc1);
            });
        });

        describe("method getCanvasFont", () => {
            it("should exist", () => {
                expect(Font).toHaveMethod("getCanvasFont");
            });
            it("should return the canvas font style", () => {
                expect(new Font("times", 10, false, false).getCanvasFont()).toBe("10pt times");
                expect(new Font("times,serif", 10, false, false).getCanvasFont()).toBe("10pt times,serif");
                expect(new Font("times", 10, true, false).getCanvasFont()).toBe("bold 10pt times");
                expect(new Font("times", 10, false, true).getCanvasFont()).toBe("italic 10pt times");
                expect(new Font("times", 10, true, true).getCanvasFont()).toBe("bold italic 10pt times");
                expect(new Font("Times New Roman", 10, false, false).getCanvasFont()).toBe("10pt times new roman");
            });
        });

        describe("method getNormalLineHeight", () => {
            it("should exist", () => {
                expect(Font).toHaveMethod("getNormalLineHeight");
            });
        });

        describe("method getBaseLineOffset", () => {
            it("should exist", () => {
                expect(Font).toHaveMethod("getBaseLineOffset");
            });
        });

        describe("method getTextWidth", () => {
            it("should exist", () => {
                expect(Font).toHaveMethod("getTextWidth");
            });
            it("should return the text width", () => {
                const fontDesc = new Font("times", 10, false, false);
                expect(fontDesc.getTextWidth("")).toBe(0);
                const a = fontDesc.getTextWidth("a");
                expect(a).toBeGreaterThan(0);
                const ab = fontDesc.getTextWidth("ab");
                expect(ab).toBeGreaterThan(a);
            });
        });

        describe("method getDigitMeasures", () => {
            it("should exist", () => {
                expect(Font).toHaveMethod("getDigitMeasures");
            });
            it("should return the digit measures", () => {
                const fontDesc = new Font("times", 10, false, false);
                const digitData = fontDesc.getDigitMeasures();
                expect(digitData).toBeObject();
                expect(digitData.widths).toBeArrayOfSize(10);
                expect(digitData.widths.map(_.identity)).toEqual(_.map("0123456789", c => fontDesc.getTextWidth(c)));
                const sorted = _.sortBy(digitData.widths);
                expect(digitData).toHaveProperty("minWidth", sorted[0]);
                expect(digitData).toHaveProperty("maxWidth", sorted[9]);
                expect(digitData).toHaveProperty("minDigit");
                expect(digitData.minDigit).toBeInInterval(0, 9);
                expect(digitData).toHaveProperty("maxDigit");
                expect(digitData.maxDigit).toBeInInterval(0, 9);
            });
        });

        describe("method getTextLines", () => {
            it("should exist", () => {
                expect(Font).toHaveMethod("getTextLines");
            });
        });

        describe("method getCustomFontMetrics", () => {
            it("should exist", () => {
                expect(Font).toHaveMethod("getCustomFontMetrics");
            });
        });
    });
});
