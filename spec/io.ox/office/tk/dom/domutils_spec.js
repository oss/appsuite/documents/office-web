/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import * as domutils from "@/io.ox/office/tk/dom/domutils";

// constants ==================================================================

const div = document.createElement("div");
const $div = $(div);

// tests ======================================================================

describe("module tk/dom/domutils", () => {

    // constants --------------------------------------------------------------

    describe("constant MAX_DBLCLICK_DURATION", () => {
        const { MAX_DBLCLICK_DURATION } = domutils;
        it("should exist", () => {
            expect(MAX_DBLCLICK_DURATION).toBeNumber();
        });
    });

    describe("constant MAX_NODE_SIZE", () => {
        const { MAX_NODE_SIZE } = domutils;
        it("should exist", () => {
            expect(MAX_NODE_SIZE).toBeNumber();
        });
    });

    // public functions -------------------------------------------------------

    describe("function escapeHTML", () => {
        const { escapeHTML } = domutils;
        it("should exist", () => {
            expect(escapeHTML).toBeFunction();
        });
        it("should escape all special characters", () => {
            expect(escapeHTML("abc&def")).toBe("abc&amp;def");
            expect(escapeHTML("abc<def")).toBe("abc&lt;def");
            expect(escapeHTML("abc>def")).toBe("abc&gt;def");
            expect(escapeHTML("abc/def")).toBe("abc&#47;def");
            expect(escapeHTML('abc"def')).toBe("abc&quot;def");
            expect(escapeHTML("abc'def")).toBe("abc&#39;def");
            expect(escapeHTML("abc\xa0def")).toBe("abc&nbsp;def");
            expect(escapeHTML("a&&b<<c")).toBe("a&amp;&amp;b&lt;&lt;c");
        });
    });

    describe("function cleanXML", () => {
        const { cleanXML } = domutils;
        it("should exist", () => {
            expect(cleanXML).toBeFunction();
        });
        it("should remove all invalid chars from the string", () => {
            let text = "";
            function addCharRange(...args) {
                for (let i = 0, l = args.length; i < l; i += 2) {
                    for (let code = args[i], end = args[i + 1]; code <= end; code += 1) {
                        text += String.fromCodePoint(code);
                    }
                }
            }
            addCharRange(0x0, 0x8, 0xB, 0xC, 0xE, 0x1F, 0x7F, 0x84, 0x86, 0x9F, 0xFDD0, 0xFDEF);
            for (let plane = 1; plane <= 16; plane += 1) {
                const code = plane * 0x10000 + 0xFFFE;
                addCharRange(code, code + 1);
            }
            expect(cleanXML("abc" + text + "def")).toBe("abcdef");
        });
    });

    describe("function isNodeList", () => {
        const { isNodeList } = domutils;
        it("should exist", () => {
            expect(isNodeList).toBeFunction();
        });
        it("should return whether the value is a DOM node list", () => {
            expect(isNodeList(1)).toBeFalse();
            expect(isNodeList("1")).toBeFalse();
            expect(isNodeList(true)).toBeFalse();
            expect(isNodeList(null)).toBeFalse();
            expect(isNodeList()).toBeFalse();
            expect(isNodeList({})).toBeFalse();
            expect(isNodeList([])).toBeFalse();
            expect(isNodeList(new Number(1))).toBeFalse();
            expect(isNodeList(new String("1"))).toBeFalse();
            expect(isNodeList(new Boolean(true))).toBeFalse();
            expect(isNodeList(new Object())).toBeFalse();
            expect(isNodeList(new Array())).toBeFalse();
            expect(isNodeList(Boolean)).toBeFalse();
            expect(isNodeList($())).toBeFalse();
            expect(isNodeList($div)).toBeFalse();
            expect(isNodeList(div)).toBeFalse();
            expect(isNodeList([div])).toBeFalse();
            expect(isNodeList(document.childNodes)).toBeTrue();
        });
    });

    describe("function isJQuery", () => {
        const { isJQuery } = domutils;
        it("should exist", () => {
            expect(isJQuery).toBeFunction();
        });
        it("should return whether the value is a jQuery collection", () => {
            expect(isJQuery(1)).toBeFalse();
            expect(isJQuery("1")).toBeFalse();
            expect(isJQuery(true)).toBeFalse();
            expect(isJQuery(null)).toBeFalse();
            expect(isJQuery()).toBeFalse();
            expect(isJQuery({})).toBeFalse();
            expect(isJQuery([])).toBeFalse();
            expect(isJQuery(new Number(1))).toBeFalse();
            expect(isJQuery(new String("1"))).toBeFalse();
            expect(isJQuery(new Boolean(true))).toBeFalse();
            expect(isJQuery(new Object())).toBeFalse();
            expect(isJQuery(new Array())).toBeFalse();
            expect(isJQuery(Boolean)).toBeFalse();
            expect(isJQuery($())).toBeTrue();
            expect(isJQuery($div)).toBeTrue();
            expect(isJQuery(div)).toBeFalse();
            expect(isJQuery([div])).toBeFalse();
            expect(isJQuery(document.childNodes)).toBeFalse();
        });
    });

    describe("function isNodeSource", () => {
        const { isNodeSource } = domutils;
        it("should exist", () => {
            expect(isNodeSource).toBeFunction();
        });
        it("should return true for DOM nodes", () => {
            expect(isNodeSource(div)).toBeTrue();
            expect(isNodeSource(document)).toBeTrue();
            expect(isNodeSource(document.body)).toBeTrue();
            expect(isNodeSource(document.createTextNode(""))).toBeTrue();
            expect(isNodeSource(document.createDocumentFragment())).toBeTrue();
        });
        it("should return true for arrays", () => {
            expect(isNodeSource([])).toBeTrue();
            expect(isNodeSource(new Array())).toBeTrue();
            expect(isNodeSource([div])).toBeTrue();
        });
        it("should return true for DOM node lists", () => {
            expect(isNodeSource(document.querySelectorAll("body"))).toBeTrue();
            expect(isNodeSource(document.childNodes)).toBeTrue();
            expect(isNodeSource(document.createDocumentFragment().childNodes)).toBeTrue();
        });
        it("should return true for JQuery collections", () => {
            expect(isNodeSource($(document))).toBeTrue();
            expect(isNodeSource($("body"))).toBeTrue();
            expect(isNodeSource($())).toBeTrue();
            expect(isNodeSource($div)).toBeTrue();
        });
        it("should return true for nullish values", () => {
            expect(isNodeSource(null)).toBeTrue();
            expect(isNodeSource(undefined)).toBeTrue();
        });
        it("should return false for other values", () => {
            expect(isNodeSource(1)).toBeFalse();
            expect(isNodeSource("1")).toBeFalse();
            expect(isNodeSource(true)).toBeFalse();
            expect(isNodeSource({})).toBeFalse();
            expect(isNodeSource(new Number(1))).toBeFalse();
            expect(isNodeSource(new String("1"))).toBeFalse();
            expect(isNodeSource(new Boolean(true))).toBeFalse();
            expect(isNodeSource(new Object())).toBeFalse();
            expect(isNodeSource(Boolean)).toBeFalse();
        });
    });

    describe("function toNode", () => {
        const { toNode } = domutils;
        it("should exist", () => {
            expect(toNode).toBeFunction();
        });
        it("should convert source to DOM node", () => {
            expect(toNode(div)).toBe(div);
            expect(toNode($div)).toBe(div);
            expect(toNode($(div, "<div>"))).toBe(div);
            expect(toNode($())).toBeNull();
            expect(toNode(document)).toBe(document);
            expect(toNode($(document))).toBe(document);
            expect(toNode(null)).toBeNull();
            expect(toNode(undefined)).toBeNull();
        });
    });

    describe("function toJQuery", () => {
        const { toJQuery } = domutils;
        it("should exist", () => {
            expect(toJQuery).toBeFunction();
        });
    });

    describe("function detachChildren", () => {
        const { detachChildren } = domutils;
        it("should exist", () => {
            expect(detachChildren).toBeFunction();
        });
        it("should detach all child nodes", () => {
            const container = document.createElement("div");
            for (let i = 0; i < 3; i += 1) {
                container.appendChild(document.createElement("div"));
            }
            expect(container.childNodes).toHaveLength(3);
            detachChildren(container);
            expect(container.childNodes).toHaveLength(0);
        });
    });

    describe("function insertChildren", () => {
        const { insertChildren } = domutils;
        it("should exist", () => {
            expect(insertChildren).toBeFunction();
        });
        it("should insert nodes", () => {
            const container = document.createElement("div");
            const children = _.times(3, () => container.appendChild(document.createElement("div")));
            expect(container.childNodes).toHaveLength(3);
            const newNode1 = document.createElement("div");
            insertChildren(container, 0, newNode1);
            expect(container.childNodes).toHaveLength(4);
            expect(container.childNodes[0]).toBe(newNode1);
            expect(container.childNodes[1]).toBe(children[0]);
            const newNode2 = document.createElement("div");
            const newNode3 = document.createElement("div");
            insertChildren(container, 2, newNode2, newNode3);
            expect(container.childNodes).toHaveLength(6);
            expect(container.childNodes[1]).toBe(children[0]);
            expect(container.childNodes[2]).toBe(newNode2);
            expect(container.childNodes[3]).toBe(newNode3);
            expect(container.childNodes[4]).toBe(children[1]);
            const newNode4 = document.createElement("div");
            insertChildren(container, 6, newNode4);
            expect(container.childNodes).toHaveLength(7);
            expect(container.childNodes[5]).toBe(children[2]);
            expect(container.childNodes[6]).toBe(newNode4);
            const newNode5 = document.createElement("div");
            insertChildren(container, -1, newNode5);
            expect(container.childNodes).toHaveLength(8);
            expect(container.childNodes[7]).toBe(newNode5);
            const newNode6 = document.createElement("div");
            insertChildren(container, -3, newNode6);
            expect(container.childNodes).toHaveLength(9);
            expect(container.childNodes[6]).toBe(newNode6);
            const newNode7 = document.createElement("div");
            insertChildren(container, -10, newNode7);
            expect(container.childNodes).toHaveLength(10);
            expect(container.childNodes[0]).toBe(newNode7);
        });
        it("should throw for invalid index", () => {
            const container = document.createElement("div");
            _.times(3, () => container.append(document.createElement("div")));
            expect(container.childNodes).toHaveLength(3);
            const newNode = document.createElement("div");
            expect(() => insertChildren(container, 4, newNode)).toThrow("invalid child index");
            expect(container.childNodes).toHaveLength(3);
            expect(() => insertChildren(container, -5, newNode)).toThrow("invalid child index");
            expect(container.childNodes).toHaveLength(3);
        });
    });

    describe("function replaceChildren", () => {
        const { replaceChildren } = domutils;
        it("should exist", () => {
            expect(replaceChildren).toBeFunction();
        });
        it("should replace nodes", () => {
            const container = document.createElement("div");
            container.appendChild(document.createElement("span"));
            container.appendChild(document.createElement("span"));
            container.appendChild(document.createElement("span"));
            expect(container.childNodes).toHaveLength(3);
            const newNode1 = document.createElement("div");
            const newNode2 = document.createElement("div");
            replaceChildren(container, newNode1, newNode2);
            expect(container.childNodes).toHaveLength(2);
            expect(container.childNodes[0]).toBe(newNode1);
            expect(container.childNodes[1]).toBe(newNode2);
        });
    });

    describe('function setElementClasses', function () {
        const { setElementClasses } = domutils;
        it('should exist', function () {
            expect(setElementClasses).toBeFunction();
        });
        it('should update id and classes', function () {
            const node = $('<div id="id" class="a b">')[0];
            setElementClasses(node, { id: 'new', classes: 'c d' });
            expect(node.id).toBe('new');
            expect(node).toHaveClass('a b c d', { exact: true });
        });
        it('should not update classes', function () {
            const node = $('<div id="id" class="a b">')[0];
            setElementClasses(node, { id: 'new' });
            expect(node.id).toBe('new');
            expect(node).toHaveClass('a b', { exact: true });
        });
        it('should not update id', function () {
            const node = $('<div id="id" class="a b">')[0];
            setElementClasses(node, { classes: 'c d' });
            expect(node.id).toBe('id');
            expect(node).toHaveClass('a b c d', { exact: true });
        });
        it('should clear id and classes', function () {
            const node = $('<div id="id" class="a b">')[0];
            setElementClasses(node, { classes: 'c d', clear: true });
            expect(node.id).toBe('');
            expect(node).toHaveClass('c d', { exact: true });
        });
        it('should accept array of classes', function () {
            const node = $('<div id="id" class="a b">')[0];
            setElementClasses(node, { classes: ['b', null, undefined, 'c'] });
            expect(node).toHaveClass('a b c', { exact: true });
        });
        it('should accept class toggle map', function () {
            const node = $('<div id="id" class="a b c d">')[0];
            setElementClasses(node, { classes: { a: false, b: null, c: undefined, d: true, e: true } });
            expect(node).toHaveClass('d e', { exact: true });
        });
    });

    describe('function setElementAttribute', function () {
        const { setElementAttribute } = domutils;
        it('should exist', function () {
            expect(setElementAttribute).toBeFunction();
        });
    });

    describe('function setElementAttributes', function () {
        const { setElementAttributes } = domutils;
        it('should exist', function () {
            expect(setElementAttributes).toBeFunction();
        });
    });

    describe('function setElementDataAttr', function () {
        const { setElementDataAttr } = domutils;
        it('should exist', function () {
            expect(setElementDataAttr).toBeFunction();
        });
        it('should set data attributes', function () {
            const node = $('<div data-key1="42" data-key2="abc">')[0];
            setElementDataAttr(node, "key2", true);
            setElementDataAttr(node, "key3", -1);
            setElementDataAttr(node, "key4", "val");
            setElementDataAttr(node, "key5", false);
            setElementDataAttr(node, "key6", 0);
            expect(node.dataset.key1).toBe("42");
            expect(node.dataset.key2).toBe("true");
            expect(node.dataset.key3).toBe("-1");
            expect(node.dataset.key4).toBe("val");
            expect(node.dataset.key5).toBe("false");
            expect(node.dataset.key6).toBe("0");
        });
        it('should remove data attributes', function () {
            const node = $('<div data-key1="42" data-key2="abc", data-key3="1" data-key4="val">')[0];
            setElementDataAttr(node, "key2", "");
            setElementDataAttr(node, "key3", null);
            setElementDataAttr(node, "key4", undefined);
            expect(node.dataset.key1).toBe("42");
            expect(node.dataset).not.toHaveProperty("key2");
            expect(node.dataset).not.toHaveProperty("key3");
            expect(node.dataset).not.toHaveProperty("key4");
        });
    });

    describe('function setElementDataSet', function () {
        const { setElementDataSet } = domutils;
        it('should exist', function () {
            expect(setElementDataSet).toBeFunction();
        });
        it('should set data attributes', function () {
            const node = $('<div data-key1="42" data-key2="abc">')[0];
            setElementDataSet(node, { key2: true, key3: -1, key4: "val", key5: false, key6: 0 });
            expect(node.dataset.key1).toBe("42");
            expect(node.dataset.key2).toBe("true");
            expect(node.dataset.key3).toBe("-1");
            expect(node.dataset.key4).toBe("val");
            expect(node.dataset.key5).toBe("false");
            expect(node.dataset.key6).toBe("0");
        });
        it('should remove data attributes', function () {
            const node = $('<div data-key1="42" data-key2="abc", data-key3="1" data-key4="val">')[0];
            setElementDataSet(node, { key2: "", key3: null, key4: undefined });
            expect(node.dataset.key1).toBe("42");
            expect(node.dataset).not.toHaveProperty("key2");
            expect(node.dataset).not.toHaveProperty("key3");
            expect(node.dataset).not.toHaveProperty("key4");
        });
        it('should clear other data attributes', function () {
            const node = $('<div data-key1="42" data-key2="abc">')[0];
            setElementDataSet(node, { key2: true, key3: -1 }, { clear: true });
            expect(node.dataset).not.toHaveProperty("key1");
            expect(node.dataset.key2).toBe("true");
            expect(node.dataset.key3).toBe("-1");
        });
    });

    describe('function setElementStyle', function () {
        const { setElementStyle } = domutils;
        it('should exist', function () {
            expect(setElementStyle).toBeFunction();
        });
        it('should set CSS styles', function () {
            const node = $('<div style="overflow:scroll;display:table;">')[0];
            setElementStyle(node, "display", "none");
            expect(node).toHaveStyle({ overflow: 'scroll', display: 'none' });
            setElementStyle(node, "width", 42);
            expect(node).toHaveStyle({ width: '42px' });
            setElementStyle(node, "position", null);
            expect(node).toHaveStyle({ position: '' });
        });
        it('should clear CSS styles', function () {
            const node = $('<div style="overflow:scroll;display:table;">')[0];
            setElementStyle(node, "display", "none", { clear: true });
            expect(node).toHaveStyle({ overflow: '', display: 'none' });
        });
    });

    describe('function setElementStyles', function () {
        const { setElementStyles } = domutils;
        it('should exist', function () {
            expect(setElementStyles).toBeFunction();
        });
        it('should set CSS styles', function () {
            const node = $('<div style="overflow:scroll;display:table;">')[0];
            setElementStyles(node, { display: 'none', position: 'absolute' });
            expect(node).toHaveStyle({ overflow: 'scroll', display: 'none', position: 'absolute' });
            setElementStyles(node, { width: 42, position: null });
            expect(node).toHaveStyle({ width: '42px', position: '' });
        });
        it('should clear CSS styles', function () {
            const node = $('<div style="overflow:scroll;display:table;">')[0];
            setElementStyles(node, { display: 'none', position: 'absolute' }, { clear: true });
            expect(node).toHaveStyle({ overflow: '', display: 'none', position: 'absolute' });
        });
    });

    describe('function setElementLabel', function () {
        const { setElementLabel } = domutils;
        it('should exist', function () {
            expect(setElementLabel).toBeFunction();
        });
    });

    describe("function parseIntAttribute", () => {
        const { parseIntAttribute } = domutils;
        it("should exist", () => {
            expect(parseIntAttribute).toBeFunction();
        });
        it("should parse the attribute", () => {
            const node = document.createElement("div");
            node.dataset.num = "+42";
            node.dataset.text = "abc";
            expect(parseIntAttribute(node, "data-num")).toBe(42);
            expect(parseIntAttribute(node, "data-num", 43)).toBe(42);
            expect(parseIntAttribute(node, "data-text")).toBeUndefined();
            expect(parseIntAttribute(node, "data-text", 43)).toBe(43);
            expect(parseIntAttribute(node, "data-missing")).toBeUndefined();
            expect(parseIntAttribute(node, "data-missing", 43)).toBe(43);
        });
    });

    describe("function containsNode", () => {
        const { containsNode } = domutils;
        it("should exist", () => {
            expect(containsNode).toBeFunction();
        });
        it("should accept regular DOM elements", () => {
            const outer = $("<div>"), inner = $("<i>");
            expect(containsNode(outer, inner)).toBeFalse();
            expect(containsNode(inner, outer)).toBeFalse();
            outer.append(inner);
            expect(containsNode(outer, inner)).toBeTrue();
            expect(containsNode(inner, outer)).toBeFalse();
            expect(containsNode(outer[0], inner[0])).toBeTrue();
            expect(containsNode(inner[0], outer[0])).toBeFalse();
            $("<span>").append(inner).appendTo(outer);
            expect(containsNode(outer, inner)).toBeTrue();
            expect(containsNode(inner, outer)).toBeFalse();
        });
        it("should accept text nodes", () => {
            const outer = $("<span>"), inner = $(document.createTextNode("abc"));
            expect(containsNode(outer, inner)).toBeFalse();
            expect(containsNode(inner, outer)).toBeFalse();
            outer.append(inner);
            expect(containsNode(outer, inner)).toBeTrue();
            expect(containsNode(inner, outer)).toBeFalse();
            expect(containsNode(outer[0], inner[0])).toBeTrue();
            expect(containsNode(inner[0], outer[0])).toBeFalse();
        });
        it("should accept comment nodes", () => {
            const outer = $("<span>"), inner = $(document.createComment("abc"));
            expect(containsNode(outer, inner)).toBeFalse();
            expect(containsNode(inner, outer)).toBeFalse();
            outer.append(inner);
            expect(containsNode(outer, inner)).toBeTrue();
            expect(containsNode(inner, outer)).toBeFalse();
            expect(containsNode(outer[0], inner[0])).toBeTrue();
            expect(containsNode(inner[0], outer[0])).toBeFalse();
        });
        it("should process the document and the body element", () => {
            expect(containsNode(document, document.body)).toBeTrue();
            expect(containsNode(document.body, document)).toBeFalse();
            const inner = $("<div>");
            expect(containsNode(document, inner)).toBeFalse();
            expect(containsNode(inner, document)).toBeFalse();
            expect(containsNode(document.body, inner)).toBeFalse();
            expect(containsNode(inner, document.body)).toBeFalse();
            $(document.body).append(inner);
            expect(containsNode(document, inner)).toBeTrue();
            expect(containsNode(inner, document)).toBeFalse();
            expect(containsNode(document.body, inner)).toBeTrue();
            expect(containsNode(inner, document.body)).toBeFalse();
        });
        it("should process equal outer and inner node", () => {
            const node = $("<div>");
            expect(containsNode(node, node)).toBeFalse();
            expect(containsNode(node, node, { allowSelf: true })).toBeTrue();
        });
        it("should return false if the inner node is not a element", () => {
            const element = $('<div class="text">'), node = element[0].getAttributeNode("class");
            expect(containsNode(element, node)).toBeFalse();
            expect(containsNode(node, element)).toBeFalse();
            expect(containsNode(node, node)).toBeFalse();
        });
    });

    describe("function getDocumentSize", () => {
        const { getDocumentSize } = domutils;
        it("should exist", () => {
            expect(getDocumentSize).toBeFunction();
        });
        it("should return a size", () => {
            const size = getDocumentSize();
            expect(size.width).toBeNumber();
            expect(size.height).toBeNumber();
        });
    });

    describe("function getExactNodeSize", () => {
        const { getExactNodeSize } = domutils;
        it("should exist", () => {
            expect(getExactNodeSize).toBeFunction();
        });
        it("should return a size", () => {
            const size = getExactNodeSize(document.body);
            expect(size.width).toBeNumber();
            expect(size.height).toBeNumber();
        });
    });

    describe("function getFloorNodeSize", () => {
        const { getFloorNodeSize } = domutils;
        it("should exist", () => {
            expect(getFloorNodeSize).toBeFunction();
        });
        it("should return a size", () => {
            const size = getFloorNodeSize(document.body);
            expect(size.width).toBeInteger();
            expect(size.height).toBeInteger();
        });
    });

    describe("function getCeilNodeSize", () => {
        const { getCeilNodeSize } = domutils;
        it("should exist", () => {
            expect(getCeilNodeSize).toBeFunction();
        });
        it("should return a size", () => {
            const size = getCeilNodeSize(document.body);
            expect(size.width).toBeInteger();
            expect(size.height).toBeInteger();
        });
    });

    describe("function getElementPosition", () => {
        const { getElementPosition } = domutils;
        it("should exist", () => {
            expect(getElementPosition).toBeFunction();
        });
        it("should return a position", () => {
            const pos = getElementPosition(document.body);
            expect(pos.left).toBeNumber();
            expect(pos.top).toBeNumber();
            expect(pos.right).toBeNumber();
            expect(pos.bottom).toBeNumber();
            expect(pos.width).toBeNumber();
            expect(pos.height).toBeNumber();
        });
        it("should return an inner position", () => {
            const pos = getElementPosition(document.body, { inner: true });
            expect(pos.left).toBeNumber();
            expect(pos.top).toBeNumber();
            expect(pos.right).toBeNumber();
            expect(pos.bottom).toBeNumber();
            expect(pos.width).toBeNumber();
            expect(pos.height).toBeNumber();
        });
    });

    describe("function createText", () => {
        const { createText } = domutils;
        it("should exist", () => {
            expect(createText).toBeFunction();
        });
        it("should create a text node", () => {
            const node = createText("abc");
            expect(node).toBeInstanceOf(window.Text);
            expect(node).toHaveTextContent("abc");
        });
        it("should create a text node for a number", () => {
            expect(createText(42)).toHaveTextContent("42");
            expect(createText(-42)).toHaveTextContent("-42");
        });
        it("should create an empty text node", () => {
            expect(createText()).toHaveTextContent("");
            expect(createText(undefined)).toHaveTextContent("");
            expect(createText(null)).toHaveTextContent("");
            expect(createText("")).toHaveTextContent("");
        });
    });

    describe('function createElement', function () {
        const { createElement } = domutils;
        it("should exist", () => {
            expect(createElement).toBeFunction();
        });
        it("should create HTML elements", () => {
            expect(createElement("div")).toBeInstanceOf(HTMLDivElement);
            expect(createElement("span")).toBeInstanceOf(HTMLSpanElement);
        });
        it("should add CSS classes", () => {
            const node = createElement("div", "a b");
            expect(node).toHaveClass("a b", { exact: true });
            expect(node.childNodes).toHaveLength(0);
        });
        it("should add id, classes, styles, data attributes, tooltip, and label", () => {
            const node = createElement("div", { id: "id", classes: "a b", style: { display: "none" }, dataset: { key1: 42, key2: "abc", key3: true }, tooltip: "tooltip", label: "label" });
            expect(node.id).toBe("id");
            expect(node).toHaveClass("a b", { exact: true });
            expect(node).toHaveStyle({ display: "none" });
            expect(node.dataset.key1).toBe("42");
            expect(node.dataset.key2).toBe("abc");
            expect(node.dataset.key3).toBe("true");
            expect(node.title).toBe("tooltip");
            expect(node).toHaveTextContent("label");
        });
        it("should create empty text nodes", () => {
            const node = createElement("div", { label: "" });
            const child = node.childNodes[0];
            expect(child).toBeInstanceOf(window.Text);
            expect(child.data).toBe("");
        });
    });

    describe("function createDiv", () => {
        const { createDiv } = domutils;
        it("should exist", () => {
            expect(createDiv).toBeFunction();
        });
        it("should create HTML div elements", () => {
            expect(createDiv()).toBeInstanceOf(HTMLDivElement);
        });
        it("should add CSS classes", () => {
            const node = createDiv("a b");
            expect(node).toHaveClass("a b", { exact: true });
        });
        it("should add id, classes, styles, tooltip, and label", () => {
            const node = createDiv({ id: "id", classes: "a b", style: { display: "none" }, tooltip: "tooltip", label: "label" });
            expect(node.id).toBe("id");
            expect(node).toHaveClass("a b", { exact: true });
            expect(node).toHaveStyle({ display: "none" });
            expect(node.title).toBe("tooltip");
            expect(node).toHaveTextContent("label");
        });
        it("should create empty text nodes", () => {
            const node = createDiv({ label: "" });
            const child = node.childNodes[0];
            expect(child).toBeInstanceOf(window.Text);
            expect(child.data).toBe("");
        });
    });

    describe("function createSpan", () => {
        const { createSpan } = domutils;
        it("should exist", () => {
            expect(createSpan).toBeFunction();
        });
        it("should create HTML span elements", () => {
            expect(createSpan()).toBeInstanceOf(HTMLSpanElement);
        });
        it("should add CSS classes", () => {
            const node = createSpan("a b");
            expect(node).toHaveClass("a b", { exact: true });
        });
        it("should add id, classes, styles, tooltip, and label", () => {
            const node = createSpan({ id: "id", classes: "a b", style: { display: "none" }, tooltip: "tooltip", label: "label" });
            expect(node.id).toBe("id");
            expect(node).toHaveClass("a b", { exact: true });
            expect(node).toHaveStyle({ display: "none" });
            expect(node.title).toBe("tooltip");
            expect(node).toHaveTextContent("label");
        });
        it("should create empty text nodes", () => {
            const node = createSpan({ label: "" });
            const child = node.childNodes[0];
            expect(child).toBeInstanceOf(window.Text);
            expect(child.data).toBe("");
        });
    });

    describe("function createAnchor", () => {
        const { createAnchor } = domutils;
        it("should exist", () => {
            expect(createAnchor).toBeFunction();
        });
        it("should create HTML anchor elements", () => {
            expect(createAnchor()).toBeInstanceOf(HTMLAnchorElement);
        });
        it("should add CSS classes", () => {
            const node = createAnchor("http://example.org/", "a b");
            expect(node.href).toBe("http://example.org/");
            expect(node).toHaveClass("a b", { exact: true });
        });
        it("should add id, classes, styles, tooltip, label, and href", () => {
            const node = createAnchor("http://example.org/", { id: "id", classes: "a b", style: { display: "none" }, tooltip: "tooltip", label: "label" });
            expect(node.id).toBe("id");
            expect(node).toHaveClass("a b", { exact: true });
            expect(node).toHaveStyle({ display: "none" });
            expect(node.title).toBe("tooltip");
            expect(node).toHaveTextContent("label");
            expect(node.href).toBe("http://example.org/");
        });
    });

    describe("function createSvg", () => {
        const { createSvg } = domutils;
        it("should exist", () => {
            expect(createSvg).toBeFunction();
        });
        it("should create HTML elements", () => {
            expect(createSvg("svg")).toBeInstanceOf(SVGElement);
            // ES6-LATER: `SVGUseElement` not defined in Jest tests (JSDOM)
            // expect(createSvg("use")).toBeInstanceOf(window.SVGUseElement);
            expect(createSvg("use")).toBeInstanceOf(SVGElement);
            expect(createSvg("use").tagName).toEqualCaseInsensitive("use");
        });
        it("should add CSS classes", () => {
            const node = createSvg("svg", "a b");
            expect(node).toHaveClass("a b", { exact: true });
        });
        it("should add id, classes, and styles", () => {
            const node = createSvg("svg", { id: "id", classes: "a b", style: { display: "none" }, tooltip: "tooltip" });
            expect(node.id).toBe("id");
            expect(node).toHaveClass("a b", { exact: true });
            expect(node).toHaveStyle({ display: "none" });
        });
    });

    describe("function createCanvas", () => {
        const { createCanvas } = domutils;
        it("should exist", () => {
            expect(createCanvas).toBeFunction();
        });
        it("should create a canvas element", () => {
            const node = createCanvas(1, 2);
            expect(node).toBeInstanceOf(HTMLCanvasElement);
            expect(node.width).toBe(1);
            expect(node.height).toBe(2);
        });
        it("should add CSS classes", () => {
            const node = createCanvas(1, 2, "a b");
            expect(node).toHaveClass("a b", { exact: true });
        });
        it("should add id, classes, styles, and tooltip", () => {
            const node = createCanvas(1, 2, { id: "id", classes: "a b", style: { display: "none" }, tooltip: "tooltip" });
            expect(node.id).toBe("id");
            expect(node).toHaveClass("a b", { exact: true });
            expect(node).toHaveStyle({ display: "none" });
            expect(node.title).toBe("tooltip");
        });
    });

    describe("function createFragment", () => {
        const { createFragment } = domutils;
        it("should exist", () => {
            expect(createFragment).toBeFunction();
        });
        it("should create a fragment", () => {
            const fragment = createFragment();
            expect(fragment).toBeInstanceOf(DocumentFragment);
        });
    });

    describe("function createNodeList", () => {
        const { createNodeList } = domutils;
        it("should exist", () => {
            expect(createNodeList).toBeFunction();
        });
        it("should create a node list", () => {
            const list = createNodeList();
            expect(list).toBeInstanceOf(window.NodeList);
        });
    });

    describe("function convertLength", () => {
        const { convertLength } = domutils;
        it("should exist", () => {
            expect(convertLength).toBeFunction();
        });
        it("should convert pixels to a target unit", () => {
            expect(convertLength(4800, "px", "px")).toBeAlmost(4800);
            expect(convertLength(4800, "px", "pc")).toBeAlmost(43200);
            expect(convertLength(4800, "px", "pt")).toBeAlmost(3600);
            expect(convertLength(4800, "px", "in")).toBeAlmost(50);
            expect(convertLength(4800, "px", "cm")).toBeAlmost(127);
            expect(convertLength(4800, "px", "mm")).toBeAlmost(1270);
        });
        it("should convert picas to a target unit", () => {
            expect(convertLength(43200, "pc", "px")).toBeAlmost(4800);
            expect(convertLength(43200, "pc", "pc")).toBeAlmost(43200);
            expect(convertLength(43200, "pc", "pt")).toBeAlmost(3600);
            expect(convertLength(43200, "pc", "in")).toBeAlmost(50);
            expect(convertLength(43200, "pc", "cm")).toBeAlmost(127);
            expect(convertLength(43200, "pc", "mm")).toBeAlmost(1270);
        });
        it("should convert points to a target unit", () => {
            expect(convertLength(2653200, "pt", "px")).toBeAlmost(3537600);
            expect(convertLength(2653200, "pt", "pc")).toBeAlmost(31838400);
            expect(convertLength(2653200, "pt", "pt")).toBeAlmost(2653200);
            expect(convertLength(2653200, "pt", "in")).toBeAlmost(36850);
            expect(convertLength(2653200, "pt", "cm")).toBeAlmost(93599);
            expect(convertLength(2653200, "pt", "mm")).toBeAlmost(935990);
        });
        it("should convert inches to a target unit", () => {
            expect(convertLength(50, "in", "px")).toBeAlmost(4800);
            expect(convertLength(50, "in", "pc")).toBeAlmost(43200);
            expect(convertLength(50, "in", "pt")).toBeAlmost(3600);
            expect(convertLength(50, "in", "in")).toBeAlmost(50);
            expect(convertLength(50, "in", "cm")).toBeAlmost(127);
            expect(convertLength(50, "in", "mm")).toBeAlmost(1270);
        });
        it("should convert centimeters to a target unit", () => {
            expect(convertLength(127, "cm", "px")).toBeAlmost(4800);
            expect(convertLength(127, "cm", "pc")).toBeAlmost(43200);
            expect(convertLength(127, "cm", "pt")).toBeAlmost(3600);
            expect(convertLength(127, "cm", "in")).toBeAlmost(50);
            expect(convertLength(127, "cm", "cm")).toBeAlmost(127);
            expect(convertLength(127, "cm", "mm")).toBeAlmost(1270);
        });
        it("should convert millimeters to a target unit", () => {
            expect(convertLength(1270, "mm", "px")).toBeAlmost(4800);
            expect(convertLength(1270, "mm", "pc")).toBeAlmost(43200);
            expect(convertLength(1270, "mm", "pt")).toBeAlmost(3600);
            expect(convertLength(1270, "mm", "in")).toBeAlmost(50);
            expect(convertLength(1270, "mm", "cm")).toBeAlmost(127);
            expect(convertLength(1270, "mm", "mm")).toBeAlmost(1270);
        });
        it("should round to the specified precision", () => {
            expect(convertLength(1, "in", "mm", 0.01)).toBe(25.4);
            expect(convertLength(1, "in", "mm", 0.1)).toBe(25.4);
            expect(convertLength(1, "in", "mm", 1)).toBe(25);
            expect(convertLength(1, "in", "mm", 10)).toBe(30);
            expect(convertLength(1, "in", "mm", 100)).toBe(0);
        });
    });

    describe("function convertLengthToHmm", () => {
        const { convertLengthToHmm } = domutils;
        it("should exist", () => {
            expect(convertLengthToHmm).toBeFunction();
        });
        it("should convert a source unit to 1/100 mm", () => {
            expect(convertLengthToHmm(216, "px")).toBe(5715);
            expect(convertLengthToHmm(216, "pc")).toBe(635);
            expect(convertLengthToHmm(216, "pt")).toBe(7620);
            expect(convertLengthToHmm(216, "in")).toBe(548640);
            expect(convertLengthToHmm(216, "cm")).toBe(216000);
            expect(convertLengthToHmm(216, "mm")).toBe(21600);
        });
        it("should round to integers", () => {
            expect(convertLengthToHmm(0.015, "mm")).toBe(2);
        });
    });

    describe("function convertHmmToLength", () => {
        const { convertHmmToLength } = domutils;
        it("should exist", () => {
            expect(convertHmmToLength).toBeFunction();
        });
        it("should convert 1/100 mm to a target unit", () => {
            expect(convertHmmToLength(127000, "px")).toBeAlmost(4800);
            expect(convertHmmToLength(127000, "pc")).toBeAlmost(43200);
            expect(convertHmmToLength(127000, "pt")).toBeAlmost(3600);
            expect(convertHmmToLength(127000, "in")).toBeAlmost(50);
            expect(convertHmmToLength(127000, "cm")).toBeAlmost(127);
            expect(convertHmmToLength(127000, "mm")).toBeAlmost(1270);
        });
        it("should round to the specified precision", () => {
            expect(convertHmmToLength(2540, "mm", 0.01)).toBe(25.4);
            expect(convertHmmToLength(2540, "mm", 0.1)).toBe(25.4);
            expect(convertHmmToLength(2540, "mm", 1)).toBe(25);
            expect(convertHmmToLength(2540, "mm", 10)).toBe(30);
            expect(convertHmmToLength(2540, "mm", 100)).toBe(0);
        });
    });

    describe("function convertCssLength", () => {
        const { convertCssLength } = domutils;
        it("should exist", () => {
            expect(convertCssLength).toBeFunction();
        });
        it("should convert pixels to a target unit", () => {
            expect(convertCssLength("4800px", "px")).toBeAlmost(4800);
            expect(convertCssLength("4800px", "pc")).toBeAlmost(43200);
            expect(convertCssLength("4800px", "pt")).toBeAlmost(3600);
            expect(convertCssLength("4800px", "in")).toBeAlmost(50);
            expect(convertCssLength("4800px", "cm")).toBeAlmost(127);
            expect(convertCssLength("4800px", "mm")).toBeAlmost(1270);
        });
        it("should convert picas to a target unit", () => {
            expect(convertCssLength("43200pc", "px")).toBeAlmost(4800);
            expect(convertCssLength("43200pc", "pc")).toBeAlmost(43200);
            expect(convertCssLength("43200pc", "pt")).toBeAlmost(3600);
            expect(convertCssLength("43200pc", "in")).toBeAlmost(50);
            expect(convertCssLength("43200pc", "cm")).toBeAlmost(127);
            expect(convertCssLength("43200pc", "mm")).toBeAlmost(1270);
        });
        it("should convert points to a target unit", () => {
            expect(convertCssLength("2653200pt", "px")).toBeAlmost(3537600);
            expect(convertCssLength("2653200pt", "pc")).toBeAlmost(31838400);
            expect(convertCssLength("2653200pt", "pt")).toBeAlmost(2653200);
            expect(convertCssLength("2653200pt", "in")).toBeAlmost(36850);
            expect(convertCssLength("2653200pt", "cm")).toBeAlmost(93599);
            expect(convertCssLength("2653200pt", "mm")).toBeAlmost(935990);
        });
        it("should convert inches to a target unit", () => {
            expect(convertCssLength("50in", "px")).toBeAlmost(4800);
            expect(convertCssLength("50in", "pc")).toBeAlmost(43200);
            expect(convertCssLength("50in", "pt")).toBeAlmost(3600);
            expect(convertCssLength("50in", "in")).toBeAlmost(50);
            expect(convertCssLength("50in", "cm")).toBeAlmost(127);
            expect(convertCssLength("50in", "mm")).toBeAlmost(1270);
        });
        it("should convert centimeters to a target unit", () => {
            expect(convertCssLength("127cm", "px")).toBeAlmost(4800);
            expect(convertCssLength("127cm", "pc")).toBeAlmost(43200);
            expect(convertCssLength("127cm", "pt")).toBeAlmost(3600);
            expect(convertCssLength("127cm", "in")).toBeAlmost(50);
            expect(convertCssLength("127cm", "cm")).toBeAlmost(127);
            expect(convertCssLength("127cm", "mm")).toBeAlmost(1270);
        });
        it("should convert millimeters to a target unit", () => {
            expect(convertCssLength("1270mm", "px")).toBeAlmost(4800);
            expect(convertCssLength("1270mm", "pc")).toBeAlmost(43200);
            expect(convertCssLength("1270mm", "pt")).toBeAlmost(3600);
            expect(convertCssLength("1270mm", "in")).toBeAlmost(50);
            expect(convertCssLength("1270mm", "cm")).toBeAlmost(127);
            expect(convertCssLength("1270mm", "mm")).toBeAlmost(1270);
        });
        it("should round to the specified precision", () => {
            expect(convertCssLength("1in", "mm", 0.01)).toBe(25.4);
            expect(convertCssLength("1in", "mm", 0.1)).toBe(25.4);
            expect(convertCssLength("1in", "mm", 1)).toBe(25);
            expect(convertCssLength("1in", "mm", 10)).toBe(30);
            expect(convertCssLength("1in", "mm", 100)).toBe(0);
        });
        it("should convert numbers to a target unit", () => {
            expect(convertCssLength(4800, "px")).toBeAlmost(4800);
            expect(convertCssLength(4800, "pc")).toBeAlmost(43200);
            expect(convertCssLength(4800, "pt")).toBeAlmost(3600);
            expect(convertCssLength(4800, "in")).toBeAlmost(50);
            expect(convertCssLength(4800, "cm")).toBeAlmost(127);
            expect(convertCssLength(4800, "mm")).toBeAlmost(1270);
        });
        it("should return 0", () => {
            expect(convertCssLength("bla")).toBe(0);
        });
    });

    describe("function convertCssLengthToHmm", () => {
        const { convertCssLengthToHmm } = domutils;
        it("should exist", () => {
            expect(convertCssLengthToHmm).toBeFunction();
        });
        it("should convert a source unit to 1/100 mm", () => {
            expect(convertCssLengthToHmm("216px")).toBe(5715);
            expect(convertCssLengthToHmm("216pc")).toBe(635);
            expect(convertCssLengthToHmm("216pt")).toBe(7620);
            expect(convertCssLengthToHmm("216in")).toBe(548640);
            expect(convertCssLengthToHmm("216cm")).toBe(216000);
            expect(convertCssLengthToHmm("216mm")).toBe(21600);
            expect(convertCssLengthToHmm(216)).toBe(5715);
        });
        it("should round to integers", () => {
            expect(convertCssLengthToHmm("0.015mm")).toBe(2);
        });
    });

    describe("function convertHmmToCssLength", () => {
        const { convertHmmToCssLength } = domutils;
        it("should exist", () => {
            expect(convertHmmToCssLength).toBeFunction();
        });
        it("should convert 1/100 mm to a target unit", () => {
            expect(convertHmmToCssLength(127000, "px", 1)).toBe("4800px");
            expect(convertHmmToCssLength(127000, "pc", 1)).toBe("43200pc");
            expect(convertHmmToCssLength(127000, "pt", 1)).toBe("3600pt");
            expect(convertHmmToCssLength(127000, "in", 1)).toBe("50in");
            expect(convertHmmToCssLength(127000, "cm", 1)).toBe("127cm");
            expect(convertHmmToCssLength(127000, "mm", 1)).toBe("1270mm");
        });
        it("should round to the specified precision", () => {
            expect(convertHmmToCssLength(2540, "mm", 0.01)).toBe("25.4mm");
            expect(convertHmmToCssLength(2540, "mm", 0.1)).toBe("25.4mm");
            expect(convertHmmToCssLength(2540, "mm", 1)).toBe("25mm");
            expect(convertHmmToCssLength(2540, "mm", 10)).toBe("30mm");
            expect(convertHmmToCssLength(2540, "mm", 100)).toBe("0mm");
        });
    });

    describe("function parseCssLength", () => {
        const { parseCssLength } = domutils;
        it("should exist", () => {
            expect(parseCssLength).toBeFunction();
        });
        it("should return the length value in pixels", () => {
            const node = $('<div style="width:100px;height:30pt;margin-left:auto;">');
            expect(parseCssLength(node, "width")).toBe(100);
            expect(parseCssLength(node[0], "height")).toBe(40);
            expect(parseCssLength(node, "marginTop")).toBe(0);
            expect(parseCssLength(node, "marginLeft")).toBe(0);
            expect(parseCssLength($(), "width")).toBe(0);
        });
    });

    describe("function setCssRectToElement", () => {
        const { setCssRectToElement } = domutils;
        it("should exist", () => {
            expect(setCssRectToElement).toBeFunction();
        });
        it("should set the CSS rectangle", () => {
            const node = document.createElement("div");
            setCssRectToElement(node, { left: 0, top: -10, width: 20, height: 30 });
            expect(node).toHaveStyle({ left: "0", top: "-10px", width: "20px", height: "30px" });
        });
        it("should use custom length unit", () => {
            const node = document.createElement("div");
            setCssRectToElement(node, { left: 0, top: -10, width: 20, height: 30 }, "cm");
            expect(node).toHaveStyle({ left: "0cm", top: "-10cm", width: "20cm", height: "30cm" });
        });
    });

    describe("function resolveCssVar", () => {
        const { resolveCssVar } = domutils;
        it("should exist", () => {
            expect(resolveCssVar).toBeFunction();
        });
    });

    describe("function isHorizontalPosition", () => {
        const { isHorizontalPosition } = domutils;
        it("should exist", () => {
            expect(isHorizontalPosition).toBeFunction();
        });
        it("should return whether the position is horizontal", () => {
            expect(isHorizontalPosition("top")).toBeFalse();
            expect(isHorizontalPosition("bottom")).toBeFalse();
            expect(isHorizontalPosition("left")).toBeTrue();
            expect(isHorizontalPosition("right")).toBeTrue();
        });
    });

    describe("function isVerticalPosition", () => {
        const { isVerticalPosition } = domutils;
        it("should exist", () => {
            expect(isVerticalPosition).toBeFunction();
        });
        it("should return whether the position is vertical", () => {
            expect(isVerticalPosition("top")).toBeTrue();
            expect(isVerticalPosition("bottom")).toBeTrue();
            expect(isVerticalPosition("left")).toBeFalse();
            expect(isVerticalPosition("right")).toBeFalse();
        });
    });

    describe("function isLeadingPosition", () => {
        const { isLeadingPosition } = domutils;
        it("should exist", () => {
            expect(isLeadingPosition).toBeFunction();
        });
        it("should return whether the position is leading", () => {
            expect(isLeadingPosition("top")).toBeTrue();
            expect(isLeadingPosition("bottom")).toBeFalse();
            expect(isLeadingPosition("left")).toBeTrue();
            expect(isLeadingPosition("right")).toBeFalse();
        });
    });

    describe("function isTrailingPosition", () => {
        const { isTrailingPosition } = domutils;
        it("should exist", () => {
            expect(isTrailingPosition).toBeFunction();
        });
        it("should return whether the position is trailing", () => {
            expect(isTrailingPosition("top")).toBeFalse();
            expect(isTrailingPosition("bottom")).toBeTrue();
            expect(isTrailingPosition("left")).toBeFalse();
            expect(isTrailingPosition("right")).toBeTrue();
        });
    });

    describe("function insertHiddenNodes", () => {
        const { insertHiddenNodes } = domutils;
        it("should exist", () => {
            expect(insertHiddenNodes).toBeFunction();
        });
    });

    describe("function findHiddenNodes", () => {
        const { findHiddenNodes } = domutils;
        it("should exist", () => {
            expect(findHiddenNodes).toBeFunction();
        });
    });

    describe("function hasHiddenNodes", () => {
        const { hasHiddenNodes } = domutils;
        it("should exist", () => {
            expect(hasHiddenNodes).toBeFunction();
        });
    });
});
