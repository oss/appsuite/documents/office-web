/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as colorutils from "@/io.ox/office/tk/dom/colorutils";

// private functions ==========================================================

// verifies a color channel in the passed color model
function verifyChannel(colorModel, channelName, exp) {
    expect(colorModel[channelName]).toBeCloseTo(exp, 2);
}

// checks the "rgb" property of the passed color descriptor
function verifyRGB(colorDesc, expR, expG, expB, expA) {
    expect(colorDesc.rgb).toBeInstanceOf(colorutils.RGBModel);
    verifyChannel(colorDesc.rgb, "r", expR);
    verifyChannel(colorDesc.rgb, "g", expG);
    verifyChannel(colorDesc.rgb, "b", expB);
    verifyChannel(colorDesc.rgb, "a", expA);
}

// checks the "hsl" property of the passed color descriptor
function verifyHSL(colorDesc, expH, expS, expL, expA) {
    expect(colorDesc.hsl).toBeInstanceOf(colorutils.HSLModel);
    verifyChannel(colorDesc.hsl, "h", expH);
    verifyChannel(colorDesc.hsl, "s", expS);
    verifyChannel(colorDesc.hsl, "l", expL);
    verifyChannel(colorDesc.hsl, "a", expA);
}

// verifies the passed color descriptor
function verifyColorDescRGB(colorDesc, expR, expG, expB, expA, expHex, expCSS, expType) {
    verifyRGB(colorDesc, expR, expG, expB, expA);
    verifyChannel(colorDesc, "a", expA);
    expect(colorDesc).toHaveProperty("hex", expHex.toUpperCase());
    expect(colorDesc).toHaveProperty("css", expCSS);
    expect(colorDesc).toHaveProperty("type", expType);
}

// verifies the passed color descriptor
function verifyColorDescHSL(colorDesc, expH, expS, expL, expA) {
    verifyHSL(colorDesc, expH, expS, expL, expA);
    verifyChannel(colorDesc, "a", expA);
    expect(colorDesc).toHaveProperty("type", "hsl");
}

// tests ======================================================================

describe("module tk/dom/colorutils", () => {

    // class AlphaModel  ------------------------------------------------------

    describe("class AlphaModel", () => {
        it("should exist", () => {
            expect(colorutils.AlphaModel).toBeClass();
        });
    });

    // class RGBModel ---------------------------------------------------------

    describe("class RGBModel", () => {
        it("should subclass AlphaModel", () => {
            expect(colorutils.RGBModel).toBeSubClassOf(colorutils.AlphaModel);
        });
    });

    // class HSLModel ---------------------------------------------------------

    describe("class HSLModel", () => {
        it("should subclass AlphaModel", () => {
            expect(colorutils.HSLModel).toBeSubClassOf(colorutils.AlphaModel);
        });
    });

    // public functions -------------------------------------------------------

    describe("function getPresetColor", () => {
        const { getPresetColor } = colorutils;
        it("should exist", () => {
            expect(getPresetColor).toBeFunction();
        });
        it("should return the settings of a preset color", () => {
            expect(getPresetColor("LightYellow")).toMatch(/^[0-9a-f]{6}$/i);
            expect(getPresetColor("_invalid_")).toBeNull();
        });
    });

    describe("function getSystemColor", () => {
        const { getSystemColor } = colorutils;
        it("should exist", () => {
            expect(getSystemColor).toBeFunction();
        });
        it("should return the settings of a system color", () => {
            expect(getSystemColor("WindowText")).toMatch(/^[0-9a-f]{6}$/i);
            expect(getSystemColor("_invalid_")).toBeNull();
        });
    });

    describe("function parseCssColor", () => {
        const { parseCssColor } = colorutils;
        it("should exist", () => {
            expect(parseCssColor).toBeFunction();
        });
        it("should return null for empty or invalid string", () => {
            expect(parseCssColor("")).toBeNull();
            expect(parseCssColor("_invalid_")).toBeNull();
        });
        it("should parse keyword 'transparent'", () => {
            verifyColorDescRGB(parseCssColor("transparent"), 0, 0, 0, 0, "000000", "transparent", "transparent");
        });
        it("should parse preset color names", () => {
            verifyColorDescRGB(parseCssColor("black"), 0, 0, 0, 1, "000000", "#000000", "preset");
            verifyColorDescRGB(parseCssColor("white"), 1, 1, 1, 1, "FFFFFF", "#ffffff", "preset");
        });
        it("should parse system color names", () => {
            expect(parseCssColor("window")).toHaveProperty("type", "system");
        });
        it("should parse hexadecimal RGB color", () => {
            verifyColorDescRGB(parseCssColor("#0066cc"), 0, 0.4, 0.8, 1, "0066CC", "#0066cc", "rgb");
            verifyColorDescRGB(parseCssColor("#3399FF"), 0.2, 0.6, 1, 1, "3399FF", "#3399ff", "rgb");
            verifyColorDescRGB(parseCssColor("#06c"), 0, 0.4, 0.8, 1, "0066CC", "#0066cc", "rgb");
            verifyColorDescRGB(parseCssColor("#39F"), 0.2, 0.6, 1, 1, "3399FF", "#3399ff", "rgb");
        });
        it("should parse hexadecimal RGBA color", () => {
            verifyColorDescRGB(parseCssColor("#0066cc00"), 0, 0.4, 0.8, 0, "0066CC", "transparent", "rgb");
            verifyColorDescRGB(parseCssColor("#0066cccc"), 0, 0.4, 0.8, 0.8, "0066CC", "rgba(0,102,204,0.8)", "rgb");
            verifyColorDescRGB(parseCssColor("#0066ccFF"), 0, 0.4, 0.8, 1, "0066CC", "#0066cc", "rgb");
            verifyColorDescRGB(parseCssColor("#06c0"), 0, 0.4, 0.8, 0, "0066CC", "transparent", "rgb");
            verifyColorDescRGB(parseCssColor("#06cc"), 0, 0.4, 0.8, 0.8, "0066CC", "rgba(0,102,204,0.8)", "rgb");
            verifyColorDescRGB(parseCssColor("#06cF"), 0, 0.4, 0.8, 1, "0066CC", "#0066cc", "rgb");
        });
        it("should reject invalid hexadecimal RGB color", () => {
            expect(parseCssColor("#")).toBeNull();
            expect(parseCssColor("#4")).toBeNull();
            expect(parseCssColor("#48")).toBeNull();
            expect(parseCssColor("#4080c")).toBeNull();
            expect(parseCssColor("#4080c04")).toBeNull();
            expect(parseCssColor("#4080c0400")).toBeNull();
            expect(parseCssColor("#4080cg")).toBeNull();
        });
        it("should parse function-style RGB(A) color", () => {
            verifyColorDescRGB(parseCssColor("rgb(0,102,204)"), 0, 0.4, 0.8, 1, "0066CC", "#0066cc", "rgb");
            verifyColorDescRGB(parseCssColor("rgb( 51 , 153 , 255 )"), 0.2, 0.6, 1, 1, "3399FF", "#3399ff", "rgb");
            verifyColorDescRGB(parseCssColor("rgb(0,102,204,1)"), 0, 0.4, 0.8, 1, "0066CC", "#0066cc", "rgb");
            verifyColorDescRGB(parseCssColor("rgb( 51 , 153 , 255 , 0.8)"), 0.2, 0.6, 1, 0.8, "3399FF", "rgba(51,153,255,0.8)", "rgb");
            verifyColorDescRGB(parseCssColor("rgb(0,102,204,0)"), 0, 0.4, 0.8, 0, "0066CC", "transparent", "rgb");
            verifyColorDescRGB(parseCssColor("rgba(0,102,204)"), 0, 0.4, 0.8, 1, "0066CC", "#0066cc", "rgb");
            verifyColorDescRGB(parseCssColor("rgba( 51 , 153 , 255 )"), 0.2, 0.6, 1, 1, "3399FF", "#3399ff", "rgb");
            verifyColorDescRGB(parseCssColor("rgba(0,102,204,1)"), 0, 0.4, 0.8, 1, "0066CC", "#0066cc", "rgb");
            verifyColorDescRGB(parseCssColor("rgba( 51 , 153 , 255 , 0.8)"), 0.2, 0.6, 1, 0.8, "3399FF", "rgba(51,153,255,0.8)", "rgb");
            verifyColorDescRGB(parseCssColor("rgba(0,102,204,0)"), 0, 0.4, 0.8, 0, "0066CC", "transparent", "rgb");
            expect(parseCssColor("rgb(64,128)")).toBeNull();
            verifyColorDescRGB(parseCssColor("rgb(0,102,1000)"), 0, 0.4, 1, 1, "0066FF", "#0066ff", "rgb");
            expect(parseCssColor("rgb(64,128,ff)")).toBeNull();
            expect(parseCssColor("rgb(64,128,192,1,1)")).toBeNull();
            expect(parseCssColor("rgb(64,128,192,2)")).toBeNull();
            expect(parseCssColor("rgba(64,128,ff)")).toBeNull();
            expect(parseCssColor("rgba(64,128,192,1,1)")).toBeNull();
            expect(parseCssColor("rgba(64,128,192,2)")).toBeNull();
        });
        it("should parse function-style HSL(A) color", () => {
            verifyColorDescHSL(parseCssColor("hsl(60,100%,50%)"), 60, 1, 0.5, 1);
            verifyColorDescHSL(parseCssColor("hsl( 0 , 0% , 0% )"), 0, 0, 0, 1);
            verifyColorDescHSL(parseCssColor("hsl(60,100%,50%,1)"), 60, 1, 0.5, 1);
            verifyColorDescHSL(parseCssColor("hsl( 60 , 100% , 50% , 0.42 )"), 60, 1, 0.5, 0.42);
            verifyColorDescHSL(parseCssColor("hsl( 60 , 100% , 50% , 0 )"), 60, 1, 0.5, 0);
            verifyColorDescHSL(parseCssColor("hsla(60,100%,50%)"), 60, 1, 0.5, 1);
            verifyColorDescHSL(parseCssColor("hsla( 0 , 0% , 0% )"), 0, 0, 0, 1);
            verifyColorDescHSL(parseCssColor("hsla(60,100%,50%,1)"), 60, 1, 0.5, 1);
            verifyColorDescHSL(parseCssColor("hsla( 60 , 100% , 50% , 0.42 )"), 60, 1, 0.5, 0.42);
            verifyColorDescHSL(parseCssColor("hsla( 60 , 100% , 50% , 0 )"), 60, 1, 0.5, 0);
            expect(parseCssColor("hsl(60,100%)")).toBeNull();
            expect(parseCssColor("hsl(60,100%,50)")).toBeNull();
            expect(parseCssColor("hsl(60%,100%,50%)")).toBeNull();
            verifyColorDescHSL(parseCssColor("hsl(420,200%,50%)"), 60, 1, 0.5, 1);
            expect(parseCssColor("hsl(60,100%,ff%)")).toBeNull();
            expect(parseCssColor("hsl(60,100%,50%,1,1)")).toBeNull();
            expect(parseCssColor("hsl(60,100%,50%,2)")).toBeNull();
            expect(parseCssColor("hsla(60,100%,ff%)")).toBeNull();
            expect(parseCssColor("hsla(60,100%,50%,1,1)")).toBeNull();
            expect(parseCssColor("hsla(60,100%,50%,2)")).toBeNull();
        });
    });

    describe("function getSchemeColorCount", () => {
        const { getSchemeColorCount } = colorutils;
        it("should exist", () => {
            expect(getSchemeColorCount).toBeFunction();
        });
        it("should return a positive index", () => {
            expect(getSchemeColorCount()).toBeGreaterThanOrEqual(1);
        });
    });

    describe("function getSchemeColorIndex", () => {
        const { getSchemeColorIndex } = colorutils;
        it("should exist", () => {
            expect(getSchemeColorIndex).toBeFunction();
        });
        it("should return a positive index", () => {
            expect(getSchemeColorIndex(2)).toBeGreaterThanOrEqual(1);
        });
    });
});
