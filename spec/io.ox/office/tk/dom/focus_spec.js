/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Logger } from "@/io.ox/office/tk/utils/logger";
import { createSvg } from "@/io.ox/office/tk/dom/domutils";
import * as focus from "@/io.ox/office/tk/dom/focus";

// tests ======================================================================

describe("module tk/dom/focus", () => {

    // singletons -------------------------------------------------------------

    describe("singleton focusLogger", () => {
        const { focusLogger } = focus;
        it("should exist", () => {
            expect(focusLogger).toBeInstanceOf(Logger);
            expect(focusLogger).toRespondTo("logTarget");
        });
    });

    // public functions -------------------------------------------------------

    describe("function isFocusableElement", () => {
        const { isFocusableElement } = focus;
        it("should exist", () => {
            expect(isFocusableElement).toBeFunction();
        });
        it("should check types of values", () => {
            expect(isFocusableElement(null)).toBeFalse();
            expect(isFocusableElement(42)).toBeFalse();
            expect(isFocusableElement({})).toBeFalse();
            expect(isFocusableElement({ focus() {} })).toBeFalse();
            expect(isFocusableElement(document)).toBeFalse();
            expect(isFocusableElement(document.body)).toBeTrue();
            expect(isFocusableElement(createSvg())).toBeTrue();
            expect(isFocusableElement(document.createTextNode("a"))).toBeFalse();
        });
    });

    describe("function getFocus", () => {
        const { getFocus } = focus;
        it("should exist", () => {
            expect(getFocus).toBeFunction();
        });
    });

    describe("function setFocus", () => {
        const { setFocus } = focus;
        it("should exist", () => {
            expect(setFocus).toBeFunction();
        });
    });

    describe("function hideFocus", () => {
        const { hideFocus } = focus;
        it("should exist", () => {
            expect(hideFocus).toBeFunction();
        });
    });

    describe("function isFocused", () => {
        const { isFocused } = focus;
        it("should exist", () => {
            expect(isFocused).toBeFunction();
        });
    });

    describe("function containsFocus", () => {
        const { containsFocus } = focus;
        it("should exist", () => {
            expect(containsFocus).toBeFunction();
        });
    });

    describe("function isFocusable", () => {
        const { isFocusable } = focus;
        it("should exist", () => {
            expect(isFocusable).toBeFunction();
        });
    });

    describe("function findFocusable", () => {
        const { findFocusable } = focus;
        it("should exist", () => {
            expect(findFocusable).toBeFunction();
        });
    });

    describe("function setFocusInto", () => {
        const { setFocusInto } = focus;
        it("should exist", () => {
            expect(setFocusInto).toBeFunction();
        });
    });

    describe("function clearBrowserSelection", () => {
        const { clearBrowserSelection } = focus;
        it("should exist", () => {
            expect(clearBrowserSelection).toBeFunction();
        });
    });
});
