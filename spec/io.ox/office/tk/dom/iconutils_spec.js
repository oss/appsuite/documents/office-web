/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as iconutils from "@/io.ox/office/tk/dom/iconutils";

// tests ======================================================================

describe("module tk/dom/iconutils", () => {

    // public functions -------------------------------------------------------

    describe("function createIcon", () => {
        const { createIcon } = iconutils;
        it("should exist", () => {
            expect(createIcon).toBeFunction();
        });
        it("should create a Bootstrap SVG icon", () => {
            const icon1 = createIcon("bi:x");
            expect(icon1).toBeInstanceOf(SVGElement);
            expect(icon1).toHaveClass("bi", { exact: true });
            expect(icon1.dataset.iconId).toBe("x");
            const icon2 = createIcon("bi:x", "custom-class");
            expect(icon2).toHaveClass("bi custom-class", { exact: true });
            expect(icon2.dataset.iconId).toBe("x");
            const icon3 = createIcon("bi:x", 24);
            expect(icon3).toHaveStyle({ width: "24px", height: "24px" });
        });
        it("should create a Documents SVG icon", () => {
            const icon1 = createIcon("svg:comment");
            expect(icon1).toBeInstanceOf(SVGElement);
            expect(icon1).toHaveClass("bi docs-icon", { exact: true }); // class "bi" is also set for documents SVG icons
            expect(icon1.dataset.iconId).toBe("comment");
            // ES6-LATER: `SVGUseElement` not defined in Jest tests (JSDOM)
            expect(icon1.firstChild).toBeInstanceOf(SVGElement);
            expect(icon1.firstChild.tagName).toEqualCaseInsensitive("use");
            const icon2 = createIcon("svg:comment", "custom-class");
            expect(icon2).toHaveClass("bi docs-icon custom-class", { exact: true }); // class "bi" is also set for documents SVG icons
            expect(icon2.dataset.iconId).toBe("comment");
            const icon3 = createIcon("svg:comment", 24);
            expect(icon3).toHaveStyle({ width: "24px", height: "24px" });
        });
        it("should create a Documents PNG icon", () => {
            const icon1 = createIcon("png:bold");
            expect(icon1).toBeInstanceOf(HTMLElement);
            expect(icon1.tagName).toEqualCaseInsensitive("i");
            expect(icon1).toHaveClass("docs-icon", { exact: true });
            expect(icon1.dataset.iconId).toBe("bold");
            expect(icon1.childNodes).toHaveLength(0);
            const icon2 = createIcon("png:bold", "custom-class");
            expect(icon2).toBeInstanceOf(HTMLElement);
            expect(icon2.tagName).toEqualCaseInsensitive("i");
            expect(icon2).toHaveClass("docs-icon custom-class", { exact: true });
            expect(icon2.dataset.iconId).toBe("bold");
            expect(icon2.childNodes).toHaveLength(0);
        });
        it("should create a plain-text icon", () => {
            const icon1 = createIcon("txt:xy");
            expect(icon1).toBeInstanceOf(HTMLElement);
            expect(icon1.tagName).toEqualCaseInsensitive("i");
            expect(icon1).toHaveClass("docs-txt-icon", { exact: true });
            expect(icon1.dataset.iconId).toBe("xy");
            expect(icon1).toHaveTextContent("xy");
            const icon2 = createIcon("txt:xy", "custom-class");
            expect(icon2).toBeInstanceOf(HTMLElement);
            expect(icon2.tagName).toEqualCaseInsensitive("i");
            expect(icon2).toHaveClass("docs-txt-icon custom-class", { exact: true });
            expect(icon2.dataset.iconId).toBe("xy");
            expect(icon2).toHaveTextContent("xy");
        });
    });

    describe("function createIconMarkup", () => {
        const { createIconMarkup } = iconutils;
        it("should exist", () => {
            expect(createIconMarkup).toBeFunction();
        });
        it("should create a Bootstrap SVG icon", () => {
            const markup = createIconMarkup("bi:x");
            expect(markup).toMatch(/^<svg .*<\/svg>$/);
            expect(markup).toInclude('class="bi"');
            expect(markup).toInclude('data-icon-id="x"');
        });
        it("should create a Documents SVG icon", () => {
            const markup = createIconMarkup("svg:comment");
            expect(markup).toMatch(/^<svg .*<\/svg>$/);
            expect(markup).toInclude('class="bi docs-icon"');
            expect(markup).toInclude('data-icon-id="comment"');
        });
        it("should create a Documents PNG icon", () => {
            const markup = createIconMarkup("png:bold");
            expect(markup).toMatch(/^<i .*><\/i>$/);
            expect(markup).toInclude('class="docs-icon"');
            expect(markup).toInclude('data-icon-id="bold"');
        });
    });
});
