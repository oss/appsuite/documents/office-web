/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Rectangle } from "@/io.ox/office/tk/dom/rectangle";

// private functions ==========================================================

function rad(a) { return a * Math.PI / 180; }

// shortcut for creating new rectangles
function r(l, t, w, h) {
    return new Rectangle(l, t, w, h);
}

function expectPoint(point, expX, expY) {
    expect(point.x).toBeCloseTo(expX, 11);
    expect(point.y).toBeCloseTo(expY, 11);
}

function expectPolar(polar, expR, expA) {
    expect(polar.r).toBeCloseTo(expR, 11);
    expect(polar.a).toBeCloseTo(rad(expA), 11);
}

// tests ======================================================================

describe("module tk/dom/rectangle", () => {

    // class Rectangle --------------------------------------------------------

    describe("class Rectangle", () => {

        it("should exist", () => {
            expect(Rectangle).toBeFunction();
        });

        describe("constructor", () => {
            it("should take all parameters", () => {
                const rect = new Rectangle(-1, 1, 10, 11.5);
                expect(rect).toHaveProperty("left", -1);
                expect(rect).toHaveProperty("top", 1);
                expect(rect).toHaveProperty("width", 10);
                expect(rect).toHaveProperty("height", 11.5);
            });
        });

        describe("static function from", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveStaticMethod("from");
            });
            it("should create a rectangle", () => {
                const rect = Rectangle.from({ left: -1, top: 1, width: 10, height: 11.5 });
                expect(rect).toBeInstanceOf(Rectangle);
                expect(rect).toHaveProperty("left", -1);
                expect(rect).toHaveProperty("top", 1);
                expect(rect).toHaveProperty("width", 10);
                expect(rect).toHaveProperty("height", 11.5);
            });
            it("should create a rectangle from partial data", () => {
                const rect = Rectangle.from({});
                expect(rect).toBeInstanceOf(Rectangle);
                expect(rect).toHaveProperty("left", 0);
                expect(rect).toHaveProperty("top", 0);
                expect(rect).toHaveProperty("width", 0);
                expect(rect).toHaveProperty("height", 0);
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("clone");
            });
            it("should return a clone", () => {
                const rect1 = r(0, 1, 2, 3);
                const rect2 = rect1.clone();
                expect(rect2).not.toBe(rect1);
                expect(rect2).toEqual(rect1);
            });
        });

        describe("method equals", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("equals");
            });
            const rect = r(0, 1, 2, 3);
            it("should return whether the rectangles are equal", () => {
                expect(rect.equals(rect)).toBeTrue();
                expect(rect.equals(r(0, 1, 2, 3))).toBeTrue();
                expect(rect.equals(r(9, 1, 2, 3))).toBeFalse();
                expect(rect.equals(r(0, 9, 2, 3))).toBeFalse();
                expect(rect.equals(r(0, 1, 9, 3))).toBeFalse();
                expect(rect.equals(r(0, 1, 2, 9))).toBeFalse();
            });
            it("should accept an object", () => {
                expect(rect.equals({ left: 0, top: 1, width: 2, height: 3 })).toBeTrue();
            });
        });

        describe("method differs", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("differs");
            });
            const rect = r(0, 1, 2, 3);
            it("should return whether the rectangles are different", () => {
                expect(rect.differs(rect)).toBeFalse();
                expect(rect.differs(r(0, 1, 2, 3))).toBeFalse();
                expect(rect.differs(r(9, 1, 2, 3))).toBeTrue();
                expect(rect.differs(r(0, 9, 2, 3))).toBeTrue();
                expect(rect.differs(r(0, 1, 9, 3))).toBeTrue();
                expect(rect.differs(r(0, 1, 2, 9))).toBeTrue();
            });
            it("should accept an object", () => {
                expect(rect.differs({ left: 0, top: 1, width: 2, height: 3 })).toBeFalse();
            });
        });

        describe("method right", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("right");
            });
            it("should return the position of the right border", () => {
                expect(r(1, 2, 5, 5).right()).toBe(6);
            });
        });

        describe("method bottom", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("bottom");
            });
            it("should return the position of the lower border", () => {
                expect(r(1, 2, 5, 5).bottom()).toBe(7);
            });
        });

        describe("method centerX", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("centerX");
            });
            it("should return the X coordinate of the center", () => {
                expect(r(1, 2, 5, 5).centerX()).toBe(3.5);
            });
        });

        describe("method centerY", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("centerY");
            });
            it("should return the Y coordinate of the center", () => {
                expect(r(1, 2, 5, 5).centerY()).toBe(4.5);
            });
        });

        describe("method diag", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("diag");
            });
            it("should return the length of the diagonal", () => {
                expect(r(1, 2, 3, 4).diag()).toBe(5);
            });
        });

        describe("method area", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("area");
            });
            it("should return the area size", () => {
                expect(r(1, 2, 3, 4).area()).toBe(12);
            });
        });

        describe("method square", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("square");
            });
            it("should return whether a rectangle is a square", () => {
                expect(r(1, 2, 3, 4).square()).toBeFalse();
                expect(r(1, 2, 3, 3).square()).toBeTrue();
                expect(r(1, 2, 4, 3).square()).toBeFalse();
            });
        });

        describe("method topLeft", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("topLeft");
            });
            it("should return the top-left point", () => {
                expect(r(1, 2, 3, 4).topLeft()).toEqual({ x: 1, y: 2 });
            });
        });

        describe("method topRight", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("topRight");
            });
            it("should return the top-right point", () => {
                expect(r(1, 2, 3, 4).topRight()).toEqual({ x: 4, y: 2 });
            });
        });

        describe("method bottomLeft", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("bottomLeft");
            });
            it("should return the bottom-left point", () => {
                expect(r(1, 2, 3, 4).bottomLeft()).toEqual({ x: 1, y: 6 });
            });
        });

        describe("method bottomRight", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("bottomRight");
            });
            it("should return the bottom-right point", () => {
                expect(r(1, 2, 3, 4).bottomRight()).toEqual({ x: 4, y: 6 });
            });
        });

        describe("method leftCenter", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("leftCenter");
            });
            it("should return the left center point", () => {
                expect(r(1, 2, 3, 4).leftCenter()).toEqual({ x: 1, y: 4 });
            });
        });

        describe("method rightCenter", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("rightCenter");
            });
            it("should return the right center point", () => {
                expect(r(1, 2, 3, 4).rightCenter()).toEqual({ x: 4, y: 4 });
            });
        });

        describe("method topCenter", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("topCenter");
            });
            it("should return the top center point", () => {
                expect(r(1, 2, 3, 4).topCenter()).toEqual({ x: 2.5, y: 2 });
            });
        });

        describe("method bottomCenter", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("bottomCenter");
            });
            it("should return the bottom center point", () => {
                expect(r(1, 2, 3, 4).bottomCenter()).toEqual({ x: 2.5, y: 6 });
            });
        });

        describe("method center", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("center");
            });
            it("should return the center point", () => {
                expect(r(1, 2, 3, 4).center()).toEqual({ x: 2.5, y: 4 });
            });
        });

        describe("method containsX", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("containsX");
            });
            it("should return whether the X coordinate is inside", () => {
                const rect = r(1, 2, 5, 5);
                expect(rect.containsX(0.5)).toBeFalse();
                expect(rect.containsX(1)).toBeTrue();
                expect(rect.containsX(3)).toBeTrue();
                expect(rect.containsX(6)).toBeTrue();
                expect(rect.containsX(6.5)).toBeFalse();
            });
        });

        describe("method containsY", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("containsY");
            });
            it("should return whether the Y coordinate is inside", () => {
                const rect = r(1, 2, 5, 5);
                expect(rect.containsY(1.5)).toBeFalse();
                expect(rect.containsY(2)).toBeTrue();
                expect(rect.containsY(4)).toBeTrue();
                expect(rect.containsY(7)).toBeTrue();
                expect(rect.containsY(7.5)).toBeFalse();
            });
        });

        describe("method containsPoint", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("containsPoint");
            });
            const rect = r(1, 2, 5, 5);
            it("should return whether the point is inside", () => {
                expect(rect.containsPoint(0.5, 2)).toBeFalse();
                expect(rect.containsPoint(1, 1.5)).toBeFalse();
                expect(rect.containsPoint(1, 2)).toBeTrue();
                expect(rect.containsPoint(3, 4)).toBeTrue();
                expect(rect.containsPoint(6, 7)).toBeTrue();
                expect(rect.containsPoint(6.5, 7)).toBeFalse();
                expect(rect.containsPoint(6, 7.5)).toBeFalse();
            });
            it("should accept an object", () => {
                expect(rect.containsPoint({ x: 3, y: 4 })).toBeTrue();
            });
        });

        describe("method containsPixel", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("containsPixel");
            });
            const rect = r(1, 2, 5, 5);
            it("should return whether the pixel is inside", () => {
                expect(rect.containsPixel(0.5, 2)).toBeFalse();
                expect(rect.containsPixel(1, 1.5)).toBeFalse();
                expect(rect.containsPixel(1, 2)).toBeTrue();
                expect(rect.containsPixel(3, 4)).toBeTrue();
                expect(rect.containsPixel(5, 6)).toBeTrue();
                expect(rect.containsPixel(5.5, 6)).toBeFalse();
                expect(rect.containsPixel(5, 6.5)).toBeFalse();
            });
            it("should accept an object", () => {
                expect(rect.containsPixel({ x: 3, y: 4 })).toBeTrue();
            });
        });

        describe("method contains", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("contains");
            });
            const rect = r(1, 2, 5, 5);
            it("should return whether the rectangle is inside", () => {
                expect(rect.contains(r(0.5, 2, 3, 3))).toBeFalse();
                expect(rect.contains(r(1, 1.5, 3, 3))).toBeFalse();
                expect(rect.contains(r(1, 2, 3, 3))).toBeTrue();
                expect(rect.contains(r(3, 4, 3, 3))).toBeTrue();
                expect(rect.contains(r(3.5, 4, 3, 3))).toBeFalse();
                expect(rect.contains(r(3, 4.5, 3, 3))).toBeFalse();
            });
            it("should accept a plain JS object", () => {
                expect(rect.contains({ left: 1, top: 2, width: 3, height: 3 })).toBeTrue();
            });
        });

        describe("method overlaps", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("overlaps");
            });
            const rect = r(1, 2, 5, 5);
            it("should return whether the rectangles overlap", () => {
                expect(rect.overlaps(r(0, 3, 1, 1))).toBeFalse();
                expect(rect.overlaps(r(0.5, 3, 1, 1))).toBeTrue();
                expect(rect.overlaps(r(5.5, 3, 1, 1))).toBeTrue();
                expect(rect.overlaps(r(6, 3, 1, 1))).toBeFalse();
                expect(rect.overlaps(r(2, 1, 1, 1))).toBeFalse();
                expect(rect.overlaps(r(2, 1.5, 1, 1))).toBeTrue();
                expect(rect.overlaps(r(2, 6.5, 1, 1))).toBeTrue();
                expect(rect.overlaps(r(2, 7, 1, 1))).toBeFalse();
                expect(rect.overlaps(r(3, 3, 1, 1))).toBeTrue();
                expect(rect.overlaps(r(0, 3, 10, 1))).toBeTrue();
                expect(rect.overlaps(r(3, 0, 1, 10))).toBeTrue();
                expect(rect.overlaps(r(0, 0, 10, 10))).toBeTrue();
            });
            it("should accept a plain JS object", () => {
                expect(rect.overlaps({ left: 3, top: 3, width: 1, height: 1 })).toBeTrue();
            });
        });

        describe("method boundary", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("boundary");
            });
            const rect = r(1, 2, 5, 5);
            it("should return the bounding rectangle", () => {
                expect(rect.boundary(r(0, 0, 1, 1))).toEqual(r(0, 0, 6, 7));
                expect(rect.boundary(r(4, 4, 1, 1))).toEqual(r(1, 2, 5, 5));
                expect(rect.boundary(r(8, 8, 1, 1))).toEqual(r(1, 2, 8, 7));
            });
            it("should accept a plain JS object", () => {
                expect(rect.boundary({ left: 0, top: 0, width: 1, height: 1 })).toEqual(r(0, 0, 6, 7));
            });
        });

        describe("method intersect", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("intersect");
            });
            const rect = r(1, 2, 5, 5);
            it("should return the common area", () => {
                expect(rect.intersect(r(0, 3, 1, 1))).toBeNull();
                expect(rect.intersect(r(0.5, 3, 1, 1))).toEqual(r(1, 3, 0.5, 1));
                expect(rect.intersect(r(5.5, 3, 1, 1))).toEqual(r(5.5, 3, 0.5, 1));
                expect(rect.intersect(r(6, 3, 1, 1))).toBeNull();
                expect(rect.intersect(r(2, 1, 1, 1))).toBeNull();
                expect(rect.intersect(r(2, 1.5, 1, 1))).toEqual(r(2, 2, 1, 0.5));
                expect(rect.intersect(r(2, 6.5, 1, 1))).toEqual(r(2, 6.5, 1, 0.5));
                expect(rect.intersect(r(2, 7, 1, 1))).toBeNull();
                expect(rect.intersect(r(3, 3, 1, 1))).toEqual(r(3, 3, 1, 1));
                expect(rect.intersect(r(0, 3, 10, 1))).toEqual(r(1, 3, 5, 1));
                expect(rect.intersect(r(3, 0, 1, 10))).toEqual(r(3, 2, 1, 5));
                expect(rect.intersect(r(0, 0, 10, 10))).toEqual(r(1, 2, 5, 5));
            });
            it("should accept a plain JS object", () => {
                expect(rect.intersect({ left: 3, top: 3, width: 1, height: 1 })).toEqual(r(3, 3, 1, 1));
            });
        });

        describe("method remaining", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("remaining");
            });
            it("should return the remaining areas", () => {
                const rect = r(1, 2, 5, 5);
                expect(rect.remaining(r(0, 0, 1, 1))).toEqual([r(1, 2, 5, 5)]);
                expect(rect.remaining(r(0, 0, 3, 3))).toEqual([r(3, 2, 3, 1), r(1, 3, 5, 4)]);
                expect(rect.remaining(r(2, 0, 3, 3))).toEqual([r(1, 2, 1, 1), r(5, 2, 1, 1), r(1, 3, 5, 4)]);
                expect(rect.remaining(r(4, 0, 3, 3))).toEqual([r(1, 2, 3, 1), r(1, 3, 5, 4)]);
                expect(rect.remaining(r(0, 3, 3, 3))).toEqual([r(1, 2, 5, 1), r(3, 3, 3, 3), r(1, 6, 5, 1)]);
                expect(rect.remaining(r(2, 3, 3, 3))).toEqual([r(1, 2, 5, 1), r(1, 3, 1, 3), r(5, 3, 1, 3), r(1, 6, 5, 1)]);
                expect(rect.remaining(r(4, 3, 3, 3))).toEqual([r(1, 2, 5, 1), r(1, 3, 3, 3), r(1, 6, 5, 1)]);
                expect(rect.remaining(r(0, 6, 3, 3))).toEqual([r(1, 2, 5, 4), r(3, 6, 3, 1)]);
                expect(rect.remaining(r(2, 6, 3, 3))).toEqual([r(1, 2, 5, 4), r(1, 6, 1, 1), r(5, 6, 1, 1)]);
                expect(rect.remaining(r(4, 6, 3, 3))).toEqual([r(1, 2, 5, 4), r(1, 6, 3, 1)]);
                expect(rect.remaining(r(0, 0, 9, 3))).toEqual([r(1, 3, 5, 4)]);
                expect(rect.remaining(r(0, 3, 9, 3))).toEqual([r(1, 2, 5, 1), r(1, 6, 5, 1)]);
                expect(rect.remaining(r(0, 6, 9, 3))).toEqual([r(1, 2, 5, 4)]);
                expect(rect.remaining(r(0, 0, 3, 9))).toEqual([r(3, 2, 3, 5)]);
                expect(rect.remaining(r(2, 0, 3, 9))).toEqual([r(1, 2, 1, 5), r(5, 2, 1, 5)]);
                expect(rect.remaining(r(4, 0, 3, 9))).toEqual([r(1, 2, 3, 5)]);
                expect(rect.remaining(r(0, 0, 9, 9))).toEqual([]);
            });
        });

        describe("method pointToPolar", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("pointToPolar");
            });
            const rect = r(1, 2, 4, 6);
            it("should return the polar coordinates of a point", () => {
                const d = Math.sqrt(12.5);
                expect(rect.pointToPolar(3, 5)).toEqual({ r: 0, a: 0 });
                expectPolar(rect.pointToPolar(8, 5), 5, 0);
                expectPolar(rect.pointToPolar(3 + d, 5 + d), 5, 45);
                expectPolar(rect.pointToPolar(3, 10), 5, 90);
                expectPolar(rect.pointToPolar(3 - d, 5 + d), 5, 135);
                expectPolar(rect.pointToPolar(-2, 5), 5, 180);
                expectPolar(rect.pointToPolar(3 + d, 5 - d), 5, -45);
                expectPolar(rect.pointToPolar(3, 0), 5, -90);
                expectPolar(rect.pointToPolar(3 - d, 5 - d), 5, -135);
            });
            it("should accept an object", () => {
                expect(rect.pointToPolar({ x: 3, y: 5 })).toEqual({ r: 0, a: 0 });
            });
        });

        describe("method polarToPoint", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("polarToPoint");
            });
            const rect = r(1, 2, 4, 6);
            it("should return the coordinates of a point", () => {
                const d = Math.sqrt(12.5);
                expect(rect.polarToPoint(0, 0)).toEqual({ x: 3, y: 5 });
                expectPoint(rect.polarToPoint(5, rad(0)), 8, 5);
                expectPoint(rect.polarToPoint(5, rad(45)), 3 + d, 5 + d);
                expectPoint(rect.polarToPoint(5, rad(90)), 3, 10);
                expectPoint(rect.polarToPoint(5, rad(135)), 3 - d, 5 + d);
                expectPoint(rect.polarToPoint(5, rad(180)), -2, 5);
                expectPoint(rect.polarToPoint(5, rad(225)), 3 - d, 5 - d);
                expectPoint(rect.polarToPoint(5, rad(270)), 3, 0);
                expectPoint(rect.polarToPoint(5, rad(315)), 3 + d, 5 - d);
            });
            it("should accept an object", () => {
                expect(rect.polarToPoint({ r: 0, a: 0 })).toEqual({ x: 3, y: 5 });
            });
        });

        describe("method rotatePoint", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("rotatePoint");
            });
            const rect = r(1, 2, 4, 6);
            it("should rotate the point", () => {
                const d = Math.sqrt(12.5);
                expect(rect.rotatePoint(0, 3, 5)).toEqual({ x: 3, y: 5 });
                expectPoint(rect.rotatePoint(rad(0), 3 + d, 5 + d), 3 + d, 5 + d);
                expectPoint(rect.rotatePoint(rad(45), 3 + d, 5 + d), 3, 10);
                expectPoint(rect.rotatePoint(rad(90), 3 + d, 5 + d), 3 - d, 5 + d);
                expectPoint(rect.rotatePoint(rad(135), 3 + d, 5 + d), -2, 5);
                expectPoint(rect.rotatePoint(rad(180), 3 + d, 5 + d), 3 - d, 5 - d);
                expectPoint(rect.rotatePoint(rad(225), 3 + d, 5 + d), 3, 0);
                expectPoint(rect.rotatePoint(rad(270), 3 + d, 5 + d), 3 + d, 5 - d);
                expectPoint(rect.rotatePoint(rad(315), 3 + d, 5 + d), 8, 5);
            });
            it("should accept an object", () => {
                expect(rect.rotatePoint(0, { x: 3, y: 5 })).toEqual({ x: 3, y: 5 });
            });
        });

        describe("method rotatedBoundingBox", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("rotatedBoundingBox");
            });
            it("should return the rotated bounding box", () => {
                expect(r(100, 50, 1000, 500).rotatedBoundingBox(rad(0)).roundSelf()).toEqual(r(100, 50, 1000, 500));
                expect(r(100, 50, 1000, 500).rotatedBoundingBox(rad(30)).roundSelf()).toEqual(r(42, -167, 1116, 933));
                expect(r(100, 50, 1000, 500).rotatedBoundingBox(rad(60)).roundSelf()).toEqual(r(133, -258, 933, 1116));
                expect(r(100, 50, 1000, 500).rotatedBoundingBox(rad(90)).roundSelf()).toEqual(r(350, -200, 500, 1000));
                expect(r(100, 50, 1000, 500).rotatedBoundingBox(rad(110)).roundSelf()).toEqual(r(194, -255, 812, 1111));
                expect(r(100, 50, 1000, 500).rotatedBoundingBox(rad(155)).roundSelf()).toEqual(r(41, -138, 1118, 876));
                expect(r(100, 50, 1000, 500).rotatedBoundingBox(rad(180)).roundSelf()).toEqual(r(100, 50, 1000, 500));
                expect(r(100, 50, 1000, 500).rotatedBoundingBox(rad(225)).roundSelf()).toEqual(r(70, -230, 1061, 1061));
            });
        });

        describe("method set", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("set");
            });
            it("should set new coordinates", () => {
                const rect = r(0, 0, 0, 0);
                expect(rect.set(1, 2, 3, 4)).toBe(rect);
                expect(rect).toEqual(r(1, 2, 3, 4));
            });
        });

        describe("method assign", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("assign");
            });
            it("should set new coordinates", () => {
                const rect1 = r(0, 0, 0, 0), rect2 = r(1, 2, 3, 4);
                expect(rect1.assign(rect2)).toBe(rect1);
                expect(rect1).toEqual(r(1, 2, 3, 4));
            });
        });

        describe("method expandSelf", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("expandSelf");
            });
            it("should expand the rectangle in-place", () => {
                const rect = r(1, 2, 5, 5);
                expect(rect.expandSelf(1, 2, 3, 4)).toBe(rect);
                expect(rect.left).toBe(0);
                expect(rect.top).toBe(0);
                expect(rect.width).toBe(9);
                expect(rect.height).toBe(11);
            });
            it("should shrink the rectangle in-place", () => {
                const rect = r(1, 2, 5, 5);
                expect(rect.expandSelf(-1, -2, -3, -4)).toBe(rect);
                expect(rect.left).toBe(2);
                expect(rect.top).toBe(4);
                expect(rect.width).toBe(1);
                expect(rect.height).toBe(0);
            });
            it("should support omitted parameters", () => {
                const rect = r(1, 2, 5, 5);
                rect.expandSelf(1);
                expect(rect.left).toBe(0);
                expect(rect.top).toBe(1);
                expect(rect.width).toBe(7);
                expect(rect.height).toBe(7);
                rect.expandSelf(-1, -2);
                expect(rect.left).toBe(1);
                expect(rect.top).toBe(3);
                expect(rect.width).toBe(5);
                expect(rect.height).toBe(3);
                rect.expandSelf(1, 2, 3);
                expect(rect.left).toBe(0);
                expect(rect.top).toBe(1);
                expect(rect.width).toBe(9);
                expect(rect.height).toBe(7);
            });
        });

        describe("method translateSelf", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("translateSelf");
            });
            it("should translate the rectangle in-place", () => {
                const rect = r(1, 2, 5, 5);
                expect(rect.translateSelf(2, -0.5)).toBe(rect);
                expect(rect.left).toBe(3);
                expect(rect.top).toBe(1.5);
                expect(rect.width).toBe(5);
                expect(rect.height).toBe(5);
            });
        });

        describe("method scaleSelf", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("scaleSelf");
            });
            it("should scale the rectangle in-place", () => {
                const rect = r(1, 2, 5, 5);
                expect(rect.scaleSelf(2, 0.5)).toBe(rect);
                expect(rect.left).toBe(2);
                expect(rect.top).toBe(1);
                expect(rect.width).toBe(10);
                expect(rect.height).toBe(2.5);
            });
            it("should accept one parameter", () => {
                const rect = r(1, 2, 3, 4);
                expect(rect.scaleSelf(2)).toBe(rect);
                expect(rect.left).toBe(2);
                expect(rect.top).toBe(4);
                expect(rect.width).toBe(6);
                expect(rect.height).toBe(8);
            });
        });

        describe("method rotateSelf", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("rotateSelf");
            });
            it("should rotate the rectangle in-place", () => {
                const rect = r(1, 2, 5, 6);
                expect(rect.rotateSelf()).toBe(rect);
                expect(rect.left).toBe(0.5);
                expect(rect.top).toBe(2.5);
                expect(rect.width).toBe(6);
                expect(rect.height).toBe(5);
            });
        });

        describe("method roundSelf", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("roundSelf");
            });
            it("should round the coordinates in-place", () => {
                const rect = r(1.2, 2.5, 5.8, 5);
                expect(rect.roundSelf()).toBe(rect);
                expect(rect.left).toBe(1);
                expect(rect.top).toBe(3);
                expect(rect.width).toBe(6);
                expect(rect.height).toBe(5);
            });
        });

        describe("method toCSS", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("toCSS");
            });
            const rect = r(1, 2, 3, 4);
            it("should return the CSS properties", () => {
                const css = rect.toCSS();
                expect(css).toBeObject();
                expect(css).not.toBeInstanceOf(Rectangle);
                expect(css).toEqual({ left: "1px", top: "2px", width: "3px", height: "4px" });
            });
            it("should take an arbitrary unit", () => {
                expect(rect.toCSS("em")).toEqual({ left: "1em", top: "2em", width: "3em", height: "4em" });
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("toString");
            });
            it("should stringify the rectangle", () => {
                expect(r(1, 2, 3, 4).toString()).toBeString();
            });
        });

        describe("method toJSON", () => {
            it("should exist", () => {
                expect(Rectangle).toHaveMethod("toJSON");
            });
            const rect = r(1, 2, 3, 4);
            const exp = { left: 1, top: 2, width: 3, height: 4 };
            it("should return the JSON representation", () => {
                const json = rect.toJSON();
                expect(json).toBeObject();
                expect(json).not.toBeInstanceOf(Rectangle);
                expect(json).toEqual(exp);
            });
            it("should stringify implicitly", () => {
                expect(JSON.parse(JSON.stringify(rect))).toEqual(exp);
            });
        });
    });
});
