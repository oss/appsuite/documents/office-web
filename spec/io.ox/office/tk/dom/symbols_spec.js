/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as symbols from "@/io.ox/office/tk/dom/symbols";

// tests ======================================================================

describe("module tk/dom/symbols", () => {

    // public functions -------------------------------------------------------

    describe("function isSymbolFont", () => {
        const { isSymbolFont } = symbols;
        it("should exist", () => {
            expect(isSymbolFont).toBeFunction();
        });
        it("should return true for supported symbol fonts", () => {
            expect(isSymbolFont("symbol")).toBeTrue();
            expect(isSymbolFont("Symbol")).toBeTrue();
            expect(isSymbolFont("SYMBOL")).toBeTrue();
            expect(isSymbolFont("starsymbol")).toBeTrue();
            expect(isSymbolFont("StarSymbol")).toBeTrue();
            expect(isSymbolFont("opensymbol")).toBeTrue();
            expect(isSymbolFont("OpenSymbol")).toBeTrue();
            expect(isSymbolFont("wingdings")).toBeTrue();
            expect(isSymbolFont("WingDings")).toBeTrue();
            expect(isSymbolFont("wingdings2")).toBeTrue();
            expect(isSymbolFont("WingDings2")).toBeTrue();
            expect(isSymbolFont("wingdings 2")).toBeTrue();
            expect(isSymbolFont("WingDings 2")).toBeTrue();
        });
        it("should return false for other fonts", () => {
            expect(isSymbolFont("arial")).toBeFalse();
            expect(isSymbolFont("serif")).toBeFalse();
        });
    });

    describe("function getSymbolReplacement", () => {
        const { getSymbolReplacement } = symbols;
        it("should exist", () => {
            expect(getSymbolReplacement).toBeFunction();
        });
        it("should return supported symbol replacements", () => {
            expect(getSymbolReplacement("symbol", "!")).toEqual({ char: "!", serif: true });
            expect(getSymbolReplacement("Symbol", "A")).toEqual({ char: "\u0391", serif: true }); // capital alpha
            expect(getSymbolReplacement("SYMBOL", "a")).toEqual({ char: "\u03b1", serif: true }); // small alpha
            expect(getSymbolReplacement("symbol", "\xc0")).toEqual({ char: "\u2135", serif: false }); // alef symbol
            expect(getSymbolReplacement("symbol", "\uf0c0")).toEqual({ char: "\u2135", serif: false }); // alef symbol
            expect(getSymbolReplacement("wingdings", "A")).toEqual({ char: "\u270c", serif: false });
            expect(getSymbolReplacement("WingDings", "\uf041")).toEqual({ char: "\u270c", serif: false });
            expect(getSymbolReplacement("wingdings2", "\x97")).toEqual({ char: "\u25cf", serif: false });
            expect(getSymbolReplacement("WingDings2", "\uf097")).toEqual({ char: "\u25cf", serif: false });
            expect(getSymbolReplacement("wingdings 2", "\x97")).toEqual({ char: "\u25cf", serif: false });
            expect(getSymbolReplacement("WingDings 2", "\uf097")).toEqual({ char: "\u25cf", serif: false });
            expect(getSymbolReplacement("starsymbol", "A")).toEqual({ char: "A", serif: false });
            expect(getSymbolReplacement("StarSymbol", "\ue001")).toEqual({ char: "\u2b2a", serif: false });
            expect(getSymbolReplacement("STARSYMBOL", "\ue080")).toEqual({ char: "\u2030", serif: true });
            expect(getSymbolReplacement("opensymbol", "A")).toEqual({ char: "A", serif: false });
            expect(getSymbolReplacement("OpenSymbol", "\ue001")).toEqual({ char: "\u2b2a", serif: false });
            expect(getSymbolReplacement("OPENSYMBOL", "\ue080")).toEqual({ char: "\u2030", serif: true });
        });
        it("should return undefined for invalid fonts", () => {
            expect(getSymbolReplacement("arial", "A")).toBeUndefined();
            expect(getSymbolReplacement("serif", "A")).toBeUndefined();
        });
        it("should return undefined for invalid symbols", () => {
            expect(getSymbolReplacement("symbol", "")).toBeUndefined();
            expect(getSymbolReplacement("symbol", "AB")).toBeUndefined();
            expect(getSymbolReplacement("symbol", "\xff")).toBeUndefined();
        });
    });
});
