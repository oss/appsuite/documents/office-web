/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as device from "@/io.ox/office/tk/dom/device";

// tests ======================================================================

describe("module tk/dom/device", () => {

    // constants --------------------------------------------------------------

    describe("constants", () => {
        it("should exist", () => {
            expect(device.RETINA_DISPLAY).toBeBoolean();
            expect(device.TOUCH_DEVICE).toBeBoolean();
            expect(device.SMALL_DEVICE).toBeBoolean();
            expect(device.MEDIUM_DEVICE).toBeBoolean();
            expect(device.COMPACT_DEVICE).toBeBoolean();
            expect(device.IOS_SAFARI_DEVICE).toBeBoolean();
            expect(device.SCROLLBAR_WIDTH).toBeNumber();
            expect(device.SCROLLBAR_HEIGHT).toBeNumber();
        });
    });

    // public functions -------------------------------------------------------

    describe("function isPortrait", () => {
        const { isPortrait } = device;
        it("should exist", () => {
            expect(isPortrait).toBeFunction();
        });
        it("should return a boolean", () => {
            expect(isPortrait()).toBeBoolean();
        });
    });

    describe("function isLandscape", () => {
        const { isLandscape } = device;
        it("should exist", () => {
            expect(isLandscape).toBeFunction();
        });
        it("should return a boolean", () => {
            expect(isLandscape()).toBeBoolean();
        });
    });

    describe("function getScreenWidth", () => {
        const { getScreenWidth } = device;
        it("should exist", () => {
            expect(getScreenWidth).toBeFunction();
        });
        it("should return a number", () => {
            expect(getScreenWidth()).toBeNumber();
        });
    });

    describe("function getScreenHeight", () => {
        const { getScreenHeight } = device;
        it("should exist", () => {
            expect(getScreenHeight).toBeFunction();
        });
        it("should return a number", () => {
            expect(getScreenHeight()).toBeNumber();
        });
    });

    describe("function getWindowWidth", () => {
        const { getWindowWidth } = device;
        it("should exist", () => {
            expect(getWindowWidth).toBeFunction();
        });
        it("should return a number", () => {
            expect(getWindowWidth()).toBeNumber();
        });
    });

    describe("function getWindowHeight", () => {
        const { getWindowHeight } = device;
        it("should exist", () => {
            expect(getWindowHeight).toBeFunction();
        });
        it("should return a number", () => {
            expect(getWindowHeight()).toBeNumber();
        });
    });

    describe("function getDeviceResolution", () => {
        const { getDeviceResolution } = device;
        it("should exist", () => {
            expect(getDeviceResolution).toBeFunction();
        });
        it("should return a number", () => {
            expect(getDeviceResolution()).toBeNumber();
        });
    });

    describe("function addDeviceMarkers", () => {
        const { addDeviceMarkers } = device;
        it("should exist", () => {
            expect(addDeviceMarkers).toBeFunction();
        });
        it("should add browser-specific classes to a node", () => {
            const elem = document.createElement("div");
            addDeviceMarkers(elem);
            expect(elem.classList.length).toBeGreaterThanOrEqual(1);
        });
    });
});
