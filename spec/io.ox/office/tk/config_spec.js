/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as config from "@/io.ox/office/tk/config";

// tests ======================================================================

describe("module tk/config", () => {

    // constants --------------------------------------------------------------

    describe("constant STORAGE_AVAILABLE", () => {
        it("should exist", () => {
            expect(config.STORAGE_AVAILABLE).toBeBoolean();
        });
    });

    describe("constant DEBUG_AVAILABLE", () => {
        it("should exist", () => {
            expect(config.DEBUG_AVAILABLE).toBeBoolean();
        });
    });

    describe("constant DEBUG_ON_DEMAND", () => {
        it("should exist", () => {
            expect(config.DEBUG_ON_DEMAND).toBeBoolean();
        });
    });

    describe("constant DEBUG", () => {
        it("should exist", () => {
            expect(config.DEBUG).toBeBoolean();
        });
    });

    describe("constant AUTOTEST", () => {
        it("should exist", () => {
            expect(config.AUTOTEST).toBeBoolean();
        });
    });

    describe("constant UNITTEST", () => {
        it("should exist", () => {
            expect(config.UNITTEST).toBeTrue();
        });
    });

    describe("constant LOG_ERROR_DATA", () => {
        it("should exist", () => {
            expect(config.LOG_ERROR_DATA).toBeBoolean();
        });
    });

    describe("constant LOG_PERFORMANCE_DATA", () => {
        it("should exist", () => {
            expect(config.LOG_PERFORMANCE_DATA).toBeBoolean();
        });
    });

    // public functions -------------------------------------------------------

    describe("function getValue", () => {
        it("should exist", () => {
            expect(config.getValue).toBeFunction();
        });
        it("should return something", () => {
            expect(config.getValue("_missing_")).toBeUndefined();
            expect(config.getValue("_missing_", 42)).toBe(42);
        });
    });

    describe("function getFlag", () => {
        it("should exist", () => {
            expect(config.getFlag).toBeFunction();
        });
        it("should return something", () => {
            expect(config.getFlag("_missing_")).toBeFalse();
            expect(config.getFlag("_missing_", false)).toBeFalse();
            expect(config.getFlag("_missing_", true)).toBeTrue();
        });
    });

    describe("function getStr", () => {
        it("should exist", () => {
            expect(config.getStr).toBeFunction();
        });
        it("should return something", () => {
            expect(config.getStr("_missing_")).toBe("");
            expect(config.getStr("_missing_", "42")).toBe("42");
        });
    });

    describe("function getInt", () => {
        it("should exist", () => {
            expect(config.getInt).toBeFunction();
        });
        it("should return something", () => {
            expect(config.getInt("_missing_")).toBe(0);
            expect(config.getInt("_missing_", 42)).toBe(42);
        });
    });

    describe("function setValue", () => {
        it("should exist", () => {
            expect(config.setValue).toBeFunction();
        });
    });

    describe("function getStorageValue", () => {
        it("should exist", () => {
            expect(config.getStorageValue).toBeFunction();
        });
    });

    describe("function getStorageFlag", () => {
        it("should exist", () => {
            expect(config.getStorageFlag).toBeFunction();
        });
    });

    describe("function getStorageStr", () => {
        it("should exist", () => {
            expect(config.getStorageStr).toBeFunction();
        });
    });

    describe("function getStorageInt", () => {
        it("should exist", () => {
            expect(config.getStorageInt).toBeFunction();
        });
    });

    describe("function setStorageValue", () => {
        it("should exist", () => {
            expect(config.setStorageValue).toBeFunction();
        });
    });

    describe("function removeStorageValue", () => {
        it("should exist", () => {
            expect(config.removeStorageValue).toBeFunction();
        });
    });

    describe("function getUrlFlag", () => {
        it("should exist", () => {
            expect(config.getUrlFlag).toBeFunction();
        });
        it("should return something", () => {
            expect(config.getUrlFlag("_missing_")).toBeFalse();
            expect(config.getUrlFlag("_missing_", { default: false })).toBeFalse();
            expect(config.getUrlFlag("_missing_", { default: true })).toBeTrue();
        });
    });

    describe("function getDebugValue", () => {
        it("should exist", () => {
            expect(config.getDebugValue).toBeFunction();
        });
    });

    describe("function getDebugFlag", () => {
        it("should exist", () => {
            expect(config.getDebugFlag).toBeFunction();
        });
        it("should return something", () => {
            expect(config.getDebugFlag("_missing_")).toBeFalse();
        });
    });

    describe("function setDebugFlag", () => {
        it("should exist", () => {
            expect(config.setDebugFlag).toBeFunction();
        });
    });
});
