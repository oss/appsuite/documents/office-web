/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as button from "@/io.ox/office/tk/control/button";
import * as captioncontrol from "@/io.ox/office/tk/control/captioncontrol";
import * as checkbox from "@/io.ox/office/tk/control/checkbox";
import * as combofield from "@/io.ox/office/tk/control/combofield";
import * as group from "@/io.ox/office/tk/control/group";
import * as label from "@/io.ox/office/tk/control/label";
import * as lengthfield from "@/io.ox/office/tk/control/lengthfield";
import * as percentfield from "@/io.ox/office/tk/control/percentfield";
import * as radiogroup from "@/io.ox/office/tk/control/radiogroup";
import * as radiolist from "@/io.ox/office/tk/control/radiolist";
import * as spinfield from "@/io.ox/office/tk/control/spinfield";
import * as textfield from "@/io.ox/office/tk/control/textfield";
import * as userpicture from "@/io.ox/office/tk/control/userpicture";

import * as controls from "@/io.ox/office/tk/controls";

// tests ======================================================================

describe("module tk/controls", () => {

    // re-exports -------------------------------------------------------------

    it("re-exports should exist", () => {
        expect(controls).toContainProps(button);
        expect(controls).toContainProps(captioncontrol);
        expect(controls).toContainProps(checkbox);
        expect(controls).toContainProps(combofield);
        expect(controls).toContainProps(group);
        expect(controls).toContainProps(label);
        expect(controls).toContainProps(lengthfield);
        expect(controls).toContainProps(percentfield);
        expect(controls).toContainProps(radiogroup);
        expect(controls).toContainProps(radiolist);
        expect(controls).toContainProps(spinfield);
        expect(controls).toContainProps(textfield);
        expect(controls).toContainProps(userpicture);
    });
});
