/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import * as utils from "@/io.ox/office/tk/utils";

// tests ======================================================================

describe("module tk/utils", () => {

    // constants --------------------------------------------------------------

    describe("constant BREAK", () => {
        it("should exist", () => {
            expect(utils.BREAK).toBeObject();
        });
    });

    describe("constant SMALL_DEVICE", () => {
        it("should exist", () => {
            expect(utils.SMALL_DEVICE).toBeBoolean();
        });
    });

    describe("constant COMPACT_DEVICE", () => {
        it("should exist", () => {
            expect(utils.COMPACT_DEVICE).toBeBoolean();
        });
    });

    describe("constant CHROME_ON_ANDROID", () => {
        it("should exist", () => {
            expect(utils.CHROME_ON_ANDROID).toBeBoolean();
        });
    });

    describe("constant CLIPBOARD_API_SUPPORTED", () => {
        it("should exist", () => {
            expect(utils.CLIPBOARD_API_SUPPORTED).toBeBoolean();
        });
    });

    // functions --------------------------------------------------------------

    describe("function addProperty", () => {
        it("should exist", () => {
            expect(utils.addProperty).toBeFunction();
        });
        it("should add a property to all objects in an array", () => {
            const arr = [{}, { p1: 2 }, { p1: 2, p2: 2 }];
            expect(utils.addProperty(arr, "p2", 42)).toBe(arr);
            expect(arr).toEqual([{ p2: 42 }, { p1: 2, p2: 42 }, { p1: 2, p2: 42 }]);
        });
        it("should add a property to all objects in an object", () => {
            const obj = { a: {}, b: { p1: 2 }, c: { p1: 2, p2: 2 } };
            expect(utils.addProperty(obj, "p2", 42)).toBe(obj);
            expect(obj).toEqual({ a: { p2: 42 }, b: { p1: 2, p2: 42 }, c: { p1: 2, p2: 42 } });
        });
    });

    describe("function deleteProperty", () => {
        it("should exist", () => {
            expect(utils.deleteProperty).toBeFunction();
        });
        it("should delete a property from all objects in an array", () => {
            const arr = [{}, { p1: 2 }, { p1: 2, p2: 2 }];
            expect(utils.deleteProperty(arr, "p1")).toBe(arr);
            expect(arr).toEqual([{}, {}, { p2: 2 }]);
        });
        it("should delete a property from all objects in an object", () => {
            const obj = { a: {}, b: { p1: 2 }, c: { p1: 2, p2: 2 } };
            expect(utils.deleteProperty(obj, "p1")).toBe(obj);
            expect(obj).toEqual({ a: {}, b: {}, c: { p2: 2 } });
        });
    });

    // TODO: this must be tested in a real browser, it does not make sense to test a DOM polyfill environment
    describe.skip("function parseAndSanitizeHTML", () => {
        it("should exist", () => {
            expect(utils.parseAndSanitizeHTML).toBeFunction();
        });
        it("should not remove simple href attribute", () => {
            const markup = '<a href="http://www.example.org">test</a>';
            expect(utils.parseAndSanitizeHTML(markup)[0].outerHTML).toBe(markup);
        });
        it("should remove NUL characters", () => {
            const markup = '<a href="http://www.example.org">\x00test\x00</a>';
            expect(utils.parseAndSanitizeHTML(markup)[0].outerHTML).toBe(markup.replace(/\x00/g, ""));
        });

        function checkRemoveAll(markup) {
            const nodes = utils.parseAndSanitizeHTML(markup);
            expect(nodes).toHaveLength(0);
        }

        function checkRemoveAttr(markup, attr) {
            const nodes = utils.parseAndSanitizeHTML(markup);
            expect(nodes).toHaveLength(1);
            expect(nodes[0].attributes).not.toHaveProperty(attr);
        }

        it("should remove insecure event handlers", () => {
            checkRemoveAttr('<a onunload="something" href="http://www.example.org">test</a>', "onunload");
        });
        it("should remove CSS attribute 'position'", () => {
            const nodes = utils.parseAndSanitizeHTML('<a href="http://www.example.org" style="position:absolute;">test</a>');
            expect(nodes).toHaveLength(1);
            expect(nodes[0]).toHaveStyle({ position: "" });
        });
        it("should remove script tag", () => {
            checkRemoveAll('<script src="http://www.example.org"></script>');
            checkRemoveAll('<script/xss src="http://www.example.org"></script>');
            checkRemoveAll("<script src=http://www.example.com/xss.js?< b >");
            checkRemoveAll("<script src=//www.example.com/.j>");
            checkRemoveAll("</script><script>something</script>");
            checkRemoveAll("</title><script>something</script>");

            const nodes1 = utils.parseAndSanitizeHTML('<img """><script>something</script>">');
            expect(nodes1.is("img")).toBeTrue();
            expect(nodes1.is("script")).toBeFalse();

            const nodes2 = utils.parseAndSanitizeHTML("<<script>something//<</script>");
            expect(nodes2.is("script")).toBeFalse();
        });
        it("should remove body tag", () => {
            checkRemoveAll("<body onload!#$%&()*~+-_.,:;?@[/|\\]^`=something>");
            checkRemoveAll('<body background="javascript:something">');
            checkRemoveAll("<body onload=something>");
        });
        it("should remove half open tag", () => {
            checkRemoveAll('<img src="javascript:something"');
        });
        it("should remove tag with double open angle brackets", () => {
            checkRemoveAll("<iframe src=http://www.example.com/scriptlet.html <");
        });
        it("should remove src attribute", () => {
            checkRemoveAttr('<img src="javascript:something;">', "src");
            checkRemoveAttr("<img src=javascript:something>", "src");
            checkRemoveAttr("<img src=JaVaScRiPt:something>", "src");
            checkRemoveAttr("<img src=&#106;&#97;&#118;&#97;&#115;&#99;&#114;&#105;&#112;&#116;&#58;&#97;&#108;&#101;&#114;&#116;&#40;&#39;&#88;&#83;&#83;&#39;&#41;>", "src");
            checkRemoveAttr("<img src=&#x6A&#x61&#x76&#x61&#x73&#x63&#x72&#x69&#x70&#x74&#x3A&#x61&#x6C&#x65&#x72&#x74&#x28&#x27&#x58&#x53&#x53&#x27&#x29>", "src");
            checkRemoveAttr("<img src=&#0000106&#0000097&#0000118&#0000097&#0000115&#0000099&#0000114&#0000105&#0000112&#0000116&#0000058&#0000097&#0000108&#0000101&#0000114&#0000116&#0000040&#0000039&#0000088&#0000083&#0000083&#0000039&#0000041>", "src");
            checkRemoveAttr('<img src="jav&#x09;ascript:something;">', "src");
            checkRemoveAttr('<img src="jav&#x0A;ascript:something;">', "src");
            checkRemoveAttr('<img src="jav&#x0D;ascript:something;">', "src");
            checkRemoveAttr('<img src=" &#14;  javascript:something;">', "src");
            checkRemoveAttr('<input type="image" src="javascript:something">', "src");
            checkRemoveAttr('<img dynsrc="javascript:something">', "src");
            checkRemoveAttr('<img lowsrc="javascript:something">', "src");
            checkRemoveAttr('<img src="vbscript:something">', "src");
        });
        it("should remove insecure event handlers #2", () => {
            checkRemoveAttr('<img src=# onmouseover="something">', "onmouseover");
            checkRemoveAttr('<img src= onmouseover="something">', "onmouseover");
            checkRemoveAttr('<img onmouseover="something">', "onmouseover");
            checkRemoveAttr('<img src=/ onerror="something">', "onerror");
            checkRemoveAttr('<img src=">" onerror="something">', "onerror");
            checkRemoveAttr('<img src=x onerror="&#0000106&#0000097&#0000118&#0000097&#0000115&#0000099&#0000114&#0000105&#0000112&#0000116&#0000058&#0000097&#0000108&#0000101&#0000114&#0000116&#0000040&#0000039&#0000088&#0000083&#0000083&#0000039&#0000041">', "onerror");
            checkRemoveAttr("<svg/onload=something>", "onload");
        });
    });

    const OPT = {
        s: "a",
        se: "",
        i: 1,
        p: 2.5,
        bt: true,
        bf: false,
        n: null,
        u: undefined,
        a: [1],
        ae: [],
        o: { v: 1 },
        oe: {},
        f: $.noop,
        r: /a/
    };

    describe("function getOption", () => {
        it("should exist", () => {
            expect(utils.getOption).toBeFunction();
        });
        it("should return the specified property", () => {
            expect(utils.getOption(OPT, "s")).toBe("a");
            expect(utils.getOption(OPT, "se")).toBe("");
            expect(utils.getOption(OPT, "i")).toBe(1);
            expect(utils.getOption(OPT, "p")).toBe(2.5);
            expect(utils.getOption(OPT, "bt")).toBeTrue();
            expect(utils.getOption(OPT, "bf")).toBeFalse();
            expect(utils.getOption(OPT, "n")).toBeNull();
            expect(utils.getOption(OPT, "u")).toBeUndefined();
            expect(utils.getOption(OPT, "a")).toEqual([1]);
            expect(utils.getOption(OPT, "ae")).toEqual([]);
            expect(utils.getOption(OPT, "o")).toEqual({ v: 1 });
            expect(utils.getOption(OPT, "oe")).toEqual({});
            expect(utils.getOption(OPT, "f")).toBe($.noop);
            expect(utils.getOption(OPT, "r")).toEqual(/a/);
        });
        it("should return undefined for missing properties", () => {
            expect(utils.getOption({}, "x")).toBeUndefined();
            expect(utils.getOption({ X: 1 }, "x")).toBeUndefined();
        });
        it("should return undefined for missing object", () => {
            expect(utils.getOption(null, "x")).toBeUndefined();
            expect(utils.getOption(undefined, "x")).toBeUndefined();
            expect(utils.getOption("a", "x")).toBeUndefined();
            expect(utils.getOption(1, "x")).toBeUndefined();
            expect(utils.getOption(true, "x")).toBeUndefined();
            expect(utils.getOption([1], "x")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(utils.getOption({}, "x", "a")).toBe("a");
            expect(utils.getOption({}, "x", 1)).toBe(1);
            expect(utils.getOption({}, "x", true)).toBeTrue();
            expect(utils.getOption({}, "x", null)).toBeNull();
            expect(utils.getOption({}, "x", [1])).toEqual([1]);
            expect(utils.getOption({}, "x", { v: 1 })).toEqual({ v: 1 });
            expect(utils.getOption({}, "x", $.noop)).toBe($.noop);
            expect(utils.getOption({}, "x", /a/)).toEqual(/a/);
            expect(utils.getOption(null, "x", "a")).toBe("a");
        });
    });

    describe("function getStringOption", () => {
        it("should exist", () => {
            expect(utils.getStringOption).toBeFunction();
        });
        it("should return the specified string property", () => {
            expect(utils.getStringOption(OPT, "s")).toBe("a");
            expect(utils.getStringOption(OPT, "se")).toBe("");
        });
        it("should return undefined for other properties", () => {
            expect(utils.getStringOption(OPT, "i")).toBeUndefined();
            expect(utils.getStringOption(OPT, "p")).toBeUndefined();
            expect(utils.getStringOption(OPT, "bt")).toBeUndefined();
            expect(utils.getStringOption(OPT, "bf")).toBeUndefined();
            expect(utils.getStringOption(OPT, "n")).toBeUndefined();
            expect(utils.getStringOption(OPT, "u")).toBeUndefined();
            expect(utils.getStringOption(OPT, "a")).toBeUndefined();
            expect(utils.getStringOption(OPT, "ae")).toBeUndefined();
            expect(utils.getStringOption(OPT, "o")).toBeUndefined();
            expect(utils.getStringOption(OPT, "oe")).toBeUndefined();
            expect(utils.getStringOption(OPT, "f")).toBeUndefined();
            expect(utils.getStringOption(OPT, "r")).toBeUndefined();
        });
        it("should return undefined for missing object", () => {
            expect(utils.getStringOption(null, "x")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(utils.getStringOption(OPT, "i", "b")).toBe("b");
            expect(utils.getStringOption(OPT, "i", 2.5)).toBe(2.5);
            expect(utils.getStringOption({}, "x", "b")).toBe("b");
            expect(utils.getStringOption({}, "x", 2.5)).toBe(2.5);
            expect(utils.getStringOption(null, "x", "b")).toBe("b");
            expect(utils.getStringOption(null, "x", 2.5)).toBe(2.5);
        });
        it("should return the default value for empty strings", () => {
            expect(utils.getStringOption(OPT, "s", "b", true)).toBe("a");
            expect(utils.getStringOption(OPT, "se", "b", true)).toBe("b");
            expect(utils.getStringOption(OPT, "se", 2.5, true)).toBe(2.5);
        });
    });

    describe("function getBooleanOption", () => {
        it("should exist", () => {
            expect(utils.getBooleanOption).toBeFunction();
        });
        it("should return the specified Boolean property", () => {
            expect(utils.getBooleanOption(OPT, "bt")).toBeTrue();
            expect(utils.getBooleanOption(OPT, "bf")).toBeFalse();
        });
        it("should return undefined for other properties", () => {
            expect(utils.getBooleanOption(OPT, "s")).toBeUndefined();
            expect(utils.getBooleanOption(OPT, "se")).toBeUndefined();
            expect(utils.getBooleanOption(OPT, "i")).toBeUndefined();
            expect(utils.getBooleanOption(OPT, "p")).toBeUndefined();
            expect(utils.getBooleanOption(OPT, "n")).toBeUndefined();
            expect(utils.getBooleanOption(OPT, "u")).toBeUndefined();
            expect(utils.getBooleanOption(OPT, "a")).toBeUndefined();
            expect(utils.getBooleanOption(OPT, "ae")).toBeUndefined();
            expect(utils.getBooleanOption(OPT, "o")).toBeUndefined();
            expect(utils.getBooleanOption(OPT, "oe")).toBeUndefined();
            expect(utils.getBooleanOption(OPT, "f")).toBeUndefined();
            expect(utils.getBooleanOption(OPT, "r")).toBeUndefined();
        });
        it("should return undefined for missing object", () => {
            expect(utils.getBooleanOption(null, "x")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(utils.getBooleanOption(OPT, "i", true)).toBeTrue();
            expect(utils.getBooleanOption(OPT, "i", 2.5)).toBe(2.5);
            expect(utils.getBooleanOption({}, "x", true)).toBeTrue();
            expect(utils.getBooleanOption({}, "x", 2.5)).toBe(2.5);
            expect(utils.getBooleanOption(null, "x", true)).toBeTrue();
            expect(utils.getBooleanOption(null, "x", 2.5)).toBe(2.5);
        });
    });

    describe("function getNumberOption", () => {
        it("should exist", () => {
            expect(utils.getNumberOption).toBeFunction();
        });
        it("should return the specified number property", () => {
            expect(utils.getNumberOption(OPT, "i")).toBe(1);
            expect(utils.getNumberOption(OPT, "p")).toBe(2.5);
        });
        it("should return undefined for other properties", () => {
            expect(utils.getNumberOption(OPT, "s")).toBeUndefined();
            expect(utils.getNumberOption(OPT, "se")).toBeUndefined();
            expect(utils.getNumberOption(OPT, "bt")).toBeUndefined();
            expect(utils.getNumberOption(OPT, "bf")).toBeUndefined();
            expect(utils.getNumberOption(OPT, "n")).toBeUndefined();
            expect(utils.getNumberOption(OPT, "u")).toBeUndefined();
            expect(utils.getNumberOption(OPT, "a")).toBeUndefined();
            expect(utils.getNumberOption(OPT, "ae")).toBeUndefined();
            expect(utils.getNumberOption(OPT, "o")).toBeUndefined();
            expect(utils.getNumberOption(OPT, "oe")).toBeUndefined();
            expect(utils.getNumberOption(OPT, "f")).toBeUndefined();
            expect(utils.getNumberOption(OPT, "r")).toBeUndefined();
        });
        it("should return undefined for missing object", () => {
            expect(utils.getNumberOption(null, "x")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(utils.getNumberOption(OPT, "s", 2.5)).toBe(2.5);
            expect(utils.getNumberOption(OPT, "s", true)).toBeTrue();
            expect(utils.getNumberOption({}, "x", 2.5)).toBe(2.5);
            expect(utils.getNumberOption({}, "x", true)).toBeTrue();
            expect(utils.getNumberOption(null, "x", 2.5)).toBe(2.5);
            expect(utils.getNumberOption(null, "x", true)).toBeTrue();
        });
        it("should return the minimum value", () => {
            expect(utils.getNumberOption(OPT, "p", 1, 0)).toBe(2.5);
            expect(utils.getNumberOption(OPT, "p", 1, 0, 5)).toBe(2.5);
            expect(utils.getNumberOption(OPT, "p", 1, 4)).toBe(4);
            expect(utils.getNumberOption(OPT, "p", 1, 4, 5)).toBe(4);
        });
        it("should return the maximum value", () => {
            expect(utils.getNumberOption(OPT, "p", 1, 0, 4)).toBe(2.5);
            expect(utils.getNumberOption(OPT, "p", 1, 0, 1)).toBe(1);
        });
        it("should round to the specified precision", () => {
            expect(utils.getNumberOption(OPT, "p", 1, 0, 4, 1)).toBe(3);
            expect(utils.getNumberOption(OPT, "p", 1, 0, 4, 0.2)).toBe(2.6);
        });
    });

    describe("function getIntegerOption", () => {
        it("should exist", () => {
            expect(utils.getIntegerOption).toBeFunction();
        });
        it("should return the specified integer property", () => {
            expect(utils.getIntegerOption(OPT, "i")).toBe(1);
            expect(utils.getIntegerOption(OPT, "p")).toBe(3);
        });
        it("should return undefined for other properties", () => {
            expect(utils.getIntegerOption(OPT, "s")).toBeUndefined();
            expect(utils.getIntegerOption(OPT, "se")).toBeUndefined();
            expect(utils.getIntegerOption(OPT, "bt")).toBeUndefined();
            expect(utils.getIntegerOption(OPT, "bf")).toBeUndefined();
            expect(utils.getIntegerOption(OPT, "n")).toBeUndefined();
            expect(utils.getIntegerOption(OPT, "u")).toBeUndefined();
            expect(utils.getIntegerOption(OPT, "a")).toBeUndefined();
            expect(utils.getIntegerOption(OPT, "ae")).toBeUndefined();
            expect(utils.getIntegerOption(OPT, "o")).toBeUndefined();
            expect(utils.getIntegerOption(OPT, "oe")).toBeUndefined();
            expect(utils.getIntegerOption(OPT, "f")).toBeUndefined();
            expect(utils.getIntegerOption(OPT, "r")).toBeUndefined();
        });
        it("should return undefined for missing object", () => {
            expect(utils.getIntegerOption(null, "x")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(utils.getIntegerOption(OPT, "s", 2.5)).toBe(3);
            expect(utils.getIntegerOption(OPT, "s", true)).toBeTrue();
            expect(utils.getIntegerOption({}, "x", 2.5)).toBe(3);
            expect(utils.getIntegerOption({}, "x", true)).toBeTrue();
            expect(utils.getIntegerOption(null, "x", 2.5)).toBe(3);
            expect(utils.getIntegerOption(null, "x", true)).toBeTrue();
        });
        it("should return the minimum value", () => {
            expect(utils.getIntegerOption(OPT, "p", 1, 0)).toBe(3);
            expect(utils.getIntegerOption(OPT, "p", 1, 0, 5)).toBe(3);
            expect(utils.getIntegerOption(OPT, "p", 1, 4)).toBe(4);
            expect(utils.getIntegerOption(OPT, "p", 1, 4, 5)).toBe(4);
        });
        it("should return the maximum value", () => {
            expect(utils.getIntegerOption(OPT, "p", 1, 0, 4)).toBe(3);
            expect(utils.getIntegerOption(OPT, "p", 1, 0, 1)).toBe(1);
        });
    });

    describe("function getObjectOption", () => {
        it("should exist", () => {
            expect(utils.getObjectOption).toBeFunction();
        });
        it("should return the specified object property", () => {
            expect(utils.getObjectOption(OPT, "o")).toEqual({ v: 1 });
            expect(utils.getObjectOption(OPT, "oe")).toEqual({});
        });
        it("should return undefined for other properties", () => {
            expect(utils.getObjectOption(OPT, "s")).toBeUndefined();
            expect(utils.getObjectOption(OPT, "se")).toBeUndefined();
            expect(utils.getObjectOption(OPT, "i")).toBeUndefined();
            expect(utils.getObjectOption(OPT, "p")).toBeUndefined();
            expect(utils.getObjectOption(OPT, "bt")).toBeUndefined();
            expect(utils.getObjectOption(OPT, "bf")).toBeUndefined();
            expect(utils.getObjectOption(OPT, "n")).toBeUndefined();
            expect(utils.getObjectOption(OPT, "u")).toBeUndefined();
            expect(utils.getObjectOption(OPT, "a")).toBeUndefined();
            expect(utils.getObjectOption(OPT, "ae")).toBeUndefined();
            expect(utils.getObjectOption(OPT, "f")).toBeUndefined();
            expect(utils.getObjectOption(OPT, "r")).toBeUndefined();
        });
        it("should return undefined for missing object", () => {
            expect(utils.getObjectOption(null, "x")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(utils.getObjectOption(OPT, "s", 2.5)).toBe(2.5);
            expect(utils.getObjectOption(OPT, "s", true)).toBeTrue();
            expect(utils.getObjectOption({}, "x", 2.5)).toBe(2.5);
            expect(utils.getObjectOption({}, "x", true)).toBeTrue();
            expect(utils.getObjectOption(null, "x", 2.5)).toBe(2.5);
            expect(utils.getObjectOption(null, "x", true)).toBeTrue();
        });
    });

    describe("function getFunctionOption", () => {
        it("should exist", () => {
            expect(utils.getFunctionOption).toBeFunction();
        });
        it("should return the specified function property", () => {
            expect(utils.getFunctionOption(OPT, "f")).toBe($.noop);
        });
        it("should return undefined for other properties", () => {
            expect(utils.getFunctionOption(OPT, "s")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "se")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "i")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "p")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "bt")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "bf")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "n")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "u")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "a")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "ae")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "o")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "oe")).toBeUndefined();
            expect(utils.getFunctionOption(OPT, "r")).toBeUndefined();
        });
        it("should return undefined for missing object", () => {
            expect(utils.getFunctionOption(null, "x")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(utils.getFunctionOption(OPT, "s", 2.5)).toBe(2.5);
            expect(utils.getFunctionOption(OPT, "s", true)).toBeTrue();
            expect(utils.getFunctionOption({}, "x", 2.5)).toBe(2.5);
            expect(utils.getFunctionOption({}, "x", true)).toBeTrue();
            expect(utils.getFunctionOption(null, "x", 2.5)).toBe(2.5);
            expect(utils.getFunctionOption(null, "x", true)).toBeTrue();
        });
    });

    describe("function getArrayOption", () => {
        it("should exist", () => {
            expect(utils.getArrayOption).toBeFunction();
        });
        it("should return the specified array property", () => {
            expect(utils.getArrayOption(OPT, "a")).toEqual([1]);
            expect(utils.getArrayOption(OPT, "ae")).toEqual([]);
        });
        it("should return undefined for other properties", () => {
            expect(utils.getArrayOption(OPT, "s")).toBeUndefined();
            expect(utils.getArrayOption(OPT, "se")).toBeUndefined();
            expect(utils.getArrayOption(OPT, "i")).toBeUndefined();
            expect(utils.getArrayOption(OPT, "p")).toBeUndefined();
            expect(utils.getArrayOption(OPT, "bt")).toBeUndefined();
            expect(utils.getArrayOption(OPT, "bf")).toBeUndefined();
            expect(utils.getArrayOption(OPT, "n")).toBeUndefined();
            expect(utils.getArrayOption(OPT, "u")).toBeUndefined();
            expect(utils.getArrayOption(OPT, "o")).toBeUndefined();
            expect(utils.getArrayOption(OPT, "oe")).toBeUndefined();
            expect(utils.getArrayOption(OPT, "f")).toBeUndefined();
            expect(utils.getArrayOption(OPT, "r")).toBeUndefined();
        });
        it("should return undefined for missing object", () => {
            expect(utils.getArrayOption(null, "x")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(utils.getArrayOption(OPT, "i", "b")).toBe("b");
            expect(utils.getArrayOption(OPT, "i", 2.5)).toBe(2.5);
            expect(utils.getArrayOption({}, "x", "b")).toBe("b");
            expect(utils.getArrayOption({}, "x", 2.5)).toBe(2.5);
            expect(utils.getArrayOption(null, "x", "b")).toBe("b");
            expect(utils.getArrayOption(null, "x", 2.5)).toBe(2.5);
        });
        it("should return the default value for empty arrays", () => {
            expect(utils.getArrayOption(OPT, "a", [2], true)).toEqual([1]);
            expect(utils.getArrayOption(OPT, "ae", [2], true)).toEqual([2]);
            expect(utils.getArrayOption(OPT, "ae", 2.5, true)).toBe(2.5);
        });
    });

    describe("function extendOptions", () => {

        const O1 = { a: 1, b: "b", c: [1, 2] }, O2 = { d: true }, O3 = { a: 2, e: null };

        it("should exist", () => {
            expect(utils.extendOptions).toBeFunction();
        });
        it("should extend multiple objects", () => {
            expect(utils.extendOptions(O1, O2, O3)).toEqual({ a: 2, b: "b", c: [1, 2], d: true, e: null });
            expect(utils.extendOptions(O3, O2, O1)).toEqual({ a: 1, b: "b", c: [1, 2], d: true, e: null });
        });
        it("should keep the parameters unmodified", () => {
            expect(O1).toEqual({ a: 1, b: "b", c: [1, 2] });
            expect(O2).toEqual({ d: true });
            expect(O3).toEqual({ a: 2, e: null });
        });
        it("should deeply extend embedded objects", () => {
            expect(utils.extendOptions({ a: { b: { c: 1 } } }, { a: { b: { d: 2 }, e: 3 } })).toEqual({ a: { b: { c: 1, d: 2 }, e: 3 } });
        });
        it("should not deeply extend embedded arrays", () => {
            expect(utils.extendOptions({ a: [1] }, { a: { b: 1 } })).toEqual({ a: { b: 1 } });
        });
        it("should accept missing parameters", () => {
            expect(utils.extendOptions(null, { a: 1 }, undefined, { b: 2 })).toEqual({ a: 1, b: 2 });
        });
    });

    describe("selector JQ_TEXTNODE_SELECTOR", () => {
        const span = $("<span>abc</span>");
        it("should exist", () => {
            expect(utils.JQ_TEXTNODE_SELECTOR).toBeFunction();
        });
        it("should match text nodes", () => {
            expect(span.contents().is(utils.JQ_TEXTNODE_SELECTOR)).toBeTrue();
        });
        it("should not match other nodes", () => {
            expect(span.is(utils.JQ_TEXTNODE_SELECTOR)).toBeFalse();
            expect($(document).is(utils.JQ_TEXTNODE_SELECTOR)).toBeFalse();
            expect($(window).is(utils.JQ_TEXTNODE_SELECTOR)).toBeFalse();
        });
    });

    describe("function getDomNode", () => {
        it("should exist", () => {
            expect(utils.getDomNode).toBeFunction();
        });
        const nodes = $("<div></div><div></div>");
        it("should return the passed DOM node", () => {
            expect(utils.getDomNode(nodes[0])).toBe(nodes[0]);
            expect(utils.getDomNode(nodes[1])).toBe(nodes[1]);
        });
        it("should return the DOM node from a jQuery collection", () => {
            expect(utils.getDomNode(nodes)).toBe(nodes[0]);
        });
    });

    describe("function isElementNode", () => {
        it("should exist", () => {
            expect(utils.isElementNode).toBeFunction();
        });
        it("should return false, if no node is given", () => {
            expect(utils.isElementNode(null, ".testSelector")).toBeFalse();
        });
        const span = document.createElement("span");
        const div = document.createElement("div");
        it("should return true for an element", () => {
            expect(utils.isElementNode(span)).toBeTrue();
            expect(utils.isElementNode(div)).toBeTrue();
        });
        it("should filter for div element", () => {
            expect(utils.isElementNode(span, "div")).toBeFalse();
            expect(utils.isElementNode(div, "div")).toBeTrue();
        });
    });

    describe("function getNodeName", () => {
        it("should exist", () => {
            expect(utils.getNodeName).toBeFunction();
        });
        it("should return 'div' for a normal div element", () => {
            const node = document.createElement("div");
            expect(utils.getNodeName(node)).toBe("div");
        });
    });

    describe("function compareNodes", () => {
        it("should exist", () => {
            expect(utils.compareNodes).toBeFunction();
        });
        it("should return a negative number, if node1 precedes or contains node2", () => {
            const node2 = $("<div>"),
                node1 = $("<div>").append(node2);
            expect(utils.compareNodes(node1, node2)).toBe(-1);
        });
        it("should return a positive number, if node2 precedes or contains node1", () => {
            const node1 = $("<div>"),
                node2 = $("<div>").append(node1);
            expect(utils.compareNodes(node1, node2)).toBe(1);
        });
        it("should return a zero, if the nodes are equal", () => {
            const node1 = $("<div>");
            expect(utils.compareNodes(node1, node1)).toBe(0);
        });
    });

    describe("function iterateDescendantNodes", () => {
        it("should exist", () => {
            expect(utils.iterateDescendantNodes).toBeFunction();
        });
        it("should iterate 5 times (childrenOnly = false)", () => {
            const subChild1 = $("<div>"),
                child1 = $("<div>").append(subChild1),
                child2 = $("<div>"),
                child3 = $("<span>"),
                child4 = $("<p>"),
                container = $("<div>").append(child1, child2, child3, child4);
            const spy = jest.fn();

            utils.iterateDescendantNodes(container, spy);

            expect(spy).toHaveBeenCalledTimes(5);
        });
        it("should iterate 4 times (childrenOnly)", () => {
            const subChild1 = $("<div>"),
                child1 = $("<div>").append(subChild1),
                child2 = $("<div>"),
                child3 = $("<span>"),
                child4 = $("<p>"),
                container = $("<div>").append(child1, child2, child3, child4),
                spy = jest.fn();

            utils.iterateDescendantNodes(container, spy, null, { children: true });

            expect(spy).toHaveBeenCalledTimes(4);
        });
        it("should iterate 4 times (reverse)", () => {
            const subChild1 = $("<div>"),
                child1 = $("<div>").append(subChild1),
                child2 = $("<div>"),
                child3 = $("<span>"),
                child4 = $("<p>"),
                container = $("<div>").append(child1, child2, child3, child4);
            const spy = jest.fn();

            utils.iterateDescendantNodes(container, spy, null, { reverse: true });

            expect(spy).toHaveBeenCalledTimes(5);
        });
    });

    describe("function iterateSelectedDescendantNodes", () => {
        it("should exist", () => {
            expect(utils.iterateSelectedDescendantNodes).toBeFunction();
        });
        it("should iterate 3 times for div, 1 time for span (only div/span, childrenOnly = false)", () => {
            const subChild1 = $("<div>"),
                child1 = $("<div>").append(subChild1),
                child2 = $("<div>"),
                child3 = $("<span>"),
                child4 = $("<p>"),
                container = $("<div>").append(child1, child2, child3, child4);

            const spy1 = jest.fn();
            const spy2 = jest.fn();

            utils.iterateSelectedDescendantNodes(container, "div", spy1);
            utils.iterateSelectedDescendantNodes(container, "span", spy2);

            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy2).toHaveBeenCalledTimes(1);
        });
    });

    describe("function findDescendantNode", () => {
        it("should exist", () => {
            expect(utils.findDescendantNode).toBeFunction();
        });
        it("should return the first (child) div", () => {
            const subChild1 = $("<div>"),
                child1 = $("<div>").append(subChild1),
                child2 = $("<div>"),
                child3 = $("<span>"),
                child4 = $("<p>"),
                container = $("<div>").append(child1, child2, child3, child4);

            expect(utils.findDescendantNode(container, "div")).toEqual(_.clone(child1)[0]);
        });
    });

    describe("function getSelectedChildNodeByIndex", () => {
        it("should exist", () => {
            expect(utils.getSelectedChildNodeByIndex).toBeFunction();
        });
        it("should return the second (child) div", () => {
            const subChild1 = $("<div>"),
                child1 = $("<div>").append(subChild1),
                child2 = $("<div>"),
                child3 = $("<span>"),
                child4 = $("<p>"),
                container = $("<div>").append(child1, child2, child3, child4);

            expect(utils.getSelectedChildNodeByIndex(container, "div", 1)).toEqual(_.clone(child2)[0]);
        });
    });

    describe("function findClosest", () => {
        it("should exist", () => {
            expect(utils.findClosest).toBeFunction();
        });
        it("should return the second (child) div", () => {
            const subChild1 = $("<div>"),
                subChild2 = $("<span>"),
                subChild3 = $("<span>"),
                child1 = $("<div>").append(subChild1, subChild2, subChild3),
                child2 = $("<div>"),
                child3 = $("<span>"),
                child4 = $("<p>"),
                container = $("<div>").append(child1, child2, child3, child4);

            expect(utils.findClosest(container, child1, "div")).toEqual(_.clone(child1)[0]);
            expect(utils.findClosest(container, container, "span")).toBeNull();
            expect(utils.findClosest(container, subChild2, "div")).toEqual(_.clone(child1)[0]);
            expect(utils.findClosest(child1, child3, "p")).toBeNull();
        });
    });

    describe("function findFarthest", () => {
        it("should exist", () => {
            expect(utils.findFarthest).toBeFunction();
        });
        it("should return the second (child) div", () => {
            const subChild1 = $("<div>"),
                subChild2 = $("<span>"),
                subChild3 = $("<span>"),
                child1 = $("<div>").append(subChild1, subChild2, subChild3),
                child2 = $("<div>"),
                child3 = $("<span>"),
                child4 = $("<p>"),
                container = $("<div>").append(child1, child2, child3, child4);

            expect(utils.findFarthest(container, child1, "div")).toEqual(_.clone(container)[0]);
            expect(utils.findFarthest(container, container, "span")).toBeNull();
            expect(utils.findFarthest(container, subChild2, "div")).toEqual(_.clone(container)[0]);
            expect(utils.findFarthest(child1, child3, "p")).toBeNull();
        });
    });

    describe("function findPreviousSiblingNode", () => {
        it("should exist", () => {
            expect(utils.findPreviousSiblingNode).toBeFunction();
        });
        it("should return the second (child) div", () => {
            const subChild1 = $("<div>"),
                subChild2 = $("<span>"),
                subChild3 = $("<span>"),
                child1 = $("<div>").append(subChild1, subChild2, subChild3),
                child2 = $("<div>"),
                child3 = $("<span>"),
                child4 = $("<p>"),
                container = $("<div>").append(child1, child2, child3, child4);

            expect(utils.findPreviousSiblingNode(child2, "div")).toEqual(_.clone(child1)[0]);
            expect(utils.findPreviousSiblingNode(subChild3, "span")).toEqual(_.clone(subChild2)[0]);
            expect(utils.findPreviousSiblingNode(subChild3, "div")).toEqual(_.clone(subChild1)[0]);
            expect(utils.findPreviousSiblingNode(child1, "div")).toBeNull();
            expect(utils.findPreviousSiblingNode(container, "div")).toBeNull();
            expect(utils.findPreviousSiblingNode(container)).toBeNull();
        });
    });

    describe("function findNextSiblingNode", () => {
        it("should exist", () => {
            expect(utils.findNextSiblingNode).toBeFunction();
        });
        it("should return the second (child) div", () => {
            const subChild1 = $("<div>"),
                subChild2 = $("<span>"),
                subChild3 = $("<span>"),
                child1 = $("<div>").append(subChild1, subChild2, subChild3),
                child2 = $("<div>"),
                child3 = $("<span>"),
                child4 = $("<p>"),
                container = $("<div>").append(child1, child2, child3, child4);

            expect(utils.findNextSiblingNode(child1, "div")).toEqual(_.clone(child2)[0]);
            expect(utils.findNextSiblingNode(subChild2, "span")).toEqual(_.clone(subChild3)[0]);
            expect(utils.findNextSiblingNode(child1, "p")).toEqual(_.clone(child4)[0]);
            expect(utils.findNextSiblingNode(child2, "div")).toBeNull();
            expect(utils.findNextSiblingNode(container, "div")).toBeNull();
            expect(utils.findNextSiblingNode(container)).toBeNull();
        });
    });

    describe("function findPreviousNode", () => {
        it("should exist", () => {
            expect(utils.findPreviousNode).toBeFunction();
        });
        it("should return the previous node", () => {
            const subChild1 = $("<div>"),
                subChild2 = $("<span>"),
                subChild3 = $("<span>"),
                child1 = $("<div>").append(subChild1, subChild2, subChild3),
                child2 = $("<div>"),
                child3 = $("<span>"),
                child4 = $("<p>"),
                container = $("<div>").append(child1, child2, child3, child4);

            expect(utils.findPreviousNode(container, child2, "div")).toEqual(_.clone(subChild1)[0]);
            expect(utils.findPreviousNode(container, child1, "div")).toBeNull();
            expect(utils.findPreviousNode(container, subChild1, "div")).toEqual(_.clone(child1)[0]);

            expect(utils.findPreviousNode(container, child2, "div", "span")).toEqual(_.clone(subChild1)[0]);
            expect(utils.findPreviousNode(container, child2)).toEqual(_.clone(subChild3)[0]);
        });
    });

    describe("function findNextNode", () => {
        it("should exist", () => {
            expect(utils.findNextNode).toBeFunction();
        });
        it("should return the previous node", () => {
            const subChild1 = $("<div>"),
                subChild2 = $("<span>"),
                subChild3 = $("<span>"),
                subChild4 = $("<p>"),

                child1 = $("<div>").append(subChild1, subChild2, subChild3),
                child2 = $("<div>").append(subChild4),
                child3 = $("<span>"),
                child4 = $("<p>"),

                container = $("<div>").append(child1, child2, child3, child4);

            expect(utils.findNextNode(container, child2, "div")).toBeNull();
            expect(utils.findNextNode(container, child1, "div")).toEqual(_.clone(child2)[0]);
            expect(utils.findNextNode(container, subChild1, "div")).toEqual(_.clone(child2)[0]);

            expect(utils.findNextNode(container, child1, "span", "div")).toEqual(_.clone(child3)[0]);
            expect(utils.findNextNode(container, child1, "p")).toEqual(_.clone(subChild4)[0]);

            expect(utils.findNextNode(container, child1)).toEqual(_.clone(child2)[0]);
        });
    });
});
