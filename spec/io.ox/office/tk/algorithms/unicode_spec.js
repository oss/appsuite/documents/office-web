/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as unicode from "@/io.ox/office/tk/algorithms/unicode";

// tests ======================================================================

describe("module tk/algorithms/unicode", () => {

    // public functions -------------------------------------------------------

    describe("function hasHighSurrogate", () => {
        it("should exist", () => {
            expect(unicode.hasHighSurrogate).toBeFunction();
        });
        it("should check for high surrogates", () => {
            expect(unicode.hasHighSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 0)).toBeFalse();
            expect(unicode.hasHighSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 1)).toBeTrue();
            expect(unicode.hasHighSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 2)).toBeFalse();
            expect(unicode.hasHighSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 3)).toBeTrue();
            expect(unicode.hasHighSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 4)).toBeFalse();
            expect(unicode.hasHighSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 5)).toBeFalse();
            expect(unicode.hasHighSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 6)).toBeFalse();
            expect(unicode.hasHighSurrogate("\0\ud800\udc00\udbff\udfff\uffff", -1)).toBeFalse();
            expect(unicode.hasHighSurrogate("", 0)).toBeFalse();
            expect(unicode.hasHighSurrogate("", 1)).toBeFalse();
            expect(unicode.hasHighSurrogate("", -1)).toBeFalse();
        });
    });

    describe("function hasLowSurrogate", () => {
        it("should exist", () => {
            expect(unicode.hasLowSurrogate).toBeFunction();
        });
        it("should check for low surrogates", () => {
            expect(unicode.hasLowSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 0)).toBeFalse();
            expect(unicode.hasLowSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 1)).toBeFalse();
            expect(unicode.hasLowSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 2)).toBeTrue();
            expect(unicode.hasLowSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 3)).toBeFalse();
            expect(unicode.hasLowSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 4)).toBeTrue();
            expect(unicode.hasLowSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 5)).toBeFalse();
            expect(unicode.hasLowSurrogate("\0\ud800\udc00\udbff\udfff\uffff", 6)).toBeFalse();
            expect(unicode.hasLowSurrogate("\0\ud800\udc00\udbff\udfff\uffff", -1)).toBeFalse();
            expect(unicode.hasLowSurrogate("", 0)).toBeFalse();
            expect(unicode.hasLowSurrogate("", 1)).toBeFalse();
            expect(unicode.hasLowSurrogate("", -1)).toBeFalse();
        });
    });

    describe("function hasSurrogatePair", () => {
        it("should exist", () => {
            expect(unicode.hasSurrogatePair).toBeFunction();
        });
        it("should check for surrogate pairs", () => {
            expect(unicode.hasSurrogatePair("\0\ud83d\udd95\ufffd", 0)).toBeFalse();
            expect(unicode.hasSurrogatePair("\0\ud83d\udd95\ufffd", 1)).toBeTrue();
            expect(unicode.hasSurrogatePair("\0\ud83d\udd95\ufffd", 2)).toBeFalse();
            expect(unicode.hasSurrogatePair("\0\ud83d\udd95\ufffd", 3)).toBeFalse();
            expect(unicode.hasSurrogatePair("\0\ud83d\udd95\ufffd", 4)).toBeFalse();
            expect(unicode.hasSurrogatePair("\0\ud83d\udd95\ufffd", -1)).toBeFalse();
            expect(unicode.hasSurrogatePair("\ud83d\ufffd", 0)).toBeFalse();
            expect(unicode.hasSurrogatePair("\udd95\ufffd", 0)).toBeFalse();
            expect(unicode.hasSurrogatePair("", 0)).toBeFalse();
        });
    });

    describe("function sanitizeOffset", () => {
        it("should exist", () => {
            expect(unicode.sanitizeOffset).toBeFunction();
        });
        it("should return substring without splitting surrogate pairs", () => {
            expect(unicode.sanitizeOffset("ab\uD83D\uDE00c", 0)).toBe(0);
            expect(unicode.sanitizeOffset("ab\uD83D\uDE00c", 1)).toBe(1);
            expect(unicode.sanitizeOffset("ab\uD83D\uDE00c", 2)).toBe(2);
            expect(unicode.sanitizeOffset("ab\uD83D\uDE00c", 3)).toBe(2);
            expect(unicode.sanitizeOffset("ab\uD83D\uDE00c", 4)).toBe(4);
            expect(unicode.sanitizeOffset("ab\uD83D\uDE00c", 5)).toBe(5);
            expect(unicode.sanitizeOffset("ab\uD83D\uDE00c", 6)).toBe(6);
            expect(unicode.sanitizeOffset("", 0)).toBe(0);
            expect(unicode.sanitizeOffset("", 1)).toBe(1);
        });
    });

    describe("function length", () => {
        it("should exist", () => {
            expect(unicode.length).toBeFunction();
        });
        it("should count the Unicode characters", () => {
            expect(unicode.length("\0\uffff")).toBe(2);
            expect(unicode.length("\0\ud83d\udd95\uffff")).toBe(3);
            expect(unicode.length("\0\udd95\ud83d\uffff")).toBe(4);
            expect(unicode.length("\0\ud83d\uffff")).toBe(3);
            expect(unicode.length("\0\udd95\uffff")).toBe(3);
            expect(unicode.length("")).toBe(0);
        });
    });

    describe("function offset", () => {
        it("should exist", () => {
            expect(unicode.offset).toBeFunction();
        });
        it("should return the code unit offset", () => {
            expect(unicode.offset("\0\ud83d\udd95\uffff", 0)).toBe(0);
            expect(unicode.offset("\0\ud83d\udd95\uffff", 1)).toBe(1);
            expect(unicode.offset("\0\ud83d\udd95\uffff", 2)).toBe(3);
            expect(unicode.offset("\0\ud83d\udd95\uffff", 3)).toBeNaN();
            expect(unicode.offset("\0\ud83d\udd95\uffff", 4)).toBeNaN();
            expect(unicode.offset("\0\ud83d\udd95\uffff", -1)).toBe(3);
            expect(unicode.offset("\0\ud83d\udd95\uffff", -2)).toBe(1);
            expect(unicode.offset("\0\ud83d\udd95\uffff", -3)).toBe(0);
            expect(unicode.offset("\0\ud83d\udd95\uffff", -4)).toBeNaN();
            expect(unicode.offset("\0\ud83d\udd95\uffff", -5)).toBeNaN();
            // clamped offset
            expect(unicode.offset("\0\ud83d\udd95\uffff", 3, true)).toBe(4);
            expect(unicode.offset("\0\ud83d\udd95\uffff", 4, true)).toBe(4);
            expect(unicode.offset("\0\ud83d\udd95\uffff", -4, true)).toBe(0);
            expect(unicode.offset("\0\ud83d\udd95\uffff", -5, true)).toBe(0);
        });
        it("should handle orphaned surrogates", () => {
            expect(unicode.offset("\0\udd95\ud83d\uffff", 0)).toBe(0);
            expect(unicode.offset("\0\udd95\ud83d\uffff", 1)).toBe(1);
            expect(unicode.offset("\0\udd95\ud83d\uffff", 2)).toBe(2);
            expect(unicode.offset("\0\udd95\ud83d\uffff", 3)).toBe(3);
            expect(unicode.offset("\0\udd95\ud83d\uffff", 4)).toBeNaN();
            expect(unicode.offset("\0\udd95\ud83d\uffff", 5)).toBeNaN();
            expect(unicode.offset("\0\udd95\ud83d\uffff", -1)).toBe(3);
            expect(unicode.offset("\0\udd95\ud83d\uffff", -2)).toBe(2);
            expect(unicode.offset("\0\udd95\ud83d\uffff", -3)).toBe(1);
            expect(unicode.offset("\0\udd95\ud83d\uffff", -4)).toBe(0);
            expect(unicode.offset("\0\udd95\ud83d\uffff", -5)).toBeNaN();
            expect(unicode.offset("\0\udd95\ud83d\uffff", -6)).toBeNaN();
        });
        it("should handle empty string", () => {
            expect(unicode.offset("", 0)).toBeNaN();
            expect(unicode.offset("", 1)).toBeNaN();
            expect(unicode.offset("", -1)).toBeNaN();
            // clamped offset
            expect(unicode.offset("", 0, true)).toBe(0);
            expect(unicode.offset("", 1, true)).toBe(0);
            expect(unicode.offset("", -1, true)).toBe(0);
        });
    });

    describe("function sliceOffsets", () => {
        it("should exist", () => {
            expect(unicode.sliceOffsets).toBeFunction();
        });
        it("should return the Unicode slice", () => {
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff")).toEqual([0, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 0)).toEqual([0, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 1)).toEqual([1, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 2)).toEqual([3, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 3)).toEqual([4, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 4)).toEqual([4, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 1, 0)).toEqual([1, 1]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 1, 1)).toEqual([1, 1]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 1, 2)).toEqual([1, 3]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 1, 3)).toEqual([1, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 1, 4)).toEqual([1, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 1, 5)).toEqual([1, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 1, -1)).toEqual([1, 3]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 1, -2)).toEqual([1, 1]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 1, -3)).toEqual([1, 1]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", 1, -4)).toEqual([1, 1]);
        });
        it("should return the Unicode slice (negative start)", () => {
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -1)).toEqual([3, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -2)).toEqual([1, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -3)).toEqual([0, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -4)).toEqual([0, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -5)).toEqual([0, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -2, 0)).toEqual([1, 1]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -2, 1)).toEqual([1, 1]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -2, 2)).toEqual([1, 3]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -2, 3)).toEqual([1, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -2, 4)).toEqual([1, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -2, 5)).toEqual([1, 4]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -2, -1)).toEqual([1, 3]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -2, -2)).toEqual([1, 1]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -2, -3)).toEqual([1, 1]);
            expect(unicode.sliceOffsets("\0\ud83d\udd95\uffff", -2, -4)).toEqual([1, 1]);
        });
        it("should handle empty string", () => {
            expect(unicode.sliceOffsets("")).toEqual([0, 0]);
            expect(unicode.sliceOffsets("", 0)).toEqual([0, 0]);
            expect(unicode.sliceOffsets("", 1)).toEqual([0, 0]);
            expect(unicode.sliceOffsets("", -1)).toEqual([0, 0]);
            expect(unicode.sliceOffsets("", 0, 1)).toEqual([0, 0]);
            expect(unicode.sliceOffsets("", 1, -1)).toEqual([0, 0]);
        });
    });

    describe("function charAt", () => {
        it("should exist", () => {
            expect(unicode.charAt).toBeFunction();
        });
        it("should return the Unicode character", () => {
            expect(unicode.charAt("\0\ud83d\udd95\uffff", 0)).toBe("\0");
            expect(unicode.charAt("\0\ud83d\udd95\uffff", 1)).toBe("\ud83d\udd95");
            expect(unicode.charAt("\0\ud83d\udd95\uffff", 2)).toBe("\uffff");
            expect(unicode.charAt("\0\ud83d\udd95\uffff", 3)).toBe("");
            expect(unicode.charAt("\0\ud83d\udd95\uffff", 4)).toBe("");
            expect(unicode.charAt("\0\ud83d\udd95\uffff", -1)).toBe("\uffff");
            expect(unicode.charAt("\0\ud83d\udd95\uffff", -2)).toBe("\ud83d\udd95");
            expect(unicode.charAt("\0\ud83d\udd95\uffff", -3)).toBe("\0");
            expect(unicode.charAt("\0\ud83d\udd95\uffff", -4)).toBe("");
            expect(unicode.charAt("\0\ud83d\udd95\uffff", -5)).toBe("");
        });
        it("should handle empty string", () => {
            expect(unicode.charAt("", 0)).toBe("");
            expect(unicode.charAt("", 1)).toBe("");
            expect(unicode.charAt("", -1)).toBe("");
        });
    });

    describe("function slice", () => {
        it("should exist", () => {
            expect(unicode.slice).toBeFunction();
        });
        it("should return the Unicode slice", () => {
            expect(unicode.slice("\0\ud83d\udd95\uffff")).toBe("\0\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 0)).toBe("\0\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 1)).toBe("\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 2)).toBe("\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 3)).toBe("");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 4)).toBe("");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 1, 0)).toBe("");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 1, 1)).toBe("");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 1, 2)).toBe("\ud83d\udd95");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 1, 3)).toBe("\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 1, 4)).toBe("\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 1, 5)).toBe("\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 1, -1)).toBe("\ud83d\udd95");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 1, -2)).toBe("");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 1, -3)).toBe("");
            expect(unicode.slice("\0\ud83d\udd95\uffff", 1, -4)).toBe("");
        });
        it("should return the Unicode slice (negative start)", () => {
            expect(unicode.slice("\0\ud83d\udd95\uffff", -1)).toBe("\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -2)).toBe("\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -3)).toBe("\0\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -4)).toBe("\0\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -5)).toBe("\0\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -2, 0)).toBe("");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -2, 1)).toBe("");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -2, 2)).toBe("\ud83d\udd95");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -2, 3)).toBe("\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -2, 4)).toBe("\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -2, 5)).toBe("\ud83d\udd95\uffff");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -2, -1)).toBe("\ud83d\udd95");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -2, -2)).toBe("");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -2, -3)).toBe("");
            expect(unicode.slice("\0\ud83d\udd95\uffff", -2, -4)).toBe("");
        });
        it("should handle empty string", () => {
            expect(unicode.slice("")).toBe("");
            expect(unicode.slice("", 0)).toBe("");
            expect(unicode.slice("", 1)).toBe("");
            expect(unicode.slice("", -1)).toBe("");
            expect(unicode.slice("", 0, 1)).toBe("");
            expect(unicode.slice("", 1, -1)).toBe("");
        });
    });
});
