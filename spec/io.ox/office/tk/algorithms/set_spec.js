/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as set from "@/io.ox/office/tk/algorithms/set";

// tests ======================================================================

describe("module tk/algorithms/set", () => {

    // public functions -------------------------------------------------------

    describe("function from", () => {
        it("should exist", () => {
            expect(set.from).toBeFunction();
        });
        it("should collect an array", () => {
            const a1 = ["a", "b", "c"];
            const result = set.from(a1, v => (v === "b") ? undefined : `${v}${v}`);
            expect(result).toBeInstanceOf(Set);
            expect(Array.from(result)).toEqual(["aa", "cc"]);
        });
        it("should collect a set", () => {
            const s1 = new Set(["a", "b", "c"]);
            const result = set.from(s1, v => (v === "b") ? undefined : `${v}${v}`);
            expect(result).toBeInstanceOf(Set);
            expect(result).not.toBe(s1);
            expect(Array.from(result)).toEqual(["aa", "cc"]);
        });
        it("should collect a map", () => {
            const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);
            const result = set.from(m1, ([k, v]) => (v === "b") ? undefined : `${k}${v}`);
            expect(result).toBeInstanceOf(Set);
            expect(Array.from(result)).toEqual(["3a", "1c"]);
        });
        it("should collect a string", () => {
            const result = set.from("abc", v => (v === "b") ? undefined : `${v}${v}`);
            expect(result).toBeInstanceOf(Set);
            expect(Array.from(result)).toEqual(["aa", "cc"]);
        });
        it("should collect an iterator", () => {
            const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);
            const result = set.from(m1.values(), v => (v === "b") ? undefined : `${v}${v}`);
            expect(result).toBeInstanceOf(Set);
            expect(Array.from(result)).toEqual(["aa", "cc"]);
        });
    });

    describe("function filterFrom", () => {
        it("should exist", () => {
            expect(set.filterFrom).toBeFunction();
        });
        it("should collect an array", () => {
            const a1 = ["a", "b", "c"];
            const result = set.filterFrom(a1, v => v !== "b");
            expect(result).toBeInstanceOf(Set);
            expect(Array.from(result)).toEqual(["a", "c"]);
        });
        it("should collect a set", () => {
            const s1 = new Set(["a", "b", "c"]);
            const result = set.filterFrom(s1, v => v !== "b");
            expect(result).toBeInstanceOf(Set);
            expect(result).not.toBe(s1);
            expect(Array.from(result)).toEqual(["a", "c"]);
        });
        it("should collect a map", () => {
            const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);
            const result = set.filterFrom(m1, ([_k, v]) => v !== "b");
            expect(result).toBeInstanceOf(Set);
            expect(Array.from(result)).toEqual([[3, "a"], [1, "c"]]);
        });
        it("should collect a string", () => {
            const result = set.filterFrom("abc", v => v !== "b");
            expect(result).toBeInstanceOf(Set);
            expect(Array.from(result)).toEqual(["a", "c"]);
        });
        it("should collect an iterator", () => {
            const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);
            const result = set.filterFrom(m1.values(), v => v !== "b");
            expect(result).toBeInstanceOf(Set);
            expect(Array.from(result)).toEqual(["a", "c"]);
        });
    });

    describe("function yieldFrom", () => {
        it("should exist", () => {
            expect(set.yieldFrom).toBeFunction();
        });
        it("should collect the results of a generator", () => {
            const spy = jest.fn().mockReturnValue([42, "a"].values());
            const s1 = set.yieldFrom(spy, spy);
            expect(s1).toBeInstanceOf(Set);
            expect(Array.from(s1)).toEqual([42, "a"]);
            expect(spy).toHaveBeenCalledOnce();
            expect(spy).toHaveBeenCalledOn(spy);
        });
    });

    describe("function destroy", () => {
        it("should exist", () => {
            expect(set.destroy).toBeFunction();
        });
        it("should destroy set elements", () => {
            const spy = jest.fn();
            const obj = { destroy: spy };
            const s1 = new Set([obj]);
            set.destroy(s1);
            expect(spy).toHaveBeenCalledOnce();
            expect(s1.size).toBe(0);
        });
    });

    describe("function toggle", () => {
        it("should exist", () => {
            expect(set.toggle).toBeFunction();
        });
        it("should insert an entry", () => {
            const s1 = new Set();
            expect(set.toggle(s1, 1, true)).toBeTrue();
            expect(s1.has(1)).toBeTrue();
            expect(s1.has(2)).toBeFalse();
            expect(set.toggle(s1, 2, true)).toBeTrue();
            expect(s1.has(1)).toBeTrue();
            expect(s1.has(2)).toBeTrue();
            expect(set.toggle(s1, 1, true)).toBeFalse();
            expect(s1.has(1)).toBeTrue();
            expect(s1.has(2)).toBeTrue();
        });
        it("should remove an entry", () => {
            const s2 = new Set([1, 2]);
            expect(set.toggle(s2, 1, false)).toBeTrue();
            expect(s2.has(1)).toBeFalse();
            expect(s2.has(2)).toBeTrue();
            expect(set.toggle(s2, 2, false)).toBeTrue();
            expect(s2.has(1)).toBeFalse();
            expect(s2.has(2)).toBeFalse();
            expect(set.toggle(s2, 1, false)).toBeFalse();
            expect(s2.has(1)).toBeFalse();
            expect(s2.has(2)).toBeFalse();
        });
        it("should toggle an entry", () => {
            const s1 = new Set();
            expect(set.toggle(s1, 1)).toBeTrue();
            expect(s1.has(1)).toBeTrue();
            expect(s1.has(2)).toBeFalse();
            expect(set.toggle(s1, 2)).toBeTrue();
            expect(s1.has(1)).toBeTrue();
            expect(s1.has(2)).toBeTrue();
            expect(set.toggle(s1, 1)).toBeTrue();
            expect(s1.has(1)).toBeFalse();
            expect(s1.has(2)).toBeTrue();
            expect(set.toggle(s1, 2)).toBeTrue();
            expect(s1.has(1)).toBeFalse();
            expect(s1.has(2)).toBeFalse();
        });
        it("should toggle an entry in a weak set", () => {
            const s1 = new WeakSet();
            const o1 = {}, o2 = {};
            expect(set.toggle(s1, o1)).toBeTrue();
            expect(s1.has(o1)).toBeTrue();
            expect(s1.has(o2)).toBeFalse();
            expect(set.toggle(s1, o2)).toBeTrue();
            expect(s1.has(o1)).toBeTrue();
            expect(s1.has(o2)).toBeTrue();
            expect(set.toggle(s1, o1)).toBeTrue();
            expect(s1.has(o1)).toBeFalse();
            expect(s1.has(o2)).toBeTrue();
            expect(set.toggle(s1, o2)).toBeTrue();
            expect(s1.has(o1)).toBeFalse();
            expect(s1.has(o2)).toBeFalse();
        });
    });

    describe("function assign", () => {
        it("should exist", () => {
            expect(set.assign).toBeFunction();
        });
        it("should assign values to a set", () => {
            const s1 = new Set();
            expect(set.assign(s1, new Set([1, 2]), null, new Set([3, 4]).values(), [5, 6], undefined, new Map([["a", 7], ["b", 8]]).values())).toBe(s1);
            expect(Array.from(s1)).toEqual([1, 2, 3, 4, 5, 6, 7, 8]);
        });
    });

    describe("function equals", () => {
        it("should exist", () => {
            expect(set.equals).toBeFunction();
        });
        it("should return true for equal sets", () => {
            const s1 = new Set([1, 2, 3]);
            const s2 = new Set([3, 2, 1]);
            expect(set.equals(s1, s1)).toBeTrue();
            expect(set.equals(s1, s2)).toBeTrue();
        });
        it("should return false for different sets", () => {
            const s1 = new Set([1, 2, 3]);
            const s2 = new Set([1, 2, 4]);
            const s3 = new Set([1, 2]);
            expect(set.equals(s1, s2)).toBeFalse();
            expect(set.equals(s1, s3)).toBeFalse();
            expect(set.equals(s3, s1)).toBeFalse();
        });
        it("should not compare sets deeply", () => {
            expect(set.equals(new Set([{}]), new Set([{}]))).toBeFalse();
        });
    });

    describe("function subset", () => {
        it("should exist", () => {
            expect(set.subset).toBeFunction();
        });
        it("should return true for equal sets", () => {
            const s1 = new Set([1, 2, 3]);
            const s2 = new Set([3, 2, 1]);
            expect(set.subset(s1, s1)).toBeTrue();
            expect(set.subset(s1, s2)).toBeTrue();
            expect(set.subset(s2, s1)).toBeTrue();
        });
        it("should recognize subsets", () => {
            const s1 = new Set([1, 2, 3]);
            const s2 = new Set([1, 4, 2]);
            const s3 = new Set([2, 1]);
            expect(set.subset(s1, s2)).toBeFalse();
            expect(set.subset(s1, s3)).toBeFalse();
            expect(set.subset(s2, s1)).toBeFalse();
            expect(set.subset(s3, s1)).toBeTrue();
            expect(set.subset(s2, s3)).toBeFalse();
            expect(set.subset(s3, s2)).toBeTrue();
            expect(set.subset(new Set(), s1)).toBeTrue();
        });
        it("should not compare sets deeply", () => {
            expect(set.subset(new Set([{}]), new Set([{}]))).toBeFalse();
        });
    });

    describe("function find", () => {
        it("should exist", () => {
            expect(set.find).toBeFunction();
        });
        it("should return matching element", () => {
            const s1 = new Set(["a", "b", "c"]);
            const spy = jest.fn(v => v === "b");
            expect(set.find(s1, spy)).toBe("b");
            expect(spy).toHaveBeenCalledTimes(2);
        });
        it("should return undefined without matching element", () => {
            const s1 = new Set(["a", "b", "c"]);
            const spy = jest.fn(v => v === "d");
            expect(set.find(s1, spy)).toBeUndefined();
            expect(spy).toHaveBeenCalledTimes(3);
        });
    });

    describe("function shiftValues", () => {
        it("should exist", () => {
            expect(set.shiftValues).toBeFunction();
        });
        it("should shift from a set", () => {
            const s1 = new Set([1, 2]);
            const it = set.shiftValues(s1);
            expect(it).toBeIterator();
            expect(Array.from(s1)).toEqual([1, 2]);
            expect(it.next()).toEqual({ done: false, value: 1 });
            expect(Array.from(s1)).toEqual([2]);
            s1.add(3);
            expect(it.next()).toEqual({ done: false, value: 2 });
            expect(Array.from(s1)).toEqual([3]);
            expect(it.next()).toEqual({ done: false, value: 3 });
            expect(Array.from(s1)).toEqual([]);
            expect(it.next()).toEqual({ done: true, value: undefined });
        });
    });
});
