/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as proto from "@/io.ox/office/tk/algorithms/prototype";

// tests ======================================================================

describe("module tk/algorithms/prototype", () => {

    // public functions -------------------------------------------------------

    describe("function hasOwn", () => {
        it("should exist", () => {
            expect(proto.hasOwn).toBeFunction();
        });
        it("should return true for own properties", () => {
            expect(proto.hasOwn([], "length")).toBeTrue();
            expect(proto.hasOwn([], "constructor")).toBeFalse();
        });
    });

    describe("function getOwn", () => {
        it("should exist", () => {
            expect(proto.getOwn).toBeFunction();
        });
        it("should return own properties", () => {
            expect(proto.getOwn([], "length")).toBe(0);
            expect(proto.getOwn([], "constructor")).toBeUndefined();
        });
    });

    describe("function getPrototypeOf", () => {
        it("should exist", () => {
            expect(proto.getPrototypeOf).toBeFunction();
        });
        it("should return class prototype", () => {
            expect(proto.getPrototypeOf([])).toBe(Array.prototype);
            expect(proto.getPrototypeOf(Array.prototype)).toBe(Object.prototype);
        });
    });
});
