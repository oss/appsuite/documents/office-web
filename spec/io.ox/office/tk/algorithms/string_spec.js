/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as str from "@/io.ox/office/tk/algorithms/string";

// tests ======================================================================

describe("module tk/algorithms/string", () => {

    // constants --------------------------------------------------------------

    describe("constant ELLIPSIS_CHAR", () => {
        it("should exist", () => {
            expect(str.ELLIPSIS_CHAR).toBe("\u2026");
        });
    });

    describe("constant RECTANGLE_CHAR", () => {
        it("should exist", () => {
            expect(str.RECTANGLE_CHAR).toBe("\u25af");
        });
    });

    // public functions -------------------------------------------------------

    describe("function startsWithICC", () => {
        it("should exist", () => {
            expect(str.startsWithICC).toBeFunction();
        });
        it("should return whether the string starts with a prefix", () => {
            expect(str.startsWithICC("abcd", "a")).toBeTrue();
            expect(str.startsWithICC("abcd", "ab")).toBeTrue();
            expect(str.startsWithICC("abcd", "abc")).toBeTrue();
            expect(str.startsWithICC("abcd", "abcd")).toBeTrue();
            expect(str.startsWithICC("abcd", "b")).toBeFalse();
            expect(str.startsWithICC("abcd", "A")).toBeTrue();
            expect(str.startsWithICC("ABCD", "ab")).toBeTrue();
        });
        it("should accept empty strings", () => {
            expect(str.startsWithICC("x", "")).toBeTrue();
            expect(str.startsWithICC("", "x")).toBeFalse();
            expect(str.startsWithICC("", "")).toBeTrue();
        });
    });

    describe("function str.endsWithICC", () => {
        it("should exist", () => {
            expect(str.endsWithICC).toBeFunction();
        });
        it("should return whether the string ends with a suffix", () => {
            expect(str.endsWithICC("abcd", "d")).toBeTrue();
            expect(str.endsWithICC("abcd", "cd")).toBeTrue();
            expect(str.endsWithICC("abcd", "bcd")).toBeTrue();
            expect(str.endsWithICC("abcd", "abcd")).toBeTrue();
            expect(str.endsWithICC("abcd", "c")).toBeFalse();
            expect(str.endsWithICC("abcd", "D")).toBeTrue();
            expect(str.endsWithICC("ABCD", "cd")).toBeTrue();
        });
        it("should accept empty strings", () => {
            expect(str.endsWithICC("x", "")).toBeTrue();
            expect(str.endsWithICC("", "x")).toBeFalse();
            expect(str.endsWithICC("", "")).toBeTrue();
        });
    });

    describe("function compareICC", () => {
        it("should exist", () => {
            expect(str.compareICC).toBeFunction();
        });
        it("should compare two strings", () => {
            expect(str.compareICC("a", "b")).toBe(-1);
            expect(str.compareICC("a", "ab")).toBe(-1);
            expect(str.compareICC("aa", "ab")).toBe(-1);
            expect(str.compareICC("a", "B")).toBe(-1);
            expect(str.compareICC("", "a")).toBe(-1);
            expect(str.compareICC("b", "a")).toBe(1);
            expect(str.compareICC("ab", "a")).toBe(1);
            expect(str.compareICC("ab", "aa")).toBe(1);
            expect(str.compareICC("B", "a")).toBe(1);
            expect(str.compareICC("a", "")).toBe(1);
            expect(str.compareICC("A", "a")).toBe(0);
            expect(str.compareICC("a", "A")).toBe(0);
            expect(str.compareICC("a", "a")).toBe(0);
            expect(str.compareICC("", "")).toBe(0);
        });
    });

    describe("function equalsICC", () => {
        it("should exist", () => {
            expect(str.equalsICC).toBeFunction();
        });
        it("should compare two strings", () => {
            expect(str.equalsICC("a", "b")).toBeFalse();
            expect(str.equalsICC("a", "ab")).toBeFalse();
            expect(str.equalsICC("aa", "ab")).toBeFalse();
            expect(str.equalsICC("a", "B")).toBeFalse();
            expect(str.equalsICC("", "a")).toBeFalse();
            expect(str.equalsICC("b", "a")).toBeFalse();
            expect(str.equalsICC("ab", "a")).toBeFalse();
            expect(str.equalsICC("ab", "aa")).toBeFalse();
            expect(str.equalsICC("B", "a")).toBeFalse();
            expect(str.equalsICC("a", "")).toBeFalse();
            expect(str.equalsICC("A", "a")).toBeTrue();
            expect(str.equalsICC("a", "A")).toBeTrue();
            expect(str.equalsICC("a", "a")).toBeTrue();
            expect(str.equalsICC("", "")).toBeTrue();
        });
    });

    describe("function insertSubStr", () => {
        it("should exist", () => {
            expect(str.insertSubStr).toBeFunction();
        });
        it("should replace a substring from beginning", () => {
            expect(str.insertSubStr("abc", 0, "def")).toBe("defabc");
            expect(str.insertSubStr("abc", 1, "def")).toBe("adefbc");
            expect(str.insertSubStr("abc", 2, "def")).toBe("abdefc");
            expect(str.insertSubStr("abc", 3, "def")).toBe("abcdef");
            expect(str.insertSubStr("abc", 4, "def")).toBe("abcdef");
        });
        it("should replace a substring from end", () => {
            expect(str.insertSubStr("abc", -1, "def")).toBe("abdefc");
            expect(str.insertSubStr("abc", -2, "def")).toBe("adefbc");
            expect(str.insertSubStr("abc", -3, "def")).toBe("defabc");
            expect(str.insertSubStr("abc", -4, "def")).toBe("defabc");
        });
    });

    describe("function replaceSubStr", () => {
        it("should exist", () => {
            expect(str.replaceSubStr).toBeFunction();
        });
        it("should replace a substring", () => {
            expect(str.replaceSubStr("abcdef", 0, 1, "ghi")).toBe("ghibcdef");
            expect(str.replaceSubStr("abcdef", 1, 5, "ghi")).toBe("aghif");
            expect(str.replaceSubStr("abcdef", 5, 6, "ghi")).toBe("abcdeghi");
            expect(str.replaceSubStr("abcdef", 5, 9, "ghi")).toBe("abcdeghi");
            expect(str.replaceSubStr("abcdef", 8, 9, "ghi")).toBe("abcdefghi");
            expect(str.replaceSubStr("abcdef", 3, 3, "ghi")).toBe("abcghidef");
        });
    });

    describe("function trimAll", () => {
        it("should exist", () => {
            expect(str.trimAll).toBeFunction();
        });
        it("should trim whitespace at the beginning", () => {
            expect(str.trimAll("   abcd")).toBe("abcd");
            expect(str.trimAll("\xa0 \xa0abcd")).toBe("abcd");
            expect(str.trimAll("\u2028 \u2029abcd")).toBe("abcd");
            expect(str.trimAll("\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0fabcd")).toBe("abcd");
            expect(str.trimAll("\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1fabcd")).toBe("abcd");
            expect(str.trimAll("\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8a\x8b\x8c\x8d\x8e\x8fabcd")).toBe("abcd");
            expect(str.trimAll("\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9a\x9b\x9c\x9d\x9e\x9fabcd")).toBe("abcd");
        });
        it("should trim whitespace at the end", () => {
            expect(str.trimAll("abcd   ")).toBe("abcd");
            expect(str.trimAll("abcd\xa0 \xa0")).toBe("abcd");
            expect(str.trimAll("abcd\u2028 \u2029")).toBe("abcd");
            expect(str.trimAll("abcd\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f")).toBe("abcd");
            expect(str.trimAll("abcd\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f")).toBe("abcd");
            expect(str.trimAll("abcd\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8a\x8b\x8c\x8d\x8e\x8f")).toBe("abcd");
            expect(str.trimAll("abcd\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f")).toBe("abcd");
        });
        it("should trim whitespace at both sides", () => {
            expect(str.trimAll("   abcd   ")).toBe("abcd");
            expect(str.trimAll("\xa0 \xa0abcd\xa0 \xa0")).toBe("abcd");
            expect(str.trimAll("\u2028 \u2029abcd\u2028 \u2029")).toBe("abcd");
        });
        it("should not trim whitespace in the middle", () => {
            expect(str.trimAll("ab  cd")).toBe("ab  cd");
        });
    });

    describe("function cleanNPC", () => {
        it("should exist", () => {
            expect(str.cleanNPC).toBeFunction();
        });
        it("should replace all kinds of non-printable characters with whitespace", () => {
            expect(str.cleanNPC("a\x00b\x01c\x02d\x03e\x04f\x05g\x06h\x07i\x08j\x09k\x0al\x0bm\x0cn\x0do\x0ep\x0fq")).toBe("a b c d e f g h i j k l m n o p q");
            expect(str.cleanNPC("a\x10b\x11c\x12d\x13e\x14f\x15g\x16h\x17i\x18j\x19k\x1al\x1bm\x1cn\x1do\x1ep\x1fq")).toBe("a b c d e f g h i j k l m n o p q");
            expect(str.cleanNPC("a\x80b\x81c\x82d\x83e\x84f\x85g\x86h\x87i\x88j\x89k\x8al\x8bm\x8cn\x8do\x8ep\x8fq")).toBe("a b c d e f g h i j k l m n o p q");
            expect(str.cleanNPC("a\x90b\x91c\x92d\x93e\x94f\x95g\x96h\x97i\x98j\x99k\x9al\x9bm\x9cn\x9do\x9ep\x9fq")).toBe("a b c d e f g h i j k l m n o p q");
        });
        it("should retain character count", () => {
            expect(str.cleanNPC(" \t\n ")).toBe("    ");
        });
        it("should use custom replacement character", () => {
            expect(str.cleanNPC(" \t\n ", "x")).toBe(" xx ");
        });
    });

    describe("function trimAndCleanNPC", () => {
        it("should exist", () => {
            expect(str.trimAndCleanNPC).toBeFunction();
        });
        it("should trim and clean the string", () => {
            expect(str.trimAndCleanNPC("\xa0  a\x00b\x01c\x02d\x03e\x04f\x05g\x06h\x07i\x08j\x09k\x0al\x0bm\x0cn\x0do\x0ep\x0fq  \xa0")).toBe("a b c d e f g h i j k l m n o p q");
            expect(str.trimAndCleanNPC("  a \t\n b  ")).toBe("a    b");
        });
        it("should use custom replacement character", () => {
            expect(str.trimAndCleanNPC("  a \t\n b  ", "x")).toBe("a xx b");
        });
    });

    describe("function splitTokens", () => {
        it("should exist", () => {
            expect(str.splitTokens).toBeFunction();
        });
        it("should split tokens", () => {
            expect(str.splitTokens("a b\tc\n \nd")).toEqual(["a", "b", "c", "d"]);
            expect(str.splitTokens(" a \n \n")).toEqual(["a"]);
            expect(str.splitTokens("")).toEqual([]);
            expect(str.splitTokens(" \n")).toEqual([]);
        });
        it("should limit token count", () => {
            expect(str.splitTokens("a b c d", 2)).toEqual(["a", "b"]);
            expect(str.splitTokens("a", 2)).toEqual(["a"]);
            expect(str.splitTokens("", 2)).toEqual([]);
        });
    });

    describe("function concatTokens", () => {
        it("should exist", () => {
            expect(str.concatTokens).toBeFunction();
        });
        it("should concatenate string tokens", () => {
            expect(str.concatTokens("a", "b")).toBe("a b");
            expect(str.concatTokens("a", "b", "c")).toBe("a b c");
            expect(str.concatTokens("a b", "c")).toBe("a b c");
            expect(str.concatTokens("a", "b c")).toBe("a b c");
            expect(str.concatTokens("a b", "c d")).toBe("a b c d");
        });
        it("should accept nully values", () => {
            expect(str.concatTokens("a", "", "b")).toBe("a b");
            expect(str.concatTokens("a", null, "b")).toBe("a b");
            expect(str.concatTokens("a", undefined, "b")).toBe("a b");
            expect(str.concatTokens("", "a", "b")).toBe("a b");
            expect(str.concatTokens(null, "a", "b")).toBe("a b");
            expect(str.concatTokens(undefined, "a", "b")).toBe("a b");
            expect(str.concatTokens("")).toBe("");
            expect(str.concatTokens()).toBe("");
            expect(str.concatTokens(null)).toBe("");
        });
    });

    describe("function joinTail", () => {
        it("should exist", () => {
            expect(str.joinTail).toBeFunction();
        });
        it("should join strings", () => {
            expect(str.joinTail(["a", "", "b"], ",")).toBe("a,,b,");
            expect(str.joinTail(["a", "b"], ",")).toBe("a,b,");
            expect(str.joinTail(["a", ""], ",")).toBe("a,,");
            expect(str.joinTail(["", "a"], ",")).toBe(",a,");
            expect(str.joinTail(["a"], ",")).toBe("a,");
            expect(str.joinTail([""], ",")).toBe(",");
            expect(str.joinTail([], ",")).toBe("");
        });
    });

    describe("function capitalizeFirst", () => {
        it("should exist", () => {
            expect(str.capitalizeFirst).toBeFunction();
        });
        it("should capitalize the first letter of the string", () => {
            expect(str.capitalizeFirst("abc def ghi jkl")).toBe("Abc def ghi jkl");
            expect(str.capitalizeFirst("Abc def ghi \n jkl")).toBe("Abc def ghi \n jkl");
            expect(str.capitalizeFirst("")).toBe("");
        });
    });

    describe("function capitalizeWords", () => {
        it("should exist", () => {
            expect(str.capitalizeWords).toBeFunction();
        });
        it("should capitalize the first letter of the string", () => {
            expect(str.capitalizeWords("abc def ghi \n jkl")).toBe("Abc Def Ghi \n Jkl");
            expect(str.capitalizeWords("Abc def ghi \n jkl")).toBe("Abc Def Ghi \n Jkl");
        });
    });

    describe("function noI18n", () => {
        it("should exist", () => {
            expect(str.noI18n).toBeFunction();
        });
        it("should mark the passed strings", () => {
            expect(str.noI18n("abc")).toBe("abc");
            expect(str.noI18n("")).toBe("");
            expect(str.noI18n(undefined)).toBeUndefined();
            expect(str.noI18n(null)).toBeNull();
        });
    });
});
