/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as math from "@/io.ox/office/tk/algorithms/math";

// tests ======================================================================

describe("module tk/algorithms/math", () => {

    // constants --------------------------------------------------------------

    describe("constant MIN_NORM_NUMBER", () => {
        it("should exist", () => {
            expect(math.MIN_NORM_NUMBER).toBeNumber();
            expect(math.MIN_NORM_NUMBER).toBeGreaterThan(0);
            expect(math.MIN_NORM_NUMBER).toBeLessThan(Number.EPSILON);
        });
    });

    // public functions -------------------------------------------------------

    describe("function sgn", () => {
        it("should exist", () => {
            expect(math.sgn).toBeFunction();
        });
        it("should return 0 for zero", () => {
            expect(math.sgn(0)).toBe(0);
        });
        it("should return 1 for positive numbers", () => {
            expect(math.sgn(1)).toBe(1);
            expect(math.sgn(2)).toBe(1);
            expect(math.sgn(Number.MIN_VALUE)).toBe(1);
            expect(math.sgn(Number.MAX_VALUE)).toBe(1);
            expect(math.sgn(Infinity)).toBe(1);
        });
        it("should return -1 for negative numbers", () => {
            expect(math.sgn(-1)).toBe(-1);
            expect(math.sgn(-2)).toBe(-1);
            expect(math.sgn(-Number.MIN_VALUE)).toBe(-1);
            expect(math.sgn(-Number.MAX_VALUE)).toBe(-1);
            expect(math.sgn(-Infinity)).toBe(-1);
        });
    });

    describe("function inflate", () => {
        it("should exist", () => {
            expect(math.inflate).toBeFunction();
        });
        it("should round up positive numbers", () => {
            expect(math.inflate(1.1)).toBe(2);
            expect(math.inflate(5.5)).toBe(6);
            expect(math.inflate(9.9)).toBe(10);
        });
        it("should round down negative numbers", () => {
            expect(math.inflate(-1.1)).toBe(-2);
            expect(math.inflate(-5.5)).toBe(-6);
            expect(math.inflate(-9.9)).toBe(-10);
        });
        it("should return integers unmodified", () => {
            expect(math.inflate(-1)).toBe(-1);
            expect(math.inflate(0)).toBe(0);
            expect(math.inflate(1)).toBe(1);
        });
    });

    describe("function clamp", () => {
        it("should exist", () => {
            expect(math.clamp).toBeFunction();
        });
        it("should return the interval minimum for small values", () => {
            expect(math.clamp(1, 3, 5)).toBe(3);
            expect(math.clamp(2, 3, 5)).toBe(3);
        });
        it("should return the passed value if it is inside the interval", () => {
            expect(math.clamp(3, 3, 5)).toBe(3);
            expect(math.clamp(4, 3, 5)).toBe(4);
            expect(math.clamp(5, 3, 5)).toBe(5);
        });
        it("should return the interval maximum for large values", () => {
            expect(math.clamp(6, 3, 5)).toBe(5);
            expect(math.clamp(7, 3, 5)).toBe(5);
        });
    });

    describe("function modulo", () => {
        it("should exist", () => {
            expect(math.modulo).toBeFunction();
        });
        it("should return modulo for zero", () => {
            expect(math.modulo(0, 5)).toBe(0);
        });
        it("should return modulo for positive values", () => {
            expect(math.modulo(2, 5)).toBe(2);
            expect(math.modulo(5, 5)).toBe(0);
            expect(math.modulo(7, 5)).toBe(2);
            expect(math.modulo(10, 5)).toBe(0);
        });
        it("should return modulo for negative values", () => {
            expect(math.modulo(-2, 5)).toBe(3);
            expect(math.modulo(-5, 5)).toBe(0);
            expect(math.modulo(-7, 5)).toBe(3);
            expect(math.modulo(-10, 5)).toBe(0);
        });
    });

    describe("function radius", () => {
        it("should exist", () => {
            expect(math.radius).toBeFunction();
        });
        it("should return the radius", () => {
            expect(math.radius(3, 4)).toBe(5);
            expect(math.radius(3, -4)).toBe(5);
            expect(math.radius(-3, 4)).toBe(5);
            expect(math.radius(-3, -4)).toBe(5);
            expect(math.radius(3, 0)).toBe(3);
            expect(math.radius(0, 4)).toBe(4);
            expect(math.radius(0, 0)).toBe(0);
        });
    });

    describe("function roundp", () => {
        it("should exist", () => {
            expect(math.roundp).toBeFunction();
        });
        it("should round positive numbers to integer precision", () => {
            expect(math.roundp(2, 1)).toBe(2);
            expect(math.roundp(2.1, 1)).toBe(2);
            expect(math.roundp(2.2, 1)).toBe(2);
            expect(math.roundp(2.3, 1)).toBe(2);
            expect(math.roundp(2.4, 1)).toBe(2);
            expect(math.roundp(2.5, 1)).toBe(3);
            expect(math.roundp(2.6, 1)).toBe(3);
            expect(math.roundp(2.7, 1)).toBe(3);
            expect(math.roundp(2.8, 1)).toBe(3);
            expect(math.roundp(2.9, 1)).toBe(3);
            expect(math.roundp(3, 1)).toBe(3);
        });
        it("should round negative numbers to integer precision", () => {
            expect(math.roundp(-2, 1)).toBe(-2);
            expect(math.roundp(-2.1, 1)).toBe(-2);
            expect(math.roundp(-2.2, 1)).toBe(-2);
            expect(math.roundp(-2.3, 1)).toBe(-2);
            expect(math.roundp(-2.4, 1)).toBe(-2);
            expect(math.roundp(-2.5, 1)).toBe(-2);
            expect(math.roundp(-2.6, 1)).toBe(-3);
            expect(math.roundp(-2.7, 1)).toBe(-3);
            expect(math.roundp(-2.8, 1)).toBe(-3);
            expect(math.roundp(-2.9, 1)).toBe(-3);
            expect(math.roundp(-3, 1)).toBe(-3);
        });
        it("should round numbers near 0 to integer precision", () => {
            expect(math.roundp(-0.9, 1)).toBe(-1);
            expect(math.roundp(-0.500001, 1)).toBe(-1);
            expect(math.roundp(-0.5, 1)).toBe(-0);
            expect(math.roundp(-0.1, 1)).toBe(-0);
            expect(math.roundp(0, 1)).toBe(0);
            expect(math.roundp(0.1, 1)).toBe(0);
            expect(math.roundp(0.499999, 1)).toBe(0);
            expect(math.roundp(0.5, 1)).toBe(1);
            expect(math.roundp(0.9, 1)).toBe(1);
        });
        it("should round positive numbers to custom precision", () => {
            expect(math.roundp(227, 10)).toBe(230);
            expect(math.roundp(227, 20)).toBe(220);
            expect(math.roundp(227, 50)).toBe(250);
            expect(math.roundp(227, 100)).toBe(200);
            expect(math.roundp(227, 200)).toBe(200);
            expect(math.roundp(227, 500)).toBe(0);
            expect(math.roundp(227, 0.1)).toBe(227);
            expect(math.roundp(2.27, 0.01)).toBe(2.27);
            expect(math.roundp(2.27, 0.02)).toBe(2.28);
            expect(math.roundp(2.27, 0.05)).toBe(2.25);
            expect(math.roundp(2.27, 0.1)).toBe(2.3);
            expect(math.roundp(2.27, 0.2)).toBe(2.2);
            expect(math.roundp(2.27, 0.5)).toBe(2.5);
        });
        it("should round negative numbers to custom precision", () => {
            expect(math.roundp(-227, 10)).toBe(-230);
            expect(math.roundp(-227, 20)).toBe(-220);
            expect(math.roundp(-227, 50)).toBe(-250);
            expect(math.roundp(-227, 100)).toBe(-200);
            expect(math.roundp(-227, 200)).toBe(-200);
            expect(math.roundp(-227, 500)).toBe(-0);
            expect(math.roundp(-227, 0.1)).toBe(-227);
            expect(math.roundp(-2.27, 0.01)).toBe(-2.27);
            expect(math.roundp(-2.27, 0.02)).toBe(-2.26);
            expect(math.roundp(-2.27, 0.05)).toBe(-2.25);
            expect(math.roundp(-2.27, 0.1)).toBe(-2.3);
            expect(math.roundp(-2.27, 0.2)).toBe(-2.2);
            expect(math.roundp(-2.27, 0.5)).toBe(-2.5);
        });
        it("should round numbers near 0 to custom precision", () => {
            expect(math.roundp(-5.000001, 10)).toBe(-10);
            expect(math.roundp(-5, 10)).toBe(-0);
            expect(math.roundp(0, 10)).toBe(0);
            expect(math.roundp(4.999999, 10)).toBe(0);
            expect(math.roundp(5, 10)).toBe(10);
            expect(math.roundp(-0.05000001, 0.1)).toBe(-0.1);
            expect(math.roundp(-0.05, 0.1)).toBe(-0);
            expect(math.roundp(0, 0.1)).toBe(0);
            expect(math.roundp(0.04999999, 0.1)).toBe(0);
            expect(math.roundp(0.05, 0.1)).toBe(0.1);
        });
    });

    describe("function floorp", () => {
        it("should exist", () => {
            expect(math.floorp).toBeFunction();
        });
        it("should round positive numbers to integer precision", () => {
            expect(math.floorp(2, 1)).toBe(2);
            expect(math.floorp(2.1, 1)).toBe(2);
            expect(math.floorp(2.2, 1)).toBe(2);
            expect(math.floorp(2.3, 1)).toBe(2);
            expect(math.floorp(2.4, 1)).toBe(2);
            expect(math.floorp(2.5, 1)).toBe(2);
            expect(math.floorp(2.6, 1)).toBe(2);
            expect(math.floorp(2.7, 1)).toBe(2);
            expect(math.floorp(2.8, 1)).toBe(2);
            expect(math.floorp(2.9, 1)).toBe(2);
            expect(math.floorp(3, 1)).toBe(3);
        });
        it("should round negative numbers to integer precision", () => {
            expect(math.floorp(-2, 1)).toBe(-2);
            expect(math.floorp(-2.1, 1)).toBe(-3);
            expect(math.floorp(-2.2, 1)).toBe(-3);
            expect(math.floorp(-2.3, 1)).toBe(-3);
            expect(math.floorp(-2.4, 1)).toBe(-3);
            expect(math.floorp(-2.5, 1)).toBe(-3);
            expect(math.floorp(-2.6, 1)).toBe(-3);
            expect(math.floorp(-2.7, 1)).toBe(-3);
            expect(math.floorp(-2.8, 1)).toBe(-3);
            expect(math.floorp(-2.9, 1)).toBe(-3);
            expect(math.floorp(-3, 1)).toBe(-3);
        });
        it("should round numbers near 0 to integer precision", () => {
            expect(math.floorp(-0.9, 1)).toBe(-1);
            expect(math.floorp(-0.5, 1)).toBe(-1);
            expect(math.floorp(-0.1, 1)).toBe(-1);
            expect(math.floorp(0, 1)).toBe(0);
            expect(math.floorp(0.1, 1)).toBe(0);
            expect(math.floorp(0.5, 1)).toBe(0);
            expect(math.floorp(0.9, 1)).toBe(0);
        });
        it("should round positive numbers to custom precision", () => {
            expect(math.floorp(227, 10)).toBe(220);
            expect(math.floorp(227, 20)).toBe(220);
            expect(math.floorp(227, 50)).toBe(200);
            expect(math.floorp(227, 100)).toBe(200);
            expect(math.floorp(227, 200)).toBe(200);
            expect(math.floorp(227, 500)).toBe(0);
            expect(math.floorp(227, 0.1)).toBe(227);
            expect(math.floorp(2.27, 0.01)).toBe(2.27);
            expect(math.floorp(2.27, 0.02)).toBe(2.26);
            expect(math.floorp(2.27, 0.05)).toBe(2.25);
            expect(math.floorp(2.27, 0.1)).toBe(2.2);
            expect(math.floorp(2.27, 0.2)).toBe(2.2);
            expect(math.floorp(2.27, 0.5)).toBe(2);
        });
        it("should round negative numbers to custom precision", () => {
            expect(math.floorp(-227, 10)).toBe(-230);
            expect(math.floorp(-227, 20)).toBe(-240);
            expect(math.floorp(-227, 50)).toBe(-250);
            expect(math.floorp(-227, 100)).toBe(-300);
            expect(math.floorp(-227, 200)).toBe(-400);
            expect(math.floorp(-227, 500)).toBe(-500);
            expect(math.floorp(-227, 0.1)).toBe(-227);
            expect(math.floorp(-2.27, 0.01)).toBe(-2.27);
            expect(math.floorp(-2.27, 0.02)).toBe(-2.28);
            expect(math.floorp(-2.27, 0.05)).toBe(-2.3);
            expect(math.floorp(-2.27, 0.1)).toBe(-2.3);
            expect(math.floorp(-2.27, 0.2)).toBe(-2.4);
            expect(math.floorp(-2.27, 0.5)).toBe(-2.5);
        });
        it("should round numbers near 0 to custom precision", () => {
            expect(math.floorp(-5, 10)).toBe(-10);
            expect(math.floorp(0, 10)).toBe(0);
            expect(math.floorp(5, 10)).toBe(0);
            expect(math.floorp(-0.05, 0.1)).toBe(-0.1);
            expect(math.floorp(0, 0.1)).toBe(0);
            expect(math.floorp(0.05, 0.1)).toBe(0);
        });
    });

    describe("function ceilp", () => {
        it("should exist", () => {
            expect(math.ceilp).toBeFunction();
        });
        it("should round positive numbers to integer precision", () => {
            expect(math.ceilp(2, 1)).toBe(2);
            expect(math.ceilp(2.1, 1)).toBe(3);
            expect(math.ceilp(2.2, 1)).toBe(3);
            expect(math.ceilp(2.3, 1)).toBe(3);
            expect(math.ceilp(2.4, 1)).toBe(3);
            expect(math.ceilp(2.5, 1)).toBe(3);
            expect(math.ceilp(2.6, 1)).toBe(3);
            expect(math.ceilp(2.7, 1)).toBe(3);
            expect(math.ceilp(2.8, 1)).toBe(3);
            expect(math.ceilp(2.9, 1)).toBe(3);
            expect(math.ceilp(3, 1)).toBe(3);
        });
        it("should round negative numbers to integer precision", () => {
            expect(math.ceilp(-2, 1)).toBe(-2);
            expect(math.ceilp(-2.1, 1)).toBe(-2);
            expect(math.ceilp(-2.2, 1)).toBe(-2);
            expect(math.ceilp(-2.3, 1)).toBe(-2);
            expect(math.ceilp(-2.4, 1)).toBe(-2);
            expect(math.ceilp(-2.5, 1)).toBe(-2);
            expect(math.ceilp(-2.6, 1)).toBe(-2);
            expect(math.ceilp(-2.7, 1)).toBe(-2);
            expect(math.ceilp(-2.8, 1)).toBe(-2);
            expect(math.ceilp(-2.9, 1)).toBe(-2);
            expect(math.ceilp(-3, 1)).toBe(-3);
        });
        it("should round numbers near 0 to integer precision", () => {
            expect(math.ceilp(-0.9, 1)).toBe(-0);
            expect(math.ceilp(-0.5, 1)).toBe(-0);
            expect(math.ceilp(-0.1, 1)).toBe(-0);
            expect(math.ceilp(0, 1)).toBe(0);
            expect(math.ceilp(0.1, 1)).toBe(1);
            expect(math.ceilp(0.5, 1)).toBe(1);
            expect(math.ceilp(0.9, 1)).toBe(1);
        });
        it("should round positive numbers to custom precision", () => {
            expect(math.ceilp(227, 10)).toBe(230);
            expect(math.ceilp(227, 20)).toBe(240);
            expect(math.ceilp(227, 50)).toBe(250);
            expect(math.ceilp(227, 100)).toBe(300);
            expect(math.ceilp(227, 200)).toBe(400);
            expect(math.ceilp(227, 500)).toBe(500);
            expect(math.ceilp(227, 0.1)).toBe(227);
            expect(math.ceilp(2.27, 0.01)).toBe(2.27);
            expect(math.ceilp(2.27, 0.02)).toBe(2.28);
            expect(math.ceilp(2.27, 0.05)).toBe(2.3);
            expect(math.ceilp(2.27, 0.1)).toBe(2.3);
            expect(math.ceilp(2.27, 0.2)).toBe(2.4);
            expect(math.ceilp(2.27, 0.5)).toBe(2.5);
        });
        it("should round negative numbers to custom precision", () => {
            expect(math.ceilp(-227, 10)).toBe(-220);
            expect(math.ceilp(-227, 20)).toBe(-220);
            expect(math.ceilp(-227, 50)).toBe(-200);
            expect(math.ceilp(-227, 100)).toBe(-200);
            expect(math.ceilp(-227, 200)).toBe(-200);
            expect(math.ceilp(-227, 500)).toBe(-0);
            expect(math.ceilp(-227, 0.1)).toBe(-227);
            expect(math.ceilp(-2.27, 0.01)).toBe(-2.27);
            expect(math.ceilp(-2.27, 0.02)).toBe(-2.26);
            expect(math.ceilp(-2.27, 0.05)).toBe(-2.25);
            expect(math.ceilp(-2.27, 0.1)).toBe(-2.2);
            expect(math.ceilp(-2.27, 0.2)).toBe(-2.2);
            expect(math.ceilp(-2.27, 0.5)).toBe(-2);
        });
        it("should round numbers near 0 to custom precision", () => {
            expect(math.ceilp(-5, 10)).toBe(-0);
            expect(math.ceilp(0, 10)).toBe(0);
            expect(math.ceilp(5, 10)).toBe(10);
            expect(math.ceilp(-0.05, 0.1)).toBe(-0);
            expect(math.ceilp(0, 0.1)).toBe(0);
            expect(math.ceilp(0.05, 0.1)).toBe(0.1);
        });
    });

    describe("function truncp", () => {
        it("should exist", () => {
            expect(math.truncp).toBeFunction();
        });
        it("should round positive numbers to custom precision", () => {
            expect(math.truncp(227, 10)).toBe(220);
            expect(math.truncp(227, 100)).toBe(200);
        });
        it("should round negative numbers to custom precision", () => {
            expect(math.truncp(-227, 10)).toBe(-220);
            expect(math.truncp(-227, 100)).toBe(-200);
        });
    });

    describe("function inflatep", () => {
        it("should exist", () => {
            expect(math.inflatep).toBeFunction();
        });
        it("should round positive numbers to custom precision", () => {
            expect(math.inflatep(227, 10)).toBe(230);
            expect(math.inflatep(227, 100)).toBe(300);
        });
        it("should round negative numbers to custom precision", () => {
            expect(math.inflatep(-227, 10)).toBe(-230);
            expect(math.inflatep(-227, 100)).toBe(-300);
        });
    });

    describe("function isZero", () => {
        it("should exist", () => {
            expect(math.isZero).toBeFunction();
        });
        it("should return true for zero and denormalized numbers", () => {
            expect(math.isZero(0)).toBeTrue();
            expect(math.isZero(math.MIN_NORM_NUMBER / 2)).toBeTrue();
            expect(math.isZero(-math.MIN_NORM_NUMBER / 2)).toBeTrue();
        });
        it("should return false for non-zero numbers, infinite, and NaN", () => {
            expect(math.isZero(math.MIN_NORM_NUMBER)).toBeFalse();
            expect(math.isZero(-math.MIN_NORM_NUMBER)).toBeFalse();
            expect(math.isZero(1)).toBeFalse();
            expect(math.isZero(-1)).toBeFalse();
            expect(math.isZero(Infinity)).toBeFalse();
            expect(math.isZero(-Infinity)).toBeFalse();
            expect(math.isZero(NaN)).toBeFalse();
        });
    });

    describe("function splitNumber", () => {
        it("should exist", () => {
            expect(math.splitNumber).toBeFunction();
        });
        it("should return mantissa and exponent for positive numbers", () => {
            expect(math.splitNumber(1).mant).toBe(1);
            expect(math.splitNumber(1).expn).toBe(0);
            expect(math.splitNumber(10).mant).toBe(1);
            expect(math.splitNumber(10).expn).toBe(1);
            expect(math.splitNumber(100).mant).toBe(1);
            expect(math.splitNumber(100).expn).toBe(2);
            expect(math.splitNumber(1000).mant).toBe(1);
            expect(math.splitNumber(1000).expn).toBe(3);
            expect(math.splitNumber(1e200).mant).toBeAlmost(1);
            expect(math.splitNumber(1e200).expn).toBe(200);
            expect(math.splitNumber(2.3).mant).toBeAlmost(2.3);
            expect(math.splitNumber(2.3).expn).toBe(0);
            expect(math.splitNumber(23.4).mant).toBeAlmost(2.34);
            expect(math.splitNumber(23.4).expn).toBe(1);
            expect(math.splitNumber(234.5).mant).toBeAlmost(2.345);
            expect(math.splitNumber(234.5).expn).toBe(2);
            expect(math.splitNumber(2345.6).mant).toBeAlmost(2.3456);
            expect(math.splitNumber(2345.6).expn).toBe(3);
            expect(math.splitNumber(0.1).mant).toBe(1);
            expect(math.splitNumber(0.1).expn).toBe(-1);
            expect(math.splitNumber(0.01).mant).toBe(1);
            expect(math.splitNumber(0.01).expn).toBe(-2);
            expect(math.splitNumber(0.001).mant).toBe(1);
            expect(math.splitNumber(0.001).expn).toBe(-3);
            expect(math.splitNumber(1e-200).mant).toBeAlmost(1);
            expect(math.splitNumber(1e-200).expn).toBe(-200);
            expect(math.splitNumber(0.23).mant).toBeAlmost(2.3);
            expect(math.splitNumber(0.23).expn).toBe(-1);
            expect(math.splitNumber(0.0234).mant).toBeAlmost(2.34);
            expect(math.splitNumber(0.0234).expn).toBe(-2);
            expect(math.splitNumber(0.002345).mant).toBeAlmost(2.345);
            expect(math.splitNumber(0.002345).expn).toBe(-3);
            expect(math.splitNumber(0.00023456).mant).toBeAlmost(2.3456);
            expect(math.splitNumber(0.00023456).expn).toBe(-4);
        });
        it("should return mantissa and exponent for negative numbers", () => {
            expect(math.splitNumber(-1).mant).toBe(-1);
            expect(math.splitNumber(-1).expn).toBe(0);
            expect(math.splitNumber(-2345.6).mant).toBeAlmost(-2.3456);
            expect(math.splitNumber(-2345.6).expn).toBe(3);
            expect(math.splitNumber(-0.00023456).mant).toBeAlmost(-2.3456);
            expect(math.splitNumber(-0.00023456).expn).toBe(-4);
        });
        it("should return mantissa and exponent for zero", () => {
            expect(math.splitNumber(0).mant).toBe(0);
            expect(math.splitNumber(0).expn).toBe(-Infinity);
        });
    });

    describe("function randomInt", () => {
        it("should exist", () => {
            expect(math.randomInt).toBeFunction();
        });
        it("should return random integers", () => {
            for (var i = 0; i < 100; i += 1) {
                expect(math.randomInt(1, 9)).toBeInInterval(1, 9);
                expect(math.randomInt(1, 2)).toBeInInterval(1, 2);
                expect(math.randomInt(1000, 1005)).toBeInInterval(1000, 1005);
                expect(math.randomInt(1234, 1239)).toBeInInterval(1234, 1239);
                expect(math.randomInt(1, 1)).toBe(1);
                expect(math.randomInt(5, 5)).toBe(5);
                expect(math.randomInt(-9, -1)).toBeInInterval(-9, -1);
                expect(math.randomInt(-1005, -1000)).toBeInInterval(-1005, -1000);
            }
        });
    });

    describe("function compare", () => {
        it("should exist", () => {
            expect(math.compare).toBeFunction();
        });
        it("should compare two numbers", () => {
            expect(math.compare(0, 2)).toBe(-1);
            expect(math.compare(2, 0)).toBe(1);
            expect(math.compare(0, 0)).toBe(0);
            expect(math.compare(2, 2)).toBe(0);
        });
        it("should compare two strings", () => {
            expect(math.compare("a", "b")).toBe(-1);
            expect(math.compare("a", "ab")).toBe(-1);
            expect(math.compare("aa", "ab")).toBe(-1);
            expect(math.compare("B", "a")).toBe(-1);
            expect(math.compare("A", "a")).toBe(-1);
            expect(math.compare("", "a")).toBe(-1);
            expect(math.compare("b", "a")).toBe(1);
            expect(math.compare("ab", "a")).toBe(1);
            expect(math.compare("ab", "aa")).toBe(1);
            expect(math.compare("a", "B")).toBe(1);
            expect(math.compare("a", "A")).toBe(1);
            expect(math.compare("a", "")).toBe(1);
            expect(math.compare("a", "a")).toBe(0);
            expect(math.compare("", "")).toBe(0);
        });
        it("should compare two booleans", () => {
            expect(math.compare(false, true)).toBe(-1);
            expect(math.compare(true, false)).toBe(1);
            expect(math.compare(false, false)).toBe(0);
            expect(math.compare(true, true)).toBe(0);
        });
    });

    describe("function less", () => {
        it("should exist", () => {
            expect(math.less).toBeFunction();
        });
        it("should compare two numbers", () => {
            expect(math.less(0, 2)).toBeTrue();
            expect(math.less(2, 0)).toBeFalse();
            expect(math.less(0, 0)).toBeFalse();
            expect(math.less(2, 2)).toBeFalse();
        });
        it("should compare two strings", () => {
            expect(math.less("a", "b")).toBeTrue();
            expect(math.less("a", "ab")).toBeTrue();
            expect(math.less("aa", "ab")).toBeTrue();
            expect(math.less("B", "a")).toBeTrue();
            expect(math.less("A", "a")).toBeTrue();
            expect(math.less("", "a")).toBeTrue();
            expect(math.less("b", "a")).toBeFalse();
            expect(math.less("ab", "a")).toBeFalse();
            expect(math.less("ab", "aa")).toBeFalse();
            expect(math.less("a", "B")).toBeFalse();
            expect(math.less("a", "A")).toBeFalse();
            expect(math.less("a", "")).toBeFalse();
            expect(math.less("a", "a")).toBeFalse();
            expect(math.less("", "")).toBeFalse();
        });
        it("should compare two booleans", () => {
            expect(math.less(false, true)).toBeTrue();
            expect(math.less(true, false)).toBeFalse();
            expect(math.less(false, false)).toBeFalse();
            expect(math.less(true, true)).toBeFalse();
        });
    });

    describe("function comparePairs", () => {
        it("should exist", () => {
            expect(math.comparePairs).toBeFunction();
        });
        function compare(n1, n2) { return n1 - n2; }
        function reverse(n1, n2) { return n2 - n1; }
        it("should return 0 for equal pairs", () => {
            expect(math.comparePairs(3, 5, 3, 5)).toBe(0);
            expect(math.comparePairs(3, 5, 3, 5, compare)).toBe(0);
            expect(math.comparePairs(3, 5, 3, 5, reverse)).toBe(0);
        });
        it("should return result for pairs in order", () => {
            expect(math.comparePairs(3, 6, 5, 4)).toBe(-1);
            expect(math.comparePairs(3, 6, 5, 6)).toBe(-1);
            expect(math.comparePairs(3, 6, 5, 8)).toBe(-1);
            expect(math.comparePairs(3, 6, 3, 8)).toBe(-1);
            expect(math.comparePairs(3, 6, 5, 4, compare)).toBe(-1);
            expect(math.comparePairs(3, 6, 5, 6, compare)).toBe(-1);
            expect(math.comparePairs(3, 6, 5, 8, compare)).toBe(-1);
            expect(math.comparePairs(3, 6, 3, 8, compare)).toBe(-1);
            expect(math.comparePairs(3, 6, 5, 4, reverse)).toBe(1);
            expect(math.comparePairs(3, 6, 5, 6, reverse)).toBe(1);
            expect(math.comparePairs(3, 6, 5, 8, reverse)).toBe(1);
            expect(math.comparePairs(3, 6, 3, 8, reverse)).toBe(1);
        });
        it("should return result for pairs in reversed order", () => {
            expect(math.comparePairs(3, 6, 1, 4)).toBe(1);
            expect(math.comparePairs(3, 6, 1, 6)).toBe(1);
            expect(math.comparePairs(3, 6, 1, 8)).toBe(1);
            expect(math.comparePairs(3, 6, 3, 4)).toBe(1);
            expect(math.comparePairs(3, 6, 1, 4, compare)).toBe(1);
            expect(math.comparePairs(3, 6, 1, 6, compare)).toBe(1);
            expect(math.comparePairs(3, 6, 1, 8, compare)).toBe(1);
            expect(math.comparePairs(3, 6, 3, 4, compare)).toBe(1);
            expect(math.comparePairs(3, 6, 1, 4, reverse)).toBe(-1);
            expect(math.comparePairs(3, 6, 1, 6, reverse)).toBe(-1);
            expect(math.comparePairs(3, 6, 1, 8, reverse)).toBe(-1);
            expect(math.comparePairs(3, 6, 3, 4, reverse)).toBe(-1);
        });
    });
});
