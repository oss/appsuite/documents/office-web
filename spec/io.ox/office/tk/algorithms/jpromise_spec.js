/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import * as jpromise from "@/io.ox/office/tk/algorithms/jpromise";

// tests ======================================================================

describe("module tk/algorithms/jpromise", () => {

    // public functions -------------------------------------------------------

    describe("function is", () => {
        it("should exist", () => {
            expect(jpromise.is).toBeFunction();
        });
        it("should return true for JQuery promises", () => {
            expect(jpromise.is($.Deferred())).toBeTrue();
            expect(jpromise.is($.Deferred().resolve())).toBeTrue();
            expect(jpromise.is($.Deferred().reject())).toBeTrue();
            expect(jpromise.is($.Deferred().promise())).toBeTrue();
        });
        it("should return false for other values", () => {
            expect(jpromise.is(null)).toBeFalse();
            expect(jpromise.is(1)).toBeFalse();
            expect(jpromise.is("a")).toBeFalse();
            expect(jpromise.is(true)).toBeFalse();
            expect(jpromise.is(/a/)).toBeFalse();
            expect(jpromise.is([1])).toBeFalse();
            expect(jpromise.is({})).toBeFalse();
            expect(jpromise.is({ promise: null, then: null })).toBeFalse();
            expect(jpromise.is($.Deferred)).toBeFalse();
            expect(jpromise.is(Promise.resolve())).toBeFalse();
        });
    });

    describe("function isPending", () => {
        it("should exist", () => {
            expect(jpromise.isPending).toBeFunction();
        });
        it("should return true for pending promise", () => {
            expect(jpromise.isPending($.Deferred().promise())).toBeTrue();
        });
        it("should return false for settled promise", () => {
            expect(jpromise.isPending($.Deferred().resolve().promise())).toBeFalse();
            expect(jpromise.isPending($.Deferred().reject().promise())).toBeFalse();
        });
    });

    describe("function isSettled", () => {
        it("should exist", () => {
            expect(jpromise.isSettled).toBeFunction();
        });
        it("should return true for pending promise", () => {
            expect(jpromise.isSettled($.Deferred().resolve().promise())).toBeTrue();
            expect(jpromise.isSettled($.Deferred().reject().promise())).toBeTrue();
        });
        it("should return false for settled promise", () => {
            expect(jpromise.isSettled($.Deferred().promise())).toBeFalse();
        });
    });

    describe("function isFulfilled", () => {
        it("should exist", () => {
            expect(jpromise.isFulfilled).toBeFunction();
        });
        it("should return true for fulfilled promise", () => {
            expect(jpromise.isFulfilled($.Deferred().resolve().promise())).toBeTrue();
        });
        it("should return false for unfulfilled promise", () => {
            expect(jpromise.isFulfilled($.Deferred().promise())).toBeFalse();
            expect(jpromise.isFulfilled($.Deferred().reject().promise())).toBeFalse();
        });
    });

    describe("function isRejected", () => {
        it("should exist", () => {
            expect(jpromise.isRejected).toBeFunction();
        });
        it("should return true for rejected promise", () => {
            expect(jpromise.isRejected($.Deferred().reject().promise())).toBeTrue();
        });
        it("should return false for unrejected promise", () => {
            expect(jpromise.isRejected($.Deferred().promise())).toBeFalse();
            expect(jpromise.isRejected($.Deferred().resolve().promise())).toBeFalse();
        });
    });

    describe("function new", () => {
        it("should exist", () => {
            expect(jpromise.new).toBeFunction();
        });
        it("should create a new promise and fulfil it", () => {
            const spy = jest.fn();
            const promise = jpromise.new(spy);
            expect(jpromise.isPending(promise)).toBeTrue();
            expect(spy).toHaveBeenCalledOnce();
            const [fulfilFn, rejectFn] = spy.mock.calls[0];
            expect(fulfilFn).toBeFunction();
            expect(rejectFn).toBeFunction();
            fulfilFn(42);
            expect(jpromise.isFulfilled(promise)).toBeTrue();
            promise.done(value => expect(value).toBe(42));
            rejectFn(1);
            expect(jpromise.isFulfilled(promise)).toBeTrue();
        });
        it("should create a new promise and reject it", () => {
            const spy = jest.fn();
            const promise = jpromise.new(spy);
            expect(jpromise.isPending(promise)).toBeTrue();
            expect(spy).toHaveBeenCalledOnce();
            const [fulfilFn, rejectFn] = spy.mock.calls[0];
            rejectFn(42);
            expect(jpromise.isRejected(promise)).toBeTrue();
            promise.fail(value => expect(value).toBe(42));
            fulfilFn(1);
            expect(jpromise.isRejected(promise)).toBeTrue();
        });
        it("should create a rejected promise from exception", () => {
            const spy = jest.fn().mockImplementation(() => { throw new Error(); });
            const promise = jpromise.new(spy);
            expect(jpromise.isRejected(promise)).toBeTrue();
            expect(spy).toHaveBeenCalledOnce();
            promise.fail(value => expect(value).toBeInstanceOf(Error));
        });
    });

    describe("function deferred", () => {
        it("should exist", () => {
            expect(jpromise.deferred).toBeFunction();
        });
        it("should create a new deferred", () => {
            const deferred = jpromise.deferred();
            expect(jpromise.is(deferred)).toBeTrue();
            expect(jpromise.isPending(deferred)).toBeTrue();
            expect(deferred).toRespondTo("resolve");
            expect(deferred).toRespondTo("reject");
            expect(deferred).toRespondTo("notify");
        });
    });

    describe("function resolve", () => {
        it("should exist", () => {
            expect(jpromise.resolve).toBeFunction();
        });
        it("should create new fulfilled promise", async () => {
            const promise = jpromise.resolve(42);
            expect(jpromise.is(promise)).toBeTrue();
            expect(jpromise.isFulfilled(promise)).toBeTrue();
            await expect(promise).resolves.toBe(42);
        });
        it("should create new fulfilled promise without value", async () => {
            const promise = jpromise.resolve();
            expect(jpromise.isFulfilled(promise)).toBeTrue();
            await expect(promise).resolves.toBeUndefined();
        });
    });

    describe("function reject", () => {
        it("should exist", () => {
            expect(jpromise.reject).toBeFunction();
        });
        it("should create new rejected promise", async () => {
            const err = new Error("");
            const promise = jpromise.reject(err);
            expect(jpromise.is(promise)).toBeTrue();
            expect(jpromise.isRejected(promise)).toBeTrue();
            expect(promise.state()).toBe("rejected");
            await expect(promise).rejects.toThrow(err);
        });
    });

    describe("function from", () => {
        it("should exist", () => {
            expect(jpromise.from).toBeFunction();
        });
        it("should handle pending Promise", async () => {
            const promise1 = new Promise(resolve => { window.setTimeout(() => resolve(42), 2); });
            const promise2 = jpromise.invoke(() => promise1);
            expect(jpromise.is(promise2)).toBeTrue();
            expect(jpromise.isPending(promise2)).toBeTrue();
            await expect(promise2).resolves.toBe(42);
        });
        it("should handle fulfilled Promise", async () => {
            const promise1 = Promise.resolve(42);
            const promise2 = jpromise.invoke(() => promise1);
            expect(jpromise.isPending(promise2)).toBeTrue();
            await expect(promise2).resolves.toBe(42);
        });
        it("should handle rejected Promise", async () => {
            const err = new Error("");
            const promise1 = Promise.reject(err);
            const promise2 = jpromise.invoke(() => promise1);
            promise1.catch(() => {}); // needed to silence Node"s `PromiseRejectionHandledWarning`
            expect(jpromise.isPending(promise2)).toBeTrue();
            await expect(promise2).rejects.toThrow(err);
        });
        it("should handle pending JPromise", async () => {
            const promise1 = jpromise.new(resolve => window.setTimeout(() => resolve(42), 2));
            const promise2 = jpromise.invoke(() => promise1);
            expect(jpromise.isPending(promise2)).toBeTrue();
            expect(promise2).toBe(promise1);
            await expect(promise2).resolves.toBe(42);
        });
        it("should handle fulfilled JPromise", async () => {
            const promise1 = jpromise.resolve(42);
            const promise2 = jpromise.invoke(() => promise1);
            expect(jpromise.isFulfilled(promise2)).toBeTrue();
            expect(promise2).toBe(promise1);
            await expect(promise2).resolves.toBe(42);
        });
        it("should handle rejected JPromise", async () => {
            const err = new Error("");
            const promise1 = jpromise.reject(err);
            const promise2 = jpromise.invoke(() => promise1);
            expect(jpromise.isRejected(promise2)).toBeTrue();
            expect(promise2).toBe(promise1);
            await expect(promise2).rejects.toThrow(err);
        });
        it("should handle synchronous return value", async () => {
            const promise = jpromise.invoke(() => 42);
            expect(jpromise.isFulfilled(promise)).toBeTrue();
            await expect(promise).resolves.toBe(42);
        });
        it("should handle synchronous exception", async () => {
            const err = new Error("");
            const promise = jpromise.invoke(() => { throw err; });
            expect(jpromise.isRejected(promise)).toBeTrue();
            await expect(promise).rejects.toThrow(err);
        });
    });

    describe("function invoke", () => {
        it("should exist", () => {
            expect(jpromise.invoke).toBeFunction();
        });
        it("should handle pending Promise", async () => {
            const promise1 = new Promise(resolve => { window.setTimeout(() => resolve(42), 2); });
            const promise2 = jpromise.invoke(() => promise1);
            expect(jpromise.is(promise2)).toBeTrue();
            expect(jpromise.isPending(promise2)).toBeTrue();
            await expect(promise2).resolves.toBe(42);
        });
        it("should handle fulfilled Promise", async () => {
            const promise1 = Promise.resolve(42);
            const promise2 = jpromise.invoke(() => promise1);
            expect(jpromise.isPending(promise2)).toBeTrue();
            await expect(promise2).resolves.toBe(42);
        });
        it("should handle rejected Promise", async () => {
            const err = new Error("");
            const promise1 = Promise.reject(err);
            const promise2 = jpromise.invoke(() => promise1);
            promise1.catch(() => {}); // needed to silence Node"s `PromiseRejectionHandledWarning`
            expect(jpromise.isPending(promise2)).toBeTrue();
            await expect(promise2).rejects.toThrow(err);
        });
        it("should handle pending JPromise", async () => {
            const promise1 = jpromise.new(resolve => window.setTimeout(() => resolve(42), 2));
            const promise2 = jpromise.invoke(() => promise1);
            expect(jpromise.isPending(promise2)).toBeTrue();
            expect(promise2).toBe(promise1);
            await expect(promise2).resolves.toBe(42);
        });
        it("should handle fulfilled JPromise", async () => {
            const promise1 = jpromise.resolve(42);
            const promise2 = jpromise.invoke(() => promise1);
            expect(jpromise.isFulfilled(promise2)).toBeTrue();
            expect(promise2).toBe(promise1);
            await expect(promise2).resolves.toBe(42);
        });
        it("should handle rejected JPromise", async () => {
            const err = new Error("");
            const promise1 = jpromise.reject(err);
            const promise2 = jpromise.invoke(() => promise1);
            expect(jpromise.isRejected(promise2)).toBeTrue();
            expect(promise2).toBe(promise1);
            await expect(promise2).rejects.toThrow(err);
        });
        it("should handle synchronous return value", async () => {
            const promise = jpromise.invoke(() => 42);
            expect(jpromise.isFulfilled(promise)).toBeTrue();
            await expect(promise).resolves.toBe(42);
        });
        it("should handle synchronous exception", async () => {
            const err = new Error("");
            const promise = jpromise.invoke(() => { throw err; });
            expect(jpromise.isRejected(promise)).toBeTrue();
            await expect(promise).rejects.toThrow(err);
        });
    });

    describe("function floating", () => {
        it("should exist", () => {
            expect(jpromise.floating).toBeFunction();
        });
    });

    describe("function invokeFloating", () => {
        it("should exist", () => {
            expect(jpromise.invokeFloating).toBeFunction();
        });
    });

    describe("function wrapFloating", () => {
        it("should exist", () => {
            expect(jpromise.wrapFloating).toBeFunction();
        });
    });

    describe("function invokeWithFinally", () => {
        it("should exist", () => {
            expect(jpromise.invokeWithFinally).toBeFunction();
        });
        it("should invoke finally handler for synchronous result", () => {
            const spy1 = jest.fn().mockReturnValue(42);
            const spy2 = jest.fn();
            expect(jpromise.invokeWithFinally(spy1, spy2)).toBe(42);
            expect(spy1).toHaveBeenCalledOnce();
            expect(spy2).toHaveBeenCalledOnce();
            expect(spy1).toHaveBeenCalledBefore(spy2);
        });
        it("should invoke finally handler for synchronous exception", () => {
            const spy1 = jest.fn().mockImplementation(() => { throw new Error("42"); });
            const spy2 = jest.fn();
            expect(() => jpromise.invokeWithFinally(spy1, spy2)).toThrow("42");
            expect(spy1).toHaveBeenCalledOnce();
            expect(spy2).toHaveBeenCalledOnce();
            expect(spy1).toHaveBeenCalledBefore(spy2);
        });
        it("should invoke finally handler for asynchronous result", async () => {
            const spy1 = jest.fn().mockImplementation(() => {
                return new Promise(resolve => {
                    window.setTimeout(() => { spy2(); resolve(42); }, 10);
                });
            });
            const spy2 = jest.fn();
            const spy3 = jest.fn();
            await expect(jpromise.invokeWithFinally(spy1, spy3)).resolves.toBe(42);
            expect(spy1).toHaveBeenCalledOnce();
            expect(spy2).toHaveBeenCalledOnce();
            expect(spy3).toHaveBeenCalledOnce();
            expect(spy1).toHaveBeenCalledBefore(spy2);
            expect(spy2).toHaveBeenCalledBefore(spy3);
        });
        it("should invoke finally handler for asynchronous rejection", async () => {
            const spy1 = jest.fn().mockImplementation(() => {
                return new Promise((_resolve, reject) => {
                    window.setTimeout(() => { spy2(); reject(new Error("42")); }, 10);
                });
            });
            const spy2 = jest.fn();
            const spy3 = jest.fn();
            await expect(jpromise.invokeWithFinally(spy1, spy3)).rejects.toThrow("42");
            expect(spy1).toHaveBeenCalledOnce();
            expect(spy2).toHaveBeenCalledOnce();
            expect(spy3).toHaveBeenCalledOnce();
            expect(spy1).toHaveBeenCalledBefore(spy2);
            expect(spy2).toHaveBeenCalledBefore(spy3);
        });
    });

    describe("function fastThen", () => {
        it("should exist", () => {
            expect(jpromise.fastThen).toBeFunction();
        });
        it("should return a fulfilled promise for a value", async () => {
            const promise = jpromise.fastThen(42, v => v + 1);
            expect(jpromise.is(promise)).toBeTrue();
            expect(jpromise.isFulfilled(promise)).toBeTrue();
            await expect(promise).resolves.toBe(43);
        });
        it("should return a fulfilled promise for a fulfilled JQuery promise", async () => {
            const promise = jpromise.fastThen(jpromise.resolve(42), v => v + 1);
            expect(jpromise.is(promise)).toBeTrue();
            expect(jpromise.isFulfilled(promise)).toBeTrue();
            await expect(promise).resolves.toBe(43);
        });
        it("should return a fulfilled promise for a pending JQuery promise", async () => {
            const promise = jpromise.fastThen(jpromise.resolve().then(() => 42), v => v + 1);
            expect(jpromise.is(promise)).toBeTrue();
            expect(jpromise.isPending(promise)).toBeTrue();
            await expect(promise).resolves.toBe(43);
        });
        it("should return a fulfilled promise for a native promise", async () => {
            const promise = jpromise.fastThen(Promise.resolve(42), v => v + 1);
            expect(jpromise.is(promise)).toBeTrue();
            expect(jpromise.isPending(promise)).toBeTrue();
            await expect(promise).resolves.toBe(43);
        });
    });

    describe("function fastCatch", () => {
        it("should exist", () => {
            expect(jpromise.fastCatch).toBeFunction();
        });
    });

    describe("function fastFinally", () => {
        it("should exist", () => {
            expect(jpromise.fastFinally).toBeFunction();
        });
    });

    describe("function fastChain", () => {
        it("should exist", () => {
            expect(jpromise.fastChain).toBeFunction();
        });
    });
});
