/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as hash from "@/io.ox/office/tk/algorithms/hash";

// tests ======================================================================

describe("module tk/algorithms/hash", () => {

    // public functions -------------------------------------------------------

    describe("function sha256", () => {
        it("should exist", () => {
            expect(hash.sha256).toBeFunction();
        });
        it("should return the sha256 hash of empty string (utf-8)", async () => {
            await expect(hash.sha256("", "utf-8")).resolves.toBe("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
        });
        it("should return the sha256 hash of empty string (base64)", async () => {
            await expect(hash.sha256("", "base64")).resolves.toBe("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
        });
        it("should return the sha256 hash of a string (utf-8)", async () => {
            await expect(hash.sha256('test', "utf-8")).resolves.toBe("9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08");
        });
        it("should return the sha256 hash of the base64 encoded string 'test')", async () => {
            await expect(hash.sha256('dGVzdA==', "base64")).resolves.toBe("9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08");
        });
    });
});
