/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { validate, version } from "uuid";

import * as uuid from "@/io.ox/office/tk/algorithms/uuid";

// tests ======================================================================

describe("module tk/algorithms/uuid", () => {

    // public functions -------------------------------------------------------

    describe("function is", () => {
        it("should exist", () => {
            expect(uuid.is).toBeFunction();
        });
        it("should match UUIDs", () => {
            expect(uuid.is("00000000-0000-0000-0000-000000000000")).toBeTrue();
            expect(uuid.is("01234567-89ab-1def-8123-456789abcdef")).toBeTrue();
            expect(uuid.is("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF")).toBeTrue();
        });
        it("should not match other strings", () => {
            expect(uuid.is("01234567-89ab-0def-8123-456789abcdef")).toBeFalse();
            expect(uuid.is("01234567-89ab-adef-8123-456789abcdef")).toBeFalse();
            expect(uuid.is("01234567-89ab-1def-7123-456789abcdef")).toBeFalse();
            expect(uuid.is("01234567-89ab-1def-c123-456789abcdef")).toBeFalse();
            expect(uuid.is("00000000-0000-0000-0000-00000000000")).toBeFalse();
            expect(uuid.is("g0000000-0000-0000-0000-000000000000")).toBeFalse();
            expect(uuid.is("000000000000-0000-0000-000000000000")).toBeFalse();
            expect(uuid.is("{00000000-0000-0000-0000-000000000000}")).toBeFalse();
            expect(uuid.is("")).toBeFalse();
        });
        it("should match UUIDs with curly braces", () => {
            expect(uuid.is("{00000000-0000-0000-0000-000000000000}", true)).toBeTrue();
            expect(uuid.is("00000000-0000-0000-0000-000000000000", true)).toBeFalse();
        });
    });

    describe("function random", () => {
        it("should exist", () => {
            expect(uuid.random).toBeFunction();
        });
        function isv4(id) {
            return validate(id) && (version(id) === 4);
        }
        it("should create a random UUID compliant to RFC 4122", () => {
            const id1 = uuid.random();
            expect(isv4(id1)).toBeTrue();
            const id2 = uuid.random(false);
            expect(isv4(id2)).toBeTrue();
            expect(id1).not.toBe(id2);
        });
        it("should add curly braces", () => {
            const id = uuid.random(true);
            expect(id).toMatch(/^\{.+\}$/);
            expect(isv4(id.slice(1, -1))).toBeTrue();
        });
    });
});
