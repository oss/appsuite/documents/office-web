/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { identity, pluck2nd } from "@/io.ox/office/tk/algorithms/function";
import * as dict from "@/io.ox/office/tk/algorithms/dict";

// tests ======================================================================

describe("module tk/algorithms/dict", () => {

    // public functions -------------------------------------------------------

    describe("function create", () => {
        it("should exist", () => {
            expect(dict.create).toBeFunction();
        });
        it("should create an empty dictionary", () => {
            const d1 = dict.create();
            expect(d1).toEqual({});
            expect(Object.getPrototypeOf(d1)).toBeNull();
            expect("constructor" in d1).toBeFalse();
        });
    });

    describe("function createRec", () => {
        it("should exist", () => {
            expect(dict.createRec).toBeFunction();
        });
        it("should create an empty dictionary", () => {
            const d1 = dict.createRec();
            expect(d1).toEqual({});
            expect(Object.getPrototypeOf(d1)).toBeNull();
            expect("constructor" in d1).toBeFalse();
        });
    });

    describe("function from", () => {
        it("should exist", () => {
            expect(dict.from).toBeFunction();
        });
        it("should convert an arary", () => {
            expect(dict.from([["c", 1], ["b", 2], ["a", 3]])).toEqual({ c: 1, b: 2, a: 3 });
            expect(dict.from([1, 2, 3], v => [`k${v}`, 4 - v])).toEqual({ k1: 3, k2: 2, k3: 1 });
        });
        it("should convert a map", () => {
            const m1 = new Map([["c", 1], ["b", 2], ["a", 3]]);
            expect(dict.from(m1)).toEqual({ c: 1, b: 2, a: 3 });
            const m2 = new Map([[1, "c"], [2, "b"], [3, "a"]]);
            expect(dict.from(m2, ([k, v]) => [v, k])).toEqual({ c: 1, b: 2, a: 3 });
        });
        it("should convert an iterator", () => {
            const m1 = new Map([["c", 1], ["b", 2], ["a", 3]]);
            expect(dict.from(m1.entries())).toEqual({ c: 1, b: 2, a: 3 });
            expect(dict.from(m1.values(), v => [`k${v}`, 4 - v])).toEqual({ k1: 3, k2: 2, k3: 1 });
        });
    });

    describe("function fill", () => {
        it("should exist", () => {
            expect(dict.fill).toBeFunction();
        });
        it("should create a dictionary", () => {
            expect(dict.fill(["a", "b", "c"], 1)).toEqual({ a: 1, b: 1, c: 1 });
            expect(dict.fill("a b\t\nc", 1)).toEqual({ a: 1, b: 1, c: 1 });
            expect(dict.fill(" a b c ", 1)).toEqual({ a: 1, b: 1, c: 1 });
        });
        it("should accept an empty key list", () => {
            expect(dict.fill([], 1)).toEqual({});
            expect(dict.fill("", 1)).toEqual({});
            expect(dict.fill(" \t\n ", 1)).toEqual({});
        });
    });

    describe("function fillRec", () => {
        it("should exist", () => {
            expect(dict.fillRec).toBeFunction();
        });
        it("should create a record", () => {
            expect(dict.fillRec(["a", "b", "c"], 1)).toEqual({ a: 1, b: 1, c: 1 });
        });
        it("should accept an empty key list", () => {
            expect(dict.fillRec([], 1)).toEqual({});
        });
    });

    describe("function generate", () => {
        it("should exist", () => {
            expect(dict.generate).toBeFunction();
        });
        it("should create a dictionary", () => {
            expect(dict.generate(["key1", "key2"], identity)).toEqual({ key1: "key1", key2: "key2" });
            expect(dict.generate(["key1", "key2"], pluck2nd)).toEqual({ key1: 0, key2: 1 });
            expect(dict.generate("key1 key2", identity)).toEqual({ key1: "key1", key2: "key2" });
            expect(dict.generate(" key1\t\nkey2 ", identity)).toEqual({ key1: "key1", key2: "key2" });
        });
        it("should skip undefined as return value", () => {
            const fn = key => (key !== "key2") ? key : undefined;
            expect(dict.generate(["key1", "key2", "key3"], fn)).toEqual({ key1: "key1", key3: "key3" });
        });
        it("should accept an empty key list", () => {
            expect(dict.generate([], identity)).toEqual({});
            expect(dict.generate("", identity)).toEqual({});
            expect(dict.generate(" \t\n ", identity)).toEqual({});
        });
    });

    describe("function generateRec", () => {
        it("should exist", () => {
            expect(dict.generateRec).toBeFunction();
        });
        it("should create a record", () => {
            expect(dict.generateRec(["key1", "key2"], identity)).toEqual({ key1: "key1", key2: "key2" });
            expect(dict.generateRec(["key1", "key2"], pluck2nd)).toEqual({ key1: 0, key2: 1 });
        });
        it("should accept an empty key list", () => {
            expect(dict.generateRec([], identity)).toEqual({});
        });
    });

    describe("function equals", () => {
        it("should exist", () => {
            expect(dict.equals).toBeFunction();
        });
        const dict1 = { a: 1, b: [2, 3], c: "abc", d: 4, e: 5 };
        const dict2 = { a: 1, b: [2, 3], c: "abc", d: "4" };
        it("should return true for equal property values", () => {
            expect(dict.equals(dict1, dict2, "a")).toBeTrue();
            expect(dict.equals(dict1, dict2, ["c"])).toBeTrue();
            expect(dict.equals(dict1, dict2, ["a", "c"])).toBeTrue();
        });
        it("should return true for missing properties", () => {
            expect(dict.equals(dict1, dict2, "x")).toBeTrue();
            expect(dict.equals(dict1, dict2, ["a", "x"])).toBeTrue();
        });
        it("should return false for different property values", () => {
            expect(dict.equals(dict1, dict2, "b")).toBeFalse();
            expect(dict.equals(dict1, dict2, "d")).toBeFalse();
            expect(dict.equals(dict1, dict2, ["a", "b"])).toBeFalse();
            expect(dict.equals(dict1, dict2, "e")).toBeFalse();
        });
        it("should accept an empty key array", () => {
            expect(dict.equals(dict1, dict2, [])).toBeTrue();
            expect(dict.equals({}, dict2, [])).toBeTrue();
        });
        it("should use the predicate", () => {
            function comp(v1, v2) { return String(v1) === String(v2); }
            expect(dict.equals(dict1, dict2, "d", comp)).toBeTrue();
        });
    });

    describe("function forEach", () => {
        it("should exist", () => {
            expect(dict.forEach).toBeFunction();
        });
    });

    describe("function forEachKey", () => {
        it("should exist", () => {
            expect(dict.forEachKey).toBeFunction();
        });
    });

    describe("function some", () => {
        it("should exist", () => {
            expect(dict.some).toBeFunction();
        });
    });

    describe("function someKey", () => {
        it("should exist", () => {
            expect(dict.someKey).toBeFunction();
        });
    });

    describe("function every", () => {
        it("should exist", () => {
            expect(dict.every).toBeFunction();
        });
    });

    describe("function everyKey", () => {
        it("should exist", () => {
            expect(dict.everyKey).toBeFunction();
        });
    });

    describe("function reduce", () => {
        it("should exist", () => {
            expect(dict.reduce).toBeFunction();
        });
        it("should reduce a dictionary", () => {
            expect(dict.reduce({ a: 3, b: 2, c: 1 }, "|", (s, v, k) => `${s}${k}${v}|`)).toBe("|a3|b2|c1|");
        });
    });

    describe("function find", () => {
        it("should exist", () => {
            expect(dict.find).toBeFunction();
        });
    });

    describe("function findKey", () => {
        it("should exist", () => {
            expect(dict.findKey).toBeFunction();
        });
    });

    describe("function keyOf", () => {
        it("should exist", () => {
            expect(dict.keyOf).toBeFunction();
        });
        it("should return the key of a property", () => {
            expect(dict.keyOf({}, 42)).toBeUndefined();
            expect(dict.keyOf({ a: 1, b: 42, c: 1 }, 42)).toBe("b");
            expect(dict.keyOf({ a: 1, b: 42, c: 1 }, 1)).toBeOneOf(["a", "c"]);
            expect(dict.keyOf({ a: 1, b: 42, c: 1 }, 7)).toBeUndefined();
        });
    });

    describe("function singleKey", () => {
        it("should exist", () => {
            expect(dict.singleKey).toBeFunction();
        });
        it("should return whether a dictionary has a single property", () => {
            expect(dict.singleKey({})).toBeFalse();
            expect(dict.singleKey({ a: 1 })).toBeTrue();
            expect(dict.singleKey({ a: 1, b: 2 })).toBeFalse();
        });
    });

    describe("function anyKey", () => {
        it("should exist", () => {
            expect(dict.anyKey).toBeFunction();
        });
        it("should return the key of a property", () => {
            expect(dict.anyKey({})).toBeUndefined();
            expect(dict.anyKey({ a: 1 })).toBe("a");
            expect(dict.anyKey({ a: 1, b: 2 })).toBeOneOf(["a", "b"]);
        });
    });

    describe("function values", () => {
        it("should exist", () => {
            expect(dict.values).toBeFunction();
        });
        it("should iterate a dictionary", () => {
            const it1 = dict.values({ a: 42, b: undefined, length: 4711 });
            expect(it1).toBeIterator();
            expect(Array.from(it1)).toEqual([42, undefined, 4711]);
        });
    });

    describe("function keys", () => {
        it("should exist", () => {
            expect(dict.keys).toBeFunction();
        });
        it("should iterate a dictionary", () => {
            const it1 = dict.keys({ a: 42, b: undefined, length: 4711 });
            expect(it1).toBeIterator();
            expect(Array.from(it1)).toEqual(["a", "b", "length"]);
        });
    });

    describe("function entries", () => {
        it("should exist", () => {
            expect(dict.entries).toBeFunction();
        });
        it("should iterate a dictionary", () => {
            const it1 = dict.entries({ a: 42, b: undefined, length: 4711 });
            expect(it1).toBeIterator();
            expect(Array.from(it1)).toEqual([["a", 42], ["b", undefined], ["length", 4711]]);
        });
    });

    describe("function mapDict", () => {
        it("should exist", () => {
            expect(dict.mapDict).toBeFunction();
        });
        it("should map a dictionary", () => {
            expect(dict.mapDict({ a: 42, b: undefined, length: 4711 }, identity)).toEqual({ a: 42, length: 4711 });
        });
        it("should accept an empty dictionary", () => {
            expect(dict.mapDict({}, identity)).toEqual({});
        });
    });

    describe("function mapRecord", () => {
        it("should exist", () => {
            expect(dict.mapRecord).toBeFunction();
        });
        it("should map a record", () => {
            expect(dict.mapRecord({ a: 42, length: 4711 }, n => n + 1)).toEqual({ a: 43, length: 4712 });
        });
        it("should accept an empty record", () => {
            expect(dict.mapRecord({}, identity)).toEqual({});
        });
    });
});
