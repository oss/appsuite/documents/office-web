/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as debug from "@/io.ox/office/tk/algorithms/debug";

// tests ======================================================================

describe("module tk/algorithms/debug", () => {

    // public functions -------------------------------------------------------

    describe("function stringifyForDebug", () => {
        it("should exist", () => {
            expect(debug.stringify).toBeFunction();
        });
        it("should stringify scalar values", () => {
            expect(debug.stringify(undefined)).toBe("");
            expect(debug.stringify(null)).toBe("null");
            expect(debug.stringify(0)).toBe("0");
            expect(debug.stringify(42)).toBe("42");
            expect(debug.stringify(-42)).toBe("-42");
            expect(debug.stringify(true)).toBe("true");
            expect(debug.stringify(false)).toBe("false");
            expect(debug.stringify("")).toBe('""');
            expect(debug.stringify("abc")).toBe('"abc"');
            expect(debug.stringify('a"c')).toBe('"a\\"c"');
            expect(debug.stringify("a c")).toBe('"a c"');
        });
        it("should stringify arrays", () => {
            expect(debug.stringify([])).toBe("[]");
            expect(debug.stringify([1, 2, 3])).toBe("[1,2,3]");
            expect(debug.stringify([3, 2, 1])).toBe("[3,2,1]");
            expect(debug.stringify(["a c"])).toBe('["a c"]');
        });
        it("should stringify objects and sort their properties", () => {
            expect(debug.stringify({})).toBe("{}");
            expect(debug.stringify({ a: "a c" })).toBe('{a:"a c"}');
        });
        it("should stringify objects and arrays recursively", () => {
            expect(debug.stringify([{ a: 1 }, { b: 2 }])).toBe("[{a:1},{b:2}]");
            expect(debug.stringify({ a: [{ b: 2 }, { c: 3 }] })).toBe("{a:[{b:2},{c:3}]}");
        });
    });

    describe("function logScriptError", () => {
        it("should exist", () => {
            expect(debug.logScriptError).toBeFunction();
        });
        it("should log an error to the console", () => {
            const logError = jest.spyOn(console, "error").mockImplementation(() => undefined);
            debug.logScriptError(0);
            debug.logScriptError(null);
            debug.logScriptError({ name: "SyntaxError" });
            debug.logScriptError(new Error("test"));
            expect(logError).not.toHaveBeenCalled();
            debug.logScriptError(new EvalError("test"));
            debug.logScriptError(new RangeError("test"));
            debug.logScriptError(new ReferenceError("test"));
            debug.logScriptError(new SyntaxError("test"));
            debug.logScriptError(new TypeError("test"));
            debug.logScriptError(new URIError("test"));
            debug.logScriptError(new window.DOMException("test"));
            expect(logError).toHaveBeenCalledTimes(7);
            logError.mockRestore();
        });
    });
});
