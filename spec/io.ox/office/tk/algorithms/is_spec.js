/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import * as is from "@/io.ox/office/tk/algorithms/is";

// tests ======================================================================

describe("module tk/algorithms/is", () => {

    class SubArray extends Array { }

    const arr = new SubArray();
    const symbol = Symbol("desc");

    // public functions -------------------------------------------------------

    describe("function undefined", () => {
        it("should exist", () => {
            expect(is.undefined).toBeFunction();
        });
        it("should return whether the value is undefined", () => {
            expect(is.undefined(1)).toBeFalse();
            expect(is.undefined(1n)).toBeFalse();
            expect(is.undefined("1")).toBeFalse();
            expect(is.undefined(true)).toBeFalse();
            expect(is.undefined(null)).toBeFalse();
            expect(is.undefined(undefined)).toBeTrue();
            expect(is.undefined(symbol)).toBeFalse();
            expect(is.undefined({})).toBeFalse();
            expect(is.undefined([])).toBeFalse();
            expect(is.undefined(new String("1"))).toBeFalse();
            expect(is.undefined(Boolean)).toBeFalse();
            expect(is.undefined($())).toBeFalse();
            expect(is.undefined(arr)).toBeFalse();
        });
    });

    describe("function null", () => {
        it("should exist", () => {
            expect(is.null).toBeFunction();
        });
        it("should return whether the value is null", () => {
            expect(is.null(1)).toBeFalse();
            expect(is.null(1n)).toBeFalse();
            expect(is.null("1")).toBeFalse();
            expect(is.null(true)).toBeFalse();
            expect(is.null(null)).toBeTrue();
            expect(is.null(undefined)).toBeFalse();
            expect(is.null(symbol)).toBeFalse();
            expect(is.null({})).toBeFalse();
            expect(is.null([])).toBeFalse();
            expect(is.null(new String("1"))).toBeFalse();
            expect(is.null(Boolean)).toBeFalse();
            expect(is.null($())).toBeFalse();
            expect(is.null(arr)).toBeFalse();
        });
    });

    describe("function nullish", () => {
        it("should exist", () => {
            expect(is.nullish).toBeFunction();
        });
        it("should return whether the value is undefined or null", () => {
            expect(is.nullish(1)).toBeFalse();
            expect(is.nullish(1n)).toBeFalse();
            expect(is.nullish("1")).toBeFalse();
            expect(is.nullish(true)).toBeFalse();
            expect(is.nullish(null)).toBeTrue();
            expect(is.nullish(undefined)).toBeTrue();
            expect(is.nullish(symbol)).toBeFalse();
            expect(is.nullish({})).toBeFalse();
            expect(is.nullish([])).toBeFalse();
            expect(is.nullish(new String("1"))).toBeFalse();
            expect(is.nullish(Boolean)).toBeFalse();
            expect(is.nullish($())).toBeFalse();
            expect(is.nullish(arr)).toBeFalse();
        });
    });

    describe("function number", () => {
        it("should exist", () => {
            expect(is.number).toBeFunction();
        });
        it("should return whether the value is a number literal", () => {
            expect(is.number(1)).toBeTrue();
            expect(is.number(0)).toBeTrue();
            expect(is.number(-1)).toBeTrue();
            expect(is.number(Infinity)).toBeTrue();
            expect(is.number(-Infinity)).toBeTrue();
            expect(is.number(NaN)).toBeTrue();
            expect(is.number(1n)).toBeFalse();
            expect(is.number("1")).toBeFalse();
            expect(is.number(true)).toBeFalse();
            expect(is.number(null)).toBeFalse();
            expect(is.number(undefined)).toBeFalse();
            expect(is.number(symbol)).toBeFalse();
            expect(is.number({})).toBeFalse();
            expect(is.number([1])).toBeFalse();
            expect(is.number(new Number(1))).toBeFalse();
            expect(is.number(Boolean)).toBeFalse();
            expect(is.number($())).toBeFalse();
            expect(is.number(arr)).toBeFalse();
        });
    });

    describe("function bigint", () => {
        it("should exist", () => {
            expect(is.bigint).toBeFunction();
        });
        it("should return whether the value is a bigint literal", () => {
            expect(is.bigint(1)).toBeFalse();
            expect(is.bigint(1n)).toBeTrue();
            expect(is.bigint("1")).toBeFalse();
            expect(is.bigint(true)).toBeFalse();
            expect(is.bigint(null)).toBeFalse();
            expect(is.bigint(undefined)).toBeFalse();
            expect(is.bigint(symbol)).toBeFalse();
            expect(is.bigint({})).toBeFalse();
            expect(is.bigint([])).toBeFalse();
            expect(is.bigint(new Boolean(true))).toBeFalse();
            expect(is.bigint(Boolean)).toBeFalse();
            expect(is.bigint($())).toBeFalse();
            expect(is.bigint(arr)).toBeFalse();
        });
    });

    describe("function string", () => {
        it("should exist", () => {
            expect(is.string).toBeFunction();
        });
        it("should return whether the value is a string literal", () => {
            expect(is.string(1)).toBeFalse();
            expect(is.string(1n)).toBeFalse();
            expect(is.string("1")).toBeTrue();
            expect(is.string(true)).toBeFalse();
            expect(is.string(null)).toBeFalse();
            expect(is.string(undefined)).toBeFalse();
            expect(is.string(symbol)).toBeFalse();
            expect(is.string({})).toBeFalse();
            expect(is.string([])).toBeFalse();
            expect(is.string(new String("1"))).toBeFalse();
            expect(is.string(Boolean)).toBeFalse();
            expect(is.string($())).toBeFalse();
            expect(is.string(arr)).toBeFalse();
        });
    });

    describe("function boolean", () => {
        it("should exist", () => {
            expect(is.boolean).toBeFunction();
        });
        it("should return whether the value is a boolean literal", () => {
            expect(is.boolean(1)).toBeFalse();
            expect(is.boolean(1n)).toBeFalse();
            expect(is.boolean("1")).toBeFalse();
            expect(is.boolean(true)).toBeTrue();
            expect(is.boolean(null)).toBeFalse();
            expect(is.boolean(undefined)).toBeFalse();
            expect(is.boolean(symbol)).toBeFalse();
            expect(is.boolean({})).toBeFalse();
            expect(is.boolean([])).toBeFalse();
            expect(is.boolean(new Boolean(true))).toBeFalse();
            expect(is.boolean(Boolean)).toBeFalse();
            expect(is.boolean($())).toBeFalse();
            expect(is.boolean(arr)).toBeFalse();
        });
    });

    describe("function symbol", () => {
        it("should exist", () => {
            expect(is.symbol).toBeFunction();
        });
        it("should return whether the value is a symbol", () => {
            expect(is.symbol(1)).toBeFalse();
            expect(is.symbol(1n)).toBeFalse();
            expect(is.symbol("1")).toBeFalse();
            expect(is.symbol(true)).toBeFalse();
            expect(is.symbol(null)).toBeFalse();
            expect(is.symbol(undefined)).toBeFalse();
            expect(is.symbol(symbol)).toBeTrue();
            expect(is.symbol(Symbol("a"))).toBeTrue();
            expect(is.symbol(Symbol.iterator)).toBeTrue();
            expect(is.symbol({})).toBeFalse();
            expect(is.symbol([])).toBeFalse();
            expect(is.symbol(new Boolean(true))).toBeFalse();
            expect(is.symbol(Boolean)).toBeFalse();
            expect(is.symbol($())).toBeFalse();
            expect(is.symbol(arr)).toBeFalse();
        });
    });

    describe("function primitive", () => {
        it("should exist", () => {
            expect(is.primitive).toBeFunction();
        });
        it("should return whether the value is a primitive", () => {
            expect(is.primitive(1)).toBeTrue();
            expect(is.primitive(1n)).toBeTrue();
            expect(is.primitive("1")).toBeTrue();
            expect(is.primitive(true)).toBeTrue();
            expect(is.primitive(null)).toBeTrue();
            expect(is.primitive(undefined)).toBeTrue();
            expect(is.primitive(symbol)).toBeTrue();
            expect(is.primitive({})).toBeFalse();
            expect(is.primitive([])).toBeFalse();
            expect(is.primitive(new Boolean(true))).toBeFalse();
            expect(is.primitive(Boolean)).toBeFalse();
            expect(is.primitive($())).toBeFalse();
            expect(is.primitive(arr)).toBeFalse();
        });
    });

    describe("function dict", () => {
        it("should exist", () => {
            expect(is.dict).toBeFunction();
        });
        it("should return whether the value is a simple object", () => {
            expect(is.dict(1)).toBeFalse();
            expect(is.dict(1n)).toBeFalse();
            expect(is.dict("1")).toBeFalse();
            expect(is.dict(true)).toBeFalse();
            expect(is.dict(null)).toBeFalse();
            expect(is.dict(undefined)).toBeFalse();
            expect(is.dict(symbol)).toBeFalse();
            expect(is.dict({})).toBeTrue();
            expect(is.dict([])).toBeFalse();
            expect(is.dict(new Number(1))).toBeFalse();
            expect(is.dict(new String("1"))).toBeFalse();
            expect(is.dict(new Boolean(true))).toBeFalse();
            expect(is.dict(new Object())).toBeTrue();
            expect(is.dict(new Array())).toBeFalse();
            expect(is.dict(Object.create(null))).toBeTrue();
            expect(is.dict(Object.create(Object.prototype))).toBeTrue();
            expect(is.dict(Object.create(Array.prototype))).toBeFalse();
            expect(is.dict({ constructor: 42 })).toBeTrue();
            expect(is.dict({ constructor: null })).toBeTrue();
            expect(is.dict({ constructor() {} })).toBeTrue();
            expect(is.dict(() => {})).toBeFalse();
            expect(is.dict(Boolean)).toBeFalse();
            expect(is.dict($())).toBeFalse();
            expect(is.dict(arr)).toBeFalse();
        });
    });

    describe("function object", () => {
        it("should exist", () => {
            expect(is.object).toBeFunction();
        });
        it("should return whether the value is an object", () => {
            expect(is.object(1)).toBeFalse();
            expect(is.object(1n)).toBeFalse();
            expect(is.object("1")).toBeFalse();
            expect(is.object(true)).toBeFalse();
            expect(is.object(null)).toBeFalse();
            expect(is.object(undefined)).toBeFalse();
            expect(is.object(symbol)).toBeFalse();
            expect(is.object({})).toBeTrue();
            expect(is.object([])).toBeTrue();
            expect(is.object(new Number(1))).toBeTrue();
            expect(is.object(new String("1"))).toBeTrue();
            expect(is.object(new Boolean(true))).toBeTrue();
            expect(is.object(new Object())).toBeTrue();
            expect(is.object(new Array())).toBeTrue();
            expect(is.object(Object.create(null))).toBeFalse();
            expect(is.object(Object.create(Object.prototype))).toBeTrue();
            expect(is.object(Object.create(Array.prototype))).toBeTrue();
            expect(is.object({ constructor: 42 })).toBeTrue();
            expect(is.object({ constructor: null })).toBeTrue();
            expect(is.object({ constructor() {} })).toBeTrue();
            expect(is.object(() => {})).toBeTrue();
            expect(is.object(Boolean)).toBeTrue();
            expect(is.object($())).toBeTrue();
            expect(is.object(arr)).toBeTrue();
        });
    });

    describe("function array", () => {
        it("should exist", () => {
            expect(is.array).toBeFunction();
        });
        it("should return whether the value is an array", () => {
            expect(is.array(1)).toBeFalse();
            expect(is.array(1n)).toBeFalse();
            expect(is.array("1")).toBeFalse();
            expect(is.array(true)).toBeFalse();
            expect(is.array(null)).toBeFalse();
            expect(is.array(undefined)).toBeFalse();
            expect(is.array(symbol)).toBeFalse();
            expect(is.array({})).toBeFalse();
            expect(is.array([])).toBeTrue();
            expect(is.array(new Number(1))).toBeFalse();
            expect(is.array(new String("1"))).toBeFalse();
            expect(is.array(new Boolean(true))).toBeFalse();
            expect(is.array(new Object())).toBeFalse();
            expect(is.array(new Array())).toBeTrue();
            expect(is.array(Boolean)).toBeFalse();
            expect(is.array($())).toBeFalse();
            expect(is.array(arr)).toBeTrue();
        });
    });

    describe("function readonlyArray", () => {
        it("should exist", () => {
            expect(is.readonlyArray).toBeFunction();
        });
        it("should return whether the value is an array", () => {
            expect(is.readonlyArray(1)).toBeFalse();
            expect(is.readonlyArray(1n)).toBeFalse();
            expect(is.readonlyArray("1")).toBeFalse();
            expect(is.readonlyArray(true)).toBeFalse();
            expect(is.readonlyArray(null)).toBeFalse();
            expect(is.readonlyArray(undefined)).toBeFalse();
            expect(is.readonlyArray(symbol)).toBeFalse();
            expect(is.readonlyArray({})).toBeFalse();
            expect(is.readonlyArray([])).toBeTrue();
            expect(is.readonlyArray(new Number(1))).toBeFalse();
            expect(is.readonlyArray(new String("1"))).toBeFalse();
            expect(is.readonlyArray(new Boolean(true))).toBeFalse();
            expect(is.readonlyArray(new Object())).toBeFalse();
            expect(is.readonlyArray(new Array())).toBeTrue();
            expect(is.readonlyArray(Boolean)).toBeFalse();
            expect(is.readonlyArray($())).toBeFalse();
            expect(is.readonlyArray(arr)).toBeTrue();
        });
    });

    describe("function nativeArray", () => {
        it("should exist", () => {
            expect(is.nativeArray).toBeFunction();
        });
        it("should return whether the value is a simple array", () => {
            expect(is.nativeArray(1)).toBeFalse();
            expect(is.nativeArray(1n)).toBeFalse();
            expect(is.nativeArray("1")).toBeFalse();
            expect(is.nativeArray(true)).toBeFalse();
            expect(is.nativeArray(null)).toBeFalse();
            expect(is.nativeArray(undefined)).toBeFalse();
            expect(is.nativeArray(symbol)).toBeFalse();
            expect(is.nativeArray({})).toBeFalse();
            expect(is.nativeArray([])).toBeTrue();
            expect(is.nativeArray(new Number(1))).toBeFalse();
            expect(is.nativeArray(new String("1"))).toBeFalse();
            expect(is.nativeArray(new Boolean(true))).toBeFalse();
            expect(is.nativeArray(new Object())).toBeFalse();
            expect(is.nativeArray(new Array())).toBeTrue();
            expect(is.nativeArray(Object.create(null))).toBeFalse();
            expect(is.nativeArray(Object.create(Object.prototype))).toBeFalse();
            expect(is.nativeArray(Object.create(Array.prototype))).toBeTrue();
            expect(is.nativeArray(Boolean)).toBeFalse();
            expect(is.nativeArray($())).toBeFalse();
            expect(is.nativeArray(arr)).toBeFalse();
        });
    });

    describe("function arrayLike", () => {
        it("should exist", () => {
            expect(is.arrayLike).toBeFunction();
        });
        it("should return whether the value is an array-like", () => {
            expect(is.arrayLike(1)).toBeFalse();
            expect(is.arrayLike(1n)).toBeFalse();
            expect(is.arrayLike("1")).toBeFalse();
            expect(is.arrayLike(true)).toBeFalse();
            expect(is.arrayLike(null)).toBeFalse();
            expect(is.arrayLike(undefined)).toBeFalse();
            expect(is.arrayLike(symbol)).toBeFalse();
            expect(is.arrayLike({})).toBeFalse();
            expect(is.arrayLike([])).toBeTrue();
            expect(is.arrayLike(new Number(1))).toBeFalse();
            expect(is.arrayLike(new String("1"))).toBeFalse();
            expect(is.arrayLike(new Boolean(true))).toBeFalse();
            expect(is.arrayLike(new Object())).toBeFalse();
            expect(is.arrayLike(new Array())).toBeTrue();
            expect(is.arrayLike(Object.create(null))).toBeFalse();
            expect(is.arrayLike(Object.create(Object.prototype))).toBeFalse();
            expect(is.arrayLike(Object.create(Array.prototype))).toBeTrue();
            expect(is.arrayLike(Boolean)).toBeFalse();
            expect(is.arrayLike($())).toBeTrue();
            expect(is.arrayLike(arr)).toBeTrue();
            expect(is.arrayLike({ 0: "a" })).toBeFalse();
            expect(is.arrayLike({ length: -1 })).toBeFalse();
            expect(is.arrayLike({ length: 0 })).toBeTrue();
            expect(is.arrayLike({ length: 1 })).toBeFalse();
            expect(is.arrayLike({ length: 1, 0: "a" })).toBeTrue();
            expect(is.arrayLike({ length: 2, 0: "a", 1: "b" })).toBeTrue();
        });
    });

    describe("function function", () => {
        it("should exist", () => {
            expect(is.function).toBeFunction();
        });
        it("should return whether the value is a function", () => {
            expect(is.function(1)).toBeFalse();
            expect(is.function(1n)).toBeFalse();
            expect(is.function("1")).toBeFalse();
            expect(is.function(true)).toBeFalse();
            expect(is.function(null)).toBeFalse();
            expect(is.function(undefined)).toBeFalse();
            expect(is.function(symbol)).toBeFalse();
            expect(is.function({})).toBeFalse();
            expect(is.function([])).toBeFalse();
            expect(is.function(new Number(1))).toBeFalse();
            expect(is.function(new String("1"))).toBeFalse();
            expect(is.function(new Boolean(true))).toBeFalse();
            expect(is.function(new Object())).toBeFalse();
            expect(is.function(new Array())).toBeFalse();
            expect(is.function(() => {})).toBeTrue();
            expect(is.function(async () => {})).toBeTrue();
            expect(is.function(function *() {})).toBeTrue();
            expect(is.function(async function *() {})).toBeTrue();
            expect(is.function(() => {})).toBeTrue();
            expect(is.function(async () => {})).toBeTrue();
            expect(is.function(Boolean)).toBeTrue();
            expect(is.function($())).toBeFalse();
            expect(is.function(arr)).toBeFalse();
        });
    });

    describe("function empty", () => {
        it("should exist", () => {
            expect(is.empty).toBeFunction();
        });
        it("should return whether the value is empty", () => {
            expect(is.empty(0)).toBeTrue();
            expect(is.empty(1)).toBeFalse();
            expect(is.empty(Number.NaN)).toBeTrue();
            expect(is.empty(Infinity)).toBeFalse();
            expect(is.empty(0n)).toBeTrue();
            expect(is.empty(1n)).toBeFalse();
            expect(is.empty("")).toBeTrue();
            expect(is.empty("0")).toBeFalse();
            expect(is.empty(false)).toBeTrue();
            expect(is.empty(true)).toBeFalse();
            expect(is.empty(null)).toBeTrue();
            expect(is.empty(undefined)).toBeTrue();
            expect(is.empty(symbol)).toBeFalse();
            expect(is.empty({})).toBeTrue();
            expect(is.empty(new Object())).toBeTrue();
            expect(is.empty(Object.create(null))).toBeTrue();
            expect(is.empty({ a: 1 })).toBeFalse();
            expect(is.empty([])).toBeTrue();
            expect(is.empty([1])).toBeFalse();
            expect(is.empty(Boolean)).toBeFalse();
            expect(is.empty($())).toBeFalse();
            expect(is.empty(arr)).toBeTrue();
        });
    });

    describe("function scriptError", () => {
        it("should exist", () => {
            expect(is.scriptError).toBeFunction();
        });
        it("should return whether the value is an internal exception", () => {
            expect(is.scriptError(0)).toBeFalse();
            expect(is.scriptError(null)).toBeFalse();
            expect(is.scriptError({ name: "SyntaxError" })).toBeFalse();
            expect(is.scriptError(new Error("test"))).toBeFalse();
            expect(is.scriptError(new EvalError("test"))).toBeTrue();
            expect(is.scriptError(new RangeError("test"))).toBeTrue();
            expect(is.scriptError(new ReferenceError("test"))).toBeTrue();
            expect(is.scriptError(new SyntaxError("test"))).toBeTrue();
            expect(is.scriptError(new TypeError("test"))).toBeTrue();
            expect(is.scriptError(new URIError("test"))).toBeTrue();
        });
    });

    describe("function destroyable", () => {
        it("should exist", () => {
            expect(is.destroyable).toBeFunction();
        });
        it("should return whether the value is destroyable", () => {
            expect(is.destroyable(0)).toBeFalse();
            expect(is.destroyable("")).toBeFalse();
            expect(is.destroyable(null)).toBeFalse();
            expect(is.destroyable({})).toBeFalse();
            expect(is.destroyable({ destroy: 42 })).toBeFalse();
            expect(is.destroyable({ destroy() {} })).toBeTrue();
            expect(is.destroyable(Object.assign([], { destroy() {} }))).toBeTrue();
            expect(is.destroyable({ disconnect() {} })).toBeFalse();
            expect(is.destroyable({ abort() {} })).toBeFalse();
            expect(is.destroyable({ destroy() {}, disconnect() {}, abort() {} })).toBeTrue();
        });
    });

    describe("function disconnectable", () => {
        it("should exist", () => {
            expect(is.disconnectable).toBeFunction();
        });
        it("should return whether the value is disconnectable", () => {
            expect(is.disconnectable(0)).toBeFalse();
            expect(is.disconnectable("")).toBeFalse();
            expect(is.disconnectable(null)).toBeFalse();
            expect(is.disconnectable({})).toBeFalse();
            expect(is.disconnectable({ destroy() {} })).toBeFalse();
            expect(is.disconnectable({ disconnect: 42 })).toBeFalse();
            expect(is.disconnectable({ disconnect() {} })).toBeTrue();
            expect(is.disconnectable(Object.assign([], { disconnect() {} }))).toBeTrue();
            expect(is.disconnectable({ abort() {} })).toBeFalse();
            expect(is.disconnectable({ destroy() {}, disconnect() {}, abort() {} })).toBeTrue();
        });
    });

    describe("function abortable", () => {
        it("should exist", () => {
            expect(is.abortable).toBeFunction();
        });
        it("should return whether the value is abortable", () => {
            expect(is.abortable(0)).toBeFalse();
            expect(is.abortable("")).toBeFalse();
            expect(is.abortable(null)).toBeFalse();
            expect(is.abortable({})).toBeFalse();
            expect(is.abortable({ destroy() {} })).toBeFalse();
            expect(is.abortable({ disconnect() {} })).toBeFalse();
            expect(is.abortable({ abort: 42 })).toBeFalse();
            expect(is.abortable({ abort() {} })).toBeTrue();
            expect(is.abortable(Object.assign([], { abort() {} }))).toBeTrue();
            expect(is.abortable({ destroy() {}, disconnect() {}, abort() {} })).toBeTrue();
        });
    });
});
