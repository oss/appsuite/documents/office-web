/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as itr from "@/io.ox/office/tk/algorithms/iterable";

// tests ======================================================================

describe("module tk/algorithms/iterable", () => {

    const a1 = [3, 2, 1];
    const s1 = new Set([3, 2, 1]);
    const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);

    function even(n) { return n % 2 === 0; }
    function odd(n) { return n % 2 !== 0; }

    // public functions -------------------------------------------------------

    describe("function empty", () => {
        it("should exist", () => {
            expect(itr.empty).toBeFunction();
        });
        it("should create empty iterator", () => {
            const it = itr.empty();
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([]);
        });
    });

    describe("function once", () => {
        it("should exist", () => {
            expect(itr.once).toBeFunction();
        });
        it("should create an iterator", () => {
            const it = itr.once(42);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([42]);
        });
    });

    describe("function forever", () => {
        it("should exist", () => {
            expect(itr.forever).toBeFunction();
        });
        it("should create an iterator", () => {
            const it = itr.forever(42);
            expect(it).toBeIterator();
            expect(it.next()).toEqual({ done: false, value: 42 });
            expect(it.next()).toEqual({ done: false, value: 42 });
            expect(it.next()).toEqual({ done: false, value: 42 });
            expect(it.next()).toEqual({ done: false, value: 42 });
            expect(it.next()).toEqual({ done: false, value: 42 });
        });
    });

    describe("function yieldFirst", () => {
        it("should exist", () => {
            expect(itr.yieldFirst).toBeFunction();
        });
        it("should create an iterator for an array", () => {
            const it = itr.yieldFirst(a1);
            expect(it).toBeIterator();
            expect(it.next()).toEqual({ done: false, value: 3 });
            expect(it.next()).toEqual({ done: false, value: 3 });
            expect(it.next()).toEqual({ done: false, value: 3 });
            a1.unshift(4);
            expect(it.next()).toEqual({ done: false, value: 4 });
            expect(it.next()).toEqual({ done: false, value: 4 });
            expect(it.next()).toEqual({ done: false, value: 4 });
            a1.shift();
            expect(it.next()).toEqual({ done: false, value: 3 });
            expect(it.next()).toEqual({ done: false, value: 3 });
            expect(it.next()).toEqual({ done: false, value: 3 });
        });
        it("should create an iterator for a set", () => {
            const it = itr.yieldFirst(s1);
            expect(it.next()).toEqual({ done: false, value: 3 });
            expect(it.next()).toEqual({ done: false, value: 3 });
            expect(it.next()).toEqual({ done: false, value: 3 });
        });
    });

    describe("function sequence", () => {
        it("should exist", () => {
            expect(itr.sequence).toBeFunction();
        });
        it("should create an increasing sequence", () => {
            const it1 = itr.sequence(2, 5);
            expect(it1).toBeIterator();
            expect(Array.from(it1)).toEqual([2, 3, 4]);
            const it2 = itr.sequence(2, 5, 2);
            expect(it2).toBeIterator();
            expect(Array.from(it2)).toEqual([2, 4]);
        });
        it("should create a decreasing sequence", () => {
            const it1 = itr.sequence(5, 2, -1);
            expect(it1).toBeIterator();
            expect(Array.from(it1)).toEqual([5, 4, 3]);
            const it2 = itr.sequence(5, 2, -2);
            expect(it2).toBeIterator();
            expect(Array.from(it2)).toEqual([5, 3]);
        });
    });

    describe("function interval", () => {
        it("should exist", () => {
            expect(itr.interval).toBeFunction();
        });
        it("should create an interval iterator", () => {
            const it = itr.interval(2, 4);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([2, 3, 4]);
        });
        it("should create a reverse interval iterator", () => {
            const it = itr.interval(2, 4, true);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([4, 3, 2]);
        });
    });

    describe("function some", () => {
        it("should exist", () => {
            expect(itr.some).toBeFunction();
        });
        it("should visit an array", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.some(a1, spy1)).toBeFalse();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 3);
            expect(spy1).toHaveBeenNthCalledWith(2, 2);
            expect(spy1).toHaveBeenNthCalledWith(3, 1);
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.some(a1, spy2)).toBeTrue();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 3);
            expect(spy2).toHaveBeenNthCalledWith(2, 2);
        });
        it("should visit a set", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.some(s1, spy1)).toBeFalse();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 3);
            expect(spy1).toHaveBeenNthCalledWith(2, 2);
            expect(spy1).toHaveBeenNthCalledWith(3, 1);
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.some(s1, spy2)).toBeTrue();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 3);
            expect(spy2).toHaveBeenNthCalledWith(2, 2);
        });
        it("should visit a map", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.some(m1, spy1)).toBeFalse();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, [3, "a"]);
            expect(spy1).toHaveBeenNthCalledWith(2, [2, "b"]);
            expect(spy1).toHaveBeenNthCalledWith(3, [1, "c"]);
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.some(m1, spy2)).toBeTrue();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, [3, "a"]);
            expect(spy2).toHaveBeenNthCalledWith(2, [2, "b"]);
        });
        it("should visit a string", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.some("abc", spy1)).toBeFalse();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, "a");
            expect(spy1).toHaveBeenNthCalledWith(2, "b");
            expect(spy1).toHaveBeenNthCalledWith(3, "c");
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.some("abc", spy2)).toBeTrue();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, "a");
            expect(spy2).toHaveBeenNthCalledWith(2, "b");
        });
        it("should visit an iterator", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.some(m1.values(), spy1)).toBeFalse();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, "a");
            expect(spy1).toHaveBeenNthCalledWith(2, "b");
            expect(spy1).toHaveBeenNthCalledWith(3, "c");
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.some(m1.values(), spy2)).toBeTrue();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, "a");
            expect(spy2).toHaveBeenNthCalledWith(2, "b");
        });
        it("should accept empty source", () => {
            const spy = jest.fn();
            expect(itr.some([], spy)).toBeFalse();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe("function every", () => {
        it("should exist", () => {
            expect(itr.every).toBeFunction();
        });
        it("should visit an array", () => {
            const spy1 = jest.fn().mockReturnValue(true);
            expect(itr.every(a1, spy1)).toBeTrue();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 3);
            expect(spy1).toHaveBeenNthCalledWith(2, 2);
            expect(spy1).toHaveBeenNthCalledWith(3, 1);
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(false).mockReturnValue(true);
            expect(itr.every(a1, spy2)).toBeFalse();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 3);
            expect(spy2).toHaveBeenNthCalledWith(2, 2);
        });
        it("should visit a set", () => {
            const spy1 = jest.fn().mockReturnValue(true);
            expect(itr.every(s1, spy1)).toBeTrue();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 3);
            expect(spy1).toHaveBeenNthCalledWith(2, 2);
            expect(spy1).toHaveBeenNthCalledWith(3, 1);
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(false).mockReturnValue(true);
            expect(itr.every(s1, spy2)).toBeFalse();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 3);
            expect(spy2).toHaveBeenNthCalledWith(2, 2);
        });
        it("should visit a map", () => {
            const spy1 = jest.fn().mockReturnValue(true);
            expect(itr.every(m1, spy1)).toBeTrue();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, [3, "a"]);
            expect(spy1).toHaveBeenNthCalledWith(2, [2, "b"]);
            expect(spy1).toHaveBeenNthCalledWith(3, [1, "c"]);
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(false).mockReturnValue(true);
            expect(itr.every(m1, spy2)).toBeFalse();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, [3, "a"]);
            expect(spy2).toHaveBeenNthCalledWith(2, [2, "b"]);
        });
        it("should visit a string", () => {
            const spy1 = jest.fn().mockReturnValue(true);
            expect(itr.every("abc", spy1)).toBeTrue();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, "a");
            expect(spy1).toHaveBeenNthCalledWith(2, "b");
            expect(spy1).toHaveBeenNthCalledWith(3, "c");
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(false).mockReturnValue(true);
            expect(itr.every("abc", spy2)).toBeFalse();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, "a");
            expect(spy2).toHaveBeenNthCalledWith(2, "b");
        });
        it("should visit an iterator", () => {
            const spy1 = jest.fn().mockReturnValue(true);
            expect(itr.every(m1.values(), spy1)).toBeTrue();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, "a");
            expect(spy1).toHaveBeenNthCalledWith(2, "b");
            expect(spy1).toHaveBeenNthCalledWith(3, "c");
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(false).mockReturnValue(true);
            expect(itr.every(m1.values(), spy2)).toBeFalse();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, "a");
            expect(spy2).toHaveBeenNthCalledWith(2, "b");
        });
        it("should accept empty source", () => {
            const spy = jest.fn();
            expect(itr.every([], spy)).toBeTrue();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe("function reduce", () => {
        it("should exist", () => {
            expect(itr.reduce).toBeFunction();
        });
        it("should reduce an array", () => {
            expect(itr.reduce(a1, ".", (r, v) => (v === 2) ? undefined : `${r}${v}.`)).toBe(".3.1.");
        });
        it("should reduce a set", () => {
            expect(itr.reduce(s1, ".", (r, v) => (v === 2) ? undefined : `${r}${v}.`)).toBe(".3.1.");
        });
        it("should reduce a map", () => {
            expect(itr.reduce(m1, ".", (r, [k, v]) => (v === "b") ? undefined : `${r}${k}.`)).toBe(".3.1.");
        });
        it("should reduce a string", () => {
            expect(itr.reduce("abc", ".", (r, v) => (v === "b") ? undefined : `${r}${v}.`)).toBe(".a.c.");
        });
        it("should reduce an iterator", () => {
            expect(itr.reduce(m1.keys(), ".", (r, v) => (v === 2) ? undefined : `${r}${v}.`)).toBe(".3.1.");
        });
        it("should accept empty source", () => {
            expect(itr.reduce([], ".", (r, v) => `${r}${v}.`)).toBe(".");
        });
    });

    describe("function size", () => {
        it("should exist", () => {
            expect(itr.size).toBeFunction();
        });
        it("should visit an array", () => {
            expect(itr.size(a1)).toBe(3);
        });
        it("should visit a set", () => {
            expect(itr.size(s1)).toBe(3);
        });
        it("should visit a map", () => {
            expect(itr.size(m1)).toBe(3);
        });
        it("should visit a string", () => {
            expect(itr.size("abc")).toBe(3);
        });
        it("should visit an iterator", () => {
            expect(itr.size(m1.values())).toBe(3);
        });
    });

    describe("function count", () => {
        it("should exist", () => {
            expect(itr.count).toBeFunction();
        });
        it("should visit an array", () => {
            expect(itr.count(a1, v => v < 3)).toBe(2);
            expect(itr.count(a1, v => v === 2)).toBe(1);
        });
        it("should visit a set", () => {
            expect(itr.count(s1, v => v < 3)).toBe(2);
            expect(itr.count(s1, v => v === 2)).toBe(1);
        });
        it("should visit a map", () => {
            expect(itr.count(m1, e => e[0] < 3)).toBe(2);
            expect(itr.count(m1, e => e[1] === "b")).toBe(1);
        });
        it("should visit a string", () => {
            expect(itr.count("abc", v => v < "c")).toBe(2);
            expect(itr.count("abc", v => v === "b")).toBe(1);
        });
        it("should visit an iterator", () => {
            expect(itr.count(m1.values(), v => v < "c")).toBe(2);
            expect(itr.count(m1.values(), v => v === "b")).toBe(1);
        });
    });

    describe("function group", () => {
        it("should exist", () => {
            expect(itr.group).toBeFunction();
        });
        it("should group an array", () => {
            const result = itr.group([2, 6, 5, 4, 3], v => `k${v % 3}`);
            expect(result).toBeInstanceOf(Map);
            expect(result.size).toBe(3);
            expect(result.get("k0")).toEqual([6, 3]);
            expect(result.get("k1")).toEqual([4]);
            expect(result.get("k2")).toEqual([2, 5]);
        });
        it("should group a set", () => {
            const result = itr.group(new Set([2, 6, 5, 4, 3]), v => `k${v % 3}`);
            expect(result).toBeInstanceOf(Map);
            expect(result.size).toBe(3);
            expect(result.get("k0")).toEqual([6, 3]);
            expect(result.get("k1")).toEqual([4]);
            expect(result.get("k2")).toEqual([2, 5]);
        });
        it("should group a string", () => {
            const result = itr.group("abcdef", c => /[acf]/.test(c) ? 0 : 1);
            expect(result).toBeInstanceOf(Map);
            expect(result.size).toBe(2);
            expect(result.get(0)).toEqual(["a", "c", "f"]);
            expect(result.get(1)).toEqual(["b", "d", "e"]);
        });
    });

    describe("function split", () => {
        it("should exist", () => {
            expect(itr.split).toBeFunction();
        });
        it("should split an array", () => {
            const result = itr.split([2, 6, 5, 4, 3], v => v % 2 === 1);
            expect(result).toEqual([[2, 6, 4], [5, 3]]);
        });
        it("should split a set", () => {
            const result = itr.split(new Set([2, 6, 5, 4, 3]), v => v % 2 === 1);
            expect(result).toEqual([[2, 6, 4], [5, 3]]);
        });
        it("should split a string", () => {
            const result = itr.split("abcdef", c => /[acf]/.test(c));
            expect(result).toEqual([["b", "d", "e"], ["a", "c", "f"]]);
        });
    });

    describe("function shift", () => {
        it("should exist", () => {
            expect(itr.shift).toBeFunction();
        });
        it("should shift from an iterator", () => {
            const it = a1.values();
            expect(itr.shift(it)).toBe(3);
            expect(itr.shift(it)).toBe(2);
            expect(itr.shift(it)).toBe(1);
            expect(itr.shift(it)).toBeUndefined();
        });
    });

    describe("function first", () => {
        it("should exist", () => {
            expect(itr.first).toBeFunction();
        });
        it("should visit an array", () => {
            expect(itr.first(a1)).toBe(3);
            expect(itr.first(a1)).toBe(3);
        });
        it("should visit a set", () => {
            expect(itr.first(s1)).toBe(3);
            expect(itr.first(s1)).toBe(3);
        });
        it("should visit a map", () => {
            expect(itr.first(m1)).toEqual([3, "a"]);
            expect(itr.first(m1)).toEqual([3, "a"]);
        });
        it("should visit a string", () => {
            expect(itr.first("abc")).toBe("a");
        });
        it("should visit an iterator", () => {
            const iter = a1.values();
            expect(itr.first(iter)).toBe(3);
            expect(itr.first(iter)).toBe(2);
            expect(itr.first(iter)).toBe(1);
        });
    });

    describe("function find", () => {
        it("should exist", () => {
            expect(itr.find).toBeFunction();
        });
        it("should visit an array", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.find(a1, spy1)).toBeUndefined();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 3);
            expect(spy1).toHaveBeenNthCalledWith(2, 2);
            expect(spy1).toHaveBeenNthCalledWith(3, 1);
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.find(a1, spy2)).toBe(2);
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 3);
            expect(spy2).toHaveBeenNthCalledWith(2, 2);
        });
        it("should visit a set", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.find(s1, spy1)).toBeUndefined();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 3);
            expect(spy1).toHaveBeenNthCalledWith(2, 2);
            expect(spy1).toHaveBeenNthCalledWith(3, 1);
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.find(s1, spy2)).toBe(2);
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 3);
            expect(spy2).toHaveBeenNthCalledWith(2, 2);
        });
        it("should visit a map", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.find(m1, spy1)).toBeUndefined();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, [3, "a"]);
            expect(spy1).toHaveBeenNthCalledWith(2, [2, "b"]);
            expect(spy1).toHaveBeenNthCalledWith(3, [1, "c"]);
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.find(m1, spy2)).toEqual([2, "b"]);
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, [3, "a"]);
            expect(spy2).toHaveBeenNthCalledWith(2, [2, "b"]);
        });
        it("should visit a string", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.find("abc", spy1)).toBeUndefined();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, "a");
            expect(spy1).toHaveBeenNthCalledWith(2, "b");
            expect(spy1).toHaveBeenNthCalledWith(3, "c");
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.find("abc", spy2)).toBe("b");
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, "a");
            expect(spy2).toHaveBeenNthCalledWith(2, "b");
        });
        it("should visit an iterator", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.find(m1.values(), spy1)).toBeUndefined();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, "a");
            expect(spy1).toHaveBeenNthCalledWith(2, "b");
            expect(spy1).toHaveBeenNthCalledWith(3, "c");
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.find(m1.values(), spy2)).toBe("b");
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, "a");
            expect(spy2).toHaveBeenNthCalledWith(2, "b");
        });
    });

    describe("function findLast", () => {
        it("should exist", () => {
            expect(itr.findLast).toBeFunction();
        });
        it("should visit an array", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.findLast(a1, spy1)).toBeUndefined();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 3);
            expect(spy1).toHaveBeenNthCalledWith(2, 2);
            expect(spy1).toHaveBeenNthCalledWith(3, 1);
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.findLast(a1, spy2)).toBe(2);
            expect(spy2).toHaveBeenCalledTimes(3);
            expect(spy2).toHaveBeenNthCalledWith(1, 3);
            expect(spy2).toHaveBeenNthCalledWith(2, 2);
            expect(spy2).toHaveBeenNthCalledWith(3, 1);
        });
        it("should visit a set", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.findLast(s1, spy1)).toBeUndefined();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 3);
            expect(spy1).toHaveBeenNthCalledWith(2, 2);
            expect(spy1).toHaveBeenNthCalledWith(3, 1);
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.findLast(s1, spy2)).toBe(2);
            expect(spy2).toHaveBeenCalledTimes(3);
            expect(spy2).toHaveBeenNthCalledWith(1, 3);
            expect(spy2).toHaveBeenNthCalledWith(2, 2);
            expect(spy2).toHaveBeenNthCalledWith(3, 1);
        });
        it("should visit a map", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.findLast(m1, spy1)).toBeUndefined();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, [3, "a"]);
            expect(spy1).toHaveBeenNthCalledWith(2, [2, "b"]);
            expect(spy1).toHaveBeenNthCalledWith(3, [1, "c"]);
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.findLast(m1, spy2)).toEqual([2, "b"]);
            expect(spy2).toHaveBeenCalledTimes(3);
            expect(spy2).toHaveBeenNthCalledWith(1, [3, "a"]);
            expect(spy2).toHaveBeenNthCalledWith(2, [2, "b"]);
            expect(spy2).toHaveBeenNthCalledWith(3, [1, "c"]);
        });
        it("should visit a string", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.findLast("abc", spy1)).toBeUndefined();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, "a");
            expect(spy1).toHaveBeenNthCalledWith(2, "b");
            expect(spy1).toHaveBeenNthCalledWith(3, "c");
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.findLast("abc", spy2)).toBe("b");
            expect(spy2).toHaveBeenCalledTimes(3);
            expect(spy2).toHaveBeenNthCalledWith(1, "a");
            expect(spy2).toHaveBeenNthCalledWith(2, "b");
            expect(spy2).toHaveBeenNthCalledWith(3, "c");
        });
        it("should visit an iterator", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(itr.findLast(m1.values(), spy1)).toBeUndefined();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, "a");
            expect(spy1).toHaveBeenNthCalledWith(2, "b");
            expect(spy1).toHaveBeenNthCalledWith(3, "c");
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(true).mockReturnValue(false);
            expect(itr.findLast(m1.values(), spy2)).toBe("b");
            expect(spy2).toHaveBeenCalledTimes(3);
            expect(spy2).toHaveBeenNthCalledWith(1, "a");
            expect(spy2).toHaveBeenNthCalledWith(2, "b");
            expect(spy2).toHaveBeenNthCalledWith(3, "c");
        });
    });

    describe("function mapFirst", () => {
        it("should exist", () => {
            expect(itr.mapFirst).toBeFunction();
        });
        it("should visit an array", () => {
            expect(itr.mapFirst(a1, v => even(v) ? (2 * v) : undefined)).toBe(4);
            expect(itr.mapFirst(a1, v => odd(v) ? (2 * v) : undefined)).toBe(6);
            expect(itr.mapFirst(a1, v => (v === 0) ? v : undefined)).toBeUndefined();
        });
        it("should visit a set", () => {
            expect(itr.mapFirst(s1, v => even(v) ? (2 * v) : undefined)).toBe(4);
            expect(itr.mapFirst(s1, v => odd(v) ? (2 * v) : undefined)).toBe(6);
            expect(itr.mapFirst(s1, v => (v === 0) ? v : undefined)).toBeUndefined();
        });
        it("should visit a map", () => {
            expect(itr.mapFirst(m1, ([k, v]) => even(k) ? v : undefined)).toBe("b");
            expect(itr.mapFirst(m1, ([k, v]) => odd(k) ? v : undefined)).toBe("a");
            expect(itr.mapFirst(m1, ([k, v]) => (k === 0) ? v : undefined)).toBeUndefined();
        });
        it("should visit a string", () => {
            expect(itr.mapFirst("abc", c => (c !== "b") ? c : undefined)).toBe("a");
            expect(itr.mapFirst("abc", c => (c === "x") ? c : undefined)).toBeUndefined();
        });
        it("should visit an iterator", () => {
            expect(itr.mapFirst(m1.keys(), k => even(k) ? (2 * k) : undefined)).toBe(4);
            expect(itr.mapFirst(m1.keys(), k => odd(k) ? (2 * k) : undefined)).toBe(6);
            expect(itr.mapFirst(m1.keys(), k => (k === 0) ? k : undefined)).toBeUndefined();
        });
    });

    describe("function mapLast", () => {
        it("should exist", () => {
            expect(itr.mapLast).toBeFunction();
        });
        it("should visit an array", () => {
            expect(itr.mapLast(a1, v => even(v) ? (2 * v) : undefined)).toBe(4);
            expect(itr.mapLast(a1, v => odd(v) ? (2 * v) : undefined)).toBe(2);
            expect(itr.mapLast(a1, v => (v === 0) ? v : undefined)).toBeUndefined();
        });
        it("should visit a set", () => {
            expect(itr.mapLast(s1, v => even(v) ? (2 * v) : undefined)).toBe(4);
            expect(itr.mapLast(s1, v => odd(v) ? (2 * v) : undefined)).toBe(2);
            expect(itr.mapLast(s1, v => (v === 0) ? v : undefined)).toBeUndefined();
        });
        it("should visit a map", () => {
            expect(itr.mapLast(m1, ([k, v]) => even(k) ? v : undefined)).toBe("b");
            expect(itr.mapLast(m1, ([k, v]) => odd(k) ? v : undefined)).toBe("c");
            expect(itr.mapLast(m1, ([k, v]) => (k === 0) ? v : undefined)).toBeUndefined();
        });
        it("should visit a string", () => {
            expect(itr.mapLast("abc", c => (c !== "b") ? c : undefined)).toBe("c");
            expect(itr.mapLast("abc", c => (c === "x") ? c : undefined)).toBeUndefined();
        });
        it("should visit an iterator", () => {
            expect(itr.mapLast(m1.keys(), k => even(k) ? (2 * k) : undefined)).toBe(4);
            expect(itr.mapLast(m1.keys(), k => odd(k) ? (2 * k) : undefined)).toBe(2);
            expect(itr.mapLast(m1.keys(), k => (k === 0) ? k : undefined)).toBeUndefined();
        });
    });

    describe("function nearest", () => {
        it("should exist", () => {
            expect(itr.nearest).toBeFunction();
        });
        it("should visit an array", () => {
            expect(itr.nearest(a1, 0, v => v ** 2)).toBe(1);
            expect(itr.nearest(a1, 2, v => v ** 2)).toBe(1);
            expect(itr.nearest(a1, 3, v => v ** 2)).toBe(2);
            expect(itr.nearest(a1, 6, v => v ** 2)).toBe(2);
            expect(itr.nearest(a1, 7, v => v ** 2)).toBe(3);
            expect(itr.nearest(a1, 1000, v => v ** 2)).toBe(3);
        });
        it("should visit a set", () => {
            expect(itr.nearest(s1, 0, v => v ** 2)).toBe(1);
            expect(itr.nearest(s1, 2, v => v ** 2)).toBe(1);
            expect(itr.nearest(s1, 3, v => v ** 2)).toBe(2);
            expect(itr.nearest(s1, 6, v => v ** 2)).toBe(2);
            expect(itr.nearest(s1, 7, v => v ** 2)).toBe(3);
            expect(itr.nearest(s1, 1000, v => v ** 2)).toBe(3);
        });
        it("should visit a map", () => {
            expect(itr.nearest(m1, 0, e => e[0] ** 2)).toEqual([1, "c"]);
            expect(itr.nearest(m1, 2, e => e[0] ** 2)).toEqual([1, "c"]);
            expect(itr.nearest(m1, 3, e => e[0] ** 2)).toEqual([2, "b"]);
            expect(itr.nearest(m1, 6, e => e[0] ** 2)).toEqual([2, "b"]);
            expect(itr.nearest(m1, 7, e => e[0] ** 2)).toEqual([3, "a"]);
            expect(itr.nearest(m1, 1000, e => e[0] ** 2)).toEqual([3, "a"]);
        });
        it("should visit a string", () => {
            expect(itr.nearest("abc", 96, v => v.charCodeAt(0))).toBe("a");
            expect(itr.nearest("abc", 97, v => v.charCodeAt(0))).toBe("a");
            expect(itr.nearest("abc", 98, v => v.charCodeAt(0))).toBe("b");
            expect(itr.nearest("abc", 99, v => v.charCodeAt(0))).toBe("c");
            expect(itr.nearest("abc", 100, v => v.charCodeAt(0))).toBe("c");
        });
        it("should visit an iterator", () => {
            expect(itr.nearest(m1.keys(), 0, v => v ** 2)).toBe(1);
            expect(itr.nearest(m1.keys(), 2, v => v ** 2)).toBe(1);
            expect(itr.nearest(m1.keys(), 3, v => v ** 2)).toBe(2);
            expect(itr.nearest(m1.keys(), 6, v => v ** 2)).toBe(2);
            expect(itr.nearest(m1.keys(), 7, v => v ** 2)).toBe(3);
            expect(itr.nearest(m1.keys(), 1000, v => v ** 2)).toBe(3);
        });
    });

    describe("function indexed", () => {
        it("should exist", () => {
            expect(itr.indexed).toBeFunction();
        });
        it("should visit an array", () => {
            const it = itr.indexed(a1);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([[0, 3], [1, 2], [2, 1]]);
        });
        it("should visit a set", () => {
            const it = itr.indexed(s1);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([[0, 3], [1, 2], [2, 1]]);
        });
        it("should visit a map", () => {
            const it = itr.indexed(m1);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([[0, [3, "a"]], [1, [2, "b"]], [2, [1, "c"]]]);
        });
        it("should visit a string", () => {
            const it = itr.indexed("abc");
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([[0, "a"], [1, "b"], [2, "c"]]);
        });
        it("should visit an iterator", () => {
            const it = itr.indexed(m1.values());
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([[0, "a"], [1, "b"], [2, "c"]]);
        });
    });

    describe("function chain", () => {
        it("should exist", () => {
            expect(itr.chain).toBeFunction();
        });
        it("should create a chained iterator", () => {
            const it = itr.chain(a1, s1, "abc", m1.keys());
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([3, 2, 1, 3, 2, 1, "a", "b", "c", 3, 2, 1]);
        });
    });

    describe("function flatten", () => {
        it("should exist", () => {
            expect(itr.flatten).toBeFunction();
        });
        it("should visit an array", () => {
            const it = itr.flatten([a1, s1, "abc", m1.keys()]);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([3, 2, 1, 3, 2, 1, "a", "b", "c", 3, 2, 1]);
        });
        it("should visit a set", () => {
            const it = itr.flatten(new Set([a1, s1, "abc", m1.keys()]));
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([3, 2, 1, 3, 2, 1, "a", "b", "c", 3, 2, 1]);
        });
        it("should visit an iterator", () => {
            const it = itr.flatten([a1, s1, "abc", m1.keys()].values());
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([3, 2, 1, 3, 2, 1, "a", "b", "c", 3, 2, 1]);
        });
    });

    describe("function filter", () => {
        it("should exist", () => {
            expect(itr.filter).toBeFunction();
        });
        it("should visit an array", () => {
            const it = itr.filter(a1, v => v !== 2);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([3, 1]);
        });
        it("should visit a set", () => {
            const it = itr.filter(s1, v => v !== 2);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([3, 1]);
        });
        it("should visit a map", () => {
            const it = itr.filter(m1, e => e[0] !== 2);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([[3, "a"], [1, "c"]]);
        });
        it("should visit a string", () => {
            const it = itr.filter("abc", v => v !== "b");
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual(["a", "c"]);
        });
        it("should visit an iterator", () => {
            const it = itr.filter(m1.values(), v => v !== "b");
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual(["a", "c"]);
        });
    });

    describe("function map", () => {
        it("should exist", () => {
            expect(itr.map).toBeFunction();
        });
        it("should visit an array", () => {
            const it = itr.map(a1, v => (v === 2) ? undefined : -v);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([-3, -1]);
        });
        it("should visit a set", () => {
            const it = itr.map(s1, v => (v === 2) ? undefined : -v);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([-3, -1]);
        });
        it("should visit a map", () => {
            const it = itr.map(m1, e => (e[0] === 2) ? undefined : `${e[1]}1`);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual(["a1", "c1"]);
        });
        it("should visit a string", () => {
            const it = itr.map("abc", v => (v === "b") ? undefined : `${v}1`);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual(["a1", "c1"]);
        });
        it("should visit an iterator", () => {
            const it = itr.map(m1.values(), v => (v === "b") ? undefined : `${v}1`);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual(["a1", "c1"]);
        });
    });

    describe("function unify", () => {
        it("should exist", () => {
            expect(itr.unify).toBeFunction();
        });
        it("should unify an array", () => {
            const it = itr.unify([1, 3, 1, 2, 3]);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([1, 3, 2]);
        });
        it("should unify a string", () => {
            const it = itr.unify("acabc");
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual(["a", "c", "b"]);
        });
        it("should unify an iterator", () => {
            const it = itr.unify([1, 3, 1, 2, 3].values());
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([1, 3, 2]);
        });
    });

    describe("function merge", () => {
        it("should exist", () => {
            expect(itr.merge).toBeFunction();
        });
        it("should merge an array", () => {
            const it = itr.merge([1, 2, 3, 3, 3, 2, 1, 1], (v1, v2) => (v1 === v2) ? v1 : undefined);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([1, 2, 3, 2, 1]);
        });
        it("should merge an empty source", () => {
            const it = itr.merge([], v => v);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([]);
        });
    });
});
