/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as map from "@/io.ox/office/tk/algorithms/map";

// tests ======================================================================

describe("module tk/algorithms/map", () => {

    // public functions -------------------------------------------------------

    describe("function from", () => {
        it("should exist", () => {
            expect(map.from).toBeFunction();
        });
        it("should collect an array", () => {
            const a1 = ["a", "b", "c"];
            const result = map.from(a1, v => (v === "b") ? undefined : [v, `${v}${v}`]);
            expect(result).toBeInstanceOf(Map);
            expect(Array.from(result)).toEqual([["a", "aa"], ["c", "cc"]]);
        });
        it("should collect a set", () => {
            const s1 = ["a", "b", "c"];
            const result = map.from(s1, v => (v === "b") ? undefined : [v, `${v}${v}`]);
            expect(result).toBeInstanceOf(Map);
            expect(Array.from(result)).toEqual([["a", "aa"], ["c", "cc"]]);
        });
        it("should collect a map", () => {
            const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);
            const result = map.from(m1, ([k, v]) => (v === "b") ? undefined : [v, k]);
            expect(result).toBeInstanceOf(Map);
            expect(Array.from(result)).toEqual([["a", 3], ["c", 1]]);
        });
        it("should collect a string", () => {
            const result = map.from("abc", v => (v === "b") ? undefined : [v, `${v}${v}`]);
            expect(result).toBeInstanceOf(Map);
            expect(Array.from(result)).toEqual([["a", "aa"], ["c", "cc"]]);
        });
        it("should collect an iterator", () => {
            const a1 = ["a", "b", "c"];
            const result = map.from(a1.entries(), ([i, v]) => (v === "b") ? undefined : [i + 2, `${v}${v}`]);
            expect(result).toBeInstanceOf(Map);
            expect(Array.from(result)).toEqual([[2, "aa"], [4, "cc"]]);
        });
    });

    describe("function filterFrom", () => {
        it("should exist", () => {
            expect(map.filterFrom).toBeFunction();
        });
        it("should collect an array", () => {
            const a1 = [[1, "a"], [2, "b"], [3, "c"]];
            const result = map.filterFrom(a1, v => v !== "b");
            expect(result).toBeInstanceOf(Map);
            expect(Array.from(result)).toEqual([[1, "a"], [3, "c"]]);
        });
        it("should collect a map", () => {
            const m1 = new Map([[1, "a"], [2, "b"], [3, "c"]]);
            const result = map.filterFrom(m1, v => v !== "b");
            expect(result).toBeInstanceOf(Map);
            expect(result).not.toBe(m1);
            expect(Array.from(result)).toEqual([[1, "a"], [3, "c"]]);
        });
        it("should collect an iterator", () => {
            const a1 = ["a", "b", "c"];
            const result = map.filterFrom(a1.entries(), (_v, i) => i !== 1);
            expect(result).toBeInstanceOf(Map);
            expect(Array.from(result)).toEqual([[0, "a"], [2, "c"]]);
        });
    });

    describe("function yieldFrom", () => {
        it("should exist", () => {
            expect(map.yieldFrom).toBeFunction();
        });
        it("should collect the results of a generator", () => {
            const spy = jest.fn().mockReturnValue([["a", 42], ["b", 43]].values());
            const m1 = map.yieldFrom(spy, spy);
            expect(m1).toBeInstanceOf(Map);
            expect(Array.from(m1)).toEqual([["a", 42], ["b", 43]]);
            expect(spy).toHaveBeenCalledOnce();
            expect(spy).toHaveBeenCalledOn(spy);
        });
    });

    describe("function destroy", () => {
        it("should exist", () => {
            expect(map.destroy).toBeFunction();
        });
        it("should destroy map elements", () => {
            const spy = jest.fn();
            const obj = { destroy: spy };
            const m1 = new Map([[1, obj]]);
            map.destroy(m1);
            expect(spy).toHaveBeenCalled();
            expect(m1.size).toBe(0);
        });
    });

    describe("function toggle", () => {
        it("should exist", () => {
            expect(map.toggle).toBeFunction();
        });
        it("should insert or delete elements", () => {
            const m1 = new Map([[1, 42]]);
            expect(map.toggle(m1, 1, 3)).toBe(3);
            expect(m1.get(1)).toBe(3);
            expect(map.toggle(m1, 1, undefined)).toBeUndefined();
            expect(m1.has(1)).toBeFalse();
        });
        it("should insert or delete elements in a weak map", () => {
            const o1 = {};
            const m1 = new WeakMap([[o1, 42]]);
            expect(map.toggle(m1, o1, 3)).toBe(3);
            expect(m1.get(o1)).toBe(3);
            expect(map.toggle(m1, o1, undefined)).toBeUndefined();
            expect(m1.has(o1)).toBeFalse();
        });
    });

    describe("function remove", () => {
        it("should exist", () => {
            expect(map.remove).toBeFunction();
        });
        it("should remove elements", () => {
            const m1 = new Map([[1, "a"], [2, "b"]]);
            expect(map.remove(m1, 1)).toBe("a");
            expect(m1.has(1)).toBeFalse();
            expect(map.remove(m1, 3)).toBeUndefined();
        });
    });

    describe("function visit", () => {
        it("should exist", () => {
            expect(map.visit).toBeFunction();
        });
        it("should visit elements", () => {
            const m1 = new Map([[1, "a"], [2, "b"]]);
            const spy = jest.fn();
            map.visit(m1, 1, spy);
            expect(spy).toHaveBeenCalledWith("a", 1);
            expect(m1.get(1)).toBe("a");
            spy.mockClear();
            map.visit(m1, 3, spy);
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe("function upsert", () => {
        it("should exist", () => {
            expect(map.upsert).toBeFunction();
        });
        it("should return existing or create new elements", () => {
            const m1 = new Map([[1, 42]]);
            const spy = jest.fn(key => key * 2);
            expect(map.upsert(m1, 1, spy)).toBe(42);
            expect(spy).not.toHaveBeenCalled();
            expect(m1.get(2)).toBeUndefined();
            expect(map.upsert(m1, 2, spy)).toBe(4);
            expect(m1.get(2)).toBe(4);
            expect(spy).toHaveBeenCalledOnce();
        });
    });

    describe("function update", () => {
        it("should exist", () => {
            expect(map.update).toBeFunction();
        });
        it("should update elements", () => {
            const fn = n => (n === 42) ? undefined : ((n || 0) + 2);
            const m1 = new Map([[1, 2], [3, 42]]);
            expect(map.update(m1, 1, fn)).toBe(4);
            expect(map.update(m1, 2, fn)).toBe(2);
            expect(map.update(m1, 3, fn)).toBeUndefined();
            expect(m1.get(1)).toBe(4);
            expect(m1.get(2)).toBe(2);
            expect(m1.has(3)).toBeFalse();
        });
    });

    describe("function add", () => {
        it("should exist", () => {
            expect(map.add).toBeFunction();
        });
        it("should update elements", () => {
            const m1 = new Map([[1, 2], [3, 42]]);
            expect(map.add(m1, 1, 1)).toBe(3);
            expect(m1.get(1)).toBe(3);
            expect(map.add(m1, 1, -2)).toBe(1);
            expect(m1.get(1)).toBe(1);
            expect(map.add(m1, 2, 3)).toBe(3);
            expect(m1.get(2)).toBe(3);
        });
    });

    describe("function move", () => {
        it("should exist", () => {
            expect(map.move).toBeFunction();
        });
        it("should move an element", () => {
            const m1 = new Map([[1, "a"], [2, "b"], [3, "c"]]);
            expect(map.move(m1, 1, 3)).toBe("a");
            expect(m1.get(3)).toBe("a");
            expect(m1.has(1)).toBeFalse();
            expect(map.move(m1, 1, 3)).toBeUndefined();
            expect(m1.get(3)).toBe("a");
        });
    });

    describe("function assign", () => {
        it("should exist", () => {
            expect(map.assign).toBeFunction();
        });
        it("should assign entries to a map", () => {
            const m1 = new Map();
            expect(map.assign(m1, new Map([["a", 1], ["b", 2]]), null, new Map([["c", 3], ["d", 4]]).entries(), undefined, [["e", 5], ["f", 6]])).toBe(m1);
            expect(Array.from(m1)).toEqual([["a", 1], ["b", 2], ["c", 3], ["d", 4], ["e", 5], ["f", 6]]);
        });
        it("should overwrite existing entries", () => {
            const m1 = new Map([["a", 1], ["b", 2]]);
            map.assign(m1, [["b", 3], ["c", 4]]);
            expect(Array.from(m1)).toEqual([["a", 1], ["b", 3], ["c", 4]]);
        });
    });

    describe("function equals", () => {
        it("should exist", () => {
            expect(map.equals).toBeFunction();
        });
        it("should recognize references to the same map", () => {
            const m1 = new Map([["a", 2]]);
            expect(map.equals(m1, m1)).toBeTrue();
        });
        it("should recognize maps with equal elements", () => {
            const m1 = new Map([["a", 2], ["b", 1]]);
            const m2 = new Map([["b", 1], ["a", 2]]);
            expect(map.equals(m1, m2)).toBeTrue();
        });
        it("should recognize maps with unequal size or keys or elements", () => {
            const m1 = new Map([["a", 2], ["b", 1]]);
            const m2 = new Map([["a", 2], ["c", 1]]);
            const m3 = new Map([["a", 2], ["b", 2]]);
            const m4 = new Map([["a", 2]]);
            expect(map.equals(m1, m2)).toBeFalse();
            expect(map.equals(m1, m3)).toBeFalse();
            expect(map.equals(m1, m4)).toBeFalse();
            expect(map.equals(m2, m1)).toBeFalse();
            expect(map.equals(m3, m1)).toBeFalse();
            expect(map.equals(m4, m1)).toBeFalse();
        });
        it("should not compare maps deeply", () => {
            const m1 = new Map([["a", {}]]);
            const m2 = new Map([["a", {}]]);
            expect(map.equals(m1, m2)).toBeFalse();
        });
        it("should use a callback function", () => {
            function equality(a, b) { return a + 1 === b; }
            const m1 = new Map([["a", 1]]);
            const m2 = new Map([["a", 2]]);
            expect(map.equals(m1, m2, equality)).toBeTrue();
        });
    });

    describe("function first", () => {
        it("should exist", () => {
            expect(map.first).toBeFunction();
        });
        it("should return first element", () => {
            const m1 = new Map([[1, "a"], [2, "b"], [3, "c"]]);
            expect(map.first(m1)).toBe("a");
        });
        it("should return undefined for empty map", () => {
            const m1 = new Map();
            expect(map.first(m1)).toBeUndefined();
        });
    });

    describe("function find", () => {
        it("should exist", () => {
            expect(map.find).toBeFunction();
        });
        it("should return matching element", () => {
            const m1 = new Map([[1, "a"], [2, "b"], [3, "c"]]);
            const spy = jest.fn(v => v === "b");
            expect(map.find(m1, spy)).toBe("b");
            expect(spy).toHaveBeenCalledTimes(2);
        });
        it("should return undefined without matching element", () => {
            const m1 = new Map([[1, "a"], [2, "b"], [3, "c"]]);
            const spy = jest.fn(v => v === "d");
            expect(map.find(m1, spy)).toBeUndefined();
            expect(spy).toHaveBeenCalledTimes(3);
        });
    });

    describe("function findKey", () => {
        it("should exist", () => {
            expect(map.findKey).toBeFunction();
        });
        it("should return matching element", () => {
            const m1 = new Map([[1, "a"], [2, "b"], [3, "c"]]);
            const spy = jest.fn(v => v === "b");
            expect(map.findKey(m1, spy)).toBe(2);
            expect(spy).toHaveBeenCalledTimes(2);
        });
        it("should return undefined without matching element", () => {
            const m1 = new Map([[1, "a"], [2, "b"], [3, "c"]]);
            const spy = jest.fn(v => v === "d");
            expect(map.findKey(m1, spy)).toBeUndefined();
            expect(spy).toHaveBeenCalledTimes(3);
        });
    });

    describe("function findEntry", () => {
        it("should exist", () => {
            expect(map.findEntry).toBeFunction();
        });
        it("should return matching entry", () => {
            const m1 = new Map([[1, "a"], [2, "b"], [3, "c"]]);
            const spy = jest.fn(e => e[1] === "b");
            expect(map.findEntry(m1, spy)).toEqual([2, "b"]);
            expect(spy).toHaveBeenCalledTimes(2);
        });
        it("should return undefined without matching element", () => {
            const m1 = new Map([[1, "a"], [2, "b"], [3, "c"]]);
            const spy = jest.fn(e => e[1] === "d");
            expect(map.findEntry(m1, spy)).toBeUndefined();
            expect(spy).toHaveBeenCalledTimes(3);
        });
    });

    describe("function shiftValues", () => {
        it("should exist", () => {
            expect(map.shiftValues).toBeFunction();
        });
        it("should shift values from a map", () => {
            const m1 = new Map([[1, "a"], [2, "b"]]);
            const it = map.shiftValues(m1);
            expect(it).toBeIterator();
            expect(Array.from(m1)).toEqual([[1, "a"], [2, "b"]]);
            expect(it.next()).toEqual({ done: false, value: "a" });
            expect(Array.from(m1)).toEqual([[2, "b"]]);
            m1.set(3, "c");
            expect(it.next()).toEqual({ done: false, value: "b" });
            expect(Array.from(m1)).toEqual([[3, "c"]]);
            expect(it.next()).toEqual({ done: false, value: "c" });
            expect(Array.from(m1)).toEqual([]);
            expect(it.next()).toEqual({ done: true, value: undefined });
        });
    });

    describe("function shiftKeys", () => {
        it("should exist", () => {
            expect(map.shiftKeys).toBeFunction();
        });
        it("should shift keys from a map", () => {
            const m1 = new Map([[1, "a"], [2, "b"]]);
            const it = map.shiftKeys(m1);
            expect(it).toBeIterator();
            expect(Array.from(m1)).toEqual([[1, "a"], [2, "b"]]);
            expect(it.next()).toEqual({ done: false, value: 1 });
            expect(Array.from(m1)).toEqual([[2, "b"]]);
            m1.set(3, "c");
            expect(it.next()).toEqual({ done: false, value: 2 });
            expect(Array.from(m1)).toEqual([[3, "c"]]);
            expect(it.next()).toEqual({ done: false, value: 3 });
            expect(Array.from(m1)).toEqual([]);
            expect(it.next()).toEqual({ done: true, value: undefined });
        });
    });

    describe("function shiftEntries", () => {
        it("should exist", () => {
            expect(map.shiftEntries).toBeFunction();
        });
        it("should shift entries from a map", () => {
            const m1 = new Map([[1, "a"], [2, "b"]]);
            const it = map.shiftEntries(m1);
            expect(it).toBeIterator();
            expect(Array.from(m1)).toEqual([[1, "a"], [2, "b"]]);
            expect(it.next()).toEqual({ done: false, value: [1, "a"] });
            expect(Array.from(m1)).toEqual([[2, "b"]]);
            m1.set(3, "c");
            expect(it.next()).toEqual({ done: false, value: [2, "b"] });
            expect(Array.from(m1)).toEqual([[3, "c"]]);
            expect(it.next()).toEqual({ done: false, value: [3, "c"] });
            expect(Array.from(m1)).toEqual([]);
            expect(it.next()).toEqual({ done: true, value: undefined });
        });
    });
});
