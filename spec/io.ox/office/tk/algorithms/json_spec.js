/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as json from "@/io.ox/office/tk/algorithms/json";

// tests ======================================================================

describe("module tk/algorithms/json", () => {

    // public functions -------------------------------------------------------

    describe("function flatClone", () => {
        it("should exist", () => {
            expect(json.flatClone).toBeFunction();
        });
        it("should return scalars", () => {
            expect(json.flatClone(null)).toBeNull();
            expect(json.flatClone(42)).toBe(42);
            expect(json.flatClone("42")).toBe("42");
            expect(json.flatClone(true)).toBeTrue();
        });
        it("should clone arrays", () => {
            const source = [null, 42, "42", true, [1], { a: 1 }];
            const clone = json.flatClone(source);
            expect(clone).toEqual(source);
            expect(clone).not.toBe(source);
            for (const [i, e] of clone.entries()) {
                expect(e).toBe(source[i]);
            }
        });
        it("should clone objects", () => {
            const source = { a: null, b: 42, c: "42", d: true, e: [1], f: { a: 1 } };
            const clone = json.flatClone(source);
            expect(clone).toEqual(source);
            expect(clone).not.toBe(source);
            for (const [k, v] of Object.entries(clone)) {
                expect(v).toBe(source[k]);
            }
        });
    });

    describe("function deepClone", () => {
        it("should exist", () => {
            expect(json.deepClone).toBeFunction();
        });
        it("should return scalars", () => {
            expect(json.deepClone(null)).toBeNull();
            expect(json.deepClone(42)).toBe(42);
            expect(json.deepClone("42")).toBe("42");
            expect(json.deepClone(true)).toBeTrue();
        });
        it("should clone arrays", () => {
            const source = [null, 42, "42", true, [1], { a: 1 }];
            const clone = json.deepClone(source);
            expect(clone).toEqual(source);
            expect(clone).not.toBe(source);
            expect(clone[4]).not.toBe(source[4]);
            expect(clone[5]).not.toBe(source[5]);
        });
        it("should clone objects", () => {
            const source = { a: null, b: 42, c: "42", d: true, e: [1], f: { a: 1 } };
            const clone = json.deepClone(source);
            expect(clone).toEqual(source);
            expect(clone).not.toBe(source);
            expect(clone.e).not.toBe(source.e);
            expect(clone.f).not.toBe(source.f);
        });
    });

    describe("function tryParse", () => {
        it("should exist", () => {
            expect(json.tryParse).toBeFunction();
        });
        it("should parse valid JSON", () => {
            expect(json.tryParse("null")).toBeNull();
            expect(json.tryParse("42")).toBe(42);
            expect(json.tryParse('"ab\\"c"')).toBe('ab"c');
            expect(json.tryParse("{}")).toEqual({});
            expect(json.tryParse('{"a":42,"b":null}')).toEqual({ a: 42, b: null });
            expect(json.tryParse("[]")).toEqual([]);
            expect(json.tryParse("[42,null]")).toEqual([42, null]);
        });
        it("should throw for invalid JSON", () => {
            expect(json.tryParse.bind(null, "")).toThrow();
            expect(json.tryParse.bind(null, "invalid")).toThrow();
        });
    });

    describe("function safeParse", () => {
        it("should exist", () => {
            expect(json.safeParse).toBeFunction();
        });
        it("should parse valid JSON", () => {
            expect(json.safeParse("null")).toBeNull();
            expect(json.safeParse("42")).toBe(42);
            expect(json.safeParse('"ab\\"c"')).toBe('ab"c');
            expect(json.safeParse("{}")).toEqual({});
            expect(json.safeParse('{"a":42,"b":null}')).toEqual({ a: 42, b: null });
            expect(json.safeParse("[]")).toEqual([]);
            expect(json.safeParse("[42,null]")).toEqual([42, null]);
        });
        it("should return default for invalid JSON", () => {
            expect(json.safeParse("", 42)).toBe(42);
            expect(json.safeParse("invalid")).toBeUndefined();
        });
    });

    describe("function tryStringify", () => {
        it("should exist", () => {
            expect(json.tryStringify).toBeFunction();
        });
        it("should stringify valid JSON", () => {
            expect(json.tryStringify(null)).toBe("null");
            expect(json.tryStringify(42)).toBe("42");
            expect(json.tryStringify('ab"c')).toBe('"ab\\"c"');
            expect(json.tryStringify({})).toBe("{}");
            expect(json.tryStringify({ a: 42, b: null })).toBe('{"a":42,"b":null}');
            expect(json.tryStringify([])).toBe("[]");
            expect(json.tryStringify([42, null])).toBe("[42,null]");
        });
        it("should sort object keys", () => {
            expect(json.tryStringify({ a: 1, b: 2, c: 3 }, { sortKeys: true })).toBe('{"a":1,"b":2,"c":3}');
            expect(json.tryStringify({ b: 2, a: 1, c: 3 }, { sortKeys: true })).toBe('{"a":1,"b":2,"c":3}');
            expect(json.tryStringify({ c: 3, b: 2, a: 1 }, { sortKeys: true })).toBe('{"a":1,"b":2,"c":3}');
            expect(json.tryStringify([{ a: 1, b: 2, c: 3 }, { c: 3, b: 2, a: 1 }], { sortKeys: true })).toBe('[{"a":1,"b":2,"c":3},{"a":1,"b":2,"c":3}]');
            expect(json.tryStringify({ c: [2, 1], b: { b: 2, a: 1 }, a: [{ b: 2, a: 1 }] }, { sortKeys: true })).toBe('{"a":[{"a":1,"b":2}],"b":{"a":1,"b":2},"c":[2,1]}');
        });
        it("should generate indentation", () => {
            expect(json.tryStringify({ a: 1, b: 2, c: 3 }, { sortKeys: true, indent: 2 })).toBe('{\n  "a":1,\n  "b":2,\n  "c":3\n}');
            expect(json.tryStringify([{ a: 1, b: 2, c: 3 }, {}], { sortKeys: true, indent: 2 })).toBe('[\n  {\n    "a":1,\n    "b":2,\n    "c":3\n  },\n  {}\n]');
            expect(json.tryStringify({ c: [2, 1], b: { b: 2, a: [] }, a: [{ b: 2, a: 1 }] }, { sortKeys: true, indent: 2 })).toBe('{\n  "a":[\n    {\n      "a":1,\n      "b":2\n    }\n  ],\n  "b":{\n    "a":[],\n    "b":2\n  },\n  "c":[\n    2,\n    1\n  ]\n}');
        });
        it("should throw for invalid JSON", () => {
            const obj1 = {};
            const obj2 = { a: obj1 };
            obj1.b = obj2;
            expect(json.tryStringify.bind(null, obj2)).toThrow();
        });
    });

    describe("function safeStringify", () => {
        it("should exist", () => {
            expect(json.safeStringify).toBeFunction();
        });
        it("should stringify valid JSON", () => {
            expect(json.safeStringify(null)).toBe("null");
            expect(json.safeStringify(42)).toBe("42");
            expect(json.safeStringify('ab"c')).toBe('"ab\\"c"');
            expect(json.safeStringify({})).toBe("{}");
            expect(json.safeStringify({ a: 42, b: null })).toBe('{"a":42,"b":null}');
            expect(json.safeStringify([])).toBe("[]");
            expect(json.safeStringify([42, null])).toBe("[42,null]");
        });
        it("should sort object keys", () => {
            expect(json.safeStringify({ a: 1, b: 2, c: 3 }, { sortKeys: true })).toBe('{"a":1,"b":2,"c":3}');
            expect(json.safeStringify({ b: 2, a: 1, c: 3 }, { sortKeys: true })).toBe('{"a":1,"b":2,"c":3}');
            expect(json.safeStringify({ c: 3, b: 2, a: 1 }, { sortKeys: true })).toBe('{"a":1,"b":2,"c":3}');
            expect(json.safeStringify([{ a: 1, b: 2, c: 3 }, { c: 3, b: 2, a: 1 }], { sortKeys: true })).toBe('[{"a":1,"b":2,"c":3},{"a":1,"b":2,"c":3}]');
            expect(json.safeStringify({ c: [2, 1], b: { b: 2, a: 1 }, a: [{ b: 2, a: 1 }] }, { sortKeys: true })).toBe('{"a":[{"a":1,"b":2}],"b":{"a":1,"b":2},"c":[2,1]}');
        });
        it("should generate indentation", () => {
            expect(json.safeStringify({ a: 1, b: 2, c: 3 }, { sortKeys: true, indent: 2 })).toBe('{\n  "a":1,\n  "b":2,\n  "c":3\n}');
            expect(json.safeStringify([{ a: 1, b: 2, c: 3 }, {}], { sortKeys: true, indent: 2 })).toBe('[\n  {\n    "a":1,\n    "b":2,\n    "c":3\n  },\n  {}\n]');
            expect(json.safeStringify({ c: [2, 1], b: { b: 2, a: [] }, a: [{ b: 2, a: 1 }] }, { sortKeys: true, indent: 2 })).toBe('{\n  "a":[\n    {\n      "a":1,\n      "b":2\n    }\n  ],\n  "b":{\n    "a":[],\n    "b":2\n  },\n  "c":[\n    2,\n    1\n  ]\n}');
        });
        it("should return default for invalid JSON", () => {
            const obj1 = {};
            const obj2 = { a: obj1 };
            obj1.b = obj2;
            expect(json.safeStringify(obj2)).toBe("null");
            expect(json.safeStringify(obj2, { default: "abc" })).toBe("abc");
            expect(json.safeStringify(obj2, { default: "" })).toBe("");
        });
    });
});
