/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as pick from "@/io.ox/office/tk/algorithms/pick";

// tests ======================================================================

describe("module tk/algorithms/pick", () => {

    // public functions -------------------------------------------------------

    describe("function prop", () => {
        it("should exist", () => {
            expect(pick.prop).toBeFunction();
        });
        it("should return the property value", () => {
            expect(pick.prop(1, "a")).toBeUndefined();
            expect(pick.prop("a", "a")).toBeUndefined();
            expect(pick.prop(true, "a")).toBeUndefined();
            expect(pick.prop(null, "a")).toBeUndefined();
            expect(pick.prop({}, "a")).toBeUndefined();
            expect(pick.prop({ a: 42, b: 1 }, "a", "def")).toBe(42);
            expect(pick.prop({ b: 42, c: 1 }, "a")).toBeUndefined();
            expect(pick.prop([], "a")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(pick.prop({ a: 42, b: 1 }, "a", "def")).toBe(42);
            expect(pick.prop({ b: 1 }, "a", "def")).toBe("def");
        });
    });

    describe("function number", () => {
        it("should exist", () => {
            expect(pick.number).toBeFunction();
        });
        it("should return the property number value", () => {
            expect(pick.number(1, "a")).toBeUndefined();
            expect(pick.number({}, "a")).toBeUndefined();
            expect(pick.number({ a: 42, b: 1 }, "a")).toBe(42);
            expect(pick.number({ a: "42", b: 1 }, "a")).toBeUndefined();
            expect(pick.number({ a: "abc", b: 1 }, "a")).toBeUndefined();
            expect(pick.number({ b: 42, c: 1 }, "a")).toBeUndefined();
            expect(pick.number([], "a")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(pick.number({ a: 42, b: 1 }, "a", -1)).toBe(42);
            expect(pick.number({ a: "abc", b: 1 }, "a", -1)).toBe(-1);
            expect(pick.number({ b: 42, c: 1 }, "a", -1)).toBe(-1);
        });
    });

    describe("function bigint", () => {
        it("should exist", () => {
            expect(pick.bigint).toBeFunction();
        });
        it("should return the property number value", () => {
            expect(pick.bigint(1, "a")).toBeUndefined();
            expect(pick.bigint({}, "a")).toBeUndefined();
            expect(pick.bigint({ a: 42n, b: 1 }, "a")).toBe(42n);
            expect(pick.bigint({ a: 42, b: 1 }, "a")).toBeUndefined();
            expect(pick.bigint({ a: "abc", b: 1 }, "a")).toBeUndefined();
            expect(pick.bigint({ b: 42n, c: 1 }, "a")).toBeUndefined();
            expect(pick.bigint([], "a")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(pick.bigint({ a: 42n, b: 1 }, "a", -1n)).toBe(42n);
            expect(pick.bigint({ a: 42, b: 1 }, "a", -1n)).toBe(-1n);
            expect(pick.bigint({ b: 42n, c: 1 }, "a", -1n)).toBe(-1n);
        });
    });

    describe("function boolean", () => {
        it("should exist", () => {
            expect(pick.boolean).toBeFunction();
        });
        it("should return the property boolean value", () => {
            expect(pick.boolean(1, "a")).toBeUndefined();
            expect(pick.boolean({}, "a")).toBeUndefined();
            expect(pick.boolean({ a: true, b: 1 }, "a")).toBeTrue();
            expect(pick.boolean({ a: "abc", b: 1 }, "a")).toBeUndefined();
            expect(pick.boolean({ b: true, c: 1 }, "a")).toBeUndefined();
            expect(pick.boolean([], "a")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(pick.boolean({ a: true, b: 1 }, "a", false)).toBeTrue();
            expect(pick.boolean({ a: "abc", b: 1 }, "a", false)).toBeFalse();
            expect(pick.boolean({ b: true, c: 1 }, "a", false)).toBeFalse();
        });
    });

    describe("function string", () => {
        it("should exist", () => {
            expect(pick.string).toBeFunction();
        });
        it("should return the property string value", () => {
            expect(pick.string(1, "a")).toBeUndefined();
            expect(pick.string({}, "a")).toBeUndefined();
            expect(pick.string({ a: 42, b: 1 }, "a")).toBeUndefined();
            expect(pick.string({ a: "abc", b: 1 }, "a")).toBe("abc");
            expect(pick.string({ b: 42, c: 1 }, "a")).toBeUndefined();
            expect(pick.string([], "a")).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(pick.string({ a: 42, b: 1 }, "a", "def")).toBe("def");
            expect(pick.string({ a: "abc", b: 1 }, "a", "def")).toBe("abc");
            expect(pick.string({ b: 42, c: 1 }, "a", "def")).toBe("def");
        });
    });

    describe("function enum", () => {
        it("should exist", () => {
            expect(pick.enum).toBeFunction();
        });
        const Enum = { A: "a", B: "b" };
        var obj = { a: 42, b: Enum.B };
        it("should return the property enum value", () => {
            expect(pick.enum(1, "a", Enum)).toBeUndefined();
            expect(pick.enum({}, "a", Enum)).toBeUndefined();
            expect(pick.enum(obj, "a", Enum)).toBeUndefined();
            expect(pick.enum(obj, "b", Enum)).toBe(Enum.B);
            expect(pick.enum(obj, "c", Enum)).toBeUndefined();
            expect(pick.enum([], "a", Enum)).toBeUndefined();
        });
        it("should return the default value", () => {
            expect(pick.enum(obj, "a", Enum, Enum.A)).toBe(Enum.A);
            expect(pick.enum(obj, "b", Enum, Enum.A)).toBe(Enum.B);
            expect(pick.enum(obj, "c", Enum, Enum.A)).toBe(Enum.A);
        });
    });

    describe("function dict", () => {
        it("should exist", () => {
            expect(pick.dict).toBeFunction();
        });
        var obj = { a: 42, b: { x: 1 } };
        it("should return the property object value", () => {
            expect(pick.dict(1, "a")).toBeUndefined();
            expect(pick.dict({}, "a")).toBeUndefined();
            expect(pick.dict(obj, "a")).toBeUndefined();
            expect(pick.dict(obj, "b")).toBe(obj.b);
            expect(pick.dict(obj, "c")).toBeUndefined();
            expect(pick.dict([], "a")).toBeUndefined();
        });
        it("should return the default value", () => {
            var def = { x: 0 };
            expect(pick.dict(obj, "a", def)).toBe(def);
            expect(pick.dict(obj, "b", def)).toBe(obj.b);
            expect(pick.dict(obj, "c", def)).toBe(def);
            expect(pick.dict(obj, "a", true)).toEqual({});
            expect(pick.dict(obj, "b", true)).toBe(obj.b);
            expect(pick.dict(obj, "c", true)).toEqual({});
        });
    });

    describe("function array", () => {
        it("should exist", () => {
            expect(pick.array).toBeFunction();
        });
        var obj = { a: 42, b: [1] };
        it("should return the property array value", () => {
            expect(pick.array(1, "a")).toBeUndefined();
            expect(pick.array({}, "a")).toBeUndefined();
            expect(pick.array(obj, "a")).toBeUndefined();
            expect(pick.array(obj, "b")).toBe(obj.b);
            expect(pick.array(obj, "c")).toBeUndefined();
            expect(pick.array([], "a")).toBeUndefined();
        });
        it("should return the default value", () => {
            var def = [2, 3];
            expect(pick.array(obj, "a", def)).toBe(def);
            expect(pick.array(obj, "b", def)).toBe(obj.b);
            expect(pick.array(obj, "c", def)).toBe(def);
            expect(pick.array(obj, "a", true)).toEqual([]);
            expect(pick.array(obj, "b", true)).toBe(obj.b);
            expect(pick.array(obj, "c", true)).toEqual([]);
        });
    });

    describe("function function", () => {
        it("should exist", () => {
            expect(pick.function).toBeFunction();
        });
        var obj = { a: 42, b: () => false };
        it("should return the property array value", () => {
            expect(pick.function(1, "a")).toBeUndefined();
            expect(pick.function({}, "a")).toBeUndefined();
            expect(pick.function(obj, "a")).toBeUndefined();
            expect(pick.function(obj, "b")).toBe(obj.b);
            expect(pick.function(obj, "c")).toBeUndefined();
            expect(pick.function([], "a")).toBeUndefined();
        });
        it("should return the default value", () => {
            var def = () => false;
            expect(pick.function(obj, "a", def)).toBe(def);
            expect(pick.function(obj, "b", def)).toBe(obj.b);
            expect(pick.function(obj, "c", def)).toBe(def);
        });
    });
});
