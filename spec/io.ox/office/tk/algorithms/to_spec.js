/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as to from "@/io.ox/office/tk/algorithms/to";

// tests ======================================================================

describe("module tk/algorithms/to", () => {

    // public functions -------------------------------------------------------

    describe("function number", () => {
        it("should exist", () => {
            expect(to.number).toBeFunction();
        });
        it("should return numbers unmodified", () => {
            expect(to.number(42)).toBe(42);
            expect(to.number(42, 0)).toBe(42);
            expect(to.number(42, 2)).toBe(42);
        });
        it("should return default for other values", () => {
            expect(to.number("abc")).toBeUndefined();
            expect(to.number("abc", 0)).toBe(0);
            expect(to.number("abc", 2)).toBe(2);
            expect(to.number(42n, 2)).toBe(2);
            expect(to.number(true, 2)).toBe(2);
            expect(to.number(null, 2)).toBe(2);
            expect(to.number([42], 2)).toBe(2);
            expect(to.number({}, 2)).toBe(2);
        });
    });

    describe("function bigint", () => {
        it("should exist", () => {
            expect(to.bigint).toBeFunction();
        });
        it("should return numbers unmodified", () => {
            expect(to.bigint(42n)).toBe(42n);
            expect(to.bigint(42n, 0n)).toBe(42n);
            expect(to.bigint(42n, 2n)).toBe(42n);
        });
        it("should return default for other values", () => {
            expect(to.bigint("abc")).toBeUndefined();
            expect(to.bigint("abc", 0n)).toBe(0n);
            expect(to.bigint("abc", 2n)).toBe(2n);
            expect(to.bigint(42, 2n)).toBe(2n);
            expect(to.bigint(true, 2n)).toBe(2n);
            expect(to.bigint(null, 2n)).toBe(2n);
            expect(to.bigint([42], 2n)).toBe(2n);
            expect(to.bigint({}, 2n)).toBe(2n);
        });
    });

    describe("function string", () => {
        it("should exist", () => {
            expect(to.string).toBeFunction();
        });
        it("should return strings unmodified", () => {
            expect(to.string("abc")).toBe("abc");
            expect(to.string("abc", "")).toBe("abc");
            expect(to.string("abc", "def")).toBe("abc");
        });
        it("should return default for other values", () => {
            expect(to.string(42)).toBeUndefined();
            expect(to.string(42, "")).toBe("");
            expect(to.string(42, "def")).toBe("def");
            expect(to.string(42n, "def")).toBe("def");
            expect(to.string(true, "def")).toBe("def");
            expect(to.string(null, "def")).toBe("def");
            expect(to.string(["abc"], "def")).toBe("def");
            expect(to.string({}, "def")).toBe("def");
        });
    });

    describe("function enum", () => {
        it("should exist", () => {
            expect(to.enum).toBeFunction();
        });
        var EnumType = { E1: "e1", E2: "e2" };
        it("should cast existing enums", () => {
            expect(to.enum(EnumType, "e1")).toBe(EnumType.E1);
            expect(to.enum(EnumType, "e2")).toBe(EnumType.E2);
        });
        const ArrayType = ["e1", "e2"];
        it("should cast enum arrays", () => {
            expect(to.enum(ArrayType, "e1")).toBe("e1");
            expect(to.enum(ArrayType, "e2")).toBe("e2");
        });
        it("should return the default value for other values", () => {
            expect(to.enum(EnumType, "")).toBeUndefined();
            expect(to.enum(EnumType, "e3")).toBeUndefined();
            expect(to.enum(EnumType, "e3", EnumType.E2)).toBe(EnumType.E2);
            expect(to.enum(EnumType, null)).toBeUndefined();
            expect(to.enum(EnumType, null, EnumType.E2)).toBe(EnumType.E2);
            expect(to.enum(EnumType, 42)).toBeUndefined();
            expect(to.enum(EnumType, 42, EnumType.E2)).toBe(EnumType.E2);
            expect(to.enum(EnumType, 42n, EnumType.E2)).toBe(EnumType.E2);
            expect(to.enum(ArrayType, "")).toBeUndefined();
            expect(to.enum(ArrayType, "e3")).toBeUndefined();
            expect(to.enum(ArrayType, "e3", "e2")).toBe("e2");
            expect(to.enum(ArrayType, null)).toBeUndefined();
            expect(to.enum(ArrayType, null, "e2")).toBe("e2");
            expect(to.enum(ArrayType, 42)).toBeUndefined();
            expect(to.enum(ArrayType, 42, "e2")).toBe("e2");
            expect(to.enum(ArrayType, 42n, "e2")).toBe("e2");
        });
    });

    describe("function boolean", () => {
        it("should exist", () => {
            expect(to.boolean).toBeFunction();
        });
        it("should return booleans unmodified", () => {
            expect(to.boolean(true)).toBeTrue();
            expect(to.boolean(false)).toBeFalse();
            expect(to.boolean(true, false)).toBeTrue();
            expect(to.boolean(false, true)).toBeFalse();
        });
        it("should return default for other values", () => {
            expect(to.boolean(42)).toBeUndefined();
            expect(to.boolean(42, false)).toBeFalse();
            expect(to.boolean(42, true)).toBeTrue();
            expect(to.boolean(42n, true)).toBeTrue();
            expect(to.boolean("abc", true)).toBeTrue();
            expect(to.boolean(null, true)).toBeTrue();
            expect(to.boolean([false], true)).toBeTrue();
            expect(to.boolean({}, true)).toBeTrue();
        });
    });

    describe("function symbol", () => {
        it("should exist", () => {
            expect(to.symbol).toBeFunction();
        });
        it("should return symbols unmodified", () => {
            const symbol1 = Symbol("s1"), symbol2 = Symbol("s2");
            expect(to.symbol(symbol1)).toBe(symbol1);
            expect(to.symbol(symbol2)).toBe(symbol2);
            expect(to.symbol(symbol1, symbol2)).toBe(symbol1);
        });
        it("should return default for other values", () => {
            const symbol = Symbol("s1");
            expect(to.symbol(42)).toBeUndefined();
            expect(to.symbol(42, symbol)).toBe(symbol);
            expect(to.symbol(42n)).toBeUndefined();
            expect(to.symbol("abc")).toBeUndefined();
            expect(to.symbol(null)).toBeUndefined();
            expect(to.symbol(false)).toBeUndefined();
            expect(to.symbol([])).toBeUndefined();
            expect(to.symbol(/a/)).toBeUndefined();
        });
    });

    describe("function dict", () => {
        it("should exist", () => {
            expect(to.dict).toBeFunction();
        });
        it("should return dictionaries unmodified", () => {
            var dict1 = {}, dict2 = { a: 42 };
            expect(to.dict(dict1)).toBe(dict1);
            expect(to.dict(dict1, dict2)).toBe(dict1);
            expect(to.dict(dict1, true)).toBe(dict1);
            expect(to.dict(dict2)).toBe(dict2);
            expect(to.dict(dict2, dict1)).toBe(dict2);
            expect(to.dict(dict2, true)).toBe(dict2);
        });
        it("should return default for other values", () => {
            var dict1 = {};
            expect(to.dict(42)).toBeUndefined();
            expect(to.dict(42, dict1)).toBe(dict1);
            expect(to.dict(42, true)).toEqual({});
            expect(to.dict(42n, true)).toEqual({});
            expect(to.dict("abc")).toBeUndefined();
            expect(to.dict(null)).toBeUndefined();
            expect(to.dict(false)).toBeUndefined();
            expect(to.dict([])).toBeUndefined();
            expect(to.dict(/a/)).toBeUndefined();
        });
    });

    describe("function array", () => {
        it("should exist", () => {
            expect(to.array).toBeFunction();
        });
        it("should return arrays unmodified", () => {
            var arr1 = [], arr2 = ["a", 42];
            expect(to.array(arr1)).toBe(arr1);
            expect(to.array(arr1, arr2)).toBe(arr1);
            expect(to.array(arr1, true)).toBe(arr1);
            expect(to.array(arr2)).toBe(arr2);
            expect(to.array(arr2, arr1)).toBe(arr2);
            expect(to.array(arr2, true)).toBe(arr2);
        });
        it("should return default for other values", () => {
            var arr1 = [];
            expect(to.array(42)).toBeUndefined();
            expect(to.array(42, arr1)).toBe(arr1);
            expect(to.array(42, true)).toEqual([]);
            expect(to.array(42n, true)).toEqual([]);
            expect(to.array("abc")).toBeUndefined();
            expect(to.array(null)).toBeUndefined();
            expect(to.array(false)).toBeUndefined();
            expect(to.array({})).toBeUndefined();
            expect(to.array(/a/)).toBeUndefined();
        });
    });

    describe("function function", () => {
        it("should exist", () => {
            expect(to.function).toBeFunction();
        });
        it("should return functions unmodified", () => {
            function f1() {}
            function f2() {}
            expect(to.function(f1)).toBe(f1);
            expect(to.function(f1, f2)).toBe(f1);
            expect(to.function(f2)).toBe(f2);
            expect(to.function(f2, f1)).toBe(f2);
        });
        it("should return default for other values", () => {
            function f1() {}
            expect(to.function(42)).toBeUndefined();
            expect(to.function(42, f1)).toBe(f1);
            expect(to.function(42n, f1)).toBe(f1);
            expect(to.function("abc")).toBeUndefined();
            expect(to.function(null)).toBeUndefined();
            expect(to.function(false)).toBeUndefined();
            expect(to.function({})).toBeUndefined();
            expect(to.function([])).toBeUndefined();
            expect(to.function(/a/)).toBeUndefined();
        });
    });
});
