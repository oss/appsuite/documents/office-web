/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as fun from "@/io.ox/office/tk/algorithms/function";

// tests ======================================================================

describe("module tk/algorithms/function", () => {

    // public functions -------------------------------------------------------

    describe("function const", () => {
        it("should exist", () => {
            expect(fun.const).toBeFunction();
        });
        it("should return a constant function", () => {
            const fn = fun.const([]);
            expect(fn).toBeFunction();
            expect(fn()).toBeArray();
            expect(fn()).toBe(fn()); // always the same array
        });
    });

    describe("function undef", () => {
        it("should exist", () => {
            expect(fun.undef).toBeFunction();
        });
        it("should return false", () => {
            expect(fun.undef()).toBeUndefined();
            expect(fun.undef()).toBeUndefined();
        });
    });

    describe("function null", () => {
        it("should exist", () => {
            expect(fun.null).toBeFunction();
        });
        it("should return false", () => {
            expect(fun.null()).toBeNull();
            expect(fun.null()).toBeNull();
        });
    });

    describe("function false", () => {
        it("should exist", () => {
            expect(fun.false).toBeFunction();
        });
        it("should return false", () => {
            expect(fun.false()).toBeFalse();
            expect(fun.false()).toBeFalse();
        });
    });

    describe("function true", () => {
        it("should exist", () => {
            expect(fun.true).toBeFunction();
        });
        it("should return false", () => {
            expect(fun.true()).toBeTrue();
            expect(fun.true()).toBeTrue();
        });
    });

    describe("function not", () => {
        it("should exist", () => {
            expect(fun.not).toBeFunction();
        });
        it("should return the negated value", () => {
            expect(fun.not(true)).toBeFalse();
            expect(fun.not(false)).toBeTrue();
        });
    });

    describe("function identity", () => {
        it("should exist", () => {
            expect(fun.identity).toBeFunction();
        });
        it("should return first argument", () => {
            expect(fun.identity(1, 2, 3, 4)).toBe(1);
            expect(fun.identity()).toBeUndefined();
        });
    });

    describe("function pluck2nd", () => {
        it("should exist", () => {
            expect(fun.pluck2nd).toBeFunction();
        });
        it("should return second argument", () => {
            expect(fun.pluck2nd(1, 2, 3, 4)).toBe(2);
            expect(fun.pluck2nd(1)).toBeUndefined();
        });
    });

    describe("function pluck3rd", () => {
        it("should exist", () => {
            expect(fun.pluck3rd).toBeFunction();
        });
        it("should return third argument", () => {
            expect(fun.pluck3rd(1, 2, 3, 4)).toBe(3);
            expect(fun.pluck3rd(1, 2)).toBeUndefined();
        });
    });

    describe("function pluck4th", () => {
        it("should exist", () => {
            expect(fun.pluck4th).toBeFunction();
        });
        it("should return fourth argument", () => {
            expect(fun.pluck4th(1, 2, 3, 4)).toBe(4);
            expect(fun.pluck4th(1, 2, 3)).toBeUndefined();
        });
    });

    describe("function do", () => {
        it("should exist", () => {
            expect(fun.do).toBeFunction();
        });
        it("should invoke callback", () => {
            const spy = jest.fn().mockReturnValue(42);
            expect(fun.do(spy)).toBe(42);
            expect(spy).toHaveBeenCalledOnce();
        });
    });

    describe("function yield", () => {
        it("should exist", () => {
            expect(fun.yield).toBeFunction();
        });
        it("should invoke callback", () => {
            const spy = jest.fn().mockReturnValue([42].values());
            const iter = fun.yield(spy, spy);
            expect(iter).toBeIterator();
            expect(spy).not.toHaveBeenCalled();
            expect(Array.from(iter)).toEqual([42]);
            expect(spy).toHaveBeenCalledOnce();
            expect(spy).toHaveBeenCalledOn(spy);
        });
    });

    describe("function try", () => {
        it("should exist", () => {
            expect(fun.try).toBeFunction();
        });
        it("should invoke callback", () => {
            const spy = jest.fn().mockReturnValue(42);
            expect(fun.try(spy, -1)).toBe(42);
            expect(spy).toHaveBeenCalledOnce();
        });
        it("should return default value on exception", () => {
            const spy = jest.fn().mockImplementation(() => { throw new Error(); });
            expect(fun.try(spy, 42)).toBe(42);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(fun.try(spy)).toBeUndefined();
            expect(spy).toHaveBeenCalledTimes(2);
        });
    });

    describe("function guard", () => {
        it("should exist", () => {
            expect(fun.guard).toBeFunction();
        });
        it("should not invoke function recursively", () => {
            const fn = fun.guard(n => { fn(n + 1); return n; });
            const spy = jest.fn(fn);
            expect(spy).not.toHaveBeenCalled();
            expect(spy(42)).toBe(42);
            expect(spy).toHaveBeenCalledOnce();
        });
    });

    describe("function once", () => {
        it("should exist", () => {
            expect(fun.once).toBeFunction();
        });
        it("should invoke function once, and always return cached result", () => {
            const spy = jest.fn(fun.identity);
            const wrapped = fun.once(spy);
            expect(spy).not.toHaveBeenCalled();
            expect(wrapped("a")).toBe("a");
            expect(spy).toHaveBeenCalledOnce();
            expect(wrapped("b")).toBe("a");
            expect(spy).toHaveBeenCalledOnce();
        });
        it("should invoke function once, and always throw cached exception", () => {
            const err = new Error();
            const spy = jest.fn().mockImplementation(() => { throw new Error(); });
            const wrapped = fun.once(spy);
            expect(spy).not.toHaveBeenCalled();
            expect(wrapped).toThrow(err);
            expect(spy).toHaveBeenCalledOnce();
            expect(wrapped).toThrow(err);
            expect(spy).toHaveBeenCalledOnce();
        });
    });

    describe("function memoize", () => {
        it("should exist", () => {
            expect(fun.memoize).toBeFunction();
        });
        it("should invoke hasher and function, and always return cached result", () => {
            const fnSpy = jest.fn(fun.identity);
            const hashSpy = jest.fn(fun.identity);
            const wrapped = fun.memoize(fnSpy, hashSpy);
            expect(fnSpy).not.toHaveBeenCalled();
            expect(hashSpy).not.toHaveBeenCalled();
            expect(wrapped("a")).toBe("a");
            expect(fnSpy).toHaveBeenCalledTimes(1);
            expect(hashSpy).toHaveBeenCalledTimes(1);
            expect(wrapped("b")).toBe("b");
            expect(fnSpy).toHaveBeenCalledTimes(2);
            expect(hashSpy).toHaveBeenCalledTimes(2);
            expect(wrapped("a")).toBe("a");
            expect(fnSpy).toHaveBeenCalledTimes(2);
            expect(hashSpy).toHaveBeenCalledTimes(3);
        });
        it("should invoke hasher and function, and always throw cached exception", () => {
            const err = new Error();
            const fnSpy = jest.fn().mockImplementation(() => { throw new Error(); });
            const hashSpy = jest.fn(fun.identity);
            const wrapped = fun.memoize(fnSpy, hashSpy);
            expect(fnSpy).not.toHaveBeenCalled();
            expect(hashSpy).not.toHaveBeenCalled();
            expect(() => wrapped("a")).toThrow(err);
            expect(fnSpy).toHaveBeenCalledTimes(1);
            expect(hashSpy).toHaveBeenCalledTimes(1);
            expect(() => wrapped("b")).toThrow(err);
            expect(fnSpy).toHaveBeenCalledTimes(2);
            expect(hashSpy).toHaveBeenCalledTimes(2);
            expect(() => wrapped("a")).toThrow(err);
            expect(fnSpy).toHaveBeenCalledTimes(2);
            expect(hashSpy).toHaveBeenCalledTimes(3);
        });
    });
});
