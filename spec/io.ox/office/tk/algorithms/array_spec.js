/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import * as math from "@/io.ox/office/tk/algorithms/math";
import * as ary from "@/io.ox/office/tk/algorithms/array";

// constants ==================================================================

// array-like objects
const AL0 = { length: 0 };
const AL1 = { length: 3, 0: 1, 1: 2, 2: 3 };

// functions ==================================================================

// generates a predicate function that returns whether its parameter is greater than "value"
function greater(value) { return p => p > value; }

// tests ======================================================================

describe("module tk/algorithms/array", () => {

    // public functions -------------------------------------------------------

    describe("function from", () =>  {
        it("should exist", () => {
            expect(ary.from).toBeFunction();
        });
        it("should collect an array", () => {
            const ARRAY = ["a", "b", "c"];
            const result = ary.from(ARRAY, v => (v === "b") ? undefined : `${v}${v}`);
            expect(result).not.toBe(ARRAY);
            expect(result).toEqual(["aa", "cc"]);
        });
        it("should collect a set", () => {
            const s1 = new Set(["a", "b", "c"]);
            const result = ary.from(s1, v => (v === "b") ? undefined : `${v}${v}`);
            expect(result).toEqual(["aa", "cc"]);
        });
        it("should collect a map", () => {
            const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);
            const result = ary.from(m1, ([k, v]) => (v === "b") ? undefined : `${k}${v}`);
            expect(result).toEqual(["3a", "1c"]);
        });
        it("should collect a string", () => {
            const result = ary.from("abc", v => (v === "b") ? undefined : `${v}${v}`);
            expect(result).toEqual(["aa", "cc"]);
        });
        it("should collect an iterator", () => {
            const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);
            const result = ary.from(m1.values(), v => (v === "b") ? undefined : `${v}${v}`);
            expect(result).toEqual(["aa", "cc"]);
        });
    });

    describe("function filterFrom", () => {
        it("should exist", () => {
            expect(ary.filterFrom).toBeFunction();
        });
        it("should collect an array", () => {
            const ARRAY = ["a", "b", "c"];
            const result = ary.filterFrom(ARRAY, v => v !== "b");
            expect(result).not.toBe(ARRAY);
            expect(result).toEqual(["a", "c"]);
        });
        it("should collect a set", () => {
            const s1 = new Set(["a", "b", "c"]);
            const result = ary.filterFrom(s1, v => v !== "b");
            expect(result).toEqual(["a", "c"]);
        });
        it("should collect a map", () => {
            const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);
            const result = ary.filterFrom(m1, ([_k, v]) => v !== "b");
            expect(result).toEqual([[3, "a"], [1, "c"]]);
        });
        it("should collect a string", () => {
            const result = ary.filterFrom("abc", v => v !== "b");
            expect(result).toEqual(["a", "c"]);
        });
        it("should collect an iterator", () => {
            const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);
            const result = ary.filterFrom(m1.values(), v => v !== "b");
            expect(result).toEqual(["a", "c"]);
        });
    });

    describe("function flatFrom", () => {
        it("should exist", () => {
            expect(ary.flatFrom).toBeFunction();
        });
        it("should collect an array", () => {
            const ARRAY = ["a", "b", "c"];
            const result = ary.flatFrom(ARRAY, v => (v === "b") ? undefined : [v, `${v}:${v}`]);
            expect(result).not.toBe(ARRAY);
            expect(result).toEqual(["a", "a:a", "c", "c:c"]);
        });
        it("should collect a set", () => {
            const s1 = new Set(["a", "b", "c"]);
            const result = ary.flatFrom(s1, v => (v === "b") ? undefined : new Set([v, `${v}:${v}`]));
            expect(result).toEqual(["a", "a:a", "c", "c:c"]);
        });
        it("should collect a map", () => {
            const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);
            const result = ary.flatFrom(m1, ([k, v]) => (v === "b") ? undefined : [k, v]);
            expect(result).toEqual([3, "a", 1, "c"]);
        });
        it("should collect a string", () => {
            const result = ary.flatFrom("abc", v => (v === "b") ? undefined : `${v}:${v}`);
            expect(result).toEqual(["a", ":", "a", "c", ":", "c"]);
        });
        it("should collect an iterator", () => {
            const m1 = new Map([[3, "a"], [2, "b"], [1, "c"]]);
            const result = ary.flatFrom(m1.values(), v => (v === "b") ? undefined : [v, `${v}:${v}`]);
            expect(result).toEqual(["a", "a:a", "c", "c:c"]);
        });
    });

    describe("function sortFrom", () => {
        it("should exist", () => {
            expect(ary.sortFrom).toBeFunction();
        });
        it("should collect an array", () => {
            const ARRAY = [1, 3, 2];
            const result = ary.sortFrom(ARRAY, v => -v);
            expect(result).not.toBe(ARRAY);
            expect(result).toEqual([3, 2, 1]);
        });
        it("should collect a set", () => {
            const s1 = new Set([1, 3, 2]);
            const result = ary.sortFrom(s1, v => -v);
            expect(result).toEqual([3, 2, 1]);
        });
        it("should collect a string", () => {
            const result = ary.sortFrom("acb", v => v);
            expect(result).toEqual(["a", "b", "c"]);
        });
        it("should collect an iterator", () => {
            const m1 = new Map([[1, "c"], [3, "a"], [2, "b"]]);
            const result = ary.sortFrom(m1.keys(), k => -k);
            expect(result).toEqual([3, 2, 1]);
        });
    });

    describe("function yieldFrom", () => {
        it("should exist", () => {
            expect(ary.yieldFrom).toBeFunction();
        });
        it("should collect the results of a generator", () => {
            const spy = jest.fn().mockReturnValue([42, "a"].values());
            const a1 = ary.yieldFrom(spy, spy);
            expect(a1).toBeArray();
            expect(Array.from(a1)).toEqual([42, "a"]);
            expect(spy).toHaveBeenCalledOnce();
            expect(spy).toHaveBeenCalledOn(spy);
        });
    });

    describe("function wrap", () => {
        it("should exist", () => {
            expect(ary.wrap).toBeFunction();
        });
        it("should return arrays unmodified", () => {
            const ARRAY = [1];
            expect(ary.wrap(ARRAY)).toBe(ARRAY);
        });
        it("should wrap other values in an array", () => {
            expect(ary.wrap(42)).toEqual([42]);
            expect(ary.wrap("abc")).toEqual(["abc"]);
            expect(ary.wrap(undefined)).toEqual([]);
            const v1 = { a: 1 }, a1 = ary.wrap(v1);
            expect(a1).toBeArrayOfSize(1);
            expect(a1[0]).toBe(v1);
            const v2 = $("body"), a2 = ary.wrap(v2);
            expect(a2).toBeArrayOfSize(1);
            expect(a2[0]).toBe(v2);
        });
    });

    describe("function concat", () => {
        it("should exist", () => {
            expect(ary.concat).toBeFunction();
        });
        it("should concatenate values", () => {
            expect(ary.concat(1, undefined, [2], null, [3, 4])).toEqual([1, 2, null, 3, 4]);
            const ARRAY = [1];
            expect(ary.concat(ARRAY)).not.toBe(ARRAY);
        });
    });

    describe("function sequence", () => {
        it("should exist", () => {
            expect(ary.sequence).toBeFunction();
        });
        it("should fill an array", () => {
            expect(ary.sequence(3)).toEqual([0, 1, 2]);
            expect(ary.sequence(3, 2)).toEqual([2, 3, 4]);
            expect(ary.sequence(3, 2, 2)).toEqual([2, 4, 6]);
            expect(ary.sequence(3, 2, 0)).toEqual([2, 2, 2]);
            expect(ary.sequence(3, -1, -1)).toEqual([-1, -2, -3]);
            expect(ary.sequence(0, 2)).toEqual([]);
        });
    });

    describe("function fill", () => {
        it("should exist", () => {
            expect(ary.fill).toBeFunction();
        });
        it("should fill an array", () => {
            expect(ary.fill(3, 1)).toEqual([1, 1, 1]);
            expect(ary.fill(2, "a")).toEqual(["a", "a"]);
            expect(ary.fill(0, 42)).toEqual([]);
        });
    });

    describe("function generate", () => {
        it("should exist", () => {
            expect(ary.generate).toBeFunction();
        });
        it("should create an array", () => {
            const fn = i => i * 2;
            expect(ary.generate(3, fn)).toEqual([0, 2, 4]);
            expect(ary.generate(0, fn)).toEqual([]);
        });
    });

    describe("function equals", () => {
        it("should exist", () => {
            expect(ary.equals).toBeFunction();
        });
        it("should recognize references to the same array", () => {
            const ARRAY = [1, 2];
            expect(ary.equals(ARRAY, ARRAY)).toBeTrue();
        });
        it("should recognize arrays with equal elements", () => {
            const obj = {};
            expect(ary.equals([1, obj], [1, obj])).toBeTrue();
        });
        it("should recognize arrays with unequal length", () => {
            expect(ary.equals([1, 2], [1])).toBeFalse();
            expect(ary.equals([1], [1, 2])).toBeFalse();
        });
        it("should recognize arrays with unequal elements", () => {
            expect(ary.equals([1, 2], [1, 3])).toBeFalse();
            expect(ary.equals([2, 1], [3, 1])).toBeFalse();
        });
        it("should not compare arrays deeply", () => {
            expect(ary.equals([1, [2, 3]], [1, [2, 3]])).toBeFalse();
        });
        it("should use a callback function", () => {
            function equality(a, b) { return a + 1 === b; }
            expect(ary.equals([1, 2], [2, 3], equality)).toBeTrue();
        });
    });

    describe("function compare", () => {
        it("should exist", () => {
            expect(ary.compare).toBeFunction();
        });
        const cmp = (n1, n2) => n1 - n2;
        const rev = (n1, n2) => n2 - n1;
        it("should recognize equal arrays", () => {
            expect(ary.compare([1], [1])).toBe(0);
            expect(ary.compare([1], [1], cmp)).toBe(0);
            expect(ary.compare([1], [1], rev)).toBe(0);
            expect(ary.compare([1, 2, 3, 4], [1, 2, 3, 4])).toBe(0);
            expect(ary.compare([1, 2, 3, 4], [1, 2, 3, 4], cmp)).toBe(0);
            expect(ary.compare([1, 2, 3, 4], [1, 2, 3, 4], rev)).toBe(0);
        });
        it("should recognize different arrays", () => {
            expect(ary.compare([1], [2])).toBe(-1);
            expect(ary.compare([1], [2], cmp)).toBe(-1);
            expect(ary.compare([1], [2], rev)).toBe(1);
            expect(ary.compare([2], [1])).toBe(1);
            expect(ary.compare([2], [1], cmp)).toBe(1);
            expect(ary.compare([2], [1], rev)).toBe(-1);
            expect(ary.compare([1, 2, 3, 4], [1, 2, 4, 3])).toBe(-1);
            expect(ary.compare([1, 2, 3, 4], [1, 2, 4, 3], cmp)).toBe(-1);
            expect(ary.compare([1, 2, 3, 4], [1, 2, 4, 3], rev)).toBe(1);
            expect(ary.compare([1, 2, 4, 3], [1, 2, 3, 4])).toBe(1);
            expect(ary.compare([1, 2, 4, 3], [1, 2, 3, 4], cmp)).toBe(1);
            expect(ary.compare([1, 2, 4, 3], [1, 2, 3, 4], rev)).toBe(-1);
        });
        it("should consider extra array elements", () => {
            expect(ary.compare([1, 2, 3], [1, 2, 3, 4])).toBe(-1);
            expect(ary.compare([1, 2, 3], [1, 2, 3, 4], cmp)).toBe(-1);
            expect(ary.compare([1, 2, 3], [1, 2, 3, 4], rev)).toBe(-1);
            expect(ary.compare([1, 2, 3, 4], [1, 2, 3])).toBe(1);
            expect(ary.compare([1, 2, 3, 4], [1, 2, 3], cmp)).toBe(1);
            expect(ary.compare([1, 2, 3, 4], [1, 2, 3], rev)).toBe(1);
        });
        it("should accept empty arrays", () => {
            expect(ary.compare([], [], rev)).toBe(0);
            expect(ary.compare([], [1], rev)).toBe(-1);
            expect(ary.compare([1], [], rev)).toBe(1);
        });
    });

    describe("function entries", () => {
        it("should exist", () => {
            expect(ary.entries).toBeFunction();
        });
        it("should visit array elements", () => {
            const it1 = ary.entries(AL1);
            expect(it1).toBeIterator();
            expect(Array.from(it1)).toEqual([[0, 1], [1, 2], [2, 3]]);
            const it2 = ary.entries(AL1, { begin: 1 });
            expect(Array.from(it2)).toEqual([[1, 2], [2, 3]]);
            const it3 = ary.entries(AL1, { end: 2 });
            expect(Array.from(it3)).toEqual([[0, 1], [1, 2]]);
            const it4 = ary.entries(AL1, { reverse: true });
            expect(Array.from(it4)).toEqual([[2, 3], [1, 2], [0, 1]]);
            const it5 = ary.entries(AL1, { begin: 1, reverse: true });
            expect(Array.from(it5)).toEqual([[1, 2], [0, 1]]);
            const it6 = ary.entries(AL1, { end: 0, reverse: true });
            expect(Array.from(it6)).toEqual([[2, 3], [1, 2]]);
        });
    });

    describe("function values", () => {
        it("should exist", () => {
            expect(ary.values).toBeFunction();
        });
        it("should visit array elements", () => {
            const it1 = ary.values(AL1);
            expect(it1).toBeIterator();
            expect(Array.from(it1)).toEqual([1, 2, 3]);
            const it2 = ary.values(AL1, { begin: 1 });
            expect(Array.from(it2)).toEqual([2, 3]);
            const it3 = ary.values(AL1, { end: 2 });
            expect(Array.from(it3)).toEqual([1, 2]);
            const it4 = ary.values(AL1, { reverse: true });
            expect(Array.from(it4)).toEqual([3, 2, 1]);
            const it5 = ary.values(AL1, { begin: 1, reverse: true });
            expect(Array.from(it5)).toEqual([2, 1]);
            const it6 = ary.values(AL1, { end: 0, reverse: true });
            expect(Array.from(it6)).toEqual([3, 2]);
        });
    });

    describe("function interval", () => {
        it("should exist", () => {
            expect(ary.interval).toBeFunction();
        });
        it("should visit array elements", () => {
            const it1 = ary.interval(AL1, 0, 2);
            expect(it1).toBeIterator();
            expect(Array.from(it1)).toEqual([1, 2, 3]);
            const it2 = ary.interval(AL1, 1, 2);
            expect(Array.from(it2)).toEqual([2, 3]);
            const it3 = ary.interval(AL1, 0, 1);
            expect(Array.from(it3)).toEqual([1, 2]);
            const it4 = ary.interval(AL1, 0, 2, true);
            expect(Array.from(it4)).toEqual([3, 2, 1]);
            const it5 = ary.interval(AL1, 0, 1, true);
            expect(Array.from(it5)).toEqual([2, 1]);
            const it6 = ary.interval(AL1, 1, 2, true);
            expect(Array.from(it6)).toEqual([3, 2]);
        });
    });

    describe("function forEach", () => {
        it("should exist", () => {
            expect(ary.forEach).toBeFunction();
        });
        it("should visit array elements", () => {
            const spy = jest.fn();
            ary.forEach(AL1, spy);
            expect(spy).toHaveBeenCalledTimes(3);
            expect(spy).toHaveBeenNthCalledWith(1, 1, 0);
            expect(spy).toHaveBeenNthCalledWith(2, 2, 1);
            expect(spy).toHaveBeenNthCalledWith(3, 3, 2);
        });
        it("should visit array slices", () => {
            const spy1 = jest.fn();
            ary.forEach(AL1, spy1, { begin: 1 });
            expect(spy1).toHaveBeenCalledTimes(2);
            expect(spy1).toHaveBeenNthCalledWith(1, 2, 1);
            expect(spy1).toHaveBeenNthCalledWith(2, 3, 2);
            const spy2 = jest.fn();
            ary.forEach(AL1, spy2, { end: 2 });
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 1, 0);
            expect(spy2).toHaveBeenNthCalledWith(2, 2, 1);
            const spy3 = jest.fn();
            ary.forEach(AL1, spy3, { begin: 1, end: 2 });
            expect(spy3).toHaveBeenCalledTimes(1);
            expect(spy3).toHaveBeenNthCalledWith(1, 2, 1);
            const spy4 = jest.fn();
            ary.forEach(AL1, spy4, { begin: 1, end: 0 });
            expect(spy4).not.toHaveBeenCalled();
        });
        it("should visit array elements in reverse order", () => {
            const spy = jest.fn();
            ary.forEach(AL1, spy, { reverse: true });
            expect(spy).toHaveBeenCalledTimes(3);
            expect(spy).toHaveBeenNthCalledWith(1, 3, 2);
            expect(spy).toHaveBeenNthCalledWith(2, 2, 1);
            expect(spy).toHaveBeenNthCalledWith(3, 1, 0);
        });
        it("should visit array slices in reversed order", () => {
            const spy1 = jest.fn();
            ary.forEach(AL1, spy1, { reverse: true, begin: 1 });
            expect(spy1).toHaveBeenCalledTimes(2);
            expect(spy1).toHaveBeenNthCalledWith(1, 2, 1);
            expect(spy1).toHaveBeenNthCalledWith(2, 1, 0);
            const spy2 = jest.fn();
            ary.forEach(AL1, spy2, { reverse: true, end: 0 });
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 3, 2);
            expect(spy2).toHaveBeenNthCalledWith(2, 2, 1);
            const spy3 = jest.fn();
            ary.forEach(AL1, spy3, { reverse: true, begin: 1, end: 0 });
            expect(spy3).toHaveBeenCalledTimes(1);
            expect(spy3).toHaveBeenNthCalledWith(1, 2, 1);
            const spy4 = jest.fn();
            ary.forEach(AL1, spy4, { reverse: true, begin: 1, end: 2 });
            expect(spy4).not.toHaveBeenCalled();
        });
    });

    describe("function some", () => {
        it("should exist", () => {
            expect(ary.some).toBeFunction();
        });
        it("should visit array elements", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(ary.some(AL1, spy1)).toBeFalse();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 1, 0);
            expect(spy1).toHaveBeenNthCalledWith(2, 2, 1);
            expect(spy1).toHaveBeenNthCalledWith(3, 3, 2);
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true);
            expect(ary.some(AL1, spy2)).toBeTrue();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 1, 0);
            expect(spy2).toHaveBeenNthCalledWith(2, 2, 1);
        });
        it("should visit array elements in reversed order", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(ary.some(AL1, spy1, { reverse: true })).toBeFalse();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 3, 2);
            expect(spy1).toHaveBeenNthCalledWith(2, 2, 1);
            expect(spy1).toHaveBeenNthCalledWith(3, 1, 0);
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true);
            expect(ary.some(AL1, spy2, { reverse: true })).toBeTrue();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 3, 2);
            expect(spy2).toHaveBeenNthCalledWith(2, 2, 1);
        });
        it("should accept empty arrays", () => {
            const spy = jest.fn();
            expect(ary.some(AL0, spy)).toBeFalse();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe("function every", () => {
        it("should exist", () => {
            expect(ary.every).toBeFunction();
        });
        it("should visit array elements", () => {
            const spy1 = jest.fn().mockReturnValue(true);
            expect(ary.every(AL1, spy1)).toBeTrue();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 1, 0);
            expect(spy1).toHaveBeenNthCalledWith(2, 2, 1);
            expect(spy1).toHaveBeenNthCalledWith(3, 3, 2);
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(false);
            expect(ary.every(AL1, spy2)).toBeFalse();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 1, 0);
            expect(spy2).toHaveBeenNthCalledWith(2, 2, 1);
        });
        it("should visit array elements in reversed order", () => {
            const spy1 = jest.fn().mockReturnValue(true);
            expect(ary.every(AL1, spy1, { reverse: true })).toBeTrue();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenNthCalledWith(1, 3, 2);
            expect(spy1).toHaveBeenNthCalledWith(2, 2, 1);
            expect(spy1).toHaveBeenNthCalledWith(3, 1, 0);
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(false);
            expect(ary.every(AL1, spy2, { reverse: true })).toBeFalse();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenNthCalledWith(1, 3, 2);
            expect(spy2).toHaveBeenNthCalledWith(2, 2, 1);
        });
        it("should accept empty arrays", () => {
            const spy = jest.fn();
            expect(ary.every(AL0, spy)).toBeTrue();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe("function count", () => {
        it("should exist", () => {
            expect(ary.count).toBeFunction();
        });
        it("should count all matching elements", () => {
            const ARRAY = [1, 2, 3, 4, 5, 4, 3, 2, 1];
            expect(ary.count(ARRAY, v => v < 4)).toBe(6);
            expect(ary.count(ARRAY, v => v < 4, { begin: 2 })).toBe(4);
            expect(ary.count(ARRAY, v => v < 4, { end: 7 })).toBe(4);
            expect(ary.count(ARRAY, v => v < 4, { begin: 2, end: 7 })).toBe(2);
        });
    });

    describe("function group", () => {
        it("should exist", () => {
            expect(ary.group).toBeFunction();
        });
        it("should group an array", () => {
            const result = ary.group([2, 6, 5, 4, 3], (_v, i) => `k${i % 3}`);
            expect(result).toBeInstanceOf(Map);
            expect(result.size).toBe(3);
            expect(result.get("k0")).toEqual([2, 4]);
            expect(result.get("k1")).toEqual([6, 3]);
            expect(result.get("k2")).toEqual([5]);
        });
        it("should group slice of an array", () => {
            const result = ary.group([2, 6, 5, 4, 3], (_v, i) => `k${i % 2}`, { begin: 3, end: 0, reverse: true });
            expect(result).toBeInstanceOf(Map);
            expect(result.size).toBe(2);
            expect(result.get("k0")).toEqual([5]);
            expect(result.get("k1")).toEqual([4, 6]);
        });
    });

    describe("function split", () => {
        it("should exist", () => {
            expect(ary.split).toBeFunction();
        });
        it("should split an array", () => {
            const result = ary.split([2, 6, 5, 4, 3], (_v, i) => i % 2 === 1);
            expect(result).toEqual([[2, 5, 3], [6, 4]]);
        });
        it("should split slice of an array", () => {
            const result = ary.split([2, 6, 5, 4, 3], (_v, i) => i % 2 === 1, { begin: 3, end: 0, reverse: true });
            expect(result).toEqual([[5], [4, 6]]);
        });
    });

    describe("function append", () => {
        it("should exist", () => {
            expect(ary.append).toBeFunction();
        });
        it("should append elements", () => {
            const ARRAY = [1, 2];
            expect(ary.append(ARRAY, [3, 4])).toBe(ARRAY);
            expect(ARRAY).toEqual([1, 2, 3, 4]);
            expect(ary.append(ARRAY, new Set([5, 6]))).toBe(ARRAY);
            expect(ARRAY).toEqual([1, 2, 3, 4, 5, 6]);
        });
        it("should append mapped elements", () => {
            const ARRAY = [1, 2];
            const fn = v => 2 * v;
            expect(ary.append(ARRAY, [3, 4], fn)).toBe(ARRAY);
            expect(ARRAY).toEqual([1, 2, 6, 8]);
            expect(ary.append(ARRAY, new Set([5, 6]), fn)).toBe(ARRAY);
            expect(ARRAY).toEqual([1, 2, 6, 8, 10, 12]);
        });
    });

    describe("function insert", () => {
        it("should exist", () => {
            expect(ary.insert).toBeFunction();
        });
        it("should insert elements", () => {
            const ARRAY = [1, 2];
            expect(ary.insert(ARRAY, 1, [3, 4])).toBe(ARRAY);
            expect(ARRAY).toEqual([1, 3, 4, 2]);
            expect(ary.insert(ARRAY, -2, new Set([5, 6]))).toBe(ARRAY);
            expect(ARRAY).toEqual([1, 3, 5, 6, 4, 2]);
        });
        it("should insert mapped elements", () => {
            const ARRAY = [1, 2];
            const fn = v => 2 * v;
            expect(ary.insert(ARRAY, 1, [3, 4], fn)).toBe(ARRAY);
            expect(ARRAY).toEqual([1, 6, 8, 2]);
            expect(ary.insert(ARRAY, -2, new Set([5, 6]), fn)).toBe(ARRAY);
            expect(ARRAY).toEqual([1, 6, 10, 12, 8, 2]);
        });
    });

    describe("function insertAt", () => {
        it("should exist", () => {
            expect(ary.insertAt).toBeFunction();
        });
        it("should insert an element in-place", () => {
            const ARRAY = [1, 2, 3, 4, 3, 2, 1];
            expect(ary.insertAt(ARRAY, 2, 5)).toBe(5);
            expect(ARRAY).toEqual([1, 2, 5, 3, 4, 3, 2, 1]);
            expect(ary.insertAt(ARRAY, -2, 5)).toBe(5);
            expect(ARRAY).toEqual([1, 2, 5, 3, 4, 3, 5, 2, 1]);
        });
        it("should accept indexes not contained in the array", () => {
            const ARRAY = [1, 2, 3, 4, 3, 2, 1];
            expect(ary.insertAt(ARRAY, 8, 5)).toBe(5);
            expect(ARRAY).toEqual([1, 2, 3, 4, 3, 2, 1, 5]);
            expect(ary.insertAt(ARRAY, -10, 5)).toBe(5);
            expect(ARRAY).toEqual([5, 1, 2, 3, 4, 3, 2, 1, 5]);
        });
    });

    describe("function deleteAt", () => {
        it("should exist", () => {
            expect(ary.deleteAt).toBeFunction();
        });
        it("should remove an element in-place", () => {
            const ARRAY = [1, 2, 3, 4, 5, 4, 3, 2, 1];
            expect(ary.deleteAt(ARRAY, 3)).toBe(4);
            expect(ARRAY).toEqual([1, 2, 3, 5, 4, 3, 2, 1]);
            expect(ary.deleteAt(ARRAY, -2)).toBe(2);
            expect(ARRAY).toEqual([1, 2, 3, 5, 4, 3, 1]);
        });
        it("should accept indexes not contained in the array", () => {
            const ARRAY = [1, 2, 3, 4, 5, 4, 3, 2, 1];
            expect(ary.deleteAt(ARRAY, 9)).toBeUndefined();
            expect(ary.deleteAt(ARRAY, -10)).toBeUndefined();
            expect(ARRAY).toEqual([1, 2, 3, 4, 5, 4, 3, 2, 1]);
        });
    });

    describe("function deleteFirst", () => {
        it("should exist", () => {
            expect(ary.deleteFirst).toBeFunction();
        });
        it("should remove first matching element", () => {
            const ARRAY = [1, 2, 3, 4, 5, 4, 3, 2, 1];
            expect(ary.deleteFirst(ARRAY, 4)).toBeTrue();
            expect(ARRAY).toEqual([1, 2, 3, 5, 4, 3, 2, 1]);
            expect(ary.deleteFirst(ARRAY, 4)).toBeTrue();
            expect(ARRAY).toEqual([1, 2, 3, 5, 3, 2, 1]);
            expect(ary.deleteFirst(ARRAY, 4)).toBeFalse();
            expect(ARRAY).toEqual([1, 2, 3, 5, 3, 2, 1]);
        });
    });

    describe("function deleteAll", () => {
        it("should exist", () => {
            expect(ary.deleteAll).toBeFunction();
        });
        it("should remove matching elements", () => {
            const ARRAY = [1, 2, 3, 4, 5, 4, 3, 2, 1];
            expect(ary.deleteAll(ARRAY, 4)).toBe(2);
            expect(ARRAY).toEqual([1, 2, 3, 5, 3, 2, 1]);
            expect(ary.deleteAll(ARRAY, 6)).toBe(0);
            expect(ARRAY).toEqual([1, 2, 3, 5, 3, 2, 1]);
        });
    });

    describe("function deleteFirstMatching", () => {
        it("should exist", () => {
            expect(ary.deleteFirstMatching).toBeFunction();
        });
        it("should remove first matching element", () => {
            const ARRAY = [1, 2, 3, 4, 5, 4, 3, 2, 1];
            expect(ary.deleteFirstMatching(ARRAY, v => v >= 4)).toBeTrue();
            expect(ARRAY).toEqual([1, 2, 3, 5, 4, 3, 2, 1]);
            expect(ary.deleteFirstMatching(ARRAY, v => v >= 4)).toBeTrue();
            expect(ARRAY).toEqual([1, 2, 3, 4, 3, 2, 1]);
            expect(ary.deleteFirstMatching(ARRAY, v => v === 5)).toBeFalse();
            expect(ARRAY).toEqual([1, 2, 3, 4, 3, 2, 1]);
        });
    });

    describe("function deleteAllMatching", () => {
        it("should exist", () => {
            expect(ary.deleteAllMatching).toBeFunction();
        });
        it("should remove all matching elements", () => {
            const ARRAY = [1, 2, 3, 4, 5, 4, 3, 2, 1];
            expect(ary.deleteAllMatching(ARRAY, v => v >= 4)).toBe(3);
            expect(ARRAY).toEqual([1, 2, 3, 3, 2, 1]);
            expect(ary.deleteAllMatching(ARRAY, v => v >= 4)).toBe(0);
            expect(ARRAY).toEqual([1, 2, 3, 3, 2, 1]);
        });
    });

    describe("function insertSorted", () => {
        it("should exist", () => {
            expect(ary.insertSorted).toBeFunction();
        });
        it("should insert elements in-place", () => {
            const ARRAY = [];
            expect(ary.insertSorted(ARRAY, 3, math.compare)).toBe(3);
            expect(ARRAY).toEqual([3]);
            expect(ary.insertSorted(ARRAY, 1, math.compare)).toBe(1);
            expect(ARRAY).toEqual([1, 3]);
            expect(ary.insertSorted(ARRAY, 2, math.compare)).toBe(2);
            expect(ARRAY).toEqual([1, 2, 3]);
            expect(ary.insertSorted(ARRAY, 4, math.compare)).toBe(4);
            expect(ARRAY).toEqual([1, 2, 3, 4]);
        });
        it("should append equalish values", () => {
            const ARRAY = [];
            const fn = (v, e) => math.compare(v % 10, e % 10); // sort by last digit
            ary.insertSorted(ARRAY, 13, fn);
            expect(ARRAY).toEqual([13]);
            ary.insertSorted(ARRAY, 21, fn);
            expect(ARRAY).toEqual([21, 13]);
            ary.insertSorted(ARRAY, 11, fn);
            expect(ARRAY).toEqual([21, 11, 13]);
            ary.insertSorted(ARRAY, 42, fn);
            expect(ARRAY).toEqual([21, 11, 42, 13]);
            ary.insertSorted(ARRAY, 52, fn);
            expect(ARRAY).toEqual([21, 11, 42, 52, 13]);
            ary.insertSorted(ARRAY, 3, fn);
            expect(ARRAY).toEqual([21, 11, 42, 52, 13, 3]);
        });
        it("should replace equalish values", () => {
            const ARRAY = [];
            const fn = (v, e) => math.compare(v % 10, e % 10); // sort by last digit
            ary.insertSorted(ARRAY, 13, fn, { unique: true });
            expect(ARRAY).toEqual([13]);
            ary.insertSorted(ARRAY, 21, fn, { unique: true });
            expect(ARRAY).toEqual([21, 13]);
            ary.insertSorted(ARRAY, 11, fn, { unique: true });
            expect(ARRAY).toEqual([11, 13]);
            ary.insertSorted(ARRAY, 42, fn, { unique: true });
            expect(ARRAY).toEqual([11, 42, 13]);
            ary.insertSorted(ARRAY, 52, fn, { unique: true });
            expect(ARRAY).toEqual([11, 52, 13]);
            ary.insertSorted(ARRAY, 3, fn, { unique: true });
            expect(ARRAY).toEqual([11, 52, 3]);
        });
    });

    describe("function deleteSorted", () => {
        it("should exist", () => {
            expect(ary.deleteSorted).toBeFunction();
        });
        it("should delete elements in-place", () => {
            const ARRAY = [1, 2, 3, 4];
            expect(ary.deleteSorted(ARRAY, 5, math.compare)).toBeUndefined();
            expect(ary.deleteSorted(ARRAY, 2.5, math.compare)).toBeUndefined();
            expect(ary.deleteSorted(ARRAY, 0, math.compare)).toBeUndefined();
            expect(ARRAY).toEqual([1, 2, 3, 4]);
            expect(ary.deleteSorted(ARRAY, 3, math.compare)).toBe(3);
            expect(ARRAY).toEqual([1, 2, 4]);
            expect(ary.deleteSorted(ARRAY, 1, math.compare)).toBe(1);
            expect(ARRAY).toEqual([2, 4]);
            expect(ary.deleteSorted(ARRAY, 4, math.compare)).toBe(4);
            expect(ARRAY).toEqual([2]);
            expect(ary.deleteSorted(ARRAY, 2, math.compare)).toBe(2);
            expect(ARRAY).toEqual([]);
            expect(ary.deleteSorted(ARRAY, 2, math.compare)).toBeUndefined();
            expect(ARRAY).toEqual([]);
        });
        it("should handle same sort index for different values", () => {
            const ARRAY = [21, 11, 42, 52, 13, 3];
            const fn = (v, e) => math.compare(v % 10, e % 10); // sort by last digit
            expect(ary.deleteSorted(ARRAY, 2, fn)).toBe(42);
            expect(ARRAY).toEqual([21, 11, 52, 13, 3]);
            expect(ary.deleteSorted(ARRAY, 2, fn)).toBe(52);
            expect(ARRAY).toEqual([21, 11, 13, 3]);
            expect(ary.deleteSorted(ARRAY, 11, fn)).toBe(21);
            expect(ARRAY).toEqual([11, 13, 3]);
            expect(ary.deleteSorted(ARRAY, 11, fn)).toBe(11);
            expect(ARRAY).toEqual([13, 3]);
            expect(ary.deleteSorted(ARRAY, 3, fn)).toBe(13);
            expect(ARRAY).toEqual([3]);
            expect(ary.deleteSorted(ARRAY, 3, fn)).toBe(3);
            expect(ARRAY).toEqual([]);
        });
    });

    describe("function sortBy", () => {
        it("should exist", () => {
            expect(ary.sortBy).toBeFunction();
        });
        it("should sort the array", () => {
            const ARRAY = [1, 3, 2];
            const result = ary.sortBy(ARRAY, v => -v);
            expect(result).toBe(ARRAY);
            expect(result).toEqual([3, 2, 1]);
        });
    });

    describe("function unify", () => {
        it("should exist", () => {
            expect(ary.unify).toBeFunction();
        });
        it("should unify the array", () => {
            const ARRAY = [1, 2, 1, 4, 1, 3, 1, 4, 1, 2, 1];
            const result = ary.unify(ARRAY);
            expect(result).toBe(ARRAY);
            expect(result).toEqual([1, 2, 4, 3]);
        });
    });

    describe("function permute", () => {
        it("should exist", () => {
            expect(ary.permute).toBeFunction();
        });
        it("should reorder the array", () => {
            const ARRAY = ["a", "b", "c", "d"];
            expect(ary.permute(ARRAY, [0, 1, 2, 3])).toBe(ARRAY);
            expect(ARRAY).toEqual(["a", "b", "c", "d"]);
            expect(ary.permute(ARRAY, [3, 0, 2, 1])).toBe(ARRAY);
            expect(ARRAY).toEqual(["d", "a", "c", "b"]);
        });
    });

    describe("function at", () => {
        it("should exist", () => {
            expect(ary.at).toBeFunction();
        });
        it("should return element", () => {
            const ARRAY = [1, 3, 5, 7];
            expect(ary.at(ARRAY, 0)).toBe(1);
            expect(ary.at(ARRAY, 1)).toBe(3);
            expect(ary.at(ARRAY, 2)).toBe(5);
            expect(ary.at(ARRAY, 3)).toBe(7);
            expect(ary.at(ARRAY, 4)).toBeUndefined();
            expect(ary.at(ARRAY, -1)).toBe(7);
            expect(ary.at(ARRAY, -2)).toBe(5);
            expect(ary.at(ARRAY, -3)).toBe(3);
            expect(ary.at(ARRAY, -4)).toBe(1);
            expect(ary.at(ARRAY, -5)).toBeUndefined();
        });
        it("should return element from array-like", () => {
            const ARRAY = { length: 4, 0: 1, 1: 3, 2: 5, 3: 7 };
            expect(ary.at(ARRAY, 0)).toBe(1);
            expect(ary.at(ARRAY, 1)).toBe(3);
            expect(ary.at(ARRAY, 2)).toBe(5);
            expect(ary.at(ARRAY, 3)).toBe(7);
            expect(ary.at(ARRAY, 4)).toBeUndefined();
            expect(ary.at(ARRAY, -1)).toBe(7);
            expect(ary.at(ARRAY, -2)).toBe(5);
            expect(ary.at(ARRAY, -3)).toBe(3);
            expect(ary.at(ARRAY, -4)).toBe(1);
            expect(ary.at(ARRAY, -5)).toBeUndefined();
        });
    });

    describe("function last", () => {
        it("should exist", () => {
            expect(ary.last).toBeFunction();
        });
        it("should return element", () => {
            expect(ary.last([1, 3])).toBe(3);
            expect(ary.last([1])).toBe(1);
            expect(ary.last([])).toBeUndefined();
        });
        it("should return element from array-like", () => {
            expect(ary.last({ length: 2, 0: 1, 1: 3 })).toBe(3);
            expect(ary.last({ length: 1, 0: 1 })).toBe(1);
            expect(ary.last({ length: 0 })).toBeUndefined();
        });
    });

    describe("function initial", () => {
        it("should exist", () => {
            expect(ary.initial).toBeFunction();
        });
        it("should remove the last element", () => {
            expect(ary.initial([1, 3])).toEqual([1]);
            expect(ary.initial([1])).toEqual([]);
            expect(ary.initial([])).toEqual([]);
        });
        it("should remove the last element from array-like", () => {
            expect(ary.initial({ length: 2, 0: 1, 1: 3 })).toEqual([1]);
            expect(ary.initial({ length: 1, 0: 1 })).toEqual([]);
            expect(ary.initial({ length: 0 })).toEqual([]);
        });
        it("should remove the last n elements from array", () => {
            expect(ary.initial([1, 2, 3], -2)).toEqual([1, 2, 3]);
            expect(ary.initial([1, 2, 3], -1)).toEqual([1, 2, 3]);
            expect(ary.initial([1, 2, 3], 0)).toEqual([1, 2, 3]);
            expect(ary.initial([1, 2, 3], 1)).toEqual([1, 2]);
            expect(ary.initial([1, 2, 3], 2)).toEqual([1]);
            expect(ary.initial([1, 2, 3], 3)).toEqual([]);
            expect(ary.initial([1, 2, 3], 4)).toEqual([]);
        });
        it("should remove the last n elements from an array-like", () => {
            expect(ary.initial({ length: 3, 0: 1, 1: 2, 2: 3 }, -1)).toEqual([1, 2, 3]);
            expect(ary.initial({ length: 3, 0: 1, 1: 2, 2: 3 }, -2)).toEqual([1, 2, 3]);
            expect(ary.initial({ length: 3, 0: 1, 1: 2, 2: 3 }, 0)).toEqual([1, 2, 3]);
            expect(ary.initial({ length: 3, 0: 1, 1: 2, 2: 3 }, 1)).toEqual([1, 2]);
            expect(ary.initial({ length: 3, 0: 1, 1: 2, 2: 3 }, 2)).toEqual([1]);
            expect(ary.initial({ length: 3, 0: 1, 1: 2, 2: 3 }, 3)).toEqual([]);
            expect(ary.initial({ length: 3, 0: 1, 1: 2, 2: 3 }, 4)).toEqual([]);
        });
        it("should handle an empty array correctly", () => {
            expect(ary.initial([], -2)).toEqual([]);
            expect(ary.initial([], -1)).toEqual([]);
            expect(ary.initial([], 0)).toEqual([]);
            expect(ary.initial([], 1)).toEqual([]);
            expect(ary.initial([], 2)).toEqual([]);
        });
        it("should handle an empty array-like correctly", () => {
            expect(ary.initial({ length: 0 }, -2)).toEqual([]);
            expect(ary.initial({ length: 0 }, -1)).toEqual([]);
            expect(ary.initial({ length: 0 }, 0)).toEqual([]);
            expect(ary.initial({ length: 0 }, 1)).toEqual([]);
            expect(ary.initial({ length: 0 }, 2)).toEqual([]);
        });
    });

    describe("function findIndex", () => {
        it("should exist", () => {
            expect(ary.findIndex).toBeFunction();
        });
        const ARRAY = [1, 3, 5, 7, 5, 3, 1];
        it("should return array index of first matching element", () => {
            expect(ary.findIndex(ARRAY, greater(-1))).toBe(0);
            expect(ary.findIndex(ARRAY, greater(0))).toBe(0);
            expect(ary.findIndex(ARRAY, greater(1))).toBe(1);
            expect(ary.findIndex(ARRAY, greater(2))).toBe(1);
            expect(ary.findIndex(ARRAY, greater(3))).toBe(2);
            expect(ary.findIndex(ARRAY, greater(4))).toBe(2);
            expect(ary.findIndex(ARRAY, greater(5))).toBe(3);
            expect(ary.findIndex(ARRAY, greater(6))).toBe(3);
        });
        it("should return array index of last matching element", () => {
            expect(ary.findIndex(ARRAY, greater(-1), { reverse: true })).toBe(6);
            expect(ary.findIndex(ARRAY, greater(0), { reverse: true })).toBe(6);
            expect(ary.findIndex(ARRAY, greater(1), { reverse: true })).toBe(5);
            expect(ary.findIndex(ARRAY, greater(2), { reverse: true })).toBe(5);
            expect(ary.findIndex(ARRAY, greater(3), { reverse: true })).toBe(4);
            expect(ary.findIndex(ARRAY, greater(4), { reverse: true })).toBe(4);
            expect(ary.findIndex(ARRAY, greater(5), { reverse: true })).toBe(3);
            expect(ary.findIndex(ARRAY, greater(6), { reverse: true })).toBe(3);
        });
        it("should return invalid index if no array element matches", () => {
            expect(ary.findIndex(ARRAY, greater(7))).toBe(7);
            expect(ary.findIndex(ARRAY, greater(8))).toBe(7);
            expect(ary.findIndex(ARRAY, greater(7), { reverse: true })).toBe(-1);
            expect(ary.findIndex(ARRAY, greater(8), { reverse: true })).toBe(-1);
        });
        it("should use specified array slice", () => {
            expect(ary.findIndex(ARRAY, greater(4), { begin: 3 })).toBe(3);
            expect(ary.findIndex(ARRAY, greater(4), { begin: 4 })).toBe(4);
            expect(ary.findIndex(ARRAY, greater(4), { begin: 5 })).toBe(7);
            expect(ary.findIndex(ARRAY, greater(4), { begin: 6 })).toBe(7);
            expect(ary.findIndex(ARRAY, greater(4), { end: 3 })).toBe(2);
            expect(ary.findIndex(ARRAY, greater(4), { end: 2 })).toBe(7);
            expect(ary.findIndex(ARRAY, greater(4), { reverse: true, begin: 3 })).toBe(3);
            expect(ary.findIndex(ARRAY, greater(4), { reverse: true, begin: 2 })).toBe(2);
            expect(ary.findIndex(ARRAY, greater(4), { reverse: true, begin: 1 })).toBe(-1);
            expect(ary.findIndex(ARRAY, greater(4), { reverse: true, begin: 0 })).toBe(-1);
            expect(ary.findIndex(ARRAY, greater(4), { reverse: true, end: 3 })).toBe(4);
            expect(ary.findIndex(ARRAY, greater(4), { reverse: true, end: 4 })).toBe(-1);
        });
        it("should accept an empty array", () => {
            expect(ary.findIndex([], greater(0))).toBe(0);
        });
    });

    describe("function findValue", () => {
        it("should exist", () => {
            expect(ary.findValue).toBeFunction();
        });
        const ARRAY = [1, 3, 5, 7, 5, 3, 1];
        it("should return first matching element", () => {
            expect(ary.findValue(ARRAY, greater(-1))).toBe(1);
            expect(ary.findValue(ARRAY, greater(0))).toBe(1);
            expect(ary.findValue(ARRAY, greater(1))).toBe(3);
            expect(ary.findValue(ARRAY, greater(2))).toBe(3);
            expect(ary.findValue(ARRAY, greater(3))).toBe(5);
            expect(ary.findValue(ARRAY, greater(4))).toBe(5);
            expect(ary.findValue(ARRAY, greater(5))).toBe(7);
            expect(ary.findValue(ARRAY, greater(6))).toBe(7);
        });
        it("should return last matching element", () => {
            expect(ary.findValue(ARRAY, greater(-1), { reverse: true })).toBe(1);
            expect(ary.findValue(ARRAY, greater(0), { reverse: true })).toBe(1);
            expect(ary.findValue(ARRAY, greater(1), { reverse: true })).toBe(3);
            expect(ary.findValue(ARRAY, greater(2), { reverse: true })).toBe(3);
            expect(ary.findValue(ARRAY, greater(3), { reverse: true })).toBe(5);
            expect(ary.findValue(ARRAY, greater(4), { reverse: true })).toBe(5);
            expect(ary.findValue(ARRAY, greater(5), { reverse: true })).toBe(7);
            expect(ary.findValue(ARRAY, greater(6), { reverse: true })).toBe(7);
        });
        it("should return undefined if no array element matches", () => {
            expect(ary.findValue(ARRAY, greater(7))).toBeUndefined();
            expect(ary.findValue(ARRAY, greater(8))).toBeUndefined();
            expect(ary.findValue(ARRAY, greater(7), { reverse: true })).toBeUndefined();
            expect(ary.findValue(ARRAY, greater(8), { reverse: true })).toBeUndefined();
        });
        it("should use specified array slice", () => {
            expect(ary.findValue(ARRAY, greater(4), { begin: 3 })).toBe(7);
            expect(ary.findValue(ARRAY, greater(4), { begin: 4 })).toBe(5);
            expect(ary.findValue(ARRAY, greater(4), { begin: 5 })).toBeUndefined();
            expect(ary.findValue(ARRAY, greater(4), { begin: 6 })).toBeUndefined();
            expect(ary.findValue(ARRAY, greater(4), { end: 3 })).toBe(5);
            expect(ary.findValue(ARRAY, greater(4), { end: 2 })).toBeUndefined();
            expect(ary.findValue(ARRAY, greater(4), { reverse: true, begin: 3 })).toBe(7);
            expect(ary.findValue(ARRAY, greater(4), { reverse: true, begin: 2 })).toBe(5);
            expect(ary.findValue(ARRAY, greater(4), { reverse: true, begin: 1 })).toBeUndefined();
            expect(ary.findValue(ARRAY, greater(4), { reverse: true, begin: 0 })).toBeUndefined();
            expect(ary.findValue(ARRAY, greater(4), { reverse: true,  end: 3 })).toBe(5);
            expect(ary.findValue(ARRAY, greater(4), { reverse: true,  end: 4 })).toBeUndefined();
        });
        it("should accept an empty array", () => {
            expect(ary.findValue([], greater(0))).toBeUndefined();
        });
    });

    describe("function fastFindFirstIndex", () => {
        it("should exist", () => {
            expect(ary.fastFindFirstIndex).toBeFunction();
        });
        const ARRAY = ary.generate(32, i => 2 * i);
        it("should return array index of first matching element", () => {
            expect(ary.fastFindFirstIndex(ARRAY, greater(-4))).toBe(0);
            expect(ary.fastFindFirstIndex(ARRAY, greater(-2))).toBe(0);
            expect(ary.fastFindFirstIndex(ARRAY, greater(-1))).toBe(0);
            expect(ary.fastFindFirstIndex(ARRAY, greater(0))).toBe(1);
            expect(ary.fastFindFirstIndex(ARRAY, greater(1))).toBe(1);
            expect(ary.fastFindFirstIndex(ARRAY, greater(2))).toBe(2);
            expect(ary.fastFindFirstIndex(ARRAY, greater(3))).toBe(2);
            expect(ary.fastFindFirstIndex(ARRAY, greater(4))).toBe(3);
            expect(ary.fastFindFirstIndex(ARRAY, greater(6))).toBe(4);
            expect(ary.fastFindFirstIndex(ARRAY, greater(8))).toBe(5);
            expect(ary.fastFindFirstIndex(ARRAY, greater(14))).toBe(8);
            expect(ary.fastFindFirstIndex(ARRAY, greater(17))).toBe(9);
            expect(ary.fastFindFirstIndex(ARRAY, greater(20))).toBe(11);
            expect(ary.fastFindFirstIndex(ARRAY, greater(26))).toBe(14);
            expect(ary.fastFindFirstIndex(ARRAY, greater(32))).toBe(17);
            expect(ary.fastFindFirstIndex(ARRAY, greater(38))).toBe(20);
            expect(ary.fastFindFirstIndex(ARRAY, greater(44))).toBe(23);
            expect(ary.fastFindFirstIndex(ARRAY, greater(50))).toBe(26);
            expect(ary.fastFindFirstIndex(ARRAY, greater(56))).toBe(29);
            expect(ary.fastFindFirstIndex(ARRAY, greater(58))).toBe(30);
            expect(ary.fastFindFirstIndex(ARRAY, greater(59))).toBe(30);
            expect(ary.fastFindFirstIndex(ARRAY, greater(60))).toBe(31);
            expect(ary.fastFindFirstIndex(ARRAY, greater(61))).toBe(31);
        });
        it("should return invalid array index if no array element matches", () => {
            expect(ary.fastFindFirstIndex(ARRAY, greater(62))).toBe(32);
            expect(ary.fastFindFirstIndex(ARRAY, greater(63))).toBe(32);
        });
        it("should use specified array slice", () => {
            expect(ary.fastFindFirstIndex(ARRAY, greater(4), 2)).toBe(3);
            expect(ary.fastFindFirstIndex(ARRAY, greater(4), 3)).toBe(3);
            expect(ary.fastFindFirstIndex(ARRAY, greater(4), 4)).toBe(4);
            expect(ary.fastFindFirstIndex(ARRAY, greater(4), 5)).toBe(5);
        });
        it("should return array index of matching element for short arrays", () => {
            expect(ary.fastFindFirstIndex([1], greater(0))).toBe(0);
            expect(ary.fastFindFirstIndex([1], greater(1))).toBe(1);
            expect(ary.fastFindFirstIndex([1, 2], greater(0))).toBe(0);
            expect(ary.fastFindFirstIndex([1, 2], greater(1))).toBe(1);
            expect(ary.fastFindFirstIndex([1, 2], greater(2))).toBe(2);
        });
        it("should use a binary search algorithm", () => {
            const spy = jest.fn(greater(55));
            ary.fastFindFirstIndex(ARRAY, spy);
            expect(spy.mock.calls.length).toBeLessThan(6);
        });
        it("should accept an empty array", () => {
            expect(ary.fastFindFirstIndex([], greater(0))).toBe(0);
        });
    });

    describe("function fastFindFirstValue", () => {
        it("should exist", () => {
            expect(ary.fastFindFirstValue).toBeFunction();
        });
        const ARRAY = ary.generate(32, i => 2 * i);
        it("should return first matching element", () => {
            expect(ary.fastFindFirstValue(ARRAY, greater(-4))).toBe(0);
            expect(ary.fastFindFirstValue(ARRAY, greater(-2))).toBe(0);
            expect(ary.fastFindFirstValue(ARRAY, greater(-1))).toBe(0);
            expect(ary.fastFindFirstValue(ARRAY, greater(0))).toBe(2);
            expect(ary.fastFindFirstValue(ARRAY, greater(1))).toBe(2);
            expect(ary.fastFindFirstValue(ARRAY, greater(2))).toBe(4);
            expect(ary.fastFindFirstValue(ARRAY, greater(3))).toBe(4);
            expect(ary.fastFindFirstValue(ARRAY, greater(4))).toBe(6);
            expect(ary.fastFindFirstValue(ARRAY, greater(6))).toBe(8);
            expect(ary.fastFindFirstValue(ARRAY, greater(8))).toBe(10);
            expect(ary.fastFindFirstValue(ARRAY, greater(14))).toBe(16);
            expect(ary.fastFindFirstValue(ARRAY, greater(17))).toBe(18);
            expect(ary.fastFindFirstValue(ARRAY, greater(20))).toBe(22);
            expect(ary.fastFindFirstValue(ARRAY, greater(26))).toBe(28);
            expect(ary.fastFindFirstValue(ARRAY, greater(32))).toBe(34);
            expect(ary.fastFindFirstValue(ARRAY, greater(38))).toBe(40);
            expect(ary.fastFindFirstValue(ARRAY, greater(44))).toBe(46);
            expect(ary.fastFindFirstValue(ARRAY, greater(50))).toBe(52);
            expect(ary.fastFindFirstValue(ARRAY, greater(56))).toBe(58);
            expect(ary.fastFindFirstValue(ARRAY, greater(58))).toBe(60);
            expect(ary.fastFindFirstValue(ARRAY, greater(59))).toBe(60);
            expect(ary.fastFindFirstValue(ARRAY, greater(60))).toBe(62);
            expect(ary.fastFindFirstValue(ARRAY, greater(61))).toBe(62);
        });
        it("should return undefined if no array element matches", () => {
            expect(ary.fastFindFirstValue(ARRAY, greater(62))).toBeUndefined();
            expect(ary.fastFindFirstValue(ARRAY, greater(63))).toBeUndefined();
        });
        it("should use specified array slice", () => {
            expect(ary.fastFindFirstValue(ARRAY, greater(4), 2)).toBe(6);
            expect(ary.fastFindFirstValue(ARRAY, greater(4), 3)).toBe(6);
            expect(ary.fastFindFirstValue(ARRAY, greater(4), 4)).toBe(8);
            expect(ary.fastFindFirstValue(ARRAY, greater(4), 5)).toBe(10);
        });
        it("should return matching element for short arrays", () => {
            expect(ary.fastFindFirstValue([1], greater(0))).toBe(1);
            expect(ary.fastFindFirstValue([1], greater(1))).toBeUndefined();
            expect(ary.fastFindFirstValue([1, 2], greater(0))).toBe(1);
            expect(ary.fastFindFirstValue([1, 2], greater(1))).toBe(2);
            expect(ary.fastFindFirstValue([1, 2], greater(2))).toBeUndefined();
        });
        it("should use a binary search algorithm", () => {
            const spy = jest.fn(greater(55));
            ary.fastFindFirstValue(ARRAY, spy);
            expect(spy.mock.calls.length).toBeLessThan(6);
        });
        it("should accept an empty array", () => {
            expect(ary.fastFindFirstValue([], greater(0))).toBeUndefined();
        });
    });

    describe("function fastFindLastIndex", () => {
        it("should exist", () => {
            expect(ary.fastFindLastIndex).toBeFunction();
        });
        const ARRAY = ary.generate(32, i => 62 - 2 * i);
        it("should return array index of last matching element", () => {
            expect(ary.fastFindLastIndex(ARRAY, greater(-4))).toBe(31);
            expect(ary.fastFindLastIndex(ARRAY, greater(-2))).toBe(31);
            expect(ary.fastFindLastIndex(ARRAY, greater(-1))).toBe(31);
            expect(ary.fastFindLastIndex(ARRAY, greater(0))).toBe(30);
            expect(ary.fastFindLastIndex(ARRAY, greater(1))).toBe(30);
            expect(ary.fastFindLastIndex(ARRAY, greater(2))).toBe(29);
            expect(ary.fastFindLastIndex(ARRAY, greater(3))).toBe(29);
            expect(ary.fastFindLastIndex(ARRAY, greater(4))).toBe(28);
            expect(ary.fastFindLastIndex(ARRAY, greater(6))).toBe(27);
            expect(ary.fastFindLastIndex(ARRAY, greater(8))).toBe(26);
            expect(ary.fastFindLastIndex(ARRAY, greater(14))).toBe(23);
            expect(ary.fastFindLastIndex(ARRAY, greater(17))).toBe(22);
            expect(ary.fastFindLastIndex(ARRAY, greater(20))).toBe(20);
            expect(ary.fastFindLastIndex(ARRAY, greater(26))).toBe(17);
            expect(ary.fastFindLastIndex(ARRAY, greater(32))).toBe(14);
            expect(ary.fastFindLastIndex(ARRAY, greater(38))).toBe(11);
            expect(ary.fastFindLastIndex(ARRAY, greater(44))).toBe(8);
            expect(ary.fastFindLastIndex(ARRAY, greater(50))).toBe(5);
            expect(ary.fastFindLastIndex(ARRAY, greater(56))).toBe(2);
            expect(ary.fastFindLastIndex(ARRAY, greater(58))).toBe(1);
            expect(ary.fastFindLastIndex(ARRAY, greater(59))).toBe(1);
            expect(ary.fastFindLastIndex(ARRAY, greater(60))).toBe(0);
            expect(ary.fastFindLastIndex(ARRAY, greater(61))).toBe(0);
        });
        it("should return invalid array index if no array element matches", () => {
            expect(ary.fastFindLastIndex(ARRAY, greater(62))).toBe(-1);
            expect(ary.fastFindLastIndex(ARRAY, greater(63))).toBe(-1);
        });
        it("should use specified array slice", () => {
            expect(ary.fastFindLastIndex(ARRAY, greater(4), 29)).toBe(28);
            expect(ary.fastFindLastIndex(ARRAY, greater(4), 28)).toBe(28);
            expect(ary.fastFindLastIndex(ARRAY, greater(4), 27)).toBe(27);
            expect(ary.fastFindLastIndex(ARRAY, greater(4), 26)).toBe(26);
        });
        it("should return array index of matching element for short arrays", () => {
            expect(ary.fastFindLastIndex([1], greater(0))).toBe(0);
            expect(ary.fastFindLastIndex([1], greater(1))).toBe(-1);
            expect(ary.fastFindLastIndex([2, 1], greater(0))).toBe(1);
            expect(ary.fastFindLastIndex([2, 1], greater(1))).toBe(0);
            expect(ary.fastFindLastIndex([2, 1], greater(2))).toBe(-1);
        });
        it("should use a binary search algorithm", () => {
            const spy = jest.fn(greater(55));
            ary.fastFindLastIndex(ARRAY, spy);
            expect(spy.mock.calls.length).toBeLessThan(6);
        });
        it("should accept an empty array", () => {
            expect(ary.fastFindLastIndex([], greater(0))).toBe(-1);
        });
    });

    describe("function fastFindLastValue", () => {
        it("should exist", () => {
            expect(ary.fastFindLastValue).toBeFunction();
        });
        const ARRAY = ary.generate(32, i => 62 - 2 * i);
        it("should return last matching element", () => {
            expect(ary.fastFindLastValue(ARRAY, greater(-4))).toBe(0);
            expect(ary.fastFindLastValue(ARRAY, greater(-2))).toBe(0);
            expect(ary.fastFindLastValue(ARRAY, greater(-1))).toBe(0);
            expect(ary.fastFindLastValue(ARRAY, greater(0))).toBe(2);
            expect(ary.fastFindLastValue(ARRAY, greater(1))).toBe(2);
            expect(ary.fastFindLastValue(ARRAY, greater(2))).toBe(4);
            expect(ary.fastFindLastValue(ARRAY, greater(3))).toBe(4);
            expect(ary.fastFindLastValue(ARRAY, greater(4))).toBe(6);
            expect(ary.fastFindLastValue(ARRAY, greater(6))).toBe(8);
            expect(ary.fastFindLastValue(ARRAY, greater(8))).toBe(10);
            expect(ary.fastFindLastValue(ARRAY, greater(14))).toBe(16);
            expect(ary.fastFindLastValue(ARRAY, greater(17))).toBe(18);
            expect(ary.fastFindLastValue(ARRAY, greater(20))).toBe(22);
            expect(ary.fastFindLastValue(ARRAY, greater(26))).toBe(28);
            expect(ary.fastFindLastValue(ARRAY, greater(32))).toBe(34);
            expect(ary.fastFindLastValue(ARRAY, greater(38))).toBe(40);
            expect(ary.fastFindLastValue(ARRAY, greater(44))).toBe(46);
            expect(ary.fastFindLastValue(ARRAY, greater(50))).toBe(52);
            expect(ary.fastFindLastValue(ARRAY, greater(56))).toBe(58);
            expect(ary.fastFindLastValue(ARRAY, greater(58))).toBe(60);
            expect(ary.fastFindLastValue(ARRAY, greater(59))).toBe(60);
            expect(ary.fastFindLastValue(ARRAY, greater(60))).toBe(62);
            expect(ary.fastFindLastValue(ARRAY, greater(61))).toBe(62);
        });
        it("should return undefined if no array element matches", () => {
            expect(ary.fastFindLastValue(ARRAY, greater(62))).toBeUndefined();
            expect(ary.fastFindLastValue(ARRAY, greater(63))).toBeUndefined();
        });
        it("should use specified array slice", () => {
            expect(ary.fastFindLastValue(ARRAY, greater(4), 29)).toBe(6);
            expect(ary.fastFindLastValue(ARRAY, greater(4), 28)).toBe(6);
            expect(ary.fastFindLastValue(ARRAY, greater(4), 27)).toBe(8);
            expect(ary.fastFindLastValue(ARRAY, greater(4), 26)).toBe(10);
        });
        it("should return matching element for short arrays", () => {
            expect(ary.fastFindLastValue([1], greater(0))).toBe(1);
            expect(ary.fastFindLastValue([1], greater(1))).toBeUndefined();
            expect(ary.fastFindLastValue([2, 1], greater(0))).toBe(1);
            expect(ary.fastFindLastValue([2, 1], greater(1))).toBe(2);
            expect(ary.fastFindLastValue([2, 1], greater(2))).toBeUndefined();
        });
        it("should use a binary search algorithm", () => {
            const spy = jest.fn(greater(55));
            ary.fastFindLastValue(ARRAY, spy);
            expect(spy.mock.calls.length).toBeLessThan(6);
        });
        it("should accept an empty array", () => {
            expect(ary.fastFindLastValue([], greater(0))).toBeUndefined();
        });
    });

    describe("function nearestIndex", () => {
        it("should exist", () => {
            expect(ary.nearestIndex).toBeFunction();
        });
        const ARRAY = [1, 2, 3];
        const sqr = n => n ** 2;
        it("should return array index of nearest element", () => {
            expect(ary.nearestIndex(ARRAY, -100, sqr)).toBe(0);
            expect(ary.nearestIndex(ARRAY, 0, sqr)).toBe(0);
            expect(ary.nearestIndex(ARRAY, 1, sqr)).toBe(0);
            expect(ary.nearestIndex(ARRAY, 2, sqr)).toBe(0);
            expect(ary.nearestIndex(ARRAY, 2.5, sqr)).toBe(0);
            expect(ary.nearestIndex(ARRAY, 3, sqr)).toBe(1);
            expect(ary.nearestIndex(ARRAY, 4, sqr)).toBe(1);
            expect(ary.nearestIndex(ARRAY, 6, sqr)).toBe(1);
            expect(ary.nearestIndex(ARRAY, 6.5, sqr)).toBe(1);
            expect(ary.nearestIndex(ARRAY, 7, sqr)).toBe(2);
            expect(ary.nearestIndex(ARRAY, 9, sqr)).toBe(2);
            expect(ary.nearestIndex(ARRAY, 10, sqr)).toBe(2);
            expect(ary.nearestIndex(ARRAY, 100, sqr)).toBe(2);
        });
        it("should accept an empty array", () => {
            expect(ary.nearestIndex([], 5, sqr)).toBe(0);
        });
    });

    describe("function nearestValue", () => {
        it("should exist", () => {
            expect(ary.nearestValue).toBeFunction();
        });
        const ARRAY = [1, 2, 3];
        const sqr = n => n ** 2;
        it("should return the nearest element", () => {
            expect(ary.nearestValue(ARRAY, -100, sqr)).toBe(1);
            expect(ary.nearestValue(ARRAY, 0, sqr)).toBe(1);
            expect(ary.nearestValue(ARRAY, 1, sqr)).toBe(1);
            expect(ary.nearestValue(ARRAY, 2, sqr)).toBe(1);
            expect(ary.nearestValue(ARRAY, 2.5, sqr)).toBe(1);
            expect(ary.nearestValue(ARRAY, 3, sqr)).toBe(2);
            expect(ary.nearestValue(ARRAY, 4, sqr)).toBe(2);
            expect(ary.nearestValue(ARRAY, 6, sqr)).toBe(2);
            expect(ary.nearestValue(ARRAY, 6.5, sqr)).toBe(2);
            expect(ary.nearestValue(ARRAY, 7, sqr)).toBe(3);
            expect(ary.nearestValue(ARRAY, 9, sqr)).toBe(3);
            expect(ary.nearestValue(ARRAY, 10, sqr)).toBe(3);
            expect(ary.nearestValue(ARRAY, 100, sqr)).toBe(3);
        });
        it("should accept an empty array", () => {
            expect(ary.nearestValue([], 5, sqr)).toBeUndefined();
        });
    });

    describe("function shiftValues", () => {
        it("should exist", () => {
            expect(ary.shiftValues).toBeFunction();
        });
        it("should shift from an array", () => {
            const ARRAY = [1, 2];
            const it = ary.shiftValues(ARRAY);
            expect(it).toBeIterator();
            expect(ARRAY).toEqual([1, 2]);
            expect(it.next()).toEqual({ done: false, value: 1 });
            expect(ARRAY).toEqual([2]);
            ARRAY.push(3);
            expect(it.next()).toEqual({ done: false, value: 2 });
            expect(ARRAY).toEqual([3]);
            ARRAY.unshift(4);
            expect(it.next()).toEqual({ done: false, value: 4 });
            expect(ARRAY).toEqual([3]);
            expect(it.next()).toEqual({ done: false, value: 3 });
            expect(ARRAY).toEqual([]);
            expect(it.next()).toEqual({ done: true, value: undefined });
        });
    });

    describe("function popValues", () => {
        it("should exist", () => {
            expect(ary.popValues).toBeFunction();
        });
        it("should pop from an array", () => {
            const ARRAY = [2, 1];
            const it = ary.popValues(ARRAY);
            expect(it).toBeIterator();
            expect(ARRAY).toEqual([2, 1]);
            expect(it.next()).toEqual({ done: false, value: 1 });
            expect(ARRAY).toEqual([2]);
            ARRAY.unshift(3);
            expect(it.next()).toEqual({ done: false, value: 2 });
            expect(ARRAY).toEqual([3]);
            ARRAY.push(4);
            expect(it.next()).toEqual({ done: false, value: 4 });
            expect(ARRAY).toEqual([3]);
            expect(it.next()).toEqual({ done: false, value: 3 });
            expect(ARRAY).toEqual([]);
            expect(it.next()).toEqual({ done: true, value: undefined });
        });
    });
});
