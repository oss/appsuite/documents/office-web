/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as re from "@/io.ox/office/tk/algorithms/regexp";

// tests ======================================================================

describe("module tk/algorithms/regexp", () => {

    // public functions -------------------------------------------------------

    describe("function escape", () => {
        it("should exist", () => {
            expect(re.escape).toBeFunction();
        });
        it("should return a pattern for the passed strings", () => {
            expect(re.escape("ab.cd*ef[gh]ij\\kl(mn)op")).toBe("ab\\.cd\\*ef\\[gh\\]ij\\\\kl\\(mn\\)op");
        });
    });

    describe("function alt", () => {
        it("should exist", () => {
            expect(re.alt).toBeFunction();
        });
        it("should return list pattern for the passed strings", () => {
            expect(re.alt("abc", null, false, "def", undefined, "", "*")).toBe("abc|def|*");
            expect(re.alt()).toBe("");
        });
    });

    describe("function capture", () => {
        it("should exist", () => {
            expect(re.capture).toBeFunction();
        });
        it("should return capturing group pattern for the passed strings", () => {
            expect(re.capture("abc", null, false, "def", undefined, "", "*")).toBe("(abc|def|*)");
            expect(re.capture()).toBe("()");
        });
    });

    describe("function group", () => {
        it("should exist", () => {
            expect(re.group).toBeFunction();
        });
        it("should return group pattern for the passed strings", () => {
            expect(re.group("abc", null, false, "def", undefined, "", "*")).toBe("(?:abc|def|*)");
            expect(re.group()).toBe("(?:)");
        });
    });

    describe("function group.opt", () => {
        it("should exist", () => {
            expect(re.group.opt).toBeFunction();
        });
        it("should return optional group pattern for the passed strings", () => {
            expect(re.group.opt("abc", null, false, "def", undefined, "", "*")).toBe("(?:abc|def|*)?");
            expect(re.group.opt()).toBe("(?:)?");
        });
    });

    describe("function ahead", () => {
        it("should exist", () => {
            expect(re.ahead).toBeFunction();
        });
        it("should return lookahead group for the passed strings", () => {
            expect(re.ahead("abc", null, false, "def", undefined, "", "*")).toBe("(?=abc|def|*)");
            expect(re.ahead()).toBe("(?=)");
        });
    });

    describe("function ahead.neg", () => {
        it("should exist", () => {
            expect(re.ahead.neg).toBeFunction();
        });
        it("should return negative lookahead group for the passed strings", () => {
            expect(re.ahead.neg("abc", null, false, "def", undefined, "", "*")).toBe("(?!abc|def|*)");
            expect(re.ahead.neg()).toBe("(?!)");
        });
    });

    describe("function behind", () => {
        it("should exist", () => {
            expect(re.behind).toBeFunction();
        });
        it("should return lookbehind group for the passed strings", () => {
            expect(re.behind("abc", null, false, "def", undefined, "", "*")).toBe("(?<=abc|def|*)");
            expect(re.behind()).toBe("(?<=)");
        });
    });

    describe("function behind.neg", () => {
        it("should exist", () => {
            expect(re.behind.neg).toBeFunction();
        });
        it("should return negative lookbehind group for the passed strings", () => {
            expect(re.behind.neg("abc", null, false, "def", undefined, "", "*")).toBe("(?<!abc|def|*)");
            expect(re.behind.neg()).toBe("(?<!)");
        });
    });

    describe("function class", () => {
        it("should exist", () => {
            expect(re.class).toBeFunction();
        });
        it("should return character class for the passed strings", () => {
            expect(re.class("a", null, false, "b-c", undefined, "", "\\d")).toBe("[ab-c\\d]");
            expect(re.class()).toBe("[]");
        });
    });

    describe("function class.neg", () => {
        it("should exist", () => {
            expect(re.class.neg).toBeFunction();
        });
        it("should return character class for the passed strings", () => {
            expect(re.class.neg("a", null, false, "b-c", undefined, "", "\\d")).toBe("[^ab-c\\d]");
            expect(re.class.neg()).toBe("[^]");
        });
    });
});
