/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as fmt from "@/io.ox/office/tk/algorithms/format";

// tests ======================================================================

describe("module tk/algorithms/format", () => {

    // public functions -------------------------------------------------------

    describe("function formatInt", () => {
        it("should exist", () => {
            expect(fmt.formatInt).toBeFunction();
        });
        it("should convert decimal numbers", () => {
            expect(fmt.formatInt(0, 16)).toBe("0");
            expect(fmt.formatInt(1, 16)).toBe("1");
            expect(fmt.formatInt(2, 16)).toBe("2");
            expect(fmt.formatInt(10, 16)).toBe("A");
            expect(fmt.formatInt(15, 16)).toBe("F");
            expect(fmt.formatInt(16, 16)).toBe("10");
            expect(fmt.formatInt(15, 2)).toBe("1111");
            expect(fmt.formatInt(644641299, 36)).toBe("ANSWER");
        });
        it("should return lower-case letters", () => {
            expect(fmt.formatInt(644641299, 36, { lower: true })).toBe("answer");
        });
        it("should pad with zeros", () => {
            expect(fmt.formatInt(65535, 16, { digits: 2 })).toBe("FFFF");
            expect(fmt.formatInt(65535, 16, { digits: 4 })).toBe("FFFF");
            expect(fmt.formatInt(65535, 16, { digits: 5 })).toBe("0FFFF");
            expect(fmt.formatInt(65535, 16, { digits: 8 })).toBe("0000FFFF");
            expect(fmt.formatInt(65535, 16, { digits: 8, lower: true })).toBe("0000ffff");
        });
    });

    describe("function parseInt", () => {
        it("should exist", () => {
            expect(fmt.parseInt).toBeFunction();
        });
        it("should convert string to integer", () => {
            expect(fmt.parseInt("0", 2)).toBe(0);
            expect(fmt.parseInt("0", 10)).toBe(0);
            expect(fmt.parseInt("101", 2)).toBe(5);
            expect(fmt.parseInt("102", 3)).toBe(11);
            expect(fmt.parseInt("103", 4)).toBe(19);
            expect(fmt.parseInt("104", 5)).toBe(29);
            expect(fmt.parseInt("109", 10)).toBe(109);
            expect(fmt.parseInt("10f", 16)).toBe(271);
            expect(fmt.parseInt("10Z", 36)).toBe(1331);
        });
        it("should process sign character", () => {
            expect(fmt.parseInt("+0101010", 2)).toBe(42);
            expect(fmt.parseInt("-002A", 16)).toBe(-42);
        });
        it("should return NaN for invalid parameter", () => {
            expect(fmt.parseInt(" 1", 2)).toBeNaN();
            expect(fmt.parseInt("1 ", 2)).toBeNaN();
            expect(fmt.parseInt("2", 2)).toBeNaN();
            expect(fmt.parseInt("19", 9)).toBeNaN();
            expect(fmt.parseInt("1g", 16)).toBeNaN();
            expect(fmt.parseInt("1.1", 10)).toBeNaN();
            expect(fmt.parseInt("1,1", 10)).toBeNaN();
            expect(fmt.parseInt("+ 1", 2)).toBeNaN();
            expect(fmt.parseInt("++1", 2)).toBeNaN();
            expect(fmt.parseInt("--1", 2)).toBeNaN();
            expect(fmt.parseInt("+1", 10, { unsigned: true })).toBeNaN();
            expect(fmt.parseInt("-1", 10, { unsigned: true })).toBeNaN();
        });
    });

    describe("function formatAlphabetic", () => {
        it("should exist", () => {
            expect(fmt.formatAlphabetic).toBeFunction();
        });
        it("should convert decimal numbers for method DIGIT", () => {
            expect(fmt.formatAlphabetic(1)).toBe("A");
            expect(fmt.formatAlphabetic(2)).toBe("B");
            expect(fmt.formatAlphabetic(3)).toBe("C");
            expect(fmt.formatAlphabetic(24)).toBe("X");
            expect(fmt.formatAlphabetic(25)).toBe("Y");
            expect(fmt.formatAlphabetic(26)).toBe("Z");
            expect(fmt.formatAlphabetic(27)).toBe("AA");
            expect(fmt.formatAlphabetic(28)).toBe("AB");
            expect(fmt.formatAlphabetic(51)).toBe("AY");
            expect(fmt.formatAlphabetic(52)).toBe("AZ");
            expect(fmt.formatAlphabetic(53)).toBe("BA");
            expect(fmt.formatAlphabetic(702)).toBe("ZZ");
            expect(fmt.formatAlphabetic(703)).toBe("AAA");
            expect(fmt.formatAlphabetic(704)).toBe("AAB");
        });
        it("should convert decimal numbers for method REPEAT", () => {
            expect(fmt.formatAlphabetic(1, { repeat: true })).toBe("A");
            expect(fmt.formatAlphabetic(2, { repeat: true })).toBe("B");
            expect(fmt.formatAlphabetic(3, { repeat: true })).toBe("C");
            expect(fmt.formatAlphabetic(24, { repeat: true })).toBe("X");
            expect(fmt.formatAlphabetic(25, { repeat: true })).toBe("Y");
            expect(fmt.formatAlphabetic(26, { repeat: true })).toBe("Z");
            expect(fmt.formatAlphabetic(27, { repeat: true })).toBe("AA");
            expect(fmt.formatAlphabetic(28, { repeat: true })).toBe("BB");
            expect(fmt.formatAlphabetic(51, { repeat: true })).toBe("YY");
            expect(fmt.formatAlphabetic(52, { repeat: true })).toBe("ZZ");
            expect(fmt.formatAlphabetic(53, { repeat: true })).toBe("AAA");
            expect(fmt.formatAlphabetic(54, { repeat: true })).toBe("BBB");
            expect(fmt.formatAlphabetic(77, { repeat: true })).toBe("YYY");
            expect(fmt.formatAlphabetic(78, { repeat: true })).toBe("ZZZ");
            expect(fmt.formatAlphabetic(79, { repeat: true })).toBe("AAAA");
        });
        it("should return empty string for invalid numbers", () => {
            expect(fmt.formatAlphabetic(0)).toBe("");
            expect(fmt.formatAlphabetic(0, { repeat: true })).toBe("");
            expect(fmt.formatAlphabetic(-1)).toBe("");
            expect(fmt.formatAlphabetic(-1, { repeat: true })).toBe("");
        });
        it("should return lower-case numbers", () => {
            expect(fmt.formatAlphabetic(27, { lower: true })).toBe("aa");
        });
    });

    describe("function parseAlphabetic", () => {
        it("should exist", () => {
            expect(fmt.parseAlphabetic).toBeFunction();
        });
        it("should convert to decimal with method DIGIT", () => {
            expect(fmt.parseAlphabetic("")).toBe(0);
            expect(fmt.parseAlphabetic("A")).toBe(1);
            expect(fmt.parseAlphabetic("B")).toBe(2);
            expect(fmt.parseAlphabetic("C")).toBe(3);
            expect(fmt.parseAlphabetic("X")).toBe(24);
            expect(fmt.parseAlphabetic("Y")).toBe(25);
            expect(fmt.parseAlphabetic("Z")).toBe(26);
            expect(fmt.parseAlphabetic("AA")).toBe(27);
            expect(fmt.parseAlphabetic("AB")).toBe(28);
            expect(fmt.parseAlphabetic("AY")).toBe(51);
            expect(fmt.parseAlphabetic("AZ")).toBe(52);
            expect(fmt.parseAlphabetic("BA")).toBe(53);
            expect(fmt.parseAlphabetic("BB")).toBe(54);
            expect(fmt.parseAlphabetic("ZY")).toBe(701);
            expect(fmt.parseAlphabetic("ZZ")).toBe(702);
            expect(fmt.parseAlphabetic("AAA")).toBe(703);
        });
        it("should convert to decimal with method REPEAT", () => {
            expect(fmt.parseAlphabetic("", { repeat: true })).toBe(0);
            expect(fmt.parseAlphabetic("A", { repeat: true })).toBe(1);
            expect(fmt.parseAlphabetic("B", { repeat: true })).toBe(2);
            expect(fmt.parseAlphabetic("C", { repeat: true })).toBe(3);
            expect(fmt.parseAlphabetic("X", { repeat: true })).toBe(24);
            expect(fmt.parseAlphabetic("Y", { repeat: true })).toBe(25);
            expect(fmt.parseAlphabetic("Z", { repeat: true })).toBe(26);
            expect(fmt.parseAlphabetic("AA", { repeat: true })).toBe(27);
            expect(fmt.parseAlphabetic("BB", { repeat: true })).toBe(28);
            expect(fmt.parseAlphabetic("YY", { repeat: true })).toBe(51);
            expect(fmt.parseAlphabetic("ZZ", { repeat: true })).toBe(52);
            expect(fmt.parseAlphabetic("AAA", { repeat: true })).toBe(53);
            expect(fmt.parseAlphabetic("ZZZ", { repeat: true })).toBe(78);
        });
        it("should accept lower-case strings", () => {
            expect(fmt.parseAlphabetic("Aa")).toBe(27);
            expect(fmt.parseAlphabetic("aA", { repeat: true })).toBe(27);
        });
        it("should return NaN for invalid parameter", () => {
            expect(fmt.parseAlphabetic("I I")).toBeNaN();
            expect(fmt.parseAlphabetic("I I", { repeat: true })).toBeNaN();
            expect(fmt.parseAlphabetic("42")).toBeNaN();
            expect(fmt.parseAlphabetic("42", { repeat: true })).toBeNaN();
            expect(fmt.parseAlphabetic("abc", { repeat: true })).toBeNaN();
        });
    });

    describe("function formatRoman", () => {
        it("should exist", () => {
            expect(fmt.formatRoman).toBeFunction();
        });
        it("should return roman numbers for decimal numbers 1 to 9", () => {
            expect(fmt.formatRoman(1, { depth: -1 })).toBe("I");
            expect(fmt.formatRoman(1, { depth: 0 })).toBe("I");
            expect(fmt.formatRoman(1, { depth: 1 })).toBe("I");
            expect(fmt.formatRoman(1, { depth: 2 })).toBe("I");
            expect(fmt.formatRoman(1, { depth: 3 })).toBe("I");
            expect(fmt.formatRoman(1, { depth: 4 })).toBe("I");
            expect(fmt.formatRoman(2, { depth: 0 })).toBe("II");
            expect(fmt.formatRoman(3, { depth: 0 })).toBe("III");
            expect(fmt.formatRoman(3, { depth: 0 })).toBe("III");
            expect(fmt.formatRoman(4, { depth: -1 })).toBe("IIII");
            expect(fmt.formatRoman(4, { depth: 0 })).toBe("IV");
            expect(fmt.formatRoman(4, { depth: 1 })).toBe("IV");
            expect(fmt.formatRoman(4, { depth: 2 })).toBe("IV");
            expect(fmt.formatRoman(4, { depth: 3 })).toBe("IV");
            expect(fmt.formatRoman(4, { depth: 4 })).toBe("IV");
            expect(fmt.formatRoman(5, { depth: 0 })).toBe("V");
            expect(fmt.formatRoman(6, { depth: 0 })).toBe("VI");
            expect(fmt.formatRoman(7, { depth: 0 })).toBe("VII");
            expect(fmt.formatRoman(8, { depth: 0 })).toBe("VIII");
            expect(fmt.formatRoman(9, { depth: -1 })).toBe("VIIII");
            expect(fmt.formatRoman(9, { depth: 0 })).toBe("IX");
            expect(fmt.formatRoman(9, { depth: 1 })).toBe("IX");
            expect(fmt.formatRoman(9, { depth: 2 })).toBe("IX");
            expect(fmt.formatRoman(9, { depth: 3 })).toBe("IX");
            expect(fmt.formatRoman(9, { depth: 4 })).toBe("IX");
        });
        it("should return roman numbers for decimal numbers 10 to 99", () => {
            expect(fmt.formatRoman(10, { depth: 0 })).toBe("X");
            expect(fmt.formatRoman(19, { depth: -1 })).toBe("XVIIII");
            expect(fmt.formatRoman(19, { depth: 0 })).toBe("XIX");
            expect(fmt.formatRoman(19, { depth: 4 })).toBe("XIX");
            expect(fmt.formatRoman(20, { depth: 0 })).toBe("XX");
            expect(fmt.formatRoman(35, { depth: 0 })).toBe("XXXV");
            expect(fmt.formatRoman(35, { depth: 4 })).toBe("XXXV");
            expect(fmt.formatRoman(39, { depth: 0 })).toBe("XXXIX");
            expect(fmt.formatRoman(39, { depth: 4 })).toBe("XXXIX");
            expect(fmt.formatRoman(40, { depth: 0 })).toBe("XL");
            expect(fmt.formatRoman(40, { depth: 4 })).toBe("XL");
            expect(fmt.formatRoman(44, { depth: 0 })).toBe("XLIV");
            expect(fmt.formatRoman(44, { depth: 4 })).toBe("XLIV");
            expect(fmt.formatRoman(45, { depth: 0 })).toBe("XLV");
            expect(fmt.formatRoman(45, { depth: 1 })).toBe("VL");
            expect(fmt.formatRoman(45, { depth: 2 })).toBe("VL");
            expect(fmt.formatRoman(45, { depth: 3 })).toBe("VL");
            expect(fmt.formatRoman(45, { depth: 4 })).toBe("VL");
            expect(fmt.formatRoman(48, { depth: 0 })).toBe("XLVIII");
            expect(fmt.formatRoman(48, { depth: 1 })).toBe("VLIII");
            expect(fmt.formatRoman(49, { depth: 0 })).toBe("XLIX");
            expect(fmt.formatRoman(49, { depth: 1 })).toBe("VLIV");
            expect(fmt.formatRoman(49, { depth: 2 })).toBe("IL");
            expect(fmt.formatRoman(49, { depth: 3 })).toBe("IL");
            expect(fmt.formatRoman(49, { depth: 4 })).toBe("IL");
            expect(fmt.formatRoman(50, { depth: 0 })).toBe("L");
            expect(fmt.formatRoman(50, { depth: 4 })).toBe("L");
            expect(fmt.formatRoman(94, { depth: 0 })).toBe("XCIV");
            expect(fmt.formatRoman(94, { depth: 4 })).toBe("XCIV");
            expect(fmt.formatRoman(95, { depth: 0 })).toBe("XCV");
            expect(fmt.formatRoman(95, { depth: 1 })).toBe("VC");
            expect(fmt.formatRoman(95, { depth: 4 })).toBe("VC");
            expect(fmt.formatRoman(99, { depth: 0 })).toBe("XCIX");
            expect(fmt.formatRoman(99, { depth: 1 })).toBe("VCIV");
            expect(fmt.formatRoman(99, { depth: 2 })).toBe("IC");
            expect(fmt.formatRoman(99, { depth: 3 })).toBe("IC");
            expect(fmt.formatRoman(99, { depth: 4 })).toBe("IC");
        });
        it("should return roman numbers for decimal numbers 100 to 999", () => {
            expect(fmt.formatRoman(100, { depth: 0 })).toBe("C");
            expect(fmt.formatRoman(100, { depth: 4 })).toBe("C");
            expect(fmt.formatRoman(199, { depth: 0 })).toBe("CXCIX");
            expect(fmt.formatRoman(199, { depth: 1 })).toBe("CVCIV");
            expect(fmt.formatRoman(199, { depth: 2 })).toBe("CIC");
            expect(fmt.formatRoman(199, { depth: 3 })).toBe("CIC");
            expect(fmt.formatRoman(199, { depth: 4 })).toBe("CIC");
            expect(fmt.formatRoman(300, { depth: 0 })).toBe("CCC");
            expect(fmt.formatRoman(300, { depth: 4 })).toBe("CCC");
            expect(fmt.formatRoman(400, { depth: 0 })).toBe("CD");
            expect(fmt.formatRoman(400, { depth: 4 })).toBe("CD");
            expect(fmt.formatRoman(450, { depth: 0 })).toBe("CDL");
            expect(fmt.formatRoman(450, { depth: 1 })).toBe("LD");
            expect(fmt.formatRoman(450, { depth: 4 })).toBe("LD");
            expect(fmt.formatRoman(490, { depth: 0 })).toBe("CDXC");
            expect(fmt.formatRoman(490, { depth: 1 })).toBe("LDXL");
            expect(fmt.formatRoman(490, { depth: 2 })).toBe("XD");
            expect(fmt.formatRoman(490, { depth: 3 })).toBe("XD");
            expect(fmt.formatRoman(490, { depth: 4 })).toBe("XD");
            expect(fmt.formatRoman(495, { depth: 0 })).toBe("CDXCV");
            expect(fmt.formatRoman(495, { depth: 1 })).toBe("LDVL");
            expect(fmt.formatRoman(495, { depth: 2 })).toBe("XDV");
            expect(fmt.formatRoman(495, { depth: 3 })).toBe("VD");
            expect(fmt.formatRoman(495, { depth: 4 })).toBe("VD");
            expect(fmt.formatRoman(499, { depth: 0 })).toBe("CDXCIX");
            expect(fmt.formatRoman(499, { depth: 1 })).toBe("LDVLIV");
            expect(fmt.formatRoman(499, { depth: 2 })).toBe("XDIX");
            expect(fmt.formatRoman(499, { depth: 3 })).toBe("VDIV");
            expect(fmt.formatRoman(499, { depth: 4 })).toBe("ID");
            expect(fmt.formatRoman(500, { depth: 0 })).toBe("D");
            expect(fmt.formatRoman(500, { depth: 4 })).toBe("D");
            expect(fmt.formatRoman(888, { depth: 0 })).toBe("DCCCLXXXVIII");
            expect(fmt.formatRoman(888, { depth: 4 })).toBe("DCCCLXXXVIII");
            expect(fmt.formatRoman(999, { depth: -1 })).toBe("DCCCCLXXXXVIIII");
            expect(fmt.formatRoman(999, { depth: 0 })).toBe("CMXCIX");
            expect(fmt.formatRoman(999)).toBe("CMXCIX");
            expect(fmt.formatRoman(999, { depth: 1 })).toBe("LMVLIV");
            expect(fmt.formatRoman(999, { depth: 2 })).toBe("XMIX");
            expect(fmt.formatRoman(999, { depth: 3 })).toBe("VMIV");
            expect(fmt.formatRoman(999, { depth: 4 })).toBe("IM");
        });
        it("should return roman numbers for decimal numbers 1000 to 3999", () => {
            expect(fmt.formatRoman(1000, { depth: 0 })).toBe("M");
            expect(fmt.formatRoman(1000, { depth: 4 })).toBe("M");
            expect(fmt.formatRoman(3888, { depth: 0 })).toBe("MMMDCCCLXXXVIII");
            expect(fmt.formatRoman(3888, { depth: 4 })).toBe("MMMDCCCLXXXVIII");
            expect(fmt.formatRoman(3999, { depth: 0 })).toBe("MMMCMXCIX");
            expect(fmt.formatRoman(3999, { depth: 1 })).toBe("MMMLMVLIV");
            expect(fmt.formatRoman(3999, { depth: 2 })).toBe("MMMXMIX");
            expect(fmt.formatRoman(3999, { depth: 3 })).toBe("MMMVMIV");
            expect(fmt.formatRoman(3999, { depth: 4 })).toBe("MMMIM");
        });
        it("should return an empty string", () => {
            expect(fmt.formatRoman(0, { depth: -1 })).toBe("");
            expect(fmt.formatRoman(0, { depth: 0 })).toBe("");
            expect(fmt.formatRoman(0, { depth: 1 })).toBe("");
            expect(fmt.formatRoman(0, { depth: 2 })).toBe("");
            expect(fmt.formatRoman(0, { depth: 3 })).toBe("");
            expect(fmt.formatRoman(0, { depth: 4 })).toBe("");
            expect(fmt.formatRoman(-1, { depth: 0 })).toBe("");
            expect(fmt.formatRoman(4000, { depth: 0 })).toBe("");
        });
        it("should return lower-case numbers", () => {
            expect(fmt.formatRoman(1666, { lower: true })).toBe("mdclxvi");
        });
    });

    describe("function parseRoman", () => {
        it("should exist", () => {
            expect(fmt.parseRoman).toBeFunction();
        });
        it("should convert roman to decimal", () => {
            expect(fmt.parseRoman("")).toBe(0);
            expect(fmt.parseRoman("I")).toBe(1);
            expect(fmt.parseRoman("V")).toBe(5);
            expect(fmt.parseRoman("X")).toBe(10);
            expect(fmt.parseRoman("L")).toBe(50);
            expect(fmt.parseRoman("C")).toBe(100);
            expect(fmt.parseRoman("D")).toBe(500);
            expect(fmt.parseRoman("M")).toBe(1000);
            expect(fmt.parseRoman("II")).toBe(2);
            expect(fmt.parseRoman("III")).toBe(3);
            expect(fmt.parseRoman("IIII")).toBe(4);
            expect(fmt.parseRoman("IIIII")).toBe(5);
            expect(fmt.parseRoman("MMMDDDCCCLLLXXXVVVIII")).toBe(4998);
            expect(fmt.parseRoman("IV")).toBe(4);
            expect(fmt.parseRoman("IX")).toBe(9);
            expect(fmt.parseRoman("XXXIX")).toBe(39);
            expect(fmt.parseRoman("IM")).toBe(999);
            expect(fmt.parseRoman("VMIV")).toBe(999);
            expect(fmt.parseRoman("XMIX")).toBe(999);
            expect(fmt.parseRoman("LMVLIV")).toBe(999);
            expect(fmt.parseRoman("CMXCIX")).toBe(999);
            expect(fmt.parseRoman("DMCDLCXLVXIV")).toBe(999);
            expect(fmt.parseRoman("MIM")).toBe(1999);
            expect(fmt.parseRoman("MMMIM")).toBe(3999);
            expect(fmt.parseRoman("MMMIM")).toBe(3999);
            expect(fmt.parseRoman("IIV")).toBe(3);
            expect(fmt.parseRoman("IIIV")).toBe(2);
            expect(fmt.parseRoman("IIIIV")).toBe(1);
            expect(fmt.parseRoman("IIIIIV")).toBe(0);
            expect(fmt.parseRoman("IIIIIIV")).toBe(-1);
            expect(fmt.parseRoman("IMIVIX")).toBe(1002);
            expect(fmt.parseRoman("MDCLXVI")).toBe(1666);
            expect(fmt.parseRoman("IVXLCDM")).toBe(334);
        });
        it("should accept lower-case strings", () => {
            expect(fmt.parseRoman("mdclxvi")).toBe(1666);
            expect(fmt.parseRoman("mDcLxVi")).toBe(1666);
            expect(fmt.parseRoman("MdClXvI")).toBe(1666);
        });
        it("should return NaN for invalid parameter", () => {
            expect(fmt.parseRoman("abc")).toBeNaN();
            expect(fmt.parseRoman("I I")).toBeNaN();
            expect(fmt.parseRoman("42")).toBeNaN();
        });
    });
});
