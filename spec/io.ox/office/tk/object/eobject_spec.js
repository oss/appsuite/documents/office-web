/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import { is } from "@/io.ox/office/tk/algorithms";
import { EventHub } from "@/io.ox/office/tk/events";
import { DObject } from "@/io.ox/office/tk/object/dobject";
import { AbortError } from "@/io.ox/office/tk/object/aborterror";
import { BreakLoopToken, breakLoop, EObject } from "@/io.ox/office/tk/object/eobject";

import { createJestFns, useFakeClock } from "~/asynchelper";

// private functions ==========================================================

function expectPending(promise) {
    expect(promise.state()).toBe("pending");
}

function expectFulfilled(promise) {
    expect(promise.state()).toBe("resolved");
}

function expectRejected(promise, ErrorCtor) {
    expect(promise.state()).toBe("rejected");
    if (ErrorCtor) {
        promise.fail(function (result) {
            expect(result).toBeInstanceOf(ErrorCtor);
        });
    }
}

// tests ======================================================================

describe("module tk/object/eobject", () => {

    const clock = useFakeClock();

    // class BreakLoopToken ---------------------------------------------------

    describe("class BreakLoopToken", () => {
        it("should subclass Error", () => {
            expect(BreakLoopToken).toBeSubClassOf(Error);
        });
    });

    // public functions -------------------------------------------------------

    describe("function breakLoop", () => {
        it("should exist", () => {
            expect(breakLoop).toBeFunction();
        });
        it("should throw", () => {
            expect(breakLoop).toThrow(BreakLoopToken);
        });
    });

    // class EObject ----------------------------------------------------------

    describe("class EObject", () => {

        it("should subclass DObject", () => {
            expect(EObject).toBeSubClassOf(DObject);
        });

        describe("method listenTo", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("listenTo");
            });
            it("should call the event handler for a JQuery object", () => {
                const obj = new EObject(), spies = createJestFns(4);
                // always create new JQuery wrappers for "document"
                obj.listenTo($(document), "test1", spies[0]);
                obj.listenTo($(document), ["test1", "test2"], spies[1]);
                obj.listenTo($(document), "test2", spies[2]);
                obj.listenTo($(document), ["test2", "test3"], spies[3]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                $(document).trigger("wrong");
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                $(document).trigger("test1");
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 0, 0);
                $(document).trigger("test2");
                expect(spies).toHaveAllBeenCalledTimes(1, 2, 1, 1);
                $(document).trigger("test3");
                expect(spies).toHaveAllBeenCalledTimes(1, 2, 1, 2);
                expect(spies[0]).toHaveBeenAlwaysCalledOn(obj);
                expect(spies[1]).toHaveBeenAlwaysCalledOn(obj);
                expect(spies[2]).toHaveBeenAlwaysCalledOn(obj);
                expect(spies[3]).toHaveBeenAlwaysCalledOn(obj);
            });
            it("should remove the event handler on destruction", () => {
                const obj = new EObject(), hub = new EventHub(), spy = jest.fn();
                obj.listenTo(hub, "test", spy);
                obj.listenTo($(document), "test", spy);
                expect(spy).not.toHaveBeenCalled();
                obj.destroy();
                hub.emit("test", 42);
                $(document).trigger("test");
                expect(spy).not.toHaveBeenCalled();
            });
        });

        describe("method listenTo with flag 'once'", () => {
            it("should call the event handler once", () => {
                const obj = new EObject(), hub = new EventHub(), spies = createJestFns(2);
                obj.listenTo(hub, "test", spies[0], { once: true });
                obj.listenTo(hub, "test", spies[1]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                hub.emit("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
                hub.emit("test", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 2);
            });
            it("should remove the event handler on destruction", () => {
                const obj = new EObject(), hub = new EventHub(), spy = jest.fn();
                obj.listenTo(hub, "test", spy, { once: true });
                expect(spy).not.toHaveBeenCalled();
                obj.destroy();
                hub.emit("test", 42);
                expect(spy).not.toHaveBeenCalled();
            });
        });

        describe("method listenTo with promise", () => {
            it("should call the event handler", () => {
                const obj = new EObject(), def = $.Deferred(), hub = new EventHub(), spies = createJestFns(2);
                obj.listenTo(hub, "test", spies[0], { while: def.promise() });
                obj.listenTo(hub, "test", spies[1], { while: def.promise() });
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                hub.emit("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
                hub.emit("test", 43);
                expect(spies).toHaveAllBeenCalledTimes(2, 2);
                def.resolve();
                hub.emit("test", 44);
                expect(spies).toHaveAllBeenCalledTimes(2, 2);
            });
            it("should remove the event handler on destruction", () => {
                const obj = new EObject(), def = $.Deferred(), hub = new EventHub(), spy = jest.fn();
                obj.listenTo(hub, "test", spy, { while: def.promise() });
                expect(spy).not.toHaveBeenCalled();
                obj.destroy();
                hub.emit("test", 42);
                expect(spy).not.toHaveBeenCalled();
            });
        });

        describe("method listenTo with promise and flag 'once'", () => {
            it("should call the event handler", () => {
                const obj = new EObject(), def = $.Deferred(), hub = new EventHub(), spies = createJestFns(3);
                obj.listenTo(hub, "test", spies[0], { while: def.promise(), once: true });
                obj.listenTo(hub, "test", spies[1], { while: def.promise() });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0);
                hub.emit("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 0);
                hub.emit("test", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 2, 0);
                obj.listenTo(hub, "test", spies[2], { while: def.promise(), once: true });
                def.resolve();
                hub.emit("test", 44);
                expect(spies).toHaveAllBeenCalledTimes(1, 2, 0);
            });
            it("should remove the event handler on destruction", () => {
                const obj = new EObject(), def = $.Deferred(), hub = new EventHub(), spy = jest.fn();
                obj.listenTo(hub, "test", spy, { while: def.promise(), once: true });
                expect(spy).not.toHaveBeenCalled();
                obj.destroy();
                hub.emit("test", 42);
                expect(spy).not.toHaveBeenCalled();
            });
        });

        describe("method stopListeningTo", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("stopListeningTo");
            });
            it("should remove an event handler from an event hub", () => {
                const obj = new EObject(), def = $.Deferred(), hub = new EventHub(), spies = createJestFns(8);
                obj.listenTo(hub, "test", spies[0]);
                obj.listenTo(hub, "test", spies[1]);
                obj.listenTo(hub, "test", spies[2], { once: true });
                obj.listenTo(hub, "test", spies[3], { once: true });
                obj.listenTo(hub, "test", spies[4], { while: def.promise() });
                obj.listenTo(hub, "test", spies[5], { while: def.promise() });
                obj.listenTo(hub, "test", spies[6], { while: def.promise(), once: true });
                obj.listenTo(hub, "test", spies[7], { while: def.promise(), once: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0, 0, 0, 0, 0);
                obj.stopListeningTo(hub, "test", spies[0]);
                obj.stopListeningTo(hub, "test", spies[2]);
                obj.stopListeningTo(hub, "test", spies[4]);
                obj.stopListeningTo(hub, "test", spies[6]);
                hub.emit("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(0, 1, 0, 1, 0, 1, 0, 1);
            });
            it("should remove all handlers from an event hub", () => {
                const obj = new EObject(), def = $.Deferred(), hub = new EventHub(), spies = createJestFns(4);
                obj.listenTo(hub, "test", spies[0]);
                obj.listenTo(hub, "test", spies[1], { once: true });
                obj.listenTo(hub, "test", spies[2], { while: def.promise() });
                obj.listenTo(hub, "test", spies[3], { while: def.promise(), once: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                obj.stopListeningTo(hub);
                hub.emit("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
            });
            it("should remove event handlers from a JQuery object", () => {
                // always create new JQuery wrappers for "document"
                const obj = new EObject(), spies = createJestFns(2);
                obj.listenTo($(document), ["test1", "test2"], spies[0]);
                obj.listenTo($(document), ["test1", "test2"], spies[1]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                $(document).trigger("test1");
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
                obj.stopListeningTo($(document));
                $(document).trigger("test1 test2");
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
            });
            it("should remove an event type from a JQuery object", () => {
                // always create new JQuery wrappers for "document"
                const obj = new EObject(), spies = createJestFns(2);
                obj.listenTo($(document), ["test1", "test2", "test3"], spies[0]);
                obj.listenTo($(document), ["test2", "test3", "test4"], spies[1]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                $(document).trigger("test1").trigger("test2").trigger("test3").trigger("test4");
                expect(spies).toHaveAllBeenCalledTimes(3, 3);
                obj.stopListeningTo($(document), ["test2", "test3"]);
                $(document).trigger("test1");
                expect(spies).toHaveAllBeenCalledTimes(4, 3);
                $(document).trigger("test2").trigger("test3");
                expect(spies).toHaveAllBeenCalledTimes(4, 3);
                $(document).trigger("test4");
                expect(spies).toHaveAllBeenCalledTimes(4, 4);
            });
            it("should remove an event type and handler from a JQuery object", () => {
                // always create new JQuery wrappers for "document"
                const obj = new EObject(), spies = createJestFns(2);
                obj.listenTo($(document), ["test1", "test2", "test3"], spies[0]);
                obj.listenTo($(document), ["test2", "test3", "test4"], spies[1]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                $(document).trigger("test1").trigger("test2").trigger("test3").trigger("test4");
                expect(spies).toHaveAllBeenCalledTimes(3, 3);
                obj.stopListeningTo($(document), ["test1", "test2", "test3"], spies[1]);
                $(document).trigger("test1");
                expect(spies).toHaveAllBeenCalledTimes(4, 3);
                $(document).trigger("test2").trigger("test3");
                expect(spies).toHaveAllBeenCalledTimes(6, 3);
                $(document).trigger("test4");
                expect(spies).toHaveAllBeenCalledTimes(6, 4);
            });
        });

        describe("method stopListeningToScope", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("stopListeningToScope");
            });
            it("should remove scoped event handlers", () => {
                const obj = new EObject(), hub = new EventHub(), spies = createJestFns(6);
                const sym1 = Symbol("s1"), sym2 = Symbol("s2");
                obj.listenTo(hub, "test", spies[0]);
                obj.listenTo(hub, "test", spies[1], { scope: sym1 });
                obj.listenTo(hub, "test", spies[2], { scope: sym2 });
                obj.listenTo($(document), "test", spies[3]);
                obj.listenTo($(document), "test", spies[4], { scope: sym1 });
                obj.listenTo($(document), "test", spies[5], { scope: sym2 });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0, 0, 0);
                obj.stopListeningToScope(sym1, hub);
                hub.emit("test");
                $(document).trigger("test");
                expect(spies).toHaveAllBeenCalledTimes(1, 0, 1, 1, 1, 1);
                obj.stopListeningToScope(sym2);
                hub.emit("test");
                $(document).trigger("test");
                expect(spies).toHaveAllBeenCalledTimes(2, 0, 1, 2, 2, 1);
            });
        });

        describe("method stopListening", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("stopListening");
            });
            it("should remove all event handlers", () => {
                const obj = new EObject(), hub = new EventHub(), spies = createJestFns(4);
                obj.listenTo(hub, "test1", spies[0]);
                obj.listenTo(hub, "test2", spies[1]);
                obj.listenTo($(document), "test1", spies[2]);
                obj.listenTo($(document), "test2", spies[3]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                obj.stopListening();
                hub.emit("test1");
                hub.emit("test2");
                $(document).trigger("test1");
                $(document).trigger("test2");
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
            });
        });

        describe("method listenToAllEvents", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("listenToAllEvents");
            });
        });

        describe("method stopListeningToAllEvents", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("stopListeningToAllEvents");
            });
        });

        describe("method listenToGlobal", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("listenToGlobal");
            });
        });

        describe("method stopListeningToGlobal", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("stopListeningToGlobal");
            });
        });

        describe("method listenToProp", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("listenToProp");
            });
        });

        describe("method on", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("on");
            });
        });

        describe("method trigger", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("trigger");
            });
            it("should invoke registered handlers", () => {
                const obj = new EObject(), spies = createJestFns(3);
                obj.on("test1", spies[0]);
                obj.on("test1", spies[1]);
                obj.on("test2", spies[1]);
                obj.on("test2", spies[2]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0);
                obj.trigger("test1", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 0);
                expect(spies[0]).toHaveBeenCalledWith(42);
                expect(spies[0]).toHaveBeenCalledOn(obj);
                expect(spies[1]).toHaveBeenCalledWith(42);
                expect(spies[1]).toHaveBeenCalledOn(obj);
                obj.trigger("test2", "abc", "def");
                expect(spies).toHaveAllBeenCalledTimes(1, 2, 1);
                expect(spies[1]).toHaveBeenCalledWith("abc", "def");
                expect(spies[2]).toHaveBeenCalledWith("abc", "def");
            });
        });

        describe("method one", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("one");
            });
            it("should invoke a handler once", () => {
                const obj = new EObject(), spies = createJestFns(2);
                obj.on("test", spies[0]);
                obj.one("test", spies[1]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                obj.trigger("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
                expect(spies[1]).toHaveBeenCalledOn(obj);
                obj.trigger("test", 43);
                expect(spies).toHaveAllBeenCalledTimes(2, 1);
            });
        });

        describe("methods on/one with type '*'", () => {
            it("should invoke all-handler", () => {
                const obj = new EObject(), spies = createJestFns(4);
                obj.on("test1", spies[0]);
                obj.on("test2", spies[1]);
                obj.on("*", spies[2]);
                obj.one("*", spies[3]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                obj.trigger("test1", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 0, 1, 1);
                expect(spies[0]).toHaveBeenCalledWith(42);
                expect(spies[2]).toHaveBeenCalledWith("test1", 42);
                expect(spies[2]).toHaveBeenCalledOn(obj);
                expect(spies[3]).toHaveBeenCalledWith("test1", 42);
                expect(spies[3]).toHaveBeenCalledOn(obj);
                obj.trigger("test2", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 2, 1);
                expect(spies[1]).toHaveBeenCalledWith(43);
                expect(spies[2]).toHaveBeenCalledWith("test2", 43);
            });
        });

        describe("method off", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("off");
            });
            it("should not invoke a removed handler", () => {
                const obj = new EObject(), spies = createJestFns(6);
                obj.on("test1", spies[0]);
                obj.on("test1", spies[1]);
                obj.on("test2", spies[2]);
                obj.on("test2", spies[3]);
                obj.on("*", spies[4]);
                obj.on("*", spies[5]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0, 0, 0);
                obj.trigger("test1", 42);
                obj.trigger("test2", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 1, 1, 2, 2);
                obj.off("test1", spies[0]);
                obj.off("test2", spies[2]);
                obj.off("*", spies[4]);
                obj.trigger("test1", 42);
                obj.trigger("test2", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 2, 1, 2, 2, 4);
                // remove non-existing handlers does not harm
                obj.off("test1", spies[0]);
                obj.trigger("test1", 44);
                expect(spies).toHaveAllBeenCalledTimes(1, 3, 1, 2, 2, 5);
            });
            it("should remove all handlers", () => {
                const obj = new EObject(), spies = createJestFns(4);
                obj.on("test", spies[0]);
                obj.on("test", spies[1]);
                obj.on("*", spies[2]);
                obj.on("*", spies[3]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                obj.trigger("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 1, 1);
                obj.off("test");
                obj.trigger("test", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 2, 2);
                obj.off("*");
                obj.trigger("test", 44);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 2, 2);
            });
        });

        describe("method setTimeout", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("setTimeout");
            });
            it("should invoke callback after specified delay", () => {
                const obj = new EObject(), spy = jest.fn();
                obj.setTimeout(spy, 10);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(9);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(1);
                expect(spy).toHaveBeenCalledOnce();
                expect(spy).toHaveBeenCalledOn(obj);
            });
            it("should invoke callbacks in correct order", () => {
                const obj = new EObject(), spy1 = jest.fn(), spy2 = jest.fn();
                obj.setTimeout(spy1, 12);
                obj.setTimeout(spy2, 10);
                clock.tick(9);
                expect(spy1).not.toHaveBeenCalled();
                expect(spy2).not.toHaveBeenCalled();
                clock.tick(1);
                expect(spy1).not.toHaveBeenCalled();
                expect(spy2).toHaveBeenCalledOnce();
                clock.tick(2);
                expect(spy1).toHaveBeenCalledOnce();
            });
            it("should not invoke callback when aborting the timer", () => {
                const obj = new EObject(), spy = jest.fn();
                const timer = obj.setTimeout(spy, 10);
                expect(is.abortable(timer)).toBeTrue();
                clock.tick(9);
                timer.abort();
                clock.tick(1000);
                expect(spy).not.toHaveBeenCalled();
                timer.abort(); // repeated call must be ignored
                clock.tick(1000);
                expect(spy).not.toHaveBeenCalled();
            });
            it("should not invoke callback after destruction", () => {
                const obj = new EObject(), spy = jest.fn();
                obj.setTimeout(spy, 10);
                clock.tick(9);
                obj.destroy();
                clock.tick(1000);
                expect(spy).not.toHaveBeenCalled();
            });
        });

        describe("method setInterval", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("setInterval");
            });
            it("should invoke callback and stop on breakLoop()", () => {
                const obj = new EObject();
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockImplementationOnce(() => { throw new BreakLoopToken(); });
                obj.setInterval(spy, 10);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(9);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(1);
                expect(spy).toHaveBeenCalledOn(obj);
                clock.tick(9);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(29);
                expect(spy).toHaveBeenCalledTimes(4);
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(5);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(spy).toHaveBeenNthCalledWith(1, 0);
                expect(spy).toHaveBeenNthCalledWith(2, 1);
                expect(spy).toHaveBeenNthCalledWith(3, 2);
                expect(spy).toHaveBeenNthCalledWith(4, 3);
                expect(spy).toHaveBeenNthCalledWith(5, 4);
                obj.destroy();
            });
            it("should invoke callback after specified delays", () => {
                const obj = new EObject();
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockImplementationOnce(() => { throw new BreakLoopToken(); });
                obj.setInterval(spy, { interval: 5, delay: 100 });
                clock.tick(99);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(1);
                expect(spy).toHaveBeenCalledOn(obj);
                clock.tick(4);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(15);
                expect(spy).toHaveBeenCalledTimes(5);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(5);
                obj.destroy();
            });
            it("should use specified number of iterations", () => {
                const obj = new EObject(), spy = jest.fn();
                obj.setInterval(spy, { interval: 10, count: 3 });
                clock.tick(30);
                expect(spy).toHaveBeenCalledTimes(3);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(3);
                obj.destroy();
            });
            it("should not invoke callback when aborting the timer", () => {
                const obj = new EObject(), spy = jest.fn();
                const timer = obj.setInterval(spy, 10);
                expect(is.abortable(timer)).toBeTrue();
                clock.tick(20);
                expect(spy).toHaveBeenCalledTimes(2);
                timer.abort();
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(2);
                timer.abort(); // repeated call must be ignored
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(2);
                obj.destroy();
            });
            it("should not invoke callback when aborting the timer from callback", () => {
                const obj = new EObject(), spy = jest.fn(function (i) { if (i === 4) { timer.abort(); } });
                const timer = obj.setInterval(spy, 10);
                clock.tick(49);
                expect(spy).toHaveBeenCalledTimes(4);
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(5);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(5);
                obj.destroy();
            });
            it("should not invoke callbacks after destruction", () => {
                const obj = new EObject(), spy = jest.fn();
                obj.setInterval(spy, 10);
                clock.tick(20);
                expect(spy).toHaveBeenCalledTimes(2);
                obj.destroy();
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(2);
            });
        });

        describe("method queueMicrotask", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("queueMicrotask");
            });
            it("should invoke callback", async () => {
                const obj = new EObject(), spy = jest.fn();
                obj.queueMicrotask(spy);
                expect(spy).not.toHaveBeenCalled();
                await clock.wait(1);
                expect(spy).toHaveBeenCalledOnce();
                expect(spy).toHaveBeenCalledOn(obj);
            });
            it("should not invoke callback when aborting the task", async () => {
                const obj = new EObject(), spy = jest.fn();
                const timer = obj.queueMicrotask(spy);
                expect(is.abortable(timer)).toBeTrue();
                timer.abort();
                await clock.wait(1000);
                expect(spy).not.toHaveBeenCalled();
                timer.abort();
            });
            it("should not invoke callback after destruction", async () => {
                const obj = new EObject(), spy = jest.fn();
                obj.queueMicrotask(spy);
                obj.destroy();
                await clock.wait(1000);
                expect(spy).not.toHaveBeenCalled();
            });
        });

        describe("method requestAnimationFrame", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("requestAnimationFrame");
            });
            it("should invoke callback", () => {
                const obj = new EObject(), spy = jest.fn();
                obj.requestAnimationFrame(spy);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(100);
                expect(spy).toHaveBeenCalledOnce();
                expect(spy).toHaveBeenCalledOn(obj);
            });
            it("should not invoke callback when aborting the frame", () => {
                const obj = new EObject(), spy = jest.fn();
                const timer = obj.requestAnimationFrame(spy);
                expect(is.abortable(timer)).toBeTrue();
                timer.abort();
                clock.tick(1000);
                expect(spy).not.toHaveBeenCalled();
                timer.abort();
            });
            it("should not invoke callback after destruction", () => {
                const obj = new EObject(), spy = jest.fn();
                obj.requestAnimationFrame(spy);
                obj.destroy();
                clock.tick(1000);
                expect(spy).not.toHaveBeenCalled();
            });
        });

        describe("method asyncLoop", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("asyncLoop");
            });

            /**
             * Creates a callback spy that simulates to execute for 150ms, returns
             * `ret` for the first four invocations, and breaks at the fifth
             * invocation.
             */
            function makeSpy(ret) {
                return jest.fn(function (i) {
                    clock.tick(150);
                    return (i === 4) ? breakLoop() : ret;
                });
            }

            it("should invoke callback in time slices and stop on breakLoop()", () => {
                const obj = new EObject(), spy = makeSpy(null);
                // spy will run twice (150 each)
                const promise = obj.asyncLoop(spy);
                expectPending(promise);
                expect(spy).toHaveBeenCalledTimes(2);
                // next cycle starts in 10
                clock.tick(9);
                expect(spy).toHaveBeenCalledTimes(2);
                // clock is now 1 before next cycle
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(4);
                // clock is now after cycle, next cycle starts in 10
                clock.tick(9);
                expect(spy).toHaveBeenCalledTimes(4);
                // clock is now 1 before next cycle
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(spy).toHaveBeenAlwaysCalledOn(obj);
                expectFulfilled(promise);
                expect(spy).toHaveBeenNthCalledWith(1, 0);
                expect(spy).toHaveBeenNthCalledWith(2, 1);
                expect(spy).toHaveBeenNthCalledWith(3, 2);
                expect(spy).toHaveBeenNthCalledWith(4, 3);
                expect(spy).toHaveBeenNthCalledWith(5, 4);
            });
            it("should invoke callbacks after specified slice/interval times", () => {
                const obj = new EObject(), spy = makeSpy();
                // spy will run trice (150 each)
                const promise = obj.asyncLoop(spy, { slice: 400, interval: 100 });
                expectPending(promise);
                expect(spy).toHaveBeenCalledTimes(3);
                // clock is now after first cycle, next cycle starts in 100
                clock.tick(99);
                expect(spy).toHaveBeenCalledTimes(3);
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(5);
                expectFulfilled(promise);
            });
            it("should use specified number of iterations", () => {
                const obj = new EObject(), spy = makeSpy();
                // spy will run twice (150 each)
                const promise = obj.asyncLoop(spy, { interval: 100, count: 3 });
                expectPending(promise);
                expect(spy).toHaveBeenCalledTimes(2);
                // clock is now after first cycle, next cycle starts in 100
                clock.tick(99);
                expect(spy).toHaveBeenCalledTimes(2);
                // clock is now 1 before next cycle
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(3);
                expectFulfilled(promise);
            });
            it("should defer next iteration to callback promise", () => {
                const obj = new EObject();
                const def = $.Deferred();
                const spy = makeSpy(def.promise());
                // spy will run once (150 each) and return a pending promise
                const promise = obj.asyncLoop(spy);
                expectPending(promise);
                expect(spy).toHaveBeenCalledTimes(1);
                // loop will wait for the promise
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(1);
                // resolve the deferred object to let the loop continue
                def.resolve();
                expectPending(promise);
                expect(spy).toHaveBeenCalledTimes(1);
                // trigger the next cycles, spy will run twice because it returns a resolved promise
                clock.tick(10);
                expect(spy).toHaveBeenCalledTimes(3);
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(5);
                expectFulfilled(promise);
            });
            it("should abort on rejected callback promise", () => {
                const obj = new EObject();
                const def = $.Deferred();
                const spy = makeSpy(def.promise());
                // spy will run once (150 each) and return a pending promise
                const promise = obj.asyncLoop(spy);
                expectPending(promise);
                expect(spy).toHaveBeenCalledTimes(1);
                // loop will wait for the promise
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(1);
                // reject the deferred object
                def.reject();
                expect(spy).toHaveBeenCalledTimes(1);
                expectRejected(promise);
                // loop must not continue after a rejected promise
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(1);
            });
            it("should not invoke callbacks after destruction", () => {
                const obj = new EObject(), spy = makeSpy();
                // spy will run twice (150 each)
                const promise = obj.asyncLoop(spy);
                expectPending(promise);
                expect(spy).toHaveBeenCalledTimes(2);
                // destroy the object
                obj.destroy();
                expectRejected(promise, AbortError);
                expect(spy).toHaveBeenCalledTimes(2);
                // loop must not continue after destruction
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(2);
            });
        });

        describe("method asyncForEach", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("asyncForEach");
            });
        });

        describe("method asyncReduce", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("asyncReduce");
            });
        });

        describe("method debounce", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("debounce");
            });
            it("should invoke callbacks", () => {
                const obj = new EObject(), spies = createJestFns(4);
                const debounce1 = obj.debounce(spies[0]);
                const debounce2 = obj.debounce(spies[1], spies[2]);
                const debounce3 = obj.debounce(spies[3], { immediate: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                debounce1("a", 1);
                debounce2("a", 1);
                debounce3("a", 1);
                expect(spies).toHaveAllBeenCalledTimes(0, 1, 0, 1);
                expect(spies[1]).toHaveBeenCalledOn(obj);
                expect(spies[3]).toHaveBeenCalledOn(obj);
                debounce1("b", 2);
                debounce2("b", 2);
                debounce3("b", 2);
                expect(spies).toHaveAllBeenCalledTimes(0, 2, 0, 2);
                debounce1("c", 3);
                debounce2("c", 3);
                debounce3("c", 3);
                expect(spies).toHaveAllBeenCalledTimes(0, 3, 0, 3);
                clock.tick(2);
                expect(spies).toHaveAllBeenCalledTimes(1, 3, 1, 4);
                expect(spies[0]).toHaveBeenCalledOn(obj);
                expect(spies[2]).toHaveBeenCalledOn(obj);
                expect(spies[1]).toHaveBeenNthCalledWith(1, "a", 1);
                expect(spies[1]).toHaveBeenNthCalledWith(2, "b", 2);
                expect(spies[1]).toHaveBeenNthCalledWith(3, "c", 3);
            });
            it("should use specified delay time", () => {
                const obj = new EObject(), spies = createJestFns(4);
                const debounce1 = obj.debounce(spies[0], { delay: 10 });
                const debounce2 = obj.debounce(spies[1], spies[2], { delay: 10 });
                const debounce3 = obj.debounce(spies[3], { delay: 10, immediate: true });
                debounce1();
                debounce2();
                debounce3();
                clock.tick(2);
                expect(spies).toHaveAllBeenCalledTimes(0, 1, 0, 1);
                debounce1();
                debounce2();
                debounce3();
                clock.tick(9);
                expect(spies).toHaveAllBeenCalledTimes(0, 2, 0, 2);
                debounce1();
                debounce2();
                debounce3();
                clock.tick(9);
                expect(spies).toHaveAllBeenCalledTimes(0, 3, 0, 3);
                clock.tick(3);
                expect(spies).toHaveAllBeenCalledTimes(1, 3, 1, 4);
            });
            it("should use specified maximum delay time", () => {
                const obj = new EObject(), spies = createJestFns(3);
                const debounce1 = obj.debounce(spies[0], { delay: 100, maxDelay: 300 });
                const debounce2 = obj.debounce(spies[1], spies[2], { delay: 100, maxDelay: 300 });
                debounce1();
                debounce2();
                clock.tick(99);
                debounce1();
                debounce2();
                clock.tick(99);
                debounce1();
                debounce2();
                clock.tick(99);
                expect(spies).toHaveAllBeenCalledTimes(0, 3, 0);
                debounce1();
                debounce2();
                clock.tick(2);
                expect(spies).toHaveAllBeenCalledTimes(0, 4, 0);
                clock.tick(2);
                expect(spies).toHaveAllBeenCalledTimes(1, 4, 1);
                debounce1();
                debounce2();
                clock.tick(99);
                expect(spies).toHaveAllBeenCalledTimes(1, 5, 1);
                clock.tick(2);
                expect(spies).toHaveAllBeenCalledTimes(2, 5, 2);
            });
            it("should not invoke callback after destruction", () => {
                const obj = new EObject(), spies = createJestFns(3);
                const debounce1 = obj.debounce(spies[0], { delay: 10 });
                const debounce2 = obj.debounce(spies[1], spies[2], { delay: 10 });
                debounce1();
                debounce2();
                clock.tick(9);
                expect(spies).toHaveAllBeenCalledTimes(0, 1, 0);
                obj.destroy();
                clock.tick(1000);
                expect(spies).toHaveAllBeenCalledTimes(0, 1, 0);
            });
            it("should invoke callbacks in animation frame", () => {
                const obj = new EObject(), spies = createJestFns(4);
                const frameSpy = jest.spyOn(window, "requestAnimationFrame");
                const debounce1 = obj.debounce(spies[0], { delay: "animationframe" });
                const debounce2 = obj.debounce(spies[1], spies[2], { delay: "animationframe" });
                const debounce3 = obj.debounce(spies[3], { delay: "animationframe", immediate: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                expect(frameSpy).not.toHaveBeenCalled();
                debounce1("a", 1);
                debounce2("a", 1);
                debounce3("a", 1);
                expect(spies).toHaveAllBeenCalledTimes(0, 1, 0, 1);
                expect(frameSpy).toHaveBeenCalledTimes(3);
                expect(spies[1]).toHaveBeenCalledOn(obj);
                expect(spies[3]).toHaveBeenCalledOn(obj);
                debounce1("b", 2);
                debounce2("b", 2);
                debounce3("b", 2);
                expect(spies).toHaveAllBeenCalledTimes(0, 2, 0, 2);
                expect(frameSpy).toHaveBeenCalledTimes(3);
                debounce1("c", 3);
                debounce2("c", 3);
                debounce3("c", 3);
                expect(spies).toHaveAllBeenCalledTimes(0, 3, 0, 3);
                expect(frameSpy).toHaveBeenCalledTimes(3);
                clock.tick(50); // one animation frame lasts 16ms usually
                expect(spies).toHaveAllBeenCalledTimes(1, 3, 1, 4);
                expect(spies[0]).toHaveBeenCalledOn(obj);
                expect(spies[2]).toHaveBeenCalledOn(obj);
                expect(spies[1]).toHaveBeenNthCalledWith(1, "a", 1);
                expect(spies[1]).toHaveBeenNthCalledWith(2, "b", 2);
                expect(spies[1]).toHaveBeenNthCalledWith(3, "c", 3);
                expect(frameSpy).toHaveBeenCalledTimes(3);
                debounce1("d", 4);
                debounce2("d", 4);
                debounce3("d", 4);
                expect(spies).toHaveAllBeenCalledTimes(1, 4, 1, 5);
                expect(frameSpy).toHaveBeenCalledTimes(6);
                clock.tick(50); // one animation frame lasts 16ms usually
                expect(spies).toHaveAllBeenCalledTimes(2, 4, 2, 6);
                expect(spies[1]).toHaveBeenNthCalledWith(4, "d", 4);
                expect(frameSpy).toHaveBeenCalledTimes(6);
            });
            it("should not invoke callback in animation frame after destruction", () => {
                const obj = new EObject(), spy = jest.fn();
                const debounce = obj.debounce(spy, { delay: "animationframe" });
                debounce();
                expect(spy).not.toHaveBeenCalled();
                obj.destroy();
                clock.tick(1000);
                expect(spy).not.toHaveBeenCalled();
            });
        });

        describe("method synchronize", () => {
            it("should exist", () => {
                expect(EObject).toHaveMethod("synchronize");
            });
            it("should invoke callback synchronized", () => {
                const obj = new EObject(), spy = jest.fn();
                const defs = _.times(3, () => {
                    const def = new $.Deferred();
                    spy.mockReturnValueOnce(def);
                    return def;
                });
                const synchronize = obj.synchronize(spy);
                expect(spy).not.toHaveBeenCalled();
                const promise0 = synchronize();
                expect(spy).toHaveBeenCalledTimes(1);
                expectPending(promise0);
                const promise1 = synchronize();
                expect(spy).toHaveBeenCalledTimes(1);
                expectPending(promise1);
                const promise2 = synchronize();
                expect(spy).toHaveBeenCalledTimes(1);
                expectPending(promise2);
                clock.tick(100);
                defs[0].resolve();
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(2);
                expectFulfilled(promise0);
                expectPending(promise1);
                clock.tick(100);
                defs[2].reject();
                expect(spy).toHaveBeenCalledTimes(2);
                expectPending(promise1);
                expectPending(promise2);
                clock.tick(100);
                defs[1].resolve();
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(3);
                expectFulfilled(promise1);
                expectRejected(promise2);
            });
            it("should pass parameters to callback", () => {
                const obj = new EObject(), spy = jest.fn();
                const defs = _.times(3, () => {
                    const def = new $.Deferred();
                    spy.mockReturnValueOnce(def);
                    return def;
                });
                const synchronize = obj.synchronize(spy);
                expect(spy).not.toHaveBeenCalled();
                synchronize(1, "a");
                synchronize(2, "b");
                synchronize(3, "c");
                clock.tick(20);
                defs[0].resolve();
                clock.tick(100);
                defs[2].resolve();
                clock.tick(100);
                defs[1].resolve();
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(spy).toHaveBeenAlwaysCalledOn(obj);
                expect(spy).toHaveBeenNthCalledWith(1, 1, "a");
                expect(spy).toHaveBeenNthCalledWith(2, 2, "b");
                expect(spy).toHaveBeenNthCalledWith(3, 3, "c");
            });
            it("should synchronize recursive invocations", () => {
                const obj = new EObject();
                let running = false;
                const spy = jest.fn(i => {
                    expect(running).toBeFalse();
                    running = true;
                    if (i < 3) { synchronize(i + 1); }
                    running = false;
                });
                const synchronize = obj.synchronize(spy);
                synchronize(1);
            });
            it("should not invoke callbacks after destruction", () => {
                const obj = new EObject(), spy = jest.fn();
                const defs = _.times(3, () => {
                    const def = new $.Deferred();
                    spy.mockReturnValueOnce(def);
                    return def;
                });
                const synchronize = obj.synchronize(spy);
                const promises = defs.map(synchronize);
                clock.tick(100);
                defs[0].resolve();
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(2);
                expectFulfilled(promises[0]);
                clock.tick(100);
                expectPending(promises[1]);
                expectPending(promises[2]);
                obj.destroy();
                defs[1].resolve();
                clock.tick(1000);
                defs[2].resolve();
                expect(spy).toHaveBeenCalledTimes(2);
                expectRejected(promises[1], AbortError);
                expectRejected(promises[2], AbortError);
            });
        });
    });
});
