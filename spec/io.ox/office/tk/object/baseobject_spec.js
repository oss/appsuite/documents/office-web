/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { itr, jpromise } from "@/io.ox/office/tk/algorithms";
import { EObject } from "@/io.ox/office/tk/object/eobject";
import { BaseObject } from "@/io.ox/office/tk/object/baseobject";
import * as Utils from "@/io.ox/office/tk/utils";

import { useFakeClock } from "~/asynchelper";

// tests ======================================================================

describe("module tk/object/baseobject", () => {

    /* eslint-disable promise/valid-params */

    // JQuery.Deferred base test ----------------------------------------------

    describe("class jQuery.Deferred", () => {
        it("should exist", () => {
            expect($.Deferred).toBeFunction();
        });
        it("should create a pending deferred object", () => {
            const def = $.Deferred();
            expect(def).toBeObject();
            expect(def).toRespondTo("state");
            expect(def).toRespondTo("promise");
            expect(def).toRespondTo("resolve");
            expect(def).toRespondTo("reject");
            expect(def).toRespondTo("done");
            expect(def).toRespondTo("fail");
            expect(def).toRespondTo("then");
        });
        it("should have pending state", () => {
            expect($.Deferred().state()).toBe("pending");
        });
        it("should resolve immediately", () => {
            const spy = jest.fn();
            const def = $.Deferred().done(spy);
            expect(spy).not.toHaveBeenCalled();
            expect(def.resolve()).toBe(def);
            expect(def.state()).toBe("resolved");
            expect(spy).toHaveBeenCalledOnce();
        });
        it("should reject immediately", () => {
            const spy = jest.fn();
            const def = $.Deferred().fail(spy);
            expect(spy).not.toHaveBeenCalled();
            expect(def.reject()).toBe(def);
            expect(def.state()).toBe("rejected");
            expect(spy).toHaveBeenCalledOnce();
        });
        it("should resolve chained promises asynchronously", async () => {
            const def = $.Deferred();
            expect(def.state()).toBe("pending");
            const promise1 = def.then();
            expect(promise1.state()).toBe("pending");
            // when resolving the deferred, promise1 must remain pending
            expect(def.resolve()).toBe(def);
            expect(def.state()).toBe("resolved");
            expect(promise1.state()).toBe("pending");
            // promise2 piped to the resolved deferred must remain pending too
            const promise2 = def.then();
            expect(promise2.state()).toBe("pending");
            await new Promise(resolve => { window.setTimeout(resolve, 10); });
            expect(promise1.state()).toBe("resolved");
            expect(promise2.state()).toBe("resolved");
        });
    });

    // class BaseObject -------------------------------------------------------

    describe("class BaseObject", () => {

        const clock = useFakeClock();

        it("should subclass EObject", () => {
            expect(BaseObject).toBeSubClassOf(EObject);
        });

        describe("method registerDestructor", () => {
            it("should exist", () => {
                expect(BaseObject).toHaveMethod("registerDestructor");
            });
            it("should register a destructor callback", () => {
                const obj = new BaseObject(), spy = jest.fn();
                obj.registerDestructor(spy);
                expect(spy).not.toHaveBeenCalled();
            });
        });

        describe("method createAbortablePromise", () => {
            const obj = new BaseObject();
            it("should exist", () => {
                expect(BaseObject).toHaveMethod("createAbortablePromise");
            });
            it("should create an abortable promise", () => {
                const promise = obj.createAbortablePromise($.Deferred());
                expect(promise).toBeObject();
                expect(promise).toRespondTo("state");
                expect(promise).toRespondTo("promise");
                expect(promise).toRespondTo("abort");
                expect(promise.state()).toBe("pending");
                promise.abort();
                expect(promise.state()).toBe("rejected");
            });
            it("should create an abortable promise of a resolved deferred object", () => {
                const promise = obj.createAbortablePromise($.Deferred().resolve());
                expect(promise).toBeObject();
                expect(promise).toRespondTo("abort");
                expect(promise.state()).toBe("resolved");
                promise.abort();
                expect(promise.state()).toBe("resolved");
            });
            it("should invoke the callback on abort", () => {
                const spy = jest.fn();
                const promise = obj.createAbortablePromise($.Deferred(), spy);
                expect(spy).not.toHaveBeenCalled();
                promise.abort();
                expect(spy).toHaveBeenCalledOnce();
                expect(spy).toHaveBeenCalledOn(obj);
                expect(spy).toHaveBeenCalledWith("abort");
            });
            it("should abort the promise after a timeout", () => {
                const promise = obj.createAbortablePromise($.Deferred(), null, 10);
                expect(promise.state()).toBe("pending");
                clock.tick(11);
                expect(promise.state()).toBe("rejected");
            });
            it("should override promise.then() to return an abortable promise", () => {
                const promise1 = obj.createAbortablePromise($.Deferred());
                const promise2 = promise1.then();
                expect(promise1).not.toBe(promise2);
                expect(promise2).toRespondTo("abort");
            });
            it("should forward the result of the done handler of the chained promise", () => {
                const spy1 = jest.fn(), spy2 = jest.fn(), spy3 = jest.fn();
                const deferred = new $.Deferred();
                const promise1 = obj.createAbortablePromise(deferred).done(spy1);
                const promise2 = promise1.then(() => 2).done(spy2);
                const promise3 = promise2.then(() => { throw 3; }).fail(spy3);
                deferred.resolve(1);
                expect(promise1.state()).toBe("resolved");
                expect(promise2.state()).toBe("pending");
                expect(promise3.state()).toBe("pending");
                clock.tick(10);
                expect(promise2.state()).toBe("resolved");
                expect(promise3.state()).toBe("rejected");
                expect(spy1).toHaveBeenCalledWith(1);
                expect(spy2).toHaveBeenCalledWith(2);
                expect(spy3).toHaveBeenCalledWith(3);
            });
            it("should forward the result of the fail handler of the chained promise", () => {
                const spy1 = jest.fn(), spy2 = jest.fn(), spy3 = jest.fn();
                const deferred = new $.Deferred();
                const promise1 = obj.createAbortablePromise(deferred).fail(spy1);
                const promise2 = promise1.then(null, () => { throw 2; }).fail(spy2);
                const promise3 = promise2.then(null, () => 3).done(spy3);
                deferred.reject(1);
                expect(promise1.state()).toBe("rejected");
                expect(promise2.state()).toBe("pending");
                expect(promise3.state()).toBe("pending");
                clock.tick(10);
                expect(promise2.state()).toBe("rejected");
                expect(promise3.state()).toBe("resolved");
                expect(spy1).toHaveBeenCalledWith(1);
                expect(spy2).toHaveBeenCalledWith(2);
                expect(spy3).toHaveBeenCalledWith(3);
            });
            it("should forward asynchronous results of the chained promise", () => {
                const deferred1 = new $.Deferred();
                const promise1 = obj.createAbortablePromise(deferred1);
                const deferred2 = new $.Deferred();
                const promise2 = promise1.then(() => deferred2);
                const spy1 = jest.fn(), spy2 = jest.fn();
                promise1.done(spy1);
                promise2.done(spy2);
                clock.tick(10);
                expect(promise1.state()).toBe("pending");
                expect(promise2.state()).toBe("pending");
                expect(spy1).not.toHaveBeenCalled();
                expect(spy2).not.toHaveBeenCalled();
                deferred1.resolve();
                expect(promise1.state()).toBe("resolved");
                expect(promise2.state()).toBe("pending");
                expect(spy1).toHaveBeenCalledOnce();
                expect(spy2).not.toHaveBeenCalled();
                clock.tick(10);
                expect(promise2.state()).toBe("pending");
                expect(spy2).not.toHaveBeenCalled();
                deferred2.resolve();
                expect(promise2.state()).toBe("resolved");
                expect(spy2).toHaveBeenCalledOnce();
            });
            it("should abort a promise chain synchronously when aborting the root promise", () => {
                const spy = jest.fn();
                const promise1 = obj.createAbortablePromise($.Deferred(), spy).fail(spy);
                const promise2 = promise1.then(null, spy, null, spy).fail(spy);
                const promise3 = promise2.then(null, spy, null, spy).fail(spy);
                promise1.abort(42);
                expect(promise1.state()).toBe("rejected");
                expect(promise2.state()).toBe("rejected");
                expect(promise3.state()).toBe("rejected");
                expect(spy).toHaveBeenCalledTimes(8);
                for (let i = 1; i <= 8; i += 1) {
                    expect(spy).toHaveBeenNthCalledWith(i, 42);
                }
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(8);
            });
            it("should abort a promise chain synchronously when aborting the leaf promise", () => {
                const spy = jest.fn();
                const promise1 = obj.createAbortablePromise($.Deferred(), spy).fail(spy);
                const promise2 = promise1.then(null, spy, null, spy).fail(spy);
                const promise3 = promise2.then(null, spy, null, spy).fail(spy);
                promise3.abort(42);
                expect(promise1.state()).toBe("rejected");
                expect(promise2.state()).toBe("rejected");
                expect(promise3.state()).toBe("rejected");
                expect(spy).toHaveBeenCalledTimes(8);
                for (let i = 1; i <= 8; i += 1) {
                    expect(spy).toHaveBeenNthCalledWith(i, 42);
                }
            });
            // bug 55078, 55585: promises created by another BaseObject do not get aborted in the chain
            it("should abort embedded promises from all sources", () => {
                const def1 = $.Deferred();
                const spy1 = jest.fn();
                const promise1 = obj.createAbortablePromise(def1, spy1);
                const obj2 = new BaseObject();
                const def2 = $.Deferred();
                const spy2 = jest.fn();
                const promise2 = obj2.createAbortablePromise(def2, spy2);
                const root = obj.createResolvedPromise();
                const chain1 = root.then(() => promise1);
                const chain2 = root.then(() => promise2);
                expect(spy1).not.toHaveBeenCalled();
                expect(spy2).not.toHaveBeenCalled();
                clock.tick(1000);
                expect(chain1.state()).toBe("pending");
                expect(chain2.state()).toBe("pending");
                root.abort();
                expect(spy1).toHaveBeenCalledOnce();
                expect(chain1.state()).toBe("rejected");
                expect(def1.state()).toBe("rejected");
                expect(spy2).toHaveBeenCalledOnce();
                expect(chain2.state()).toBe("rejected");
                expect(def2.state()).toBe("rejected");
            });
            it("should abort the promise on destruction", () => {
                const spy = jest.fn();
                const promise = obj.createAbortablePromise($.Deferred(), spy);
                expect(promise.state()).toBe("pending");
                obj.destroy();
                expect(promise.state()).toBe("rejected");
                expect(spy).toHaveBeenCalledWith("destroy");
            });
        });

        describe("method createResolvedPromise", () => {
            const obj = new BaseObject();
            it("should exist", () => {
                expect(BaseObject).toHaveMethod("createResolvedPromise");
            });
            it("should create a resolved abortable promise", () => {
                const promise = obj.createResolvedPromise(42);
                expect(promise).toBeObject();
                expect(promise).toRespondTo("abort");
                expect(promise.state()).toBe("resolved");
                promise.abort();
                expect(promise.state()).toBe("resolved");
            });
            it("should override promise.then() to return an abortable promise", () => {
                const promise1 = obj.createResolvedPromise(42);
                const promise2 = promise1.then();
                expect(promise1).not.toBe(promise2);
                expect(promise2).toRespondTo("abort");
                expect(promise2.state()).toBe("pending");
            });
        });

        describe("method createRejectedPromise", () => {
            const obj = new BaseObject();
            it("should exist", () => {
                expect(BaseObject).toHaveMethod("createRejectedPromise");
            });
            it("should create a rejected abortable promise", () => {
                const promise = obj.createRejectedPromise(42);
                expect(promise).toBeObject();
                expect(promise).toRespondTo("abort");
                expect(promise.state()).toBe("rejected");
                promise.abort();
                expect(promise.state()).toBe("rejected");
            });
            it("should override promise.then() to return an abortable promise", () => {
                const promise1 = obj.createRejectedPromise(42);
                const promise2 = promise1.then();
                expect(promise1).not.toBe(promise2);
                expect(promise2).toRespondTo("abort");
                expect(promise2.state()).toBe("pending");
            });
        });

        describe("method convertToPromise", () => {
            const obj = new BaseObject();
            it("should exist", () => {
                expect(BaseObject).toHaveMethod("convertToPromise");
            });
            it("should create a resolved promise", () => {
                const spy = jest.fn();
                const promise = obj.convertToPromise(42).done(spy);
                expect(promise).toRespondTo("abort");
                expect(promise.state()).toBe("resolved");
                expect(spy).toHaveBeenCalledOnce();
                expect(spy).toHaveBeenCalledWith(42);
            });
            it("should return an existing promise", () => {
                const promise1 = obj.createResolvedPromise(42);
                const promise2 = obj.createRejectedPromise(42);
                const promise3 = new $.Deferred().promise();
                const promise4 = Promise.resolve();
                expect(obj.convertToPromise(promise1)).toBe(promise1);
                expect(obj.convertToPromise(promise2)).toBe(promise2);
                expect(obj.convertToPromise(promise3)).toBe(promise3);
                const promise5 = obj.convertToPromise(promise4);
                expect(jpromise.is(promise5)).toBeTrue();
                expect(promise5.state()).toBe("pending");
            });
        });

        describe("method createDeferred", () => {
            const obj = new BaseObject();
            it("should exist", () => {
                expect(BaseObject).toHaveMethod("createDeferred");
            });
            it("should create a pending deferred object", () => {
                const def = obj.createDeferred();
                expect(def).toBeObject();
                expect(def).toRespondTo("state");
                expect(def).toRespondTo("promise");
                expect(def).toRespondTo("resolve");
                expect(def).toRespondTo("reject");
                expect(def.state()).toBe("pending");
            });
            it("should provide abortable promises", () => {
                const def = obj.createDeferred();
                expect(def.state()).toBe("pending");
                const promise = def.promise();
                expect(promise).toRespondTo("state");
                expect(promise).toRespondTo("promise");
                expect(promise).toRespondTo("abort");
                expect(promise.state()).toBe("pending");
                promise.abort();
                expect(promise.state()).toBe("rejected");
                expect(def.state()).toBe("rejected");
            });
            it("should reject the deferred object on destruction", () => {
                const spy = jest.fn();
                const def = obj.createDeferred().fail(spy);
                expect(def.state()).toBe("pending");
                obj.destroy();
                expect(def.state()).toBe("rejected");
                expect(spy).toHaveBeenCalledWith("destroy");
            });
        });

        describe("method waitForEvent", () => {
            it("should exist", () => {
                expect(BaseObject).toHaveMethod("waitForEvent");
            });
            it("should wait for an event", () => {
                const obj = new BaseObject(), src = $({}), spy = jest.fn();
                const promise = obj.waitForEvent(src, "test").done(spy);
                expect(promise.state()).toBe("pending");
                clock.tick(10);
                expect(promise.state()).toBe("pending");
                src.trigger("other");
                expect(promise.state()).toBe("pending");
                src.trigger("test", [42, "abc"]);
                expect(promise.state()).toBe("resolved");
                expect(spy).toHaveBeenCalledOnce();
                const args = spy.mock.calls[0];
                expect(args).toBeArrayOfSize(1);
                expect(args[0]).toBeArrayOfSize(3);
                expect(args[0][0]).toMatchObject({ type: "test" });
                expect(args[0][1]).toBe(42);
                expect(args[0][2]).toBe("abc");
            });
            it("should reject on a timeout", () => {
                const obj = new BaseObject(), src = $({}), spy = jest.fn();
                const promise = obj.waitForEvent(src, "test", 15).fail(spy);
                expect(promise.state()).toBe("pending");
                clock.tick(10);
                expect(promise.state()).toBe("pending");
                src.trigger("other");
                expect(promise.state()).toBe("pending");
                clock.tick(10);
                expect(promise.state()).toBe("rejected");
                expect(spy).toHaveBeenCalledOnce();
                expect(spy).toHaveBeenCalledWith("timeout");
            });
        });

        describe("method executeDelayed", () => {
            const obj = new BaseObject("window-0");
            it("should exist", () => {
                expect(BaseObject).toHaveMethod("executeDelayed");
            });
            it("should invoke callback delayed", () => {
                const spy = jest.fn();
                const timer = obj.executeDelayed(spy);
                expect(spy).not.toHaveBeenCalled();
                expect(timer.state()).toBe("pending");
                clock.tick(2);
                expect(timer.state()).toBe("resolved");
                expect(spy).toHaveBeenCalledOnce();
                expect(spy).toHaveBeenCalledOn(obj);
            });
            it("should invoke callbacks after specified delay time", () => {
                const spy = jest.fn();
                obj.executeDelayed(spy, 10);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(9);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(2);
                expect(spy).toHaveBeenCalledOnce();
            });
            it("should invoke callbacks in correct order", () => {
                const spy1 = jest.fn(), spy2 = jest.fn();
                obj.executeDelayed(spy1, 12); // fires at 12
                obj.executeDelayed(spy2, 10); // fires at 10
                clock.tick(11); // timer fires at 10
                expect(spy1).not.toHaveBeenCalled();
                expect(spy2).toHaveBeenCalledOnce();
                // move time to 13
                clock.tick(2); // timer fires at 12
                expect(spy1).toHaveBeenCalledOnce();
            });
            it("should allow to abort a timer", () => {
                const spy = jest.fn();
                const timer = obj.executeDelayed(spy, 10);
                clock.tick(9);
                timer.abort();
                expect(timer.state()).toBe("rejected");
                clock.tick(1000);
                expect(spy).not.toHaveBeenCalled();
            });
            it("should forward the callback result", () => {
                const spy1 = jest.fn().mockReturnValue(42);
                const spy2 = jest.fn();
                const timer = obj.executeDelayed(spy1, 10).done(spy2);
                clock.tick(20);
                expect(timer.state()).toBe("resolved");
                expect(spy1).toHaveBeenCalledOnce();
                expect(spy2).toHaveBeenCalledWith(42);
            });
            it("should defer timer promise to callback promise", () => {
                const def = $.Deferred();
                const spy1 = jest.fn().mockReturnValue(def.promise());
                const spy2 = jest.fn();
                const timer = obj.executeDelayed(spy1, 10).done(spy2);
                clock.tick(20);
                expect(spy1).toHaveBeenCalledOnce();
                expect(timer.state()).toBe("pending");
                expect(spy2).not.toHaveBeenCalled();
                def.resolve(42);
                expect(timer.state()).toBe("resolved");
                expect(spy2).toHaveBeenCalledWith(42);
            });
            it("should abort the nested promise", () => {
                const promise = obj.createAbortablePromise($.Deferred());
                const spy = jest.fn(() => promise);
                const timer = obj.executeDelayed(spy, 10);
                clock.tick(9);
                expect(timer.state()).toBe("pending");
                expect(spy).not.toHaveBeenCalled();
                clock.tick(2);
                expect(timer.state()).toBe("pending");
                expect(spy).toHaveBeenCalledOnce();
                expect(promise.state()).toBe("pending");
                timer.abort();
                expect(timer.state()).toBe("rejected");
                expect(promise.state()).toBe("rejected");
            });
            it("should not invoke callbacks after destruction", () => {
                const spy = jest.fn();
                const timer = obj.executeDelayed(spy, 10);
                clock.tick(9);
                obj.destroy();
                clock.tick(1000);
                expect(spy).not.toHaveBeenCalled();
                expect(timer.state()).toBe("rejected");
            });
        });

        describe("method repeatDelayed", () => {
            const obj = new BaseObject();
            it("should exist", () => {
                expect(BaseObject).toHaveMethod("repeatDelayed");
            });
            it("should invoke callback delayed and stop on Utils.BREAK", () => {
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce(Utils.BREAK);
                const timer = obj.repeatDelayed(spy, { delay: 0 });
                expect(spy).not.toHaveBeenCalled();
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(spy).toHaveBeenAlwaysCalledOn(obj);
                expect(timer.state()).toBe("resolved");
            });
            it("should pass iteration index to callback", () => {
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce(Utils.BREAK);
                const timer = obj.repeatDelayed(spy, { delay: 0 });
                clock.tick(1000);
                expect(spy).toHaveBeenNthCalledWith(1, 0);
                expect(spy).toHaveBeenNthCalledWith(2, 1);
                expect(spy).toHaveBeenNthCalledWith(3, 2);
                expect(timer.state()).toBe("resolved");
            });
            it("should invoke callbacks after specified delay time", () => {
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce(Utils.BREAK);
                const timer = obj.repeatDelayed(spy, { delay: 100 }); // fires at 100, 200, 300, ...
                clock.tick(99);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(2);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(98);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(2);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(98);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(2);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(timer.state()).toBe("resolved");
            });
            it("should accept delay time directly as numeric parameter", () => {
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce(Utils.BREAK);
                const timer = obj.repeatDelayed(spy, 100);
                clock.tick(99);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(2);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(timer.state()).toBe("resolved");
            });
            it("should run first iteration synchronously", () => {
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce(Utils.BREAK);
                const timer = obj.repeatDelayed(spy);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(10000);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(timer.state()).toBe("resolved");
            });
            it("should use different repetition delay time", () => {
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce(Utils.BREAK);
                const timer = obj.repeatDelayed(spy, { delay: 10, repeatDelay: 100 });
                clock.tick(9);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(2);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(98);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(2);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(98);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(2);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(timer.state()).toBe("resolved");
            });
            it("should use specified number of cycles", () => {
                const spy = jest.fn();
                const timer = obj.repeatDelayed(spy, { delay: 100, cycles: 3 });
                clock.tick(99);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(2);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(timer.state()).toBe("resolved");
            });
            it("should allow to abort a timer", () => {
                const spy = jest.fn();
                const timer = obj.repeatDelayed(spy, 100);
                clock.tick(250);
                expect(spy).toHaveBeenCalledTimes(2);
                timer.abort();
                expect(timer.state()).toBe("rejected");
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(2);
            });
            it("should defer next iteration to callback promise", () => {
                const def = $.Deferred();
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(def).mockReturnValueOnce(Utils.BREAK);
                const timer = obj.repeatDelayed(spy, 100);
                clock.tick(150);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(200);
                expect(spy).toHaveBeenCalledTimes(2);
                def.resolve();
                clock.tick(99);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(2);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(timer.state()).toBe("resolved");
            });
            it("should abort on rejected callback promise", () => {
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce($.Deferred().reject());
                const timer = obj.repeatDelayed(spy, 100);
                clock.tick(250);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(timer.state()).toBe("rejected");
            });
            it("should abort the nested promise", () => {
                const promise = obj.createAbortablePromise($.Deferred());
                const spy = jest.fn(() => promise);
                const timer = obj.repeatDelayed(spy, 100);
                clock.tick(99);
                expect(timer.state()).toBe("pending");
                expect(spy).not.toHaveBeenCalled();
                clock.tick(2);
                expect(timer.state()).toBe("pending");
                expect(spy).toHaveBeenCalledOnce();
                expect(promise.state()).toBe("pending");
                timer.abort();
                expect(timer.state()).toBe("rejected");
                expect(promise.state()).toBe("rejected");
            });
            it("should reduce delay time after a pending promise in fastAsync mode", () => {
                const def1 = $.Deferred();
                const def2 = $.Deferred();
                const spy = jest.fn().mockReturnValueOnce(def1).mockReturnValueOnce(def2).mockReturnValueOnce(Utils.BREAK);
                const timer = obj.repeatDelayed(spy, { delay: 100, fastAsync: true });
                clock.tick(110);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(40);
                def1.resolve();
                clock.tick(40);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(20);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(200);
                expect(spy).toHaveBeenCalledTimes(2);
                def2.resolve();
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(timer.state()).toBe("resolved");
            });
            it("should not invoke callbacks after destruction", () => {
                const spy = jest.fn();
                const timer = obj.repeatDelayed(spy, 100);
                clock.tick(250);
                expect(spy).toHaveBeenCalledTimes(2);
                obj.destroy();
                expect(timer.state()).toBe("rejected");
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(2);
            });
        });

        describe("method repeatSliced", () => {

            // Creates a callback spy that simulates to execute for 150ms, returns "ret"
            // for the first four invocations, and Utils.BREAK for the fifth invocation.
            function makeSpy(ret) {
                return jest.fn(i => {
                    clock.tick(150);
                    return (i === 4) ? Utils.BREAK : ret;
                });
            }

            const obj = new BaseObject();

            it("should exist", () => {
                expect(BaseObject).toHaveMethod("repeatSliced");
            });
            it("should invoke callback in time slices and stop on Utils.BREAK", () => {
                const spy = makeSpy(null);
                const timer = obj.repeatSliced(spy, { delay: 0 });
                expect(spy).not.toHaveBeenCalled();
                // tick once to trigger first slice; spy will run twice (150 each)
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                // clock is now 1 after slice#1, next cycle starts in 9
                clock.tick(8);
                expect(spy).toHaveBeenCalledTimes(2);
                // clock is now 1 before slice#2
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(4);
                // clock is now after slice#2, next cycle starts in 10
                clock.tick(9);
                expect(spy).toHaveBeenCalledTimes(4);
                // clock is now 1 before slice#3
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(spy).toHaveBeenAlwaysCalledOn(obj);
                expect(timer.state()).toBe("resolved");
            });
            it("should pass iteration index to callback", () => {
                const spy = makeSpy();
                const timer = obj.repeatSliced(spy, { delay: 0 });
                // run all time slices at once
                clock.tick(10000);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
                expect(spy).toHaveBeenNthCalledWith(1, 0);
                expect(spy).toHaveBeenNthCalledWith(2, 1);
                expect(spy).toHaveBeenNthCalledWith(3, 2);
                expect(spy).toHaveBeenNthCalledWith(4, 3);
                expect(spy).toHaveBeenNthCalledWith(5, 4);
            });
            it("should invoke callbacks after specified delay/slice/interval times", () => {
                const spy = makeSpy();
                const timer = obj.repeatSliced(spy, { delay: 10, slice: 200, interval: 100 });
                expect(spy).not.toHaveBeenCalled();
                clock.tick(9);
                expect(spy).not.toHaveBeenCalled();
                // clock is now at 9, tick once to trigger the first cycle
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                // clock is now after first slice, the next cycle starts in 100
                clock.tick(99);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(4);
                // clock is now after slice#2, the next cycle starts in 100
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
            });
            it("should use specified number of cycles", () => {
                const spy = makeSpy();
                const timer = obj.repeatSliced(spy, { cycles: 3, interval: 100 });
                // tick once to trigger the first slice; spy will run twice (150 each)
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                // clock is now 1 after slice#1, next time slice starts in 99
                clock.tick(98);
                expect(spy).toHaveBeenCalledTimes(2);
                // clock is now 1 before slice#2
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(timer.state()).toBe("resolved");
            });
            it("should run first iteration synchronously", () => {
                const spy = makeSpy();
                const timer = obj.repeatSliced(spy);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(10000);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
            });
            it("should allow to abort a timer", () => {
                const spy = makeSpy();
                const timer = obj.repeatSliced(spy, { delay: 0 });
                // tick once to trigger the first cycle; spy will run twice (150 each)
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                timer.abort();
                expect(timer.state()).toBe("rejected");
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(2);
            });
            it("should defer next iteration to callback promise", () => {
                const def = $.Deferred();
                const spy = makeSpy(def.promise());
                const timer = obj.repeatSliced(spy, { delay: 0 });
                // tick once to trigger the first cycle; spy will run once (150 each) and return a pending promise
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(1);
                // loop will wait for the promise
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(1);
                // resolve the deferred object to let the loop continue
                def.resolve();
                expect(timer.state()).toBe("pending");
                expect(spy).toHaveBeenCalledTimes(1);
                // trigger the next cycles, spy will run twice because it returns a resolved promise
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(3);
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
            });
            it("should abort on rejected callback promise", () => {
                const def = $.Deferred();
                const spy = makeSpy(def.promise());
                const timer = obj.repeatSliced(spy, { delay: 0 });
                // tick once to trigger the first cycle; spy will run once (150 each) and return a pending promise
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(1);
                // clock is now 1 after first cycle, loop will wait for the promise
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(1);
                // reject the deferred object
                def.reject();
                expect(spy).toHaveBeenCalledTimes(1);
                expect(timer.state()).toBe("rejected");
                // loop must not continue after a rejected promise
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(1);
            });
            it("should abort the nested promise", () => {
                const promise = obj.createAbortablePromise($.Deferred());
                const spy = jest.fn(() => promise);
                const timer = obj.repeatSliced(spy, { delay: 10 });
                clock.tick(9);
                expect(timer.state()).toBe("pending");
                expect(spy).not.toHaveBeenCalled();
                clock.tick(2);
                expect(timer.state()).toBe("pending");
                expect(spy).toHaveBeenCalledOnce();
                expect(promise.state()).toBe("pending");
                timer.abort();
                expect(timer.state()).toBe("rejected");
                expect(promise.state()).toBe("rejected");
            });
            it("should not invoke callbacks after destruction", () => {
                const spy = makeSpy();
                const timer = obj.repeatSliced(spy, { delay: 0 });
                // tick once to trigger the first cycle; spy will run twice (150 each)
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                // destroy the object
                obj.destroy();
                expect(timer.state()).toBe("rejected");
                // loop must not continue after destruction
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(2);
            });
        });

        describe("method iterateSliced", () => {

            // See test cases for BaseObject.repeatSliced() above for details about how
            // these tests work.

            // Creates a callback spy that simulates to execute for 150ms, and returns "ret".
            function makeSpy(ret) {
                return jest.fn(() => {
                    clock.tick(150);
                    return ret;
                });
            }

            const obj = new BaseObject();

            it("should exist", () => {
                expect(BaseObject).toHaveMethod("iterateSliced");
            });
            it("should invoke callback in time slices", () => {
                const spy = makeSpy();
                const timer = obj.iterateSliced(itr.interval(0, 4), spy, { delay: 0 });
                expect(spy).not.toHaveBeenCalled();
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(8);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(4);
                clock.tick(9);
                expect(spy).toHaveBeenCalledTimes(4);
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(spy).toHaveBeenAlwaysCalledOn(obj);
                expect(timer.state()).toBe("resolved");
            });
            it("should stop on Utils.BREAK", () => {
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(Utils.BREAK);
                const timer = obj.iterateSliced(itr.interval(0, 4), spy, { delay: 0 });
                expect(spy).not.toHaveBeenCalled();
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                expect(timer.state()).toBe("resolved");
            });
            it("should pass correct values to callback function", () => {
                const spy = makeSpy();
                const timer = obj.iterateSliced(itr.interval(0, 4), spy, { delay: 0 });
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
                expect(spy).toHaveBeenNthCalledWith(1, 0);
                expect(spy).toHaveBeenNthCalledWith(2, 1);
                expect(spy).toHaveBeenNthCalledWith(3, 2);
                expect(spy).toHaveBeenNthCalledWith(4, 3);
                expect(spy).toHaveBeenNthCalledWith(5, 4);
            });
            it("should invoke callbacks after specified delay/slice/interval time", () => {
                const spy = makeSpy();
                const timer = obj.iterateSliced(itr.interval(0, 4), spy, { delay: 10, slice: 200, interval: 100 });
                expect(spy).not.toHaveBeenCalled();
                clock.tick(9);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(4);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
            });
            it("should run first iteration synchronously", () => {
                const spy = makeSpy();
                const timer = obj.iterateSliced(itr.interval(0, 4), spy);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(10000);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
            });
            it("should allow to abort a timer", () => {
                const spy = makeSpy();
                const timer = obj.iterateSliced(itr.interval(0, 4), spy, { delay: 0 });
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                timer.abort();
                expect(timer.state()).toBe("rejected");
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(2);
            });
            it("should defer next iteration to callback promise", () => {
                const def = $.Deferred();
                const spy = makeSpy(def.promise());
                const timer = obj.iterateSliced(itr.interval(0, 4), spy, { delay: 0 });
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(1);
                def.resolve();
                expect(timer.state()).toBe("pending");
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(3);
                clock.tick(9);
                expect(spy).toHaveBeenCalledTimes(5);
                // last array element visited, but iterator did not return the final {done:true} yet
                expect(timer.state()).toBe("pending");
                clock.tick(10);
                expect(timer.state()).toBe("resolved");
            });
            it("should abort on rejected callback promise", () => {
                const def = $.Deferred();
                const spy = makeSpy(def.promise());
                const timer = obj.iterateSliced(itr.interval(0, 4), spy, { delay: 0 });
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(1);
                def.reject();
                expect(timer.state()).toBe("rejected");
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(1);
            });
            it("should abort the nested promise", () => {
                const promise = obj.createAbortablePromise($.Deferred());
                const spy = jest.fn(() => promise);
                const timer = obj.iterateSliced(itr.interval(0, 4), spy, { delay: 10 });
                clock.tick(9);
                expect(timer.state()).toBe("pending");
                expect(spy).not.toHaveBeenCalled();
                clock.tick(2);
                expect(timer.state()).toBe("pending");
                expect(spy).toHaveBeenCalledOnce();
                expect(promise.state()).toBe("pending");
                timer.abort();
                expect(timer.state()).toBe("rejected");
                expect(promise.state()).toBe("rejected");
            });
            it("should not invoke callbacks after destruction", () => {
                const spy = makeSpy();
                const timer = obj.iterateSliced(itr.interval(0, 4), spy, { delay: 0 });
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                obj.destroy();
                expect(timer.state()).toBe("rejected");
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(2);
            });
        });

        describe("method iterateArraySliced", () => {

            // See test cases for BaseObject.repeatSliced() above for details about how
            // these tests work.

            // Creates a callback spy that simulates to execute for 150ms, and returns "ret".
            function makeSpy(ret) {
                return jest.fn(() => {
                    clock.tick(150);
                    return ret;
                });
            }

            const ARRAY = ["a", "b", "c", "d", "e"];
            const obj = new BaseObject();

            it("should exist", () => {
                expect(BaseObject).toHaveMethod("iterateArraySliced");
            });
            it("should return a resolved promise, if an empty array is given", () => {
                const spy = jest.fn();
                const timer = obj.iterateArraySliced([], spy, { delay: 0 });
                expect(timer).toBeObject();
                expect(timer.state()).toBe("resolved");
                expect(spy).not.toHaveBeenCalled();
            });
            it("should invoke callback in time slices", () => {
                const spy = makeSpy();
                const timer = obj.iterateArraySliced(ARRAY, spy, { delay: 0 });
                expect(spy).not.toHaveBeenCalled();
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(8);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(4);
                clock.tick(9);
                expect(spy).toHaveBeenCalledTimes(4);
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(spy).toHaveBeenAlwaysCalledOn(obj);
                expect(timer.state()).toBe("resolved");
            });
            it("should stop on Utils.BREAK", () => {
                const spy = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(Utils.BREAK);
                const timer = obj.iterateArraySliced(ARRAY, spy, { delay: 0 });
                expect(spy).not.toHaveBeenCalled();
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                expect(timer.state()).toBe("resolved");
            });
            it("should pass array elements to callback", () => {
                const spy = makeSpy();
                const timer = obj.iterateArraySliced(ARRAY, spy, { delay: 0 });
                clock.tick(10000);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
                expect(spy).toHaveBeenNthCalledWith(1, "a", 0, ARRAY);
                expect(spy).toHaveBeenNthCalledWith(2, "b", 1, ARRAY);
                expect(spy).toHaveBeenNthCalledWith(3, "c", 2, ARRAY);
                expect(spy).toHaveBeenNthCalledWith(4, "d", 3, ARRAY);
                expect(spy).toHaveBeenNthCalledWith(5, "e", 4, ARRAY);
            });
            it("should process array elements in reversed order", () => {
                const spy = makeSpy();
                const timer = obj.iterateArraySliced(ARRAY, spy, { reverse: true });
                clock.tick(10000);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
                expect(spy).toHaveBeenNthCalledWith(1, "e", 4, ARRAY);
                expect(spy).toHaveBeenNthCalledWith(2, "d", 3, ARRAY);
                expect(spy).toHaveBeenNthCalledWith(3, "c", 2, ARRAY);
                expect(spy).toHaveBeenNthCalledWith(4, "b", 1, ARRAY);
                expect(spy).toHaveBeenNthCalledWith(5, "a", 0, ARRAY);
            });
            it("should work with array-like objects", () => {
                const spy = makeSpy();
                const timer = obj.iterateArraySliced("abcde", spy, { delay: 0 });
                clock.tick(10000);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
                expect(spy).toHaveBeenNthCalledWith(1, "a", 0, "abcde");
                expect(spy).toHaveBeenNthCalledWith(2, "b", 1, "abcde");
                expect(spy).toHaveBeenNthCalledWith(3, "c", 2, "abcde");
                expect(spy).toHaveBeenNthCalledWith(4, "d", 3, "abcde");
                expect(spy).toHaveBeenNthCalledWith(5, "e", 4, "abcde");
            });
            it("should invoke callbacks after specified delay/slice/interval time", () => {
                const spy = makeSpy();
                const timer = obj.iterateArraySliced(ARRAY, spy, { delay: 10, slice: 200, interval: 100 });
                expect(spy).not.toHaveBeenCalled();
                clock.tick(9);
                expect(spy).not.toHaveBeenCalled();
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(4);
                clock.tick(100);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
            });
            it("should run first iteration synchronously", () => {
                const spy = makeSpy();
                const timer = obj.iterateArraySliced(ARRAY, spy);
                expect(spy).toHaveBeenCalledTimes(2);
                clock.tick(10000);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
            });
            it("should allow to abort a timer", () => {
                const spy = makeSpy();
                const timer = obj.iterateArraySliced(ARRAY, spy, { delay: 0 });
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                timer.abort();
                expect(timer.state()).toBe("rejected");
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(2);
            });
            it("should defer next iteration to callback promise", () => {
                const def = $.Deferred();
                const spy = makeSpy(def.promise());
                const timer = obj.iterateArraySliced(ARRAY, spy, { delay: 0 });
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(1);
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(1);
                def.resolve();
                expect(timer.state()).toBe("pending");
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(3);
                clock.tick(9);
                expect(spy).toHaveBeenCalledTimes(5);
                expect(timer.state()).toBe("resolved");
            });
            it("should abort on rejected callback promise", () => {
                const def = $.Deferred();
                const spy = makeSpy(def.promise());
                const timer = obj.iterateArraySliced(ARRAY, spy, { delay: 0 });
                clock.tick(1);
                expect(spy).toHaveBeenCalledOnce();
                clock.tick(1000);
                expect(spy).toHaveBeenCalledOnce();
                def.reject();
                expect(timer.state()).toBe("rejected");
                clock.tick(1000);
                expect(spy).toHaveBeenCalledOnce();
            });
            it("should abort the nested promise", () => {
                const promise = obj.createAbortablePromise($.Deferred());
                const spy = jest.fn(() => promise);
                const timer = obj.iterateArraySliced(ARRAY, spy, { delay: 10 });
                clock.tick(9);
                expect(timer.state()).toBe("pending");
                expect(spy).not.toHaveBeenCalled();
                clock.tick(2);
                expect(timer.state()).toBe("pending");
                expect(spy).toHaveBeenCalledOnce();
                expect(promise.state()).toBe("pending");
                timer.abort();
                expect(timer.state()).toBe("rejected");
                expect(promise.state()).toBe("rejected");
            });
            it("should not invoke callbacks after destruction", () => {
                const spy = makeSpy();
                const timer = obj.iterateArraySliced(ARRAY, spy, { delay: 0 });
                clock.tick(1);
                expect(spy).toHaveBeenCalledTimes(2);
                obj.destroy();
                expect(timer.state()).toBe("rejected");
                clock.tick(1000);
                expect(spy).toHaveBeenCalledTimes(2);
            });
        });

        describe("method destroy", () => {

            const obj = new BaseObject();
            const src1 = $({}), src2 = $({});
            const def = $.Deferred();
            const spy1 = jest.fn();
            const spy2 = jest.fn();
            const spy3 = jest.fn();

            obj.registerDestructor(spy1);
            obj.registerDestructor(spy2);
            obj.listenTo(src1, "test1", spy3);
            obj.listenTo(src2, "test2", spy3);
            obj.onFulfilled(def, spy3);

            it("should exist", () => {
                expect(BaseObject).toHaveMethod("destroy");
            });
            it("should call the registered destructors in reversed order", () => {
                obj.myProperty = true;
                obj.registerDestructor(() => {
                    expect(obj.destroyed).toBeTrue();
                });
                expect(obj.destroyed).toBeFalse();
                obj.destroy();
                expect(obj.destroyed).toBeTrue();
                expect(spy1).toHaveBeenCalledOnce();
                expect(spy2).toHaveBeenCalledOnce();
                expect(spy2).toHaveBeenCalledBefore(spy1);
            });
            it("should delete the prototype and public methods", () => {
                expect(obj).not.toHaveProperty("myProperty");
            });
            it("should release event handlers", () => {
                src1.trigger("test1");
                expect(spy3).not.toHaveBeenCalled();
            });
            it("should release promise handlers", () => {
                def.resolve();
                expect(spy3).not.toHaveBeenCalled();
            });
        });
    });

    /* eslint-enable promise/valid-params */
});
