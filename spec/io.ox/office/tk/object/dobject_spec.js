/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import { DObject } from "@/io.ox/office/tk/object/dobject";

import { createJestFns } from "~/asynchelper";

// private functions ==========================================================

function wait(t) {
    return new Promise(resolve => { window.setTimeout(resolve, t); });
}

// tests ======================================================================

describe("module tk/object/dobject", () => {

    // class DObject ----------------------------------------------------------

    describe("class DObject", () => {

        it("should exist", () => {
            expect(DObject).toBeClass();
        });

        class BaseClass extends DObject { destructor() { super.destructor(); } }
        class MidClass extends BaseClass { }
        class SubClass extends MidClass { destructor() { super.destructor(); } }

        describe("static function makeUid", () => {
            it("should exist", () => {
                expect(DObject).toHaveStaticMethod("makeUid");
            });
            it("should create different identifiers", () => {
                const uid1 = DObject.makeUid(), uid2 = DObject.makeUid();
                expect(uid1).toMatch(/^uid\d+$/);
                expect(uid2).toMatch(/^uid\d+$/);
                expect(uid1).not.toBe(uid2);
            });
        });

        describe("static function isDestroyed", () => {
            it("should exist", () => {
                expect(DObject).toHaveStaticMethod("isDestroyed");
            });
            it("should return false for other values", () => {
                expect(DObject.isDestroyed(null)).toBeFalse();
                expect(DObject.isDestroyed(42)).toBeFalse();
                expect(DObject.isDestroyed("abc")).toBeFalse();
                expect(DObject.isDestroyed({})).toBeFalse();
                expect(DObject.isDestroyed({ destroyed: true })).toBeFalse();
            });
        });

        describe("property uid", () => {
            const obj1 = new DObject(), obj2 = new DObject();
            it("should exist", () => {
                expect(obj1.uid).toMatch(/^uid\d+$/);
                expect(obj2.uid).toMatch(/^uid\d+$/);
            });
            it("should differ for different objects", () => {
                expect(obj1.uid).not.toBe(obj2.uid);
            });
        });

        describe("property destroyed", () => {
            const obj = new DObject();
            it("should not exist", () => {
                expect(obj.destroyed).toBeFalse();
            });
        });

        describe("method onFulfilled", () => {
            it("should exist", () => {
                expect(DObject).toHaveMethod("onFulfilled");
            });
            it("should wait for fulfilment of native promise", async () => {
                const obj = new DObject(), p1 = Promise.resolve(42), p2 = Promise.reject("error"), spies = createJestFns(4);
                obj.onFulfilled(p1, spies[0]);
                obj.onFulfilled(p1, spies[1], { async: true });
                obj.onFulfilled(p2, spies[2]);
                obj.onFulfilled(p2, spies[3], { async: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                await wait(1);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 0, 0);
                expect(spies[0]).toHaveBeenCalledWith(42);
                expect(spies[0]).toHaveBeenCalledOn(obj);
                expect(spies[1]).toHaveBeenCalledWith(42);
                expect(spies[1]).toHaveBeenCalledOn(obj);
            });
            it("should wait for fulfilment of JQuery promise", async () => {
                const obj = new DObject(), def1 = $.Deferred(), def2 = $.Deferred(), spies = createJestFns(4);
                obj.onFulfilled(def1.promise(), spies[0]);
                obj.onFulfilled(def1.promise(), spies[1], { async: true });
                obj.onFulfilled(def2.promise(), spies[2]);
                obj.onFulfilled(def2.promise(), spies[3], { async: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                def1.resolve(42);
                def2.reject("error");
                expect(spies).toHaveAllBeenCalledTimes(1, 0, 0, 0);
                expect(spies[0]).toHaveBeenCalledWith(42);
                expect(spies[0]).toHaveBeenCalledOn(obj);
                await wait(1);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 0, 0);
                expect(spies[1]).toHaveBeenCalledWith(42);
                expect(spies[1]).toHaveBeenCalledOn(obj);
            });
            it("should handle plain values", async () => {
                const obj = new DObject(), spies = createJestFns(2);
                obj.onFulfilled(42, spies[0]);
                obj.onFulfilled(42, spies[1], { async: true });
                expect(spies).toHaveAllBeenCalledTimes(1, 0);
                expect(spies[0]).toHaveBeenCalledWith(42);
                expect(spies[0]).toHaveBeenCalledOn(obj);
                await wait(1);
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
                expect(spies[1]).toHaveBeenCalledWith(42);
                expect(spies[1]).toHaveBeenCalledOn(obj);
            });
            it("should not invoke callback after destruction", async () => {
                const obj = new DObject(), def = $.Deferred(), spies = createJestFns(2);
                obj.onFulfilled(42, spies[0], { async: true });
                obj.onFulfilled(def.promise(), spies[1]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                obj.destroy();
                def.resolve();
                await wait(10);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
            });
        });

        describe("method onRejected", () => {
            it("should exist", () => {
                expect(DObject).toHaveMethod("onRejected");
            });
            it("should wait for rejection of native promise", async () => {
                const obj = new DObject(), p1 = Promise.resolve(42), p2 = Promise.reject("error"), spies = createJestFns(4);
                obj.onRejected(p1, spies[0]);
                obj.onRejected(p1, spies[1], { async: true });
                obj.onRejected(p2, spies[2]);
                obj.onRejected(p2, spies[3], { async: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                await wait(1);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 1, 1);
                expect(spies[2]).toHaveBeenCalledWith("error");
                expect(spies[2]).toHaveBeenCalledOn(obj);
                expect(spies[3]).toHaveBeenCalledWith("error");
                expect(spies[3]).toHaveBeenCalledOn(obj);
            });
            it("should wait for rejection of JQuery promise", async () => {
                const obj = new DObject(), def1 = $.Deferred(), def2 = $.Deferred(), spies = createJestFns(4);
                obj.onRejected(def1.promise(), spies[0]);
                obj.onRejected(def1.promise(), spies[1], { async: true });
                obj.onRejected(def2.promise(), spies[2]);
                obj.onRejected(def2.promise(), spies[3], { async: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                def1.resolve(42);
                def2.reject("error");
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 1, 0);
                expect(spies[2]).toHaveBeenCalledWith("error");
                expect(spies[2]).toHaveBeenCalledOn(obj);
                await wait(1);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 1, 1);
                expect(spies[3]).toHaveBeenCalledWith("error");
                expect(spies[3]).toHaveBeenCalledOn(obj);
            });
            it("should handle plain values", async () => {
                const obj = new DObject(), spies = createJestFns(2);
                obj.onRejected(42, spies[0]);
                obj.onRejected(42, spies[1], { async: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                await wait(1);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
            });
            it("should not invoke callback after destruction", async () => {
                const obj = new DObject(), def = $.Deferred(), spies = createJestFns(2);
                obj.onRejected(42, spies[0], { async: true });
                obj.onRejected(def.promise(), spies[1]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                obj.destroy();
                def.reject();
                await wait(10);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
            });
        });

        describe("method onSettled", () => {
            it("should exist", () => {
                expect(DObject).toHaveMethod("onSettled");
            });
            it("should wait for settlement of native promise", async () => {
                const obj = new DObject(), p1 = Promise.resolve(42), p2 = Promise.reject("error"), spies = createJestFns(4);
                obj.onSettled(p1, spies[0]);
                obj.onSettled(p1, spies[1], { async: true });
                obj.onSettled(p2, spies[2]);
                obj.onSettled(p2, spies[3], { async: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                await wait(1);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 1, 1);
                expect(spies[0]).toHaveBeenCalledWith({ fulfilled: true, value: 42 });
                expect(spies[0]).toHaveBeenCalledOn(obj);
                expect(spies[1]).toHaveBeenCalledWith({ fulfilled: true, value: 42 });
                expect(spies[1]).toHaveBeenCalledOn(obj);
                expect(spies[2]).toHaveBeenCalledWith({ fulfilled: false, value: "error" });
                expect(spies[2]).toHaveBeenCalledOn(obj);
                expect(spies[3]).toHaveBeenCalledWith({ fulfilled: false, value: "error" });
                expect(spies[3]).toHaveBeenCalledOn(obj);
            });
            it("should wait for settlement of JQuery promise", async () => {
                const obj = new DObject(), def1 = $.Deferred(), def2 = $.Deferred(), spies = createJestFns(4);
                obj.onSettled(def1.promise(), spies[0]);
                obj.onSettled(def1.promise(), spies[1], { async: true });
                obj.onSettled(def2.promise(), spies[2]);
                obj.onSettled(def2.promise(), spies[3], { async: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                def1.resolve(42);
                def2.reject("error");
                expect(spies).toHaveAllBeenCalledTimes(1, 0, 1, 0);
                expect(spies[0]).toHaveBeenCalledWith({ fulfilled: true, value: 42 });
                expect(spies[0]).toHaveBeenCalledOn(obj);
                expect(spies[2]).toHaveBeenCalledWith({ fulfilled: false, value: "error" });
                expect(spies[2]).toHaveBeenCalledOn(obj);
                await wait(1);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 1, 1);
                expect(spies[1]).toHaveBeenCalledWith({ fulfilled: true, value: 42 });
                expect(spies[1]).toHaveBeenCalledOn(obj);
                expect(spies[3]).toHaveBeenCalledWith({ fulfilled: false, value: "error" });
                expect(spies[3]).toHaveBeenCalledOn(obj);
            });
            it("should handle plain values", async () => {
                const obj = new DObject(), spies = createJestFns(2);
                obj.onSettled(42, spies[0]);
                obj.onSettled(42, spies[1], { async: true });
                expect(spies).toHaveAllBeenCalledTimes(1, 0);
                expect(spies[0]).toHaveBeenCalledWith({ fulfilled: true, value: 42 });
                expect(spies[0]).toHaveBeenCalledOn(obj);
                await wait(1);
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
                expect(spies[1]).toHaveBeenCalledWith({ fulfilled: true, value: 42 });
                expect(spies[1]).toHaveBeenCalledOn(obj);
            });
            it("should not invoke callback after destruction", async () => {
                const obj = new DObject(), def = $.Deferred(), spies = createJestFns(2);
                obj.onSettled(42, spies[0], { async: true });
                obj.onSettled(def.promise(), spies[1]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                obj.destroy();
                def.resolve();
                await wait(10);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
            });
        });

        describe("method destroy", () => {
            it("should exist", () => {
                expect(DObject).toHaveMethod("destroy");
            });
            it("should call the destructors in reversed order", () => {
                const spy1 = jest.spyOn(BaseClass.prototype, "destructor");
                const spy2 = jest.spyOn(SubClass.prototype, "destructor");
                const obj = new SubClass();
                obj.myProperty = true;
                const { uid } = obj;
                expect(obj.destroyed).toBeFalse();
                expect(DObject.isDestroyed(obj)).toBeFalse();
                expect(obj.destroy()).toBeUndefined();
                expect(DObject.isDestroyed(obj)).toBeTrue();
                expect(obj.destroyed).toBeTrue();
                expect(spy1).toHaveBeenCalledOnce();
                expect(spy2).toHaveBeenCalledOnce();
                expect(spy2).toHaveBeenCalledBefore(spy1);
                // delete public properties
                expect(obj).not.toHaveProperty("myProperty");
                // do not delete the UID
                expect(obj.uid).toBe(uid);
            });
        });

        describe("method member", () => {
            it("should exist", () => {
                expect(DObject).toHaveMethod("member");
            });
            it("should destroy registered members", () => {
                const objects = _.times(4, () => ({ destroy: jest.fn() }));
                const observers = _.times(4, () => ({ disconnect: jest.fn() }));
                const spies = [..._.pluck(objects, "destroy"), ..._.pluck(observers, "disconnect")];
                const obj = new DObject();
                expect(obj.member(objects[0])).toBe(objects[0]);
                expect(obj.member(observers[0])).toBe(observers[0]);
                const arr = [objects[1], observers[1]];
                expect(obj.member(arr)).toBe(arr);
                const map = new Map([["key1", objects[2]], ["key2", observers[2]]]);
                expect(obj.member(map)).toBe(map);
                const set = new Set([objects[3], observers[3]]);
                expect(obj.member(set)).toBe(set);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0, 0, 0, 0, 0);
                obj.destroy();
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 1, 1, 1, 1, 1, 1);
                expect(spies).toHaveAllBeenCalledInOrder(3, 7, 2, 6, 1, 5, 4, 0);
            });
        });
    });
});
