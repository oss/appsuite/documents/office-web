/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { EObject } from "@/io.ox/office/tk/object/eobject";
import { guardMethod, onceMethod, memoizeMethod, debounceMethod } from "@/io.ox/office/tk/object/decorators";

import { createJestFns, useFakeClock } from "~/asynchelper";

// test classes ===============================================================

function hash(text) { return text[0]; }

// test class for basic method decorators
class TestClass1 {

    static onceSpies = createJestFns(2);
    @onceMethod static static1(arg) { TestClass1.onceSpies[0](); return arg; }
    @onceMethod static static2(arg) { TestClass1.onceSpies[1](); throw new Error(arg); }

    guardSpies = createJestFns(2);
    @guardMethod guard1() { this.guardSpies[0](); this.guard2(); }
    @guardMethod guard2() { this.guardSpies[1](); this.guard1(); }

    onceSpies = createJestFns(2);
    @onceMethod once1(arg) { this.onceSpies[0](); return arg; }
    @onceMethod once2(arg) { this.onceSpies[1](); throw new Error(arg); }

    memoizeSpies = createJestFns(2);
    @memoizeMethod(hash) memoize1(arg) { this.memoizeSpies[0](); return arg; }
    @memoizeMethod(hash) memoize2(arg) { this.memoizeSpies[1](); throw new Error(arg); }
}

// test class for EObject method decorators
class TestClass2 extends EObject {

    debounceSpies = createJestFns(3);
    @debounceMethod({ delay: 20 }) debounce1(...args) { this.debounceSpies[0](...args); }
    @debounceMethod({ delay: 10, immediate: true }) debounce2(...args) { this.debounceSpies[1](...args); }
    @debounceMethod({ delay: 20, maxDelay: 20 }) debounce3(...args) { this.debounceSpies[2](...args); }
    debounce() { this.debounce1(); this.debounce2(); this.debounce3(); }

    taskSpies = createJestFns(2);
    @debounceMethod({ delay: "microtask" }) task1() { this.taskSpies[0](); }
    @debounceMethod({ delay: "microtask", immediate: true }) task2() { this.taskSpies[1](); }
    task() { this.task1(); this.task2(); }

    frameSpies = createJestFns(2);
    @debounceMethod({ delay: "animationframe" }) frame1() { this.frameSpies[0](); }
    @debounceMethod({ delay: "animationframe", immediate: true }) frame2() { this.frameSpies[1](); }
    frame() { this.frame1(); this.frame2(); }
}

// tests ======================================================================

describe("module tk/object/decorators", () => {

    const clock = useFakeClock();

    // public functions -------------------------------------------------------

    describe("decorator @guardMethod", () => {
        it("should exist", () => {
            expect(guardMethod).toBeFunction();
        });
        it("should run decorated methods", () => {
            const obj = new TestClass1();
            const spies = obj.guardSpies;
            expect(spies).toHaveAllBeenCalledTimes(0, 0);
            obj.guard1();
            expect(spies).toHaveAllBeenCalledTimes(1, 1);
            obj.guard2();
            expect(spies).toHaveAllBeenCalledTimes(2, 2);
        });
    });

    describe("decorator @onceMethod", () => {
        it("should exist", () => {
            expect(onceMethod).toBeFunction();
        });
        it("should run decorated methods", () => {
            const obj1 = new TestClass1(), obj2 = new TestClass1();
            const spies = [...obj1.onceSpies, ...obj2.onceSpies];
            expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
            expect(obj1.once1(42)).toBe(42);
            expect(obj2.once1(43)).toBe(43);
            expect(spies).toHaveAllBeenCalledTimes(1, 0, 1, 0);
            expect(obj1.once1(44)).toBe(42);
            expect(obj2.once1(45)).toBe(43);
            expect(spies).toHaveAllBeenCalledTimes(1, 0, 1, 0);
            expect(() => obj1.once2("42")).toThrow("42");
            expect(() => obj2.once2("43")).toThrow("43");
            expect(spies).toHaveAllBeenCalledTimes(1, 1, 1, 1);
            expect(() => obj1.once2("44")).toThrow("42");
            expect(() => obj2.once2("45")).toThrow("43");
            expect(spies).toHaveAllBeenCalledTimes(1, 1, 1, 1);
        });
        it("should run decorated static methods", () => {
            const spies = TestClass1.onceSpies;
            expect(spies).toHaveAllBeenCalledTimes(0, 0);
            expect(TestClass1.static1(42)).toBe(42);
            expect(spies).toHaveAllBeenCalledTimes(1, 0);
            expect(TestClass1.static1(43)).toBe(42);
            expect(spies).toHaveAllBeenCalledTimes(1, 0);
            expect(() => TestClass1.static2("42")).toThrow("42");
            expect(spies).toHaveAllBeenCalledTimes(1, 1);
            expect(() => TestClass1.static2("43")).toThrow("42");
            expect(spies).toHaveAllBeenCalledTimes(1, 1);
        });
    });

    describe("decorator @memoizeMethod", () => {
        it("should exist", () => {
            expect(memoizeMethod).toBeFunction();
        });
        it("should run decorated methods", () => {
            const obj1 = new TestClass1(), obj2 = new TestClass1();
            const spies = [...obj1.memoizeSpies, ...obj2.memoizeSpies];
            expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
            expect(obj1.memoize1("abc")).toBe("abc");
            expect(obj2.memoize1("ace")).toBe("ace");
            expect(spies).toHaveAllBeenCalledTimes(1, 0, 1, 0);
            expect(obj1.memoize1("ace")).toBe("abc");
            expect(obj2.memoize1("abc")).toBe("ace");
            expect(spies).toHaveAllBeenCalledTimes(1, 0, 1, 0);
            expect(obj1.memoize1("cba")).toBe("cba");
            expect(obj2.memoize1("cde")).toBe("cde");
            expect(spies).toHaveAllBeenCalledTimes(2, 0, 2, 0);
            expect(obj1.memoize1("cde")).toBe("cba");
            expect(obj2.memoize1("cba")).toBe("cde");
            expect(spies).toHaveAllBeenCalledTimes(2, 0, 2, 0);
            expect(() => obj1.memoize2("abc")).toThrow("abc");
            expect(() => obj2.memoize2("ace")).toThrow("ace");
            expect(spies).toHaveAllBeenCalledTimes(2, 1, 2, 1);
            expect(() => obj1.memoize2("ace")).toThrow("abc");
            expect(() => obj2.memoize2("abc")).toThrow("ace");
            expect(spies).toHaveAllBeenCalledTimes(2, 1, 2, 1);
            expect(() => obj1.memoize2("cba")).toThrow("cba");
            expect(() => obj2.memoize2("cde")).toThrow("cde");
            expect(spies).toHaveAllBeenCalledTimes(2, 2, 2, 2);
            expect(() => obj1.memoize2("cde")).toThrow("cba");
            expect(() => obj2.memoize2("cba")).toThrow("cde");
            expect(spies).toHaveAllBeenCalledTimes(2, 2, 2, 2);
        });
    });

    describe("decorator @debounceMethod", () => {
        it("should exist", () => {
            expect(debounceMethod).toBeFunction();
        });
        it("should decorate methods with timeout", () => {
            const obj = new TestClass2();
            const spies = obj.debounceSpies;
            expect(spies).toHaveAllBeenCalledTimes(0, 0, 0);
            obj.debounce();
            expect(spies).toHaveAllBeenCalledTimes(0, 1, 0);
            obj.debounce();
            expect(spies).toHaveAllBeenCalledTimes(0, 2, 0);
            clock.tick(15);
            expect(spies).toHaveAllBeenCalledTimes(0, 3, 0);
            obj.debounce();
            expect(spies).toHaveAllBeenCalledTimes(0, 4, 0);
            clock.tick(15);
            expect(spies).toHaveAllBeenCalledTimes(0, 5, 1);
            clock.tick(15);
            expect(spies).toHaveAllBeenCalledTimes(1, 5, 1);
            obj.debounce();
            expect(spies).toHaveAllBeenCalledTimes(1, 6, 1);
            obj.destroy();
            clock.tick(1000);
            expect(spies).toHaveAllBeenCalledTimes(1, 6, 1);
        });
        it("should decorate methods with microtask", async () => {
            const obj = new TestClass2();
            const spies = obj.taskSpies;
            expect(spies).toHaveAllBeenCalledTimes(0, 0);
            obj.task();
            expect(spies).toHaveAllBeenCalledTimes(0, 1);
            obj.task();
            expect(spies).toHaveAllBeenCalledTimes(0, 2);
            obj.task();
            expect(spies).toHaveAllBeenCalledTimes(0, 3);
            await clock.wait(1);
            expect(spies).toHaveAllBeenCalledTimes(1, 4);
            obj.task();
            expect(spies).toHaveAllBeenCalledTimes(1, 5);
            await clock.wait(1);
            expect(spies).toHaveAllBeenCalledTimes(2, 6);
            obj.task();
            expect(spies).toHaveAllBeenCalledTimes(2, 7);
            obj.destroy();
            await clock.wait(1000);
            expect(spies).toHaveAllBeenCalledTimes(2, 7);
        });
        it("should decorate methods with animation frame", () => {
            const obj = new TestClass2();
            const spies = obj.frameSpies;
            expect(spies).toHaveAllBeenCalledTimes(0, 0);
            obj.frame();
            expect(spies).toHaveAllBeenCalledTimes(0, 1);
            obj.frame();
            expect(spies).toHaveAllBeenCalledTimes(0, 2);
            obj.frame();
            expect(spies).toHaveAllBeenCalledTimes(0, 3);
            clock.tick(50); // one animation frame lasts 16ms usually
            expect(spies).toHaveAllBeenCalledTimes(1, 4);
            obj.frame();
            expect(spies).toHaveAllBeenCalledTimes(1, 5);
            clock.tick(50); // one animation frame lasts 16ms usually
            expect(spies).toHaveAllBeenCalledTimes(2, 6);
            obj.frame();
            expect(spies).toHaveAllBeenCalledTimes(2, 7);
            obj.destroy();
            clock.tick(1000);
            expect(spies).toHaveAllBeenCalledTimes(2, 7);
        });
    });
});
