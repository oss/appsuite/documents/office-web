/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { KeySet } from "@/io.ox/office/tk/container/keyset";

// tests ======================================================================

describe("module tk/container/keyset", () => {

    const E1 = { key: "E1" };
    const E2 = { key: "E2" };
    const E3 = { key: "E3" };
    const N1 = { key: "E1" };
    const N2 = { key: "E2" };
    const N3 = { key: "E3" };

    // class KeySet -----------------------------------------------------------

    describe("class KeySet", () => {

        it("should subclass Set", () => {
            expect(KeySet).toBeSubClassOf(Set);
        });

        describe("getter size", () => {
            it("should return size of a set", () => {
                const set1 = new KeySet();
                expect(set1.size).toBe(0);
                const set2 = new KeySet([E1, E2, E3]);
                expect(set2.size).toBe(3);
                const set3 = new KeySet(new Set([E1, E2, E3]));
                expect(set3.size).toBe(3);
            });
        });

        describe("getter Symbol.toStringTag", () => {
            it("should return class name", () => {
                const set = new KeySet();
                expect(set[Symbol.toStringTag]).toBe("Set");
                expect(String(set)).toBe("[object Set]");
            });
        });

        describe("method has", () => {
            it("should exist", () => {
                expect(KeySet).toHaveMethod("has");
            });
            it("should return whether the element exists", () => {
                const set = new KeySet([E1, E3]);
                expect(set.has(E1)).toBeTrue();
                expect(set.has(E2)).toBeFalse();
                expect(set.has(E3)).toBeTrue();
                expect(set.has(N1)).toBeTrue();
                expect(set.has(N2)).toBeFalse();
                expect(set.has(N3)).toBeTrue();
            });
        });

        describe("method get", () => {
            it("should exist", () => {
                expect(KeySet).toHaveMethod("get");
            });
            it("should return existing elements", () => {
                const set = new KeySet([E1, E3]);
                expect(set.get("E1")).toBe(E1);
                expect(set.get("E2")).toBeUndefined();
                expect(set.get("E3")).toBe(E3);
            });
        });

        describe("method at", () => {
            it("should exist", () => {
                expect(KeySet).toHaveMethod("at");
            });
            it("should return existing elements", () => {
                const set = new KeySet([E1, E3]);
                expect(set.at(E1)).toBe(E1);
                expect(set.at(E2)).toBeUndefined();
                expect(set.at(E3)).toBe(E3);
                expect(set.at(N1)).toBe(E1);
                expect(set.at(N2)).toBeUndefined();
                expect(set.at(N3)).toBe(E3);
            });
        });

        describe("method add", () => {
            it("should exist", () => {
                expect(KeySet).toHaveMethod("add");
            });
            it("should insert set elements", () => {
                const set = new KeySet([E1, E3]);
                expect(set.get("E1")).toBe(E1);
                expect(set.add(N1)).toBe(set);
                expect(set.size).toBe(2);
                expect(set.get("E1")).toBe(N1);
            });
        });

        describe("method delete", () => {
            it("should exist", () => {
                expect(KeySet).toHaveMethod("delete");
            });
            it("should remove set elements", () => {
                const set = new KeySet([E1, E2, E3]);
                expect(set.delete(E1)).toBeTrue();
                expect(set.size).toBe(2);
                expect(set.at(E1)).toBeUndefined();
                expect(set.delete(N2)).toBeTrue();
                expect(set.size).toBe(1);
                expect(set.at(E2)).toBeUndefined();
                expect(set.delete(E1)).toBeFalse();
                expect(set.size).toBe(1);
            });
        });

        describe("method clear", () => {
            it("should exist", () => {
                expect(KeySet).toHaveMethod("clear");
            });
            it("should clear the set", () => {
                const set = new KeySet([E1, E2, E3]);
                expect(set.size).toBe(3);
                set.clear();
                expect(set.size).toBe(0);
            });
        });

        describe("method forEach", () => {
            it("should exist", () => {
                expect(KeySet).toHaveMethod("forEach");
            });
            it("should visit all elements", () => {
                const set = new KeySet([E1, E3]);
                const spy = jest.fn(), context = {};
                set.forEach(spy, context);
                expect(spy).toHaveBeenCalledTimes(2);
                expect(spy).toHaveBeenAlwaysCalledOn(context);
                expect(spy).toHaveBeenNthCalledWith(1, E1, E1, set);
                expect(spy).toHaveBeenNthCalledWith(2, E3, E3, set);
            });
        });

        describe("method values", () => {
            it("should exist", () => {
                expect(KeySet).toHaveMethod("values");
            });
            it("should return an iterator", () => {
                const set = new KeySet([E1, E3]);
                const iter = set.values();
                expect(iter).toBeIterator();
                expect(Array.from(iter)).toEqual([E1, E3]);
            });
        });

        describe("method keys", () => {
            it("should exist", () => {
                expect(KeySet).toHaveMethod("keys");
            });
            it("should return an iterator", () => {
                const set = new KeySet([E1, E3]);
                const iter = set.keys();
                expect(iter).toBeIterator();
                expect(Array.from(iter)).toEqual([E1, E3]);
            });
        });

        describe("method entries", () => {
            it("should exist", () => {
                expect(KeySet).toHaveMethod("entries");
            });
            it("should return an iterator", () => {
                const set = new KeySet([E1, E3]);
                const iter = set.entries();
                expect(iter).toBeIterator();
                expect(Array.from(iter)).toEqual([[E1, E1], [E3, E3]]);
            });
        });

        describe("method @@iterator", () => {
            it("should exist", () => {
                expect(KeySet).toHaveMethod(Symbol.iterator);
            });
            it("should return an iterator", () => {
                const set = new KeySet([E1, E3]);
                const iter = set[Symbol.iterator]();
                expect(iter).toBeIterator();
                expect(Array.from(iter)).toEqual([E1, E3]);
            });
            it("should run in a for-of loop", () => {
                const set = new KeySet([E1, E3]);
                const values = [];
                for (const value of set) { values.push(value); }
                expect(values).toEqual([E1, E3]);
            });
        });
    });
});
