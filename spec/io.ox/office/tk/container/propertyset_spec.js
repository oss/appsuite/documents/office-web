/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, str } from "@/io.ox/office/tk/algorithms";
import { EventHub } from "@/io.ox/office/tk/events";
import { PropertySet } from "@/io.ox/office/tk/container/propertyset";

// tests ======================================================================

describe("module tk/container/propertyset", () => {

    const PROP_DEFS = {
        prop1: { def: 42, validate: Math.abs },
        prop2: { def: "", validate: v => is.string(v) ? v : undefined, equals: str.equalsICC },
        prop3: { def: false }
    };

    // class PropertySet ------------------------------------------------------

    describe("class PropertySet", () => {

        it("should subclass EventHub", () => {
            expect(PropertySet).toBeSubClassOf(EventHub);
        });

        describe("method get", () => {
            it("should exist", () => {
                expect(PropertySet).toHaveMethod("get");
            });
            it("should return property value", () => {
                const propSet = new PropertySet(PROP_DEFS);
                expect(propSet.get("prop1")).toBe(42);
                expect(propSet.get("unknown")).toBeUndefined();
            });
        });

        describe("method all", () => {
            it("should exist", () => {
                expect(PropertySet).toHaveMethod("all");
            });
            it("should return all property values", () => {
                const propSet = new PropertySet(PROP_DEFS);
                expect(propSet.all()).toEqual({ prop1: 42, prop2: "", prop3: false });
            });
        });

        describe("method set", () => {
            it("should exist", () => {
                expect(PropertySet).toHaveMethod("set");
            });
            it("should set a value", () => {
                const propSet = new PropertySet(PROP_DEFS);
                propSet.set("prop1", 3);
                expect(propSet.get("prop1")).toBe(3);
                propSet.set("prop2", "abc");
                expect(propSet.get("prop2")).toBe("abc");
                expect(propSet.all()).toEqual({ prop1: 3, prop2: "abc", prop3: false });
                // validator that changes value
                propSet.set("prop1", -5);
                expect(propSet.get("prop1")).toBe(5);
                // validator that returns "undefined"
                propSet.set("prop2", 42);
                expect(propSet.get("prop2")).toBe("abc");
                // property's "equals" callback
                propSet.set("prop2", "ABC");
                expect(propSet.get("prop2")).toBe("abc");
                // bypass property's "equals" callback
                propSet.set("prop2", "ABC", { force: true });
                expect(propSet.get("prop2")).toBe("ABC");
                // do not bypass property's "validate" callback
                propSet.set("prop2", 42, { force: true });
                expect(propSet.get("prop2")).toBe("ABC");
            });
            it("should trigger change events", () => {
                const propSet = new PropertySet(PROP_DEFS);
                const spy = jest.fn();
                propSet.on("change:props", spy);
                propSet.set("prop1", 3);
                expect(spy).toHaveBeenNthCalledWith(1, { prop1: 3 }, { prop1: 42 });
                propSet.set("prop2", "abc");
                expect(spy).toHaveBeenNthCalledWith(2, { prop2: "abc" }, { prop2: "" });
                propSet.set("prop2", 42);
                expect(spy).toHaveBeenCalledTimes(2);
                propSet.set("prop2", "ABC");
                expect(spy).toHaveBeenCalledTimes(2);
                propSet.set("prop2", "ABC", { force: true });
                expect(spy).toHaveBeenNthCalledWith(3, { prop2: "ABC" }, { prop2: "abc" });
            });
            it("should throw for invalid property name", () => {
                const propSet = new PropertySet(PROP_DEFS);
                expect(() => propSet.set("unknown", true)).toThrow();
            });
        });

        describe("method update", () => {
            it("should exist", () => {
                expect(PropertySet).toHaveMethod("update");
            });
            it("should update multiple values", () => {
                const propSet = new PropertySet(PROP_DEFS);
                propSet.update({ prop1: 3, prop2: "abc" });
                expect(propSet.all()).toEqual({ prop1: 3, prop2: "abc", prop3: false });
                // validators
                propSet.update({ prop1: -5, prop2: 42 });
                expect(propSet.all()).toEqual({ prop1: 5, prop2: "abc", prop3: false });
                // property's "equals" callback
                propSet.update({ prop2: "ABC" });
                expect(propSet.get("prop2")).toBe("abc");
                // bypass property's "equals" callback
                propSet.update({ prop2: "ABC" }, { force: true });
                expect(propSet.get("prop2")).toBe("ABC");
            });
            it("should trigger change events", () => {
                const propSet = new PropertySet(PROP_DEFS);
                const spy = jest.fn();
                propSet.on("change:props", spy);
                propSet.update({ prop1: 3, prop2: "abc" });
                expect(spy).toHaveBeenNthCalledWith(1, { prop1: 3, prop2: "abc" }, { prop1: 42, prop2: "" });
                propSet.update({ prop1: 3, prop2: 42 });
                expect(spy).toHaveBeenCalledTimes(1);
                propSet.update({ prop2: "ABC" });
                expect(spy).toHaveBeenCalledTimes(1);
                propSet.update({ prop1: 3, prop2: "ABC" }, { force: true });
                expect(spy).toHaveBeenNthCalledWith(2, { prop1: 3, prop2: "ABC" }, { prop1: 3, prop2: "abc" });
            });
            it("should throw for invalid property name", () => {
                const propSet = new PropertySet(PROP_DEFS);
                expect(() => propSet.update({ prop1: 3, unknown: true })).toThrow();
            });
        });
    });
});
