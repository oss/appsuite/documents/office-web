/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";
import Backbone from "$/backbone";
import Events from "$/io.ox/core/event";

import { EventHub } from "@/io.ox/office/tk/event/eventhub";
import { EObject } from "@/io.ox/office/tk/object/eobject";
import * as eventutils from "@/io.ox/office/tk/event/eventutils";

// tests ======================================================================

describe("module tk/event/eventutils", () => {

    const divEl = document.createElement("div");
    const textNode = document.createTextNode("");
    const jqWindow = $(window);
    const jqDoc = $(document);
    const jqDiv = $(divEl);
    const bbEvents = { ...Backbone.Events };
    const bbModel = new Backbone.Model();
    const coreObj1 = new Events();
    const coreObj2 = Events.extend({});
    const eventHub = new EventHub();
    const docsObj1 = new EObject();
    const docsObj2 = new (class extends EObject { })();
    const dict = { on: 42, addEventListener: 42 };

    // public functions -------------------------------------------------------

    describe("function isDOMEmitter", () => {
        const { isDOMEmitter } = eventutils;
        it("should exist", () => {
            expect(isDOMEmitter).toBeFunction();
        });
        it("should return true for DOM emitters", () => {
            expect(isDOMEmitter(window)).toBeTrue();
            expect(isDOMEmitter(document)).toBeTrue();
            expect(isDOMEmitter(document.body)).toBeTrue();
            expect(isDOMEmitter(divEl)).toBeTrue();
            expect(isDOMEmitter(textNode)).toBeTrue();
        });
        it("should return false for JQuery emitters", () => {
            expect(isDOMEmitter($())).toBeFalse();
            expect(isDOMEmitter(jqWindow)).toBeFalse();
            expect(isDOMEmitter(jqDoc)).toBeFalse();
            expect(isDOMEmitter(jqDiv)).toBeFalse();
        });
        it("should return false for Backbone emitters", () => {
            expect(isDOMEmitter(Backbone)).toBeFalse();
            expect(isDOMEmitter(bbEvents)).toBeFalse();
            expect(isDOMEmitter(bbModel)).toBeFalse();
        });
        it("should return false for CoreEmitter", () => {
            expect(isDOMEmitter(coreObj1)).toBeFalse();
            expect(isDOMEmitter(coreObj2)).toBeFalse();
        });
        it("should return false for EventHub", () => {
            expect(isDOMEmitter(eventHub)).toBeFalse();
        });
        it("should return false for DocsEmitter", () => {
            expect(isDOMEmitter(docsObj1)).toBeFalse();
            expect(isDOMEmitter(docsObj2)).toBeFalse();
        });
        it("should return false for other values", () => {
            expect(isDOMEmitter(null)).toBeFalse();
            expect(isDOMEmitter(42)).toBeFalse();
            expect(isDOMEmitter({})).toBeFalse();
            expect(isDOMEmitter([])).toBeFalse();
            expect(isDOMEmitter(dict)).toBeFalse();
            expect(isDOMEmitter(Backbone.Events)).toBeFalse();
        });
    });

    describe("function isJQueryEmitter", () => {
        const { isJQueryEmitter } = eventutils;
        it("should exist", () => {
            expect(isJQueryEmitter).toBeFunction();
        });
        it("should return false for DOM emitters", () => {
            expect(isJQueryEmitter(window)).toBeFalse();
            expect(isJQueryEmitter(document)).toBeFalse();
            expect(isJQueryEmitter(document.body)).toBeFalse();
            expect(isJQueryEmitter(divEl)).toBeFalse();
            expect(isJQueryEmitter(textNode)).toBeFalse();
        });
        it("should return true for JQuery emitters", () => {
            expect(isJQueryEmitter($())).toBeTrue();
            expect(isJQueryEmitter(jqWindow)).toBeTrue();
            expect(isJQueryEmitter(jqDoc)).toBeTrue();
            expect(isJQueryEmitter(jqDiv)).toBeTrue();
        });
        it("should return false for Backbone emitters", () => {
            expect(isJQueryEmitter(Backbone)).toBeFalse();
            expect(isJQueryEmitter(bbEvents)).toBeFalse();
            expect(isJQueryEmitter(bbModel)).toBeFalse();
        });
        it("should return false for CoreEmitter", () => {
            expect(isJQueryEmitter(coreObj1)).toBeFalse();
            expect(isJQueryEmitter(coreObj2)).toBeFalse();
        });
        it("should return false for EventHub", () => {
            expect(isJQueryEmitter(eventHub)).toBeFalse();
        });
        it("should return false for DocsEmitter", () => {
            expect(isJQueryEmitter(docsObj1)).toBeFalse();
            expect(isJQueryEmitter(docsObj2)).toBeFalse();
        });
        it("should return false for other values", () => {
            expect(isJQueryEmitter(null)).toBeFalse();
            expect(isJQueryEmitter(42)).toBeFalse();
            expect(isJQueryEmitter({})).toBeFalse();
            expect(isJQueryEmitter([])).toBeFalse();
            expect(isJQueryEmitter(dict)).toBeFalse();
            expect(isJQueryEmitter(Backbone.Events)).toBeFalse();
        });
    });

    describe("function isBackboneEmitter", () => {
        const { isBackboneEmitter } = eventutils;
        it("should exist", () => {
            expect(isBackboneEmitter).toBeFunction();
        });
        it("should return false for DOM emitters", () => {
            expect(isBackboneEmitter(window)).toBeFalse();
            expect(isBackboneEmitter(document)).toBeFalse();
            expect(isBackboneEmitter(document.body)).toBeFalse();
            expect(isBackboneEmitter(divEl)).toBeFalse();
            expect(isBackboneEmitter(textNode)).toBeFalse();
        });
        it("should return false for JQuery emitters", () => {
            expect(isBackboneEmitter($())).toBeFalse();
            expect(isBackboneEmitter(jqWindow)).toBeFalse();
            expect(isBackboneEmitter(jqDoc)).toBeFalse();
            expect(isBackboneEmitter(jqDiv)).toBeFalse();
        });
        it("should return true for Backbone emitters", () => {
            expect(isBackboneEmitter(Backbone)).toBeTrue();
            expect(isBackboneEmitter(bbEvents)).toBeTrue();
            expect(isBackboneEmitter(bbModel)).toBeTrue();
        });
        it("should return false for CoreEmitter", () => {
            expect(isBackboneEmitter(coreObj1)).toBeFalse();
            expect(isBackboneEmitter(coreObj2)).toBeFalse();
        });
        it("should return false for EventHub", () => {
            expect(isBackboneEmitter(eventHub)).toBeFalse();
        });
        it("should return false for DocsEmitter", () => {
            expect(isBackboneEmitter(docsObj1)).toBeFalse();
            expect(isBackboneEmitter(docsObj2)).toBeFalse();
        });
        it("should return false for other values", () => {
            expect(isBackboneEmitter(null)).toBeFalse();
            expect(isBackboneEmitter(42)).toBeFalse();
            expect(isBackboneEmitter({})).toBeFalse();
            expect(isBackboneEmitter([])).toBeFalse();
            expect(isBackboneEmitter(dict)).toBeFalse();
            expect(isBackboneEmitter(Backbone.Events)).toBeFalse();
        });
    });

    describe("function isCoreEmitter", () => {
        const { isCoreEmitter } = eventutils;
        it("should exist", () => {
            expect(isCoreEmitter).toBeFunction();
        });
        it("should return false for DOM emitters", () => {
            expect(isCoreEmitter(window)).toBeFalse();
            expect(isCoreEmitter(document)).toBeFalse();
            expect(isCoreEmitter(document.body)).toBeFalse();
            expect(isCoreEmitter(divEl)).toBeFalse();
            expect(isCoreEmitter(textNode)).toBeFalse();
        });
        it("should return false for JQuery emitters", () => {
            expect(isCoreEmitter($())).toBeFalse();
            expect(isCoreEmitter(jqWindow)).toBeFalse();
            expect(isCoreEmitter(jqDoc)).toBeFalse();
            expect(isCoreEmitter(jqDiv)).toBeFalse();
        });
        it("should return false for Backbone emitters", () => {
            expect(isCoreEmitter(Backbone)).toBeFalse();
            expect(isCoreEmitter(bbEvents)).toBeFalse();
            expect(isCoreEmitter(bbModel)).toBeFalse();
        });
        it("should return true for CoreEmitter", () => {
            expect(isCoreEmitter(coreObj1)).toBeTrue();
            expect(isCoreEmitter(coreObj2)).toBeTrue();
        });
        it("should return false for EventHub", () => {
            expect(isCoreEmitter(eventHub)).toBeFalse();
        });
        it("should return false for DocsEmitter", () => {
            expect(isCoreEmitter(docsObj1)).toBeFalse();
            expect(isCoreEmitter(docsObj2)).toBeFalse();
        });
        it("should return false for other values", () => {
            expect(isCoreEmitter(null)).toBeFalse();
            expect(isCoreEmitter(42)).toBeFalse();
            expect(isCoreEmitter({})).toBeFalse();
            expect(isCoreEmitter([])).toBeFalse();
            expect(isCoreEmitter(dict)).toBeFalse();
            expect(isCoreEmitter(Backbone.Events)).toBeFalse();
        });
    });

    describe("function isDocsEmitter", () => {
        const { isDocsEmitter } = eventutils;
        it("should exist", () => {
            expect(isDocsEmitter).toBeFunction();
        });
        it("should return false for DOM emitters", () => {
            expect(isDocsEmitter(window)).toBeFalse();
            expect(isDocsEmitter(document)).toBeFalse();
            expect(isDocsEmitter(document.body)).toBeFalse();
            expect(isDocsEmitter(divEl)).toBeFalse();
            expect(isDocsEmitter(textNode)).toBeFalse();
        });
        it("should return false for JQuery emitters", () => {
            expect(isDocsEmitter($())).toBeFalse();
            expect(isDocsEmitter(jqWindow)).toBeFalse();
            expect(isDocsEmitter(jqDoc)).toBeFalse();
            expect(isDocsEmitter(jqDiv)).toBeFalse();
        });
        it("should return false for Backbone emitters", () => {
            expect(isDocsEmitter(Backbone)).toBeFalse();
            expect(isDocsEmitter(bbEvents)).toBeFalse();
            expect(isDocsEmitter(bbModel)).toBeFalse();
        });
        it("should return false for CoreEmitter", () => {
            expect(isDocsEmitter(coreObj1)).toBeFalse();
            expect(isDocsEmitter(coreObj2)).toBeFalse();
        });
        it("should return false for EventHub", () => {
            expect(isDocsEmitter(eventHub)).toBeFalse();
        });
        it("should return true for DocsEmitter", () => {
            expect(isDocsEmitter(docsObj1)).toBeTrue();
            expect(isDocsEmitter(docsObj2)).toBeTrue();
        });
        it("should return false for other values", () => {
            expect(isDocsEmitter(null)).toBeFalse();
            expect(isDocsEmitter(42)).toBeFalse();
            expect(isDocsEmitter({})).toBeFalse();
            expect(isDocsEmitter([])).toBeFalse();
            expect(isDocsEmitter(dict)).toBeFalse();
            expect(isDocsEmitter(Backbone.Events)).toBeFalse();
        });
    });

    describe("function invokeHandler", () => {
        const { invokeHandler } = eventutils;
        it("should exist", () => {
            expect(invokeHandler).toBeFunction();
        });
        it("should run the handler", () => {
            const handlerSpy = jest.fn().mockReturnValue(true);
            expect(invokeHandler(handlerSpy, "abc", 42)).toBeTrue();
            expect(handlerSpy).toHaveBeenCalledWith("abc", 42);
        });
        it("should catch exception thrown from the handler", () => {
            const handlerSpy = jest.fn().mockImplementation(() => { throw new Error(); });
            const consoleSpy = jest.spyOn(console, "error").mockImplementation(() => undefined);
            expect(invokeHandler(handlerSpy, 42)).toBeUndefined();
            expect(handlerSpy).toHaveBeenCalledWith(42);
            expect(consoleSpy).toHaveBeenCalled();
            consoleSpy.mockRestore();
        });
    });
});
