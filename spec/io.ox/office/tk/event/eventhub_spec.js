/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as eventhub from "@/io.ox/office/tk/event/eventhub";

import { createJestFns } from "~/asynchelper";

// tests ======================================================================

describe("module tk/event/eventhub", () => {

    // class EventHub --------------------------------------------------------

    describe("class EventHub", () => {

        const { EventHub } = eventhub;
        it("should exist", () => {
            expect(EventHub).toBeClass();
        });

        describe("method on", () => {
            it("should exist", () => {
                expect(EventHub).toHaveMethod("on");
            });
        });

        describe("method emit", () => {
            it("should exist", () => {
                expect(EventHub).toHaveMethod("emit");
            });
            it("should invoke registered handlers", () => {
                const hub = new EventHub(), spies = createJestFns(3);
                hub.on("test1", spies[0]);
                hub.on(["test1", "test2"], spies[1]);
                hub.on("test2", spies[2]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0);
                hub.emit("test1", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 0);
                expect(spies[0]).toHaveBeenCalledWith(42);
                expect(spies[0]).toHaveBeenCalledOn(hub);
                expect(spies[1]).toHaveBeenCalledWith(42);
                expect(spies[1]).toHaveBeenCalledOn(hub);
                hub.emit("test2", "abc", "def");
                expect(spies).toHaveAllBeenCalledTimes(1, 2, 1);
                expect(spies[1]).toHaveBeenCalledWith("abc", "def");
                expect(spies[2]).toHaveBeenCalledWith("abc", "def");
            });
            it("should not invoke handlers twice", () => {
                const hub = new EventHub(), spies = createJestFns(3);
                hub.on("test", spies[0]);
                hub.on("test", spies[1]);
                hub.on("test", spies[0]);
                hub.on("test", spies[2]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0);
                hub.emit("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 1);
                expect(spies).toHaveAllBeenCalledInOrder(1, 0, 2);
                expect(spies[0]).toHaveBeenCalledWith(42);
                expect(spies[1]).toHaveBeenCalledWith(42);
                expect(spies[2]).toHaveBeenCalledWith(42);
            });
        });

        describe("method on with flag 'once'", () => {
            it("should invoke a handler once", () => {
                const hub = new EventHub(), spies = createJestFns(3);
                hub.on("test", spies[0]);
                hub.on("test", spies[1]);
                hub.on("test", spies[2], { once: true });
                hub.on("test", spies[0], { once: true });
                hub.on("test", spies[2]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0);
                hub.emit("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 1);
                expect(spies).toHaveAllBeenCalledInOrder(1, 0, 2);
                hub.emit("test", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 2, 2);
            });
        });

        describe("method on with type '*'", () => {
            it("should invoke all-handler", () => {
                const hub = new EventHub(), spies = createJestFns(4);
                hub.on("test1", spies[0]);
                hub.on("test2", spies[1]);
                hub.on("*", spies[2]);
                hub.on("*", spies[3], { once: true });
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                hub.emit("test1", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 0, 1, 1);
                expect(spies[0]).toHaveBeenCalledWith(42);
                expect(spies[2]).toHaveBeenCalledWith("test1", 42);
                expect(spies[2]).toHaveBeenCalledOn(hub);
                expect(spies[3]).toHaveBeenCalledWith("test1", 42);
                expect(spies[3]).toHaveBeenCalledOn(hub);
                hub.emit("test2", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 2, 1);
                expect(spies[1]).toHaveBeenCalledWith(43);
                expect(spies[2]).toHaveBeenCalledWith("test2", 43);
            });
        });

        describe("method off", () => {
            it("should exist", () => {
                expect(EventHub).toHaveMethod("off");
            });
            it("should not invoke a removed handler", () => {
                const hub = new EventHub(), spies = createJestFns(6);
                hub.on("test1", spies[0]);
                hub.on("test1", spies[1]);
                hub.on("test2", spies[2]);
                hub.on("test2", spies[3]);
                hub.on("*", spies[4]);
                hub.on("*", spies[5]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0, 0, 0);
                hub.emit("test1", 42);
                hub.emit("test2", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 1, 1, 2, 2);
                hub.off("test1", spies[0]);
                hub.off("test2", spies[2]);
                hub.off("*", spies[4]);
                hub.emit("test1", 42);
                hub.emit("test2", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 2, 1, 2, 2, 4);
                // remove non-existing handlers does not harm
                hub.off("test1", spies[0]);
                hub.emit("test1", 44);
                expect(spies).toHaveAllBeenCalledTimes(1, 3, 1, 2, 2, 5);
            });
            it("should remove all handlers of a type", () => {
                const hub = new EventHub(), spies = createJestFns(4);
                hub.on("test", spies[0]);
                hub.on("test", spies[1]);
                hub.on("*", spies[2]);
                hub.on("*", spies[3]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                hub.emit("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 1, 1);
                hub.off("test");
                hub.emit("test", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 2, 2);
                hub.off("*");
                hub.emit("test", 44);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 2, 2);
            });
            it("should remove all handlers", () => {
                const hub = new EventHub(), spies = createJestFns(4);
                hub.on("test", spies[0]);
                hub.on("test", spies[1]);
                hub.on("*", spies[2]);
                hub.on("*", spies[3]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0, 0, 0);
                hub.emit("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 1, 1);
                hub.off();
                hub.emit("test", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 1, 1, 1);
            });
        });

        describe("method destroy", () => {
            it("should exist", () => {
                expect(EventHub).toHaveMethod("destroy");
            });
            it("should not invoke handlers after destroy", () => {
                const hub = new EventHub(), spies = createJestFns(2);
                hub.on("test", spies[0]);
                hub.on("*", spies[1]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                hub.emit("test", 42);
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
                hub.destroy();
                hub.emit("test", 43);
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
            });
        });
    });
});
