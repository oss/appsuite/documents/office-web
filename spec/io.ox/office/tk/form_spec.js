/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as validators from "@/io.ox/office/tk/form/validators";
import * as control from "@/io.ox/office/tk/form/control";
import * as checkbox from "@/io.ox/office/tk/form/checkbox";
import * as checklist from "@/io.ox/office/tk/form/checklist";
import * as textfield from "@/io.ox/office/tk/form/textfield";
import * as spinfield from "@/io.ox/office/tk/form/spinfield";
import * as listcontrol from "@/io.ox/office/tk/form/listcontrol";
import * as buttonlist from "@/io.ox/office/tk/form/buttonlist";
import * as selectlist from "@/io.ox/office/tk/form/selectlist";
import * as dropdownlist from "@/io.ox/office/tk/form/dropdownlist";

import * as form from "@/io.ox/office/tk/form";

// tests ======================================================================

describe("module tk/form", () => {

    // re-exports -------------------------------------------------------------

    it("re-exports should exist", () => {
        expect(form).toContainProps(validators);
        expect(form).toContainProps(control);
        expect(form).toContainProps(checkbox);
        expect(form).toContainProps(checklist);
        expect(form).toContainProps(textfield);
        expect(form).toContainProps(spinfield);
        expect(form).toContainProps(listcontrol);
        expect(form).toContainProps(buttonlist);
        expect(form).toContainProps(selectlist);
        expect(form).toContainProps(dropdownlist);
    });
});
