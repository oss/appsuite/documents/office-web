/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { DObject } from "@/io.ox/office/tk/objects";
import { TaskPipeline } from "@/io.ox/office/tk/worker/taskpipeline";

import { useFakeClock, observePromise } from "~/asynchelper";

// tests ======================================================================

describe("module tk/worker/taskpipeline", () => {

    const clock = useFakeClock();

    async function expectPending(observed) {
        // let other timeouts settle, without modifying the system time
        // let the promise settle (promises remain async, also with fake timers!)
        await clock.wait(0);
        expect(observed.state).toBe("pending");
    }

    async function expectResolved(observed) {
        // let other timeouts settle, without modifying the system time
        // let the promise settle (promises remain async, also with fake timers!)
        await clock.wait(0);
        expect(observed.state).toBe("resolved");
    }

    // class TaskPipeline -----------------------------------------------------

    describe("class TaskPipeline", () => {

        it("should subclass DObject", () => {
            expect(TaskPipeline).toBeSubClassOf(DObject);
        });

        describe("method pushTask", () => {
            it("should exist", () => {
                expect(TaskPipeline).toHaveMethod("pushTask");
            });
            it("should run registered tasks sequentially", async () => {

                // create the task pipeline
                const pipeline = new TaskPipeline();
                expect(pipeline.running).toBeFalse();

                // register first task
                const def1 = $.Deferred();
                const task1 = jest.fn().mockReturnValue(def1.promise());
                const observed1 = observePromise(pipeline.pushTask(task1));
                expect(pipeline.running).toBeTrue();
                expect(task1).not.toHaveBeenCalled();

                // register second task
                const def2 = $.Deferred();
                const task2 = jest.fn().mockReturnValue(def2.promise());
                const observed2 = observePromise(pipeline.pushTask(task2));
                expect(task2).not.toHaveBeenCalled();

                // let the pipeline start the first task
                await clock.wait(1);
                expect(task1).toHaveBeenCalled();
                expect(task2).not.toHaveBeenCalled();
                await clock.wait(100);
                expect(task2).not.toHaveBeenCalled();

                // resolve the first task
                await expectPending(observed1);
                def1.resolve();
                await expectResolved(observed1);
                expect(pipeline.running).toBeTrue();
                expect(task2).not.toHaveBeenCalled();

                // let the pipeline start the second task
                await clock.wait(10);
                expect(task2).toHaveBeenCalledOnce();
                await clock.wait(100);
                await expectPending(observed2);

                // register a third task while the worker is running
                const def3 = $.Deferred();
                const task3 = jest.fn().mockReturnValue(def3.promise());
                const observed3 = observePromise(pipeline.pushTask(task3));
                expect(task3).not.toHaveBeenCalled();

                // register a fourth priority task while the worker is running
                const def4 = $.Deferred();
                const task4 = jest.fn().mockReturnValue(def4.promise());
                const observed4 = observePromise(pipeline.pushTask(task4, { priority: true }));
                expect(task4).not.toHaveBeenCalled();

                // resolve the second task
                def2.resolve();
                await expectResolved(observed2);
                expect(pipeline.running).toBeTrue();

                // let the pipeline start the fourth (priority) task
                await clock.wait(10);
                expect(task3).not.toHaveBeenCalled();
                expect(task4).toHaveBeenCalledOnce();
                await clock.wait(100);
                await expectPending(observed4);

                // resolve the fourth task
                def4.resolve();
                await expectPending(observed3);
                await expectResolved(observed4);
                expect(pipeline.running).toBeTrue();
                expect(task3).not.toHaveBeenCalled();

                // let the pipeline start the third task
                await clock.wait(10);
                expect(task3).toHaveBeenCalledOnce();
                await clock.wait(100);
                await expectPending(observed3);

                // resolve the third task
                def3.resolve();
                await expectResolved(observed3);
                expect(pipeline.running).toBeTrue();

                // queue must drain
                await clock.wait(1);
                expect(pipeline.running).toBeFalse();
                await clock.wait(10);

                // register fifth task
                const def5 = $.Deferred();
                const task5 = jest.fn().mockReturnValue(def5.promise());
                const observed5 = observePromise(pipeline.pushTask(task5));
                expect(pipeline.running).toBeTrue();
                expect(task5).not.toHaveBeenCalled();

                // run the fifth task
                await clock.wait(1);
                expect(task5).toHaveBeenCalledOnce();
                def5.resolve();
                await expectResolved(observed5);
                expect(pipeline.running).toBeTrue();

                // register sixth task in draining phase
                const def6 = $.Deferred();
                const task6 = jest.fn().mockReturnValue(def6.promise());
                const observed6 = observePromise(pipeline.pushTask(task6));

                // run the sixth task
                await clock.wait(10);
                expect(pipeline.running).toBeTrue();
                await clock.wait(1);
                expect(task6).toHaveBeenCalledOnce();
                def6.resolve();
                await expectResolved(observed6);
                expect(pipeline.running).toBeTrue();

                // queue must drain again
                await clock.wait(11);
                expect(pipeline.running).toBeFalse();
            });
        });
    });
});
