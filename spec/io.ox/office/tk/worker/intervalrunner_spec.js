/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { DObject } from "@/io.ox/office/tk/objects";
import { IntervalRunner } from "@/io.ox/office/tk/worker/intervalrunner";

import { useFakeClock } from "~/asynchelper";

// tests ======================================================================

describe("module tk/worker/intervalrunner", () => {

    const clock = useFakeClock();

    // class IntervalRunner ---------------------------------------------------

    describe("class IntervalRunner", () => {

        it("should subclass DObject", () => {
            expect(IntervalRunner).toBeSubClassOf(DObject);
        });

        describe("method abort", () => {
            it("should exist", () => {
                expect(IntervalRunner).toHaveMethod("abort");
            });
        });

        describe("method start", () => {
            it("should exist", () => {
                expect(IntervalRunner).toHaveMethod("start");
            });

            it("should run synchronous tasks", async () => {

                // create the runner
                const task = jest.fn();
                const runner = new IntervalRunner(task, { delay: 10, interval: 100 });
                expect(runner.running).toBeFalse();

                // start the runner
                await clock.wait(10);
                expect(runner.running).toBeFalse();
                runner.start();
                expect(runner.running).toBeTrue();
                expect(task).not.toHaveBeenCalled();

                // wait the initial delay
                await clock.wait(9);
                expect(task).not.toHaveBeenCalled();
                await clock.wait(1);
                expect(task).toHaveBeenCalledTimes(1);
                expect(task).toHaveBeenNthCalledWith(1, 0);

                // wait the interval time
                await clock.wait(99);
                expect(task).toHaveBeenCalledTimes(1);
                await clock.wait(1);
                expect(task).toHaveBeenCalledTimes(2);
                expect(task).toHaveBeenNthCalledWith(2, 1);

                // wait the interval time again
                await clock.wait(99);
                expect(task).toHaveBeenCalledTimes(2);
                await clock.wait(1);
                expect(task).toHaveBeenCalledTimes(3);
                expect(task).toHaveBeenNthCalledWith(3, 2);

                // abort the runner
                await clock.wait(99);
                expect(task).toHaveBeenCalledTimes(3);
                runner.abort();
                await clock.wait(250);
                expect(task).toHaveBeenCalledTimes(3);

                // start the runner again
                runner.start();
                await clock.wait(9);
                expect(task).toHaveBeenCalledTimes(3);
                await clock.wait(1);
                expect(task).toHaveBeenCalledTimes(4);
                expect(task).toHaveBeenNthCalledWith(4, 0);
                await clock.wait(99);
                expect(task).toHaveBeenCalledTimes(4);

                // start a running instance does nothing
                runner.start();
                expect(task).toHaveBeenCalledTimes(4);
                clock.tick(1);
                expect(task).toHaveBeenCalledTimes(5);
                expect(task).toHaveBeenNthCalledWith(5, 1);

                // destroy the runner
                runner.destroy();
                clock.tick(250);
                expect(task).toHaveBeenCalledTimes(5);
            });

            it("should run asynchronous tasks", async () => {

                // create the runner
                const def = $.Deferred();
                const task = jest.fn().mockReturnValue(def.promise());
                const runner = new IntervalRunner(task, 10);
                expect(runner.running).toBeFalse();

                // start the runner
                runner.start();
                expect(runner.running).toBeTrue();
                expect(task).not.toHaveBeenCalled();
                await clock.wait(1);
                expect(task).toHaveBeenCalledTimes(1);

                // wait for the promise
                await clock.wait(250);
                expect(task).toHaveBeenCalledTimes(1);
                def.resolve();
                await clock.wait(9);
                expect(task).toHaveBeenCalledTimes(1);
                await clock.wait(1);
                expect(task).toHaveBeenCalledTimes(2);

                // destroy the runner
                runner.destroy();
                await clock.wait(250);
                expect(task).toHaveBeenCalledTimes(2);
            });
        });

        describe("method toggle", () => {
            it("should exist", () => {
                expect(IntervalRunner).toHaveMethod("toggle");
            });
        });
    });
});
