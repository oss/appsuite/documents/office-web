/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { DObject } from "@/io.ox/office/tk/objects";
import { DataPipeline } from "@/io.ox/office/tk/worker/datapipeline";

import { useFakeClock } from "~/asynchelper";

// tests ======================================================================

describe("module tk/worker/datapipeline", () => {

    const clock = useFakeClock();

    // class DataPipeline -----------------------------------------------------

    describe("class DataPipeline", () => {

        it("should subclass DObject", () => {
            expect(DataPipeline).toBeSubClassOf(DObject);
        });

        describe("method pushValue", () => {
            it("should exist", () => {
                expect(DataPipeline).toHaveMethod("pushValue");
            });
            it("should process registered values sequentially", async () => {

                // create the data pipeline
                const defs = [];
                const task = jest.fn(() => { const def = new $.Deferred(); defs.push(def); return def.promise(); });
                const pipeline = new DataPipeline(task, { slice: 0 });
                expect(pipeline.running).toBeFalse();

                // register first value
                pipeline.pushValue(1);
                expect(pipeline.running).toBeTrue();
                expect(task).not.toHaveBeenCalled();

                // register second value
                pipeline.pushValue(3);
                expect(task).not.toHaveBeenCalled();

                // let the pipeline start the first step
                await clock.wait(1);
                expect(task).toHaveBeenCalledTimes(1);
                expect(task).toHaveBeenNthCalledWith(1, 1, 0);
                await clock.wait(100);
                expect(task).toHaveBeenCalledTimes(1);

                // resolve the first step
                defs[0].resolve();
                expect(pipeline.running).toBeTrue();
                expect(task).toHaveBeenCalledTimes(1);

                // let the pipeline start the second step
                await clock.wait(10);
                expect(task).toHaveBeenCalledTimes(2);
                expect(task).toHaveBeenNthCalledWith(2, 3, 1);
                await clock.wait(100);

                // register a third value while the worker is running
                pipeline.pushValue(5);
                expect(task).toHaveBeenCalledTimes(2);

                // reject the second step (pipeline must continue)
                defs[1].reject(new Error());
                expect(pipeline.running).toBeTrue();

                // let the pipeline start the third step
                await clock.wait(10);
                expect(task).toHaveBeenCalledTimes(3);
                expect(task).toHaveBeenNthCalledWith(3, 5, 2);
                await clock.wait(100);

                // resolve the third step
                defs[2].resolve();
                expect(pipeline.running).toBeTrue();

                // queue must drain
                await clock.wait(10);
                expect(pipeline.running).toBeFalse();
                await clock.wait(10);

                // register fourth value
                pipeline.pushValue(7);
                expect(pipeline.running).toBeTrue();
                expect(task).toHaveBeenCalledTimes(3);

                // run the fourth step
                await clock.wait(1);
                expect(task).toHaveBeenCalledTimes(4);
                expect(task).toHaveBeenNthCalledWith(4, 7, 3);
                defs[3].resolve();
                expect(pipeline.running).toBeTrue();

                // register fifth step in draining phase
                await clock.wait(1);
                expect(pipeline.running).toBeTrue();
                pipeline.pushValue(9);

                // run the fifth step
                await clock.wait(2);
                expect(pipeline.running).toBeTrue();
                await clock.wait(1);
                expect(task).toHaveBeenCalledTimes(5);
                expect(task).toHaveBeenNthCalledWith(5, 9, 4);
                defs[4].resolve();
                expect(pipeline.running).toBeTrue();

                // queue must drain again
                await clock.wait(10);
                expect(pipeline.running).toBeFalse();
            });
        });
    });
});
