/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { AbortError, EObject } from "@/io.ox/office/tk/objects";
import * as asyncworker from "@/io.ox/office/tk/worker/asyncworker";

import { createJestFns, useFakeClock, observePromise } from "~/asynchelper";

// private functions ==========================================================

const runnerMatch = expect.any(asyncworker.AsyncRunner);

// expects that the passed step has been called once with a runner instance
function expectStepCall(step) {
    expect(step).toHaveBeenCalledTimes(1);
    expect(step).toHaveBeenNthCalledWith(1, runnerMatch);
}

// expects that the passed step has been called repeatedly in an asynchronous loop
function expectLoopCalls(step, count) {
    expect(step).toHaveBeenCalledTimes(count);
    for (let index = 0; index < count; index += 1) {
        expect(step).toHaveBeenNthCalledWith(index + 1, index, runnerMatch);
    }
}

// expects that the passed step has been called repeatedly in an asynchronous iterator loop
function expectIterateCalls(step, values) {
    expect(step).toHaveBeenCalledTimes(values.length);
    for (const [index, value] of values.entries()) {
        expect(step).toHaveBeenNthCalledWith(index + 1, value, runnerMatch);
    }
}

function expectWorkerDone(spy, index) {
    expect(spy).toHaveBeenCalledTimes(index + 1);
    expect(spy.mock.calls[index][0]).toEqual({ state: "done" });
}

function expectWorkerFail(spy, index, error) {
    expect(spy).toHaveBeenCalledTimes(index + 1);
    expect(spy.mock.calls[index][0]).toEqual({ state: "fail", error });
}

function expectWorkerAbort(spy, index, destroyed) {
    expect(spy).toHaveBeenCalledTimes(index + 1);
    expect(spy.mock.calls[index][0]).toEqual({ state: "abort", destroyed });
}

// tests ======================================================================

describe("module tk/worker/asyncworker", () => {

    const clock = useFakeClock();

    async function expectResolved(observed) {
        // let other timeouts settle, without modifying the system time
        // let the promise settle (promises remain async, also with fake timers!)
        await clock.wait(0);
        expect(observed.state).toBe("resolved");
    }

    async function expectRejected(observed, expected) {
        // let other timeouts settle, without modifying the system time
        // let the promise settle (promises remain async, also with fake timers!)
        await clock.wait(0);
        expect(observed.state).toBe("rejected");
        if (expected) {
            expect(observed.value).toBe(expected);
        }
    }

    async function expectAborted(observed, destroyed) {
        await expectRejected(observed);
        expect(observed.value).toBeInstanceOf(AbortError);
        expect(observed.value.aborted).toBeTrue();
        expect(observed.value.destroyed).toBe(!!destroyed);
    }

    // class BreakLoopToken ---------------------------------------------------

    const { BreakLoopToken } = asyncworker;
    describe("class BreakLoopToken", () => {
        it("should subclass Error", () => {
            expect(BreakLoopToken).toBeSubClassOf(Error);
        });
    });

    // public functions -------------------------------------------------------

    const { breakLoop } = asyncworker;
    describe("function breakLoop", () => {
        it("should exist", () => {
            expect(breakLoop).toBeFunction();
        });
        it("should throw", () => {
            expect(breakLoop).toThrow(BreakLoopToken);
        });
    });

    // class PromiseExecutor --------------------------------------------------

    describe("class PromiseExecutor", () => {
        const { PromiseExecutor } = asyncworker;
        it("should exist", () => {
            expect(PromiseExecutor).toBeClass();
        });
        it("should resolve the promise", async () => {
            const executor = new PromiseExecutor();
            expect(executor.promise).toBeInstanceOf(Promise);
            expect(executor.resolve).toBeFunction();
            expect(executor.reject).toBeFunction();
            const spies = createJestFns(2);
            executor.promise.then(...spies).catch(() => null);
            expect(spies).toHaveAllBeenCalledTimes(0, 0);
            executor.resolve(42);
            expect(spies).toHaveAllBeenCalledTimes(0, 0);
            await Promise.resolve();
            expect(spies).toHaveAllBeenCalledTimes(1, 0);
            expect(spies[0]).toHaveBeenNthCalledWith(1, 42);
        });
        it("should reject the promise", async () => {
            const executor = new PromiseExecutor();
            expect(executor.promise).toBeInstanceOf(Promise);
            expect(executor.resolve).toBeFunction();
            expect(executor.reject).toBeFunction();
            const spies = createJestFns(2);
            executor.promise.then(...spies).catch(() => null);
            expect(spies).toHaveAllBeenCalledTimes(0, 0);
            executor.reject(42);
            expect(spies).toHaveAllBeenCalledTimes(0, 0);
            await Promise.resolve();
            expect(spies).toHaveAllBeenCalledTimes(0, 1);
            expect(spies[1]).toHaveBeenCalledWith(42);
        });
    });

    // class WorkerError ------------------------------------------------------

    const { WorkerError } = asyncworker;
    describe("class WorkerError", () => {
        it("should subclass Error", () => {
            expect(WorkerError).toBeSubClassOf(Error);
        });
    });

    // class AsyncRunner ------------------------------------------------------

    describe("class AsyncRunner", () => {
        const { AsyncRunner } = asyncworker;
        it("should subclass EObject", () => {
            expect(AsyncRunner).toBeSubClassOf(EObject);
        });
    });

    // class AsyncWorkerSteps -------------------------------------------------

    const { AsyncWorkerSteps } = asyncworker;
    describe("class AsyncWorkerSteps", () => {

        it("should exist", () => {
            expect(AsyncWorkerSteps).toBeClass();
        });

        describe("method @@iterator", () => {
            it("should exist", () => {
                expect(AsyncWorkerSteps).toHaveMethod(Symbol.iterator);
            });
            it("should return an iterator", () => {
                const steps = new AsyncWorkerSteps();
                expect(steps[Symbol.iterator]()).toBeIterator();
                expect(Array.from(steps)).toEqual([]);
            });
        });

        describe("method addDelay", () => {
            it("should exist", () => {
                expect(AsyncWorkerSteps).toHaveMethod("addDelay");
            });
            it("should append a worker step", () => {
                const steps = new AsyncWorkerSteps();
                steps.addDelay(10);
                expect(Array.from(steps)).toHaveLength(1);
            });
        });

        describe("method addStep", () => {
            it("should exist", () => {
                expect(AsyncWorkerSteps).toHaveMethod("addStep");
            });
            it("should append a worker step", () => {
                const steps = new AsyncWorkerSteps();
                steps.addStep(() => null);
                expect(Array.from(steps)).toHaveLength(1);
            });
        });

        describe("method addLoop", () => {
            it("should exist", () => {
                expect(AsyncWorkerSteps).toHaveMethod("addLoop");
            });
            it("should append a worker step", () => {
                const steps = new AsyncWorkerSteps();
                steps.addLoop(() => null);
                expect(Array.from(steps)).toHaveLength(1);
            });
        });

        describe("method addIterator", () => {
            it("should exist", () => {
                expect(AsyncWorkerSteps).toHaveMethod("addIterator");
            });
            it("should append a worker step", () => {
                const steps = new AsyncWorkerSteps();
                steps.addIterator(() => [], () => null);
                expect(Array.from(steps)).toHaveLength(1);
            });
        });
    });

    // class AsyncWorker ------------------------------------------------------

    describe("class AsyncWorker", () => {

        const { AsyncWorker } = asyncworker;
        it("should subclass EObject", () => {
            expect(AsyncWorker).toBeSubClassOf(EObject);
        });

        describe("property steps", () => {
            it("should exist", () => {
                const worker = new AsyncWorker();
                expect(worker.steps).toBeInstanceOf(AsyncWorkerSteps);
            });
        });

        describe("property running", () => {
            it("should exist", () => {
                const worker = new AsyncWorker();
                expect(worker.running).toBeFalse();
            });
        });

        describe("method start", () => {
            it("should exist", () => {
                expect(AsyncWorker).toHaveMethod("start");
            });
            it("should start an empty worker", async () => {
                // initialize worker
                const worker = new AsyncWorker(), spies = createJestFns(2);
                worker.on("worker:start", spies[0]);
                worker.on("worker:finish", spies[1]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                expect(worker.running).toBeFalse();
                // start the worker
                const observed1 = observePromise(worker.start());
                expect(spies).toHaveAllBeenCalledTimes(1, 0);
                expect(worker.running).toBeTrue();
                // check result
                await clock.wait(1);
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
                expect(worker.running).toBeFalse();
                expectWorkerDone(spies[1], 0);
                await expectResolved(observed1);
                // start the worker again
                const observed2 = observePromise(worker.start());
                expect(spies).toHaveAllBeenCalledTimes(2, 1);
                expect(worker.running).toBeTrue();
                // check result
                await clock.wait(1);
                expect(spies).toHaveAllBeenCalledTimes(2, 2);
                expect(worker.running).toBeFalse();
                expectWorkerDone(spies[1], 1);
                await expectResolved(observed2);
            });
            it("should fail to start a running worker", async () => {
                // initialize worker
                const worker = new AsyncWorker();
                const promise = observePromise(worker.start());
                await expect(worker.start()).rejects.toThrow(WorkerError);
                // check result
                await clock.wait(1);
                await expectResolved(promise);
            });
        });

        describe("method abort", () => {
            it("should exist", () => {
                expect(AsyncWorker).toHaveMethod("abort");
            });
            it("should abort an empty worker", async () => {
                // initialize worker
                const worker = new AsyncWorker(), spy = jest.fn();
                worker.on("worker:finish", spy);
                // start the worker
                const observed = observePromise(worker.start());
                expect(spy).not.toHaveBeenCalled();
                // abort the worker immediately
                worker.abort();
                // check result
                expectWorkerAbort(spy, 0, false);
                await expectAborted(observed);
            });
            it("should not fail to abort an idle worker", () => {
                // initialize worker
                const worker = new AsyncWorker(), spy = jest.fn();
                worker.on("worker:finish", spy);
                // abort the worker
                worker.abort();
                expect(spy).not.toHaveBeenCalled();
            });
        });

        describe("method destroy", () => {
            it("should exist", () => {
                expect(AsyncWorker).toHaveMethod("destroy");
            });
            it("should destroy an empty idle worker", () => {
                // initialize worker
                const worker = new AsyncWorker(), spy = jest.fn();
                worker.on("worker:finish", spy);
                // destroy the worker immediately
                worker.destroy();
                // check result
                expect(spy).not.toHaveBeenCalled();
            });
            it("should abort an empty running worker", async () => {
                // initialize worker
                const worker = new AsyncWorker(), spy = jest.fn();
                worker.on("worker:finish", spy);
                // start the worker
                const observed = observePromise(worker.start());
                expect(spy).not.toHaveBeenCalled();
                // destroy the worker immediately
                worker.destroy();
                // check result
                expectWorkerAbort(spy, 0, true);
                await expectAborted(observed, true);
            });
        });

        describe("worker delay step", () => {
            it("should delay the worker", async () => {
                // initialize worker
                const worker = new AsyncWorker(), spy = jest.fn();
                worker.on("worker:finish", spy);
                worker.steps.addDelay(10);
                // start the worker
                const observed = observePromise(worker.start());
                expect(spy).not.toHaveBeenCalled();
                await clock.wait(11);
                // check result
                expectWorkerDone(spy, 0);
                await expectResolved(observed);
            });
            it("should abort a delay step", async () => {
                // initialize worker
                const worker = new AsyncWorker(), spy = jest.fn();
                worker.on("worker:finish", spy);
                worker.steps.addDelay(10);
                // start the worker
                const observed = observePromise(worker.start());
                clock.tick(5);
                expect(spy).not.toHaveBeenCalled();
                // abort the worker
                worker.abort();
                // check result
                expectWorkerAbort(spy, 0, false);
                await expectAborted(observed);
            });
            it("should abort a delay step on destruction", async () => {
                // initialize worker
                const worker = new AsyncWorker(), spy = jest.fn();
                worker.on("worker:finish", spy);
                worker.steps.addDelay(10);
                // start the worker
                const observed = observePromise(worker.start());
                clock.tick(5);
                expect(spy).not.toHaveBeenCalled();
                // abort the worker
                worker.destroy();
                // check result
                expectWorkerAbort(spy, 0, true);
                await expectAborted(observed, true);
            });
        });

        describe("worker step", () => {
            it("should register and run synchronous steps", async () => {
                // initialize worker
                const worker = new AsyncWorker(), spy = jest.fn();
                worker.on("worker:finish", spy);
                // initialize steps
                const steps = createJestFns(2);
                worker.steps.addDelay(500);
                worker.steps.addStep(steps[0]);
                worker.steps.addStep(steps[1]);
                worker.steps.addDelay(10);
                worker.steps.addStep(steps[0]);
                worker.steps.addStep(steps[1]);
                // start the worker
                const observed = observePromise(worker.start());
                expect(spy).not.toHaveBeenCalled();
                expect(steps).toHaveAllBeenCalledTimes(0, 0);
                // execute step1
                await clock.wait(500);
                expect(steps).toHaveAllBeenCalledTimes(0, 0);
                await clock.wait(10);
                expect(steps).toHaveAllBeenCalledTimes(1, 1);
                expectStepCall(steps[0]);
                expectStepCall(steps[1]);
                expect(spy).not.toHaveBeenCalled();
                // execute step2
                await clock.wait(1);
                expect(steps).toHaveAllBeenCalledTimes(1, 1);
                await clock.wait(10);
                expect(steps).toHaveAllBeenCalledTimes(2, 2);
                // check result
                await expectResolved(observed);
                expectWorkerDone(spy, 0);
            });
            it("should fail on synchronous exceptions", async () => {
                // initialize worker
                const worker = new AsyncWorker(), spy = jest.fn();
                worker.on("worker:finish", spy);
                // initialize steps
                const step1 = jest.fn();
                const error = new Error("step2");
                const step2 = jest.fn().mockImplementation(() => { throw error; });
                const step3 = jest.fn();
                worker.steps.addStep(step1);
                worker.steps.addStep(step2);
                worker.steps.addStep(step3);
                // start the worker
                const observed = observePromise(worker.start());
                // execute all steps
                await clock.wait(5);
                expect(step1).toHaveBeenCalledOnce();
                expect(step2).toHaveBeenCalledOnce();
                expect(step3).not.toHaveBeenCalled();
                // check result
                await expectRejected(observed, error);
                expectWorkerFail(spy, 0, error);
            });
            it("should wait for asynchronous steps", async () => {
                // initialize worker
                const worker = new AsyncWorker();
                const def1 = $.Deferred(), def2 = $.Deferred();
                const step1 = jest.fn().mockReturnValue(def1.promise());
                const step2 = jest.fn().mockReturnValue(def2.promise());
                worker.steps.addStep(step1);
                worker.steps.addStep(step2);
                // start the worker
                const observed = observePromise(worker.start());
                expect(step1).not.toHaveBeenCalled();
                expect(step2).not.toHaveBeenCalled();
                // execute step1
                await clock.wait(1);
                expect(step1).toHaveBeenCalledOnce();
                expect(step2).not.toHaveBeenCalled();
                await clock.wait(1000);
                expect(step2).not.toHaveBeenCalled();
                def1.resolve(2);
                // execute step2
                await clock.wait(5);
                expect(step2).not.toHaveBeenCalled();
                await clock.wait(5);
                expect(step2).toHaveBeenCalledOnce();
                await clock.wait(1000);
                def2.resolve(3);
                // check result
                await clock.wait(1);
                await expectResolved(observed);
            });
            it("should fail after rejected step", async () => {
                // initialize worker
                const worker = new AsyncWorker();
                const def1 = $.Deferred(), def2 = $.Deferred();
                const step1 = jest.fn().mockReturnValue(def1.promise());
                const step2 = jest.fn().mockReturnValue(def2.promise());
                const step3 = jest.fn();
                worker.steps.addStep(step1);
                worker.steps.addStep(step2);
                worker.steps.addStep(step3);
                // start the worker
                const observed = observePromise(worker.start());
                expect(step1).not.toHaveBeenCalled();
                expect(step2).not.toHaveBeenCalled();
                expect(step3).not.toHaveBeenCalled();
                // execute step1
                await clock.wait(1);
                expect(step1).toHaveBeenCalledOnce();
                expect(step2).not.toHaveBeenCalled();
                await clock.wait(1000);
                expect(step2).not.toHaveBeenCalled();
                def1.resolve(2);
                // execute step2 (fails on error)
                await clock.wait(10);
                expect(step2).toHaveBeenCalledOnce();
                expect(step3).not.toHaveBeenCalled();
                await clock.wait(1000);
                expect(step3).not.toHaveBeenCalled();
                def2.reject(42);
                // check result
                await clock.wait(1);
                await expectRejected(observed, 42);
            });
            it("should run a sub-worker", async () => {
                // initialize worker
                const worker1 = new AsyncWorker();
                const worker2 = new AsyncWorker();
                worker2.steps.addDelay(10);
                const spies = createJestFns(2);
                worker2.on("worker:start", spies[0]);
                worker2.on("worker:finish", spies[1]);
                worker1.steps.addDelay(10);
                worker1.steps.addStep(worker2);
                worker1.steps.addDelay(10);
                // start the worker
                const observed = observePromise(worker1.start());
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                // execute steps
                await clock.wait(11);
                expect(spies).toHaveAllBeenCalledTimes(1, 0);
                expect(worker2.running).toBeTrue();
                await clock.wait(5);
                expect(spies).toHaveAllBeenCalledTimes(1, 0);
                expect(worker2.running).toBeTrue();
                await clock.wait(6);
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
                expect(worker1.running).toBeTrue();
                expect(worker2.running).toBeFalse();
                // check result
                await clock.wait(11);
                expect(worker1.running).toBeFalse();
                await expectResolved(observed);
            });
            it("should abort a sub-worker", async () => {
                // initialize workers
                const worker1 = new AsyncWorker();
                const worker2 = new AsyncWorker();
                worker2.steps.addDelay(10);
                const spies = createJestFns(2);
                worker2.on("worker:start", spies[0]);
                worker2.on("worker:finish", spies[1]);
                const step = jest.fn();
                worker1.steps.addDelay(10);
                worker1.steps.addStep(worker2);
                worker1.steps.addStep(step);
                // start the worker
                const observed = observePromise(worker1.start());
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                // execute steps
                await clock.wait(11);
                expect(spies).toHaveAllBeenCalledTimes(1, 0);
                expect(step).not.toHaveBeenCalled();
                expect(worker2.running).toBeTrue();
                // abort the main worker
                worker1.abort();
                expect(worker1.running).toBeFalse();
                expect(worker2.running).toBeFalse();
                // check result
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
                expectWorkerAbort(spies[1], 0, false);
                await expectAborted(observed);
                await clock.wait(100);
                expect(step).not.toHaveBeenCalled();
            });
            it("should abort a sub-worker on destruction", async () => {
                // initialize workers
                const worker1 = new AsyncWorker();
                const worker2 = new AsyncWorker();
                worker2.steps.addDelay(10);
                const spies = createJestFns(2);
                worker2.on("worker:start", spies[0]);
                worker2.on("worker:finish", spies[1]);
                const step = jest.fn();
                worker1.steps.addDelay(10);
                worker1.steps.addStep(worker2);
                worker1.steps.addStep(step);
                // start the worker
                const observed = observePromise(worker1.start());
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                // execute steps
                await clock.wait(11);
                expect(spies).toHaveAllBeenCalledTimes(1, 0);
                expect(step).not.toHaveBeenCalled();
                expect(worker2.running).toBeTrue();
                // destroy the main worker
                worker1.destroy();
                expect(worker1.running).toBeFalse();
                expect(worker2.running).toBeFalse();
                // check result
                expect(spies).toHaveAllBeenCalledTimes(1, 1);
                expectWorkerAbort(spies[1], 0, false);
                await expectAborted(observed, true);
                await clock.wait(100);
                expect(step).not.toHaveBeenCalled();
            });
            it("should break from step", async () => {
                // initialize worker
                const worker = new AsyncWorker();
                const step1 = jest.fn();
                const step2 = jest.fn();
                worker.steps.addStep(step1);
                worker.steps.addStep(breakLoop);
                worker.steps.addStep(step2);
                // start the worker
                const observed = observePromise(worker.start());
                await clock.wait(1);
                // check result
                expect(step1).toHaveBeenCalledOnce();
                expect(step2).not.toHaveBeenCalled();
                expect(worker.running).toBeFalse();
                await expectResolved(observed);
            });
        });

        describe("worker loop step", () => {
            function delay100(index) {
                if (index === 5) { breakLoop(); }
                clock.tick(100);
            }
            it("should pack synchronous steps into same frame", async () => {
                // initialize worker
                const worker = new AsyncWorker();
                const step = jest.fn(delay100);
                worker.steps.addLoop(step);
                // start the worker
                const observed = observePromise(worker.start());
                expect(step).not.toHaveBeenCalled();
                clock.tick(1);
                // check that first time slice is filled with two iterations
                expect(step).toHaveBeenCalledTimes(2);
                // check that no new slice is started during delay time
                clock.tick(8);
                expect(step).toHaveBeenCalledTimes(2);
                // check that second time slice is filled with two iterations
                clock.tick(1);
                expect(step).toHaveBeenCalledTimes(4);
                // check that no new slice is started during delay time
                clock.tick(9);
                expect(step).toHaveBeenCalledTimes(4);
                // check that third time slice exits the loop
                clock.tick(1);
                expect(step).toHaveBeenCalledTimes(6);
                // check result
                await expectResolved(observed);
                expectLoopCalls(step, 6);
            });
            it("should pack asynchronous steps into same frame", async () => {
                // initialize worker
                const worker = new AsyncWorker();
                const defs = [$.Deferred(), $.Deferred(), $.Deferred(), $.Deferred()];
                const step = jest.fn(index => defs[index].promise());
                worker.steps.addLoop(step);
                // start the worker
                const observed = observePromise(worker.start());
                expect(step).not.toHaveBeenCalled();
                // resolve first iteration
                await clock.wait(1);
                expect(step).toHaveBeenCalledTimes(1);
                await clock.wait(99);
                expect(step).toHaveBeenCalledTimes(1);
                defs[0].resolve();
                expect(step).toHaveBeenCalledTimes(2); // re-call step function synchronously
                // resolve second iteration
                await clock.wait(100);
                expect(step).toHaveBeenCalledTimes(2);
                defs[1].resolve();
                expect(step).toHaveBeenCalledTimes(2); // time slice is full
                // delay between time slices
                await clock.wait(10);
                expect(step).toHaveBeenCalledTimes(3);
                // resolve third iteration
                await clock.wait(100);
                expect(step).toHaveBeenCalledTimes(3);
                defs[2].resolve();
                expect(step).toHaveBeenCalledTimes(4);
                // resolve forth iteration
                await clock.wait(10);
                expect(step).toHaveBeenCalledTimes(4);
                defs[3].reject(new BreakLoopToken());
                expect(step).toHaveBeenCalledTimes(4);
                // check result
                await clock.wait(1);
                await expectResolved(observed);
                expectLoopCalls(step, 4);
            });
            it("should use custom slice time and interval time", async () => {
                // initialize worker
                const worker = new AsyncWorker();
                const step = jest.fn(delay100);
                worker.steps.addLoop(step, { slice: 300, interval: 100 });
                // start the worker
                const observed = observePromise(worker.start());
                expect(step).not.toHaveBeenCalled();
                await clock.wait(1);
                // check that first time slice is filled with three iterations
                expect(step).toHaveBeenCalledTimes(3);
                // check that no new slice is started during delay time
                await clock.wait(98);
                expect(step).toHaveBeenCalledTimes(3);
                // check that third second slice exits the loop
                await clock.wait(1);
                expect(step).toHaveBeenCalledTimes(6);
                // check result
                await expectResolved(observed);
                expectLoopCalls(step, 6);
            });
            it("should break after specified loop count", async () => {
                // initialize worker
                const worker = new AsyncWorker();
                const step = jest.fn();
                worker.steps.addLoop(step, { count: 5 });
                // start the worker
                const observed = observePromise(worker.start());
                await clock.wait(1);
                // check result
                await expectResolved(observed);
                expect(step).toHaveBeenCalledTimes(5);
                expectLoopCalls(step, 5);
            });
            it("should fail on synchronous exception in loop", async () => {
                // initialize worker
                const worker = new AsyncWorker();
                const error = new Error();
                const step1 = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockImplementationOnce(() => { throw error; });
                const step2 = jest.fn();
                worker.steps.addLoop(step1);
                worker.steps.addStep(step2);
                // start the worker
                const observed = observePromise(worker.start());
                await clock.wait(1);
                // check result
                await expectRejected(observed, error);
                expectLoopCalls(step1, 3);
                expect(step2).not.toHaveBeenCalled();
            });
            it("should fail on rejected promise in loop", async () => {
                // initialize worker
                const worker = new AsyncWorker();
                const def1 = $.Deferred();
                const step1 = jest.fn().mockReturnValueOnce(undefined).mockReturnValueOnce(undefined).mockReturnValueOnce(def1.promise());
                const step2 = jest.fn();
                worker.steps.addLoop(step1);
                worker.steps.addStep(step2);
                // start the worker
                const observed = observePromise(worker.start());
                await clock.wait(1);
                // reject the pending step
                const error = new Error();
                def1.reject(error);
                // check result
                await expectRejected(observed, error);
                expectLoopCalls(step1, 3);
                expect(step2).not.toHaveBeenCalled();
            });
        });

        describe("worker iterator step", () => {
            function delay100() { clock.tick(100); }
            it("should pack synchronous steps into same frame", async () => {
                // initialize worker
                const worker = new AsyncWorker();
                const step = jest.fn(delay100);
                worker.steps.addIterator(() => [1, 3, 5, 7, 9], step);
                // start the worker
                const observed = observePromise(worker.start());
                expect(step).not.toHaveBeenCalled();
                clock.tick(1);
                // check that first time slice is filled with two iterations
                expect(step).toHaveBeenCalledTimes(2);
                // check that no new slice is started during delay time
                clock.tick(8);
                expect(step).toHaveBeenCalledTimes(2);
                // check that second time slice is filled with two iterations
                clock.tick(1);
                expect(step).toHaveBeenCalledTimes(4);
                // check that no new slice is started during delay time
                clock.tick(9);
                expect(step).toHaveBeenCalledTimes(4);
                // check that third time slice exits the loop
                clock.tick(1);
                expect(step).toHaveBeenCalledTimes(5);
                // check result
                await expectResolved(observed);
                expectIterateCalls(step, [1, 3, 5, 7, 9]);
            });
        });

        describe("method restart", () => {
            it("should exist", () => {
                expect(AsyncWorker).toHaveMethod("restart");
            });
            it("should restart a worker", async () => {
                // initialize worker
                const worker = new AsyncWorker(), spies = createJestFns(2);
                worker.steps.addDelay(10);
                worker.on("worker:start", spies[0]);
                worker.on("worker:finish", spies[1]);
                expect(spies).toHaveAllBeenCalledTimes(0, 0);
                expect(worker.running).toBeFalse();
                // restart the pending worker
                const observed1 = observePromise(worker.restart());
                await clock.wait(8);
                expect(spies).toHaveAllBeenCalledTimes(1, 0);
                expect(worker.running).toBeTrue();
                // restart the running worker
                const observed2 = observePromise(worker.restart());
                await expectAborted(observed1);
                expectWorkerAbort(spies[1], 0, false);
                await clock.wait(8);
                expect(spies).toHaveAllBeenCalledTimes(2, 1);
                expect(worker.running).toBeTrue();
                // check result
                await clock.wait(5);
                await clock.wait(0);
                await expectResolved(observed2);
                expect(spies).toHaveAllBeenCalledTimes(2, 2);
                expectWorkerDone(spies[1], 1);
                expect(worker.running).toBeFalse();
            });
        });
    });
});
