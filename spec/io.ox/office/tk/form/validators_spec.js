/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as validators from "@/io.ox/office/tk/form/validators";

// tests ======================================================================

describe("module tk/form/validators", () => {

    // class AbstractValidator ------------------------------------------------

    const { AbstractValidator } = validators;
    describe("class AbstractValidator", () => {
        it("should exist", () => {
            expect(AbstractValidator).toBeClass();
        });
    });

    // class TextValidator ----------------------------------------------------

    describe("class TextValidator", () => {
        const { TextValidator } = validators;
        it("should exist", () => {
            expect(TextValidator).toBeSubClassOf(AbstractValidator);
        });

        describe("method valueToText", () => {
            it("should exist", () => {
                expect(TextValidator).toHaveMethod("valueToText");
            });
            it("should return text as is", () => {
                const val = new TextValidator();
                expect(val.valueToText("")).toBe("");
                expect(val.valueToText("abc")).toBe("abc");
                expect(val.valueToText("abcdef")).toBe("abcdef");
            });
            it("should shorten text to maximum length", () => {
                const val = new TextValidator({ maxLength: 3 });
                expect(val.valueToText("")).toBe("");
                expect(val.valueToText("abc")).toBe("abc");
                expect(val.valueToText("abcdef")).toBe("abc");
            });
        });

        describe("method textToValue", () => {
            it("should exist", () => {
                expect(TextValidator).toHaveMethod("textToValue");
            });
            it("should return text as is", () => {
                const val = new TextValidator();
                expect(val.textToValue("")).toBe("");
                expect(val.textToValue("abc")).toBe("abc");
                expect(val.textToValue("abcdef")).toBe("abcdef");
            });
            it("should not shorten text to maximum length", () => {
                const val = new TextValidator({ maxLength: 3 });
                expect(val.textToValue("")).toBe("");
                expect(val.textToValue("abc")).toBe("abc");
                expect(val.textToValue("abcdef")).toBe("abcdef");
            });
        });

        describe("method validate", () => {
            it("should exist", () => {
                expect(TextValidator).toHaveMethod("validate");
            });
            it("should accept all text", () => {
                const val = new TextValidator();
                expect(val.validate("")).toBeTrue();
                expect(val.validate("abc")).toBeTrue();
                expect(val.validate("abcdef")).toBeTrue();
            });
            it("should not accept text beyond maximum length", () => {
                const val = new TextValidator({ maxLength: 3 });
                expect(val.validate("")).toBeTrue();
                expect(val.validate("abc")).toBeTrue();
                expect(val.validate("abcdef")).toBeFalse();
            });
        });
    });

    // class AbstractNumberValidator ------------------------------------------

    const { AbstractNumberValidator } = validators;
    describe("class AbstractNumberValidator", () => {
        it("should exist", () => {
            expect(AbstractNumberValidator).toBeSubClassOf(AbstractValidator);
        });

        describe("method validate", () => {
            it("should exist", () => {
                expect(AbstractNumberValidator).toHaveMethod("validate");
            });
        });

        describe("method restrictValue", () => {
            it("should exist", () => {
                expect(AbstractNumberValidator).toHaveMethod("restrictValue");
            });
        });

        describe("method valueToStepValue", () => {
            it("should exist", () => {
                expect(AbstractNumberValidator).toHaveMethod("valueToStepValue");
            });
        });

        describe("method stepValueToValue", () => {
            it("should exist", () => {
                expect(AbstractNumberValidator).toHaveMethod("stepValueToValue");
            });
        });
    });

    // class NumberValidator --------------------------------------------------

    describe("class NumberValidator", () => {
        const { NumberValidator } = validators;
        it("should exist", () => {
            expect(NumberValidator).toBeSubClassOf(AbstractNumberValidator);
        });

        describe("method valueToText", () => {
            it("should convert numbers to text", () => {
                const val = new NumberValidator();
                expect(val.valueToText(0)).toBe("0");
                expect(val.valueToText(4.5)).toBe("5");
                expect(val.valueToText(99)).toBe("99");
                expect(val.valueToText(-99)).toBe("-99");
            });
            it("should convert numbers to text with precision 0.1", () => {
                const val = new NumberValidator({ precision: 0.1 });
                expect(val.valueToText(0)).toBe("0");
                expect(val.valueToText(2.34)).toBe("2,3");
                expect(val.valueToText(-4.56)).toBe("-4,6");
            });
            it("should convert numbers to text with precision 10", () => {
                const val = new NumberValidator({ precision: 10 });
                expect(val.valueToText(0)).toBe("0");
                expect(val.valueToText(14.5)).toBe("10");
                expect(val.valueToText(-25.6)).toBe("-30");
            });
        });

        describe("method textToValue", () => {
            it("should convert text to numbers", () => {
                const val = new NumberValidator();
                expect(val.textToValue("0")).toBe(0);
                expect(val.textToValue("4,5")).toBe(5);
                expect(val.textToValue("099,0")).toBe(99);
                expect(val.textToValue("+099,0")).toBe(99);
                expect(val.textToValue("-099,0")).toBe(-99);
                expect(val.textToValue("12abc")).toBe(12);
                expect(val.textToValue("abc12")).toBeNull();
                expect(val.textToValue("")).toBeNull();
            });
            it("should convert text to numbers with precision 0.1", () => {
                const val = new NumberValidator({ precision: 0.1 });
                expect(val.textToValue("0")).toBe(0);
                expect(val.textToValue("2,34")).toBe(2.3);
                expect(val.textToValue("-4,56")).toBe(-4.6);
            });
            it("should convert text to numbers with precision 10", () => {
                const val = new NumberValidator({ precision: 10 });
                expect(val.textToValue("0")).toBe(0);
                expect(val.textToValue("14,5")).toBe(10);
                expect(val.textToValue("-25,6")).toBe(-30);
            });
        });

        describe("method validate", () => {
            it("should exist", () => {
                expect(NumberValidator).toHaveMethod("validate");
            });
        });

        describe("method restrictValue", () => {
            it("should exist", () => {
                expect(NumberValidator).toHaveMethod("restrictValue");
            });
        });

        describe("method valueToStepValue", () => {
            it("should exist", () => {
                expect(NumberValidator).toHaveMethod("valueToStepValue");
            });
        });

        describe("method stepValueToValue", () => {
            it("should exist", () => {
                expect(NumberValidator).toHaveMethod("stepValueToValue");
            });
        });
    });

    // class PercentValidator -------------------------------------------------

    describe("class PercentValidator", () => {
        const { PercentValidator } = validators;
        it("should exist", () => {
            expect(PercentValidator).toBeSubClassOf(AbstractNumberValidator);
        });

        describe("method valueToText", () => {
            it("should exist", () => {
                expect(PercentValidator).toHaveMethod("valueToText");
            });
        });

        describe("method textToValue", () => {
            it("should exist", () => {
                expect(PercentValidator).toHaveMethod("textToValue");
            });
        });

        describe("method validate", () => {
            it("should exist", () => {
                expect(PercentValidator).toHaveMethod("validate");
            });
        });

        describe("method restrictValue", () => {
            it("should exist", () => {
                expect(PercentValidator).toHaveMethod("restrictValue");
            });
        });

        describe("method valueToStepValue", () => {
            it("should exist", () => {
                expect(PercentValidator).toHaveMethod("valueToStepValue");
            });
        });

        describe("method stepValueToValue", () => {
            it("should exist", () => {
                expect(PercentValidator).toHaveMethod("stepValueToValue");
            });
        });
    });

    // class LengthValidator --------------------------------------------------

    describe("class LengthValidator", () => {
        const { LengthValidator } = validators;
        it("should exist", () => {
            expect(LengthValidator).toBeSubClassOf(AbstractNumberValidator);
        });

        describe("method valueToText", () => {
            it("should exist", () => {
                expect(LengthValidator).toHaveMethod("valueToText");
            });
        });

        describe("method textToValue", () => {
            it("should exist", () => {
                expect(LengthValidator).toHaveMethod("textToValue");
            });
        });

        describe("method validate", () => {
            it("should exist", () => {
                expect(LengthValidator).toHaveMethod("validate");
            });
        });

        describe("method restrictValue", () => {
            it("should exist", () => {
                expect(LengthValidator).toHaveMethod("restrictValue");
            });
        });

        describe("method valueToStepValue", () => {
            it("should exist", () => {
                expect(LengthValidator).toHaveMethod("valueToStepValue");
            });
        });

        describe("method stepValueToValue", () => {
            it("should exist", () => {
                expect(LengthValidator).toHaveMethod("stepValueToValue");
            });
        });
    });
});
