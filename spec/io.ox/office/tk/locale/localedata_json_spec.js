/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import json from "@/io.ox/office/tk/locale/localedata.json";

// constants ==================================================================

const LANG_PATTERN = "[a-z]{2,3}";
const DIALECT_PATTERN = "[a-z]{3,5}";
const SCRIPT_PATTERN = "[A-Z][a-z]{3}";
const REGION_PATTERN = "[A-Z]{2}";
const UNM49_PATTERN = "\\d{3}";
const VARIANT_PATTERN = "[a-z]{2,}";
const PRIVATE_PATTERN = "x-[a-z]+";

const GEN_REGION_PATTERN = "(" + REGION_PATTERN + "|" + UNM49_PATTERN + ")";

const NATIVE_LC_PATTERN = LANG_PATTERN + "(-" + DIALECT_PATTERN + ")?(-" + SCRIPT_PATTERN + ")?(-" + GEN_REGION_PATTERN + ")?([-_]" + VARIANT_PATTERN + "|-" + PRIVATE_PATTERN + ")?";
const SIMPLE_LC_PATTERN = LANG_PATTERN + "(-" + REGION_PATTERN + ")?";

const NATIVE_LC_RE = new RegExp("^" + NATIVE_LC_PATTERN + "$");

const NATIVE_LC_LIST_RE = new RegExp("^" + NATIVE_LC_PATTERN + "(\\|" + NATIVE_LC_PATTERN + ")*$");
const SIMPLE_LC_LIST_RE = new RegExp("^" + SIMPLE_LC_PATTERN + "(\\|" + SIMPLE_LC_PATTERN + ")*$");

// tests ======================================================================

describe("module tk/locale/localedata.json", () => {

    it("should exist", () => {
        expect(json).toBeObject();
    });

    describe("shortDateCodes entries", () => {
        it("should exist", () => {
            expect(json.shortDateCodes).toBeObject();
        });
        it("should be valid key/value pairs", () => {
            _.forEach(json.shortDateCodes, (value, key) => {
                expect(key, "locale code").toMatch(SIMPLE_LC_LIST_RE);
                expect(value, key).toMatch(/^./);
            });
        });
    });

    describe("longDateCodes entries", () => {
        it("should exist", () => {
            expect(json.longDateCodes).toBeObject();
        });
        it("should be valid key/value pairs", () => {
            _.forEach(json.longDateCodes, (value, key) => {
                expect(key, "locale code").toMatch(SIMPLE_LC_LIST_RE);
                expect(value, key).toMatch(/^./);
            });
        });
    });

    describe("shortTimeCodes entries", () => {
        it("should exist", () => {
            expect(json.shortTimeCodes).toBeObject();
        });
        it("should be valid key/value pairs", () => {
            _.forEach(json.shortTimeCodes, (value, key) => {
                expect(key, "locale code").toMatch(SIMPLE_LC_LIST_RE);
                expect(value, key).toMatch(/^./);
            });
        });
    });

    describe("longTimeCodes entries", () => {
        it("should exist", () => {
            expect(json.longTimeCodes).toBeObject();
        });
        it("should be valid key/value pairs", () => {
            _.forEach(json.longTimeCodes, (value, key) => {
                expect(key, "locale code").toMatch(SIMPLE_LC_LIST_RE);
                expect(value, key).toMatch(/^./);
            });
        });
    });

    describe("stdTokens entries", () => {
        it("should exist", () => {
            expect(json.stdTokens).toBeObject();
        });
        it("should be valid key/value pairs", () => {
            _.forEach(json.stdTokens, (value, key) => {
                expect(key, "locale code").toMatch(SIMPLE_LC_LIST_RE);
                expect(value, key).toMatch(/^./);
            });
        });
    });

    describe("dateTokens entries", () => {
        it("should exist", () => {
            expect(json.dateTokens).toBeObject();
        });
        it("should be valid key/value pairs", () => {
            _.forEach(json.dateTokens, (value, key) => {
                expect(key, "locale code").toMatch(SIMPLE_LC_LIST_RE);
                expect(value, key).toMatch(/^.{10}$/);
                expect(value, key).not.toMatch(/(.).*\1/); // no duplicate characters
            });
        });
    });

    function describeStringMap(propKey, keyRegExp, listSize) {
        describe(propKey + " entries", () => {
            it("should exist", () => {
                expect(json[propKey]).toBeObject();
            });
            it("should be valid key/value pairs", () => {
                const valueRegExp = new RegExp(`^[^|]+(\\|[^|]+){${listSize - 1}}$`);
                _.forEach(json[propKey], (value, key) => {
                    expect(key, "locale code").toMatch(keyRegExp);
                    expect(value, key).toMatch(valueRegExp);
                });
            });
        });
    }

    describeStringMap("colorTokens", SIMPLE_LC_LIST_RE, 8);

    describeStringMap("shortQuarters", NATIVE_LC_LIST_RE, 4);
    describeStringMap("longQuarters", NATIVE_LC_LIST_RE, 4);
    describeStringMap("shortMonths", NATIVE_LC_LIST_RE, 12);
    describeStringMap("longMonths", NATIVE_LC_LIST_RE, 12);
    describeStringMap("shortWeekdays", NATIVE_LC_LIST_RE, 7);
    describeStringMap("longWeekdays", NATIVE_LC_LIST_RE, 7);
    describeStringMap("hourModes", NATIVE_LC_LIST_RE, 2);

    describe("defRegions entries", () => {
        it("should exist", () => {
            expect(json.defRegions).toBeObject();
        });
        it("should be valid key/value pairs", () => {
            const regionRE = new RegExp("^" + GEN_REGION_PATTERN + "$");
            _.forEach(json.defRegions, (langs, region) => {
                expect(region, "region key").toMatch(regionRE);
                expect(langs, `language keys for region ${region}`).toMatch(/^[a-z]{2,3}(\|[a-z]{2,3})*$/);
            });
        });
    });

    describe("defLangs entries", () => {
        it("should exist", () => {
            expect(json.defLangs).toBeObject();
        });
        it("should be valid key/value pairs", () => {
            const regionsRE = new RegExp("^" + GEN_REGION_PATTERN + "(\\|" + GEN_REGION_PATTERN + ")*$");
            _.forEach(json.defLangs, (regions, lang) => {
                expect(lang, "language key").toMatch(/^[a-z]{2,3}$/);
                expect(regions, `region keys for ${lang}`).toMatch(regionsRE);
            });
        });
    });

    describe("lcidMap entries", () => {
        it("should exist", () => {
            expect(json.lcidMap).toBeObject();
        });
        it("should be valid key/value pairs", () => {
            _.forEach(json.lcidMap, (locale, lcid) => {
                expect(lcid, "LCID").toMatch(/^[0-9A-F]{4}$/);
                expect(locale, `locale code for ${lcid}`).toMatch(NATIVE_LC_RE);
            });
        });
    });
});
