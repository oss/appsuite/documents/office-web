/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { CollatorWrapper } from "@/io.ox/office/tk/locale/collatorwrapper";

// tests ======================================================================

describe("module tk/locale/collatorwrapper", () => {

    // class CollatorWrapper --------------------------------------------------

    describe("class CollatorWrapper", () => {

        it("should exist", () => {
            expect(CollatorWrapper).toBeClass();
        });

        describe("method equals", () => {
            it("should exist", () => {
                expect(CollatorWrapper).toHaveMethod("equals");
            });
            it("should compare strings regarding case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "variant" });
                expect(collator.equals("cafe", "cafe")).toBeTrue();
                expect(collator.equals("cafe", "CafE")).toBeFalse();
                expect(collator.equals("cafe", "café")).toBeFalse();
                expect(collator.equals("cafe", "CafÉ")).toBeFalse();
            });
            it("should compare strings ignoring case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "accent" });
                expect(collator.equals("cafe", "cafe")).toBeTrue();
                expect(collator.equals("cafe", "CafE")).toBeTrue();
                expect(collator.equals("cafe", "café")).toBeFalse();
                expect(collator.equals("cafe", "CafÉ")).toBeFalse();
            });
            it("should compare strings regarding case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "case" });
                expect(collator.equals("cafe", "cafe")).toBeTrue();
                expect(collator.equals("cafe", "CafE")).toBeFalse();
                expect(collator.equals("cafe", "café")).toBeTrue();
                expect(collator.equals("cafe", "CafÉ")).toBeFalse();
            });
            it("should compare strings ignoring case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "base" });
                expect(collator.equals("cafe", "cafe")).toBeTrue();
                expect(collator.equals("cafe", "CafE")).toBeTrue();
                expect(collator.equals("cafe", "café")).toBeTrue();
                expect(collator.equals("cafe", "CafÉ")).toBeTrue();
            });
        });

        describe("method compare", () => {
            it("should exist", () => {
                expect(CollatorWrapper).toHaveMethod("compare");
            });
            it("should compare strings regarding case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "variant" });
                expect(collator.compare("cafe", "cafe")).toBe(0);
                expect(collator.compare("cafe", "CafE")).toBe(-1);
                expect(collator.compare("cafe", "café")).toBe(-1);
                expect(collator.compare("cafe", "CafÉ")).toBe(-1);
                expect(collator.compare("cafe10", "cafe2")).toBe(-1);
            });
            it("should compare strings ignoring case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "accent" });
                expect(collator.compare("cafe", "cafe")).toBe(0);
                expect(collator.compare("cafe", "CafE")).toBe(0);
                expect(collator.compare("cafe", "café")).toBe(-1);
                expect(collator.compare("cafe", "CafÉ")).toBe(-1);
                expect(collator.compare("cafe10", "cafe2")).toBe(-1);
            });
            it("should compare strings regarding case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "case" });
                expect(collator.compare("cafe", "cafe")).toBe(0);
                expect(collator.compare("cafe", "CafE")).toBe(-1);
                expect(collator.compare("cafe", "café")).toBe(0);
                expect(collator.compare("cafe", "CafÉ")).toBe(-1);
                expect(collator.compare("cafe10", "cafe2")).toBe(-1);
            });
            it("should compare strings ignoring case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "base" });
                expect(collator.compare("cafe", "cafe")).toBe(0);
                expect(collator.compare("cafe", "CafE")).toBe(0);
                expect(collator.compare("cafe", "café")).toBe(0);
                expect(collator.compare("cafe", "CafÉ")).toBe(0);
                expect(collator.compare("cafe10", "cafe2")).toBe(-1);
            });
            it("should compare strings regarding case, regarding accent, numeric mode", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "variant", numeric: true });
                expect(collator.compare("cafe", "cafe")).toBe(0);
                expect(collator.compare("cafe", "CafE")).toBe(-1);
                expect(collator.compare("cafe", "café")).toBe(-1);
                expect(collator.compare("cafe", "CafÉ")).toBe(-1);
                expect(collator.compare("cafe10", "cafe2")).toBe(1);
            });
        });

        describe("method indexOf", () => {
            it("should exist", () => {
                expect(CollatorWrapper).toHaveMethod("indexOf");
            });
            it("should search for strings regarding case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "variant" });
                expect(collator.indexOf("cafe", "fe")).toBe(2);
                expect(collator.indexOf("cafe", "fE")).toBe(-1);
                expect(collator.indexOf("cafe", "fé")).toBe(-1);
                expect(collator.indexOf("cafe", "fÉ")).toBe(-1);
            });
            it("should search for strings ignoring case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "accent" });
                expect(collator.indexOf("cafe", "fe")).toBe(2);
                expect(collator.indexOf("cafe", "fE")).toBe(2);
                expect(collator.indexOf("cafe", "fé")).toBe(-1);
                expect(collator.indexOf("cafe", "fÉ")).toBe(-1);
            });
            it("should search for strings regarding case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "case" });
                expect(collator.indexOf("cafe", "fe")).toBe(2);
                expect(collator.indexOf("cafe", "fE")).toBe(-1);
                expect(collator.indexOf("cafe", "fé")).toBe(2);
                expect(collator.indexOf("cafe", "fÉ")).toBe(-1);
            });
            it("should search for strings ignoring case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "base" });
                expect(collator.indexOf("cafe", "fe")).toBe(2);
                expect(collator.indexOf("cafe", "fE")).toBe(2);
                expect(collator.indexOf("cafe", "fé")).toBe(2);
                expect(collator.indexOf("cafe", "fÉ")).toBe(2);
            });
        });

        describe("method sort", () => {
            it("should exist", () => {
                expect(CollatorWrapper).toHaveMethod("sort");
            });
            it("should sort the array in-place", () => {
                const collator = new CollatorWrapper("de-DE");
                const arr = ["b", "c", "a"];
                expect(collator.sort(arr)).toBe(arr);
                expect(arr).toEqual(["a", "b", "c"]);
            });
            it("should sort regarding case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "variant" });
                expect(collator.sort(["CafÉ", "cafe", "CafE", "café"])).toEqual(["cafe", "CafE", "café", "CafÉ"]);
            });
            it("should sort ignoring case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "accent" });
                expect(collator.sort(["CafÉ", "cafe", "CafE", "café"])).toEqual(["cafe", "CafE", "CafÉ", "café"]);
            });
            it("should sort regarding case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "case" });
                expect(collator.sort(["CafÉ", "cafe", "CafE", "café"])).toEqual(["cafe", "café", "CafÉ", "CafE"]);
            });
            it("should sort ignoring case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "base" });
                expect(collator.sort(["CafÉ", "cafe", "CafE", "café"])).toEqual(["CafÉ", "cafe", "CafE", "café"]);
            });
        });

        describe("method sortBy", () => {
            it("should exist", () => {
                expect(CollatorWrapper).toHaveMethod("sortBy");
            });
            it("should sort the array in-place", () => {
                const collator = new CollatorWrapper("de-DE");
                const arr = [{ p: "b" }, { p: "c" }, { p: "a" }];
                expect(collator.sortBy(arr, o => o.p)).toBe(arr);
                expect(arr).toEqual([{ p: "a" }, { p: "b" }, { p: "c" }]);
            });
            const o1 = { p: "CafÉ" }, o2 = { p: "cafe" }, o3 = { p: "CafE" }, o4 = { p: "café" };
            it("should sort regarding case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "variant" });
                expect(collator.sortBy([o1, o2, o3, o4], o => o.p)).toEqual([o2, o3, o4, o1]);
            });
            it("should sort ignoring case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "accent" });
                expect(collator.sortBy([o1, o2, o3, o4], o => o.p)).toEqual([o2, o3, o1, o4]);
            });
            it("should sort regarding case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "case" });
                expect(collator.sortBy([o1, o2, o3, o4], o => o.p)).toEqual([o2, o4, o1, o3]);
            });
            it("should sort ignoring case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "base" });
                expect(collator.sortBy([o1, o2, o3, o4], o => o.p)).toEqual([o1, o2, o3, o4]);
            });
        });

        describe("method sortByProp", () => {
            it("should exist", () => {
                expect(CollatorWrapper).toHaveMethod("sortByProp");
            });
            it("should sort the array in-place", () => {
                const collator = new CollatorWrapper("de-DE");
                const arr = [{ p: "b" }, { p: "c" }, { p: "a" }];
                expect(collator.sortByProp(arr, "p")).toBe(arr);
                expect(arr).toEqual([{ p: "a" }, { p: "b" }, { p: "c" }]);
            });
            const o1 = { p: "CafÉ" }, o2 = { p: "cafe" }, o3 = { p: "CafE" }, o4 = { p: "café" };
            it("should sort regarding case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "variant" });
                expect(collator.sortByProp([o1, o2, o3, o4], "p")).toEqual([o2, o3, o4, o1]);
            });
            it("should sort ignoring case, regarding accent", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "accent" });
                expect(collator.sortByProp([o1, o2, o3, o4], "p")).toEqual([o2, o3, o1, o4]);
            });
            it("should sort regarding case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "case" });
                expect(collator.sortByProp([o1, o2, o3, o4], "p")).toEqual([o2, o4, o1, o3]);
            });
            it("should sort ignoring case, ignoring accents", () => {
                const collator = new CollatorWrapper("de-DE", { sensitivity: "base" });
                expect(collator.sortByProp([o1, o2, o3, o4], "p")).toEqual([o1, o2, o3, o4]);
            });
        });
    });
});
