/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as localedataui from "@/io.ox/office/tk/locale/localedataui";

// tests ======================================================================

describe("module tk/locale/localedataui", () => {

    // constants --------------------------------------------------------------

    describe("constant LOCALE_DATA", () => {

        const { LOCALE_DATA } = localedataui;
        it("should exist", () => {
            expect(LOCALE_DATA).toBeObject();
        });

        it("should contain locale properties", () => {
            expect(LOCALE_DATA.lc).toBe("de_DE");
            expect(LOCALE_DATA.intl).toBe("de-DE");
            expect(LOCALE_DATA.language).toBe("de");
            expect(LOCALE_DATA.region).toBe("DE");
            expect(LOCALE_DATA.dec).toBe(",");
            expect(LOCALE_DATA.group).toBe(".");
            expect(LOCALE_DATA.rtl).toBeFalse();
            expect(LOCALE_DATA.cjk).toBeFalse();
            expect(LOCALE_DATA.unit).toBe("cm");
            expect(LOCALE_DATA.currency).toEqual({ iso: "EUR", symbol: "€", precision: 2 });
            expect(LOCALE_DATA.shortDate).toBe("DD.MM.YYYY");
            expect(LOCALE_DATA.longDate).toBe("DDDD, D. MMMM YYYY");
            expect(LOCALE_DATA.dateSep).toBe(".");
            expect(LOCALE_DATA.leadingMonth).toBeFalse();
            expect(LOCALE_DATA.leadingYear).toBeFalse();
            expect(LOCALE_DATA.shortTime).toBe("hh:mm");
            expect(LOCALE_DATA.longTime).toBe("hh:mm:ss");
            expect(LOCALE_DATA.timeSep).toBe(":");
            expect(LOCALE_DATA.hours24).toBeTrue();
            expect(LOCALE_DATA.stdToken).toBe("Standard");
            expect(LOCALE_DATA.dateTokens).toEqual({ Y: "J", M: "M", D: "T", h: "h", m: "m", s: "s", b: "b", g: "g", e: "e", a: "a" });
            expect(LOCALE_DATA.shortQuarters).toBeArray();
            expect(LOCALE_DATA.longQuarters).toBeArray();
            expect(LOCALE_DATA.shortWeekdays).toBeArray();
            expect(LOCALE_DATA.longWeekdays).toBeArray();
            expect(LOCALE_DATA.shortMonths).toBeArray();
            expect(LOCALE_DATA.longMonths).toBeArray();
            expect(LOCALE_DATA.hourModes).toEqual({ am: "AM", pm: "PM" });
        });

        describe("method parseDecimal", () => {
            it("should exist", () => {
                expect(LOCALE_DATA).toRespondTo("parseDecimal");
            });
            it("should convert strings to number using local decimal separator", () => {
                expect(LOCALE_DATA.parseDecimal("0")).toBe(0);
                expect(LOCALE_DATA.parseDecimal("42")).toBe(42);
                expect(LOCALE_DATA.parseDecimal("-1")).toBe(-1);
                expect(LOCALE_DATA.parseDecimal("0,5")).toBe(0.5);
                expect(LOCALE_DATA.parseDecimal("-12,5")).toBe(-12.5);
            });
            it("should return NaN for invalid strings", () => {
                expect(LOCALE_DATA.parseDecimal("")).toBeNaN();
                expect(LOCALE_DATA.parseDecimal("a")).toBeNaN();
            });
        });

        describe("method formatDecimal", () => {
            it("should exist", () => {
                expect(LOCALE_DATA).toRespondTo("formatDecimal");
            });
            it("should convert numbers to string using local decimal separator", () => {
                expect(LOCALE_DATA.formatDecimal(0)).toBe("0");
                expect(LOCALE_DATA.formatDecimal(42)).toBe("42");
                expect(LOCALE_DATA.formatDecimal(-1)).toBe("-1");
                expect(LOCALE_DATA.formatDecimal(0.5)).toBe("0,5");
                expect(LOCALE_DATA.formatDecimal(-12.5)).toBe("-12,5");
            });
        });

        describe("method getCollator", () => {
            it("should exist", () => {
                expect(LOCALE_DATA).toRespondTo("getCollator");
            });
        });

        describe("method getLanguageName", () => {
            it("should exist", () => {
                expect(LOCALE_DATA).toRespondTo("getLanguageName");
            });
            it("should return language names", () => {
                expect(LOCALE_DATA.getLanguageName("de")).toBe("Deutsch");
                expect(LOCALE_DATA.getLanguageName("fr_FR")).toBe("Französisch");
                expect(LOCALE_DATA.getLanguageName("en-DE")).toBe("Englisch");
            });
        });

        describe("method getRegionName", () => {
            it("should exist", () => {
                expect(LOCALE_DATA).toRespondTo("getRegionName");
            });
            it("should return region names", () => {
                expect(LOCALE_DATA.getRegionName("fr_FR")).toBe("Frankreich");
                expect(LOCALE_DATA.getRegionName("en-DE")).toBe("Deutschland");
            });
            it("should return undefined for missing region", () => {
                expect(LOCALE_DATA.getRegionName("de")).toBeUndefined();
                expect(LOCALE_DATA.getRegionName("en")).toBeUndefined();
            });
        });

        describe("method getLanguageAndRegionName", () => {
            it("should exist", () => {
                expect(LOCALE_DATA).toRespondTo("getLanguageAndRegionName");
            });
            it("should return language and region names", () => {
                expect(LOCALE_DATA.getLanguageAndRegionName("fr_FR")).toBe("Französisch (Frankreich)");
                expect(LOCALE_DATA.getLanguageAndRegionName("en-DE")).toBe("Englisch (Deutschland)");
            });
            it("should return language names for missing region", () => {
                expect(LOCALE_DATA.getLanguageAndRegionName("de")).toBe("Deutsch");
                expect(LOCALE_DATA.getLanguageAndRegionName("en")).toBe("Englisch");
            });
            it("should omit default region names", () => {
                expect(LOCALE_DATA.getLanguageAndRegionName("fr_FR", true)).toBe("Französisch");
                expect(LOCALE_DATA.getLanguageAndRegionName("en-DE", true)).toBe("Englisch (Deutschland)");
            });
            it("should not omit default region names for certain languages", () => {
                expect(LOCALE_DATA.getLanguageAndRegionName("pt-PT", true)).toBe("Portugiesisch (Portugal)");
                expect(LOCALE_DATA.getLanguageAndRegionName("pt-BR", true)).toBe("Portugiesisch (Brasilien)");
            });
        });

        describe("method getCurrencyName", () => {
            it("should exist", () => {
                expect(LOCALE_DATA).toRespondTo("getCurrencyName");
            });
            it("should return currency names", () => {
                expect(LOCALE_DATA.getCurrencyName("EUR")).toBe("Euro");
                expect(LOCALE_DATA.getCurrencyName("USD")).toBe("US-Dollar");
            });
        });

        describe("method setUnit", () => {
            it("should exist", () => {
                expect(LOCALE_DATA).toRespondTo("setUnit");
            });
        });
    });
});
