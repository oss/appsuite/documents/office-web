/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as localeid from "@/io.ox/office/tk/locale/localeid";

// tests ======================================================================

describe("module tk/locale/localeid", () => {

    // public functions -------------------------------------------------------

    describe("function parseLocaleId", () => {
        const { parseLocaleId } = localeid;
        it("should exist", () => {
            expect(parseLocaleId).toBeFunction();
        });
        it("should return the parsed locale code", () => {
            // language
            expect(parseLocaleId("en")).toEqual({ lc: "en", intl: "en", language: "en", dialect: undefined, script: undefined, region: undefined, variant: undefined });
            expect(parseLocaleId("EN")).toEqual({ lc: "en", intl: "en", language: "en", dialect: undefined, script: undefined, region: undefined, variant: undefined });
            expect(parseLocaleId("Haw")).toEqual({ lc: "haw", intl: "haw", language: "haw", dialect: undefined, script: undefined, region: undefined, variant: undefined });
            // region
            expect(parseLocaleId("en-IE")).toEqual({ lc: "en_IE", intl: "en-IE", language: "en", dialect: undefined, script: undefined, region: "IE", variant: undefined });
            expect(parseLocaleId("EN_ie")).toEqual({ lc: "en_IE", intl: "en-IE", language: "en", dialect: undefined, script: undefined, region: "IE", variant: undefined });
            expect(parseLocaleId("Haw-us")).toEqual({ lc: "haw_US", intl: "haw-US", language: "haw", dialect: undefined, script: undefined, region: "US", variant: undefined });
            expect(parseLocaleId("en-001")).toEqual({ lc: "en_001", intl: "en-001", language: "en", dialect: undefined, script: undefined, region: "001", variant: undefined });
            // dialect
            expect(parseLocaleId("zh-yue")).toEqual({ lc: "zh_yue", intl: "zh-yue", language: "zh", dialect: "yue", script: undefined, region: undefined, variant: undefined });
            expect(parseLocaleId("zh-yue-hk")).toEqual({ lc: "zh_yue_HK", intl: "zh-yue-HK", language: "zh", dialect: "yue", script: undefined, region: "HK", variant: undefined });
            // script
            expect(parseLocaleId("bs-cyrl")).toEqual({ lc: "bs_Cyrl", intl: "bs-Cyrl", language: "bs", dialect: undefined, script: "Cyrl", region: undefined, variant: undefined });
            expect(parseLocaleId("bs-cyrl-ba")).toEqual({ lc: "bs_Cyrl_BA", intl: "bs-Cyrl-BA", language: "bs", dialect: undefined, script: "Cyrl", region: "BA", variant: undefined });
            // pseudo locales
            expect(parseLocaleId("ar-ploc")).toEqual({ lc: "ar_ploc", intl: "ar-ploc", language: "ar", dialect: "ploc", script: undefined, region: undefined, variant: undefined });
            expect(parseLocaleId("ar-ploc-sa")).toEqual({ lc: "ar_ploc_SA", intl: "ar-ploc-SA", language: "ar", dialect: "ploc", script: undefined, region: "SA", variant: undefined });
            expect(parseLocaleId("ja-ploc")).toEqual({ lc: "ja_ploc", intl: "ja-ploc", language: "ja", dialect: "ploc", script: undefined, region: undefined, variant: undefined });
            expect(parseLocaleId("ja-ploc-JP")).toEqual({ lc: "ja_ploc_JP", intl: "ja-ploc-JP", language: "ja", dialect: "ploc", script: undefined, region: "JP", variant: undefined });
            expect(parseLocaleId("qps-ploc")).toEqual({ lc: "qps_ploc", intl: "qps-ploc", language: "qps", dialect: "ploc", script: undefined, region: undefined, variant: undefined });
            expect(parseLocaleId("qps-ploca")).toEqual({ lc: "qps_ploca", intl: "qps-ploca", language: "qps", dialect: "ploca", script: undefined, region: undefined, variant: undefined });
            expect(parseLocaleId("qps-plocm")).toEqual({ lc: "qps_plocm", intl: "qps-plocm", language: "qps", dialect: "plocm", script: undefined, region: undefined, variant: undefined });
            // variants
            expect(parseLocaleId("ca-es-Valencia")).toEqual({ lc: "ca_ES_valencia", intl: "ca-ES-valencia", language: "ca", dialect: undefined, script: undefined, region: "ES", variant: "valencia" });
            expect(parseLocaleId("ru-x-genlower")).toEqual({ lc: "ru_x-genlower", intl: "ru-x-genlower", language: "ru", dialect: undefined, script: undefined, region: undefined, variant: "x-genlower" });
            expect(parseLocaleId("ru-ru-X-GenLower")).toEqual({ lc: "ru_RU_x-genlower", intl: "ru-RU-x-genlower", language: "ru", dialect: undefined, script: undefined, region: "RU", variant: "x-genlower" });
        });
        it("should return undefined for invalid locale codes", () => {
            expect(parseLocaleId("")).toBeUndefined();
            expect(parseLocaleId("e")).toBeUndefined();
            expect(parseLocaleId("en1")).toBeUndefined();
            expect(parseLocaleId("en-1")).toBeUndefined();
            expect(parseLocaleId("enen")).toBeUndefined();
            expect(parseLocaleId("enen-IE")).toBeUndefined();
            expect(parseLocaleId("en-en-en-en")).toBeUndefined();
            expect(parseLocaleId("x-genlower")).toBeUndefined();
            expect(parseLocaleId("ru-y-genlower")).toBeUndefined();
        });
    });
});
