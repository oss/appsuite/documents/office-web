/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as localeregistry from "@/io.ox/office/tk/locale/localeregistry";

// tests ======================================================================

describe("module tk/locale/localeregistry", () => {

    // constants --------------------------------------------------------------

    describe("constant localeDataRegistry", () => {

        const { localeDataRegistry } = localeregistry;
        it("should exist", () => {
            expect(localeDataRegistry).toBeObject();
        });

        describe("method getLocaleData", () => {
            it("should exist", () => {
                expect(localeDataRegistry).toRespondTo("getLocaleData");
            });
            let localeDataEn = null;
            let localeDataDe = null;
            it("should return the locale en-US by locale code", () => {
                localeDataEn = localeDataRegistry.getLocaleData("en_US");
                expect(localeDataEn).toBeObject();
                expect(localeDataEn.lc).toBe("en_US");
                expect(localeDataEn.intl).toBe("en-US");
                expect(localeDataEn.language).toBe("en");
                expect(localeDataEn.region).toBe("US");
                expect(localeDataEn.cjk).toBeFalse();
                expect(localeDataEn.rtl).toBeFalse();
                expect(localeDataEn.dec).toBe(".");
                expect(localeDataEn.group).toBe(",");
                expect(localeDataEn.unit).toBe("in");
                expect(localeDataEn.currency).toEqual({ symbol: "$", iso: "USD", precision: 2 });
                expect(localeDataEn.shortDate).toBe("M/D/YYYY");
                expect(localeDataEn.longDate).toBe("DDDD, MMMM D, YYYY");
                expect(localeDataEn.dateSep).toBe("/");
                expect(localeDataEn.leadingMonth).toBeTrue();
                expect(localeDataEn.leadingYear).toBeFalse();
                expect(localeDataEn.shortTime).toBe("h:mm AM/PM");
                expect(localeDataEn.longTime).toBe("h:mm:ss AM/PM");
                expect(localeDataEn.timeSep).toBe(":");
                expect(localeDataEn.hours24).toBeFalse();
                expect(localeDataEn.stdToken).toBe("General");
                expect(localeDataEn.dateTokens).toEqual({ Y: "Y", M: "M", D: "D", h: "h", m: "m", s: "s", b: "b", g: "g", e: "e", a: "a" });
                expect(localeDataEn.colorTokens).toEqual({ black: "Black", blue: "Blue", cyan: "Cyan", green: "Green", magenta: "Magenta", red: "Red", white: "White", yellow: "Yellow" });
                expect(localeDataEn.shortQuarters).toEqual(["Q1", "Q2", "Q3", "Q4"]);
                expect(localeDataEn.longQuarters).toEqual(["1st quarter", "2nd quarter", "3rd quarter", "4th quarter"]);
                expect(localeDataEn.shortMonths).toEqual(["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]);
                expect(localeDataEn.longMonths).toEqual(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]);
                expect(localeDataEn.shortWeekdays).toEqual(["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]);
                expect(localeDataEn.longWeekdays).toEqual(["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]);
                expect(localeDataRegistry.getLocaleData("en_US")).toBe(localeDataEn);
                expect(localeDataRegistry.getLocaleData("en-US")).toBe(localeDataEn);
                expect(localeDataRegistry.getLocaleData("en_us")).toBe(localeDataEn);
                expect(localeDataRegistry.getLocaleData("en-us")).toBe(localeDataEn);
                expect(localeDataRegistry.getLocaleData("EN_US")).toBe(localeDataEn);
                expect(localeDataRegistry.getLocaleData("EN-US")).toBe(localeDataEn);
            });
            it("should return the locale de-DE by locale code", () => {
                localeDataDe = localeDataRegistry.getLocaleData("de_DE");
                expect(localeDataDe).toBeObject();
                expect(localeDataDe.lc).toBe("de_DE");
                expect(localeDataDe.intl).toBe("de-DE");
                expect(localeDataDe.language).toBe("de");
                expect(localeDataDe.region).toBe("DE");
                expect(localeDataDe.cjk).toBeFalse();
                expect(localeDataDe.rtl).toBeFalse();
                expect(localeDataDe.dec).toBe(",");
                expect(localeDataDe.group).toBe(".");
                expect(localeDataDe.unit).toBe("cm");
                expect(localeDataDe.currency).toEqual({ iso: "EUR", symbol: "€", precision: 2 });
                expect(localeDataDe.shortDate).toBe("DD.MM.YYYY");
                expect(localeDataDe.longDate).toBe("DDDD, D. MMMM YYYY");
                expect(localeDataDe.dateSep).toBe(".");
                expect(localeDataDe.leadingMonth).toBeFalse();
                expect(localeDataDe.leadingYear).toBeFalse();
                expect(localeDataDe.shortTime).toBe("hh:mm");
                expect(localeDataDe.longTime).toBe("hh:mm:ss");
                expect(localeDataDe.timeSep).toBe(":");
                expect(localeDataDe.hours24).toBeTrue();
                expect(localeDataDe.stdToken).toBe("Standard",);
                expect(localeDataDe.dateTokens).toEqual({ Y: "J", M: "M", D: "T", h: "h", m: "m", s: "s", b: "b", g: "g", e: "e", a: "a" },);
                expect(localeDataDe.colorTokens).toEqual({ black: "Schwarz", blue: "Blau", cyan: "Zyan", green: "Grün", magenta: "Magenta", red: "Rot", white: "Weiß", yellow: "Gelb" },);
                expect(localeDataDe.shortQuarters).toEqual(["Q1", "Q2", "Q3", "Q4"]);
                expect(localeDataDe.longQuarters).toEqual(["1. Quartal", "2. Quartal", "3. Quartal", "4. Quartal"]);
                expect(localeDataDe.shortMonths).toEqual(["Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"]);
                expect(localeDataDe.longMonths).toEqual(["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"]);
                expect(localeDataDe.shortWeekdays).toEqual(["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"]);
                expect(localeDataDe.longWeekdays).toEqual(["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"]);
                expect(localeDataRegistry.getLocaleData("de_DE")).toBe(localeDataDe);
                expect(localeDataRegistry.getLocaleData("de-DE")).toBe(localeDataDe);
                expect(localeDataRegistry.getLocaleData("de_de")).toBe(localeDataDe);
                expect(localeDataRegistry.getLocaleData("de-de")).toBe(localeDataDe);
                expect(localeDataRegistry.getLocaleData("DE_DE")).toBe(localeDataDe);
                expect(localeDataRegistry.getLocaleData("DE-DE")).toBe(localeDataDe);
            });
            it("should return the locale ja-JP by locale code", () => {
                const localeData = localeDataRegistry.getLocaleData("ja_JP");
                expect(localeData).toBeObject();
                expect(localeData.intl).toBe("ja-JP");
                expect(localeData.cjk).toBeTrue();
            });
            it("should return settings for mixed locale en-DE", () => {
                const localeData = localeDataRegistry.getLocaleData("en_DE");
                expect(localeData.lc).toBe("en_DE");
                expect(localeData.intl).toBe("en-DE");
                expect(localeData.language).toBe("en");
                expect(localeData.region).toBe("DE");
                expect(localeData.dec).toBe(",");
                expect(localeData.currency.iso).toBe("EUR");
                expect(localeData.longMonths[0]).toBe("January");
            });
            it("should use fallback for missing region", () => {
                const localeData1 = localeDataRegistry.getLocaleData("en");
                expect(localeData1.region).toBe("US");
                expect(localeData1.currency.iso).toBe("USD");
                const localeData2 = localeDataRegistry.getLocaleData("de");
                expect(localeData2.region).toBe("DE");
                expect(localeData2.currency.iso).toBe("EUR");
            });
        });

        describe("method getCurrencyData", () => {
            it("should exist", () => {
                expect(localeDataRegistry).toRespondTo("getCurrencyData");
            });
            it("should return currency data objects", () => {
                const data = localeDataRegistry.getCurrencyData("en_US");
                expect(data).toEqual({ iso: "USD", symbol: "$", precision: 2 });
                expect(localeDataRegistry.getCurrencyData("en-US")).toBe(data);
                expect(localeDataRegistry.getCurrencyData("EN-us")).toBe(data);
                expect(localeDataRegistry.getCurrencyData("de-DE")).toEqual({ iso: "EUR", symbol: "€", precision: 2 });
                expect(localeDataRegistry.getCurrencyData("de-CH")).toEqual({ iso: "CHF", symbol: "Fr.", precision: 2 });
            });
        });

        describe("method yieldCurrencyData", () => {
            it("should exist", () => {
                expect(localeDataRegistry).toRespondTo("yieldCurrencyData");
            });
        });

        describe("method getDefaultRegion", () => {
            it("should exist", () => {
                expect(localeDataRegistry).toRespondTo("getDefaultRegion");
            });
            it("should return the default region", () => {
                expect(localeDataRegistry.getDefaultRegion("de")).toBe("DE");
                expect(localeDataRegistry.getDefaultRegion("en")).toBe("US");
                expect(localeDataRegistry.getDefaultRegion("fi")).toBe("FI");
                expect(localeDataRegistry.getDefaultRegion("sl")).toBe("SI");
                expect(localeDataRegistry.getDefaultRegion("sv")).toBe("SE");
                expect(localeDataRegistry.getDefaultRegion("eo")).toBe("001");
                expect(localeDataRegistry.getDefaultRegion("EN")).toBe("US");
                expect(localeDataRegistry.getDefaultRegion("qq")).toBeUndefined();
            });
        });

        describe("method getDefaultLanguage", () => {
            it("should exist", () => {
                expect(localeDataRegistry).toRespondTo("getDefaultLanguage");
            });
            it("should return the default language", () => {
                expect(localeDataRegistry.getDefaultLanguage("AT")).toBe("de");
                expect(localeDataRegistry.getDefaultLanguage("DE")).toBe("de");
                expect(localeDataRegistry.getDefaultLanguage("FI")).toBe("fi");
                expect(localeDataRegistry.getDefaultLanguage("GB")).toBe("en");
                expect(localeDataRegistry.getDefaultLanguage("SI")).toBe("sl");
                expect(localeDataRegistry.getDefaultLanguage("SE")).toBe("sv");
                expect(localeDataRegistry.getDefaultLanguage("US")).toBe("en");
                expect(localeDataRegistry.getDefaultLanguage("001")).toBe("en");
                expect(localeDataRegistry.getDefaultLanguage("419")).toBe("es");
                expect(localeDataRegistry.getDefaultLanguage("us")).toBe("en");
                expect(localeDataRegistry.getDefaultLanguage("QQ")).toBeUndefined();
            });
        });

        describe("method resolveLCID", () => {
            it("should exist", () => {
                expect(localeDataRegistry).toRespondTo("resolveLCID");
            });
            it("should return locale code", () => {
                expect(localeDataRegistry.resolveLCID(0x0409)).toContainProps({ intl: "en-US" });
                expect(localeDataRegistry.resolveLCID(0x0407)).toContainProps({ intl: "de-DE" });
                expect(localeDataRegistry.resolveLCID(-1)).toBeUndefined();
                expect(localeDataRegistry.resolveLCID(0xFFFFFFFF)).toBeUndefined();
            });
        });
    });
});
