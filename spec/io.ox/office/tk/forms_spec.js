/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as forms from "@/io.ox/office/tk/forms";

// tests ======================================================================

describe("module tk/forms", () => {

    // constants --------------------------------------------------------------

    describe("constant HIDDEN_CLASS", () => {
        it("should exist", () => {
            expect(forms.HIDDEN_CLASS).toBeString();
            expect(forms.HIDDEN_CLASS).not.toBe("");
        });
    });

    describe("constant HIDDEN_SELECTOR", () => {
        it("should exist", () => {
            expect(forms.HIDDEN_SELECTOR).toBeString();
            expect(forms.HIDDEN_SELECTOR).not.toBe("");
        });
    });

    describe("constant VISIBLE_SELECTOR", () => {
        it("should exist", () => {
            expect(forms.VISIBLE_SELECTOR).toBeString();
            expect(forms.VISIBLE_SELECTOR).not.toBe("");
        });
    });

    describe("constant DISABLED_CLASS", () => {
        it("should exist", () => {
            expect(forms.DISABLED_CLASS).toBeString();
            expect(forms.DISABLED_CLASS).not.toBe("");
        });
    });

    describe("constant DISABLED_SELECTOR", () => {
        it("should exist", () => {
            expect(forms.DISABLED_SELECTOR).toBeString();
            expect(forms.DISABLED_SELECTOR).not.toBe("");
        });
    });

    describe("constant ENABLED_SELECTOR", () => {
        it("should exist", () => {
            expect(forms.ENABLED_SELECTOR).toBeString();
            expect(forms.ENABLED_SELECTOR).not.toBe("");
        });
    });

    describe("constant SELECTED_CLASS", () => {
        it("should exist", () => {
            expect(forms.SELECTED_CLASS).toBeString();
            expect(forms.SELECTED_CLASS).not.toBe("");
        });
    });

    describe("constant SELECTED_SELECTOR", () => {
        it("should exist", () => {
            expect(forms.SELECTED_SELECTOR).toBeString();
            expect(forms.SELECTED_SELECTOR).not.toBe("");
        });
    });

    describe("constant FOCUSED_CLASS", () => {
        it("should exist", () => {
            expect(forms.FOCUSED_CLASS).toBeString();
            expect(forms.FOCUSED_CLASS).not.toBe("");
        });
    });

    describe("constant BUTTON_CLASS", () => {
        it("should exist", () => {
            expect(forms.BUTTON_CLASS).toBeString();
            expect(forms.BUTTON_CLASS).not.toBe("");
        });
    });

    describe("constant BUTTON_SELECTOR", () => {
        it("should exist", () => {
            expect(forms.BUTTON_SELECTOR).toBeString();
            expect(forms.BUTTON_SELECTOR).not.toBe("");
        });
    });
});
