/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import RunLoop from '@/io.ox/office/rt2/shared/jss_runloop';

import { useFakeClock } from "~/asynchelper";

//-------------------------------------------------------------------------
var /* object  */ CONTEXT   = window;

//-------------------------------------------------------------------------
describe('RT2 module RunLoop', function () {

    const clock = useFakeClock();

    var loggerContext = { write() {}, getLevel() {} };

    it('should exist', function () {
        expect(RunLoop).toBeFunction();
    });

    //---------------------------------------------------------------------
    // static methods

    //---------------------------------------------------------------------
    describe('method create', function () {
        it('should return new inst always', function () {
            var /* RunLoop */ aLoopA = RunLoop.create(null, null);
            var /* RunLoop */ aLoopB = RunLoop.create(null, null);
            expect(aLoopA).not.toBe(aLoopB);
        });
    });

    //---------------------------------------------------------------------
    // complex cases

    //---------------------------------------------------------------------
    describe('run loop handler', function () {
        //-----------------------------------------------------------------
        it('should be called on times only with expected value if it return TRUE', function () {
            // handler has to return TRUE if event was handled -> run loop wont call handler again
            var /* function */ fOKHandler = jest.fn().mockReturnValue(true);
            var /* object   */ TEST_DATA  = 'test-data';
            var /* int      */ FREQUENCY  = 1;
            var /* int      */ LOOPS      = FREQUENCY * 5;
            var /* RunLoop  */ aRunLoop   = RunLoop.create(CONTEXT, fOKHandler, loggerContext);

            aRunLoop.setFrequency(FREQUENCY);

            aRunLoop.start();

            aRunLoop.enqueue(TEST_DATA);
            clock.tick(LOOPS); // force multiple calls to handler ...

            expect(fOKHandler).toHaveBeenCalledOnce();
            expect(fOKHandler).toHaveBeenCalledWith(TEST_DATA);

            aRunLoop.stop();
        });

        //-----------------------------------------------------------------
        it('should be called again and again if it return FALSE', function () {
            // handler has to return FALSE to simulate error - run loop will cal it again and again until it return true
            var /* function */ fFAILHandler = jest.fn().mockReturnValue(false);
            var /* object   */ TEST_DATA    = 'test-data';
            var /* int      */ FREQUENCY    = 1;
            var /* int      */ LOOPS        = FREQUENCY * 5;
            var /* RunLoop  */ aRunLoop     = RunLoop.create(CONTEXT, fFAILHandler, loggerContext);

            aRunLoop.setFrequency(FREQUENCY);

            aRunLoop.start();

            aRunLoop.enqueue(TEST_DATA);
            clock.tick(LOOPS); // force multiple calls to handler ...

            expect(fFAILHandler).toHaveBeenCalledTimes(LOOPS);
            expect(fFAILHandler).toHaveBeenCalledWith(TEST_DATA);

            aRunLoop.stop();
        });

        //-----------------------------------------------------------------
        it('should run after stop/restart etcpp', function () {
            var /* function */ fOKHandler   = jest.fn().mockReturnValue(true);
            var /* object   */ TEST_DATA_1  = 'test-data-1';
            var /* object   */ TEST_DATA_2  = 'test-data-2';
            var /* int      */ FREQUENCY    = 1;
            var /* int      */ LOOPS        = FREQUENCY * 5;
            var /* RunLoop  */ aRunLoop     = RunLoop.create(CONTEXT, fOKHandler, loggerContext);

            aRunLoop.setFrequency(FREQUENCY);

            aRunLoop.start();

            aRunLoop.enqueue(TEST_DATA_1);
            clock.tick(LOOPS); // force multiple calls to handler ...

            expect(fOKHandler).toHaveBeenCalledOnce();
            expect(fOKHandler).toHaveBeenCalledWith(TEST_DATA_1);

            aRunLoop.stop();
            fOKHandler.mockClear();
            aRunLoop.start();

            aRunLoop.enqueue(TEST_DATA_2);
            clock.tick(LOOPS); // force multiple calls to handler ...

            expect(fOKHandler).toHaveBeenCalledOnce();
            expect(fOKHandler).toHaveBeenCalledWith(TEST_DATA_2);

            aRunLoop.stop();
        });
    });

});
