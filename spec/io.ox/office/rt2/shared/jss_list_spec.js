/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import List from '@/io.ox/office/rt2/shared/jss_list';

//-------------------------------------------------------------------------
describe('RT2 module List', function () {
    it('should exist', function () {
        expect(List).toBeFunction();
    });

    var /* List */ LIST_EMPTY  = new List();
    var /* List */ LIST_1_ITEM = new List();
    var /* List */ LIST_5_ITEM = new List();

    LIST_1_ITEM.add(1);

    LIST_5_ITEM.add(1);
    LIST_5_ITEM.add(2);
    LIST_5_ITEM.add(3);
    LIST_5_ITEM.add(4);
    LIST_5_ITEM.add(5);

    //---------------------------------------------------------------------
    describe('method size', function () {
        it('should return 0 for empty list', function () {
            expect(LIST_EMPTY.size()).toBe(0);
        });
        it('should return 1 for list with 1 item', function () {
            expect(LIST_1_ITEM.size()).toBe(1);
        });
        it('should return 5 for list with 5 items', function () {
            expect(LIST_5_ITEM.size()).toBe(5);
        });
    });

    //---------------------------------------------------------------------
    describe('method isEmpty', function () {
        it('should return true for empty list', function () {
            expect(LIST_EMPTY.isEmpty()).toBeTrue();
        });
        it('should return false for filled list', function () {
            expect(LIST_1_ITEM.isEmpty()).toBeFalse();
        });
    });

    //---------------------------------------------------------------------
    describe('method contains', function () {
        it('should return false for empty list', function () {
            expect(LIST_EMPTY.contains(1)).toBeFalse();
        });
        it('1 should return true for test list 1 item', function () {
            expect(LIST_1_ITEM.contains(1)).toBeTrue();
        });
        it('2 should return false for test list 1 item', function () {
            expect(LIST_1_ITEM.contains(2)).toBeFalse();
        });
        it('1 should return true for test list 5 items', function () {
            expect(LIST_5_ITEM.contains(1)).toBeTrue();
        });
        it('5 should return true for test list 5 items', function () {
            expect(LIST_5_ITEM.contains(5)).toBeTrue();
        });
        it('6 should return false for test list 5 items', function () {
            expect(LIST_5_ITEM.contains(6)).toBeFalse();
        });
    });

    //---------------------------------------------------------------------
    describe('method add', function () {
        var /* List */ aList4Add = new List();

        it('should return true adding non existing item', function () {
            expect(aList4Add.add(1)).toBeTrue();
        });
        it('should return false adding existing item', function () {
            expect(aList4Add.add(1)).toBeFalse();
        });
    });

    //---------------------------------------------------------------------
    describe('method remove', function () {
        var /* List */ aList4Remove = new List();
        aList4Remove.add(1);
        aList4Remove.add(5);

        it('should return false removing non existing item', function () {
            expect(aList4Remove.remove(2)).toBeFalse();
            expect(aList4Remove.contains(2)).toBeFalse();
        });
        it('should return true removing existing item', function () {
            expect(aList4Remove.remove(1)).toBeTrue();
        });
    });

    //---------------------------------------------------------------------
    describe('method get', function () {
        it('should return real value for existing items', function () {
            expect(LIST_5_ITEM.get(0)).toBe(1);
            expect(LIST_5_ITEM.get(1)).toBe(2);
            expect(LIST_5_ITEM.get(2)).toBe(3);
            expect(LIST_5_ITEM.get(3)).toBe(4);
            expect(LIST_5_ITEM.get(4)).toBe(5);
        });
        it('should return undefined for non existing items', function () {
            expect(LIST_5_ITEM.get(-1)).toBeUndefined();
            expect(LIST_5_ITEM.get(5)).toBeUndefined();
        });
    });

    //---------------------------------------------------------------------
    describe('method addAll', function () {
        var /* List */ aList4Add = new List();

        it('should return true adding non existing item', function () {
            expect(aList4Add.addAll([1, 2, 3])).toBeTrue();
        });
    });

    //---------------------------------------------------------------------
    describe('method iterator', function () {
        it('should work as expected', function () {
            var /* List< T > */ aList = new List();
            aList.addAll([1, 2, 3, 4, 5]);

            var /* Iterator< T > */ aIt    = aList.iterator();
            var /* int           */ nCheck = 1;
            while (aIt.hasNext()) {
                expect(aIt.next()).toBe(nCheck++);
            }

            expect(nCheck).toBe(6);
        });
    });
});
