/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import RT2Message from '@/io.ox/office/rt2/shared/rt2message';
import RT2MessageFactory from '@/io.ox/office/rt2/shared/rt2messagefactory';
import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';

//-------------------------------------------------------------------------
describe('RT2 module RT2Message', function () {
    it('should exist', function () {
        expect(RT2Message).toBeFunction();
    });

    //---------------------------------------------------------------------
    // static methods

    //---------------------------------------------------------------------
    describe('method :', function () {
        it.skip('getHeader/setHeader', function () {
            var /* RT2Message */ aMsg = RT2MessageFactory.newMessage(RT2Protocol.REQUEST_JOIN);
            aMsg.setHeader(RT2Protocol.HEADER_MSG_ID, '12345');
            expect(aMsg.getHeader(RT2Protocol.HEADER_MSG_ID)).toBe("12345");
        });
        it.skip('flags', function () {
            var /* RT2Message */ aMsg = RT2MessageFactory.newMessage(RT2Protocol.REQUEST_JOIN);
            console.log('##### DBG : msg = ' + aMsg.toString());
            console.log('##### DBG : CHECK FOR SEQUENCE : ');
            console.log('##### DBG : is sequence msg : ' + aMsg.isSequenceMessage());
            console.log('##### DBG : CHECK FOR SESSION  : ');
            console.log('##### DBG : is session  msg : ' + aMsg.isSessionMessage());
        });
    });
});
