/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';

//-------------------------------------------------------------------------
describe('RT2 module StringUtils', function () {
    it('should exist', function () {
        expect(StringUtils).toBeFunction();
    });

    //---------------------------------------------------------------------
    // static methods

    //---------------------------------------------------------------------
    describe('method isString', function () {
        it('should return false for <undefined>', function () {
            expect(StringUtils.isString(undefined)).toBeFalse();
        });
        it('should return false for <null>', function () {
            expect(StringUtils.isString(null)).toBeFalse();
        });
        it('should return true for ""', function () {
            expect(StringUtils.isString('')).toBeTrue();
        });
        it('should return true for "something"', function () {
            expect(StringUtils.isString('something')).toBeTrue();
        });
    });

    //---------------------------------------------------------------------
    describe('method isEmpty', function () {
        it('should return true for <undefined>', function () {
            expect(StringUtils.isEmpty(undefined)).toBeTrue();
        });
        it('should return true for <null>', function () {
            expect(StringUtils.isEmpty(null)).toBeTrue();
        });
        it('should return true for ""', function () {
            expect(StringUtils.isEmpty('')).toBeTrue();
        });
        it('should return false for "something"', function () {
            expect(StringUtils.isEmpty('something')).toBeFalse();
        });
    });

    //---------------------------------------------------------------------
    describe('method equals', function () {
        it('should return true for <undefined>.equals(<undefined>)', function () {
            expect(StringUtils.equals(undefined, undefined)).toBeTrue();
        });
        it('should return true for <undefined>.equals(<null>)', function () {
            expect(StringUtils.equals(undefined, null)).toBeTrue();
        });
        it('should return true for <null>.equals(<undefined>)', function () {
            expect(StringUtils.equals(null, undefined)).toBeTrue();
        });
        it('should return true for <null>.equals(<null>)', function () {
            expect(StringUtils.equals(null, null)).toBeTrue();
        });
        it('should return false for <null>.equals("")', function () {
            expect(StringUtils.equals(null, '')).toBeFalse();
        });
        it('should return false for "".equals(<null>)', function () {
            expect(StringUtils.equals('', null)).toBeFalse();
        });
    });

    //---------------------------------------------------------------------
    describe('method toLower', function () {
        it('should return <undefined> for <undefined>', function () {
            expect(StringUtils.toLower(undefined)).toBeUndefined();
        });
        it('should return <null> for <null>', function () {
            expect(StringUtils.toLower(null)).toBeNull();
        });
        it('should return "" for ""', function () {
            expect(StringUtils.toLower('')).toBe("");
        });
        it('should return "abc" for "ABC"', function () {
            expect(StringUtils.toLower('ABC')).toBe("abc");
        });
        it('should return "lsmf" for "LsMf"', function () {
            expect(StringUtils.toLower('LsMf')).toBe("lsmf");
        });
    });

    //---------------------------------------------------------------------
    describe('method toUpper', function () {
        it('should return <undefined> for <undefined>', function () {
            expect(StringUtils.toUpper(undefined)).toBeUndefined();
        });
        it('should return <null> for <null>', function () {
            expect(StringUtils.toUpper(null)).toBeNull();
        });
        it('should return "" for ""', function () {
            expect(StringUtils.toUpper('')).toBe("");
        });
        it('should return "ABC" for "abc"', function () {
            expect(StringUtils.toUpper('abc')).toBe("ABC");
        });
        it('should return "LSMF" for "LsMf"', function () {
            expect(StringUtils.toUpper('LsMf')).toBe("LSMF");
        });
    });

    //---------------------------------------------------------------------
    describe('method startsWith', function () {
        it('should return true for <undefined>.startsWith(<undefined>)', function () {
            expect(StringUtils.startsWith(undefined, undefined)).toBeTrue();
        });
        it('should return true for <undefined>.startsWith(<null>)', function () {
            expect(StringUtils.startsWith(undefined, null)).toBeTrue();
        });
        it('should return true for <null>.startsWith(<undefined>)', function () {
            expect(StringUtils.startsWith(null, undefined)).toBeTrue();
        });
        it('should return true for <null>.startsWith(<null>)', function () {
            expect(StringUtils.startsWith(null, null)).toBeTrue();
        });
        it('should return true for "".startsWith("")', function () {
            expect(StringUtils.startsWith('', '')).toBeTrue();
        });
        it('should return true for "abcd".startsWith("ab")', function () {
            expect(StringUtils.startsWith('abcd', 'ab')).toBeTrue();
        });
        it('should return false for "abcd".startsWith("xy")', function () {
            expect(StringUtils.startsWith('abcd', 'xy')).toBeFalse();
        });
        it('should return false for "abcd".startsWith("A")', function () {
            expect(StringUtils.startsWith('abcd', 'A')).toBeFalse();
        });
        it('should return false for "AbCd".startsWith("aB")', function () {
            expect(StringUtils.startsWith('AbCd', 'aB')).toBeFalse();
        });
    });

    //---------------------------------------------------------------------
    describe('method startsWithIgnoreCase', function () {
        it('should return true for <undefined>.startsWith(<undefined>)', function () {
            expect(StringUtils.startsWithIgnoreCase(undefined, undefined)).toBeTrue();
        });
        it('should return true for <undefined>.startsWith(<null>)', function () {
            expect(StringUtils.startsWithIgnoreCase(undefined, null)).toBeTrue();
        });
        it('should return true for <null>.startsWith(<undefined>)', function () {
            expect(StringUtils.startsWithIgnoreCase(null, undefined)).toBeTrue();
        });
        it('should return true for <null>.startsWith(<null>)', function () {
            expect(StringUtils.startsWithIgnoreCase(null, null)).toBeTrue();
        });
        it('should return true for "".startsWith("")', function () {
            expect(StringUtils.startsWithIgnoreCase('', '')).toBeTrue();
        });
        it('should return true for "abcd".startsWith("ab")', function () {
            expect(StringUtils.startsWithIgnoreCase('abcd', 'ab')).toBeTrue();
        });
        it('should return false for "abcd".startsWith("xy")', function () {
            expect(StringUtils.startsWithIgnoreCase('abcd', 'xy')).toBeFalse();
        });
        it('should return true for "abcd".startsWith("A")', function () {
            expect(StringUtils.startsWithIgnoreCase('abcd', 'A')).toBeTrue();
        });
        it('should return true for "AbCd".startsWith("aB")', function () {
            expect(StringUtils.startsWithIgnoreCase('AbCd', 'aB')).toBeTrue();
        });
    });

    //---------------------------------------------------------------------
    describe('method getLength', function () {
        it('should return 0 for <undefined>', function () {
            expect(StringUtils.getLength(undefined)).toBe(0);
        });
        it('should return 0 for <null>', function () {
            expect(StringUtils.getLength(null)).toBe(0);
        });
        it('should return 0 for ""', function () {
            expect(StringUtils.getLength('')).toBe(0);
        });
        it('should return 1 for "a"', function () {
            expect(StringUtils.getLength('a')).toBe(1);
        });
        it('should return 9 for "123456789"', function () {
            expect(StringUtils.getLength('123456789')).toBe(9);
        });
    });

    //---------------------------------------------------------------------
    describe('method substringAfterLast', function () {
        it('should return <undefined> for <undefined>', function () {
            expect(StringUtils.substringAfterLast(undefined, '.')).toBeUndefined();
        });
        it('should return <null> for <null>', function () {
            expect(StringUtils.substringAfterLast(null, '.')).toBeNull();
        });
        it('should return "" for ""', function () {
            expect(StringUtils.substringAfterLast('', '.')).toBe("");
        });
        it('should return "c" for "a.b.c"', function () {
            expect(StringUtils.substringAfterLast('a.b.c', '.')).toBe("c");
        });
        it('should return " world" for "hello world"', function () {
            expect(StringUtils.substringAfterLast('hello world', 'hello')).toBe(" world");
        });
    });

    //---------------------------------------------------------------------
    describe('method abbreviate', function () {
        it('should return <undefined> for <undefined>', function () {
            expect(StringUtils.abbreviate(undefined)).toBeUndefined();
        });
        it('should return <null> for <null>', function () {
            expect(StringUtils.abbreviate(null)).toBeNull();
        });
        it('should return "" for ""', function () {
            expect(StringUtils.abbreviate('')).toBe("");
        });
        it('should return "Hello" for abbreviate("Hello", 5)', function () {
            expect(StringUtils.abbreviate('Hello', 5)).toBe("Hello");
        });
        it('should return "123" for abbreviate("123", 3)', function () {
            expect(StringUtils.abbreviate('123', 3)).toBe("123");
        });
        it('should return "Hello World" for abbreviate("He...", 5)', function () {
            expect(StringUtils.abbreviate('Hello World', 5)).toBe("He...");
        });
        it('should return "..." for abbreviate("123", 1)', function () {
            expect(StringUtils.abbreviate('123', 1)).toBe("...");
        });

        // unicode
        it('should return substring without splitting surrogate pairs', function () {
            expect(StringUtils.abbreviate('ab\uD83D\uDE00c', 0)).toBe("...");
            expect(StringUtils.abbreviate('ab\uD83D\uDE00c', -1)).toBe("...");
            expect(StringUtils.abbreviate('ab\uD83D\uDE00c', 0)).toBe("...");
            expect(StringUtils.abbreviate('ab\uD83D\uDE00c', 1)).toBe("...");
            expect(StringUtils.abbreviate('ab\uD83D\uDE00c', 2)).toBe("...");
            expect(StringUtils.abbreviate('ab\uD83D\uDE00c', 3)).toBe("...");
            expect(StringUtils.abbreviate('ab\uD83D\uDE00c', 4)).toBe("a...");
            // with length 5 the while string can be returned...
            expect(StringUtils.abbreviate('ab\uD83D\uDE00c', 5)).toBe("ab\uD83D\uDE00c");
            expect(StringUtils.abbreviate('ab\uD83D\uDE00c', 6)).toBe("ab\uD83D\uDE00c");
            expect(StringUtils.abbreviate('\uD83D\uDE00', 0)).toBe("...");
            expect(StringUtils.abbreviate('\uD83D\uDE00', 1)).toBe("...");
            expect(StringUtils.abbreviate('\uD83D\uDE00', 2)).toBe("\uD83D\uDE00");
            expect(StringUtils.abbreviate('\uD83D\uDE00', 3)).toBe("\uD83D\uDE00");
        });
    });

    //---------------------------------------------------------------------
    describe('method unicodeSafeSplit', function () {
        it('should return substring without splitting surrogate pairs', function () {
            expect(StringUtils.unicodeSafeSplit('ab\uD83D\uDE00c', -1)).toBe("");
            expect(StringUtils.unicodeSafeSplit('ab\uD83D\uDE00c', 0)).toBe("");
            expect(StringUtils.unicodeSafeSplit('ab\uD83D\uDE00c', 1)).toBe("a");
            expect(StringUtils.unicodeSafeSplit('ab\uD83D\uDE00c', 2)).toBe("ab");
            expect(StringUtils.unicodeSafeSplit('ab\uD83D\uDE00c', 3)).toBe("ab");
            expect(StringUtils.unicodeSafeSplit('ab\uD83D\uDE00c', 4)).toBe("ab\uD83D\uDE00");
            expect(StringUtils.unicodeSafeSplit('ab\uD83D\uDE00c', 5)).toBe("ab\uD83D\uDE00c");
            expect(StringUtils.unicodeSafeSplit('ab\uD83D\uDE00c', 6)).toBe("ab\uD83D\uDE00c");
            expect(StringUtils.unicodeSafeSplit('\uD83D\uDE00', 0)).toBe("");
            expect(StringUtils.unicodeSafeSplit('\uD83D\uDE00', 1)).toBe("");
            expect(StringUtils.unicodeSafeSplit('\uD83D\uDE00', 2)).toBe("\uD83D\uDE00");
            expect(StringUtils.unicodeSafeSplit('\uD83D\uDE00', 3)).toBe("\uD83D\uDE00");
        });
        it('should handle empty string', function () {
            expect(StringUtils.unicodeSafeSplit('', 0)).toBe("");
            expect(StringUtils.unicodeSafeSplit('', 1)).toBe("");
            expect(StringUtils.unicodeSafeSplit('', -1)).toBe("");
        });
        it('should handle negative index correctly', function () {
            expect(StringUtils.unicodeSafeSplit('abc', -1)).toBe("");
            expect(StringUtils.unicodeSafeSplit('\uD83D\uDE00', -1)).toBe("");
        });
    });

    //---------------------------------------------------------------------
    describe('method defaultString', function () {
        it('should return <empty string> for <undefined>', function () {
            expect(StringUtils.defaultString(undefined)).toBe("");
        });
        it('should return <empty string> for <null>', function () {
            expect(StringUtils.defaultString(null)).toBe("");
        });
        it('should return <given default> for <undefined>', function () {
            expect(StringUtils.defaultString(undefined, 'bla')).toBe("bla");
        });
        it('should return <given default> for <null>', function () {
            expect(StringUtils.defaultString(null, 'foo')).toBe("foo");
        });
        it('should return <empty string> for <undefined, undefined>', function () {
            expect(StringUtils.defaultString(undefined, undefined)).toBe("");
        });
        it('should return <given string> for <string, undefined>', function () {
            expect(StringUtils.defaultString('blafoo', undefined)).toBe("blafoo");
        });
    });
});
