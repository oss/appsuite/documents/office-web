/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import HashMap from '@/io.ox/office/rt2/shared/jss_hashmap';

//-------------------------------------------------------------------------
describe('RT2 module HashMap', function () {
    it('should exist', function () {
        expect(HashMap).toBeFunction();
    });

    var /* Map< int, int > */ MAP_EMPTY  = new HashMap();
    var /* Map< int, int > */ MAP_1_ITEM = new HashMap();
    var /* Map< int, int > */ MAP_5_ITEM = new HashMap();

    MAP_1_ITEM.put(1, 10);

    MAP_5_ITEM.put(1, 10);
    MAP_5_ITEM.put(2, 20);
    MAP_5_ITEM.put(3, 30);
    MAP_5_ITEM.put(4, 40);
    MAP_5_ITEM.put(5, 50);

    // console.log ('key   set : '+MAP_5_ITEM.keySet ().toString ());
    // console.log ('value set : '+MAP_5_ITEM.values ().toString ());

    //---------------------------------------------------------------------
    describe('method size', function () {
        it('should return 0 for empty map', function () {
            expect(MAP_EMPTY.size()).toBe(0);
        });
        it('should return 1 for map with 1 item', function () {
            expect(MAP_1_ITEM.size()).toBe(1);
        });
        it('should return 5 for map with 5 items', function () {
            expect(MAP_5_ITEM.size()).toBe(5);
        });
    });

    //---------------------------------------------------------------------
    describe('method isEmpty', function () {
        it('should return true for empty map', function () {
            expect(MAP_EMPTY.isEmpty()).toBeTrue();
        });
        it('should return false for map with 1 item', function () {
            expect(MAP_1_ITEM.isEmpty()).toBeFalse();
        });
        it('should return false for map with 5 items', function () {
            expect(MAP_5_ITEM.isEmpty()).toBeFalse();
        });
    });

    //---------------------------------------------------------------------
    describe('method containsKey', function () {
        it('1 should return false for empty map', function () {
            expect(MAP_EMPTY.containsKey(1)).toBeFalse();
        });
        it('1 should return true for map with 1 item', function () {
            expect(MAP_1_ITEM.containsKey(1)).toBeTrue();
        });
        it('1 should return true for map with 5 items', function () {
            expect(MAP_5_ITEM.containsKey(1)).toBeTrue();
        });
        it('99 should return false for map with 5 items', function () {
            expect(MAP_5_ITEM.containsKey(99)).toBeFalse();
        });
    });

    //---------------------------------------------------------------------
    describe('method containsValue', function () {
        it('10 should return false for empty map', function () {
            expect(MAP_EMPTY.containsValue(10)).toBeFalse();
        });
        it('10 should return true for map with 1 item', function () {
            expect(MAP_1_ITEM.containsValue(10)).toBeTrue();
        });
        it('10 should return true for map with 5 items', function () {
            expect(MAP_5_ITEM.containsValue(10)).toBeTrue();
        });
        it('99 should return false for map with 5 items', function () {
            expect(MAP_5_ITEM.containsValue(99)).toBeFalse();
        });
    });

});
