/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { BaseObject } from '@/io.ox/office/tk/objects';

import { BaseResourceManager } from '@/io.ox/office/baseframework/resource/resourcemanager';
import { BaseModel } from '@/io.ox/office/baseframework/model/basemodel';
import { BaseView } from '@/io.ox/office/baseframework/view/baseview';
import { BaseController } from '@/io.ox/office/baseframework/controller/basecontroller';
import { BaseApplication } from '@/io.ox/office/baseframework/app/baseapplication';

import { createCoreApp } from '~/apphelper';

// class TestBaseModel ========================================================

/**
 * A real `BaseModel` with some test specific additions.
 */
class TestBaseModel extends BaseModel {
}

// class TestBaseView =========================================================

/**
 * A real `BaseView` with some test specific additions.
 */
class TestBaseView extends BaseView {
    constructor(docApp, docModel, config) {
        super(docApp, docModel, { zoomSteps: [0.5, 1, 2], ...config });
    }
    implInitialize() { }
}

// class TestBaseController ===================================================

/**
 * A real `BaseController` with some test specific additions.
 */
class TestBaseController extends BaseController {
}

// tests ======================================================================

describe('module baseframework/app/baseapplication', function () {

    // class BaseApplication --------------------------------------------------

    describe('class BaseApplication', function () {

        it('should subclass BaseObject', function () {
            expect(BaseApplication).toBeSubClassOf(BaseObject);
        });

        let coreApp = null;
        let docApp = null;

        describe('constructor', function () {
            it('should initialize the MVC instances', () => new Promise(resolve => {
                const resManSpy = jest.fn(app => new BaseResourceManager(app, {}));
                const modelSpy = jest.fn(app => new TestBaseModel(app));
                const viewSpy = jest.fn((app, docModel) => new TestBaseView(app, docModel));
                const contrSpy = jest.fn((app, docModel, docView) => new TestBaseController(app, docModel, docView));
                const mvcFactory = { createResourceManager: resManSpy, createModel: modelSpy, createView: viewSpy, createController: contrSpy };
                coreApp = createCoreApp('base');
                coreApp.set("docsLaunchConfig", { action: 'load', file: { id: '1', folder_id: '1' } });
                docApp = new BaseApplication(coreApp, mvcFactory);
                expect(resManSpy).not.toHaveBeenCalled();
                expect(modelSpy).not.toHaveBeenCalled();
                expect(viewSpy).not.toHaveBeenCalled();
                expect(contrSpy).not.toHaveBeenCalled();
                docApp.onInit(() => {
                    expect(resManSpy).toHaveBeenCalledOnce();
                    expect(modelSpy).toHaveBeenCalledOnce();
                    expect(viewSpy).toHaveBeenCalledOnce();
                    expect(contrSpy).toHaveBeenCalledOnce();
                    resolve();
                });
            }));
            it('should extend a new application instance', function () {
                expect(docApp).toRespondTo('getModel');
                expect(docApp).toRespondTo('getView');
                expect(docApp).toRespondTo('getController');
            });
            it('should provide methods of DObject', function () {
                expect(docApp).toRespondTo('onFulfilled');
                expect(docApp).toRespondTo('onRejected');
                expect(docApp).toRespondTo('onSettled');
            });
            it('should provide methods of EObject', function () {
                expect(docApp).toRespondTo('setTimeout');
                expect(docApp).toRespondTo('asyncForEach');
            });
            it('should provide methods of BaseObject', function () {
                expect(docApp).toRespondTo('registerDestructor');
                expect(docApp).toRespondTo('executeDelayed');
            });
            it('should provide methods of XAppAccess', function () {
                expect(docApp).toRespondTo('waitForImportStart');
                expect(docApp).toRespondTo('waitForImportSuccess');
                expect(docApp).toRespondTo('waitForImportFailure');
                expect(docApp).toRespondTo('waitForImport');
            });
        });

        describe('property resourceManager', function () {
            it('should exist', function () {
                expect(docApp.resourceManager).toBeInstanceOf(BaseResourceManager);
            });
        });

        describe('property docModel', function () {
            it('should exist', function () {
                expect(docApp.docModel).toBeInstanceOf(BaseModel);
            });
        });

        describe('property docView', function () {
            it('should exist', function () {
                expect(docApp.docView).toBeInstanceOf(BaseView);
            });
        });

        describe('property docController', function () {
            it('should exist', function () {
                expect(docApp.docController).toBeInstanceOf(BaseController);
            });
        });
    });
});
