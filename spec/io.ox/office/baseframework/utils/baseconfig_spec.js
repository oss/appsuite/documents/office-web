/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as config from '@/io.ox/office/tk/config';
import * as baseconfig from '@/io.ox/office/baseframework/utils/baseconfig';

describe('module baseframework/utils/baseconfig', function () {

    // re-exports -------------------------------------------------------------

    it('should export toolkit configuration', function () {
        expect(baseconfig).toContainProps(config);
    });

    // constants --------------------------------------------------------------

    describe('constant MAIL_AVAILABLE', function () {
        it('should exist', function () {
            expect(baseconfig.MAIL_AVAILABLE).toBeBoolean();
        });
    });

    describe('constant CONTACTS_AVAILABLE', function () {
        it('should exist', function () {
            expect(baseconfig.CONTACTS_AVAILABLE).toBeBoolean();
        });
    });

    describe('constant CONVERTER_AVAILABLE', function () {
        it('should exist', function () {
            expect(baseconfig.CONVERTER_AVAILABLE).toBeBoolean();
        });
    });

    describe('constant GUARD_AVAILABLE', function () {
        it('should exist', function () {
            expect(baseconfig.GUARD_AVAILABLE).toBeBoolean();
        });
    });

    describe('constant PRESENTER_AVAILABLE', function () {
        it('should exist', function () {
            expect(baseconfig.PRESENTER_AVAILABLE).toBeBoolean();
        });
    });

    describe('constant REMOTE_PRESENTER_AVAILABLE', function () {
        it('should exist', function () {
            expect(baseconfig.REMOTE_PRESENTER_AVAILABLE).toBeBoolean();
        });
    });

    describe('constant TEXT_AVAILABLE', function () {
        it('should exist', function () {
            expect(baseconfig.TEXT_AVAILABLE).toBeBoolean();
        });
    });

    describe('constant SPREADSHEET_AVAILABLE', function () {
        it('should exist', function () {
            expect(baseconfig.SPREADSHEET_AVAILABLE).toBeBoolean();
        });
    });

    describe('constant PRESENTATION_AVAILABLE', function () {
        it('should exist', function () {
            expect(baseconfig.PRESENTATION_AVAILABLE).toBeBoolean();
        });
    });
});
