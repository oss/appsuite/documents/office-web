/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { EventHub } from "@/io.ox/office/tk/events";
import { ZoomState } from "@/io.ox/office/baseframework/utils/zoomstate";

// tests ======================================================================

describe("module baseframework/utils/zoomstate", function () {

    // class ZoomState --------------------------------------------------------

    describe("class ZoomState", function () {
        it("should subclass EventHub", function () {
            expect(ZoomState).toBeSubClassOf(EventHub);
        });

        describe("property steps", function () {
            it("should contain sorted copy", function () {
                const steps = [3, 2, 6, 4];
                const zoomState = new ZoomState(steps);
                expect(zoomState.steps).not.toBe(steps);
                expect(zoomState.steps).toEqual([2, 3, 4, 6]);
            });
        });

        describe("property min", function () {
            it("should contain smallest step", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                expect(zoomState.min).toBe(2);
            });
        });

        describe("property max", function () {
            it("should contain largest step", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                expect(zoomState.max).toBe(6);
            });
        });

        describe("getter type", function () {
            it("should return current type", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                expect(zoomState.type).toBe("fixed");
            });
        });

        describe("getter factor", function () {
            it("should return current factor", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                expect(zoomState.factor).toBe(2);
            });
        });

        describe("method clampFactor", function () {
            it("should exist", function () {
                expect(ZoomState).toHaveMethod("clampFactor");
            });
            it("should clamp factor to zoom range", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                expect(zoomState.clampFactor(0)).toBe(2);
                expect(zoomState.clampFactor(1)).toBe(2);
                expect(zoomState.clampFactor(2)).toBe(2);
                expect(zoomState.clampFactor(3)).toBe(3);
                expect(zoomState.clampFactor(4)).toBe(4);
                expect(zoomState.clampFactor(5)).toBe(5);
                expect(zoomState.clampFactor(6)).toBe(6);
                expect(zoomState.clampFactor(7)).toBe(6);
                expect(zoomState.clampFactor(8)).toBe(6);
                expect(zoomState.factor).toBe(2);
            });
        });

        describe("method clampScale", function () {
            it("should exist", function () {
                expect(ZoomState).toHaveMethod("clampScale");
            });
            it("should clamp scale to zoom range", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                expect(zoomState.clampScale(0)).toBe(1);
                expect(zoomState.clampScale(1)).toBe(1);
                expect(zoomState.clampScale(2)).toBe(2);
                expect(zoomState.clampScale(3)).toBe(3);
                expect(zoomState.clampScale(4)).toBe(3);
                expect(zoomState.factor).toBe(2);
            });
        });

        describe("method register", function () {
            it("should exist", function () {
                expect(ZoomState).toHaveMethod("register");
            });
            it("should set a custom type", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                zoomState.register("custom", () => 1);
                expect(() => zoomState.register("custom", () => 1)).toThrow(RangeError);
            });
            it("should fail to replace built-in type", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                expect(() => zoomState.register("fixed", () => 1)).toThrow(RangeError);
            });
        });

        describe("method resolve", function () {
            it("should exist", function () {
                expect(ZoomState).toHaveMethod("resolve");
            });
            it("should resolve zoom factors", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                zoomState.register("double", () => zoomState.factor * 2);
                expect(zoomState.type).toBe("fixed");
                expect(zoomState.factor).toBe(2);
                expect(zoomState.resolve("fixed")).toBe(2);
                expect(zoomState.resolve("double")).toBe(4);
                expect(() => zoomState.resolve("unknown")).toThrow(RangeError);
                expect(zoomState.type).toBe("fixed");
                expect(zoomState.factor).toBe(2);
            });
        });

        describe("method set", function () {
            it("should exist", function () {
                expect(ZoomState).toHaveMethod("set");
            });
            it("should set zoom factors", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                zoomState.set("fixed", 1);
                expect(zoomState.factor).toBe(2);
                zoomState.set("fixed", 2);
                expect(zoomState.factor).toBe(2);
                zoomState.set("fixed", 5);
                expect(zoomState.factor).toBe(5);
                zoomState.set("fixed", 6);
                expect(zoomState.factor).toBe(6);
                zoomState.set("fixed", 7);
                expect(zoomState.factor).toBe(6);
                zoomState.set("fixed");
                expect(zoomState.factor).toBe(6);
                zoomState.set("fixed", 2.019);
                expect(zoomState.factor).toBe(2.01);
            });
            it("should use custom zoom types", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                zoomState.register("double", () => zoomState.factor * 2);
                expect(zoomState.type).toBe("fixed");
                expect(zoomState.factor).toBe(2);
                zoomState.set("double");
                expect(zoomState.type).toBe("double");
                expect(zoomState.factor).toBe(4);
                zoomState.set("double", 2.5);
                expect(zoomState.type).toBe("double");
                expect(zoomState.factor).toBe(2.5);
                zoomState.set("double");
                expect(zoomState.type).toBe("double");
                expect(zoomState.factor).toBe(5);
                zoomState.set("double");
                expect(zoomState.type).toBe("double");
                expect(zoomState.factor).toBe(10);
            });
            it("should fail for unknown type", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                expect(() => zoomState.set("unknown")).toThrow(RangeError);
            });
            it("should emit change events", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                zoomState.register("custom", () => 2.5);
                const spy = jest.fn();
                zoomState.on("change:state", spy);
                expect(spy).not.toHaveBeenCalled();
                zoomState.set("fixed", 5);
                expect(spy).toHaveBeenCalledWith({ type: "fixed", factor: 5, scale: 2.5, options: undefined });
                spy.mockClear();
                zoomState.set("fixed", 5);
                expect(spy).not.toHaveBeenCalled();
                zoomState.set("fixed", 2.5);
                expect(spy).toHaveBeenCalledWith({ type: "fixed", factor: 2.5, scale: 0.5, options: undefined });
                spy.mockClear();
                zoomState.set("custom");
                expect(spy).toHaveBeenCalledWith({ type: "custom", factor: 2.5, scale: 1, options: undefined });
                spy.mockClear();
                zoomState.set("custom", 2.5);
                expect(spy).not.toHaveBeenCalled();
                zoomState.set("custom", 2.5, { force: true });
                expect(spy).toHaveBeenCalledWith({ type: "custom", factor: 2.5, scale: 1, options: { force: true } });
                spy.mockClear();
                zoomState.set("fixed", 2, { silent: true });
                expect(spy).not.toHaveBeenCalled();
                zoomState.set("fixed", 3, { custom: 42 });
                expect(spy).toHaveBeenCalledWith({ type: "fixed", factor: 3, scale: 1.5, options: { custom: 42 } });
            });
        });

        describe("method refresh", function () {
            it("should exist", function () {
                expect(ZoomState).toHaveMethod("refresh");
            });
            it("should refresh zoom factors", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                zoomState.register("double", () => zoomState.factor * 2);
                const spy = jest.fn();
                zoomState.on("change:state", spy);
                zoomState.refresh();
                expect(spy).not.toHaveBeenCalled();
                zoomState.refresh({ force: true, custom: 42 });
                expect(spy).toHaveBeenCalledWith({ type: "fixed", factor: 2, scale: 1, options: { force: true, custom: 42 } });
                spy.mockClear();
                zoomState.set("double", 2);
                expect(spy).toHaveBeenCalledWith({ type: "double", factor: 2, scale: 1, options: undefined });
                spy.mockClear();
                zoomState.refresh();
                expect(zoomState.factor).toBe(4);
                expect(spy).toHaveBeenCalledWith({ type: "double", factor: 4, scale: 2, options: undefined });
                spy.mockClear();
                zoomState.refresh({ silent: true });
                expect(zoomState.factor).toBe(8);
                expect(spy).not.toHaveBeenCalled();
            });
        });

        describe("method canDec", function () {
            it("should exist", function () {
                expect(ZoomState).toHaveMethod("canDec");
            });
            it("should return whether decreasing is possible", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                zoomState.set("fixed", 2);
                expect(zoomState.canDec()).toBeFalse();
                zoomState.set("fixed", 4);
                expect(zoomState.canDec()).toBeTrue();
                zoomState.set("fixed", 6);
                expect(zoomState.canDec()).toBeTrue();
            });
        });

        describe("method dec", function () {
            it("should exist", function () {
                expect(ZoomState).toHaveMethod("dec");
            });
            it("should decrease factor", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                zoomState.register("custom", () => 5);
                zoomState.set("custom");
                expect(zoomState.type).toBe("custom");
                expect(zoomState.factor).toBe(5);
                zoomState.dec();
                expect(zoomState.type).toBe("fixed");
                expect(zoomState.factor).toBe(4);
                zoomState.dec();
                expect(zoomState.factor).toBe(3);
                zoomState.dec();
                expect(zoomState.factor).toBe(2);
                zoomState.dec();
                expect(zoomState.factor).toBe(2);
            });
        });

        describe("method canInc", function () {
            it("should exist", function () {
                expect(ZoomState).toHaveMethod("canInc");
            });
            it("should return whether increasing is possible", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                zoomState.set("fixed", 2);
                expect(zoomState.canInc()).toBeTrue();
                zoomState.set("fixed", 4);
                expect(zoomState.canInc()).toBeTrue();
                zoomState.set("fixed", 6);
                expect(zoomState.canInc()).toBeFalse();
            });
        });

        describe("method inc", function () {
            it("should exist", function () {
                expect(ZoomState).toHaveMethod("inc");
            });
            it("should increase factor", function () {
                const zoomState = new ZoomState([3, 2, 6, 4]);
                zoomState.register("custom", () => 2.5);
                zoomState.set("custom");
                expect(zoomState.type).toBe("custom");
                expect(zoomState.factor).toBe(2.5);
                zoomState.inc();
                expect(zoomState.type).toBe("fixed");
                expect(zoomState.factor).toBe(3);
                zoomState.inc();
                expect(zoomState.factor).toBe(4);
                zoomState.inc();
                expect(zoomState.factor).toBe(6);
                zoomState.inc();
                expect(zoomState.factor).toBe(6);
            });
        });
    });
});
