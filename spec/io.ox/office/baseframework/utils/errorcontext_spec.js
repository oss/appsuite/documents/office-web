/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as errorcontext from '@/io.ox/office/baseframework/utils/errorcontext';

describe('module baseframework/utils/errorcontext', function () {

    describe('constants', function () {
        it('should exist', function () {
            expect(errorcontext.GENERAL).toBeString();
            expect(errorcontext.CREATEDOC).toBeString();
            expect(errorcontext.LOAD).toBeString();
            expect(errorcontext.TEXT).toBeString();
            expect(errorcontext.PRESENTATION).toBeString();
            expect(errorcontext.SPREADSHEET).toBeString();
            expect(errorcontext.SAVE).toBeString();
            expect(errorcontext.SAVEAS).toBeString();
            expect(errorcontext.CLOSE).toBeString();
            expect(errorcontext.RENAME).toBeString();
            expect(errorcontext.DOWNLOAD).toBeString();
            expect(errorcontext.OFFLINE).toBeString();
            expect(errorcontext.CONNECTION).toBeString();
            expect(errorcontext.SYNCHRONIZATION).toBeString();
            expect(errorcontext.FLUSH).toBeString();
            expect(errorcontext.READONLY).toBeString();
            expect(errorcontext.RESTOREDOC).toBeString();
        });
    });
});
