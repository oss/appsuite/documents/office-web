/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as apputils from '@/io.ox/office/baseframework/utils/apputils';

// tests ======================================================================

describe('module baseframework/utils/apputils', function () {

    // enums ------------------------------------------------------------------

    const { AppType } = apputils;
    describe('enum AppType', function () {
        it('should exist', function () {
            expect(AppType).toBeObject();
            expect(AppType).toHaveProperty('TEXT', 'text');
            expect(AppType).toHaveProperty('SPREADSHEET', 'spreadsheet');
            expect(AppType).toHaveProperty('PRESENTATION', 'presentation');
        });
    });

    describe('enum FileFormatType', function () {
        const { FileFormatType } = apputils;
        it('should exist', function () {
            expect(FileFormatType).toBeObject();
            expect(FileFormatType).toHaveProperty('OOX', 'ooxml');
            expect(FileFormatType).toHaveProperty('ODF', 'odf');
        });
    });

    describe('enum FlushReason', function () {
        const { FlushReason } = apputils;
        it('should exist', function () {
            expect(FlushReason).toBeObject();
            expect(FlushReason).toHaveProperty('DOWNLOAD');
            expect(FlushReason).toHaveProperty('EMAIL');
            expect(FlushReason).toHaveProperty('QUIT');
            expect(FlushReason).toHaveProperty('PRESENT');
            expect(FlushReason).toHaveProperty('USER');
        });
    });

    describe('enum QuitReason', function () {
        const { QuitReason } = apputils;
        it('should exist', function () {
            expect(QuitReason).toBeObject();
            expect(QuitReason).toHaveProperty('USER');
            expect(QuitReason).toHaveProperty('RELOAD');
            expect(QuitReason).toHaveProperty('AUTO');
            expect(QuitReason).toHaveProperty('ERROR');
            expect(QuitReason).toHaveProperty('LOGOUT');
        });
    });

    // constants --------------------------------------------------------------

    describe('constant DOCS_MODULE_PREFIX', function () {
        const { DOCS_MODULE_PREFIX } = apputils;
        it('should exist', function () {
            expect(DOCS_MODULE_PREFIX).toBeString();
        });
    });

    describe('constant PORTAL_MODULE_PREFIX', function () {
        const { PORTAL_MODULE_PREFIX } = apputils;
        it('should exist', function () {
            expect(PORTAL_MODULE_PREFIX).toBeString();
        });
    });

    const { TEXT_APP_CONFIG } = apputils;
    describe('constant TEXT_APP_CONFIG', function () {
        it('should exist', function () {
            expect(TEXT_APP_CONFIG).toBeObject();
            expect(TEXT_APP_CONFIG).toHaveProperty('appType', AppType.TEXT);
            expect(TEXT_APP_CONFIG.appIcon).toBeString();
            expect(TEXT_APP_CONFIG.appTitle).toBeString();
            expect(TEXT_APP_CONFIG.appExtRE).toBeInstanceOf(RegExp);
        });
    });

    const { SPREADSHEET_APP_CONFIG } = apputils;
    describe('constant SPREADSHEET_APP_CONFIG', function () {
        it('should exist', function () {
            expect(SPREADSHEET_APP_CONFIG).toBeObject();
            expect(SPREADSHEET_APP_CONFIG).toHaveProperty('appType', AppType.SPREADSHEET);
            expect(SPREADSHEET_APP_CONFIG.appIcon).toBeString();
            expect(SPREADSHEET_APP_CONFIG.appTitle).toBeString();
            expect(SPREADSHEET_APP_CONFIG.appExtRE).toBeInstanceOf(RegExp);
        });
    });

    const { PRESENTATION_APP_CONFIG } = apputils;
    describe('constant PRESENTATION_APP_CONFIG', function () {
        it('should exist', function () {
            expect(PRESENTATION_APP_CONFIG).toBeObject();
            expect(PRESENTATION_APP_CONFIG).toHaveProperty('appType', AppType.PRESENTATION);
            expect(PRESENTATION_APP_CONFIG.appIcon).toBeString();
            expect(PRESENTATION_APP_CONFIG.appTitle).toBeString();
            expect(PRESENTATION_APP_CONFIG.appExtRE).toBeInstanceOf(RegExp);
        });
    });

    describe('constant GLOBAL_APP_CONFIGS', function () {
        const { GLOBAL_APP_CONFIGS } = apputils;
        it('should exist', function () {
            expect(GLOBAL_APP_CONFIGS).toBeArray();
            expect(GLOBAL_APP_CONFIGS).toContain(TEXT_APP_CONFIG);
            expect(GLOBAL_APP_CONFIGS).toContain(SPREADSHEET_APP_CONFIG);
            expect(GLOBAL_APP_CONFIGS).toContain(PRESENTATION_APP_CONFIG);
        });
    });

    // functions --------------------------------------------------------------

    describe('function getGlobalAppConfig', function () {
        const { getGlobalAppConfig } = apputils;
        it('should exist', function () {
            expect(getGlobalAppConfig).toBeFunction();
        });
        it('should return the application configuration', function () {
            expect(getGlobalAppConfig(AppType.TEXT)).toBe(TEXT_APP_CONFIG);
            expect(getGlobalAppConfig(AppType.SPREADSHEET)).toBe(SPREADSHEET_APP_CONFIG);
            expect(getGlobalAppConfig(AppType.PRESENTATION)).toBe(PRESENTATION_APP_CONFIG);
            expect(getGlobalAppConfig(AppType.PRESENTER)).toBeUndefined();
        });
    });

    describe('function getPortalModulePath', function () {
        const { getPortalModulePath } = apputils;
        it('should exist', function () {
            expect(getPortalModulePath).toBeFunction();
        });
        it('should return the portal module path', function () {
            expect(getPortalModulePath(AppType.TEXT)).toBe("io.ox/office/portal/text");
            expect(getPortalModulePath(AppType.SPREADSHEET)).toBe("io.ox/office/portal/spreadsheet");
            expect(getPortalModulePath(AppType.PRESENTATION)).toBe("io.ox/office/portal/presentation");
            expect(getPortalModulePath(AppType.PRESENTER)).toBe("io.ox/office/portal/presenter");
        });
    });

    describe('function getPortalCapabilities', function () {
        const { getPortalCapabilities } = apputils;
        it('should exist', function () {
            expect(getPortalCapabilities).toBeFunction();
        });
        it('should return the capabilities', function () {
            expect(getPortalCapabilities(AppType.TEXT)).toBeString();
        });
    });

    describe('function isPortalAvailable', function () {
        const { isPortalAvailable } = apputils;
        it('should exist', function () {
            expect(isPortalAvailable).toBeFunction();
        });
        it('should return a boolean', function () {
            expect(isPortalAvailable(AppType.TEXT)).toBeBoolean();
        });
    });

    describe('function getEditorModulePath', function () {
        const { getEditorModulePath } = apputils;
        it('should exist', function () {
            expect(getEditorModulePath).toBeFunction();
        });
        it('should return the application module path', function () {
            expect(getEditorModulePath(AppType.TEXT)).toBe("io.ox/office/text");
            expect(getEditorModulePath(AppType.SPREADSHEET)).toBe("io.ox/office/spreadsheet");
            expect(getEditorModulePath(AppType.PRESENTATION)).toBe("io.ox/office/presentation");
            expect(getEditorModulePath(AppType.PRESENTER)).toBe("io.ox/office/presenter");
        });
    });

    describe('function getEditorCapabilities', function () {
        const { getEditorCapabilities } = apputils;
        it('should exist', function () {
            expect(getEditorCapabilities).toBeFunction();
        });
        it('should return the capabilities', function () {
            expect(getEditorCapabilities(AppType.TEXT)).toBeString();
        });
    });

    describe('function isEditorAvailable', function () {
        const { isEditorAvailable } = apputils;
        it('should exist', function () {
            expect(isEditorAvailable).toBeFunction();
        });
        it('should return a boolean', function () {
            expect(isEditorAvailable(AppType.TEXT)).toBeBoolean();
        });
    });

    describe('function parseModulePath', function () {
        const { parseModulePath } = apputils;
        it('should exist', function () {
            expect(parseModulePath).toBeFunction();
        });
        it('should return the specification for editor apps', function () {
            expect(parseModulePath("io.ox/office/text")).toEqual({ appType: AppType.TEXT, isPortal: false });
            expect(parseModulePath("io.ox/office/spreadsheet")).toEqual({ appType: AppType.SPREADSHEET, isPortal: false });
            expect(parseModulePath("io.ox/office/presentation")).toEqual({ appType: AppType.PRESENTATION, isPortal: false });
            expect(parseModulePath("io.ox/office/presenter")).toEqual({ appType: AppType.PRESENTER, isPortal: false });
        });
        it('should return the specification for portal apps', function () {
            expect(parseModulePath("io.ox/office/portal/text")).toEqual({ appType: AppType.TEXT, isPortal: true });
            expect(parseModulePath("io.ox/office/portal/spreadsheet")).toEqual({ appType: AppType.SPREADSHEET, isPortal: true });
            expect(parseModulePath("io.ox/office/portal/presentation")).toEqual({ appType: AppType.PRESENTATION, isPortal: true });
            expect(parseModulePath("io.ox/office/portal/presenter")).toEqual({ appType: AppType.PRESENTER, isPortal: true });
        });
        it('should return undefined for other module paths', function () {
            expect(parseModulePath("")).toBeUndefined();
            expect(parseModulePath("invalid")).toBeUndefined();
            expect(parseModulePath("io.ox/invalid")).toBeUndefined();
            expect(parseModulePath("io.ox/office/invalid")).toBeUndefined();
            expect(parseModulePath("io.ox/office/text/invalid")).toBeUndefined();
            expect(parseModulePath("io.ox/office/portal/invalid")).toBeUndefined();
            expect(parseModulePath("io.ox/office/portal/text/invalid")).toBeUndefined();
        });
    });

    describe('function getModulePath', function () {
        const { getModulePath } = apputils;
        it('should exist', function () {
            expect(getModulePath).toBeFunction();
        });
        it('should return the module path for editor apps', function () {
            expect(getModulePath({ appType: AppType.TEXT, isPortal: false })).toBe("io.ox/office/text");
            expect(getModulePath({ appType: AppType.SPREADSHEET, isPortal: false })).toBe("io.ox/office/spreadsheet");
            expect(getModulePath({ appType: AppType.PRESENTATION, isPortal: false })).toBe("io.ox/office/presentation");
            expect(getModulePath({ appType: AppType.PRESENTER, isPortal: false })).toBe("io.ox/office/presenter");
        });
        it('should return the module path for portal apps', function () {
            expect(getModulePath({ appType: AppType.TEXT, isPortal: true })).toBe("io.ox/office/portal/text");
            expect(getModulePath({ appType: AppType.SPREADSHEET, isPortal: true })).toBe("io.ox/office/portal/spreadsheet");
            expect(getModulePath({ appType: AppType.PRESENTATION, isPortal: true })).toBe("io.ox/office/portal/presentation");
            expect(getModulePath({ appType: AppType.PRESENTER, isPortal: true })).toBe("io.ox/office/portal/presenter");
        });
    });

    describe('function hasFloatingApp', function () {
        const { hasFloatingApp } = apputils;
        it('should exist', function () {
            expect(hasFloatingApp).toBeFunction();
        });
        it('should return a boolean', function () {
            expect(hasFloatingApp()).toBeFalse();
        });
    });

    describe('function trackActionEvent', function () {
        const { trackActionEvent } = apputils;
        it('should exist', function () {
            expect(trackActionEvent).toBeFunction();
        });
    });

    describe('function downloadTextFile', function () {
        const { downloadTextFile } = apputils;
        it('should exist', function () {
            expect(downloadTextFile).toBeFunction();
        });
    });
});
