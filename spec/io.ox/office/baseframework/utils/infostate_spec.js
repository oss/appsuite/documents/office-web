/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as infostate from '@/io.ox/office/baseframework/utils/infostate';

describe('module baseframework/utils/infostate', function () {

    describe('constants', function () {
        it('should exist', function () {
            expect(infostate.INFO_SYNCHRONIZATION_SUCCESSFUL).toBeString();
            expect(infostate.INFO_SYNCHRONIZATION_RO_SUCCESSFUL).toBeString();
            expect(infostate.INFO_EDITRIGHTS_ARE_TRANSFERED).toBeString();
            expect(infostate.INFO_EDITRIGHTS_SAVE_IN_PROGRESS).toBeString();
            expect(infostate.INFO_EDITRIGHTS_PLEASE_WAIT).toBeString();
            expect(infostate.INFO_ALREADY_TRANSFERING_EDIT_RIGHTS).toBeString();
            expect(infostate.INFO_CONNECTION_WAS_LOST_TRYING_SYNC).toBeString();
            expect(infostate.INFO_CONNECTION_WAS_LOST).toBeString();
            expect(infostate.INFO_CONNECTION_LOST_RELOAD_DOC).toBeString();
            expect(infostate.INFO_NO_PERMISSION_TO_CHANGE_DOC).toBeString();
            expect(infostate.INFO_PREPARE_LOSING_EDIT_RIGHTS).toBeString();
            expect(infostate.INFO_USER_IS_CURRENTLY_EDIT_DOC).toBeString();
            expect(infostate.INFO_EDITRIGHTS_RECEIVED).toBeString();
            expect(infostate.INFO_DOC_CONVERT_STORED_IN_DEFFOLDER).toBeString();
            expect(infostate.INFO_DOC_CONVERTED_AND_STORED).toBeString();
            expect(infostate.INFO_DOC_CREATED_IN_DEFAULTFOLDER).toBeString();
            expect(infostate.INFO_LOADING_IN_PROGRESS).toBeString();
            expect(infostate.INFO_DOC_SAVED_IN_FOLDER).toBeString();
            expect(infostate.INFO_DOC_SAVED_AS_TEMPLATE).toBeString();
            expect(infostate.INFO_DOC_RESCUED_SUCCESSFULLY).toBeString();
            expect(infostate.INFO_DOC_SAVE_FAILED).toBeString();
            expect(infostate.INFO_SYNCHRONIZATION_FAILED).toBeString();
            expect(infostate.INFO_CANNOT_EMBED_IMAGE_MUST_USE_URL).toBeString();
        });
    });
});
