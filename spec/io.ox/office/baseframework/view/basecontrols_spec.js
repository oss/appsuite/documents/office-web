/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as controls from "@/io.ox/office/tk/controls";
import * as appcontrols from "@/io.ox/office/baseframework/view/control/appcontrols";
import * as dynamiclabel from "@/io.ox/office/baseframework/view/control/dynamiclabel";
import * as percentlabel from "@/io.ox/office/baseframework/view/control/percentlabel";
import * as compoundbutton from "@/io.ox/office/baseframework/view/control/compoundbutton";
import * as compoundsplitbutton from "@/io.ox/office/baseframework/view/control/compoundsplitbutton";
import * as compoundcheckbox from "@/io.ox/office/baseframework/view/control/compoundcheckbox";

import * as basecontrols from "@/io.ox/office/baseframework/view/basecontrols";

// tests ======================================================================

describe("module baseframework/view/basecontrols", function () {

    // re-exports -------------------------------------------------------------

    it("re-exports should exist", function () {
        expect(basecontrols).toContainProps(controls);
        expect(basecontrols).toContainProps(appcontrols);
        expect(basecontrols).toContainProps(dynamiclabel);
        expect(basecontrols).toContainProps(percentlabel);
        expect(basecontrols).toContainProps(compoundbutton);
        expect(basecontrols).toContainProps(compoundsplitbutton);
        expect(basecontrols).toContainProps(compoundcheckbox);
    });
});
