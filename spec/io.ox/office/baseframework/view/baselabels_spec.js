/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as baselabels from "@/io.ox/office/baseframework/view/baselabels";

// tests ======================================================================

describe("module baseframework/view/baselabels", () => {

    // re-exports -------------------------------------------------------------

    it("should export toolkit labels", function () {
        expect(baselabels.OK_LABEL).toBeString();
        expect(baselabels.CANCEL_LABEL).toBeString();
    });


    it("should export settings labels", function () {
        expect(baselabels.DOCUMENTS_HEADER_LABEL).toBeString();
        expect(baselabels.DOCUMENTS_SETTINGS_TITLE).toBeString();
    });

    // constants --------------------------------------------------------------

    describe("constants for application types", () => {
        it("should exist", () => {
            expect(baselabels.TEXT_DOCUMENT_NAME).toBeString();
            expect(baselabels.TEXT_DOCUMENT_NAME_ENCRYPTED).toBeString();
            expect(baselabels.TEXT_DOCUMENT_NAME_NEW).toBeString();
            expect(baselabels.TEXT_DOCUMENT_NAME_ENCRYPTED_NEW).toBeString();
            expect(baselabels.SPREADSHEET_DOCUMENT_NAME).toBeString();
            expect(baselabels.SPREADSHEET_DOCUMENT_NAME_ENCRYPTED).toBeString();
            expect(baselabels.SPREADSHEET_DOCUMENT_NAME_NEW).toBeString();
            expect(baselabels.SPREADSHEET_DOCUMENT_NAME_ENCRYPTED_NEW).toBeString();
            expect(baselabels.PRESENTATION_DOCUMENT_NAME).toBeString();
            expect(baselabels.PRESENTATION_DOCUMENT_NAME_ENCRYPTED).toBeString();
            expect(baselabels.PRESENTATION_DOCUMENT_NAME_NEW).toBeString();
            expect(baselabels.PRESENTATION_DOCUMENT_NAME_ENCRYPTED_NEW).toBeString();
        });
    });

    describe("constants for UI labels", () => {
        it("should exist", () => {
            expect(baselabels.VIEW_LABEL).toBeString();
            expect(baselabels.MORE_LABEL).toBeString();
            expect(baselabels.OPTIONS_LABEL).toBeString();
            expect(baselabels.ZOOM_LABEL).toBeString();
        });
    });

    describe("option bags for form controls", () => {
        it("should exist", () => {
            expect(baselabels.QUIT_BUTTON_OPTIONS).toHaveProperty("icon");
            expect(baselabels.ZOOMOUT_BUTTON_OPTIONS).toHaveProperty("icon");
            expect(baselabels.ZOOMIN_BUTTON_OPTIONS).toHaveProperty("icon");
        });
    });
});
