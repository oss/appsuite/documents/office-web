/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as cellattrs from "@/io.ox/office/spreadsheet/utils/cellattrs";

// tests ======================================================================

describe("module spreadsheet/utils/cellattrs", () => {

    // functions --------------------------------------------------------------

    describe("function hasWrappingAttributes", () => {
        const { hasWrappingAttributes } = cellattrs;
        it("should exist", () => {
            expect(hasWrappingAttributes).toBeFunction();
        });
        it("should return the correct result", () => {
            expect(hasWrappingAttributes({ cell: { wrapText: false, alignHor: "auto", alignVert: "bottom" } })).toBeFalse();
            expect(hasWrappingAttributes({ cell: { wrapText: true, alignHor: "auto", alignVert: "bottom" } })).toBeTrue();
            expect(hasWrappingAttributes({ cell: { wrapText: false, alignHor: "justify", alignVert: "bottom" } })).toBeTrue();
            expect(hasWrappingAttributes({ cell: { wrapText: false, alignHor: "auto", alignVert: "justify" } })).toBeTrue();
        });
    });
});
