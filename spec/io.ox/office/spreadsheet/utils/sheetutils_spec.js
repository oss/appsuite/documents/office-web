/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { HAIR_WIDTH_HMM as HAIR, THIN_WIDTH_HMM as THIN, MEDIUM_WIDTH_HMM as MEDIUM, THICK_WIDTH_HMM as THICK } from "@/io.ox/office/editframework/utils/border";
import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import { AddressArray } from "@/io.ox/office/spreadsheet/utils/addressarray";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";
import { AddressSet } from "@/io.ox/office/spreadsheet/utils/addressset";
import { RangeSet } from "@/io.ox/office/spreadsheet/utils/rangeset";
import * as sheetutils from "@/io.ox/office/spreadsheet/utils/sheetutils";

import { a, r } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/sheetutils", () => {

    // re-exports -------------------------------------------------------------

    it("re-exports should exist", () => {
        expect(sheetutils).toHaveProperty("Address", Address);
        expect(sheetutils).toHaveProperty("Interval", Interval);
        expect(sheetutils).toHaveProperty("Range", Range);
        expect(sheetutils).toHaveProperty("Range3D", Range3D);
        expect(sheetutils).toHaveProperty("AddressArray", AddressArray);
        expect(sheetutils).toHaveProperty("IntervalArray", IntervalArray);
        expect(sheetutils).toHaveProperty("RangeArray", RangeArray);
        expect(sheetutils).toHaveProperty("Range3DArray", Range3DArray);
        expect(sheetutils).toHaveProperty("AddressSet", AddressSet);
        expect(sheetutils).toHaveProperty("RangeSet", RangeSet);
    });

    // constants --------------------------------------------------------------

    it("constants should exist", () => {
        expect(sheetutils.MAX_NAME_LENGTH).toBeIntegerInInterval(1, 1e5);
        expect(sheetutils.MAX_FILL_CELL_COUNT).toBeIntegerInInterval(1, 1e9);
        expect(sheetutils.MAX_AUTOFILL_COL_ROW_COUNT).toBeIntegerInInterval(1, 1e9);
        expect(sheetutils.MAX_MERGED_RANGES_COUNT).toBeIntegerInInterval(1, 1e9);
        expect(sheetutils.MAX_UNMERGE_CELL_COUNT).toBeIntegerInInterval(1, 1e9);
        expect(sheetutils.MAX_CHANGE_COLS_COUNT).toBeIntegerInInterval(1, 1e9);
        expect(sheetutils.MAX_CHANGE_ROWS_COUNT).toBeIntegerInInterval(1, 1e9);
        expect(sheetutils.MAX_SORT_LINES_COUNT).toBeIntegerInInterval(1, 1e9);
        expect(sheetutils.MAX_LENGTH_STANDARD_CELL).toBeIntegerInInterval(1, 1e5);
        expect(sheetutils.MAX_LENGTH_STANDARD_EDIT).toBeIntegerInInterval(1, 1e5);
        expect(sheetutils.MAX_LENGTH_STANDARD_FORMULA).toBeIntegerInInterval(1, 1e5);
        expect(sheetutils.MIN_CELL_SIZE).toBeIntegerInInterval(1, 1e5);
        expect(sheetutils.MAX_BORDER_WIDTH).toBeIntegerInInterval(1, sheetutils.MIN_CELL_SIZE);
        expect(sheetutils.MULTI_SELECTION).toBeBoolean();
    });

    describe("constant OUTER_BORDER_KEYS", () => {
        const { OUTER_BORDER_KEYS } = sheetutils;
        it("should exist", () => {
            expect(OUTER_BORDER_KEYS).toBeArrayOfSize(4);
            expect(OUTER_BORDER_KEYS).toIncludeSameMembers(["t", "b", "l", "r"]);
        });
    });

    describe("constant INNER_BORDER_KEYS", () => {
        const { INNER_BORDER_KEYS } = sheetutils;
        it("should exist", () => {
            expect(INNER_BORDER_KEYS).toBeArrayOfSize(2);
            expect(INNER_BORDER_KEYS).toIncludeSameMembers(["h", "v"]);
        });
    });

    describe("constant DIAGONAL_BORDER_KEYS", () => {
        const { DIAGONAL_BORDER_KEYS } = sheetutils;
        it("should exist", () => {
            expect(DIAGONAL_BORDER_KEYS).toBeArrayOfSize(2);
            expect(DIAGONAL_BORDER_KEYS).toIncludeSameMembers(["u", "d"]);
        });
    });

    describe("constant ATTR_BORDER_KEYS", () => {
        const { ATTR_BORDER_KEYS } = sheetutils;
        it("should exist", () => {
            expect(ATTR_BORDER_KEYS).toBeArrayOfSize(6);
            expect(ATTR_BORDER_KEYS).toIncludeSameMembers(["t", "b", "l", "r", "u", "d"]);
        });
    });

    describe("constant BORDER_CORNER_KEYS", () => {
        const { BORDER_CORNER_KEYS } = sheetutils;
        it("should exist", () => {
            expect(BORDER_CORNER_KEYS).toBeArrayOfSize(4);
            expect(BORDER_CORNER_KEYS).toIncludeSameMembers(["tl", "tr", "bl", "br"]);
        });
    });

    // enumerations -----------------------------------------------------------

    const { SheetType } = sheetutils;
    describe("enum SheetType", () => {
        it("should exist", () => {
            expect(SheetType).toBeObject();
        });
        it("should contain all sheet types", () => {
            expect(SheetType).toHaveProperty("WORKSHEET");
            expect(SheetType).toHaveProperty("CHARTSHEET");
            expect(SheetType).toHaveProperty("MACROSHEET");
            expect(SheetType).toHaveProperty("DIALOGSHEET");
        });
    });

    const { SplitMode } = sheetutils;
    describe("enum SplitMode", () => {
        it("should exist", () => {
            expect(SplitMode).toBeObject();
        });
        it("should contain all split modes", () => {
            expect(SplitMode).toHaveProperty("SPLIT");
            expect(SplitMode).toHaveProperty("FROZEN");
            expect(SplitMode).toHaveProperty("FROZEN_SPLIT");
        });
    });

    const { Direction } = sheetutils;
    describe("enum Direction", () => {
        it("should exist", () => {
            expect(Direction).toBeObject();
        });
        it("should contain all directions", () => {
            expect(Direction).toHaveProperty("LEFT");
            expect(Direction).toHaveProperty("RIGHT");
            expect(Direction).toHaveProperty("UP");
            expect(Direction).toHaveProperty("DOWN");
        });
    });

    const { MergeMode } = sheetutils;
    describe("enum MergeMode", () => {
        it("should exist", () => {
            expect(MergeMode).toBeObject();
        });
        it("should contain all merge modes", () => {
            expect(MergeMode).toHaveProperty("MERGE");
            expect(MergeMode).toHaveProperty("HORIZONTAL");
            expect(MergeMode).toHaveProperty("VERTICAL");
            expect(MergeMode).toHaveProperty("UNMERGE");
        });
    });

    const { AnchorMode } = sheetutils;
    describe("enum AnchorMode", () => {
        it("should exist", () => {
            expect(AnchorMode).toBeObject();
        });
        it("should contain all anchor modes", () => {
            expect(AnchorMode).toHaveProperty("ABSOLUTE");
            expect(AnchorMode).toHaveProperty("ONE_CELL");
            expect(AnchorMode).toHaveProperty("TWO_CELL");
        });
    });

    const { FindMatchType } = sheetutils;
    describe("enum FindMatchType", () => {
        it("should exist", () => {
            expect(FindMatchType).toBeObject();
        });
        it("should contain all anchor modes", () => {
            expect(FindMatchType).toHaveProperty("OVERLAP");
            expect(FindMatchType).toHaveProperty("CONTAIN");
            expect(FindMatchType).toHaveProperty("COVER");
            expect(FindMatchType).toHaveProperty("EXACT");
            expect(FindMatchType).toHaveProperty("PARTIAL");
            expect(FindMatchType).toHaveProperty("REFERENCE");
        });
    });

    // public functions -------------------------------------------------------

    describe("function mapKey", () => {
        const { mapKey } = sheetutils;
        it("should exist", () => {
            expect(mapKey).toBeFunction();
        });
        it("should return the map key of a name", () => {
            expect(mapKey("abcdef")).toBe("ABCDEF");
            expect(mapKey("aBcDeF")).toBe("ABCDEF");
            expect(mapKey("AbCdEf")).toBe("ABCDEF");
            expect(mapKey("ABCDEF")).toBe("ABCDEF");
        });
    });

    describe("function inverseSortVector", () => {
        const { inverseSortVector } = sheetutils;
        it("should exist", () => {
            expect(inverseSortVector).toBeFunction();
        });
    });

    describe("function isSupportedSheetType", () => {
        const { isSupportedSheetType } = sheetutils;
        it("should exist", () => {
            expect(isSupportedSheetType).toBeFunction();
        });
        it("should return whether sheet type is supported", () => {
            expect(isSupportedSheetType(SheetType.WORKSHEET)).toBeTrue();
            expect(isSupportedSheetType(SheetType.CHARTSHEET)).toBeTrue();
            expect(isSupportedSheetType(SheetType.MACROSHEET)).toBeFalse();
            expect(isSupportedSheetType(SheetType.DIALOGSHET)).toBeFalse();
        });
    });

    describe("function isCellSheetType", () => {
        const { isCellSheetType } = sheetutils;
        it("should exist", () => {
            expect(isCellSheetType).toBeFunction();
        });
        it("should return whether sheet type contains cells", () => {
            expect(isCellSheetType(SheetType.WORKSHEET)).toBeTrue();
            expect(isCellSheetType(SheetType.CHARTSHEET)).toBeFalse();
            expect(isCellSheetType(SheetType.MACROSHEET)).toBeTrue();
            expect(isCellSheetType(SheetType.DIALOGSHET)).toBeFalse();
        });
    });

    describe("function generateSheetName", () => {
        const { generateSheetName } = sheetutils;
        it("should exist", () => {
            expect(generateSheetName).toBeFunction();
        });
        it("should return the English sheet name", () => {
            expect(generateSheetName(1)).toBe("Sheet1");
            expect(generateSheetName(2)).toBe("Sheet2");
            expect(generateSheetName(100)).toBe("Sheet100");
        });
    });

    describe("function getTableName", () => {
        const { getTableName } = sheetutils;
        it("should exist", () => {
            expect(getTableName).toBeFunction();
        });
        it("should return the English table name", () => {
            expect(getTableName()).toBe("Table");
        });
    });

    describe("function getTableColName", () => {
        const { getTableColName } = sheetutils;
        it("should exist", () => {
            expect(getTableColName).toBeFunction();
        });
        it("should return the English column name", () => {
            expect(getTableColName()).toBe("Column");
        });
    });

    describe("function getTextOrientation", () => {

        const { getTextOrientation } = sheetutils;

        function getTextAlignment(value, align, display, preferText) {
            return getTextOrientation(value, align, display, preferText).cssTextAlign;
        }

        it("should exist", () => {
            expect(getTextOrientation).toBeFunction();
        });
        it("should return the correct cell text alignment for numbers", () => {
            expect(getTextAlignment(2, "left", "2")).toBe("left");
            expect(getTextAlignment(2, "center", "2")).toBe("center");
            expect(getTextAlignment(2, "right", "2")).toBe("right");
            expect(getTextAlignment(2, "justify", "2")).toBe("justify");
            expect(getTextAlignment(2, "distribute", "2")).toBe("justify");
            expect(getTextAlignment(2, "auto", "2")).toBe("right");
            expect(getTextAlignment(2, "auto", "2", true)).toBe("left");
        });
        it("should return the correct cell text alignment for strings", () => {
            expect(getTextAlignment("a", "left", "a")).toBe("left");
            expect(getTextAlignment("a", "center", "a")).toBe("center");
            expect(getTextAlignment("a", "right", "a")).toBe("right");
            expect(getTextAlignment("a", "justify", "a")).toBe("justify");
            expect(getTextAlignment("a", "distribute", "a")).toBe("justify");
            expect(getTextAlignment("a", "auto", "a")).toBe("left");
            expect(getTextAlignment("a", "auto", "a", true)).toBe("left");
        });
        it("should return the correct cell text alignment for booleans", () => {
            expect(getTextAlignment(true, "left", "TRUE")).toBe("left");
            expect(getTextAlignment(true, "center", "TRUE")).toBe("center");
            expect(getTextAlignment(true, "right", "TRUE")).toBe("right");
            expect(getTextAlignment(true, "justify", "TRUE")).toBe("justify");
            expect(getTextAlignment(true, "distribute", "TRUE")).toBe("justify");
            expect(getTextAlignment(true, "auto", "TRUE")).toBe("center");
            expect(getTextAlignment(true, "auto", "TRUE", true)).toBe("center");
        });
        it("should return the correct cell text alignment for error codes", () => {
            expect(getTextAlignment(ErrorCode.NUM, "left", "#NUM!")).toBe("left");
            expect(getTextAlignment(ErrorCode.NUM, "center", "#NUM!")).toBe("center");
            expect(getTextAlignment(ErrorCode.NUM, "right", "#NUM!")).toBe("right");
            expect(getTextAlignment(ErrorCode.NUM, "justify", "#NUM!")).toBe("justify");
            expect(getTextAlignment(ErrorCode.NUM, "distribute", "#NUM!")).toBe("justify");
            expect(getTextAlignment(ErrorCode.NUM, "auto", "#NUM!")).toBe("center");
            expect(getTextAlignment(ErrorCode.NUM, "auto", "#NUM!", true)).toBe("center");
        });
        it("should return the correct cell text alignment for blanks", () => {
            expect(getTextAlignment(null, "left", "")).toBe("left");
            expect(getTextAlignment(null, "center", "")).toBe("center");
            expect(getTextAlignment(null, "right", "")).toBe("right");
            expect(getTextAlignment(null, "justify", "")).toBe("justify");
            expect(getTextAlignment(null, "distribute", "")).toBe("justify");
            expect(getTextAlignment(null, "auto", "")).toMatch(/^(left|right)$/);
            expect(getTextAlignment(null, "auto", "", true)).toMatch(/^(left|right)$/);
        });
        it("should accept missing display parameter", () => {
            expect(getTextAlignment(2, "auto")).toBe("right");
            expect(getTextAlignment("a", "auto")).toBe("left");
            expect(getTextAlignment(true, "auto")).toBe("center");
            expect(getTextAlignment(ErrorCode.NUM, "auto")).toBe("center");
            expect(getTextAlignment(null, "auto")).toMatch(/^(left|right)$/);
        });
    });

    describe("function getOuterBorderKey", () => {
        const { getOuterBorderKey } = sheetutils;
        it("should exist", () => {
            expect(getOuterBorderKey).toBeFunction();
        });
        it("should return the correct border key", () => {
            expect(getOuterBorderKey(false, true)).toBe("t");
            expect(getOuterBorderKey(false, false)).toBe("b");
            expect(getOuterBorderKey(true, true)).toBe("l");
            expect(getOuterBorderKey(true, false)).toBe("r");
        });
    });

    describe("function getInnerBorderKey", () => {
        const { getInnerBorderKey } = sheetutils;
        it("should exist", () => {
            expect(getInnerBorderKey).toBeFunction();
        });
        it("should return the correct border key", () => {
            expect(getInnerBorderKey(false)).toBe("h");
            expect(getInnerBorderKey(true)).toBe("v");
        });
    });

    describe("function getBorderName", () => {
        const { getBorderName } = sheetutils;
        it("should exist", () => {
            expect(getBorderName).toBeFunction();
        });
        it("should return the correct border name", () => {
            expect(getBorderName("t")).toBe("borderTop");
            expect(getBorderName("b")).toBe("borderBottom");
            expect(getBorderName("l")).toBe("borderLeft");
            expect(getBorderName("r")).toBe("borderRight");
            expect(getBorderName("d")).toBe("borderDown");
            expect(getBorderName("u")).toBe("borderUp");
            expect(getBorderName("h")).toBe("borderInsideHor");
            expect(getBorderName("v")).toBe("borderInsideVert");
        });
    });

    describe("function getOuterBorderName", () => {
        const { getOuterBorderName } = sheetutils;
        it("should exist", () => {
            expect(getOuterBorderName).toBeFunction();
        });
        it("should return the correct border name", () => {
            expect(getOuterBorderName(false, true)).toBe("borderTop");
            expect(getOuterBorderName(false, false)).toBe("borderBottom");
            expect(getOuterBorderName(true, true)).toBe("borderLeft");
            expect(getOuterBorderName(true, false)).toBe("borderRight");
        });
    });

    describe("function getInnerBorderName", () => {
        const { getInnerBorderName } = sheetutils;
        it("should exist", () => {
            expect(getInnerBorderName).toBeFunction();
        });
        it("should return the correct border name", () => {
            expect(getInnerBorderName(false)).toBe("borderInsideHor");
            expect(getInnerBorderName(true)).toBe("borderInsideVert");
        });
    });

    describe("function getPresetStyleForBorder", () => {
        const { getPresetStyleForBorder } = sheetutils;
        it("should exist", () => {
            expect(getPresetStyleForBorder).toBeFunction();
        });
        it('should return "none" if the style attribute is "none"', () => {
            expect(getPresetStyleForBorder({ style: "none" })).toBe("none");
            expect(getPresetStyleForBorder({ style: "none", width: THICK })).toBe("none");
        });
        it('should return "null" if the given attributes are ambiguous', () => {
            expect(getPresetStyleForBorder({ style: "dashed", width: 0 })).toBeNull();
            expect(getPresetStyleForBorder({ width: 2 })).toBeNull();
        });
        it("should return the correct preset style", () => {
            expect(getPresetStyleForBorder({ style: "dashed", width: HAIR })).toBe("dashed:hair");
            expect(getPresetStyleForBorder({ style: "solid", width: THIN })).toBe("solid:thin");
            expect(getPresetStyleForBorder({ style: "dotted", width: MEDIUM })).toBe("dotted:medium");
            expect(getPresetStyleForBorder({ style: "dashed", width: THICK })).toBe("dashed:thick");
        });
    });

    describe("function getBorderForPresetStyle", () => {
        const { getBorderForPresetStyle } = sheetutils;
        it("should exist", () => {
            expect(getBorderForPresetStyle).toBeFunction();
        });
        it("should return object with correct border attributes", () => {
            expect(getBorderForPresetStyle("dashed:hair")).toEqual({ style: "dashed", width: HAIR });
            expect(getBorderForPresetStyle("solid:thin")).toEqual({ style: "solid", width: THIN });
            expect(getBorderForPresetStyle("dotted:medium")).toEqual({ style: "dotted", width: MEDIUM });
            expect(getBorderForPresetStyle("dashed:thick")).toEqual({ style: "dashed", width: THICK });
        });
        it('should return "empty" border object', () => {
            expect(getBorderForPresetStyle("none")).toEqual({ style: "none", width: 0 });
        });
        it('should return "empty" border object (unknown preset border)', () => {
            expect(getBorderForPresetStyle("notExist")).toEqual({ style: "none", width: 0 });
        });
    });

    describe("function getTextPadding", () => {
        const { getTextPadding } = sheetutils;
        it("should exist", () => {
            expect(getTextPadding).toBeFunction();
        });
        it("should return text padding according to digit width", () => {
            expect(getTextPadding(4)).toBe(1);
            expect(getTextPadding(5)).toBe(2);
            expect(getTextPadding(6)).toBe(2);
            expect(getTextPadding(7)).toBe(2);
            expect(getTextPadding(8)).toBe(2);
            expect(getTextPadding(9)).toBe(3);
        });
    });

    describe("function getTotalCellPadding", () => {
        const { getTotalCellPadding } = sheetutils;
        it("should exist", () => {
            expect(getTotalCellPadding).toBeFunction();
        });
        it("should return text padding according to zoom factor", () => {
            expect(getTotalCellPadding(4)).toBe(3);
            expect(getTotalCellPadding(5)).toBe(5);
            expect(getTotalCellPadding(6)).toBe(5);
            expect(getTotalCellPadding(7)).toBe(5);
            expect(getTotalCellPadding(8)).toBe(5);
            expect(getTotalCellPadding(9)).toBe(7);
        });
    });

    describe("function isVerticalDir", () => {
        const { isVerticalDir } = sheetutils;
        it("should exist", () => {
            expect(isVerticalDir).toBeFunction();
        });
        it("should return correct flag", () => {
            expect(isVerticalDir(Direction.LEFT)).toBeFalse();
            expect(isVerticalDir(Direction.RIGHT)).toBeFalse();
            expect(isVerticalDir(Direction.UP)).toBeTrue();
            expect(isVerticalDir(Direction.DOWN)).toBeTrue();
        });
    });

    describe("function isLeadingDir", () => {
        const { isLeadingDir } = sheetutils;
        it("should exist", () => {
            expect(isLeadingDir).toBeFunction();
        });
        it("should return correct flag", () => {
            expect(isLeadingDir(Direction.LEFT)).toBeTrue();
            expect(isLeadingDir(Direction.RIGHT)).toBeFalse();
            expect(isLeadingDir(Direction.UP)).toBeTrue();
            expect(isLeadingDir(Direction.DOWN)).toBeFalse();
        });
    });

    describe("function getReverseDirection", () => {
        const { getReverseDirection } = sheetutils;
        it("should exist", () => {
            expect(getReverseDirection).toBeFunction();
        });
        it("should return correct directions", () => {
            expect(getReverseDirection(Direction.LEFT)).toBe(Direction.RIGHT);
            expect(getReverseDirection(Direction.RIGHT)).toBe(Direction.LEFT);
            expect(getReverseDirection(Direction.UP)).toBe(Direction.DOWN);
            expect(getReverseDirection(Direction.DOWN)).toBe(Direction.UP);
        });
    });

    describe("function getAdjacentRange", () => {
        const { getAdjacentRange } = sheetutils;
        it("should exist", () => {
            expect(getAdjacentRange).toBeFunction();
        });
        it("should return correct range", () => {
            expect(getAdjacentRange(r("D4:F6"), Direction.LEFT, 2)).toStringifyTo("B4:C6");
            expect(getAdjacentRange(r("D4:F6"), Direction.RIGHT, 2)).toStringifyTo("G4:H6");
            expect(getAdjacentRange(r("D4:F6"), Direction.UP, 2)).toStringifyTo("D2:F3");
            expect(getAdjacentRange(r("D4:F6"), Direction.DOWN, 2)).toStringifyTo("D7:F8");
        });
    });

    describe("function isOriginCell", () => {
        const { isOriginCell } = sheetutils;
        it("should exist", () => {
            expect(isOriginCell).toBeFunction();
        });
        it("should return results for merge mode MERGE", () => {
            expect(isOriginCell(a("B2"), r("B2:C3"), MergeMode.MERGE)).toBeTrue();
            expect(isOriginCell(a("B3"), r("B2:C3"), MergeMode.MERGE)).toBeFalse();
            expect(isOriginCell(a("C2"), r("B2:C3"), MergeMode.MERGE)).toBeFalse();
            expect(isOriginCell(a("C3"), r("B2:C3"), MergeMode.MERGE)).toBeFalse();
        });
        it("should return results for merge mode HORIZONTAL", () => {
            expect(isOriginCell(a("B2"), r("B2:C3"), MergeMode.HORIZONTAL)).toBeTrue();
            expect(isOriginCell(a("B3"), r("B2:C3"), MergeMode.HORIZONTAL)).toBeTrue();
            expect(isOriginCell(a("C2"), r("B2:C3"), MergeMode.HORIZONTAL)).toBeFalse();
            expect(isOriginCell(a("C3"), r("B2:C3"), MergeMode.HORIZONTAL)).toBeFalse();
        });
        it("should return results for merge mode VERTICAL", () => {
            expect(isOriginCell(a("B2"), r("B2:C3"), MergeMode.VERTICAL)).toBeTrue();
            expect(isOriginCell(a("B3"), r("B2:C3"), MergeMode.VERTICAL)).toBeFalse();
            expect(isOriginCell(a("C2"), r("B2:C3"), MergeMode.VERTICAL)).toBeTrue();
            expect(isOriginCell(a("C3"), r("B2:C3"), MergeMode.VERTICAL)).toBeFalse();
        });
        it("should return results for merge mode UNMERGE", () => {
            expect(isOriginCell(a("B2"), r("B2:C3"), MergeMode.UNMERGE)).toBeFalse();
            expect(isOriginCell(a("B3"), r("B2:C3"), MergeMode.UNMERGE)).toBeFalse();
            expect(isOriginCell(a("C2"), r("B2:C3"), MergeMode.UNMERGE)).toBeFalse();
            expect(isOriginCell(a("C3"), r("B2:C3"), MergeMode.UNMERGE)).toBeFalse();
        });
    });
});
