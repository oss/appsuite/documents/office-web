/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Direction } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import * as paneutils from "@/io.ox/office/spreadsheet/utils/paneutils";

// tests ======================================================================

describe("module spreadsheet/utils/paneutils", () => {

    // types ------------------------------------------------------------------

    const { ColPaneSide } = paneutils;
    describe("enum ColPaneSide", () => {
        it("should exist", () => {
            expect(ColPaneSide).toBeObject();
            expect(ColPaneSide).toHaveProperty("L");
            expect(ColPaneSide).toHaveProperty("R");
        });
    });

    const { RowPaneSide } = paneutils;
    describe("enum RowPaneSide", () => {
        it("should exist", () => {
            expect(RowPaneSide).toBeObject();
            expect(RowPaneSide).toHaveProperty("T");
            expect(RowPaneSide).toHaveProperty("B");
        });
    });

    const { PaneSide } = paneutils;
    describe("enum PaneSide", () => {
        it("should exist", () => {
            expect(PaneSide).toBeObject();
            expect(PaneSide).toHaveProperty("L");
            expect(PaneSide).toHaveProperty("R");
            expect(PaneSide).toHaveProperty("T");
            expect(PaneSide).toHaveProperty("B");
        });
    });

    const { PanePos } = paneutils;
    describe("enum PanePos", () => {
        it("should exist", () => {
            expect(PanePos).toBeObject();
            expect(PanePos).toHaveProperty("TL");
            expect(PanePos).toHaveProperty("TR");
            expect(PanePos).toHaveProperty("BL");
            expect(PanePos).toHaveProperty("BR");
        });
    });

    const { SelectionMode } = paneutils;
    describe("enum SelectionMode", () => {
        it("should exist", () => {
            expect(SelectionMode).toBeObject();
            expect(SelectionMode).toHaveProperty("SELECT");
            expect(SelectionMode).toHaveProperty("APPEND");
            expect(SelectionMode).toHaveProperty("EXTEND");
        });
    });

    // constants --------------------------------------------------------------

    it("constants should exist", () => {
        expect(paneutils.ALL_PANE_SIDES).toBeArrayOfSize(4);
        expect(paneutils.ALL_PANE_POSITIONS).toBeArrayOfSize(4);
        expect(paneutils.MIN_PANE_SIZE).toBeIntegerInInterval(0, 1e9);
        expect(paneutils.ADDITIONAL_SIZE_RATIO).toBeInInterval(0, 10);
    });

    // functions --------------------------------------------------------------

    describe("function isColumnSide", () => {
        const { isColumnSide } = paneutils;
        it("should exist", () => {
            expect(isColumnSide).toBeFunction();
        });
        it("should return true for left/right", () => {
            expect(isColumnSide(PaneSide.L)).toBeTrue();
            expect(isColumnSide(PaneSide.R)).toBeTrue();
        });
        it("should return false for top/botttom", () => {
            expect(isColumnSide(PaneSide.T)).toBeFalse();
            expect(isColumnSide(PaneSide.B)).toBeFalse();
        });
    });

    describe("function isLeadingSide", () => {
        const { isLeadingSide } = paneutils;
        it("should exist", () => {
            expect(isLeadingSide).toBeFunction();
        });
        it("should return true for left/top", () => {
            expect(isLeadingSide(PaneSide.L)).toBeTrue();
            expect(isLeadingSide(PaneSide.T)).toBeTrue();
        });
        it("should return false for right/bottom", () => {
            expect(isLeadingSide(PaneSide.R)).toBeFalse();
            expect(isLeadingSide(PaneSide.B)).toBeFalse();
        });
    });

    describe("function getScrollAnchorName", () => {
        const { getScrollAnchorName } = paneutils;
        it("should exist", () => {
            expect(getScrollAnchorName).toBeFunction();
        });
        it("should return the correct anchor name", () => {
            expect(getScrollAnchorName(PaneSide.L)).toBe("anchorLeft");
            expect(getScrollAnchorName(PaneSide.R)).toBe("anchorRight");
            expect(getScrollAnchorName(PaneSide.T)).toBe("anchorTop");
            expect(getScrollAnchorName(PaneSide.B)).toBe("anchorBottom");
        });
    });

    describe("function getColPaneSide", () => {
        const { getColPaneSide } = paneutils;
        it("should exist", () => {
            expect(getColPaneSide).toBeFunction();
        });
        it("should return the correct pane side", () => {
            expect(getColPaneSide(PanePos.TL)).toBe(PaneSide.L);
            expect(getColPaneSide(PanePos.TR)).toBe(PaneSide.R);
            expect(getColPaneSide(PanePos.BL)).toBe(PaneSide.L);
            expect(getColPaneSide(PanePos.BR)).toBe(PaneSide.R);
        });
    });

    describe("function getRowPaneSide", () => {
        const { getRowPaneSide } = paneutils;
        it("should exist", () => {
            expect(getRowPaneSide).toBeFunction();
        });
        it("should return the correct pane side", () => {
            expect(getRowPaneSide(PanePos.TL)).toBe(PaneSide.T);
            expect(getRowPaneSide(PanePos.TR)).toBe(PaneSide.T);
            expect(getRowPaneSide(PanePos.BL)).toBe(PaneSide.B);
            expect(getRowPaneSide(PanePos.BR)).toBe(PaneSide.B);
        });
    });

    describe("function getNextColPanePos", () => {
        const { getNextColPanePos } = paneutils;
        it("should exist", () => {
            expect(getNextColPanePos).toBeFunction();
        });
        it("should return the correct pane position", () => {
            expect(getNextColPanePos(PanePos.TL)).toBe(PanePos.TR);
            expect(getNextColPanePos(PanePos.TR)).toBe(PanePos.TL);
            expect(getNextColPanePos(PanePos.BL)).toBe(PanePos.BR);
            expect(getNextColPanePos(PanePos.BR)).toBe(PanePos.BL);
        });
    });

    describe("function getNextRowPanePos", () => {
        const { getNextRowPanePos } = paneutils;
        it("should exist", () => {
            expect(getNextRowPanePos).toBeFunction();
        });
        it("should return the correct pane position", () => {
            expect(getNextRowPanePos(PanePos.TL)).toBe(PanePos.BL);
            expect(getNextRowPanePos(PanePos.TR)).toBe(PanePos.BR);
            expect(getNextRowPanePos(PanePos.BL)).toBe(PanePos.TL);
            expect(getNextRowPanePos(PanePos.BR)).toBe(PanePos.TR);
        });
    });

    describe("function getPanePos", () => {
        const { getPanePos } = paneutils;
        it("should exist", () => {
            expect(getPanePos).toBeFunction();
        });
        it("should return the correct pane position", () => {
            expect(getPanePos(PaneSide.L, PaneSide.T)).toBe(PanePos.TL);
            expect(getPanePos(PaneSide.L, PaneSide.B)).toBe(PanePos.BL);
            expect(getPanePos(PaneSide.R, PaneSide.T)).toBe(PanePos.TR);
            expect(getPanePos(PaneSide.R, PaneSide.B)).toBe(PanePos.BR);
        });
    });

    describe("function getNextPanePos", () => {
        const { getNextPanePos } = paneutils;
        it("should exist", () => {
            expect(getNextPanePos).toBeFunction();
        });
        it("should return the correct pane position", () => {
            expect(getNextPanePos(PanePos.TL, PaneSide.T)).toBe(PanePos.TL);
            expect(getNextPanePos(PanePos.TL, PaneSide.B)).toBe(PanePos.BL);
            expect(getNextPanePos(PanePos.TL, PaneSide.L)).toBe(PanePos.TL);
            expect(getNextPanePos(PanePos.TL, PaneSide.R)).toBe(PanePos.TR);
            expect(getNextPanePos(PanePos.TR, PaneSide.T)).toBe(PanePos.TR);
            expect(getNextPanePos(PanePos.TR, PaneSide.B)).toBe(PanePos.BR);
            expect(getNextPanePos(PanePos.TR, PaneSide.L)).toBe(PanePos.TL);
            expect(getNextPanePos(PanePos.TR, PaneSide.R)).toBe(PanePos.TR);
            expect(getNextPanePos(PanePos.BL, PaneSide.T)).toBe(PanePos.TL);
            expect(getNextPanePos(PanePos.BL, PaneSide.B)).toBe(PanePos.BL);
            expect(getNextPanePos(PanePos.BL, PaneSide.L)).toBe(PanePos.BL);
            expect(getNextPanePos(PanePos.BL, PaneSide.R)).toBe(PanePos.BR);
            expect(getNextPanePos(PanePos.BR, PaneSide.T)).toBe(PanePos.TR);
            expect(getNextPanePos(PanePos.BR, PaneSide.B)).toBe(PanePos.BR);
            expect(getNextPanePos(PanePos.BR, PaneSide.L)).toBe(PanePos.BL);
            expect(getNextPanePos(PanePos.BR, PaneSide.R)).toBe(PanePos.BR);
        });
    });

    describe("function getSelectionMode", () => {
        const { getSelectionMode } = paneutils;
        it("should exist", () => {
            expect(getSelectionMode).toBeFunction();
        });
        it("should return the correct selection mode", () => {
            expect(getSelectionMode({ type: "keydown", shiftKey: false, ctrlKey: false, metaKey: false })).toBe(SelectionMode.SELECT);
            expect(getSelectionMode({ type: "keydown", shiftKey: true, ctrlKey: false, metaKey: false })).toBe(SelectionMode.EXTEND);
            expect(getSelectionMode({ type: "keydown", shiftKey: false, ctrlKey: true, metaKey: false })).toBe(SelectionMode.APPEND);
            expect(getSelectionMode({ type: "keydown", shiftKey: true, ctrlKey: true, metaKey: false })).toBe(SelectionMode.SELECT);
            expect(getSelectionMode({ type: "keydown", shiftKey: false, ctrlKey: false, metaKey: true })).toBe(SelectionMode.APPEND);
            expect(getSelectionMode({ type: "keydown", shiftKey: true, ctrlKey: false, metaKey: true })).toBe(SelectionMode.SELECT);
            expect(getSelectionMode({ type: "keydown", shiftKey: false, ctrlKey: true, metaKey: true })).toBe(SelectionMode.APPEND);
            expect(getSelectionMode({ type: "keydown", shiftKey: true, ctrlKey: true, metaKey: true })).toBe(SelectionMode.SELECT);
        });
    });

    describe("function getCellMoveDirection", () => {
        const { getCellMoveDirection } = paneutils;
        it("should exist", () => {
            expect(getCellMoveDirection).toBeFunction();
        });
        it("should return the move direction", () => {
            expect(getCellMoveDirection({ type: "keydown", key: "Enter", keyCode: 13, shiftKey: false })).toBe(Direction.DOWN);
            expect(getCellMoveDirection({ type: "keydown", key: "Enter", keyCode: 13, shiftKey: true })).toBe(Direction.UP);
            expect(getCellMoveDirection({ type: "keydown", key: "Tab", keyCode: 9, shiftKey: false })).toBe(Direction.RIGHT);
            expect(getCellMoveDirection({ type: "keydown", key: "Tab", keyCode: 9, shiftKey: true })).toBe(Direction.LEFT);
        });
        it("should return undefined for other keys", () => {
            expect(getCellMoveDirection({ type: "keydown", key: " ", keyCode: 32, shift: false })).toBeUndefined();
            expect(getCellMoveDirection({ type: "keydown", key: "Backspace", keyCode: 8, shiftKey: false })).toBeUndefined();
            expect(getCellMoveDirection({ type: "keydown", key: "ArrowLeft", keyCode: 37, shiftKey: false })).toBeUndefined();
        });
    });
});
