/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ArrayBase } from "@/io.ox/office/spreadsheet/utils/arraybase";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";

import { i, ia, orderedIntervalsMatcher as orderedMatcher, unorderedIntervalsMatcher as unorderedMatcher } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/intervalarray", () => {

    // class IntervalArray ----------------------------------------------------

    describe("class IntervalArray", () => {

        it("should subclass ArrayBase", () => {
            expect(IntervalArray).toBeSubClassOf(ArrayBase);
            expect(IntervalArray).toBeSubClassOf(Array);
        });

        describe("constructor", () => {
            it("should create an interval array", () => {
                const ia1 = new IntervalArray();
                expect(ia1).toBeArrayOfSize(0);
            });
            const i1 = i("1:2"), i2 = i("3:4");
            it("should insert single intervals", () => {
                const ia1 = new IntervalArray(i1);
                expect(ia1).toHaveLength(1);
                expect(ia1[0]).toBe(i1);
                const ia2 = new IntervalArray(i1, i2, null, i1);
                expect(ia2).toHaveLength(3);
                expect(ia2[0]).toBe(i1);
                expect(ia2[1]).toBe(i2);
                expect(ia2[2]).toBe(i1);
            });
            it("should insert plain arrays of intervals", () => {
                const ia1 = new IntervalArray([i1]);
                expect(ia1).toHaveLength(1);
                expect(ia1[0]).toBe(i1);
                const ia2 = new IntervalArray([i1, i2], [], [i1]);
                expect(ia2).toHaveLength(3);
                expect(ia2[0]).toBe(i1);
                expect(ia2[1]).toBe(i2);
                expect(ia2[2]).toBe(i1);
            });
            it("should copy-construct interval arrays", () => {
                const ia0 = new IntervalArray(i1, i2);
                const ia1 = new IntervalArray(ia0);
                expect(ia1).toHaveLength(2);
                expect(ia1[0]).toBe(i1);
                expect(ia1[1]).toBe(i2);
                const ia2 = new IntervalArray(ia0, new IntervalArray(), ia1);
                expect(ia2).toHaveLength(4);
                expect(ia2[0]).toBe(i1);
                expect(ia2[1]).toBe(i2);
                expect(ia2[2]).toBe(i1);
                expect(ia2[3]).toBe(i2);
            });
        });

        describe("static function from", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveStaticMethod("from");
            });
            it("should convert plain arrays", () => {
                const ia1 = [i("1:2"), i("2:3")];
                const ia2 = IntervalArray.from(ia1);
                expect(ia2).toBeInstanceOf(IntervalArray);
                expect(ia2).toHaveLength(ia1.length);
                ia1.forEach((v, idx) => expect(ia2[idx]).toBe(v));
            });
            it("should convert sets", () => {
                const ia1 = [i("1:2"), i("2:3")];
                const ia2 = IntervalArray.from(new Set(ia1));
                expect(ia2).toBeInstanceOf(IntervalArray);
                expect(ia2).toHaveLength(ia1.length);
                ia1.forEach((v, idx) => expect(ia2[idx]).toBe(v));
            });
        });

        describe("static function cast", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveStaticMethod("cast");
            });
            it("should not convert interval arrays", () => {
                const ia1 = ia("1:2 2:3");
                expect(IntervalArray.cast(ia1)).toBe(ia1);
            });
            it("should convert plain arrays", () => {
                const ia1 = [i("1:2"), i("2:3")];
                const ia2 = IntervalArray.cast(ia1);
                expect(ia2).toBeInstanceOf(IntervalArray);
                expect(ia2).toHaveLength(ia1.length);
                ia1.forEach(function (v, idx) { expect(ia2[idx]).toBe(v); });
            });
            it("should convert single object to array", () => {
                const i1 = i("1:2");
                const ia1 = IntervalArray.cast(i1);
                expect(ia1).toBeInstanceOf(IntervalArray);
                expect(ia1).toHaveLength(1);
                expect(ia1[0]).toBe(i1);
            });
            it("should convert nullish to empty arrays", () => {
                const ia1 = IntervalArray.cast(null);
                const ia2 = IntervalArray.cast(undefined);
                expect(ia1).toBeInstanceOf(IntervalArray);
                expect(ia1).toHaveLength(0);
                expect(ia2).toBeInstanceOf(IntervalArray);
                expect(ia2).toHaveLength(0);
            });
        });

        describe("static function filter", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveStaticMethod("filter");
            });
        });

        describe("static function map", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveStaticMethod("map");
            });
        });

        describe("static function merge", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveStaticMethod("merge");
            });
        });

        describe("static function mergeIntervals", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveStaticMethod("mergeIntervals");
            });
            const arr = "6:7 3:4 4:5 1:1".split(" ");
            const ia1 = arr.map(i);
            it("should merge a plain array of intervals", () => {
                expect(IntervalArray.mergeIntervals(ia1)).toEqual(ia("1:1 3:7"));
            });
            it("should merge a set of intervals", () => {
                expect(IntervalArray.mergeIntervals(new Set(ia1))).toEqual(ia("1:1 3:7"));
            });
            it("should merge a value source with map function", () => {
                expect(IntervalArray.mergeIntervals(arr, i)).toEqual(ia("1:1 3:7"));
            });
            it("should accept an empty array", () => {
                expect(IntervalArray.mergeIntervals([])).toBeArrayOfSize(0);
            });
        });

        describe("static function mergeIndexes", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveStaticMethod("mergeIndexes");
            });
            it("should join indexes to intervals", () => {
                expect(IntervalArray.mergeIndexes([1, 3, 4, 2, 1])).toEqual(ia("2:5"));
                expect(IntervalArray.mergeIndexes([5, 3, 5, 1, 2])).toEqual(ia("2:4 6:6"));
            });
            it("should merge a set of indexes", () => {
                expect(IntervalArray.mergeIndexes(new Set([1, 3, 4, 2, 1]))).toEqual(ia("2:5"));
            });
            it("should merge a value source with map function", () => {
                expect(IntervalArray.mergeIndexes("13421", d => parseInt(d, 10))).toEqual(ia("2:5"));
            });
            it("should accept an empty array", () => {
                expect(IntervalArray.mergeIndexes([])).toBeArrayOfSize(0);
            });
        });

        describe("property key", () => {
            it("should return unique key for the array", () => {
                expect(ia("2:4 3:5").key).toBe("1:3 2:4");
                expect(ia("").key).toBe("");
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("clone");
            });
            it("should return a shallow clone", () => {
                const ia1 = ia("2:4 3:5"), ia2 = ia1.clone();
                expect(ia2).toBeInstanceOf(IntervalArray);
                expect(ia2).not.toBe(ia1);
                expect(ia2[0]).toBe(ia1[0]);
                expect(ia2[1]).toBe(ia1[1]);
                expect(ia2).toEqual(ia1);
            });
        });

        describe("method deepClone", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("deepClone");
            });
            it("should return a deep clone", () => {
                const ia1 = ia("2:4 3:5"), ia2 = ia1.deepClone();
                expect(ia2).toBeInstanceOf(IntervalArray);
                expect(ia2).not.toBe(ia1);
                expect(ia2[0]).not.toBe(ia1[0]);
                expect(ia2[1]).not.toBe(ia1[1]);
                expect(ia2).toEqual(ia1);
            });
        });

        describe("method size", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("size");
            });
            it("should count indexes in intervals", () => {
                expect(ia("1:3 2:5 7:7").size()).toBe(8);
                expect(new IntervalArray().size()).toBe(0);
            });
        });

        describe("method indexOf", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("indexOf");
            });
            const ia1 = ia("2:3 3:4 4:5 3:4 2:3");
            it("should return array index of strictly equal interval", () => {
                expect(ia1.indexOf(ia1[0])).toBe(0);
                expect(ia1.indexOf(ia1[1])).toBe(1);
                expect(ia1.indexOf(ia1[2])).toBe(2);
                expect(ia1.indexOf(ia1[3])).toBe(1);
                expect(ia1.indexOf(ia1[4])).toBe(0);
            });
            it("should return array index of deeply equal interval", () => {
                expect(ia1.indexOf(i("2:3"))).toBe(0);
                expect(ia1.indexOf(i("3:4"))).toBe(1);
                expect(ia1.indexOf(i("4:5"))).toBe(2);
                expect(ia1.indexOf(i("5:6"))).toBe(-1);
            });
        });

        describe("method lastIndexOf", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("lastIndexOf");
            });
            const ia1 = ia("2:3 3:4 4:5 3:4 2:3");
            it("should return array index of strictly equal interval", () => {
                expect(ia1.lastIndexOf(ia1[0])).toBe(4);
                expect(ia1.lastIndexOf(ia1[1])).toBe(3);
                expect(ia1.lastIndexOf(ia1[2])).toBe(2);
                expect(ia1.lastIndexOf(ia1[3])).toBe(3);
                expect(ia1.lastIndexOf(ia1[4])).toBe(4);
            });
            it("should return array index of deeply equal interval", () => {
                expect(ia1.lastIndexOf(i("2:3"))).toBe(4);
                expect(ia1.lastIndexOf(i("3:4"))).toBe(3);
                expect(ia1.lastIndexOf(i("4:5"))).toBe(2);
                expect(ia1.lastIndexOf(i("5:6"))).toBe(-1);
            });
        });

        describe("method containsIndex", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("containsIndex");
            });
            const ia1 = ia("2:3 5:7");
            it("should return false for indexes outside the intervals", () => {
                expect(ia1.containsIndex(0)).toBeFalse();
                expect(ia1.containsIndex(3)).toBeFalse();
                expect(ia1.containsIndex(7)).toBeFalse();
                expect(ia1.containsIndex(8)).toBeFalse();
                expect(ia1.containsIndex(9)).toBeFalse();
            });
            it("should return true for indexes inside the intervals", () => {
                expect(ia1.containsIndex(1)).toBeTrue();
                expect(ia1.containsIndex(2)).toBeTrue();
                expect(ia1.containsIndex(4)).toBeTrue();
                expect(ia1.containsIndex(5)).toBeTrue();
                expect(ia1.containsIndex(6)).toBeTrue();
            });
        });

        describe("method contains", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("contains");
            });
            const ia1 = ia("2:3 5:7");
            it("should return false for intervals outside the intervals", () => {
                expect(ia1.contains(ia("1:1 2:3 5:7"))).toBeFalse();
                expect(ia1.contains(ia("2:3 5:7 4:4"))).toBeFalse();
                expect(ia1.contains(ia("8:9 2:3 5:7"))).toBeFalse();
            });
            it("should return false for overlapping intervals", () => {
                expect(ia1.contains(ia("1:3 5:7"))).toBeFalse();
                expect(ia1.contains(ia("2:4 5:7"))).toBeFalse();
                expect(ia1.contains(ia("2:3 4:7"))).toBeFalse();
                expect(ia1.contains(ia("2:3 5:8"))).toBeFalse();
            });
            it("should return true for contained intervals", () => {
                expect(ia1.contains(ia("5:6 6:7 2:3"))).toBeTrue();
                expect(ia1.contains(ia1)).toBeTrue();
            });
            it("should accept empty array", () => {
                const ia0 = new IntervalArray();
                expect(ia0.contains(ia1)).toBeFalse();
                expect(ia1.contains(ia0)).toBeTrue();
                expect(ia0.contains(ia0)).toBeTrue();
            });
            it("should accept single interval", () => {
                expect(ia1.contains(i("3:5"))).toBeFalse();
                expect(ia1.contains(i("2:3"))).toBeTrue();
                expect(ia1.contains(i("6:7"))).toBeTrue();
            });
        });

        describe("method overlaps", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("overlaps");
            });
            const ia1 = ia("2:3 5:7");
            it("should return false for distinct intervals", () => {
                expect(ia1.overlaps(ia("1:1 4:4 8:9"))).toBeFalse();
            });
            it("should return true for overlapping intervals", () => {
                expect(ia1.overlaps(ia("1:1 4:5 8:9"))).toBeTrue();
                expect(ia1.overlaps(ia("4:4 1:2 8:9"))).toBeTrue();
                expect(ia1.overlaps(ia("1:1 4:4 7:9"))).toBeTrue();
            });
            it("should accept empty array", () => {
                const ia0 = new IntervalArray();
                expect(ia0.overlaps(ia)).toBeFalse();
                expect(ia1.overlaps(ia0)).toBeFalse();
                expect(ia0.overlaps(ia0)).toBeFalse();
            });
            it("should accept single interval", () => {
                expect(ia1.overlaps(i("4:4"))).toBeFalse();
                expect(ia1.overlaps(i("7:9"))).toBeTrue();
            });
        });

        describe("method distinct", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("distinct");
            });
            it("should return true for distinct intervals", () => {
                expect(ia("5:7").distinct()).toBeTrue();
                expect(ia("5:7 2:3").distinct()).toBeTrue();
                expect(ia("5:7 2:3 4:4").distinct()).toBeTrue();
                expect(ia("5:7 2:3 4:4 1:1").distinct()).toBeTrue();
            });
            it("should return false for overlapping intervals", () => {
                expect(ia("1:2 2:3").distinct()).toBeFalse();
                expect(ia("1:2 3:4 2:3").distinct()).toBeFalse();
                expect(ia("5:7 2:3 4:4 1:2").distinct()).toBeFalse();
                expect(ia("5:7 2:3 4:5 1:1").distinct()).toBeFalse();
                expect(ia("5:7 1:3 4:4 1:1").distinct()).toBeFalse();
                expect(ia("4:7 2:3 4:4 1:1").distinct()).toBeFalse();
            });
            it("should accept empty array", () => {
                expect(new IntervalArray().distinct()).toBeTrue();
            });
        });

        describe("method boundary", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("boundary");
            });
            it("should return the bounding interval", () => {
                expect(ia("1:3 5:7").boundary()).toEqual(i("1:7"));
                expect(ia("3:7 1:5").boundary()).toEqual(i("1:7"));
            });
            it("should return a clone of a single interval", () => {
                const ia1 = ia("1:3"), i1 = ia1.boundary();
                expect(i1).toEqual(ia1[0]);
                expect(i1).not.toBe(ia1[0]);
            });
            it("should return undefined for an empty array", () => {
                expect(new IntervalArray().boundary()).toBeUndefined();
            });
        });

        describe("method unify", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("unify");
            });
            it("should return a shallow copy", () => {
                const ia1 = ia("1:2 4:5"), ia2 = ia1.unify();
                expect(ia2).not.toBe(ia1);
                expect(ia2[0]).toBe(ia1[0]);
                expect(ia2[1]).toBe(ia1[1]);
            });
            it("should remove all duplicates", () => {
                const ia1 = ia("2:3 3:4 2:3 2:4 3:4 2:4"), ia2 = ia1.unify();
                expect(ia2).toHaveLength(3);
                expect(ia2[0]).toBe(ia1[0]);
                expect(ia2[1]).toBe(ia1[1]);
                expect(ia2[2]).toBe(ia1[3]);
            });
            it("should accept empty array", () => {
                expect(new IntervalArray().unify()).toBeArrayOfSize(0);
            });
        });

        describe("method merge", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("merge");
            });
            it("should return a new array", () => {
                const ia1 = ia("1:2 4:5"), ia2 = ia1.merge();
                expect(ia2).toEqual(ia1);
                expect(ia2).not.toBe(ia1);
            });
            it("should sort but not merge distinct intervals", () => {
                expect(ia("5:6 2:3 8:9").merge()).toEqual(ia("2:3 5:6 8:9"));
            });
            it("should merge adjacent intervals", () => {
                expect(ia("4:5 2:3 6:7 8:9").merge()).toEqual(ia("2:9"));
            });
            it("should merge overlapping intervals", () => {
                expect(ia("4:7 2:4 4:7 7:9").merge()).toEqual(ia("2:9"));
            });
            it("should accept empty array", () => {
                expect(new IntervalArray().merge()).toBeArrayOfSize(0);
            });
        });

        describe("method partition", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("partition");
            });
            it("should not partition distinct intervals", () => {
                expect(ia("7:8 1:2 4:5").partition()).toSatisfy(orderedMatcher("1:2 4:5 7:8"));
            });
            it("should not partition adjacent ranges", () => {
                expect(ia("1:2 3:4").partition()).toSatisfy(orderedMatcher("1:2 3:4"));
                expect(ia("3:4 1:2").partition()).toSatisfy(orderedMatcher("1:2 3:4"));
            });
            it("should partition overlapping intervals", () => {
                expect(ia("1:4 3:6").partition()).toSatisfy(orderedMatcher("1:2 3:4 5:6"));
                expect(ia("1:3 5:7 3:5").partition()).toSatisfy(orderedMatcher("1:2 3:3 4:4 5:5 6:7"));
                expect(ia("3:4 1:6 3:4").partition()).toSatisfy(orderedMatcher("1:2 3:4 5:6"));
            });
            it("should contain the original intervals", () => {
                const intervals = ia("1:4 3:6");
                const partition = intervals.partition();
                expect(partition[0].coveredBy).toSatisfy(unorderedMatcher("1:4"));
                expect(partition[1].coveredBy).toSatisfy(unorderedMatcher("1:4 3:6"));
                expect(partition[2].coveredBy).toSatisfy(unorderedMatcher("3:6"));
            });
            it("should clone all intervals", () => {
                const i1 = i("3:5"), intervals = new IntervalArray(i1).partition();
                expect(intervals).toSatisfy(orderedMatcher("3:5"));
                expect(intervals[0]).not.toBe(i1);
            });
            it("should accept empty array", () => {
                const ia1 = new IntervalArray(), ia2 = ia1.partition();
                expect(ia2).toHaveLength(0);
                expect(ia2).not.toBe(ia1);
            });
        });

        describe("method intersect", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("intersect");
            });
            it("should return empty array for distinct intervals", () => {
                expect(ia("1:2 5:6 9:10").intersect(ia("3:4 7:8"))).toHaveLength(0);
                expect(ia("1:2 5:6 9:10").intersect(i("3:4"))).toHaveLength(0);
            });
            it("should accept empty arrays", () => {
                expect(new IntervalArray().intersect(ia("3:4"))).toHaveLength(0);
                expect(ia("3:4").intersect(new IntervalArray())).toHaveLength(0);
            });
            it("should return intersection intervals", () => {
                const ia1 = ia("1:3 2:4 3:5 4:6 5:7");
                expect(ia1.intersect(ia("2:4 4:6"))).toEqual(ia("2:3 2:4 4:4 3:4 4:5 4:4 4:6 5:6"));
                expect(ia1.intersect(i("2:4"))).toEqual(ia("2:3 2:4 3:4 4:4"));
            });
        });

        describe("method difference", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("difference");
            });
            it("should return remaining intervals", () => {
                expect(ia("2:4 6:8 10:12 14:16").difference(ia("4:6 11:11 14:16"))).toEqual(ia("2:3 7:8 10:10 12:12"));
                expect(ia("2:6 4:10").difference(ia("5:5"))).toEqual(ia("2:4 6:6 4:4 6:10"));
                expect(ia("2:6 4:10").difference(ia("9:9 5:6"))).toEqual(ia("2:4 4:4 7:8 10:10"));
            });
            it("should accept empty array", () => {
                const ia0 = new IntervalArray(), ia1 = ia("4:6"), ia2 = ia1.difference(ia0);
                expect(ia2).toEqual(ia1);
                expect(ia2).not.toBe(ia1);
                expect(ia0.difference(ia1)).toHaveLength(0);
                expect(ia0.difference(ia0)).toHaveLength(0);
                expect(ia0.difference(ia0)).not.toBe(ia0);
            });
            it("should accept single interval object instead of array", () => {
                expect(ia("2:6 4:10").difference(i("5:5"))).toEqual(ia("2:4 6:6 4:4 6:10"));
            });
        });

        describe("method move", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("move");
            });
            it("should move the indexes in the intervals", () => {
                expect(ia("5:6 7:8").move(-3)).toEqual(ia("2:3 4:5"));
                expect(ia("5:6 7:8").move(-2)).toEqual(ia("3:4 5:6"));
                expect(ia("5:6 7:8").move(-1)).toEqual(ia("4:5 6:7"));
                expect(ia("5:6 7:8").move(0)).toEqual(ia("5:6 7:8"));
                expect(ia("5:6 7:8").move(1)).toEqual(ia("6:7 8:9"));
                expect(ia("5:6 7:8").move(2)).toEqual(ia("7:8 9:10"));
                expect(ia("5:6 7:8").move(3)).toEqual(ia("8:9 10:11"));
            });
            it("should return itself", () => {
                const ia1 = ia("5:6 7:8");
                expect(ia1.move(1)).toBe(ia1);
            });
        });

        describe("method indexes", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("indexes");
            });
            it("should visit all indexes in an interval array", () => {
                const ia1 = ia("5:7 7:9"), it = ia1.indexes();
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([4, 5, 6, 6, 7, 8]);
            });
            it("should visit all indexes in an interval array in reversed order", () => {
                const ia1 = ia("5:7 7:9"), it = ia1.indexes({ reverse: true });
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([8, 7, 6, 6, 5, 4]);
            });
        });

        describe("method toOpStrCols", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("toOpStrCols");
            });
            const ia0 = new IntervalArray(), ia1 = ia("A:B D:D");
            it("should stringify to column intervals", () => {
                expect(ia1.toOpStrCols()).toBe("A:B D");
            });
            it("should return empty string for an empty array", () => {
                expect(ia0.toOpStrCols()).toBe("");
            });
        });

        describe("method toOpStrRows", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("toOpStrRows");
            });
            const ia0 = new IntervalArray(), ia1 = ia("1:2 4:4");
            it("should stringify to row intervals", () => {
                expect(ia1.toOpStrRows()).toBe("1:2 4");
            });
            it("should return empty string for an empty array", () => {
                expect(ia0.toOpStrRows()).toBe("");
            });
        });

        describe("method toOpStr", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("toOpStr");
            });
            const ia0 = new IntervalArray(), ia1 = ia("A:B D:D");
            it("should stringify to column intervals", () => {
                expect(ia1.toOpStr(true)).toBe("A:B D");
                expect(ia0.toOpStr(true)).toBe("");
            });
            it("should stringify to row intervals", () => {
                expect(ia1.toOpStr(false)).toBe("1:2 4");
                expect(ia0.toOpStr(false)).toBe("");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(IntervalArray).toHaveMethod("toString");
            });
            const ia1 = ia("1:2 3:4");
            it("should stringify to row interval", () => {
                expect(ia1.toString()).toBe("1:2,3:4");
            });
            it("should use the separator string", () => {
                expect(ia1.toString(" + ")).toBe("1:2 + 3:4");
            });
            it("should stringify implicitly", () => {
                expect("<" + ia1 + ">").toBe("<1:2,3:4>");
            });
        });
    });
});
