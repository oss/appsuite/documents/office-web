/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { MIN_NORM_NUMBER } from "@/io.ox/office/tk/algorithms/math";
import * as scalar from "@/io.ox/office/spreadsheet/utils/scalar";

const { ScalarTypeId, CompareNullMode, CompareResult, ErrorLiteral, ErrorCode } = scalar;
const { LESS, EQUAL, GREATER } = CompareResult;

// constants ==================================================================

const OPT_CONVERT  = { nullMode: CompareNullMode.CONVERT };
const OPT_NULL_MIN = { nullMode: CompareNullMode.NULL_MIN };
const OPT_NULL_MAX = { nullMode: CompareNullMode.NULL_MAX };

// tests ======================================================================

describe("module spreadsheet/utils/scalar", () => {

    // types ------------------------------------------------------------------

    describe("enum ScalarTypeId", () => {
        it("should exist", () => {
            expect(ScalarTypeId).toBeObject();
            expect(ScalarTypeId).toHaveProperty("NULL", 1);
            expect(ScalarTypeId).toHaveProperty("NUMBER", 2);
            expect(ScalarTypeId).toHaveProperty("STRING", 3);
            expect(ScalarTypeId).toHaveProperty("BOOLEAN", 4);
            expect(ScalarTypeId).toHaveProperty("ERROR", 5);
            expect(ScalarTypeId).toHaveProperty("NULL_MAX", 6);
        });
    });

    describe("enum CompareNullMode", () => {
        it("should exist", () => {
            expect(CompareNullMode).toBeObject();
            expect(CompareNullMode).toHaveProperty("CONVERT", 0);
            expect(CompareNullMode).toHaveProperty("NULL_MIN", 1);
            expect(CompareNullMode).toHaveProperty("NULL_MAX", 2);
        });
    });

    describe("enum CompareResult", () => {
        it("should exist", () => {
            expect(CompareResult).toBeObject();
            expect(CompareResult).toHaveProperty("LESS", -1);
            expect(CompareResult).toHaveProperty("EQUAL", 0);
            expect(CompareResult).toHaveProperty("GREATER", 1);
            expect(CompareResult).toHaveProperty("NA");
            expect(CompareResult.NA).toBeNaN();
        });
    });

    // class ErrorLiteral -----------------------------------------------------

    describe("class ErrorLiteral", () => {
        it("should exist", () => {
            expect(ErrorLiteral).toBeFunction();
        });

        describe("constructor", () => {
            it("should take key", () => {
                const err = new ErrorLiteral("TEST");
                expect(err.key).toBe("TEST");
                expect(err.num).toBeNaN();
            });
            it("should take key and number", () => {
                const err = new ErrorLiteral("TEST", 1);
                expect(err.key).toBe("TEST");
                expect(err.num).toBe(1);
            });
        });

        describe("method throw", () => {
            it("should exist", () => {
                expect(ErrorLiteral).toHaveMethod("throw");
            });
            it("should throw itself", () => {
                const err = new ErrorLiteral("TEST");
                expect(() => err.throw()).toThrowValue(err);
            });
        });

        describe("method toJSON", () => {
            const err = new ErrorLiteral("TEST");
            it("should exist", () => {
                expect(ErrorLiteral).toHaveMethod("toJSON");
            });
            it("should convert error code to the string representation", () => {
                expect(JSON.stringify(err)).toBe('"#TEST"');
            });
        });

        describe("method toString", () => {
            const err = new ErrorLiteral("TEST");
            it("should exist", () => {
                expect(ErrorLiteral).toHaveMethod("toString");
            });
            it("should convert error code to the string representation", () => {
                expect(err.toString()).toBe("#TEST");
                expect("<" + err + ">").toBe("<#TEST>");
            });
        });
    });

    describe("enum ErrorCode", () => {
        it("should exist", () => {
            expect(ErrorCode).toBeObject();
        });
        it("should provide built-in error code #NULL!", () => {
            expect(ErrorCode.NULL).toBeInstanceOf(ErrorLiteral);
            expect(ErrorCode.NULL.key).toBe("NULL");
            expect(ErrorCode.NULL.num).toBe(1);
        });
        it("should provide built-in error code #DIV/0!", () => {
            expect(ErrorCode.DIV0).toBeInstanceOf(ErrorLiteral);
            expect(ErrorCode.DIV0.key).toBe("DIV0");
            expect(ErrorCode.DIV0.num).toBe(2);
        });
        it("should provide built-in error code #VALUE!", () => {
            expect(ErrorCode.VALUE).toBeInstanceOf(ErrorLiteral);
            expect(ErrorCode.VALUE.key).toBe("VALUE");
            expect(ErrorCode.VALUE.num).toBe(3);
        });
        it("should provide built-in error code #REF!", () => {
            expect(ErrorCode.REF).toBeInstanceOf(ErrorLiteral);
            expect(ErrorCode.REF.key).toBe("REF");
            expect(ErrorCode.REF.num).toBe(4);
        });
        it("should provide built-in error code #NAME?", () => {
            expect(ErrorCode.NAME).toBeInstanceOf(ErrorLiteral);
            expect(ErrorCode.NAME.key).toBe("NAME");
            expect(ErrorCode.NAME.num).toBe(5);
        });
        it("should provide built-in error code #NUM!", () => {
            expect(ErrorCode.NUM).toBeInstanceOf(ErrorLiteral);
            expect(ErrorCode.NUM.key).toBe("NUM");
            expect(ErrorCode.NUM.num).toBe(6);
        });
        it("should provide built-in error code #N/A", () => {
            expect(ErrorCode.NA).toBeInstanceOf(ErrorLiteral);
            expect(ErrorCode.NA.key).toBe("NA");
            expect(ErrorCode.NA.num).toBe(7);
        });
    });

    // functions --------------------------------------------------------------

    describe("function isErrorCode", () => {
        const { isErrorCode } = scalar;
        it("should exist", () => {
            expect(isErrorCode).toBeFunction();
        });
        it("should return true for error codes", () => {
            expect(isErrorCode(ErrorCode.NUM)).toBeTrue();
            expect(isErrorCode(ErrorCode.DIV0)).toBeTrue();
        });
        it("should return false for other values", () => {
            expect(isErrorCode(0)).toBeFalse();
            expect(isErrorCode(42)).toBeFalse();
            expect(isErrorCode("")).toBeFalse();
            expect(isErrorCode("abc")).toBeFalse();
            expect(isErrorCode("#NULL!")).toBeFalse();
            expect(isErrorCode(false)).toBeFalse();
            expect(isErrorCode(true)).toBeFalse();
            expect(isErrorCode(null)).toBeFalse();
            expect(isErrorCode({})).toBeFalse();
            expect(isErrorCode({ key: "NULL" })).toBeFalse();
        });
    });

    describe("function throwErrorCode", () => {
        const { throwErrorCode } = scalar;
        it("should exist", () => {
            expect(throwErrorCode).toBeFunction();
        });
        it("should return scalar values", () => {
            expect(throwErrorCode(42)).toBe(42);
            expect(throwErrorCode("abc")).toBe("abc");
            expect(throwErrorCode(true)).toBeTrue();
            expect(throwErrorCode(new Date())).toBeDate();
            expect(throwErrorCode(null)).toBeNull();
        });
        it("should throw error codes", () => {
            expect(() => throwErrorCode(ErrorCode.NULL)).toThrowValue(ErrorCode.NULL);
            expect(() => throwErrorCode(ErrorCode.VALUE)).toThrowValue(ErrorCode.VALUE);
        });
    });

    describe("function getScalarType", () => {
        const { getScalarType } = scalar;
        it("should exist", () => {
            expect(getScalarType).toBeFunction();
        });
        it("should recognize numbers", () => {
            expect(getScalarType(0)).toBe(ScalarTypeId.NUMBER);
            expect(getScalarType(3)).toBe(ScalarTypeId.NUMBER);
            expect(getScalarType(-3)).toBe(ScalarTypeId.NUMBER);
            expect(getScalarType(1 / 0)).toBe(ScalarTypeId.NUMBER);
            expect(getScalarType(Number.NaN)).toBe(ScalarTypeId.NUMBER);
        });
        it("should recognize strings", () => {
            expect(getScalarType("")).toBe(ScalarTypeId.STRING);
            expect(getScalarType("abc")).toBe(ScalarTypeId.STRING);
            expect(getScalarType("ABC")).toBe(ScalarTypeId.STRING);
        });
        it("should recognize booleans", () => {
            expect(getScalarType(false)).toBe(ScalarTypeId.BOOLEAN);
            expect(getScalarType(true)).toBe(ScalarTypeId.BOOLEAN);
        });
        it("should recognize error codes", () => {
            expect(getScalarType(ErrorCode.DIV0)).toBe(ScalarTypeId.ERROR);
            expect(getScalarType(ErrorCode.NULL)).toBe(ScalarTypeId.ERROR);
        });
        it("should recognize null", () => {
            expect(getScalarType(null)).toBe(ScalarTypeId.NULL);
            expect(getScalarType(null, false)).toBe(ScalarTypeId.NULL);
            expect(getScalarType(null, true)).toBe(ScalarTypeId.NULL_MAX);
        });
    });

    describe("function getCellValue", () => {
        const { getCellValue } = scalar;
        it("should exist", () => {
            expect(getCellValue).toBeFunction();
        });
        it("should not convert regular scalar values", () => {
            expect(getCellValue(0)).toBe(0);
            expect(getCellValue(1)).toBe(1);
            expect(getCellValue("")).toBe("");
            expect(getCellValue("a")).toBe("a");
            expect(getCellValue(false)).toBeFalse();
            expect(getCellValue(true)).toBeTrue();
            expect(getCellValue(ErrorCode.NULL)).toBe(ErrorCode.NULL);
        });
        it("should convert null to zero", () => {
            expect(getCellValue(null)).toBe(0);
        });
    });

    describe("function equalNumbers", () => {
        const { equalNumbers } = scalar;
        it("should exist", () => {
            expect(equalNumbers).toBeFunction();
        });
        it("should return true, if the numbers are equal", () => {
            expect(equalNumbers(-3, -3)).toBeTrue();
            expect(equalNumbers(0, 0)).toBeTrue();
            expect(equalNumbers(3, 3)).toBeTrue();
            expect(equalNumbers(0, MIN_NORM_NUMBER / 2)).toBeTrue();
            expect(equalNumbers(1, 1 + 2 * MIN_NORM_NUMBER)).toBeTrue();
            expect(equalNumbers(100, 100 * (1 + 2 * MIN_NORM_NUMBER))).toBeTrue();
        });
        it("should return false, if the numbers are not equal", () => {
            expect(equalNumbers(-3, 0)).toBeFalse();
            expect(equalNumbers(3, 0)).toBeFalse();
            expect(equalNumbers(0, -3)).toBeFalse();
            expect(equalNumbers(0, 3)).toBeFalse();
            expect(equalNumbers(1, 1 + 1e-14)).toBeFalse();
            expect(equalNumbers(100, 100 + 1e-12)).toBeFalse();
        });
        it("should return false, if either number is not finite", () => {
            expect(equalNumbers(3, 1 / 0)).toBeFalse();
            expect(equalNumbers(1 / 0, 3)).toBeFalse();
            expect(equalNumbers(1 / 0, 1 / 0)).toBeFalse();
            expect(equalNumbers(3, Number.NaN)).toBeFalse();
            expect(equalNumbers(Number.NaN, 3)).toBeFalse();
            expect(equalNumbers(Number.NaN, Number.NaN)).toBeFalse();
        });
    });

    describe("function equalStrings", () => {
        const { equalStrings } = scalar;
        it("should exist", () => {
            expect(equalStrings).toBeFunction();
        });
        it("should return false, if first string is less than second string (ignoring case)", () => {
            expect(equalStrings("a", "b")).toBeFalse();
            expect(equalStrings("A", "b")).toBeFalse();
            expect(equalStrings("a", "B")).toBeFalse();
            expect(equalStrings("A", "B")).toBeFalse();
            expect(equalStrings("a", "aa")).toBeFalse();
            expect(equalStrings("ab", "ba")).toBeFalse();
            expect(equalStrings("", "a")).toBeFalse();
        });
        it("should return true, if the strings are equal (ignoring case)", () => {
            expect(equalStrings("a", "a")).toBeTrue();
            expect(equalStrings("a", "A")).toBeTrue();
            expect(equalStrings("A", "a")).toBeTrue();
            expect(equalStrings("A", "A")).toBeTrue();
            expect(equalStrings("abc", "abc")).toBeTrue();
            expect(equalStrings("", "")).toBeTrue();
        });
        it("should return false, if first string is greater than second string (ignoring case)", () => {
            expect(equalStrings("b", "a")).toBeFalse();
            expect(equalStrings("B", "a")).toBeFalse();
            expect(equalStrings("b", "A")).toBeFalse();
            expect(equalStrings("B", "A")).toBeFalse();
            expect(equalStrings("aa", "a")).toBeFalse();
            expect(equalStrings("ba", "ab")).toBeFalse();
            expect(equalStrings("a", "")).toBeFalse();
        });
        it("should return false, if first string is less than second string (regarding case)", () => {
            expect(equalStrings("a", "A", { withCase: true })).toBeFalse();
            expect(equalStrings("a", "b", { withCase: true })).toBeFalse();
            expect(equalStrings("a", "B", { withCase: true })).toBeFalse();
            expect(equalStrings("a", "c", { withCase: true })).toBeFalse();
            expect(equalStrings("a", "C", { withCase: true })).toBeFalse();
            expect(equalStrings("a", "aa", { withCase: true })).toBeFalse();
            expect(equalStrings("ba", "bA", { withCase: true })).toBeFalse();
            expect(equalStrings("", "a", { withCase: true })).toBeFalse();
        });
        it("should return true, if the strings are equal (regarding case)", () => {
            expect(equalStrings("a", "a", { withCase: true })).toBeTrue();
            expect(equalStrings("A", "A", { withCase: true })).toBeTrue();
            expect(equalStrings("abc", "abc", { withCase: true })).toBeTrue();
            expect(equalStrings("", "", { withCase: true })).toBeTrue();
        });
        it("should return false, if first string is greater than second string (regarding case)", () => {
            expect(equalStrings("A", "a", { withCase: true })).toBeFalse();
            expect(equalStrings("b", "a", { withCase: true })).toBeFalse();
            expect(equalStrings("B", "a", { withCase: true })).toBeFalse();
            expect(equalStrings("c", "a", { withCase: true })).toBeFalse();
            expect(equalStrings("C", "a", { withCase: true })).toBeFalse();
            expect(equalStrings("aa", "a", { withCase: true })).toBeFalse();
            expect(equalStrings("bA", "ba", { withCase: true })).toBeFalse();
            expect(equalStrings("a", "", { withCase: true })).toBeFalse();
        });
    });

    describe("function equalScalars", () => {
        const { equalScalars } = scalar;
        it("should exist", () => {
            expect(equalScalars).toBeFunction();
        });
        it("should compare null to other types", () => {
            expect(equalScalars(null, 0, OPT_NULL_MIN)).toBeFalse();
            expect(equalScalars(null, 0, OPT_NULL_MAX)).toBeFalse();
            expect(equalScalars(null, "", OPT_NULL_MIN)).toBeFalse();
            expect(equalScalars(null, "", OPT_NULL_MAX)).toBeFalse();
            expect(equalScalars(null, false, OPT_NULL_MIN)).toBeFalse();
            expect(equalScalars(null, false, OPT_NULL_MAX)).toBeFalse();
            expect(equalScalars(null, ErrorCode.NULL, OPT_NULL_MIN)).toBeFalse();
            expect(equalScalars(null, ErrorCode.NULL, OPT_NULL_MAX)).toBeFalse();
        });
        it("should compare numbers to other types", () => {
            expect(equalScalars(0, null, OPT_NULL_MIN)).toBeFalse();
            expect(equalScalars(0, null, OPT_NULL_MAX)).toBeFalse();
            expect(equalScalars(0, "0")).toBeFalse();
            expect(equalScalars(0, false)).toBeFalse();
            expect(equalScalars(0, ErrorCode.NULL)).toBeFalse();
        });
        it("should compare strings to other types", () => {
            expect(equalScalars("", null, OPT_NULL_MIN)).toBeFalse();
            expect(equalScalars("", null, OPT_NULL_MAX)).toBeFalse();
            expect(equalScalars("0", 0)).toBeFalse();
            expect(equalScalars("false", false)).toBeFalse();
            expect(equalScalars("#NULL!", ErrorCode.NULL)).toBeFalse();
        });
        it("should compare booleans to other types", () => {
            expect(equalScalars(false, null, OPT_NULL_MIN)).toBeFalse();
            expect(equalScalars(false, null, OPT_NULL_MAX)).toBeFalse();
            expect(equalScalars(false, 0)).toBeFalse();
            expect(equalScalars(false, "false")).toBeFalse();
            expect(equalScalars(false, ErrorCode.NULL)).toBeFalse();
        });
        it("should compare error codes to other types", () => {
            expect(equalScalars(ErrorCode.NULL, null)).toBeFalse();
            expect(equalScalars(ErrorCode.NULL, null, OPT_NULL_MIN)).toBeFalse();
            expect(equalScalars(ErrorCode.NULL, null, OPT_NULL_MAX)).toBeFalse();
            expect(equalScalars(ErrorCode.NULL, 0)).toBeFalse();
            expect(equalScalars(ErrorCode.NULL, "#NULL!")).toBeFalse();
            expect(equalScalars(ErrorCode.NULL, false)).toBeFalse();
        });
        it("should compare numbers", () => {
            expect(equalScalars(0, 1)).toBeFalse();
            expect(equalScalars(1, 1)).toBeTrue();
            expect(equalScalars(1, 0)).toBeFalse();
            expect(equalScalars(3, 1 / 0)).toBeFalse();
        });
        it("should compare strings (ignoring case)", () => {
            expect(equalScalars("a", "a")).toBeTrue();
            expect(equalScalars("a", "A")).toBeTrue();
            expect(equalScalars("a", "b")).toBeFalse();
            expect(equalScalars("a", "B")).toBeFalse();
            expect(equalScalars("b", "a")).toBeFalse();
            expect(equalScalars("B", "a")).toBeFalse();
        });
        it("should compare strings (regarding case)", () => {
            expect(equalScalars("a", "a", { withCase: true })).toBeTrue();
            expect(equalScalars("a", "A", { withCase: true })).toBeFalse();
            expect(equalScalars("A", "a", { withCase: true })).toBeFalse();
            expect(equalScalars("a", "B", { withCase: true })).toBeFalse();
            expect(equalScalars("A", "b", { withCase: true })).toBeFalse();
        });
        it("should compare booleans", () => {
            expect(equalScalars(false, false)).toBeTrue();
            expect(equalScalars(false, true)).toBeFalse();
            expect(equalScalars(true, false)).toBeFalse();
            expect(equalScalars(true, true)).toBeTrue();
        });
        it("should compare error codes", () => {
            expect(equalScalars(ErrorCode.NULL, ErrorCode.NULL)).toBeTrue();
            expect(equalScalars(ErrorCode.NULL, ErrorCode.DIV0)).toBeFalse();
            expect(equalScalars(ErrorCode.DIV0, ErrorCode.NULL)).toBeFalse();
        });
        it("should convert null value to zero", () => {
            expect(equalScalars(null, -1, OPT_CONVERT)).toBeFalse();
            expect(equalScalars(null, 0, OPT_CONVERT)).toBeTrue();
            expect(equalScalars(null, 0)).toBeTrue();
            expect(equalScalars(null, 1, OPT_CONVERT)).toBeFalse();
            expect(equalScalars(-1, null, OPT_CONVERT)).toBeFalse();
            expect(equalScalars(0, null, OPT_CONVERT)).toBeTrue();
            expect(equalScalars(0, null)).toBeTrue();
            expect(equalScalars(1, null, OPT_CONVERT)).toBeFalse();
        });
        it("should convert null value to empty string", () => {
            expect(equalScalars(null, "", OPT_CONVERT)).toBeTrue();
            expect(equalScalars(null, "")).toBeTrue();
            expect(equalScalars(null, "a", OPT_CONVERT)).toBeFalse();
            expect(equalScalars("", null, OPT_CONVERT)).toBeTrue();
            expect(equalScalars("", null)).toBeTrue();
            expect(equalScalars("a", null, OPT_CONVERT)).toBeFalse();
        });
        it("should convert null value to FALSE", () => {
            expect(equalScalars(null, false, OPT_CONVERT)).toBeTrue();
            expect(equalScalars(null, false)).toBeTrue();
            expect(equalScalars(null, true, OPT_CONVERT)).toBeFalse();
            expect(equalScalars(null, true)).toBeFalse();
            expect(equalScalars(false, null, OPT_CONVERT)).toBeTrue();
            expect(equalScalars(false, null)).toBeTrue();
            expect(equalScalars(true, null, OPT_CONVERT)).toBeFalse();
            expect(equalScalars(true, null)).toBeFalse();
        });
        it("should not convert null value to error code", () => {
            expect(equalScalars(null, ErrorCode.NULL, OPT_CONVERT)).toBeFalse();
            expect(equalScalars(null, ErrorCode.NULL)).toBeFalse();
            expect(equalScalars(ErrorCode.NULL, null, OPT_CONVERT)).toBeFalse();
            expect(equalScalars(ErrorCode.NULL, null)).toBeFalse();
        });
        it("should compare two null values", () => {
            expect(equalScalars(null, null)).toBeTrue();
            expect(equalScalars(null, null, OPT_NULL_MIN)).toBeTrue();
            expect(equalScalars(null, null, OPT_NULL_MAX)).toBeTrue();
            expect(equalScalars(null, null, OPT_CONVERT)).toBeTrue();
        });
    });

    describe("function compareNumbers", () => {
        const { compareNumbers } = scalar;
        it("should exist", () => {
            expect(compareNumbers).toBeFunction();
        });
        it("should return -1, if first number is less than second number", () => {
            expect(compareNumbers(-3, 0)).toBe(LESS);
            expect(compareNumbers(0, 3)).toBe(LESS);
            expect(compareNumbers(1, 1 + 1e-14)).toBe(LESS);
            expect(compareNumbers(100, 100 + 1e-12)).toBe(LESS);
        });
        it("should return 0, if the numbers are equal", () => {
            expect(compareNumbers(-3, -3)).toBe(EQUAL);
            expect(compareNumbers(0, 0)).toBe(EQUAL);
            expect(compareNumbers(3, 3)).toBe(EQUAL);
            expect(compareNumbers(0, MIN_NORM_NUMBER / 2)).toBe(EQUAL);
            expect(compareNumbers(1, 1 + 2 * MIN_NORM_NUMBER)).toBe(EQUAL);
            expect(compareNumbers(100, 100 * (1 + 2 * MIN_NORM_NUMBER))).toBe(EQUAL);
        });
        it("should return 1, if first number is greater than second number", () => {
            expect(compareNumbers(3, 0)).toBe(GREATER);
            expect(compareNumbers(0, -3)).toBe(GREATER);
            expect(compareNumbers(1 + 1e-14, 1)).toBe(GREATER);
            expect(compareNumbers(100 + 1e-12, 100)).toBe(GREATER);
        });
        it("should return NaN, if either number is not finite", () => {
            expect(compareNumbers(3, 1 / 0)).toBeNaN();
            expect(compareNumbers(1 / 0, 3)).toBeNaN();
            expect(compareNumbers(1 / 0, 1 / 0)).toBeNaN();
            expect(compareNumbers(3, Number.NaN)).toBeNaN();
            expect(compareNumbers(Number.NaN, 3)).toBeNaN();
            expect(compareNumbers(Number.NaN, Number.NaN)).toBeNaN();
        });
    });

    describe("function compareStrings", () => {
        const { compareStrings } = scalar;
        it("should exist", () => {
            expect(compareStrings).toBeFunction();
        });
        it("should return -1, if first string is less than second string (ignoring case)", () => {
            expect(compareStrings("a", "b")).toBe(LESS);
            expect(compareStrings("A", "b")).toBe(LESS);
            expect(compareStrings("a", "B")).toBe(LESS);
            expect(compareStrings("A", "B")).toBe(LESS);
            expect(compareStrings("a", "aa")).toBe(LESS);
            expect(compareStrings("ab", "ba")).toBe(LESS);
            expect(compareStrings("", "a")).toBe(LESS);
        });
        it("should return 0, if the strings are equal (ignoring case)", () => {
            expect(compareStrings("a", "a")).toBe(EQUAL);
            expect(compareStrings("a", "A")).toBe(EQUAL);
            expect(compareStrings("A", "a")).toBe(EQUAL);
            expect(compareStrings("A", "A")).toBe(EQUAL);
            expect(compareStrings("abc", "abc")).toBe(EQUAL);
            expect(compareStrings("", "")).toBe(EQUAL);
        });
        it("should return 1, if first string is greater than second string (ignoring case)", () => {
            expect(compareStrings("b", "a")).toBe(GREATER);
            expect(compareStrings("B", "a")).toBe(GREATER);
            expect(compareStrings("b", "A")).toBe(GREATER);
            expect(compareStrings("B", "A")).toBe(GREATER);
            expect(compareStrings("aa", "a")).toBe(GREATER);
            expect(compareStrings("ba", "ab")).toBe(GREATER);
            expect(compareStrings("a", "")).toBe(GREATER);
        });
        it("should return -1, if first string is less than second string (regarding case)", () => {
            expect(compareStrings("a", "A", { withCase: true })).toBe(LESS);
            expect(compareStrings("a", "b", { withCase: true })).toBe(LESS);
            expect(compareStrings("a", "B", { withCase: true })).toBe(LESS);
            expect(compareStrings("a", "c", { withCase: true })).toBe(LESS);
            expect(compareStrings("a", "C", { withCase: true })).toBe(LESS);
            expect(compareStrings("a", "aa", { withCase: true })).toBe(LESS);
            expect(compareStrings("a", "Aa", { withCase: true })).toBe(LESS);
            expect(compareStrings("A", "aa", { withCase: true })).toBe(LESS);
            expect(compareStrings("aa", "ab", { withCase: true })).toBe(LESS);
            expect(compareStrings("aa", "Ab", { withCase: true })).toBe(LESS);
            expect(compareStrings("Aa", "ab", { withCase: true })).toBe(LESS);
            expect(compareStrings("", "a", { withCase: true })).toBe(LESS);
        });
        it("should return 0, if the strings are equal (regarding case)", () => {
            expect(compareStrings("a", "a", { withCase: true })).toBe(EQUAL);
            expect(compareStrings("A", "A", { withCase: true })).toBe(EQUAL);
            expect(compareStrings("abc", "abc", { withCase: true })).toBe(EQUAL);
            expect(compareStrings("", "", { withCase: true })).toBe(EQUAL);
        });
        it("should return 1, if first string is greater than second string (regarding case)", () => {
            expect(compareStrings("A", "a", { withCase: true })).toBe(GREATER);
            expect(compareStrings("b", "a", { withCase: true })).toBe(GREATER);
            expect(compareStrings("B", "a", { withCase: true })).toBe(GREATER);
            expect(compareStrings("c", "a", { withCase: true })).toBe(GREATER);
            expect(compareStrings("C", "a", { withCase: true })).toBe(GREATER);
            expect(compareStrings("aa", "a", { withCase: true })).toBe(GREATER);
            expect(compareStrings("bA", "ba", { withCase: true })).toBe(GREATER);
            expect(compareStrings("a", "", { withCase: true })).toBe(GREATER);
        });
    });

    describe("function compareBooleans", () => {
        const { compareBooleans } = scalar;
        it("should exist", () => {
            expect(compareBooleans).toBeFunction();
        });
        it("should return the result of Boolean comparison", () => {
            expect(compareBooleans(false, false)).toBe(EQUAL);
            expect(compareBooleans(false, true)).toBe(LESS);
            expect(compareBooleans(true, false)).toBe(GREATER);
            expect(compareBooleans(true, true)).toBe(EQUAL);
        });
    });

    describe("function compareErrorCodes", () => {
        const { compareErrorCodes } = scalar;
        it("should exist", () => {
            expect(compareErrorCodes).toBeFunction();
        });
        it("should return the result of error code comparison", () => {
            expect(compareErrorCodes(ErrorCode.NULL, ErrorCode.NULL)).toBe(EQUAL);
            expect(compareErrorCodes(ErrorCode.NULL, ErrorCode.DIV0)).toBe(LESS);
            expect(compareErrorCodes(ErrorCode.DIV0, ErrorCode.NULL)).toBe(GREATER);
        });
    });

    describe("function compareScalars", () => {
        const { compareScalars } = scalar;
        it("should exist", () => {
            expect(compareScalars).toBeFunction();
        });
        it("should compare null to other types", () => {
            expect(compareScalars(null, 0, OPT_NULL_MIN)).toBe(LESS);
            expect(compareScalars(null, 0, OPT_NULL_MAX)).toBe(GREATER);
            expect(compareScalars(null, "", OPT_NULL_MIN)).toBe(LESS);
            expect(compareScalars(null, "", OPT_NULL_MAX)).toBe(GREATER);
            expect(compareScalars(null, false, OPT_NULL_MIN)).toBe(LESS);
            expect(compareScalars(null, false, OPT_NULL_MAX)).toBe(GREATER);
            expect(compareScalars(null, ErrorCode.NULL, OPT_NULL_MIN)).toBe(LESS);
            expect(compareScalars(null, ErrorCode.NULL, OPT_NULL_MAX)).toBe(GREATER);
        });
        it("should compare numbers to other types", () => {
            expect(compareScalars(0, null, OPT_NULL_MIN)).toBe(GREATER);
            expect(compareScalars(0, null, OPT_NULL_MAX)).toBe(LESS);
            expect(compareScalars(0, "0")).toBe(LESS);
            expect(compareScalars(0, false)).toBe(LESS);
            expect(compareScalars(0, ErrorCode.NULL)).toBe(LESS);
        });
        it("should compare strings to other types", () => {
            expect(compareScalars("", null, OPT_NULL_MIN)).toBe(GREATER);
            expect(compareScalars("", null, OPT_NULL_MAX)).toBe(LESS);
            expect(compareScalars("0", 0)).toBe(GREATER);
            expect(compareScalars("false", false)).toBe(LESS);
            expect(compareScalars("#NULL!", ErrorCode.NULL)).toBe(LESS);
        });
        it("should compare booleans to other types", () => {
            expect(compareScalars(false, null, OPT_NULL_MIN)).toBe(GREATER);
            expect(compareScalars(false, null, OPT_NULL_MAX)).toBe(LESS);
            expect(compareScalars(false, 0)).toBe(GREATER);
            expect(compareScalars(false, "false")).toBe(GREATER);
            expect(compareScalars(false, ErrorCode.NULL)).toBe(LESS);
        });
        it("should compare error codes to other types", () => {
            expect(compareScalars(ErrorCode.NULL, null)).toBe(GREATER);
            expect(compareScalars(ErrorCode.NULL, null, OPT_NULL_MIN)).toBe(GREATER);
            expect(compareScalars(ErrorCode.NULL, null, OPT_NULL_MAX)).toBe(LESS);
            expect(compareScalars(ErrorCode.NULL, 0)).toBe(GREATER);
            expect(compareScalars(ErrorCode.NULL, "#NULL!")).toBe(GREATER);
            expect(compareScalars(ErrorCode.NULL, false)).toBe(GREATER);
        });
        it("should compare numbers", () => {
            expect(compareScalars(0, 1)).toBe(LESS);
            expect(compareScalars(1, 1)).toBe(EQUAL);
            expect(compareScalars(1, 0)).toBe(GREATER);
            expect(compareScalars(3, 1 / 0)).toBeNaN();
        });
        it("should compare strings (ignoring case)", () => {
            expect(compareScalars("a", "a")).toBe(EQUAL);
            expect(compareScalars("a", "A")).toBe(EQUAL);
            expect(compareScalars("a", "b")).toBe(LESS);
            expect(compareScalars("a", "B")).toBe(LESS);
            expect(compareScalars("b", "a")).toBe(GREATER);
            expect(compareScalars("B", "a")).toBe(GREATER);
        });
        it("should compare strings (regarding case)", () => {
            expect(compareScalars("a", "a", { withCase: true })).toBe(EQUAL);
            expect(compareScalars("a", "A", { withCase: true })).toBe(LESS);
            expect(compareScalars("A", "a", { withCase: true })).toBe(GREATER);
            expect(compareScalars("a", "B", { withCase: true })).toBe(LESS);
            expect(compareScalars("A", "b", { withCase: true })).toBe(LESS);
        });
        it("should compare booleans", () => {
            expect(compareScalars(false, false)).toBe(EQUAL);
            expect(compareScalars(false, true)).toBe(LESS);
            expect(compareScalars(true, false)).toBe(GREATER);
            expect(compareScalars(true, true)).toBe(EQUAL);
        });
        it("should compare error codes", () => {
            expect(compareScalars(ErrorCode.NULL, ErrorCode.NULL)).toBe(EQUAL);
            expect(compareScalars(ErrorCode.NULL, ErrorCode.DIV0)).toBe(LESS);
            expect(compareScalars(ErrorCode.DIV0, ErrorCode.NULL)).toBe(GREATER);
        });
        it("should convert null value to zero", () => {
            expect(compareScalars(null, -1, OPT_CONVERT)).toBe(GREATER);
            expect(compareScalars(null, 0, OPT_CONVERT)).toBe(EQUAL);
            expect(compareScalars(null, 1, OPT_CONVERT)).toBe(LESS);
            expect(compareScalars(null, 0)).toBe(EQUAL);
            expect(compareScalars(-1, null, OPT_CONVERT)).toBe(LESS);
            expect(compareScalars(0, null, OPT_CONVERT)).toBe(EQUAL);
            expect(compareScalars(0, null)).toBe(EQUAL);
            expect(compareScalars(1, null, OPT_CONVERT)).toBe(GREATER);
        });
        it("should convert null value to empty string", () => {
            expect(compareScalars(null, "", OPT_CONVERT)).toBe(EQUAL);
            expect(compareScalars(null, "")).toBe(EQUAL);
            expect(compareScalars(null, "a", OPT_CONVERT)).toBe(LESS);
            expect(compareScalars("", null, OPT_CONVERT)).toBe(EQUAL);
            expect(compareScalars("", null)).toBe(EQUAL);
            expect(compareScalars("a", null, OPT_CONVERT)).toBe(GREATER);
        });
        it("should convert null value to FALSE", () => {
            expect(compareScalars(null, false, OPT_CONVERT)).toBe(EQUAL);
            expect(compareScalars(null, false)).toBe(EQUAL);
            expect(compareScalars(null, true, OPT_CONVERT)).toBe(LESS);
            expect(compareScalars(null, true)).toBe(LESS);
            expect(compareScalars(false, null, OPT_CONVERT)).toBe(EQUAL);
            expect(compareScalars(false, null)).toBe(EQUAL);
            expect(compareScalars(true, null, OPT_CONVERT)).toBe(GREATER);
            expect(compareScalars(true, null)).toBe(GREATER);
        });
        it("should not convert null value to error code", () => {
            expect(compareScalars(null, ErrorCode.NULL, OPT_CONVERT)).toBe(LESS);
            expect(compareScalars(null, ErrorCode.NULL)).toBe(LESS);
            expect(compareScalars(ErrorCode.NULL, null, OPT_CONVERT)).toBe(GREATER);
            expect(compareScalars(ErrorCode.NULL, null)).toBe(GREATER);
        });
        it("should compare two null values", () => {
            expect(compareScalars(null, null)).toBe(EQUAL);
            expect(compareScalars(null, null, OPT_NULL_MIN)).toBe(EQUAL);
            expect(compareScalars(null, null, OPT_NULL_MAX)).toBe(EQUAL);
            expect(compareScalars(null, null, OPT_CONVERT)).toBe(EQUAL);
        });
    });

    describe("function scalarToString", () => {
        const { scalarToString } = scalar;
        it("should exist", () => {
            expect(scalarToString).toBeFunction();
        });
        it("should convert strings", () => {
            expect(scalarToString("abc")).toBe('"abc"');
            expect(scalarToString('a"b"c')).toBe('"a""b""c"');
            expect(scalarToString("")).toBe('""');
        });
        it("should convert dates", () => {
            const now = new Date();
            expect(scalarToString(now)).toBe(now.toISOString());
        });
        it("should convert null and booleans", () => {
            expect(scalarToString(null)).toBe("null");
            expect(scalarToString(true)).toBe("TRUE");
            expect(scalarToString(false)).toBe("FALSE");
        });
        it("should convert numbers", () => {
            expect(scalarToString(42)).toBe("42");
            expect(scalarToString(-12.5)).toBe("-12.5");
        });
        it("should convert error codes", () => {
            expect(scalarToString(ErrorCode.REF)).toBe("#REF");
        });
    });
});
