/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";
import { ArrayBase } from "@/io.ox/office/spreadsheet/utils/arraybase";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { Range3DArray } from "@/io.ox/office/spreadsheet/utils/range3darray";

import { i, r3d, ia, ra, r3da } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/range3darray", () => {

    // class Range3DArray -----------------------------------------------------

    describe("Spreadsheet class Range3DArray", () => {

        it("should subclass ArrayBase", () => {
            expect(Range3DArray).toBeSubClassOf(ArrayBase);
            expect(Range3DArray).toBeSubClassOf(Array);
        });

        describe("constructor", () => {
            it("should create a range array", () => {
                const ra1 = new Range3DArray();
                expect(ra1).toBeArrayOfSize(0);
            });
            const r1 = r3d("1:2!A2:C4"), r2 = r3d("6:8!B3:D5");
            it("should insert single ranges", () => {
                const ra1 = new Range3DArray(r1);
                expect(ra1).toHaveLength(1);
                expect(ra1[0]).toBe(r1);
                const ra2 = new Range3DArray(r1, r2, null, r1);
                expect(ra2).toHaveLength(3);
                expect(ra2[0]).toBe(r1);
                expect(ra2[1]).toBe(r2);
                expect(ra2[2]).toBe(r1);
            });
            it("should insert plain arrays of ranges", () => {
                const ra1 = new Range3DArray([r1]);
                expect(ra1).toHaveLength(1);
                expect(ra1[0]).toBe(r1);
                const ra2 = new Range3DArray([r1, r2], [], [r1]);
                expect(ra2).toHaveLength(3);
                expect(ra2[0]).toBe(r1);
                expect(ra2[1]).toBe(r2);
                expect(ra2[2]).toBe(r1);
            });
            it("should copy-construct range arrays", () => {
                const ra0 = new Range3DArray(r1, r2);
                const ra1 = new Range3DArray(ra0);
                expect(ra1).toHaveLength(2);
                expect(ra1[0]).toBe(r1);
                expect(ra1[1]).toBe(r2);
                const ra2 = new Range3DArray(ra0, new Range3DArray(), ra1);
                expect(ra2).toHaveLength(4);
                expect(ra2[0]).toBe(r1);
                expect(ra2[1]).toBe(r2);
                expect(ra2[2]).toBe(r1);
                expect(ra2[3]).toBe(r2);
            });
        });

        describe("static function cast", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveStaticMethod("cast");
            });
            it("should not convert range arrays", () => {
                const ra1 = r3da("1:2!A1:B2 2:3!C3:D4");
                expect(Range3DArray.cast(ra1)).toBe(ra1);
            });
            it("should convert plain arrays", () => {
                const ra1 = [r3d("1:2!A1:B2"), r3d("2:3!C3:D4")];
                const ra2 = Range3DArray.cast(ra1);
                expect(ra2).toBeInstanceOf(Range3DArray);
                expect(ra2).toHaveLength(ra1.length);
                ra1.forEach(function (v, idx) { expect(ra2[idx]).toBe(v); });
            });
            it("should convert single object to array", () => {
                const r1 = r3d("1:2!A1:B2");
                const ra1 = Range3DArray.cast(r1);
                expect(ra1).toBeInstanceOf(Range3DArray);
                expect(ra1).toHaveLength(1);
                expect(ra1[0]).toBe(r1);
            });
            it("should convert nullish to empty arrays", () => {
                const ra1 = Range3DArray.cast(null);
                const ra2 = Range3DArray.cast(undefined);
                expect(ra1).toBeInstanceOf(Range3DArray);
                expect(ra1).toHaveLength(0);
                expect(ra2).toBeInstanceOf(Range3DArray);
                expect(ra2).toHaveLength(0);
            });
        });

        describe("static function filter", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveStaticMethod("filter");
            });
        });

        describe("static function map", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveStaticMethod("map");
            });
        });

        describe("static function fromRanges", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveStaticMethod("fromRanges");
            });
            const ra1 = ra("A2:C4 B3:D5"), ra2 = r3da("6:8!A2:C4 6:8!B3:D5");
            it("should return a range array", () => {
                const ra3 = Range3DArray.fromRanges(ra1, 6, 8);
                expect(ra3).toBeInstanceOf(Range3DArray);
                expect(ra3).toHaveLength(2);
                expect(ra3[0]).toBeInstanceOf(Range3D);
                expect(ra3[1]).toBeInstanceOf(Range3D);
            });
            it("should return ranges with adjusted sheet indexes", () => {
                expect(Range3DArray.fromRanges(ra1, 6, 8)).toEqual(ra2);
                expect(Range3DArray.fromRanges(ra1, 8, 6)).toEqual(ra2);
            });
            it("should accept single sheet index", () => {
                const ra3 = Range3DArray.fromRanges(ra1, 6);
                expect(ra3[0]).toHaveProperty("sheet1", 6);
                expect(ra3[0]).toHaveProperty("sheet2", 6);
                expect(ra3[1]).toHaveProperty("sheet1", 6);
                expect(ra3[1]).toHaveProperty("sheet2", 6);
            });
            it("should clone addresses", () => {
                const ra3 = Range3DArray.fromRanges(ra1, 6, 8);
                expect(ra3[0].a1).toEqual(ra1[0].a1);
                expect(ra3[0].a1).not.toBe(ra1[0].a1);
                expect(ra3[1].a2).toEqual(ra1[1].a2);
                expect(ra3[1].a2).not.toBe(ra1[1].a2);
            });
        });

        describe("static function fromIntervals", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveStaticMethod("fromIntervals");
            });
            const ia1 = ia("B:C C:D"), ia2 = ia("3:4 4:5");
            it("should create range addresses from column and row intervals", () => {
                expect(Range3DArray.fromIntervals(ia1, ia2, 6, 8)).toEqual(r3da("6:8!B3:C4 6:8!C3:D4 6:8!B4:C5 6:8!C4:D5"));
            });
            it("should accept single intervals", () => {
                const i1 = i("B:D"), i2 = i("3:5");
                expect(Range3DArray.fromIntervals(i1, ia2, 6, 8)).toEqual(r3da("6:8!B3:D4 6:8!B4:D5"));
                expect(Range3DArray.fromIntervals(ia1, i2, 6, 8)).toEqual(r3da("6:8!B3:C5 6:8!C3:D5"));
                expect(Range3DArray.fromIntervals(i1, i2, 6, 8)).toEqual(r3da("6:8!B3:D5"));
            });
            it("should accept empty arrays", () => {
                const ia0 = new IntervalArray();
                expect(Range3DArray.fromIntervals(ia1, ia0, 6, 8)).toHaveLength(0);
                expect(Range3DArray.fromIntervals(ia0, ia2, 6, 8)).toHaveLength(0);
                expect(Range3DArray.fromIntervals(ia0, ia0, 6, 8)).toHaveLength(0);
            });
            it("should accept single sheet index", () => {
                expect(Range3DArray.fromIntervals(i("B:D"), i("3:5"), 6)).toEqual(r3da("6:6!B3:D5"));
            });
        });

        describe("property key", () => {
            it("should return unique key for the array", () => {
                expect(r3da("1:1!A2:C4 6:8!B3:D5").key).toBe("1:1!0,1:2,3 6:8!1,2:3,4");
                expect(r3da("").key).toBe("");
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveMethod("clone");
            });
            const ra1 = r3da("1:1!A2:C4 6:8!B3:D5");
            it("should return a shallow clone", () => {
                const ra2 = ra1.clone();
                expect(ra2).toBeInstanceOf(Range3DArray);
                expect(ra2).not.toBe(ra1);
                expect(ra2[0]).toBe(ra1[0]);
                expect(ra2[1]).toBe(ra1[1]);
                expect(ra2).toEqual(ra1);
            });
        });

        describe("method deepClone", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveMethod("deepClone");
            });
            const ra1 = r3da("1:1!A2:C4 6:8!B3:D5");
            it("should return a deep clone", () => {
                const ra2 = ra1.deepClone();
                expect(ra2).toBeInstanceOf(Range3DArray);
                expect(ra2).not.toBe(ra1);
                expect(ra2[0]).not.toBe(ra1[0]);
                expect(ra2[1]).not.toBe(ra1[1]);
                expect(ra2).toEqual(ra1);
            });
        });

        describe("method cells", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveMethod("cells");
            });
            it("should count cells in ranges", () => {
                expect(r3da("1:1!A1:C3 6:8!B2:D4").cells()).toBe(36);
                expect(new Range3DArray().cells()).toBe(0);
            });
        });

        describe("method singleSheet", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveMethod("singleSheet");
            });
            it("should return true if all ranges refer to the same single sheet", () => {
                expect(r3da("1:1!A1:C3 1:1!B2:D4").singleSheet()).toBeTrue();
            });
            it("should return false if ranges refer to different single sheets", () => {
                expect(r3da("1:1!A1:C3 2:2!B2:D4").singleSheet()).toBeFalse();
            });
            it("should return false if ranges refer to multiple sheets", () => {
                expect(r3da("1:2!A1:C3 1:2!B2:D4").singleSheet()).toBeFalse();
            });
        });

        describe("method overlaps", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveMethod("overlaps");
            });
            const ra1 = r3da("1:2!A1:B2 2:3!B2:C3 3:4!C3:D4");
            it("should return false for distinct ranges", () => {
                expect(ra1.overlaps(r3da("0:0!A1:D4 3:3!A1:A1"))).toBeFalse();
            });
            it("should return true for overlapping ranges", () => {
                expect(ra1.overlaps(r3da("0:0!A1:D4 3:3!A1:B2"))).toBeTrue();
            });
            it("should accept empty array", () => {
                const ra0 = new Range3DArray();
                expect(ra0.overlaps(ra1)).toBeFalse();
                expect(ra1.overlaps(ra0)).toBeFalse();
                expect(ra0.overlaps(ra0)).toBeFalse();
            });
            it("should accept single range address", () => {
                expect(ra1.overlaps(r3d("0:0!A1:D4"))).toBeFalse();
                expect(ra1.overlaps(r3d("3:3!A1:B2"))).toBeTrue();
            });
        });

        describe("method boundary", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveMethod("boundary");
            });
            it("should return the bounding range", () => {
                expect(r3da("2:4!B3:C4 1:3!C4:D5").boundary()).toEqual(r3d("1:4!B3:D5"));
                expect(r3da("6:8!C4:D5 0:0!B3:C4 7:7!A2:B3").boundary()).toEqual(r3d("0:8!A2:D5"));
            });
            it("should return a clone of a single range", () => {
                const ra1 = r3da("0:1!B3:C4"), r1 = ra1.boundary();
                expect(r1).toEqual(ra1[0]);
                expect(r1).not.toBe(ra1[0]);
            });
            it("should return undefined for an empty array", () => {
                expect(new Range3DArray().boundary()).toBeUndefined();
            });
        });

        describe("method unify", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveMethod("unify");
            });
            it("should return a shallow copy", () => {
                const ra1 = r3da("1:1!A2:C4 1:1!B3:D5"), ra2 = ra1.unify();
                expect(ra2).not.toBe(ra1);
                expect(ra2[0]).toBe(ra1[0]);
                expect(ra2[1]).toBe(ra1[1]);
            });
            it("should remove all duplicates", () => {
                const ra1 = r3da("1:1!A2:C4 1:1!B3:D5 1:1!A2:C4 1:2!A2:C4 1:1!B3:D5 1:2!A2:C4"), ra2 = ra1.unify();
                expect(ra2).toHaveLength(3);
                expect(ra2[0]).toBe(ra1[0]);
                expect(ra2[1]).toBe(ra1[1]);
                expect(ra2[2]).toBe(ra1[3]);
            });
            it("should accept empty array", () => {
                const ra0 = new Range3DArray(), ra1 = ra0.unify();
                expect(ra1).toBeInstanceOf(Range3DArray);
                expect(ra1).toHaveLength(0);
                expect(ra1).not.toBe(ra0);
            });
        });

        describe("method intersect", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveMethod("intersect");
            });
            const ra1 = r3da("4:6!B3:D5 6:8!D5:F7");
            it("should return empty array for distinct ranges", () => {
                expect(ra1.intersect(r3da("4:8!E3:F4 4:8!B6:C7"))).toHaveLength(0);
            });
            it("should return intersection ranges", () => {
                expect(ra1.intersect(ra1)).toEqual(r3da("4:6!B3:D5 6:6!D5:D5 6:6!D5:D5 6:8!D5:F7"));
                expect(ra1.intersect(r3da("4:6!B3:D5"))).toEqual(r3da("4:6!B3:D5 6:6!D5:D5"));
                expect(ra1.intersect(r3da("6:7!D5:E6"))).toEqual(r3da("6:6!D5:D5 6:7!D5:E6"));
                expect(ra1.intersect(r3da("5:7!A1:C9"))).toEqual(r3da("5:6!B3:C5"));
                expect(ra1.intersect(r3da("5:7!A5:I6"))).toEqual(r3da("5:6!B5:D5 6:7!D5:F6"));
                expect(ra1.intersect(r3da("3:7!D1:D9 5:9!A5:I5"))).toEqual(r3da("4:6!D3:D5 5:6!B5:D5 6:7!D5:D7 6:8!D5:F5"));
            });
            it("should accept empty arrays", () => {
                const ra0 = new Range3DArray();
                expect(ra0.intersect(ra1)).toHaveLength(0);
                expect(ra1.intersect(ra0)).toHaveLength(0);
                expect(ra0.intersect(ra0)).toHaveLength(0);
            });
            it("should accept single range address", () => {
                expect(ra1.intersect(r3d("4:6!B3:D5"))).toEqual(r3da("4:6!B3:D5 6:6!D5:D5"));
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(Range3DArray).toHaveMethod("toString");
            });
            const ra1 = r3da("1:1!A2:C4 6:8!B3:D5");
            it("should stringify the ranges", () => {
                expect(ra1.toString()).toBe("1:1!A2:C4,6:8!B3:D5");
            });
            it("should use the separator string", () => {
                expect(ra1.toString(" + ")).toBe("1:1!A2:C4 + 6:8!B3:D5");
            });
            it("should stringify implicitly", () => {
                expect("<" + ra1 + ">").toBe("<1:1!A2:C4,6:8!B3:D5>");
            });
        });
    });
});
