/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { KeySet } from "@/io.ox/office/tk/containers";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { FindMatchType, RangeSet } from "@/io.ox/office/spreadsheet/utils/rangeset";

import { r, ra } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/rangeset", () => {

    // types ------------------------------------------------------------------

    describe("enum FindMatchType", () => {
        it("should exist", () => {
            expect(FindMatchType).toBeObject();
        });
        it("should contain all anchor modes", () => {
            expect(FindMatchType).toHaveProperty("OVERLAP");
            expect(FindMatchType).toHaveProperty("CONTAIN");
            expect(FindMatchType).toHaveProperty("COVER");
            expect(FindMatchType).toHaveProperty("EXACT");
            expect(FindMatchType).toHaveProperty("PARTIAL");
            expect(FindMatchType).toHaveProperty("REFERENCE");
        });
    });

    // class RangeSet ---------------------------------------------------------

    describe("class RangeSet", () => {

        it("should subclass KeySet", () => {
            expect(RangeSet).toBeSubClassOf(KeySet);
        });

        describe("static function from", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveStaticMethod("from");
            });
            it("should create a range set", () => {
                const ranges = ra("A2:B3 D5:G8 I10:J11");
                const set = RangeSet.from(ranges);
                expect(set).toBeInstanceOf(RangeSet);
                const values = RangeArray.from(set);
                expect(values).toEqual(ranges);
                expect(values[0]).toBe(ranges[0]);
            });
            it("should create a range set with mapper function", () => {
                const ranges = "A2:B3 D5:G8 I10:J11";
                const set = RangeSet.from(ranges.split(" "), r);
                expect(set).toBeInstanceOf(RangeSet);
                expect(RangeArray.from(set).toOpStr()).toBe(ranges);
            });
        });

        describe("property size", () => {
            it("should return the number of addresses", () => {
                expect(new RangeSet().size).toBe(0);
                expect(new RangeSet(ra("A2:B3 D5:g8 I10:J11 A2:B3")).size).toBe(3);
            });
        });

        describe("method clear", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("clear");
            });
            it("should clear the set", () => {
                const set = new RangeSet(ra("A2:B3"));
                expect(set.size).toBe(1);
                set.clear();
                expect(set.size).toBe(0);
                expect(Array.from(set)).toEqual([]);
            });
        });

        describe("method has", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("has");
            });
            it("should return whether the range exists", () => {
                const set = new RangeSet(ra("A2:B3"));
                expect(set.has(r("A2:B3"))).toBeTrue();
                expect(set.has(r("A1:B3"))).toBeFalse();
                expect(set.has(r("A2:C3"))).toBeFalse();
            });
        });

        describe("method at", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("at");
            });
            it("should return the range in the set", () => {
                const r1 = r("A2:B3");
                const set = new RangeSet([r1]);
                expect(set.at(r1)).toBe(r1);
                expect(set.at(r("A2:B3"))).toBe(r1);
                expect(set.at(r("A1:B3"))).toBeUndefined();
                expect(set.at(r("A2:C3"))).toBeUndefined();
            });
        });

        describe("method add", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("add");
            });
            const set = new RangeSet();
            it("should insert a new range", () => {
                const r1 = r("A2:B3"), r2 = r1.clone();
                expect(set.add(r1)).toBe(set);
                expect(set.has(r1)).toBeTrue();
                expect(set.has(r2)).toBeTrue();
                expect(set.at(r1)).toBe(r1);
                expect(set.at(r2)).toBe(r1);
                expect(set.add(r2)).toBe(set);
                expect(set.has(r1)).toBeTrue();
                expect(set.has(r2)).toBeTrue();
                expect(set.at(r1)).toBe(r2);
                expect(set.at(r2)).toBe(r2);
            });
            it("should insert other ranges", () => {
                set.add(r("A1:B1")).add(r("C5:D10"));
                expect(RangeArray.from(set).toOpStr()).toBe("A2:B3 A1:B1 C5:D10");
            });
        });

        describe("method delete", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("delete");
            });
            it("should remove ranges", () => {
                const r1 = r("A2:C4");
                const set = new RangeSet([r1, r("B3:C4"), r("C4:E6")]);
                expect(set.size).toBe(3);
                expect(set.delete(r("A2:B3"))).toBeFalse();
                expect(set.size).toBe(3);
                expect(set.delete(r("A2:C4"))).toBeTrue();
                expect(RangeArray.from(set).toOpStr()).toBe("B3:C4 C4:E6");
            });
        });

        describe("method forEach", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("forEach");
            });
            it("should visit all values", () => {
                const set = new RangeSet(ra("A2:B3 D5:G8 I10:J11"));
                const spy = jest.fn(), context = {};
                set.forEach(spy, context);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(spy).toHaveBeenAlwaysCalledOn(context);
            });
        });

        describe("method boundary", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("boundary");
            });
            it("should return undefined for an empty set", () => {
                const set = new RangeSet();
                expect(set.boundary()).toBeUndefined();
            });
            it("should return the boundary", () => {
                const set = new RangeSet(ra("B8:C9 I2:J3"));
                expect(set.boundary()).toStringifyTo("B2:J9");
            });
        });

        describe("method yieldMatching", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("yieldMatching");
            });
        });

        describe("method yieldByAddress", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("yieldByAddress");
            });
        });

        describe("method containsAddress", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("containsAddress");
            });
        });

        describe("method findOneByAddress", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("findOneByAddress");
            });
        });

        describe("method findOne", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("findOne");
            });
        });

        describe("method deleteMatching", () => {
            it("should exist", () => {
                expect(RangeSet).toHaveMethod("deleteMatching");
            });
        });
    });
});
