/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { KeySet } from "@/io.ox/office/tk/containers";
import { AddressSet } from "@/io.ox/office/spreadsheet/utils/addressset";

import { a, r, aa } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/addressset", () => {

    // class AddressSet -------------------------------------------------------

    describe("class AddressSet", () => {

        it("should subclass KeySet", () => {
            expect(AddressSet).toBeSubClassOf(KeySet);
        });

        describe("method has", () => {
            it("should exist", () => {
                expect(AddressSet).toHaveMethod("has");
            });
            it("should return whether the address exists", () => {
                const set = new AddressSet(aa("B3"));
                expect(set.has(a("B3"))).toBeTrue();
                expect(set.has(a("B2"))).toBeFalse();
                expect(set.has(a("A3"))).toBeFalse();
            });
        });

        describe("method at", () => {
            it("should exist", () => {
                expect(AddressSet).toHaveMethod("at");
            });
            it("should return the address in the set", () => {
                const a1 = a("B3");
                const set = new AddressSet([a1]);
                expect(set.at(a1)).toBe(a1);
                expect(set.at(a("B3"))).toBe(a1);
                expect(set.at(a("B2"))).toBeUndefined();
                expect(set.at(a("A3"))).toBeUndefined();
            });
        });

        describe("method add", () => {
            it("should exist", () => {
                expect(AddressSet).toHaveMethod("add");
            });
            const set = new AddressSet();
            it("should insert a new address", () => {
                const a1 = a("A2"), a2 = a1.clone();
                expect(set.add(a1)).toBe(set);
                expect(set.has(a1)).toBeTrue();
                expect(set.has(a2)).toBeTrue();
                expect(set.at(a1)).toBe(a1);
                expect(set.at(a2)).toBe(a1);
                expect(set.add(a2)).toBe(set);
                expect(set.has(a1)).toBeTrue();
                expect(set.has(a2)).toBeTrue();
                expect(set.at(a1)).toBe(a2);
                expect(set.at(a2)).toBe(a2);
            });
            it("should insert other addresses", () => {
                set.add(a("A5")).add(a("A4")).add(a("A3")).add(a("E2")).add(a("D2")).add(a("C2"));
                expect(Array.from(set)).toStringifyTo("A2,A5,A4,A3,E2,D2,C2");
            });
        });

        describe("method delete", () => {
            it("should exist", () => {
                expect(AddressSet).toHaveMethod("delete");
            });
            it("should remove addresses", () => {
                const set = new AddressSet([a("A2"), a("B3"), a("C4")]);
                expect(set.delete(a("A1"))).toBeFalse();
                expect(set.has(a("A2"))).toBeTrue();
                expect(set.delete(a("A2"))).toBeTrue();
                expect(set.has(a("A2"))).toBeFalse();
            });
        });

        describe("method first", () => {
            it("should exist", () => {
                expect(AddressSet).toHaveMethod("first");
            });
            it("should return nullish for an empty set", () => {
                const set = new AddressSet();
                expect(set.first()).toBeUndefined();
                expect(set.first(true)).toBeUndefined();
            });
            it("should return the results", () => {
                const set = new AddressSet(aa("B4 C3 D4 C5"));
                expect(set.first()).toStringifyTo("C3");
                expect(set.first(true)).toStringifyTo("B4");
            });
        });

        describe("method boundary", () => {
            it("should exist", () => {
                expect(AddressSet).toHaveMethod("boundary");
            });
            it("should return undefined for an empty set", () => {
                const set = new AddressSet();
                expect(set.boundary()).toBeUndefined();
            });
            it("should return the results", () => {
                const set = new AddressSet(aa("B4 C3 D4 C5"));
                expect(set.boundary()).toStringifyTo("B3:D5");
            });
        });

        describe("method ordered", () => {
            it("should exist", () => {
                expect(AddressSet).toHaveMethod("ordered");
            });
            it("should visit all addresses", () => {
                const set = new AddressSet(aa("A2 A4 A6 C2 C4 C6 E2 E4 E6"));
                const it1 = set.ordered();
                expect(it1).toBeIterator();
                expect(Array.from(it1)).toStringifyTo("A2,C2,E2,A4,C4,E4,A6,C6,E6");
                const it2 = set.ordered({ vertical: true });
                expect(Array.from(it2)).toStringifyTo("A2,A4,A6,C2,C4,C6,E2,E4,E6");
                const it5 = set.ordered({ reverse: true });
                expect(Array.from(it5)).toStringifyTo("E6,C6,A6,E4,C4,A4,E2,C2,A2");
                const it6 = set.ordered({ vertical: true, reverse: true });
                expect(Array.from(it6)).toStringifyTo("E6,E4,E2,C6,C4,C2,A6,A4,A2");
            });
            it("should visit all addresses in a range", () => {
                const set = new AddressSet(aa("A2 A4 A6 C2 C4 C6 E2 E4 E6"));
                const it1 = set.ordered({ range: r("A1:C10") });
                expect(it1).toBeIterator();
                expect(Array.from(it1)).toStringifyTo("A2,C2,A4,C4,A6,C6");
                const it2 = set.ordered({ range: r("A3:J5") });
                expect(Array.from(it2)).toStringifyTo("A4,C4,E4");
                const it3 = set.ordered({ range: r("D3:D5") });
                expect(Array.from(it3)).toStringifyTo("");
                const it4 = set.ordered({ range: r("A1:C10"), vertical: true });
                expect(Array.from(it4)).toStringifyTo("A2,A4,A6,C2,C4,C6");
                const it5 = set.ordered({ range: r("A1:C10"), reverse: true });
                expect(Array.from(it5)).toStringifyTo("C6,A6,C4,A4,C2,A2");
                const it6 = set.ordered({ range: r("A1:C10"), vertical: true, reverse: true });
                expect(Array.from(it6)).toStringifyTo("C6,C4,C2,A6,A4,A2");
            });
        });
    });
});
