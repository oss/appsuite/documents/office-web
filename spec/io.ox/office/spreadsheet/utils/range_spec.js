/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { rangeKey, rangeKeyFromIndexes, Range } from "@/io.ox/office/spreadsheet/utils/range";

import { i, a, r } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/range", () => {

    // functions --------------------------------------------------------------

    describe("function rangeKey", () => {
        it("should exist", () => {
            expect(rangeKey).toBeFunction();
        });
        it("should return unique key for cell range address", () => {
            expect(rangeKey(a("A2"), a("C4"))).toBe("0,1:2,3");
        });
    });

    describe("function rangeKeyFromIndexes", () => {
        it("should exist", () => {
            expect(rangeKeyFromIndexes).toBeFunction();
        });
        it("should return unique key for cell range address", () => {
            expect(rangeKeyFromIndexes(0, 1, 2, 3)).toBe("0,1:2,3");
        });
    });

    // class Range ------------------------------------------------------------

    describe("class Range", () => {

        it("should exist", () => {
            expect(Range).toBeFunction();
        });

        describe("constructor", () => {
            const a1 = a("A2"), a2 = a("C4");
            it("should create a range address", () => {
                const r1 = new Range(a1, a2);
                expect(r1.a1).toBe(a1);
                expect(r1.a2).toBe(a2);
            });
            it("should create a range address from a single address", () => {
                const r1 = new Range(a1);
                expect(r1.a1).toBe(a1);
                expect(r1.a2).toEqual(a1);
                expect(r1.a2).not.toBe(a1);
            });
        });

        describe("static function fromIndexes", () => {
            it("should exist", () => {
                expect(Range).toHaveStaticMethod("fromIndexes");
            });
            it("should return the adjusted range address", () => {
                const r1 = new Range(a("A2"), a("C4"));
                expect(Range.fromIndexes(0, 1, 2, 3)).toEqual(r1);
                expect(Range.fromIndexes(2, 1, 0, 3)).toEqual(r1);
                expect(Range.fromIndexes(0, 3, 2, 1)).toEqual(r1);
                expect(Range.fromIndexes(2, 3, 0, 1)).toEqual(r1);
            });
        });

        describe("static function fromIntervals", () => {
            it("should exist", () => {
                expect(Range).toHaveStaticMethod("fromIntervals");
            });
            it("should create range address from intervals", () => {
                expect(Range.fromIntervals(i("B:C"), i("3:4"))).toEqual(r("B3:C4"));
            });
        });

        describe("static function fromColInterval", () => {
            it("should exist", () => {
                expect(Range).toHaveStaticMethod("fromColInterval");
            });
            it("should create range address from column interval and rows", () => {
                expect(Range.fromColInterval(i("B:C"), 2, 3)).toEqual(r("B3:C4"));
                expect(Range.fromColInterval(i("B:C"), 2)).toEqual(r("B3:C3"));
            });
        });

        describe("static function fromRowInterval", () => {
            it("should exist", () => {
                expect(Range).toHaveStaticMethod("fromRowInterval");
            });
            it("should create range address from row interval and columns", () => {
                expect(Range.fromRowInterval(i("3:4"), 1, 2)).toEqual(r("B3:C4"));
                expect(Range.fromRowInterval(i("3:4"), 1)).toEqual(r("B3:B4"));
            });
        });

        describe("static function fromAddresses", () => {
            it("should exist", () => {
                expect(Range).toHaveStaticMethod("fromAddresses");
            });
            it("should return the adjusted range address", () => {
                expect(Range.fromAddresses(a("B3"), a("D5"))).toEqual(r("B3:D5"));
                expect(Range.fromAddresses(a("B5"), a("D3"))).toEqual(r("B3:D5"));
                expect(Range.fromAddresses(a("D3"), a("B5"))).toEqual(r("B3:D5"));
                expect(Range.fromAddresses(a("D5"), a("B3"))).toEqual(r("B3:D5"));
            });
            it("should return range address for single cell", () => {
                expect(Range.fromAddresses(a("B3"))).toEqual(r("B3:B3"));
            });
            it("should clone addresses", () => {
                const a1 = a("A2"), a2 = a("C4"), r1 = Range.fromAddresses(a1, a2);
                expect(r1.a1).toEqual(a1);
                expect(r1.a1).not.toBe(a1);
                expect(r1.a2).toEqual(a2);
                expect(r1.a2).not.toBe(a2);
            });
        });

        describe("static function fromAddressAndSize", () => {
            it("should exist", () => {
                expect(Range).toHaveStaticMethod("fromAddressAndSize");
            });
            it("should return the resulting range address", () => {
                expect(Range.fromAddressAndSize(a("B3"), 1, 1)).toEqual(r("B3:B3"));
                expect(Range.fromAddressAndSize(a("B3"), 4, 1)).toEqual(r("B3:E3"));
                expect(Range.fromAddressAndSize(a("B3"), 1, 3)).toEqual(r("B3:B5"));
                expect(Range.fromAddressAndSize(a("B3"), 4, 3)).toEqual(r("B3:E5"));
            });
            it("should clone addresses", () => {
                const a1 = a("A2"), r1 = Range.fromAddressAndSize(a1, 1, 1);
                expect(r1.a1).toEqual(a1);
                expect(r1.a1).not.toBe(a1);
                expect(r1.a2).toEqual(a1);
                expect(r1.a2).not.toBe(a1);
            });
        });

        describe("static function parse", () => {
            it("should exist", () => {
                expect(Range).toHaveStaticMethod("parse");
            });
            const r1 = Range.fromIndexes(0, 1, 2, 3);
            it("should return the correct range address", () => {
                expect(Range.parse("A2:C4")).toEqual(r1);
                expect(Range.parse("A1:ZZZZ99999999")).toEqual(Range.fromIndexes(0, 0, 475253, 99999998));
            });
            it("should return the adjusted range address", () => {
                expect(Range.parse("C2:A4")).toEqual(r1);
                expect(Range.parse("A4:C2")).toEqual(r1);
                expect(Range.parse("C4:A2")).toEqual(r1);
            });
            it("should accept single cell address", () => {
                expect(Range.parse("A2")).toEqual(Range.fromIndexes(0, 1, 0, 1));
            });
            it("should accept lower-case columns and leading zero characters", () => {
                expect(Range.parse("a01:aa010")).toEqual(Range.fromIndexes(0, 0, 26, 9));
                expect(Range.parse("A001:aA0010")).toEqual(Range.fromIndexes(0, 0, 26, 9));
            });
            it("should return null for invalid range names", () => {
                expect(Range.parse("")).toBeNull();
                expect(Range.parse("A")).toBeNull();
                expect(Range.parse("B:C")).toBeNull();
                expect(Range.parse("1")).toBeNull();
                expect(Range.parse("2:3")).toBeNull();
                expect(Range.parse("$")).toBeNull();
                expect(Range.parse("$A$1:$A$1")).toBeNull();
                expect(Range.parse("A0:A1")).toBeNull();
                expect(Range.parse("AAAAA1:AAAAA1")).toBeNull();
                expect(Range.parse("A100000000:A100000000")).toBeNull();
            });
        });

        describe("static function compare", () => {
            const r1 = r("C3:E5");
            it("should exist", () => {
                expect(Range).toHaveStaticMethod("compare");
            });
            it("should return 0 for equal ranges", () => {
                expect(Range.compare(r1, r1)).toBe(0);
            });
            it("should return negative value for ranges in order", () => {
                expect(Range.compare(r1, r("B4:D4"))).toBe(-1);
                expect(Range.compare(r1, r("D3:D4"))).toBe(-1);
                expect(Range.compare(r1, r("C3:D6"))).toBe(-1);
                expect(Range.compare(r1, r("C3:F5"))).toBe(-1);
            });
            it("should return positive value for ranges in reversed order", () => {
                expect(Range.compare(r1, r("D2:F6"))).toBe(1);
                expect(Range.compare(r1, r("B3:F6"))).toBe(1);
                expect(Range.compare(r1, r("C3:F4"))).toBe(1);
                expect(Range.compare(r1, r("C3:D5"))).toBe(1);
            });
        });

        // public getters -------------------------------------------------

        describe("property key", () => {
            it("should return unique key for range address", () => {
                expect(r("A2:A2").key).toBe("0,1:0,1");
                expect(r("B3:D5").key).toBe("1,2:3,4");
                expect(r("A1:ZZ1000").key).toBe("0,0:701,999");
            });
        });

        // public methods -------------------------------------------------

        describe("method clone", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("clone");
            });
            it("should return a clone", () => {
                const r1 = r("A2:C4"), r2 = r1.clone();
                expect(r2).toBeInstanceOf(Range);
                expect(r2).not.toBe(r1);
                expect(r2.a1).not.toBe(r1.a1);
                expect(r2.a2).not.toBe(r1.a2);
                expect(r2).toEqual(r1);
            });
        });

        describe("method equals", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("equals");
            });
            const range = r("A2:C4");
            it("should return true for equal ranges", () => {
                expect(r("A2:A4").equals(r("A2:A4"))).toBeTrue();
                expect(r("A2:C2").equals(r("A2:C2"))).toBeTrue();
                expect(r("A2:C4").equals(r("A2:C4"))).toBeTrue();
                expect(range.equals(range)).toBeTrue();
            });
            it("should return false for different ranges", () => {
                expect(range.equals(r("A2:C5"))).toBeFalse();
                expect(range.equals(r("A2:D4"))).toBeFalse();
                expect(range.equals(r("A3:C4"))).toBeFalse();
                expect(range.equals(r("B2:C4"))).toBeFalse();
            });
        });

        describe("method differs", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("differs");
            });
            const range = r("A2:C4");
            it("should return false for equal ranges", () => {
                expect(r("A2:A4").differs(r("A2:A4"))).toBeFalse();
                expect(r("A2:C2").differs(r("A2:C2"))).toBeFalse();
                expect(r("A2:C4").differs(r("A2:C4"))).toBeFalse();
                expect(range.differs(range)).toBeFalse();
            });
            it("should return true for different ranges", () => {
                expect(range.differs(r("A2:C5"))).toBeTrue();
                expect(range.differs(r("A2:D4"))).toBeTrue();
                expect(range.differs(r("A3:C4"))).toBeTrue();
                expect(range.differs(r("B2:C4"))).toBeTrue();
            });
        });

        describe("method compareTo", () => {
            const r1 = r("C3:E5");
            it("should exist", () => {
                expect(Range).toHaveMethod("compareTo");
            });
            it("should return 0 for equal ranges", () => {
                expect(r1.compareTo(r1)).toBe(0);
            });
            it("should return negative value for ranges in order", () => {
                expect(r1.compareTo(r("B4:D4"))).toBe(-1);
                expect(r1.compareTo(r("D3:D4"))).toBe(-1);
                expect(r1.compareTo(r("C3:D6"))).toBe(-1);
                expect(r1.compareTo(r("C3:F5"))).toBe(-1);
            });
            it("should return positive value for ranges in reversed order", () => {
                expect(r1.compareTo(r("D2:F6"))).toBe(1);
                expect(r1.compareTo(r("B3:F6"))).toBe(1);
                expect(r1.compareTo(r("C3:F4"))).toBe(1);
                expect(r1.compareTo(r("C3:D5"))).toBe(1);
            });
        });

        describe("method cols", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("cols");
            });
            it("should return the correct count", () => {
                expect(r("A2:A3").cols()).toBe(1);
                expect(r("C2:C3").cols()).toBe(1);
                expect(r("C2:D3").cols()).toBe(2);
                expect(r("C2:E3").cols()).toBe(3);
            });
        });

        describe("method rows", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("rows");
            });
            it("should return the correct count", () => {
                expect(r("B1:C1").rows()).toBe(1);
                expect(r("B3:C3").rows()).toBe(1);
                expect(r("B3:C4").rows()).toBe(2);
                expect(r("B3:C5").rows()).toBe(3);
            });
        });

        describe("method size", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("size");
            });
            it("should return the correct coumn count", () => {
                expect(r("B1:C1").size(true)).toBe(2);
            });
            it("should return the correct row count", () => {
                expect(r("B1:C1").size(false)).toBe(1);
            });
        });

        describe("method cells", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("cells");
            });
            it("should return the correct count", () => {
                expect(r("A1:A1").cells()).toBe(1);
                expect(r("C3:C3").cells()).toBe(1);
                expect(r("C3:D3").cells()).toBe(2);
                expect(r("C3:E3").cells()).toBe(3);
                expect(r("C3:C4").cells()).toBe(2);
                expect(r("C3:C5").cells()).toBe(3);
                expect(r("C3:D4").cells()).toBe(4);
                expect(r("C3:E5").cells()).toBe(9);
            });
        });

        describe("method singleCol", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("singleCol");
            });
            it("should return the correct result", () => {
                expect(r("B3:B3").singleCol()).toBeTrue();
                expect(r("B3:D3").singleCol()).toBeFalse();
                expect(r("B3:B5").singleCol()).toBeTrue();
                expect(r("B3:D5").singleCol()).toBeFalse();
            });
        });

        describe("method singleRow", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("singleRow");
            });
            it("should return the correct result", () => {
                expect(r("B3:B3").singleRow()).toBeTrue();
                expect(r("B3:D3").singleRow()).toBeTrue();
                expect(r("B3:B5").singleRow()).toBeFalse();
                expect(r("B3:D5").singleRow()).toBeFalse();
            });
        });

        describe("method singleLine", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("singleLine");
            });
            it("should return the correct result for columns", () => {
                expect(r("B3:B3").singleLine(true)).toBeTrue();
                expect(r("B3:D3").singleLine(true)).toBeFalse();
                expect(r("B3:B5").singleLine(true)).toBeTrue();
                expect(r("B3:D5").singleLine(true)).toBeFalse();
            });
            it("should return the correct result for rows", () => {
                expect(r("B3:B3").singleLine(false)).toBeTrue();
                expect(r("B3:D3").singleLine(false)).toBeTrue();
                expect(r("B3:B5").singleLine(false)).toBeFalse();
                expect(r("B3:D5").singleLine(false)).toBeFalse();
            });
        });

        describe("method single", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("single");
            });
            it("should return the correct result", () => {
                expect(r("B3:B3").single()).toBeTrue();
                expect(r("B3:D3").single()).toBeFalse();
                expect(r("B3:B5").single()).toBeFalse();
                expect(r("B3:D5").single()).toBeFalse();
            });
        });

        describe("method equalSize", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("equalSize");
            });
            it("should return the correct result", () => {
                const r1 = r("B3:D5");
                expect(r1.equalSize(r("B3:D5"))).toBeTrue();
                expect(r1.equalSize(r("C3:E5"))).toBeTrue();
                expect(r1.equalSize(r("B4:D6"))).toBeTrue();
                expect(r1.equalSize(r("B3:C5"))).toBeFalse();
                expect(r1.equalSize(r("B3:E5"))).toBeFalse();
                expect(r1.equalSize(r("B3:D4"))).toBeFalse();
                expect(r1.equalSize(r("B3:D6"))).toBeFalse();
            });
        });

        describe("method equalCols", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("equalCols");
            });
            it("should return the correct result", () => {
                const r1 = r("B3:D5");
                expect(r1.equalCols(r("B3:D5"))).toBeTrue();
                expect(r1.equalCols(r("B2:D6"))).toBeTrue();
                expect(r1.equalCols(r("B3:C5"))).toBeFalse();
                expect(r1.equalCols(r("B3:E5"))).toBeFalse();
                expect(r1.equalCols(r("A3:D5"))).toBeFalse();
                expect(r1.equalCols(r("C3:D5"))).toBeFalse();
                expect(r1.equalCols(r("A3:E5"))).toBeFalse();
            });
        });

        describe("method equalRows", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("equalRows");
            });
            it("should return the correct result", () => {
                const r1 = r("B3:D5");
                expect(r1.equalRows(r("B3:D5"))).toBeTrue();
                expect(r1.equalRows(r("A3:E5"))).toBeTrue();
                expect(r1.equalRows(r("B3:D4"))).toBeFalse();
                expect(r1.equalRows(r("B3:D6"))).toBeFalse();
                expect(r1.equalRows(r("B2:D5"))).toBeFalse();
                expect(r1.equalRows(r("B4:D5"))).toBeFalse();
                expect(r1.equalRows(r("B2:D6"))).toBeFalse();
            });
        });

        describe("method sizeFitsInto", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("sizeFitsInto");
            });
            const r1 = r("B1:E6");
            it("should return false for mismatching sizes", () => {
                expect(r("A1:C1").sizeFitsInto(r1)).toBeFalse();
                expect(r("A1:A4").sizeFitsInto(r1)).toBeFalse();
            });
            it("should return true for matching sizes", () => {
                expect(r("A1:A1").sizeFitsInto(r1)).toBeTrue();
                expect(r("A1:B1").sizeFitsInto(r1)).toBeTrue();
                expect(r("A1:D1").sizeFitsInto(r1)).toBeTrue();
                expect(r("A1:A2").sizeFitsInto(r1)).toBeTrue();
                expect(r("A1:A3").sizeFitsInto(r1)).toBeTrue();
                expect(r("A1:A6").sizeFitsInto(r1)).toBeTrue();
            });
        });

        describe("method startsAt", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("startsAt");
            });
            it("should return the correct result", () => {
                const r1 = r("B3:D5");
                expect(r1.startsAt(a("B3"))).toBeTrue();
                expect(r1.startsAt(a("B2"))).toBeFalse();
                expect(r1.startsAt(a("B4"))).toBeFalse();
                expect(r1.startsAt(a("A3"))).toBeFalse();
                expect(r1.startsAt(a("D3"))).toBeFalse();
                expect(r1.startsAt(a("D5"))).toBeFalse();
            });
        });

        describe("method endsAt", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("endsAt");
            });
            it("should return the correct result", () => {
                const r1 = r("B3:D5");
                expect(r1.endsAt(a("D5"))).toBeTrue();
                expect(r1.endsAt(a("D4"))).toBeFalse();
                expect(r1.endsAt(a("D6"))).toBeFalse();
                expect(r1.endsAt(a("C5"))).toBeFalse();
                expect(r1.endsAt(a("E5"))).toBeFalse();
                expect(r1.endsAt(a("B3"))).toBeFalse();
            });
        });

        describe("method indexAt", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("indexAt");
            });
            it("should return the correct result", () => {
                const r1 = r("B3:D5");
                expect(r1.indexAt(a("B3"))).toBe(0);
                expect(r1.indexAt(a("C3"))).toBe(1);
                expect(r1.indexAt(a("D3"))).toBe(2);
                expect(r1.indexAt(a("B4"))).toBe(3);
                expect(r1.indexAt(a("C4"))).toBe(4);
                expect(r1.indexAt(a("D4"))).toBe(5);
                expect(r1.indexAt(a("B5"))).toBe(6);
                expect(r1.indexAt(a("C5"))).toBe(7);
                expect(r1.indexAt(a("D5"))).toBe(8);
            });
        });

        describe("method addressAt", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("addressAt");
            });
            it("should return the correct result", () => {
                const r1 = r("B3:D5");
                expect(r1.addressAt(0)).toEqual(a("B3"));
                expect(r1.addressAt(1)).toEqual(a("C3"));
                expect(r1.addressAt(2)).toEqual(a("D3"));
                expect(r1.addressAt(3)).toEqual(a("B4"));
                expect(r1.addressAt(4)).toEqual(a("C4"));
                expect(r1.addressAt(5)).toEqual(a("D4"));
                expect(r1.addressAt(6)).toEqual(a("B5"));
                expect(r1.addressAt(7)).toEqual(a("C5"));
                expect(r1.addressAt(8)).toEqual(a("D5"));
            });
        });

        describe("method containsCol", () => {
            const range = r("C2:D9");
            it("should exist", () => {
                expect(Range).toHaveMethod("containsCol");
            });
            it("should return false for columns left of the range", () => {
                expect(range.containsCol(0)).toBeFalse();
                expect(range.containsCol(1)).toBeFalse();
            });
            it("should return true for columns inside the range", () => {
                expect(range.containsCol(2)).toBeTrue();
                expect(range.containsCol(3)).toBeTrue();
            });
            it("should return false for columns right of the range", () => {
                expect(range.containsCol(4)).toBeFalse();
                expect(range.containsCol(9)).toBeFalse();
            });
        });

        describe("method containsRow", () => {
            const range = r("B3:I4");
            it("should exist", () => {
                expect(Range).toHaveMethod("containsRow");
            });
            it("should return false for rows above the range", () => {
                expect(range.containsRow(0)).toBeFalse();
                expect(range.containsRow(1)).toBeFalse();
            });
            it("should return true for rows inside the range", () => {
                expect(range.containsRow(2)).toBeTrue();
                expect(range.containsRow(3)).toBeTrue();
            });
            it("should return false for rows below the range", () => {
                expect(range.containsRow(4)).toBeFalse();
                expect(range.containsRow(9)).toBeFalse();
            });
        });

        describe("method containsIndex", () => {
            const range = r("C2:C2");
            it("should exist", () => {
                expect(Range).toHaveMethod("containsIndex");
            });
            it("should check column indexes", () => {
                expect(range.containsIndex(1, true)).toBeFalse();
                expect(range.containsIndex(2, true)).toBeTrue();
                expect(range.containsIndex(3, true)).toBeFalse();
            });
            it("should check row indexes", () => {
                expect(range.containsIndex(0, false)).toBeFalse();
                expect(range.containsIndex(1, false)).toBeTrue();
                expect(range.containsIndex(2, false)).toBeFalse();
            });
        });

        describe("method containsAddress", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("containsAddress");
            });
            const range = r("B4:C5");
            it("should return false for cells outside the range", () => {
                "A3 B3 C3 D3 A4 D4 A5 D5 A6 B6 C6 D6".split(" ").forEach(text => {
                    expect(range.containsAddress(a(text))).toBeFalse();
                });
            });
            it("should return true for cells inside the range", () => {
                "B4 C4 B5 C5".split(" ").forEach(text => {
                    expect(range.containsAddress(a(text))).toBeTrue();
                });
            });
        });

        describe("method isBorderAddress", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("isBorderAddress");
            });
            const range = r("B4:D6");
            it("should return false for outer cells", () => {
                "A3 B3 C3 D3 E3 A4 E4 A5 E5 A6 E6 A7 B7 C7 D7 E7".split(" ").map(a).forEach(function (address) {
                    expect(range.isBorderAddress(address)).toBeFalse();
                });
            });
            it("should return false for inner cells", () => {
                expect(range.isBorderAddress(a("C5"))).toBeFalse();
            });
            it("should return true for border cells", () => {
                "B4 C4 D4 B5 D5 B6 C6 D6".split(" ").map(a).forEach(function (address) {
                    expect(range.isBorderAddress(address)).toBeTrue();
                });
            });
        });

        describe("method contains", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("contains");
            });
            const range = r("C3:F6");
            it("should return false for ranges outside the range", () => {
                "A1:B8 G1:H8 A1:H2 A7:H8".split(" ").forEach(text => {
                    expect(range.contains(r(text))).toBeFalse();
                });
            });
            it("should return false for ranges partly overlapping the range", () => {
                "A1:C8 F1:H8 A1:H3 A6:H8 D1:E3 D6:E8 A4:C5 F5:H6".split(" ").forEach(text => {
                    expect(range.contains(r(text))).toBeFalse();
                });
            });
            it("should return true for ranges inside the range", () => {
                "C3:C3 C3:F3 F3:F3 F3:F6 F6:F6 C6:F6 C6:C6 C3:C6 C3:F6 D4:E5".split(" ").forEach(text => {
                    expect(range.contains(r(text))).toBeTrue();
                });
            });
        });

        describe("method overlaps", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("overlaps");
            });
            const range = r("B3:C4");
            it("should return false for distinct ranges", () => {
                expect(range.overlaps(r("D3:E4"))).toBeFalse();
                expect(r("D3:E4").overlaps(range)).toBeFalse();
                expect(range.overlaps(r("B5:C6"))).toBeFalse();
                expect(r("B5:C6").overlaps(range)).toBeFalse();
            });
            it("should return true for partly overlapping ranges", () => {
                expect(range.overlaps(r("C4:D5"))).toBeTrue();
                expect(range.overlaps(r("C2:D3"))).toBeTrue();
                expect(range.overlaps(r("A2:B3"))).toBeTrue();
                expect(range.overlaps(r("A4:B5"))).toBeTrue();
            });
            it("should return true for ranges containing each other", () => {
                expect(range.overlaps(r("B3:B3"))).toBeTrue();
                expect(range.overlaps(r("B3:B4"))).toBeTrue();
                expect(range.overlaps(r("B3:C4"))).toBeTrue();
                expect(r("B3:B4").overlaps(range)).toBeTrue();
                expect(r("B3:B3").overlaps(range)).toBeTrue();
                expect(range.overlaps(range)).toBeTrue();
            });
        });

        describe("method colInterval", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("colInterval");
            });
            it("should return the column interval", () => {
                expect(r("B3:B3").colInterval()).toEqual(i("B:B"));
                expect(r("B3:C4").colInterval()).toEqual(i("B:C"));
                expect(r("B3:D5").colInterval()).toEqual(i("B:D"));
                expect(r("C4:D5").colInterval()).toEqual(i("C:D"));
                expect(r("D5:D5").colInterval()).toEqual(i("D:D"));
            });
        });

        describe("method rowInterval", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("rowInterval");
            });
            it("should return the row interval", () => {
                expect(r("B3:B3").rowInterval()).toEqual(i("3:3"));
                expect(r("B3:C4").rowInterval()).toEqual(i("3:4"));
                expect(r("B3:D5").rowInterval()).toEqual(i("3:5"));
                expect(r("C4:D5").rowInterval()).toEqual(i("4:5"));
                expect(r("D5:D5").rowInterval()).toEqual(i("5:5"));
            });
        });

        describe("method interval", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("interval");
            });
            it("should return the column interval", () => {
                expect(r("B3:C4").interval(true)).toEqual(i("B:C"));
                expect(r("B3:D5").interval(true)).toEqual(i("B:D"));
                expect(r("C4:D5").interval(true)).toEqual(i("C:D"));
            });
            it("should return the row interval", () => {
                expect(r("B3:C4").interval(false)).toEqual(i("3:4"));
                expect(r("B3:D5").interval(false)).toEqual(i("3:5"));
                expect(r("C4:D5").interval(false)).toEqual(i("4:5"));
            });
        });

        describe("method colRange", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("colRange");
            });
            it("should return the column range", () => {
                expect(r("B3:D5").colRange(0)).toEqual(r("B3:B5"));
                expect(r("B3:D5").colRange(1)).toEqual(r("C3:C5"));
                expect(r("B3:D5").colRange(1, 2)).toEqual(r("C3:D5"));
            });
        });

        describe("method rowRange", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("rowRange");
            });
            it("should return the row range", () => {
                expect(r("B3:D5").rowRange(0)).toEqual(r("B3:D3"));
                expect(r("B3:D5").rowRange(1)).toEqual(r("B4:D4"));
                expect(r("B3:D5").rowRange(1, 2)).toEqual(r("B4:D5"));
            });
        });

        describe("method lineRange", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("lineRange");
            });
            it("should return the column range", () => {
                expect(r("B3:D5").lineRange(true, 0)).toEqual(r("B3:B5"));
                expect(r("B3:D5").lineRange(true, 1)).toEqual(r("C3:C5"));
                expect(r("B3:D5").lineRange(true, 1, 2)).toEqual(r("C3:D5"));
            });
            it("should return the row range", () => {
                expect(r("B3:D5").lineRange(false, 0)).toEqual(r("B3:D3"));
                expect(r("B3:D5").lineRange(false, 1)).toEqual(r("B4:D4"));
                expect(r("B3:D5").lineRange(false, 1, 2)).toEqual(r("B4:D5"));
            });
        });

        describe("method leadingCol", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("leadingCol");
            });
            it("should return the leading column range", () => {
                expect(r("B3:D5").leadingCol()).toEqual(r("B3:B5"));
            });
        });

        describe("method trailingCol", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("trailingCol");
            });
            it("should return the trailing column range", () => {
                expect(r("B3:D5").trailingCol()).toEqual(r("D3:D5"));
            });
        });

        describe("method headerRow", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("headerRow");
            });
            it("should return the header row range", () => {
                expect(r("B3:D5").headerRow()).toEqual(r("B3:D3"));
            });
        });

        describe("method footerRow", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("footerRow");
            });
            it("should return the footer row range", () => {
                expect(r("B3:D5").footerRow()).toEqual(r("B5:D5"));
            });
        });

        describe("method mirror", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("mirror");
            });
            it("should return the mirrored range", () => {
                expect(r("A2:D3").mirror()).toEqual(r("B1:C4"));
                expect(r("B1:C4").mirror()).toEqual(r("A2:D3"));
            });
        });

        describe("method expand", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("expand");
            });
            it("should return the expanded range", () => {
                const r1 = r("B3:D5"), r2 = r1.expand(a("C4"));
                expect(r2).toEqual(r1);
                expect(r2).not.toBe(r1);
                expect(r1.expand(a("A4"))).toEqual(r("A3:D5"));
                expect(r1.expand(a("A1"))).toEqual(r("A1:D5"));
                expect(r1.expand(a("C1"))).toEqual(r("B1:D5"));
                expect(r1.expand(a("E1"))).toEqual(r("B1:E5"));
                expect(r1.expand(a("E4"))).toEqual(r("B3:E5"));
                expect(r1.expand(a("E7"))).toEqual(r("B3:E7"));
                expect(r1.expand(a("C7"))).toEqual(r("B3:D7"));
                expect(r1.expand(a("A7"))).toEqual(r("A3:D7"));
            });
        });

        describe("method boundary", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("boundary");
            });
            it("should return the bounding range", () => {
                expect(r("B3:C4").boundary(r("C4:D5"))).toEqual(r("B3:D5"));
                expect(r("C3:D4").boundary(r("B4:C5"))).toEqual(r("B3:D5"));
                expect(r("B4:C5").boundary(r("C3:D4"))).toEqual(r("B3:D5"));
                expect(r("C4:D5").boundary(r("B3:C4"))).toEqual(r("B3:D5"));
                expect(r("C4:C4").boundary(r("B3:D5"))).toEqual(r("B3:D5"));
                expect(r("B3:D5").boundary(r("C4:C4"))).toEqual(r("B3:D5"));
            });
        });

        describe("method intersect", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("intersect");
            });
            it("should return null for distinct ranges", () => {
                expect(r("B3:C4").intersect(r("D3:E4"))).toBeNull();
                expect(r("D3:E4").intersect(r("B3:C4"))).toBeNull();
                expect(r("B3:C4").intersect(r("B5:C6"))).toBeNull();
                expect(r("B5:C6").intersect(r("B3:C4"))).toBeNull();
            });
            it("should return intersection range for overlapping ranges", () => {
                const range = r("B3:E5");
                expect(range.intersect(r("A2:B3"))).toEqual(r("B3:B3"));
                expect(range.intersect(r("A2:B6"))).toEqual(r("B3:B5"));
                expect(range.intersect(r("A4:C6"))).toEqual(r("B4:C5"));
                expect(range.intersect(r("A4:F4"))).toEqual(r("B4:E4"));
                expect(range.intersect(r("C1:D8"))).toEqual(r("C3:D5"));
                expect(range.intersect(r("A1:F8"))).toEqual(r("B3:E5"));
                expect(range.intersect(r("B3:E5"))).toEqual(r("B3:E5"));
                expect(range.intersect(r("C3:D5"))).toEqual(r("C3:D5"));
                expect(range.intersect(r("C4:C4"))).toEqual(r("C4:C4"));
                expect(range.intersect(range)).toEqual(range);
                expect(range.intersect(range)).not.toBe(range);
            });
        });

        describe("method getStart", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("getStart");
            });
            it("should return the correct index", () => {
                const r1 = r("B3:D5");
                expect(r1.getStart(true)).toBe(1);
                expect(r1.getStart(false)).toBe(2);
            });
        });

        describe("method getEnd", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("getEnd");
            });
            it("should return the correct index", () => {
                const r1 = r("B3:D5");
                expect(r1.getEnd(true)).toBe(3);
                expect(r1.getEnd(false)).toBe(4);
            });
        });

        describe("method setStart", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("setStart");
            });
            it("should change the correct index", () => {
                const r1 = r("A1:F6");
                expect(r1).toEqual(r("A1:F6"));
                r1.setStart(1, true);
                expect(r1).toEqual(r("B1:F6"));
                r1.setStart(2, false);
                expect(r1).toEqual(r("B3:F6"));
            });
            it("should return itself", () => {
                const r1 = r("A1:F6");
                expect(r1.setStart(1, true)).toBe(r1);
            });
        });

        describe("method setEnd", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("setEnd");
            });
            it("should change the correct index", () => {
                const r1 = r("A1:F6");
                expect(r1).toEqual(r("A1:F6"));
                r1.setEnd(3, true);
                expect(r1).toEqual(r("A1:D6"));
                r1.setEnd(4, false);
                expect(r1).toEqual(r("A1:D5"));
            });
            it("should return itself", () => {
                const r1 = r("A1:F6");
                expect(r1.setEnd(1, true)).toBe(r1);
            });
        });

        describe("method setBoth", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("setBoth");
            });
            it("should change the correct index", () => {
                const r1 = r("A1:F6");
                expect(r1).toEqual(r("A1:F6"));
                r1.setBoth(3, true);
                expect(r1).toEqual(r("D1:D6"));
                r1.setBoth(4, false);
                expect(r1).toEqual(r("D5:D5"));
            });
            it("should return itself", () => {
                const r1 = r("A1:F6");
                expect(r1.setBoth(1, true)).toBe(r1);
            });
        });

        describe("method moveBoth", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("moveBoth");
            });
            it("should change the correct indexes", () => {
                const r1 = r("A1:F6");
                expect(r1).toEqual(r("A1:F6"));
                r1.moveBoth(3, true);
                expect(r1).toEqual(r("D1:I6"));
                r1.moveBoth(4, false);
                expect(r1).toEqual(r("D5:I10"));
            });
            it("should return itself", () => {
                const r1 = r("A1:F6");
                expect(r1.moveBoth(1, true)).toBe(r1);
            });
        });

        describe("method move", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("move");
            });
            it("should change the correct indexes", () => {
                const r1 = r("A1:F6");
                expect(r1).toEqual(r("A1:F6"));
                r1.move(3, 4);
                expect(r1).toEqual(r("D5:I10"));
            });
            it("should return itself", () => {
                const r1 = r("A1:F6");
                expect(r1.move(1, 1)).toBe(r1);
            });
        });

        describe("method colIndexes", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("colIndexes");
            });
            it("should return a column index iterator", () => {
                const it = r("B3:C4").colIndexes();
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([1, 2]);
            });
            it("should return a reverse column index iterator", () => {
                const it = r("B3:C4").colIndexes({ reverse: true });
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([2, 1]);
            });
        });

        describe("method rowIndexes", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("rowIndexes");
            });
            it("should return a row index iterator", () => {
                const it = r("B3:C4").rowIndexes();
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([2, 3]);
            });
            it("should return a reverse row index iterator", () => {
                const it = r("B3:C4").rowIndexes({ reverse: true });
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([3, 2]);
            });
        });

        describe("method indexes", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("indexes");
            });
            it("should return a column index iterator", () => {
                const it = r("B3:C4").indexes(true);
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([1, 2]);
            });
            it("should return a reverse column index iterator", () => {
                const it = r("B3:C4").indexes(true, { reverse: true });
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([2, 1]);
            });
            it("should return a row index iterator", () => {
                const it = r("B3:C4").indexes(false);
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([2, 3]);
            });
            it("should return a reverse row index iterator", () => {
                const it = r("B3:C4").indexes(false, { reverse: true });
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([3, 2]);
            });
        });

        describe("method addresses", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("addresses");
            });
            it("should return a horizontal address iterator", () => {
                const it = r("B3:C4").addresses();
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([a("B3"), a("C3"), a("B4"), a("C4")]);
            });
            it("should return a horizontal reverse address iterator", () => {
                const it = r("B3:C4").addresses({ reverse: true });
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([a("C4"), a("B4"), a("C3"), a("B3")]);
            });
            it("should return a vertical address iterator", () => {
                const it = r("B3:C4").addresses({ columns: true });
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([a("B3"), a("B4"), a("C3"), a("C4")]);
            });
            it("should return a vertical reverse address iterator", () => {
                const it = r("B3:C4").addresses({ reverse: true, columns: true });
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([a("C4"), a("C3"), a("B4"), a("B3")]);
            });
        });

        describe("method toOpStr", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("toOpStr");
            });
            it("should stringify the range without maximum address", () => {
                expect(r("A2:A2").toOpStr()).toBe("A2");
                expect(r("A2:C4").toOpStr()).toBe("A2:C4");
                expect(r("A1:IV1").toOpStr()).toBe("A1:IV1");
                expect(r("A2:IV4").toOpStr()).toBe("A2:IV4");
                expect(r("A1:A256").toOpStr()).toBe("A1:A256");
                expect(r("A1:C256").toOpStr()).toBe("A1:C256");
                expect(r("A1:IV256").toOpStr()).toBe("A1:IV256");
                expect(r("A1:ZZZZ99999999").toOpStr()).toBe("A1:ZZZZ99999999");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(Range).toHaveMethod("toString");
            });
            it("should stringify the range", () => {
                expect(r("A2:C4").toString()).toBe("A2:C4");
                expect(r("A1:ZZZZ99999999").toString()).toBe("A1:ZZZZ99999999");
            });
            it("should stringify implicitly", () => {
                expect("<" + r("A2:C4") + ">").toBe("<A2:C4>");
            });
        });
    });
});
