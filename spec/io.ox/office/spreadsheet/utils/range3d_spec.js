/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { Range3D } from "@/io.ox/office/spreadsheet/utils/range3d";

import { i, a, r, r3d } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/range3d", () => {

    // class Range3D ----------------------------------------------------------

    describe("class Range3D", () => {

        it("should subclass Range", () => {
            expect(Range3D).toBeSubClassOf(Range);
        });

        describe("constructor", () => {
            const a1 = a("A2"), a2 = a("C4");
            it("should create a range address", () => {
                const r1 = new Range3D(6, 8, a1, a2);
                expect(r1).toHaveProperty("sheet1", 6);
                expect(r1).toHaveProperty("sheet2", 8);
                expect(r1).toHaveProperty("a1", a1);
                expect(r1).toHaveProperty("a2", a2);
            });
            it("should create a range address from a single address", () => {
                const r1 = new Range3D(6, 8, a1);
                expect(r1).toHaveProperty("sheet1", 6);
                expect(r1).toHaveProperty("sheet2", 8);
                expect(r1).toHaveProperty("a1", a1);
                expect(r1).toHaveProperty("a2");
                expect(r1.a2).toEqual(a1);
                expect(r1.a2).not.toBe(a1);
            });
        });

        describe("static function fromIndexes3D", () => {
            it("should exist", () => {
                expect(Range3D).toHaveStaticMethod("fromIndexes3D");
            });
            it("should return the adjusted range address", () => {
                const r1 = new Range3D(6, 8, a("A2"), a("C4"));
                expect(Range3D.fromIndexes3D(0, 1, 2, 3, 6, 8)).toEqual(r1);
                expect(Range3D.fromIndexes3D(2, 1, 0, 3, 8, 6)).toEqual(r1);
                expect(Range3D.fromIndexes3D(0, 3, 2, 1, 6, 8)).toEqual(r1);
                expect(Range3D.fromIndexes3D(2, 3, 0, 1, 8, 6)).toEqual(r1);
            });
            it("should accept single sheet index", () => {
                const r1 = new Range3D(6, 6, a("A2"), a("C4"));
                expect(Range3D.fromIndexes3D(0, 1, 2, 3, 6) + "!").toEqual(r1 + "!");
            });
        });

        describe("static function fromAddress3D", () => {
            it("should exist", () => {
                expect(Range3D).toHaveStaticMethod("fromAddress3D");
            });
            const a1 = a("A2"), r1 = new Range3D(6, 8, a1);
            it("should return a range", () => {
                expect(Range3D.fromAddress3D(a1, 6, 8)).toBeInstanceOf(Range3D);
            });
            it("should return a range with adjusted sheet indexes", () => {
                expect(Range3D.fromAddress3D(a1, 6, 8)).toEqual(r1);
                expect(Range3D.fromAddress3D(a1, 8, 6)).toEqual(r1);
            });
            it("should accept single sheet index", () => {
                const r2 = Range3D.fromAddress3D(a1, 6);
                expect(r2).toHaveProperty("sheet1", 6);
                expect(r2).toHaveProperty("sheet2", 6);
            });
            it("should clone the address", () => {
                const r2 = Range3D.fromAddress3D(a1, 6, 8);
                expect(r2.a1).toEqual(a1);
                expect(r2.a1).not.toBe(a1);
                expect(r2.a2).toEqual(a1);
                expect(r2.a2).not.toBe(a1);
            });
        });

        describe("static function fromAddresses3D", () => {
            it("should exist", () => {
                expect(Range3D).toHaveStaticMethod("fromAddresses3D");
            });
            const a1 = a("A2"), a2 = a("C4"), r1 = new Range3D(6, 8, a1, a2);
            it("should return a range", () => {
                expect(Range3D.fromAddresses3D(a1, a2, 6, 8)).toBeInstanceOf(Range3D);
            });
            it("should return a range with adjusted indexes", () => {
                expect(Range3D.fromAddresses3D(a1, a2, 6, 8)).toEqual(r1);
                expect(Range3D.fromAddresses3D(a2, a1, 8, 6)).toEqual(r1);
            });
            it("should accept single sheet index", () => {
                const r2 = Range3D.fromAddresses3D(a1, a2, 6);
                expect(r2).toHaveProperty("sheet1", 6);
                expect(r2).toHaveProperty("sheet2", 6);
            });
            it("should clone the addresses", () => {
                const r2 = Range3D.fromAddresses3D(a1, a2, 6, 8);
                expect(r2.a1).toEqual(a1);
                expect(r2.a1).not.toBe(a1);
                expect(r2.a2).toEqual(a2);
                expect(r2.a2).not.toBe(a2);
            });
        });

        describe("static function fromRange3D", () => {
            it("should exist", () => {
                expect(Range3D).toHaveStaticMethod("fromRange3D");
            });
            const r1 = r("A2:C4"), r2 = new Range3D(6, 8, r1.a1, r1.a2);
            it("should return a range", () => {
                expect(Range3D.fromRange3D(r1, 6, 8)).toBeInstanceOf(Range3D);
            });
            it("should return a range with adjusted sheet indexes", () => {
                expect(Range3D.fromRange3D(r1, 6, 8)).toEqual(r2);
                expect(Range3D.fromRange3D(r1, 8, 6)).toEqual(r2);
            });
            it("should accept single sheet index", () => {
                const r3 = Range3D.fromRange3D(r1, 6);
                expect(r3).toHaveProperty("sheet1", 6);
                expect(r3).toHaveProperty("sheet2", 6);
            });
            it("should clone addresses", () => {
                const r3 = Range3D.fromRange3D(r1, 6, 8);
                expect(r3.a1).toEqual(r2.a1);
                expect(r3.a1).not.toBe(r2.a1);
                expect(r3.a2).toEqual(r2.a2);
                expect(r3.a2).not.toBe(r2.a2);
            });
        });

        describe("static function fromIntervals3D", () => {
            it("should exist", () => {
                expect(Range3D).toHaveStaticMethod("fromIntervals3D");
            });
            it("should create range address from intervals", () => {
                const r1 = Range3D.fromIntervals3D(i("B:C"), i("3:4"), 6, 8);
                expect(r1.a1).toEqual(a("B3"));
                expect(r1.a2).toEqual(a("C4"));
                expect(r1).toHaveProperty("sheet1", 6);
                expect(r1).toHaveProperty("sheet2", 8);
            });
            it("should accept single sheet index", () => {
                const r1 = Range3D.fromIntervals3D(i("B:C"), i("3:4"), 6);
                expect(r1.a1).toEqual(a("B3"));
                expect(r1.a2).toEqual(a("C4"));
                expect(r1).toHaveProperty("sheet1", 6);
                expect(r1).toHaveProperty("sheet2", 6);
            });
        });

        describe("property key", () => {
            it("should return unique key for range address", () => {
                expect(r3d("0:0!A2:A2").key).toBe("0:0!0,1:0,1");
                expect(r3d("5:6!B3:D5").key).toBe("5:6!1,2:3,4");
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("clone");
            });
            it("should return a clone", () => {
                const r1 = r3d("6:8!A2:C4"), r2 = r1.clone();
                expect(r2).toBeInstanceOf(Range3D);
                expect(r2).not.toBe(r1);
                expect(r2.a1).not.toBe(r1.a1);
                expect(r2.a2).not.toBe(r1.a2);
                expect(r2).toEqual(r1);
            });
        });

        describe("method equals", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("equals");
            });
            const range = r3d("6:8!A2:C4");
            it("should return true for equal ranges", () => {
                expect(r3d("1:1!A2:A4").equals(r3d("1:1!A2:A4"))).toBeTrue();
                expect(r3d("2:3!A2:C2").equals(r3d("2:3!A2:C2"))).toBeTrue();
                expect(r3d("6:8!A2:C4").equals(r3d("6:8!A2:C4"))).toBeTrue();
                expect(range.equals(range)).toBeTrue();
            });
            it("should return false for different ranges", () => {
                expect(range.equals(r3d("5:8!A2:C4"))).toBeFalse();
                expect(range.equals(r3d("6:7!A2:C4"))).toBeFalse();
                expect(range.equals(r3d("6:8!A2:C5"))).toBeFalse();
                expect(range.equals(r3d("6:8!A2:D4"))).toBeFalse();
                expect(range.equals(r3d("6:8!A3:C4"))).toBeFalse();
                expect(range.equals(r3d("6:8!B2:C4"))).toBeFalse();
            });
        });

        describe("method differs", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("differs");
            });
            const range = r3d("6:8!A2:C4");
            it("should return false for equal ranges", () => {
                expect(r3d("1:1!A2:A4").differs(r3d("1:1!A2:A4"))).toBeFalse();
                expect(r3d("2:3!A2:C2").differs(r3d("2:3!A2:C2"))).toBeFalse();
                expect(r3d("6:8!A2:C4").differs(r3d("6:8!A2:C4"))).toBeFalse();
                expect(range.differs(range)).toBeFalse();
            });
            it("should return true for different ranges", () => {
                expect(range.differs(r3d("5:8!A2:C4"))).toBeTrue();
                expect(range.differs(r3d("6:7!A2:C4"))).toBeTrue();
                expect(range.differs(r3d("6:8!A2:C5"))).toBeTrue();
                expect(range.differs(r3d("6:8!A2:D4"))).toBeTrue();
                expect(range.differs(r3d("6:8!A3:C4"))).toBeTrue();
                expect(range.differs(r3d("6:8!B2:C4"))).toBeTrue();
            });
        });

        describe("method sheets", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("sheets");
            });
            it("should return the correct sheet count", () => {
                expect(r3d("0:0!A2:C4").sheets()).toBe(1);
                expect(r3d("0:1!A2:C4").sheets()).toBe(2);
                expect(r3d("1:1!A2:C4").sheets()).toBe(1);
                expect(r3d("1:2!A2:C4").sheets()).toBe(2);
                expect(r3d("1:3!A2:C4").sheets()).toBe(3);
                expect(r3d("1:4!A2:C4").sheets()).toBe(4);
            });
        });

        describe("method cells", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("cells");
            });
            it("should return the correct count", () => {
                expect(r3d("0:0!A1:A1").cells()).toBe(1);
                expect(r3d("1:2!C3:C3").cells()).toBe(2);
                expect(r3d("2:4!C3:D3").cells()).toBe(6);
                expect(r3d("2:5!C3:E5").cells()).toBe(36);
            });
        });

        describe("method singleSheet", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("singleSheet");
            });
            it("should return true for equals sheets", () => {
                expect(r3d("1:1!A2:A4").singleSheet()).toBeTrue();
                expect(r3d("6:6!A2:A4").singleSheet()).toBeTrue();
            });
            it("should return false for different sheets", () => {
                expect(r3d("1:2!A2:A4").singleSheet()).toBeFalse();
                expect(r3d("6:8!A2:A4").singleSheet()).toBeFalse();
            });
        });

        describe("method isSheet", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("isSheet");
            });
            it("should return true for correct sheet", () => {
                expect(r3d("1:1!A2:A4").isSheet(1)).toBeTrue();
                expect(r3d("6:6!A2:A4").isSheet(6)).toBeTrue();
            });
            it("should return false for other sheets", () => {
                expect(r3d("1:1!A2:A4").isSheet(0)).toBeFalse();
                expect(r3d("1:1!A2:A4").isSheet(2)).toBeFalse();
                expect(r3d("6:6!A2:A4").isSheet(5)).toBeFalse();
                expect(r3d("6:6!A2:A4").isSheet(7)).toBeFalse();
            });
            it("should return false for mult-sheet ranges", () => {
                const r1 = r3d("6:8!A2:A4");
                expect(r1.isSheet(5)).toBeFalse();
                expect(r1.isSheet(6)).toBeFalse();
                expect(r1.isSheet(7)).toBeFalse();
                expect(r1.isSheet(8)).toBeFalse();
                expect(r1.isSheet(9)).toBeFalse();
            });
        });

        describe("method containsSheet", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("containsSheet");
            });
            const range = r3d("6:8!A2:A4");
            it("should return true for inner sheet indexes", () => {
                expect(range.containsSheet(6)).toBeTrue();
                expect(range.containsSheet(7)).toBeTrue();
                expect(range.containsSheet(8)).toBeTrue();
            });
            it("should return false for outer sheet indexes", () => {
                expect(range.containsSheet(4)).toBeFalse();
                expect(range.containsSheet(5)).toBeFalse();
                expect(range.containsSheet(9)).toBeFalse();
                expect(range.containsSheet(10)).toBeFalse();
            });
        });

        describe("method contains", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("contains");
            });
            const range = r3d("6:8!C3:F6");
            it("should return false for ranges outside the range", () => {
                expect(range.contains(r3d("7:7!A1:B2"))).toBeFalse();
                expect(range.contains(r3d("4:5!D4:E5"))).toBeFalse();
            });
            it("should return false for ranges partly overlapping the range", () => {
                expect(range.contains(r3d("7:7!B2:D4"))).toBeFalse();
                expect(range.contains(r3d("5:7!D4:E5"))).toBeFalse();
                expect(range.contains(r3d("7:9!D4:E5"))).toBeFalse();
            });
            it("should return true for ranges inside the range", () => {
                expect(range.contains(r3d("7:7!D4:E5"))).toBeTrue();
                expect(range.contains(r3d("6:7!D4:E5"))).toBeTrue();
                expect(range.contains(r3d("7:8!D4:E5"))).toBeTrue();
                expect(range.contains(r3d("6:8!D4:E5"))).toBeTrue();
            });
        });

        describe("method overlaps", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("overlaps");
            });
            const range = r3d("6:8!C3:F6");
            it("should return false for distinct ranges", () => {
                expect(range.overlaps(r3d("5:7!A1:B2"))).toBeFalse();
                expect(range.overlaps(r3d("7:9!G7:H8"))).toBeFalse();
                expect(range.overlaps(r3d("5:5!B2:D4"))).toBeFalse();
                expect(range.overlaps(r3d("9:9!E5:G7"))).toBeFalse();
            });
            it("should return true for ranges partly overlapping the range", () => {
                expect(range.overlaps(r3d("5:7!B2:D4"))).toBeTrue();
                expect(range.overlaps(r3d("7:9!E5:G7"))).toBeTrue();
            });
            it("should return true for ranges containing each other", () => {
                expect(range.overlaps(r3d("7:7!D4:E5"))).toBeTrue();
                expect(range.overlaps(r3d("5:9!B2:G7"))).toBeTrue();
            });
        });

        describe("method colRange", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("colRange");
            });
            it("should return the column range", () => {
                expect(r3d("2:3!B3:D5").colRange(0)).toEqual(r3d("2:3!B3:B5"));
                expect(r3d("2:3!B3:D5").colRange(1)).toEqual(r3d("2:3!C3:C5"));
                expect(r3d("2:3!B3:D5").colRange(1, 2)).toEqual(r3d("2:3!C3:D5"));
            });
        });

        describe("method rowRange", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("rowRange");
            });
            it("should return the row range", () => {
                expect(r3d("2:3!B3:D5").rowRange(0)).toEqual(r3d("2:3!B3:D3"));
                expect(r3d("2:3!B3:D5").rowRange(1)).toEqual(r3d("2:3!B4:D4"));
                expect(r3d("2:3!B3:D5").rowRange(1, 2)).toEqual(r3d("2:3!B4:D5"));
            });
        });

        describe("method lineRange", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("lineRange");
            });
            it("should return the column range", () => {
                expect(r3d("2:3!B3:D5").lineRange(true, 0)).toEqual(r3d("2:3!B3:B5"));
                expect(r3d("2:3!B3:D5").lineRange(true, 1)).toEqual(r3d("2:3!C3:C5"));
                expect(r3d("2:3!B3:D5").lineRange(true, 1, 2)).toEqual(r3d("2:3!C3:D5"));
            });
            it("should return the row range", () => {
                expect(r3d("2:3!B3:D5").lineRange(false, 0)).toEqual(r3d("2:3!B3:D3"));
                expect(r3d("2:3!B3:D5").lineRange(false, 1)).toEqual(r3d("2:3!B4:D4"));
                expect(r3d("2:3!B3:D5").lineRange(false, 1, 2)).toEqual(r3d("2:3!B4:D5"));
            });
        });

        describe("method boundary", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("boundary");
            });
            it("should return the bounding interval", () => {
                expect(r3d("1:1!B3:C4").boundary(r3d("1:1!C4:D5"))).toEqual(r3d("1:1!B3:D5"));
                expect(r3d("1:1!C3:D4").boundary(r3d("3:3!B4:C5"))).toEqual(r3d("1:3!B3:D5"));
                expect(r3d("3:3!B4:C5").boundary(r3d("1:1!C3:D4"))).toEqual(r3d("1:3!B3:D5"));
                expect(r3d("1:3!C4:D5").boundary(r3d("2:4!B3:C4"))).toEqual(r3d("1:4!B3:D5"));
                expect(r3d("2:4!C4:C4").boundary(r3d("1:3!B3:D5"))).toEqual(r3d("1:4!B3:D5"));
                expect(r3d("1:4!B3:D5").boundary(r3d("2:3!C4:C4"))).toEqual(r3d("1:4!B3:D5"));
            });
        });

        describe("method intersect", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("intersect");
            });
            it("should return null for distinct ranges", () => {
                expect(r3d("6:8!B3:C4").intersect(r3d("6:8!D3:E4"))).toBeNull();
                expect(r3d("6:8!D3:E4").intersect(r3d("6:8!B3:C4"))).toBeNull();
                expect(r3d("6:8!B3:C4").intersect(r3d("6:8!B5:C6"))).toBeNull();
                expect(r3d("6:8!B5:C6").intersect(r3d("6:8!B3:C4"))).toBeNull();
                expect(r3d("1:2!B3:C4").intersect(r3d("6:8!B3:C4"))).toBeNull();
            });
            it("should return intersection range for overlapping ranges", () => {
                const range = r3d("6:8!B3:E5");
                expect(range.intersect(r3d("5:7!A2:B3"))).toEqual(r3d("6:7!B3:B3"));
                expect(range.intersect(r3d("7:9!A2:B6"))).toEqual(r3d("7:8!B3:B5"));
                expect(range.intersect(r3d("7:7!A4:C6"))).toEqual(r3d("7:7!B4:C5"));
                expect(range.intersect(r3d("5:9!A4:F4"))).toEqual(r3d("6:8!B4:E4"));
                expect(range.intersect(r3d("6:8!C1:D8"))).toEqual(r3d("6:8!C3:D5"));
                expect(range.intersect(r3d("6:8!A1:F8"))).toEqual(r3d("6:8!B3:E5"));
                expect(range.intersect(r3d("6:8!B3:E5"))).toEqual(r3d("6:8!B3:E5"));
                expect(range.intersect(r3d("6:8!C3:D5"))).toEqual(r3d("6:8!C3:D5"));
                expect(range.intersect(r3d("6:8!C4:C4"))).toEqual(r3d("6:8!C4:C4"));
                expect(range.intersect(range)).toEqual(range);
                expect(range.intersect(range)).not.toBe(range);
            });
        });

        describe("method toRange", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("toRange");
            });
            it("should convert the range to a 2D range", () => {
                const r1 = r3d("6:8!A2:C4").toRange();
                expect(r1).toBeInstanceOf(Range);
                expect(r1).toEqual(r("A2:C4"));
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(Range3D).toHaveMethod("toString");
            });
            it("should stringify the range", () => {
                expect(r3d("6:8!A2:C4").toString()).toBe("6:8!A2:C4");
            });
            it("should stringify implicitly", () => {
                expect("<" + r3d("6:8!A2:C4") + ">").toBe("<6:8!A2:C4>");
            });
        });
    });
});
