/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as iterator from "@/io.ox/office/spreadsheet/utils/iterator";

// constants ==================================================================

const UD = undefined; // shortcut for "undefined"

// tests ======================================================================

describe("module spreadsheet/utils/iterator", () => {

    describe("function iterateParallelSynchronized", () => {
        const { iterateParallelSynchronized } = iterator;
        it("should exist", () => {
            expect(iterateParallelSynchronized).toBeFunction();
        });
        it("should create a parallel iterator", () => {
            const s1 = [0, 2, 4];
            const s2 = new Set([1, 2, 3]);
            const s3 = [3, 4, 7].values();
            const spy = jest.fn((v, i) => v - i);
            const it = iterateParallelSynchronized([s1, s2, s3], spy);
            expect(it).toBeIterator();
            expect(Array.from(it)).toEqual([
                { values: [0,  1, UD], offset: 0, complete: false, first: 0 },
                { values: [UD, 2,  3], offset: 1, complete: false, first: 2 },
                { values: [2,  3,  4], offset: 2, complete: true,  first: 2 },
                { values: [4, UD, UD], offset: 4, complete: false, first: 4 },
                { values: [UD, UD, 7], offset: 5, complete: false, first: 7 }
            ]);
            expect(spy).toHaveBeenNthCalledWith(1, 0, 0);
            expect(spy).toHaveBeenNthCalledWith(2, 1, 1);
            expect(spy).toHaveBeenNthCalledWith(3, 3, 2);
        });
    });
});
