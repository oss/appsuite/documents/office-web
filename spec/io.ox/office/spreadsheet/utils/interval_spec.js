/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { intervalKey, Interval } from "@/io.ox/office/spreadsheet/utils/interval";

import { i } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/interval", () => {

    // functions --------------------------------------------------------------

    describe("function intervalKey", () => {
        it("should exist", () => {
            expect(intervalKey).toBeFunction();
        });
        it("should return unique key for interval", () => {
            expect(intervalKey(0, 1)).toBe("0:1");
            expect(intervalKey(1, 2)).toBe("1:2");
        });
    });

    // class Interval ---------------------------------------------------------

    describe("class Interval", () => {

        it("should exist", () => {
            expect(Interval).toBeFunction();
        });

        describe("constructor", () => {
            it("should create an interval", () => {
                const i1 = new Interval(1, 3);
                expect(i1).toHaveProperty("first", 1);
                expect(i1).toHaveProperty("last", 3);
            });
            it("should create an interval from a single parameter", () => {
                const i1 = new Interval(1);
                expect(i1).toHaveProperty("first", 1);
                expect(i1).toHaveProperty("last", 1);
            });
        });

        describe("static function create", () => {
            it("should exist", () => {
                expect(Interval).toHaveStaticMethod("create");
            });
            const i1 = new Interval(0, 1);
            it("should return the adjusted interval", () => {
                expect(Interval.create(0, 1)).toEqual(i1);
                expect(Interval.create(1, 0)).toEqual(i1);
            });
        });

        describe("static function parseAsCols", () => {
            it("should exist", () => {
                expect(Interval).toHaveStaticMethod("parseAsCols");
            });
            it("should return the correct column interval", () => {
                expect(Interval.parseAsCols("A:A")).toEqual(new Interval(0));
                expect(Interval.parseAsCols("A:B")).toEqual(new Interval(0, 1));
                expect(Interval.parseAsCols("A:C")).toEqual(new Interval(0, 2));
                expect(Interval.parseAsCols("B:C")).toEqual(new Interval(1, 2));
                expect(Interval.parseAsCols("C:C")).toEqual(new Interval(2));
                expect(Interval.parseAsCols("AA:ZZ")).toEqual(new Interval(26, 701));
                expect(Interval.parseAsCols("ZZZZ:ZZZZ")).toEqual(new Interval(475253));
                expect(Interval.parseAsCols("A")).toEqual(new Interval(0));
                expect(Interval.parseAsCols("ZZZZ")).toEqual(new Interval(475253));
            });
            it("should return the adjusted column interval", () => {
                expect(Interval.parseAsCols("C:A")).toEqual(new Interval(0, 2));
            });
            it("should accept lower-case columns", () => {
                expect(Interval.parseAsCols("aA:Aa")).toEqual(new Interval(26, 26));
                expect(Interval.parseAsCols("aa:AA")).toEqual(new Interval(26, 26));
            });
            it("should return null for invalid intervals", () => {
                expect(Interval.parseAsCols("")).toBeNull();
                expect(Interval.parseAsCols("1")).toBeNull();
                expect(Interval.parseAsCols("1:1")).toBeNull();
                expect(Interval.parseAsCols("$")).toBeNull();
                expect(Interval.parseAsCols("$A:$A")).toBeNull();
                expect(Interval.parseAsCols("AAAAA:AAAAA")).toBeNull();
            });
        });

        describe("static function parseAsRows", () => {
            it("should exist", () => {
                expect(Interval).toHaveStaticMethod("parseAsRows");
            });
            it("should return the correct column interval", () => {
                expect(Interval.parseAsRows("1:1")).toEqual(new Interval(0));
                expect(Interval.parseAsRows("1:2")).toEqual(new Interval(0, 1));
                expect(Interval.parseAsRows("1:3")).toEqual(new Interval(0, 2));
                expect(Interval.parseAsRows("2:3")).toEqual(new Interval(1, 2));
                expect(Interval.parseAsRows("3:3")).toEqual(new Interval(2));
                expect(Interval.parseAsRows("10:999")).toEqual(new Interval(9, 998));
                expect(Interval.parseAsRows("99999999:99999999")).toEqual(new Interval(99999998));
                expect(Interval.parseAsRows("1")).toEqual(new Interval(0));
                expect(Interval.parseAsRows("99999999")).toEqual(new Interval(99999998));
            });
            it("should return the adjusted row interval", () => {
                expect(Interval.parseAsRows("3:1")).toEqual(new Interval(0, 2));
            });
            it("should accept leading zeros", () => {
                expect(Interval.parseAsRows("01:000999")).toEqual(new Interval(0, 998));
            });
            it("should return null for invalid intervals", () => {
                expect(Interval.parseAsRows("")).toBeNull();
                expect(Interval.parseAsRows("A")).toBeNull();
                expect(Interval.parseAsRows("A:A")).toBeNull();
                expect(Interval.parseAsRows("$")).toBeNull();
                expect(Interval.parseAsRows("$1:$1")).toBeNull();
                expect(Interval.parseAsRows("100000000:100000000")).toBeNull();
            });
        });

        describe("static function parseAs", () => {
            it("should exist", () => {
                expect(Interval).toHaveStaticMethod("parseAs");
            });
            it("should return the correct column interval", () => {
                expect(Interval.parseAs("A:C", true)).toEqual(new Interval(0, 2));
                expect(Interval.parseAs("C:A", true)).toEqual(new Interval(0, 2));
                expect(Interval.parseAs("A", true)).toEqual(new Interval(0));
            });
            it("should return the correct row interval", () => {
                expect(Interval.parseAs("1:3", false)).toEqual(new Interval(0, 2));
                expect(Interval.parseAs("3:1", false)).toEqual(new Interval(0, 2));
                expect(Interval.parseAs("1", false)).toEqual(new Interval(0));
            });
        });

        describe("static function compare", () => {
            const i1 = i("3:5");
            it("should exist", () => {
                expect(Interval).toHaveStaticMethod("compare");
            });
            it("should return 0 for equal intervals", () => {
                expect(Interval.compare(i1, i1)).toBe(0);
                expect(Interval.compare(i1, i("3:5"))).toBe(0);
            });
            it("should return negative value for intervals in order", () => {
                expect(Interval.compare(i1, i("4:4"))).toBe(-1);
                expect(Interval.compare(i1, i("4:5"))).toBe(-1);
                expect(Interval.compare(i1, i("4:6"))).toBe(-1);
                expect(Interval.compare(i1, i("3:6"))).toBe(-1);
            });
            it("should return positive value for intervals in reversed order", () => {
                expect(Interval.compare(i1, i("2:4"))).toBe(1);
                expect(Interval.compare(i1, i("2:5"))).toBe(1);
                expect(Interval.compare(i1, i("2:6"))).toBe(1);
                expect(Interval.compare(i1, i("3:4"))).toBe(1);
            });
        });

        describe("static function compareLast", () => {
            const i1 = i("3:5");
            it("should exist", () => {
                expect(Interval).toHaveStaticMethod("compareLast");
            });
            it("should return 0 for equal intervals", () => {
                expect(Interval.compareLast(i1, i1)).toBe(0);
                expect(Interval.compareLast(i1, i("3:5"))).toBe(0);
            });
            it("should return negative value for intervals in order", () => {
                expect(Interval.compareLast(i1, i("2:6"))).toBe(-1);
                expect(Interval.compareLast(i1, i("3:6"))).toBe(-1);
                expect(Interval.compareLast(i1, i("4:6"))).toBe(-1);
                expect(Interval.compareLast(i1, i("4:5"))).toBe(-1);
            });
            it("should return positive value for intervals in reversed order", () => {
                expect(Interval.compareLast(i1, i("2:4"))).toBe(1);
                expect(Interval.compareLast(i1, i("3:4"))).toBe(1);
                expect(Interval.compareLast(i1, i("4:4"))).toBe(1);
                expect(Interval.compareLast(i1, i("2:5"))).toBe(1);
            });
        });

        describe("property key", () => {
            it("should return unique key for intervals", () => {
                expect(i("1:1").key).toBe("0:0");
                expect(i("2:3").key).toBe("1:2");
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("clone");
            });
            it("should return a clone", () => {
                const i1 = i("2:4"), i2 = i1.clone();
                expect(i2).toBeInstanceOf(Interval);
                expect(i2).not.toBe(i1);
                expect(i2).toEqual(i1);
            });
        });

        describe("method equals", () => {
            const i1 = i("2:4");
            it("should exist", () => {
                expect(Interval).toHaveMethod("equals");
            });
            it("should return true for equal intervals", () => {
                expect(i1.equals(i1)).toBeTrue();
                expect(i1.equals(i1.clone())).toBeTrue();
            });
            it("should return false for different intervals", () => {
                expect(i1.equals(i("3:4"))).toBeFalse();
                expect(i1.equals(i("2:3"))).toBeFalse();
                expect(i1.equals(i("3:3"))).toBeFalse();
            });
        });

        describe("method differs", () => {
            const i1 = i("2:4");
            it("should exist", () => {
                expect(Interval).toHaveMethod("differs");
            });
            it("should return false for equal intervals", () => {
                expect(i1.differs(i1)).toBeFalse();
                expect(i1.differs(i1.clone())).toBeFalse();
            });
            it("should return true for different intervals", () => {
                expect(i1.differs(i("3:4"))).toBeTrue();
                expect(i1.differs(i("2:3"))).toBeTrue();
                expect(i1.differs(i("3:3"))).toBeTrue();
            });
        });

        describe("method compareTo", () => {
            const i1 = i("3:5");
            it("should exist", () => {
                expect(Interval).toHaveMethod("compareTo");
            });
            it("should return 0 for equal intervals", () => {
                expect(i1.compareTo(i1)).toBe(0);
                expect(i("1:3").compareTo(i("1:3"))).toBe(0);
            });
            it("should return negative value for intervals in order", () => {
                expect(i1.compareTo(i("4:4"))).toBe(-1);
                expect(i1.compareTo(i("4:5"))).toBe(-1);
                expect(i1.compareTo(i("4:6"))).toBe(-1);
                expect(i1.compareTo(i("3:6"))).toBe(-1);
            });
            it("should return positive value for intervals in reversed order", () => {
                expect(i1.compareTo(i("2:4"))).toBe(1);
                expect(i1.compareTo(i("2:5"))).toBe(1);
                expect(i1.compareTo(i("2:6"))).toBe(1);
                expect(i1.compareTo(i("3:4"))).toBe(1);
            });
        });

        describe("method size", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("size");
            });
            it("should return the correct interval size", () => {
                expect(i("1:1").size()).toBe(1);
                expect(i("1:2").size()).toBe(2);
                expect(i("1:7").size()).toBe(7);
                expect(i("1:8").size()).toBe(8);
                expect(i("2:8").size()).toBe(7);
                expect(i("7:8").size()).toBe(2);
                expect(i("8:8").size()).toBe(1);
            });
        });

        describe("method single", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("single");
            });
            it("should return the correct result", () => {
                expect(i("1:1").single()).toBeTrue();
                expect(i("3:3").single()).toBeTrue();
                expect(i("3:4").single()).toBeFalse();
            });
        });

        describe("method containsIndex", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("containsIndex");
            });
            const i1 = i("3:5");
            it("should return false for indexes outside the interval", () => {
                expect(i1.containsIndex(0)).toBeFalse();
                expect(i1.containsIndex(1)).toBeFalse();
                expect(i1.containsIndex(5)).toBeFalse();
                expect(i1.containsIndex(6)).toBeFalse();
            });
            it("should return true for indexes inside the interval", () => {
                expect(i1.containsIndex(2)).toBeTrue();
                expect(i1.containsIndex(3)).toBeTrue();
                expect(i1.containsIndex(4)).toBeTrue();
            });
        });

        describe("method contains", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("contains");
            });
            const i1 = i("3:5");
            it("should return false for distinct intervals", () => {
                expect(i1.contains(i("1:2"))).toBeFalse();
                expect(i1.contains(i("6:7"))).toBeFalse();
                expect(i1.contains(i("8:9"))).toBeFalse();
                expect(i("1:2").contains(i1)).toBeFalse();
                expect(i("6:7").contains(i1)).toBeFalse();
                expect(i("8:9").contains(i1)).toBeFalse();
            });
            it("should return false for partly overlapping intervals", () => {
                expect(i1.contains(i("1:3"))).toBeFalse();
                expect(i1.contains(i("2:4"))).toBeFalse();
                expect(i1.contains(i("4:6"))).toBeFalse();
                expect(i1.contains(i("5:7"))).toBeFalse();
            });
            it("should return true for intervals containing the other interval", () => {
                expect(i1.contains(i("3:3"))).toBeTrue();
                expect(i1.contains(i("3:4"))).toBeTrue();
                expect(i1.contains(i("3:5"))).toBeTrue();
                expect(i1.contains(i("4:5"))).toBeTrue();
                expect(i1.contains(i("5:5"))).toBeTrue();
            });
            it("should return false for intervals contained by the other interval", () => {
                expect(i("3:3").contains(i1)).toBeFalse();
                expect(i("3:4").contains(i1)).toBeFalse();
                expect(i("4:5").contains(i1)).toBeFalse();
                expect(i("5:5").contains(i1)).toBeFalse();
            });
        });

        describe("method overlaps", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("overlaps");
            });
            const i1 = i("3:5");
            it("should return false for distinct intervals", () => {
                expect(i1.overlaps(i("1:2"))).toBeFalse();
                expect(i1.overlaps(i("6:7"))).toBeFalse();
                expect(i1.overlaps(i("8:9"))).toBeFalse();
                expect(i("1:2").overlaps(i1)).toBeFalse();
                expect(i("6:7").overlaps(i1)).toBeFalse();
                expect(i("8:9").overlaps(i1)).toBeFalse();
            });
            it("should return true for partly overlapping intervals", () => {
                expect(i1.overlaps(i("1:3"))).toBeTrue();
                expect(i1.overlaps(i("2:4"))).toBeTrue();
                expect(i1.overlaps(i("4:6"))).toBeTrue();
                expect(i1.overlaps(i("5:7"))).toBeTrue();
            });
            it("should return true for intervals containing each other", () => {
                expect(i1.overlaps(i1)).toBeTrue();
                expect(i1.overlaps(i("3:3"))).toBeTrue();
                expect(i("3:3").overlaps(i1)).toBeTrue();
                expect(i1.overlaps(i("3:4"))).toBeTrue();
                expect(i("3:4").overlaps(i1)).toBeTrue();
                expect(i1.overlaps(i("4:5"))).toBeTrue();
                expect(i("4:5").overlaps(i1)).toBeTrue();
                expect(i1.overlaps(i("5:5"))).toBeTrue();
                expect(i("5:5").overlaps(i1)).toBeTrue();
            });
        });

        describe("method boundary", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("boundary");
            });
            it("should return the bounding interval", () => {
                expect(i("1:3").boundary(i("5:7"))).toEqual(i("1:7"));
                expect(i("5:7").boundary(i("1:3"))).toEqual(i("1:7"));
                expect(i("1:7").boundary(i("3:5"))).toEqual(i("1:7"));
                expect(i("3:5").boundary(i("1:7"))).toEqual(i("1:7"));
            });
        });

        describe("method intersect", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("intersect");
            });
            const i1 = i("3:5");
            it("should return null for distinct intervals", () => {
                expect(i1.intersect(i("1:2"))).toBeNull();
                expect(i1.intersect(i("6:7"))).toBeNull();
                expect(i1.intersect(i("8:9"))).toBeNull();
                expect(i("1:2").intersect(i1)).toBeNull();
                expect(i("6:7").intersect(i1)).toBeNull();
                expect(i("8:9").intersect(i1)).toBeNull();
            });
            it("should return intersection for overlapping intervals", () => {
                expect(i1.intersect(i("1:3"))).toEqual(i("3:3"));
                expect(i1.intersect(i("2:4"))).toEqual(i("3:4"));
                expect(i1.intersect(i("4:6"))).toEqual(i("4:5"));
                expect(i1.intersect(i("5:7"))).toEqual(i("5:5"));
            });
            it("should return intersection for intervals containing each other", () => {
                expect(i1.intersect(i1)).toEqual(i1);
                expect(i1.intersect(i("3:3"))).toEqual(i("3:3"));
                expect(i("3:3").intersect(i1)).toEqual(i("3:3"));
                expect(i1.intersect(i("3:4"))).toEqual(i("3:4"));
                expect(i("3:4").intersect(i1)).toEqual(i("3:4"));
                expect(i1.intersect(i("4:5"))).toEqual(i("4:5"));
                expect(i("4:5").intersect(i1)).toEqual(i("4:5"));
                expect(i1.intersect(i("5:5"))).toEqual(i("5:5"));
                expect(i("5:5").intersect(i1)).toEqual(i("5:5"));
            });
        });

        describe("method assign", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("assign");
            });
            it("should change the indexes", () => {
                const i1 = i("5:6");
                expect(i1).toEqual(i("5:6"));
                i1.assign(i("2:3"));
                expect(i1).toEqual(i("2:3"));
            });
            it("should return itself", () => {
                const i1 = i("1:2");
                expect(i1.assign(i("3:4"))).toBe(i1);
            });
        });

        describe("method move", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("move");
            });
            it("should move the indexes in the interval", () => {
                expect(i("5:6").move(-3)).toEqual(i("2:3"));
                expect(i("5:6").move(-2)).toEqual(i("3:4"));
                expect(i("5:6").move(-1)).toEqual(i("4:5"));
                expect(i("5:6").move(0)).toEqual(i("5:6"));
                expect(i("5:6").move(1)).toEqual(i("6:7"));
                expect(i("5:6").move(2)).toEqual(i("7:8"));
                expect(i("5:6").move(3)).toEqual(i("8:9"));
            });
            it("should return itself", () => {
                const i1 = i("5:6");
                expect(i1.move(1)).toBe(i1);
            });
        });

        describe("method indexes", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("indexes");
            });
            it("should return an index iterator", () => {
                const it = i("2:4").indexes();
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([1, 2, 3]);
            });
            it("should return a reverse index iterator", () => {
                const it = i("2:4").indexes({ reverse: true });
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([3, 2, 1]);
            });
        });

        describe("method toOpStrCols", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("toOpStrCols");
            });
            it("should stringify to column interval", () => {
                expect(i("A:A").toOpStrCols()).toBe("A");
                expect(i("B:C").toOpStrCols()).toBe("B:C");
                expect(i("A:ZZ").toOpStrCols()).toBe("A:ZZ");
            });
        });

        describe("method toOpStrRows", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("toOpStrRows");
            });
            it("should stringify to row interval", () => {
                expect(i("1:1").toOpStrRows()).toBe("1");
                expect(i("2:3").toOpStrRows()).toBe("2:3");
                expect(i("1:1000").toOpStrRows()).toBe("1:1000");
            });
        });

        describe("method toOpStr", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("toOpStr");
            });
            it("should stringify to column interval", () => {
                expect(i("1:1").toOpStr(true)).toBe("A");
                expect(i("2:3").toOpStr(true)).toBe("B:C");
            });
            it("should stringify to row interval", () => {
                expect(i("A:A").toOpStr(false)).toBe("1");
                expect(i("B:C").toOpStr(false)).toBe("2:3");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(Interval).toHaveMethod("toString");
            });
            it("should stringify to interval", () => {
                expect(i("B:C").toString()).toBe("2:3");
                expect(i("B:C").toString(false)).toBe("2:3");
                expect(i("B:C").toString(true)).toBe("B:C");
            });
            it("should stringify implicitly", () => {
                expect("<" + i("B:C") + ">").toBe("<2:3>");
            });
        });
    });
});
