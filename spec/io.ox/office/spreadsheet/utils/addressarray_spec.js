/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ArrayBase } from "@/io.ox/office/spreadsheet/utils/arraybase";
import { AddressArray } from "@/io.ox/office/spreadsheet/utils/addressarray";

import { a, r, aa } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/addressarray", () => {

    // class AddressArray -----------------------------------------------------

    describe("class AddressArray", () => {

        it("should subclass ArrayBase", () => {
            expect(AddressArray).toBeSubClassOf(ArrayBase);
            expect(AddressArray).toBeSubClassOf(Array);
        });

        describe("constructor", () => {
            it("should create an address array", () => {
                const aa1 = new AddressArray();
                expect(aa1).toBeArrayOfSize(0);
            });
            const a1 = a("A2"), a2 = a("C4");
            it("should insert single addresses", () => {
                const aa1 = new AddressArray(a1);
                expect(aa1).toHaveLength(1);
                expect(aa1[0]).toBe(a1);
                const aa2 = new AddressArray(a1, a2, null, a1);
                expect(aa2).toHaveLength(3);
                expect(aa2[0]).toBe(a1);
                expect(aa2[1]).toBe(a2);
                expect(aa2[2]).toBe(a1);
            });
            it("should insert plain arrays of addresses", () => {
                const aa1 = new AddressArray([a1]);
                expect(aa1).toHaveLength(1);
                expect(aa1[0]).toBe(a1);
                const aa2 = new AddressArray([a1, a2], [], [a1]);
                expect(aa2).toHaveLength(3);
                expect(aa2[0]).toBe(a1);
                expect(aa2[1]).toBe(a2);
                expect(aa2[2]).toBe(a1);
            });
            it("should copy-construct address arrays", () => {
                const aa0 = new AddressArray(a1, a2);
                const aa1 = new AddressArray(aa0);
                expect(aa1).toHaveLength(2);
                expect(aa1[0]).toBe(a1);
                expect(aa1[1]).toBe(a2);
                const aa2 = new AddressArray(aa0, new AddressArray(), aa1);
                expect(aa2).toHaveLength(4);
                expect(aa2[0]).toBe(a1);
                expect(aa2[1]).toBe(a2);
                expect(aa2[2]).toBe(a1);
                expect(aa2[3]).toBe(a2);
            });
        });

        describe("static function cast", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveStaticMethod("cast");
            });
            it("should not convert address arrays", () => {
                const aa1 = aa("A1 B2");
                expect(AddressArray.cast(aa1)).toBe(aa1);
            });
            it("should convert plain arrays", () => {
                const aa1 = [a("A1"), a("B2")];
                const aa2 = AddressArray.cast(aa1);
                expect(aa2).toBeInstanceOf(AddressArray);
                expect(aa2).toHaveLength(aa1.length);
                aa1.forEach(function (v, i) { expect(aa2[i]).toBe(v); });
            });
            it("should convert single object to array", () => {
                const a1 = a("A1");
                const aa1 = AddressArray.cast(a1);
                expect(aa1).toBeInstanceOf(AddressArray);
                expect(aa1).toHaveLength(1);
                expect(aa1[0]).toBe(a1);
            });
            it("should convert nullish to empty arrays", () => {
                const aa1 = AddressArray.cast(null);
                const aa2 = AddressArray.cast(undefined);
                expect(aa1).toBeInstanceOf(AddressArray);
                expect(aa1).toHaveLength(0);
                expect(aa2).toBeInstanceOf(AddressArray);
                expect(aa2).toHaveLength(0);
            });
        });

        describe("static function filter", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveStaticMethod("filter");
            });
        });

        describe("static function map", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveStaticMethod("map");
            });
        });

        describe("property key", () => {
            it("should return unique key for the array", () => {
                expect(aa("A2 B3").key).toBe("0,1 1,2");
                expect(aa("").key).toBe("");
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveMethod("clone");
            });
            it("should return a shallow clone", () => {
                const aa1 = aa("A2 C4"), aa2 = aa1.clone();
                expect(aa2).toBeInstanceOf(AddressArray);
                expect(aa2).not.toBe(aa1);
                expect(aa2[0]).toBe(aa1[0]);
                expect(aa2[1]).toBe(aa1[1]);
                expect(aa2).toEqual(aa1);
            });
        });

        describe("method deepClone", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveMethod("deepClone");
            });
            it("should return a deep clone", () => {
                const aa1 = aa("A2 C4"), aa2 = aa1.deepClone();
                expect(aa2).toBeInstanceOf(AddressArray);
                expect(aa2).not.toBe(aa1);
                expect(aa2[0]).not.toBe(aa1[0]);
                expect(aa2[1]).not.toBe(aa1[1]);
                expect(aa2).toEqual(aa1);
            });
        });

        describe("method indexOf", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveMethod("indexOf");
            });
            const aa1 = aa("B4 C5 D6 C5 B4");
            it("should return array index of strictly equal address", () => {
                expect(aa1.indexOf(aa1[0])).toBe(0);
                expect(aa1.indexOf(aa1[1])).toBe(1);
                expect(aa1.indexOf(aa1[2])).toBe(2);
                expect(aa1.indexOf(aa1[3])).toBe(1);
                expect(aa1.indexOf(aa1[4])).toBe(0);
            });
            it("should return array index of deeply equal address", () => {
                expect(aa1.indexOf(a("B4"))).toBe(0);
                expect(aa1.indexOf(a("C5"))).toBe(1);
                expect(aa1.indexOf(a("D6"))).toBe(2);
                expect(aa1.indexOf(a("E7"))).toBe(-1);
            });
        });

        describe("method lastIndexOf", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveMethod("lastIndexOf");
            });
            const aa1 = aa("B4 C5 D6 C5 B4");
            it("should return array index of strictly equal address", () => {
                expect(aa1.lastIndexOf(aa1[0])).toBe(4);
                expect(aa1.lastIndexOf(aa1[1])).toBe(3);
                expect(aa1.lastIndexOf(aa1[2])).toBe(2);
                expect(aa1.lastIndexOf(aa1[3])).toBe(3);
                expect(aa1.lastIndexOf(aa1[4])).toBe(4);
            });
            it("should return array index of deeply equal address", () => {
                expect(aa1.lastIndexOf(a("B4"))).toBe(4);
                expect(aa1.lastIndexOf(a("C5"))).toBe(3);
                expect(aa1.lastIndexOf(a("D6"))).toBe(2);
                expect(aa1.lastIndexOf(a("E7"))).toBe(-1);
            });
        });

        describe("method contains", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveMethod("contains");
            });
            const aa1 = aa("B4 C5");
            it("should return false for cells outside the array", () => {
                expect(aa1.contains(a("A4"))).toBeFalse();
                expect(aa1.contains(a("C4"))).toBeFalse();
                expect(aa1.contains(a("B3"))).toBeFalse();
                expect(aa1.contains(a("B5"))).toBeFalse();
            });
            it("should return true for cells inside the array", () => {
                expect(aa1.contains(a("B4"))).toBeTrue();
                expect(aa1.contains(a("C5"))).toBeTrue();
            });
        });

        describe("method boundary", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveMethod("boundary");
            });
            it("should return undefined for an empty array", () => {
                expect(new AddressArray().boundary()).toBeUndefined();
            });
            it("should return the boundary of the addresses", () => {
                expect(aa("C2 E3 D5 B4").boundary()).toEqual(r("B2:E5"));
            });
        });

        describe("method unify", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveMethod("unify");
            });
            it("should return a shallow copy", () => {
                const aa1 = aa("A2 C4"), aa2 = aa1.unify();
                expect(aa2).toBeInstanceOf(AddressArray);
                expect(aa2).toHaveLength(2);
                expect(aa2).not.toBe(aa1);
                expect(aa2[0]).toBe(aa1[0]);
                expect(aa2[1]).toBe(aa1[1]);
            });
            it("should remove all duplicates", () => {
                const aa1 = aa("A2 C4 A2 A4 C4 A4"), aa2 = aa1.unify();
                expect(aa2).toHaveLength(3);
                expect(aa2[0]).toBe(aa1[0]);
                expect(aa2[1]).toBe(aa1[1]);
                expect(aa2[2]).toBe(aa1[3]);
            });
            it("should accept empty array", () => {
                expect(new AddressArray().unify()).toBeArrayOfSize(0);
            });
        });

        describe("method toOpStr", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveMethod("toOpStr");
            });
            const aa1 = aa("A2 C4");
            it("should stringify the addresses", () => {
                expect(aa1.toOpStr()).toBe("A2 C4");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(AddressArray).toHaveMethod("toString");
            });
            const aa1 = aa("A2 C4");
            it("should stringify the addresses", () => {
                expect(aa1.toString()).toBe("A2,C4");
            });
            it("should use the separator string", () => {
                expect(aa1.toString(" + ")).toBe("A2 + C4");
            });
            it("should stringify implicitly", () => {
                expect("<" + aa1 + ">").toBe("<A2,C4>");
            });
        });
    });
});
