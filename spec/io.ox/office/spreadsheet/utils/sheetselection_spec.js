/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { SheetSelection } from "@/io.ox/office/spreadsheet/utils/sheetselection";

import { a, r, ra } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/sheetselection", () => {

    // class SheetSelection ---------------------------------------------------

    describe("class SheetSelection", () => {

        it("should exist", () => {
            expect(SheetSelection).toBeFunction();
        });

        describe("constructor", () => {
            const ranges = ra("B3:D5 D5:F7"), range = r("D5:F7"), address = a("D5"), drawings = [[1, 2], [1, 3]];
            it("should create a selection", () => {
                const s = new SheetSelection(ranges, 1, address, drawings);
                expect(s).toHaveProperty("ranges", ranges);
                expect(s).toHaveProperty("active", 1);
                expect(s).toHaveProperty("address", address);
                expect(s).toHaveProperty("origin", null);
                expect(s).toHaveProperty("drawings", drawings);
            });
            it("should accept single range address", () => {
                const s = new SheetSelection(range, 0, address, drawings);
                expect(s.ranges).toBeArrayOfSize(1);
                expect(s.ranges[0]).toBe(range);
                expect(s).toHaveProperty("active", 0);
                expect(s).toHaveProperty("address", address);
                expect(s).toHaveProperty("origin", null);
                expect(s).toHaveProperty("drawings", drawings);
            });
            it("should accept missing drawings", () => {
                const s = new SheetSelection(ranges, 1, address);
                expect(s).toHaveProperty("ranges", ranges);
                expect(s).toHaveProperty("active", 1);
                expect(s).toHaveProperty("address", address);
                expect(s).toHaveProperty("origin", null);
                expect(s.drawings).toBeArrayOfSize(0);
            });
            it("should accept missing active cell", () => {
                const s = new SheetSelection(ranges, 1);
                expect(s).toHaveProperty("ranges", ranges);
                expect(s).toHaveProperty("active", 1);
                expect(s.address).toEqual(ranges[1].a1);
                expect(s.address).not.toBe(ranges[1].a1);
                expect(s).toHaveProperty("origin", null);
                expect(s.drawings).toBeArrayOfSize(0);
            });
            it("should accept missing active range index", () => {
                const s = new SheetSelection(ranges);
                expect(s).toHaveProperty("ranges", ranges);
                expect(s).toHaveProperty("active", 0);
                expect(s.address).toEqual(ranges[0].a1);
                expect(s.address).not.toBe(ranges[0].a1);
                expect(s).toHaveProperty("origin", null);
                expect(s.drawings).toBeArrayOfSize(0);
            });
            it("should create a default selection", () => {
                const s = new SheetSelection();
                expect(s.ranges).toBeArrayOfSize(0);
                expect(s).toHaveProperty("active", -1);
                expect(s.address).toEqual(a("A1"));
                expect(s).toHaveProperty("origin", null);
                expect(s.drawings).toBeArrayOfSize(0);
            });
        });

        describe("static function createFromAddress", () => {
            it("should exist", () => {
                expect(SheetSelection).toHaveStaticMethod("createFromAddress");
            });
            it("should create a selection", () => {
                const s = SheetSelection.createFromAddress(a("C4"));
                expect(s).toEqual(new SheetSelection(r("C4:C4"), 0, a("C4")));
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(SheetSelection).toHaveMethod("clone");
            });
            it("should return a deep clone", () => {
                const ranges = ra("B3:D5 D5:F7"), address = a("B3"), origin = a("C4"), drawings = [[1, 2], [1, 3]];
                const s1 = new SheetSelection(ranges, 1, address, drawings);
                s1.origin = origin;
                const s2 = s1.clone();
                expect(s2).toBeInstanceOf(SheetSelection);
                expect(s2).not.toBe(s1);
                expect(s2).toEqual(s1);
                expect(s2.ranges).not.toBe(s1.ranges);
                expect(s2.address).not.toBe(s1.address);
                expect(s2.origin).not.toBe(s1.origin);
                expect(s2.drawings).not.toBe(s1.drawings);
            });
        });

        describe("method activeRange", () => {
            it("should exist", () => {
                expect(SheetSelection).toHaveMethod("activeRange");
            });
            it("should return the active range", () => {
                const ranges = ra("B3:D5 D5:F7"), s = new SheetSelection(ranges, 0);
                expect(s.activeRange()).toBe(ranges[0]);
                s.active = 1;
                expect(s.activeRange()).toBe(ranges[1]);
            });
            it("should return null for empty selection", () => {
                const s = new SheetSelection();
                expect(s.activeRange()).toBeNull();
            });
        });

        describe("method containsAddress", () => {
            it("should exist", () => {
                expect(SheetSelection).toHaveMethod("containsAddress");
            });
            it("should return the correct result", () => {
                const ranges = ra("B3:D5 D5:F7"), s = new SheetSelection(ranges, 0);
                expect(s.containsAddress(a("B3"))).toBeTrue();
                expect(s.containsAddress(a("C4"))).toBeTrue();
                expect(s.containsAddress(a("D5"))).toBeTrue();
                expect(s.containsAddress(a("E6"))).toBeTrue();
                expect(s.containsAddress(a("F7"))).toBeTrue();
                expect(s.containsAddress(a("A2"))).toBeFalse();
                expect(s.containsAddress(a("G8"))).toBeFalse();
                expect(s.containsAddress(a("E4"))).toBeFalse();
                expect(s.containsAddress(a("C6"))).toBeFalse();
            });
        });

        describe("method isActive", () => {
            it("should exist", () => {
                expect(SheetSelection).toHaveMethod("isActive");
            });
            it("should return the correct result", () => {
                const ranges = ra("B3:D5 D5:F7"), address = a("D5"), s = new SheetSelection(ranges, 0, address);
                expect(s.isActive(a("B3"))).toBeFalse();
                expect(s.isActive(a("C4"))).toBeFalse();
                expect(s.isActive(a("D5"))).toBeTrue();
                expect(s.isActive(a("E6"))).toBeFalse();
                expect(s.isActive(a("F7"))).toBeFalse();
                expect(s.isActive(a("A2"))).toBeFalse();
                expect(s.isActive(a("G8"))).toBeFalse();
                expect(s.isActive(a("E4"))).toBeFalse();
                expect(s.isActive(a("C6"))).toBeFalse();
            });
            it("should return false for empty selection", () => {
                expect(new SheetSelection().isActive(a("B3"))).toBeFalse();
            });
        });
    });
});
