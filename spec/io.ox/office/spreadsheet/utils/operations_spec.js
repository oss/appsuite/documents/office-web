/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as textops from "@/io.ox/office/textframework/utils/operations";
import * as operations from "@/io.ox/office/spreadsheet/utils/operations";

// tests ======================================================================

describe("module spreadsheet/utils/operations", () => {

    it("re-exports should exist", () => {
        expect(operations).toContainProps(textops);
    });

    it("constants should exist", () => {
        expect(operations.INSERT_NUMBER_FORMAT).toBe("insertNumberFormat");
        expect(operations.DELETE_NUMBER_FORMAT).toBe("deleteNumberFormat");
        expect(operations.INSERT_SHEET).toBe("insertSheet");
        expect(operations.DELETE_SHEET).toBe("deleteSheet");
        expect(operations.CHANGE_SHEET).toBe("changeSheet");
        expect(operations.MOVE_SHEET).toBe("moveSheet");
        expect(operations.MOVE_SHEETS).toBe("moveSheets");
        expect(operations.COPY_SHEET).toBe("copySheet");
        expect(operations.INSERT_NAME).toBe("insertName");
        expect(operations.CHANGE_NAME).toBe("changeName");
        expect(operations.DELETE_NAME).toBe("deleteName");
        expect(operations.INSERT_TABLE).toBe("insertTable");
        expect(operations.CHANGE_TABLE).toBe("changeTable");
        expect(operations.DELETE_TABLE).toBe("deleteTable");
        expect(operations.CHANGE_TABLE_COLUMN).toBe("changeTableColumn");
        expect(operations.INSERT_DVRULE).toBe("insertDVRule");
        expect(operations.CHANGE_DVRULE).toBe("changeDVRule");
        expect(operations.DELETE_DVRULE).toBe("deleteDVRule");
        expect(operations.INSERT_CFRULE).toBe("insertCFRule");
        expect(operations.CHANGE_CFRULE).toBe("changeCFRule");
        expect(operations.DELETE_CFRULE).toBe("deleteCFRule");
        expect(operations.INSERT_COLUMNS).toBe("insertColumns");
        expect(operations.DELETE_COLUMNS).toBe("deleteColumns");
        expect(operations.CHANGE_COLUMNS).toBe("changeColumns");
        expect(operations.INSERT_ROWS).toBe("insertRows");
        expect(operations.DELETE_ROWS).toBe("deleteRows");
        expect(operations.CHANGE_ROWS).toBe("changeRows");
        expect(operations.CHANGE_CELLS).toBe("changeCells");
        expect(operations.MOVE_CELLS).toBe("moveCells");
        expect(operations.MERGE_CELLS).toBe("mergeCells");
        expect(operations.INSERT_NOTE).toBe("insertNote");
        expect(operations.CHANGE_NOTE).toBe("changeNote");
        expect(operations.DELETE_NOTE).toBe("deleteNote");
        expect(operations.MOVE_NOTES).toBe("moveNotes");
        expect(operations.INSERT_COMMENT).toBe("insertComment");
        expect(operations.CHANGE_COMMENT).toBe("changeComment");
        expect(operations.DELETE_COMMENT).toBe("deleteComment");
        expect(operations.MOVE_COMMENTS).toBe("moveComments");
        expect(operations.SHEET_SELECTION).toBe("sheetSelection");
    });
});
