/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as address from "@/io.ox/office/spreadsheet/utils/address";

import { a } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/address", () => {

    // constants --------------------------------------------------------------

    it("constants should exist", () => {
        expect(address.COL_PATTERN).toBeString();
        expect(address.ROW_PATTERN).toBeString();
    });

    // functions --------------------------------------------------------------

    describe("function addressKey", () => {
        const { addressKey } = address;
        it("should exist", () => {
            expect(addressKey).toBeFunction();
        });
        it("should return unique key for cell address", () => {
            expect(addressKey(0, 1)).toBe("0,1");
            expect(addressKey(1, 2)).toBe("1,2");
        });
    });

    describe("function stringifyCol", () => {
        const { stringifyCol } = address;
        it("should exist", () => {
            expect(stringifyCol).toBeFunction();
        });
        it("should return the correct column name", () => {
            expect(stringifyCol(0)).toBe("A");
            expect(stringifyCol(1)).toBe("B");
            expect(stringifyCol(2)).toBe("C");
            expect(stringifyCol(25)).toBe("Z");
            expect(stringifyCol(26)).toBe("AA");
            expect(stringifyCol(27)).toBe("AB");
            expect(stringifyCol(28)).toBe("AC");
            expect(stringifyCol(51)).toBe("AZ");
            expect(stringifyCol(52)).toBe("BA");
            expect(stringifyCol(78)).toBe("CA");
            expect(stringifyCol(701)).toBe("ZZ");
            expect(stringifyCol(702)).toBe("AAA");
            expect(stringifyCol(18277)).toBe("ZZZ");
            expect(stringifyCol(18278)).toBe("AAAA");
            expect(stringifyCol(475253)).toBe("ZZZZ");
        });
    });

    describe("function stringifyRow", () => {
        const { stringifyRow } = address;
        it("should exist", () => {
            expect(stringifyRow).toBeFunction();
        });
        it("should return the correct row name", () => {
            expect(stringifyRow(0)).toBe("1");
            expect(stringifyRow(1)).toBe("2");
            expect(stringifyRow(2)).toBe("3");
            expect(stringifyRow(8)).toBe("9");
            expect(stringifyRow(9)).toBe("10");
            expect(stringifyRow(10)).toBe("11");
            expect(stringifyRow(99999998)).toBe("99999999");
        });
    });

    describe("function stringifyIndex", () => {
        const { stringifyIndex } = address;
        it("should exist", () => {
            expect(stringifyIndex).toBeFunction();
        });
        it("should return the column name", () => {
            expect(stringifyIndex(0, true)).toBe("A");
            expect(stringifyIndex(26, true)).toBe("AA");
        });
        it("should return the row name", () => {
            expect(stringifyIndex(0, false)).toBe("1");
            expect(stringifyIndex(41, false)).toBe("42");
        });
    });

    describe("function parseCol", () => {
        const { parseCol } = address;
        it("should exist", () => {
            expect(parseCol).toBeFunction();
        });
        it("should return the correct column index", () => {
            expect(parseCol("A")).toBe(0);
            expect(parseCol("B")).toBe(1);
            expect(parseCol("C")).toBe(2);
            expect(parseCol("Z")).toBe(25);
            expect(parseCol("AA")).toBe(26);
            expect(parseCol("AB")).toBe(27);
            expect(parseCol("AC")).toBe(28);
            expect(parseCol("AZ")).toBe(51);
            expect(parseCol("BA")).toBe(52);
            expect(parseCol("CA")).toBe(78);
            expect(parseCol("ZZ")).toBe(701);
            expect(parseCol("AAA")).toBe(702);
            expect(parseCol("ZZZ")).toBe(18277);
            expect(parseCol("AAAA")).toBe(18278);
            expect(parseCol("ZZZZ")).toBe(475253);
        });
        it("should accept lower-case and mixed-case column names", () => {
            expect(parseCol("a")).toBe(0);
            expect(parseCol("z")).toBe(25);
            expect(parseCol("aa")).toBe(26);
            expect(parseCol("aA")).toBe(26);
            expect(parseCol("Aa")).toBe(26);
        });
        it("should return -1 for invalid column names", () => {
            expect(parseCol("")).toBe(-1);
            expect(parseCol("0")).toBe(-1);
            expect(parseCol("$")).toBe(-1);
            expect(parseCol(" A ")).toBe(-1);
            expect(parseCol("AAAAA")).toBe(-1);
        });
    });

    describe("function parseRow", () => {
        const { parseRow } = address;
        it("should exist", () => {
            expect(parseRow).toBeFunction();
        });
        it("should return the correct row index", () => {
            expect(parseRow("1")).toBe(0);
            expect(parseRow("2")).toBe(1);
            expect(parseRow("3")).toBe(2);
            expect(parseRow("9")).toBe(8);
            expect(parseRow("10")).toBe(9);
            expect(parseRow("11")).toBe(10);
            expect(parseRow("99999999")).toBe(99999998);
        });
        it("should accept leading zero characters", () => {
            expect(parseRow("01")).toBe(0);
            expect(parseRow("0002")).toBe(1);
            expect(parseRow("099999999")).toBe(99999998);
        });
        it("should return -1 for invalid row names", () => {
            expect(parseRow("")).toBe(-1);
            expect(parseRow("0")).toBe(-1);
            expect(parseRow("-1")).toBe(-1);
            expect(parseRow("A")).toBe(-1);
            expect(parseRow("$")).toBe(-1);
            expect(parseRow(" 1 ")).toBe(-1);
            expect(parseRow("100000000")).toBe(-1);
        });
    });

    // class Address ----------------------------------------------------------

    describe("class Address", () => {

        const { Address } = address;
        it("should exist", () => {
            expect(Address).toBeClass();
        });

        describe("constructor", () => {
            it("should create an address", () => {
                const a1 = new Address(1, 3);
                expect(a1).toHaveProperty("c", 1);
                expect(a1).toHaveProperty("r", 3);
            });
        });

        describe("static function fromDir", () => {
            it("should exist", () => {
                expect(Address).toHaveStaticMethod("fromDir");
            });
            it("should return the correct cell address", () => {
                expect(Address.fromDir(true, 1, 2)).toEqual(new Address(1, 2));
                expect(Address.fromDir(false, 1, 2)).toEqual(new Address(2, 1));
            });
        });

        describe("static function parse", () => {
            it("should exist", () => {
                expect(Address).toHaveStaticMethod("parse");
            });
            it("should return the correct cell address", () => {
                expect(Address.parse("A1")).toEqual(new Address(0, 0));
                expect(Address.parse("B1")).toEqual(new Address(1, 0));
                expect(Address.parse("C1")).toEqual(new Address(2, 0));
                expect(Address.parse("A2")).toEqual(new Address(0, 1));
                expect(Address.parse("A3")).toEqual(new Address(0, 2));
                expect(Address.parse("ZZZZ99999999")).toEqual(new Address(475253, 99999998));
            });
            it("should accept lower-case columns and leading zero characters", () => {
                expect(Address.parse("aa010")).toEqual(new Address(26, 9));
                expect(Address.parse("aA0010")).toEqual(new Address(26, 9));
                expect(Address.parse("Aa00010")).toEqual(new Address(26, 9));
            });
            it("should return null for invalid cell addresses", () => {
                expect(Address.parse("")).toBeNull();
                expect(Address.parse("A")).toBeNull();
                expect(Address.parse("1")).toBeNull();
                expect(Address.parse("$")).toBeNull();
                expect(Address.parse("$A$1")).toBeNull();
                expect(Address.parse("A0")).toBeNull();
                expect(Address.parse("AAAAA1")).toBeNull();
                expect(Address.parse("A100000000")).toBeNull();
            });
        });

        describe("static function compare", () => {
            const a1 = a("C3");
            it("should exist", () => {
                expect(Address).toHaveStaticMethod("compare");
            });
            it("should return 0 for equal addresses", () => {
                expect(Address.compare(a1, a1)).toBe(0);
            });
            it("should return negative value for addresses in order", () => {
                expect(Address.compare(a1, a("D3"))).toBe(-1);
                expect(Address.compare(a1, a("C4"))).toBe(-1);
                expect(Address.compare(a1, a("A4"))).toBe(-1);
            });
            it("should return positive value for addresses in reversed order", () => {
                expect(Address.compare(a1, a("B3"))).toBe(1);
                expect(Address.compare(a1, a("C2"))).toBe(1);
                expect(Address.compare(a1, a("E2"))).toBe(1);
            });
        });

        describe("static function compareVert", () => {
            const a1 = a("C3");
            it("should exist", () => {
                expect(Address).toHaveStaticMethod("compareVert");
            });
            it("should return 0 for equal addresses", () => {
                expect(Address.compareVert(a1, a1)).toBe(0);
            });
            it("should return negative value for addresses in order", () => {
                expect(Address.compareVert(a1, a("C4"))).toBe(-1);
                expect(Address.compareVert(a1, a("D3"))).toBe(-1);
                expect(Address.compareVert(a1, a("D1"))).toBe(-1);
            });
            it("should return positive value for addresses in reversed order", () => {
                expect(Address.compareVert(a1, a("C2"))).toBe(1);
                expect(Address.compareVert(a1, a("B3"))).toBe(1);
                expect(Address.compareVert(a1, a("B5"))).toBe(1);
            });
        });

        describe("property key", () => {
            it("should return unique key for cell address", () => {
                expect(a("A2").key).toBe("0,1");
                expect(a("B3").key).toBe("1,2");
                expect(a("ZZ999").key).toBe("701,998");
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(Address).toHaveMethod("clone");
            });
            it("should return a clone", () => {
                const a1 = a("B4"), a2 = a1.clone();
                expect(a2).toBeInstanceOf(Address);
                expect(a2).not.toBe(a1);
                expect(a2).toEqual(a1);
            });
        });

        describe("method equals", () => {
            const a1 = a("B4");
            it("should exist", () => {
                expect(Address).toHaveMethod("equals");
            });
            it("should return true for equal addresses", () => {
                expect(a1.equals(a1)).toBeTrue();
                expect(a1.equals(a1.clone())).toBeTrue();
            });
            it("should return false for different addresses", () => {
                expect(a1.equals(a("C4"))).toBeFalse();
                expect(a1.equals(a("B3"))).toBeFalse();
                expect(a1.equals(a("C3"))).toBeFalse();
            });
        });

        describe("method differs", () => {
            const a1 = a("B4");
            it("should exist", () => {
                expect(Address).toHaveMethod("differs");
            });
            it("should return false for equal addresses", () => {
                expect(a1.differs(a1)).toBeFalse();
                expect(a1.differs(a1.clone())).toBeFalse();
            });
            it("should return true for different addresses", () => {
                expect(a1.differs(a("C4"))).toBeTrue();
                expect(a1.differs(a("B3"))).toBeTrue();
                expect(a1.differs(a("C3"))).toBeTrue();
            });
        });

        describe("method compareTo", () => {
            const a1 = a("C3");
            it("should exist", () => {
                expect(Address).toHaveMethod("compareTo");
            });
            it("should return 0 for equal addresses", () => {
                expect(a1.compareTo(a1)).toBe(0);
            });
            it("should return negative value for addresses in order", () => {
                expect(a1.compareTo(a("D3"))).toBe(-1);
                expect(a1.compareTo(a("C4"))).toBe(-1);
                expect(a1.compareTo(a("A4"))).toBe(-1);
            });
            it("should return positive value for addresses in reversed order", () => {
                expect(a1.compareTo(a("B3"))).toBe(1);
                expect(a1.compareTo(a("C2"))).toBe(1);
                expect(a1.compareTo(a("E2"))).toBe(1);
            });
        });

        describe("method compareVertTo", () => {
            const a1 = a("C3");
            it("should exist", () => {
                expect(Address).toHaveMethod("compareVertTo");
            });
            it("should return 0 for equal addresses", () => {
                expect(a1.compareVertTo(a1)).toBe(0);
            });
            it("should return negative value for addresses in order", () => {
                expect(a1.compareVertTo(a("C4"))).toBe(-1);
                expect(a1.compareVertTo(a("D3"))).toBe(-1);
                expect(a1.compareVertTo(a("D1"))).toBe(-1);
            });
            it("should return positive value for addresses in reversed order", () => {
                expect(a1.compareVertTo(a("C2"))).toBe(1);
                expect(a1.compareVertTo(a("B3"))).toBe(1);
                expect(a1.compareVertTo(a("B5"))).toBe(1);
            });
        });

        describe("method get", () => {
            it("should exist", () => {
                expect(Address).toHaveMethod("get");
            });
            it("should return the correct index", () => {
                const a1 = a("B3");
                expect(a1.get(true)).toBe(1);
                expect(a1.get(false)).toBe(2);
            });
        });

        describe("method mirror", () => {
            it("should exist", () => {
                expect(Address).toHaveMethod("mirror");
            });
            it("should return the mirrored address", () => {
                expect(a("A2").mirror()).toEqual(a("B1"));
                expect(a("B1").mirror()).toEqual(a("A2"));
            });
        });

        describe("method set", () => {
            it("should exist", () => {
                expect(Address).toHaveMethod("set");
            });
            it("should change the correct index", () => {
                const a1 = a("A1");
                expect(a1).toEqual(a("A1"));
                a1.set(1, true);
                expect(a1).toEqual(a("B1"));
                a1.set(2, false);
                expect(a1).toEqual(a("B3"));
            });
            it("should return itself", () => {
                const a1 = a("A1");
                expect(a1.set(1, true)).toBe(a1);
            });
        });

        describe("method assign", () => {
            it("should exist", () => {
                expect(Address).toHaveMethod("assign");
            });
            it("should change the indexes", () => {
                const a1 = a("C4");
                expect(a1).toEqual(a("C4"));
                a1.assign(a("B3"));
                expect(a1).toEqual(a("B3"));
            });
            it("should return itself", () => {
                const a1 = a("A1");
                expect(a1.assign(a("B3"))).toBe(a1);
            });
        });

        describe("method move", () => {
            it("should exist", () => {
                expect(Address).toHaveMethod("move");
            });
            it("should change the correct index", () => {
                const a1 = a("C4");
                expect(a1).toEqual(a("C4"));
                a1.move(2, true);
                expect(a1).toEqual(a("E4"));
                a1.move(-2, true);
                expect(a1).toEqual(a("C4"));
                a1.move(2, false);
                expect(a1).toEqual(a("C6"));
                a1.move(-2, false);
                expect(a1).toEqual(a("C4"));
            });
            it("should return itself", () => {
                const a1 = a("A1");
                expect(a1.move(1, true)).toBe(a1);
            });
        });

        describe("method toOpStr", () => {
            it("should exist", () => {
                expect(Address).toHaveMethod("toOpStr");
            });
            it("should stringify the address", () => {
                expect(a("A1").toOpStr()).toBe("A1");
                expect(a("B1").toOpStr()).toBe("B1");
                expect(a("C1").toOpStr()).toBe("C1");
                expect(a("A2").toOpStr()).toBe("A2");
                expect(a("A3").toOpStr()).toBe("A3");
                expect(a("ZZZZ99999999").toOpStr()).toBe("ZZZZ99999999");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(Address).toHaveMethod("toString");
            });
            it("should stringify the address", () => {
                expect(a("A1").toString()).toBe("A1");
                expect(a("B1").toString()).toBe("B1");
                expect(a("C1").toString()).toBe("C1");
                expect(a("A2").toString()).toBe("A2");
                expect(a("A3").toString()).toBe("A3");
                expect(a("ZZZZ99999999").toString()).toBe("ZZZZ99999999");
            });
            it("should stringify implicitly", () => {
                expect("<" + a("A3") + ">").toBe("<A3>");
            });
        });
    });
});
