/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ArrayBase } from "@/io.ox/office/spreadsheet/utils/arraybase";
import { IntervalArray } from "@/io.ox/office/spreadsheet/utils/intervalarray";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";

import { i, a, r, ia, aa, ra, unorderedRangesMatcher as rangesMatcher } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/utils/rangearray", () => {

    // class RangeArray -------------------------------------------------------

    describe("class RangeArray", () => {

        it("should subclass ArrayBase", () => {
            expect(RangeArray).toBeSubClassOf(ArrayBase);
            expect(RangeArray).toBeSubClassOf(Array);
        });

        describe("constructor", () => {
            it("should create a range array", () => {
                const ra1 = new RangeArray();
                expect(ra1).toBeArrayOfSize(0);
            });
            const r1 = r("A2:C4"), r2 = r("B3:D5");
            it("should insert single ranges", () => {
                const ra1 = new RangeArray(r1);
                expect(ra1).toHaveLength(1);
                expect(ra1[0]).toBe(r1);
                const ra2 = new RangeArray(r1, r2, null, r1);
                expect(ra2).toHaveLength(3);
                expect(ra2[0]).toBe(r1);
                expect(ra2[1]).toBe(r2);
                expect(ra2[2]).toBe(r1);
            });
            it("should insert plain arrays of ranges", () => {
                const ra1 = new RangeArray([r1]);
                expect(ra1).toHaveLength(1);
                expect(ra1[0]).toBe(r1);
                const ra2 = new RangeArray([r1, r2], [], [r1]);
                expect(ra2).toHaveLength(3);
                expect(ra2[0]).toBe(r1);
                expect(ra2[1]).toBe(r2);
                expect(ra2[2]).toBe(r1);
            });
            it("should copy-construct range arrays", () => {
                const ra0 = new RangeArray(r1, r2);
                const ra1 = new RangeArray(ra0);
                expect(ra1).toHaveLength(2);
                expect(ra1[0]).toBe(r1);
                expect(ra1[1]).toBe(r2);
                const ra2 = new RangeArray(ra0, new RangeArray(), ra1);
                expect(ra2).toHaveLength(4);
                expect(ra2[0]).toBe(r1);
                expect(ra2[1]).toBe(r2);
                expect(ra2[2]).toBe(r1);
                expect(ra2[3]).toBe(r2);
            });
        });

        describe("static function cast", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveStaticMethod("cast");
            });
            it("should not convert range arrays", () => {
                const ra1 = ra("A1:B2 C3:D4");
                expect(RangeArray.cast(ra1)).toBe(ra1);
            });
            it("should convert plain arrays", () => {
                const ra1 = [r("A1:B2"), r("C3:D4")];
                const ra2 = RangeArray.cast(ra1);
                expect(ra2).toBeInstanceOf(RangeArray);
                expect(ra2).toHaveLength(ra1.length);
                ra1.forEach((v, idx) => expect(ra2[idx]).toBe(v));
            });
            it("should convert single object to array", () => {
                const r1 = r("A1:B2");
                const ra1 = RangeArray.cast(r1);
                expect(ra1).toBeInstanceOf(RangeArray);
                expect(ra1).toHaveLength(1);
                expect(ra1[0]).toBe(r1);
            });
            it("should convert nullish to empty arrays", () => {
                const ra1 = RangeArray.cast(null);
                const ra2 = RangeArray.cast(undefined);
                expect(ra1).toBeInstanceOf(RangeArray);
                expect(ra1).toHaveLength(0);
                expect(ra2).toBeInstanceOf(RangeArray);
                expect(ra2).toHaveLength(0);
            });
        });

        describe("static function filter", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveStaticMethod("filter");
            });
        });

        describe("static function map", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveStaticMethod("map");
            });
        });

        describe("static function fromIntervals", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveStaticMethod("fromIntervals");
            });
            const ia1 = ia("B:C C:D"), ia2 = ia("3:4 4:5");
            it("should create range addresses from column and row intervals", () => {
                expect(RangeArray.fromIntervals(ia1, ia2)).toEqual(ra("B3:C4 C3:D4 B4:C5 C4:D5"));
            });
            it("should accept single intervals", () => {
                const i1 = i("B:D"), i2 = i("3:5");
                expect(RangeArray.fromIntervals(i1, ia2)).toEqual(ra("B3:D4 B4:D5"));
                expect(RangeArray.fromIntervals(ia1, i2)).toEqual(ra("B3:C5 C3:D5"));
                expect(RangeArray.fromIntervals(i1, i2)).toEqual(ra("B3:D5"));
            });
            it("should accept empty arrays", () => {
                const ia0 = new IntervalArray();
                expect(RangeArray.fromIntervals(ia1, ia0)).toHaveLength(0);
                expect(RangeArray.fromIntervals(ia0, ia2)).toHaveLength(0);
                expect(RangeArray.fromIntervals(ia0, ia0)).toHaveLength(0);
            });
        });

        describe("static function fromColIntervals", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveStaticMethod("fromColIntervals");
            });
            const ia1 = ia("B:C C:D");
            it("should create range addresses from column intervals", () => {
                expect(RangeArray.fromColIntervals(ia1, 2, 3)).toEqual(ra("B3:C4 C3:D4"));
                expect(RangeArray.fromColIntervals(ia1, 2)).toEqual(ra("B3:C3 C3:D3"));
            });
        });

        describe("static function fromRowIntervals", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveStaticMethod("fromRowIntervals");
            });
            const ia1 = ia("3:4 4:5");
            it("should create range addresses from row intervals", () => {
                expect(RangeArray.fromRowIntervals(ia1, 1, 2)).toEqual(ra("B3:C4 B4:C5"));
                expect(RangeArray.fromRowIntervals(ia1, 1)).toEqual(ra("B3:B4 B4:B5"));
            });
        });

        describe("static function mergeAddresses", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveStaticMethod("mergeAddresses");
            });
            it("should merge cell addresses to range", () => {
                expect(RangeArray.mergeAddresses(aa("A1 A3 A4 A2 A1"))).toEqual(ra("A1:A4"));
                expect(RangeArray.mergeAddresses(aa("A1 B2 B1 A2"))).toEqual(ra("A1:B2"));
            });
            it("should merge a set of addresses", () => {
                expect(RangeArray.mergeAddresses(new Set(aa("A1 A3 A4 A2")))).toEqual(ra("A1:A4"));
            });
            it("should merge a value source with map function", () => {
                expect(RangeArray.mergeAddresses(["A1", "A3", "A4", "A2"], a)).toEqual(ra("A1:A4"));
                expect(RangeArray.mergeAddresses(new Set(["A1", "A3", "A4", "A2"]), a)).toEqual(ra("A1:A4"));
            });
            it("should accept an empty array", () => {
                expect(RangeArray.mergeAddresses([])).toBeArrayOfSize(0);
            });
        });

        describe("static function mergeRanges", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveStaticMethod("mergeRanges");
            });
            it("should merge cell ranges", () => {
                expect(RangeArray.mergeRanges(ra("C6:D7 B3:C4 E4:F5"))).toSatisfy(rangesMatcher("B3:C4 E4:F5 C6:D7"));
            });
            it("should merge a set of ranges", () => {
                expect(RangeArray.mergeRanges(new Set(ra("C6:D7 B3:C4 E4:F5")))).toSatisfy(rangesMatcher("B3:C4 E4:F5 C6:D7"));
            });
            it("should merge a value source with map function", () => {
                expect(RangeArray.mergeRanges(["C6:D7", "B3:C4", "E4:F5"], r)).toSatisfy(rangesMatcher("B3:C4 E4:F5 C6:D7"));
                expect(RangeArray.mergeRanges(new Set(["C6:D7", "B3:C4", "E4:F5"]), r)).toSatisfy(rangesMatcher("B3:C4 E4:F5 C6:D7"));
            });
            it("should accept an empty array", () => {
                expect(RangeArray.mergeRanges([])).toBeArrayOfSize(0);
            });
        });

        describe("static function splitRanges", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveStaticMethod("splitRanges");
            });
            it("should accept an empty array and null", () => {
                expect(RangeArray.splitRanges([])).toBeArrayOfSize(0);
                expect(RangeArray.splitRanges(null)).toBeArrayOfSize(0);
            });
        });

        describe("property key", () => {
            it("should return unique key for the array", () => {
                expect(ra("A2:C4 B3:D5").key).toBe("0,1:2,3 1,2:3,4");
                expect(ra("").key).toBe("");
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("clone");
            });
            it("should return a shallow clone", () => {
                const ra1 = ra("A2:C4 B3:D5"), ra2 = ra1.clone();
                expect(ra2).toBeInstanceOf(RangeArray);
                expect(ra2).not.toBe(ra1);
                expect(ra2[0]).toBe(ra1[0]);
                expect(ra2[1]).toBe(ra1[1]);
                expect(ra2).toEqual(ra1);
            });
        });

        describe("method deepClone", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("deepClone");
            });
            it("should return a deep clone", () => {
                const ra1 = ra("A2:C4 B3:D5"), ra2 = ra1.deepClone();
                expect(ra2).toBeInstanceOf(RangeArray);
                expect(ra2).not.toBe(ra1);
                expect(ra2[0]).not.toBe(ra1[0]);
                expect(ra2[1]).not.toBe(ra1[1]);
                expect(ra2).toEqual(ra1);
            });
        });

        describe("method indexOf", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("indexOf");
            });
            const ra1 = ra("B4:C5 C5:D6 D6:E7 C5:D6 B4:C5");
            it("should return array index of strictly equal range", () => {
                expect(ra1.indexOf(ra1[0])).toBe(0);
                expect(ra1.indexOf(ra1[1])).toBe(1);
                expect(ra1.indexOf(ra1[2])).toBe(2);
                expect(ra1.indexOf(ra1[3])).toBe(1);
                expect(ra1.indexOf(ra1[4])).toBe(0);
            });
            it("should return array index of deeply equal range", () => {
                expect(ra1.indexOf(r("B4:C5"))).toBe(0);
                expect(ra1.indexOf(r("C5:D6"))).toBe(1);
                expect(ra1.indexOf(r("D6:E7"))).toBe(2);
                expect(ra1.indexOf(r("E7:F8"))).toBe(-1);
            });
        });

        describe("method lastIndexOf", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("lastIndexOf");
            });
            const ra1 = ra("B4:C5 C5:D6 D6:E7 C5:D6 B4:C5");
            it("should return array index of strictly equal range", () => {
                expect(ra1.lastIndexOf(ra1[0])).toBe(4);
                expect(ra1.lastIndexOf(ra1[1])).toBe(3);
                expect(ra1.lastIndexOf(ra1[2])).toBe(2);
                expect(ra1.lastIndexOf(ra1[3])).toBe(3);
                expect(ra1.lastIndexOf(ra1[4])).toBe(4);
            });
            it("should return array index of deeply equal range", () => {
                expect(ra1.lastIndexOf(r("B4:C5"))).toBe(4);
                expect(ra1.lastIndexOf(r("C5:D6"))).toBe(3);
                expect(ra1.lastIndexOf(r("D6:E7"))).toBe(2);
                expect(ra1.lastIndexOf(r("E7:F8"))).toBe(-1);
            });
        });

        describe("method cells", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("cells");
            });
            it("should count cells in ranges", () => {
                expect(ra("A1:C3 B2:D4").cells()).toBe(18);
                expect(new RangeArray().cells()).toBe(0);
            });
        });

        describe("method containsCol", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("containsCol");
            });
            const ra1 = ra("B4:D6 G9:H10 D6:E7");
            it("should return false for columns outside the ranges", () => {
                expect(ra1.containsCol(0)).toBeFalse();
                expect(ra1.containsCol(5)).toBeFalse();
                expect(ra1.containsCol(8)).toBeFalse();
                expect(ra1.containsCol(9)).toBeFalse();
                expect(ra1.containsCol(10)).toBeFalse();
                expect(ra1.containsCol(11)).toBeFalse();
                expect(ra1.containsCol(12)).toBeFalse();
            });
            it("should return true for columns inside the ranges", () => {
                expect(ra1.containsCol(1)).toBeTrue();
                expect(ra1.containsCol(2)).toBeTrue();
                expect(ra1.containsCol(3)).toBeTrue();
                expect(ra1.containsCol(4)).toBeTrue();
                expect(ra1.containsCol(6)).toBeTrue();
                expect(ra1.containsCol(7)).toBeTrue();
            });
        });

        describe("method containsRow", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("containsRow");
            });
            const ra1 = ra("B4:D6 G9:H10 D6:E7");
            it("should return false for rows outside the ranges", () => {
                expect(ra1.containsRow(0)).toBeFalse();
                expect(ra1.containsRow(1)).toBeFalse();
                expect(ra1.containsRow(2)).toBeFalse();
                expect(ra1.containsRow(7)).toBeFalse();
                expect(ra1.containsRow(10)).toBeFalse();
                expect(ra1.containsRow(11)).toBeFalse();
                expect(ra1.containsRow(12)).toBeFalse();
            });
            it("should return true for rows inside the ranges", () => {
                expect(ra1.containsRow(3)).toBeTrue();
                expect(ra1.containsRow(4)).toBeTrue();
                expect(ra1.containsRow(5)).toBeTrue();
                expect(ra1.containsRow(6)).toBeTrue();
                expect(ra1.containsRow(8)).toBeTrue();
                expect(ra1.containsRow(9)).toBeTrue();
            });
        });

        describe("method containsIndex", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("containsIndex");
            });
            const ra1 = ra("C2:C2 E4:E4");
            it("should check column indexes", () => {
                expect(ra1.containsIndex(1, true)).toBeFalse();
                expect(ra1.containsIndex(2, true)).toBeTrue();
                expect(ra1.containsIndex(3, true)).toBeFalse();
                expect(ra1.containsIndex(4, true)).toBeTrue();
                expect(ra1.containsIndex(5, true)).toBeFalse();
            });
            it("should check row indexes", () => {
                expect(ra1.containsIndex(0, false)).toBeFalse();
                expect(ra1.containsIndex(1, false)).toBeTrue();
                expect(ra1.containsIndex(2, false)).toBeFalse();
                expect(ra1.containsIndex(3, false)).toBeTrue();
                expect(ra1.containsIndex(4, false)).toBeFalse();
            });
        });

        describe("method containsAddress", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("containsAddress");
            });
            const ra1 = ra("B4:C5 C5:D6");
            it("should return false for cells outside the ranges", () => {
                "A3 B3 C3 D3 E3 A4 D4 E4 A5 E5 A6 B6 E6 A7 B7 C7 D7 E7".split(" ").forEach(text => {
                    expect(ra1.containsAddress(a(text))).toBeFalse();
                });
            });
            it("should return true for cells inside the ranges", () => {
                expect(ra1.containsAddress(a("B4"))).toBeTrue();
                expect(ra1.containsAddress(a("C4"))).toBeTrue();
                expect(ra1.containsAddress(a("B5"))).toBeTrue();
                expect(ra1.containsAddress(a("C5"))).toBeTrue();
                expect(ra1.containsAddress(a("D5"))).toBeTrue();
                expect(ra1.containsAddress(a("C6"))).toBeTrue();
                expect(ra1.containsAddress(a("D6"))).toBeTrue();
            });
        });

        describe("method contains", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("contains");
            });
            const ra1 = ra("A1:B2 B2:C3 C3:D4");
            it("should return false for ranges outside the ranges", () => {
                expect(ra1.contains(ra("C1:D1 B2:C3 C3:D4"))).toBeFalse();
                expect(ra1.contains(ra("A1:B2 C1:D1 C3:D4"))).toBeFalse();
                expect(ra1.contains(ra("A1:B2 B2:C3 C1:D1"))).toBeFalse();
            });
            it("should return false for overlapping ranges", () => {
                expect(ra1.contains(ra("A1:B3 B2:C3 C3:D4"))).toBeFalse();
                expect(ra1.contains(ra("A1:C2 B2:C3 C3:D4"))).toBeFalse();
                expect(ra1.contains(ra("A1:B2 A2:C3 C3:D4"))).toBeFalse();
                expect(ra1.contains(ra("A1:B2 B1:C3 C3:D4"))).toBeFalse();
                expect(ra1.contains(ra("A1:B2 B2:D3 C3:D4"))).toBeFalse();
                expect(ra1.contains(ra("A1:B2 B2:C4 C3:D4"))).toBeFalse();
                expect(ra1.contains(ra("A1:B2 B2:C3 B3:D4"))).toBeFalse();
                expect(ra1.contains(ra("A1:B2 B2:C3 C2:D4"))).toBeFalse();
                expect(ra1.contains(ra("A1:B2 B2:C3 C3:E4"))).toBeFalse();
                expect(ra1.contains(ra("A1:B2 B2:C3 C3:D5"))).toBeFalse();
            });
            it("should return true for contained ranges", () => {
                expect(ra1.contains(ra("A1:B1 A1:A2 A1:B2"))).toBeTrue();
                expect(ra1.contains(ra("A1:B1 B2:C2 C3:D3"))).toBeTrue();
                expect(ra1.contains(ra1)).toBeTrue();
            });
            it("should accept empty array", () => {
                const ra0 = new RangeArray();
                expect(ra0.contains(ra1)).toBeFalse();
                expect(ra1.contains(ra0)).toBeTrue();
                expect(ra0.contains(ra0)).toBeTrue();
            });
            it("should accept single range address", () => {
                expect(ra1.contains(r("C1:D1"))).toBeFalse();
                expect(ra1.contains(r("A1:B3"))).toBeFalse();
                expect(ra1.contains(r("B2:C3"))).toBeTrue();
            });
        });

        describe("method overlaps", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("overlaps");
            });
            const ra1 = ra("A1:B2 B2:C3 C3:D4");
            it("should return false for distinct ranges", () => {
                expect(ra1.overlaps(ra("C1:D1 A3:A4 D1:D2 A4:B4"))).toBeFalse();
            });
            it("should return true for overlapping ranges", () => {
                expect(ra1.overlaps(ra("C1:D1 A3:B4 D1:D2"))).toBeTrue();
            });
            it("should accept empty array", () => {
                const ra0 = new RangeArray();
                expect(ra0.overlaps(ra1)).toBeFalse();
                expect(ra1.overlaps(ra0)).toBeFalse();
                expect(ra0.overlaps(ra0)).toBeFalse();
            });
            it("should accept single range address", () => {
                expect(ra1.overlaps(r("C1:D1"))).toBeFalse();
                expect(ra1.overlaps(r("A3:B4"))).toBeTrue();
            });
        });

        describe("method distinct", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("distinct");
            });
            it("should return true for distinct ranges", () => {
                expect(ra("B3:C4").distinct()).toBeTrue();
                expect(ra("B3:C4 D3:E4").distinct()).toBeTrue();
                expect(ra("B3:C4 D3:E4 B5:C6").distinct()).toBeTrue();
                expect(ra("B3:C4 D3:E4 B5:C6 D5:E6").distinct()).toBeTrue();
            });
            it("should return false for overlapping ranges", () => {
                expect(ra("B3:C4 C4:D5").distinct()).toBeFalse();
                expect(ra("B3:C4 D5:E6 C4:D5").distinct()).toBeFalse();
                expect(ra("B3:C4 D3:E4 B5:D6 D5:E6 B3:C4").distinct()).toBeFalse();
                expect(ra("B3:C4 D3:E4 B5:D6 D5:E6 C4:D5").distinct()).toBeFalse();
            });
            it("should accept empty array", () => {
                expect(new RangeArray().distinct()).toBeTrue();
            });
        });

        describe("method findByCol", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("findByCol");
            });
            const ra1 = ra("B4:D6 G9:H10 D6:E7");
            it("should return null for columns outside the ranges", () => {
                expect(ra1.findByCol(0)).toBeNull();
                expect(ra1.findByCol(5)).toBeNull();
                expect(ra1.findByCol(8)).toBeNull();
                expect(ra1.findByCol(9)).toBeNull();
                expect(ra1.findByCol(10)).toBeNull();
                expect(ra1.findByCol(11)).toBeNull();
                expect(ra1.findByCol(12)).toBeNull();
            });
            it("should return first existing range", () => {
                expect(ra1.findByCol(1)).toBe(ra1[0]);
                expect(ra1.findByCol(2)).toBe(ra1[0]);
                expect(ra1.findByCol(3)).toBe(ra1[0]);
                expect(ra1.findByCol(4)).toBe(ra1[2]);
                expect(ra1.findByCol(6)).toBe(ra1[1]);
                expect(ra1.findByCol(7)).toBe(ra1[1]);
            });
        });

        describe("method findByRow", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("findByRow");
            });
            const ra1 = ra("B4:D6 G9:H10 D6:E7");
            it("should return null for rows outside the ranges", () => {
                expect(ra1.findByRow(0)).toBeNull();
                expect(ra1.findByRow(1)).toBeNull();
                expect(ra1.findByRow(2)).toBeNull();
                expect(ra1.findByRow(7)).toBeNull();
                expect(ra1.findByRow(10)).toBeNull();
                expect(ra1.findByRow(11)).toBeNull();
                expect(ra1.findByRow(12)).toBeNull();
            });
            it("should return first existing range", () => {
                expect(ra1.findByRow(3)).toBe(ra1[0]);
                expect(ra1.findByRow(4)).toBe(ra1[0]);
                expect(ra1.findByRow(5)).toBe(ra1[0]);
                expect(ra1.findByRow(6)).toBe(ra1[2]);
                expect(ra1.findByRow(8)).toBe(ra1[1]);
                expect(ra1.findByRow(9)).toBe(ra1[1]);
            });
        });

        describe("method findByIndex", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("findByIndex");
            });
            const ra1 = ra("C2:C2 E4:E4");
            it("should check column indexes", () => {
                expect(ra1.findByIndex(1, true)).toBeNull();
                expect(ra1.findByIndex(2, true)).toBe(ra1[0]);
                expect(ra1.findByIndex(3, true)).toBeNull();
                expect(ra1.findByIndex(4, true)).toBe(ra1[1]);
                expect(ra1.findByIndex(5, true)).toBeNull();
            });
            it("should check row indexes", () => {
                expect(ra1.findByIndex(0, false)).toBeNull();
                expect(ra1.findByIndex(1, false)).toBe(ra1[0]);
                expect(ra1.findByIndex(2, false)).toBeNull();
                expect(ra1.findByIndex(3, false)).toBe(ra1[1]);
                expect(ra1.findByIndex(4, false)).toBeNull();
            });
        });

        describe("method findByAddress", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("findByAddress");
            });
            const ra1 = ra("B4:C5 C5:D6");
            it("should return null for cells outside the ranges", () => {
                "A3 B3 C3 D3 E3 A4 D4 E4 A5 E5 A6 B6 E6 A7 B7 C7 D7 E7".split(" ").forEach(text => {
                    expect(ra1.findByAddress(a(text))).toBeNull();
                });
            });
            it("should return first existing range", () => {
                expect(ra1.findByAddress(a("B4"))).toBe(ra1[0]);
                expect(ra1.findByAddress(a("C4"))).toBe(ra1[0]);
                expect(ra1.findByAddress(a("B5"))).toBe(ra1[0]);
                expect(ra1.findByAddress(a("C5"))).toBe(ra1[0]);
                expect(ra1.findByAddress(a("D5"))).toBe(ra1[1]);
                expect(ra1.findByAddress(a("C6"))).toBe(ra1[1]);
                expect(ra1.findByAddress(a("D6"))).toBe(ra1[1]);
            });
        });

        describe("method colIntervals", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("colIntervals");
            });
            it("should return the column intervals", () => {
                expect(ra("B3:C4 D5:E6 C4:D5").colIntervals()).toEqual(ia("B:C D:E C:D"));
                expect(new RangeArray().colIntervals()).toBeArrayOfSize(0);
            });
        });

        describe("method rowIntervals", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("rowIntervals");
            });
            it("should return the row intervals", () => {
                expect(ra("B3:C4 D5:E6 C4:D5").rowIntervals()).toEqual(ia("3:4 5:6 4:5"));
                expect(new RangeArray().rowIntervals()).toBeArrayOfSize(0);
            });
        });

        describe("method intervals", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("intervals");
            });
            it("should return the column intervals", () => {
                expect(ra("B3:C4 D5:E6 C4:D5").intervals(true)).toEqual(ia("B:C D:E C:D"));
            });
            it("should return the row intervals", () => {
                expect(ra("B3:C4 D5:E6 C4:D5").intervals(false)).toEqual(ia("3:4 5:6 4:5"));
            });
        });

        describe("method boundary", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("boundary");
            });
            it("should return the bounding range", () => {
                expect(ra("B3:C4 C4:D5").boundary()).toEqual(r("B3:D5"));
                expect(ra("C4:D5 B3:C4 A2:B3").boundary()).toEqual(r("A2:D5"));
            });
            it("should return a clone of a single range", () => {
                const ra1 = ra("B3:C4"), r1 = ra1.boundary();
                expect(r1).toEqual(ra1[0]);
                expect(r1).not.toBe(ra1[0]);
            });
            it("should return undefined for an empty array", () => {
                expect(new RangeArray().boundary()).toBeUndefined();
            });
        });

        describe("method refAddress", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("refAddress");
            });
            it("should return the reference address", () => {
                expect(ra("B3:C4 C4:D5").refAddress()).toEqual(a("B3"));
                expect(ra("C4:D5 B3:C4 A2:B3").refAddress()).toEqual(a("A2"));
            });
            it("should return a clone of the start address of a single range", () => {
                const ra1 = ra("B3:C4"), a1 = ra1.refAddress();
                expect(a1).toEqual(ra1[0].a1);
                expect(a1).not.toBe(ra1[0].a1);
            });
            it("should return undefined for an empty array", () => {
                expect(new RangeArray().refAddress()).toBeUndefined();
            });
        });

        describe("method unify", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("unify");
            });
            it("should return a shallow copy", () => {
                const ra1 = ra("A2:C4 B3:D5"), ra2 = ra1.unify();
                expect(ra2).not.toBe(ra1);
                expect(ra2[0]).toBe(ra1[0]);
                expect(ra2[1]).toBe(ra1[1]);
            });
            it("should remove all duplicates", () => {
                const ra1 = ra("A2:C4 B3:D5 A2:C4 B3:C4 B3:D5 B3:C4"), ra2 = ra1.unify();
                expect(ra2).toHaveLength(3);
                expect(ra2[0]).toBe(ra1[0]);
                expect(ra2[1]).toBe(ra1[1]);
                expect(ra2[2]).toBe(ra1[3]);
            });
            it("should accept empty array", () => {
                const ra0 = new RangeArray(), ra1 = ra0.unify();
                expect(ra1).toBeInstanceOf(RangeArray);
                expect(ra1).toHaveLength(0);
                expect(ra1).not.toBe(ra0);
            });
        });

        describe("method filterCovered", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("filterCovered");
            });
            it("should return a shallow copy", () => {
                const ra1 = ra("A2:C4 B3:D5"), ra2 = ra1.filterCovered();
                expect(ra2).not.toBe(ra1);
                expect(ra2[0]).toBe(ra1[0]);
                expect(ra2[1]).toBe(ra1[1]);
            });
            it("should remove all covered ranges", () => {
                const ra1 = ra("B2:D4 D4:F6 D4:E5 A1:A1 A1:B2 B2:D4"), ra2 = ra1.filterCovered();
                expect(ra2).toHaveLength(3);
                expect(ra2[0]).toBe(ra1[0]);
                expect(ra2[1]).toBe(ra1[1]);
                expect(ra2[2]).toBe(ra1[4]);
            });
            it("should accept an empty array", () => {
                const ra0 = new RangeArray(), ra1 = ra0.filterCovered();
                expect(ra1).toBeInstanceOf(RangeArray);
                expect(ra1).toHaveLength(0);
                expect(ra1).not.toBe(ra0);
            });
        });

        describe("method getColBands", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("getColBands");
            });
            it("should return the band intervals", () => {
                expect(ra("A2:C4 B3:D5").getColBands()).toStringifyTo("1:1,2:3,4:4");
                expect(ra("A2:D5 B3:C4").getColBands()).toStringifyTo("1:1,2:3,4:4");
            });
            it("should return the original ranges", () => {
                const colBands = ra("A2:C4 B3:D5").getColBands({ ranges: true });
                expect(colBands).toBeArray();
                for (const colBand of colBands) {
                    expect(colBand.ranges).toBeInstanceOf(RangeArray);
                    expect(colBand).toHaveProperty("intervals", null);
                }
            });
            it("should return the merged row intervals", () => {
                const colBands = ra("A2:C4 B3:D5").getColBands({ intervals: true });
                expect(colBands).toBeArray();
                for (const colBand of colBands) {
                    expect(colBand).toHaveProperty("ranges", null);
                    expect(colBand.intervals).toBeInstanceOf(IntervalArray);
                }
            });
            it("should return the original ranges and merged row intervals", () => {
                const colBands = ra("A2:C4 B3:D5").getColBands({ ranges: true, intervals: true });
                expect(colBands).toBeArray();
                for (const colBand of colBands) {
                    expect(colBand.ranges).toBeInstanceOf(RangeArray);
                    expect(colBand.intervals).toBeInstanceOf(IntervalArray);
                }
            });
        });

        describe("method getRowBands", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("getRowBands");
            });
            it("should return the band intervals", () => {
                expect(ra("A2:C4 B3:D5").getRowBands()).toStringifyTo("2:2,3:4,5:5");
                expect(ra("A2:D5 B3:C4").getRowBands()).toStringifyTo("2:2,3:4,5:5");
            });
            it("should return the original ranges", () => {
                const rowBands = ra("A2:C4 B3:D5").getRowBands({ ranges: true });
                expect(rowBands).toBeArray();
                for (const rowBand of rowBands) {
                    expect(rowBand.ranges).toBeInstanceOf(RangeArray);
                    expect(rowBand).toHaveProperty("intervals", null);
                }
            });
            it("should return the merged column intervals", () => {
                const rowBands = ra("A2:C4 B3:D5").getRowBands({ intervals: true });
                expect(rowBands).toBeArray();
                for (const rowBand of rowBands) {
                    expect(rowBand).toHaveProperty("ranges", null);
                    expect(rowBand.intervals).toBeInstanceOf(IntervalArray);
                }
            });
            it("should return the original ranges and merged column intervals", () => {
                const rowBands = ra("A2:C4 B3:D5").getRowBands({ ranges: true, intervals: true });
                expect(rowBands).toBeArray();
                for (const rowBand of rowBands) {
                    expect(rowBand.ranges).toBeInstanceOf(RangeArray);
                    expect(rowBand.intervals).toBeInstanceOf(IntervalArray);
                }
            });
        });

        describe("method merge", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("merge");
            });
            it("should not merge distinct ranges", () => {
                expect(ra("C6:D7 B3:C4 E4:F5").merge()).toSatisfy(rangesMatcher("B3:C4 E4:F5 C6:D7"));
            });
            it("should merge adjacent ranges", () => {
                expect(ra("B5:C6 B3:C4").merge()).toEqual(ra("B3:C6"));
                expect(ra("D3:E4 B3:C4").merge()).toEqual(ra("B3:E4"));
                expect(ra("D3:E4 B5:C6 B3:C4").merge()).toSatisfy(rangesMatcher("B3:E4 B5:C6"));
                expect(ra("D3:E4 B5:C6 B3:C4 D5:E6").merge()).toEqual(ra("B3:E6"));
                expect(ra("B3:B5 B6:D6 E4:E6 C3:E3 C4:D5").merge()).toEqual(ra("B3:E6"));
            });
            it("should merge overlapping ranges", () => {
                expect(ra("B4:C6 B3:C5").merge()).toEqual(ra("B3:C6"));
                expect(ra("C3:E4 B3:D4").merge()).toEqual(ra("B3:E4"));
                expect(ra("C3:E5 E5:G7").merge()).toSatisfy(rangesMatcher("C3:E4 C5:G5 E6:G7"));
                expect(ra("C3:E4 B4:C6 B3:D4").merge()).toSatisfy(rangesMatcher("B3:E4 B5:C6"));
                expect(ra("C3:E4 B5:D6 B3:D4 D4:E6").merge()).toEqual(ra("B3:E6"));
                expect(ra("B3:B6 B6:E6 E3:E6 B3:E3 C3:C5 C4:E4 D4:D6 B5:D5").merge()).toEqual(ra("B3:E6"));
            });
            it("should split ranges to prevent covering a cell multiple times", () => {
                expect(ra("C3:C5 B3:D3").merge()).toSatisfy(rangesMatcher("B3:D3 C4:C5"));
                expect(ra("C3:C5 B4:D4").merge()).toSatisfy(rangesMatcher("C3:C3 B4:D4 C5:C5"));
                expect(ra("C3:C5 B5:D5").merge()).toSatisfy(rangesMatcher("C3:C4 B5:D5"));
            });
            it("should accept empty array", () => {
                const ra1 = new RangeArray(), ra2 = ra1.merge();
                expect(ra2).toBeArrayOfSize(0);
                expect(ra2).not.toBe(ra1);
            });
        });

        describe("method partition", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("partition");
            });
            it("should not partition distinct ranges", () => {
                expect(ra("C6:D7 B3:C4 E4:F5").partition()).toSatisfy(rangesMatcher("B3:C4 E4:F5 C6:D7"));
            });
            it("should not partition adjacent ranges", () => {
                expect(ra("B5:C6 B3:C4").partition()).toSatisfy(rangesMatcher("B3:C4 B5:C6"));
                expect(ra("C5:D6 B3:C4").partition()).toSatisfy(rangesMatcher("B3:C4 C5:D6"));
                expect(ra("D3:E4 B3:C4").partition()).toSatisfy(rangesMatcher("B3:C4 D3:E4"));
                expect(ra("D4:E5 B3:C4").partition()).toSatisfy(rangesMatcher("B3:C4 D4:E5"));
                expect(ra("D3:E4 B5:C6 B3:C4").partition()).toSatisfy(rangesMatcher("B3:C4 D3:E4 B5:C6"));
                expect(ra("D3:E4 B5:C6 B3:C4 D5:E6").partition()).toSatisfy(rangesMatcher("B3:C4 D3:E4 B5:C6 D5:E6"));
            });
            it("should partition overlapping ranges", () => {
                expect(ra("B3:C5 B4:C6").partition()).toSatisfy(rangesMatcher("B3:C3 B4:C5 B6:C6"));
                expect(ra("B4:C6 B3:C5").partition()).toSatisfy(rangesMatcher("B3:C3 B4:C5 B6:C6"));
                expect(ra("B3:D4 C3:E4").partition()).toSatisfy(rangesMatcher("B3:B4 C3:D4 E3:E4"));
                expect(ra("C3:E4 B3:D4").partition()).toSatisfy(rangesMatcher("B3:B4 C3:D4 E3:E4"));
                expect(ra("C3:E5 E5:G7").partition()).toSatisfy(rangesMatcher("C3:E4 C5:D5 E5:E5 F5:G5 E6:G7"));
                expect(ra("E5:G7 C3:E5").partition()).toSatisfy(rangesMatcher("C3:E4 C5:D5 E5:E5 F5:G5 E6:G7"));
                expect(ra("C3:E5 D4:D4").partition()).toSatisfy(rangesMatcher("C3:E3 C4:C4 D4:D4 E4:E4 C5:E5"));
                expect(ra("D4:D4 C3:E5").partition()).toSatisfy(rangesMatcher("C3:E3 C4:C4 D4:D4 E4:E4 C5:E5"));
                expect(ra("B3:C3 B4:C4 B3:B4 C3:C4").partition()).toSatisfy(rangesMatcher("B3:B3 C3:C3 B4:B4 C4:C4"));
                expect(ra("B3:E3 B6:E6 B3:B6 E3:E6").partition()).toSatisfy(rangesMatcher("B3:B3 C3:D3 E3:E3 B4:B5 E4:E5 B6:B6 C6:D6 E6:E6"));
            });
            it("should contain the original ranges", () => {
                const ranges = ra("C3:E5 E5:G7");
                for (const partRange of ranges.partition()) {
                    expect(partRange.coveredBy).toBeInstanceOf(RangeArray);
                    ranges.forEach(function (range) {
                        const index = partRange.coveredBy.indexOf(range);
                        if (range.contains(partRange)) {
                            expect(index).toBeGreaterThanOrEqual(0);
                        } else {
                            expect(index).toBeLessThan(0);
                        }
                    });
                }
            });
            it("should clone all ranges", () => {
                const r1 = r("C3:E5"), ranges = new RangeArray(r1).partition();
                expect(ranges).toSatisfy(rangesMatcher("C3:E5"));
                expect(ranges[0]).not.toBe(r1);
            });
            it("should accept empty array", () => {
                const ra1 = new RangeArray(), ra2 = ra1.partition();
                expect(ra2).toHaveLength(0);
                expect(ra2).not.toBe(ra1);
            });
        });

        describe("method split", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("split");
            });
        });

        describe("method intersect", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("intersect");
            });
            const ra1 = ra("B3:D5 D5:F7");
            it("should return empty array for distinct ranges", () => {
                expect(ra1.intersect(ra("E3:F4 B6:C7"))).toHaveLength(0);
            });
            it("should return intersection ranges", () => {
                expect(ra1.intersect(ra1)).toEqual(ra("B3:D5 D5:D5 D5:D5 D5:F7"));
                expect(ra1.intersect(ra("B3:D5"))).toEqual(ra("B3:D5 D5:D5"));
                expect(ra1.intersect(ra("D5:E6"))).toEqual(ra("D5:D5 D5:E6"));
                expect(ra1.intersect(ra("A1:C9"))).toEqual(ra("B3:C5"));
                expect(ra1.intersect(ra("A5:I6"))).toEqual(ra("B5:D5 D5:F6"));
                expect(ra1.intersect(ra("D1:D9 A5:I5"))).toEqual(ra("D3:D5 B5:D5 D5:D7 D5:F5"));
            });
            it("should accept empty arrays", () => {
                const ra0 = new RangeArray();
                expect(ra0.intersect(ra1)).toHaveLength(0);
                expect(ra1.intersect(ra0)).toHaveLength(0);
                expect(ra0.intersect(ra0)).toHaveLength(0);
            });
        });

        describe("method difference", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("difference");
            });
            const ra1 = ra("B3:D5 D5:F7");
            it("should return remaining ranges", () => {
                expect(ra1.difference(ra("E3:F4 B6:C7"))).toEqual(ra1);
                expect(ra1.difference(ra("E3:F4 B6:C7"))).not.toBe(ra1);
                expect(ra1.difference(ra("D3:F5 B5:D7"))).toEqual(ra("B3:C4 E6:F7"));
                expect(ra1.difference(ra("C4:E6"))).toEqual(ra("B3:D3 B4:B5 F5:F6 D7:F7"));
                expect(ra1.difference(ra("C4:C4 E6:E6"))).toEqual(ra("B3:D3 B4:B4 D4:D4 B5:D5 D5:F5 D6:D6 F6:F6 D7:F7"));
                expect(ra1.difference(ra("C4:C4 E6:E6 D5:D5"))).toEqual(ra("B3:D3 B4:B4 D4:D4 B5:C5 E5:F5 D6:D6 F6:F6 D7:F7"));
                expect(ra1.difference(ra("B3:F7"))).toHaveLength(0);
            });
            it("should accept empty array", () => {
                const ra0 = new RangeArray();
                expect(ra1.difference(ra0)).toEqual(ra1);
                expect(ra1.difference(ra0)).not.toBe(ra1);
                expect(ra0.difference(ra1)).toHaveLength(0);
                expect(ra0.difference(ra0)).toHaveLength(0);
            });
            it("should clone all ranges", () => {
                const r1 = r("C3:E5"), ranges = new RangeArray(r1).difference(new RangeArray());
                expect(ranges[0]).toEqual(r1);
                expect(ranges[0]).not.toBe(r1);
            });
            it("should accept single range object instead of array", () => {
                expect(ra("B3:D5 D5:F7").difference(r("C4:E6"))).toEqual(ra("B3:D3 B4:B5 F5:F6 D7:F7"));
            });
        });

        describe("method addresses", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("addresses");
            });
            it("should visit the addresses in a range array", () => {
                const ra1 = ra("B3:C4 C4:D5"), it = ra1.addresses();
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([a("B3"), a("C3"), a("B4"), a("C4"), a("C4"), a("D4"), a("C5"), a("D5")]);
            });
            it("should visit the addresses in a range array in vertical reversed order", () => {
                const ra1 = ra("B3:C4 C4:D5"), it = ra1.addresses({ reverse: true, columns: true });
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([a("D5"), a("D4"), a("C5"), a("C4"), a("C4"), a("C3"), a("B4"), a("B3")]);
            });
        });

        describe("method toOpStr", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("toOpStr");
            });
            const ra1 = ra("A2:C4 B3:B3");
            it("should stringify the ranges", () => {
                expect(ra1.toOpStr()).toBe("A2:C4 B3");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(RangeArray).toHaveMethod("toString");
            });
            const ra1 = ra("A2:C4 B3:D5");
            it("should stringify the ranges", () => {
                expect(ra1.toString()).toBe("A2:C4,B3:D5");
            });
            it("should use the separator string", () => {
                expect(ra1.toString(" + ")).toBe("A2:C4 + B3:D5");
            });
            it("should stringify implicitly", () => {
                expect("<" + ra1 + ">").toBe("<A2:C4,B3:D5>");
            });
        });
    });
});
