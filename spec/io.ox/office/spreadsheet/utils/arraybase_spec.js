/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as arraybase from "@/io.ox/office/spreadsheet/utils/arraybase";

// constants ==================================================================

const ctx = {};

// tests ======================================================================

describe("module spreadsheet/utils/arraybase", () => {

    // functions --------------------------------------------------------------

    describe("function yieldArraySource", () => {
        const { yieldArraySource } = arraybase;
        it("should exist", () => {
            expect(yieldArraySource).toBeFunction();
        });
        it("should visit array elements", () => {
            const a1 = [1, 2, 3];
            const iter = yieldArraySource(a1);
            expect(iter).toBeIterator();
            expect(Array.from(iter)).toEqual(a1);
        });
        it("should visit single values", () => {
            const iter = yieldArraySource(1);
            expect(iter).toBeIterator();
            expect(Array.from(iter)).toEqual([1]);
        });
        it("should accept empty array sources", () => {
            const iter = yieldArraySource(null);
            expect(iter).toBeIterator();
            expect(Array.from(iter)).toEqual([]);
        });
    });

    describe("function consumeArraySource", () => {
        const { consumeArraySource } = arraybase;
        it("should exist", () => {
            expect(consumeArraySource).toBeFunction();
        });
        it("should visit array elements", () => {
            const a1 = [1, 2, 3], spy = jest.fn();
            consumeArraySource(a1, spy, ctx);
            expect(spy).toHaveBeenCalledTimes(3);
            expect(spy).toHaveBeenAlwaysCalledOn(ctx);
            expect(spy).toHaveBeenNthCalledWith(1, 1, 0);
            expect(spy).toHaveBeenNthCalledWith(2, 2, 1);
            expect(spy).toHaveBeenNthCalledWith(3, 3, 2);
        });
        it("should visit single values", () => {
            const spy = jest.fn();
            consumeArraySource(1, spy, ctx);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenAlwaysCalledOn(ctx);
            expect(spy).toHaveBeenCalledWith(1, 0);
        });
        it("should accept empty array sources", () => {
            const spy = jest.fn();
            consumeArraySource([], spy, ctx);
            consumeArraySource(null, spy, ctx);
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe("function someInArraySource", () => {
        const { someInArraySource } = arraybase;
        it("should exist", () => {
            expect(someInArraySource).toBeFunction();
        });
        it("should visit array elements", () => {
            const a1 = [1, 2, 3];
            const spy1 = jest.fn().mockReturnValue(false);
            expect(someInArraySource(a1, spy1, ctx)).toBeFalse();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenAlwaysCalledOn(ctx);
            expect(spy1).toHaveBeenNthCalledWith(1, 1, 0);
            expect(spy1).toHaveBeenNthCalledWith(2, 2, 1);
            expect(spy1).toHaveBeenNthCalledWith(3, 3, 2);
            const spy2 = jest.fn().mockReturnValueOnce(false).mockReturnValueOnce(true).mockReturnValue(false);
            expect(someInArraySource(a1, spy2, ctx)).toBeTrue();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenAlwaysCalledOn(ctx);
            expect(spy2).toHaveBeenNthCalledWith(1, 1, 0);
            expect(spy2).toHaveBeenNthCalledWith(2, 2, 1);
        });
        it("should visit single values", () => {
            const spy1 = jest.fn().mockReturnValue(false);
            expect(someInArraySource(1, spy1, ctx)).toBeFalse();
            expect(spy1).toHaveBeenCalledTimes(1);
            expect(spy1).toHaveBeenAlwaysCalledOn(ctx);
            expect(spy1).toHaveBeenCalledWith(1, 0);
            const spy2 = jest.fn().mockReturnValue(true);
            expect(someInArraySource(1, spy2, ctx)).toBeTrue();
            expect(spy2).toHaveBeenCalledTimes(1);
            expect(spy2).toHaveBeenAlwaysCalledOn(ctx);
            expect(spy2).toHaveBeenCalledWith(1, 0);
        });
        it("should accept empty array sources", () => {
            const spy = jest.fn();
            expect(someInArraySource([], spy, ctx)).toBeFalse();
            expect(someInArraySource(null, spy, ctx)).toBeFalse();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe("function everyInArraySource", () => {
        const { everyInArraySource } = arraybase;
        it("should exist", () => {
            expect(everyInArraySource).toBeFunction();
        });
        it("should visit array elements", () => {
            const a1 = [1, 2, 3];
            const spy1 = jest.fn().mockReturnValue(true);
            expect(everyInArraySource(a1, spy1, ctx)).toBeTrue();
            expect(spy1).toHaveBeenCalledTimes(3);
            expect(spy1).toHaveBeenAlwaysCalledOn(ctx);
            expect(spy1).toHaveBeenNthCalledWith(1, 1, 0);
            expect(spy1).toHaveBeenNthCalledWith(2, 2, 1);
            expect(spy1).toHaveBeenNthCalledWith(3, 3, 2);
            const spy2 = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(false).mockReturnValue(true);
            expect(everyInArraySource(a1, spy2, ctx)).toBeFalse();
            expect(spy2).toHaveBeenCalledTimes(2);
            expect(spy2).toHaveBeenAlwaysCalledOn(ctx);
            expect(spy2).toHaveBeenNthCalledWith(1, 1, 0);
            expect(spy2).toHaveBeenNthCalledWith(2, 2, 1);
        });
        it("should visit single values", () => {
            const spy1 = jest.fn().mockReturnValue(true);
            expect(everyInArraySource(1, spy1, ctx)).toBeTrue();
            expect(spy1).toHaveBeenCalledTimes(1);
            expect(spy1).toHaveBeenAlwaysCalledOn(ctx);
            expect(spy1).toHaveBeenCalledWith(1, 0);
            const spy2 = jest.fn().mockReturnValue(false);
            expect(everyInArraySource(1, spy2, ctx)).toBeFalse();
            expect(spy2).toHaveBeenCalledTimes(1);
            expect(spy2).toHaveBeenAlwaysCalledOn(ctx);
            expect(spy2).toHaveBeenCalledWith(1, 0);
        });
        it("should accept empty array sources", () => {
            const spy = jest.fn();
            expect(everyInArraySource([], spy, ctx)).toBeTrue();
            expect(everyInArraySource(null, spy, ctx)).toBeTrue();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    // class ArrayBase --------------------------------------------------------

    describe("class ArrayBase", () => {
        const { ArrayBase } = arraybase;

        it("should subclass Array", () => {
            expect(ArrayBase).toBeSubClassOf(Array);
        });
    });
});
