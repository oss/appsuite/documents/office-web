/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as labels from "@/io.ox/office/spreadsheet/view/labels";

import { a } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/view/labels", () => {

    // public functions -------------------------------------------------------

    describe("function getColLabel", () => {
        const { getColLabel } = labels;
        it("should exist", () => {
            expect(getColLabel).toBeFunction();
        });
        it("should return the column label", () => {
            expect(getColLabel(0)).toBe("A");
            expect(getColLabel(25)).toBe("Z");
            expect(getColLabel(26)).toBe("AA");
        });
        it("should return the column label with prefix", () => {
            expect(getColLabel(0, { prefix: true })).toBe("Column A");
            expect(getColLabel(25, { prefix: true })).toBe("Column Z");
            expect(getColLabel(26, { prefix: true })).toBe("Column AA");
        });
        it("should return the column label with ZWSP", () => {
            expect(getColLabel(0, { a11y: true })).toBe("A");
            expect(getColLabel(25, { a11y: true })).toBe("Z");
            expect(getColLabel(26, { a11y: true })).toBe("A\u200bA");
        });
        it("should return the column label with prefix and ZWSP", () => {
            expect(getColLabel(0, { prefix: true, a11y: true })).toBe("Column A");
            expect(getColLabel(25, { prefix: true, a11y: true })).toBe("Column Z");
            expect(getColLabel(26, { prefix: true, a11y: true })).toBe("Column A\u200bA");
        });
    });

    describe("function getRowLabel", () => {
        const { getRowLabel } = labels;
        it("should exist", () => {
            expect(getRowLabel).toBeFunction();
        });
        it("should return the row label", () => {
            expect(getRowLabel(0)).toBe("1");
            expect(getRowLabel(8)).toBe("9");
            expect(getRowLabel(9)).toBe("10");
        });
        it("should return the row label with prefix", () => {
            expect(getRowLabel(0, { prefix: true })).toBe("Row 1");
            expect(getRowLabel(8, { prefix: true })).toBe("Row 9");
            expect(getRowLabel(9, { prefix: true })).toBe("Row 10");
        });
        it("should return the row label without ZWSP", () => {
            expect(getRowLabel(0, { a11y: true })).toBe("1");
            expect(getRowLabel(8, { a11y: true })).toBe("9");
            expect(getRowLabel(9, { a11y: true })).toBe("10");
        });
        it("should return the row label with prefix and without ZWSP", () => {
            expect(getRowLabel(0, { prefix: true, a11y: true })).toBe("Row 1");
            expect(getRowLabel(8, { prefix: true, a11y: true })).toBe("Row 9");
            expect(getRowLabel(9, { prefix: true, a11y: true })).toBe("Row 10");
        });
    });

    describe("function getColRowLabel", () => {
        const { getColRowLabel } = labels;
        it("should exist", () => {
            expect(getColRowLabel).toBeFunction();
        });
        it("should return the column label", () => {
            expect(getColRowLabel(0, true)).toBe("A");
            expect(getColRowLabel(25, true)).toBe("Z");
            expect(getColRowLabel(26, true)).toBe("AA");
        });
        it("should return the row label", () => {
            expect(getColRowLabel(0, false)).toBe("1");
            expect(getColRowLabel(8, false)).toBe("9");
            expect(getColRowLabel(9, false)).toBe("10");
        });
        it("should return the column label with prefix", () => {
            expect(getColRowLabel(0, true, { prefix: true })).toBe("Column A");
            expect(getColRowLabel(25, true, { prefix: true })).toBe("Column Z");
            expect(getColRowLabel(26, true, { prefix: true })).toBe("Column AA");
        });
        it("should return the row label with prefix", () => {
            expect(getColRowLabel(0, false, { prefix: true })).toBe("Row 1");
            expect(getColRowLabel(8, false, { prefix: true })).toBe("Row 9");
            expect(getColRowLabel(9, false, { prefix: true })).toBe("Row 10");
        });
        it("should return the column label with ZWSP", () => {
            expect(getColRowLabel(0, true, { a11y: true })).toBe("A");
            expect(getColRowLabel(25, true, { a11y: true })).toBe("Z");
            expect(getColRowLabel(26, true, { a11y: true })).toBe("A\u200bA");
        });
        it("should return the row label without ZWSP", () => {
            expect(getColRowLabel(0, false, { a11y: true })).toBe("1");
            expect(getColRowLabel(8, false, { a11y: true })).toBe("9");
            expect(getColRowLabel(9, false, { a11y: true })).toBe("10");
        });
        it("should return the column label with prefix and ZWSP", () => {
            expect(getColRowLabel(0, true, { prefix: true, a11y: true })).toBe("Column A");
            expect(getColRowLabel(25, true, { prefix: true, a11y: true })).toBe("Column Z");
            expect(getColRowLabel(26, true, { prefix: true, a11y: true })).toBe("Column A\u200bA");
        });
        it("should return the row label with prefix and without ZWSP", () => {
            expect(getColRowLabel(0, false, { prefix: true, a11y: true })).toBe("Row 1");
            expect(getColRowLabel(8, false, { prefix: true, a11y: true })).toBe("Row 9");
            expect(getColRowLabel(9, false, { prefix: true, a11y: true })).toBe("Row 10");
        });
    });

    describe("function getCellLabel", () => {
        const { getCellLabel } = labels;
        it("should exist", () => {
            expect(getCellLabel).toBeFunction();
        });
        it("should return the cell name", () => {
            expect(getCellLabel(a("A1"))).toBe("A1");
            expect(getCellLabel(a("Z9"))).toBe("Z9");
            expect(getCellLabel(a("AA10"))).toBe("AA10");
        });
        it("should return the cell name with prefix", () => {
            expect(getCellLabel(a("A1"), { prefix: true })).toBe("Cell A1");
            expect(getCellLabel(a("Z9"), { prefix: true })).toBe("Cell Z9");
            expect(getCellLabel(a("AA10"), { prefix: true })).toBe("Cell AA10");
        });
        it("should return the cell name with ZWSP", () => {
            expect(getCellLabel(a("A1"), { a11y: true })).toBe("A\u200b1");
            expect(getCellLabel(a("Z9"), { a11y: true })).toBe("Z\u200b9");
            expect(getCellLabel(a("AA10"), { a11y: true })).toBe("A\u200bA\u200b10");
        });
        it("should return the cell name with prefix and ZWSP", () => {
            expect(getCellLabel(a("A1"), { prefix: true, a11y: true })).toBe("Cell A\u200b1");
            expect(getCellLabel(a("Z9"), { prefix: true, a11y: true })).toBe("Cell Z\u200b9");
            expect(getCellLabel(a("AA10"), { prefix: true, a11y: true })).toBe("Cell A\u200bA\u200b10");
        });
    });

    describe("function getSeriesLabel", () => {
        const { getSeriesLabel } = labels;
        it("should exist", () => {
            expect(getSeriesLabel).toBeFunction();
        });
        it("should return the series label", () => {
            expect(getSeriesLabel(0)).toBe("Series 1");
            expect(getSeriesLabel(9)).toBe("Series 10");
        });
    });

    describe("function getCategoryLabel", () => {
        const { getCategoryLabel } = labels;
        it("should exist", () => {
            expect(getCategoryLabel).toBeFunction();
        });
        it("should return the category label", () => {
            expect(getCategoryLabel("math")).toBe("Mathematical");
            expect(getCategoryLabel("text")).toBe("Text");
        });
    });
});
