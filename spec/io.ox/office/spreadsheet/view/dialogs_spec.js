/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as editdialogs from "@/io.ox/office/editframework/view/editdialogs";
import * as sheetnamedialog from "@/io.ox/office/spreadsheet/view/dialog/sheetnamedialog";
import * as movesheetsdialog from "@/io.ox/office/spreadsheet/view/dialog/movesheetsdialog";
import * as definednamedialog from "@/io.ox/office/spreadsheet/view/dialog/definednamedialog";
import * as functiondialog from "@/io.ox/office/spreadsheet/view/dialog/functiondialog";
import * as unhidesheetsdialog from "@/io.ox/office/spreadsheet/view/dialog/unhidesheetsdialog";
import * as customsortdialog from "@/io.ox/office/spreadsheet/view/dialog/customsortdialog";

import * as dialogs from "@/io.ox/office/spreadsheet/view/dialogs";

// tests ======================================================================

describe("module spreadsheet/view/dialogs", () => {

    // re-exports -------------------------------------------------------------

    it("re-exports should exist", () => {
        expect(dialogs).toContainProps(editdialogs);
        expect(dialogs).toContainProps(sheetnamedialog);
        expect(dialogs).toContainProps(movesheetsdialog);
        expect(dialogs).toContainProps(definednamedialog);
        expect(dialogs).toContainProps(functiondialog);
        expect(dialogs).toContainProps(unhidesheetsdialog);
        expect(dialogs).toContainProps(customsortdialog);
    });
});
