/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { getDataForBubbleSeriesOperations } from "@/io.ox/office/spreadsheet/view/chartcreator";

import { r, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/view/chartcreator", () => {

    // the operations to be applied by the document model
    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertSheet(0, "Sheet1"),
        op.changeCells(0, "A1", [
            [null, "X1", "X2", "X3", "X4", "X5", "X6", "X7", "X8"],
            ["Y1", 10.1, 20.1, 30.1, 40.1, 50.1, 60.1, 70.1, 80.1],
            ["Y2", 10.2, 20.2, 30.2, 40.2, 50.2, 60.2, 70.2, 80.2],
            ["Y3", 10.3, 20.3, 30.3, 40.3, 50.3, 60.3, 70.3, 80.3],
            ["Y4", 10.4, 20.4, 30.4, 40.4, 50.4, 60.4, 70.4, 80.4],
            ["Y5", 10.5, 20.5, 30.5, 40.5, 50.5, 60.5, 70.5, 80.5],
            ["Y6", 10.6, 20.6, 30.6, 40.6, 50.6, 60.6, 70.6, 80.6],
            ["Y7", 10.7, 20.7, 30.7, 40.7, 50.7, 60.7, 70.7, 80.7],
            ["Y8", 10.8, 20.8, 30.8, 40.8, 50.8, 60.8, 70.8, 80.8]
        ])
    ];

    // initialize test document
    let docModel = null;
    createSpreadsheetApp("ooxml", OPERATIONS).done(function (docApp) {
        docModel = docApp.docModel;
    });

    function checkBubbleArray(series, index, bubbleCount) {
        if (bubbleCount) {
            expect(series[index].bubbleArray).toHaveLength(bubbleCount);
        } else {
            expect(series[index].bubbleArray).toBeNull();
        }

    }

    function checkTitle(series, index, rangeString) {
        if (rangeString) {
            expect(Range.compare(r(rangeString), new Range(series[index].title, series[index].title))).toBe(0);
        } else {
            expect(series[index].title).toBeNull();
        }
    }

    function checkNames(series, index, rangeString) {
        if (rangeString) {
            expect(Range.compare(r(rangeString), new Range(series[index].namesFrom, series[index].namesTo))).toBe(0);
        } else {
            expect(series[index].namesFrom).toBe();
            expect(series[index].namesTo).toBe();
        }
    }

    function checkValues(series, index, rangeString) {
        if (rangeString) {
            expect(Range.compare(r(rangeString), new Range(series[index].valueFrom, series[index].valueTo))).toBe(0);
        } else {
            expect(series[index].valueFrom).toBe();
            expect(series[index].valueTo).toBe();
        }
    }

    function checkBubbles(series, index, rangeString) {
        if (rangeString) {
            expect(Range.compare(r(rangeString), new Range(series[index].bubbleFrom, series[index].bubbleTo))).toBe(0);
        } else {
            expect(series[index].bubbleFrom).toBeNull();
            expect(series[index].bubbleTo).toBeNull();
        }
    }

    function checkSeriesLength(series, seriesLength) {
        expect(series).toHaveLength(seriesLength);
    }

    let series;

    describe("create bubblecharts without title cells:", () => {

        describe("1 column", () => {

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:B3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:B3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B3");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:B4"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B3:B3");
                checkNames(series, 0, "B2:B2");
                checkBubbles(series, 0, "B4:B4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:B4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B4");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
                checkTitle(series, 0);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:B5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:B4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:B5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:B5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B5");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
                checkTitle(series, 0);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:B6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:B3");
                checkNames(series, 0, "B2:B2");
                checkBubbles(series, 0, "B4:B4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B5:B5");
                checkNames(series, 1, "B2:B2");
                checkBubbles(series, 1, "B6:B6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:B6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B6");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
                checkTitle(series, 0);
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:B7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:B4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:B5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:B6");
                checkNames(series, 2);
                checkBubbles(series, 2, "B7:B7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:B7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B7");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
                checkTitle(series, 0);
            });
        });

        describe("2 column", () => {

            it("1 row", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C2"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B2");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:C2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
                checkTitle(series, 0);
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:C2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B3");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C4"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B3:C3");
                checkNames(series, 0, "B2:C2");
                checkBubbles(series, 0, "B4:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B4");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C5"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B5");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

            });
            //{start:[0,0],series:0,attrs:{series:{values:"Sheet1!$B$2:$C$2",type:"bubble",dataLabel:"",bubbles:"Sheet1!$B$3:$C$3",axisXIndex:0,axisYIndex:1},fill:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}},line:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}}}}
            //{start:[0,0],series:1,attrs:{series:{values:"Sheet1!$B$4:$C$4",type:"bubble",dataLabel:"",bubbles:"Sheet1!$B$5:$C$5",axisXIndex:0,axisYIndex:1},fill:{type:"solid",color:{type:"scheme",value:"accent2",transformations:[{type:"alpha",value:75000}],fallbackValue:"ED7D31"}},line:{type:"solid",color:{type:"scheme",value:"accent2",transformations:[{type:"alpha",value:75000}],fallbackValue:"ED7D31"}}}}

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:C2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:C4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:C5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:C3");
                checkNames(series, 0, "B2:C2");
                checkBubbles(series, 0, "B4:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B5:C5");
                checkNames(series, 1, "B2:C2");
                checkBubbles(series, 1, "B6:C6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });
            //{start:[0,0],series:0,attrs:{series:{values:"Sheet1!$B$2:$B$6",type:"bubble",dataLabel:"",bubbles:"Sheet1!$C$2:$C$6",axisXIndex:0,axisYIndex:1,dataPoints:[{fill:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}},line:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}}},{fill:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}},line:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}}},{fill:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}},line:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}}},{fill:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}},line:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}}},{fill:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}},line:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}}}]},fill:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}},line:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}}}}
            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B6");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });
            //{start:[0,0],series:0,attrs:{series:{values:"Sheet1!$B$2:$C$2",type:"bubble",dataLabel:"",bubbles:"Sheet1!$B$3:$C$3",axisXIndex:0,axisYIndex:1},fill:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}},line:{type:"solid",color:{type:"scheme",value:"accent1",transformations:[{type:"alpha",value:75000}],fallbackValue:"5B9BD5"}}}}
            //{start:[0,0],series:1,attrs:{series:{values:"Sheet1!$B$4:$C$4",type:"bubble",dataLabel:"",bubbles:"Sheet1!$B$5:$C$5",axisXIndex:0,axisYIndex:1},fill:{type:"solid",color:{type:"scheme",value:"accent2",transformations:[{type:"alpha",value:75000}],fallbackValue:"ED7D31"}},line:{type:"solid",color:{type:"scheme",value:"accent2",transformations:[{type:"alpha",value:75000}],fallbackValue:"ED7D31"}}}}
            //{start:[0,0],series:2,attrs:{series:{values:"Sheet1!$B$6:$C$6",type:"bubble",dataLabel:"",bubbles:"Sheet1!$B$7:$C$7",axisXIndex:0,axisYIndex:1},fill:{type:"solid",color:{type:"scheme",value:"accent3",transformations:[{type:"alpha",value:75000}],fallbackValue:"A5A5A5"}},line:{type:"solid",color:{type:"scheme",value:"accent3",transformations:[{type:"alpha",value:75000}],fallbackValue:"A5A5A5"}}}}

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C7"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B7");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:C7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:C2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:C4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:C5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:C6");
                checkNames(series, 2);
                checkBubbles(series, 2, "B7:C7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });
        });

        describe("3 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D2"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "C2:C2");
                checkNames(series, 0, "B2:B2");
                checkBubbles(series, 0, "D2:D2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:D2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
                checkTitle(series, 0);
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "C2:C3");
                checkNames(series, 0, "B2:B3");
                checkBubbles(series, 0, "D2:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:D2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D4"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B3:D3");
                checkNames(series, 0, "B2:D2");
                checkBubbles(series, 0, "B4:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "C2:C4");
                checkNames(series, 0, "B2:B4");
                checkBubbles(series, 0, "D2:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D5"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C2:C5");
                checkNames(series, 0, "B2:B5");
                checkBubbles(series, 0, "D2:D5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:D2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:D4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D6"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C2:C6");
                checkNames(series, 0, "B2:B6");
                checkBubbles(series, 0, "D2:D6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:D3");
                checkNames(series, 0, "B2:D2");
                checkBubbles(series, 0, "B4:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B5:D5");
                checkNames(series, 1, "B2:D2");
                checkBubbles(series, 1, "B6:D6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D7"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C2:C7");
                checkNames(series, 0, "B2:B7");
                checkBubbles(series, 0, "D2:D7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:D7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:D2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:D4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:D6");
                checkNames(series, 2);
                checkBubbles(series, 2, "B7:D7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });
        });

        describe("4 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E2"), 0);
                checkSeriesLength(series, 2);
                checkValues(series, 0, "B2:B2");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D2");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E2");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:E2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
                checkTitle(series, 0);
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:E2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:E3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B3");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D3");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E4"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B3:E3");
                checkNames(series, 0, "B2:E2");
                checkBubbles(series, 0, "B4:E4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B4");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D4");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:E2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:E3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:E4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:E5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B5");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D5");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:E3");
                checkNames(series, 0, "B2:E2");
                checkBubbles(series, 0, "B4:E4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B5:E5");
                checkNames(series, 1, "B2:E2");
                checkBubbles(series, 1, "B6:E6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B6");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D6");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E7"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B7");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D7");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:E7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:E2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:E3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:E4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:E5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:E6");
                checkNames(series, 2);
                checkBubbles(series, 2, "B7:E7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });
        });

        describe("5 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F2"), 0);
                checkSeriesLength(series, 2);
                checkValues(series, 0, "C2:C2");
                checkNames(series, 0, "B2:B2");
                checkBubbles(series, 0, "D2:D2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E2:E2");
                checkNames(series, 1, "B2:B2");
                checkBubbles(series, 1, "F2:F2");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:F2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
                checkTitle(series, 0);
            });
            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F3"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C3");
                checkNames(series, 0, "B2:B3");
                checkBubbles(series, 0, "D2:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E2:E3");
                checkNames(series, 1, "B2:B3");
                checkBubbles(series, 1, "F2:F3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:F2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });
            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F4"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B3:F3");
                checkNames(series, 0, "B2:F2");
                checkBubbles(series, 0, "B4:F4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C4");
                checkNames(series, 0, "B2:B4");
                checkBubbles(series, 0, "D2:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E2:E4");
                checkNames(series, 1, "B2:B4");
                checkBubbles(series, 1, "F2:F4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C5");
                checkNames(series, 0, "B2:B5");
                checkBubbles(series, 0, "D2:D5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E2:E5");
                checkNames(series, 1, "B2:B5");
                checkBubbles(series, 1, "F2:F5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:F2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:F4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:F5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:F3");
                checkNames(series, 0, "B2:F2");
                checkBubbles(series, 0, "B4:F4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B5:F5");
                checkNames(series, 1, "B2:F2");
                checkBubbles(series, 1, "B6:F6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C6");
                checkNames(series, 0, "B2:B6");
                checkBubbles(series, 0, "D2:D6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E2:E6");
                checkNames(series, 1, "B2:B6");
                checkBubbles(series, 1, "F2:F6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });
            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F7"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C7");
                checkNames(series, 0, "B2:B7");
                checkBubbles(series, 0, "D2:D7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E2:E7");
                checkNames(series, 1, "B2:B7");
                checkBubbles(series, 1, "F2:F7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:F7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:F2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:F4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:F5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:F6");
                checkNames(series, 2);
                checkBubbles(series, 2, "B7:F7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });
        });

        describe("6 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G2"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B2");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D2");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E2");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F2:F2");
                checkNames(series, 2);
                checkBubbles(series, 2, "G2:G2");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:G2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
                checkTitle(series, 0);
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G3"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:G2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:G3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B3");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D3");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F2:F3");
                checkNames(series, 2);
                checkBubbles(series, 2, "G2:G3");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G4"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B3:G3");
                checkNames(series, 0, "B2:G2");
                checkBubbles(series, 0, "B4:G4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B4");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D4");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F2:F4");
                checkNames(series, 2);
                checkBubbles(series, 2, "G2:G4");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:G2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:G3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:G4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:G5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B5");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D5");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F2:F5");
                checkNames(series, 2);
                checkBubbles(series, 2, "G2:G5");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:G3");
                checkNames(series, 0, "B2:G2");
                checkBubbles(series, 0, "B4:G4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B5:G5");
                checkNames(series, 1, "B2:G2");
                checkBubbles(series, 1, "B6:G6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B6");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D6");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F2:F6");
                checkNames(series, 2);
                checkBubbles(series, 2, "G2:G6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:G2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:G3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:G4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:G5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:G6");
                checkNames(series, 2);
                checkBubbles(series, 2, "B7:G7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B2:G7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B7");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D7");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F2:F7");
                checkNames(series, 2);
                checkBubbles(series, 2, "G2:G7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });
        });
    });

    // ---- TOP TITLE ------
    describe("create bubblecharts with top title cells:", () => {

        describe("1 column", () => {
            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B2"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 1);
                checkTitle(series, 0, "B1:B1");
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B2");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 1);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B3"), 0);
                checkSeriesLength(series, 2);
                checkValues(series, 0, "B1:B1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:B3");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
                checkTitle(series, 1);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B3");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
                checkTitle(series, 0, "B1:B1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B2");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:B4");
                checkNames(series, 1, "B1:B1");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
                checkTitle(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B4");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
                checkTitle(series, 0, "B1:B1");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B5"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:B1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:B3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:B4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:B5");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);
                checkTitle(series, 2);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B5");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
                checkTitle(series, 0, "B1:B1");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B6"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B2");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:B4");
                checkNames(series, 1, "B1:B1");
                checkBubbles(series, 1, "B5:B5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:B6");
                checkNames(series, 2, "B1:B1");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);
                checkTitle(series, 2);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B6");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
                checkTitle(series, 0, "B1:B1");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B7"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:B1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:B3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:B4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:B5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:B6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B7:B7");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);
                checkTitle(series, 3);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B7");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
                checkTitle(series, 0, "B1:B1");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B8"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B2");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:B4");
                checkNames(series, 1, "B1:B1");
                checkBubbles(series, 1, "B5:B5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:B6");
                checkNames(series, 2, "B1:B1");
                checkBubbles(series, 2, "B7:B7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B8:B8");
                checkNames(series, 3, "B1:B1");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);
                checkTitle(series, 3);

            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:B8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B8");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 7);
                checkTitle(series, 0, "B1:B1");
            });

        });

        describe("2 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C1"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "C1:C1");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 1);
                checkTitle(series, 0, "B1:B1");
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C1"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "C1:C1");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 1);
                checkTitle(series, 0);
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C2"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B2");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:C2");
                checkNames(series, 0, "B1:C1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C3"), 0);
                checkSeriesLength(series, 2);
                checkValues(series, 0, "B1:C1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:C3");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 2);
                checkTitle(series, 1);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "C2:C3");
                checkNames(series, 0, "B2:B3");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
                checkTitle(series, 0, "C1:C1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C4"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C2:C4");
                checkNames(series, 0, "B2:B4");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
                checkTitle(series, 0, "C1:C1");
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:C1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:C4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C5"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C2:C5");
                checkNames(series, 0, "B2:B5");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
                checkTitle(series, 0, "C1:C1");
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:C1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:C4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:C5");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 2);
                checkTitle(series, 2);
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C6"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C2:C6");
                checkNames(series, 0, "B2:B6");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
                checkTitle(series, 0, "C1:C1");
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:C1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:C4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:C5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:C6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C7"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C2:C7");
                checkNames(series, 0, "B2:B7");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
                checkTitle(series, 0, "C1:C1");
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:C1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:C4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:C5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:C6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B7:C7");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 2);
                checkTitle(series, 3);
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C8"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C2:C8");
                checkNames(series, 0, "B2:B8");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 7);
                checkTitle(series, 0, "C1:C1");
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:C8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:C1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:C4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:C5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:C6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B7:C7");
                checkNames(series, 3);
                checkBubbles(series, 3, "B8:C8");
                checkBubbleArray(series, 3);
                checkTitle(series, 3);
            });
        });

        describe("3 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D1"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:B1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C1:C1");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D1:D1");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
                checkTitle(series, 1);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D1"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C1:D1");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
                checkTitle(series, 0, "B1:B1");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D2"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "C2:C2");
                checkNames(series, 0, "B2:B2");
                checkBubbles(series, 0, "D2:D2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:D2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "C1:C3");
                checkNames(series, 0, "B1:B3");
                checkBubbles(series, 0, "D1:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C1:D1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:D2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "C3:D3");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 2);
                checkTitle(series, 1, "B3:B3");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:D2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:D4");
                checkNames(series, 1, "B1:D1");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 3);
                checkTitle(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C2:C4");
                checkNames(series, 0, "B2:B4");
                checkBubbles(series, 0, "D2:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D5"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C1:C5");
                checkNames(series, 0, "B1:B5");
                checkBubbles(series, 0, "D1:D5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C1:D1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:D2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "C3:D3");
                checkNames(series, 1);
                checkBubbles(series, 1, "C4:D4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "B3:B3");

                checkValues(series, 2, "C5:D5");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 2);
                checkTitle(series, 2, "B5:B5");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D6"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C2:C6");
                checkNames(series, 0, "B2:B6");
                checkBubbles(series, 0, "D2:D6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:D2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:D4");
                checkNames(series, 1, "B1:D1");
                checkBubbles(series, 1, "B5:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:D6");
                checkNames(series, 2, "B1:D1");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 3);
                checkTitle(series, 2);
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D7"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C1:C7");
                checkNames(series, 0, "B1:B7");
                checkBubbles(series, 0, "D1:D7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "C1:D1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:D2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "C3:D3");
                checkNames(series, 1);
                checkBubbles(series, 1, "C4:D4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "B3:B3");

                checkValues(series, 2, "C5:D5");
                checkNames(series, 2);
                checkBubbles(series, 2, "C6:D6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "B5:B5");

                checkValues(series, 3, "C7:D7");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 2);
                checkTitle(series, 3, "B7:B7");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D8"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C2:C8");
                checkNames(series, 0, "B2:B8");
                checkBubbles(series, 0, "D2:D8");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:D8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:D2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:D4");
                checkNames(series, 1, "B1:D1");
                checkBubbles(series, 1, "B5:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:D6");
                checkNames(series, 2, "B1:D1");
                checkBubbles(series, 2, "B7:D7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B8:D8");
                checkNames(series, 3, "B1:D1");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 3);
                checkTitle(series, 3);
            });
        });

        describe("4 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E1"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C1:C1");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0, "D1:D1");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E1:E1");
                checkNames(series, 1, "B1:B1");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
                checkTitle(series, 1);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E1"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C1:E1");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
                checkTitle(series, 0, "B1:B1");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E2"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B2");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D2");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E2");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");
            });

            it("2rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:E2");
                checkNames(series, 0, "B1:E1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E3"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:E1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:E2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:E3");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 4);
                checkTitle(series, 1);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C3");
                checkNames(series, 0, "B2:B3");
                checkBubbles(series, 0, "D2:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E3");
                checkNames(series, 1, "B2:B3");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 2);
                checkTitle(series, 1, "E1:E1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:E1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:E2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:E3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C4");
                checkNames(series, 0, "B2:B4");
                checkBubbles(series, 0, "D2:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E4");
                checkNames(series, 1, "B2:B4");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 3);
                checkTitle(series, 1, "E1:E1");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E5"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:E1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:E2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:E3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:E5");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 4);
                checkTitle(series, 2);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C5");
                checkNames(series, 0, "B2:B5");
                checkBubbles(series, 0, "D2:D5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E5");
                checkNames(series, 1, "B2:B5");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 4);
                checkTitle(series, 1, "E1:E1");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C6");
                checkNames(series, 0, "B2:B6");
                checkBubbles(series, 0, "D2:D6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E6");
                checkNames(series, 1, "B2:B6");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 5);
                checkTitle(series, 1, "E1:E1");
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:E1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:E2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:E3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:E5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:E6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E7"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C7");
                checkNames(series, 0, "B2:B7");
                checkBubbles(series, 0, "D2:D7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E7");
                checkNames(series, 1, "B2:B7");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 6);
                checkTitle(series, 1, "E1:E1");
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:E1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:E2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:E3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:E5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:E6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B7:E7");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 4);
                checkTitle(series, 3);
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E8"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C8");
                checkNames(series, 0, "B2:B8");
                checkBubbles(series, 0, "D2:D8");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E8");
                checkNames(series, 1, "B2:B8");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 7);
                checkTitle(series, 1, "E1:E1");
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:E8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:E1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:E2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:E3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:E5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:E6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B7:E7");
                checkNames(series, 3);
                checkBubbles(series, 3, "B8:E8");
                checkBubbleArray(series, 3);
                checkTitle(series, 3);
            });
        });

        describe("5 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F1"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:B1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C1:C1");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D1:D1");
                checkNames(series, 1);
                checkBubbles(series, 1, "E1:E1");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F1:F1");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);
                checkTitle(series, 2);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F1"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C1:F1");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
                checkTitle(series, 0, "B1:B1");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F2"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C2");
                checkNames(series, 0, "B2:B2");
                checkBubbles(series, 0, "D2:D2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E2");
                checkNames(series, 1, "B2:B2");
                checkBubbles(series, 1, "F2:F2");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:F2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F3"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C1:C3");
                checkNames(series, 0, "B1:B3");
                checkBubbles(series, 0, "D1:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E1:E3");
                checkNames(series, 1, "B1:B3");
                checkBubbles(series, 1, "F1:F3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C1:F1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:F2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "C3:F3");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 4);
                checkTitle(series, 1, "B3:B3");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:F2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:F4");
                checkNames(series, 1, "B1:F1");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 5);
                checkTitle(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C4");
                checkNames(series, 0, "B2:B4");
                checkBubbles(series, 0, "D2:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E4");
                checkNames(series, 1, "B2:B4");
                checkBubbles(series, 1, "F2:F4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C1:C5");
                checkNames(series, 0, "B1:B5");
                checkBubbles(series, 0, "D1:D5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E1:E5");
                checkNames(series, 1, "B1:B5");
                checkBubbles(series, 1, "F1:F5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C1:F1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:F2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "C3:F3");
                checkNames(series, 1);
                checkBubbles(series, 1, "C4:F4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "B3:B3");

                checkValues(series, 2, "C5:F5");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 4);
                checkTitle(series, 2, "B5:B5");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F6"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:F2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:F4");
                checkNames(series, 1, "B1:F1");
                checkBubbles(series, 1, "B5:F5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:F6");
                checkNames(series, 2, "B1:F1");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 5);
                checkTitle(series, 2);

            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C6");
                checkNames(series, 0, "B2:B6");
                checkBubbles(series, 0, "D2:D6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E6");
                checkNames(series, 1, "B2:B6");
                checkBubbles(series, 1, "F2:F6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F7"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C1:C7");
                checkNames(series, 0, "B1:B7");
                checkBubbles(series, 0, "D1:D7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E1:E7");
                checkNames(series, 1, "B1:B7");
                checkBubbles(series, 1, "F1:F7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "C1:F1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:F2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "C3:F3");
                checkNames(series, 1);
                checkBubbles(series, 1, "C4:F4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "B3:B3");

                checkValues(series, 2, "C5:F5");
                checkNames(series, 2);
                checkBubbles(series, 2, "C6:F6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "B5:B5");

                checkValues(series, 3, "C7:F7");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 4);
                checkTitle(series, 3, "B7:B7");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F8"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C2:C8");
                checkNames(series, 0, "B2:B8");
                checkBubbles(series, 0, "D2:D8");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E8");
                checkNames(series, 1, "B2:B8");
                checkBubbles(series, 1, "F2:F8");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:F8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:F2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:F4");
                checkNames(series, 1, "B1:F1");
                checkBubbles(series, 1, "B5:F5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:F6");
                checkNames(series, 2, "B1:F1");
                checkBubbles(series, 2, "B7:F7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B8:F8");
                checkNames(series, 3, "B1:F1");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 5);
                checkTitle(series, 3);
            });
        });

        describe("6 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G1"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C1:C1");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0, "D1:D1");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E1:E1");
                checkNames(series, 1, "B1:B1");
                checkBubbles(series, 1, "F1:F1");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "G1:G1");
                checkNames(series, 2, "B1:B1");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);
                checkTitle(series, 2);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G1"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C1:G1");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
                checkTitle(series, 0, "B1:B1");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G2"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B2");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D2");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E2");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F2");
                checkNames(series, 2);
                checkBubbles(series, 2, "G2:G2");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");

            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:G2");
                checkNames(series, 0, "B1:G1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G3"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:G1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:G2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:G3");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 6);
                checkTitle(series, 1);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C2:C3");
                checkNames(series, 0, "B2:B3");
                checkBubbles(series, 0, "D2:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E3");
                checkNames(series, 1, "B2:B3");
                checkBubbles(series, 1, "F2:F3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G3");
                checkNames(series, 2, "B2:B3");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 2);
                checkTitle(series, 2, "G1:G1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:G1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:G2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:G3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:G4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C2:C4");
                checkNames(series, 0, "B2:B4");
                checkBubbles(series, 0, "D2:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E4");
                checkNames(series, 1, "B2:B4");
                checkBubbles(series, 1, "F2:F4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G4");
                checkNames(series, 2, "B2:B4");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 3);
                checkTitle(series, 2, "G1:G1");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G5"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:G1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:G2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:G3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:G4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:G5");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 6);
                checkTitle(series, 2);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C2:C5");
                checkNames(series, 0, "B2:B5");
                checkBubbles(series, 0, "D2:D5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E5");
                checkNames(series, 1, "B2:B5");
                checkBubbles(series, 1, "F2:F5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G5");
                checkNames(series, 2, "B2:B5");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 4);
                checkTitle(series, 2, "G1:G1");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G6"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:G1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:G2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:G3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:G4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:G5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:G6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C2:C6");
                checkNames(series, 0, "B2:B6");
                checkBubbles(series, 0, "D2:D6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E6");
                checkNames(series, 1, "B2:B6");
                checkBubbles(series, 1, "F2:F6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G6");
                checkNames(series, 2, "B2:B6");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 5);
                checkTitle(series, 2, "G1:G1");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G7"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:G1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:G2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:G3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:G4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:G5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:G6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B7:G7");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 6);
                checkTitle(series, 3);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C2:C7");
                checkNames(series, 0, "B2:B7");
                checkBubbles(series, 0, "D2:D7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E7");
                checkNames(series, 1, "B2:B7");
                checkBubbles(series, 1, "F2:F7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G7");
                checkNames(series, 2, "B2:B7");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 6);
                checkTitle(series, 2, "G1:G1");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G8"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C2:C8");
                checkNames(series, 0, "B2:B8");
                checkBubbles(series, 0, "D2:D8");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E8");
                checkNames(series, 1, "B2:B8");
                checkBubbles(series, 1, "F2:F8");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G8");
                checkNames(series, 2, "B2:B8");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 7);
                checkTitle(series, 2, "G1:G1");
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:G8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:G1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:G2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:G3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:G4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:G5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:G6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B7:G7");
                checkNames(series, 3);
                checkBubbles(series, 3, "B8:G8");
                checkBubbleArray(series, 3);
                checkTitle(series, 3);
            });
        });

        describe("7 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H1"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:B1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C1:C1");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D1:D1");
                checkNames(series, 1);
                checkBubbles(series, 1, "E1:E1");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F1:F1");
                checkNames(series, 2);
                checkBubbles(series, 2, "G1:G1");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "H1:H1");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);
                checkTitle(series, 3);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H1"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C1:H1");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
                checkTitle(series, 0, "B1:B1");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H2"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C2:C2");
                checkNames(series, 0, "B2:B2");
                checkBubbles(series, 0, "D2:D2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E2");
                checkNames(series, 1, "B2:B2");
                checkBubbles(series, 1, "F2:F2");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G2");
                checkNames(series, 2, "B2:B2");
                checkBubbles(series, 2, "H2:H2");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "G1:G1");
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:H2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 7);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H3"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C1:C3");
                checkNames(series, 0, "B1:B3");
                checkBubbles(series, 0, "D1:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E1:E3");
                checkNames(series, 1, "B1:B3");
                checkBubbles(series, 1, "F1:F3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "G1:G3");
                checkNames(series, 2, "B1:B3");
                checkBubbles(series, 2, "H1:H3");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "C1:H1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:H2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "C3:H3");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 6);
                checkTitle(series, 1, "B3:B3");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:H2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0, "B3:H3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:H4");
                checkNames(series, 1, "B1:H1");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 7);
                checkTitle(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C2:C4");
                checkNames(series, 0, "B2:B4");
                checkBubbles(series, 0, "D2:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E4");
                checkNames(series, 1, "B2:B4");
                checkBubbles(series, 1, "F2:F4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G4");
                checkNames(series, 2, "B2:B4");
                checkBubbles(series, 2, "H2:H4");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "G1:G1");

            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H5"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C1:C5");
                checkNames(series, 0, "B1:B5");
                checkBubbles(series, 0, "D1:D5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E1:E5");
                checkNames(series, 1, "B1:B5");
                checkBubbles(series, 1, "F1:F5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "G1:G5");
                checkNames(series, 2, "B1:B5");
                checkBubbles(series, 2, "H1:H5");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C1:H1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:H2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "C3:H3");
                checkNames(series, 1);
                checkBubbles(series, 1, "C4:H4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "B3:B3");

                checkValues(series, 2, "C5:H5");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 6);
                checkTitle(series, 2, "B5:B5");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H6"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:H2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0, "B3:H3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:H4");
                checkNames(series, 1, "B1:H1");
                checkBubbles(series, 1, "B5:H5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:H6");
                checkNames(series, 2, "B1:H1");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 7);
                checkTitle(series, 2);

            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C2:C6");
                checkNames(series, 0, "B2:B6");
                checkBubbles(series, 0, "D2:D6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E6");
                checkNames(series, 1, "B2:B6");
                checkBubbles(series, 1, "F2:F6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G6");
                checkNames(series, 2, "B2:B6");
                checkBubbles(series, 2, "H2:H6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "G1:G1");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C1:C7");
                checkNames(series, 0, "B1:B7");
                checkBubbles(series, 0, "D1:D7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E1:E7");
                checkNames(series, 1, "B1:B7");
                checkBubbles(series, 1, "F1:F7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "G1:G7");
                checkNames(series, 2, "B1:B7");
                checkBubbles(series, 2, "H1:H7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "C1:H1");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:H2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "C3:H3");
                checkNames(series, 1);
                checkBubbles(series, 1, "C4:H4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "B3:B3");

                checkValues(series, 2, "C5:H5");
                checkNames(series, 2);
                checkBubbles(series, 2, "C6:H6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "B5:B5");

                checkValues(series, 3, "C7:H7");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 6);
                checkTitle(series, 3, "B7:B7");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H8"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:H2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0, "B3:H3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B4:H4");
                checkNames(series, 1, "B1:H1");
                checkBubbles(series, 1, "B5:H5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B6:H6");
                checkNames(series, 2, "B1:H1");
                checkBubbles(series, 2, "B7:H7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B8:H8");
                checkNames(series, 3, "B1:H1");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 7);
                checkTitle(series, 3);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:H8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "C2:C8");
                checkNames(series, 0, "B2:B8");
                checkBubbles(series, 0, "D2:D8");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E8");
                checkNames(series, 1, "B2:B8");
                checkBubbles(series, 1, "F2:F8");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G8");
                checkNames(series, 2, "B2:B8");
                checkBubbles(series, 2, "H2:H8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "G1:G1");

            });
        });

        describe("8 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I1"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "C1:C1");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0, "D1:D1");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "E1:E1");
                checkNames(series, 1, "B1:B1");
                checkBubbles(series, 1, "F1:F1");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "G1:G1");
                checkNames(series, 2, "B1:B1");
                checkBubbles(series, 2, "H1:H1");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "I1:I1");
                checkNames(series, 3, "B1:B1");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);
                checkTitle(series, 3);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I1"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "C1:I1");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 7);
                checkTitle(series, 0, "B1:B1");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I2"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B2");
                checkNames(series, 0);
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D2");
                checkNames(series, 1);
                checkBubbles(series, 1, "E2:E2");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F2");
                checkNames(series, 2);
                checkBubbles(series, 2, "G2:G2");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");

                checkValues(series, 3, "H2:H2");
                checkNames(series, 3);
                checkBubbles(series, 3, "I2:I2");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "H1:H1");
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:I2");
                checkNames(series, 0, "B1:I1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 8);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I3"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:I1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:I2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:I3");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 8);
                checkTitle(series, 1);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "C2:C3");
                checkNames(series, 0, "B2:B3");
                checkBubbles(series, 0, "D2:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E3");
                checkNames(series, 1, "B2:B3");
                checkBubbles(series, 1, "F2:F3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G3");
                checkNames(series, 2, "B2:B3");
                checkBubbles(series, 2, "H2:H3");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "G1:G1");

                checkValues(series, 3, "I2:I3");
                checkNames(series, 3, "B2:B3");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 2);
                checkTitle(series, 3, "I1:I1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:I1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:I2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:I3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:I4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "C2:C4");
                checkNames(series, 0, "B2:B4");
                checkBubbles(series, 0, "D2:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E4");
                checkNames(series, 1, "B2:B4");
                checkBubbles(series, 1, "F2:F4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G4");
                checkNames(series, 2, "B2:B4");
                checkBubbles(series, 2, "H2:H4");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "G1:G1");

                checkValues(series, 3, "I2:I4");
                checkNames(series, 3, "B2:B4");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 3);
                checkTitle(series, 3, "I1:I1");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I5"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:I1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:I2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:I3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:I4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:I5");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 8);
                checkTitle(series, 2);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "C2:C5");
                checkNames(series, 0, "B2:B5");
                checkBubbles(series, 0, "D2:D5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E5");
                checkNames(series, 1, "B2:B5");
                checkBubbles(series, 1, "F2:F5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G5");
                checkNames(series, 2, "B2:B5");
                checkBubbles(series, 2, "H2:H5");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "G1:G1");

                checkValues(series, 3, "I2:I5");
                checkNames(series, 3, "B2:B5");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 4);
                checkTitle(series, 3, "I1:I1");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I6"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:I1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:I2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:I3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:I4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:I5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:I6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "C2:C6");
                checkNames(series, 0, "B2:B6");
                checkBubbles(series, 0, "D2:D6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E6");
                checkNames(series, 1, "B2:B6");
                checkBubbles(series, 1, "F2:F6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G6");
                checkNames(series, 2, "B2:B6");
                checkBubbles(series, 2, "H2:H6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "G1:G1");

                checkValues(series, 3, "I2:I6");
                checkNames(series, 3, "B2:B6");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 5);
                checkTitle(series, 3, "I1:I1");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I7"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:I1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:I2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:I3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:I4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:I5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:I6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B7:I7");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 8);
                checkTitle(series, 3);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "C2:C7");
                checkNames(series, 0, "B2:B7");
                checkBubbles(series, 0, "D2:D7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E7");
                checkNames(series, 1, "B2:B7");
                checkBubbles(series, 1, "F2:F7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G7");
                checkNames(series, 2, "B2:B7");
                checkBubbles(series, 2, "H2:H7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "G1:G1");

                checkValues(series, 3, "I2:I7");
                checkNames(series, 3, "B2:B7");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 6);
                checkTitle(series, 3, "I1:I1");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I8"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:I1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:I2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "B3:I3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:I4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "B5:I5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:I6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "B7:I7");
                checkNames(series, 3);
                checkBubbles(series, 3, "B8:I8");
                checkBubbleArray(series, 3);
                checkTitle(series, 3);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("B1:I8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "C2:C8");
                checkNames(series, 0, "B2:B8");
                checkBubbles(series, 0, "D2:D8");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "C1:C1");

                checkValues(series, 1, "E2:E8");
                checkNames(series, 1, "B2:B8");
                checkBubbles(series, 1, "F2:F8");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "E1:E1");

                checkValues(series, 2, "G2:G8");
                checkNames(series, 2, "B2:B8");
                checkBubbles(series, 2, "H2:H8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "G1:G1");

                checkValues(series, 3, "I2:I8");
                checkNames(series, 3, "B2:B8");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 7);
                checkTitle(series, 3, "I1:I1");
            });
        });
    });

    // ---- LEFT TITLE ------
    describe("create bubblecharts left title cells:", () => {

        describe("1 column", () => {

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "A3:A3");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 1);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "A3:A3");
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 1);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A2");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "A3:A3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "A4:A4");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "A3:A4");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
                checkTitle(series, 0, "A2:A2");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A3:A3");
                checkTitle(series, 0);
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0, "A4:A4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "A5:A5");
                checkTitle(series, 1);
                checkNames(series, 1, "A2:A2");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "A3:A5");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
                checkTitle(series, 0, "A2:A2");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A6"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A2");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "A3:A3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "A4:A4");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "A5:A5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "A6:A6");
                checkTitle(series, 2);
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "A3:A6");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
                checkTitle(series, 0, "A2:A2");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A3:A3");
                checkTitle(series, 0);
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0, "A4:A4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "A5:A5");
                checkTitle(series, 1);
                checkNames(series, 1, "A2:A2");
                checkBubbles(series, 1, "A6:A6");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "A7:A7");
                checkTitle(series, 2);
                checkNames(series, 2, "A2:A2");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);

            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "A3:A7");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
                checkTitle(series, 0, "A2:A2");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A8"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A2");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "A3:A3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "A4:A4");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "A5:A5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "A6:A6");
                checkTitle(series, 2);
                checkNames(series, 2);
                checkBubbles(series, 2, "A7:A7");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "A8:A8");
                checkTitle(series, 3);
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);

            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "A3:A8");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
                checkTitle(series, 0, "A2:A2");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A9"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A3:A3");
                checkTitle(series, 0);
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0, "A4:A4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "A5:A5");
                checkTitle(series, 1);
                checkNames(series, 1, "A2:A2");
                checkBubbles(series, 1, "A6:A6");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "A7:A7");
                checkTitle(series, 2);
                checkNames(series, 2, "A2:A2");
                checkBubbles(series, 2, "A8:A8");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "A9:A9");
                checkTitle(series, 3);
                checkNames(series, 3, "A2:A2");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);

            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:A9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "A3:A9");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 7);
                checkTitle(series, 0, "A2:A2");
            });
        });

        describe("2 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B2"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 1);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B2");
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 1);
                checkTitle(series, 0);
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B3");
                checkNames(series, 0, "A2:A3");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
                checkTitle(series, 0);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B4"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B3:B3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:B2");
                checkBubbles(series, 0, "B4:B4");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B4");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
                checkTitle(series, 0);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:B4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:B5");
                checkBubbleArray(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B5");
                checkNames(series, 0, "A2:A5");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
                checkTitle(series, 0);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:B3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:B2");
                checkBubbles(series, 0, "B4:B4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:B5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:B2");
                checkBubbles(series, 1, "B6:B6");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B6");
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
                checkTitle(series, 0);
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:B4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:B5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:B6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2);
                checkBubbles(series, 2, "B7:B7");
                checkBubbleArray(series, 2);

            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B7");
                checkNames(series, 0, "A2:A7");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
                checkTitle(series, 0);
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B8"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:B3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:B2");
                checkBubbles(series, 0, "B4:B4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:B5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:B2");
                checkBubbles(series, 1, "B6:B6");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B7:B7");
                checkTitle(series, 2, "A7:A7");
                checkNames(series, 2, "B2:B2");
                checkBubbles(series, 2, "B8:B8");
                checkBubbleArray(series, 2);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B8");
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 7);
                checkTitle(series, 0);
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B9"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0);
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:B4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1);
                checkBubbles(series, 1, "B5:B5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:B6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2);
                checkBubbles(series, 2, "B7:B7");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "B8:B8");
                checkTitle(series, 3, "A8:A8");
                checkNames(series, 3);
                checkBubbles(series, 3, "B9:B9");
                checkBubbleArray(series, 3);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:B9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B9");
                checkNames(series, 0, "A2:A9");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 8);
                checkTitle(series, 0);
            });
        });

        describe("3 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C2"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A2");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C2");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:C2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
                checkTitle(series, 0, "A2:A2");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B3:C3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:C2");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A3");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C3");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 2);
                checkTitle(series, 1);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C4"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B3:C3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:C2");
                checkBubbles(series, 0, "B4:C4");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A4");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C4");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 3);
                checkTitle(series, 1);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A5");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B5");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C5");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 4);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:C3");
                checkNames(series, 0, "B2:C2");
                checkBubbles(series, 0, "B4:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:C5");
                checkNames(series, 1, "B2:C2");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 2);
                checkTitle(series, 1, "A5:A5");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:C3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:C2");
                checkBubbles(series, 0, "B4:C4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:C5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:C2");
                checkBubbles(series, 1, "B6:C6");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A6");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C6");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 5);
                checkTitle(series, 1);
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C7"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A7");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B7");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C7");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 6);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:C3");
                checkNames(series, 0, "B2:C2");
                checkBubbles(series, 0, "B4:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:C5");
                checkNames(series, 1, "B2:C2");
                checkBubbles(series, 1, "B6:C6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:C7");
                checkNames(series, 2, "B2:C2");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 2);
                checkTitle(series, 2, "A7:A7");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C8"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:C3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:C2");
                checkBubbles(series, 0, "B4:C4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:C5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:C2");
                checkBubbles(series, 1, "B6:C6");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B7:C7");
                checkTitle(series, 2, "A7:A7");
                checkNames(series, 2, "B2:C2");
                checkBubbles(series, 2, "B8:C8");
                checkBubbleArray(series, 2);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A8");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B8");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C8");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 7);
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C9"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A9");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B9");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C9");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 8);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:C9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B3:C3");
                checkNames(series, 0, "B2:C2");
                checkBubbles(series, 0, "B4:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:C5");
                checkNames(series, 1, "B2:C2");
                checkBubbles(series, 1, "B6:C6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:C7");
                checkNames(series, 2, "B2:C2");
                checkBubbles(series, 2, "B8:C8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A7:A7");

                checkValues(series, 3, "B9:C9");
                checkNames(series, 3, "B2:C2");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 2);
                checkTitle(series, 3, "A9:A9");
            });
        });

        describe("4 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D2"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0);
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D2");
                checkTitle(series, 1);
                checkNames(series, 1, "A2:A2");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:D2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
                checkTitle(series, 0, "A2:A2");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B3:D3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:D2");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A3");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D4"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B3:D3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:D2");
                checkBubbles(series, 0, "B4:D4");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B4");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D4");
                checkNames(series, 1, "A2:A4");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 3);
                checkTitle(series, 1);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A5");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B5");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C5");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D5");
                checkBubbleArray(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:D3");
                checkNames(series, 0, "B2:D2");
                checkBubbles(series, 0, "B4:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:D5");
                checkNames(series, 1, "B2:D2");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 3);
                checkTitle(series, 1, "A5:A5");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B6");
                checkTitle(series, 0);
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D6");
                checkTitle(series, 1);
                checkNames(series, 1, "A2:A6");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 5);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:D3");
                checkNames(series, 0, "B2:D2");
                checkBubbles(series, 0, "B4:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:D5");
                checkNames(series, 1, "B2:D2");
                checkBubbles(series, 1, "B6:D6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D7"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A7");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B7");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C7");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D7");
                checkBubbleArray(series, 1);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:D3");
                checkNames(series, 0, "B2:D2");
                checkBubbles(series, 0, "B4:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:D5");
                checkNames(series, 1, "B2:D2");
                checkBubbles(series, 1, "B6:D6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:D7");
                checkNames(series, 2, "B2:D2");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 3);
                checkTitle(series, 2, "A7:A7");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D8"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B8");
                checkTitle(series, 0);
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0, "C2:C8");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D8");
                checkTitle(series, 1);
                checkNames(series, 1, "A2:A8");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 7);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:D3");
                checkNames(series, 0, "B2:D2");
                checkBubbles(series, 0, "B4:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:D5");
                checkNames(series, 1, "B2:D2");
                checkBubbles(series, 1, "B6:D6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:D7");
                checkNames(series, 2, "B2:D2");
                checkBubbles(series, 2, "B8:D8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A7:A7");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D9"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A9");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B9");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C9");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D9");
                checkBubbleArray(series, 1);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:D9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B3:D3");
                checkNames(series, 0, "B2:D2");
                checkBubbles(series, 0, "B4:D4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:D5");
                checkNames(series, 1, "B2:D2");
                checkBubbles(series, 1, "B6:D6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:D7");
                checkNames(series, 2, "B2:D2");
                checkBubbles(series, 2, "B8:D8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A7:A7");

                checkValues(series, 3, "B9:D9");
                checkNames(series, 3, "B2:D2");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 3);
                checkTitle(series, 3, "A9:A9");
            });

        });

        describe("5 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E2"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A2");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C2");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D2");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "E2:E2");
                checkTitle(series, 2);
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:E2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
                checkTitle(series, 0, "A2:A2");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B3:E3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:E2");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A3");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E3");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 2);
                checkTitle(series, 2);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E4"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B3:E3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:E2");
                checkBubbles(series, 0, "B4:E4");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A4");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C4");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E4");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 3);
                checkTitle(series, 2);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:E3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:E2");
                checkBubbles(series, 0, "B4:E4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:E5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:E2");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 4);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A5");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C5");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E5");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 4);
                checkTitle(series, 2);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:E3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:E2");
                checkBubbles(series, 0, "B4:E4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:E5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:E2");
                checkBubbles(series, 1, "B6:E6");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A6");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C6");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E6");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 5);
                checkTitle(series, 2);
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A7");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B7");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C7");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D7");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "E2:E7");
                checkTitle(series, 2);
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 6);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:E3");
                checkNames(series, 0, "B2:E2");
                checkBubbles(series, 0, "B4:E4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:E5");
                checkNames(series, 1, "B2:E2");
                checkBubbles(series, 1, "B6:E6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:E7");
                checkNames(series, 2, "B2:E2");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 4);
                checkTitle(series, 2, "A7:A7");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E8"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:E3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:E2");
                checkBubbles(series, 0, "B4:E4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:E5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:E2");
                checkBubbles(series, 1, "B6:E6");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B7:E7");
                checkTitle(series, 2, "A7:A7");
                checkNames(series, 2, "B2:E2");
                checkBubbles(series, 2, "B8:E8");
                checkBubbleArray(series, 2);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A8");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B8");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C8");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D8");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E8");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 7);
                checkTitle(series, 2);
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E9"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A9");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B9");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C9");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D9");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "E2:E9");
                checkTitle(series, 2);
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 8);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:E9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B3:E3");
                checkNames(series, 0, "B2:E2");
                checkBubbles(series, 0, "B4:E4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:E5");
                checkNames(series, 1, "B2:E2");
                checkBubbles(series, 1, "B6:E6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:E7");
                checkNames(series, 2, "B2:E2");
                checkBubbles(series, 2, "B8:E8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A7:A7");

                checkValues(series, 3, "B9:E9");
                checkNames(series, 3, "B2:E2");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 4);
                checkTitle(series, 3, "A9:A9");
            });
        });

        describe("6 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F2"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0);
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D2");
                checkTitle(series, 1);
                checkNames(series, 1, "A2:A2");
                checkBubbles(series, 1, "E2:E2");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F2");
                checkTitle(series, 2);
                checkNames(series, 2, "A2:A2");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:F2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
                checkTitle(series, 0, "A2:A2");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B3:F3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:F2");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A3");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E3");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F3");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F4"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B3:F3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:F2");
                checkBubbles(series, 0, "B4:F4");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B4");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D4");
                checkNames(series, 1, "A2:A4");
                checkBubbles(series, 1, "E2:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F2:F4");
                checkNames(series, 2, "A2:A4");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 3);
                checkTitle(series, 2);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:F3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:F2");
                checkBubbles(series, 0, "B4:F4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:F5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:F2");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 5);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A5");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C5");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E5");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F5");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:F3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:F2");
                checkBubbles(series, 0, "B4:F4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:F5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:F2");
                checkBubbles(series, 1, "B6:F6");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B6");
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D6");
                checkNames(series, 1, "A2:A6");
                checkBubbles(series, 1, "E2:E6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F2:F6");
                checkNames(series, 2, "A2:A6");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 5);
                checkTitle(series, 2);
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A7");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B7");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C7");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D7");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "E2:E7");
                checkTitle(series, 2);
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F7");
                checkBubbleArray(series, 2);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:F3");
                checkNames(series, 0, "B2:F2");
                checkBubbles(series, 0, "B4:F4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:F5");
                checkNames(series, 1, "B2:F2");
                checkBubbles(series, 1, "B6:F6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:F7");
                checkNames(series, 2, "B2:F2");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 5);
                checkTitle(series, 2, "A7:A7");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F8"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B8");
                checkTitle(series, 0);
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0, "C2:C8");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D8");
                checkTitle(series, 1);
                checkNames(series, 1, "A2:A8");
                checkBubbles(series, 1, "E2:E8");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F8");
                checkTitle(series, 2);
                checkNames(series, 2, "A2:A8");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 7);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:F3");
                checkNames(series, 0, "B2:F2");
                checkBubbles(series, 0, "B4:F4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:F5");
                checkNames(series, 1, "B2:F2");
                checkBubbles(series, 1, "B6:F6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:F7");
                checkNames(series, 2, "B2:F2");
                checkBubbles(series, 2, "B8:F8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A7:A7");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F9"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A9");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B9");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C9");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D9");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "E2:E9");
                checkTitle(series, 2);
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F9");
                checkBubbleArray(series, 2);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:F9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B3:F3");
                checkNames(series, 0, "B2:F2");
                checkBubbles(series, 0, "B4:F4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:F5");
                checkNames(series, 1, "B2:F2");
                checkBubbles(series, 1, "B6:F6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:F7");
                checkNames(series, 2, "B2:F2");
                checkBubbles(series, 2, "B8:F8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A7:A7");

                checkValues(series, 3, "B9:F9");
                checkNames(series, 3, "B2:F2");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 5);
                checkTitle(series, 3, "A9:A9");
            });
        });

        describe("7 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G2"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A2");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C2");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D2");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "E2:E2");
                checkTitle(series, 2);
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F2");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "G2:G2");
                checkTitle(series, 3);
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:G2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
                checkTitle(series, 0, "A2:A2");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B3:G3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:G2");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A3");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E3");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F3");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "G2:G3");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 2);
                checkTitle(series, 3);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G4"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B3:G3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:G2");
                checkBubbles(series, 0, "B4:G4");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A4");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C4");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E4");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F4");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "G2:G4");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 3);
                checkTitle(series, 3);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:G3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:G2");
                checkBubbles(series, 0, "B4:G4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:G5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:G2");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 6);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A5");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C5");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E5");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F5");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "G2:G5");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 4);
                checkTitle(series, 3);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:G3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:G2");
                checkBubbles(series, 0, "B4:G4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:G5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:G2");
                checkBubbles(series, 1, "B6:G6");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A6");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C6");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E6");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "G2:G6");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 5);
                checkTitle(series, 3);
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:G3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:G2");
                checkBubbles(series, 0, "B4:G4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:G5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:G2");
                checkBubbles(series, 1, "B6:G6");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B7:G7");
                checkTitle(series, 2, "A7:A7");
                checkNames(series, 2, "B2:G2");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 6);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A7");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C7");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E7");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "G2:G7");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 6);
                checkTitle(series, 3);
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G8"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:G3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:G2");
                checkBubbles(series, 0, "B4:G4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:G5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:G2");
                checkBubbles(series, 1, "B6:G6");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B7:G7");
                checkTitle(series, 2, "A7:A7");
                checkNames(series, 2, "B2:G2");
                checkBubbles(series, 2, "B8:G8");
                checkBubbleArray(series, 2);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A8");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B8");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C8");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D8");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E8");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "G2:G8");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 7);
                checkTitle(series, 3);
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G9"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A9");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B9");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C9");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D9");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "E2:E9");
                checkTitle(series, 2);
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F9");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "G2:G9");
                checkTitle(series, 3);
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 8);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:G9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B3:G3");
                checkNames(series, 0, "B2:G2");
                checkBubbles(series, 0, "B4:G4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:G5");
                checkNames(series, 1, "B2:G2");
                checkBubbles(series, 1, "B6:G6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:G7");
                checkNames(series, 2, "B2:G2");
                checkBubbles(series, 2, "B8:G8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A7:A7");

                checkValues(series, 3, "B9:G9");
                checkNames(series, 3, "B2:G2");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 6);
                checkTitle(series, 3, "A9:A9");
            });
        });

        describe("8 column", () => {

            it("1 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H2"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0);
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D2");
                checkTitle(series, 1);
                checkNames(series, 1, "A2:A2");
                checkBubbles(series, 1, "E2:E2");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F2");
                checkTitle(series, 2);
                checkNames(series, 2, "A2:A2");
                checkBubbles(series, 2, "G2:G2");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "H2:H2");
                checkTitle(series, 3);
                checkNames(series, 3, "A2:A2");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);
            });

            it("1 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:H2");
                checkNames(series, 0);
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 7);
                checkTitle(series, 0, "A2:A2");
            });

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H3"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B3:H3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:H2");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 7);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A3");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E3");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F3");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "G2:G3");
                checkNames(series, 3);
                checkBubbles(series, 3, "H2:H3");
                checkBubbleArray(series, 3);
                checkTitle(series, 3);
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H4"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B3:H3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:H2");
                checkBubbles(series, 0, "B4:H4");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B4");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D4");
                checkNames(series, 1, "A2:A4");
                checkBubbles(series, 1, "E2:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F2:F4");
                checkNames(series, 2, "A2:A4");
                checkBubbles(series, 2, "G2:G4");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "H2:H4");
                checkNames(series, 3, "A2:A4");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 3);
                checkTitle(series, 3);
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:H3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:H2");
                checkBubbles(series, 0, "B4:H4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:H5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:H2");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 7);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A5");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C5");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E5");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F5");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "G2:G5");
                checkNames(series, 3);
                checkBubbles(series, 3, "H2:H5");
                checkBubbleArray(series, 3);
                checkTitle(series, 3);
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B3:H3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:H2");
                checkBubbles(series, 0, "B4:H4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:H5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:H2");
                checkBubbles(series, 1, "B6:H6");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B6");
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D6");
                checkNames(series, 1, "A2:A6");
                checkBubbles(series, 1, "E2:E6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F2:F6");
                checkNames(series, 2, "A2:A6");
                checkBubbles(series, 2, "G2:G6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "H2:H6");
                checkNames(series, 3, "A2:A6");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 5);
                checkTitle(series, 3);
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:H3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:H2");
                checkBubbles(series, 0, "B4:H4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:H5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:H2");
                checkBubbles(series, 1, "B6:H6");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B7:H7");
                checkTitle(series, 2, "A7:A7");
                checkNames(series, 2, "B2:H2");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 7);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A7");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "C2:C7");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "E2:E7");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "G2:G7");
                checkNames(series, 3);
                checkBubbles(series, 3, "H2:H7");
                checkBubbleArray(series, 3);
                checkTitle(series, 3);
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H8"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B3:H3");
                checkTitle(series, 0, "A3:A3");
                checkNames(series, 0, "B2:H2");
                checkBubbles(series, 0, "B4:H4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B5:H5");
                checkTitle(series, 1, "A5:A5");
                checkNames(series, 1, "B2:H2");
                checkBubbles(series, 1, "B6:H6");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B7:H7");
                checkTitle(series, 2, "A7:A7");
                checkNames(series, 2, "B2:H2");
                checkBubbles(series, 2, "B8:H8");
                checkBubbleArray(series, 2);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B8");
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0, "C2:C8");
                checkBubbleArray(series, 0);
                checkTitle(series, 0);

                checkValues(series, 1, "D2:D8");
                checkNames(series, 1, "A2:A8");
                checkBubbles(series, 1, "E2:E8");
                checkBubbleArray(series, 1);
                checkTitle(series, 1);

                checkValues(series, 2, "F2:F8");
                checkNames(series, 2, "A2:A8");
                checkBubbles(series, 2, "G2:G8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2);

                checkValues(series, 3, "H2:H8");
                checkNames(series, 3, "A2:A8");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 7);
                checkTitle(series, 3);
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H9"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A9");
                checkTitle(series, 0);
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B9");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C9");
                checkTitle(series, 1);
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D9");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "E2:E9");
                checkTitle(series, 2);
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F9");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "G2:G9");
                checkTitle(series, 3);
                checkNames(series, 3);
                checkBubbles(series, 3, "H2:H9");
                checkBubbleArray(series, 3);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A2:H9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B3:H3");
                checkNames(series, 0, "B2:H2");
                checkBubbles(series, 0, "B4:H4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A3:A3");

                checkValues(series, 1, "B5:H5");
                checkNames(series, 1, "B2:H2");
                checkBubbles(series, 1, "B6:H6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A5:A5");

                checkValues(series, 2, "B7:H7");
                checkNames(series, 2, "B2:H2");
                checkBubbles(series, 2, "B8:H8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A7:A7");

                checkValues(series, 3, "B9:H9");
                checkNames(series, 3, "B2:H2");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 7);
                checkTitle(series, 3, "A9:A9");
            });
        });
    });

    // ---- LEFT TOP TITLE ------
    describe("create bubblecharts left and top title cells:", () => {

        describe("2 column", () => {

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B2"), 0);
                checkSeriesLength(series, 1);
                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 1);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B2");
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 1);
                checkTitle(series, 0, "B1:B1");
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B3"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:B1");
                checkTitle(series, 0, "A1:A1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B3:B3");
                checkTitle(series, 1, "A3:A3");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B3");
                checkNames(series, 0, "A2:A3");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
                checkTitle(series, 0, "B1:B1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:B4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:B1");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B4");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
                checkTitle(series, 0, "B1:B1");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B5"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:B1");
                checkTitle(series, 0, "A1:A1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B3:B3");
                checkTitle(series, 1, "A3:A3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:B4");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B5:B5");
                checkTitle(series, 2, "A5:A5");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B5");
                checkNames(series, 0, "A2:A5");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
                checkTitle(series, 0, "B1:B1");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B6"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:B4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:B1");
                checkBubbles(series, 1, "B5:B5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:B6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:B1");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B6");
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
                checkTitle(series, 0, "B1:B1");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B7"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:B1");
                checkTitle(series, 0, "A1:A1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B3:B3");
                checkTitle(series, 1, "A3:A3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:B4");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B5:B5");
                checkTitle(series, 2, "A5:A5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:B6");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "B7:B7");
                checkTitle(series, 3, "A7:A7");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B7");
                checkNames(series, 0, "A2:A7");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
                checkTitle(series, 0, "B1:B1");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B8"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:B1");
                checkBubbles(series, 0, "B3:B3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:B4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:B1");
                checkBubbles(series, 1, "B5:B5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:B6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:B1");
                checkBubbles(series, 2, "B7:B7");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "B8:B8");
                checkTitle(series, 3, "A8:A8");
                checkNames(series, 3, "B1:B1");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B8");
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 7);
                checkTitle(series, 0, "B1:B1");
            });

            it("9 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B9"), 0);
                checkSeriesLength(series, 5);

                checkValues(series, 0, "B1:B1");
                checkTitle(series, 0, "A1:A1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B3:B3");
                checkTitle(series, 1, "A3:A3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:B4");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B5:B5");
                checkTitle(series, 2, "A5:A5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:B6");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "B7:B7");
                checkTitle(series, 3, "A7:A7");
                checkNames(series, 3);
                checkBubbles(series, 3, "B8:B8");
                checkBubbleArray(series, 3);

                checkValues(series, 4, "B9:B9");
                checkTitle(series, 4, "A9:A9");
                checkNames(series, 4);
                checkBubbles(series, 4);
                checkBubbleArray(series, 4, 1);
            });

            it("9 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:B9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B9");
                checkNames(series, 0, "A2:A9");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 8);
                checkTitle(series, 0, "B1:B1");
            });
        });

        describe("3 column", () => {

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C2"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A2");
                checkTitle(series, 0, "A1:A1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C2");
                checkTitle(series, 1, "C1:C1");
                checkNames(series, 1);
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:C2");
                checkNames(series, 0, "B1:C1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 2);
                checkTitle(series, 0, "A2:A2");
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C3"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:C2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:C1");
                checkBubbles(series, 0, "B3:C3");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B3");
                checkNames(series, 0, "A2:A3");
                checkBubbles(series, 0, "C2:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C4"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B4");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:C1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:C4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C5"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B5");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A5");
                checkBubbles(series, 0, "C2:C5");
                checkBubbleArray(series, 0);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:C2");
                checkNames(series, 0, "B1:C1");
                checkBubbles(series, 0, "B3:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A2:A2");

                checkValues(series, 1, "B4:C4");
                checkNames(series, 1, "B1:C1");
                checkBubbles(series, 1, "B5:C5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A4:A4");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C6"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B6");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:C1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:C4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");

                checkValues(series, 2, "B5:C5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:C6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A5:A5");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C7"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B7");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A7");
                checkBubbles(series, 0, "C2:C7");
                checkBubbleArray(series, 0);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:C2");
                checkNames(series, 0, "B1:C1");
                checkBubbles(series, 0, "B3:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A2:A2");

                checkValues(series, 1, "B4:C4");
                checkNames(series, 1, "B1:C1");
                checkBubbles(series, 1, "B5:C5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A4:A4");

                checkValues(series, 2, "B6:C6");
                checkNames(series, 2, "B1:C1");
                checkBubbles(series, 2, "B7:C7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A6:A6");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C8"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B8");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0, "C2:C8");
                checkBubbleArray(series, 0);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:C1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:C2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:C4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");

                checkValues(series, 2, "B5:C5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:C6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A5:A5");

                checkValues(series, 3, "B7:C7");
                checkNames(series, 3);
                checkBubbles(series, 3, "B8:C8");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "A7:A7");
            });

            it("9 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C9"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:B9");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A9");
                checkBubbles(series, 0, "C2:C9");
                checkBubbleArray(series, 0);
            });

            it("9 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:C9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:C2");
                checkNames(series, 0, "B1:C1");
                checkBubbles(series, 0, "B3:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A2:A2");

                checkValues(series, 1, "B4:C4");
                checkNames(series, 1, "B1:C1");
                checkBubbles(series, 1, "B5:C5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A4:A4");

                checkValues(series, 2, "B6:C6");
                checkNames(series, 2, "B1:C1");
                checkBubbles(series, 2, "B7:C7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A6:A6");

                checkValues(series, 3, "B8:C8");
                checkNames(series, 3, "B1:C1");
                checkBubbles(series, 3, "B9:C9");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "A8:A8");
            });
        });

        describe("4 column", () => {

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D2"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D2");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A2");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 1);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:D2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 3);
                checkTitle(series, 0, "A2:A2");
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D3"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:D2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A3");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:D2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:D4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:D1");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 3);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B4");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D4");
                checkNames(series, 1, "A2:A4");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 3);
                checkTitle(series, 1, "D1:D1");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:D2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:D4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:D1");
                checkBubbles(series, 1, "B5:D5");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A5");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C5");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B6");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D6");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A6");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 5);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:D2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A2:A2");

                checkValues(series, 1, "B4:D4");
                checkNames(series, 1, "B1:D1");
                checkBubbles(series, 1, "B5:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A4:A4");

                checkValues(series, 2, "B6:D6");
                checkNames(series, 2, "B1:D1");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 3);
                checkTitle(series, 2, "A6:A6");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:D2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:D4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:D1");
                checkBubbles(series, 1, "B5:D5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:D6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:D1");
                checkBubbles(series, 2, "B7:D7");
                checkBubbleArray(series, 2);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A7");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C7");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D8"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B8");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0, "C2:C8");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D8");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A8");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 7);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:D2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A2:A2");

                checkValues(series, 1, "B4:D4");
                checkNames(series, 1, "B1:D1");
                checkBubbles(series, 1, "B5:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A4:A4");

                checkValues(series, 2, "B6:D6");
                checkNames(series, 2, "B1:D1");
                checkBubbles(series, 2, "B7:D7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A6:A6");

                checkValues(series, 3, "B8:D8");
                checkNames(series, 3, "B1:D1");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 3);
                checkTitle(series, 3, "A8:A8");
            });

            it("9 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D9"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:D2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:D1");
                checkBubbles(series, 0, "B3:D3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:D4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:D1");
                checkBubbles(series, 1, "B5:D5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:D6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:D1");
                checkBubbles(series, 2, "B7:D7");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "B8:D8");
                checkTitle(series, 3, "A8:A8");
                checkNames(series, 3, "B1:D1");
                checkBubbles(series, 3, "B9:D9");
                checkBubbleArray(series, 3);
            });

            it("9 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:D9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "A2:A9");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B9");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C9");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D9");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");
            });
        });

        describe("5 column", () => {

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E2"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A2");
                checkTitle(series, 0, "A1:A1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C2");
                checkTitle(series, 1, "C1:C1");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D2");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "E2:E2");
                checkTitle(series, 2, "E1:E1");
                checkNames(series, 2);
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:E2");
                checkNames(series, 0, "B1:E1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 4);
                checkTitle(series, 0, "A2:A2");
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E3"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:E2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:E1");
                checkBubbles(series, 0, "B3:E3");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B3");
                checkNames(series, 0, "A2:A3");
                checkBubbles(series, 0, "C2:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D3");
                checkNames(series, 1, "A2:A3");
                checkBubbles(series, 1, "E2:E3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B4");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D4");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A4");
                checkBubbles(series, 1, "E2:E4");
                checkBubbleArray(series, 1);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:E1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:E2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:E3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:E2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:E1");
                checkBubbles(series, 0, "B3:E3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:E4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:E1");
                checkBubbles(series, 1, "B5:E5");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B5");
                checkNames(series, 0, "A2:A5");
                checkBubbles(series, 0, "C2:C5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D5");
                checkNames(series, 1, "A2:A5");
                checkBubbles(series, 1, "E2:E5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E6"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B6");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D6");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A6");
                checkBubbles(series, 1, "E2:E6");
                checkBubbleArray(series, 1);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:E1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:E2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:E3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");

                checkValues(series, 2, "B5:E5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:E6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A5:A5");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E7"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B7");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A7");
                checkBubbles(series, 0, "C2:C7");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D7");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A7");
                checkBubbles(series, 1, "E2:E7");
                checkBubbleArray(series, 1);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:E2");
                checkNames(series, 0, "B1:E1");
                checkBubbles(series, 0, "B3:E3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A2:A2");

                checkValues(series, 1, "B4:E4");
                checkNames(series, 1, "B1:E1");
                checkBubbles(series, 1, "B5:E5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A4:A4");

                checkValues(series, 2, "B6:E6");
                checkNames(series, 2, "B1:E1");
                checkBubbles(series, 2, "B7:E7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A6:A6");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E8"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B8");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0, "C2:C8");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D8");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A8");
                checkBubbles(series, 1, "E2:E8");
                checkBubbleArray(series, 1);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:E1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:E2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:E3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");

                checkValues(series, 2, "B5:E5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:E6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A5:A5");

                checkValues(series, 3, "B7:E7");
                checkNames(series, 3);
                checkBubbles(series, 3, "B8:E8");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "A7:A7");
            });

            it("9 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E9"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:B9");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A9");
                checkBubbles(series, 0, "C2:C9");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D9");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A9");
                checkBubbles(series, 1, "E2:E9");
                checkBubbleArray(series, 1);
            });

            it("9 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:E9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:E2");
                checkNames(series, 0, "B1:E1");
                checkBubbles(series, 0, "B3:E3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A2:A2");

                checkValues(series, 1, "B4:E4");
                checkNames(series, 1, "B1:E1");
                checkBubbles(series, 1, "B5:E5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A4:A4");

                checkValues(series, 2, "B6:E6");
                checkNames(series, 2, "B1:E1");
                checkBubbles(series, 2, "B7:E7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A6:A6");

                checkValues(series, 3, "B8:E8");
                checkNames(series, 3, "B1:E1");
                checkBubbles(series, 3, "B9:E9");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "A8:A8");
            });
        });

        describe("6 column", () => {

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F2"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D2");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A2");
                checkBubbles(series, 1, "E2:E2");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F2");
                checkTitle(series, 2, "F1:F1");
                checkNames(series, 2, "A2:A2");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 1);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:F2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 5);
                checkTitle(series, 0, "A2:A2");
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F3"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:F2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A3");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");

                checkValues(series, 2, "E2:E3");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F3");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "E1:E1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:F2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:F4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:F1");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 5);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B4");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D4");
                checkNames(series, 1, "A2:A4");
                checkBubbles(series, 1, "E2:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F4");
                checkNames(series, 2, "A2:A4");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 3);
                checkTitle(series, 2, "F1:F1");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:F2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:F4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:F1");
                checkBubbles(series, 1, "B5:F5");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A5");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C5");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");

                checkValues(series, 2, "E2:E5");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F5");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "E1:E1");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F6"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:F2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:F4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:F1");
                checkBubbles(series, 1, "B5:F5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:F6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:F1");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 5);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B6");
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D6");
                checkNames(series, 1, "A2:A6");
                checkBubbles(series, 1, "E2:E6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F6");
                checkNames(series, 2, "A2:A6");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 5);
                checkTitle(series, 2, "F1:F1");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:F2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:F4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:F1");
                checkBubbles(series, 1, "B5:F5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:F6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:F1");
                checkBubbles(series, 2, "B7:F7");
                checkBubbleArray(series, 2);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A7");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C7");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");

                checkValues(series, 2, "E2:E7");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "E1:E1");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F8"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B8");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0, "C2:C8");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D8");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A8");
                checkBubbles(series, 1, "E2:E8");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F8");
                checkTitle(series, 2, "F1:F1");
                checkNames(series, 2, "A2:A8");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 7);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:F2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A2:A2");

                checkValues(series, 1, "B4:F4");
                checkNames(series, 1, "B1:F1");
                checkBubbles(series, 1, "B5:F5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A4:A4");

                checkValues(series, 2, "B6:F6");
                checkNames(series, 2, "B1:F1");
                checkBubbles(series, 2, "B7:F7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A6:A6");

                checkValues(series, 3, "B8:F8");
                checkNames(series, 3, "B1:F1");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 5);
                checkTitle(series, 3, "A8:A8");
            });

            it("9 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F9"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:F2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:F1");
                checkBubbles(series, 0, "B3:F3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:F4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:F1");
                checkBubbles(series, 1, "B5:F5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:F6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:F1");
                checkBubbles(series, 2, "B7:F7");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "B8:F8");
                checkTitle(series, 3, "A8:A8");
                checkNames(series, 3, "B1:F1");
                checkBubbles(series, 3, "B9:F9");
                checkBubbleArray(series, 3);
            });

            it("9 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:F9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "A2:A9");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B9");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C9");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D9");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");

                checkValues(series, 2, "E2:E9");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F9");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "E1:E1");
            });
        });

        describe("7 column", () => {

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G2"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A2");
                checkTitle(series, 0, "A1:A1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C2");
                checkTitle(series, 1, "C1:C1");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D2");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "E2:E2");
                checkTitle(series, 2, "E1:E1");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F2");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "G2:G2");
                checkTitle(series, 3, "G1:G1");
                checkNames(series, 3);
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:G2");
                checkNames(series, 0, "B1:G1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 6);
                checkTitle(series, 0, "A2:A2");
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G3"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:G2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:G1");
                checkBubbles(series, 0, "B3:G3");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B3");
                checkNames(series, 0, "A2:A3");
                checkBubbles(series, 0, "C2:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D3");
                checkNames(series, 1, "A2:A3");
                checkBubbles(series, 1, "E2:E3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F3");
                checkNames(series, 2, "A2:A3");
                checkBubbles(series, 2, "G2:G3");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G4"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B4");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D4");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A4");
                checkBubbles(series, 1, "E2:E4");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F4");
                checkTitle(series, 2, "F1:F1");
                checkNames(series, 2, "A2:A4");
                checkBubbles(series, 2, "G2:G4");
                checkBubbleArray(series, 2);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:G1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:G2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:G3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:G4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:G2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:G1");
                checkBubbles(series, 0, "B3:G3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:G4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:G1");
                checkBubbles(series, 1, "B5:G5");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B5");
                checkNames(series, 0, "A2:A5");
                checkBubbles(series, 0, "C2:C5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D5");
                checkNames(series, 1, "A2:A5");
                checkBubbles(series, 1, "E2:E5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F5");
                checkNames(series, 2, "A2:A5");
                checkBubbles(series, 2, "G2:G5");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G6"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B6");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D6");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A6");
                checkBubbles(series, 1, "E2:E6");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F6");
                checkTitle(series, 2, "F1:F1");
                checkNames(series, 2, "A2:A6");
                checkBubbles(series, 2, "G2:G6");
                checkBubbleArray(series, 2);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:G1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:G2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:G3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:G4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");

                checkValues(series, 2, "B5:G5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:G6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A5:A5");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:G2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:G1");
                checkBubbles(series, 0, "B3:G3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:G4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:G1");
                checkBubbles(series, 1, "B5:G5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:G6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:G1");
                checkBubbles(series, 2, "B7:G7");
                checkBubbleArray(series, 2);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B7");
                checkNames(series, 0, "A2:A7");
                checkBubbles(series, 0, "C2:C7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D7");
                checkNames(series, 1, "A2:A7");
                checkBubbles(series, 1, "E2:E7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F7");
                checkNames(series, 2, "A2:A7");
                checkBubbles(series, 2, "G2:G7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");

            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G8"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B8");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0, "C2:C8");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D8");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A8");
                checkBubbles(series, 1, "E2:E8");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F8");
                checkTitle(series, 2, "F1:F1");
                checkNames(series, 2, "A2:A8");
                checkBubbles(series, 2, "G2:G8");
                checkBubbleArray(series, 2);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:G1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:G2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:G3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:G4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");

                checkValues(series, 2, "B5:G5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:G6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A5:A5");

                checkValues(series, 3, "B7:G7");
                checkNames(series, 3);
                checkBubbles(series, 3, "B8:G8");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "A7:A7");
            });

            it("9 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G9"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:B9");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A9");
                checkBubbles(series, 0, "C2:C9");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D9");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A9");
                checkBubbles(series, 1, "E2:E9");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F9");
                checkTitle(series, 2, "F1:F1");
                checkNames(series, 2, "A2:A9");
                checkBubbles(series, 2, "G2:G9");
                checkBubbleArray(series, 2);
            });

            it("9 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:G9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:G2");
                checkNames(series, 0, "B1:G1");
                checkBubbles(series, 0, "B3:G3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A2:A2");

                checkValues(series, 1, "B4:G4");
                checkNames(series, 1, "B1:G1");
                checkBubbles(series, 1, "B5:G5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A4:A4");

                checkValues(series, 2, "B6:G6");
                checkNames(series, 2, "B1:G1");
                checkBubbles(series, 2, "B7:G7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A6:A6");

                checkValues(series, 3, "B8:G8");
                checkNames(series, 3, "B1:G1");
                checkBubbles(series, 3, "B9:G9");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "A8:A8");
            });
        });

        describe("8 column", () => {

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H2"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B2");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A2");
                checkBubbles(series, 0, "C2:C2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D2");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A2");
                checkBubbles(series, 1, "E2:E2");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F2");
                checkTitle(series, 2, "F1:F1");
                checkNames(series, 2, "A2:A2");
                checkBubbles(series, 2, "G2:G2");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "H2:H2");
                checkTitle(series, 3, "H1:H1");
                checkNames(series, 3, "A2:A2");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 1);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:H2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 7);
                checkTitle(series, 0, "A2:A2");
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H3"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:H2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0, "B3:H3");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A3");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C3");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");

                checkValues(series, 2, "E2:E3");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F3");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "E1:E1");

                checkValues(series, 3, "G2:G3");
                checkNames(series, 3);
                checkBubbles(series, 3, "H2:H3");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "G1:G1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H4"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:H2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0, "B3:H3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:H4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:H1");
                checkBubbles(series, 1);
                checkBubbleArray(series, 1, 7);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B4");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D4");
                checkNames(series, 1, "A2:A4");
                checkBubbles(series, 1, "E2:E4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F4");
                checkNames(series, 2, "A2:A4");
                checkBubbles(series, 2, "G2:G4");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");

                checkValues(series, 3, "H2:H4");
                checkNames(series, 3, "A2:A4");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 3);
                checkTitle(series, 3, "H1:H1");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:H2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0, "B3:H3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:H4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:H1");
                checkBubbles(series, 1, "B5:H5");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A5");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C5");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");

                checkValues(series, 2, "E2:E5");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F5");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "E1:E1");

                checkValues(series, 3, "G2:G5");
                checkNames(series, 3);
                checkBubbles(series, 3, "H2:H5");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "G1:G1");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H6"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:H2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0, "B3:H3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:H4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:H1");
                checkBubbles(series, 1, "B5:H5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:H6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:H1");
                checkBubbles(series, 2);
                checkBubbleArray(series, 2, 7);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B6");
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D6");
                checkNames(series, 1, "A2:A6");
                checkBubbles(series, 1, "E2:E6");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F6");
                checkNames(series, 2, "A2:A6");
                checkBubbles(series, 2, "G2:G6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");

                checkValues(series, 3, "H2:H6");
                checkNames(series, 3, "A2:A6");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 5);
                checkTitle(series, 3, "H1:H1");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:H2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0, "B3:H3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:H4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:H1");
                checkBubbles(series, 1, "B5:H5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:H6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:H1");
                checkBubbles(series, 2, "B7:H7");
                checkBubbleArray(series, 2);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A7");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C7");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");

                checkValues(series, 2, "E2:E7");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "E1:E1");

                checkValues(series, 3, "G2:G7");
                checkNames(series, 3);
                checkBubbles(series, 3, "H2:H7");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "G1:G1");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H8"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:H2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0, "B3:H3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:H4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:H1");
                checkBubbles(series, 1, "B5:H5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:H6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:H1");
                checkBubbles(series, 2, "B7:H7");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "B8:H8");
                checkTitle(series, 3, "A8:A8");
                checkNames(series, 3, "B1:H1");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 7);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B8");
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0, "C2:C8");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D8");
                checkNames(series, 1, "A2:A8");
                checkBubbles(series, 1, "E2:E8");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F8");
                checkNames(series, 2, "A2:A8");
                checkBubbles(series, 2, "G2:G8");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");

                checkValues(series, 3, "H2:H8");
                checkNames(series, 3, "A2:A8");
                checkBubbles(series, 3);
                checkBubbleArray(series, 3, 7);
                checkTitle(series, 3, "H1:H1");
            });

            it("9 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H9"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:H2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:H1");
                checkBubbles(series, 0, "B3:H3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:H4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:H1");
                checkBubbles(series, 1, "B5:H5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:H6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:H1");
                checkBubbles(series, 2, "B7:H7");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "B8:H8");
                checkTitle(series, 3, "A8:A8");
                checkNames(series, 3, "B1:H1");
                checkBubbles(series, 3, "B9:H9");
                checkBubbleArray(series, 3);
            });

            it("9 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:H9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "A2:A9");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B9");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "C2:C9");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D9");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "C1:C1");

                checkValues(series, 2, "E2:E9");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F9");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "E1:E1");

                checkValues(series, 3, "G2:G9");
                checkNames(series, 3);
                checkBubbles(series, 3, "H2:H9");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "G1:G1");
            });
        });

        describe("9 column", () => {

            it("2 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I2"), 0);
                checkSeriesLength(series, 5);

                checkValues(series, 0, "A2:A2");
                checkTitle(series, 0, "A1:A1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:B2");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "C2:C2");
                checkTitle(series, 1, "C1:C1");
                checkNames(series, 1);
                checkBubbles(series, 1, "D2:D2");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "E2:E2");
                checkTitle(series, 2, "E1:E1");
                checkNames(series, 2);
                checkBubbles(series, 2, "F2:F2");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "G2:G2");
                checkTitle(series, 3, "G1:G1");
                checkNames(series, 3);
                checkBubbles(series, 3, "H2:H2");
                checkBubbleArray(series, 3);

                checkValues(series, 4, "I2:I2");
                checkTitle(series, 4, "I1:I1");
                checkNames(series, 4);
                checkBubbles(series, 4);
                checkBubbleArray(series, 4, 1);
            });

            it("2 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I2"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:I2");
                checkNames(series, 0, "B1:I1");
                checkBubbles(series, 0);
                checkBubbleArray(series, 0, 8);
                checkTitle(series, 0, "A2:A2");
            });

            it("3 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I3"), 0);
                checkSeriesLength(series, 1);

                checkValues(series, 0, "B2:I2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:I1");
                checkBubbles(series, 0, "B3:I3");
                checkBubbleArray(series, 0);
            });

            it("3 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I3"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B3");
                checkNames(series, 0, "A2:A3");
                checkBubbles(series, 0, "C2:C3");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D3");
                checkNames(series, 1, "A2:A3");
                checkBubbles(series, 1, "E2:E3");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F3");
                checkNames(series, 2, "A2:A3");
                checkBubbles(series, 2, "G2:G3");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");

                checkValues(series, 3, "H2:H3");
                checkNames(series, 3, "A2:A3");
                checkBubbles(series, 3, "I2:I3");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "H1:H1");
            });

            it("4 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I4"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B4");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A4");
                checkBubbles(series, 0, "C2:C4");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D4");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A4");
                checkBubbles(series, 1, "E2:E4");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F4");
                checkTitle(series, 2, "F1:F1");
                checkNames(series, 2, "A2:A4");
                checkBubbles(series, 2, "G2:G4");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "H2:H4");
                checkTitle(series, 3, "H1:H1");
                checkNames(series, 3, "A2:A4");
                checkBubbles(series, 3, "I2:I4");
                checkBubbleArray(series, 3);
            });

            it("4 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I4"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B1:I1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:I2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:I3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:I4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");
            });

            it("5 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I5"), 0);
                checkSeriesLength(series, 2);

                checkValues(series, 0, "B2:I2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:I1");
                checkBubbles(series, 0, "B3:I3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:I4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:I1");
                checkBubbles(series, 1, "B5:I5");
                checkBubbleArray(series, 1);
            });

            it("5 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I5"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B5");
                checkNames(series, 0, "A2:A5");
                checkBubbles(series, 0, "C2:C5");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D5");
                checkNames(series, 1, "A2:A5");
                checkBubbles(series, 1, "E2:E5");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F5");
                checkNames(series, 2, "A2:A5");
                checkBubbles(series, 2, "G2:G5");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");

                checkValues(series, 3, "H2:H5");
                checkNames(series, 3, "A2:A5");
                checkBubbles(series, 3, "I2:I5");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "H1:H1");
            });

            it("6 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I6"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B6");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A6");
                checkBubbles(series, 0, "C2:C6");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D6");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A6");
                checkBubbles(series, 1, "E2:E6");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F6");
                checkTitle(series, 2, "F1:F1");
                checkNames(series, 2, "A2:A6");
                checkBubbles(series, 2, "G2:G6");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "H2:H6");
                checkTitle(series, 3, "H1:H1");
                checkNames(series, 3, "A2:A6");
                checkBubbles(series, 3, "I2:I6");
                checkBubbleArray(series, 3);
            });

            it("6 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I6"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B1:I1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:I2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:I3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:I4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");

                checkValues(series, 2, "B5:I5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:I6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A5:A5");
            });

            it("7 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I7"), 0);
                checkSeriesLength(series, 3);

                checkValues(series, 0, "B2:I2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:I1");
                checkBubbles(series, 0, "B3:I3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:I4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:I1");
                checkBubbles(series, 1, "B5:I5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:I6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:I1");
                checkBubbles(series, 2, "B7:I7");
                checkBubbleArray(series, 2);
            });

            it("7 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I7"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B7");
                checkNames(series, 0, "A2:A7");
                checkBubbles(series, 0, "C2:C7");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D7");
                checkNames(series, 1, "A2:A7");
                checkBubbles(series, 1, "E2:E7");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F7");
                checkNames(series, 2, "A2:A7");
                checkBubbles(series, 2, "G2:G7");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");

                checkValues(series, 3, "H2:H7");
                checkNames(series, 3, "A2:A7");
                checkBubbles(series, 3, "I2:I7");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "H1:H1");
            });

            it("8 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I8"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B8");
                checkTitle(series, 0, "B1:B1");
                checkNames(series, 0, "A2:A8");
                checkBubbles(series, 0, "C2:C8");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "D2:D8");
                checkTitle(series, 1, "D1:D1");
                checkNames(series, 1, "A2:A8");
                checkBubbles(series, 1, "E2:E8");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "F2:F8");
                checkTitle(series, 2, "F1:F1");
                checkNames(series, 2, "A2:A8");
                checkBubbles(series, 2, "G2:G8");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "H2:H8");
                checkTitle(series, 3, "H1:H1");
                checkNames(series, 3, "A2:A8");
                checkBubbles(series, 3, "I2:I8");
                checkBubbleArray(series, 3);
            });

            it("8 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I8"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B1:I1");
                checkNames(series, 0);
                checkBubbles(series, 0, "B2:I2");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "A1:A1");

                checkValues(series, 1, "B3:I3");
                checkNames(series, 1);
                checkBubbles(series, 1, "B4:I4");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "A3:A3");

                checkValues(series, 2, "B5:I5");
                checkNames(series, 2);
                checkBubbles(series, 2, "B6:I6");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "A5:A5");

                checkValues(series, 3, "B7:I7");
                checkNames(series, 3);
                checkBubbles(series, 3, "B8:I8");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "A7:A7");
            });

            it("9 rows", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I9"), 0);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:I2");
                checkTitle(series, 0, "A2:A2");
                checkNames(series, 0, "B1:I1");
                checkBubbles(series, 0, "B3:I3");
                checkBubbleArray(series, 0);

                checkValues(series, 1, "B4:I4");
                checkTitle(series, 1, "A4:A4");
                checkNames(series, 1, "B1:I1");
                checkBubbles(series, 1, "B5:I5");
                checkBubbleArray(series, 1);

                checkValues(series, 2, "B6:I6");
                checkTitle(series, 2, "A6:A6");
                checkNames(series, 2, "B1:I1");
                checkBubbles(series, 2, "B7:I7");
                checkBubbleArray(series, 2);

                checkValues(series, 3, "B8:I8");
                checkTitle(series, 3, "A8:A8");
                checkNames(series, 3, "B1:I1");
                checkBubbles(series, 3, "B9:I9");
                checkBubbleArray(series, 3);
            });

            it("9 rows switch Row/Col", () => {
                series = getDataForBubbleSeriesOperations(docModel, r("A1:I9"), 0, 1 - series[0].activeAxis, true);
                checkSeriesLength(series, 4);

                checkValues(series, 0, "B2:B9");
                checkNames(series, 0, "A2:A9");
                checkBubbles(series, 0, "C2:C9");
                checkBubbleArray(series, 0);
                checkTitle(series, 0, "B1:B1");

                checkValues(series, 1, "D2:D9");
                checkNames(series, 1, "A2:A9");
                checkBubbles(series, 1, "E2:E9");
                checkBubbleArray(series, 1);
                checkTitle(series, 1, "D1:D1");

                checkValues(series, 2, "F2:F9");
                checkNames(series, 2, "A2:A9");
                checkBubbles(series, 2, "G2:G9");
                checkBubbleArray(series, 2);
                checkTitle(series, 2, "F1:F1");

                checkValues(series, 3, "H2:H9");
                checkNames(series, 3, "A2:A9");
                checkBubbles(series, 3, "I2:I9");
                checkBubbleArray(series, 3);
                checkTitle(series, 3, "H1:H1");
            });
        });

    });
});
