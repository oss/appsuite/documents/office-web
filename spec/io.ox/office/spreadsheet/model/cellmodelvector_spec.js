/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { CellModel } from "@/io.ox/office/spreadsheet/model/cellmodel";
import { CellModelVector } from "@/io.ox/office/spreadsheet/model/cellmodelvector";

import { i, a } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/cellmodelvector", () => {

    // class CellModelVector --------------------------------------------------

    describe("class CellModelVector", () => {

        it("should be a class", () => {
            expect(CellModelVector).toBeClass();
        });

        describe("constructor", () => {
            it("should take a cell model", () => {
                const m1 = new CellModel(a("B3"));
                const colVector = new CellModelVector(m1, true);
                expect(colVector.index).toBe(1);
                const rowVector = new CellModelVector(m1, false);
                expect(rowVector.index).toBe(2);
            });
        });

        describe("method insertModel", () => {
            it("should exist", () => {
                expect(CellModelVector).toHaveMethod("insertModel");
            });
            it("should insert cell models into a column", () => {
                const m3 = new CellModel(a("B3"));
                const vector = new CellModelVector(m3, true);
                const m5 = new CellModel(a("B5"));
                vector.insertModel(m5);
                expect(Array.from(vector)).toEqual([m3, m5]);
                const m2 = new CellModel(a("B2"));
                vector.insertModel(m2);
                expect(Array.from(vector)).toEqual([m2, m3, m5]);
                const m4 = new CellModel(a("B4"));
                vector.insertModel(m4);
                expect(Array.from(vector)).toEqual([m2, m3, m4, m5]);
            });
            it("should insert cell models into a row", () => {
                const mB = new CellModel(a("B3"));
                const vector = new CellModelVector(mB, false);
                const mD = new CellModel(a("D3"));
                vector.insertModel(mD);
                expect(Array.from(vector)).toEqual([mB, mD]);
                const mA = new CellModel(a("A3"));
                vector.insertModel(mA);
                expect(Array.from(vector)).toEqual([mA, mB, mD]);
                const mC = new CellModel(a("C3"));
                vector.insertModel(mC);
                expect(Array.from(vector)).toEqual([mA, mB, mC, mD]);
            });
        });

        describe("method removeModel", () => {
            it("should exist", () => {
                expect(CellModelVector).toHaveMethod("removeModel");
            });
            it("should remove cell models from a column", () => {
                const m2 = new CellModel(a("B2"));
                const m3 = new CellModel(a("B3"));
                const m4 = new CellModel(a("B4"));
                const m5 = new CellModel(a("B5"));
                const vector = new CellModelVector(m2, true);
                vector.insertModel(m3);
                vector.insertModel(m4);
                vector.insertModel(m5);
                expect(Array.from(vector)).toEqual([m2, m3, m4, m5]);
                expect(vector.removeModel(m4)).toBeFalse();
                expect(Array.from(vector)).toEqual([m2, m3, m5]);
                expect(vector.removeModel(m2)).toBeFalse();
                expect(Array.from(vector)).toEqual([m3, m5]);
                expect(vector.removeModel(m5)).toBeFalse();
                expect(Array.from(vector)).toEqual([m3]);
                expect(vector.removeModel(m3)).toBeTrue();
                expect(Array.from(vector)).toEqual([]);
            });
            it("should remove cell models from a row", () => {
                const mA = new CellModel(a("A3"));
                const mB = new CellModel(a("B3"));
                const mC = new CellModel(a("C3"));
                const mD = new CellModel(a("D3"));
                const vector = new CellModelVector(mA, false);
                vector.insertModel(mB);
                vector.insertModel(mC);
                vector.insertModel(mD);
                expect(Array.from(vector)).toEqual([mA, mB, mC, mD]);
                expect(vector.removeModel(mC)).toBeFalse();
                expect(Array.from(vector)).toEqual([mA, mB, mD]);
                expect(vector.removeModel(mA)).toBeFalse();
                expect(Array.from(vector)).toEqual([mB, mD]);
                expect(vector.removeModel(mD)).toBeFalse();
                expect(Array.from(vector)).toEqual([mB]);
                expect(vector.removeModel(mB)).toBeTrue();
                expect(Array.from(vector)).toEqual([]);
            });
        });

        describe("method @@iterator", () => {
            it("should exist", () => {
                expect(CellModelVector).toHaveMethod(Symbol.iterator);
            });
            it("should create an iterator", () => {
                const m2 = new CellModel(a("B2"));
                const m3 = new CellModel(a("B3"));
                const vector = new CellModelVector(m2, true);
                vector.insertModel(m3);
                const it1 = vector[Symbol.iterator]();
                expect(it1).toBeIterator();
                expect(Array.from(it1)).toEqual([m2, m3]);
                // implicit call via `Array.from`
                expect(Array.from(vector)).toEqual([m2, m3]);
            });
        });

        describe("method yieldInterval", () => {
            it("should exist", () => {
                expect(CellModelVector).toHaveMethod("yieldInterval");
            });
            it("should create iterators for intervals", () => {
                const m2 = new CellModel(a("B2"));
                const m4 = new CellModel(a("B4"));
                const m6 = new CellModel(a("B6"));
                const m8 = new CellModel(a("B8"));
                const vector = new CellModelVector(m2, true);
                vector.insertModel(m4);
                vector.insertModel(m6);
                vector.insertModel(m8);
                const it1 = vector.yieldInterval(i("1:9"));
                expect(it1).toBeIterator();
                expect(Array.from(it1)).toEqual([m2, m4, m6, m8]);
                expect(Array.from(vector.yieldInterval(i("2:9")))).toEqual([m2, m4, m6, m8]);
                expect(Array.from(vector.yieldInterval(i("3:9")))).toEqual([m4, m6, m8]);
                expect(Array.from(vector.yieldInterval(i("4:9")))).toEqual([m4, m6, m8]);
                expect(Array.from(vector.yieldInterval(i("5:9")))).toEqual([m6, m8]);
                expect(Array.from(vector.yieldInterval(i("5:8")))).toEqual([m6, m8]);
                expect(Array.from(vector.yieldInterval(i("5:7")))).toEqual([m6]);
                expect(Array.from(vector.yieldInterval(i("5:6")))).toEqual([m6]);
                expect(Array.from(vector.yieldInterval(i("5:5")))).toEqual([]);
            });
            it("should create reverse iterators for intervals", () => {
                const m2 = new CellModel(a("B2"));
                const m4 = new CellModel(a("B4"));
                const m6 = new CellModel(a("B6"));
                const m8 = new CellModel(a("B8"));
                const vector = new CellModelVector(m2, true);
                vector.insertModel(m4);
                vector.insertModel(m6);
                vector.insertModel(m8);
                const it1 = vector.yieldInterval(i("1:9"), true);
                expect(it1).toBeIterator();
                expect(Array.from(it1)).toEqual([m8, m6, m4, m2]);
                expect(Array.from(vector.yieldInterval(i("2:9"), true))).toEqual([m8, m6, m4, m2]);
                expect(Array.from(vector.yieldInterval(i("3:9"), true))).toEqual([m8, m6, m4]);
                expect(Array.from(vector.yieldInterval(i("4:9"), true))).toEqual([m8, m6, m4]);
                expect(Array.from(vector.yieldInterval(i("5:9"), true))).toEqual([m8, m6]);
                expect(Array.from(vector.yieldInterval(i("5:8"), true))).toEqual([m8, m6]);
                expect(Array.from(vector.yieldInterval(i("5:7"), true))).toEqual([m6]);
                expect(Array.from(vector.yieldInterval(i("5:6"), true))).toEqual([m6]);
                expect(Array.from(vector.yieldInterval(i("5:5"), true))).toEqual([]);
            });
        });
    });
});
