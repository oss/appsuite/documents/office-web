/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { AttributedModel } from "@/io.ox/office/editframework/model/attributedmodel";
import { TableColumnModel } from "@/io.ox/office/spreadsheet/model/tablecolumnmodel";
import { NoneFilterMatcher, DiscreteFilterMatcher } from "@/io.ox/office/spreadsheet/model/tablefiltermatcher";

import { createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/tablecolumnmodel", () => {

    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertSheet(0, "Sheet1")
    ];

    // initialize test document
    let sheetModel;
    createSpreadsheetApp("ooxml", OPERATIONS).done(docApp => {
        sheetModel = docApp.docModel.getSheetModel(0);
    });

    // class TableColumnModel -------------------------------------------------

    describe("class TableColumnModel", () => {

        it("should subclass AttributedModel", () => {
            expect(TableColumnModel).toBeSubClassOf(AttributedModel);
        });

        describe("method isButtonVisible", () => {
            it("should exist", () => {
                expect(TableColumnModel).toHaveMethod("isButtonVisible");
            });
            it("should return whether the column shows its filter button", () => {
                const model = new TableColumnModel(sheetModel);
                expect(model.isButtonVisible()).toBeTrue();
                model.setAttributes({ filter: { hideButton: true } });
                expect(model.isButtonVisible()).toBeFalse();
                model.setAttributes({ filter: { hideButton: null } });
                expect(model.isButtonVisible()).toBeTrue();
            });
        });

        describe("method isFiltered", () => {
            it("should exist", () => {
                expect(TableColumnModel).toHaveMethod("isFiltered");
            });
            it("should return whether the column is filtered", () => {
                const model = new TableColumnModel(sheetModel);
                expect(model.isFiltered()).toBeFalse();
                model.setAttributes({ filter: { type: "discrete" } });
                expect(model.isFiltered()).toBeTrue();
                model.setAttributes({ filter: { type: null } });
                expect(model.isFiltered()).toBeFalse();
            });
        });

        describe("method getDiscreteEntries", () => {
            it("should exist", () => {
                expect(TableColumnModel).toHaveMethod("getDiscreteEntries");
            });
            it("should return the discrete filter entries", () => {
                const model = new TableColumnModel(sheetModel);
                expect(model.isFiltered()).toBeFalse();
                expect(model.getDiscreteEntries()).toBeUndefined();
                model.setAttributes({ filter: { type: "discrete", entries: ["a", "TRUE", " B ", { t: "date", v: "2000" }] } });
                expect(model.isFiltered()).toBeTrue();
                expect(model.getDiscreteEntries()).toEqual(["a", "true", "b", { t: "date", v: "2000" }]);
                model.setAttributes({ filter: { type: "none" } });
                expect(model.getDiscreteEntries()).toBeUndefined();
            });
        });

        describe("method isSorted", () => {
            it("should exist", () => {
                expect(TableColumnModel).toHaveMethod("isSorted");
            });
            it("should return whether the column is sorted", () => {
                const model = new TableColumnModel(sheetModel);
                expect(model.isSorted()).toBeFalse();
                model.setAttributes({ sort: { type: "value" } });
                expect(model.isSorted()).toBeTrue();
                model.setAttributes({ sort: { type: null } });
                expect(model.isSorted()).toBeFalse();
            });
        });

        describe("method isDescending", () => {
            it("should exist", () => {
                expect(TableColumnModel).toHaveMethod("isDescending");
            });
            it("should return whether the column is sorted in descending order", () => {
                const model = new TableColumnModel(sheetModel, { sort: { type: "value" } });
                expect(model.isDescending()).toBeFalse();
                model.setAttributes({ sort: { descending: true } });
                expect(model.isDescending()).toBeTrue();
                model.setAttributes({ sort: { descending: null } });
                expect(model.isDescending()).toBeFalse();
            });
        });

        describe("method isRefreshable", () => {
            it("should exist", () => {
                expect(TableColumnModel).toHaveMethod("isRefreshable");
            });
            it("should return whether the column is refreshable", () => {
                const model = new TableColumnModel(sheetModel);
                expect(model.isRefreshable()).toBeFalse();
                model.setAttributes({ filter: { type: "discrete" } });
                expect(model.isRefreshable()).toBeTrue();
                model.setAttributes({ sort: { type: "value" } });
                expect(model.isRefreshable()).toBeTrue();
                model.setAttributes({ filter: { type: null } });
                expect(model.isRefreshable()).toBeTrue();
                model.setAttributes({ sort: { type: null } });
                expect(model.isRefreshable()).toBeFalse();
            });
        });

        describe("method getFilterMatcher", () => {
            it("should exist", () => {
                expect(TableColumnModel).toHaveMethod("getFilterMatcher");
            });
            it("should return none filter matcher", () => {
                const model = new TableColumnModel(sheetModel);
                const matcher = model.getFilterMatcher();
                expect(matcher).toBeInstanceOf(NoneFilterMatcher);
                expect(model.getFilterMatcher()).toBe(matcher);
            });
            it("should return discrete filter matcher", () => {
                const model1 = new TableColumnModel(sheetModel, { filter: { type: "discrete" } });
                const matcher = model1.getFilterMatcher();
                expect(matcher).toBeInstanceOf(DiscreteFilterMatcher);
                expect(model1.getFilterMatcher()).toBe(matcher);
            });
        });
    });
});
