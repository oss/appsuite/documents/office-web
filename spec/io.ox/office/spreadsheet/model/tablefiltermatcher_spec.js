/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { TableFilterMatcher, NoneFilterMatcher, DiscreteFilterMatcher } from "@/io.ox/office/spreadsheet/model/tablefiltermatcher";

import { dts, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/tablefiltermatcher", () => {

    const OPERATIONS = [
        op.changeConfig({ document: { fv: 2, cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertSheet(0, "Sheet1")
    ];

    // initialize test document
    let docModel, formatter;
    createSpreadsheetApp("ooxml", OPERATIONS).done(docApp => {
        docModel = docApp.docModel;
        formatter = docModel.numberFormatter;
    });

    // class TableFilterMatcher -----------------------------------------------

    describe("class TableFilterMatcher", () => {

        it("should subclass ModelObject", () => {
            expect(TableFilterMatcher).toBeSubClassOf(ModelObject);
        });

        describe("method active", () => {
            it("should exist", () => {
                expect(TableFilterMatcher).toHaveMethod("active");
            });
        });

        describe("method test", () => {
            it("should exist", () => {
                expect(TableFilterMatcher).toHaveMethod("test");
            });
        });
    });

    // class NoneFilterMatcher ------------------------------------------------

    describe("class NoneFilterMatcher", () => {

        it("should subclass TableFilterMatcher", () => {
            expect(NoneFilterMatcher).toBeSubClassOf(TableFilterMatcher);
        });

        describe("method active", () => {
            it("should return false", () => {
                const matcher = new NoneFilterMatcher(docModel);
                expect(matcher.active()).toBeFalse();
            });
        });

        describe("method test", () => {
            it("should return true", () => {
                const matcher = new NoneFilterMatcher(docModel);
                expect(matcher.test(0, formatter.standardFormat, "")).toBeTrue();
                expect(matcher.test("abc", formatter.standardFormat, "abc")).toBeTrue();
            });
        });
    });

    // class DiscreteFilterMatcher --------------------------------------------

    describe("class DiscreteFilterMatcher", () => {

        it("should subclass TableFilterMatcher", () => {
            expect(DiscreteFilterMatcher).toBeSubClassOf(TableFilterMatcher);
        });

        describe("method active", () => {
            it("should return false for missing/invalid entries", () => {
                const matcher1 = new DiscreteFilterMatcher(docModel, []);
                expect(matcher1.active()).toBeFalse();
                const matcher2 = new DiscreteFilterMatcher(docModel, [1, true, null, [], { t: "__invalid__", v: "" }]);
                expect(matcher2.active()).toBeFalse();
            });
            it("should return true for valid entries", () => {
                const matcher1 = new DiscreteFilterMatcher(docModel, ["a"]);
                expect(matcher1.active()).toBeTrue();
                const matcher2 = new DiscreteFilterMatcher(docModel, [{ t: "date", v: "2000" }]);
                expect(matcher2.active()).toBeTrue();
                const matcher3 = new DiscreteFilterMatcher(docModel, ["a", 1, true, { t: "date", v: "2000" }]);
                expect(matcher3.active()).toBeTrue();
            });
        });

        describe("method test", () => {
            it("should match display strings", () => {
                const matcher = new DiscreteFilterMatcher(docModel, ["abc", "2", "$4", "falsch"]);
                const format1 = formatter.standardFormat;
                const format2 = formatter.getCurrencyFormat();
                expect(matcher.test("abc", format1, "abc")).toBeTrue();
                expect(matcher.test("ABC", format1, "ABC")).toBeTrue();
                expect(matcher.test(" ABC ", format1, " ABC ")).toBeTrue();
                expect(matcher.test("ab", format1, "ab")).toBeFalse();
                expect(matcher.test("ab c", format1, "ab c")).toBeFalse();
                expect(matcher.test(2, format1, "2")).toBeTrue();
                expect(matcher.test(2, format2, "$2")).toBeFalse();
                expect(matcher.test(4, format1, "4")).toBeFalse();
                expect(matcher.test(4, format2, "$4")).toBeTrue();
                expect(matcher.test(false, format1, "FALSE")).toBeFalse();
                expect(matcher.test(false, format2, "FALSCH")).toBeTrue();
                expect(matcher.test(true, format1, "TRUE")).toBeFalse();
                expect(matcher.test(true, format2, "WAHR")).toBeFalse();
            });
            it("should match date groups", () => {
                const matcher = new DiscreteFilterMatcher(docModel, [
                    { t: "date", v: "1999" },
                    { t: "date", v: "2000 6" },
                    { t: "date", v: "2000 12 15" },
                    { t: "date", v: "2000 12 31 8" },
                    { t: "date", v: "2000 12 31 16 20" },
                    { t: "date", v: "2000 12 31 16 40 30" }
                ]);
                const format1 = formatter.standardFormat;
                const format2 = formatter.getSystemDateFormat();
                const format3 = formatter.getSystemTimeFormat();
                const format4 = formatter.getSystemDateTimeFormat();
                // date and date/time only
                expect(matcher.test(dts("1999-01-01"), format1, "36161")).toBeFalse();
                expect(matcher.test(dts("1999-01-01"), format2, "01.01.1999")).toBeTrue();
                expect(matcher.test(dts("1999-01-01"), format3, "00:00:00")).toBeFalse();
                expect(matcher.test(dts("1999-01-01"), format4, "01.01.1999 00:00:00")).toBeTrue();
                expect(matcher.test("01.01.1999", format2, "01.01.1999")).toBeFalse();
                // every date in year 1999
                expect(matcher.test(dts("1999-07-31"), format2, "31.07.1999")).toBeTrue();
                expect(matcher.test(dts("1999-12-31 23:59:59.999"), format4, "31.12.1999 23:23:59")).toBeTrue();
                // every day in June 2000
                expect(matcher.test(dts("2000-05-31 23:59:59.999"), format4, "31.05.2000 23:59:59")).toBeFalse();
                expect(matcher.test(dts("2000-06-01"), format2, "01.06.2000")).toBeTrue();
                expect(matcher.test(dts("2000-06-15"), format2, "15.06.2000")).toBeTrue();
                expect(matcher.test(dts("2000-06-30 23:59:59.999"), format4, "30.06.2000 23:59:59")).toBeTrue();
                expect(matcher.test(dts("2000-07-01"), format2, "01.07.2000")).toBeFalse();
                // every time of 2000-12-15
                expect(matcher.test(dts("2000-12-14 23:59:59.999"), format4, "14.12.2000 23:59:59")).toBeFalse();
                expect(matcher.test(dts("2000-12-15"), format2, "15.12.2000")).toBeTrue();
                expect(matcher.test(dts("2000-12-15 12:00:00"), format2, "15.12.2000")).toBeTrue();
                expect(matcher.test(dts("2000-12-15 12:00:00"), format4, "15.12.2000 12:00:00")).toBeTrue();
                expect(matcher.test(dts("2000-12-15 23:59:59.999"), format4, "15.12.2000 23:59:59")).toBeTrue();
                expect(matcher.test(dts("2000-12-16"), format2, "16.12.2000")).toBeFalse();
                // every minute of 2000-12-31 8am
                expect(matcher.test(dts("2000-12-31 07:59:59.999"), format4, "31.12.2000 07:59:59")).toBeFalse();
                expect(matcher.test(dts("2000-12-31 08:00:00"), format4, "31.12.2000 08:00:00")).toBeTrue();
                expect(matcher.test(dts("2000-12-31 08:30:00"), format4, "31.12.2000 08:30:00")).toBeTrue();
                expect(matcher.test(dts("2000-12-31 08:59:59.999"), format4, "31.12.2000 08:59:59")).toBeTrue();
                expect(matcher.test(dts("2000-12-31 09:00:00"), format4, "31.12.2000 09:00:00")).toBeFalse();
                // every second of 2000-12-31 16:20
                expect(matcher.test(dts("2000-12-31 16:19:59.999"), format4, "31.12.2000 16:19:59")).toBeFalse();
                expect(matcher.test(dts("2000-12-31 16:20:00"), format4, "31.12.2000 16:20:00")).toBeTrue();
                expect(matcher.test(dts("2000-12-31 16:20:30"), format4, "31.12.2000 16:20:30")).toBeTrue();
                expect(matcher.test(dts("2000-12-31 16:20:59.999"), format4, "31.12.2000 16:20:59")).toBeTrue();
                expect(matcher.test(dts("2000-12-31 16:21:00"), format4, "31.12.2000 16:21:00")).toBeFalse();
                // every fractional second of 2000-12-31 16:40:30
                expect(matcher.test(dts("2000-12-31 16:40:29.999"), format4, "31.12.2000 16:40:29")).toBeFalse();
                expect(matcher.test(dts("2000-12-31 16:40:30"), format4, "31.12.2000 16:40:30")).toBeTrue();
                expect(matcher.test(dts("2000-12-31 16:40:30.001"), format4, "31.12.2000 16:40:30")).toBeTrue();
                expect(matcher.test(dts("2000-12-31 16:40:30.5"), format4, "31.12.2000 16:40:30")).toBeTrue();
                expect(matcher.test(dts("2000-12-31 16:40:30.999"), format4, "31.12.2000 16:40:30")).toBeTrue();
                expect(matcher.test(dts("2000-12-31 16:40:31"), format4, "31.12.2000 16:40:31")).toBeFalse();
            });
            it("should match display string instead of date group", () => {
                const matcher = new DiscreteFilterMatcher(docModel, [{ t: "date", v: "1999" }, "31.12.2000"]);
                const format = formatter.getSystemDateFormat();
                expect(matcher.test(dts("1999-01-01"), format, "01.01.1999")).toBeTrue();
                expect(matcher.test(dts("1999-12-31"), format, "31.12.1999")).toBeTrue();
                expect(matcher.test(dts("2000-01-01"), format, "01.01.2000")).toBeFalse();
                expect(matcher.test(dts("2000-12-31"), format, "31.12.2000")).toBeTrue(); // matches display string
                expect(matcher.test("31.12.2000", format, "31.12.2000")).toBeTrue(); // matches display string
            });
        });
    });
});
