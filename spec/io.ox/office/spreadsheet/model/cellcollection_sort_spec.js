/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { CellCollection } from "@/io.ox/office/spreadsheet/model/cellcollection";

import { r, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

const { NULL, DIV0 } = ErrorCode;

// tests ======================================================================

describe("module spreadsheet/model/cellcollection", () => {

    // the values to be sorted
    const SORT_TEST_VALUES = [
        [null, null, true],
        [3,    "c",  null],
        [null, null, null],
        [null, "c",  "1"],
        [1,    null, NULL],
        [5,    "a",  DIV0],
        [3,    "b",  { v: 1, f: "F8+$F$8" }]
    ];

    // the values around the sorted values that MUST NOT be sorted
    const SORT_FRAME_VALUES = [
        ["A1", "B1", "C1", "D1", "E1"],
        ["A2", null, null, null, "E2"],
        ["A3", null, null, null, "E3"],
        ["A4", null, null, null, "E4"],
        ["A5", null, null, null, "E5"],
        ["A6", null, null, null, "E6"],
        ["A7", null, null, null, "E7"],
        ["A8", null, null, null, "E8"],
        ["A9", "B9", "C9", "D9", "E9"]
    ];

    // the operations to be applied by the document model
    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertSheet(0, "Sheet1"),
        op.changeCells(0, "A1", SORT_FRAME_VALUES),
        op.changeCells(0, "B2", SORT_TEST_VALUES)
    ];

    // initialize test document
    let docModel, sheetTester, sheetModel, cellCollection;
    createSpreadsheetApp("ooxml", OPERATIONS).done(function (docApp) {
        docModel = docApp.docModel;
        sheetTester = docModel.expect.sheet(0);
        sheetModel = sheetTester.sheetModel;
        cellCollection = sheetModel.cellCollection;
    });

    function expectRangeValues(expected) {
        for (const address of r("A1:E9").addresses()) {
            const exp = SORT_FRAME_VALUES[address.r][address.c];
            if (exp) {
                const value = cellCollection.getValue(address);
                expect(value).toBe(exp);
            }
        }
        for (const address of r("B2:D8").addresses()) {
            const exp = expected[address.r - 1][address.c - 1];
            const expV = (exp && exp.f) ? exp.v : exp;
            const expF = (exp && exp.f) ? exp.f : null;
            expect(cellCollection.getValue(address)).toBe(expV);
            expect(cellCollection.getFormula(address, "op")).toBe(expF);
        }
    }

    function describeSortTest(title, indexes, vertical, expected) {
        describe("generate sort operations (" + title + ")", () => {
            it("should sort the cells", async () => {
                await sheetModel.createAndApplyOperations(generator => {
                    const sortRules = indexes.map(function (index) {
                        return { index: Math.abs(index), descending: index < 0 };
                    });
                    return cellCollection.generateSortOperations(generator, r("B2:D8"), vertical, sortRules);
                });
                expectRangeValues(expected);
                await docModel.undoManager.undo();
                expectRangeValues(SORT_TEST_VALUES);
            });
        });
    }

    // class CellCollection ---------------------------------------------------

    describe("class CellCollection", () => {

        describe("method generateSortOperations", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("generateSortOperations");
            });

            describeSortTest("test V1: column B ascending", [1], true, [
                [1,    null, NULL],
                [3,    "c",  null],
                [3,    "b",  { v: 1, f: "F4+$F$8" }],
                [5,    "a",  DIV0],
                [null, null, true],
                [null, null, null],
                [null, "c",  "1"]
            ]);

            describeSortTest("test V2: column B descending", [-1], true, [
                [5,    "a",  DIV0],
                [3,    "c",  null],
                [3,    "b",  { v: 1, f: "F4+$F$8" }],
                [1,    null, NULL],
                [null, null, true],
                [null, null, null],
                [null, "c",  "1"]
            ]);

            describeSortTest("test V3: column C ascending", [2], true, [
                [5,    "a",  DIV0],
                [3,    "b",  { v: 1, f: "F3+$F$8" }],
                [3,    "c",  null],
                [null, "c",  "1"],
                [null, null, true],
                [null, null, null],
                [1,    null, NULL]
            ]);

            describeSortTest("test V4: column C descending", [-2], true, [
                [3,    "c",  null],
                [null, "c",  "1"],
                [3,    "b",  { v: 1, f: "F4+$F$8" }],
                [5,    "a",  DIV0],
                [null, null, true],
                [null, null, null],
                [1,    null, NULL]
            ]);

            describeSortTest("test V5: column D ascending", [3], true, [
                [3,    "b",  { v: 1, f: "F2+$F$8" }],
                [null, "c",  "1"],
                [null, null, true],
                [1,    null, NULL],
                [5,    "a",  DIV0],
                [3,    "c",  null],
                [null, null, null]
            ]);

            describeSortTest("test V6: column D descending", [-3], true, [
                [5,    "a",  DIV0],
                [1,    null, NULL],
                [null, null, true],
                [null, "c",  "1"],
                [3,    "b",  { v: 1, f: "F6+$F$8" }],
                [3,    "c",  null],
                [null, null, null]
            ]);

            describeSortTest("test V7: column B asc, column C asc", [1, 2], true, [
                [1,    null, NULL],
                [3,    "b",  { v: 1, f: "F3+$F$8" }],
                [3,    "c",  null],
                [5,    "a",  DIV0],
                [null, "c",  "1"],
                [null, null, true],
                [null, null, null]
            ]);

            describeSortTest("test V8: column B asc, column C desc", [1, -2], true, [
                [1,    null, NULL],
                [3,    "c",  null],
                [3,    "b",  { v: 1, f: "F4+$F$8" }],
                [5,    "a",  DIV0],
                [null, "c",  "1"],
                [null, null, true],
                [null, null, null]
            ]);

            describeSortTest("test V9: column B desc, column C asc", [-1, 2], true, [
                [5,    "a",  DIV0],
                [3,    "b",  { v: 1, f: "F3+$F$8" }],
                [3,    "c",  null],
                [1,    null, NULL],
                [null, "c",  "1"],
                [null, null, true],
                [null, null, null]
            ]);

            describeSortTest("test V10: column B desc, column C desc", [-1, -2], true, [
                [5,    "a",  DIV0],
                [3,    "c",  null],
                [3,    "b",  { v: 1, f: "F4+$F$8" }],
                [1,    null, NULL],
                [null, "c",  "1"],
                [null, null, true],
                [null, null, null]
            ]);

            describeSortTest("test H1: row 4 asc", [4], false, _.zip(
                [true, null, null, "1",  NULL, DIV0, { v: 1, f: "D8+$F$8" }],
                [null, "c",  null, "c",  null, "a", "b"],
                [null, 3,    null, null, 1,    5,   3]
            ));

            describeSortTest("test H2: row 4 desc", [-4], false, _.zip(
                [null, "c",  null, "c",  null, "a", "b"],
                [true, null, null, "1",  NULL, DIV0, { v: 1, f: "E8+$F$8" }],
                [null, 3,    null, null, 1,    5,   3]
            ));
        });
    });
});
