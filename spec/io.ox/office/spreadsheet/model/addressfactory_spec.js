/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Interval } from "@/io.ox/office/spreadsheet/utils/interval";
import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { Range } from "@/io.ox/office/spreadsheet/utils/range";
import { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";

import { i, a, r, ia, aa, ra } from "~/spreadsheet/sheethelper";

// singletons =================================================================

const factory = new AddressFactory(256, 65536);

// tests ======================================================================

describe("module spreadsheet/model/addressfactory", () => {

    // class AddressFactory ---------------------------------------------------

    describe("class AddressFactory", () => {

        it("should exist", () => {
            expect(AddressFactory).toBeFunction();
        });

        describe("property maxCol", () => {
            it("should exist", () => {
                expect(factory.maxCol).toBe(255);
            });
        });

        describe("property maxRow", () => {
            it("should exist", () => {
                expect(factory.maxRow).toBe(65535);
            });
        });

        describe("property colCount", () => {
            it("should exist", () => {
                expect(factory.colCount).toBe(256);
            });
        });

        describe("property rowCount", () => {
            it("should exist", () => {
                expect(factory.rowCount).toBe(65536);
            });
        });

        describe("method getMaxIndex", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getMaxIndex");
            });
            it("should return the maximum index", () => {
                expect(factory.getMaxIndex(false)).toBe(65535);
                expect(factory.getMaxIndex(true)).toBe(255);
            });
        });

        describe("method getSize", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getSize");
            });
            it("should return the sheet size", () => {
                expect(factory.getSize(false)).toBe(65536);
                expect(factory.getSize(true)).toBe(256);
            });
        });

        describe("method updateSheetSize", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("updateSheetSize");
            });
            it("should return the sheet size", () => {
                const factory2 = new AddressFactory(256, 65536);
                expect(factory2.getSize(false)).toBe(65536);
                expect(factory2.getSize(true)).toBe(256);
                factory2.updateSheetSize(16384, 1048576);
                expect(factory2.getSize(false)).toBe(1048576);
                expect(factory2.getSize(true)).toBe(16384);
            });
        });

        describe("method parseInterval", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("parseInterval");
            });
            it("should parse row interval", () => {
                expect(factory.parseInterval("2:3", false)).toEqual(i("2:3"));
                expect(factory.parseInterval("3:2", false)).toEqual(i("2:3"));
                expect(factory.parseInterval("2", false)).toEqual(i("2"));
                expect(factory.parseInterval("1:65536", false)).toEqual(i("1:65536"));
            });
            it("should parse column interval", () => {
                expect(factory.parseInterval("B:C", true)).toEqual(i("B:C"));
                expect(factory.parseInterval("C:B", true)).toEqual(i("B:C"));
                expect(factory.parseInterval("B", true)).toEqual(i("B"));
                expect(factory.parseInterval("A:IV", true)).toEqual(i("A:IV"));
            });
            it("should return undefined for invalid intervals", () => {
                expect(factory.parseInterval("", false)).toBeUndefined();
                expect(factory.parseInterval("B:C", false)).toBeUndefined();
                expect(factory.parseInterval("1:65537", false)).toBeUndefined();
                expect(factory.parseInterval("65537", false)).toBeUndefined();
                expect(factory.parseInterval("", true)).toBeUndefined();
                expect(factory.parseInterval("2:3", true)).toBeUndefined();
                expect(factory.parseInterval("A:IW", true)).toBeUndefined();
                expect(factory.parseInterval("IW", true)).toBeUndefined();
            });
        });

        describe("method parseIntervalList", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("parseIntervalList");
            });
            it("should parse row intervals", () => {
                expect(factory.parseIntervalList("2:3 3:2 2 1:65536", false)).toEqual(ia("2:3 2:3 2 1:65536"));
                expect(factory.parseIntervalList("1 B B:C 2:C B:3 4", false)).toEqual(ia("1 4"));
            });
            it("should parse column intervals", () => {
                expect(factory.parseIntervalList("B:C C:B B A:IV", true)).toEqual(ia("B:C B:C B A:IV"));
                expect(factory.parseIntervalList("A 2 2:3 B:3 2:C D", true)).toEqual(ia("A D"));
            });
            it("should return undefined for empty interval list", () => {
                expect(factory.parseIntervalList("", false)).toBeUndefined();
                expect(factory.parseIntervalList("B:C 1:65537 65537", false)).toBeUndefined();
                expect(factory.parseIntervalList("2,3", false)).toBeUndefined();
                expect(factory.parseIntervalList("", true)).toBeUndefined();
                expect(factory.parseIntervalList("2:3 A:IW IW", true)).toBeUndefined();
                expect(factory.parseIntervalList("B,C", true)).toBeUndefined();
            });
        });

        describe("method isValidInterval", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("isValidInterval");
            });
            it("should return true for valid row intervals", () => {
                expect(factory.isValidInterval(i("1"), false)).toBeTrue();
                expect(factory.isValidInterval(i("2:3"), false)).toBeTrue();
                expect(factory.isValidInterval(i("1:65536"), false)).toBeTrue();
            });
            it("should return false for invalid row intervals", () => {
                expect(factory.isValidInterval(new Interval(-1, 0), false)).toBeFalse();
                expect(factory.isValidInterval(new Interval(0, -1), false)).toBeFalse();
                expect(factory.isValidInterval(new Interval(2, 1), false)).toBeFalse();
                expect(factory.isValidInterval(i("1:65537"), false)).toBeFalse();
                expect(factory.isValidInterval(i("65537"), false)).toBeFalse();
            });
            it("should return true for valid column intervals", () => {
                expect(factory.isValidInterval(i("A"), true)).toBeTrue();
                expect(factory.isValidInterval(i("B:C"), true)).toBeTrue();
                expect(factory.isValidInterval(i("A:IV"), true)).toBeTrue();
            });
            it("should return false for invalid column intervals", () => {
                expect(factory.isValidInterval(new Interval(-1, 0), true)).toBeFalse();
                expect(factory.isValidInterval(new Interval(0, -1), true)).toBeFalse();
                expect(factory.isValidInterval(new Interval(2, 1), true)).toBeFalse();
                expect(factory.isValidInterval(i("A:IW"), true)).toBeFalse();
                expect(factory.isValidInterval(i("IW"), true)).toBeFalse();
            });
        });

        describe("method getEndInterval", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getEndInterval");
            });
            it("should return a row interval", () => {
                expect(factory.getEndInterval(0, false)).toEqual(i("1:65536"));
                expect(factory.getEndInterval(1, false)).toEqual(i("2:65536"));
                expect(factory.getEndInterval(65535, false)).toEqual(i("65536"));
            });
            it("should return a column interval", () => {
                expect(factory.getEndInterval(0, true)).toEqual(i("A:IV"));
                expect(factory.getEndInterval(1, true)).toEqual(i("B:IV"));
                expect(factory.getEndInterval(255, true)).toEqual(i("IV"));
            });
        });

        describe("method getFullInterval", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getFullInterval");
            });
            it("should return a row interval", () => {
                expect(factory.getFullInterval(false)).toEqual(i("1:65536"));
            });
            it("should return a column interval", () => {
                expect(factory.getFullInterval(true)).toEqual(i("A:IV"));
            });
        });

        describe("method parseAddress", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("parseAddress");
            });
            it("should parse cell addresses", () => {
                expect(factory.parseAddress("A1")).toEqual(a("A1"));
                expect(factory.parseAddress("B1")).toEqual(a("B1"));
                expect(factory.parseAddress("A2")).toEqual(a("A2"));
                expect(factory.parseAddress("B2")).toEqual(a("B2"));
                expect(factory.parseAddress("IV1")).toEqual(a("IV1"));
                expect(factory.parseAddress("A65536")).toEqual(a("A65536"));
                expect(factory.parseAddress("IV65536")).toEqual(a("IV65536"));
            });
            it("should return undefined for invalid addresses", () => {
                expect(factory.parseAddress("")).toBeUndefined();
                expect(factory.parseAddress("1A")).toBeUndefined();
                expect(factory.parseAddress("IW1")).toBeUndefined();
                expect(factory.parseAddress("A65537")).toBeUndefined();
                expect(factory.parseAddress("IW65537")).toBeUndefined();
            });
        });

        describe("method parseAddressList", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("parseAddressList");
            });
            it("should parse cell addresses", () => {
                expect(factory.parseAddressList("A1 IV1 A65536 IV65536")).toEqual(aa("A1 IV1 A65536 IV65536"));
                expect(factory.parseAddressList("A1 1A IW65537 B2")).toEqual(aa("A1 B2"));
            });
            it("should return undefined for invalid addresses", () => {
                expect(factory.parseAddressList("")).toBeUndefined();
                expect(factory.parseAddressList("1A IW1 A65537 IW65537")).toBeUndefined();
                expect(factory.parseAddressList("A1,B2")).toBeUndefined();
            });
        });

        describe("method isValidAddress", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("isValidAddress");
            });
            it("should return true for valid addresses", () => {
                expect(factory.isValidAddress(a("A1"))).toBeTrue();
                expect(factory.isValidAddress(a("B1"))).toBeTrue();
                expect(factory.isValidAddress(a("A2"))).toBeTrue();
                expect(factory.isValidAddress(a("B2"))).toBeTrue();
                expect(factory.isValidAddress(a("IV1"))).toBeTrue();
                expect(factory.isValidAddress(a("A65536"))).toBeTrue();
                expect(factory.isValidAddress(a("IV65536"))).toBeTrue();
            });
            it("should return false for invalid addresses", () => {
                expect(factory.isValidAddress(new Address(-1, 1))).toBeFalse();
                expect(factory.isValidAddress(new Address(1, -1))).toBeFalse();
                expect(factory.isValidAddress(new Address(-1, -1))).toBeFalse();
                expect(factory.isValidAddress(a("IW2"))).toBeFalse();
                expect(factory.isValidAddress(a("B65537"))).toBeFalse();
                expect(factory.isValidAddress(a("IW65537"))).toBeFalse();
            });
        });

        describe("method getClampedAddress", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getClampedAddress");
            });
            it("should return a clone of a valid address", () => {
                const a1 = a("A1");
                const a2 = factory.getClampedAddress(a1);
                expect(a2).not.toBe(a1);
                expect(a2).toEqual(a1);
            });
            it("should clamp invalid addresses", () => {
                expect(factory.getClampedAddress(new Address(-1, 1))).toEqual(a("A2"));
                expect(factory.getClampedAddress(new Address(1, -1))).toEqual(a("B1"));
                expect(factory.getClampedAddress(new Address(-1, -1))).toEqual(a("A1"));
                expect(factory.getClampedAddress(a("IW2"))).toEqual(a("IV2"));
                expect(factory.getClampedAddress(a("B65537"))).toEqual(a("B65536"));
                expect(factory.getClampedAddress(a("IW65537"))).toEqual(a("IV65536"));
                expect(factory.getClampedAddress(new Address(-1e6, 1e6))).toEqual(a("A65536"));
                expect(factory.getClampedAddress(new Address(1e6, -1e6))).toEqual(a("IV1"));
            });
        });

        describe("method parseRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("parseRange");
            });
            it("should parse cell range addresses", () => {
                expect(factory.parseRange("A1")).toEqual(r("A1"));
                expect(factory.parseRange("B2:C3")).toEqual(r("B2:C3"));
                expect(factory.parseRange("C2:B3")).toEqual(r("B2:C3"));
                expect(factory.parseRange("B3:C2")).toEqual(r("B2:C3"));
                expect(factory.parseRange("C3:B2")).toEqual(r("B2:C3"));
                expect(factory.parseRange("A1:IV1")).toEqual(r("A1:IV1"));
                expect(factory.parseRange("A1:A65536")).toEqual(r("A1:A65536"));
                expect(factory.parseRange("A1:IV65536")).toEqual(r("A1:IV65536"));
            });
            it("should return undefined for invalid cell range addresses", () => {
                expect(factory.parseRange("")).toBeUndefined();
                expect(factory.parseRange("1A")).toBeUndefined();
                expect(factory.parseRange("A1:1A")).toBeUndefined();
                expect(factory.parseRange("A1:IW1")).toBeUndefined();
                expect(factory.parseRange("IW1")).toBeUndefined();
                expect(factory.parseRange("A65537")).toBeUndefined();
                expect(factory.parseRange("A1:A65537")).toBeUndefined();
                expect(factory.parseRange("IW65537")).toBeUndefined();
                expect(factory.parseRange("A1:IW65537")).toBeUndefined();
                expect(factory.parseRange("IW65537:A1")).toBeUndefined();
            });
        });

        describe("method parseRangeList", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("parseRangeList");
            });
            it("should parse cell range addresses", () => {
                expect(factory.parseRangeList("A1 C3:B2 A1:IV65536")).toEqual(ra("A1 B2:C3 A1:IV65536"));
                expect(factory.parseRangeList("A1 2B IW2 C3")).toEqual(ra("A1 C3"));
            });
            it("should return undefined for invalid cell range addresses", () => {
                expect(factory.parseRangeList("")).toBeUndefined();
                expect(factory.parseRangeList("1A IW1 A1:IW65537")).toBeUndefined();
            });
        });

        describe("method isValidRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("isValidRange");
            });
            it("should return true for valid ranges", () => {
                expect(factory.isValidRange(r("A1"))).toBeTrue();
                expect(factory.isValidRange(r("B2:C3"))).toBeTrue();
                expect(factory.isValidRange(r("B1:C65536"))).toBeTrue();
                expect(factory.isValidRange(r("A2:IV3"))).toBeTrue();
                expect(factory.isValidRange(r("A1:IV65536"))).toBeTrue();
            });
            it("should return false for invalid ranges", () => {
                expect(factory.isValidRange(new Range(new Address(-1, 1), a("C3")))).toBeFalse();
                expect(factory.isValidRange(new Range(new Address(1, -1), a("C3")))).toBeFalse();
                expect(factory.isValidRange(new Range(new Address(-1, -1), a("C3")))).toBeFalse();
                expect(factory.isValidRange(r("A1:IW2"))).toBeFalse();
                expect(factory.isValidRange(r("A1:B65537"))).toBeFalse();
                expect(factory.isValidRange(r("A1:IW65537"))).toBeFalse();
                expect(factory.isValidRange(new Range(a("C2"), a("B3")))).toBeFalse();
                expect(factory.isValidRange(new Range(a("B3"), a("C2")))).toBeFalse();
                expect(factory.isValidRange(new Range(a("C3"), a("B2")))).toBeFalse();
            });
        });

        describe("method isSheetRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("isSheetRange");
            });
            it("should return true for the sheet range", () => {
                expect(factory.isSheetRange(r("A1:IV65536"))).toBeTrue();
            });
            it("should return false for other ranges", () => {
                expect(factory.isSheetRange(r("A2:IV65536"))).toBeFalse();
                expect(factory.isSheetRange(r("B1:IV65536"))).toBeFalse();
                expect(factory.isSheetRange(r("A1:IU65536"))).toBeFalse();
                expect(factory.isSheetRange(r("A1:IV65535"))).toBeFalse();
            });
        });

        describe("method getSheetRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getSheetRange");
            });
            it("should return the sheet range", () => {
                expect(factory.getSheetRange()).toEqual(r("A1:IV65536"));
            });
        });

        describe("method isColRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("isColRange");
            });
            it("should return true for column ranges", () => {
                expect(factory.isColRange(r("A1:A65536"))).toBeTrue();
                expect(factory.isColRange(r("B1:C65536"))).toBeTrue();
                expect(factory.isColRange(r("A1:IV65536"))).toBeTrue();
            });
            it("should return false for other ranges", () => {
                expect(factory.isColRange(r("A2:A65536"))).toBeFalse();
                expect(factory.isColRange(r("A1:A65535"))).toBeFalse();
                expect(factory.isColRange(r("A2:A65535"))).toBeFalse();
                expect(factory.isColRange(r("A1:IV1"))).toBeFalse();
            });
        });

        describe("method getColRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getColRange");
            });
            it("should return a column range for an index", () => {
                expect(factory.getColRange(0)).toEqual(r("A1:A65536"));
                expect(factory.getColRange(1)).toEqual(r("B1:B65536"));
                expect(factory.getColRange(255)).toEqual(r("IV1:IV65536"));
            });
            it("should return a column range for an interval", () => {
                expect(factory.getColRange(i("A"))).toEqual(r("A1:A65536"));
                expect(factory.getColRange(i("B:C"))).toEqual(r("B1:C65536"));
                expect(factory.getColRange(i("A:IV"))).toEqual(r("A1:IV65536"));
            });
        });

        describe("method getColRangeList", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getColRangeList");
            });
            it("should return a column range list for an interval", () => {
                expect(factory.getColRangeList(i("A"))).toEqual(ra("A1:A65536"));
                expect(factory.getColRangeList(i("B:C"))).toEqual(ra("B1:C65536"));
                expect(factory.getColRangeList(i("A:IV"))).toEqual(ra("A1:IV65536"));
            });
            it("should return a column range list for an interval list", () => {
                expect(factory.getColRangeList(ia("A B:C A:IV"))).toEqual(ra("A1:A65536 B1:C65536 A1:IV65536"));
            });
            it("should return an empty list for null", () => {
                expect(factory.getColRangeList(null)).toEqual(ra(""));
            });
        });

        describe("method isRowRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("isRowRange");
            });
            it("should return true for row ranges", () => {
                expect(factory.isRowRange(r("A1:IV1"))).toBeTrue();
                expect(factory.isRowRange(r("A2:IV3"))).toBeTrue();
                expect(factory.isRowRange(r("A1:IV65536"))).toBeTrue();
            });
            it("should return false for other ranges", () => {
                expect(factory.isRowRange(r("B1:IV1"))).toBeFalse();
                expect(factory.isRowRange(r("A1:IU1"))).toBeFalse();
                expect(factory.isRowRange(r("B1:IU1"))).toBeFalse();
                expect(factory.isRowRange(r("A1:A65536"))).toBeFalse();
            });
        });

        describe("method getRowRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getRowRange");
            });
            it("should return a row range for an index", () => {
                expect(factory.getRowRange(0)).toEqual(r("A1:IV1"));
                expect(factory.getRowRange(1)).toEqual(r("A2:IV2"));
                expect(factory.getRowRange(65535)).toEqual(r("A65536:IV65536"));
            });
            it("should return a row range for an interval", () => {
                expect(factory.getRowRange(i("1"))).toEqual(r("A1:IV1"));
                expect(factory.getRowRange(i("2:3"))).toEqual(r("A2:IV3"));
                expect(factory.getRowRange(i("1:65536"))).toEqual(r("A1:IV65536"));
            });
        });

        describe("method getRowRangeList", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getRowRangeList");
            });
            it("should return a row range list for an interval", () => {
                expect(factory.getRowRangeList(i("1"))).toEqual(ra("A1:IV1"));
                expect(factory.getRowRangeList(i("2:3"))).toEqual(ra("A2:IV3"));
                expect(factory.getRowRangeList(i("1:65536"))).toEqual(ra("A1:IV65536"));
            });
            it("should return a row range list for an interval list", () => {
                expect(factory.getRowRangeList(ia("1 2:3 1:65536"))).toEqual(ra("A1:IV1 A2:IV3 A1:IV65536"));
            });
            it("should return an empty list for null", () => {
                expect(factory.getRowRangeList(null)).toEqual(ra(""));
            });
        });

        describe("method isFullRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("isFullRange");
            });
            it("should return true for row ranges", () => {
                expect(factory.isFullRange(r("A1:IV1"), false)).toBeTrue();
                expect(factory.isFullRange(r("A2:IV3"), false)).toBeTrue();
                expect(factory.isFullRange(r("A1:IV65536"), false)).toBeTrue();
            });
            it("should return true for column ranges", () => {
                expect(factory.isFullRange(r("A1:A65536"), true)).toBeTrue();
                expect(factory.isFullRange(r("B1:C65536"), true)).toBeTrue();
                expect(factory.isFullRange(r("A1:IV65536"), true)).toBeTrue();
            });
            it("should return false for other ranges", () => {
                expect(factory.isFullRange(r("B1:IV1"), false)).toBeFalse();
                expect(factory.isFullRange(r("A1:IU1"), false)).toBeFalse();
                expect(factory.isFullRange(r("B1:IU1"), false)).toBeFalse();
                expect(factory.isFullRange(r("A1:A65536"), false)).toBeFalse();
                expect(factory.isFullRange(r("A2:A65536"), true)).toBeFalse();
                expect(factory.isFullRange(r("A1:A65535"), true)).toBeFalse();
                expect(factory.isFullRange(r("A2:A65535"), true)).toBeFalse();
                expect(factory.isFullRange(r("A1:IV1"), true)).toBeFalse();
            });
        });

        describe("method getFullRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getFullRange");
            });
            it("should return a row range for an index", () => {
                expect(factory.getFullRange(0, false)).toEqual(r("A1:IV1"));
                expect(factory.getFullRange(1, false)).toEqual(r("A2:IV2"));
                expect(factory.getFullRange(65535, false)).toEqual(r("A65536:IV65536"));
            });
            it("should return a row range for an interval", () => {
                expect(factory.getFullRange(i("1"), false)).toEqual(r("A1:IV1"));
                expect(factory.getFullRange(i("2:3"), false)).toEqual(r("A2:IV3"));
                expect(factory.getFullRange(i("1:65536"), false)).toEqual(r("A1:IV65536"));
            });
            it("should return a column range for an index", () => {
                expect(factory.getFullRange(0, true)).toEqual(r("A1:A65536"));
                expect(factory.getFullRange(1, true)).toEqual(r("B1:B65536"));
                expect(factory.getFullRange(255, true)).toEqual(r("IV1:IV65536"));
            });
            it("should return a column range for an interval", () => {
                expect(factory.getFullRange(i("A"), true)).toEqual(r("A1:A65536"));
                expect(factory.getFullRange(i("B:C"), true)).toEqual(r("B1:C65536"));
                expect(factory.getFullRange(i("A:IV"), true)).toEqual(r("A1:IV65536"));
            });
        });

        describe("method getFullRangeList", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getFullRangeList");
            });
            it("should return a row range list for an interval", () => {
                expect(factory.getFullRangeList(i("1"), false)).toEqual(ra("A1:IV1"));
                expect(factory.getFullRangeList(i("2:3"), false)).toEqual(ra("A2:IV3"));
                expect(factory.getFullRangeList(i("1:65536"), false)).toEqual(ra("A1:IV65536"));
            });
            it("should return a row range list for an interval list", () => {
                expect(factory.getFullRangeList(ia("1 2:3 1:65536"), false)).toEqual(ra("A1:IV1 A2:IV3 A1:IV65536"));
            });
            it("should return a column range list for an interval", () => {
                expect(factory.getFullRangeList(i("A"), true)).toEqual(ra("A1:A65536"));
                expect(factory.getFullRangeList(i("B:C"), true)).toEqual(ra("B1:C65536"));
                expect(factory.getFullRangeList(i("A:IV"), true)).toEqual(ra("A1:IV65536"));
            });
            it("should return a column range list for an interval list", () => {
                expect(factory.getFullRangeList(ia("A B:C A:IV"), true)).toEqual(ra("A1:A65536 B1:C65536 A1:IV65536"));
            });
            it("should return an empty list for null", () => {
                expect(factory.getFullRangeList(null, false)).toEqual(ra(""));
                expect(factory.getFullRangeList(null, true)).toEqual(ra(""));
            });
        });

        describe("method getRangeGroups", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getRangeGroups");
            });
            it("should split ranges into groups", () => {
                const rangeGroups = factory.getRangeGroups(ra("A1:A65536 A1:IV1 B1:C65536 A2:IV3 A1:IV65536 B2:C65536 B2:IV3 B2:C3"));
                expect(rangeGroups).toBeObject();
                expect(rangeGroups.colRanges).toEqual(ra("A1:A65536 B1:C65536 A1:IV65536"));
                expect(rangeGroups.rowRanges).toEqual(ra("A1:IV1 A2:IV3"));
                expect(rangeGroups.cellRanges).toEqual(ra("B2:C65536 B2:IV3 B2:C3"));
            });
            it("should omit empty properties", () => {
                expect(factory.getRangeGroups(ra("A1:A65536 A1:IV1"))).toEqual({ colRanges: ra("A1:A65536"), rowRanges: ra("A1:IV1") });
                expect(factory.getRangeGroups(ra("A1:A65536 B2:C3"))).toEqual({ colRanges: ra("A1:A65536"), cellRanges: ra("B2:C3") });
                expect(factory.getRangeGroups(ra("A1:IV1 B2:C3"))).toEqual({ rowRanges: ra("A1:IV1"), cellRanges: ra("B2:C3") });
                expect(factory.getRangeGroups(ra("A1:A65536"))).toEqual({ colRanges: ra("A1:A65536") });
                expect(factory.getRangeGroups(ra("A1:IV1"))).toEqual({ rowRanges: ra("A1:IV1") });
                expect(factory.getRangeGroups(ra("B2:C3"))).toEqual({ cellRanges: ra("B2:C3") });
                expect(factory.getRangeGroups(ra(""))).toEqual({});
            });
            it("should accept single ranges and null", () => {
                expect(factory.getRangeGroups(r("A1:A65536"))).toEqual({ colRanges: ra("A1:A65536") });
                expect(factory.getRangeGroups(r("A1:IV1"))).toEqual({ rowRanges: ra("A1:IV1") });
                expect(factory.getRangeGroups(r("B2:C3"))).toEqual({ cellRanges: ra("B2:C3") });
                expect(factory.getRangeGroups(null)).toEqual({});
            });
        });

        describe("method getBoundedMovedRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getBoundedMovedRange");
            });
            it("should move the range inside the sheet", () => {
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), 0, 0)).toEqual(r("DX32768:DY32769"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), -1, 1)).toEqual(r("DW32769:DX32770"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), -2, 2)).toEqual(r("DV32770:DW32771"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), -126, 32766)).toEqual(r("B65534:C65535"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), -127, 32767)).toEqual(r("A65535:B65536"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), -128, 32768)).toEqual(r("A65535:B65536"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), -1e6, 1e6)).toEqual(r("A65535:B65536"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), 1, -1)).toEqual(r("DY32767:DZ32768"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), 2, -2)).toEqual(r("DZ32766:EA32767"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), 126, -32766)).toEqual(r("IT2:IU3"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), 127, -32767)).toEqual(r("IU1:IV2"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), 128, -32768)).toEqual(r("IU1:IV2"));
                expect(factory.getBoundedMovedRange(r("DX32768:DY32769"), 1e6, -1e6)).toEqual(r("IU1:IV2"));
            });
        });

        describe("method getCroppedMovedRange", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("getCroppedMovedRange");
            });
            it("should move and crop the range", () => {
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), 0, 0)).toEqual(r("DX32768:DY32769"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), -1, 1)).toEqual(r("DW32769:DX32770"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), -2, 2)).toEqual(r("DV32770:DW32771"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), -126, 32766)).toEqual(r("B65534:C65535"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), -127, 32767)).toEqual(r("A65535:B65536"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), -128, 32767)).toEqual(r("A65535:A65536"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), -129, 32767)).toBeUndefined();
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), -127, 32768)).toEqual(r("A65536:B65536"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), -127, 32769)).toBeUndefined();
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), -128, 32768)).toEqual(r("A65536"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), -129, 32769)).toBeUndefined();
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), 1, -1)).toEqual(r("DY32767:DZ32768"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), 2, -2)).toEqual(r("DZ32766:EA32767"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), 126, -32766)).toEqual(r("IT2:IU3"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), 127, -32767)).toEqual(r("IU1:IV2"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), 128, -32767)).toEqual(r("IV1:IV2"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), 129, -32767)).toBeUndefined();
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), 127, -32768)).toEqual(r("IU1:IV1"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), 127, -32769)).toBeUndefined();
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), 128, -32768)).toEqual(r("IV1"));
                expect(factory.getCroppedMovedRange(r("DX32768:DY32769"), 129, -32769)).toBeUndefined();
            });
        });

        describe("method parseCellAnchor", () => {
            it("should exist", () => {
                expect(AddressFactory).toHaveMethod("parseCellAnchor");
            });
            it("should parse cell anchor strings", () => {
                expect(factory.parseCellAnchor("2 B2 20 20 C3 10 10").toOpStr()).toBe("2 B2 20 20 C3 10 10");
                expect(factory.parseCellAnchor("2 C2 20 20 B3 10 10").toOpStr()).toBe("2 C2 20 20 C3 20 10");
                expect(factory.parseCellAnchor("2 B3 20 20 C2 10 10").toOpStr()).toBe("2 B3 20 20 C3 10 20");
                expect(factory.parseCellAnchor("2 C3 20 20 B2 10 10").toOpStr()).toBe("2 C3 20 20 C3 20 20");
                expect(factory.parseCellAnchor("").toOpStr()).toBe("2 A1 0 0 A1 0 0");
                expect(factory.parseCellAnchor("3 1A").toOpStr()).toBe("2 A1 0 0 A1 0 0");
            });
        });
    });
});
