/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { Direction } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { CellCollection } from "@/io.ox/office/spreadsheet/model/cellcollection";

import { i, r, dts, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

const { LEFT, RIGHT, UP, DOWN } = Direction;

// tests ======================================================================

describe("module spreadsheet/model/cellcollection", () => {

    // date/time and auto-style
    const ds = (dt, s) => ({ v: dts(dt), s });

    // numbers/strings for vertical and horizontal tests
    const NUM_STR_MATRIX = [
        [6,  11, 3, "X", "R1", "S1", "1.T", "1.U", "Mo", "Mittwoch", "Jan"],
        [8,  12, 2, "x", "R2", "S2", "2.T", "2.U", "Di", "Dienstag", "Feb"],
        [10, 16, 1, "X", "R3", "S4", "3.T", "4.U", "Mi", "Montag",   "Mrz"]
    ];

    // the operations to be applied by the document model
    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertAutoStyle("a1", { apply: { number: true }, cell: { formatId: 14 } }), // date format
        op.insertAutoStyle("a2", { apply: { number: true }, cell: { formatId: 20 } }), // time format
        op.insertAutoStyle("a3", { apply: { number: true }, cell: { formatId: 22 } }), // date/time format
        op.insertAutoStyle("a4", { apply: { number: true }, cell: { formatId: 11 } }), // scientific format
        op.insertAutoStyle("a5", { apply: { number: true }, cell: { formatId: 12 } }), // fraction format

        op.insertSheet(0, "Sheet1"),
        op.changeCols(0, "AAC", "a2", { column: { width: 100 } }),
        op.changeCols(0, "AAD", "a3", { column: { visible: false } }),
        op.changeRows(0, "1002", "a2", { row: { height: 100, customFormat: true } }),
        op.changeRows(0, "1003", "a3", { row: { visible: false, customFormat: true } }),

        // numbers/strings/lists
        op.changeCells(0, "A7", NUM_STR_MATRIX),
        op.changeCells(0, { A10: 42, B10: { s: "a3" } }),
        op.changeCells(0, "AS1", _.unzip(NUM_STR_MATRIX)),
        // date/time
        op.changeCells(0, "A26", [
            [ds("2000-01-01", "a1"), ds("2000-01-05", "a1"), ds("2001-01-01", "a1"), ds("2000-01-01", "a1"), ds("2000-01-01", "a1"), ds("16:00", "a2"), ds("2000-01-01 16:00", "a3")],
            [ds("2000-01-03", "a1"), ds("2000-01-03", "a1"), ds("2001-03-01", "a1"), ds("2002-01-01", "a1"), ds("2000-02-02", "a1"), ds("17:00", "a2"), ds("2000-01-02 17:00", "a3")],
            [ds("2000-01-05", "a1"), ds("2000-01-01", "a1"), ds("2001-05-01", "a1"), ds("2004-01-01", "a1"), ds("2000-03-03", "a1"), ds("18:00", "a2"), ds("2000-01-03 18:00", "a3")]
        ]),
        // formulas
        op.changeCells(0, "C43", [
            [{ v: 0, f: "SUM(H41:$J$43,H$41:$J43)" }, { v: 0, f: "SUM(I41:$J$43,I$41:$J43)" }],
            [{ v: 0, f: "SUM(H42:$J$43,H$41:$J44)" }, { v: 0, f: "SUM(I42:$J$43,I$41:$J44)" }]
        ]),
        // mixed
        op.changeCells(0, "CA11", [
            [1,   3,    1],
            [2,   2,    2],
            ["a", "a1", 9],
            [3,   1,    "a"],
            [4,   "b1", 1]
        ]),
        // DOCS-4432: treat various number format categories as numbers
        op.changeCells(0, { CA11: { s: "a4" }, CA12: { s: "a5" } }),
        // DOCS-4739: cell values when filling entire columns
        op.changeCells(0, "AAC2", [[42, 43, 44]]),
        op.changeCells(0, "AAC4", [["a", "b", "c"]]),
        // DOCS-4739: cell values when filling entire rows
        op.changeCells(0, "B1002", [[42], [43], [44]]),
        op.changeCells(0, "D1002", [["a"], ["b"], ["c"]]),

        // DOCS-4185: test sheet for hidden columns/rows with filter
        op.insertSheet(1, "Sheet2"),
        op.changeCells(1, "E5", [[1, 2, 3, "a1"], [2, 3, 4, "a2"], [3, 4, 5, "1a"], ["a1", "a2", "1a"]]),
        op.changeCols(1, "C F I", { column: { visible: false } }),
        op.changeRows(1, "3 6 9", { row: { visible: false } }),
        op.changeCells(1, "Z1", [["H"], [2], [3], [4], [5], [6], [7]]),
        op.insertTable(1, null, "Z1:Z7", { table: { headerRow: true } })
    ];

    // initialize test document
    let docModel;
    createSpreadsheetApp("ooxml", OPERATIONS).done(docApp => {
        docModel = docApp.docModel;
    });

    // convenience shortcut for applying an autofill operation
    async function applyAutoFill(sheet, range, dir, count, options) {
        const sheetModel = docModel.getSheetModel(sheet);
        await sheetModel.createAndApplyOperations(generator => {
            return sheetModel.cellCollection.generateAutoFillOperations(generator, range, dir, count, options);
        });
    }

    // class CellCollection ---------------------------------------------------

    describe("class CellCollection", () => {

        describe("method generateAutoFillOperations", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("generateAutoFillOperations");
            });

            describe("should generate correct autofill operations", () => {

                const NUM_STR_RANGE_V = r("A7:K9");
                const NUM_STR_RANGE_H = r("AS1:AU11");

                const EXP_NUM_STR_DOWN = [
                    [12,   18,    0,   "X",  "R4", "S1", "4.T", "1.U", "Do", "Sonntag",    "Apr"],
                    [14,   20.5, -1,   "x",  "R5", "S2", "5.T", "2.U", "Fr", "Samstag",    "Mai"],
                    [16,   23,   -2,   "X",  "R6", "S4", "6.T", "4.U", "Sa", "Freitag",    "Jun"],
                    [18,   25.5, -3,   "X",  "R7", "S1", "7.T", "1.U", "So", "Donnerstag", "Jul"],
                    [20,   28,   -4,   "x",  "R8", "S2", "8.T", "2.U", "Mo", "Mittwoch",   "Aug"],
                    [null, null, null, null, null, null, null,  null,  null, null,         null]
                ];

                const EXP_NUM_STR_DOWN_COPY = [
                    [6,    11,   3,    "X",  "R1", "S1", "1.T", "1.U", "Mo", "Mittwoch", "Jan"],
                    [8,    12,   2,    "x",  "R2", "S2", "2.T", "2.U", "Di", "Dienstag", "Feb"],
                    [10,   16,   1,    "X",  "R3", "S4", "3.T", "4.U", "Mi", "Montag",   "Mrz"],
                    [6,    11,   3,    "X",  "R1", "S1", "1.T", "1.U", "Mo", "Mittwoch", "Jan"],
                    [8,    12,   2,    "x",  "R2", "S2", "2.T", "2.U", "Di", "Dienstag", "Feb"],
                    [null, null, null, null, null, null, null,  null,  null, null,       null]
                ];

                const EXP_NUM_STR_UP = [
                    [null, null, null, null, null, null, null,  null,  null, null,         null],
                    [-4,   -2,   8,    "x",  "R4", "S2", "4.T", "2.U", "Mi", "Montag",     "Aug"],
                    [-2,   0.5,  7,    "X",  "R3", "S4", "3.T", "4.U", "Do", "Sonntag",    "Sep"],
                    [0,    3,    6,    "X",  "R2", "S1", "2.T", "1.U", "Fr", "Samstag",    "Okt"],
                    [2,    5.5,  5,    "x",  "R1", "S2", "1.T", "2.U", "Sa", "Freitag",    "Nov"],
                    [4,    8,    4,    "X",  "R0", "S4", "0.T", "4.U", "So", "Donnerstag", "Dez"]
                ];

                const EXP_NUM_STR_UP_COPY = [
                    [null, null, null, null, null, null, null,  null,  null, null,       null],
                    [8,    12,   2,    "x",  "R2", "S2", "2.T", "2.U", "Di", "Dienstag", "Feb"],
                    [10,   16,   1,    "X",  "R3", "S4", "3.T", "4.U", "Mi", "Montag",   "Mrz"],
                    [6,    11,   3,    "X",  "R1", "S1", "1.T", "1.U", "Mo", "Mittwoch", "Jan"],
                    [8,    12,   2,    "x",  "R2", "S2", "2.T", "2.U", "Di", "Dienstag", "Feb"],
                    [10,   16,   1,    "X",  "R3", "S4", "3.T", "4.U", "Mi", "Montag",   "Mrz"]
                ];

                it("(numbers/strings, down, auto mode)", async () => {
                    await applyAutoFill(0, NUM_STR_RANGE_V, DOWN, 5);
                    const sheetTester = docModel.expect.sheet(0);
                    sheetTester
                        .values("A10", EXP_NUM_STR_DOWN)
                        .styles("A10", [["a0", "a0"]]);
                    await docModel.undoManager.undo();
                    sheetTester
                        .values("A10", 42)
                        .styles("A10", [["a0", "a3"]])
                        .noCells("C10:K10 A11:K14");
                });

                it("(numbers/strings, down, copy mode)", async () => {
                    await applyAutoFill(0, NUM_STR_RANGE_V, DOWN, 5, { copy: true });
                    docModel.expect.sheet(0).values("A10", EXP_NUM_STR_DOWN_COPY);
                });

                it("(numbers/strings, up, auto mode)", async () => {
                    await applyAutoFill(0, NUM_STR_RANGE_V, UP, 5);
                    docModel.expect.sheet(0).values("A1", EXP_NUM_STR_UP);
                });

                it("(numbers/strings, up, copy mode)", async () => {
                    await applyAutoFill(0, NUM_STR_RANGE_V, UP, 5, { copy: true });
                    docModel.expect.sheet(0).values("A1", EXP_NUM_STR_UP_COPY);
                });

                it("(numbers/strings, right, auto mode)", async () => {
                    await applyAutoFill(0, NUM_STR_RANGE_H, RIGHT, 5);
                    docModel.expect.sheet(0).values("AV1", _.unzip(EXP_NUM_STR_DOWN));
                });

                it("(numbers/strings, right, copy mode)", async () => {
                    await applyAutoFill(0, NUM_STR_RANGE_H, RIGHT, 5, { copy: true });
                    docModel.expect.sheet(0).values("AV1", _.unzip(EXP_NUM_STR_DOWN_COPY));
                });

                it("(numbers/strings, left, auto mode)", async () => {
                    await applyAutoFill(0, NUM_STR_RANGE_H, LEFT, 5);
                    docModel.expect.sheet(0).values("AM1", _.unzip(EXP_NUM_STR_UP));
                });

                it("(numbers/strings, left, copy mode)", async () => {
                    await applyAutoFill(0, NUM_STR_RANGE_H, LEFT, 5, { copy: true });
                    docModel.expect.sheet(0).values("AM1", _.unzip(EXP_NUM_STR_UP_COPY));
                });

                it("(date/time, down, auto mode)", async () => {
                    await applyAutoFill(0, r("A26:G28"), DOWN, 5);
                    docModel.expect.sheet(0)
                        .values("A29", [
                            [dts("2000-01-07"), dts("1999-12-30"), dts("2001-07-01"), dts("2006-01-01"), dts("2000-01-01"), dts("19:00"), dts("2000-01-04 19:00")],
                            [dts("2000-01-09"), dts("1999-12-28"), dts("2001-09-01"), dts("2008-01-01"), dts("2000-02-02"), dts("20:00"), dts("2000-01-05 20:00")],
                            [dts("2000-01-11"), dts("1999-12-26"), dts("2001-11-01"), dts("2010-01-01"), dts("2000-03-03"), dts("21:00"), dts("2000-01-06 21:00")],
                            [dts("2000-01-13"), dts("1999-12-24"), dts("2002-01-01"), dts("2012-01-01"), dts("2000-01-01"), dts("22:00"), dts("2000-01-07 22:00")],
                            [dts("2000-01-15"), dts("1999-12-22"), dts("2002-03-01"), dts("2014-01-01"), dts("2000-02-02"), dts("23:00"), dts("2000-01-08 23:00")]
                        ])
                        .styles("A29", [
                            ["a1", "a1", "a1", "a1", "a1", "a2", "a3"],
                            ["a1", "a1", "a1", "a1", "a1", "a2", "a3"],
                            ["a1", "a1", "a1", "a1", "a1", "a2", "a3"]
                        ]);
                });

                it("(date/time, down, copy mode)", async () => {
                    await applyAutoFill(0, r("A26:G28"), DOWN, 5, { copy: true });
                    docModel.expect.sheet(0)
                        .values("A29", [
                            [dts("2000-01-01"), dts("2000-01-05"), dts("2001-01-01"), dts("2000-01-01"), dts("2000-01-01"), dts("16:00"), dts("2000-01-01 16:00")],
                            [dts("2000-01-03"), dts("2000-01-03"), dts("2001-03-01"), dts("2002-01-01"), dts("2000-02-02"), dts("17:00"), dts("2000-01-02 17:00")],
                            [dts("2000-01-05"), dts("2000-01-01"), dts("2001-05-01"), dts("2004-01-01"), dts("2000-03-03"), dts("18:00"), dts("2000-01-03 18:00")],
                            [dts("2000-01-01"), dts("2000-01-05"), dts("2001-01-01"), dts("2000-01-01"), dts("2000-01-01"), dts("16:00"), dts("2000-01-01 16:00")],
                            [dts("2000-01-03"), dts("2000-01-03"), dts("2001-03-01"), dts("2002-01-01"), dts("2000-02-02"), dts("17:00"), dts("2000-01-02 17:00")]
                        ]);
                });

                it("(date/time, up, auto mode)", async () => {
                    await applyAutoFill(0, r("A26:G28"), UP, 5);
                    docModel.expect.sheet(0)
                        .values("A21", [
                            [dts("1999-12-22"), dts("2000-01-15"), dts("2000-03-01"), dts("1990-01-01"), dts("2000-02-02"), dts("11:00"), dts("1999-12-27 11:00")],
                            [dts("1999-12-24"), dts("2000-01-13"), dts("2000-05-01"), dts("1992-01-01"), dts("2000-03-03"), dts("12:00"), dts("1999-12-28 12:00")],
                            [dts("1999-12-26"), dts("2000-01-11"), dts("2000-07-01"), dts("1994-01-01"), dts("2000-01-01"), dts("13:00"), dts("1999-12-29 13:00")],
                            [dts("1999-12-28"), dts("2000-01-09"), dts("2000-09-01"), dts("1996-01-01"), dts("2000-02-02"), dts("14:00"), dts("1999-12-30 14:00")],
                            [dts("1999-12-30"), dts("2000-01-07"), dts("2000-11-01"), dts("1998-01-01"), dts("2000-03-03"), dts("15:00"), dts("1999-12-31 15:00")]
                        ]);
                });

                it("(date/time, up, copy mode)", async () => {
                    await applyAutoFill(0, r("A26:G28"), UP, 5, { copy: true });
                    docModel.expect.sheet(0)
                        .values("A21", [
                            [dts("2000-01-03"), dts("2000-01-03"), dts("2001-03-01"), dts("2002-01-01"), dts("2000-02-02"), dts("17:00"), dts("2000-01-02 17:00")],
                            [dts("2000-01-05"), dts("2000-01-01"), dts("2001-05-01"), dts("2004-01-01"), dts("2000-03-03"), dts("18:00"), dts("2000-01-03 18:00")],
                            [dts("2000-01-01"), dts("2000-01-05"), dts("2001-01-01"), dts("2000-01-01"), dts("2000-01-01"), dts("16:00"), dts("2000-01-01 16:00")],
                            [dts("2000-01-03"), dts("2000-01-03"), dts("2001-03-01"), dts("2002-01-01"), dts("2000-02-02"), dts("17:00"), dts("2000-01-02 17:00")],
                            [dts("2000-01-05"), dts("2000-01-01"), dts("2001-05-01"), dts("2004-01-01"), dts("2000-03-03"), dts("18:00"), dts("2000-01-03 18:00")]
                        ]);
                });

                it("(formulas, down, auto mode)", async () => {
                    await applyAutoFill(0, r("C43:D44"), DOWN, 2);
                    docModel.expect.sheet(0)
                        .formulas("C45", [
                            ["SUM(H43:$J$43,H$41:$J45)", "SUM(I43:$J$43,I$41:$J45)"],
                            ["SUM(H$43:$J44,H$41:$J46)", "SUM(I$43:$J44,I$41:$J46)"]
                        ]);
                });

                it("(formulas, down, copy mode)", async () => {
                    await applyAutoFill(0, r("C43:D44"), DOWN, 2, { copy: true });
                    docModel.expect.sheet(0)
                        .formulas("C45", [
                            ["SUM(H41:$J$43,H$41:$J43)", "SUM(I41:$J$43,I$41:$J43)"],
                            ["SUM(H42:$J$43,H$41:$J44)", "SUM(I42:$J$43,I$41:$J44)"]
                        ]);
                });

                it("(formulas, up, auto mode)", async () => {
                    await applyAutoFill(0, r("C43:D44"), UP, 2);
                    docModel.expect.sheet(0)
                        .formulas("C41", [
                            ["SUM(H39:$J$43,H$41:$J41)", "SUM(I39:$J$43,I$41:$J41)"],
                            ["SUM(H40:$J$43,H$41:$J42)", "SUM(I40:$J$43,I$41:$J42)"]
                        ]);
                });

                it("(formulas, right, auto mode)", async () => {
                    await applyAutoFill(0, r("C43:D44"), RIGHT, 2);
                    docModel.expect.sheet(0)
                        .formulas("E43", [
                            ["SUM(J41:$J$43,J$41:$J43)", "SUM($J41:K$43,$J$41:K43)"],
                            ["SUM(J42:$J$43,J$41:$J44)", "SUM($J42:K$43,$J$41:K44)"]
                        ]);
                });

                it("(formulas, left, auto mode)", async () => {
                    await applyAutoFill(0, r("C43:D44"), LEFT, 2);
                    const sheetTester = docModel.expect.sheet(0);
                    sheetTester.formulas("A43", [
                        ["SUM(F41:$J$43,F$41:$J43)", "SUM(G41:$J$43,G$41:$J43)"],
                        ["SUM(F42:$J$43,F$41:$J44)", "SUM(G42:$J$43,G$41:$J44)"]
                    ]);
                });

                it("(mixed groups, down, auto mode)", async () => {
                    await applyAutoFill(0, r("CA11:CC15"), DOWN, 10);
                    docModel.expect.sheet(0)
                        .values("CA16", [
                            [3,   1,    12],
                            [4,   0,    16],
                            ["a", "a2", 20],
                            [5,   2,    "a"],
                            [6,   "b2", 2],
                            [5,   -1,   24],
                            [6,   -2,   28],
                            ["a", "a3", 32],
                            [7,   3,    "a"],
                            [8,   "b3", 3]
                        ]);
                });

                it("(mixed groups, up, auto mode)", async () => {
                    await applyAutoFill(0, r("CA11:CC15"), UP, 10);
                    docModel.expect.sheet(0)
                        .values("CA1", [
                            [-3,  7,    -24],
                            [-2,  6,    -20],
                            ["a", "a1", -16],
                            [-1,  -1,   "a"],
                            [0,   "b1", -1],
                            [-1,  5,    -12],
                            [0,   4,    -8],
                            ["a", "a0", -4],
                            [1,   0,    "a"],
                            [2,   "b0", 0]
                        ]);
                });

                it("(columns, left)", async () => {
                    await applyAutoFill(0, docModel.addressFactory.getColRange(i("AAC")), LEFT, 2);
                    docModel.expect.sheet(0)
                        .cols("AAA:AAB", "a2", { column: { width: 100 } })
                        .cols("ZZ",      "a0")
                        .values("AAA2", [[42, 42, 42]])
                        .values("AAA4", [["a", "a", "a"]]);
                });

                it("(columns, right)", async () => {
                    await applyAutoFill(0, docModel.addressFactory.getColRange(i("AAC:AAE")), RIGHT, 7);
                    docModel.expect.sheet(0)
                        .cols("AAF AAI AAL", "a2", { column: { width: 100 } })
                        .cols("AAG AAJ",     "a3", { column: { visible: false } })
                        .cols("AAH AAK AAM", "a0")
                        .values("AAC2", [[42, 43, 44, 45, 46, 47, 48]])
                        .values("AAC4", [["a", "b", "c", "a", "b", "c", "a"]]);
                });

                it("(rows, up)", async () => {
                    await applyAutoFill(0, docModel.addressFactory.getRowRange(i("1002")), UP, 2);
                    docModel.expect.sheet(0)
                        .rows("1000:1001", "a2", { row: { height: 100, customFormat: true } })
                        .rows("999",       "a0", { row: { customFormat: false } })
                        .values("B1000", [42, 42, 42])
                        .values("D1000", ["a", "a", "a"]);
                });

                it("(rows, down)", async () => {
                    await applyAutoFill(0, docModel.addressFactory.getRowRange(i("1002:1004")), DOWN, 7);
                    docModel.expect.sheet(0)
                        .rows("1005 1008 1011", "a2", { row: { height: 100, customFormat: true } })
                        .rows("1006 1009",      "a3", { row: { visible: false, customFormat: true } })
                        .rows("1007 1010 1012", "a0", { row: { customFormat: false } })
                        .values("B1002", [42, 43, 44, 45, 46, 47, 48])
                        .values("D1002", ["a", "b", "c", "a", "b", "c", "a"]);
                });
            });
        });

        // DOCS-4185: skip (all) hidden columns/rows in sheets with active filter
        describe("should not skip hidden rows and columns in unfiltered sheets", () => {
            it("(rows, down)", async () => {
                await applyAutoFill(1, r("E5:H7"), DOWN, 4);
                const sheetTester = docModel.expect.sheet(1);
                sheetTester.values("E8", [[4, 5, 6, "a3"], [5, 6, 7, "a4"], [6, 7, 8, "2a"], [7, 8, 9, "a5"]]);
                await docModel.undoManager.undo();
                sheetTester.values("E8", [["a1", "a2", "1a", null]]);
                sheetTester.values("E9:H11", null);
            });
            it("(rows, up)", async () => {
                await applyAutoFill(1, r("E5:H7"), UP, 4);
                const sheetTester = docModel.expect.sheet(1);
                sheetTester.values("E1", [[-3, -2, -1, "1a"], [-2, -1, 0, "a1"], [-1, 0, 1, "a0"], [0, 1, 2, "0a"]]);
                await docModel.undoManager.undo();
                sheetTester.values("E1:H4", null);
            });
            it("(columns, right)", async () => {
                await applyAutoFill(1, r("E5:G8"), RIGHT, 4);
                const sheetTester = docModel.expect.sheet(1);
                sheetTester.values("H5", [[4, 5, 6, 7], [5, 6, 7, 8], [6, 7, 8, 9], ["a3", "a4", "2a", "a5"]]);
                await docModel.undoManager.undo();
                sheetTester.values("H5", ["a1", "a2", "1a", null]);
                sheetTester.values("I5:L8", null);
            });
            it("(columns, left)", async () => {
                await applyAutoFill(1, r("E5:G8"), LEFT, 4);
                const sheetTester = docModel.expect.sheet(1);
                sheetTester.values("A5", [[-3, -2, -1, 0], [-2, -1, 0, 1], [-1, 0, 1, 2], ["1a", "a1", "a0", "0a"]]);
                await docModel.undoManager.undo();
                sheetTester.values("A5:D8", null);
            });
        });
        describe("should skip hidden rows and columns in filtered sheets", () => {
            beforeAll(() => {
                docModel.invokeOperationHandlers(op.changeTableCol(1, null, 0, { attrs: { filter: { type: "discrete" } } }));
            });
            it("(rows, down)", async () => {
                await applyAutoFill(1, r("E5:H7"), DOWN, 4);
                const sheetTester = docModel.expect.sheet(1);
                sheetTester.values("E8", [[5, "a2", 7, "a2"], [null, null, null, null], [7, null, 9, "2a"], [9, null, 11, "a3"]]);
                await docModel.undoManager.undo();
                sheetTester.values("E8", [["a1", "a2", "1a", null]]);
                sheetTester.values("E9:H11", null);
            });
            it("(rows, up)", async () => {
                await applyAutoFill(1, r("E5:H7"), UP, 4);
                const sheetTester = docModel.expect.sheet(1);
                sheetTester.values("E1", [[-5, null, -3, "1a"], [-3, null, -1, "a0"], [null, null, null, null], [-1, null, 1, "0a"]]);
                await docModel.undoManager.undo();
                sheetTester.values("E1:H4", null);
            });
            it("(columns, right)", async () => {
                await applyAutoFill(1, r("E5:G8"), RIGHT, 4);
                const sheetTester = docModel.expect.sheet(1);
                sheetTester.values("H5", [[5, null, 7, 9], ["a2", null, null, null], [7, null, 9, 11], ["a2", null, "2a", "a3"]]);
                await docModel.undoManager.undo();
                sheetTester.values("H5", ["a1", "a2", "1a", null]);
                sheetTester.values("I5:L8", null);
            });
            it("(columns, left)", async () => {
                await applyAutoFill(1, r("E5:G8"), LEFT, 4);
                const sheetTester = docModel.expect.sheet(1);
                sheetTester.values("A5", [[-5, -3, null, -1], [null, null, null, null], [-3, -1, null, 1], ["1a", "a0", null, "0a"]]);
                await docModel.undoManager.undo();
                sheetTester.values("A5:D8", null);
            });
        });
    });
});
