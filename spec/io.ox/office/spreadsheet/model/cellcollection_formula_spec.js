/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { CellCollection } from "@/io.ox/office/spreadsheet/model/cellcollection";

import { a, r, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/cellcollection", () => {

    // the operations to be applied by the document model
    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertName("global", { formula: "Sheet1!E2+Sheet1!$E$2+Sheet2!E2+Sheet2!$E$2" }),
        op.insertName("global2", { formula: "global" }),

        op.insertSheet(0, "Sheet1"),
        op.changeCells(0, {
            // regular formulas
            B2: { v: 1, f: "E2+1" }, C2: { v: 2, f: "F2+2" }, E2: { v: 5, f: "Sheet2!E2+5" }, F2: { v: 6, f: "Sheet2!F2+6" },
            B3: { v: 3, f: "E3+3" }, C3: { v: 4, f: "F3+4" }, E3: { v: 7, f: "Sheet2!E3+7" }, F3: { v: 8, f: "Sheet2!F3+8" },
            // shared formulas
            B5: { v: 1, f: "E2", si: 0, sr: "B5:C6" }, C5: { v: 2, si: 0 }, E5: { v: 5, f: "Sheet2!E2", si: 1, sr: "E5:F6" }, F5: { v: 6, si: 1 },
            B6: { v: 3, si: 0 },                       C6: { v: 4, si: 0 }, E6: { v: 7, si: 1 },                              F6: { v: 8, si: 1 },
            // matrix formulas
            B8: { v: 1, f: "E2:F3", mr: "B8:C9" }, C8: 2, E8: { v: 5, f: "Sheet2!E2:F3", mr: "E8:F9" }, F8: 6,
            B9: 3,                                 C9: 4, E9: 7,                                        F9: 8,
            // table range
            B11: "Col1", C11: "Col2",
            B12: 1,      C12: 2,
            // table formulas
            A13: { v: 12, f: "SUM(Table1[],Table2[],Table1[Col1],Table1[Col2],Table2[Col1],Table2[Col2],Table2[[Col1]:[Col2]])" },
            A14: { v: 32, f: "SUM(global,global2)" },
            // DOCS-1667: renamed/copied sheet
            A16: { v: 11, f: "Sheet2!E2" }, B16: { v: "=Sheet2!E2", f: "_xlfn.FORMULATEXT(A16)" }
        }),
        op.insertDVRule(0, 0, "B8:C9", { type: "between", value1: "B2+Sheet2!B2", value2: "B11+Sheet2!B11" }),
        op.insertCFRule(0, "R0", "B8:C9", { type: "between", value1: "B2+Sheet2!B2", value2: "B11+Sheet2!B11", priority: 1, attrs: {} }),
        op.insertTable(0, "Table1", "B11:C12", { table: { headerRow: true } }),

        op.insertSheet(1, "Sheet2"),
        op.changeCells(1, {
            E2: 11, F2: 12, E3: 13, F3: 14, B11: "Col1", C11: "Col2", B12: 1, C12: 2,
            // DOCS-1667: renamed/copied sheet
            A16: { v: 11, f: "Sheet2!E2" }, B16: { v: "=Sheet2!E2", f: "_xlfn.FORMULATEXT(A16)" }
        }),
        op.insertDVRule(1, 0, "B8:C9", { type: "between", value1: "B2+Sheet1!B2", value2: "B11+Sheet1!B11" }),
        op.insertDVRule(1, 1, "D8:E10", { type: "between", value1: "B2+Sheet1!B2", value2: "B11+Sheet1!B11" }),
        op.insertDVRule(1, 2, "F10:G10", { type: "between", value1: "B2+Sheet1!B2", value2: "B11+Sheet1!B11" }),
        op.insertDVRule(1, 3, "H9:H9 I10:I10", { type: "list" }),
        op.insertDVRule(1, 4, "H10:H10 I9:I9", { type: "list" }),
        op.insertCFRule(1, "R0", "B8:C9", { type: "between", value1: "B2+Sheet1!B2", value2: "B11+Sheet1!B11", priority: 1, attrs: {} }),
        op.insertCFRule(1, "R1", "D8:E10", { type: "between", value1: "B2+Sheet1!B2", value2: "B11+Sheet1!B11", priority: 2, attrs: {} }),
        op.insertCFRule(1, "R2", "F10:G10", { type: "between", value1: "B2+Sheet1!B2", value2: "B11+Sheet1!B11", priority: 3, attrs: {} }),
        op.insertTable(1, "Table2", "B11:C12", { table: { headerRow: true } }),

        op.insertSheet(2, "Sheet3"),
        op.insertChart(2, 0, { attrs: { drawing: { anchor: "2 A1 0 0 B2 0 0" } } }),
        op.insertChSeries(2, 0, 0, { series: { type: "column2d", title: "Sheet1!$J$1", names: "Sheet1!$I$2:$I$3", values: "Sheet1!$J$2:$J$3" } }),
        op.insertChSeries(2, 0, 1, { series: { type: "column2d", title: "Sheet1!$K$1", names: "Sheet1!$I$2:$I$3", values: "Sheet1!$K$2:$K$3" } }),
        op.insertChSeries(2, 0, 2, { series: { type: "column2d", title: "Sheet2!$K$1", names: "Sheet1!$I$2:$I$3", values: "Sheet2!$K$2:$K$3" } }),
        op.changeChAxis(2, 0, 0, { axis: { axPos: "b", crossAx: 1, label: true } }),
        op.changeChAxis(2, 0, 1, { axis: { axPos: "l", crossAx: 0, label: true } }),
        op.changeChTitle(2, 0, { text: { link: "Sheet1!$J$10:$J$11" } }),
        op.changeChTitle(2, 0, 0, { text: { link: "Sheet1!$K$10:$K$11" } }),
        op.changeChTitle(2, 0, 1, { text: { link: "Sheet2!$K$10:$K$11" } })
    ];

    // initialize test document
    let docModel, sheetTester0, sheetModel0, cellCollection0, sheetTester1, sheetModel1, cellCollection1;
    createSpreadsheetApp("ooxml", OPERATIONS).done(function (docApp) {
        docModel = docApp.docModel;
        sheetTester0 = docModel.expect.sheet(0);
        sheetModel0 = sheetTester0.sheetModel;
        cellCollection0 = sheetModel0.cellCollection;
        sheetTester1 = docModel.expect.sheet(1);
        sheetModel1 = sheetTester1.sheetModel;
        cellCollection1 = sheetModel1.cellCollection;
    });

    function expectTokenDesc(cellCollection, address, options, expRef, expMatRange, expFormula) {
        const tokenDesc = cellCollection.getTokenArray(a(address), options);
        if (!expRef) {
            expect(tokenDesc).toBeNull();
        } else {
            expect(tokenDesc).toBeObject();
            expect(tokenDesc.tokenArray).toBeObject();
            expect(tokenDesc.refAddress).toStringifyTo(expRef);
            expect(tokenDesc.matrixRange).toStringifyTo(expMatRange || "null");
            expect(tokenDesc).toHaveProperty("formula", expFormula);
        }
    }

    // class CellCollection ---------------------------------------------------

    describe("class CellCollection", () => {

        describe("method applyChangeCellsOperation", () => {
            it("should import normal formulas", () => {
                sheetTester0.formulas({
                    B2: { f: "E2+1",        si: null, sr: null, mr: null },
                    F3: { f: "Sheet2!F3+8", si: null, sr: null, mr: null }
                });
            });
            it("should import shared formulas", () => {
                sheetTester0.formulas({
                    B5: { f: "E2",        si: 0, sr: "B5:C6", mr: null },
                    C6: { f: null,        si: 0, sr: null,    mr: null },
                    E5: { f: "Sheet2!E2", si: 1, sr: "E5:F6", mr: null },
                    F6: { f: null,        si: 1, sr: null,    mr: null }
                });
            });
            it("should import matrix formulas", () => {
                sheetTester0.formulas({
                    B8: { f: "E2:F3",        si: null, sr: null, mr: "B8:C9" },
                    C9: { f: null,           si: null, sr: null, mr: null },
                    E8: { f: "Sheet2!E2:F3", si: null, sr: null, mr: "E8:F9" },
                    F9: { f: null,           si: null, sr: null, mr: null }
                });
            });
        });

        describe("method isFormulaCell", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("isFormulaCell");
            });
            it("should return false for blank and value cells", () => {
                expect(cellCollection1.isFormulaCell(a("A1"))).toBeFalse();
                expect(cellCollection1.isFormulaCell(a("E2"))).toBeFalse();
            });
            it("should return true for normal formula cells", () => {
                expect(cellCollection0.isFormulaCell(a("B2"))).toBeTrue();
                expect(cellCollection0.isFormulaCell(a("C3"))).toBeTrue();
                expect(cellCollection0.isFormulaCell(a("E2"))).toBeTrue();
                expect(cellCollection0.isFormulaCell(a("F3"))).toBeTrue();
            });
            it("should return true for cells in a shared formula", () => {
                expect(cellCollection0.isFormulaCell(a("B5"))).toBeTrue();
                expect(cellCollection0.isFormulaCell(a("C6"))).toBeTrue();
                expect(cellCollection0.isFormulaCell(a("E5"))).toBeTrue();
                expect(cellCollection0.isFormulaCell(a("F6"))).toBeTrue();
            });
            it("should return true for cells in a matrix formula", () => {
                expect(cellCollection0.isFormulaCell(a("B8"))).toBeTrue();
                expect(cellCollection0.isFormulaCell(a("C9"))).toBeTrue();
                expect(cellCollection0.isFormulaCell(a("E8"))).toBeTrue();
                expect(cellCollection0.isFormulaCell(a("F9"))).toBeTrue();
            });
        });

        describe("method isAnchorCell", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("isAnchorCell");
            });
            it("should return false for blank and value cells", () => {
                expect(cellCollection1.isAnchorCell(a("A1"))).toBeFalse();
                expect(cellCollection1.isAnchorCell(a("E2"))).toBeFalse();
            });
            it("should return true for normal formula cells", () => {
                expect(cellCollection0.isAnchorCell(a("B2"))).toBeTrue();
                expect(cellCollection0.isAnchorCell(a("C3"))).toBeTrue();
                expect(cellCollection0.isAnchorCell(a("E2"))).toBeTrue();
                expect(cellCollection0.isAnchorCell(a("F3"))).toBeTrue();
            });
            it("should return true for anchor cells of a shared formula", () => {
                expect(cellCollection0.isAnchorCell(a("B5"))).toBeTrue();
                expect(cellCollection0.isAnchorCell(a("C6"))).toBeFalse();
                expect(cellCollection0.isAnchorCell(a("E5"))).toBeTrue();
                expect(cellCollection0.isAnchorCell(a("F6"))).toBeFalse();
            });
            it("should return true for anchor cells of a matrix formula", () => {
                expect(cellCollection0.isAnchorCell(a("B8"))).toBeTrue();
                expect(cellCollection0.isAnchorCell(a("C9"))).toBeFalse();
                expect(cellCollection0.isAnchorCell(a("E8"))).toBeTrue();
                expect(cellCollection0.isAnchorCell(a("F9"))).toBeFalse();
            });
        });

        describe("method getTokenArray", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("getTokenArray");
            });
            it("should return null for blank and value cells", () => {
                expectTokenDesc(cellCollection1, "A1", null, null);
                expectTokenDesc(cellCollection1, "E2", null, null);
            });
            it("should return the data for normal formula cells", () => {
                expectTokenDesc(cellCollection0, "B2", null, "B2", null, null);
                expectTokenDesc(cellCollection0, "B2", { grammarType: "op" }, "B2", null, "E2+1");
                expectTokenDesc(cellCollection0, "F3", { grammarType: "op" }, "F3", null, "Sheet2!F3+8");
            });
            it("should return the data for cells in a shared formula", () => {
                expectTokenDesc(cellCollection0, "B5", { grammarType: "op" }, "B5", null, "E2");
                expectTokenDesc(cellCollection0, "F6", { grammarType: "op" }, "E5", null, "Sheet2!F3");
            });
            it("should return the data for cells in a matrix formula", () => {
                expectTokenDesc(cellCollection0, "B8", { grammarType: "op" }, "B8", "B8:C9", "E2:F3");
                expectTokenDesc(cellCollection0, "F9", { grammarType: "op" }, null);
                expectTokenDesc(cellCollection0, "F9", { fullMatrix: true }, "E8", "E8:F9", null);
                expectTokenDesc(cellCollection0, "F9", { grammarType: "op", fullMatrix: true }, "E8", "E8:F9", "Sheet2!E2:F3");
            });
        });

        describe("method getFormula", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("getFormula");
            });
            it("should return null for blank and value cells", () => {
                expect(cellCollection1.getFormula(a("A1"), "op")).toBeNull();
                expect(cellCollection1.getFormula(a("E2"), "op")).toBeNull();
            });
            it("should return the data for normal formula cells", () => {
                expect(cellCollection0.getFormula(a("B2"), "op")).toBe("E2+1");
                expect(cellCollection0.getFormula(a("F3"), "op")).toBe("Sheet2!F3+8");
            });
            it("should return the data for cells in a shared formula", () => {
                expect(cellCollection0.getFormula(a("B5"), "op")).toBe("E2");
                expect(cellCollection0.getFormula(a("F6"), "op")).toBe("Sheet2!F3");
            });
            it("should return the data for cells in a matrix formula", () => {
                expect(cellCollection0.getFormula(a("B8"), "op")).toBe("E2:F3");
                expect(cellCollection0.getFormula(a("F9"), "op")).toBeNull();
                expect(cellCollection0.getFormula(a("F9"), "op", { fullMatrix: true })).toBe("Sheet2!E2:F3");
            });
        });

        describe("method getMatrixRange", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("getMatrixRange");
            });
            it("should return the matrix range", () => {
                expect(cellCollection0.getMatrixRange(a("A1"))).toBeNull();
                expect(cellCollection0.getMatrixRange(a("B8"))).toStringifyTo("B8:C9");
                expect(cellCollection0.getMatrixRange(a("F9"))).toStringifyTo("E8:F9");
            });
        });

        describe("method coversAnyMatrixRange", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("coversAnyMatrixRange");
            });
            it("should return whether the range overlaps with a matrix", () => {
                expect(cellCollection0.coversAnyMatrixRange(r("A1:B4"))).toBeFalse();
                expect(cellCollection0.coversAnyMatrixRange(r("B8:B9"))).toBeTrue();
                expect(cellCollection0.coversAnyMatrixRange(r("C9:D10"))).toBeTrue();
            });
        });

        describe("method generateCellsOperations", () => {
            it("should update shared formula when shortening the bounding range", async () => {
                await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateCellsOperations(generator, [{ address: a("B6"), f: "1+1" }, { address: a("C6"), f: null }]);
                }, { applyImmediately: true });
                sheetTester0.formulas({
                    B5: { f: "E2",  si: 0,    sr: "B5:C5", mr: null },
                    B6: { f: "1+1", si: null, sr: null,    mr: null },
                    C6: { f: null,  si: null, sr: null,    mr: null }
                });
                await docModel.undoManager.undo();
                sheetTester0.formulas({
                    B5: { f: "E2",  si: 0, sr: "B5:C6", mr: null },
                    B6: { f: null,  si: 0, sr: null,    mr: null },
                    C6: { f: null,  si: 0, sr: null,    mr: null }
                });
            });
            it("should update shared formula when expanding the bounding range", async () => {
                await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateCellOperations(generator, a("B7"), { si: 0 });
                }, { applyImmediately: true });
                sheetTester0.formulas({
                    B5: { f: "E2", si: 0, sr: "B5:C7", mr: null },
                    B7: { f: null, si: 0, sr: null,    mr: null }
                });
                await docModel.undoManager.undo();
                sheetTester0.cell("B5").formula({ f: "E2", si: 0, sr: "B5:C6", mr: null });
                sheetTester0.cell("B7").undefined();
            });
            it("should update shared formula when deleting the anchor cell", async () => {
                await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateCellOperations(generator, a("E5"), { f: null });
                }, { applyImmediately: true });
                sheetTester0.formulas({
                    E5: { f: null,        si: null, sr: null,    mr: null },
                    F5: { f: "Sheet2!F2", si: 1,    sr: "E5:F6", mr: null },
                    E6: { f: null,        si: 1,    sr: null,    mr: null },
                    F6: { f: null,        si: 1,    sr: null,    mr: null }
                });
                await docModel.undoManager.undo();
                sheetTester0.formulas({
                    E5: { f: "Sheet2!E2", si: 1, sr: "E5:F6", mr: null },
                    F5: { f: null,        si: 1, sr: null,    mr: null },
                    E6: { f: null,        si: 1, sr: null,    mr: null },
                    F6: { f: null,        si: 1, sr: null,    mr: null }
                });
            });
        });
    });
});
