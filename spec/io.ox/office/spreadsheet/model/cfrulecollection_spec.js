/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";
import { getLocalTodayAsUTC } from "@/io.ox/office/tk/utils/dateutils";
import { BaseFormatter } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import { Color } from "@/io.ox/office/editframework/utils/color";
import { SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";
import { CFRuleCollection } from "@/io.ox/office/spreadsheet/model/cfrulecollection";

import { a, aa, r, ra, ErrorCode, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/cfrulecollection", () => {

    // class CFRuleCollection -------------------------------------------------

    describe("class CFRuleCollection", () => {

        it("should subclass SheetChildModel", () => {
            expect(CFRuleCollection).toBeSubClassOf(SheetChildModel);
        });

        // a formatter needed for date/time tests
        const formatter = new BaseFormatter("ooxml");

        function getDate(days, months) {
            const date = getLocalTodayAsUTC();
            if (is.number(days)) {
                date.setUTCDate(date.getUTCDate() + days);
            }
            if (months) {
                date.setUTCMonth(date.getUTCMonth() + months);
            }
            return formatter.convertDateToNumber(date);
        }

        function mergeResults(docModel, ...cfResults) {
            let cellAttrSet = {};
            for (const cfResult of cfResults) {
                cellAttrSet = docModel.sheetAttrPool.extendAttrSet(cfResult.cellAttrSet, cellAttrSet);
            }
            return { cellAttrSet };
        }

        function getColorResult(type, value) {
            return { cellAttrSet: { cell: { fillColor: { type, value } } } };
        }

        function getColorScaleResult(color) {
            return { renderProps: { colorScale: { color } } };
        }

        function getDataBarResult(hideValue, dataBar) {
            return { renderProps: { hideValue, dataBar } };
        }

        // the operations to be applied at the test document
        const { RED, GREEN, BLUE } = Color;
        const ACCENT2_BORDER = { width: 26, style: "single", color: Color.ACCENT2 };
        const ACCENT2_ATTRS = { cell: { fillColor: Color.ACCENT2 } };
        const ACCENT3_ATTRS = { cell: { fillColor: Color.ACCENT3 } };
        const ACCENT4_ATTRS = { cell: { fillColor: Color.ACCENT4 } };
        const ACCENT5_ATTRS = { cell: { fillColor: Color.ACCENT5 } };
        const ACCENT6_ATTRS = { cell: { fillColor: Color.ACCENT6 } };
        const OPERATIONS = [
            op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),

            op.insertSheet(0, "Sheet1"),
            op.changeCells(0, "A1", [
                [55,      null, 0.12,    null, "Hallo", null, "2hallo",             null, getDate(-1),       null, 5,     null, 84,      null, "Hallo", null, ErrorCode.DIV0, null, 100, null, 88],
                ["Hallo", null, 500,     null, "Toll",  null, "dies ist ein text",  null, getDate(-3),       null, 568,   null, 99,      null, "Du",    null, null,           null, 6,   null, 90],
                [5,       null, 89,      null, null,    null, null,                 null, getDate(7),        null, -41,   null, 358,     null, 1,       null, "halo",         null, 8,   null, 0],
                [60,      null, null,    null, 1000,    null, 2,                    null, getDate(null, -1), null, 88,    null, 0,       null, null,    null, "",             null, 7,   null, -1],
                [-10,     null, '"200"', null, -5,      null, " dies ist ein text", null, "Da",              null, '"0"', null, '"600"', null, "",      null, " "],
                [66,      null, -10,     null, 0.005,   null, "haus",               null, null,              null, null,  null, null,    null, "Du"]
            ]),
            op.insertCFRule(0, "R00", "A1:A6", { value1: 'OR(A1=55,A1="Hallo",A1>60)', priority: 37, attrs: ACCENT6_ATTRS }),
            op.insertCFRule(0, "R01", "C1:C6", { type: "notBetween", value1: "400", value2: "900", priority: 38, attrs: ACCENT6_ATTRS }),
            op.insertCFRule(0, "R02", "C1:C6", { type: "between", value1: "0", value2: "300", priority: 39, attrs: { cell: { borderLeft: ACCENT2_BORDER, borderTop: ACCENT2_BORDER, borderRight: ACCENT2_BORDER, borderBottom: ACCENT2_BORDER } } }),
            op.insertCFRule(0, "R03", "E1:E6", { type: "equal", value1: '"toll"', priority: 36, attrs: ACCENT2_ATTRS }),
            op.insertCFRule(0, "R04", "E1:E6", { type: "less", value1: "20", priority: 35, attrs: ACCENT6_ATTRS }),
            op.insertCFRule(0, "R05", "G1:G6", { type: "contains", value1: '"2"', priority: 34, attrs: ACCENT6_ATTRS }),
            op.insertCFRule(0, "R06", "G1:G6", { type: "beginsWith", value1: '"dies"', priority: 33, attrs: ACCENT2_ATTRS }),
            op.insertCFRule(0, "R07", "I1:I6", { type: "yesterday", priority: 29, attrs: ACCENT6_ATTRS }),
            op.insertCFRule(0, "R08", "I1:I6", { type: "last7Days", priority: 31, attrs: ACCENT5_ATTRS }),
            op.insertCFRule(0, "R09", "I1:I6", { type: "lastMonth", priority: 32, attrs: ACCENT3_ATTRS }),
            op.insertCFRule(0, "R0A", "I1:I6", { type: "nextWeek", priority: 30, attrs: ACCENT4_ATTRS }),
            op.insertCFRule(0, "R0B", "K1:K5", { type: "atMostAverage", priority: 28, attrs: ACCENT5_ATTRS }),
            op.insertCFRule(0, "R0C", "K1:K5", { type: "aboveAverage", value1: 1, priority: 26, attrs: ACCENT4_ATTRS }),
            op.insertCFRule(0, "R0D", "M1:M5", { type: "topN", value1: 2, priority: 12, attrs: ACCENT6_ATTRS }),
            op.insertCFRule(0, "R0E", "M1:M5", { type: "bottomPercent", value1: 30, priority: 11, attrs: ACCENT2_ATTRS }),
            op.insertCFRule(0, "R0F", "O1:O6", { type: "duplicate", priority: 8, attrs: ACCENT6_ATTRS }),
            op.insertCFRule(0, "R10", "O1:O6", { type: "unique", priority: 7, attrs: ACCENT2_ATTRS }),
            op.insertCFRule(0, "R11", "Q1:Q6", { type: "blank", priority: 4, attrs: ACCENT2_ATTRS }),
            op.insertCFRule(0, "R12", "Q1:Q6", { type: "error", priority: 3, attrs: ACCENT6_ATTRS }),
            op.insertCFRule(0, "R13", "U1:U4", { type: "greater", value1: "80", priority: 4, attrs: { character: { bold: true, italic: false }, ...ACCENT6_ATTRS } }),
            op.insertCFRule(0, "R14", "S1:S4", { type: "equal", value1: "8", priority: 3, attrs: ACCENT5_ATTRS }),
            op.insertCFRule(0, "R15", "S1:S4", { type: "greater", value1: "10", priority: 2, attrs: ACCENT6_ATTRS }),
            op.insertCFRule(0, "R16", "S1:S4 U1:U4", { type: "greaterEqual", value1: "90", priority: 1, stop: true, attrs: ACCENT2_ATTRS }),

            op.insertSheet(1, "Sheet2"),
            op.changeCells(1, "A1", [
                [-80, -40, 0, "a", true, null, 30, 70],
                [-80, -40, 0, 40,  80],
                [-80, -40, 0, 40,  80],
                [-80, -40, 0, 40,  80]
            ]),
            op.insertCFRule(1, "R00", "A1:H1", { type: "colorScale", colorScale: [{ t: "min", c: RED }, { t: "max", c: BLUE }] }),
            op.insertCFRule(1, "R01", "A2:F2", { type: "colorScale", colorScale: [{ t: "formula", v: "-(2^7)", c: RED }, { t: "formula", v: "2^7-1", c: BLUE }] }),
            op.insertCFRule(1, "R02", "A3:F3", { type: "colorScale", colorScale: [{ t: "formula", v: "40", c: RED }, { t: "formula", v: "-40", c: BLUE }] }),
            op.insertCFRule(1, "R03", "A4:F4", { type: "colorScale", colorScale: [{ t: "max", c: RED }, { t: "min", c: GREEN }, { t: "formula", v: "0", c: BLUE }] }),

            op.insertSheet(2, "Sheet3"),
            op.changeCells(2, "A1", [
                [-80, -40, 0, "a", true, null, 40, 80],
                [-80, -40, 0, 40,  80],
                [-80, -40, 0, 40,  80],
                [-80, -40, 0, 40,  80],
                [-80, -40, 0, 40,  80]
            ]),
            op.insertCFRule(2, "R00", "A1:H1", { type: "dataBar", dataBar: { r1: { t: "min" }, r2: { t: "max" }, c: RED, s: true } }),
            op.insertCFRule(2, "R01", "A2:E2", { type: "dataBar", dataBar: { r1: { t: "formula", v: "-60" }, r2: { t: "formula", v: "$H$1*0.75" }, c: RED, s: false } }),
            op.insertCFRule(2, "R02", "A3:E3", { type: "dataBar", dataBar: { r1: { t: "percent", v: "25" }, r2: { t: "percent", v: "$H$1-5" }, c: RED, s: true } }),
            op.insertCFRule(2, "R03", "A4:E4", { type: "dataBar", dataBar: { r1: { t: "percentile", v: "25" }, r2: { t: "percentile", v: "$H$1-5" }, c: RED, s: true } }),
            op.insertCFRule(2, "R04", "A5:E5", { type: "dataBar", dataBar: { r1: { t: "percentile", v: "25" }, r2: { t: "percentile", v: "$H$1-5" }, c: RED, s: true, g: true } }),

            op.insertSheet(3, "Sheet4"),
            op.insertCFRule(3, "R00", "B3:D5", { value1: "A3=1", attrs: ACCENT2_ATTRS }),
            op.insertCFRule(3, "R01", "D5:F7", { type: "between", value1: "$A$3=1", value2: "A3=1", attrs: ACCENT3_ATTRS })
        ];

        // initialize test document
        const docApp = createSpreadsheetApp("ooxml", OPERATIONS);

        const ORANGE_BORDER = {
            cellAttrSet: { cell: {
                borderBottom: { color: Color.ACCENT2, style: "single", width: 26 },
                borderLeft: { color: Color.ACCENT2, style: "single", width: 26 },
                borderRight: { color: Color.ACCENT2, style: "single", width: 26 },
                borderTop: { color: Color.ACCENT2, style: "single", width: 26 }
            } }
        };
        const GREEN_BG = getColorResult("scheme", "accent6");
        const ORANGE_BG = getColorResult("scheme", "accent2");
        const BLUE_BG = getColorResult("scheme", "accent5");
        const YELLOW_BG = getColorResult("scheme", "accent4");
        const GRAY_BG = getColorResult("scheme", "accent3");
        const BOLD = { cellAttrSet: { character: { bold: true, italic: false } } };

        describe("method resolveRules", () => {

            it("should exist", () => {
                expect(CFRuleCollection).toHaveMethod("resolveRules");
            });

            it("should render formulas", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(0).cfRuleCollection;
                expect(cfCollection.resolveRules(a("A1"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("A2"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("A3"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("A4"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("A5"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("A6"))).toContainPropsEqual(GREEN_BG);
            });
            it("should render intervals", async () => {
                const { docModel } = await docApp;
                const cfCollection = docModel.getSheetModel(0).cfRuleCollection;
                expect(cfCollection.resolveRules(a("C1"))).toContainPropsEqual(mergeResults(docModel, GREEN_BG, ORANGE_BORDER));
                expect(cfCollection.resolveRules(a("C2"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("C3"))).toContainPropsEqual(mergeResults(docModel, GREEN_BG, ORANGE_BORDER));
                expect(cfCollection.resolveRules(a("C4"))).toContainPropsEqual(mergeResults(docModel, GREEN_BG, ORANGE_BORDER));
                expect(cfCollection.resolveRules(a("C5"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("C6"))).toContainPropsEqual(GREEN_BG);
            });
            it("should render compare", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(0).cfRuleCollection;
                expect(cfCollection.resolveRules(a("E1"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("E2"))).toContainPropsEqual(ORANGE_BG);
                expect(cfCollection.resolveRules(a("E3"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("E4"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("E5"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("E6"))).toContainPropsEqual(GREEN_BG);
            });
            it("should render text", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(0).cfRuleCollection;
                expect(cfCollection.resolveRules(a("G1"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("G2"))).toContainPropsEqual(ORANGE_BG);
                expect(cfCollection.resolveRules(a("G3"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("G4"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("G5"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("G6"))).toBeUndefined();
            });
            it("should render date", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(0).cfRuleCollection;
                expect(cfCollection.resolveRules(a("I1"))).toContainPropsEqual(GREEN_BG); // yesterday
                expect(cfCollection.resolveRules(a("I2"))).toContainPropsEqual(BLUE_BG); // last7Days
                expect(cfCollection.resolveRules(a("I3"))).toContainPropsEqual(YELLOW_BG); // next Week
                expect(cfCollection.resolveRules(a("I4"))).toContainPropsEqual(GRAY_BG); // last Month
                expect(cfCollection.resolveRules(a("I5"))).toBeUndefined();
            });
            it("should render average", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(0).cfRuleCollection;
                expect(cfCollection.resolveRules(a("K1"))).toContainPropsEqual(BLUE_BG);
                expect(cfCollection.resolveRules(a("K2"))).toContainPropsEqual(YELLOW_BG);
                expect(cfCollection.resolveRules(a("K3"))).toContainPropsEqual(BLUE_BG);
                expect(cfCollection.resolveRules(a("K4"))).toContainPropsEqual(BLUE_BG);
                expect(cfCollection.resolveRules(a("K5"))).toBeUndefined();
            });
            it("should render top/bottom", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(0).cfRuleCollection;
                expect(cfCollection.resolveRules(a("M1"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("M2"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("M3"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("M4"))).toContainPropsEqual(ORANGE_BG);
                expect(cfCollection.resolveRules(a("M5"))).toBeUndefined();
            });
            it("should render quantity", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(0).cfRuleCollection;
                expect(cfCollection.resolveRules(a("O1"))).toContainPropsEqual(ORANGE_BG);
                expect(cfCollection.resolveRules(a("O2"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("O3"))).toContainPropsEqual(ORANGE_BG);
                expect(cfCollection.resolveRules(a("O4"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("O5"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("O6"))).toContainPropsEqual(GREEN_BG);
            });
            it("should render type", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(0).cfRuleCollection;
                expect(cfCollection.resolveRules(a("Q1"))).toContainPropsEqual(GREEN_BG);
                expect(cfCollection.resolveRules(a("Q2"))).toContainPropsEqual(ORANGE_BG);
                expect(cfCollection.resolveRules(a("Q3"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("Q4"))).toContainPropsEqual(ORANGE_BG);
                expect(cfCollection.resolveRules(a("Q5"))).toContainPropsEqual(ORANGE_BG);
                expect(cfCollection.resolveRules(a("Q6"))).toContainPropsEqual(ORANGE_BG);
            });
            it("should render rule over two ranges and stop", async () => {
                const { docModel } = await docApp;
                const cfCollection = docModel.getSheetModel(0).cfRuleCollection;
                expect(cfCollection.resolveRules(a("S1"))).toContainPropsEqual(ORANGE_BG);
                expect(cfCollection.resolveRules(a("S2"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("S3"))).toContainPropsEqual(BLUE_BG);
                expect(cfCollection.resolveRules(a("S4"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("U1"))).toContainPropsEqual(mergeResults(docModel, GREEN_BG, BOLD));
                expect(cfCollection.resolveRules(a("U2"))).toContainPropsEqual(ORANGE_BG);
                expect(cfCollection.resolveRules(a("U3"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("U4"))).toBeUndefined();
            });

            it("should resolve color scales", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(1).cfRuleCollection;
                // minimum/maximum
                expect(cfCollection.resolveRules(a("A1"))).toContainPropsEqual(getColorScaleResult("#ff0000"));
                expect(cfCollection.resolveRules(a("B1"))).toContainPropsEqual(getColorScaleResult("#bb0044"));
                expect(cfCollection.resolveRules(a("C1"))).toContainPropsEqual(getColorScaleResult("#770088"));
                expect(cfCollection.resolveRules(a("D1"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("E1"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("F1"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("G1"))).toContainPropsEqual(getColorScaleResult("#4400bb"));
                expect(cfCollection.resolveRules(a("H1"))).toContainPropsEqual(getColorScaleResult("#0000ff"));
                // calculated start/end
                expect(cfCollection.resolveRules(a("A2"))).toContainPropsEqual(getColorScaleResult("#cf0030"));
                expect(cfCollection.resolveRules(a("B2"))).toContainPropsEqual(getColorScaleResult("#a70058"));
                expect(cfCollection.resolveRules(a("C2"))).toContainPropsEqual(getColorScaleResult("#7f0080"));
                expect(cfCollection.resolveRules(a("D2"))).toContainPropsEqual(getColorScaleResult("#5700a8"));
                expect(cfCollection.resolveRules(a("E2"))).toContainPropsEqual(getColorScaleResult("#2f00d0"));
                // values outside interval; swapped minimum/maximum
                expect(cfCollection.resolveRules(a("A3"))).toContainPropsEqual(getColorScaleResult("#ff0000"));
                expect(cfCollection.resolveRules(a("B3"))).toContainPropsEqual(getColorScaleResult("#ff0000"));
                expect(cfCollection.resolveRules(a("C3"))).toContainPropsEqual(getColorScaleResult("#800080"));
                expect(cfCollection.resolveRules(a("D3"))).toContainPropsEqual(getColorScaleResult("#0000ff"));
                expect(cfCollection.resolveRules(a("E3"))).toContainPropsEqual(getColorScaleResult("#0000ff"));
                // three-step with unordered color stops
                expect(cfCollection.resolveRules(a("A4"))).toContainPropsEqual(getColorScaleResult("#ff0000"));
                expect(cfCollection.resolveRules(a("B4"))).toContainPropsEqual(getColorScaleResult("#808000"));
                expect(cfCollection.resolveRules(a("C4"))).toContainPropsEqual(getColorScaleResult("#00ff00"));
                expect(cfCollection.resolveRules(a("D4"))).toContainPropsEqual(getColorScaleResult("#008080"));
                expect(cfCollection.resolveRules(a("E4"))).toContainPropsEqual(getColorScaleResult("#0000ff"));
            });

            it("should resolve data bar rules", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(2).cfRuleCollection;
                expect(cfCollection.resolveRules(a("A1"))).toContainPropsEqual(getDataBarResult(false, { min: -80, max: 80, num: -80, color1: "#ff0000", color2: null }));
                expect(cfCollection.resolveRules(a("B1"))).toContainPropsEqual(getDataBarResult(false, { min: -80, max: 80, num: -40, color1: "#ff0000", color2: null }));
                expect(cfCollection.resolveRules(a("C1"))).toContainPropsEqual(getDataBarResult(false, { min: -80, max: 80, num: 0, color1: "#ff0000", color2: null }));
                expect(cfCollection.resolveRules(a("D1"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("E1"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("F1"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("G1"))).toContainPropsEqual(getDataBarResult(false, { min: -80, max: 80, num: 40, color1: "#ff0000", color2: null }));
                expect(cfCollection.resolveRules(a("H1"))).toContainPropsEqual(getDataBarResult(false, { min: -80, max: 80, num: 80, color1: "#ff0000", color2: null }));
                expect(cfCollection.resolveRules(a("I1"))).toBeUndefined();
                expect(cfCollection.resolveRules(a("A2"))).toContainPropsEqual(getDataBarResult(true, { min: -60, max: 60, num: -80, color1: "#ff0000", color2: null }));
                expect(cfCollection.resolveRules(a("E2"))).toContainPropsEqual(getDataBarResult(true, { min: -60, max: 60, num: 80, color1: "#ff0000", color2: null }));
                expect(cfCollection.resolveRules(a("A3"))).toContainPropsEqual(getDataBarResult(false, { min: -40, max: 40, num: -80, color1: "#ff0000", color2: null }));
                expect(cfCollection.resolveRules(a("E3"))).toContainPropsEqual(getDataBarResult(false, { min: -40, max: 40, num: 80, color1: "#ff0000", color2: null }));
                expect(cfCollection.resolveRules(a("A4"))).toContainPropsEqual(getDataBarResult(false, { min: -40, max: 40, num: -80, color1: "#ff0000", color2: null }));
                expect(cfCollection.resolveRules(a("E4"))).toContainPropsEqual(getDataBarResult(false, { min: -40, max: 40, num: 80, color1: "#ff0000", color2: null }));
                expect(cfCollection.resolveRules(a("E5"))).toContainPropsEqual(getDataBarResult(false, { min: -40, max: 40, num: 80, color1: "#ff0000", color2: "hsl(0,100%,85%)" }));
            });
        });

        describe("method serializePasteItems", () => {
            it("should exist", () => {
                expect(CFRuleCollection).toHaveMethod("serializePasteItems");
            });
            it("should serialize covered rules", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(3).cfRuleCollection;
                const pasteItems = cfCollection.serializePasteItems(r("C4:E6"));
                expect(pasteItems).toBeArrayOfSize(2);
                expect(pasteItems[0]).toEqual({ id: "R00", ranges: "A1:B2", ref: "B3", attrs: { type: "formula", value1: "A3=1", attrs: ACCENT2_ATTRS } });
                expect(pasteItems[1]).toEqual({ id: "R01", ranges: "B2:C3", ref: "D5", attrs: { type: "between", value1: "$A$3=1", value2: "A3=1", attrs: ACCENT3_ATTRS } });
            });
        });

        describe("method parsePasteItems", () => {
            it("should exist", () => {
                expect(CFRuleCollection).toHaveMethod("parsePasteItems");
            });
            it("should parse rule data", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(3).cfRuleCollection;
                const pasteItems = cfCollection.parsePasteItems([
                    { id: "R00", ranges: "A1:B2", ref: "B3", attrs: { type: "formula", value1: "A3=1", attrs: ACCENT2_ATTRS } },
                    { id: "R01", ranges: "B2:C3", ref: "D5", attrs: { type: "between", value1: "$A$3=1", value2: "A3=1", attrs: ACCENT3_ATTRS } }
                ]);
                expect(pasteItems).toBeArrayOfSize(2);
                expect(pasteItems[0]).toEqual({ opId: "R00", sourceRanges: ra("A1:B2"), refAddress: a("B3"), ruleAttrs: { type: "formula", value1: "A3=1", attrs: ACCENT2_ATTRS } });
                expect(pasteItems[1]).toEqual({ opId: "R01", sourceRanges: ra("B2:C3"), refAddress: a("D5"), ruleAttrs: { type: "between", value1: "$A$3=1", value2: "A3=1", attrs: ACCENT3_ATTRS } });
            });
            it("should skip invalid data", async () => {
                const cfCollection = (await docApp).docModel.getSheetModel(3).cfRuleCollection;
                expect(cfCollection.parsePasteItems(null)).toEqual([]);
                expect(cfCollection.parsePasteItems({})).toEqual([]);
                expect(cfCollection.parsePasteItems([42, "abc"])).toEqual([]);
            });
        });

        describe("method generateUpdateTaskOperations", () => {
            it("should exist", () => {
                expect(CFRuleCollection).toHaveMethod("generateUpdateTaskOperations");
            });
        });

        describe("method generatePasteOperations", () => {
            it("should exist", () => {
                expect(CFRuleCollection).toHaveMethod("generatePasteOperations");
            });
            it("should extend existing rules when pasting from same sheet", async () => {
                const { docModel } = await docApp;
                const sheetModel = docModel.getSheetModel(3);
                const pasteItem = { opId: "R00", sourceRanges: ra("A2 B1"), refAddress: a("B3") };
                await sheetModel.createAndApplyOperations(generator => sheetModel.cfRuleCollection.generatePasteOperations(generator, ra("C2:D5"), aa("C2 C4"), [pasteItem], true, true));
                docModel.expect.sheet(3)
                    .cfRule("R00", "D2 B3:C3 B4 D4 B5:C5", { cfrule: { value1: "A2=1", attrs: ACCENT2_ATTRS } })
                    .cfRule("R01", "E5:F5 D6:F7", { cfrule: { value1: "$A$3=1", attrs: ACCENT3_ATTRS } });
                await docModel.undoManager.undo();
                docModel.expect.sheet(3)
                    .cfRule("R00", "B3:D5", { cfrule: { value1: "A3=1", attrs: ACCENT2_ATTRS } })
                    .cfRule("R01", "D5:F7", { cfrule: { value1: "$A$3=1", attrs: ACCENT3_ATTRS } });
            });
            it("should create new rules when pasting from another sheet", async () => {
                const { docModel } = await docApp;
                const sheetModel = docModel.getSheetModel(3);
                const pasteItem = { opId: "R00", sourceRanges: ra("A2 B1"), refAddress: a("B3"), ruleAttrs: { value1: "A3=1", attrs: ACCENT2_ATTRS } };
                await sheetModel.createAndApplyOperations(generator => sheetModel.cfRuleCollection.generatePasteOperations(generator, ra("C2:D5"), aa("C2 C4"), [pasteItem], true, false));
                const ruleId = Array.from(sheetModel.cfRuleCollection.yieldRuleModels(), ruleModel => ruleModel.opId).find(opId => !opId.startsWith("R"));
                docModel.expect.sheet(3)
                    .cfRuleCount(3)
                    .cfRule("R00", "B3:B5", { cfrule: { value1: "A3=1", attrs: ACCENT2_ATTRS } })
                    .cfRule("R01", "E5:F5 D6:F7", { cfrule: { value1: "$A$3=1", attrs: ACCENT3_ATTRS } })
                    .cfRule(ruleId, "D2 C3 D4 C5", { cfrule: { value1: "B2=1", attrs: ACCENT2_ATTRS } });
                await docModel.undoManager.undo();
                docModel.expect.sheet(3)
                    .cfRuleCount(2)
                    .cfRule("R00", "B3:D5", { cfrule: { value1: "A3=1", attrs: ACCENT2_ATTRS } })
                    .cfRule("R01", "D5:F7", { cfrule: { value1: "$A$3=1", attrs: ACCENT3_ATTRS } })
                    .noCfRule(ruleId);
            });
            it("should remove covered rules", async () => {
                const { docModel } = await docApp;
                const sheetModel = docModel.getSheetModel(3);
                await sheetModel.createAndApplyOperations(generator => sheetModel.cfRuleCollection.generatePasteOperations(generator, ra("B2:D6"), aa("B2"), [], true, true));
                docModel.expect.sheet(3)
                    .cfRuleCount(1)
                    .noCfRule("R00")
                    .cfRule("R01", "E5:F6 D7:F7", { cfrule: { value1: "$A$3=1", attrs: ACCENT3_ATTRS } });
                await docModel.undoManager.undo();
                docModel.expect.sheet(3)
                    .cfRuleCount(2)
                    .cfRule("R00", "B3:D5", { cfrule: { value1: "A3=1", attrs: ACCENT2_ATTRS } })
                    .cfRule("R01", "D5:F7", { cfrule: { value1: "$A$3=1", attrs: ACCENT3_ATTRS } });
            });
        });
    });
});
