/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { NameCollection } from "@/io.ox/office/spreadsheet/model/namecollection";

import { ErrorCode, waitForRecalcEnd, createSpreadsheetApp, defineModelTestWithUndo } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/namecollection", () => {

    const FMLA = "$A$1&name&[0]!name&Sheet2!name&Sheet3!name";

    // initialize test document
    const appPromise = createSpreadsheetApp("ooxml", [

        op.changeConfig({ document: { cols: 16384, rows: 1048576, calcOnLoad: true } }),
        op.insertName("name", { formula: '"G0"' }),
        op.insertName("global", { formula: FMLA }),

        op.insertSheet(0, "Sheet1"),
        op.insertName("local", 0, { formula: FMLA }),
        op.changeCells(0, {
            A1: { v: "C0" },
            A2: { v: 0, f: FMLA },
            A3: { v: 0, f: "_xlfn.FORMULATEXT(A2)" },
            A4: { v: 0, f: "global" },
            A5: { v: 0, f: "local" }
        }),
        op.insertDVRule(0, 0, "B1:B3", { type: "between", value1: FMLA, value2: FMLA }),
        op.insertCFRule(0, "R0", "B1:B3", { type: "between", value1: FMLA, value2: FMLA }),

        op.insertSheet(1, "Sheet2"),
        op.insertName("name", 1, { formula: '"L1"' }),
        op.insertName("local", 1, { formula: FMLA }),
        op.changeCells(1, {
            A1: { v: "C1" },
            A2: { v: 0, f: FMLA },
            A3: { v: 0, f: "_xlfn.FORMULATEXT(A2)" },
            A4: { v: 0, f: "global" },
            A5: { v: 0, f: "local" }
        }),
        op.insertDVRule(1, 0, "B1:B3", { type: "between", value1: FMLA, value2: FMLA }),
        op.insertCFRule(1, "R0", "B1:B3", { type: "between", value1: FMLA, value2: FMLA }),

        op.insertSheet(2, "Sheet3"),
        op.insertName("name", 2, { formula: '"L2"' }),
        op.changeCells(2, {
            A1: { v: "C2" },
            A2: { v: 0, f: FMLA },
            A3: { v: 0, f: "_xlfn.FORMULATEXT(A2)" },
            A4: { v: 0, f: "global" }
        }),
        op.insertDVRule(2, 0, "B1:B3", { type: "between", value1: FMLA, value2: FMLA }),
        op.insertCFRule(2, "R0", "B1:B3", { type: "between", value1: FMLA, value2: FMLA })
    ]);

    // expects that the entire document is in initial state as after import
    async function expectInitialState(docModel) {
        docModel.expect.defName("name").noDefName("name2").defName("global", FMLA);
        docModel.expect.sheet(0)
            .defName("local", FMLA)
            .formulas("A2", FMLA)
            .dvRule(0, "B1:B3", { dvrule: { value1: FMLA, value2: FMLA } })
            .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA, value2: FMLA } });
        docModel.expect.sheet(1)
            .defName("name").noDefName("name2").defName("local", FMLA)
            .formulas("A2", FMLA)
            .dvRule(0, "B1:B3", { dvrule: { value1: FMLA, value2: FMLA } })
            .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA, value2: FMLA } });
        docModel.expect.sheet(2)
            .defName("name").noDefName("name2")
            .formulas("A2", FMLA)
            .dvRule(0, "B1:B3", { dvrule: { value1: FMLA, value2: FMLA } })
            .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA, value2: FMLA } });
        await waitForRecalcEnd(docModel);
        docModel.expect.sheet(0).values("A2", ["C0G0G0L1L2", "=" + FMLA, "C0G0G0L1L2", "C0G0G0L1L2"]);
        docModel.expect.sheet(1).values("A2", ["C1L1G0L1L2", "=" + FMLA, "C1L1G0L1L2", "C1L1G0L1L2"]);
        docModel.expect.sheet(2).values("A2", ["C2L2G0L1L2", "=" + FMLA, "C2L2G0L1L2"]);
    }

    // class NameCollection ---------------------------------------------------

    describe("class NameCollection", () => {
        it("should subclass ModelObject", () => {
            expect(NameCollection).toBeSubClassOf(ModelObject);
        });

        describe("method generateChangeNameOperations", () => {
            it("should exist", () => {
                expect(NameCollection).toHaveMethod("generateChangeNameOperations");
            });

            describe("should update formulas after relabeling a globally defined name", () => {

                function buildOperations(docModel) {
                    return docModel.buildOperations(builder => {
                        return docModel.nameCollection.generateChangeNameOperations(builder, "name", "name2", null);
                    });
                }

                async function expectChangedState(docModel) {
                    const FMLA1 = "$A$1&name&[0]!name2&Sheet2!name&Sheet3!name";
                    const FMLA2 = "$A$1&name2&[0]!name2&Sheet2!name&Sheet3!name";
                    docModel.expect.defName("name2").noDefName("name").defName("global", FMLA2);
                    docModel.expect.sheet(0)
                        .defName("local", FMLA2)
                        .formulas("A2", FMLA2)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA2, value2: FMLA2 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA2, value2: FMLA2 } });
                    docModel.expect.sheet(1)
                        .defName("name").noDefName("name2").defName("local", FMLA1)
                        .formulas("A2", FMLA1)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA1, value2: FMLA1 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA1, value2: FMLA1 } });
                    docModel.expect.sheet(2)
                        .defName("name").noDefName("name2")
                        .formulas("A2", FMLA1)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA1, value2: FMLA1 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA1, value2: FMLA1 } });
                    await waitForRecalcEnd(docModel);
                    docModel.expect.sheet(0).values("A2", ["C0G0G0L1L2", "=" + FMLA2, "C0G0G0L1L2", "C0G0G0L1L2"]);
                    docModel.expect.sheet(1).values("A2", ["C1L1G0L1L2", "=" + FMLA1, "C1G0G0L1L2", "C1L1G0L1L2"]);
                    docModel.expect.sheet(2).values("A2", ["C2L2G0L1L2", "=" + FMLA1, "C2G0G0L1L2"]);
                }

                defineModelTestWithUndo(appPromise, buildOperations, expectChangedState, expectInitialState, { redo: true });
            });

            describe("should update formulas after relabeling a locally defined name", () => {

                function buildOperations(docModel) {
                    return docModel.buildOperations(builder => {
                        return docModel.getSheetModel(1).nameCollection.generateChangeNameOperations(builder, "name", "name2", null);
                    });
                }

                async function expectChangedState(docModel) {
                    const FMLA1 = "$A$1&name&[0]!name&Sheet2!name2&Sheet3!name";
                    const FMLA2 = "$A$1&name2&[0]!name&Sheet2!name2&Sheet3!name";
                    docModel.expect.defName("name").noDefName("name2").defName("global", FMLA1);
                    docModel.expect.sheet(0)
                        .defName("local", FMLA1)
                        .formulas("A2", FMLA1)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA1, value2: FMLA1 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA1, value2: FMLA1 } });
                    docModel.expect.sheet(1)
                        .defName("name2").noDefName("name").defName("local", FMLA2)
                        .formulas("A2", FMLA2)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA2, value2: FMLA2 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA2, value2: FMLA2 } });
                    docModel.expect.sheet(2)
                        .defName("name").noDefName("name2")
                        .formulas("A2", FMLA1)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA1, value2: FMLA1 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA1, value2: FMLA1 } });
                    await waitForRecalcEnd(docModel);
                    docModel.expect.sheet(0).values("A2", ["C0G0G0L1L2", "=" + FMLA1, "C0G0G0L1L2", "C0G0G0L1L2"]);
                    docModel.expect.sheet(1).values("A2", ["C1L1G0L1L2", "=" + FMLA2, "C1G0G0L1L2", "C1L1G0L1L2"]);
                    docModel.expect.sheet(2).values("A2", ["C2L2G0L1L2", "=" + FMLA1, "C2L2G0L1L2"]);
                }

                defineModelTestWithUndo(appPromise, buildOperations, expectChangedState, expectInitialState, { redo: true });
            });
        });

        describe("method generateDeleteNameOperations", () => {
            it("should exist", () => {
                expect(NameCollection).toHaveMethod("generateDeleteNameOperations");
            });

            describe("should recalculate formulas after deleting a globally defined name", () => {

                function buildOperations(docModel) {
                    return docModel.buildOperations(builder => {
                        return docModel.nameCollection.generateDeleteNameOperations(builder, "name");
                    });
                }

                async function expectChangedState(docModel) {
                    docModel.expect.noDefName("name");
                    docModel.expect.sheet(1).defName("name");
                    docModel.expect.sheet(2).defName("name");
                    await waitForRecalcEnd(docModel);
                    docModel.expect.sheet(0).values("A2", [ErrorCode.NAME, "=" + FMLA, ErrorCode.NAME, ErrorCode.NAME]);
                    docModel.expect.sheet(1).values("A2", [ErrorCode.NAME, "=" + FMLA, ErrorCode.NAME, ErrorCode.NAME]);
                    docModel.expect.sheet(2).values("A2", [ErrorCode.NAME, "=" + FMLA, ErrorCode.NAME]);
                }

                defineModelTestWithUndo(appPromise, buildOperations, expectChangedState, expectInitialState, { redo: true });
            });

            describe("should update formulas after deleting a locally defined name", () => {

                function buildOperations(docModel) {
                    return docModel.buildOperations(builder => {
                        return docModel.getSheetModel(1).nameCollection.generateDeleteNameOperations(builder, "name");
                    });
                }

                async function expectChangedState(docModel) {
                    docModel.expect.defName("name");
                    docModel.expect.sheet(1).noDefName("name");
                    docModel.expect.sheet(2).defName("name");
                    await waitForRecalcEnd(docModel);
                    docModel.expect.sheet(0).values("A2", [ErrorCode.NAME, "=" + FMLA, ErrorCode.NAME, ErrorCode.NAME]);
                    docModel.expect.sheet(1).values("A2", [ErrorCode.NAME, "=" + FMLA, ErrorCode.NAME, ErrorCode.NAME]);
                    docModel.expect.sheet(2).values("A2", [ErrorCode.NAME, "=" + FMLA, ErrorCode.NAME]);
                }

                defineModelTestWithUndo(appPromise, buildOperations, expectChangedState, expectInitialState, { redo: true });
            });
        });
    });
});
