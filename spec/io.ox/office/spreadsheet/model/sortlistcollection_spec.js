/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { SortListCollection } from "@/io.ox/office/spreadsheet/model/sortlistcollection";

import { createSpreadsheetApp } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/sortlistcollection", () => {

    // class SortListCollection -----------------------------------------------

    describe("class SortListCollection", () => {

        it("should subclass ModelObject", () => {
            expect(SortListCollection).toBeSubClassOf(ModelObject);
        });

        // initialize test document
        let sortListCollection;
        createSpreadsheetApp("ooxml").done(docApp => {
            sortListCollection = docApp.docModel.sortListCollection;
        });

        describe("method hasList", () => {
            it("should exist", () => {
                expect(SortListCollection).toHaveMethod("hasList");
            });
            it("should return true for existing lists", () => {
                expect(sortListCollection.hasList("jan")).toBeTrue();
                expect(sortListCollection.hasList("Jan")).toBeTrue();
                expect(sortListCollection.hasList("Januar")).toBeTrue();
                expect(sortListCollection.hasList("mo")).toBeTrue();
                expect(sortListCollection.hasList("MONTAG")).toBeTrue();
            });
            it("should return false for unknown values", () => {
                expect(sortListCollection.hasList("")).toBeFalse();
                expect(sortListCollection.hasList("_unknown_")).toBeFalse();
            });
        });

        describe("method findLists", () => {
            it("should exist", () => {
                expect(SortListCollection).toHaveMethod("findLists");
            });
            it("should return existing lists", () => {
                expect(sortListCollection.findLists("jan")).toBeArrayOfSize(1);
                expect(sortListCollection.findLists("Jan")).toBeArrayOfSize(1);
                expect(sortListCollection.findLists("Januar")).toBeArrayOfSize(1);
                expect(sortListCollection.findLists("mo")).toBeArrayOfSize(1);
                expect(sortListCollection.findLists("MONTAG")).toBeArrayOfSize(1);
            });
            it("should return undefined for unknown values", () => {
                expect(sortListCollection.findLists("")).toBeUndefined();
                expect(sortListCollection.findLists("_unknown_")).toBeUndefined();
            });
        });

        describe("method insertList", () => {
            it("should exist", () => {
                expect(SortListCollection).toHaveMethod("insertList");
            });
            it("should add a new list", () => {
                expect(sortListCollection.hasList("my")).toBeFalse();
                const sortList = sortListCollection.insertList(["my", "Custom", "LIST", "for", "august"]);
                expect(sortList).toBeArray();
                expect(sortListCollection.hasList("CUSTOM")).toBeTrue();
                expect(sortListCollection.hasList("List")).toBeTrue();
                // returns built-in list first
                const sortLists = sortListCollection.findLists("august");
                expect(sortLists).toBeArrayOfSize(2);
                expect(sortLists[0]).not.toBe(sortList);
                expect(sortLists[1]).toBe(sortList);
            });
        });

        describe("method sortLists", () => {
            it("should exist", () => {
                expect(SortListCollection).toHaveMethod("sortLists");
            });
            it("should return an iterator", () => {
                expect(sortListCollection.sortLists()).toBeIterator();
            });
        });

        describe("method sortListEntries", () => {
            it("should exist", () => {
                expect(SortListCollection).toHaveMethod("sortListEntries");
            });
            it("should return an iterator", () => {
                expect(sortListCollection.sortListEntries()).toBeIterator();
            });
        });
    });
});
