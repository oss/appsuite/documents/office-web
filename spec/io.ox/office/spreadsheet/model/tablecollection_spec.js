/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { AttributedModel } from "@/io.ox/office/editframework/model/attributedmodel";
import { TableColumnModel } from "@/io.ox/office/spreadsheet/model/tablecolumnmodel";
import { TableModel } from "@/io.ox/office/spreadsheet/model/tablemodel";
import { TableCollection } from "@/io.ox/office/spreadsheet/model/tablecollection";

import { a, r, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/tablecollection", () => {

    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertSheet(0, "Sheet1"),
        op.insertSheet(1, "Sheet2"),
        op.changeCells(0, {
            B2: "Name",    C2: "Age", D2: "City",
            B3: "Miri",    C3: 30,    D3: "Hamburg",
            B4: "Michael", C4: 32,    D4: "Hamburg",
            B5: "Dennis",  C5: 32,    D5: "Heide",
            B6: "Horst",   C6: 33,    D6: "Ostrohe",
            C7: { f: "SUM(C3:C6)/4", v: 31.75 }
        }),
        op.insertTable(0, "Table1", "B2:D7", { styleId: "TableStyleMedium9", table: { headerRow: true, footerRow: true, bandsHor: true } }),
        op.changeTableCol(0, "Table1", 1, { attrs: { filter: { type: "discrete", entries: ["32", "33"] } } }),
        op.changeTableCol(0, "Table1", 2, { attrs: { sort: { type: "value" } } }),
        op.insertTable(0, null, "B10:D12", { table: { headerRow: true } })
    ];

    // initialize test document
    let tableCollection0, tableCollection1;
    createSpreadsheetApp("ooxml", OPERATIONS).done(function (docApp) {
        tableCollection0 = docApp.docModel.getSheetModel(0).tableCollection;
        tableCollection1 = docApp.docModel.getSheetModel(1).tableCollection;
    });

    // class TableCollection --------------------------------------------------

    describe("class TableCollection", () => {

        it("should subclass ModelObject", () => {
            expect(TableCollection).toBeSubClassOf(ModelObject);
        });

        describe("method hasTable", () => {
            it("should exist", () => {
                expect(TableCollection).toHaveMethod("hasTable");
            });
            it("should return true for existing table", () => {
                expect(tableCollection0.hasTable("Table1")).toBeTrue();
                expect(tableCollection0.hasTable("table1")).toBeTrue();
                expect(tableCollection0.hasTable("TABLE1")).toBeTrue();
            });
            it("should return false for nonexisting table", () => {
                expect(tableCollection0.hasTable("Table2")).toBeFalse();
            });
        });

        describe("method hasAutoFilter", () => {
            it("should exist", () => {
                expect(TableCollection).toHaveMethod("hasAutoFilter");
            });
            it("should return whether the collection contains an auto-filter", () => {
                expect(tableCollection0.hasAutoFilter()).toBeTrue();
                expect(tableCollection1.hasAutoFilter()).toBeFalse();
            });
        });

        describe("method getTableModel", () => {
            it("should exist", () => {
                expect(TableCollection).toHaveMethod("getTableModel");
            });
            it("should return the table model", () => {
                expect(tableCollection0.getTableModel("Table1")).toBeInstanceOf(TableModel);
            });
            it("should return undefined for invalid names", () => {
                expect(tableCollection0.getTableModel("Table2")).toBeUndefined();
                expect(tableCollection1.getTableModel("Table1")).toBeUndefined();
            });
        });

        describe("method getAutoFilterModel", () => {
            it("should exist", () => {
                expect(TableCollection).toHaveMethod("getAutoFilterModel");
            });
            it("should return the model of an auto-filter", () => {
                expect(tableCollection0.getAutoFilterModel()).toBeInstanceOf(TableModel);
                expect(tableCollection1.getAutoFilterModel()).toBeUndefined();
            });
        });

        describe("method getTableModelAt", () => {
            it("should exist", () => {
                expect(TableCollection).toHaveMethod("getTableModelAt");
            });
            it("should find a table model for the given address", () => {
                expect(tableCollection0.getTableModelAt(a("A1"))).toBeUndefined();
                expect(tableCollection0.getTableModelAt(a("B4"))).toBeInstanceOf(TableModel);
            });
        });

        describe("method yieldTableModels", () => {
            it("should exist", () => {
                expect(TableCollection).toHaveMethod("yieldTableModels");
            });
            it("should return all table models", () => {
                expect(Array.from(tableCollection0.yieldTableModels())).toHaveLength(1);
                expect(Array.from(tableCollection0.yieldTableModels({ autoFilter: true }))).toHaveLength(2);
            });
            it("should return all table models in a specific range", () => {
                expect(Array.from(tableCollection0.yieldTableModels({ overlapRange: r("A1:C1") }))).toHaveLength(0);
                expect(Array.from(tableCollection0.yieldTableModels({ overlapRange: r("C5:D11") }))).toHaveLength(1);
                expect(Array.from(tableCollection0.yieldTableModels({ overlapRange: r("C5:D11"), autoFilter: true }))).toHaveLength(2);
            });
        });

        describe("method generateInsertTableOperations", () => {
            it("should exist", () => {
                expect(TableCollection).toHaveMethod("generateInsertTableOperations");
            });
        });

        describe("method generateUpdateTaskOperations", () => {
            it("should exist", () => {
                expect(TableCollection).toHaveMethod("generateUpdateTaskOperations");
            });
        });
    });

    // class TableModel -------------------------------------------------------

    describe("class TableModel", () => {

        let tableModel;
        beforeAll(() => {
            tableModel = tableCollection0.getTableModel("Table1");
        });

        it("should subclass AttributedModel", () => {
            expect(TableModel).toBeSubClassOf(AttributedModel);
        });

        describe("method isRefreshable", () => {
            it("should exist", () => {
                expect(TableModel).toHaveMethod("isRefreshable");
            });
            it("should return whether the table is refreshable or not", () => {
                expect(tableModel.isRefreshable()).toBeTrue();
            });
        });

        describe("method getColumnContents", () => {
            it("should exist", () => {
                expect(TableModel).toHaveMethod("getColumnContents");
            });
            it("should return a array with the content of the given column", () => {
                expect(tableModel.getColumnContents(0)).toHaveLength(4);
            });
        });

        describe("method getColumnModel", () => {
            it("should exist", () => {
                expect(TableModel).toHaveMethod("getColumnModel");
            });
            it("should return the column model", () => {
                expect(tableModel.getColumnModel(0)).toBeInstanceOf(TableColumnModel);
            });
        });

        describe("method isSorted", () => {
            it("should exist", () => {
                expect(TableModel).toHaveMethod("isSorted");
            });
            it("should return the boolean sort-state of the table", () => {
                expect(tableModel.isSorted()).toBeTrue();
            });
        });

        describe("method getSortRules", () => {
            it("should exist", () => {
                expect(TableModel).toHaveMethod("getSortRules");
            });
            it("should return sorting rules array", () => {
                expect(tableModel.getSortRules()).toBeArray();
            });
        });
    });
});
