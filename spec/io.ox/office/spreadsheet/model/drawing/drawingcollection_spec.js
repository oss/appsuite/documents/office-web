/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { IndexedCollection } from "@/io.ox/office/drawinglayer/model/indexedcollection";
import { getModel } from "@/io.ox/office/drawinglayer/view/drawingframe";
import { SheetDrawingCollection } from "@/io.ox/office/spreadsheet/model/drawing/drawingcollection";

import { createSpreadsheetApp } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/drawing/drawingcollection", () => {

    // the operations to be applied by the document model
    const OPERATIONS = [
        { name: "setDocumentAttributes", attrs: { document: { cols: 16384, rows: 1048576 } } },
        { name: "insertSheet", sheet: 0, sheetName: "Sheet1" },
        { name: "insertDrawing", start: [0, 0], type: "image", attrs: { drawing: { anchor: "2 B2 0 0 C3 0 0" } } },
        { name: "insertDrawing", start: [0, 1], type: "shape", attrs: { drawing: { anchor: "2 B2 0 0 C3 0 0" }, geometry: { presetShape: "rect" } } },
        { name: "insertParagraph", start: [0, 1, 0], attrs: { paragraph: { alignment: "left" } } },
        { name: "insertText", start: [0, 1, 0, 0], text: "First line." },
        { name: "insertParagraph", start: [0, 1, 1], attrs: { paragraph: { alignment: "right" } } },
        { name: "insertText", start: [0, 1, 1, 0], text: "Second line." },
        { name: "insertSheet", sheet: 1, sheetName: "Sheet2" }
    ];

    // initialize test document
    let docModel = null;
    let sheetModel1 = null, drawingColl1 = null;
    let sheetModel2 = null, drawingColl2 = null;
    createSpreadsheetApp("ooxml", OPERATIONS).done(app => {
        docModel = app.getModel();
        sheetModel1 = docModel.getSheetModel(0);
        drawingColl1 = sheetModel1.drawingCollection;
        sheetModel2 = docModel.getSheetModel(1);
        drawingColl2 = sheetModel2.drawingCollection;
    });

    function expectChildNode(parentNode, testNode) {
        expect(testNode).toBeInstanceOf(HTMLElement);
        expect($.contains(parentNode, testNode)).toBeTrue();
    }

    // class SheetDrawingCollection -------------------------------------------

    describe("class SheetDrawingCollection", () => {

        it("should subclass IndexedCollection", () => {
            expect(SheetDrawingCollection).toBeSubClassOf(IndexedCollection);
        });

        describe("method getContainerNode", () => {
            it("should exist", () => {
                expect(SheetDrawingCollection).toHaveMethod("getContainerNode");
            });
            it("should return the DOM container node", () => {
                const pageNode = docModel.getNode()[0];
                const node1 = drawingColl1.getContainerNode();
                expectChildNode(pageNode, node1);
                const node2 = drawingColl2.getContainerNode();
                expectChildNode(pageNode, node2);
                expect(node1).not.toBe(node2);
            });
        });

        describe("method getDrawingFrameForModel", () => {
            it("should exist", () => {
                expect(SheetDrawingCollection).toHaveMethod("getDrawingFrameForModel");
            });
            it("should return the frame node for images", () => {
                const imageModel = drawingColl1.getModel([0]);
                const imageFrame = drawingColl1.getDrawingFrameForModel(imageModel);
                expectChildNode(drawingColl1.getContainerNode(), imageFrame[0]);
                expect(getModel(imageFrame)).toBe(imageModel);
            });
            it("should return the frame node for shapes", () => {
                const shapeModel = drawingColl1.getModel([1]);
                const shapeFrame = drawingColl1.getDrawingFrameForModel(shapeModel);
                expectChildNode(drawingColl1.getContainerNode(), shapeFrame[0]);
                expect(getModel(shapeFrame)).toBe(shapeModel);
                const textFrame = shapeFrame.find(">*>.textframe");
                expectChildNode(shapeFrame[0], textFrame[0]);
                const paraNodes = textFrame.children();
                expect(paraNodes).toHaveLength(2);
                expect(paraNodes.eq(0).text()).toBe("First line.");
                expect(paraNodes.eq(1).text()).toBe("Second line.");
            });
        });

        describe("method getRectangleForModel", () => {
            it("should exist", () => {
                expect(SheetDrawingCollection).toHaveMethod("getRectangleForModel");
            });
        });

        describe("method getAnchorAttributesForRect", () => {
            it("should exist", () => {
                expect(SheetDrawingCollection).toHaveMethod("getAnchorAttributesForRect");
            });
        });
    });
});
