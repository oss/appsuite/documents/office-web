/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { DrawingCollection } from "@/io.ox/office/drawinglayer/model/drawingcollection";
import { NoteModel } from "@/io.ox/office/spreadsheet/model/drawing/notemodel";
import { NoteCollection } from "@/io.ox/office/spreadsheet/model/drawing/notecollection";

import { createSpreadsheetApp, a } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/drawing/notecollection", () => {

    // the operations to be applied by the document model
    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertSheet(0, "Sheet1"),
        op.insertNote(0, "A1", "note1", { drawing: { anchor: "A B2 0 0 C3 0 0" } }),
        op.insertNote(0, "B2", "note2", { drawing: { anchor: "A C3 0 0 D4 0 0" } }),
        op.insertNote(0, "B1", "note3", { drawing: { anchor: "A C2 0 0 D3 0 0" } }),
        op.insertNote(0, "A2", "note4", { drawing: { anchor: "A B3 0 0 C4 0 0" } })
    ];

    // initialize test document
    let docModel, sheetModel, noteCollection;
    createSpreadsheetApp("ooxml", OPERATIONS).done(docApp => {
        docModel = docApp.docModel;
        sheetModel = docModel.getSheetModel(0);
        noteCollection = sheetModel.noteCollection;
    });

    // class NoteCollection ---------------------------------------------------

    describe("class NoteCollection", () => {

        it("should subclass DrawingCollection", () => {
            expect(NoteCollection).toBeSubClassOf(DrawingCollection);
        });

        describe("method hasNotes", () => {
            it("should exist", () => {
                expect(NoteCollection).toHaveMethod("hasNotes");
            });
            it("should return whether notes exist", () => {
                expect(noteCollection.hasNotes()).toBeTrue();
                expect(noteCollection.hasNotes({ visible: true })).toBeTrue();
                expect(noteCollection.hasNotes({ visible: false })).toBeFalse();
            });
        });

        describe("method getNotesInfo", () => {
            it("should exist", () => {
                expect(NoteCollection).toHaveMethod("getNotesInfo");
            });
            it("should return whether notes exist", () => {
                expect(noteCollection.getNotesInfo()).toEqual({
                    hasNotes: true,
                    hasVisibleNotes: true,
                    hasHiddenNotes: false,
                    hasNotesInVisibleCells: true,
                    hasVisibleNotesInVisibleCells: true,
                    hasHiddenNotesInVisibleCells: false
                });
            });
        });

        describe("method getByAddress", () => {
            it("should exist", () => {
                expect(NoteCollection).toHaveMethod("getByAddress");
            });
            it("should return note model by anchor address", () => {
                const noteModel = noteCollection.getByAddress(a("A1"));
                expect(noteModel).toBeInstanceOf(NoteModel);
                expect(noteModel.getAnchor()).toStringifyTo("A1");
                expect(noteCollection.getByAddress(a("C1"))).toBeUndefined();
            });
        });

        describe("method generateDeleteAllNotesOperations", () => {
            it("should exist", () => {
                expect(NoteCollection).toHaveMethod("generateDeleteAllNotesOperations");
            });
            it("should generate operations to delete all notes", async () => {
                await sheetModel.buildOperations(sheetCache => noteCollection.generateDeleteAllNotesOperations(sheetCache));
                expect(noteCollection.hasNotes()).toBeFalse();
                await docModel.undoManager.undo();
                expect(noteCollection.hasNotes()).toBeTrue();
            });
        });

        describe("method clientResolveZIndex", () => {
            it("should exist", () => {
                expect(NoteCollection).toHaveMethod("clientResolveZIndex");
            });
            it("should return note model by anchor address", () => {
                const noteModel1 = noteCollection.getByAddress(a("A1"));
                expect(noteCollection.clientResolveZIndex(noteModel1)).toBe(0);
                const noteModel2 = noteCollection.getByAddress(a("B1"));
                expect(noteCollection.clientResolveZIndex(noteModel2)).toBe(1);
                const noteModel3 = noteCollection.getByAddress(a("A2"));
                expect(noteCollection.clientResolveZIndex(noteModel3)).toBe(2);
                const noteModel4 = noteCollection.getByAddress(a("B2"));
                expect(noteCollection.clientResolveZIndex(noteModel4)).toBe(3);
            });
        });
    });
});
