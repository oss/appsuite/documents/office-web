/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ShapeModel } from "@/io.ox/office/drawinglayer/model/drawingmodel";
import TitleModel from "@/io.ox/office/spreadsheet/model/drawing/chart/titlemodel";
import { XSourceLinkedModel } from "@/io.ox/office/spreadsheet/model/drawing/xsourcelinkedmodel";

// tests ======================================================================

describe("module spreadsheet/model/drawing/xsourcelinkedmodel", () => {

    it("should exist", () => {
        expect(XSourceLinkedModel).toBeObject();
    });

    // public functions -------------------------------------------------------

    describe("function XSourceLinkedModel.mixin", () => {

        it("should exist", () => {
            expect(XSourceLinkedModel).toRespondTo("mixin");
        });

        it("should create a subclass", () => {
            expect(XSourceLinkedModel.mixin(ShapeModel)).toBeSubClassOf(ShapeModel);
            expect(XSourceLinkedModel.mixin(TitleModel)).toBeSubClassOf(TitleModel);
        });
    });
});
