/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";
import { Direction } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import { TransformMode, AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";

import { i, a, r, ia, aa, ra, ca } from "~/spreadsheet/sheethelper";

const { LEFT, RIGHT, UP, DOWN } = Direction;

// private functions ==========================================================

function adjustRow(address) {
    if (address.r > 128) { address.r += (65536 - 256); }
    return address;
}

function mirrorAddress(address) {
    return adjustRow(address.mirror());
}

function mirrorRange(range) {
    range = range.mirror();
    adjustRow(range.a1);
    adjustRow(range.a2);
    return range;
}

function mirrorPoint(point) {
    point.a = mirrorAddress(point.a);
    var c = point.x;
    point.x = point.y;
    point.y = c;
}

function expectResult(result, expected) {
    if (is.nullish(expected)) {
        expect(result).toBe(expected);
    } else {
        expect(result).toRespondTo("toOpStr");
        expect(result.toOpStr()).toBe(expected);
    }
}

function expectArray(result, expected) {
    expect(result).toBeArray();
    expect(result).toRespondTo("toOpStr");
    expect(result.toOpStr()).toBe(expected);
}

// tests ======================================================================

describe("module spreadsheet/model/addresstransformer", function () {

    var factory = new AddressFactory(256, 65536);
    var insColTransformer = new AddressTransformer(factory, i("2:3"), ia("B:C E:F"), RIGHT);
    var insRowTransformer = new AddressTransformer(factory, i("B:C"), ia("2:3 5:6"), DOWN);
    var delColTransformer = new AddressTransformer(factory, i("2:3"), ia("B:C E:F"), LEFT);
    var delRowTransformer = new AddressTransformer(factory, i("B:C"), ia("2:3 5:6"), UP);

    function expectIndex(index, insert, options, expected) {
        var colTransformer = insert ? insColTransformer : delColTransformer;
        expect(colTransformer.transformIndex(index, options)).toBe(expected);
    }

    function expectIntervals(intervals, options, insert, expected) {
        intervals = ia(intervals);
        expected = ia(expected).toOpStr(false);
        var colTransformer = insert ? insColTransformer : delColTransformer;
        expectArray(colTransformer.transformIntervals(intervals, options), expected);
    }

    function expectAddress(address, insert, options, expected) {

        address = a(address);
        var colTransformer = insert ? insColTransformer : delColTransformer;
        expectResult(colTransformer.transformAddress(address, options), expected);

        address = mirrorAddress(address);
        expected = expected ? mirrorAddress(a(expected)).toOpStr() : undefined;
        var rowTransformer = insert ? insRowTransformer : delRowTransformer;
        expectResult(rowTransformer.transformAddress(address, options), expected);
    }

    function expectAddresses(addresses, insert, options, expected) {

        addresses = aa(addresses);
        var colTransformer = insert ? insColTransformer : delColTransformer;
        expectArray(colTransformer.transformAddresses(addresses, options), expected);

        addresses = addresses.map(mirrorAddress);
        expected = aa(expected).clone(mirrorAddress).toOpStr();
        var rowTransformer = insert ? insRowTransformer : delRowTransformer;
        expectArray(rowTransformer.transformAddresses(addresses, options), expected);
    }

    function expectRanges(ranges, options, insert, expected) {

        ranges = ra(ranges);
        var colTransformer = insert ? insColTransformer : delColTransformer;
        expectArray(colTransformer.transformRanges(ranges, options), expected);

        ranges = ranges.map(mirrorRange);
        expected = ra(expected).clone(mirrorRange).toOpStr();
        var rowTransformer = insert ? insRowTransformer : delRowTransformer;
        expectArray(rowTransformer.transformRanges(ranges, options), expected);
    }

    function expectCellAnchor(anchor, options, insert, expected) {

        anchor = ca(anchor);
        var colTransformer = insert ? insColTransformer : delColTransformer;
        expectResult(colTransformer.transformCellAnchor(anchor, options), expected);

        mirrorPoint(anchor.point1);
        mirrorPoint(anchor.point2);
        if (expected) {
            expected = ca(expected);
            mirrorPoint(expected.point1);
            mirrorPoint(expected.point2);
            expected = expected.toOpStr();
        }
        var rowTransformer = insert ? insRowTransformer : delRowTransformer;
        expectResult(rowTransformer.transformCellAnchor(anchor, options), expected);
    }

    // enum TransformMode -----------------------------------------------------

    describe("enum TransformMode", function () {
        it("should exist", function () {
            expect(TransformMode).toBeObject();
        });
        it("should enumerate transformation modes", function () {
            expect(TransformMode).toHaveProperty("RESIZE");
            expect(TransformMode).toHaveProperty("EXPAND");
            expect(TransformMode).toHaveProperty("STRICT");
            expect(TransformMode).toHaveProperty("SPLIT");
            expect(TransformMode).toHaveProperty("FIXED");
        });
    });

    const { RESIZE, EXPAND, STRICT, SPLIT, FIXED } = TransformMode;

    // class AddressTransformer -----------------------------------------------

    describe("class AddressTransformer", function () {

        it("should exist", function () {
            expect(AddressTransformer).toBeFunction();
        });

        describe("constructor", function () {
            it("should create a transformer for inserting multiple columns", function () {
                var transformer = new AddressTransformer(factory, i("1:3"), ia("F:I B:C C:D IK:IR IT:IU"), RIGHT);
                expect(transformer.bandInterval).toEqual(i("1:3"));
                expect(transformer.sourceIntervals).toEqual(ia("B:C C:D F:I IK:IR"));
                expect(transformer.targetIntervals).toEqual(ia("B:C E:F J:M IS:IV"));
                expect(transformer.reverseIntervals).toEqual(ia("B:C E:F J:M IS:IV"));
                expect(transformer.moveFromIntervals).toEqual(ia("B C:E F:IJ"));
                expect(transformer.moveToIntervals).toEqual(ia("D G:I N:IR"));
                expect(transformer.insertIntervals).toEqual(ia("B:C E:F J:M IS:IV"));
                expect(transformer.deleteIntervals).toEqual(ia("IK:IV"));
                expect(transformer.dirtyRange).toEqual(r("B1:IV3"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(255);
            });
            it("should create a transformer for inserting multiple rows", function () {
                var transformer = new AddressTransformer(factory, i("A:C"), ia("6:9 2:3 3:4 65525:65532 65534:65535"), DOWN);
                expect(transformer.bandInterval).toEqual(i("A:C"));
                expect(transformer.sourceIntervals).toEqual(ia("2:3 3:4 6:9 65525:65532"));
                expect(transformer.targetIntervals).toEqual(ia("2:3 5:6 10:13 65533:65536"));
                expect(transformer.reverseIntervals).toEqual(ia("2:3 5:6 10:13 65533:65536"));
                expect(transformer.moveFromIntervals).toEqual(ia("2 3:5 6:65524"));
                expect(transformer.moveToIntervals).toEqual(ia("4 7:9 14:65532"));
                expect(transformer.insertIntervals).toEqual(ia("2:3 5:6 10:13 65533:65536"));
                expect(transformer.deleteIntervals).toEqual(ia("65525:65536"));
                expect(transformer.dirtyRange).toEqual(r("A2:C65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(65535);
            });
            it("should create a transformer for deleting multiple columns", function () {
                var transformer = new AddressTransformer(factory, i("1:3"), ia("F:I B:C C:D IK:IR IU:IV"), LEFT);
                expect(transformer.bandInterval).toEqual(i("1:3"));
                expect(transformer.sourceIntervals).toEqual(ia("B:D F:I IK:IR IU:IV"));
                expect(transformer.targetIntervals).toEqual(ia("B:D F:I IK:IR IU:IV"));
                expect(transformer.reverseIntervals).toEqual(ia("B:D C:F ID:IK IF:IG"));
                expect(transformer.moveFromIntervals).toEqual(ia("E J:IJ IS:IT"));
                expect(transformer.moveToIntervals).toEqual(ia("B C:IC ID:IE"));
                expect(transformer.insertIntervals).toEqual(ia("IF:IV"));
                expect(transformer.deleteIntervals).toEqual(ia("B:D F:I IK:IR IU:IV"));
                expect(transformer.dirtyRange).toEqual(r("B1:IV3"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(255);
            });
            it("should create a transformer for deleting multiple rows", function () {
                var transformer = new AddressTransformer(factory, i("A:C"), ia("6:9 2:3 3:4 65525:65532 65535:65536"), UP);
                expect(transformer.bandInterval).toEqual(i("A:C"));
                expect(transformer.sourceIntervals).toEqual(ia("2:4 6:9 65525:65532 65535:65536"));
                expect(transformer.targetIntervals).toEqual(ia("2:4 6:9 65525:65532 65535:65536"));
                expect(transformer.reverseIntervals).toEqual(ia("2:4 3:6 65518:65525 65520:65521"));
                expect(transformer.moveFromIntervals).toEqual(ia("5 10:65524 65533:65534"));
                expect(transformer.moveToIntervals).toEqual(ia("2 3:65517 65518:65519"));
                expect(transformer.insertIntervals).toEqual(ia("65520:65536"));
                expect(transformer.deleteIntervals).toEqual(ia("2:4 6:9 65525:65532 65535:65536"));
                expect(transformer.dirtyRange).toEqual(r("A2:C65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(65535);
            });
        });

        describe("static function fromIntervals", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveStaticMethod("fromIntervals");
            });

            it("should create a transformer for inserting columns at the beginning of the sheet", function () {
                var transformer = AddressTransformer.fromIntervals(factory, i("A:B"), RIGHT);
                expect(transformer.bandInterval).toEqual(i("1:65536"));
                expect(transformer.sourceIntervals).toEqual(ia("A:B"));
                expect(transformer.targetIntervals).toEqual(ia("A:B"));
                expect(transformer.reverseIntervals).toEqual(ia("A:B"));
                expect(transformer.moveFromIntervals).toEqual(ia("A:IT"));
                expect(transformer.moveToIntervals).toEqual(ia("C:IV"));
                expect(transformer.insertIntervals).toEqual(ia("A:B"));
                expect(transformer.deleteIntervals).toEqual(ia("IU:IV"));
                expect(transformer.dirtyRange).toEqual(r("A1:IV65536"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(255);
            });
            it("should create a transformer for inserting columns at the end of the sheet", function () {
                var transformer = AddressTransformer.fromIntervals(factory, i("IU:IV"), RIGHT);
                expect(transformer.bandInterval).toEqual(i("1:65536"));
                expect(transformer.sourceIntervals).toEqual(ia("IU:IV"));
                expect(transformer.targetIntervals).toEqual(ia("IU:IV"));
                expect(transformer.reverseIntervals).toEqual(ia("IU:IV"));
                expect(transformer.moveFromIntervals).toHaveLength(0);
                expect(transformer.moveToIntervals).toHaveLength(0);
                expect(transformer.insertIntervals).toEqual(ia("IU:IV"));
                expect(transformer.deleteIntervals).toEqual(ia("IU:IV"));
                expect(transformer.dirtyRange).toEqual(r("IU1:IV65536"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(255);
            });
            it("should create a transformer for inserting multiple columns", function () {
                var transformer = AddressTransformer.fromIntervals(factory, ia("F:I B:C C:D IK:IR IT:IU"), RIGHT);
                expect(transformer.bandInterval).toEqual(i("1:65536"));
                expect(transformer.sourceIntervals).toEqual(ia("B:C C:D F:I IK:IR"));
                expect(transformer.targetIntervals).toEqual(ia("B:C E:F J:M IS:IV"));
                expect(transformer.reverseIntervals).toEqual(ia("B:C E:F J:M IS:IV"));
                expect(transformer.moveFromIntervals).toEqual(ia("B C:E F:IJ"));
                expect(transformer.moveToIntervals).toEqual(ia("D G:I N:IR"));
                expect(transformer.insertIntervals).toEqual(ia("B:C E:F J:M IS:IV"));
                expect(transformer.deleteIntervals).toEqual(ia("IK:IV"));
                expect(transformer.dirtyRange).toEqual(r("B1:IV65536"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(255);
            });

            it("should create a transformer for inserting rows at the beginning of the sheet", function () {
                var transformer = AddressTransformer.fromIntervals(factory, i("1:2"), DOWN);
                expect(transformer.bandInterval).toEqual(i("A:IV"));
                expect(transformer.sourceIntervals).toEqual(ia("1:2"));
                expect(transformer.targetIntervals).toEqual(ia("1:2"));
                expect(transformer.reverseIntervals).toEqual(ia("1:2"));
                expect(transformer.moveFromIntervals).toEqual(ia("1:65534"));
                expect(transformer.moveToIntervals).toEqual(ia("3:65536"));
                expect(transformer.insertIntervals).toEqual(ia("1:2"));
                expect(transformer.deleteIntervals).toEqual(ia("65535:65536"));
                expect(transformer.dirtyRange).toEqual(r("A1:IV65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(65535);
            });
            it("should create a transformer for inserting rows at the end of the sheet", function () {
                var transformer = AddressTransformer.fromIntervals(factory, i("65535:65536"), DOWN);
                expect(transformer.bandInterval).toEqual(i("A:IV"));
                expect(transformer.sourceIntervals).toEqual(ia("65535:65536"));
                expect(transformer.targetIntervals).toEqual(ia("65535:65536"));
                expect(transformer.reverseIntervals).toEqual(ia("65535:65536"));
                expect(transformer.moveFromIntervals).toHaveLength(0);
                expect(transformer.moveToIntervals).toHaveLength(0);
                expect(transformer.insertIntervals).toEqual(ia("65535:65536"));
                expect(transformer.deleteIntervals).toEqual(ia("65535:65536"));
                expect(transformer.dirtyRange).toEqual(r("A65535:IV65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(65535);
            });
            it("should create a transformer for inserting multiple rows", function () {
                var transformer = AddressTransformer.fromIntervals(factory, ia("6:9 2:3 3:4 65525:65532 65534:65535"), DOWN);
                expect(transformer.bandInterval).toEqual(i("A:IV"));
                expect(transformer.sourceIntervals).toEqual(ia("2:3 3:4 6:9 65525:65532"));
                expect(transformer.targetIntervals).toEqual(ia("2:3 5:6 10:13 65533:65536"));
                expect(transformer.reverseIntervals).toEqual(ia("2:3 5:6 10:13 65533:65536"));
                expect(transformer.moveFromIntervals).toEqual(ia("2 3:5 6:65524"));
                expect(transformer.moveToIntervals).toEqual(ia("4 7:9 14:65532"));
                expect(transformer.insertIntervals).toEqual(ia("2:3 5:6 10:13 65533:65536"));
                expect(transformer.deleteIntervals).toEqual(ia("65525:65536"));
                expect(transformer.dirtyRange).toEqual(r("A2:IV65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(65535);
            });

            it("should create a transformer for deleting columns at the beginning of the sheet", function () {
                var transformer = AddressTransformer.fromIntervals(factory, i("A:B"), LEFT);
                expect(transformer.bandInterval).toEqual(i("1:65536"));
                expect(transformer.sourceIntervals).toEqual(ia("A:B"));
                expect(transformer.targetIntervals).toEqual(ia("A:B"));
                expect(transformer.reverseIntervals).toEqual(ia("A:B"));
                expect(transformer.moveFromIntervals).toEqual(ia("C:IV"));
                expect(transformer.moveToIntervals).toEqual(ia("A:IT"));
                expect(transformer.insertIntervals).toEqual(ia("IU:IV"));
                expect(transformer.deleteIntervals).toEqual(ia("A:B"));
                expect(transformer.dirtyRange).toEqual(r("A1:IV65536"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(255);
            });
            it("should create a transformer for deleting columns at the end of the sheet", function () {
                var transformer = AddressTransformer.fromIntervals(factory, i("IU:IV"), LEFT);
                expect(transformer.bandInterval).toEqual(i("1:65536"));
                expect(transformer.sourceIntervals).toEqual(ia("IU:IV"));
                expect(transformer.targetIntervals).toEqual(ia("IU:IV"));
                expect(transformer.reverseIntervals).toEqual(ia("IU:IV"));
                expect(transformer.moveFromIntervals).toHaveLength(0);
                expect(transformer.moveToIntervals).toHaveLength(0);
                expect(transformer.insertIntervals).toEqual(ia("IU:IV"));
                expect(transformer.deleteIntervals).toEqual(ia("IU:IV"));
                expect(transformer.dirtyRange).toEqual(r("IU1:IV65536"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(255);
            });
            it("should create a transformer for deleting multiple columns", function () {
                var transformer = AddressTransformer.fromIntervals(factory, ia("F:I B:C C:D IK:IR IU:IV"), LEFT);
                expect(transformer.bandInterval).toEqual(i("1:65536"));
                expect(transformer.sourceIntervals).toEqual(ia("B:D F:I IK:IR IU:IV"));
                expect(transformer.targetIntervals).toEqual(ia("B:D F:I IK:IR IU:IV"));
                expect(transformer.reverseIntervals).toEqual(ia("B:D C:F ID:IK IF:IG"));
                expect(transformer.moveFromIntervals).toEqual(ia("E J:IJ IS:IT"));
                expect(transformer.moveToIntervals).toEqual(ia("B C:IC ID:IE"));
                expect(transformer.insertIntervals).toEqual(ia("IF:IV"));
                expect(transformer.deleteIntervals).toEqual(ia("B:D F:I IK:IR IU:IV"));
                expect(transformer.dirtyRange).toEqual(r("B1:IV65536"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(255);
            });

            it("should create a transformer for deleting rows at the beginning of the sheet", function () {
                var transformer = AddressTransformer.fromIntervals(factory, i("1:2"), UP);
                expect(transformer.bandInterval).toEqual(i("A:IV"));
                expect(transformer.sourceIntervals).toEqual(ia("1:2"));
                expect(transformer.targetIntervals).toEqual(ia("1:2"));
                expect(transformer.reverseIntervals).toEqual(ia("1:2"));
                expect(transformer.moveFromIntervals).toEqual(ia("3:65536"));
                expect(transformer.moveToIntervals).toEqual(ia("1:65534"));
                expect(transformer.insertIntervals).toEqual(ia("65535:65536"));
                expect(transformer.deleteIntervals).toEqual(ia("1:2"));
                expect(transformer.dirtyRange).toEqual(r("A1:IV65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(65535);
            });
            it("should create a transformer for deleting rows at the end of the sheet", function () {
                var transformer = AddressTransformer.fromIntervals(factory, i("65535:65536"), UP);
                expect(transformer.bandInterval).toEqual(i("A:IV"));
                expect(transformer.sourceIntervals).toEqual(ia("65535:65536"));
                expect(transformer.targetIntervals).toEqual(ia("65535:65536"));
                expect(transformer.reverseIntervals).toEqual(ia("65535:65536"));
                expect(transformer.moveFromIntervals).toHaveLength(0);
                expect(transformer.moveToIntervals).toHaveLength(0);
                expect(transformer.insertIntervals).toEqual(ia("65535:65536"));
                expect(transformer.deleteIntervals).toEqual(ia("65535:65536"));
                expect(transformer.dirtyRange).toEqual(r("A65535:IV65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(65535);
            });
            it("should create a transformer for deleting multiple rows", function () {
                var transformer = AddressTransformer.fromIntervals(factory, ia("6:9 2:3 3:4 65525:65532 65535:65536"), UP);
                expect(transformer.bandInterval).toEqual(i("A:IV"));
                expect(transformer.sourceIntervals).toEqual(ia("2:4 6:9 65525:65532 65535:65536"));
                expect(transformer.targetIntervals).toEqual(ia("2:4 6:9 65525:65532 65535:65536"));
                expect(transformer.reverseIntervals).toEqual(ia("2:4 3:6 65518:65525 65520:65521"));
                expect(transformer.moveFromIntervals).toEqual(ia("5 10:65524 65533:65534"));
                expect(transformer.moveToIntervals).toEqual(ia("2 3:65517 65518:65519"));
                expect(transformer.insertIntervals).toEqual(ia("65520:65536"));
                expect(transformer.deleteIntervals).toEqual(ia("2:4 6:9 65525:65532 65535:65536"));
                expect(transformer.dirtyRange).toEqual(r("A2:IV65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(65535);
            });
        });

        describe("static function fromRange", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveStaticMethod("fromRange");
            });

            it("should create a transformer for inserting columns at the beginning of the sheet", function () {
                var transformer = AddressTransformer.fromRange(factory, r("A1:B3"), RIGHT);
                expect(transformer.bandInterval).toEqual(i("1:3"));
                expect(transformer.sourceIntervals).toEqual(ia("A:B"));
                expect(transformer.targetIntervals).toEqual(ia("A:B"));
                expect(transformer.moveFromIntervals).toEqual(ia("A:IT"));
                expect(transformer.moveToIntervals).toEqual(ia("C:IV"));
                expect(transformer.insertIntervals).toEqual(ia("A:B"));
                expect(transformer.deleteIntervals).toEqual(ia("IU:IV"));
                expect(transformer.dirtyRange).toEqual(r("A1:IV3"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(255);
            });
            it("should create a transformer for inserting columns at the end of the sheet", function () {
                var transformer = AddressTransformer.fromRange(factory, r("IU1:IV3"), RIGHT);
                expect(transformer.bandInterval).toEqual(i("1:3"));
                expect(transformer.sourceIntervals).toEqual(ia("IU:IV"));
                expect(transformer.targetIntervals).toEqual(ia("IU:IV"));
                expect(transformer.moveFromIntervals).toHaveLength(0);
                expect(transformer.moveToIntervals).toHaveLength(0);
                expect(transformer.insertIntervals).toEqual(ia("IU:IV"));
                expect(transformer.deleteIntervals).toEqual(ia("IU:IV"));
                expect(transformer.dirtyRange).toEqual(r("IU1:IV3"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(255);
            });

            it("should create a transformer for inserting rows at the beginning of the sheet", function () {
                var transformer = AddressTransformer.fromRange(factory, r("A1:C2"), DOWN);
                expect(transformer.bandInterval).toEqual(i("A:C"));
                expect(transformer.sourceIntervals).toEqual(ia("1:2"));
                expect(transformer.targetIntervals).toEqual(ia("1:2"));
                expect(transformer.moveFromIntervals).toEqual(ia("1:65534"));
                expect(transformer.moveToIntervals).toEqual(ia("3:65536"));
                expect(transformer.insertIntervals).toEqual(ia("1:2"));
                expect(transformer.deleteIntervals).toEqual(ia("65535:65536"));
                expect(transformer.dirtyRange).toEqual(r("A1:C65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(65535);
            });
            it("should create a transformer for inserting rows at the end of the sheet", function () {
                var transformer = AddressTransformer.fromRange(factory, r("A65535:C65536"), DOWN);
                expect(transformer.bandInterval).toEqual(i("A:C"));
                expect(transformer.sourceIntervals).toEqual(ia("65535:65536"));
                expect(transformer.targetIntervals).toEqual(ia("65535:65536"));
                expect(transformer.moveFromIntervals).toHaveLength(0);
                expect(transformer.moveToIntervals).toHaveLength(0);
                expect(transformer.insertIntervals).toEqual(ia("65535:65536"));
                expect(transformer.deleteIntervals).toEqual(ia("65535:65536"));
                expect(transformer.dirtyRange).toEqual(r("A65535:C65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeTrue();
                expect(transformer.maxIndex).toBe(65535);
            });

            it("should create a transformer for deleting columns at the beginning of the sheet", function () {
                var transformer = AddressTransformer.fromRange(factory, r("A1:B3"), LEFT);
                expect(transformer.bandInterval).toEqual(i("1:3"));
                expect(transformer.sourceIntervals).toEqual(ia("A:B"));
                expect(transformer.targetIntervals).toEqual(ia("A:B"));
                expect(transformer.moveFromIntervals).toEqual(ia("C:IV"));
                expect(transformer.moveToIntervals).toEqual(ia("A:IT"));
                expect(transformer.insertIntervals).toEqual(ia("IU:IV"));
                expect(transformer.deleteIntervals).toEqual(ia("A:B"));
                expect(transformer.dirtyRange).toEqual(r("A1:IV3"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(255);
            });
            it("should create a transformer for deleting columns at the end of the sheet", function () {
                var transformer = AddressTransformer.fromRange(factory, r("IU1:IV3"), LEFT);
                expect(transformer.bandInterval).toEqual(i("1:3"));
                expect(transformer.sourceIntervals).toEqual(ia("IU:IV"));
                expect(transformer.targetIntervals).toEqual(ia("IU:IV"));
                expect(transformer.moveFromIntervals).toHaveLength(0);
                expect(transformer.moveToIntervals).toHaveLength(0);
                expect(transformer.insertIntervals).toEqual(ia("IU:IV"));
                expect(transformer.deleteIntervals).toEqual(ia("IU:IV"));
                expect(transformer.dirtyRange).toEqual(r("IU1:IV3"));
                expect(transformer.columns).toBeTrue();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(255);
            });

            it("should create a transformer for deleting rows at the beginning of the sheet", function () {
                var transformer = AddressTransformer.fromRange(factory, r("A1:C2"), UP);
                expect(transformer.bandInterval).toEqual(i("A:C"));
                expect(transformer.sourceIntervals).toEqual(ia("1:2"));
                expect(transformer.targetIntervals).toEqual(ia("1:2"));
                expect(transformer.moveFromIntervals).toEqual(ia("3:65536"));
                expect(transformer.moveToIntervals).toEqual(ia("1:65534"));
                expect(transformer.insertIntervals).toEqual(ia("65535:65536"));
                expect(transformer.deleteIntervals).toEqual(ia("1:2"));
                expect(transformer.dirtyRange).toEqual(r("A1:C65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(65535);
            });
            it("should create a transformer for deleting rows at the end of the sheet", function () {
                var transformer = AddressTransformer.fromRange(factory, r("A65535:C65536"), UP);
                expect(transformer.bandInterval).toEqual(i("A:C"));
                expect(transformer.sourceIntervals).toEqual(ia("65535:65536"));
                expect(transformer.targetIntervals).toEqual(ia("65535:65536"));
                expect(transformer.moveFromIntervals).toHaveLength(0);
                expect(transformer.moveToIntervals).toHaveLength(0);
                expect(transformer.insertIntervals).toEqual(ia("65535:65536"));
                expect(transformer.deleteIntervals).toEqual(ia("65535:65536"));
                expect(transformer.dirtyRange).toEqual(r("A65535:C65536"));
                expect(transformer.columns).toBeFalse();
                expect(transformer.insert).toBeFalse();
                expect(transformer.maxIndex).toBe(65535);
            });
        });

        describe("method bandContainsAddress", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("bandContainsAddress");
            });
            it("should return whether the address is covered", function () {
                expect(insColTransformer.bandContainsAddress(a("A1"))).toBeFalse();
                expect(insColTransformer.bandContainsAddress(a("A2"))).toBeTrue();
                expect(insColTransformer.bandContainsAddress(a("A3"))).toBeTrue();
                expect(insColTransformer.bandContainsAddress(a("A4"))).toBeFalse();

                expect(insRowTransformer.bandContainsAddress(a("A1"))).toBeFalse();
                expect(insRowTransformer.bandContainsAddress(a("B1"))).toBeTrue();
                expect(insRowTransformer.bandContainsAddress(a("C1"))).toBeTrue();
                expect(insRowTransformer.bandContainsAddress(a("D1"))).toBeFalse();
            });
        });

        describe("method bandContainsRange", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("bandContainsRange");
            });
            it("should return whether the range is covered", function () {
                expect(insColTransformer.bandContainsRange(r("A1:A4"))).toBeFalse();
                expect(insColTransformer.bandContainsRange(r("A1:A2"))).toBeFalse();
                expect(insColTransformer.bandContainsRange(r("A2:A3"))).toBeTrue();
                expect(insColTransformer.bandContainsRange(r("A3:A4"))).toBeFalse();
                expect(insColTransformer.bandContainsRange(r("A1:A1"))).toBeFalse();
                expect(insColTransformer.bandContainsRange(r("A2:A2"))).toBeTrue();
                expect(insColTransformer.bandContainsRange(r("A3:A3"))).toBeTrue();
                expect(insColTransformer.bandContainsRange(r("A4:A4"))).toBeFalse();

                expect(insRowTransformer.bandContainsRange(r("A1:D1"))).toBeFalse();
                expect(insRowTransformer.bandContainsRange(r("A1:B1"))).toBeFalse();
                expect(insRowTransformer.bandContainsRange(r("B1:C1"))).toBeTrue();
                expect(insRowTransformer.bandContainsRange(r("C1:D1"))).toBeFalse();
                expect(insRowTransformer.bandContainsRange(r("A1:A1"))).toBeFalse();
                expect(insRowTransformer.bandContainsRange(r("B1:B1"))).toBeTrue();
                expect(insRowTransformer.bandContainsRange(r("C1:C1"))).toBeTrue();
                expect(insRowTransformer.bandContainsRange(r("D1:D1"))).toBeFalse();
            });
        });

        describe("method bandOverlapsRange", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("bandOverlapsRange");
            });
            it("should return whether the range overlaps", function () {
                expect(insColTransformer.bandOverlapsRange(r("A1:A4"))).toBeTrue();
                expect(insColTransformer.bandOverlapsRange(r("A1:A2"))).toBeTrue();
                expect(insColTransformer.bandOverlapsRange(r("A2:A3"))).toBeTrue();
                expect(insColTransformer.bandOverlapsRange(r("A3:A4"))).toBeTrue();
                expect(insColTransformer.bandOverlapsRange(r("A4:A5"))).toBeFalse();
                expect(insColTransformer.bandOverlapsRange(r("A1:A1"))).toBeFalse();
                expect(insColTransformer.bandOverlapsRange(r("A2:A2"))).toBeTrue();
                expect(insColTransformer.bandOverlapsRange(r("A3:A3"))).toBeTrue();
                expect(insColTransformer.bandOverlapsRange(r("A4:A4"))).toBeFalse();

                expect(insRowTransformer.bandOverlapsRange(r("A1:D1"))).toBeTrue();
                expect(insRowTransformer.bandOverlapsRange(r("A1:B1"))).toBeTrue();
                expect(insRowTransformer.bandOverlapsRange(r("B1:C1"))).toBeTrue();
                expect(insRowTransformer.bandOverlapsRange(r("C1:D1"))).toBeTrue();
                expect(insRowTransformer.bandOverlapsRange(r("D1:E1"))).toBeFalse();
                expect(insRowTransformer.bandOverlapsRange(r("A1:A1"))).toBeFalse();
                expect(insRowTransformer.bandOverlapsRange(r("B1:B1"))).toBeTrue();
                expect(insRowTransformer.bandOverlapsRange(r("C1:C1"))).toBeTrue();
                expect(insRowTransformer.bandOverlapsRange(r("D1:D1"))).toBeFalse();
            });
        });

        describe("method createBandLineRange", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("createBandLineRange");
            });
            it("should create a range address for columns", function () {
                expect(insColTransformer.createBandLineRange(0)).toEqual(r("A2:A3"));
                expect(insColTransformer.createBandLineRange(8)).toEqual(r("I2:I3"));
            });
            it("should create a range address for rows", function () {
                expect(insRowTransformer.createBandLineRange(0)).toEqual(r("B1:C1"));
                expect(insRowTransformer.createBandLineRange(8)).toEqual(r("B9:C9"));
            });
        });

        describe("method createBandRange", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("createBandRange");
            });
            it("should create a range address for columns", function () {
                expect(insColTransformer.createBandRange(i("A:B"))).toEqual(r("A2:B3"));
                expect(insColTransformer.createBandRange(i("H:I"))).toEqual(r("H2:I3"));
            });
            it("should create a range address for rows", function () {
                expect(insRowTransformer.createBandRange(i("1:2"))).toEqual(r("B1:C2"));
                expect(insRowTransformer.createBandRange(i("8:9"))).toEqual(r("B8:C9"));
            });
        });

        describe("method createBandRanges", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("createBandRanges");
            });
            it("should create an array of range addresses for columns", function () {
                expect(insColTransformer.createBandRanges(ia("A:B C:D"))).toEqual(ra("A2:B3 C2:D3"));
                expect(insColTransformer.createBandRanges(i("H:I"))).toEqual(ra("H2:I3"));
            });
            it("should create an array of range addresses for rows", function () {
                expect(insRowTransformer.createBandRanges(ia("1:2 3:4"))).toEqual(ra("B1:C2 B3:C4"));
                expect(insRowTransformer.createBandRanges(i("8:9"))).toEqual(ra("B8:C9"));
            });
        });

        describe("method transformIndex", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("transformIndex");
            });
            it("should transform index when inserting columns or rows", function () {
                expectIndex(0,   true, null, 0);
                expectIndex(1,   true, null, 3);
                expectIndex(2,   true, null, 4);
                expectIndex(3,   true, null, 5);
                expectIndex(4,   true, null, 8);
                expectIndex(5,   true, null, 9);
                expectIndex(6,   true, null, 10);
                expectIndex(251, true, null, 255);
                expectIndex(252, true, null);
            });
            it("should transform address when deleting columns or rows", function () {
                expectIndex(0,   false, null, 0);
                expectIndex(1,   false, null);
                expectIndex(2,   false, null);
                expectIndex(3,   false, null, 1);
                expectIndex(4,   false, null);
                expectIndex(5,   false, null);
                expectIndex(6,   false, null, 2);
                expectIndex(255, false, null, 251);
            });
            it("should transform address when inserting columns or rows (move mode)", function () {
                expectIndex(0,   true, { move: true }, 0);
                expectIndex(1,   true, { move: true }, 3);
                expectIndex(2,   true, { move: true }, 4);
                expectIndex(3,   true, { move: true }, 5);
                expectIndex(4,   true, { move: true }, 8);
                expectIndex(5,   true, { move: true }, 9);
                expectIndex(6,   true, { move: true }, 10);
                expectIndex(251, true, { move: true }, 255);
                expectIndex(252, true, { move: true }, 255);
            });
            it("should transform address when deleting columns or rows (move mode)", function () {
                expectIndex(0,   false, { move: true }, 0);
                expectIndex(1,   false, { move: true }, 1);
                expectIndex(2,   false, { move: true }, 1);
                expectIndex(3,   false, { move: true }, 1);
                expectIndex(4,   false, { move: true }, 2);
                expectIndex(5,   false, { move: true }, 2);
                expectIndex(6,   false, { move: true }, 2);
                expectIndex(255, false, { move: true }, 251);
            });
        });

        describe("method transformIntervals", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("transformIntervals");
            });

            it("should transform index intervals when inserting columns or rows", function () {

                // transform intervals
                expectIntervals("A A:B A:C A:D A:E A:F A:G", null, true, "A A:D A:E A:F A:I A:J A:K");
                expectIntervals("B B:C B:D B:E B:F B:G",     null, true, "D D:E D:F D:I D:J D:K");
                expectIntervals("C C:D C:E C:F C:G",         null, true, "E E:F E:I E:J E:K");
                expectIntervals("D D:E D:F D:G",             null, true, "F F:I F:J F:K");
                expectIntervals("E E:F E:G",                 null, true, "I I:J I:K");
                expectIntervals("F F:G",                     null, true, "J J:K");
                expectIntervals("G",                         null, true, "K");

                // transformation modes
                expectIntervals("A:C A:D A:E", null,                      true, "A:E A:F A:I");
                expectIntervals("A:C A:D A:E", { transformMode: RESIZE }, true, "A:E A:F A:I");
                expectIntervals("A:C A:D A:E", { transformMode: EXPAND }, true, "A:E A:H A:I");
                expectIntervals("A:C A:D A:E", { transformMode: STRICT }, true, "A:E A:F A:I");
                expectIntervals("A:C A:D A:E", { transformMode: SPLIT },  true, "A D:E A D:F A D:F I");
                expectIntervals("A:C B:D C:E", { transformMode: FIXED },  true, "A:C D:F E:G");

                // cap at end of sheet (`STRICT` mode: delete instead of capping)
                expectIntervals("IO:IQ IP:IR IQ:IS IR:IT IS:IU", null,                      true, "IS:IU IT:IV IU:IV IV");
                expectIntervals("IO:IQ IP:IR IQ:IS IR:IT IS:IU", { transformMode: STRICT }, true, "IS:IU IT:IV");

                // do not skip full intervals
                expectIntervals("A:IV", null,                      true, "A:IV");
                expectIntervals("A:IV", { transformMode: RESIZE }, true, "A:IV");
                expectIntervals("A:IV", { transformMode: EXPAND }, true, "A:IV");
                expectIntervals("A:IV", { transformMode: STRICT }, true, "");
                expectIntervals("A:IV", { transformMode: SPLIT },  true, "A D:F I:IV");
                expectIntervals("A:IV", { transformMode: FIXED },  true, "A:IV");
            });

            it("should transform index intervals when deleting columns or rows", function () {

                // transform intervals
                expectIntervals("A A:B A:C A:D A:E A:F A:G A:H", null, false, "A A A A:B A:B A:B A:C A:D");
                expectIntervals("B B:C B:D B:E B:F B:G B:H",     null, false, "B B B B:C B:D");
                expectIntervals("C C:D C:E C:F C:G C:H",         null, false, "B B B B:C B:D");
                expectIntervals("D D:E D:F D:G D:H",             null, false, "B B B B:C B:D");
                expectIntervals("E E:F E:G E:H",                 null, false, "C C:D");
                expectIntervals("F F:G F:H",                     null, false, "C C:D");
                expectIntervals("G G:H",                         null, false, "C C:D");
                expectIntervals("H",                             null, false, "D");

                // transformation modes
                expectIntervals("A:B B:C C:D D:E E:F F:G G:H H:I", null,                      false, "A B B C C:D D:E");
                expectIntervals("A:B B:C C:D D:E E:F F:G G:H H:I", { transformMode: RESIZE }, false, "A B B C C:D D:E");
                expectIntervals("A:B B:C C:D D:E E:F F:G G:H H:I", { transformMode: EXPAND }, false, "A B B C C:D D:E");
                expectIntervals("A:B B:C C:D D:E E:F F:G G:H H:I", { transformMode: STRICT }, false, "A B B C C:D D:E");
                expectIntervals("A:B B:C C:D D:E E:F F:G G:H H:I", { transformMode: SPLIT },  false, "A B B C C:D D:E");
                expectIntervals("A:B B:C C:D D:E E:F F:G G:H H:I", { transformMode: FIXED },  false, "A:B B:C B:C B:C C:D C:D C:D D:E");

                // do not skip full intervals
                expectIntervals("A:IV", null,                      false, "A:IR");
                expectIntervals("A:IV", { transformMode: RESIZE }, false, "A:IR");
                expectIntervals("A:IV", { transformMode: EXPAND }, false, "A:IR");
                expectIntervals("A:IV", { transformMode: STRICT }, false, "A:IR");
                expectIntervals("A:IV", { transformMode: SPLIT },  false, "A:IR");
                expectIntervals("A:IV", { transformMode: FIXED },  false, "A:IV");
            });
        });

        describe("method transformAddress", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("transformAddress");
            });
            it("should transform address when inserting columns or rows", function () {
                expectAddress("A1",  true, null, "A1");
                expectAddress("G1",  true, null, "G1");
                expectAddress("A2",  true, null, "A2");
                expectAddress("B2",  true, null, "D2");
                expectAddress("C2",  true, null, "E2");
                expectAddress("D2",  true, null, "F2");
                expectAddress("E2",  true, null, "I2");
                expectAddress("F2",  true, null, "J2");
                expectAddress("G2",  true, null, "K2");
                expectAddress("IR2", true, null, "IV2");
                expectAddress("IS2", true, null);
                expectAddress("IR3", true, null, "IV3");
                expectAddress("IR4", true, null, "IR4");
            });
            it("should transform address when deleting columns or rows", function () {
                expectAddress("A1",  false, null, "A1");
                expectAddress("G1",  false, null, "G1");
                expectAddress("A2",  false, null, "A2");
                expectAddress("B2",  false, null);
                expectAddress("C2",  false, null);
                expectAddress("D2",  false, null, "B2");
                expectAddress("E2",  false, null);
                expectAddress("F2",  false, null);
                expectAddress("G2",  false, null, "C2");
                expectAddress("IV2", false, null, "IR2");
                expectAddress("IV3", false, null, "IR3");
                expectAddress("IV4", false, null, "IV4");
            });
            it("should transform address when inserting columns or rows (move mode)", function () {
                expectAddress("A1",  true, { move: true }, "A1");
                expectAddress("G1",  true, { move: true }, "G1");
                expectAddress("A2",  true, { move: true }, "A2");
                expectAddress("B2",  true, { move: true }, "D2");
                expectAddress("C2",  true, { move: true }, "E2");
                expectAddress("D2",  true, { move: true }, "F2");
                expectAddress("E2",  true, { move: true }, "I2");
                expectAddress("F2",  true, { move: true }, "J2");
                expectAddress("G2",  true, { move: true }, "K2");
                expectAddress("IR2", true, { move: true }, "IV2");
                expectAddress("IS2", true, { move: true }, "IV2");
                expectAddress("IR3", true, { move: true }, "IV3");
                expectAddress("IR4", true, { move: true }, "IR4");
            });
            it("should transform address when deleting columns or rows (move mode)", function () {
                expectAddress("A1",  false, { move: true }, "A1");
                expectAddress("G1",  false, { move: true }, "G1");
                expectAddress("A2",  false, { move: true }, "A2");
                expectAddress("B2",  false, { move: true }, "B2");
                expectAddress("C2",  false, { move: true }, "B2");
                expectAddress("D2",  false, { move: true }, "B2");
                expectAddress("E2",  false, { move: true }, "C2");
                expectAddress("F2",  false, { move: true }, "C2");
                expectAddress("G2",  false, { move: true }, "C2");
                expectAddress("IV2", false, { move: true }, "IR2");
                expectAddress("IV3", false, { move: true }, "IR3");
                expectAddress("IV4", false, { move: true }, "IV4");
            });
        });

        describe("method transformAddresses", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("transformAddresses");
            });
            it("should transform addresses when inserting columns or rows", function () {
                expectAddresses("A1 IS2 G2 B2 A2", true, null, "A1 K2 D2 A2");
                expectAddresses("IS2",             true, null, "");
            });
            it("should transform addresses when deleting columns or rows", function () {
                expectAddresses("A1 G2 D2 B2 A2", false, null, "A1 C2 B2 A2");
                expectAddresses("B2",             false, null, "");
            });
            it("should transform addresses when inserting columns or rows (move mode)", function () {
                expectAddresses("A1 IS2 G2 B2 A2", true, { move: true }, "A1 IV2 K2 D2 A2");
            });
            it("should transform addresses when deleting columns or rows (move mode)", function () {
                expectAddresses("A1 G2 D2 B2 A2", false, { move: true }, "A1 C2 B2 B2 A2");
            });
        });

        describe("method transformRanges", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("transformRanges");
            });

            it("should transform cell ranges when inserting columns or rows", function () {

                // not in band interval: do not transform
                expectRanges("A1:G1 A1:G2 A1:G4 A3:G4", null, true, "A1:G1 A1:G2 A1:G4 A3:G4");

                // transform ranges in band interval
                expectRanges("A2:A3 A2:B3 A2:C3 A2:D3 A2:E3 A2:F3 A2:G3", null, true, "A2:A3 A2:D3 A2:E3 A2:F3 A2:I3 A2:J3 A2:K3");
                expectRanges("B2:B3 B2:C3 B2:D3 B2:E3 B2:F3 B2:G3",       null, true, "D2:D3 D2:E3 D2:F3 D2:I3 D2:J3 D2:K3");
                expectRanges("C2:C3 C2:D3 C2:E3 C2:F3 C2:G3",             null, true, "E2:E3 E2:F3 E2:I3 E2:J3 E2:K3");
                expectRanges("D2:D3 D2:E3 D2:F3 D2:G3",                   null, true, "F2:F3 F2:I3 F2:J3 F2:K3");
                expectRanges("E2:E3 E2:F3 E2:G3",                         null, true, "I2:I3 I2:J3 I2:K3");
                expectRanges("F2:F3 F2:G3",                               null, true, "J2:J3 J2:K3");
                expectRanges("G2:G3",                                     null, true, "K2:K3");

                // transformation modes
                expectRanges("A2:C3 A2:D3 A2:E3", null,                      true, "A2:E3 A2:F3 A2:I3");
                expectRanges("A2:C3 A2:D3 A2:E3", { transformMode: RESIZE }, true, "A2:E3 A2:F3 A2:I3");
                expectRanges("A2:C3 A2:D3 A2:E3", { transformMode: EXPAND }, true, "A2:E3 A2:H3 A2:I3");
                expectRanges("A2:C3 A2:D3 A2:E3", { transformMode: STRICT }, true, "A2:E3 A2:F3 A2:I3");
                expectRanges("A2:C3 A2:D3 A2:E3", { transformMode: SPLIT },  true, "A2:A3 D2:E3 A2:A3 D2:F3 A2:A3 D2:F3 I2:I3");
                expectRanges("A2:C3 B2:D3 C2:E3", { transformMode: FIXED },  true, "A2:C3 D2:F3 E2:G3");

                // cap at end of sheet (`STRICT` mode: delete instead of capping)
                expectRanges("IO2:IQ3 IP2:IR3 IQ2:IS3 IR2:IT3 IS2:IU3", null,                      true, "IS2:IU3 IT2:IV3 IU2:IV3 IV2:IV3");
                expectRanges("IO2:IQ3 IP2:IR3 IQ2:IS3 IR2:IT3 IS2:IU3", { transformMode: STRICT }, true, "IS2:IU3 IT2:IV3");

                // split partially covered ranges (splitPartial mode)
                expectRanges("A1:A4 A1:C1 A1:C2 A1:C4 A3:C4 A4:C4", { splitPartial: true }, true, "A1:A4 A1:C1 A1:C1 A2:E2 A1:C1 A4:C4 A2:E3 A4:C4 A3:E3 A4:C4");
                expectRanges("A1:C4",         { transformMode: SPLIT, splitPartial: true }, true, "A1:C1 A4:C4 A2:A3 D2:E3");

                // always skip full ranges
                expectRanges("A2:IV3", null,                      true, "A2:IV3");
                expectRanges("A2:IV3", { transformMode: RESIZE }, true, "A2:IV3");
                expectRanges("A2:IV3", { transformMode: EXPAND }, true, "A2:IV3");
                expectRanges("A2:IV3", { transformMode: STRICT }, true, "A2:IV3");
                expectRanges("A2:IV3", { transformMode: SPLIT },  true, "A2:IV3");
                expectRanges("A2:IV3", { transformMode: FIXED },  true, "A2:IV3");
            });

            it("should transform cell ranges when deleting columns or rows", function () {

                // not in band interval: do not transform
                expectRanges("A1:G1 A1:G2 A1:G4 A3:G4", null, false, "A1:G1 A1:G2 A1:G4 A3:G4");

                // transform ranges in band interval
                expectRanges("A2:A3 A2:B3 A2:C3 A2:D3 A2:E3 A2:F3 A2:G3 A2:H3", null, false, "A2:A3 A2:A3 A2:A3 A2:B3 A2:B3 A2:B3 A2:C3 A2:D3");
                expectRanges("B2:B3 B2:C3 B2:D3 B2:E3 B2:F3 B2:G3 B2:H3",       null, false, "B2:B3 B2:B3 B2:B3 B2:C3 B2:D3");
                expectRanges("C2:C3 C2:D3 C2:E3 C2:F3 C2:G3 C2:H3",             null, false, "B2:B3 B2:B3 B2:B3 B2:C3 B2:D3");
                expectRanges("D2:D3 D2:E3 D2:F3 D2:G3 D2:H3",                   null, false, "B2:B3 B2:B3 B2:B3 B2:C3 B2:D3");
                expectRanges("E2:E3 E2:F3 E2:G3 E2:H3",                         null, false, "C2:C3 C2:D3");
                expectRanges("F2:F3 F2:G3 F2:H3",                               null, false, "C2:C3 C2:D3");
                expectRanges("G2:G3 G2:H3",                                     null, false, "C2:C3 C2:D3");
                expectRanges("H2:H3",                                           null, false, "D2:D3");

                // transformation modes
                expectRanges("A2:B3 B2:C3 C2:D3 D2:E3 E2:F3", null,                      false, "A2:A3 B2:B3 B2:B3");
                expectRanges("A2:B3 B2:C3 C2:D3 D2:E3 E2:F3", { transformMode: RESIZE }, false, "A2:A3 B2:B3 B2:B3");
                expectRanges("A2:B3 B2:C3 C2:D3 D2:E3 E2:F3", { transformMode: EXPAND }, false, "A2:A3 B2:B3 B2:B3");
                expectRanges("A2:B3 B2:C3 C2:D3 D2:E3 E2:F3", { transformMode: SPLIT },  false, "A2:A3 B2:B3 B2:B3");
                expectRanges("A2:B3 B2:C3 C2:D3 D2:E3 E2:F3", { transformMode: FIXED },  false, "A2:B3 B2:C3 B2:C3 B2:C3 C2:D3");

                // split partially covered ranges (splitPartial mode)
                expectRanges("A1:A4 A1:C1 A1:C2 A1:C4 A3:C4 A4:C4", { splitPartial: true }, false, "A1:A4 A1:C1 A1:C1 A2 A1:C1 A4:C4 A2:A3 A4:C4 A3 A4:C4");

                // always skip full ranges
                expectRanges("A2:IV3", null,                      false, "A2:IV3");
                expectRanges("A2:IV3", { transformMode: RESIZE }, false, "A2:IV3");
                expectRanges("A2:IV3", { transformMode: EXPAND }, false, "A2:IV3");
                expectRanges("A2:IV3", { transformMode: STRICT }, false, "A2:IV3");
                expectRanges("A2:IV3", { transformMode: SPLIT },  false, "A2:IV3");
                expectRanges("A2:IV3", { transformMode: FIXED },  false, "A2:IV3");
            });
        });

        describe("method transformCellAnchor", function () {
            it("should exist", function () {
                expect(AddressTransformer).toHaveMethod("transformCellAnchor");
            });

            it("should transform two-cell anchor when inserting columns or rows", function () {

                // not in band interval: do not transform
                expectCellAnchor("2 A1 10 10 G1 20 20", null, true, "2 A1 10 10 G1 20 20");
                expectCellAnchor("2 A1 10 10 G2 20 20", null, true, "2 A1 10 10 G2 20 20");
                expectCellAnchor("2 A1 10 10 G4 20 20", null, true, "2 A1 10 10 G4 20 20");
                expectCellAnchor("2 A3 10 10 G4 20 20", null, true, "2 A3 10 10 G4 20 20");

                // transform ranges in band interval
                expectCellAnchor("2 A2 10 10 A3 20 20", null, true, "2 A2 10 10 A3 20 20");
                expectCellAnchor("2 A2 10 10 B3 20 20", null, true, "2 A2 10 10 D3 20 20");
                expectCellAnchor("2 A2 10 10 C3 20 20", null, true, "2 A2 10 10 E3 20 20");
                expectCellAnchor("2 A2 10 10 D3 20 20", null, true, "2 A2 10 10 F3 20 20");
                expectCellAnchor("2 A2 10 10 E3 20 20", null, true, "2 A2 10 10 I3 20 20");
                expectCellAnchor("2 A2 10 10 F3 20 20", null, true, "2 A2 10 10 J3 20 20");
                expectCellAnchor("2 A2 10 10 G3 20 20", null, true, "2 A2 10 10 K3 20 20");

                expectCellAnchor("2 B2 10 10 B3 20 20", null, true, "2 D2 10 10 D3 20 20");
                expectCellAnchor("2 B2 10 10 C3 20 20", null, true, "2 D2 10 10 E3 20 20");
                expectCellAnchor("2 B2 10 10 D3 20 20", null, true, "2 D2 10 10 F3 20 20");
                expectCellAnchor("2 B2 10 10 E3 20 20", null, true, "2 D2 10 10 I3 20 20");
                expectCellAnchor("2 B2 10 10 F3 20 20", null, true, "2 D2 10 10 J3 20 20");
                expectCellAnchor("2 B2 10 10 G3 20 20", null, true, "2 D2 10 10 K3 20 20");

                expectCellAnchor("2 C2 10 10 C3 20 20", null, true, "2 E2 10 10 E3 20 20");
                expectCellAnchor("2 C2 10 10 D3 20 20", null, true, "2 E2 10 10 F3 20 20");
                expectCellAnchor("2 C2 10 10 E3 20 20", null, true, "2 E2 10 10 I3 20 20");
                expectCellAnchor("2 C2 10 10 F3 20 20", null, true, "2 E2 10 10 J3 20 20");
                expectCellAnchor("2 C2 10 10 G3 20 20", null, true, "2 E2 10 10 K3 20 20");

                expectCellAnchor("2 D2 10 10 D3 20 20", null, true, "2 F2 10 10 F3 20 20");
                expectCellAnchor("2 D2 10 10 G3 20 20", null, true, "2 F2 10 10 K3 20 20");
                expectCellAnchor("2 E2 10 10 E3 20 20", null, true, "2 I2 10 10 I3 20 20");
                expectCellAnchor("2 E2 10 10 G3 20 20", null, true, "2 I2 10 10 K3 20 20");
                expectCellAnchor("2 F2 10 10 F3 20 20", null, true, "2 J2 10 10 J3 20 20");
                expectCellAnchor("2 F2 10 10 G3 20 20", null, true, "2 J2 10 10 K3 20 20");
                expectCellAnchor("2 G2 10 10 G3 20 20", null, true, "2 K2 10 10 K3 20 20");

                // do not expand trailing border with offset 0
                expectCellAnchor("2 A2 0 0 B3 0 0", null, true, "2 A2 0 0 B3 0 0");

                // do not shift drawing outside the sheet
                expectCellAnchor("2 IO2 10 10 IQ3 20 20", null, true, "2 IS2 10 10 IU3 20 20");
                expectCellAnchor("2 IP2 10 10 IR3 20 20", null, true, "2 IT2 10 10 IV3 20 20");
                expectCellAnchor("2 IQ2 10 10 IS3 20 20", null, true, "2 IT2 10 10 IV3 20 20");
                expectCellAnchor("2 IR2 10 10 IT3 20 20", null, true, "2 IT2 10 10 IV3 20 20");
                expectCellAnchor("2 IS2 10 10 IU3 20 20", null, true, "2 IT2 10 10 IV3 20 20");

                expectCellAnchor("2 A2 10 10 IV3 20 20", null, true, "2 A2 10 10 IV3 20 20");
                expectCellAnchor("2 C2 10 10 IV3 20 20", null, true, "2 C2 10 10 IV3 20 20");
                expectCellAnchor("2 E2 10 10 IV3 20 20", null, true, "2 E2 10 10 IV3 20 20");
                expectCellAnchor("2 G2 10 10 IV3 20 20", null, true, "2 G2 10 10 IV3 20 20");
            });

            it("should transform two-cell anchor when deleting columns or rows", function () {

                // not in band interval: do not transform
                expectCellAnchor("2 A1 10 10 G1 20 20", null, false, "2 A1 10 10 G1 20 20");
                expectCellAnchor("2 A1 10 10 G2 20 20", null, false, "2 A1 10 10 G2 20 20");
                expectCellAnchor("2 A1 10 10 G4 20 20", null, false, "2 A1 10 10 G4 20 20");
                expectCellAnchor("2 A3 10 10 G4 20 20", null, false, "2 A3 10 10 G4 20 20");

                // transform ranges in band interval
                expectCellAnchor("2 A2 10 10 A3 20 20", null, false, "2 A2 10 10 A3 20 20");
                expectCellAnchor("2 A2 10 10 B3  0 20", null, false, "2 A2 10 10 B3 0 20");
                expectCellAnchor("2 A2 10 10 B3 20 20", null, false, "2 A2 10 10 B3 0 20");
                expectCellAnchor("2 A2 10 10 C3  0 20", null, false, "2 A2 10 10 B3 0 20");
                expectCellAnchor("2 A2 10 10 C3 20 20", null, false, "2 A2 10 10 B3 0 20");
                expectCellAnchor("2 A2 10 10 D3  0 20", null, false, "2 A2 10 10 B3 0 20");
                expectCellAnchor("2 A2 10 10 D3 20 20", null, false, "2 A2 10 10 B3 20 20");
                expectCellAnchor("2 A2 10 10 E3  0 20", null, false, "2 A2 10 10 C3 0 20");
                expectCellAnchor("2 A2 10 10 E3 20 20", null, false, "2 A2 10 10 C3 0 20");
                expectCellAnchor("2 A2 10 10 F3  0 20", null, false, "2 A2 10 10 C3 0 20");
                expectCellAnchor("2 A2 10 10 F3 20 20", null, false, "2 A2 10 10 C3 0 20");
                expectCellAnchor("2 A2 10 10 G3  0 20", null, false, "2 A2 10 10 C3 0 20");
                expectCellAnchor("2 A2 10 10 G3 20 20", null, false, "2 A2 10 10 C3 20 20");
                expectCellAnchor("2 A2 10 10 H3  0 20", null, false, "2 A2 10 10 D3 0 20");
                expectCellAnchor("2 A2 10 10 H3 20 20", null, false, "2 A2 10 10 D3 20 20");

                expectCellAnchor("2 B2 10 10 B3 20 20", null, false, undefined);
                expectCellAnchor("2 B2 10 10 C3  0 20", null, false, undefined);
                expectCellAnchor("2 B2 10 10 C3 20 20", null, false, undefined);
                expectCellAnchor("2 B2 10 10 D3  0 20", null, false, undefined);
                expectCellAnchor("2 B2 10 10 D3 20 20", null, false, "2 B2 0 10 B3 20 20");
                expectCellAnchor("2 B2 10 10 E3  0 20", null, false, "2 B2 0 10 C3 0 20");
                expectCellAnchor("2 B2 10 10 E3 20 20", null, false, "2 B2 0 10 C3 0 20");
                expectCellAnchor("2 B2 10 10 F3  0 20", null, false, "2 B2 0 10 C3 0 20");
                expectCellAnchor("2 B2 10 10 F3 20 20", null, false, "2 B2 0 10 C3 0 20");
                expectCellAnchor("2 B2 10 10 G3  0 20", null, false, "2 B2 0 10 C3 0 20");
                expectCellAnchor("2 B2 10 10 G3 20 20", null, false, "2 B2 0 10 C3 20 20");
                expectCellAnchor("2 B2 10 10 H3  0 20", null, false, "2 B2 0 10 D3 0 20");
                expectCellAnchor("2 B2 10 10 H3 20 20", null, false, "2 B2 0 10 D3 20 20");

                expectCellAnchor("2 C2 10 10 C3 20 20", null, false, undefined);
                expectCellAnchor("2 C2 10 10 D3  0 20", null, false, undefined);
                expectCellAnchor("2 C2 10 10 D3 20 20", null, false, "2 B2 0 10 B3 20 20");
                expectCellAnchor("2 C2 10 10 E3  0 20", null, false, "2 B2 0 10 C3 0 20");
                expectCellAnchor("2 C2 10 10 E3 20 20", null, false, "2 B2 0 10 C3 0 20");
                expectCellAnchor("2 C2 10 10 F3  0 20", null, false, "2 B2 0 10 C3 0 20");
                expectCellAnchor("2 C2 10 10 F3 20 20", null, false, "2 B2 0 10 C3 0 20");
                expectCellAnchor("2 C2 10 10 G3  0 20", null, false, "2 B2 0 10 C3 0 20");
                expectCellAnchor("2 C2 10 10 G3 20 20", null, false, "2 B2 0 10 C3 20 20");
                expectCellAnchor("2 C2 10 10 H3  0 20", null, false, "2 B2 0 10 D3 0 20");
                expectCellAnchor("2 C2 10 10 H3 20 20", null, false, "2 B2 0 10 D3 20 20");

                expectCellAnchor("2 D2 10 10 D3 20 20", null, false, "2 B2 10 10 B3 20 20");
                expectCellAnchor("2 D2 10 10 E3  0 20", null, false, "2 B2 10 10 C3 0 20");
                expectCellAnchor("2 D2 10 10 E3 20 20", null, false, "2 B2 10 10 C3 0 20");
                expectCellAnchor("2 D2 10 10 F3  0 20", null, false, "2 B2 10 10 C3 0 20");
                expectCellAnchor("2 D2 10 10 F3 20 20", null, false, "2 B2 10 10 C3 0 20");
                expectCellAnchor("2 D2 10 10 G3  0 20", null, false, "2 B2 10 10 C3 0 20");
                expectCellAnchor("2 D2 10 10 G3 20 20", null, false, "2 B2 10 10 C3 20 20");
                expectCellAnchor("2 D2 10 10 H3  0 20", null, false, "2 B2 10 10 D3 0 20");
                expectCellAnchor("2 D2 10 10 H3 20 20", null, false, "2 B2 10 10 D3 20 20");

                expectCellAnchor("2 E2 10 10 E3 20 20", null, false, undefined);
                expectCellAnchor("2 E2 10 10 F3  0 20", null, false, undefined);
                expectCellAnchor("2 E2 10 10 F3 20 20", null, false, undefined);
                expectCellAnchor("2 E2 10 10 G3  0 20", null, false, undefined);
                expectCellAnchor("2 E2 10 10 G3 20 20", null, false, "2 C2 0 10 C3 20 20");
                expectCellAnchor("2 E2 10 10 H3  0 20", null, false, "2 C2 0 10 D3 0 20");
                expectCellAnchor("2 E2 10 10 H3 20 20", null, false, "2 C2 0 10 D3 20 20");

                expectCellAnchor("2 F2 10 10 F3 20 20", null, false, undefined);
                expectCellAnchor("2 F2 10 10 G3  0 20", null, false, undefined);
                expectCellAnchor("2 F2 10 10 G3 20 20", null, false, "2 C2 0 10 C3 20 20");
                expectCellAnchor("2 F2 10 10 H3  0 20", null, false, "2 C2 0 10 D3 0 20");
                expectCellAnchor("2 F2 10 10 H3 20 20", null, false, "2 C2 0 10 D3 20 20");

                expectCellAnchor("2 G2 10 10 G3 20 20", null, false, "2 C2 10 10 C3 20 20");
                expectCellAnchor("2 G2 10 10 H3  0 20", null, false, "2 C2 10 10 D3 0 20");
                expectCellAnchor("2 G2 10 10 H3 20 20", null, false, "2 C2 10 10 D3 20 20");

                // handle full ranges
                expectCellAnchor("2 A2 10 10 IV3 20 20", null, false, "2 A2 10 10 IR3 20 20");
            });

            it("should transform two-cell anchor when deleting columns or rows (move mode)", function () {

                expectCellAnchor("2 B2 10 10 B3 20 20", { move: true }, false, "2 B2 0 10 B3 0 20");
                expectCellAnchor("2 B2 10 10 C3  0 20", { move: true }, false, "2 B2 0 10 B3 0 20");
                expectCellAnchor("2 B2 10 10 C3 20 20", { move: true }, false, "2 B2 0 10 B3 0 20");
                expectCellAnchor("2 B2 10 10 D3  0 20", { move: true }, false, "2 B2 0 10 B3 0 20");

                expectCellAnchor("2 C2 10 10 C3 20 20", { move: true }, false, "2 B2 0 10 B3 0 20");
                expectCellAnchor("2 C2 10 10 D3  0 20", { move: true }, false, "2 B2 0 10 B3 0 20");

                expectCellAnchor("2 E2 10 10 E3 20 20", { move: true }, false, "2 C2 0 10 C3 0 20");
                expectCellAnchor("2 E2 10 10 F3  0 20", { move: true }, false, "2 C2 0 10 C3 0 20");
                expectCellAnchor("2 E2 10 10 F3 20 20", { move: true }, false, "2 C2 0 10 C3 0 20");
                expectCellAnchor("2 E2 10 10 G3  0 20", { move: true }, false, "2 C2 0 10 C3 0 20");

                expectCellAnchor("2 F2 10 10 F3 20 20", { move: true }, false, "2 C2 0 10 C3 0 20");
                expectCellAnchor("2 F2 10 10 G3  0 20", { move: true }, false, "2 C2 0 10 C3 0 20");
            });
        });
    });
});
