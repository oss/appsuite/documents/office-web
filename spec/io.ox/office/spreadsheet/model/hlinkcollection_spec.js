/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Direction, MergeMode } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { HlinkCollection } from "@/io.ox/office/spreadsheet/model/hlinkcollection";

import { a, r, ra, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/hlinkcollection", () => {

    // the operations to be applied by the document model
    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertSheet(0, "Sheet1"),
        op.insertSheet(1, "Sheet2")
    ];

    // initialize test document
    let docModel, sheetModel, hlinkCollection, mergeCollection;
    createSpreadsheetApp("ooxml", OPERATIONS).done(function (docApp) {
        docModel = docApp.docModel;
        sheetModel = docModel.getSheetModel(0);
        hlinkCollection = sheetModel.hlinkCollection;
        mergeCollection = sheetModel.mergeCollection;
    });

    // creates a new operation context
    function ctxt(json) {
        return new SheetOperationContext(docModel, json);
    }

    function linkOp(ranges, url) {
        return ctxt({ ranges, url });
    }

    function getLinkRanges(collection) {
        return collection.getLinkRanges(r("A1:ZZZ10000")).sort();
    }

    // class HlinkCollection --------------------------------------------------

    describe("class HlinkCollection", () => {

        it("should exist", () => {
            expect(HlinkCollection).toBeFunction();
        });

        describe("method applyInsertOperation", () => {
            it("should exist", () => {
                expect(HlinkCollection).toHaveMethod("applyInsertOperation");
            });
            it('should execute an "insertHyperlink" operation', () => {
                const collection = new HlinkCollection(sheetModel);
                const context = linkOp("A1:C3 E5 C3:D4", "http://www.example.org");
                collection.applyInsertOperation(context);
                const ranges = getLinkRanges(collection);
                expect(ranges).toStringifyTo("A1:C3,C3:D4,E5:E5");
                expect(ranges[0].url).toBe("http://www.example.org");
                expect(ranges[1].url).toBe("http://www.example.org");
                expect(ranges[2].url).toBe("http://www.example.org");
                collection.destroy();
            });
        });

        describe("method applyDeleteOperation", () => {
            it("should exist", () => {
                expect(HlinkCollection).toHaveMethod("applyDeleteOperation");
            });
            it('should execute a "deleteHyperlink" operation', () => {
                const collection = new HlinkCollection(sheetModel);
                const context = linkOp("A1:C3", "http://www.example.org");
                expect(getLinkRanges(collection)).toHaveLength(0);
                collection.applyInsertOperation(context);
                expect(getLinkRanges(collection)).toHaveLength(1);
                collection.applyDeleteOperation(context);
                expect(getLinkRanges(collection)).toHaveLength(0);
                collection.destroy();
            });
        });

        describe("method applyCopySheetOperation", () => {
            it("should exist", () => {
                expect(HlinkCollection).toHaveMethod("applyCopySheetOperation");
            });
            it("should clone the existing hyperlinks from the given collection", () => {
                const collection1 = new HlinkCollection(sheetModel);
                const context = linkOp("A1:C3", "http://www.example.org");
                collection1.applyInsertOperation(context);
                const sheetModel2 = docModel.getSheetModel(1);
                const collection2 = new HlinkCollection(sheetModel2);
                collection2.applyCopySheetOperation(ctxt({}), collection1);
                const ranges = getLinkRanges(collection2);
                expect(ranges).toStringifyTo("A1:C3");
                expect(ranges[0].url).toBe("http://www.example.org");
                collection2.destroy();
                collection1.destroy();
            });
        });

        describe("method coversAnyLinkRange", () => {
            it("should exist", () => {
                expect(HlinkCollection).toHaveMethod("coversAnyLinkRange");
            });
            it("should return true, if the given range overlaps a hyperlink range", () => {
                const collection = new HlinkCollection(sheetModel);
                const context = linkOp("A1:C3", "http://www.example.org");
                expect(collection.coversAnyLinkRange(r("B2:F9"))).toBeFalse();
                collection.applyInsertOperation(context);
                expect(collection.coversAnyLinkRange(r("B2:F9"))).toBeTrue();
                collection.applyDeleteOperation(context);
                expect(collection.coversAnyLinkRange(r("B2:F9"))).toBeFalse();
                collection.destroy();
            });
        });

        describe("method getCellURL", () => {
            it("should exist", () => {
                expect(HlinkCollection).toHaveMethod("getCellURL");
            });
            it("should return the correct url on the basis of an address", () => {
                const collection = new HlinkCollection(sheetModel);
                expect(collection.getCellURL(a("B3"))).toBeNull();
                const context = linkOp("A1:C3", "http://www.example.org");
                collection.applyInsertOperation(context);
                expect(collection.getCellURL(a("B3"))).toBe("http://www.example.org");
                expect(collection.getCellURL(a("Z99"))).toBeNull();
            });
        });

        describe("method generateHlinkOperations", () => {
            it("should exist", () => {
                expect(HlinkCollection).toHaveMethod("generateHlinkOperations");
            });

            it("should insert hyperlinks with correct splitting", async () => {
                await sheetModel.createAndApplyOperations(generator => {
                    hlinkCollection.generateHlinkOperations(generator, ra("A1:C3 E1:G4"), "http://www.example.org");
                });
                let ranges = getLinkRanges(hlinkCollection);
                expect(ranges).toStringifyTo("A1:C3,E1:G4");
                expect(ranges[0].url).toBe("http://www.example.org");
                expect(ranges[1].url).toBe("http://www.example.org");
                await sheetModel.createAndApplyOperations(generator => {
                    hlinkCollection.generateHlinkOperations(generator, r("B2:F6"), "http://www.test.de");
                });
                ranges = getLinkRanges(hlinkCollection);
                expect(ranges).toStringifyTo("A1:C1,E1:G1,A2:A3,B2:F6,G2:G4");
                expect(ranges[0].url).toBe("http://www.example.org");
                expect(ranges[1].url).toBe("http://www.example.org");
                expect(ranges[2].url).toBe("http://www.example.org");
                expect(ranges[3].url).toBe("http://www.test.de");
                expect(ranges[4].url).toBe("http://www.example.org");
                await docModel.undoManager.undo();
                ranges = getLinkRanges(hlinkCollection);
                expect(ranges).toStringifyTo("A1:C3,E1:G4");
                expect(ranges[0].url).toBe("http://www.example.org");
                expect(ranges[1].url).toBe("http://www.example.org");
                await docModel.undoManager.undo();
                expect(getLinkRanges(hlinkCollection)).toHaveLength(0);
            });
        });

        describe("method generateAutoFillOperations", () => {
            it("should exist", () => {
                expect(HlinkCollection).toHaveMethod("generateAutoFillOperations");
            });

            it("should create the correct hyperlinks on autofill", async () => {
                await sheetModel.createAndApplyOperations(generator => {
                    hlinkCollection.generateHlinkOperations(generator, r("A1:B2"), "http://www.example.org");
                });
                await sheetModel.createAndApplyOperations(generator => {
                    hlinkCollection.generateAutoFillOperations(generator, r("A1:B4"), Direction.DOWN, 6);
                });
                const ranges = getLinkRanges(hlinkCollection);
                expect(ranges).toStringifyTo("A1:B2,A5:B6,A9:B10");
                expect(ranges[0].url).toBe("http://www.example.org");
                expect(ranges[1].url).toBe("http://www.example.org");
                expect(ranges[2].url).toBe("http://www.example.org");
                await docModel.undoManager.undo();
                await docModel.undoManager.undo();
                expect(getLinkRanges(hlinkCollection)).toHaveLength(0);
            });
        });

        describe("method generateMergeCellsOperations", () => {
            it("should exist", () => {
                expect(HlinkCollection).toHaveMethod("generateMergeCellsOperations");
            });

            it("should generate correct merge cells operations", async () => {
                await sheetModel.createAndApplyOperations(generator => {
                    hlinkCollection.generateHlinkOperations(generator, ra("A1:B2 A5:B6 A9:B10"), "http://www.example.org");
                });
                await sheetModel.createAndApplyOperations(function (generator) {
                    return mergeCollection.generateMergeCellsOperations(generator, r("B2:F10"), MergeMode.MERGE);
                });
                expect(getLinkRanges(hlinkCollection)).toStringifyTo("A1:B1,A2:A2,B2:F10,A5:A6,A9:A10");
                await docModel.undoManager.undo();
                expect(getLinkRanges(hlinkCollection)).toStringifyTo("A1:B2,A5:B6,A9:B10");
                await docModel.undoManager.undo();
                expect(getLinkRanges(hlinkCollection)).toHaveLength(0);
            });

            it("should generate correct merge cells operations #2", async () => {
                await sheetModel.createAndApplyOperations(generator => {
                    hlinkCollection.generateHlinkOperations(generator, ra("A1:B2 A5:B6 A9:B10"), "http://www.example.org");
                });
                await sheetModel.createAndApplyOperations(function (generator) {
                    return mergeCollection.generateMergeCellsOperations(generator, r("A2:F10"), MergeMode.MERGE);
                });
                expect(getLinkRanges(hlinkCollection)).toStringifyTo("A1:B1,A2:F10");
                await docModel.undoManager.undo();
                expect(getLinkRanges(hlinkCollection)).toStringifyTo("A1:B2,A5:B6,A9:B10");
                await docModel.undoManager.undo();
                expect(getLinkRanges(hlinkCollection)).toHaveLength(0);
            });

            it("should generate correct merge cells operations #3", async () => {
                await sheetModel.createAndApplyOperations(generator => {
                    hlinkCollection.generateHlinkOperations(generator, ra("A1:B2 A5:B6 A9:B10"), "http://www.example.org");
                });
                await sheetModel.createAndApplyOperations(function (generator) {
                    return mergeCollection.generateMergeCellsOperations(generator, r("B3:F10"), MergeMode.MERGE);
                });
                expect(getLinkRanges(hlinkCollection)).toStringifyTo("A1:B2,A5:A6,A9:A10");
                await docModel.undoManager.undo();
                expect(getLinkRanges(hlinkCollection)).toStringifyTo("A1:B2,A5:B6,A9:B10");
                await docModel.undoManager.undo();
                expect(getLinkRanges(hlinkCollection)).toHaveLength(0);
            });
        });
    });
});
