/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";
import { OperationContext, SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";

import { createSpreadsheetApp } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/operation/context", () => {

    // base properties for the test operation that are not convertible to addresses, ranges, etc.
    const operation = { name: "test", num: 1.5, obj: { a: 1 }, arr: [1] };

    // initialize test document
    let docModel = null, addrContext = null, addrListContext = null, rangeContext = null;
    createSpreadsheetApp("ooxml").done(function (docApp) {
        docModel = docApp.docModel;

        // test context for cell addresses
        addrContext = new SheetOperationContext(docModel, {
            address1: "A1",
            address2: "BB99",
            address3: "ZZZZ1",
            address4: "A999999999",
            ...operation
        }, false);

        // test context for cell address lists
        addrListContext = new SheetOperationContext(docModel, {
            addresses1: "A1",
            addresses2: "A1 B2 \t BB99",
            addresses3: "A1 ZZZZ1",
            addresses4: "A1 42",
            ...operation
        }, false);

        // test context for cell range addresses
        rangeContext = new SheetOperationContext(docModel, {
            range1: "A1",
            range2: "C4:BB99",
            range3: "A1:ZZZZ1",
            range4: "A1:A999999999",
            ...operation
        }, false);
    });

    // class SheetOperationContext --------------------------------------------

    describe("class SheetOperationContext", () => {

        it("should exist", () => {
            expect(SheetOperationContext).toBeSubClassOf(OperationContext);
        });

        describe("method getAddress", () => {
            it("should exist", () => {
                expect(SheetOperationContext).toHaveMethod("getAddress");
            });
            it("should return existing properties", () => {
                expect(addrContext.getAddress("address1")).toStringifyTo("A1");
                expect(addrContext.getAddress("address2")).toStringifyTo("BB99");
            });
            it("should throw on missing properties", () => {
                expect(() => addrContext.getAddress("wrong")).toThrow(OperationError);
            });
            it("should throw on existing properties with wrong type", () => {
                expect(() => addrContext.getAddress("num")).toThrow(OperationError);
                expect(() => addrContext.getAddress("obj")).toThrow(OperationError);
                expect(() => addrContext.getAddress("arr")).toThrow(OperationError);
                expect(() => addrContext.getAddress("address3")).toThrow(OperationError);
                expect(() => addrContext.getAddress("address4")).toThrow(OperationError);
            });
        });

        describe("method optAddress", () => {
            it("should exist", () => {
                expect(SheetOperationContext).toHaveMethod("optAddress");
            });
            it("should return existing properties", () => {
                expect(addrContext.optAddress("address1")).toStringifyTo("A1");
                expect(addrContext.optAddress("address2")).toStringifyTo("BB99");
            });
            it("should return null for missing properties", () => {
                expect(addrContext.optAddress("wrong")).toBeUndefined();
            });
            it("should throw on existing properties with wrong type", () => {
                expect(() => addrContext.optAddress("num")).toThrow(OperationError);
                expect(() => addrContext.optAddress("obj")).toThrow(OperationError);
                expect(() => addrContext.optAddress("arr")).toThrow(OperationError);
                expect(() => addrContext.optAddress("address3")).toThrow(OperationError);
                expect(() => addrContext.optAddress("address4")).toThrow(OperationError);
            });
        });

        describe("method getRange", () => {
            it("should exist", () => {
                expect(SheetOperationContext).toHaveMethod("getRange");
            });
            it("should return existing properties", () => {
                expect(rangeContext.getRange("range1")).toStringifyTo("A1:A1");
                expect(rangeContext.getRange("range2")).toStringifyTo("C4:BB99");
            });
            it("should throw on missing properties", () => {
                expect(() => rangeContext.getRange("wrong")).toThrow(OperationError);
            });
            it("should throw on existing properties with wrong type", () => {
                expect(() => rangeContext.getRange("num")).toThrow(OperationError);
                expect(() => rangeContext.getRange("obj")).toThrow(OperationError);
                expect(() => rangeContext.getRange("arr")).toThrow(OperationError);
                expect(() => rangeContext.getRange("range3")).toThrow(OperationError);
                expect(() => rangeContext.getRange("range4")).toThrow(OperationError);
            });
        });

        describe("method optRange", () => {
            it("should exist", () => {
                expect(SheetOperationContext).toHaveMethod("optRange");
            });
            it("should return existing properties", () => {
                expect(rangeContext.optRange("range1")).toStringifyTo("A1:A1");
                expect(rangeContext.optRange("range2")).toStringifyTo("C4:BB99");
            });
            it("should return null for missing properties", () => {
                expect(rangeContext.optRange("wrong")).toBeUndefined();
            });
            it("should throw on existing properties with wrong type", () => {
                expect(() => rangeContext.optRange("num")).toThrow(OperationError);
                expect(() => rangeContext.optRange("obj")).toThrow(OperationError);
                expect(() => rangeContext.optRange("arr")).toThrow(OperationError);
                expect(() => rangeContext.optRange("range3")).toThrow(OperationError);
                expect(() => rangeContext.optRange("range4")).toThrow(OperationError);
            });
        });

        describe("method getAddressList", () => {
            it("should exist", () => {
                expect(SheetOperationContext).toHaveMethod("getAddressList");
            });
            it("should return existing properties", () => {
                expect(addrListContext.getAddressList("addresses1")).toStringifyTo("A1");
                expect(addrListContext.getAddressList("addresses2")).toStringifyTo("A1,B2,BB99");
            });
            it("should throw on missing properties", () => {
                expect(() => addrListContext.getAddressList("wrong")).toThrow(OperationError);
            });
            it("should throw on existing properties with wrong type", () => {
                expect(() => addrListContext.getAddressList("num")).toThrow(OperationError);
                expect(() => addrListContext.getAddressList("obj")).toThrow(OperationError);
                expect(() => addrListContext.getAddressList("arr")).toThrow(OperationError);
                expect(() => addrListContext.getAddressList("addresses3")).toThrow(OperationError);
                expect(() => addrListContext.getAddressList("addresses4")).toThrow(OperationError);
            });
        });

        describe("method optAddressList", () => {
            it("should exist", () => {
                expect(SheetOperationContext).toHaveMethod("optAddressList");
            });
            it("should return existing properties", () => {
                expect(addrListContext.optAddressList("addresses1")).toStringifyTo("A1");
                expect(addrListContext.optAddressList("addresses2")).toStringifyTo("A1,B2,BB99");
            });
            it("should return null for missing properties", () => {
                expect(addrListContext.optAddressList("wrong")).toBeUndefined();
            });
            it("should throw on existing properties with wrong type", () => {
                expect(() => addrListContext.optAddressList("num")).toThrow(OperationError);
                expect(() => addrListContext.optAddressList("obj")).toThrow(OperationError);
                expect(() => addrListContext.optAddressList("arr")).toThrow(OperationError);
                expect(() => addrListContext.optAddressList("addresses3")).toThrow(OperationError);
                expect(() => addrListContext.optAddressList("addresses4")).toThrow(OperationError);
            });
        });

        describe("method getRangeList", () => {
            it("should exist", () => {
                expect(SheetOperationContext).toHaveMethod("getRangeList");
            });
        });

        describe("method optRangeList", () => {
            it("should exist", () => {
                expect(SheetOperationContext).toHaveMethod("optRangeList");
            });
        });
    });
});
