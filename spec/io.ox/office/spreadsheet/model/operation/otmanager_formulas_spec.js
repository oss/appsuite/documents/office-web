/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import {
    insertSheet, deleteSheet, moveSheet, copySheet, moveSheets, changeSheet,
    insertCols, deleteCols, insertRows, deleteRows, changeCells, moveCells,
    insertName, changeName, changeTable,
    insertDVRule, changeDVRule, insertCFRule, changeCFRule,
    insertShape, changeDrawing, insertChSeries, changeChSeries, changeChTitle
} from "~/spreadsheet/apphelper";

import { ATTRS, createTestRunner } from "~/spreadsheet/othelper";

// tests ======================================================================

describe("module io.ox/office/spreadsheet/model/operation/otmanager", () => {

    // initialize OT test runners
    const testRunner = createTestRunner();

    // formula expressions ----------------------------------------------------

    describe("formulas", () => {

        // creates operations for sheet 1 with formula expressions to be updated
        function makeSheetFormulaOps(sheet, qrefs, urefs) {
            const f = "SUM(" + qrefs + "," + urefs + ")";
            return [
                changeCells(sheet, { A1: { f } }),
                insertName("a", sheet, { formula: f }),
                changeName("a", sheet, { formula: f }),
                insertDVRule(sheet, 1, "A1", { value1: f, value2: f }),
                changeDVRule(sheet, 1, "A1", { value1: f, value2: f }),
                insertCFRule(sheet, "R1", "A1", { value1: f, value2: f, colorScale: [{ v: f }], dataBar: { r1: { v: f }, r2: { v: f } } }),
                changeCFRule(sheet, "R1", "A1", { value1: f, value2: f, colorScale: [{ v: f }], dataBar: { r1: { v: f }, r2: { v: f } } }),
                insertShape(sheet, 1, { text: { link: f } }),
                changeDrawing(sheet, 1, { text: { link: f } }),
                insertChSeries(sheet, 1, 0, { series: { title: f, values: f, names: f, bubbles: f } }),
                changeChSeries(sheet, 1, 0, { series: { title: f, values: f, names: f, bubbles: f } }),
                changeChTitle(sheet, 1, { text: { link: f } })
            ];
        }

        // creates global operations with formula expressions to be updated
        function makeGlobalFormulaOps(qrefs) {
            const f = "SUM(" + qrefs + ")";
            return [
                insertName("global", { formula: f }),
                changeName("global", { formula: f })
            ];
        }

        // cell/name references with sheet name, and table referenecs (can be used in defined names)
        const QUALIFIED_REFS = "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]";
        // cell/name references without sheet name (not used in defined names)
        const UNQUALIFIED_REFS = "$E$5,name";

        // untransformed operations with formula expressions for sheet 1
        const SHEET_OPS = makeSheetFormulaOps(1, QUALIFIED_REFS, UNQUALIFIED_REFS);
        // untransformed operations with global formula expressions
        const GLOB_OPS = makeGlobalFormulaOps(QUALIFIED_REFS);

        // operations causing to update formula expressions (except "copySheet" which needs special care)
        // `s` = expected sheet index in transformed operation (original sheet index is 1)
        // `q` = expected transformed qualified references (original is `QUALIFIED_REFS`)
        // `u` = expected transformed unqualified references (original is `UNQUALIFIED_REFS`)
        const TRANSFORMING_OPS = [
            // delete sheet: invalidate sheet references (deleting sheet 1 would delete the sheet operations completely)
            { op: deleteSheet(0), s: 0, q: "#REF!,Sheet2!$E$5,#REF!,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]",   u: "$E$5,name" },
            { op: deleteSheet(2), s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],#REF!,#REF!", u: "$E$5,name" },
            // rename sheet: transform sheet references
            { op: changeSheet(0, "SheetA"),  s: 1, q: "SheetA!$E$5,Sheet2!$E$5,SheetA!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]",       u: "$E$5,name" },
            { op: changeSheet(1, "Sheet A"), s: 1, q: "Sheet1!$E$5,'Sheet A'!$E$5,Sheet1!name,'Sheet A'!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            { op: changeSheet(2, "SheetA"),  s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]",       u: "$E$5,name" },
            // insert columns: transform cell references
            { op: insertCols(0, "C:D"), s: 1, q: "Sheet1!$G$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            { op: insertCols(1, "C:D"), s: 1, q: "Sheet1!$E$5,Sheet2!$G$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$G$5,name" },
            { op: insertCols(2, "C:D"), s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            // delete columns: transform cell references
            { op: deleteCols(0, "C:D"), s: 1, q: "Sheet1!$C$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            { op: deleteCols(1, "C:D"), s: 1, q: "Sheet1!$E$5,Sheet2!$C$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$C$5,name" },
            { op: deleteCols(2, "C:D"), s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            // insert rows: transform cell references
            { op: insertRows(0, "3:4"), s: 1, q: "Sheet1!$E$7,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            { op: insertRows(1, "3:4"), s: 1, q: "Sheet1!$E$5,Sheet2!$E$7,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$7,name" },
            { op: insertRows(2, "3:4"), s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            // delete rows: transform cell references
            { op: deleteRows(0, "3:4"), s: 1, q: "Sheet1!$E$3,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            { op: deleteRows(1, "3:4"), s: 1, q: "Sheet1!$E$5,Sheet2!$E$3,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$3,name" },
            { op: deleteRows(2, "3:4"), s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            // move cells: transform cell references
            { op: moveCells(0, "C3:E5", "down"), s: 1, q: "Sheet1!$E$8,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            { op: moveCells(1, "C3:E5", "down"), s: 1, q: "Sheet1!$E$5,Sheet2!$E$8,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$8,name" },
            { op: moveCells(2, "C3:E5", "down"), s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            // change label of defined name: transform name references
            { op: changeName("name",    { newLabel: "name2" }), s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name2,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            { op: changeName("name", 0, { newLabel: "name2" }), s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name2,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name" },
            { op: changeName("name", 1, { newLabel: "name2" }), s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name2,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]", u: "$E$5,name2" },
            { op: changeName("name", 2, { newLabel: "name2" }), s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table1[],Table1[Col1],Table2[],Table2[Col3]",  u: "$E$5,name" },
            // rename table range: transform table references
            { op: changeTable(1, "Table1", { newName: "Table3" }), s: 1, q: "Sheet1!$E$5,Sheet2!$E$5,Sheet1!name,Sheet2!name,[0]!name,Table3[],Table3[Col1],Table2[],Table2[Col3]", u: "$E$5,name" }
        ];

        // test each of the combinations of the operations above
        for (const entry of TRANSFORMING_OPS) {
            it('with operation "' + entry.op.name + '" should transform formula expressions', () => {
                testRunner().runBidiTest(entry.op, SHEET_OPS, null, makeSheetFormulaOps(entry.s, entry.q, entry.u));
                testRunner().runBidiTest(entry.op, GLOB_OPS, null, makeGlobalFormulaOps(entry.q));
            });
        }

        // "copySheet" clones contents to new sheet and renames all sheet references, e.g. "Sheet1!A1" to "NewSheet!A1"
        describe('with operation "copySheet"', () => {
            it("should clone the operations and adapt sheet name", () => {
                const expOps1 = makeSheetFormulaOps(2, QUALIFIED_REFS, UNQUALIFIED_REFS);
                const expOps2 = makeSheetFormulaOps(0, "Sheet1!$E$5,SheetA!$E$5,Sheet1!name,SheetA!name,[0]!name,Table4[],Table4[Col1],Table2[],Table2[Col3]", UNQUALIFIED_REFS);
                const expOps = _.zip(expOps1, expOps2).flat();
                testRunner().runBidiTest(copySheet(1, 0, "SheetA", { Table1: "Table4" }), SHEET_OPS, null, expOps);
            });
            it("should skip operations if not targeting source sheet", () => {
                testRunner().runBidiTest(copySheet(2, 3, "SheetA", { Table2: "Table4" }), [SHEET_OPS, GLOB_OPS]);
            });
        });

        // situations where transformation must fail (missing implementations)
        describe("should fail", () => {
            it("for missing snapshot", () => {
                testRunner().expectBidiError(changeSheet(0, "SheetA"), changeCells(1, { A1: { f: "1" } }), { userData: null });
            });
            it("for missing reference address in rule operations", () => {
                testRunner().expectBidiError(changeSheet(0, "SheetA"), changeDVRule(1, 1, { value1: "1" }));
                testRunner().expectBidiError(changeSheet(0, "SheetA"), changeCFRule(1, "R1", { value1: "1" }));
            });
        });

        // situations where transformation must not fail (no formulas, or operation does not cause formula transformation)
        describe("should not fail", () => {
            it("for missing snapshot without formula expression", () => {
                testRunner().runBidiTest([
                    deleteSheet(2),
                    changeSheet(0, "SheetA"),
                    insertCols(0, "C:D"),
                    changeName("name", { newLabel: "name2" }),
                    changeTable(1, "Table1", { newName: "Table4" })
                ], [
                    changeCells(1, { A1: 42 }),
                    changeName("name", 1, { newLabel: "name2" }),
                    insertDVRule(1, 1, "A1", { value1: 42 }),
                    changeDVRule(1, 1, "A1", { value1: 42 }),
                    insertCFRule(1, "R1", "A1", { value1: 42 }),
                    changeCFRule(1, "R1", "A1", { value1: 42 }),
                    insertShape(1, 1, ATTRS),
                    changeDrawing(1, 1, ATTRS),
                    insertChSeries(1, 1, 0, ATTRS),
                    changeChSeries(1, 1, 0, ATTRS),
                    changeChTitle(1, 1, ATTRS)
                ]);
            });
            it("for missing snapshot without formula transformation", () => {
                testRunner().runBidiTest([
                    copySheet(0, 2, "SheetA"),              // formulas not located in source sheet
                    changeSheet(1, ATTRS),                  // sheet not renamed
                    changeName("name", { formula: "1" }),   // no new label for the name
                    changeTable(1, "Table1", "A10:B12")     // no new name for the table
                ], [SHEET_OPS, GLOB_OPS]);
            });
        });

        // transformations depending on correct intermediate update of document snapshots
        describe("should transform document snapshot", () => {
            it("when inserting sheet", () => {
                testRunner().runBidiTest(
                    changeCells(1, { A1: { f: "C3+Sheet1!C3+Sheet3!C3" } }),
                    [insertRows(1, "2"), insertCols(2, "B"), insertSheet(0, "Sheet4"), insertRows(2, "2"), insertCols(3, "B")],
                    changeCells(2, { A1: { f: "C5+Sheet1!C3+Sheet3!E3" } })
                );
            });
            it("when deleting sheet", () => {
                testRunner().runBidiTest(
                    changeCells(1, { A1: { f: "C3+Sheet1!C3+Sheet3!C3" } }),
                    [insertRows(1, "2"), insertCols(2, "B"), deleteSheet(0), insertRows(0, "2"), insertCols(1, "B")],
                    changeCells(0, { A1: { f: "C5+#REF!+Sheet3!E3" } })
                );
            });
            it("when moving sheet", () => {
                testRunner().runBidiTest(
                    changeCells(1, { A1: { f: "C3+Sheet1!C3+Sheet3!C3" } }),
                    [insertRows(1, "2"), insertCols(2, "B"), moveSheet(2, 0), insertRows(2, "2"), insertCols(0, "B")],
                    changeCells(2, { A1: { f: "C5+Sheet1!C3+Sheet3!E3" } })
                );
            });
            it("when moving sheets", () => {
                testRunner().runBidiTest(
                    changeCells(1, { A1: { f: "C3+Sheet1!C3+Sheet3!C3" } }),
                    [insertRows(1, "2"), insertCols(2, "B"), moveSheets([2, 0, 1, 3, 4, 5, 6, 7]), insertRows(2, "2"), insertCols(0, "B")],
                    changeCells(2, { A1: { f: "C5+Sheet1!C3+Sheet3!E3" } })
                );
            });
            it("when copying sheet", () => {
                testRunner().runBidiTest(
                    changeCells(1, { A1: { f: "C3+Sheet1!C3+Sheet3!C3" } }),
                    [insertRows(1, "2"), insertCols(2, "B"), copySheet(2, 0, "SheetA"), insertRows(2, "2"), insertCols(3, "B")],
                    changeCells(2, { A1: { f: "C5+Sheet1!C3+Sheet3!E3" } })
                );
            });
            it("when renaming sheet", () => {
                testRunner().runBidiTest(
                    changeCells(1, { A1: { f: "C3+Sheet1!C3+Sheet3!C3" } }),
                    [insertRows(1, "2"), insertCols(2, "B"), changeSheet(2, "SheetA"), insertRows(1, "2"), insertCols(2, "B")],
                    changeCells(1, { A1: { f: "C5+Sheet1!C3+SheetA!E3" } })
                );
            });
            // complex test with snapshot transformations on both sides
            it("on both sides", () => {
                const chCells = (s, f) => changeCells(s, { A1: { f } });
                testRunner().runTest(
                    [chCells(2, "C3+Sheet1!C3+Sheet2!C3+Sheet3!C3"), insertCols(1, "B"), changeSheet(1, "SheetA"), insertCols(1, "B"), chCells(2, "C3+Sheet1!C3+SheetA!C3+Sheet3!C3")],
                    [chCells(1, "C3+Sheet1!C3+Sheet2!C3+Sheet3!C3"), insertRows(2, "2"), changeSheet(2, "SheetB"), insertRows(2, "2"), chCells(1, "C3+Sheet1!C3+Sheet2!C3+SheetB!C3")],
                    [chCells(2, "C5+Sheet1!C3+Sheet2!C3+SheetB!C5"), insertCols(1, "B"), changeSheet(1, "SheetA"), insertCols(1, "B"), chCells(2, "C5+Sheet1!C3+SheetA!C3+SheetB!C5")],
                    [chCells(1, "E3+Sheet1!C3+SheetA!E3+Sheet3!C3"), insertRows(2, "2"), changeSheet(2, "SheetB"), insertRows(2, "2"), chCells(1, "E3+Sheet1!C3+SheetA!E3+SheetB!C3")]
                );
            });
        });
    });
});
