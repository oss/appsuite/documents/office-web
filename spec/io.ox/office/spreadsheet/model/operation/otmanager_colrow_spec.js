/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import {
    opSeries1, opSeries2,
    insertCols, deleteCols, changeCols, insertRows, deleteRows, changeRows,
    changeCells, mergeCells,
    insertTable, deleteTable, changeTable, changeTableCol,
    insertDVRule, deleteDVRule, changeDVRule, insertCFRule, deleteCFRule, changeCFRule,
    insertNote, deleteNote, changeNote, moveNotes, insertComment, deleteComment, changeComment, moveComments,
    sheetSelection, insertShape, deleteDrawing, changeDrawing
} from "~/spreadsheet/apphelper";

import {
    MM_MERGE, MM_HORIZONTAL, MM_VERTICAL, MM_UNMERGE, MM_ALL, ATTRS, COL_ATTRS, ROW_ATTRS,
    GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS,
    createTestRunner,
    colOps, rowOps, cellOps, hlinkOps, nameOps, tableOps, dvRuleOps, cfRuleOps,
    noteOps, commentOps, drawingOps, chartOps, drawingTextOps,
    selectOps, changeCellsOps, cellAnchorOps, drawingAnchorOps, drawingNoAnchorOps
} from "~/spreadsheet/othelper";

// tests ======================================================================

describe("module io.ox/office/spreadsheet/model/operation/otmanager", () => {

    // initialize OT test runner
    const testRunner = createTestRunner();

    // insertColumns ----------------------------------------------------------

    describe('"insertColumns" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(insertCols(1, "C"), [
                GLOBAL_OPS, STYLESHEET_OPS, rowOps(1), nameOps(1),
                deleteTable(1, "Table1"), deleteDVRule(1, 0), deleteCFRule(1, "R1"),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    describe('"insertColumns" and "insertColumns"', () => {
        it("should transform single intervals", () => {
            testRunner().runTest(insertCols(1, "B:D"), insertCols(1, "A:C"), insertCols(1, "E:G"), insertCols(1, "A:C"));
            testRunner().runTest(insertCols(1, "B:D"), insertCols(1, "B:D"), insertCols(1, "B:D"), insertCols(1, "E:G"));
            testRunner().runTest(insertCols(1, "B:D"), insertCols(1, "C:E"), insertCols(1, "B:D"), insertCols(1, "F:H"));
        });
        it("should transform interval lists #1", () => {
            testRunner().runTest(insertCols(1, "B D"), insertCols(1, "A B"), insertCols(1, "C F"), insertCols(1, "A C"));
            testRunner().runTest(insertCols(1, "B D"), insertCols(1, "B C"), insertCols(1, "B F"), insertCols(1, "C D"));
            testRunner().runTest(insertCols(1, "B D"), insertCols(1, "C D"), insertCols(1, "B E"), insertCols(1, "D F"));
            testRunner().runTest(insertCols(1, "B D"), insertCols(1, "D E"), insertCols(1, "B D"), insertCols(1, "F G"));
            testRunner().runTest(insertCols(1, "B D"), insertCols(1, "E F"), insertCols(1, "B D"), insertCols(1, "G H"));
        });
        it("should transform interval lists #2", () => {
            testRunner().runTest(insertCols(1, "B D"), insertCols(1, "A C"), insertCols(1, "C F"), insertCols(1, "A D"));
            testRunner().runTest(insertCols(1, "B D"), insertCols(1, "B D"), insertCols(1, "B E"), insertCols(1, "C F"));
            testRunner().runTest(insertCols(1, "B D"), insertCols(1, "C E"), insertCols(1, "B E"), insertCols(1, "D G"));
            testRunner().runTest(insertCols(1, "B D"), insertCols(1, "D F"), insertCols(1, "B D"), insertCols(1, "F H"));
            testRunner().runTest(insertCols(1, "B D"), insertCols(1, "E G"), insertCols(1, "B D"), insertCols(1, "G I"));
        });
        it("should shift intervals outside sheet", () => {
            testRunner().runBidiTest(insertCols(1, "XFB"), insertCols(1, "XFC:XFD"), null, insertCols(1, "XFD"));
            testRunner().runBidiTest(insertCols(1, "XFB"), insertCols(1, "XFD"),     null, []);
        });
        it("should not transform intervals in different sheets", () => {
            testRunner().runTest(insertCols(1, "C"), insertCols(2, "C"));
        });
    });

    describe('"insertColumns" and "deleteColumns"', () => {
        it("should transform single intervals", () => {
            testRunner().runBidiTest(insertCols(1, "A:B"), deleteCols(1, "B:C"), insertCols(1, "A:B"), deleteCols(1, "D:E"));
            testRunner().runBidiTest(insertCols(1, "B:C"), deleteCols(1, "B:C"), insertCols(1, "B:C"), deleteCols(1, "D:E"));
            testRunner().runBidiTest(insertCols(1, "C:D"), deleteCols(1, "B:C"), insertCols(1, "B:C"), deleteCols(1, "B E"));
            testRunner().runBidiTest(insertCols(1, "D:E"), deleteCols(1, "B:C"), insertCols(1, "B:C"), deleteCols(1, "B:C"));
            testRunner().runBidiTest(insertCols(1, "E:F"), deleteCols(1, "B:C"), insertCols(1, "C:D"), deleteCols(1, "B:C"));
        });
        it("should transform interval lists", () => {
            testRunner().runBidiTest(insertCols(1, "D G"), deleteCols(1, "A C"), insertCols(1, "B E"), deleteCols(1, "A C"));
            testRunner().runBidiTest(insertCols(1, "D G"), deleteCols(1, "B D"), insertCols(1, "C E"), deleteCols(1, "B E"));
            testRunner().runBidiTest(insertCols(1, "D G"), deleteCols(1, "C E"), insertCols(1, "C E"), deleteCols(1, "C F"));
            testRunner().runBidiTest(insertCols(1, "D G"), deleteCols(1, "D F"), insertCols(1, "D E"), deleteCols(1, "E G"));
            testRunner().runBidiTest(insertCols(1, "D G"), deleteCols(1, "E G"), insertCols(1, "D F"), deleteCols(1, "F I"));
            testRunner().runBidiTest(insertCols(1, "D G"), deleteCols(1, "F H"), insertCols(1, "D F"), deleteCols(1, "G J"));
            testRunner().runBidiTest(insertCols(1, "D G"), deleteCols(1, "G I"), insertCols(1, "D G"), deleteCols(1, "I K"));
            testRunner().runBidiTest(insertCols(1, "D G"), deleteCols(1, "H J"), insertCols(1, "D G"), deleteCols(1, "J L"));
        });
        it("should shift intervals outside sheet", () => {
            testRunner().runBidiTest(insertCols(1, "XFB"), deleteCols(1, "XFC:XFD"), null, deleteCols(1, "XFD"));
            testRunner().runBidiTest(insertCols(1, "XFB"), deleteCols(1, "XFD"),     null, []);
        });
        it("should not transform intervals in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), deleteCols(2, "C"));
        });
    });

    describe('"insertColumns" and "changeColumns"', () => {
        it("should transform intervals", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), opSeries2(changeCols, 1, ["A B C D", "A:B", "A:C",   "A:D",     "B:D",   "C:D", "A XFB:XFC XFC:XFD XFD:XFD A", "XFD"]),
                insertCols(1, "C"), opSeries2(changeCols, 1, ["A B D E", "A:B", "A:B D", "A:B D:E", "B D:E", "D:E", "A XFC:XFD XFD A"])
            );
            testRunner().runBidiTest(
                insertCols(1, "C:D D:E"), opSeries2(changeCols, 1, ["A B C D", "A:B", "A:C",   "A:D",     "B:E",     "C:E",   "D:E"]),
                insertCols(1, "C:D D:E"), opSeries2(changeCols, 1, ["A B E H", "A:B", "A:B E", "A:B E H", "B E H:I", "E H:I", "H:I"])
            );
        });
        it("should not transform intervals in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), changeCols(2, "A:D", "a1"));
        });
    });

    describe('"insertColumns" and "changeCells"', () => {
        it("should transform ranges", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), changeCellsOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4",       "A1 XFB1:XFC2 XFC1:XFD2 XFD1:XFD2 A2", "XFD1"),
                insertCols(1, "C"), changeCellsOps(1, "A1 B2 D3 E4", "A1:B2 B2:B3 D2:D3 D3:E4", "A1 XFC1:XFD2 XFD1:XFD2 A2")
            );
            testRunner().runBidiTest(
                insertCols(1, "C:D D:E"), changeCellsOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4 D4:E5"),
                insertCols(1, "C:D D:E"), changeCellsOps(1, "A1 B2 E3 H4", "A1:B2 B2:B3 E2:E3 E3:E4 H3:H4 H4:I5")
            );
        });
        it("should transform matrix formula range", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), changeCells(1, { A1: { mr: "A1:B2" }, C3: { mr: "C3:D4" } }),
                null,               changeCells(1, { A1: { mr: "A1:B2" }, D3: { mr: "D3:E4" } })
            );
        });
        it("should fail to split a matrix formula range", () => {
            testRunner().expectBidiError(insertCols(1, "C"), changeCells(1, { A1: { mr: "A1:D4" } }));
        });
        it("should fail to change a shared formula", () => {
            testRunner().runBidiTest(insertCols(1, "C"), changeCells(1, { A1: { si: null } }));
            testRunner().runBidiTest(insertCols(1, "C"), changeCells(1, { A1: { sr: null } }));
            testRunner().expectBidiError(insertCols(1, "C"), changeCells(1, { A1: { si: 1 } }));
            testRunner().expectBidiError(insertCols(1, "C"), changeCells(1, { A1: { sr: "A1:D4" } }));
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), changeCellsOps(2, "A1:D4"));
        });
    });

    describe('"insertColumns" and "mergeCells"', () => {
        it("should resize ranges (MERGE, HORIZONTAL, UNMERGE mode)", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), opSeries2(mergeCells, 1, "A2:B3 B4:C5 C6:D7", [MM_MERGE, MM_HORIZONTAL, MM_UNMERGE]),
                insertCols(1, "C"), opSeries2(mergeCells, 1, "A2:B3 B4:D5 D6:E7", [MM_MERGE, MM_HORIZONTAL, MM_UNMERGE])
            );
            testRunner().runBidiTest(
                insertCols(1, "XFB"), opSeries2(mergeCells, 1, "XFA1:XFC2 XFA3:XFD4", [MM_MERGE, MM_HORIZONTAL, MM_UNMERGE]),
                insertCols(1, "XFB"), opSeries2(mergeCells, 1, "XFA1:XFD2 XFA3:XFD4", [MM_MERGE, MM_HORIZONTAL, MM_UNMERGE])
            );
        });
        it("should split ranges (VERTICAL mode)", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), mergeCells(1, "A2:B3 B4:C5 C6:D7",       MM_VERTICAL),
                insertCols(1, "C"), mergeCells(1, "A2:B3 B4:B5 D4:D5 D6:E7", MM_VERTICAL)
            );
            testRunner().runBidiTest(
                insertCols(1, "XFB"), mergeCells(1, "XFA1:XFC2 XFA3:XFD4",                     MM_VERTICAL),
                insertCols(1, "XFB"), mergeCells(1, "XFA1:XFA2 XFC1:XFD2 XFA3:XFA4 XFC3:XFD4", MM_VERTICAL)
            );
        });
        it("should remove no-op operations", () => {
            testRunner().runBidiTest(insertCols(1, "C:D"), opSeries2(mergeCells, 1, "XFC1:XFD2", MM_ALL), null, []);
        });
        it("should not modify entire row ranges", () => {
            testRunner().runBidiTest(insertCols(1, "C:D"), opSeries2(mergeCells, 1, "A1:XFD2", MM_ALL));
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), mergeCells(2, "A1:D4"));
        });
    });

    describe('"insertColumns" and hyperlinks (RESIZE mode)', () => {
        it("should transform ranges", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), hlinkOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4", "A1 XFB1:XFC2 XFC1:XFD2 XFD1:XFD2 A2", "XFD1"),
                insertCols(1, "C"), hlinkOps(1, "A1 B2 D3 E4", "A1:B2 B2:D3 D3:E4", "A1 XFC1:XFD2 XFD1:XFD2 A2")
            );
            testRunner().runBidiTest(
                insertCols(1, "C:D D:E"), hlinkOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4 D4:E5"),
                insertCols(1, "C:D D:E"), hlinkOps(1, "A1 B2 E3 H4", "A1:B2 B2:E3 E3:H4 H4:I5")
            );
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), hlinkOps(2, "A1:D4"));
        });
    });

    describe('"insertColumns" and "insertTable"', () => {
        it("should shift table range", () => {
            testRunner().runBidiTest(
                insertCols(1, "E"), opSeries1(insertTable, [1, "Table3", "A5:D8"], [1, "Table4", "E5:H8"]),
                insertCols(1, "E"), opSeries1(insertTable, [1, "Table3", "A5:D8"], [1, "Table4", "F5:I8"])
            );
        });
        it("should fail to expand table range", () => {
            testRunner().expectBidiError(insertCols(1, "C"), insertTable(1, "Table3", "A5:D8"));
        });
        it("should delete table range shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertCols(1, "A:I"), insertTable(1, "Table3", "XFA5:XFD8"),
                [deleteTable(1, "Table3"), insertCols(1, "A:I")], []
            );
            testRunner().runBidiTest(
                insertCols(1, "D:E"), insertTable(1, "Table3", "XFA5:XFD8"),
                [deleteTable(1, "Table3"), insertCols(1, "D:E")], []
            );
        });
        it("should not transform tables in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), insertTable(2, "Table3", "A5:D8"));
        });
    });

    describe('"insertColumns" and "changeTable"', () => {
        it("should shift table range", () => {
            testRunner().runBidiTest(
                insertCols(1, "E"), opSeries2(changeTable, 1, "Table1", [{ attr: ATTRS }, "A1:D4", "E1:H4"]),
                insertCols(1, "E"), opSeries2(changeTable, 1, "Table1", [{ attr: ATTRS }, "A1:D4", "F1:I4"])
            );
        });
        it("should fail to expand table range", () => {
            testRunner().expectBidiError(insertCols(1, "C"), changeTable(1, "Table1", "A1:D4"));
        });
        it("should delete table range shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertCols(1, "A:I"), changeTable(1, "Table1", "XFA1:XFD4"),
                [deleteTable(1, "Table1"), insertCols(1, "A:I")], []
            );
            testRunner().runBidiTest(
                insertCols(1, "D:E"), changeTable(1, "Table1", "XFA1:XFD4"),
                [deleteTable(1, "Table1"), insertCols(1, "D:E")], []
            );
        });
        it("should not transform tables in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), changeTable(2, "Table2", "A1:D4"));
        });
    });

    describe('"insertColumns" and "changeTableColumn"', () => {
        it("should fail in same sheet", () => {
            testRunner().expectBidiError(insertCols(1, "C"), changeTableCol(1, "Table1", 0, { attrs: ATTRS }));
        });
        it("should skip different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), changeTableCol(2, "Table2", 0, { attrs: ATTRS }));
        });
    });

    describe('"insertColumns" and tables', () => {
        it("should handle insert/change sequence", () => {
            testRunner().runBidiTest(
                insertCols(1, "A:I"), [insertTable(1, "Table3", "XFA1:XFD4"), changeTable(1, "Table3", { attrs: ATTRS }), changeTableCol(1, "Table3", 0, { attrs: ATTRS })],
                [deleteTable(1, "Table3"), insertCols(1, "A:I")], []
            );
        });
        it("should handle insert/change/delete sequence", () => {
            testRunner().runBidiTest(
                insertCols(1, "A:I"), [insertTable(1, "Table3", "XFA1:XFD4"), changeTable(1, "Table3", { attrs: ATTRS }), changeTableCol(1, "Table3", 0, { attrs: ATTRS }), deleteTable(1, "Table3")],
                null, []
            );
        });
    });

    describe('"insertColumns" and "insertDVRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                insertCols(1, "E"), opSeries2(insertDVRule, 1, 0, ["A1:D4", "C1:F4", "E1:H4"]),
                insertCols(1, "E"), opSeries2(insertDVRule, 1, 0, ["A1:E4", "C1:G4", "F1:I4"])
            );
        });
        it("should delete rule shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertCols(1, "A:I"), insertDVRule(1, 0, "XFA1:XFD4"),
                [deleteDVRule(1, 0), insertCols(1, "A:I")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), insertDVRule(2, 0, "A1:D4"));
        });
    });

    describe('"insertColumns" and "changeDVRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                insertCols(1, "E"), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, "A1:D4", "C1:F4", "E1:H4"]),
                insertCols(1, "E"), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, "A1:E4", "C1:G4", "F1:I4"])
            );
        });
        it("should delete rule shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertCols(1, "A:I"), changeDVRule(1, 0, "XFA1:XFD4"),
                [deleteDVRule(1, 0), insertCols(1, "A:I")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), changeDVRule(2, 0, "A1:D4"));
        });
    });

    describe('"insertColumns" and "insertCFRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                insertCols(1, "E"), opSeries2(insertCFRule, 1, "R1", ["A1:D4", "C1:F4", "E1:H4"]),
                insertCols(1, "E"), opSeries2(insertCFRule, 1, "R1", ["A1:E4", "C1:G4", "F1:I4"])
            );
        });
        it("should delete rule shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertCols(1, "A:I"), insertCFRule(1, "R1", "XFA1:XFD4"),
                [deleteCFRule(1, "R1"), insertCols(1, "A:I")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), insertCFRule(2, "R1", "A1:D4"));
        });
    });

    describe('"insertColumns" and "changeCFRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                insertCols(1, "E"), opSeries2(changeCFRule, 1, "R1", [{ attr: ATTRS }, "A1:D4", "C1:F4", "E1:H4"]),
                insertCols(1, "E"), opSeries2(changeCFRule, 1, "R1", [{ attr: ATTRS }, "A1:E4", "C1:G4", "F1:I4"])
            );
        });
        it("should delete rule shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertCols(1, "A:I"), changeCFRule(1, "R1", "XFA1:XFD4"),
                [deleteCFRule(1, "R1"), insertCols(1, "A:I")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), changeCFRule(2, "R1", "A1:D4"));
        });
    });

    describe('"insertColumns" and cell anchor', () => {
        it("should transform cell anchor", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), cellAnchorOps(1, "A1", "B2", "C3", "D4", "XFC5"),
                insertCols(1, "C"), cellAnchorOps(1, "A1", "B2", "D3", "E4", "XFD5")
            );
            testRunner().runBidiTest(
                insertCols(1, "C:D D:E"), cellAnchorOps(1, "A1", "B2", "C3", "D4", "E5", "XEZ6"),
                insertCols(1, "C:D D:E"), cellAnchorOps(1, "A1", "B2", "E3", "H4", "I5", "XFD6")
            );
        });
        it("should handle deleted cell note", () => {
            testRunner().runBidiTest(insertCols(1, "C"), insertNote(1, "XFD1", "abc"), [deleteNote(1, "XFD1"), insertCols(1, "C")], []);
            testRunner().runBidiTest(insertCols(1, "C"), deleteNote(1, "XFD1"),        null,                                        []);
            testRunner().runBidiTest(insertCols(1, "C"), changeNote(1, "XFD1", "abc"), [deleteNote(1, "XFD1"), insertCols(1, "C")], []);
        });
        it("should handle deleted cell comment", () => {
            testRunner().runBidiTest(insertCols(1, "C"), insertComment(1, "XFD1", 0, { text: "abc" }), [deleteComment(1, "XFD1", 0), insertCols(1, "C")], []);
            testRunner().runBidiTest(insertCols(1, "C"), insertComment(1, "XFD1", 1, { text: "abc" }), null,                                              []);
            testRunner().runBidiTest(insertCols(1, "C"), deleteComment(1, "XFD1", 0),                  null,                                              []);
            testRunner().runBidiTest(insertCols(1, "C"), deleteComment(1, "XFD1", 1),                  null,                                              []);
            testRunner().runBidiTest(insertCols(1, "C"), changeComment(1, "XFD1", 0, { text: "abc" }), [deleteComment(1, "XFD1", 0), insertCols(1, "C")], []);
            testRunner().runBidiTest(insertCols(1, "C"), changeComment(1, "XFD1", 1, { text: "abc" }), null,                                              []);
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), cellAnchorOps(2, "D4"));
        });
    });

    describe('"insertColumns" and "moveNotes"', () => {
        it("should transform cell anchors", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), moveNotes(1, "A1 B2 C3 D4", "D1 C2 B3 A4"),
                insertCols(1, "C"), moveNotes(1, "A1 B2 D3 E4", "E1 D2 B3 A4")
            );
        });
        it("should handle deleted source anchor", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), moveNotes(1, "A1 XFD2 XFD3 A4", "E1 E2 XFD1 E4"),
                [deleteNote(1, "E2"), deleteNote(1, "XFD1"), insertCols(1, "C")], moveNotes(1, "A1 A4", "F1 F4")
            );
        });
        it("should handle deleted target anchor", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), moveNotes(1, "E1 E2 E3", "A1 XFD2 A3"),
                [deleteNote(1, "XFD2"), insertCols(1, "C")], [deleteNote(1, "F2"), moveNotes(1, "F1 F3", "A1 A3")]
            );
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), moveNotes(2, "D4", "E5"));
        });
    });

    describe('"insertColumns" and "moveComments"', () => {
        it("should transform cell anchors", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), moveComments(1, "A1 B2 C3 D4", "D1 C2 B3 A4"),
                insertCols(1, "C"), moveComments(1, "A1 B2 D3 E4", "E1 D2 B3 A4")
            );
        });
        it("should handle deleted source anchor", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), moveComments(1, "A1 XFD2 XFD3 A4", "E1 E2 XFD1 E4"),
                [deleteComment(1, "E2", 0), deleteComment(1, "XFD1", 0), insertCols(1, "C")], moveComments(1, "A1 A4", "F1 F4")
            );
        });
        it("should handle deleted target anchor", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), moveComments(1, "E1 E2 E3", "A1 XFD2 A3"),
                [deleteComment(1, "XFD2", 0), insertCols(1, "C")], [deleteComment(1, "F2", 0), moveComments(1, "F1 F3", "A1 A3")]
            );
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), moveComments(2, "D4", "E5"));
        });
    });

    describe('"insertColumns" and drawing anchor', () => {
        it("should transform drawing anchor", () => {
            testRunner().runBidiTest(
                insertCols(1, "C"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 C2 0 20", "2 B1 10 10 C2 20 20", "2 C1 10 10 D2 20 20", "2 G1 10 10 XFD2 20 20"),
                insertCols(1, "C"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 C2 0 20", "2 B1 10 10 D2 20 20", "2 D1 10 10 E2 20 20", "2 G1 10 10 XFD2 20 20"),
                { warnings: true } // missing transformation of absolute drawing position

            );
            testRunner().runBidiTest(
                insertCols(1, "C:D D:E"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 C2 20 20", "2 C1 10 10 D2 20 20", "2 D1 10 10 E2 20 20", "2 G1 10 10 XFC2 20 20"),
                insertCols(1, "C:D D:E"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 E2 20 20", "2 E1 10 10 H2 20 20", "2 H1 10 10 I2 20 20", "2 H1 10 10 XFD2 20 20"),
                { warnings: true } // missing transformation of absolute drawing position
            );
        });
        it("should log warnings for absolute anchor", () => {
            const anchorOps = drawingAnchorOps(1, "2 A1 0 0 C3 0 0");
            testRunner().expectBidiWarnings(insertCols(1, "C"), anchorOps, anchorOps.length);
        });
        it("should not log warnings without anchor attribute", () => {
            testRunner().expectBidiWarnings(insertCols(1, "C"), drawingNoAnchorOps(1), 0);
        });
        it("should not transform drawing anchor in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), drawingAnchorOps(2, "2 A1 10 10 D4 20 20"));
        });
    });

    describe('"insertColumns" and "sheetSelection"', () => {
        it("should transform selected ranges", () => {
            testRunner().runBidiTest(
                insertCols(1, "C:D D:E"), sheetSelection(1, "A1 B2 C3 D4 A1:B2 B2:C3 C3:D4 D4:E5 XEW1:XEZ4 XEX1:XFA4 XEY1:XFB4 XEZ1:XFC4", 0, "A1", [0, 1]),
                insertCols(1, "C:D D:E"), sheetSelection(1, "A1 B2 E3 H4 A1:B2 B2:E3 E3:H4 H4:I5 XFA1:XFD4 XFB1:XFD4 XFC1:XFD4 XFD1:XFD4", 0, "A1", [0, 1])
            );
        });
        it("should transform active address", () => {
            testRunner().runBidiTest(
                insertCols(1, "C:D D:E"), opSeries2(sheetSelection, 1, "A1:XFD4", 0, ["A1", "B2", "C3", "D4", "XEZ1", "XFA2"]),
                insertCols(1, "C:D D:E"), opSeries2(sheetSelection, 1, "A1:XFD4", 0, ["A1", "B2", "E3", "H4", "XFD1", "XFD2"])
            );
        });
        it("should transform origin address", () => {
            testRunner().runBidiTest(
                insertCols(1, "C:D D:E"), opSeries2(sheetSelection, 1, "A1:XFD4", 0, "A1", ["A1", "B2", "C3", "D4", "XEZ1", "XFA2"]),
                insertCols(1, "C:D D:E"), opSeries2(sheetSelection, 1, "A1:XFD4", 0, "A1", ["A1", "B2", "E3", "H4", "XFD1", "XFD2"])
            );
        });
        it("should transform deleted active range", () => {
            testRunner().runBidiTest(insertCols(1, "C"), sheetSelection(1, "D4 C3 XFD1:XFD4 B2 A1", 0, "D4",   "D4"), null, sheetSelection(1, "E4 D3 B2 A1", 0, "E4", "E4"));
            testRunner().runBidiTest(insertCols(1, "C"), sheetSelection(1, "D4 C3 XFD1:XFD4 B2 A1", 1, "C3",   "C3"), null, sheetSelection(1, "E4 D3 B2 A1", 1, "D3", "D3"));
            testRunner().runBidiTest(insertCols(1, "C"), sheetSelection(1, "D4 C3 XFD1:XFD4 B2 A1", 2, "XFD1", "A1"), null, sheetSelection(1, "E4 D3 B2 A1", 0, "E4"));
            testRunner().runBidiTest(insertCols(1, "C"), sheetSelection(1, "D4 C3 XFD1:XFD4 B2 A1", 3, "B2",   "B2"), null, sheetSelection(1, "E4 D3 B2 A1", 2, "B2", "B2"));
            testRunner().runBidiTest(insertCols(1, "C"), sheetSelection(1, "D4 C3 XFD1:XFD4 B2 A1", 4, "A1",   "A1"), null, sheetSelection(1, "E4 D3 B2 A1", 3, "A1", "A1"));
        });
        it("should keep active cell when deleting the selection", () => {
            testRunner().runBidiTest(insertCols(1, "C:D D:E"), sheetSelection(1, "XFC6:XFD9 XFA1:XFD4", 0, "XFC7"), null, sheetSelection(1, "XFD7", 0, "XFD7"));
            testRunner().runBidiTest(insertCols(1, "C:D D:E"), sheetSelection(1, "XFC6:XFD9 XFA1:XFD4", 1, "XFB2"), null, sheetSelection(1, "XFD2", 0, "XFD2"));
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(insertCols(1, "C"), sheetSelection(2, "A1:D4", 0, "A1"));
        });
    });

    // deleteColumns ----------------------------------------------------------

    describe('"deleteColumns" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), [
                GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS, rowOps(1), nameOps(1),
                deleteTable(1, "Table1"), deleteDVRule(1, 0), deleteCFRule(1, "R1"),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    describe('"deleteColumns" and "deleteColumns"', () => {
        it("should transform single intervals #1", () => {
            testRunner().runTest(deleteCols(1, "C:D"), deleteCols(1, "A"), deleteCols(1, "B:C"), deleteCols(1, "A"));
            testRunner().runTest(deleteCols(1, "C:D"), deleteCols(1, "B"), deleteCols(1, "B:C"), deleteCols(1, "B"));
            testRunner().runTest(deleteCols(1, "C:D"), deleteCols(1, "C"), deleteCols(1, "C"),   []);
            testRunner().runTest(deleteCols(1, "C:D"), deleteCols(1, "D"), deleteCols(1, "C"),   []);
            testRunner().runTest(deleteCols(1, "C:D"), deleteCols(1, "E"), deleteCols(1, "C:D"), deleteCols(1, "C"));
            testRunner().runTest(deleteCols(1, "C:D"), deleteCols(1, "F"), deleteCols(1, "C:D"), deleteCols(1, "D"));
        });
        it("should transform single intervals #2", () => {
            testRunner().runTest(deleteCols(1, "D"), deleteCols(1, "A:B"), deleteCols(1, "B"), deleteCols(1, "A:B"));
            testRunner().runTest(deleteCols(1, "D"), deleteCols(1, "B:C"), deleteCols(1, "B"), deleteCols(1, "B:C"));
            testRunner().runTest(deleteCols(1, "D"), deleteCols(1, "C:D"), [],                 deleteCols(1, "C"));
            testRunner().runTest(deleteCols(1, "D"), deleteCols(1, "D:E"), [],                 deleteCols(1, "D"));
            testRunner().runTest(deleteCols(1, "D"), deleteCols(1, "E:F"), deleteCols(1, "D"), deleteCols(1, "D:E"));
            testRunner().runTest(deleteCols(1, "D"), deleteCols(1, "F:G"), deleteCols(1, "D"), deleteCols(1, "E:F"));
        });
        it("should transform single intervals #3", () => {
            testRunner().runTest(deleteCols(1, "C:D"), deleteCols(1, "A:B"), deleteCols(1, "A:B"), deleteCols(1, "A:B"));
            testRunner().runTest(deleteCols(1, "C:D"), deleteCols(1, "B:C"), deleteCols(1, "B"),   deleteCols(1, "B"));
            testRunner().runTest(deleteCols(1, "C:D"), deleteCols(1, "C:D"), [],                   []);
            testRunner().runTest(deleteCols(1, "C:D"), deleteCols(1, "D:E"), deleteCols(1, "C"),   deleteCols(1, "C"));
            testRunner().runTest(deleteCols(1, "C:D"), deleteCols(1, "E:F"), deleteCols(1, "C:D"), deleteCols(1, "C:D"));
        });
        it("should transform interval lists", () => {
            testRunner().runTest(deleteCols(1, "D G"), deleteCols(1, "A C"), deleteCols(1, "B E"), deleteCols(1, "A C"));
            testRunner().runTest(deleteCols(1, "D G"), deleteCols(1, "B D"), deleteCols(1, "E"),   deleteCols(1, "B"));
            testRunner().runTest(deleteCols(1, "D G"), deleteCols(1, "C E"), deleteCols(1, "C E"), deleteCols(1, "C:D"));
            testRunner().runTest(deleteCols(1, "D G"), deleteCols(1, "D F"), deleteCols(1, "E"),   deleteCols(1, "E"));
            testRunner().runTest(deleteCols(1, "D G"), deleteCols(1, "E G"), deleteCols(1, "D"),   deleteCols(1, "D"));
            testRunner().runTest(deleteCols(1, "D G"), deleteCols(1, "F H"), deleteCols(1, "D F"), deleteCols(1, "E:F"));
            testRunner().runTest(deleteCols(1, "D G"), deleteCols(1, "G I"), deleteCols(1, "D"),   deleteCols(1, "G"));
            testRunner().runTest(deleteCols(1, "D G"), deleteCols(1, "H J"), deleteCols(1, "D G"), deleteCols(1, "F H"));
        });
        it("should not transform intervals in different sheets", () => {
            testRunner().runTest(deleteCols(1, "C"), deleteCols(2, "D"));
        });
    });

    describe('"deleteColumns" and "changeColumns"', () => {
        it("should transform intervals", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), opSeries2(changeCols, 1, ["A B C D E", "A:B B:C C:D D:E", "C"]),
                deleteCols(1, "C"), opSeries2(changeCols, 1, ["A B C D",   "A:B B C C:D"])
            );
            testRunner().runBidiTest(
                deleteCols(1, "C:D F:G"), changeCols(1, "A:C D:F G:I"),
                deleteCols(1, "C:D F:G"), changeCols(1, "A:B C D:E")
            );
        });
        it("should transform matrix formula range", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C:D"), changeCells(1, { A1: { mr: "A1:B2" }, C3: { mr: "C3:D4" }, E5: { mr: "E5:F6" } }),
                null,                 changeCells(1, { A1: { mr: "A1:B2" }, C5: { mr: "C5:D6" } })
            );
        });
        it("should fail to shrink a matrix formula range", () => {
            testRunner().expectBidiError(deleteCols(1, "C"), changeCells(1, { A1: { mr: "A1:D4" } }));
        });
        it("should fail to change a shared formula", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), changeCells(1, { A1: { si: null } }));
            testRunner().runBidiTest(deleteCols(1, "C"), changeCells(1, { A1: { sr: null } }));
            testRunner().expectBidiError(deleteCols(1, "C"), changeCells(1, { A1: { si: 1 } }));
            testRunner().expectBidiError(deleteCols(1, "C"), changeCells(1, { A1: { sr: "A1:D4" } }));
        });
        it("should not transform intervals in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), changeCols(2, "A:D", "a1"));
        });
    });

    describe('"deleteColumns" and "changeCells"', () => {
        it("should transform ranges", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), changeCellsOps(1, "A1 B2 C3 D4 E5", "A1:B2 B2:C3 C3:D4 D4:E5", "C3"),
                deleteCols(1, "C"), changeCellsOps(1, "A1 B2 C4 D5",    "A1:B2 B2:B3 C3:C4 C4:D5")
            );
            testRunner().runBidiTest(
                deleteCols(1, "C:D F:G"), changeCellsOps(1, "A1:C3 D4:F6 G7:I9"),
                deleteCols(1, "C:D F:G"), changeCellsOps(1, "A1:B3 C4:C6 D7:E9")
            );
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), changeCellsOps(2, "A1:D4"));
        });
    });

    describe('"deleteColumns" and "mergeCells"', () => {
        it("should resize ranges", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), opSeries2(mergeCells, 1, "A2:B3 B4:D5 D6:E7", MM_ALL),
                deleteCols(1, "C"), opSeries2(mergeCells, 1, "A2:B3 B4:C5 C6:D7", MM_ALL)
            );
        });
        it("should delete single ranges", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), mergeCells(1, "A1:C2 A3:C3 B4:C5 B6:C6", MM_MERGE),
                deleteCols(1, "C"), mergeCells(1, "A1:B2 A3:B3 B4:B5",       MM_MERGE) // deletes single cell
            );
            testRunner().runBidiTest(
                deleteCols(1, "C"), mergeCells(1, "A1:C2 A3:C3 B4:C5 B6:C6", MM_VERTICAL),
                deleteCols(1, "C"), mergeCells(1, "A1:B2 B4:B5",             MM_VERTICAL) // deletes row vector
            );
            testRunner().runBidiTest(
                deleteCols(1, "C"), mergeCells(1, "A1:C2 A3:C3 B4:C5 B6:C6", MM_HORIZONTAL),
                deleteCols(1, "C"), mergeCells(1, "A1:B2 A3:B3",             MM_HORIZONTAL) // deletes column vector
            );
            testRunner().runBidiTest(
                deleteCols(1, "C"), mergeCells(1, "A1:C2 A3:C3 B4:C5 B6:C6", MM_UNMERGE),
                deleteCols(1, "C"), mergeCells(1, "A1:B2 A3:B3 B4:B5 B6",    MM_UNMERGE) // keeps all
            );
        });
        it("should remove no-op operations", () => {
            testRunner().runBidiTest(deleteCols(1, "C:D"), opSeries2(mergeCells, 1, "C3:D4", MM_ALL), null, []);
        });
        it("should not modify entire row ranges", () => {
            testRunner().runBidiTest(deleteCols(1, "C:D"), opSeries2(mergeCells, 1, "A1:XFD2", MM_ALL));
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), mergeCells(2, "A1:D4"));
        });
    });

    describe('"deleteColumns" and hyperlinks (RESIZE mode)', () => {
        it("should transform ranges", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), hlinkOps(1, "A1 B2 C3 D4 E5", "A1:B2 B2:C3 C3:D4 D4:E5", "C3"),
                deleteCols(1, "C"), hlinkOps(1, "A1 B2 C4 D5",    "A1:B2 B2:B3 C3:C4 C4:D5")
            );
            testRunner().runBidiTest(
                deleteCols(1, "C:D F:G"), hlinkOps(1, "A1:C3 D4:F6 G7:I9"),
                deleteCols(1, "C:D F:G"), hlinkOps(1, "A1:B3 C4:C6 D7:E9")
            );
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), hlinkOps(2, "A1:D4"));
        });
    });

    describe('"deleteColumns" and "insertTable"', () => {
        it("should shift table range", () => {
            testRunner().runBidiTest(
                deleteCols(1, "E"), opSeries1(insertTable, [1, "Table3", "A5:D8"], [1, "Table4", "G5:J8"]),
                deleteCols(1, "E"), opSeries1(insertTable, [1, "Table3", "A5:D8"], [1, "Table4", "F5:I8"])
            );
        });
        it("should fail to shrink table range", () => {
            testRunner().expectBidiError(deleteCols(1, "C"), insertTable(1, "Table3", "A5:D8"));
        });
        it("should delete table range explicitly", () => {
            testRunner().runBidiTest(
                deleteCols(1, "B:G"), insertTable(1, "Table3", "C5:F8"),
                [deleteTable(1, "Table3"), deleteCols(1, "B:G")], []
            );
        });
        it("should not transform tables in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), insertTable(2, "Table3", "A5:D8"));
        });
    });

    describe('"deleteColumns" and "changeTable"', () => {
        it("should shift and shrink table range", () => {
            testRunner().runBidiTest(
                deleteCols(1, "E"), opSeries2(changeTable, 1, "Table1", [{ attrs: ATTRS }, "A1:D4", "G1:J4"]),
                deleteCols(1, "E"), opSeries2(changeTable, 1, "Table1", [{ attrs: ATTRS }, "A1:D4", "F1:I4"])
            );
        });
        it("should fail to shrink table range", () => {
            testRunner().expectBidiError(deleteCols(1, "C"), changeTable(1, "Table1", "A1:D4"));
        });
        it("should delete table range explicitly", () => {
            testRunner().runBidiTest(
                deleteCols(1, "B:G"), changeTable(1, "Table1", "C1:F4"),
                [deleteTable(1, "Table1"), deleteCols(1, "B:G")], []
            );
        });
        it("should not transform tables in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), changeTable(2, "Table2", "A1:D4"));
        });
    });

    describe('"deleteColumns" and "changeTableColumn"', () => {
        it("should fail in same sheet", () => {
            testRunner().expectBidiError(deleteCols(1, "C"), changeTableCol(1, "Table1", 0, { attrs: ATTRS }));
        });
        it("should skip different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), changeTableCol(2, "Table2", 0, { attrs: ATTRS }));
        });
    });

    describe('"deleteColumns" and tables', () => {
        it("should handle insert/change sequence", () => {
            testRunner().runBidiTest(
                deleteCols(1, "B:G"), [insertTable(1, "Table3", "C1:F4"), changeTable(1, "Table3", { attrs: ATTRS }), changeTableCol(1, "Table3", 0, { attrs: ATTRS })],
                [deleteTable(1, "Table3"), deleteCols(1, "B:G")], []
            );
        });
        it("should handle insert/change/delete sequence", () => {
            testRunner().runBidiTest(
                deleteCols(1, "B:G"), [insertTable(1, "Table3", "C1:F4"), changeTable(1, "Table3", { attrs: ATTRS }), changeTableCol(1, "Table3", 0, { attrs: ATTRS }), deleteTable(1, "Table3")],
                null, []
            );
        });
        it("should handle insert/change sequences on both sides", () => {
            testRunner().runTest(
                [insertTable(1, "Table3", "A1:D4"), deleteCols(2, "A:D"), changeTable(1, "Table3", { attrs: ATTRS })],
                [insertTable(2, "Table4", "A1:D4"), deleteCols(1, "A:D"), changeTable(2, "Table4", { attrs: ATTRS })],
                [deleteTable(2, "Table4"), deleteCols(2, "A:D")],
                [deleteTable(1, "Table3"), deleteCols(1, "A:D")]
            );
            testRunner().runTest(
                [insertTable(1, "Table3", "E1:H4"), deleteCols(1, "A:D"), changeTable(1, "Table3", { attrs: ATTRS })],
                [insertTable(1, "Table4", "A1:D4"), deleteCols(1, "E:H"), changeTable(1, "Table4", { attrs: ATTRS })],
                [deleteTable(1, "Table4"), deleteCols(1, "A:D")],
                [deleteTable(1, "Table3"), deleteCols(1, "A:D")]
            );
        });
        it("should handle insert/change/delete sequences on both sides", () => {
            testRunner().runTest(
                [insertTable(1, "Table3", "A1:D4"), deleteCols(2, "A:D"), changeTable(1, "Table3", { attrs: ATTRS }), deleteTable(1, "Table3")],
                [insertTable(2, "Table4", "A1:D4"), deleteCols(1, "A:D"), changeTable(2, "Table4", { attrs: ATTRS }), deleteTable(2, "Table4")],
                deleteCols(2, "A:D"),
                deleteCols(1, "A:D")
            );
            testRunner().runTest(
                [insertTable(1, "Table3", "E1:H4"), deleteCols(1, "A:D"), changeTable(1, "Table3", { attrs: ATTRS }), deleteTable(1, "Table3")],
                [insertTable(1, "Table4", "A1:D4"), deleteCols(1, "E:H"), changeTable(1, "Table4", { attrs: ATTRS }), deleteTable(1, "Table4")],
                deleteCols(1, "A:D"),
                deleteCols(1, "A:D")
            );
        });
    });

    describe('"deleteColumns" and "insertDVRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                deleteCols(1, "E"), opSeries2(insertDVRule, 1, 0, ["A1:D4", "C1:F4", "F1:I4"]),
                deleteCols(1, "E"), opSeries2(insertDVRule, 1, 0, ["A1:D4", "C1:E4", "E1:H4"])
            );
        });
        it("should delete rule explicitly", () => {
            testRunner().runBidiTest(
                deleteCols(1, "A:I"), insertDVRule(1, 0, "C1:F4"),
                [deleteDVRule(1, 0), deleteCols(1, "A:I")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), insertDVRule(2, 0, "A1:D4"));
        });
    });

    describe('"deleteColumns" and "changeDVRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                deleteCols(1, "E"), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, "A1:D4", "C1:F4", "F1:I4"]),
                deleteCols(1, "E"), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, "A1:D4", "C1:E4", "E1:H4"])
            );
        });
        it("should delete rule explicitly", () => {
            testRunner().runBidiTest(
                deleteCols(1, "A:I"), changeDVRule(1, 0, "C1:F4"),
                [deleteDVRule(1, 0), deleteCols(1, "A:I")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), changeDVRule(2, 0, "A1:D4"));
        });
    });

    describe('"deleteColumns" and "insertCFRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                deleteCols(1, "E"), opSeries2(insertCFRule, 1, "R1", ["A1:D4", "C1:F4", "F1:I4"]),
                deleteCols(1, "E"), opSeries2(insertCFRule, 1, "R1", ["A1:D4", "C1:E4", "E1:H4"])
            );
        });
        it("should delete rule explicitly", () => {
            testRunner().runBidiTest(
                deleteCols(1, "A:I"), insertCFRule(1, "R1", "C1:F4"),
                [deleteCFRule(1, "R1"), deleteCols(1, "A:I")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), insertCFRule(2, "R1", "A1:D4"));
        });
    });

    describe('"deleteColumns" and "changeCFRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                deleteCols(1, "E"), opSeries2(changeCFRule, 1, "R1", [{ attr: ATTRS }, "A1:D4", "C1:F4", "F1:I4"]),
                deleteCols(1, "E"), opSeries2(changeCFRule, 1, "R1", [{ attr: ATTRS }, "A1:D4", "C1:E4", "E1:H4"])
            );
        });
        it("should delete rule explicitly", () => {
            testRunner().runBidiTest(
                deleteCols(1, "A:I"), changeCFRule(1, "R1", "C1:F4"),
                [deleteCFRule(1, "R1"), deleteCols(1, "A:I")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), changeCFRule(2, "R1", "A1:D4"));
        });
    });

    describe('"deleteColumns" and cell anchor', () => {
        it("should transform cell anchor", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), cellAnchorOps(1, "A1", "B2", "D4", "E5"),
                deleteCols(1, "C"), cellAnchorOps(1, "A1", "B2", "C4", "D5")
            );
            testRunner().runBidiTest(
                deleteCols(1, "C:D F:G"), cellAnchorOps(1, "A1", "B2", "E5", "H8", "I9"),
                deleteCols(1, "C:D F:G"), cellAnchorOps(1, "A1", "B2", "C5", "D8", "E9")
            );
        });
        it("should handle deleted cell note", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), insertNote(1, "C3", "abc"), [deleteNote(1, "C3"), deleteCols(1, "C")], []);
            testRunner().runBidiTest(deleteCols(1, "C"), deleteNote(1, "C3"),        null,                                      []);
            testRunner().runBidiTest(deleteCols(1, "C"), changeNote(1, "C3", "abc"), [deleteNote(1, "C3"), deleteCols(1, "C")], []);
        });
        it("should handle deleted cell comment", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), insertComment(1, "C3", 0, { text: "abc" }), [deleteComment(1, "C3", 0), deleteCols(1, "C")], []);
            testRunner().runBidiTest(deleteCols(1, "C"), insertComment(1, "C3", 1, { text: "abc" }), null,                                            []);
            testRunner().runBidiTest(deleteCols(1, "C"), deleteComment(1, "C3", 0),                  null,                                            []);
            testRunner().runBidiTest(deleteCols(1, "C"), deleteComment(1, "C3", 1),                  null,                                            []);
            testRunner().runBidiTest(deleteCols(1, "C"), changeComment(1, "C3", 0, { text: "abc" }), [deleteComment(1, "C3", 0), deleteCols(1, "C")], []);
            testRunner().runBidiTest(deleteCols(1, "C"), changeComment(1, "C3", 1, { text: "abc" }), null,                                            []);
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), cellAnchorOps(2, "D4"));
        });
    });

    describe('"deleteColumns" and "moveNotes"', () => {
        it("should transform cell anchors", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), moveNotes(1, "A1 B2 D3 E4", "E1 D2 B3 A4"),
                deleteCols(1, "C"), moveNotes(1, "A1 B2 C3 D4", "D1 C2 B3 A4")
            );
        });
        it("should handle deleted source anchor", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), moveNotes(1, "B1 C2 C3 D4", "E1 E2 C1 E4"),
                [deleteNote(1, "E2"), deleteNote(1, "C1"), deleteCols(1, "C")], moveNotes(1, "B1 C4", "D1 D4")
            );
        });
        it("should handle deleted target anchor", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), moveNotes(1, "E1 E2 E3", "B1 C2 D3"),
                [deleteNote(1, "C2"), deleteCols(1, "C")], [deleteNote(1, "D2"), moveNotes(1, "D1 D3", "B1 C3")]
            );
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), moveNotes(2, "D4", "E5"));
        });
    });

    describe('"deleteColumns" and "moveComments"', () => {
        it("should transform cell anchors", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), moveComments(1, "A1 B2 D3 E4", "E1 D2 B3 A4"),
                deleteCols(1, "C"), moveComments(1, "A1 B2 C3 D4", "D1 C2 B3 A4")
            );
        });
        it("should handle deleted source anchor", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), moveComments(1, "B1 C2 C3 D4", "E1 E2 C1 E4"),
                [deleteComment(1, "E2", 0), deleteComment(1, "C1", 0), deleteCols(1, "C")], moveComments(1, "B1 C4", "D1 D4")
            );
        });
        it("should handle deleted target anchor", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), moveComments(1, "E1 E2 E3", "B1 C2 D3"),
                [deleteComment(1, "C2", 0), deleteCols(1, "C")], [deleteComment(1, "D2", 0), moveComments(1, "D1 D3", "B1 C3")]
            );
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), moveComments(2, "D4", "E5"));
        });
    });

    describe('"deleteColumns" and drawing anchor', () => {
        it("should transform drawing anchor", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 C2 20 20", "2 C1 10 10 D2 20 20", "2 D1 10 10 E2 20 20"),
                deleteCols(1, "C"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 C2 0 20",  "2 C1 0 10 C2 20 20",  "2 C1 10 10 D2 20 20"),
                { warnings: true } // missing transformation of absolute drawing position
            );
            testRunner().runBidiTest(
                deleteCols(1, "C:D F:G"), drawingAnchorOps(1, "2 A1 10 10 D2 20 20", "2 B1 10 10 E2 20 20", "2 C1 10 10 F2 20 20", "2 E1 10 10 H2 20 20"),
                deleteCols(1, "C:D F:G"), drawingAnchorOps(1, "2 A1 10 10 C2 0 20",  "2 B1 10 10 C2 20 20", "2 C1 0 10 D2 0 20",   "2 C1 10 10 D2 20 20"),
                { warnings: true } // missing transformation of absolute drawing position
            );
        });
        const DEL_ANCHOR_O = { drawing: { anchor: "2 C3 10 10 D4 20 20" } };  // original
        const DEL_ANCHOR_T = { drawing: { anchor: "2 B3 0 10 B4 0 20" } };    // transformed
        it("should handle deleted drawing anchor of drawing objects", () => {
            testRunner().runBidiTest(
                deleteCols(1, "B:E"), [insertShape(1, 0, DEL_ANCHOR_O), changeDrawing(1, 2, DEL_ANCHOR_O)],
                [deleteDrawing(1, 0), deleteDrawing(1, 1), deleteCols(1, "B:E")], []
            );
        });
        it("should handle deleted drawing anchor of notes and comments", () => {
            testRunner().runBidiTest(
                deleteCols(1, "B:E"), [insertNote(1, "A1", "abc", DEL_ANCHOR_O), changeNote(1, "A1", DEL_ANCHOR_O), insertComment(1, "A1", 0, { attrs: DEL_ANCHOR_O }), changeComment(1, "A1", 0, { attrs: DEL_ANCHOR_O })],
                deleteCols(1, "B:E"), [insertNote(1, "A1", "abc", DEL_ANCHOR_T), changeNote(1, "A1", DEL_ANCHOR_T), insertComment(1, "A1", 0, { attrs: DEL_ANCHOR_T }), changeComment(1, "A1", 0, { attrs: DEL_ANCHOR_T })],
                { warnings: true }
            );
        });
        it("should log warnings for absolute anchor", () => {
            const anchorOps = drawingAnchorOps(1, "2 A1 0 0 C3 0 0");
            testRunner().expectBidiWarnings(deleteCols(1, "C"), anchorOps, anchorOps.length);
        });
        it("should not log warnings without anchor attribute", () => {
            testRunner().expectBidiWarnings(deleteCols(1, "C"), drawingNoAnchorOps(1), 0);
        });
        it("should not transform drawing anchor in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), drawingAnchorOps(2, "2 A1 10 10 D4 20 20"));
        });
    });

    describe('"deleteColumns" and "sheetSelection"', () => {
        it("should transform selected ranges", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C:D F:G"), sheetSelection(1, "A1:C3 D4:F6 G7:I9", 0, "A1", [0, 1]),
                deleteCols(1, "C:D F:G"), sheetSelection(1, "A1:B3 C4:C6 D7:E9", 0, "A1", [0, 1])
            );
        });
        it("should transform active address", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C:D F:G"), opSeries2(sheetSelection, 1, "A1:XFD4", 0, ["A1", "B2", "C3", "D4", "E5", "F6", "G7", "H8", "I9"]),
                deleteCols(1, "C:D F:G"), opSeries2(sheetSelection, 1, "A1:XFD4", 0, ["A1", "B2", "C3", "C4", "C5", "D6", "D7", "D8", "E9"])
            );
        });
        it("should transform origin address", () => {
            testRunner().runBidiTest(
                deleteCols(1, "C:D F:G"), opSeries2(sheetSelection, 1, "A1:XFD4", 0, "A1", ["A1", "B2", "C3", "D4", "E5", "F6", "G7", "H8", "I9"]),
                deleteCols(1, "C:D F:G"), opSeries2(sheetSelection, 1, "A1:XFD4", 0, "A1", ["A1", "B2", "C3", "C4", "C5", "D6", "D7", "D8", "E9"])
            );
        });
        it("should transform deleted active range", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), sheetSelection(1, "E5 D4 C3 B2 A1", 0, "E5", "E5"), null, sheetSelection(1, "D5 C4 B2 A1", 0, "D5", "D5"));
            testRunner().runBidiTest(deleteCols(1, "C"), sheetSelection(1, "E5 D4 C3 B2 A1", 1, "D4", "D4"), null, sheetSelection(1, "D5 C4 B2 A1", 1, "C4", "C4"));
            testRunner().runBidiTest(deleteCols(1, "C"), sheetSelection(1, "E5 D4 C3 B2 A1", 2, "C3", "C3"), null, sheetSelection(1, "D5 C4 B2 A1", 0, "D5"));
            testRunner().runBidiTest(deleteCols(1, "C"), sheetSelection(1, "E5 D4 C3 B2 A1", 3, "B2", "B2"), null, sheetSelection(1, "D5 C4 B2 A1", 2, "B2", "B2"));
            testRunner().runBidiTest(deleteCols(1, "C"), sheetSelection(1, "E5 D4 C3 B2 A1", 4, "A1", "A1"), null, sheetSelection(1, "D5 C4 B2 A1", 3, "A1", "A1"));
        });
        it("should keep active cell when deleting the selection", () => {
            testRunner().runBidiTest(deleteCols(1, "C:D"), sheetSelection(1, "C6:D9 C1:D4", 0, "C7"), null, sheetSelection(1, "C7", 0, "C7"));
            testRunner().runBidiTest(deleteCols(1, "C:D"), sheetSelection(1, "C6:D9 C1:D4", 1, "D2"), null, sheetSelection(1, "C2", 0, "C2"));
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(deleteCols(1, "C"), sheetSelection(2, "A1:D4", 0, "A1"));
        });
    });

    // changeColumns ----------------------------------------------------------

    describe('"changeColumns" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(changeCols(1, "C", "a1"), [
                GLOBAL_OPS, STYLESHEET_OPS, rowOps(1), cellOps(1),
                hlinkOps(1), nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    describe('"changeColumns" and "changeColumns"', () => {

        const LCL_ATTRS = { f1: { p1: 10, p2: 20 } };     // local attributes
        const EXT_ATTRS_1 = { f2: { p1: 10 } };           // independent formatting attributes (local and external remain unchanged)
        const EXT_ATTRS_2 = { f1: { p1: 10, p3: 30 } };   // reduced formatting (both operations remain active)
        const EXT_ATTRS_3 = { f1: { p1: 10 } };           // reduced formatting (external operation becomes no-op)
        const EXT_ATTRS_4 = LCL_ATTRS;                    // equal formatting (both operations become no-op)
        const LCL_ATTRS_REDUCED = { f1: { p2: 20 } };     // expected reduced local attribute set (for steps 2 and 3)
        const EXT_ATTRS_REDUCED = { f1: { p3: 30 } };     // expected reduced external attribute set (for step 2)

        it("should not transform anything in distinct intervals", () => {
            testRunner().runTest(changeCols(1, "A:C", LCL_ATTRS), changeCols(1, "D:F", EXT_ATTRS_1));
            testRunner().runTest(changeCols(1, "A:C", LCL_ATTRS), changeCols(1, "D:F", EXT_ATTRS_2));
            testRunner().runTest(changeCols(1, "A:C", LCL_ATTRS), changeCols(1, "D:F", EXT_ATTRS_3));
            testRunner().runTest(changeCols(1, "A:C", LCL_ATTRS), changeCols(1, "D:F", EXT_ATTRS_4));
            testRunner().runTest(changeCols(1, "A:C", "a1"),      changeCols(1, "D:F", "a1"));
        });
        it("should transform with partially overlapping intervals", () => {
            testRunner().runTest(changeCols(1, "A:D", LCL_ATTRS), changeCols(1, "C:F", EXT_ATTRS_1));
            testRunner().runTest(changeCols(1, "A:D", LCL_ATTRS), changeCols(1, "C:F", EXT_ATTRS_2), changeCols(1, "A:D", LCL_ATTRS), opSeries1(changeCols, [1, "E:F", EXT_ATTRS_2], [1, "C:D", EXT_ATTRS_REDUCED]));
            testRunner().runTest(changeCols(1, "A:D", LCL_ATTRS), changeCols(1, "C:F", EXT_ATTRS_3), changeCols(1, "A:D", LCL_ATTRS), changeCols(1, "E:F", EXT_ATTRS_3));
            testRunner().runTest(changeCols(1, "A:D", LCL_ATTRS), changeCols(1, "C:F", EXT_ATTRS_4), changeCols(1, "A:B", LCL_ATTRS), changeCols(1, "E:F", EXT_ATTRS_4));
            testRunner().runTest(changeCols(1, "A:D", "a1"),      changeCols(1, "C:F", "a2"),        changeCols(1, "A:D", "a1"),      changeCols(1, "E:F", "a2"));
            testRunner().runTest(changeCols(1, "A:D", "a1"),      changeCols(1, "C:F", "a1"),        changeCols(1, "A:B", "a1"),      changeCols(1, "E:F", "a1"));
        });
        it("should transform with dominant local intervals", () => {
            testRunner().runTest(changeCols(1, "A:F", LCL_ATTRS), changeCols(1, "C:D", EXT_ATTRS_1));
            testRunner().runTest(changeCols(1, "A:F", LCL_ATTRS), changeCols(1, "C:D", EXT_ATTRS_2), changeCols(1, "A:F",     LCL_ATTRS), changeCols(1, "C:D", EXT_ATTRS_REDUCED));
            testRunner().runTest(changeCols(1, "A:F", LCL_ATTRS), changeCols(1, "C:D", EXT_ATTRS_3), changeCols(1, "A:F",     LCL_ATTRS), []);
            testRunner().runTest(changeCols(1, "A:F", LCL_ATTRS), changeCols(1, "C:D", EXT_ATTRS_4), changeCols(1, "A:B E:F", LCL_ATTRS), []);
            testRunner().runTest(changeCols(1, "A:F", "a1"),      changeCols(1, "C:D", "a2"),        changeCols(1, "A:F",     "a1"),      []);
            testRunner().runTest(changeCols(1, "A:F", "a1"),      changeCols(1, "C:D", "a1"),        changeCols(1, "A:B E:F", "a1"),      []);
        });
        it("should transform with dominant external intervals", () => {
            testRunner().runTest(changeCols(1, "C:D", LCL_ATTRS), changeCols(1, "A:F", EXT_ATTRS_1));
            testRunner().runTest(changeCols(1, "C:D", LCL_ATTRS), changeCols(1, "A:F", EXT_ATTRS_2), changeCols(1, "C:D", LCL_ATTRS_REDUCED), opSeries1(changeCols, [1, "A:B E:F", EXT_ATTRS_2], [1, "C:D", EXT_ATTRS_REDUCED]));
            testRunner().runTest(changeCols(1, "C:D", LCL_ATTRS), changeCols(1, "A:F", EXT_ATTRS_3), changeCols(1, "C:D", LCL_ATTRS_REDUCED), changeCols(1, "A:B E:F", EXT_ATTRS_3));
            testRunner().runTest(changeCols(1, "C:D", LCL_ATTRS), changeCols(1, "A:F", EXT_ATTRS_4), [],                                      changeCols(1, "A:B E:F", EXT_ATTRS_4));
            testRunner().runTest(changeCols(1, "C:D", "a1"),      changeCols(1, "A:F", "a2"),        changeCols(1, "C:D", "a1"),              changeCols(1, "A:B E:F", "a2"));
            testRunner().runTest(changeCols(1, "C:D", "a1"),      changeCols(1, "A:F", "a1"),        [],                                      changeCols(1, "A:B E:F", "a1"));
        });
        it("should transform with equal intervals", () => {
            testRunner().runTest(changeCols(1, "A:F", LCL_ATTRS), changeCols(1, "A:F", EXT_ATTRS_1));
            testRunner().runTest(changeCols(1, "A:F", LCL_ATTRS), changeCols(1, "A:F", EXT_ATTRS_2), changeCols(1, "A:F", LCL_ATTRS_REDUCED), changeCols(1, "A:F", EXT_ATTRS_REDUCED));
            testRunner().runTest(changeCols(1, "A:F", LCL_ATTRS), changeCols(1, "A:F", EXT_ATTRS_3), changeCols(1, "A:F", LCL_ATTRS_REDUCED), []);
            testRunner().runTest(changeCols(1, "A:F", LCL_ATTRS), changeCols(1, "A:F", EXT_ATTRS_4), [],                                      []);
        });
        it("should not transform anything in different sheets", () => {
            testRunner().runTest(changeCols(1, "A:D", "a1"), changeCols(2, "C:F", "a1"));
        });
    });

    describe('"changeColumns" and drawing anchor', () => {
        it("should log warnings for absolute anchor", () => {
            const count = 4 * COL_ATTRS.length;
            testRunner().expectBidiWarnings(opSeries2(changeCols, 1, "C", COL_ATTRS), drawingAnchorOps(1, "2 A1 0 0 C3 0 0"), count);
        });
        it("should not log warnings without changed column size", () => {
            testRunner().expectBidiWarnings(changeCols(1, "C", ATTRS), drawingAnchorOps(1, "2 A1 0 0 C3 0 0"), 0);
        });
        it("should not log warnings without anchor attribute", () => {
            testRunner().expectBidiWarnings(opSeries2(changeCols, 1, "C", COL_ATTRS), drawingNoAnchorOps(1), 0);
        });
    });

    // insertRows -------------------------------------------------------------

    describe('"insertRows" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(insertRows(1, "3"), [
                GLOBAL_OPS, STYLESHEET_OPS, colOps(1), nameOps(1),
                changeTableCol(1, "Table1", 0, { attrs: ATTRS }), deleteTable(1, "Table1"),
                deleteDVRule(1, 0), deleteCFRule(1, "R1"),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    describe('"insertRows" and "insertRows"', () => {
        it("should transform single intervals", () => {
            testRunner().runTest(insertRows(1, "2:4"), insertRows(1, "1:3"), insertRows(1, "5:7"), insertRows(1, "1:3"));
            testRunner().runTest(insertRows(1, "2:4"), insertRows(1, "2:4"), insertRows(1, "2:4"), insertRows(1, "5:7"));
            testRunner().runTest(insertRows(1, "2:4"), insertRows(1, "3:5"), insertRows(1, "2:4"), insertRows(1, "6:8"));
        });
        it("should transform interval lists #1", () => {
            testRunner().runTest(insertRows(1, "2 4"), insertRows(1, "1 2"), insertRows(1, "3 6"), insertRows(1, "1 3"));
            testRunner().runTest(insertRows(1, "2 4"), insertRows(1, "2 3"), insertRows(1, "2 6"), insertRows(1, "3 4"));
            testRunner().runTest(insertRows(1, "2 4"), insertRows(1, "3 4"), insertRows(1, "2 5"), insertRows(1, "4 6"));
            testRunner().runTest(insertRows(1, "2 4"), insertRows(1, "4 5"), insertRows(1, "2 4"), insertRows(1, "6 7"));
            testRunner().runTest(insertRows(1, "2 4"), insertRows(1, "5 6"), insertRows(1, "2 4"), insertRows(1, "7 8"));
        });
        it("should transform interval lists #2", () => {
            testRunner().runTest(insertRows(1, "2 4"), insertRows(1, "1 3"), insertRows(1, "3 6"), insertRows(1, "1 4"));
            testRunner().runTest(insertRows(1, "2 4"), insertRows(1, "2 4"), insertRows(1, "2 5"), insertRows(1, "3 6"));
            testRunner().runTest(insertRows(1, "2 4"), insertRows(1, "3 5"), insertRows(1, "2 5"), insertRows(1, "4 7"));
            testRunner().runTest(insertRows(1, "2 4"), insertRows(1, "4 6"), insertRows(1, "2 4"), insertRows(1, "6 8"));
            testRunner().runTest(insertRows(1, "2 4"), insertRows(1, "5 7"), insertRows(1, "2 4"), insertRows(1, "7 9"));
        });
        it("should shift intervals outside sheet", () => {
            testRunner().runBidiTest(insertRows(1, "1048574"), insertRows(1, "1048575:1048576"), null, insertRows(1, "1048576"));
            testRunner().runBidiTest(insertRows(1, "1048574"), insertRows(1, "1048576"),         null, []);
        });
        it("should not transform intervals in different sheets", () => {
            testRunner().runTest(insertRows(1, "3"), insertRows(2, "3"));
        });
    });

    describe('"insertRows" and "deleteRows"', () => {
        it("should transform single intervals", () => {
            testRunner().runBidiTest(insertRows(1, "1:2"), deleteRows(1, "2:3"), insertRows(1, "1:2"), deleteRows(1, "4:5"));
            testRunner().runBidiTest(insertRows(1, "2:3"), deleteRows(1, "2:3"), insertRows(1, "2:3"), deleteRows(1, "4:5"));
            testRunner().runBidiTest(insertRows(1, "3:4"), deleteRows(1, "2:3"), insertRows(1, "2:3"), deleteRows(1, "2 5"));
            testRunner().runBidiTest(insertRows(1, "4:5"), deleteRows(1, "2:3"), insertRows(1, "2:3"), deleteRows(1, "2:3"));
            testRunner().runBidiTest(insertRows(1, "5:6"), deleteRows(1, "2:3"), insertRows(1, "3:4"), deleteRows(1, "2:3"));
        });
        it("should transform interval lists", () => {
            testRunner().runBidiTest(insertRows(1, "4 7"), deleteRows(1, "1 3"),  insertRows(1, "2 5"), deleteRows(1, "1 3"));
            testRunner().runBidiTest(insertRows(1, "4 7"), deleteRows(1, "2 4"),  insertRows(1, "3 5"), deleteRows(1, "2 5"));
            testRunner().runBidiTest(insertRows(1, "4 7"), deleteRows(1, "3 5"),  insertRows(1, "3 5"), deleteRows(1, "3 6"));
            testRunner().runBidiTest(insertRows(1, "4 7"), deleteRows(1, "4 6"),  insertRows(1, "4 5"), deleteRows(1, "5 7"));
            testRunner().runBidiTest(insertRows(1, "4 7"), deleteRows(1, "5 7"),  insertRows(1, "4 6"), deleteRows(1, "6 9"));
            testRunner().runBidiTest(insertRows(1, "4 7"), deleteRows(1, "6 8"),  insertRows(1, "4 6"), deleteRows(1, "7 10"));
            testRunner().runBidiTest(insertRows(1, "4 7"), deleteRows(1, "7 9"),  insertRows(1, "4 7"), deleteRows(1, "9 11"));
            testRunner().runBidiTest(insertRows(1, "4 7"), deleteRows(1, "8 10"), insertRows(1, "4 7"), deleteRows(1, "10 12"));
        });
        it("should shift intervals outside sheet", () => {
            testRunner().runBidiTest(insertRows(1, "1048574"), deleteRows(1, "1048575:1048576"), null, deleteRows(1, "1048576"));
            testRunner().runBidiTest(insertRows(1, "1048574"), deleteRows(1, "1048576"),     null, []);
        });
        it("should not transform intervals in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), deleteRows(2, "3"));
        });
    });

    describe('"insertRows" and "changeRows"', () => {
        it("should transform intervals", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), opSeries2(changeRows, 1, ["1 2 3 4", "1:2", "1:3",   "1:4",     "2:4",   "3:4", "1 1048574:1048575 1048575:1048576 1048576 1", "1048576"]),
                insertRows(1, "3"), opSeries2(changeRows, 1, ["1 2 4 5", "1:2", "1:2 4", "1:2 4:5", "2 4:5", "4:5", "1 1048575:1048576 1048576 1"])
            );
            testRunner().runBidiTest(
                insertRows(1, "3:4 4:5"), opSeries2(changeRows, 1, ["1 2 3 4", "1:2", "1:3",   "1:4",     "2:5",     "3:5",   "4:5"]),
                insertRows(1, "3:4 4:5"), opSeries2(changeRows, 1, ["1 2 5 8", "1:2", "1:2 5", "1:2 5 8", "2 5 8:9", "5 8:9", "8:9"])
            );
        });
        it("should not transform intervals in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), changeRows(2, "1:4", "a1"));
        });
    });

    describe('"insertRows" and "changeCells"', () => {
        it("should transform ranges", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), changeCellsOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4",       "A1 A1048574:B1048575 A1048575:B1048576 A1048576:B1048576 B1", "A1048576"),
                insertRows(1, "3"), changeCellsOps(1, "A1 B2 C4 D5", "A1:B2 B2:C2 B4:C4 C4:D5", "A1 A1048575:B1048576 A1048576:B1048576 B1")
            );
            testRunner().runBidiTest(
                insertRows(1, "3:4 4:5"), changeCellsOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4 D4:E5"),
                insertRows(1, "3:4 4:5"), changeCellsOps(1, "A1 B2 C5 D8", "A1:B2 B2:C2 B5:C5 C5:D5 C8:D8 D8:E9")
            );
        });
        it("should transform matrix formula range", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), changeCells(1, { A1: { mr: "A1:B2" }, C3: { mr: "C3:D4" } }),
                null,               changeCells(1, { A1: { mr: "A1:B2" }, C4: { mr: "C4:D5" } })
            );
        });
        it("should fail to split a matrix formula range", () => {
            testRunner().expectBidiError(insertRows(1, "3"), changeCells(1, { A1: { mr: "A1:D4" } }));
        });
        it("should fail to change a shared formula", () => {
            testRunner().runBidiTest(insertRows(1, "3"), changeCells(1, { A1: { si: null } }));
            testRunner().runBidiTest(insertRows(1, "3"), changeCells(1, { A1: { sr: null } }));
            testRunner().expectBidiError(insertRows(1, "3"), changeCells(1, { A1: { si: 1 } }));
            testRunner().expectBidiError(insertRows(1, "3"), changeCells(1, { A1: { sr: "A1:D4" } }));
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), changeCellsOps(2, "A1:D4"));
        });
    });

    describe('"insertRows" and "mergeCells"', () => {
        it("should resize ranges (MERGE, VERTICAL, UNMERGE mode)", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), opSeries2(mergeCells, 1, "B1:C2 D2:E3 F3:G4", [MM_MERGE, MM_VERTICAL, MM_UNMERGE]),
                insertRows(1, "3"), opSeries2(mergeCells, 1, "B1:C2 D2:E4 F4:G5", [MM_MERGE, MM_VERTICAL, MM_UNMERGE])
            );
            testRunner().runBidiTest(
                insertRows(1, "1048574"), opSeries2(mergeCells, 1, "A1048573:B1048575 C1048573:D1048576", [MM_MERGE, MM_VERTICAL, MM_UNMERGE]),
                insertRows(1, "1048574"), opSeries2(mergeCells, 1, "A1048573:B1048576 C1048573:D1048576", [MM_MERGE, MM_VERTICAL, MM_UNMERGE])
            );
        });
        it("should split ranges (HORIZONTAL mode)", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), mergeCells(1, "B1:C2 D2:E3 F3:G4",       MM_HORIZONTAL),
                insertRows(1, "3"), mergeCells(1, "B1:C2 D2:E2 D4:E4 F4:G5", MM_HORIZONTAL)
            );
            testRunner().runBidiTest(
                insertRows(1, "1048574"), mergeCells(1, "A1048573:B1048575 C1048573:D1048576",                                     MM_HORIZONTAL),
                insertRows(1, "1048574"), mergeCells(1, "A1048573:B1048573 A1048575:B1048576 C1048573:D1048573 C1048575:D1048576", MM_HORIZONTAL)
            );
        });
        it("should remove no-op operations", () => {
            testRunner().runBidiTest(insertRows(1, "3:4"), opSeries2(mergeCells, 1, "A1048575:B1048576", MM_ALL), null, []);
        });
        it("should not modify entire column ranges", () => {
            testRunner().runBidiTest(insertRows(1, "3:4"), opSeries2(mergeCells, 1, "A1:B1048576", MM_ALL));
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), mergeCells(2, "A1:D4"));
        });
    });

    describe('"insertRows" and hyperlinks (RESIZE mode)', () => {
        it("should transform ranges", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), hlinkOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4", "A1 A1048574:B1048575 A1048575:B1048576 A1048576:B1048576 B1", "A1048576"),
                insertRows(1, "3"), hlinkOps(1, "A1 B2 C4 D5", "A1:B2 B2:C4 C4:D5", "A1 A1048575:B1048576 A1048576:B1048576 B1")
            );
            testRunner().runBidiTest(
                insertRows(1, "3:4 4:5"), hlinkOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4 D4:E5"),
                insertRows(1, "3:4 4:5"), hlinkOps(1, "A1 B2 C5 D8", "A1:B2 B2:C5 C5:D8 D8:E9")
            );
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), hlinkOps(2, "A1:D4"));
        });
    });

    describe('"insertRows" and "insertTable"', () => {
        it("should shift and expand table range", () => {
            testRunner().runBidiTest(
                insertRows(1, "5"), opSeries1(insertTable, [1, "Table3", "E1:H4"], [1, "Table4", "E3:H6"], [1, "Table5", "E5:H8"]),
                insertRows(1, "5"), opSeries1(insertTable, [1, "Table3", "E1:H4"], [1, "Table4", "E3:H7"], [1, "Table5", "E6:H9"])
            );
        });
        it("should delete table range shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertRows(1, "1:9"), insertTable(1, "Table3", "E1048573:H1048576"),
                [deleteTable(1, "Table3"), insertRows(1, "1:9")], []
            );
            testRunner().runBidiTest(
                insertRows(1, "4:5"), insertTable(1, "Table3", "E1048573:H1048576"),
                [deleteTable(1, "Table3"), insertRows(1, "4:5")], []
            );
        });
        it("should not transform tables in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), insertTable(2, "Table3", "E1:H4"));
        });
    });

    describe('"insertRows" and "changeTable"', () => {
        it("should shift and expand table range", () => {
            testRunner().runBidiTest(
                insertRows(1, "5"), opSeries2(changeTable, 1, "Table1", [{ attrs: ATTRS }, "A1:D4", "A3:D6", "A5:D8"]),
                insertRows(1, "5"), opSeries2(changeTable, 1, "Table1", [{ attrs: ATTRS }, "A1:D4", "A3:D7", "A6:D9"])
            );
        });
        it("should delete table range shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertRows(1, "1:9"), changeTable(1, "Table1", "A1048573:D1048576"),
                [deleteTable(1, "Table1"), insertRows(1, "1:9")], []
            );
            testRunner().runBidiTest(
                insertRows(1, "4:5"), changeTable(1, "Table1", "A1048573:D1048576"),
                [deleteTable(1, "Table1"), insertRows(1, "4:5")], []
            );
        });
        it("should not transform tables in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), changeTable(2, "Table2", "A1:D4"));
        });
    });

    describe('"insertRows" and tables', () => {
        it("should handle insert/change sequence", () => {
            testRunner().runBidiTest(
                insertRows(1, "1:9"), [insertTable(1, "Table3", "A1048573:D1048576"), changeTable(1, "Table3", { attrs: ATTRS }), changeTableCol(1, "Table3", 0, { attrs: ATTRS })],
                [deleteTable(1, "Table3"), insertRows(1, "1:9")], []
            );
        });
        it("should handle insert/change/delete sequence", () => {
            testRunner().runBidiTest(
                insertRows(1, "1:9"), [insertTable(1, "Table3", "A1048573:D1048576"), changeTable(1, "Table3", { attrs: ATTRS }), changeTableCol(1, "Table3", 0, { attrs: ATTRS }), deleteTable(1, "Table3")],
                null, []
            );
        });
    });

    describe('"insertRows" and "insertDVRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                insertRows(1, "5"), opSeries2(insertDVRule, 1, 0, ["A1:D4", "A3:D6", "A5:D8"]),
                insertRows(1, "5"), opSeries2(insertDVRule, 1, 0, ["A1:D5", "A3:D7", "A6:D9"])
            );
        });
        it("should delete rule shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertRows(1, "1:9"), insertDVRule(1, 0, "A1048573:D1048576"),
                [deleteDVRule(1, 0), insertRows(1, "1:9")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), insertDVRule(2, 0, "A1:D4"));
        });
    });

    describe('"insertRows" and "changeDVRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                insertRows(1, "5"), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, "A1:D4", "A3:D6", "A5:D8"]),
                insertRows(1, "5"), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, "A1:D5", "A3:D7", "A6:D9"])
            );
        });
        it("should delete rule shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertRows(1, "1:9"), changeDVRule(1, 0, "A1048573:D1048576"),
                [deleteDVRule(1, 0), insertRows(1, "1:9")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), changeDVRule(2, 0, "A1:D4"));
        });
    });

    describe('"insertRows" and "insertCFRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                insertRows(1, "5"), opSeries2(insertCFRule, 1, "R1", ["A1:D4", "A3:D6", "A5:D8"]),
                insertRows(1, "5"), opSeries2(insertCFRule, 1, "R1", ["A1:D5", "A3:D7", "A6:D9"])
            );
        });
        it("should delete rule shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertRows(1, "1:9"), insertCFRule(1, "R1", "A1048573:D1048576"),
                [deleteCFRule(1, "R1"), insertRows(1, "1:9")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), insertCFRule(2, "R1", "A1:D4"));
        });
    });

    describe('"insertRows" and "changeCFRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                insertRows(1, "5"), opSeries2(changeCFRule, 1, "R1", [{ attr: ATTRS }, "A1:D4", "A3:D6", "A5:D8"]),
                insertRows(1, "5"), opSeries2(changeCFRule, 1, "R1", [{ attr: ATTRS }, "A1:D5", "A3:D7", "A6:D9"])
            );
        });
        it("should delete rule shifted outside sheet", () => {
            testRunner().runBidiTest(
                insertRows(1, "1:9"), changeCFRule(1, "R1", "A1048573:D1048576"),
                [deleteCFRule(1, "R1"), insertRows(1, "1:9")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), changeCFRule(2, "R1", "A1:D4"));
        });
    });

    describe('"insertRows" and cell anchor', () => {
        it("should transform cell anchor", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), cellAnchorOps(1, "A1", "B2", "C3", "D4", "E1048575"),
                insertRows(1, "3"), cellAnchorOps(1, "A1", "B2", "C4", "D5", "E1048576")
            );
            testRunner().runBidiTest(
                insertRows(1, "3:4 4:5"), cellAnchorOps(1, "A1", "B2", "C3", "D4", "E5", "F1048572"),
                insertRows(1, "3:4 4:5"), cellAnchorOps(1, "A1", "B2", "C5", "D8", "E9", "F1048576")
            );
        });
        it("should handle deleted cell note", () => {
            testRunner().runBidiTest(insertRows(1, "3"), insertNote(1, "A1048576", "abc"), [deleteNote(1, "A1048576"), insertRows(1, "3")], []);
            testRunner().runBidiTest(insertRows(1, "3"), deleteNote(1, "A1048576"),        null,                                            []);
            testRunner().runBidiTest(insertRows(1, "3"), changeNote(1, "A1048576", "abc"), [deleteNote(1, "A1048576"), insertRows(1, "3")], []);
        });
        it("should handle deleted cell comment", () => {
            testRunner().runBidiTest(insertRows(1, "3"), insertComment(1, "A1048576", 0, { text: "abc" }), [deleteComment(1, "A1048576", 0), insertRows(1, "3")], []);
            testRunner().runBidiTest(insertRows(1, "3"), insertComment(1, "A1048576", 1, { text: "abc" }), null,                                                  []);
            testRunner().runBidiTest(insertRows(1, "3"), deleteComment(1, "A1048576", 0),                  null,                                                  []);
            testRunner().runBidiTest(insertRows(1, "3"), deleteComment(1, "A1048576", 1),                  null,                                                  []);
            testRunner().runBidiTest(insertRows(1, "3"), changeComment(1, "A1048576", 0, { text: "abc" }), [deleteComment(1, "A1048576", 0), insertRows(1, "3")], []);
            testRunner().runBidiTest(insertRows(1, "3"), changeComment(1, "A1048576", 1, { text: "abc" }), null,                                                  []);
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), cellAnchorOps(2, "D4"));
        });
    });

    describe('"insertRows" and "moveNotes"', () => {
        it("should transform cell anchors", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), moveNotes(1, "A1 B2 C3 D4", "A4 B3 C2 D1"),
                insertRows(1, "3"), moveNotes(1, "A1 B2 C4 D5", "A5 B4 C2 D1")
            );
        });
        it("should handle deleted source anchor", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), moveNotes(1, "A1 B1048576 C1048576 D1", "A5 B5 A1048576 D5"),
                [deleteNote(1, "B5"), deleteNote(1, "A1048576"), insertRows(1, "3")], moveNotes(1, "A1 D1", "A6 D6")
            );
        });
        it("should handle deleted target anchor", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), moveNotes(1, "A5 B5 C5", "A2 B1048576 C2"),
                [deleteNote(1, "B1048576"), insertRows(1, "3")], [deleteNote(1, "B6"), moveNotes(1, "A6 C6", "A2 C2")]
            );
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), moveNotes(2, "D4", "E5"));
        });
    });

    describe('"insertRows" and "moveComments"', () => {
        it("should transform cell anchors", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), moveComments(1, "A1 B2 C3 D4", "A4 B3 C2 D1"),
                insertRows(1, "3"), moveComments(1, "A1 B2 C4 D5", "A5 B4 C2 D1")
            );
        });
        it("should handle deleted source anchor", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), moveComments(1, "A1 B1048576 C1048576 D1", "A5 B5 A1048576 D5"),
                [deleteComment(1, "B5", 0), deleteComment(1, "A1048576", 0), insertRows(1, "3")], moveComments(1, "A1 D1", "A6 D6")
            );
        });
        it("should handle deleted target anchor", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), moveComments(1, "A5 B5 C5", "A2 B1048576 C2"),
                [deleteComment(1, "B1048576", 0), insertRows(1, "3")], [deleteComment(1, "B6", 0), moveComments(1, "A6 C6", "A2 C2")]
            );
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), moveComments(2, "D4", "E5"));
        });
    });

    describe('"insertRows" and drawing anchor', () => {
        it("should transform drawing anchor", () => {
            testRunner().runBidiTest(
                insertRows(1, "3"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 A2 10 10 B3 20 0", "2 A2 10 10 B3 20 20", "2 A3 10 10 B4 20 20", "2 A7 10 10 B1048576 20 20"),
                insertRows(1, "3"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 A2 10 10 B3 20 0", "2 A2 10 10 B4 20 20", "2 A4 10 10 B5 20 20", "2 A7 10 10 B1048576 20 20"),
                { warnings: true } // missing transformation of absolute drawing position
            );
            testRunner().runBidiTest(
                insertRows(1, "3:4 4:5"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 A2 10 10 B3 20 20", "2 A3 10 10 B4 20 20", "2 A4 10 10 B5 20 20", "2 A7 10 10 B1048575 20 20"),
                insertRows(1, "3:4 4:5"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 A2 10 10 B5 20 20", "2 A5 10 10 B8 20 20", "2 A8 10 10 B9 20 20", "2 A8 10 10 B1048576 20 20"),
                { warnings: true } // missing transformation of absolute drawing position
            );
        });
        it("should log warnings for absolute anchor", () => {
            const anchorOps = drawingAnchorOps(1, "2 A1 0 0 C3 0 0");
            testRunner().expectBidiWarnings(insertRows(1, "3"), anchorOps, anchorOps.length);
        });
        it("should not log warnings without anchor attribute", () => {
            testRunner().expectBidiWarnings(insertRows(1, "3"), drawingNoAnchorOps(1), 0);
        });
        it("should not transform drawing anchor in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), drawingAnchorOps(2, "2 A1 10 10 D4 20 20"));
        });
    });

    describe('"insertRows" and "sheetSelection"', () => {
        it("should transform selected ranges", () => {
            testRunner().runBidiTest(
                insertRows(1, "3:4 4:5"), sheetSelection(1, "A1 B2 C3 D4 A1:B2 B2:C3 C3:D4 D4:E5 A1048569:D1048572 A1048570:D1048573 A1048571:D1048574 A1048572:D1048575", 0, "A1", [0, 1]),
                insertRows(1, "3:4 4:5"), sheetSelection(1, "A1 B2 C5 D8 A1:B2 B2:C5 C5:D8 D8:E9 A1048573:D1048576 A1048574:D1048576 A1048575:D1048576 A1048576:D1048576", 0, "A1", [0, 1])
            );
        });
        it("should transform active address", () => {
            testRunner().runBidiTest(
                insertRows(1, "3:4 4:5"), opSeries2(sheetSelection, 1, "A1:D1048576", 0, ["A1", "B2", "C3", "D4", "D1048572", "E1048573"]),
                insertRows(1, "3:4 4:5"), opSeries2(sheetSelection, 1, "A1:D1048576", 0, ["A1", "B2", "C5", "D8", "D1048576", "E1048576"])
            );
        });
        it("should transform origin address", () => {
            testRunner().runBidiTest(
                insertRows(1, "3:4 4:5"), opSeries2(sheetSelection, 1, "A1:D1048576", 0, "A1", ["A1", "B2", "C3", "D4", "A1048572", "B1048573"]),
                insertRows(1, "3:4 4:5"), opSeries2(sheetSelection, 1, "A1:D1048576", 0, "A1", ["A1", "B2", "C5", "D8", "A1048576", "B1048576"])
            );
        });
        it("should transform deleted active range", () => {
            testRunner().runBidiTest(insertRows(1, "3"), sheetSelection(1, "D4 C3 A1048576:D1048576 B2 A1", 0, "D4",       "D4"), null, sheetSelection(1, "D5 C4 B2 A1", 0, "D5", "D5"));
            testRunner().runBidiTest(insertRows(1, "3"), sheetSelection(1, "D4 C3 A1048576:D1048576 B2 A1", 1, "C3",       "C3"), null, sheetSelection(1, "D5 C4 B2 A1", 1, "C4", "C4"));
            testRunner().runBidiTest(insertRows(1, "3"), sheetSelection(1, "D4 C3 A1048576:D1048576 B2 A1", 2, "A1048576", "A1"), null, sheetSelection(1, "D5 C4 B2 A1", 0, "D5"));
            testRunner().runBidiTest(insertRows(1, "3"), sheetSelection(1, "D4 C3 A1048576:D1048576 B2 A1", 3, "B2",       "B2"), null, sheetSelection(1, "D5 C4 B2 A1", 2, "B2", "B2"));
            testRunner().runBidiTest(insertRows(1, "3"), sheetSelection(1, "D4 C3 A1048576:D1048576 B2 A1", 4, "A1",       "A1"), null, sheetSelection(1, "D5 C4 B2 A1", 3, "A1", "A1"));
        });
        it("should keep active cell when deleting the selection", () => {
            testRunner().runBidiTest(insertRows(1, "3:4 4:5"), sheetSelection(1, "F1048575:I1048576 A1048573:D1048576", 0, "G1048575"), null, sheetSelection(1, "G1048576", 0, "G1048576"));
            testRunner().runBidiTest(insertRows(1, "3:4 4:5"), sheetSelection(1, "F1048575:I1048576 A1048573:D1048576", 1, "B1048574"), null, sheetSelection(1, "B1048576", 0, "B1048576"));
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(insertRows(1, "3"), sheetSelection(2, "A1:D4", 0, "A1"));
        });
    });

    // deleteRows -------------------------------------------------------------

    describe('"deleteRows" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), [
                GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS, colOps(1), nameOps(1),
                changeTableCol(1, "Table1", 0, { attrs: ATTRS }), deleteTable(1, "Table1"),
                deleteDVRule(1, 0), deleteCFRule(1, "R1"),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    describe('"deleteRows" and "deleteRows"', () => {
        it("should transform single intervals #1", () => {
            testRunner().runTest(deleteRows(1, "3:4"), deleteRows(1, "1"), deleteRows(1, "2:3"), deleteRows(1, "1"));
            testRunner().runTest(deleteRows(1, "3:4"), deleteRows(1, "2"), deleteRows(1, "2:3"), deleteRows(1, "2"));
            testRunner().runTest(deleteRows(1, "3:4"), deleteRows(1, "3"), deleteRows(1, "3"),   []);
            testRunner().runTest(deleteRows(1, "3:4"), deleteRows(1, "4"), deleteRows(1, "3"),   []);
            testRunner().runTest(deleteRows(1, "3:4"), deleteRows(1, "5"), deleteRows(1, "3:4"), deleteRows(1, "3"));
            testRunner().runTest(deleteRows(1, "3:4"), deleteRows(1, "6"), deleteRows(1, "3:4"), deleteRows(1, "4"));
        });
        it("should transform single intervals #2", () => {
            testRunner().runTest(deleteRows(1, "4"), deleteRows(1, "1:2"), deleteRows(1, "2"), deleteRows(1, "1:2"));
            testRunner().runTest(deleteRows(1, "4"), deleteRows(1, "2:3"), deleteRows(1, "2"), deleteRows(1, "2:3"));
            testRunner().runTest(deleteRows(1, "4"), deleteRows(1, "3:4"), [],                 deleteRows(1, "3"));
            testRunner().runTest(deleteRows(1, "4"), deleteRows(1, "4:5"), [],                 deleteRows(1, "4"));
            testRunner().runTest(deleteRows(1, "4"), deleteRows(1, "5:6"), deleteRows(1, "4"), deleteRows(1, "4:5"));
            testRunner().runTest(deleteRows(1, "4"), deleteRows(1, "6:7"), deleteRows(1, "4"), deleteRows(1, "5:6"));
        });
        it("should transform single intervals #3", () => {
            testRunner().runTest(deleteRows(1, "3:4"), deleteRows(1, "1:2"), deleteRows(1, "1:2"), deleteRows(1, "1:2"));
            testRunner().runTest(deleteRows(1, "3:4"), deleteRows(1, "2:3"), deleteRows(1, "2"),   deleteRows(1, "2"));
            testRunner().runTest(deleteRows(1, "3:4"), deleteRows(1, "3:4"), [],                   []);
            testRunner().runTest(deleteRows(1, "3:4"), deleteRows(1, "4:5"), deleteRows(1, "3"),   deleteRows(1, "3"));
            testRunner().runTest(deleteRows(1, "3:4"), deleteRows(1, "5:6"), deleteRows(1, "3:4"), deleteRows(1, "3:4"));
        });
        it("should transform interval lists", () => {
            testRunner().runTest(deleteRows(1, "4 7"), deleteRows(1, "1 3"),  deleteRows(1, "2 5"), deleteRows(1, "1 3"));
            testRunner().runTest(deleteRows(1, "4 7"), deleteRows(1, "2 4"),  deleteRows(1, "5"),   deleteRows(1, "2"));
            testRunner().runTest(deleteRows(1, "4 7"), deleteRows(1, "3 5"),  deleteRows(1, "3 5"), deleteRows(1, "3:4"));
            testRunner().runTest(deleteRows(1, "4 7"), deleteRows(1, "4 6"),  deleteRows(1, "5"),   deleteRows(1, "5"));
            testRunner().runTest(deleteRows(1, "4 7"), deleteRows(1, "5 7"),  deleteRows(1, "4"),   deleteRows(1, "4"));
            testRunner().runTest(deleteRows(1, "4 7"), deleteRows(1, "6 8"),  deleteRows(1, "4 6"), deleteRows(1, "5:6"));
            testRunner().runTest(deleteRows(1, "4 7"), deleteRows(1, "7 9"),  deleteRows(1, "4"),   deleteRows(1, "7"));
            testRunner().runTest(deleteRows(1, "4 7"), deleteRows(1, "8 10"), deleteRows(1, "4 7"), deleteRows(1, "6 8"));
        });
        it("should not transform intervals in different sheets", () => {
            testRunner().runTest(deleteRows(1, "3"), deleteRows(2, "4"));
        });
    });

    describe('"deleteRows" and "changeRows"', () => {
        it("should transform intervals", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), opSeries2(changeRows, 1, ["1 2 3 4 5", "1:2 2:3 3:4 4:5", "3"]),
                deleteRows(1, "3"), opSeries2(changeRows, 1, ["1 2 3 4",   "1:2 2 3 3:4"])
            );
            testRunner().runBidiTest(
                deleteRows(1, "3:4 6:7"), changeRows(1, "1:3 4:6 7:9"),
                deleteRows(1, "3:4 6:7"), changeRows(1, "1:2 3 4:5")
            );
        });
        it("should not transform intervals in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), changeRows(2, "1:4", "a1"));
        });
    });

    describe('"deleteRows" and "changeCells"', () => {
        it("should transform ranges", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), changeCellsOps(1, "A1 B2 C3 D4 E5", "A1:B2 B2:C3 C3:D4 D4:E5", "C3"),
                deleteRows(1, "3"), changeCellsOps(1, "A1 B2 D3 E4",    "A1:B2 B2:C2 C3:D3 D3:E4")
            );
            testRunner().runBidiTest(
                deleteRows(1, "3:4 6:7"), changeCellsOps(1, "A1:C3 D4:F6 G7:I9"),
                deleteRows(1, "3:4 6:7"), changeCellsOps(1, "A1:C2 D3:F3 G4:I5")
            );
        });
        it("should transform matrix formula range", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3:4"), changeCells(1, { A1: { mr: "A1:B2" }, C3: { mr: "C3:D4" }, E5: { mr: "E5:F6" } }),
                null,                 changeCells(1, { A1: { mr: "A1:B2" }, E3: { mr: "E3:F4" } })
            );
        });
        it("should fail to shrink a matrix formula range", () => {
            testRunner().expectBidiError(deleteRows(1, "3"), changeCells(1, { A1: { mr: "A1:D4" } }));
        });
        it("should fail to change a shared formula", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), changeCells(1, { A1: { si: null } }));
            testRunner().runBidiTest(deleteRows(1, "3"), changeCells(1, { A1: { sr: null } }));
            testRunner().expectBidiError(deleteRows(1, "3"), changeCells(1, { A1: { si: 1 } }));
            testRunner().expectBidiError(deleteRows(1, "3"), changeCells(1, { A1: { sr: "A1:D4" } }));
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), changeCellsOps(2, "A1:D4"));
        });
    });

    describe('"deleteRows" and "mergeCells"', () => {
        it("should resize ranges", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), opSeries2(mergeCells, 1, "B1:C2 D2:E4 F4:G5", MM_ALL),
                deleteRows(1, "3"), opSeries2(mergeCells, 1, "B1:C2 D2:E3 F3:G4", MM_ALL)
            );
        });
        it("should delete single ranges", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), mergeCells(1, "A1:B3 C1:C3 D2:E3 F2:F3", MM_MERGE),
                deleteRows(1, "3"), mergeCells(1, "A1:B2 C1:C2 D2:E2",       MM_MERGE) // deletes single cell
            );
            testRunner().runBidiTest(
                deleteRows(1, "3"), mergeCells(1, "A1:B3 C1:C3 D2:E3 F2:F3", MM_VERTICAL),
                deleteRows(1, "3"), mergeCells(1, "A1:B2 C1:C2",             MM_VERTICAL) // deletes row vector
            );
            testRunner().runBidiTest(
                deleteRows(1, "3"), mergeCells(1, "A1:B3 C1:C3 D2:E3 F2:F3", MM_HORIZONTAL),
                deleteRows(1, "3"), mergeCells(1, "A1:B2 D2:E2",             MM_HORIZONTAL) // deletes column vector
            );
            testRunner().runBidiTest(
                deleteRows(1, "3"), mergeCells(1, "A1:B3 C1:C3 D2:E3 F2:F3", MM_UNMERGE),
                deleteRows(1, "3"), mergeCells(1, "A1:B2 C1:C2 D2:E2 F2",    MM_UNMERGE) // keeps all
            );
        });
        it("should remove no-op operations", () => {
            testRunner().runBidiTest(deleteRows(1, "3:4"), opSeries2(mergeCells, 1, "C3:D4", MM_ALL), null, []);
        });
        it("should not modify entire column ranges", () => {
            testRunner().runBidiTest(deleteRows(1, "3:4"), opSeries2(mergeCells, 1, "A1:B1048576", MM_ALL));
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "C"), mergeCells(2, "A1:D4"));
        });
    });

    describe('"deleteRows" and hyperlinks (RESIZE mode)', () => {
        it("should transform ranges", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), hlinkOps(1, "A1 B2 C3 D4 E5", "A1:B2 B2:C3 C3:D4 D4:E5", "C3"),
                deleteRows(1, "3"), hlinkOps(1, "A1 B2 D3 E4",    "A1:B2 B2:C2 C3:D3 D3:E4")
            );
            testRunner().runBidiTest(
                deleteRows(1, "3:4 6:7"), hlinkOps(1, "A1:C3 D4:F6 G7:I9"),
                deleteRows(1, "3:4 6:7"), hlinkOps(1, "A1:C2 D3:F3 G4:I5")
            );
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), hlinkOps(2, "A1:D4"));
        });
    });

    describe('"deleteRows" and "insertTable"', () => {
        it("should shift and shrink table range", () => {
            testRunner().runBidiTest(
                deleteRows(1, "5"), opSeries1(insertTable, [1, "Table3", "E1:H4"], [1, "Table4", "E3:H6"], [1, "Table5", "E6:H9"]),
                deleteRows(1, "5"), opSeries1(insertTable, [1, "Table3", "E1:H4"], [1, "Table4", "E3:H5"], [1, "Table5", "E5:H8"])
            );
        });
        it("should delete table range explicitly", () => {
            testRunner().runBidiTest(
                deleteRows(1, "2:7"), insertTable(1, "Table3", "E3:H6"),
                [deleteTable(1, "Table3"), deleteRows(1, "2:7")], []
            );
        });
        it("should fail to delete header or footer row", () => {
            testRunner().expectBidiError(deleteRows(1, "4:6"), insertTable(1, "Table3", "E1:H4"));
            testRunner().expectBidiError(deleteRows(1, "4:6"), insertTable(1, "Table3", "E2:H5"));
            testRunner().expectBidiError(deleteRows(1, "4:6"), insertTable(1, "Table3", "E5:H8"));
            testRunner().expectBidiError(deleteRows(1, "4:6"), insertTable(1, "Table3", "E6:H9"));
        });
        it("should not transform tables in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), insertTable(2, "Table3", "E1:H4"));
        });
    });

    describe('"deleteRows" and "changeTable"', () => {
        it("should shift and shrink table range", () => {
            testRunner().runBidiTest(
                deleteRows(1, "5"), opSeries2(changeTable, 1, "Table1", [{ attrs: ATTRS }, "A1:D4", "A3:D6", "A6:D9"]),
                deleteRows(1, "5"), opSeries2(changeTable, 1, "Table1", [{ attrs: ATTRS }, "A1:D4", "A3:D5", "A5:D8"])
            );
        });
        it("should delete table range explicitly", () => {
            testRunner().runBidiTest(
                deleteRows(1, "2:7"), changeTable(1, "Table1", "A3:D6"),
                [deleteTable(1, "Table1"), deleteRows(1, "2:7")], []
            );
        });
        it("should fail to delete header or footer row", () => {
            testRunner().expectBidiError(deleteRows(1, "4:6"), changeTable(1, "Table1", "A1:D4"));
            testRunner().expectBidiError(deleteRows(1, "4:6"), changeTable(1, "Table1", "A2:D5"));
            testRunner().expectBidiError(deleteRows(1, "4:6"), changeTable(1, "Table1", "A5:D8"));
            testRunner().expectBidiError(deleteRows(1, "4:6"), changeTable(1, "Table1", "A6:D9"));
        });
        it("should not transform tables in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), changeTable(2, "Table2", "A1:D4"));
        });
    });

    describe('"deleteRows" and tables', () => {
        it("should handle insert/change sequence", () => {
            testRunner().runBidiTest(
                deleteRows(1, "2:7"), [insertTable(1, "Table3", "A3:D6"), changeTable(1, "Table3", { attrs: ATTRS }), changeTableCol(1, "Table3", 0, { attrs: ATTRS })],
                [deleteTable(1, "Table3"), deleteRows(1, "2:7")], []
            );
        });
        it("should handle insert/change/delete sequence", () => {
            testRunner().runBidiTest(
                deleteRows(1, "2:7"), [insertTable(1, "Table3", "A3:D6"), changeTable(1, "Table3", { attrs: ATTRS }), changeTableCol(1, "Table3", 0, { attrs: ATTRS }), deleteTable(1, "Table3")],
                null, []
            );
        });
    });

    describe('"deleteRows" and "insertDVRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                deleteRows(1, "5"), opSeries2(insertDVRule, 1, 0, ["A1:D4", "A3:D6", "A6:D9"]),
                deleteRows(1, "5"), opSeries2(insertDVRule, 1, 0, ["A1:D4", "A3:D5", "A5:D8"])
            );
        });
        it("should delete rule explicitly", () => {
            testRunner().runBidiTest(
                deleteRows(1, "1:9"), insertDVRule(1, 0, "A3:D6"),
                [deleteDVRule(1, 0), deleteRows(1, "1:9")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), insertDVRule(2, 0, "A1:D4"));
        });
    });

    describe('"deleteRows" and "changeDVRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                deleteRows(1, "5"), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, "A1:D4", "A3:D6", "A6:D9"]),
                deleteRows(1, "5"), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, "A1:D4", "A3:D5", "A5:D8"])
            );
        });
        it("should delete rule explicitly", () => {
            testRunner().runBidiTest(
                deleteRows(1, "1:9"), changeDVRule(1, 0, "A3:D6"),
                [deleteDVRule(1, 0), deleteRows(1, "1:9")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), changeDVRule(2, 0, "A1:D4"));
        });
    });

    describe('"deleteRows" and "insertCFRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                deleteRows(1, "5"), opSeries2(insertCFRule, 1, "R1", ["A1:D4", "A3:D6", "A6:D9"]),
                deleteRows(1, "5"), opSeries2(insertCFRule, 1, "R1", ["A1:D4", "A3:D5", "A5:D8"])
            );
        });
        it("should delete rule explicitly", () => {
            testRunner().runBidiTest(
                deleteRows(1, "1:9"), insertCFRule(1, "R1", "A3:D6"),
                [deleteCFRule(1, "R1"), deleteRows(1, "1:9")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), insertCFRule(2, "R1", "A1:D4"));
        });
    });

    describe('"deleteRows" and "changeCFRule"', () => {
        it("should shift rule", () => {
            testRunner().runBidiTest(
                deleteRows(1, "5"), opSeries2(changeCFRule, 1, "R1", [{ attr: ATTRS }, "A1:D4", "A3:D6", "A6:D9"]),
                deleteRows(1, "5"), opSeries2(changeCFRule, 1, "R1", [{ attr: ATTRS }, "A1:D4", "A3:D5", "A5:D8"])
            );
        });
        it("should delete rule explicitly", () => {
            testRunner().runBidiTest(
                deleteRows(1, "1:9"), changeCFRule(1, "R1", "A3:D6"),
                [deleteCFRule(1, "R1"), deleteRows(1, "1:9")], []
            );
        });
        it("should not transform rules in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), changeCFRule(2, "R1", "A1:D4"));
        });
    });

    describe('"deleteRows" and cell anchor', () => {
        it("should transform cell anchor", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), cellAnchorOps(1, "A1", "B2", "D4", "E5"),
                deleteRows(1, "3"), cellAnchorOps(1, "A1", "B2", "D3", "E4")
            );
            testRunner().runBidiTest(
                deleteRows(1, "3:4 6:7"), cellAnchorOps(1, "A1", "B2", "E5", "H8", "I9"),
                deleteRows(1, "3:4 6:7"), cellAnchorOps(1, "A1", "B2", "E3", "H4", "I5")
            );
        });
        it("should handle deleted cell note", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), insertNote(1, "C3", "abc"), [deleteNote(1, "C3"), deleteRows(1, "3")], []);
            testRunner().runBidiTest(deleteRows(1, "3"), deleteNote(1, "C3"),        null,                                      []);
            testRunner().runBidiTest(deleteRows(1, "3"), changeNote(1, "C3", "abc"), [deleteNote(1, "C3"), deleteRows(1, "3")], []);
        });
        it("should handle deleted cell comment", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), insertComment(1, "C3", 0, { text: "abc" }), [deleteComment(1, "C3", 0), deleteRows(1, "3")], []);
            testRunner().runBidiTest(deleteRows(1, "3"), insertComment(1, "C3", 1, { text: "abc" }), null,                                            []);
            testRunner().runBidiTest(deleteRows(1, "3"), deleteComment(1, "C3", 0),                  null,                                            []);
            testRunner().runBidiTest(deleteRows(1, "3"), deleteComment(1, "C3", 1),                  null,                                            []);
            testRunner().runBidiTest(deleteRows(1, "3"), changeComment(1, "C3", 0, { text: "abc" }), [deleteComment(1, "C3", 0), deleteRows(1, "3")], []);
            testRunner().runBidiTest(deleteRows(1, "3"), changeComment(1, "C3", 1, { text: "abc" }), null,                                            []);
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), cellAnchorOps(2, "D4"));
        });
    });

    describe('"deleteRows" and "moveNotes"', () => {
        it("should transform cell anchors", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), moveNotes(1, "A1 B2 C4 D5", "D1 C2 B4 A5"),
                deleteRows(1, "3"), moveNotes(1, "A1 B2 C3 D4", "D1 C2 B3 A4")
            );
        });
        it("should handle deleted source anchor", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), moveNotes(1, "A2 B3 C3 D4", "A5 B5 A3 D5"),
                [deleteNote(1, "B5"), deleteNote(1, "A3"), deleteRows(1, "3")], moveNotes(1, "A2 D3", "A4 D4")
            );
        });
        it("should handle deleted target anchor", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), moveNotes(1, "A5 B5 C5", "A2 B3 C4"),
                [deleteNote(1, "B3"), deleteRows(1, "3")], [deleteNote(1, "B4"), moveNotes(1, "A4 C4", "A2 C3")]
            );
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), moveNotes(2, "D4", "E5"));
        });
    });

    describe('"deleteRows" and "moveComments"', () => {
        it("should transform cell anchors", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), moveComments(1, "A1 B2 C4 D5", "D1 C2 B4 A5"),
                deleteRows(1, "3"), moveComments(1, "A1 B2 C3 D4", "D1 C2 B3 A4")
            );
        });
        it("should handle deleted source anchor", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), moveComments(1, "A2 B3 C3 D4", "A5 B5 A3 D5"),
                [deleteComment(1, "B5", 0), deleteComment(1, "A3", 0), deleteRows(1, "3")], moveComments(1, "A2 D3", "A4 D4")
            );
        });
        it("should handle deleted target anchor", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), moveComments(1, "A5 B5 C5", "A2 B3 C4"),
                [deleteComment(1, "B3", 0), deleteRows(1, "3")], [deleteComment(1, "B4", 0), moveComments(1, "A4 C4", "A2 C3")]
            );
        });
        it("should not transform cell anchor in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), moveComments(2, "D4", "E5"));
        });
    });

    describe('"deleteRows" and drawing anchor', () => {
        it("should transform drawing anchor", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 A2 10 10 B3 20 20", "2 A3 10 10 B4 20 20", "2 A4 10 10 B5 20 20"),
                deleteRows(1, "3"), drawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 A2 10 10 B3 20 0",  "2 A3 10 0 B3 20 20",  "2 A3 10 10 B4 20 20"),
                { warnings: true } // missing transformation of absolute drawing position
            );
            testRunner().runBidiTest(
                deleteRows(1, "3:4 6:7"), drawingAnchorOps(1, "2 A1 10 10 B4 20 20", "2 A2 10 10 B5 20 20", "2 A3 10 10 B6 20 20", "2 A5 10 10 B8 20 20"),
                deleteRows(1, "3:4 6:7"), drawingAnchorOps(1, "2 A1 10 10 B3 20 0",  "2 A2 10 10 B3 20 20", "2 A3 10 0 B4 20 0",   "2 A3 10 10 B4 20 20"),
                { warnings: true } // missing transformation of absolute drawing position
            );
        });
        const DEL_ANCHOR_O = { drawing: { anchor: "2 C3 10 10 D4 20 20" } };  // original
        const DEL_ANCHOR_T = { drawing: { anchor: "2 C2 10 0 D2 20 0" } };    // transformed
        it("should handle deleted drawing anchor of drawing objects", () => {
            testRunner().runBidiTest(
                deleteRows(1, "2:5"), [insertShape(1, 0, DEL_ANCHOR_O), changeDrawing(1, 1, DEL_ANCHOR_O)],
                [deleteDrawing(1, 0), deleteDrawing(1, 0), deleteRows(1, "2:5")], []
            );
        });
        it("should handle deleted drawing anchor of notes and comments", () => {
            testRunner().runBidiTest(
                deleteRows(1, "2:5"), [insertNote(1, "A1", "abc", DEL_ANCHOR_O), changeNote(1, "A1", DEL_ANCHOR_O), insertComment(1, "A1", 0, { attrs: DEL_ANCHOR_O }), changeComment(1, "A1", 0, { attrs: DEL_ANCHOR_O })],
                deleteRows(1, "2:5"), [insertNote(1, "A1", "abc", DEL_ANCHOR_T), changeNote(1, "A1", DEL_ANCHOR_T), insertComment(1, "A1", 0, { attrs: DEL_ANCHOR_T }), changeComment(1, "A1", 0, { attrs: DEL_ANCHOR_T })],
                { warnings: true });
        });
        it("should log warnings for absolute anchor", () => {
            const anchorOps = drawingAnchorOps(1, "2 A1 0 0 C3 0 0");
            testRunner().expectBidiWarnings(deleteRows(1, "3"), anchorOps, anchorOps.length);
        });
        it("should not log warnings without anchor attribute", () => {
            testRunner().expectBidiWarnings(deleteRows(1, "3"), drawingNoAnchorOps(1), 0);
        });
        it("should not transform drawing anchor in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), drawingAnchorOps(2, "2 A1 10 10 D4 20 20"));
        });
    });

    describe('"deleteRows" and "sheetSelection"', () => {
        it("should transform selected ranges", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3:4 6:7"), sheetSelection(1, "A1:C3 D4:F6 G7:I9", 0, "A1", [0, 1]),
                deleteRows(1, "3:4 6:7"), sheetSelection(1, "A1:C2 D3:F3 G4:I5", 0, "A1", [0, 1])
            );
        });
        it("should transform active address", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3:4 6:7"), opSeries2(sheetSelection, 1, "A1:D1048576", 0, ["A1", "B2", "C3", "D4", "E5", "F6", "G7", "H8", "I9"]),
                deleteRows(1, "3:4 6:7"), opSeries2(sheetSelection, 1, "A1:D1048576", 0, ["A1", "B2", "C3", "D3", "E3", "F4", "G4", "H4", "I5"])
            );
        });
        it("should transform origin address", () => {
            testRunner().runBidiTest(
                deleteRows(1, "3:4 6:7"), opSeries2(sheetSelection, 1, "A1:D1048576", 0, "A1", ["A1", "B2", "C3", "D4", "E5", "F6", "G7", "H8", "I9"]),
                deleteRows(1, "3:4 6:7"), opSeries2(sheetSelection, 1, "A1:D1048576", 0, "A1", ["A1", "B2", "C3", "D3", "E3", "F4", "G4", "H4", "I5"])
            );
        });
        it("should transform deleted active range", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), sheetSelection(1, "E5 D4 C3 B2 A1", 0, "E5", "E5"), null, sheetSelection(1, "E4 D3 B2 A1", 0, "E4", "E4"));
            testRunner().runBidiTest(deleteRows(1, "3"), sheetSelection(1, "E5 D4 C3 B2 A1", 1, "D4", "D4"), null, sheetSelection(1, "E4 D3 B2 A1", 1, "D3", "D3"));
            testRunner().runBidiTest(deleteRows(1, "3"), sheetSelection(1, "E5 D4 C3 B2 A1", 2, "C3", "C3"), null, sheetSelection(1, "E4 D3 B2 A1", 0, "E4"));
            testRunner().runBidiTest(deleteRows(1, "3"), sheetSelection(1, "E5 D4 C3 B2 A1", 3, "B2", "B2"), null, sheetSelection(1, "E4 D3 B2 A1", 2, "B2", "B2"));
            testRunner().runBidiTest(deleteRows(1, "3"), sheetSelection(1, "E5 D4 C3 B2 A1", 4, "A1", "A1"), null, sheetSelection(1, "E4 D3 B2 A1", 3, "A1", "A1"));
        });
        it("should keep active cell when deleting the selection", () => {
            testRunner().runBidiTest(deleteRows(1, "3:4"), sheetSelection(1, "F3:I4 A3:D4", 0, "G3"), null, sheetSelection(1, "G3", 0, "G3"));
            testRunner().runBidiTest(deleteRows(1, "3:4"), sheetSelection(1, "F3:I4 A3:D4", 1, "B4"), null, sheetSelection(1, "B3", 0, "B3"));
        });
        it("should not transform ranges in different sheets", () => {
            testRunner().runBidiTest(deleteRows(1, "3"), sheetSelection(2, "A1:D4", 0, "A1"));
        });
    });

    // changeRows -------------------------------------------------------------

    describe('"changeRows" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(changeRows(1, "3", "a1"), [
                GLOBAL_OPS, STYLESHEET_OPS, colOps(1), cellOps(1),
                hlinkOps(1), nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    describe('"changeRows" and "changeRows"', () => {

        const LCL_ATTRS = { f1: { p1: 10, p2: 20 } };     // local attributes
        const EXT_ATTRS_1 = { f2: { p1: 10 } };           // independent formatting attributes (local and external remain unchanged)
        const EXT_ATTRS_2 = { f1: { p1: 10, p3: 30 } };   // reduced formatting (both operations remain active)
        const EXT_ATTRS_3 = { f1: { p1: 10 } };           // reduced formatting (external operation becomes no-op)
        const EXT_ATTRS_4 = LCL_ATTRS;                    // equal formatting (both operations become no-op)
        const LCL_ATTRS_REDUCED = { f1: { p2: 20 } };     // expected reduced local attribute set (for steps 2 and 3)
        const EXT_ATTRS_REDUCED = { f1: { p3: 30 } };     // expected reduced external attribute set (for step 2)

        it("should not transform anything in distinct intervals", () => {
            testRunner().runTest(changeRows(1, "1:3", LCL_ATTRS), changeRows(1, "4:6", EXT_ATTRS_1));
            testRunner().runTest(changeRows(1, "1:3", LCL_ATTRS), changeRows(1, "4:6", EXT_ATTRS_2));
            testRunner().runTest(changeRows(1, "1:3", LCL_ATTRS), changeRows(1, "4:6", EXT_ATTRS_3));
            testRunner().runTest(changeRows(1, "1:3", LCL_ATTRS), changeRows(1, "4:6", EXT_ATTRS_4));
            testRunner().runTest(changeRows(1, "1:3", "a1"),      changeRows(1, "4:6", "a1"));
        });
        it("should transform with partially overlapping intervals", () => {
            testRunner().runTest(changeRows(1, "1:4", LCL_ATTRS), changeRows(1, "3:6", EXT_ATTRS_1));
            testRunner().runTest(changeRows(1, "1:4", LCL_ATTRS), changeRows(1, "3:6", EXT_ATTRS_2), changeRows(1, "1:4", LCL_ATTRS), opSeries1(changeRows, [1, "5:6", EXT_ATTRS_2], [1, "3:4", EXT_ATTRS_REDUCED]));
            testRunner().runTest(changeRows(1, "1:4", LCL_ATTRS), changeRows(1, "3:6", EXT_ATTRS_3), changeRows(1, "1:4", LCL_ATTRS), changeRows(1, "5:6", EXT_ATTRS_3));
            testRunner().runTest(changeRows(1, "1:4", LCL_ATTRS), changeRows(1, "3:6", EXT_ATTRS_4), changeRows(1, "1:2", LCL_ATTRS), changeRows(1, "5:6", EXT_ATTRS_4));
            testRunner().runTest(changeRows(1, "1:4", "a1"),      changeRows(1, "3:6", "a2"),        changeRows(1, "1:4", "a1"),      changeRows(1, "5:6", "a2"));
            testRunner().runTest(changeRows(1, "1:4", "a1"),      changeRows(1, "3:6", "a1"),        changeRows(1, "1:2", "a1"),      changeRows(1, "5:6", "a1"));
        });
        it("should transform with dominant local intervals", () => {
            testRunner().runTest(changeRows(1, "1:6", LCL_ATTRS), changeRows(1, "3:4", EXT_ATTRS_1));
            testRunner().runTest(changeRows(1, "1:6", LCL_ATTRS), changeRows(1, "3:4", EXT_ATTRS_2), changeRows(1, "1:6",     LCL_ATTRS), changeRows(1, "3:4", EXT_ATTRS_REDUCED));
            testRunner().runTest(changeRows(1, "1:6", LCL_ATTRS), changeRows(1, "3:4", EXT_ATTRS_3), changeRows(1, "1:6",     LCL_ATTRS), []);
            testRunner().runTest(changeRows(1, "1:6", LCL_ATTRS), changeRows(1, "3:4", EXT_ATTRS_4), changeRows(1, "1:2 5:6", LCL_ATTRS), []);
            testRunner().runTest(changeRows(1, "1:6", "a1"),      changeRows(1, "3:4", "a2"),        changeRows(1, "1:6",     "a1"),      []);
            testRunner().runTest(changeRows(1, "1:6", "a1"),      changeRows(1, "3:4", "a1"),        changeRows(1, "1:2 5:6", "a1"),      []);
        });
        it("should transform with dominant external intervals", () => {
            testRunner().runTest(changeRows(1, "3:4", LCL_ATTRS), changeRows(1, "1:6", EXT_ATTRS_1));
            testRunner().runTest(changeRows(1, "3:4", LCL_ATTRS), changeRows(1, "1:6", EXT_ATTRS_2), changeRows(1, "3:4", LCL_ATTRS_REDUCED), opSeries1(changeRows, [1, "1:2 5:6", EXT_ATTRS_2], [1, "3:4", EXT_ATTRS_REDUCED]));
            testRunner().runTest(changeRows(1, "3:4", LCL_ATTRS), changeRows(1, "1:6", EXT_ATTRS_3), changeRows(1, "3:4", LCL_ATTRS_REDUCED), changeRows(1, "1:2 5:6", EXT_ATTRS_3));
            testRunner().runTest(changeRows(1, "3:4", LCL_ATTRS), changeRows(1, "1:6", EXT_ATTRS_4), [],                                      changeRows(1, "1:2 5:6", EXT_ATTRS_4));
            testRunner().runTest(changeRows(1, "3:4", "a1"),      changeRows(1, "1:6", "a2"),        changeRows(1, "3:4", "a1"),              changeRows(1, "1:2 5:6", "a2"));
            testRunner().runTest(changeRows(1, "3:4", "a1"),      changeRows(1, "1:6", "a1"),        [],                                      changeRows(1, "1:2 5:6", "a1"));
        });
        it("should transform with equal intervals", () => {
            testRunner().runTest(changeRows(1, "1:6", LCL_ATTRS), changeRows(1, "1:6", EXT_ATTRS_1));
            testRunner().runTest(changeRows(1, "1:6", LCL_ATTRS), changeRows(1, "1:6", EXT_ATTRS_2), changeRows(1, "1:6", LCL_ATTRS_REDUCED), changeRows(1, "1:6", EXT_ATTRS_REDUCED));
            testRunner().runTest(changeRows(1, "1:6", LCL_ATTRS), changeRows(1, "1:6", EXT_ATTRS_3), changeRows(1, "1:6", LCL_ATTRS_REDUCED), []);
            testRunner().runTest(changeRows(1, "1:6", LCL_ATTRS), changeRows(1, "1:6", EXT_ATTRS_4), [],                                      []);
        });
        it("should not transform anything in different sheets", () => {
            testRunner().runTest(changeRows(1, "1:4", "a1"), changeRows(2, "3:6", "a1"));
        });
    });

    describe('"changeRows" and drawing anchor', () => {
        it("should log warnings for absolute anchor", () => {
            const count = 4 * ROW_ATTRS.length;
            testRunner().expectBidiWarnings(opSeries2(changeRows, 1, "3", ROW_ATTRS), drawingAnchorOps(1, "2 A1 0 0 C3 0 0"), count);
        });
        it("should not log warnings without changed row size", () => {
            testRunner().expectBidiWarnings(changeRows(1, "3", ATTRS), drawingAnchorOps(1, "2 A1 0 0 C3 0 0"), 0);
        });
        it("should not log warnings without anchor attribute", () => {
            testRunner().expectBidiWarnings(opSeries2(changeRows, 1, "3", ROW_ATTRS), drawingNoAnchorOps(1), 0);
        });
    });
});
