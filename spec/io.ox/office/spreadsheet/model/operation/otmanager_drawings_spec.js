/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import {
    opSeries2,
    insertNote, deleteNote, changeNote, moveNotes,
    insertComment, deleteComment, changeComment, moveComments,
    sheetSelection, insertShape, deleteDrawing, changeDrawing, moveDrawing,
    insertChSeries, changeChAxis, insertText, deleteOp, positionOp
} from "~/spreadsheet/apphelper";

import {
    ATTRS, ATTRS2, ATTRS_I1, ATTRS_I2, ATTRS_O1, ATTRS_O2, ATTRS_R1, ATTRS_R2,
    GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS,
    createTestRunner,
    noteOps, commentOps, drawingOps, chartOps, drawingTextOps,
    selectOps, changeDrawingOps
} from "~/spreadsheet/othelper";

// tests ======================================================================

describe("module io.ox/office/spreadsheet/model/operation/otmanager", () => {

    // initialize OT test runner
    const testRunner = createTestRunner();

    // cell notes -------------------------------------------------------------

    describe("cell notes and independent operations", () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(noteOps(1), [
                GLOBAL_OPS, STYLESHEET_OPS, commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    // insertNote -------------------------------------------------------------

    describe('"insertNote" and "insertNote"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runTest(insertNote(1, "A1", "abc"), insertNote(2, "A1", "abc"));
            testRunner().runTest(insertNote(1, "A1", "abc"), insertNote(1, "B2", "abc"));
        });
        it("should update notes when inserting at the same anchor", () => {
            testRunner().runTest(
                insertNote(1, "A1", "abc1"), insertNote(1, "A1", "abc2", ATTRS),
                changeNote(1, "A1", "abc1"), changeNote(1, "A1", ATTRS)
            );
            testRunner().runTest(
                insertNote(1, "A1", "abc1"), insertNote(1, "A1", "abc2"),
                changeNote(1, "A1", "abc1"), []
            );
        });
    });

    describe('"insertNote" and "deleteNote"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), deleteNote(2, "A1"));
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), deleteNote(1, "B2"));
        });
        it("should fail to insert an existing note", () => {
            testRunner().expectBidiError(insertNote(1, "A1", "abc"), deleteNote(1, "A1"));
        });
    });

    describe('"insertNote" and "changeNote"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), changeNote(2, "A1", "abc"));
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), changeNote(1, "B2", "abc"));
        });
        it("should fail to insert an existing note", () => {
            testRunner().expectBidiError(insertNote(1, "A1", "abc"), changeNote(1, "A1", "abc"));
        });
    });

    describe('"insertNote" and "moveNotes"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), moveNotes(2, "A1 B2", "B2 A1"));
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), moveNotes(1, "B2 C3", "C3 B2"));
        });
        it("should fail to insert an existing note", () => {
            testRunner().expectBidiError(insertNote(1, "A1", "abc"), moveNotes(1, "A1 B2", "B2 A1"));
        });
        it("should delete moved note before inserting a note at the address", () => {
            testRunner().runBidiTest(
                insertNote(1, "A1", "abc"), moveNotes(1, "B2 C3", "A1 B2"),
                [deleteNote(1, "A1"), insertNote(1, "A1", "abc")], moveNotes(1, "C3", "B2")
            );
        });
    });

    describe('"insertNote" and "insertComment"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), insertComment(2, "A1", 0));
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), insertComment(1, "B2", 0));
        });
        it("should delete note before inserting a comment at the same address", () => {
            testRunner().runBidiTest(
                insertNote(1, "A1", "abc"), opSeries2(insertComment, 1, "A1", [0, 1, 2]),
                [], [deleteNote(1, "A1"), opSeries2(insertComment, 1, "A1", [0, 1, 2])]
            );
        });
    });

    describe('"insertNote" and "deleteComment"', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), deleteComment(1, "A1", 0));
        });
    });

    describe('"insertNote" and "changeComment"', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), changeComment(1, "A1", 0));
        });
    });

    describe('"insertNote" and "moveComments"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), moveComments(2, "A1 B2", "B2 A1"));
            testRunner().runBidiTest(insertNote(1, "A1", "abc"), moveComments(1, "B2 C3", "C3 B2"));
        });
        it("should fail to insert note for existing comment", () => {
            testRunner().expectBidiError(insertNote(1, "A1", "abc"), moveComments(1, "A1 B2", "B2 A1"));
        });
        it("should delete note before moving a comment to the address", () => {
            testRunner().runBidiTest(
                insertNote(1, "A1", "abc"), moveComments(1, "B2 C3", "A1 B2"),
                [], [deleteNote(1, "A1"), moveComments(1, "B2 C3", "A1 B2")]
            );
        });
    });

    // deleteNote -------------------------------------------------------------

    describe('"deleteNote" and "deleteNote"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runTest(deleteNote(1, "A1"), deleteNote(2, "A1"));
            testRunner().runTest(deleteNote(1, "A1"), deleteNote(1, "B2"));
        });
        it('should set notes at same anchor to "removed" state', () => {
            testRunner().runTest(deleteNote(1, "A1"), deleteNote(1, "A1"), [], []);
        });
    });

    describe('"deleteNote" and "changeNote', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(deleteNote(2, "A1"), changeNote(1, "A1", "abc"));
            testRunner().runBidiTest(deleteNote(1, "B2"), changeNote(1, "A1", "abc"));
        });
        it('should set change operation to "removed" state', () => {
            testRunner().runBidiTest(deleteNote(1, "A1"), changeNote(1, "A1", "abc"), null, []);
        });
    });

    describe('"deleteNote" and "moveNotes', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(deleteNote(1, "A1"), moveNotes(2, "A1 B2", "B2 A1"));
            testRunner().runBidiTest(deleteNote(1, "A1"), moveNotes(1, "B2 C3", "C3 B2"));
        });
        it("should delete an existing note", () => {
            testRunner().runBidiTest(
                deleteNote(1, "A1"), moveNotes(1, "A1 B2", "B2 A1"),
                deleteNote(1, "B2"), moveNotes(1, "B2",    "A1")
            );
            testRunner().runBidiTest(
                deleteNote(1, "A1"), moveNotes(1, "A1", "B2"),
                deleteNote(1, "B2"), []
            );
        });
        it("should fail to move to an existing note", () => {
            testRunner().expectBidiError(deleteNote(1, "A1"), moveNotes(1, "B2", "A1"));
        });
    });

    describe('"deleteNote" and "insertComment"', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(deleteNote(1, "A1"), insertComment(1, "A1", 0));
        });
    });

    describe('"deleteNote" and "deleteComment"', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(deleteNote(1, "A1"), deleteComment(1, "A1", 0));
        });
    });

    describe('"deleteNote" and "changeComment"', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(deleteNote(1, "A1"), changeComment(1, "A1", 0));
        });
    });

    describe('"deleteNote" and "moveComments', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(deleteNote(1, "A1"), moveComments(1, "A1 B2", "B2 A1"));
        });
    });

    // changeNote -------------------------------------------------------------

    describe('"changeNote" and "changeNote"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runTest(changeNote(1, "A1", "abc"), changeNote(2, "A1", "abc"));
            testRunner().runTest(changeNote(1, "A1", "abc"), changeNote(1, "B2", "abc"));
        });
        it("should not process independent attributes", () => {
            testRunner().runTest(changeNote(1, "A1", ATTRS_I1), changeNote(1, "A1", ATTRS_I2));
        });
        it("should reduce attribute sets", () => {
            testRunner().runTest(
                changeNote(1, "A1", ATTRS_O1), changeNote(1, "A1", ATTRS_O2),
                changeNote(1, "A1", ATTRS_R1), changeNote(1, "A1", ATTRS_R2)
            );
        });
        it("should reduce note text", () => {
            testRunner().runTest(
                changeNote(1, "A1", "t1", ATTRS_I1), changeNote(1, "A1", "t1", ATTRS_I2),
                changeNote(1, "A1",       ATTRS_I1), changeNote(1, "A1",       ATTRS_I2)
            );
            testRunner().runTest(
                changeNote(1, "A1", "t1", ATTRS_I1), changeNote(1, "A1", "t2", ATTRS_I2),
                changeNote(1, "A1", "t1", ATTRS_I1), changeNote(1, "A1",       ATTRS_I2)
            );
            testRunner().runBidiTest(changeNote(1, "A1", "t1", ATTRS_I1), changeNote(1, "A1", ATTRS_I2));
        });
        it("should remove entire operation if nothing will change", () => {
            testRunner().runTest(changeNote(1, "A1", "t1", ATTRS), changeNote(1, "A1", "t2", ATTRS2), null,                     []);
            testRunner().runTest(changeNote(1, "A1", "t1", ATTRS), changeNote(1, "A1", "t2", ATTRS), changeNote(1, "A1", "t1"), []);
            testRunner().runTest(changeNote(1, "A1", "t1", ATTRS), changeNote(1, "A1", "t1", ATTRS), [],                        []);
        });
    });

    describe('"changeNote" and "moveNotes', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(changeNote(1, "A1", "abc"), moveNotes(2, "A1 B2", "B2 A1"));
            testRunner().runBidiTest(changeNote(1, "A1", "abc"), moveNotes(1, "B2 C3", "C3 B2"));
        });
        it("should update an existing note", () => {
            testRunner().runBidiTest(
                changeNote(1, "A1", "abc"), moveNotes(1, "A1 B2", "B2 A1"),
                changeNote(1, "B2", "abc"), null
            );
        });
    });

    describe('"changeNote" and "insertComment"', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(changeNote(1, "A1", "abc"), insertComment(1, "A1", 0));
        });
    });

    describe('"changeNote" and "deleteComment"', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(changeNote(1, "A1", "abc"), deleteComment(1, "A1", 0));
        });
    });

    describe('"changeNote" and "changeComment"', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(changeNote(1, "A1", "abc"), changeComment(1, "A1", 0));
        });
    });

    describe('"changeNote" and "moveComments', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(changeNote(1, "A1"), moveComments(1, "A1 B2", "B2 A1"));
        });
    });

    // moveNotes --------------------------------------------------------------

    describe('"moveNotes" and "moveNotes', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(moveNotes(1, "A1 C3", "C3 A1"), moveNotes(2, "A1 B2", "B2 A1"));
        });
        it("should ignore moving same note to same cell", () => {
            testRunner().runTest(
                moveNotes(1, "A1 D4", "B2 E5"), moveNotes(1, "A1 F6", "B2 G7"),
                moveNotes(1, "D4",    "E5"),    moveNotes(1, "F6",    "G7")
            );
            testRunner().runTest(moveNotes(1, "A1", "B2"), moveNotes(1, "A1", "B2"), [], []);
        });
        it("should prefer local move when moving same note to different cells", () => {
            testRunner().runTest(
                moveNotes(1, "A1 D4", "B2 E5"), moveNotes(1, "A1 F6", "C3 G7"),
                moveNotes(1, "C3 D4", "B2 E5"), moveNotes(1, "F6", "G7")
            );
            testRunner().runTest(
                moveNotes(1, "A1", "B2"), moveNotes(1, "A1", "C3"),
                moveNotes(1, "C3", "B2"), []
            );
        });
        it("should prefer local move when moving different notes to same cell", () => {
            testRunner().runTest(
                moveNotes(1, "A1 D4",    "C3 E5"),    moveNotes(1, "B2 F6", "C3 G7"),
                moveNotes(1, "A1 C3 D4", "C3 B2 E5"), moveNotes(1, "F6", "G7")
            );
            testRunner().runTest(
                moveNotes(1, "A1",    "C3"),    moveNotes(1, "B2", "C3"),
                moveNotes(1, "A1 C3", "C3 B2"), []
            );
        });
        it("should handle concurring cyclic moves", () => {
            testRunner().runTest(
                moveNotes(1, "A1 B2 D4",    "B2 A1 E5"),    moveNotes(1, "A1 C3 F6", "C3 A1 G7"),
                moveNotes(1, "C3 B2 A1 D4", "B2 A1 C3 E5"), moveNotes(1, "F6",       "G7")
            );
            testRunner().runTest(
                moveNotes(1, "A1 B2",    "B2 A1"),    moveNotes(1, "A1 C3", "C3 A1"),
                moveNotes(1, "C3 B2 A1", "B2 A1 C3"), []
            );
        });
    });

    describe('"moveNotes" and "insertComment"', () => {
        it("should skip different sheets", () => {
            testRunner().runBidiTest(moveNotes(1, "A1 B2", "B2 A1"), insertComment(2, "A1", 0));
            testRunner().runBidiTest(moveNotes(1, "B2 C3", "C3 B2"), insertComment(1, "A1", 0));
        });
        it("should fail to insert comment for existing note", () => {
            testRunner().expectBidiError(moveNotes(1, "A1 B2", "B2 A1"), insertComment(1, "A1", 0));
        });
        it("should delete moved note before inserting a comment to the address", () => {
            testRunner().runBidiTest(
                moveNotes(1, "B2 C3", "A1 B2"), insertComment(1, "A1", 0),
                moveNotes(1, "C3", "B2"), [deleteNote(1, "A1"), insertComment(1, "A1", 0)]
            );
            testRunner().runBidiTest(
                moveNotes(1, "B2", "A1"), insertComment(1, "A1", 0),
                [], [deleteNote(1, "A1"), insertComment(1, "A1", 0)]
            );
        });
    });

    describe('"moveNotes" and "deleteComment"', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(moveNotes(1, "A1 B2", "B2 A1"), deleteComment(1, "A1", 0));
        });
    });

    describe('"moveNotes" and "changeComment"', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(moveNotes(1, "A1 B2", "B2 A1"), changeComment(1, "A1", 0));
        });
    });

    describe('"moveNotes" and "moveComments', () => {
        it("should always be skipped", () => {
            testRunner().runBidiTest(moveNotes(1, "A1 B2", "B2 A1"), moveComments(1, "A1 B2", "B2 A1"));
        });
    });

    // cell comments ----------------------------------------------------------

    describe("cell comments and independent operations", () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(commentOps(1), [
                GLOBAL_OPS, STYLESHEET_OPS, selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    // insertComment ----------------------------------------------------------

    describe('"insertComment" and "insertComment"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runTest(insertComment(1, "A1", 0), insertComment(2, "A1", 0));
            testRunner().runTest(insertComment(1, "A1", 0), insertComment(2, "B2", 0));
        });
        it("should shift comment index", () => {
            testRunner().runTest(insertComment(1, "A1", 0), insertComment(1, "A1", 0), insertComment(1, "A1", 1), insertComment(1, "A1", 0));
            testRunner().runTest(insertComment(1, "A1", 0), insertComment(1, "A1", 1), insertComment(1, "A1", 0), insertComment(1, "A1", 2));
            testRunner().runTest(insertComment(1, "A1", 0), insertComment(1, "A1", 2), insertComment(1, "A1", 0), insertComment(1, "A1", 3));
            testRunner().runTest(insertComment(1, "A1", 1), insertComment(1, "A1", 0), insertComment(1, "A1", 2), insertComment(1, "A1", 0));
            testRunner().runTest(insertComment(1, "A1", 1), insertComment(1, "A1", 1), insertComment(1, "A1", 2), insertComment(1, "A1", 1));
            testRunner().runTest(insertComment(1, "A1", 1), insertComment(1, "A1", 2), insertComment(1, "A1", 1), insertComment(1, "A1", 3));
            testRunner().runTest(insertComment(1, "A1", 2), insertComment(1, "A1", 0), insertComment(1, "A1", 3), insertComment(1, "A1", 0));
            testRunner().runTest(insertComment(1, "A1", 2), insertComment(1, "A1", 1), insertComment(1, "A1", 3), insertComment(1, "A1", 1));
            testRunner().runTest(insertComment(1, "A1", 2), insertComment(1, "A1", 2), insertComment(1, "A1", 3), insertComment(1, "A1", 2));
        });
    });

    describe('"insertComment" and "deleteComment"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(insertComment(1, "A1", 0), deleteComment(2, "A1", 0));
            testRunner().runBidiTest(insertComment(1, "A1", 0), deleteComment(2, "A1", 2));
            testRunner().runBidiTest(insertComment(1, "A1", 2), deleteComment(2, "A1", 0));
            testRunner().runBidiTest(insertComment(1, "A1", 2), deleteComment(2, "A1", 2));
            testRunner().runBidiTest(insertComment(1, "A1", 0), deleteComment(2, "B2", 0));
            testRunner().runBidiTest(insertComment(1, "A1", 0), deleteComment(2, "B2", 2));
            testRunner().runBidiTest(insertComment(1, "A1", 2), deleteComment(2, "B2", 0));
            testRunner().runBidiTest(insertComment(1, "A1", 2), deleteComment(2, "B2", 2));
        });
        it("should shift comment index", () => {
            testRunner().runBidiTest(insertComment(1, "A1", 0), deleteComment(1, "A1", 1), insertComment(1, "A1", 0), deleteComment(1, "A1", 2));
            testRunner().runBidiTest(insertComment(1, "A1", 0), deleteComment(1, "A1", 2), insertComment(1, "A1", 0), deleteComment(1, "A1", 3));
            testRunner().runBidiTest(insertComment(1, "A1", 0), deleteComment(1, "A1", 3), insertComment(1, "A1", 0), deleteComment(1, "A1", 4));
            testRunner().runBidiTest(insertComment(1, "A1", 1), deleteComment(1, "A1", 1), insertComment(1, "A1", 1), deleteComment(1, "A1", 2));
            testRunner().runBidiTest(insertComment(1, "A1", 1), deleteComment(1, "A1", 2), insertComment(1, "A1", 1), deleteComment(1, "A1", 3));
            testRunner().runBidiTest(insertComment(1, "A1", 1), deleteComment(1, "A1", 3), insertComment(1, "A1", 1), deleteComment(1, "A1", 4));
            testRunner().runBidiTest(insertComment(1, "A1", 2), deleteComment(1, "A1", 1), insertComment(1, "A1", 1), deleteComment(1, "A1", 1));
            testRunner().runBidiTest(insertComment(1, "A1", 2), deleteComment(1, "A1", 2), insertComment(1, "A1", 2), deleteComment(1, "A1", 3));
            testRunner().runBidiTest(insertComment(1, "A1", 2), deleteComment(1, "A1", 3), insertComment(1, "A1", 2), deleteComment(1, "A1", 4));
        });
        it("should handle deleted threads", () => {
            testRunner().runBidiTest(insertComment(1, "A1", 0), deleteComment(1, "A1", 0), []);
            testRunner().runBidiTest(insertComment(1, "A1", 1), deleteComment(1, "A1", 0), []);
            testRunner().runBidiTest(insertComment(1, "A1", 2), deleteComment(1, "A1", 0), []);
        });
    });

    describe('"insertComment" and "changeComment"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(insertComment(1, "A1", 0), changeComment(2, "A1", 0));
            testRunner().runBidiTest(insertComment(1, "A1", 0), changeComment(2, "B2", 0));
        });
        it("should shift comment index", () => {
            testRunner().runBidiTest(insertComment(1, "A1", 0), opSeries2(changeComment, 1, "A1", [0, 1, 2]), null, opSeries2(changeComment, 1, "A1", [1, 2, 3]));
            testRunner().runBidiTest(insertComment(1, "A1", 1), opSeries2(changeComment, 1, "A1", [0, 1, 2]), null, opSeries2(changeComment, 1, "A1", [0, 2, 3]));
            testRunner().runBidiTest(insertComment(1, "A1", 2), opSeries2(changeComment, 1, "A1", [0, 1, 2]), null, opSeries2(changeComment, 1, "A1", [0, 1, 3]));
        });
    });

    describe('"insertComment" and "moveComments"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(insertComment(1, "A1", 0), moveComments(2, "A1 B2", "B2 A1"));
            testRunner().runBidiTest(insertComment(1, "A1", 0), moveComments(1, "B2 C3", "C3 B2"));
        });
        it("should insert into an existing comment", () => {
            testRunner().runBidiTest(
                insertComment(1, "A1", 1), moveComments(1, "A1 B2", "B2 A1"),
                insertComment(1, "B2", 1), null
            );
        });
    });

    // deleteComment ----------------------------------------------------------

    describe('"deleteComment" and "deleteComment"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runTest(deleteComment(1, "A1", 0), deleteComment(2, "A1", 0));
            testRunner().runTest(deleteComment(1, "A1", 0), deleteComment(2, "B2", 0));
        });
        it("should shift comment index", () => {
            testRunner().runTest(deleteComment(1, "A1", 1), deleteComment(1, "A1", 1), [],                        []);
            testRunner().runTest(deleteComment(1, "A1", 1), deleteComment(1, "A1", 2), deleteComment(1, "A1", 1), deleteComment(1, "A1", 1));
            testRunner().runTest(deleteComment(1, "A1", 1), deleteComment(1, "A1", 3), deleteComment(1, "A1", 1), deleteComment(1, "A1", 2));
            testRunner().runTest(deleteComment(1, "A1", 2), deleteComment(1, "A1", 1), deleteComment(1, "A1", 1), deleteComment(1, "A1", 1));
            testRunner().runTest(deleteComment(1, "A1", 2), deleteComment(1, "A1", 2), [],                        []);
            testRunner().runTest(deleteComment(1, "A1", 2), deleteComment(1, "A1", 3), deleteComment(1, "A1", 2), deleteComment(1, "A1", 2));
            testRunner().runTest(deleteComment(1, "A1", 3), deleteComment(1, "A1", 1), deleteComment(1, "A1", 2), deleteComment(1, "A1", 1));
            testRunner().runTest(deleteComment(1, "A1", 3), deleteComment(1, "A1", 2), deleteComment(1, "A1", 2), deleteComment(1, "A1", 2));
            testRunner().runTest(deleteComment(1, "A1", 3), deleteComment(1, "A1", 3), [],                        []);
        });
        it("should handle deleted threads", () => {
            testRunner().runTest(deleteComment(1, "A1", 0), deleteComment(1, "A1", 2), null, []);
            testRunner().runTest(deleteComment(1, "A1", 0), deleteComment(1, "A1", 1), null, []);
            testRunner().runTest(deleteComment(1, "A1", 0), deleteComment(1, "A1", 0), [], []);
            testRunner().runTest(deleteComment(1, "A1", 1), deleteComment(1, "A1", 0), []);
            testRunner().runTest(deleteComment(1, "A1", 2), deleteComment(1, "A1", 0), []);
        });
    });

    describe('"deleteComment" and "changeComment"', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(deleteComment(1, "A1", 0), changeComment(2, "A1", 0));
            testRunner().runBidiTest(deleteComment(1, "A1", 0), changeComment(2, "B2", 0));
        });
        it("should shift comment index", () => {
            testRunner().runBidiTest(deleteComment(1, "A1", 1), opSeries2(changeComment, 1, "A1", [0, 5, 1, 6, 2, 7, 3]), null, opSeries2(changeComment, 1, "A1", [0, 4, 5, 1, 6, 2]));
            testRunner().runBidiTest(deleteComment(1, "A1", 2), opSeries2(changeComment, 1, "A1", [0, 5, 1, 6, 2, 7, 3]), null, opSeries2(changeComment, 1, "A1", [0, 4, 1, 5, 6, 2]));
            testRunner().runBidiTest(deleteComment(1, "A1", 3), opSeries2(changeComment, 1, "A1", [0, 5, 1, 6, 2, 7, 3]), null, opSeries2(changeComment, 1, "A1", [0, 4, 1, 5, 2, 6]));
        });
        it("should handle deleted threads", () => {
            testRunner().runBidiTest(deleteComment(1, "A1", 0), opSeries2(changeComment, 1, "A1", [0, 5, 1, 6, 2, 7, 3]), null, []);
        });
    });

    describe('"deleteComment" and "moveComments', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(deleteComment(1, "A1", 1), moveComments(2, "A1 B2", "B2 A1"));
            testRunner().runBidiTest(deleteComment(1, "A1", 1), moveComments(1, "B2 C3", "C3 B2"));
        });
        it("should delete a comment but not the move", () => {
            testRunner().runBidiTest(
                deleteComment(1, "A1", 1), moveComments(1, "A1", "B2"),
                deleteComment(1, "B2", 1)
            );
        });
        it("should delete a comment thread and the move", () => {
            testRunner().runBidiTest(
                deleteComment(1, "A1", 0), moveComments(1, "A1 B2", "B2 A1"),
                deleteComment(1, "B2", 0), moveComments(1, "B2",    "A1")
            );
            testRunner().runBidiTest(
                deleteComment(1, "A1", 0), moveComments(1, "A1", "B2"),
                deleteComment(1, "B2", 0), []
            );
        });
        it("should fail to move to an existing comment", () => {
            testRunner().expectBidiError(deleteComment(1, "A1", 0), moveComments(1, "B2", "A1"));
        });
    });

    // changeComment ----------------------------------------------------------

    describe('"changeComment" and "changeComment"', () => {
        it("should skip different sheets, anchors, and indexes", () => {
            testRunner().runTest(changeComment(1, "A1", 1, { text: "c1" }), changeComment(2, "A1", 1, { text: "c1" }));
            testRunner().runTest(changeComment(1, "A1", 1, { text: "c1" }), changeComment(1, "B2", 1, { text: "c1" }));
            testRunner().runTest(changeComment(1, "A1", 1, { text: "c1" }), changeComment(1, "A1", 2, { text: "c1" }));
        });
        it("should not process independent attributes", () => {
            testRunner().runTest(changeComment(1, "A1", 1, { attrs: ATTRS_I1 }), changeComment(1, "A1", 1, { attrs: ATTRS_I2 }));
        });
        it("should reduce attribute sets", () => {
            testRunner().runTest(
                changeComment(1, "A1", 1, { attrs: ATTRS_O1 }), changeComment(1, "A1", 1, { attrs: ATTRS_O2 }),
                changeComment(1, "A1", 1, { attrs: ATTRS_R1 }), changeComment(1, "A1", 1, { attrs: ATTRS_R2 })
            );
        });
        it("should reduce comment text", () => {
            testRunner().runTest(
                changeComment(1, "A1", 1, { attrs: ATTRS_I1, text: "c1" }), changeComment(1, "A1", 1, { attrs: ATTRS_I2, text: "c1" }),
                changeComment(1, "A1", 1, { attrs: ATTRS_I1 }),             changeComment(1, "A1", 1, { attrs: ATTRS_I2 })
            );
            testRunner().runTest(
                changeComment(1, "A1", 1, { attrs: ATTRS_I1, text: "c1" }), changeComment(1, "A1", 1, { attrs: ATTRS_I2, text: "c2" }),
                changeComment(1, "A1", 1, { attrs: ATTRS_I1, text: "c1" }), changeComment(1, "A1", 1, { attrs: ATTRS_I2 })
            );
            testRunner().runBidiTest(changeComment(1, "A1", 1, { attrs: ATTRS_I1, text: "c1" }), changeComment(1, "A1", 1, { attrs: ATTRS_I2 }));
        });
        it("should reduce mentions", () => {
            testRunner().runTest(
                changeComment(1, "A1", 1, { attrs: ATTRS_I1, mentions: [1] }), changeComment(1, "A1", 1, { attrs: ATTRS_I2, mentions: [1] }),
                changeComment(1, "A1", 1, { attrs: ATTRS_I1 }),                changeComment(1, "A1", 1, { attrs: ATTRS_I2 })
            );
            testRunner().runTest(
                changeComment(1, "A1", 1, { attrs: ATTRS_I1, mentions: [1] }), changeComment(1, "A1", 1, { attrs: ATTRS_I2, mentions: [2] }),
                changeComment(1, "A1", 1, { attrs: ATTRS_I1, mentions: [1] }), changeComment(1, "A1", 1, { attrs: ATTRS_I2 })
            );
            testRunner().runBidiTest(changeComment(1, "A1", 1, { attrs: ATTRS_I1, mentions: [1] }), changeComment(1, "A1", 1, { attrs: ATTRS_I2 }));
        });
        it('should reduce "done" flag"', () => {
            testRunner().runTest(
                changeComment(1, "A1", 1, { attrs: ATTRS_I1, done: true }), changeComment(1, "A1", 1, { attrs: ATTRS_I2, done: true }),
                changeComment(1, "A1", 1, { attrs: ATTRS_I1 }),             changeComment(1, "A1", 1, { attrs: ATTRS_I2 })
            );
            testRunner().runTest(
                changeComment(1, "A1", 1, { attrs: ATTRS_I1, done: true }), changeComment(1, "A1", 1, { attrs: ATTRS_I2, done: false }),
                changeComment(1, "A1", 1, { attrs: ATTRS_I1, done: true }), changeComment(1, "A1", 1, { attrs: ATTRS_I2 })
            );
            testRunner().runTest(
                changeComment(1, "A1", 1, { attrs: ATTRS_I1, done: false }), changeComment(1, "A1", 1, { attrs: ATTRS_I2, done: true }),
                changeComment(1, "A1", 1, { attrs: ATTRS_I1, done: false }), changeComment(1, "A1", 1, { attrs: ATTRS_I2 })
            );
            testRunner().runBidiTest(changeComment(1, "A1", 1, { attrs: ATTRS_I1, done: true }), changeComment(1, "A1", 1, { attrs: ATTRS_I2 }));
        });
        it("should remove entire operation if nothing will change", () => {
            testRunner().runTest(changeComment(1, "A1", 1, { attrs: ATTRS, text: "c1" }), changeComment(1, "A1", 1, { attrs: ATTRS2, text: "c2" }), null,                                      []);
            testRunner().runTest(changeComment(1, "A1", 1, { attrs: ATTRS, text: "c1" }), changeComment(1, "A1", 1, { attrs: ATTRS, text: "c2" }),  changeComment(1, "A1", 1, { text: "c1" }), []);
            testRunner().runTest(changeComment(1, "A1", 1, { attrs: ATTRS, text: "c1" }), changeComment(1, "A1", 1, { attrs: ATTRS, text: "c1" }),  [],                                        []);
        });
    });

    describe('"changeComment" and "moveComments', () => {
        it("should skip different sheets and anchors", () => {
            testRunner().runBidiTest(changeComment(1, "A1", 0, "abc"), moveComments(2, "A1 B2", "B2 A1"));
            testRunner().runBidiTest(changeComment(1, "A1", 0, "abc"), moveComments(1, "B2 C3", "C3 B2"));
        });
        it("should update existing comment", () => {
            testRunner().runBidiTest(
                changeComment(1, "A1", 0, "abc"), moveComments(1, "A1 B2", "B2 A1"),
                changeComment(1, "B2", 0, "abc"), null
            );
        });
    });

    // moveComments -----------------------------------------------------------

    describe('"moveComments" and "moveComments', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(moveComments(1, "A1 C3", "C3 A1"), moveComments(2, "A1 B2", "B2 A1"));
        });
        it("should ignore moving same comment to same cell", () => {
            testRunner().runTest(
                moveComments(1, "A1 D4", "B2 E5"), moveComments(1, "A1 F6", "B2 G7"),
                moveComments(1, "D4",    "E5"),    moveComments(1, "F6",    "G7")
            );
            testRunner().runTest(moveComments(1, "A1", "B2"), moveComments(1, "A1", "B2"), [], []);
        });
        it("should prefer local move when moving same comment to different cells", () => {
            testRunner().runTest(
                moveComments(1, "A1 D4", "B2 E5"), moveComments(1, "A1 F6", "C3 G7"),
                moveComments(1, "C3 D4", "B2 E5"), moveComments(1, "F6", "G7")
            );
            testRunner().runTest(
                moveComments(1, "A1", "B2"), moveComments(1, "A1", "C3"),
                moveComments(1, "C3", "B2"), []
            );
        });
        it("should prefer local move when moving different comments to same cell", () => {
            testRunner().runTest(
                moveComments(1, "A1",    "C3"),    moveComments(1, "B2", "C3"),
                moveComments(1, "A1 C3", "C3 B2"), []
            );
        });
        it("should handle concurring cyclic moves", () => {
            testRunner().runTest(
                moveComments(1, "A1 B2 D4",    "B2 A1 E5"),    moveComments(1, "A1 C3 F6", "C3 A1 G7"),
                moveComments(1, "C3 B2 A1 D4", "B2 A1 C3 E5"), moveComments(1, "F6",       "G7")
            );
            testRunner().runTest(
                moveComments(1, "A1 B2",    "B2 A1"),    moveComments(1, "A1 C3", "C3 A1"),
                moveComments(1, "C3 B2 A1", "B2 A1 C3"), []
            );
        });
    });

    // drawing objects --------------------------------------------------------

    describe("drawing objects and independent operations", () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(drawingOps(1), [GLOBAL_OPS, STYLESHEET_OPS]);
        });
    });

    // insertDrawing ----------------------------------------------------------

    describe('"insertDrawing" and "insertDrawing"', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(insertShape(1, 0, ATTRS), insertShape(0, 1, ATTRS));
        });
        it("should shift positions of top-level shapes", () => {
            testRunner().runTest(insertShape(1, 2, ATTRS), insertShape(1, 0, ATTRS), insertShape(1, 3, ATTRS), insertShape(1, 0, ATTRS));
            testRunner().runTest(insertShape(1, 2, ATTRS), insertShape(1, 1, ATTRS), insertShape(1, 3, ATTRS), insertShape(1, 1, ATTRS));
            testRunner().runTest(insertShape(1, 2, ATTRS), insertShape(1, 2, ATTRS), insertShape(1, 3, ATTRS), insertShape(1, 2, ATTRS));
            testRunner().runTest(insertShape(1, 2, ATTRS), insertShape(1, 3, ATTRS), insertShape(1, 2, ATTRS), insertShape(1, 4, ATTRS));
            testRunner().runTest(insertShape(1, 2, ATTRS), insertShape(1, 4, ATTRS), insertShape(1, 2, ATTRS), insertShape(1, 5, ATTRS));
        });
        it("should shift position of external embedded shape", () => {
            testRunner().runTest(insertShape(1, 2, ATTRS), insertShape(1, "0 4", ATTRS), null, insertShape(1, "0 4", ATTRS));
            testRunner().runTest(insertShape(1, 2, ATTRS), insertShape(1, "1 4", ATTRS), null, insertShape(1, "1 4", ATTRS));
            testRunner().runTest(insertShape(1, 2, ATTRS), insertShape(1, "2 4", ATTRS), null, insertShape(1, "3 4", ATTRS));
            testRunner().runTest(insertShape(1, 2, ATTRS), insertShape(1, "3 4", ATTRS), null, insertShape(1, "4 4", ATTRS));
            testRunner().runTest(insertShape(1, 2, ATTRS), insertShape(1, "4 4", ATTRS), null, insertShape(1, "5 4", ATTRS));
        });
        it("should shift position of local embedded shape", () => {
            testRunner().runTest(insertShape(1, "2 4", ATTRS), insertShape(1, 0, ATTRS), insertShape(1, "3 4", ATTRS));
            testRunner().runTest(insertShape(1, "2 4", ATTRS), insertShape(1, 1, ATTRS), insertShape(1, "3 4", ATTRS));
            testRunner().runTest(insertShape(1, "2 4", ATTRS), insertShape(1, 2, ATTRS), insertShape(1, "3 4", ATTRS));
            testRunner().runTest(insertShape(1, "2 4", ATTRS), insertShape(1, 3, ATTRS), insertShape(1, "2 4", ATTRS));
            testRunner().runTest(insertShape(1, "2 4", ATTRS), insertShape(1, 4, ATTRS), insertShape(1, "2 4", ATTRS));
        });
        it("should shift positions of embedded siblings", () => {
            testRunner().runTest(insertShape(1, "2 2", ATTRS), insertShape(1, "2 0", ATTRS), insertShape(1, "2 3", ATTRS), insertShape(1, "2 0", ATTRS));
            testRunner().runTest(insertShape(1, "2 2", ATTRS), insertShape(1, "2 1", ATTRS), insertShape(1, "2 3", ATTRS), insertShape(1, "2 1", ATTRS));
            testRunner().runTest(insertShape(1, "2 2", ATTRS), insertShape(1, "2 2", ATTRS), insertShape(1, "2 3", ATTRS), insertShape(1, "2 2", ATTRS));
            testRunner().runTest(insertShape(1, "2 2", ATTRS), insertShape(1, "2 3", ATTRS), insertShape(1, "2 2", ATTRS), insertShape(1, "2 4", ATTRS));
            testRunner().runTest(insertShape(1, "2 2", ATTRS), insertShape(1, "2 4", ATTRS), insertShape(1, "2 2", ATTRS), insertShape(1, "2 5", ATTRS));
        });
        it("should skip independent embedded shapes", () => {
            testRunner().runTest(insertShape(1, "2 2", ATTRS), opSeries2(insertShape, 1, ["0 0", "0 2", "0 4", "4 0", "4 2", "4 4"], ATTRS));
        });
    });

    describe('"insertDrawing" and "deleteDrawing"', () => {
        it("should skip different sheets", () => {
            testRunner().runBidiTest(insertShape(1, 0, ATTRS), deleteDrawing(0, 1));
        });
        it("should shift positions of top-level shapes", () => {
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, 0), insertShape(1, 1, ATTRS), deleteDrawing(1, 0));
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, 1), insertShape(1, 1, ATTRS), deleteDrawing(1, 1));
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, 2), insertShape(1, 2, ATTRS), deleteDrawing(1, 3));
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, 3), insertShape(1, 2, ATTRS), deleteDrawing(1, 4));
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, 4), insertShape(1, 2, ATTRS), deleteDrawing(1, 5));
        });
        it("should shift position of external embedded shape", () => {
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, "0 4"), null, deleteDrawing(1, "0 4"));
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, "1 4"), null, deleteDrawing(1, "1 4"));
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, "2 4"), null, deleteDrawing(1, "3 4"));
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, "3 4"), null, deleteDrawing(1, "4 4"));
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, "4 4"), null, deleteDrawing(1, "5 4"));
        });
        it("should shift position of local embedded shape", () => {
            testRunner().runBidiTest(insertShape(1, "2 4", ATTRS), deleteDrawing(1, 0), insertShape(1, "1 4", ATTRS));
            testRunner().runBidiTest(insertShape(1, "2 4", ATTRS), deleteDrawing(1, 1), insertShape(1, "1 4", ATTRS));
            testRunner().runBidiTest(insertShape(1, "2 4", ATTRS), deleteDrawing(1, 2), []);
            testRunner().runBidiTest(insertShape(1, "2 4", ATTRS), deleteDrawing(1, 3), insertShape(1, "2 4", ATTRS));
            testRunner().runBidiTest(insertShape(1, "2 4", ATTRS), deleteDrawing(1, 4), insertShape(1, "2 4", ATTRS));
        });
        it("should shift positions of embedded siblings", () => {
            testRunner().runBidiTest(insertShape(1, "2 2", ATTRS), deleteDrawing(1, "2 0"), insertShape(1, "2 1", ATTRS), deleteDrawing(1, "2 0"));
            testRunner().runBidiTest(insertShape(1, "2 2", ATTRS), deleteDrawing(1, "2 1"), insertShape(1, "2 1", ATTRS), deleteDrawing(1, "2 1"));
            testRunner().runBidiTest(insertShape(1, "2 2", ATTRS), deleteDrawing(1, "2 2"), insertShape(1, "2 2", ATTRS), deleteDrawing(1, "2 3"));
            testRunner().runBidiTest(insertShape(1, "2 2", ATTRS), deleteDrawing(1, "2 3"), insertShape(1, "2 2", ATTRS), deleteDrawing(1, "2 4"));
            testRunner().runBidiTest(insertShape(1, "2 2", ATTRS), deleteDrawing(1, "2 4"), insertShape(1, "2 2", ATTRS), deleteDrawing(1, "2 5"));
        });
        it("should skip independent embedded shapes", () => {
            testRunner().runBidiTest(insertShape(1, "2 2", ATTRS), opSeries2(deleteDrawing, 1, ["0 0", "0 2", "0 4", "4 0", "4 2", "4 4"]));
        });
    });

    describe('"insertDrawing" and "moveDrawing"', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(insertShape(1, 0, ATTRS), moveDrawing(0, 1, 2));
        });
        it("should shift positions for top-level shape", () => {
            testRunner().runBidiTest(insertShape(1, 1, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 1, ATTRS), moveDrawing(1, 3, 5));
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 2, ATTRS), moveDrawing(1, 3, 5));
            testRunner().runBidiTest(insertShape(1, 3, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 2, ATTRS), moveDrawing(1, 2, 5));
            testRunner().runBidiTest(insertShape(1, 4, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 3, ATTRS), moveDrawing(1, 2, 5));
            testRunner().runBidiTest(insertShape(1, 5, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 5, ATTRS), moveDrawing(1, 2, 4));
            testRunner().runBidiTest(insertShape(1, 6, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 6, ATTRS), moveDrawing(1, 2, 4));
            testRunner().runBidiTest(insertShape(1, 1, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 1, ATTRS), moveDrawing(1, 5, 3));
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 2, ATTRS), moveDrawing(1, 5, 3));
            testRunner().runBidiTest(insertShape(1, 3, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 4, ATTRS), moveDrawing(1, 5, 2));
            testRunner().runBidiTest(insertShape(1, 4, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 5, ATTRS), moveDrawing(1, 5, 2));
            testRunner().runBidiTest(insertShape(1, 5, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 5, ATTRS), moveDrawing(1, 4, 2));
            testRunner().runBidiTest(insertShape(1, 6, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 6, ATTRS), moveDrawing(1, 4, 2));
            testRunner().runBidiTest(insertShape(1, 1, ATTRS), moveDrawing(1, 3, 3), null, []);
        });
        it("should shift positions for embedded shape", () => {
            testRunner().runBidiTest(insertShape(1, "1 1", ATTRS), moveDrawing(1, "1 2", "1 4"), insertShape(1, "1 1", ATTRS), moveDrawing(1, "1 3", "1 5"));
            testRunner().runBidiTest(insertShape(1, "1 2", ATTRS), moveDrawing(1, "1 2", "1 4"), insertShape(1, "1 2", ATTRS), moveDrawing(1, "1 3", "1 5"));
            testRunner().runBidiTest(insertShape(1, "1 3", ATTRS), moveDrawing(1, "1 2", "1 4"), insertShape(1, "1 2", ATTRS), moveDrawing(1, "1 2", "1 5"));
            testRunner().runBidiTest(insertShape(1, "1 4", ATTRS), moveDrawing(1, "1 2", "1 4"), insertShape(1, "1 3", ATTRS), moveDrawing(1, "1 2", "1 5"));
            testRunner().runBidiTest(insertShape(1, "1 5", ATTRS), moveDrawing(1, "1 2", "1 4"), insertShape(1, "1 5", ATTRS), moveDrawing(1, "1 2", "1 4"));
            testRunner().runBidiTest(insertShape(1, "1 6", ATTRS), moveDrawing(1, "1 2", "1 4"), insertShape(1, "1 6", ATTRS), moveDrawing(1, "1 2", "1 4"));
            testRunner().runBidiTest(insertShape(1, "1 1", ATTRS), moveDrawing(1, "1 4", "1 2"), insertShape(1, "1 1", ATTRS), moveDrawing(1, "1 5", "1 3"));
            testRunner().runBidiTest(insertShape(1, "1 2", ATTRS), moveDrawing(1, "1 4", "1 2"), insertShape(1, "1 2", ATTRS), moveDrawing(1, "1 5", "1 3"));
            testRunner().runBidiTest(insertShape(1, "1 3", ATTRS), moveDrawing(1, "1 4", "1 2"), insertShape(1, "1 4", ATTRS), moveDrawing(1, "1 5", "1 2"));
            testRunner().runBidiTest(insertShape(1, "1 4", ATTRS), moveDrawing(1, "1 4", "1 2"), insertShape(1, "1 5", ATTRS), moveDrawing(1, "1 5", "1 2"));
            testRunner().runBidiTest(insertShape(1, "1 5", ATTRS), moveDrawing(1, "1 4", "1 2"), insertShape(1, "1 5", ATTRS), moveDrawing(1, "1 4", "1 2"));
            testRunner().runBidiTest(insertShape(1, "1 6", ATTRS), moveDrawing(1, "1 4", "1 2"), insertShape(1, "1 6", ATTRS), moveDrawing(1, "1 4", "1 2"));
            testRunner().runBidiTest(insertShape(1, "1 1", ATTRS), moveDrawing(1, "1 3", "1 3"), null, []);
        });
        it("should shift positions of embedded inserted shapes", () => {
            testRunner().runBidiTest(insertShape(1, "1 1", ATTRS), moveDrawing(1, 2, 4), insertShape(1, "1 1", ATTRS));
            testRunner().runBidiTest(insertShape(1, "2 1", ATTRS), moveDrawing(1, 2, 4), insertShape(1, "4 1", ATTRS));
            testRunner().runBidiTest(insertShape(1, "3 1", ATTRS), moveDrawing(1, 2, 4), insertShape(1, "2 1", ATTRS));
            testRunner().runBidiTest(insertShape(1, "4 1", ATTRS), moveDrawing(1, 2, 4), insertShape(1, "3 1", ATTRS));
            testRunner().runBidiTest(insertShape(1, "5 1", ATTRS), moveDrawing(1, 2, 4), insertShape(1, "5 1", ATTRS));
        });
        it("should shift positions of embedded moved shapes", () => {
            testRunner().runBidiTest(insertShape(1, 1, ATTRS), moveDrawing(1, "3 2", "3 4"), null, moveDrawing(1, "4 2", "4 4"));
            testRunner().runBidiTest(insertShape(1, 2, ATTRS), moveDrawing(1, "3 2", "3 4"), null, moveDrawing(1, "4 2", "4 4"));
            testRunner().runBidiTest(insertShape(1, 3, ATTRS), moveDrawing(1, "3 2", "3 4"), null, moveDrawing(1, "4 2", "4 4"));
            testRunner().runBidiTest(insertShape(1, 4, ATTRS), moveDrawing(1, "3 2", "3 4"));
            testRunner().runBidiTest(insertShape(1, 5, ATTRS), moveDrawing(1, "3 2", "3 4"));
        });
        it("should not shift positions of independent shapes", () => {
            testRunner().runBidiTest(insertShape(1, "1 1",   ATTRS), moveDrawing(1, "0 0",   "0 1"));
            testRunner().runBidiTest(insertShape(1, "1 1",   ATTRS), moveDrawing(1, "2 0",   "2 1"));
            testRunner().runBidiTest(insertShape(1, "1 1 1", ATTRS), moveDrawing(1, "0 1 1", "0 1 2"));
        });
    });

    describe('"insertDrawing" and other drawing operations', () => {
        it("should skip different sheets", () => {
            testRunner().runBidiTest(insertShape(1, 0, ATTRS), changeDrawingOps(0, 1));
        });
        it("should shift positions for top-level shape", () => {
            testRunner().runBidiTest(
                insertShape(1, 1, ATTRS), changeDrawingOps(1, "0", "1", "2", "0 4", "1 4", "2 4"),
                insertShape(1, 1, ATTRS), changeDrawingOps(1, "0", "2", "3", "0 4", "2 4", "3 4")
            );
        });
        it("should shift positions for embedded shape", () => {
            testRunner().runBidiTest(
                insertShape(1, "1 1", ATTRS), changeDrawingOps(1, "1 0", "1 1", "1 2", "1 0 4", "1 1 4", "1 2 4"),
                insertShape(1, "1 1", ATTRS), changeDrawingOps(1, "1 0", "1 2", "1 3", "1 0 4", "1 2 4", "1 3 4")
            );
        });
        it("should not shift positions of parent shapes", () => {
            testRunner().runBidiTest(insertShape(1, "1 1", ATTRS), changeDrawingOps(1, 0, 2));
            testRunner().runBidiTest(insertShape(1, "1 1", ATTRS), changeDrawing(1, 1, ATTRS));
        });
        it("should not shift positions of sibling shapes", () => {
            testRunner().runBidiTest(insertShape(1, "1 1", ATTRS), changeDrawingOps(1, "0 0", "0 1", "0 2", "2 0", "2 1", "2 2"));
        });
    });

    describe('"insertDrawing" and "sheetSelection"', () => {
        it("should skip different sheets", () => {
            testRunner().runBidiTest(insertShape(0, 0, ATTRS), sheetSelection(1, "A1", 0, "A1", ["0", "1", "2"]));
        });
        it("should shift positions for top-level shape", () => {
            testRunner().runBidiTest(
                insertShape(1, 1, ATTRS), sheetSelection(1, "A1", 0, "A1", ["0", "1", "2", "0 4", "1 4", "2 4"]),
                insertShape(1, 1, ATTRS), sheetSelection(1, "A1", 0, "A1", ["0", "2", "3", "0 4", "2 4", "3 4"])
            );
        });
        it("should shift positions for embedded shape", () => {
            testRunner().runBidiTest(
                insertShape(1, "1 1", ATTRS), sheetSelection(1, "A1", 0, "A1", ["1 0", "1 1", "1 2", "1 0 4", "1 1 4", "1 2 4"]),
                insertShape(1, "1 1", ATTRS), sheetSelection(1, "A1", 0, "A1", ["1 0", "1 2", "1 3", "1 0 4", "1 2 4", "1 3 4"])
            );
        });
        it("should not shift positions of parent shapes", () => {
            testRunner().runBidiTest(insertShape(1, "1 1", ATTRS), sheetSelection(1, "A1", 0, "A1", ["0", "1", "2"]));
        });
        it("should not shift positions of sibling shapes", () => {
            testRunner().runBidiTest(insertShape(1, "1 1", ATTRS), sheetSelection(1, "A1", 0, "A1", ["0 0", "0 1", "0 2", "2 0", "2 1", "2 2"]));
        });
    });

    // deleteDrawing ----------------------------------------------------------

    describe('"deleteDrawing" and "deleteDrawing"', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(deleteDrawing(1, 0), deleteDrawing(0, 1));
        });
        it("should shift positions of top-level shapes", () => {
            testRunner().runTest(deleteDrawing(1, 2), deleteDrawing(1, 0), deleteDrawing(1, 1), deleteDrawing(1, 0));
            testRunner().runTest(deleteDrawing(1, 2), deleteDrawing(1, 1), deleteDrawing(1, 1), deleteDrawing(1, 1));
            testRunner().runTest(deleteDrawing(1, 2), deleteDrawing(1, 2), [],                   []);
            testRunner().runTest(deleteDrawing(1, 2), deleteDrawing(1, 3), deleteDrawing(1, 2), deleteDrawing(1, 2));
            testRunner().runTest(deleteDrawing(1, 2), deleteDrawing(1, 4), deleteDrawing(1, 2), deleteDrawing(1, 3));
        });
        it("should shift position of external embedded shape", () => {
            testRunner().runTest(deleteDrawing(1, 2), deleteDrawing(1, "0 4"), null, deleteDrawing(1, "0 4"));
            testRunner().runTest(deleteDrawing(1, 2), deleteDrawing(1, "1 4"), null, deleteDrawing(1, "1 4"));
            testRunner().runTest(deleteDrawing(1, 2), deleteDrawing(1, "2 4"), null, []);
            testRunner().runTest(deleteDrawing(1, 2), deleteDrawing(1, "3 4"), null, deleteDrawing(1, "2 4"));
            testRunner().runTest(deleteDrawing(1, 2), deleteDrawing(1, "4 4"), null, deleteDrawing(1, "3 4"));
        });
        it("should shift position of local embedded shape", () => {
            testRunner().runTest(deleteDrawing(1, "2 4"), deleteDrawing(1, 0), deleteDrawing(1, "1 4"));
            testRunner().runTest(deleteDrawing(1, "2 4"), deleteDrawing(1, 1), deleteDrawing(1, "1 4"));
            testRunner().runTest(deleteDrawing(1, "2 4"), deleteDrawing(1, 2), []);
            testRunner().runTest(deleteDrawing(1, "2 4"), deleteDrawing(1, 3), deleteDrawing(1, "2 4"));
            testRunner().runTest(deleteDrawing(1, "2 4"), deleteDrawing(1, 4), deleteDrawing(1, "2 4"));
        });
        it("should shift positions of embedded siblings", () => {
            testRunner().runTest(deleteDrawing(1, "2 2"), deleteDrawing(1, "2 0"), deleteDrawing(1, "2 1"), deleteDrawing(1, "2 0"));
            testRunner().runTest(deleteDrawing(1, "2 2"), deleteDrawing(1, "2 1"), deleteDrawing(1, "2 1"), deleteDrawing(1, "2 1"));
            testRunner().runTest(deleteDrawing(1, "2 2"), deleteDrawing(1, "2 2"), [],                     []);
            testRunner().runTest(deleteDrawing(1, "2 2"), deleteDrawing(1, "2 3"), deleteDrawing(1, "2 2"), deleteDrawing(1, "2 2"));
            testRunner().runTest(deleteDrawing(1, "2 2"), deleteDrawing(1, "2 4"), deleteDrawing(1, "2 2"), deleteDrawing(1, "2 3"));
        });
        it("should skip independent embedded shapes", () => {
            testRunner().runTest(deleteDrawing(1, "2 2"), deleteDrawing(1, "0 0"));
            testRunner().runTest(deleteDrawing(1, "2 2"), deleteDrawing(1, "0 2"));
            testRunner().runTest(deleteDrawing(1, "2 2"), deleteDrawing(1, "0 4"));
            testRunner().runTest(deleteDrawing(1, "2 2"), deleteDrawing(1, "4 0"));
            testRunner().runTest(deleteDrawing(1, "2 2"), deleteDrawing(1, "4 2"));
            testRunner().runTest(deleteDrawing(1, "2 2"), deleteDrawing(1, "4 4"));
        });
    });

    describe('"deleteDrawing" and "moveDrawing"', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(deleteDrawing(1, 0), moveDrawing(0, 1, 2));
        });
        it("should shift positions for top-level shape", () => {
            testRunner().runBidiTest(deleteDrawing(1, 1), moveDrawing(1, 2, 4), deleteDrawing(1, 1), moveDrawing(1, 1, 3));
            testRunner().runBidiTest(deleteDrawing(1, 2), moveDrawing(1, 2, 4), deleteDrawing(1, 4), []);
            testRunner().runBidiTest(deleteDrawing(1, 3), moveDrawing(1, 2, 4), deleteDrawing(1, 2), moveDrawing(1, 2, 3));
            testRunner().runBidiTest(deleteDrawing(1, 4), moveDrawing(1, 2, 4), deleteDrawing(1, 3), moveDrawing(1, 2, 3));
            testRunner().runBidiTest(deleteDrawing(1, 5), moveDrawing(1, 2, 4), deleteDrawing(1, 5), moveDrawing(1, 2, 4));
            testRunner().runBidiTest(deleteDrawing(1, 1), moveDrawing(1, 4, 2), deleteDrawing(1, 1), moveDrawing(1, 3, 1));
            testRunner().runBidiTest(deleteDrawing(1, 2), moveDrawing(1, 4, 2), deleteDrawing(1, 3), moveDrawing(1, 3, 2));
            testRunner().runBidiTest(deleteDrawing(1, 3), moveDrawing(1, 4, 2), deleteDrawing(1, 4), moveDrawing(1, 3, 2));
            testRunner().runBidiTest(deleteDrawing(1, 4), moveDrawing(1, 4, 2), deleteDrawing(1, 2), []);
            testRunner().runBidiTest(deleteDrawing(1, 5), moveDrawing(1, 4, 2), deleteDrawing(1, 5), moveDrawing(1, 4, 2));
            testRunner().runBidiTest(deleteDrawing(1, 1), moveDrawing(1, 3, 3), null, []);
        });
        it("should shift positions for embedded shape", () => {
            testRunner().runBidiTest(deleteDrawing(1, "1 1"), moveDrawing(1, "1 2", "1 4"), deleteDrawing(1, "1 1"), moveDrawing(1, "1 1", "1 3"));
            testRunner().runBidiTest(deleteDrawing(1, "1 2"), moveDrawing(1, "1 2", "1 4"), deleteDrawing(1, "1 4"), []);
            testRunner().runBidiTest(deleteDrawing(1, "1 3"), moveDrawing(1, "1 2", "1 4"), deleteDrawing(1, "1 2"), moveDrawing(1, "1 2", "1 3"));
            testRunner().runBidiTest(deleteDrawing(1, "1 4"), moveDrawing(1, "1 2", "1 4"), deleteDrawing(1, "1 3"), moveDrawing(1, "1 2", "1 3"));
            testRunner().runBidiTest(deleteDrawing(1, "1 5"), moveDrawing(1, "1 2", "1 4"), deleteDrawing(1, "1 5"), moveDrawing(1, "1 2", "1 4"));
            testRunner().runBidiTest(deleteDrawing(1, "1 1"), moveDrawing(1, "1 4", "1 2"), deleteDrawing(1, "1 1"), moveDrawing(1, "1 3", "1 1"));
            testRunner().runBidiTest(deleteDrawing(1, "1 2"), moveDrawing(1, "1 4", "1 2"), deleteDrawing(1, "1 3"), moveDrawing(1, "1 3", "1 2"));
            testRunner().runBidiTest(deleteDrawing(1, "1 3"), moveDrawing(1, "1 4", "1 2"), deleteDrawing(1, "1 4"), moveDrawing(1, "1 3", "1 2"));
            testRunner().runBidiTest(deleteDrawing(1, "1 4"), moveDrawing(1, "1 4", "1 2"), deleteDrawing(1, "1 2"), []);
            testRunner().runBidiTest(deleteDrawing(1, "1 5"), moveDrawing(1, "1 4", "1 2"), deleteDrawing(1, "1 5"), moveDrawing(1, "1 4", "1 2"));
            testRunner().runBidiTest(deleteDrawing(1, "1 1"), moveDrawing(1, "1 3", "1 3"), null, []);
        });
        it("should shift positions of embedded deleted shapes", () => {
            testRunner().runBidiTest(deleteDrawing(1, "1 1"), moveDrawing(1, 2, 4), deleteDrawing(1, "1 1"));
            testRunner().runBidiTest(deleteDrawing(1, "2 1"), moveDrawing(1, 2, 4), deleteDrawing(1, "4 1"));
            testRunner().runBidiTest(deleteDrawing(1, "3 1"), moveDrawing(1, 2, 4), deleteDrawing(1, "2 1"));
            testRunner().runBidiTest(deleteDrawing(1, "4 1"), moveDrawing(1, 2, 4), deleteDrawing(1, "3 1"));
            testRunner().runBidiTest(deleteDrawing(1, "5 1"), moveDrawing(1, 2, 4), deleteDrawing(1, "5 1"));
        });
        it("should shift positions of embedded moved shapes", () => {
            testRunner().runBidiTest(deleteDrawing(1, 1), moveDrawing(1, "3 2", "3 4"), null, moveDrawing(1, "2 2", "2 4"));
            testRunner().runBidiTest(deleteDrawing(1, 2), moveDrawing(1, "3 2", "3 4"), null, moveDrawing(1, "2 2", "2 4"));
            testRunner().runBidiTest(deleteDrawing(1, 3), moveDrawing(1, "3 2", "3 4"), null, []);
            testRunner().runBidiTest(deleteDrawing(1, 4), moveDrawing(1, "3 2", "3 4"));
            testRunner().runBidiTest(deleteDrawing(1, 5), moveDrawing(1, "3 2", "3 4"));
        });
        it("should not shift positions of independent shapes", () => {
            testRunner().runBidiTest(deleteDrawing(1, "1 1"),   moveDrawing(1, "0 0",   "0 1"));
            testRunner().runBidiTest(deleteDrawing(1, "1 1"),   moveDrawing(1, "2 0",   "2 1"));
            testRunner().runBidiTest(deleteDrawing(1, "1 1 1"), moveDrawing(1, "0 1 1", "0 1 2"));
        });
    });

    describe('"deleteDrawing" and other drawing operations', () => {
        it("should skip different sheets", () => {
            testRunner().runBidiTest(deleteDrawing(1, 0), changeDrawingOps(0, 1));
        });
        it("should shift positions for top-level shape", () => {
            testRunner().runBidiTest(
                deleteDrawing(1, 1), changeDrawingOps(1, "0", "1", "4", "2", "0 4", "1 5", "2 6"),
                deleteDrawing(1, 1), changeDrawingOps(1, "0",      "3", "1", "0 4",        "1 6")
            );
        });
        it("should shift positions for embedded shape", () => {
            testRunner().runBidiTest(
                deleteDrawing(1, "1 1"), changeDrawingOps(1, "1 0", "1 1", "1 4", "1 3", "1 0 4", "1 1 5", "1 2 6"),
                deleteDrawing(1, "1 1"), changeDrawingOps(1, "1 0",        "1 3", "1 2", "1 0 4",          "1 1 6")
            );
        });
        it("should not shift positions of parent shapes", () => {
            testRunner().runBidiTest(deleteDrawing(1, "1 1"), changeDrawingOps(1, 0, 2));
            testRunner().runBidiTest(deleteDrawing(1, "1 1"), changeDrawing(1, 1, ATTRS));
        });
        it("should not shift positions of sibling shapes", () => {
            testRunner().runBidiTest(deleteDrawing(1, "1 1"), changeDrawingOps(1, "0 0", "0 1", "0 2", "2 0", "2 1", "2 2"));
        });
    });

    describe('"deleteDrawing" and "sheetSelection"', () => {
        it("should skip different sheets", () => {
            testRunner().runBidiTest(deleteDrawing(0, 0), sheetSelection(1, "A1", 0, "A1", ["0", "1", "2"]));
        });
        it("should shift positions for top-level shape", () => {
            testRunner().runBidiTest(
                deleteDrawing(1, 1), sheetSelection(1, "A1", 0, "A1", ["0", "1", "4", "2", "0 4", "1 5", "2 6"]),
                deleteDrawing(1, 1), sheetSelection(1, "A1", 0, "A1", ["0",      "3", "1", "0 4",        "1 6"])
            );
        });
        it("should shift positions for embedded shape", () => {
            testRunner().runBidiTest(
                deleteDrawing(1, "1 1"), sheetSelection(1, "A1", 0, "A1", ["1 0", "1 1", "1 4", "1 3", "1 0 4", "1 1 5", "1 2 6"]),
                deleteDrawing(1, "1 1"), sheetSelection(1, "A1", 0, "A1", ["1 0",        "1 3", "1 2", "1 0 4",          "1 1 6"])
            );
        });
        it("should not shift positions of parent shapes", () => {
            testRunner().runBidiTest(deleteDrawing(1, "1 1"), sheetSelection(1, "A1", 0, "A1", ["0", "1", "2"]));
        });
        it("should not shift positions of sibling shapes", () => {
            testRunner().runBidiTest(deleteDrawing(1, "1 1"), sheetSelection(1, "A1", 0, "A1", ["0 0", "0 1", "0 2", "2 0", "2 1", "2 2"]));
        });
    });

    // changeDrawing ----------------------------------------------------------

    describe('"changeDrawing" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(changeDrawing(1, 0, ATTRS), [selectOps(1), chartOps(1), drawingTextOps(1), positionOp(1, 0)]);
        });
    });

    describe('"changeDrawing" and "changeDrawing"', () => {
        it("should not process different shapes", () => {
            testRunner().runTest(changeDrawing(1, 0, ATTRS), changeDrawing(0, 1, ATTRS));
            testRunner().runTest(changeDrawing(1, 0, ATTRS), changeDrawing(1, 1, ATTRS));
            testRunner().runBidiTest(changeDrawing(1, 0, ATTRS), changeDrawing(1, "0 1", ATTRS));
            testRunner().runBidiTest(changeDrawing(1, "0 0", ATTRS), changeDrawing(1, "0 1", ATTRS));
            testRunner().runBidiTest(changeDrawing(1, "1 1", ATTRS), changeDrawing(1, "0 1", ATTRS));
        });
        it("should not process operations for independent attributes", () => {
            testRunner().runTest(changeDrawing(1, 1, ATTRS_I1), changeDrawing(1, 1, ATTRS_I2));
        });
        it("should reduce attribute sets", () => {
            testRunner().runTest(
                changeDrawing(1, 1, ATTRS_O1), changeDrawing(1, 1, ATTRS_O2),
                changeDrawing(1, 1, ATTRS_R1), changeDrawing(1, 1, ATTRS_R2)
            );
        });
        it('should set equal operations to "removed" state', () => {
            testRunner().runTest(changeDrawing(1, 1, ATTRS), changeDrawing(1, 1, ATTRS), [], []);
            testRunner().runBidiTest(changeDrawing(1, 1, {}), changeDrawing(1, 1, ATTRS), []);
        });
    });

    // moveDrawing ------------------------------------------------------------

    describe('"moveDrawing" and "moveDrawing"', () => {
        it("should not process shapes in different sheets", () => {
            testRunner().runTest(moveDrawing(1, 0, 4), moveDrawing(0, 3, 2));
        });
        it("should process top-level shapes in same sheet", () => {
            testRunner().runTest(moveDrawing(1, 2, 4), moveDrawing(1, 3, 1), moveDrawing(1, 3, 4), moveDrawing(1, 2, 1));
            testRunner().runTest(moveDrawing(1, 1, 3), moveDrawing(1, 4, 2), moveDrawing(1, 1, 4), moveDrawing(1, 4, 1));
            testRunner().runTest(moveDrawing(1, 1, 3), moveDrawing(1, 2, 3), moveDrawing(1, 1, 2), moveDrawing(1, 1, 3));
            testRunner().runTest(moveDrawing(1, 1, 2), moveDrawing(1, 2, 4), [],                   moveDrawing(1, 1, 4));
            testRunner().runTest(moveDrawing(1, 2, 4), moveDrawing(1, 1, 2), moveDrawing(1, 1, 4), []);
            testRunner().runTest(moveDrawing(1, 1, 3), moveDrawing(1, 1, 4), moveDrawing(1, 4, 3), []);
            testRunner().runTest(moveDrawing(1, 1, 3), moveDrawing(1, 1, 3), [],                   []);
        });
        it("should process embedded shapes in same parent", () => {
            testRunner().runTest(moveDrawing(1, "5 2", "5 4"), moveDrawing(1, "5 3", "5 1"), moveDrawing(1, "5 3", "5 4"), moveDrawing(1, "5 2", "5 1"));
            testRunner().runTest(moveDrawing(1, "5 1", "5 3"), moveDrawing(1, "5 4", "5 2"), moveDrawing(1, "5 1", "5 4"), moveDrawing(1, "5 4", "5 1"));
            testRunner().runTest(moveDrawing(1, "5 1", "5 3"), moveDrawing(1, "5 2", "5 3"), moveDrawing(1, "5 1", "5 2"), moveDrawing(1, "5 1", "5 3"));
            testRunner().runTest(moveDrawing(1, "5 1", "5 2"), moveDrawing(1, "5 2", "5 4"), [],                           moveDrawing(1, "5 1", "5 4"));
            testRunner().runTest(moveDrawing(1, "5 2", "5 4"), moveDrawing(1, "5 1", "5 2"), moveDrawing(1, "5 1", "5 4"), []);
            testRunner().runTest(moveDrawing(1, "5 1", "5 3"), moveDrawing(1, "5 1", "5 4"), moveDrawing(1, "5 4", "5 3"), []);
            testRunner().runTest(moveDrawing(1, "5 1", "5 3"), moveDrawing(1, "5 1", "5 3"), [],                           []);
        });
        it("should not process embedded shapes in different parent", () => {
            testRunner().runTest(moveDrawing(1, "5 2", "5 4"), moveDrawing(1, "4 3", "4 1"));
            testRunner().runTest(moveDrawing(1, "5 1", "5 3"), moveDrawing(1, "4 4", "4 2"));
            testRunner().runTest(moveDrawing(1, "5 1", "5 3"), moveDrawing(1, "4 1", "4 4"));
            testRunner().runTest(moveDrawing(1, "5 1", "5 3"), moveDrawing(1, "4 1", "4 3"));
        });
        it("should shift embedded shape only", () => {
            testRunner().runBidiTest(moveDrawing(1, "2 2", "2 4"), moveDrawing(1, 1, 4), moveDrawing(1, "1 2", "1 4"));
            testRunner().runBidiTest(moveDrawing(1, "2 2", "2 4"), moveDrawing(1, 2, 4), moveDrawing(1, "4 2", "4 4"));
        });
    });

    describe('"moveDrawing" and other drawing operations', () => {
        it("should not process shapes in different sheets", () => {
            testRunner().runTest(moveDrawing(0, 0, 4), changeDrawingOps(1, 0, 1, 2));
        });
        it("should shift positions for top-level shape", () => {
            testRunner().runBidiTest(
                moveDrawing(1, 2, 4), changeDrawingOps(1, 1, 2, 3, 4, 5),
                moveDrawing(1, 2, 4), changeDrawingOps(1, 1, 4, 2, 3, 5)
            );
            testRunner().runBidiTest(
                moveDrawing(1, 4, 2), changeDrawingOps(1, 1, 2, 3, 4, 5),
                moveDrawing(1, 4, 2), changeDrawingOps(1, 1, 3, 4, 2, 5)
            );
            testRunner().runBidiTest(
                moveDrawing(1, 3, 3), changeDrawingOps(1, 1, 2, 3, 4, 5)
            );
        });
        it("should shift position of external embedded shape", () => {
            testRunner().runBidiTest(
                moveDrawing(1, 2, 4), changeDrawingOps(1, "1 5", "2 5", "3 5", "4 5", "5 5"),
                moveDrawing(1, 2, 4), changeDrawingOps(1, "1 5", "4 5", "2 5", "3 5", "5 5")
            );
            testRunner().runBidiTest(
                moveDrawing(1, 4, 2), changeDrawingOps(1, "1 5", "2 5", "3 5", "4 5", "5 5"),
                moveDrawing(1, 4, 2), changeDrawingOps(1, "1 5", "3 5", "4 5", "2 5", "5 5")
            );
            testRunner().runBidiTest(
                moveDrawing(1, 3, 3), changeDrawingOps(1, "1 5", "2 5", "3 5", "4 5", "5 5")
            );
        });
        it("should not shift position of parent shapes", () => {
            testRunner().runBidiTest(
                moveDrawing(1, "1 2", "1 4"), [changeDrawingOps(1, 0, 2), changeDrawing(1, 1)]
            );
        });
        it("should shift positions for embedded siblings", () => {
            testRunner().runBidiTest(
                moveDrawing(1, "1 2", "1 4"), changeDrawingOps(1, "1 1", "1 2", "1 3", "1 4", "1 5"),
                moveDrawing(1, "1 2", "1 4"), changeDrawingOps(1, "1 1", "1 4", "1 2", "1 3", "1 5")
            );
            testRunner().runBidiTest(
                moveDrawing(1, "1 4", "1 2"), changeDrawingOps(1, "1 1", "1 2", "1 3", "1 4", "1 5"),
                moveDrawing(1, "1 4", "1 2"), changeDrawingOps(1, "1 1", "1 3", "1 4", "1 2", "1 5")
            );
            testRunner().runBidiTest(
                moveDrawing(1, "1 3", "1 3"), changeDrawingOps(1, "1 1", "1 2", "1 3", "1 4", "1 5")
            );
        });
    });

    describe('"moveDrawing" and "sheetSelection"', () => {
        it("should not process shapes in different sheets", () => {
            testRunner().runTest(moveDrawing(0, 0, 4), sheetSelection(1, "A1", 0, "A1", ["0", "1", "2"]));
        });
        it("should shift positions for top-level shape", () => {
            testRunner().runBidiTest(
                moveDrawing(1, 2, 4), sheetSelection(1, "A1", 0, "A1", ["1", "2", "3", "4", "5"]),
                moveDrawing(1, 2, 4), sheetSelection(1, "A1", 0, "A1", ["1", "4", "2", "3", "5"])
            );
            testRunner().runBidiTest(
                moveDrawing(1, 4, 2), sheetSelection(1, "A1", 0, "A1", ["1", "2", "3", "4", "5"]),
                moveDrawing(1, 4, 2), sheetSelection(1, "A1", 0, "A1", ["1", "3", "4", "2", "5"])
            );
            testRunner().runBidiTest(
                moveDrawing(1, 3, 3), sheetSelection(1, "A1", 0, "A1", ["1", "2", "3", "4", "5"])
            );
        });
        it("should shift position of external embedded shape", () => {
            testRunner().runBidiTest(
                moveDrawing(1, 2, 4), sheetSelection(1, "A1", 0, "A1", ["1 5", "2 5", "3 5", "4 5", "5 5"]),
                moveDrawing(1, 2, 4), sheetSelection(1, "A1", 0, "A1", ["1 5", "4 5", "2 5", "3 5", "5 5"])
            );
            testRunner().runBidiTest(
                moveDrawing(1, 4, 2), sheetSelection(1, "A1", 0, "A1", ["1 5", "2 5", "3 5", "4 5", "5 5"]),
                moveDrawing(1, 4, 2), sheetSelection(1, "A1", 0, "A1", ["1 5", "3 5", "4 5", "2 5", "5 5"])
            );
            testRunner().runBidiTest(
                moveDrawing(1, 3, 3), sheetSelection(1, "A1", 0, "A1", ["1 5", "2 5", "3 5", "4 5", "5 5"])
            );
        });
        it("should not shift position of parent shapes", () => {
            testRunner().runBidiTest(moveDrawing(1, "1 2", "1 4"), sheetSelection(1, "A1", 0, "A1", ["0", "1", "2"]));
        });
        it("should shift positions for embedded siblings", () => {
            testRunner().runBidiTest(
                moveDrawing(1, "1 2", "1 4"), sheetSelection(1, "A1", 0, "A1", ["1 1", "1 2", "1 3", "1 4", "1 5"]),
                moveDrawing(1, "1 2", "1 4"), sheetSelection(1, "A1", 0, "A1", ["1 1", "1 4", "1 2", "1 3", "1 5"])
            );
            testRunner().runBidiTest(
                moveDrawing(1, "1 4", "1 2"), sheetSelection(1, "A1", 0, "A1", ["1 1", "1 2", "1 3", "1 4", "1 5"]),
                moveDrawing(1, "1 4", "1 2"), sheetSelection(1, "A1", 0, "A1", ["1 1", "1 3", "1 4", "1 2", "1 5"])
            );
            testRunner().runBidiTest(
                moveDrawing(1, "1 3", "1 3"), sheetSelection(1, "A1", 0, "A1", ["1 1", "1 2", "1 3", "1 4", "1 5"])
            );
        });
    });

    // charts -----------------------------------------------------------------

    describe("chart operations and independent operations", () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(chartOps(1), [GLOBAL_OPS, STYLESHEET_OPS, positionOp(1, 0), selectOps(1)]);
        });
    });

    describe("chart operations and drawing text operations", () => {
        it("should skip independent sheets", () => {
            testRunner().runBidiTest(chartOps(1), drawingTextOps(0));
        });
        it("should skip independent shapes", () => {
            testRunner().runBidiTest(insertChSeries(1, 0, 0, ATTRS), insertText(1, "1 6 7", "abc"));
            testRunner().runBidiTest(changeChAxis(1, 0, 0, ATTRS), deleteOp(1, "1 6 7"));
        });
        it("should fail for embedded shapes", () => {
            testRunner().expectBidiError(insertChSeries(1, 0, 0, ATTRS), insertText(1, "0 6 7", "abc"));
            testRunner().expectBidiError(changeChAxis(1, 0, 0, ATTRS), deleteOp(1, "0 6 7"));
        });
    });

    // position ---------------------------------------------------------------

    describe('"position" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(positionOp(1, 0), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS, selectOps(1)]);
        });
    });

    describe('"position" and drawing text operations', () => {
        // text framework implements "position" as external operation oly
        it("should skip independent sheets", () => {
            testRunner().runTest(insertText(2, "0 1 0", "abc"), positionOp(1, 0));
        });
        it("should transform position", () => {
            testRunner().runTest(insertText(1, "0 1 0", "abc"), positionOp(1, "0 1 2"), null, positionOp(1, "0 1 5"));
        });
    });
});
