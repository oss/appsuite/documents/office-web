/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import {
    insertName, deleteName, changeName, insertTable, deleteTable, changeTable, changeTableCol
} from "~/spreadsheet/apphelper";

import {
    ATTRS, ATTRS2, ATTRS_I1, ATTRS_I2, ATTRS_O1, ATTRS_O2, ATTRS_R1, ATTRS_R2,
    GLOBAL_OPS, STYLESHEET_OPS,
    createTestRunner,
    tableOps, dvRuleOps, cfRuleOps,
    noteOps, commentOps, drawingOps, chartOps, drawingTextOps, selectOps
} from "~/spreadsheet/othelper";

// tests ======================================================================

describe("module io.ox/office/spreadsheet/model/operation/otmanager", () => {

    // initialize OT test runners
    const testRunner = createTestRunner();

    // defined names ----------------------------------------------------------

    describe("defined names and independent operations", () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest([insertName("g"), deleteName("g")], [
                GLOBAL_OPS, STYLESHEET_OPS,
                tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    // insertName -------------------------------------------------------------

    describe('"insertName" and "insertName"', () => {
        it("should skip different names", () => {
            testRunner().runTest(insertName("n1"),    insertName("n2"));
            testRunner().runTest(insertName("n1"),    insertName("n1", 0));
            testRunner().runTest(insertName("n1", 0), insertName("n1"));
            testRunner().runTest(insertName("n1", 0), insertName("n1", 1));
            testRunner().runTest(insertName("n1", 0), insertName("n2", 0));
        });
        it("should fail to insert same name twice", () => {
            testRunner().expectError(insertName("n1"),    insertName("n1"));
            testRunner().expectError(insertName("n1", 0), insertName("n1", 0));
            testRunner().expectError(insertName("n1"),    insertName("N1"));
            testRunner().expectError(insertName("n1", 0), insertName("N1", 0));
        });
    });

    describe('"insertName" and "deleteName"', () => {
        it("should skip different names", () => {
            testRunner().runBidiTest(insertName("n1"),    deleteName("n2"));
            testRunner().runBidiTest(insertName("n1"),    deleteName("n1", 0));
            testRunner().runBidiTest(insertName("n1", 0), deleteName("n1"));
            testRunner().runBidiTest(insertName("n1", 0), deleteName("n1", 1));
            testRunner().runBidiTest(insertName("n1", 0), deleteName("n2", 0));
        });
        it("should handle same name", () => {
            testRunner().runBidiTest(insertName("n1"),    deleteName("n1"),    []);
            testRunner().runBidiTest(insertName("n1", 0), deleteName("n1", 0), []);
            testRunner().runBidiTest(insertName("n1"),    deleteName("N1"),    []);
            testRunner().runBidiTest(insertName("n1", 0), deleteName("N1", 0), []);
        });
    });

    describe('"insertName" and "changeName"', () => {
        it("should skip different names", () => {
            testRunner().runBidiTest(insertName("n1"),    changeName("n2"));
            testRunner().runBidiTest(insertName("n1"),    changeName("n1", 0));
            testRunner().runBidiTest(insertName("n1"),    changeName("n2", 0, { newLabel: "n1" }));
            testRunner().runBidiTest(insertName("n1", 0), changeName("n1"));
            testRunner().runBidiTest(insertName("n1", 0), changeName("n1", 1));
            testRunner().runBidiTest(insertName("n1", 0), changeName("n2", 0));
            testRunner().runBidiTest(insertName("n1", 0), changeName("n2", { newLabel: "n1" }));
            testRunner().runBidiTest(insertName("n1", 0), changeName("n2", 1, { newLabel: "n1" }));
        });
        it("should fail to insert same name again", () => {
            testRunner().expectBidiError(insertName("n1"),    changeName("n1"));
            testRunner().expectBidiError(insertName("n1", 0), changeName("n1", 0));
            testRunner().expectBidiError(insertName("n1"),    changeName("N1"));
            testRunner().expectBidiError(insertName("n1", 0), changeName("N1", 0));
        });
        it("should fail to insert and change name to same label", () => {
            testRunner().expectBidiError(insertName("n1"),    changeName("n2", { newLabel: "n1" }));
            testRunner().expectBidiError(insertName("n1", 0), changeName("n2", 0, { newLabel: "n1" }));
            testRunner().expectBidiError(insertName("n1"),    changeName("n2", { newLabel: "N1" }));
            testRunner().expectBidiError(insertName("n1", 0), changeName("n2", 0, { newLabel: "N1" }));
        });
    });

    describe('"insertName" and "insertTable"', () => {
        it("should skip different names", () => {
            testRunner().runBidiTest(insertName("n1"),    insertTable(0, null, "A1:D4"));
            testRunner().runBidiTest(insertName("n1"),    insertTable(0, "n2", "A1:D4"));
            testRunner().runBidiTest(insertName("n1", 0), insertTable(0, "n2", "A1:D4"));
            testRunner().runBidiTest(insertName("n1", 1), insertTable(0, "n2", "A1:D4"));
        });
        it("should fail to insert name and table with same label", () => {
            testRunner().expectBidiError(insertName("n1"),    insertTable(0, "n1", "A1:D4"));
            testRunner().expectBidiError(insertName("n1", 0), insertTable(0, "n1", "A1:D4"));
            testRunner().expectBidiError(insertName("n1", 1), insertTable(0, "n1", "A1:D4"));
            testRunner().expectBidiError(insertName("N1", 1), insertTable(0, "n1", "A1:D4"));
        });
    });

    describe('"insertName" and "changeTable"', () => {
        it("should skip different labels", () => {
            testRunner().runBidiTest(insertName("n1"),    changeTable(1, null,     "A1:D4"));
            testRunner().runBidiTest(insertName("n1"),    changeTable(1, "Table1", "A1:D4"));
            testRunner().runBidiTest(insertName("n1", 0), changeTable(1, "Table1", "A1:D4"));
            testRunner().runBidiTest(insertName("n1", 1), changeTable(1, "Table1", "A1:D4"));
        });
        it("should fail to insert name for existing table", () => {
            testRunner().expectBidiError(insertName("table1"),    changeTable(1, "Table1", "A1:D4"));
            testRunner().expectBidiError(insertName("table1", 0), changeTable(1, "Table1", "A1:D4"));
            testRunner().expectBidiError(insertName("table1", 1), changeTable(1, "Table1", "A1:D4"));
        });
        it("should fail to insert name and change table to same label", () => {
            testRunner().expectBidiError(insertName("table3"),    changeTable(1, "Table1", { newName: "Table3" }));
            testRunner().expectBidiError(insertName("table3", 0), changeTable(1, "Table1", { newName: "Table3" }));
            testRunner().expectBidiError(insertName("table3", 1), changeTable(1, "Table1", { newName: "Table3" }));
        });
    });

    // deleteName -------------------------------------------------------------

    describe('"deleteName" and "deleteName"', () => {
        it("should skip different names", () => {
            testRunner().runTest(deleteName("n1"),    deleteName("n2"));
            testRunner().runTest(deleteName("n1"),    deleteName("n1", 0));
            testRunner().runTest(deleteName("n1", 0), deleteName("n1"));
            testRunner().runTest(deleteName("n1", 0), deleteName("n1", 1));
            testRunner().runTest(deleteName("n1", 0), deleteName("n2", 0));
        });
        it("should handle same name", () => {
            testRunner().runTest(deleteName("n1"),    deleteName("n1"),    [], []);
            testRunner().runTest(deleteName("n1", 0), deleteName("n1", 0), [], []);
            testRunner().runTest(deleteName("n1", 0), deleteName("N1", 0), [], []);
        });
    });

    describe('"deleteName" and "changeName"', () => {
        it("should skip different names", () => {
            testRunner().runBidiTest(deleteName("n1"),    changeName("n2"));
            testRunner().runBidiTest(deleteName("n1"),    changeName("n1", 0));
            testRunner().runBidiTest(deleteName("n1", 0), changeName("n1"));
            testRunner().runBidiTest(deleteName("n1", 0), changeName("n1", 1));
            testRunner().runBidiTest(deleteName("n1", 0), changeName("n2", 0));
        });
        it("should handle same name", () => {
            testRunner().runBidiTest(deleteName("n1"),    changeName("n1"),    null, []);
            testRunner().runBidiTest(deleteName("n1", 0), changeName("n1", 0), null, []);
            testRunner().runBidiTest(deleteName("n1", 0), changeName("N1", 0), null, []);
        });
        it("should update new label in operations", () => {
            testRunner().runBidiTest(deleteName("n1"), changeName("n1", { newLabel: "n2" }), deleteName("n2"), []);
        });
    });

    // changeName -------------------------------------------------------------

    describe('"changeName" and "changeName"', () => {
        it("should skip different names", () => {
            testRunner().runTest(changeName("n1", { formula: "1" }), changeName("n2", { formula: "1" }));
            testRunner().runTest(changeName("n1", 0, { formula: "1" }), changeName("n1", { formula: "1" }));
            testRunner().runTest(changeName("n1", 0, { formula: "1" }), changeName("n1", 1, { formula: "1" }));
        });
        it("should reduce formula expression", () => {
            testRunner().runTest(changeName("n1", { formula: "A1", ref: "A1", isExpr: true }), changeName("n1", { formula: "A2", ref: "A1", isExpr: true }), null, []);
            testRunner().runTest(changeName("n1", { formula: "A1", ref: "A1", isExpr: true }), changeName("n1", { formula: "A1", ref: "A1", isExpr: true }), [], []);
            testRunner().runTest(changeName("n1", { formula: "A1", ref: "A1", isExpr: true }), changeName("N1", { formula: "A1", ref: "A1", isExpr: true }), [], []);
        });
        it("should fail to change different names in same parent to the same label", () => {
            testRunner().expectError(changeName("n1", { newLabel: "n3" }), changeName("n2", { newLabel: "n3" }));
            testRunner().expectError(changeName("n1", { newLabel: "n3" }), changeName("n2", { newLabel: "N3" }));
            testRunner().expectError(changeName("n1", 1, { newLabel: "n3" }), changeName("n2", 1, { newLabel: "n3" }));
            testRunner().expectError(changeName("n1", 1, { newLabel: "n3" }), changeName("n2", 1, { newLabel: "N3" }));
        });
        it("should allow to change names in different parents to the same label", () => {
            testRunner().runTest(changeName("n1", { newLabel: "n3" }), changeName("n1", 1, { newLabel: "n3" }));
            testRunner().runTest(changeName("n1", 1, { newLabel: "n3" }), changeName("n1", 2, { newLabel: "n3" }));
        });
        it("should update label in operations", () => {
            testRunner().runBidiTest(
                changeName("n1", { newLabel: "n2" }), changeName("n1", { formula: "" }),
                changeName("n1", { newLabel: "n2" }), changeName("n2", { formula: "" })
            );
            testRunner().runTest(
                changeName("n1", { newLabel: "n2" }), changeName("n1", { newLabel: "n3", formula: "" }),
                changeName("n3", { newLabel: "n2" }), changeName("n2", { formula: "" })
            );
            testRunner().runTest(
                changeName("n1", { newLabel: "n2" }), changeName("n1", { newLabel: "n3" }),
                changeName("n3", { newLabel: "n2" }), []
            );
        });
    });

    describe('"changeName" and "insertTable"', () => {
        it("should skip different names", () => {
            testRunner().runBidiTest(changeName("n1"),    insertTable(0, null, "A1:D4"));
            testRunner().runBidiTest(changeName("n1"),    insertTable(0, "n2", "A1:D4"));
            testRunner().runBidiTest(changeName("n1", 0), insertTable(0, "n2", "A1:D4"));
            testRunner().runBidiTest(changeName("n1", 1), insertTable(0, "n2", "A1:D4"));
        });
        it("should fail to insert table for existing name", () => {
            testRunner().expectBidiError(changeName("n1"),    insertTable(0, "n1", "A1:D4"));
            testRunner().expectBidiError(changeName("n1", 0), insertTable(0, "n1", "A1:D4"));
            testRunner().expectBidiError(changeName("n1", 1), insertTable(0, "n1", "A1:D4"));
            testRunner().expectBidiError(changeName("N1", 1), insertTable(0, "n1", "A1:D4"));
        });
        it("should fail to insert table and give a name the same label", () => {
            testRunner().expectBidiError(changeName("n2", { newLabel: "n1" }),    insertTable(0, "n1", "A1:D4"));
            testRunner().expectBidiError(changeName("n2", 0, { newLabel: "n1" }), insertTable(0, "n1", "A1:D4"));
            testRunner().expectBidiError(changeName("n2", 1, { newLabel: "n1" }), insertTable(0, "n1", "A1:D4"));
            testRunner().expectBidiError(changeName("n2", 1, { newLabel: "N1" }), insertTable(0, "n1", "A1:D4"));
        });
    });

    describe('"changeName" and "changeTable"', () => {
        it("should skip different names", () => {
            testRunner().runBidiTest(changeName("n1"),    changeTable(1, null,     "A1:D4"));
            testRunner().runBidiTest(changeName("n1"),    changeTable(1, "Table1", "A1:D4"));
            testRunner().runBidiTest(changeName("n1", 0), changeTable(1, "Table1", "A1:D4"));
            testRunner().runBidiTest(changeName("n1", 1), changeTable(1, "Table1", "A1:D4"));
        });
        it("should fail to change table name for existing name", () => {
            testRunner().expectBidiError(changeName("table3"),    changeTable(1, "Table1", { newName: "Table3" }));
            testRunner().expectBidiError(changeName("table3", 0), changeTable(1, "Table1", { newName: "Table3" }));
            testRunner().expectBidiError(changeName("table3", 1), changeTable(1, "Table1", { newName: "Table3" }));
        });
        it("should fail to change name label for existing table", () => {
            testRunner().expectBidiError(changeName("table2",    { newLabel: "table1" }), changeTable(1, "Table1", "A1:D4"));
            testRunner().expectBidiError(changeName("table2", 0, { newLabel: "table1" }), changeTable(1, "Table1", "A1:D4"));
            testRunner().expectBidiError(changeName("table2", 1, { newLabel: "table1" }), changeTable(1, "Table1", "A1:D4"));
        });
        it("should fail to give table and name the same label", () => {
            testRunner().expectBidiError(changeName("n2",    { newLabel: "table3" }), changeTable(1, "Table1", { newName: "Table3" }));
            testRunner().expectBidiError(changeName("n2", 0, { newLabel: "table3" }), changeTable(1, "Table1", { newName: "Table3" }));
            testRunner().expectBidiError(changeName("n2", 1, { newLabel: "table3" }), changeTable(1, "Table1", { newName: "Table3" }));
        });
    });

    // table ranges -----------------------------------------------------------

    describe("table ranges and independent operations", () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(tableOps(1), [
                GLOBAL_OPS, STYLESHEET_OPS, dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    // insertTable ------------------------------------------------------------

    describe('"insertTable" and "insertTable"', () => {
        it("should fail to insert table name twice", () => {
            testRunner().expectError(insertTable(1, null,     "A1:D4"), insertTable(1, null,     "E5:H8"));
            testRunner().expectError(insertTable(1, "Table3", "A1:D4"), insertTable(1, "Table3", "E5:H8"));
            testRunner().expectError(insertTable(1, "Table3", "A1:D4"), insertTable(2, "Table3", "E5:H8"));
        });
        it("should fail to insert overlapping tables", () => {
            testRunner().expectError(insertTable(1, null,     "A1:D4"), insertTable(1, "Table4", "D4:G7"));
            testRunner().expectError(insertTable(1, "Table3", "A1:D4"), insertTable(1, null,     "D4:G7"));
            testRunner().expectError(insertTable(1, "Table3", "A1:D4"), insertTable(1, "Table4", "D4:G7"));
        });
        it("should ignore overlapping table ranges on different sheets", () => {
            testRunner().runTest(insertTable(1, null,     "A1:D4"), insertTable(2, null,     "A1:D4"));
            testRunner().runTest(insertTable(1, null,     "A1:D4"), insertTable(2, "Table4", "A1:D4"));
            testRunner().runTest(insertTable(1, "Table3", "A1:D4"), insertTable(2, null,     "A1:D4"));
            testRunner().runTest(insertTable(1, "Table3", "A1:D4"), insertTable(2, "Table4", "A1:D4"));
        });
    });

    describe('"insertTable" and "deleteTable"', () => {
        it("should skip different tables", () => {
            testRunner().runBidiTest(insertTable(1, null,     "A1:D4"), deleteTable(1, "Table2"));
            testRunner().runBidiTest(insertTable(1, "Table3", "A1:D4"), deleteTable(1, null));
            testRunner().runBidiTest(insertTable(1, "Table3", "A1:D4"), deleteTable(1, "Table2"));
            testRunner().runBidiTest(insertTable(1, null,     "A1:D4"), deleteTable(2, null));
        });
        it("should fail for insert existing table", () => {
            testRunner().expectBidiError(insertTable(1, "Table1", "A1:D4"), deleteTable(2, "Table1"));
            testRunner().expectBidiError(insertTable(1, "Table1", "A1:D4"), deleteTable(1, "Table1"));
            testRunner().expectBidiError(insertTable(1, "Table1", "A1:D4"), deleteTable(1, "table1"));
        });
    });

    describe('"insertTable" and "changeTable"', () => {
        it("should skip different tables", () => {
            testRunner().runBidiTest(insertTable(1, null,     "A1:D4"), changeTable(1, "Table1", "A1:D4"));
            testRunner().runBidiTest(insertTable(1, "Table3", "A1:D4"), changeTable(1, null,     "A1:D4"));
            testRunner().runBidiTest(insertTable(1, "Table3", "A1:D4"), changeTable(1, "Table1", "A1:D4"));
            testRunner().runBidiTest(insertTable(1, null,     "A1:D4"), changeTable(2, null,     "A1:D4"));
        });
        it("should fail to insert table with used name", () => {
            testRunner().expectBidiError(insertTable(1, null,     "A1:D4"), changeTable(1, null,     "A1:D4"));
            testRunner().expectBidiError(insertTable(1, "Table1", "A1:D4"), changeTable(1, "Table1", "A1:D4"));
            testRunner().expectBidiError(insertTable(2, "Table1", "A1:D4"), changeTable(1, "Table1", "A1:D4"));
            testRunner().expectBidiError(insertTable(2, "table1", "A1:D4"), changeTable(1, "Table1", "A1:D4"));
        });
        it("should fail to insert table and change table to same name", () => {
            testRunner().expectBidiError(insertTable(1, "Table3", "A1:D4"), changeTable(1, "Table1", { newName: "Table3" }));
            testRunner().expectBidiError(insertTable(2, "Table3", "A1:D4"), changeTable(1, "Table1", { newName: "Table3" }));
            testRunner().expectBidiError(insertTable(2, "table3", "A1:D4"), changeTable(1, "Table1", { newName: "Table3" }));
        });
    });

    describe('"insertTable" and "changeTableColumn"', () => {
        it("should skip different tables", () => {
            testRunner().runBidiTest(insertTable(1, null,     "A1:D4"), changeTableCol(1, "Table2", 0, { attrs: ATTRS }));
            testRunner().runBidiTest(insertTable(1, "Table3", "A1:D4"), changeTableCol(1, null,     0, { attrs: ATTRS }));
            testRunner().runBidiTest(insertTable(1, "Table3", "A1:D4"), changeTableCol(1, "Table2", 0, { attrs: ATTRS }));
            testRunner().runBidiTest(insertTable(1, null,     "A1:D4"), changeTableCol(2, null,     0, { attrs: ATTRS }));
        });
        it("should fail to insert table with used name", () => {
            testRunner().expectBidiError(insertTable(1, null,     "A1:D4"), changeTableCol(1, null,     0, { attrs: ATTRS }));
            testRunner().expectBidiError(insertTable(1, "Table1", "A1:D4"), changeTableCol(1, "Table1", 0, { attrs: ATTRS }));
            testRunner().expectBidiError(insertTable(2, "Table1", "A1:D4"), changeTableCol(1, "Table1", 0, { attrs: ATTRS }));
            testRunner().expectBidiError(insertTable(2, "table1", "A1:D4"), changeTableCol(1, "Table1", 0, { attrs: ATTRS }));
        });
    });

    // deleteTable ------------------------------------------------------------

    describe('"deleteTable" and "deleteTable"', () => {
        it("should skip different tables", () => {
            testRunner().runTest(deleteTable(1, null),     deleteTable(1, "Table2"));
            testRunner().runTest(deleteTable(1, "Table1"), deleteTable(1, null));
            testRunner().runTest(deleteTable(1, "Table1"), deleteTable(1, "Table2"));
            testRunner().runTest(deleteTable(1, null),     deleteTable(2, null));
        });
        it("should fail for same table in different sheets", () => {
            testRunner().expectError(deleteTable(1, "Table1"), deleteTable(2, "Table1"));
            testRunner().expectError(deleteTable(1, "Table1"), deleteTable(2, "table1"));
        });
        it("should handle same table", () => {
            testRunner().runTest(deleteTable(1, null),     deleteTable(1, null),     [], []);
            testRunner().runTest(deleteTable(1, "Table1"), deleteTable(1, "Table1"), [], []);
            testRunner().runTest(deleteTable(1, "Table1"), deleteTable(1, "table1"), [], []);
        });
    });

    describe('"deleteTable" and "changeTable"', () => {
        it("should skip different tables", () => {
            testRunner().runBidiTest(deleteTable(1, null),     changeTable(1, "Table1", "A1:D4"));
            testRunner().runBidiTest(deleteTable(1, "Table3"), changeTable(1, null,     "A1:D4"));
            testRunner().runBidiTest(deleteTable(1, "Table3"), changeTable(1, "Table1", "A1:D4"));
            testRunner().runBidiTest(deleteTable(1, null),     changeTable(2, null,     "A1:D4"));
        });
        it("should fail for same table in different sheets", () => {
            testRunner().expectBidiError(deleteTable(1, "Table1"), changeTable(2, "Table1", "A1:D4"));
        });
        it("should handle same table", () => {
            testRunner().runBidiTest(deleteTable(1, null),     changeTable(1, null,     "A1:D4"), null, []);
            testRunner().runBidiTest(deleteTable(1, "Table1"), changeTable(1, "Table1", "A1:D4"), null, []);
            testRunner().runBidiTest(deleteTable(1, "Table1"), changeTable(1, "table1", "A1:D4"), null, []);
        });
        it("should update table name in operations", () => {
            testRunner().runBidiTest(deleteTable(1, "Table1"), changeTable(1, "Table1", { newName: "Table3" }), deleteTable(1, "Table3"), []);
        });
    });

    describe('"deleteTable" and "changeTableColumn"', () => {
        it("should skip different tables", () => {
            testRunner().runBidiTest(deleteTable(1, null),     changeTableCol(1, "Table2", 0, { attrs: ATTRS }));
            testRunner().runBidiTest(deleteTable(1, "Table1"), changeTableCol(1, null,     0, { attrs: ATTRS }));
            testRunner().runBidiTest(deleteTable(1, "Table1"), changeTableCol(1, "Table2", 0, { attrs: ATTRS }));
            testRunner().runBidiTest(deleteTable(1, null),     changeTableCol(2, null,     0, { attrs: ATTRS }));
        });
        it("should fail for same table in different sheets", () => {
            testRunner().expectBidiError(deleteTable(1, "Table1"), changeTableCol(2, "Table1", 0, { attrs: ATTRS }));
        });
        it("should handle same table", () => {
            testRunner().runBidiTest(deleteTable(1, null),     changeTableCol(1, null,     0, { attrs: ATTRS }), null, []);
            testRunner().runBidiTest(deleteTable(1, "Table1"), changeTableCol(1, "Table1", 0, { attrs: ATTRS }), null, []);
            testRunner().runBidiTest(deleteTable(1, "Table1"), changeTableCol(1, "table1", 0, { attrs: ATTRS }), null, []);
        });
    });

    // changeTable ------------------------------------------------------------

    describe('"changeTable" and "changeTable"', () => {
        it("should skip different tables", () => {
            testRunner().runTest(changeTable(1, null,     "A1:D4"), changeTable(1, "TableX", "E5:H8"));
            testRunner().runTest(changeTable(1, "Table1", "A1:D4"), changeTable(1, null,     "E5:H8"));
            testRunner().runTest(changeTable(1, "Table1", "A1:D4"), changeTable(1, "TableX", "E5:H8"));
            testRunner().runTest(changeTable(1, null,     "A1:D4"), changeTable(2, null,     "E5:H8"));
        });
        it("should fail for same table in different sheets", () => {
            testRunner().expectError(changeTable(1, "Table1", "A1:D4"), changeTable(2, "Table1", "A1:D4"));
        });
        it("should not process independent attributes", () => {
            testRunner().runTest(changeTable(1, "Table1", { attrs: ATTRS_I1 }), changeTable(1, "Table1", { attrs: ATTRS_I2 }));
        });
        it("should reduce attribute sets", () => {
            testRunner().runTest(
                changeTable(1, "Table1", { attrs: ATTRS_O1 }), changeTable(1, "Table1", { attrs: ATTRS_O2 }),
                changeTable(1, "Table1", { attrs: ATTRS_R1 }), changeTable(1, "Table1", { attrs: ATTRS_R2 })
            );
        });
        it("should reduce table range", () => {
            testRunner().runTest(
                changeTable(1, "Table1", "A1:D4", { attrs: ATTRS_I1 }), changeTable(1, "Table1", "A1:D4", { attrs: ATTRS_I2 }),
                changeTable(1, "Table1",          { attrs: ATTRS_I1 }), changeTable(1, "Table1",          { attrs: ATTRS_I2 })
            );
            testRunner().runTest(
                changeTable(1, "Table1", "A1:D4", { attrs: ATTRS_I1 }), changeTable(1, "Table1", "B2:E5", { attrs: ATTRS_I2 }),
                changeTable(1, "Table1", "A1:D4", { attrs: ATTRS_I1 }), changeTable(1, "Table1",          { attrs: ATTRS_I2 })
            );
            testRunner().runBidiTest(changeTable(1, "Table1", "A1:D4", { attrs: ATTRS_I1 }), changeTable(1, "Table1", { attrs: ATTRS_I2 }));
        });
        it("should fail to change different tables to the same name", () => {
            testRunner().runTest(changeTable(1, "Table1", { newName: "Table3" }), changeTable(1, "Table1", { newName: "Table3" }), [], []);
            testRunner().runTest(changeTable(1, "Table1", { newName: "Table3" }), changeTable(2, "Table2", { newName: "Table4" }));
            testRunner().expectError(changeTable(1, "Table1", { newName: "Table3" }), changeTable(2, "Table2", { newName: "Table3" }));
            testRunner().expectError(changeTable(1, "Table1", { newName: "Table3" }), changeTable(2, "Table2", { newName: "table3" }));
        });
        it("should update table name in operations", () => {
            testRunner().runBidiTest(
                changeTable(1, "Table1", { newName: "Table2" }), changeTable(1, "Table1", "A1:D4"),
                changeTable(1, "Table1", { newName: "Table2" }), changeTable(1, "Table2", "A1:D4")
            );
            testRunner().runTest(
                changeTable(1, "Table1", { newName: "Table2" }), changeTable(1, "Table1", "A1:D4", { newName: "Table3" }),
                changeTable(1, "Table3", { newName: "Table2" }), changeTable(1, "Table2", "A1:D4")
            );
            testRunner().runTest(
                changeTable(1, "Table1", { newName: "Table2" }), changeTable(1, "Table1", { newName: "Table3" }),
                changeTable(1, "Table3", { newName: "Table2" }), []
            );
        });
        it("should remove entire operation if nothing will change", () => {
            testRunner().runTest(changeTable(1, "Table1", "A1:D4", { attrs: ATTRS }), changeTable(1, "Table1", "B2:E5", { attrs: ATTRS2 }), null,                             []);
            testRunner().runTest(changeTable(1, "Table1", "A1:D4", { attrs: ATTRS }), changeTable(1, "Table1", "B2:E5", { attrs: ATTRS }), changeTable(1, "Table1", "A1:D4"), []);
            testRunner().runTest(changeTable(1, "Table1", "A1:D4", { attrs: ATTRS }), changeTable(1, "Table1", "A1:D4", { attrs: ATTRS }), [],                                []);
        });
    });

    describe('"changeTable" and "changeTableColumn"', () => {
        it("should skip different tables", () => {
            testRunner().runBidiTest(changeTable(1, null,     "A1:D4"), changeTableCol(1, "Table2", 0, { attrs: ATTRS }));
            testRunner().runBidiTest(changeTable(1, "Table1", "A1:D4"), changeTableCol(1, null,     0, { attrs: ATTRS }));
            testRunner().runBidiTest(changeTable(1, "Table1", "A1:D4"), changeTableCol(1, "Table2", 0, { attrs: ATTRS }));
            testRunner().runBidiTest(changeTable(1, null,     "A1:D4"), changeTableCol(2, null,     0, { attrs: ATTRS }));
        });
        it("should fail to change table range", () => {
            testRunner().expectBidiError(changeTable(1, null, "A1:D4"), changeTableCol(1, null, 0, { attrs: ATTRS }));
            testRunner().expectBidiError(changeTable(1, "Table1", "A1:D4"), changeTableCol(1, "Table1", 0, { attrs: ATTRS }));
        });
        it("should fail to change table name to used name", () => {
            testRunner().expectBidiError(changeTable(1, "Table2", { newName: "Table1" }), changeTableCol(0, "Table1", 0, { attrs: ATTRS }));
            testRunner().expectBidiError(changeTable(1, "Table2", { newName: "table1" }), changeTableCol(0, "Table1", 0, { attrs: ATTRS }));
        });
        it("should update table name in operations", () => {
            testRunner().runBidiTest(changeTable(1, "Table1", { newName: "Table3" }), changeTableCol(1, "Table1", 0, { attrs: ATTRS }), null, changeTableCol(1, "Table3", 0, { attrs: ATTRS }));
        });
    });

    // changeTableColumn ------------------------------------------------------

    describe('"changeTableCol" and "changeTableCol"', () => {
        it("should skip different tables or columns", () => {
            testRunner().runTest(changeTableCol(1, null,     0, { attrs: ATTRS }), changeTableCol(1, "Table2", 0, { attrs: ATTRS }));
            testRunner().runTest(changeTableCol(1, "Table1", 0, { attrs: ATTRS }), changeTableCol(1, null,     0, { attrs: ATTRS }));
            testRunner().runTest(changeTableCol(1, "Table1", 0, { attrs: ATTRS }), changeTableCol(1, "Table2", 0, { attrs: ATTRS }));
            testRunner().runTest(changeTableCol(1, "Table1", 0, { attrs: ATTRS }), changeTableCol(1, "Table1", 1, { attrs: ATTRS }));
            testRunner().runTest(changeTableCol(1, null,     0, { attrs: ATTRS }), changeTableCol(2, null,     0, { attrs: ATTRS }));
        });
        it("should fail for same table in different sheets", () => {
            testRunner().expectError(changeTableCol(1, "Table1", 0, { attrs: ATTRS }), changeTableCol(2, "Table1", 0, { attrs: ATTRS }));
        });
        it("should not process independent attributes", () => {
            testRunner().runTest(changeTableCol(1, "Table1", 0, { attrs: ATTRS_I1 }), changeTableCol(1, "Table1", 0, { attrs: ATTRS_I2 }));
        });
        it("should reduce attribute sets", () => {
            testRunner().runTest(
                changeTableCol(1, "Table1", 0, { attrs: ATTRS_O1 }), changeTableCol(1, "Table1", 0, { attrs: ATTRS_O2 }),
                changeTableCol(1, "Table1", 0, { attrs: ATTRS_R1 }), changeTableCol(1, "Table1", 0, { attrs: ATTRS_R2 })
            );
        });
        it("should fail to transform multi-column sort", () => {
            testRunner().expectBidiError(changeTableCol(1, "Table1", 0, { attrs: ATTRS_I1 }), changeTableCol(1, "Table1", 0, { attrs: ATTRS_I2, sort: true }));
        });
        it("should remove entire operation if nothing will change", () => {
            testRunner().runTest(changeTableCol(1, "Table1", 0, { attrs: ATTRS }), changeTableCol(1, "Table1", 0, { attrs: ATTRS2 }), null, []);
            testRunner().runTest(changeTableCol(1, "Table1", 0, { attrs: ATTRS }), changeTableCol(1, "Table1", 0, { attrs: ATTRS }), [], []);
        });
    });
});
