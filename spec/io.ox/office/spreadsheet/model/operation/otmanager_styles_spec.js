/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import {
    insertAutoStyle, deleteAutoStyle, insertNumFmt, deleteNumFmt,
    insertCols, changeCols, insertRows, changeRows, changeCells
} from "~/spreadsheet/apphelper";

import {
    GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS, SHEETCOLL_OPS,
    createTestRunner,
    colOps, rowOps, cellOps, hlinkOps, nameOps, tableOps, dvRuleOps, cfRuleOps,
    noteOps, commentOps, drawingOps, chartOps, drawingTextOps, selectOps
} from "~/spreadsheet/othelper";

// tests ======================================================================

describe("module io.ox/office/spreadsheet/model/operation/otmanager", () => {

    // initialize OT test runners
    const testRunner = createTestRunner();

    // auto-styles ------------------------------------------------------------

    describe("auto-styles and independent operations", () => {
        it("should skip the transformations", () => {
            testRunner().runBidiTest(AUTOSTYLE_OPS, [
                GLOBAL_OPS, STYLESHEET_OPS, SHEETCOLL_OPS, colOps(1), rowOps(1),
                cellOps(1), hlinkOps(1), nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    // insertAutoStyle --------------------------------------------------------

    describe('"insertAutoStyle" and "insertColumns"', () => {
        const OTIS = { otIndexShift: true };
        it("should transform the auto-style identifier", () => {
            testRunner().runBidiTest(insertAutoStyle("a2", {}), insertCols(1, "A", "a0"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), insertCols(1, "A", "a1"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), insertCols(1, "A", "a2"), insertAutoStyle("a2", {}, OTIS), insertCols(1, "A", "a3"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), insertCols(1, "A", "a3"), insertAutoStyle("a2", {}, OTIS), insertCols(1, "A", "a4"));
        });
        it("should fail for invalid auto-style identifier", () => {
            testRunner().expectBidiError(insertAutoStyle("a2", {}), insertCols(1, "A", "b2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), insertCols(1, "A", "a2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), insertCols(1, "A", "b2"));
        });
    });

    describe('"insertAutoStyle" and "changeColumns"', () => {
        const OTIS = { otIndexShift: true };
        it("should transform the auto-style identifier", () => {
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeCols(1, "A", "a0"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeCols(1, "A", "a1"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeCols(1, "A", "a2"), insertAutoStyle("a2", {}, OTIS), changeCols(1, "A", "a3"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeCols(1, "A", "a3"), insertAutoStyle("a2", {}, OTIS), changeCols(1, "A", "a4"));
        });
        it("should fail for invalid auto-style identifier", () => {
            testRunner().expectBidiError(insertAutoStyle("a2", {}), changeCols(1, "A", "b2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), changeCols(1, "A", "a2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), changeCols(1, "A", "b2"));
        });
    });

    describe('"insertAutoStyle" and "insertRows"', () => {
        const OTIS = { otIndexShift: true };
        it("should transform the auto-style identifier", () => {
            testRunner().runBidiTest(insertAutoStyle("a2", {}), insertRows(1, "1", "a0"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), insertRows(1, "1", "a1"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), insertRows(1, "1", "a2"), insertAutoStyle("a2", {}, OTIS), insertRows(1, "1", "a3"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), insertRows(1, "1", "a3"), insertAutoStyle("a2", {}, OTIS), insertRows(1, "1", "a4"));
        });
        it("should fail for invalid auto-style identifier", () => {
            testRunner().expectBidiError(insertAutoStyle("a2", {}), insertRows(1, "1", "b2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), insertRows(1, "1", "a2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), insertRows(1, "1", "b2"));
        });
    });

    describe('"insertAutoStyle" and "changeRows"', () => {
        const OTIS = { otIndexShift: true };
        it("should transform the auto-style identifier", () => {
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeRows(1, "1", "a0"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeRows(1, "1", "a1"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeRows(1, "1", "a2"), insertAutoStyle("a2", {}, OTIS), changeRows(1, "1", "a3"));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeRows(1, "1", "a3"), insertAutoStyle("a2", {}, OTIS), changeRows(1, "1", "a4"));
        });
        it("should fail for invalid auto-style identifier", () => {
            testRunner().expectBidiError(insertAutoStyle("a2", {}), changeRows(1, "1", "b2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), changeRows(1, "1", "a2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), changeRows(1, "1", "b2"));
        });
    });

    describe('"insertAutoStyle" and "changeCells"', () => {
        const OTIS = { otIndexShift: true };
        it("should transform the auto-style identifier", () => {
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeCells(1, { A1: { s: "a0" } }));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeCells(1, { A1: { s: "a1" } }));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeCells(1, { A1: { s: "a2" } }), insertAutoStyle("a2", {}, OTIS), changeCells(1, { A1: { s: "a3" } }));
            testRunner().runBidiTest(insertAutoStyle("a2", {}), changeCells(1, { A1: { s: "a3" } }), insertAutoStyle("a2", {}, OTIS), changeCells(1, { A1: { s: "a4" } }));
        });
        it("should fail for invalid auto-style identifier", () => {
            testRunner().expectBidiError(insertAutoStyle("a2", {}), changeCells(1, { A1: { s: "b2" } }));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), changeCells(1, { A1: { s: "a2" } }));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), changeCells(1, { A1: { s: "b2" } }));
        });
    });

    // deleteAutoStyle --------------------------------------------------------

    describe('"deleteAutoStyle" and "insertColumns"', () => {
        const OTIS = { otIndexShift: true };
        it("should transform the auto-style identifier", () => {
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertCols(1, "A", "a0"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertCols(1, "A", "a1"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertCols(1, "A", "a3"), deleteAutoStyle("a2", OTIS), insertCols(1, "A", "a2"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertCols(1, "A", "a4"), deleteAutoStyle("a2", OTIS), insertCols(1, "A", "a3"));
        });
        it("should delete the auto-style identifier", () => {
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertCols(1, "A", "a2"),     deleteAutoStyle("a2", OTIS), insertCols(1, "A"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertCols(1, "A", "a2", {}), deleteAutoStyle("a2", OTIS), insertCols(1, "A", {}));
        });
        it("should fail for invalid auto-style identifier", () => {
            testRunner().expectBidiError(deleteAutoStyle("a2"), insertCols(1, "A", "b2"));
            testRunner().expectBidiError(deleteAutoStyle("b2"), insertCols(1, "A", "a2"));
            testRunner().expectBidiError(deleteAutoStyle("b2"), insertCols(1, "A", "b2"));
        });
    });

    describe('"deleteAutoStyle" and "changeColumns"', () => {
        const OTIS = { otIndexShift: true };
        it("should transform the auto-style identifier", () => {
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCols(1, "A", "a0"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCols(1, "A", "a1"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCols(1, "A", "a3"), deleteAutoStyle("a2", OTIS), changeCols(1, "A", "a2"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCols(1, "A", "a4"), deleteAutoStyle("a2", OTIS), changeCols(1, "A", "a3"));
        });
        it("should delete the auto-style identifier", () => {
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCols(1, "A", "a2"),     deleteAutoStyle("a2", OTIS), []);
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCols(1, "A", "a2", {}), deleteAutoStyle("a2", OTIS), changeCols(1, "A", {}));
        });
        it("should fail for invalid auto-style identifier", () => {
            testRunner().expectBidiError(insertAutoStyle("a2", {}), changeCols(1, "A", "b2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), changeCols(1, "A", "a2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), changeCols(1, "A", "b2"));
        });
    });

    describe('"deleteAutoStyle" and "insertRows"', () => {
        const OTIS = { otIndexShift: true };
        it("should transform the auto-style identifier", () => {
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertRows(1, "1", "a0"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertRows(1, "1", "a1"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertRows(1, "1", "a3"), deleteAutoStyle("a2", OTIS), insertRows(1, "1", "a2"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertRows(1, "1", "a4"), deleteAutoStyle("a2", OTIS), insertRows(1, "1", "a3"));
        });
        it("should delete the auto-style identifier", () => {
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertRows(1, "1", "a2"),     deleteAutoStyle("a2", OTIS), insertRows(1, "1"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), insertRows(1, "1", "a2", {}), deleteAutoStyle("a2", OTIS), insertRows(1, "1", {}));
        });
        it("should fail for invalid auto-style identifier", () => {
            testRunner().expectBidiError(deleteAutoStyle("a2"), insertRows(1, "1", "b2"));
            testRunner().expectBidiError(deleteAutoStyle("b2"), insertRows(1, "1", "a2"));
            testRunner().expectBidiError(deleteAutoStyle("b2"), insertRows(1, "1", "b2"));
        });
    });

    describe('"deleteAutoStyle" and "changeRows"', () => {
        const OTIS = { otIndexShift: true };
        it("should transform the auto-style identifier", () => {
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeRows(1, "1", "a0"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeRows(1, "1", "a1"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeRows(1, "1", "a3"), deleteAutoStyle("a2", OTIS), changeRows(1, "1", "a2"));
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeRows(1, "1", "a4"), deleteAutoStyle("a2", OTIS), changeRows(1, "1", "a3"));
        });
        it("should delete the auto-style identifier", () => {
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeRows(1, "1", "a2"),     deleteAutoStyle("a2", OTIS), []);
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeRows(1, "1", "a2", {}), deleteAutoStyle("a2", OTIS), changeRows(1, "1", {}));
        });
        it("should fail for invalid auto-style identifier", () => {
            testRunner().expectBidiError(insertAutoStyle("a2", {}), changeRows(1, "1", "b2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), changeRows(1, "1", "a2"));
            testRunner().expectBidiError(insertAutoStyle("b2", {}), changeRows(1, "1", "b2"));
        });
    });

    describe('"deleteAutoStyle" and "changeCells"', () => {
        const OTIS = { otIndexShift: true };
        it("should transform the auto-style identifier", () => {
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCells(1, { A1: { s: "a0" } }));
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCells(1, { A1: { s: "a1" } }));
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCells(1, { A1: { s: "a3" } }), deleteAutoStyle("a2", OTIS), changeCells(1, { A1: { s: "a2" } }));
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCells(1, { A1: { s: "a4" } }), deleteAutoStyle("a2", OTIS), changeCells(1, { A1: { s: "a3" } }));
        });
        it("should delete the auto-style identifier", () => {
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCells(1, { A1: { s: "a2", v: 42 } }),  deleteAutoStyle("a2", OTIS), changeCells(1, { A1: { v: 42 } }));
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCells(1, { A1: { s: "a2" }, A2: 42 }), deleteAutoStyle("a2", OTIS), changeCells(1, { A2: 42 }));
            testRunner().runBidiTest(deleteAutoStyle("a2"), changeCells(1, { A1: { s: "a2" } }),         deleteAutoStyle("a2", OTIS), []);
        });
        it("should fail for invalid auto-style identifier", () => {
            testRunner().expectBidiError(deleteAutoStyle("a2", {}), changeCells(1, { A1: { s: "b2" } }));
            testRunner().expectBidiError(deleteAutoStyle("b2", {}), changeCells(1, { A1: { s: "a2" } }));
            testRunner().expectBidiError(deleteAutoStyle("b2", {}), changeCells(1, { A1: { s: "b2" } }));
        });
    });

    // number formats ---------------------------------------------------------

    describe("number formats and independent operations", () => {
        it("should skip the transformations", () => {
            testRunner().runBidiTest([insertNumFmt(184, "0"), deleteNumFmt(184)], [
                GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS, SHEETCOLL_OPS, colOps(1), rowOps(1),
                cellOps(1), hlinkOps(1), nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    // insertNumberFormat -----------------------------------------------------

    describe('"insertNumberFormat" and "insertNumberFormat"', () => {
        it('should set equal identifier and format code to "removed"', () => {
            testRunner().runTest(insertNumFmt(184, "0"), insertNumFmt(184, "0"), [], []);
        });
        it("should fail for different identifiers", () => {
            testRunner().expectError(insertNumFmt(184, "0"), insertNumFmt(185, "0"));
        });
        it("should fail for different format codes", () => {
            testRunner().expectError(insertNumFmt(184, "0"), insertNumFmt(184, "0.00"));
        });
    });

    describe('"insertNumberFormat" and "deleteNumberFormat"', () => {
        it('should set "deleteNumberFormat" to "removed"', () => {
            testRunner().runBidiTest(insertNumFmt(184, "0"), deleteNumFmt(184), null, []);
        });
        it("should skip different identifiers", () => {
            testRunner().runBidiTest(deleteNumFmt(184, "0"), deleteNumFmt(185));
        });
    });

    // deleteNumberFormat -----------------------------------------------------

    describe('"deleteNumberFormat" and "deleteNumberFormat"', () => {
        it('should set equal identifier to "removed"', () => {
            testRunner().runTest(deleteNumFmt(184), deleteNumFmt(184), [], []);
        });
        it("should skip different identifiers", () => {
            testRunner().runTest(deleteNumFmt(184), deleteNumFmt(185));
        });
    });
});
