/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import {
    opSeries1, opSeries2,
    insertSheet, deleteSheet, moveSheet, copySheet, moveSheets, changeSheet,
    insertTable, deleteTable, changeTable, changeTableCol, sheetSelection
} from "~/spreadsheet/apphelper";

import {
    ATTRS, ATTRS2, ATTRS_I1, ATTRS_I2, ATTRS_O1, ATTRS_O2, ATTRS_R1, ATTRS_R2, COL_ATTRS, ROW_ATTRS,
    GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS,
    createTestRunner,
    colOps, rowOps, cellOps, hlinkOps, nameOps, tableOps, dvRuleOps, cfRuleOps,
    noteOps, commentOps, drawingOps, chartOps, drawingTextOps,
    selectOps, allSheetIndexOps, drawingAnchorOps, drawingNoAnchorOps
} from "~/spreadsheet/othelper";

// tests ======================================================================

describe("module io.ox/office/spreadsheet/model/operation/otmanager", () => {

    // initialize OT test runners
    const testRunner = createTestRunner();

    // insertSheet ------------------------------------------------------------

    describe('"insertSheet" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(insertSheet(1), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS]);
        });
    });

    describe('"insertSheet" and "insertSheet"', () => {
        it("should shift sheet indexes", () => {
            testRunner().runTest(insertSheet(2, "S1"), insertSheet(0, "S2"), insertSheet(3, "S1"), insertSheet(0, "S2"));
            testRunner().runTest(insertSheet(2, "S1"), insertSheet(1, "S2"), insertSheet(3, "S1"), insertSheet(1, "S2"));
            testRunner().runTest(insertSheet(2, "S1"), insertSheet(2, "S2"), insertSheet(3, "S1"), insertSheet(2, "S2"));
            testRunner().runTest(insertSheet(2, "S1"), insertSheet(3, "S2"), insertSheet(2, "S1"), insertSheet(4, "S2"));
            testRunner().runTest(insertSheet(2, "S1"), insertSheet(4, "S2"), insertSheet(2, "S1"), insertSheet(5, "S2"));
        });
        it("should fail to give two sheets the same name", () => {
            testRunner().expectError(insertSheet(1, "S1"), insertSheet(2, "S1"));
        });
    });

    describe('"insertSheet" and "deleteSheet"', () => {
        it("should handle inserted/deleted sheets without collision", () => {
            testRunner().runBidiTest(insertSheet(0), deleteSheet(2), insertSheet(0), deleteSheet(3));
            testRunner().runBidiTest(insertSheet(1), deleteSheet(2), insertSheet(1), deleteSheet(3));
            testRunner().runBidiTest(insertSheet(2), deleteSheet(2), insertSheet(2), deleteSheet(3));
            testRunner().runBidiTest(insertSheet(3), deleteSheet(2), insertSheet(2), deleteSheet(2));
            testRunner().runBidiTest(insertSheet(4), deleteSheet(2), insertSheet(3), deleteSheet(2));
        });
        it("should handle multiple inserted/deleted sheets without collision", () => {
            testRunner().runBidiTest(
                opSeries1(insertSheet, 2, 0, 1, 2), deleteSheet(1),
                opSeries1(insertSheet, 1, 0, 1, 2), deleteSheet(4)
            );
        });
    });

    describe('"insertSheet" and "moveSheet"', () => {
        it("should handle sheet moved to end", () => {
            testRunner().runBidiTest(insertSheet(1), moveSheet(2, 4), insertSheet(1), moveSheet(3, 5));
            testRunner().runBidiTest(insertSheet(2), moveSheet(2, 4), insertSheet(2), moveSheet(3, 5));
            testRunner().runBidiTest(insertSheet(3), moveSheet(2, 4), insertSheet(2), moveSheet(2, 5));
            testRunner().runBidiTest(insertSheet(4), moveSheet(2, 4), insertSheet(3), moveSheet(2, 5));
            testRunner().runBidiTest(insertSheet(5), moveSheet(2, 4), insertSheet(5), moveSheet(2, 4));
            testRunner().runBidiTest(insertSheet(6), moveSheet(2, 4), insertSheet(6), moveSheet(2, 4));
        });
        it("should handle sheet moved to front", () => {
            testRunner().runBidiTest(insertSheet(1), moveSheet(4, 2), insertSheet(1), moveSheet(5, 3));
            testRunner().runBidiTest(insertSheet(2), moveSheet(4, 2), insertSheet(2), moveSheet(5, 3));
            testRunner().runBidiTest(insertSheet(3), moveSheet(4, 2), insertSheet(4), moveSheet(5, 2));
            testRunner().runBidiTest(insertSheet(4), moveSheet(4, 2), insertSheet(5), moveSheet(5, 2));
            testRunner().runBidiTest(insertSheet(5), moveSheet(4, 2), insertSheet(5), moveSheet(4, 2));
            testRunner().runBidiTest(insertSheet(6), moveSheet(4, 2), insertSheet(6), moveSheet(4, 2));
        });
        it("should handle no-op move", () => {
            testRunner().runBidiTest(insertSheet(1), moveSheet(3, 3), null, []);
        });
        it("should handle multiple inserted sheets without collision", () => {
            testRunner().runBidiTest(
                opSeries1(insertSheet, 1, 3, 5, 7), opSeries1(moveSheet, [1, 2], [6, 3]),
                opSeries1(insertSheet, 1, 2, 5, 8), opSeries1(moveSheet, [2, 4], [10, 6])
            );
        });
    });

    describe('"insertSheet" and "copySheet"', () => {
        it("should handle sheet copied to end", () => {
            testRunner().runBidiTest(insertSheet(1, "S1"), copySheet(2, 4, "S2"), insertSheet(1, "S1"), copySheet(3, 5, "S2"));
            testRunner().runBidiTest(insertSheet(2, "S1"), copySheet(2, 4, "S2"), insertSheet(2, "S1"), copySheet(3, 5, "S2"));
            testRunner().runBidiTest(insertSheet(3, "S1"), copySheet(2, 4, "S2"), insertSheet(3, "S1"), copySheet(2, 5, "S2"));
            testRunner().runBidiTest(insertSheet(4, "S1"), copySheet(2, 4, "S2"), insertSheet(4, "S1"), copySheet(2, 5, "S2"));
            testRunner().runBidiTest(insertSheet(5, "S1"), copySheet(2, 4, "S2"), insertSheet(6, "S1"), copySheet(2, 4, "S2"));
        });
        it("should handle sheet copied to front", () => {
            testRunner().runBidiTest(insertSheet(1, "S1"), copySheet(4, 2, "S2"), insertSheet(1, "S1"), copySheet(5, 3, "S2"));
            testRunner().runBidiTest(insertSheet(2, "S1"), copySheet(4, 2, "S2"), insertSheet(2, "S1"), copySheet(5, 3, "S2"));
            testRunner().runBidiTest(insertSheet(3, "S1"), copySheet(4, 2, "S2"), insertSheet(4, "S1"), copySheet(5, 2, "S2"));
            testRunner().runBidiTest(insertSheet(4, "S1"), copySheet(4, 2, "S2"), insertSheet(5, "S1"), copySheet(5, 2, "S2"));
            testRunner().runBidiTest(insertSheet(5, "S1"), copySheet(4, 2, "S2"), insertSheet(6, "S1"), copySheet(4, 2, "S2"));
        });
        it("should handle sheet copied to itself", () => {
            testRunner().runBidiTest(insertSheet(1, "S1"), copySheet(3, 3, "S2"), insertSheet(1, "S1"), copySheet(4, 4, "S2"));
            testRunner().runBidiTest(insertSheet(2, "S1"), copySheet(3, 3, "S2"), insertSheet(2, "S1"), copySheet(4, 4, "S2"));
            testRunner().runBidiTest(insertSheet(3, "S1"), copySheet(3, 3, "S2"), insertSheet(3, "S1"), copySheet(4, 4, "S2"));
            testRunner().runBidiTest(insertSheet(4, "S1"), copySheet(3, 3, "S2"), insertSheet(5, "S1"), copySheet(3, 3, "S2"));
            testRunner().runBidiTest(insertSheet(5, "S1"), copySheet(3, 3, "S2"), insertSheet(6, "S1"), copySheet(3, 3, "S2"));
        });
        it("should fail to give two sheets the same name", () => {
            testRunner().expectBidiError(insertSheet(1, "S1"), copySheet(2, 3, "S1"));
        });
    });

    describe('"insertSheet" and "moveSheets"', () => {
        it("should handle inserted sheet", () => {
            const MOVE_SHEETS = [0, 6, 3, 4, 2, 1, 5, 7];
            testRunner().runBidiTest(insertSheet(0), moveSheets(MOVE_SHEETS), insertSheet(0), moveSheets([0, 1, 7, 4, 5, 3, 2, 6, 8]));
            testRunner().runBidiTest(insertSheet(1), moveSheets(MOVE_SHEETS), insertSheet(1), moveSheets([0, 1, 7, 4, 5, 3, 2, 6, 8]));
            testRunner().runBidiTest(insertSheet(2), moveSheets(MOVE_SHEETS), insertSheet(2), moveSheets([0, 7, 2, 4, 5, 3, 1, 6, 8]));
            testRunner().runBidiTest(insertSheet(3), moveSheets(MOVE_SHEETS), insertSheet(3), moveSheets([0, 7, 4, 3, 5, 2, 1, 6, 8]));
            testRunner().runBidiTest(insertSheet(4), moveSheets(MOVE_SHEETS), insertSheet(4), moveSheets([0, 7, 3, 5, 4, 2, 1, 6, 8]));
            testRunner().runBidiTest(insertSheet(5), moveSheets(MOVE_SHEETS), insertSheet(5), moveSheets([0, 7, 3, 4, 2, 5, 1, 6, 8]));
            testRunner().runBidiTest(insertSheet(6), moveSheets(MOVE_SHEETS), insertSheet(6), moveSheets([0, 7, 3, 4, 2, 1, 6, 5, 8]));
            testRunner().runBidiTest(insertSheet(7), moveSheets(MOVE_SHEETS), insertSheet(7), moveSheets([0, 6, 3, 4, 2, 1, 5, 7, 8]));
        });
        it('should detect no-op "moveSheets" operation', () => {
            testRunner().runBidiTest(insertSheet(1), moveSheets([0, 1, 2, 3, 4, 5, 6, 7]), insertSheet(1), []);
        });
    });

    describe('"insertSheet" and "changeSheet"', () => {
        it("should fail to give two sheets the same name", () => {
            testRunner().expectBidiError(insertSheet(1, "S1"), changeSheet(2, "S1"));
        });
    });

    describe('"insertSheet" and sheet index operations', () => {
        it("should shift sheet index", () => {
            testRunner().runBidiTest(
                insertSheet(1), allSheetIndexOps(0, 1, 2),
                insertSheet(1), allSheetIndexOps(0, 2, 3)
            );
        });
    });

    // deleteSheet ------------------------------------------------------------

    describe('"deleteSheet" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(deleteSheet(1), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS]);
        });
    });

    describe('"deleteSheet" and "deleteSheet"', () => {
        it("should shift sheet indexes", () => {
            testRunner().runTest(deleteSheet(2), deleteSheet(0), deleteSheet(1), deleteSheet(0));
            testRunner().runTest(deleteSheet(2), deleteSheet(1), deleteSheet(1), deleteSheet(1));
            testRunner().runTest(deleteSheet(2), deleteSheet(2), [],             []);
            testRunner().runTest(deleteSheet(2), deleteSheet(3), deleteSheet(2), deleteSheet(2));
            testRunner().runTest(deleteSheet(2), deleteSheet(4), deleteSheet(2), deleteSheet(3));
        });
        it("should fail without snapshot", () => {
            testRunner().expectError(deleteSheet(0), deleteSheet(1), { userData: null });
        });
        it("should fail to delete all sheets", () => {
            // one sheet remains
            testRunner().runTest(
                deleteSheet(0), opSeries1(deleteSheet, 7, 6, 5, 4, 3, 2),
                deleteSheet(0), opSeries1(deleteSheet, 6, 5, 4, 3, 2, 1)
            );
            // all sheets deleted
            testRunner().expectError(deleteSheet(0), opSeries1(deleteSheet, 7, 6, 5, 4, 3, 2, 1));
        });
    });

    describe('"deleteSheet" and "moveSheet"', () => {
        it("should handle sheet moved to end", () => {
            testRunner().runBidiTest(deleteSheet(1), moveSheet(2, 4), deleteSheet(1), moveSheet(1, 3));
            testRunner().runBidiTest(deleteSheet(2), moveSheet(2, 4), deleteSheet(4), []);
            testRunner().runBidiTest(deleteSheet(3), moveSheet(2, 4), deleteSheet(2), moveSheet(2, 3));
            testRunner().runBidiTest(deleteSheet(4), moveSheet(2, 4), deleteSheet(3), moveSheet(2, 3));
            testRunner().runBidiTest(deleteSheet(5), moveSheet(2, 4), deleteSheet(5), moveSheet(2, 4));
        });
        it("should handle sheet moved to front", () => {
            testRunner().runBidiTest(deleteSheet(1), moveSheet(4, 2), deleteSheet(1), moveSheet(3, 1));
            testRunner().runBidiTest(deleteSheet(2), moveSheet(4, 2), deleteSheet(3), moveSheet(3, 2));
            testRunner().runBidiTest(deleteSheet(3), moveSheet(4, 2), deleteSheet(4), moveSheet(3, 2));
            testRunner().runBidiTest(deleteSheet(4), moveSheet(4, 2), deleteSheet(2), []);
            testRunner().runBidiTest(deleteSheet(5), moveSheet(4, 2), deleteSheet(5), moveSheet(4, 2));
        });
        it("should handle no-op move", () => {
            testRunner().runBidiTest(deleteSheet(1), moveSheet(3, 3), null, []);
        });
    });

    describe('"deleteSheet" and "copySheet"', () => {
        it("should handle sheet copied to end", () => {
            testRunner().runBidiTest(deleteSheet(1), copySheet(2, 4), deleteSheet(1), copySheet(1, 3));
            testRunner().runBidiTest(deleteSheet(2), copySheet(2, 4), opSeries1(deleteSheet, 2, 3), []);
            testRunner().runBidiTest(deleteSheet(3), copySheet(2, 4), deleteSheet(3), copySheet(2, 3));
            testRunner().runBidiTest(deleteSheet(4), copySheet(2, 4), deleteSheet(5), copySheet(2, 4));
            testRunner().runBidiTest(deleteSheet(5), copySheet(2, 4), deleteSheet(6), copySheet(2, 4));
            testRunner().runBidiTest(deleteSheet(1), copySheet(2, 3), deleteSheet(1), copySheet(1, 2));
            testRunner().runBidiTest(deleteSheet(2), copySheet(2, 3), opSeries1(deleteSheet, 2, 2), []);
            testRunner().runBidiTest(deleteSheet(3), copySheet(2, 3), deleteSheet(4), copySheet(2, 3));
            testRunner().runBidiTest(deleteSheet(4), copySheet(2, 3), deleteSheet(5), copySheet(2, 3));
        });
        it("should handle sheet copied to front", () => {
            testRunner().runBidiTest(deleteSheet(1), copySheet(4, 2), deleteSheet(1), copySheet(3, 1));
            testRunner().runBidiTest(deleteSheet(2), copySheet(4, 2), deleteSheet(3), copySheet(3, 2));
            testRunner().runBidiTest(deleteSheet(3), copySheet(4, 2), deleteSheet(4), copySheet(3, 2));
            testRunner().runBidiTest(deleteSheet(4), copySheet(4, 2), opSeries1(deleteSheet, 5, 2), []);
            testRunner().runBidiTest(deleteSheet(5), copySheet(4, 2), deleteSheet(6), copySheet(4, 2));
            testRunner().runBidiTest(deleteSheet(1), copySheet(3, 2), deleteSheet(1), copySheet(2, 1));
            testRunner().runBidiTest(deleteSheet(2), copySheet(3, 2), deleteSheet(3), copySheet(2, 2));
            testRunner().runBidiTest(deleteSheet(3), copySheet(3, 2), opSeries1(deleteSheet, 4, 2), []);
            testRunner().runBidiTest(deleteSheet(4), copySheet(3, 2), deleteSheet(5), copySheet(3, 2));
        });
        it("should handle sheet copied to itself", () => {
            testRunner().runBidiTest(deleteSheet(2), copySheet(3, 3), deleteSheet(2), copySheet(2, 2));
            testRunner().runBidiTest(deleteSheet(3), copySheet(3, 3), opSeries1(deleteSheet, 4, 3), []);
            testRunner().runBidiTest(deleteSheet(4), copySheet(3, 3), deleteSheet(5), copySheet(3, 3));
        });
    });

    describe('"deleteSheet" and "moveSheets"', () => {
        it("should handle deleted sheet", () => {
            const MOVE_SHEETS = [0, 6, 3, 4, 2, 1, 5, 7];
            testRunner().runBidiTest(deleteSheet(0), moveSheets(MOVE_SHEETS), deleteSheet(0), moveSheets([5, 2, 3, 1, 0, 4, 6]));
            testRunner().runBidiTest(deleteSheet(1), moveSheets(MOVE_SHEETS), deleteSheet(5), moveSheets([0, 5, 2, 3, 1, 4, 6]));
            testRunner().runBidiTest(deleteSheet(2), moveSheets(MOVE_SHEETS), deleteSheet(4), moveSheets([0, 5, 2, 3, 1, 4, 6]));
            testRunner().runBidiTest(deleteSheet(3), moveSheets(MOVE_SHEETS), deleteSheet(2), moveSheets([0, 5, 3, 2, 1, 4, 6]));
            testRunner().runBidiTest(deleteSheet(4), moveSheets(MOVE_SHEETS), deleteSheet(3), moveSheets([0, 5, 3, 2, 1, 4, 6]));
            testRunner().runBidiTest(deleteSheet(5), moveSheets(MOVE_SHEETS), deleteSheet(6), moveSheets([0, 5, 3, 4, 2, 1, 6]));
            testRunner().runBidiTest(deleteSheet(6), moveSheets(MOVE_SHEETS), deleteSheet(1), moveSheets([0, 3, 4, 2, 1, 5, 6]));
            testRunner().runBidiTest(deleteSheet(7), moveSheets(MOVE_SHEETS), deleteSheet(7), moveSheets([0, 6, 3, 4, 2, 1, 5]));
        });
        it('should detect no-op "moveSheets" operation', () => {
            testRunner().runBidiTest(deleteSheet(1), moveSheets([0, 2, 1, 3, 4, 5, 6, 7]), deleteSheet(2), []);
        });
    });

    describe('"deleteSheet" and sheet index operations', () => {
        it("should shift sheet index", () => {
            testRunner().runBidiTest(
                deleteSheet(1), allSheetIndexOps(0, 3, 1,         4, 2),
                deleteSheet(1), allSheetIndexOps(0, 2, undefined, 3, 1)
            );
        });
    });

    // moveSheet --------------------------------------------------------------

    describe('"moveSheet" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(moveSheet(2, 4), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS]);
        });
    });

    describe('"moveSheet" and "moveSheet"', () => {
        it("should handle two move operations transforming each other", () => {
            testRunner().runTest(moveSheet(2, 4), moveSheet(3, 1), moveSheet(3, 4), moveSheet(2, 1));
            testRunner().runTest(moveSheet(1, 3), moveSheet(4, 2), moveSheet(1, 4), moveSheet(4, 1));
            testRunner().runTest(moveSheet(1, 3), moveSheet(2, 3), moveSheet(1, 2), moveSheet(1, 3));
            testRunner().runTest(moveSheet(1, 2), moveSheet(2, 4), [],              moveSheet(1, 4));
            testRunner().runTest(moveSheet(2, 4), moveSheet(1, 2), moveSheet(1, 4), []);
            testRunner().runTest(moveSheet(1, 3), moveSheet(1, 4), moveSheet(4, 3), []);
            testRunner().runTest(moveSheet(1, 3), moveSheet(1, 3), [],              []);
        });
    });

    describe('"moveSheet" and "copySheet"', () => {
        it("should handle move operation and copy operation transforming each other", () => {
            testRunner().runBidiTest(moveSheet(2, 4), copySheet(1, 3), moveSheet(2, 5), copySheet(1, 2));
            testRunner().runBidiTest(moveSheet(2, 4), copySheet(2, 3), moveSheet(2, 5), copySheet(4, 2));
            testRunner().runBidiTest(moveSheet(2, 4), copySheet(3, 3), moveSheet(2, 5), copySheet(2, 2));
            testRunner().runBidiTest(moveSheet(2, 4), copySheet(3, 2), moveSheet(3, 5), copySheet(2, 2));
            testRunner().runBidiTest(moveSheet(2, 4), copySheet(3, 1), moveSheet(3, 5), copySheet(2, 1));
            testRunner().runBidiTest(moveSheet(4, 2), copySheet(1, 3), moveSheet(5, 2), copySheet(1, 4));
            testRunner().runBidiTest(moveSheet(4, 2), copySheet(2, 3), moveSheet(5, 2), copySheet(3, 4));
            testRunner().runBidiTest(moveSheet(4, 2), copySheet(3, 3), moveSheet(5, 2), copySheet(4, 4));
            testRunner().runBidiTest(moveSheet(4, 2), copySheet(3, 2), moveSheet(5, 3), copySheet(4, 2));
            testRunner().runBidiTest(moveSheet(4, 2), copySheet(3, 1), moveSheet(5, 3), copySheet(4, 1));
            testRunner().runBidiTest(moveSheet(3, 3), copySheet(2, 4), []);
        });
    });

    describe('"moveSheet" and "moveSheets"', () => {
        it("should transform the vector, and ignore the single move", () => {
            const MOVE_SHEETS = [0, 6, 3, 4, 2, 1, 5, 7];
            testRunner().runBidiTest(moveSheet(2, 1), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 3, 4, 1, 2, 5, 7]));
            testRunner().runBidiTest(moveSheet(2, 2), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 3, 4, 2, 1, 5, 7]));
            testRunner().runBidiTest(moveSheet(2, 3), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 2, 4, 3, 1, 5, 7]));
            testRunner().runBidiTest(moveSheet(2, 4), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 2, 3, 4, 1, 5, 7]));
            testRunner().runBidiTest(moveSheet(2, 5), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 2, 3, 5, 1, 4, 7]));
            testRunner().runBidiTest(moveSheet(4, 1), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 4, 1, 3, 2, 5, 7]));
            testRunner().runBidiTest(moveSheet(4, 2), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 4, 2, 3, 1, 5, 7]));
            testRunner().runBidiTest(moveSheet(4, 3), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 4, 3, 2, 1, 5, 7]));
            testRunner().runBidiTest(moveSheet(4, 4), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 3, 4, 2, 1, 5, 7]));
            testRunner().runBidiTest(moveSheet(4, 5), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 3, 5, 2, 1, 4, 7]));
        });
        it('should detect no-op "moveSheets" operation', () => {
            testRunner().runBidiTest(moveSheet(1, 2), moveSheets([0, 2, 1, 3, 4, 5, 6, 7]), [], []);
        });
    });

    describe('"moveSheet" and sheet index operations', () => {
        it("should shift sheet index", () => {
            testRunner().runBidiTest(
                moveSheet(2, 4), allSheetIndexOps(1, 2, 3, 4, 5),
                moveSheet(2, 4), allSheetIndexOps(1, 4, 2, 3, 5)
            );
            testRunner().runBidiTest(
                moveSheet(4, 2), allSheetIndexOps(1, 2, 3, 4, 5),
                moveSheet(4, 2), allSheetIndexOps(1, 3, 4, 2, 5)
            );
            testRunner().runBidiTest(
                moveSheet(3, 3), allSheetIndexOps(1, 2, 3, 4, 5)
            );
        });
    });

    // copySheet --------------------------------------------------------------

    describe('"copySheet" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(copySheet(2, 4), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS]);
        });
    });

    describe('"copySheet" and "copySheet"', () => {
        it("should handle copy operations transforming each other", () => {
            testRunner().runTest(copySheet(2, 4, "S1"), copySheet(1, 3, "S2"), copySheet(2, 5, "S1"), copySheet(1, 3, "S2"));
            testRunner().runTest(copySheet(2, 4, "S1"), copySheet(2, 3, "S2"), copySheet(2, 5, "S1"), copySheet(2, 3, "S2"));
            testRunner().runTest(copySheet(2, 4, "S1"), copySheet(3, 3, "S2"), copySheet(2, 5, "S1"), copySheet(3, 3, "S2"));
            testRunner().runTest(copySheet(2, 4, "S1"), copySheet(3, 2, "S2"), copySheet(3, 5, "S1"), copySheet(3, 2, "S2"));
            testRunner().runTest(copySheet(2, 4, "S1"), copySheet(3, 1, "S2"), copySheet(3, 5, "S1"), copySheet(3, 1, "S2"));
            testRunner().runTest(copySheet(4, 2, "S1"), copySheet(1, 3, "S2"), copySheet(5, 2, "S1"), copySheet(1, 4, "S2"));
            testRunner().runTest(copySheet(4, 2, "S1"), copySheet(2, 3, "S2"), copySheet(5, 2, "S1"), copySheet(3, 4, "S2"));
            testRunner().runTest(copySheet(4, 2, "S1"), copySheet(3, 3, "S2"), copySheet(5, 2, "S1"), copySheet(4, 4, "S2"));
            testRunner().runTest(copySheet(4, 2, "S1"), copySheet(3, 2, "S2"), copySheet(5, 3, "S1"), copySheet(4, 2, "S2"));
            testRunner().runTest(copySheet(4, 2, "S1"), copySheet(3, 1, "S2"), copySheet(5, 3, "S1"), copySheet(4, 1, "S2"));
        });
        it("should fail to give two sheets the same name", () => {
            testRunner().expectError(copySheet(1, 2, "S1"), copySheet(3, 4, "S1"));
        });
    });

    describe('"copySheet" and "moveSheets"', () => {
        it("should transform the vector, and the copy operation", () => {
            const MOVE_SHEETS = [0, 6, 3, 4, 2, 1, 5, 7];
            testRunner().runBidiTest(copySheet(2, 1), moveSheets(MOVE_SHEETS), copySheet(4, 1), moveSheets([0, 1, 7, 4, 5, 3, 2, 6, 8]));
            testRunner().runBidiTest(copySheet(2, 2), moveSheets(MOVE_SHEETS), copySheet(4, 2), moveSheets([0, 7, 2, 4, 5, 3, 1, 6, 8]));
            testRunner().runBidiTest(copySheet(2, 3), moveSheets(MOVE_SHEETS), copySheet(4, 3), moveSheets([0, 7, 4, 3, 5, 2, 1, 6, 8]));
            testRunner().runBidiTest(copySheet(2, 4), moveSheets(MOVE_SHEETS), copySheet(4, 4), moveSheets([0, 7, 3, 5, 4, 2, 1, 6, 8]));
            testRunner().runBidiTest(copySheet(2, 5), moveSheets(MOVE_SHEETS), copySheet(4, 5), moveSheets([0, 7, 3, 4, 2, 5, 1, 6, 8]));
            testRunner().runBidiTest(copySheet(3, 1), moveSheets(MOVE_SHEETS), copySheet(2, 1), moveSheets([0, 1, 7, 4, 5, 3, 2, 6, 8]));
            testRunner().runBidiTest(copySheet(3, 2), moveSheets(MOVE_SHEETS), copySheet(2, 2), moveSheets([0, 7, 2, 4, 5, 3, 1, 6, 8]));
            testRunner().runBidiTest(copySheet(3, 3), moveSheets(MOVE_SHEETS), copySheet(2, 3), moveSheets([0, 7, 4, 3, 5, 2, 1, 6, 8]));
            testRunner().runBidiTest(copySheet(3, 4), moveSheets(MOVE_SHEETS), copySheet(2, 4), moveSheets([0, 7, 3, 5, 4, 2, 1, 6, 8]));
            testRunner().runBidiTest(copySheet(3, 5), moveSheets(MOVE_SHEETS), copySheet(2, 5), moveSheets([0, 7, 3, 4, 2, 5, 1, 6, 8]));
            testRunner().runBidiTest(copySheet(4, 1), moveSheets(MOVE_SHEETS), copySheet(3, 1), moveSheets([0, 1, 7, 4, 5, 3, 2, 6, 8]));
            testRunner().runBidiTest(copySheet(4, 2), moveSheets(MOVE_SHEETS), copySheet(3, 2), moveSheets([0, 7, 2, 4, 5, 3, 1, 6, 8]));
            testRunner().runBidiTest(copySheet(4, 3), moveSheets(MOVE_SHEETS), copySheet(3, 3), moveSheets([0, 7, 4, 3, 5, 2, 1, 6, 8]));
            testRunner().runBidiTest(copySheet(4, 4), moveSheets(MOVE_SHEETS), copySheet(3, 4), moveSheets([0, 7, 3, 5, 4, 2, 1, 6, 8]));
            testRunner().runBidiTest(copySheet(4, 5), moveSheets(MOVE_SHEETS), copySheet(3, 5), moveSheets([0, 7, 3, 4, 2, 5, 1, 6, 8]));
        });
    });

    describe('"copySheet" and "changeSheet"', () => {
        it("should clone sheet attributes but not sheet name", () => {
            testRunner().runBidiTest(
                copySheet(2, 4), opSeries1(changeSheet, [2, ATTRS],             [2, "2"], [2, "2", ATTRS]),
                copySheet(2, 4), opSeries1(changeSheet, [2, ATTRS], [4, ATTRS], [2, "2"], [2, "2", ATTRS], [4, ATTRS])
            );
        });
        it("should fail to give two sheets the same name", () => {
            testRunner().expectBidiError(copySheet(1, 2, "S1"), changeSheet(3, "S1"));
        });
    });

    describe('"copySheet" and table operations', () => {
        it("should always fail", () => {
            testRunner().expectBidiError(copySheet(2, 4), insertTable(2, "Table3", "A1:D4"));
            testRunner().expectBidiError(copySheet(2, 4), changeTable(2, "Table2", "A1:D4"));
            testRunner().expectBidiError(copySheet(2, 4), deleteTable(2, "Table2"));
            testRunner().expectBidiError(copySheet(2, 4), changeTableCol(2, "Table2", 0, { attrs: ATTRS }));
        });
    });

    describe('"copySheet" and sheet index operations', () => {
        it("should clone and shift sheet index", () => {
            testRunner().runBidiTest(
                copySheet(2, 4), allSheetIndexOps(1, 2,    3, 4, 5, { skipTables: true }),
                copySheet(2, 4), allSheetIndexOps(1, 2, 4, 3, 5, 6, { skipTables: true })
            );
            testRunner().runBidiTest(
                copySheet(4, 2), allSheetIndexOps(1, 2, 3, 4,    5, { skipTables: true }),
                copySheet(4, 2), allSheetIndexOps(1, 3, 4, 5, 2, 6, { skipTables: true })
            );
            testRunner().runBidiTest(
                copySheet(3, 3), allSheetIndexOps(1, 2, 3,    4, 5, { skipTables: true }),
                copySheet(3, 3), allSheetIndexOps(1, 2, 4, 3, 5, 6, { skipTables: true })
            );
        });
        it("should handle cascaded copy operations", () => {
            testRunner().runBidiTest(
                opSeries1(copySheet, [2, 4], [4, 1]), allSheetIndexOps(1, 2,       5, { skipTables: true }),
                opSeries1(copySheet, [2, 4], [4, 1]), allSheetIndexOps(2, 3, 5, 1, 7, { skipTables: true })
            );
        });
    });

    // moveSheets -------------------------------------------------------------

    describe('"moveSheets" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(moveSheets([0, 6, 3, 4, 2, 1, 5, 7]), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS]);
        });
    });

    describe('"moveSheets" and "moveSheets"', () => {
        const MOVE_SHEETS = [0, 6, 3, 4, 2, 1, 5, 7];
        const MOVE_SHEETS_2 = [3, 2, 6, 0, 4, 5, 1, 7];
        it("should handle move operations transforming each other", () => {
            testRunner().runTest(moveSheets(MOVE_SHEETS), moveSheets(MOVE_SHEETS_2), moveSheets([3, 2, 0, 4, 1, 6, 5, 7]), []);
            testRunner().runTest(moveSheets(MOVE_SHEETS_2), moveSheets(MOVE_SHEETS), moveSheets([2, 4, 1, 0, 3, 6, 5, 7]), []);
        });
        it("should detect local no-op", () => {
            testRunner().runTest(moveSheets(MOVE_SHEETS), moveSheets(MOVE_SHEETS), [], []);
        });
    });

    describe('"moveSheets" and sheet index operations', () => {
        it("should shift sheet index", () => {
            const MOVE_SHEETS = [0, 6, 3, 4, 2, 1, 5, 7];
            testRunner().runBidiTest(
                moveSheets(MOVE_SHEETS), allSheetIndexOps(0, 1, 2, 3, 4, 5, 6, 7),
                moveSheets(MOVE_SHEETS), allSheetIndexOps(0, 5, 4, 2, 3, 6, 1, 7)
            );
        });
    });

    // changeSheet ------------------------------------------------------------

    describe('"changeSheet" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(changeSheet(1, "S1"), [
                GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS, colOps(1), rowOps(1),
                cellOps(1), hlinkOps(1), nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    describe('"changeSheet" and "changeSheet"', () => {
        it("should not process different sheets", () => {
            testRunner().runTest(changeSheet(1, "S1", ATTRS), changeSheet(2, "S2", ATTRS));
        });
        it("should not process independent attributes", () => {
            testRunner().runTest(changeSheet(1, ATTRS_I1), changeSheet(1, ATTRS_I2));
        });
        it("should reduce attribute sets", () => {
            testRunner().runTest(
                changeSheet(1, ATTRS_O1), changeSheet(1, ATTRS_O2),
                changeSheet(1, ATTRS_R1), changeSheet(1, ATTRS_R2)
            );
        });
        it("should reduce sheet name", () => {
            testRunner().runTest(
                changeSheet(1, "S1", ATTRS_I1), changeSheet(1, "S1", ATTRS_I2),
                changeSheet(1,       ATTRS_I1), changeSheet(1,       ATTRS_I2)
            );
            testRunner().runTest(
                changeSheet(1, "S1", ATTRS_I1), changeSheet(1, "S2", ATTRS_I2),
                changeSheet(1, "S1", ATTRS_I1), changeSheet(1,       ATTRS_I2)
            );
            testRunner().runBidiTest(changeSheet(1, "S1", ATTRS_I1), changeSheet(1, ATTRS_I2));
        });
        it("should remove entire operation if nothing will change", () => {
            testRunner().runTest(changeSheet(1, "S1", ATTRS), changeSheet(1, "S2", ATTRS2), null,                []);
            testRunner().runTest(changeSheet(1, "S1", ATTRS), changeSheet(1, "S2", ATTRS), changeSheet(1, "S1"), []);
            testRunner().runTest(changeSheet(1, "S1", ATTRS), changeSheet(1, "S1", ATTRS), [],                   []);
        });
        it("should fail to give two sheets the same name", () => {
            testRunner().expectError(changeSheet(1, "S1"), changeSheet(2, "S1"));
        });
    });

    describe('"changeSheet" and drawing anchor', () => {
        const SHEET_ATTRS = COL_ATTRS.concat(ROW_ATTRS);
        it("should log warnings for absolute anchor", () => {
            const count = 4 * SHEET_ATTRS.length;
            testRunner().expectBidiWarnings(opSeries2(changeSheet, 1, SHEET_ATTRS), drawingAnchorOps(1, "2 A1 0 0 C3 0 0"), count);
        });
        it("should not log warnings without changed column/row size", () => {
            testRunner().expectBidiWarnings(changeSheet(1, ATTRS), drawingAnchorOps(1, "2 A1 0 0 C3 0 0"), 0);
        });
        it("should not log warnings without anchor attribute", () => {
            testRunner().expectBidiWarnings(opSeries2(changeSheet, 1, SHEET_ATTRS), drawingNoAnchorOps(1), 0);
        });
    });

    // sheetSelection ---------------------------------------------------------

    describe('"sheetSelection" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(selectOps(1), [GLOBAL_OPS, STYLESHEET_OPS, drawingTextOps(1)]);
        });
    });

    describe('"sheetSelection" and "sheetSelection"', () => {
        it("should always fail", () => {
            testRunner().expectError(sheetSelection(1, "A1", 0, "A1"), sheetSelection(2, "A1", 0, "A1"));
        });
    });
});
