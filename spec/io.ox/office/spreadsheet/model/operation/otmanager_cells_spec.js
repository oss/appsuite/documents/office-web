/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import {
    opSeries1, opSeries2,
    changeCells, moveCells, mergeCells, insertHlink, deleteHlink,
    deleteName, insertTable, deleteTable, changeTable, changeTableCol,
    insertDVRule, deleteDVRule, changeDVRule, insertCFRule, deleteCFRule, changeCFRule
} from "~/spreadsheet/apphelper";

import {
    MM_MERGE, MM_HORIZONTAL, MM_VERTICAL, MM_UNMERGE, MM_ALL, URL,
    ATTRS, ATTRS_I1, ATTRS_I2,
    GLOBAL_OPS, STYLESHEET_OPS,
    createTestRunner,
    hlinkOps, nameOps, tableOps, dvRuleOps, cfRuleOps,
    noteOps, commentOps, drawingOps, chartOps, drawingTextOps, selectOps
} from "~/spreadsheet/othelper";

// tests ======================================================================

describe("module io.ox/office/spreadsheet/model/operation/otmanager", () => {

    // initialize OT test runners
    const testRunner = createTestRunner();
    const odfTestRunner = createTestRunner("odf");

    // changeCells ------------------------------------------------------------

    describe('"changeCells" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(changeCells(1, { A1: 42 }), [
                GLOBAL_OPS, STYLESHEET_OPS, mergeCells(1, "A1:B2"), hlinkOps(1), nameOps(1),
                changeTableCol(1, "Table1", 0, { attrs: ATTRS }), deleteTable(1, "Table1"),
                dvRuleOps(1), cfRuleOps(1), noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    describe('"changeCells" and "changeCells"', () => {
        it("should not modify independent values", () => {
            testRunner().runTest(
                changeCells(1, { A1: 42,    A2: { e: "#NAME?" } }),
                changeCells(1, { B1: "abc", B2: { e: "#NULL!" } })
            );
        });
        it("should prefer values from local operation", () => {
            testRunner().runTest(
                changeCells(1, { A1: 42,    A2: { e: "#NAME?" }, A3: 42,              A4: { e: "#NAME?" } }),
                changeCells(1, { A1: "abc", A2: "abc",           A3: { e: "#NULL!" }, A4: { e: "#NULL!" } }),
                changeCells(1, { A1: 42,    A2: { e: "#NAME?" }, A3: 42,              A4: { e: "#NAME?" } }),
                []
            );
        });
        it("should delete equal values from local operation", () => {
            testRunner().runTest(
                changeCells(1, { A1: 42, A2: { e: "#NAME?" }, A3: 42 }),
                changeCells(1, { A1: 42, A2: { e: "#NAME?" }, A4: 42 }),
                changeCells(1, { A3: 42 }),
                changeCells(1, { A4: 42 })
            );
        });
        it('should handle the "f" property', () => {
            testRunner().runTest(
                changeCells(1, { A1: { f: "1" }, A2: { f: "2" }, A3: { f: "3" } }),
                changeCells(1, { A1: { f: "1" }, A2: { f: "3" }, A4: { f: "4" } }),
                changeCells(1, { A2: { f: "2" }, A3: { f: "3" } }),
                changeCells(1, { A4: { f: "4" } })
            );
        });
        it('should handle the "s" property', () => {
            testRunner().runTest(
                changeCells(1, { A1: { s: "a1" }, A2: { s: "a2" }, A3: { s: "a3" } }),
                changeCells(1, { A1: { s: "a1" }, A2: { s: "a3" }, A4: { s: "a4" } }),
                changeCells(1, { A2: { s: "a2" }, A3: { s: "a3" } }),
                changeCells(1, { A4: { s: "a4" } })
            );
        });
        it("should decompose overlapping ranges", () => {
            testRunner().runTest(
                changeCells(1, { "A1:B2": 42, C3: 42 }),
                changeCells(1, { "B2:C3": "abc" }),
                changeCells(1, { "A1:B1": 42, A2: 42, B2: 42, C3: 42 }),
                changeCells(1, { C2: "abc", B3: "abc" })
            );
        });
        it('should handle the "u" flag', () => {
            testRunner().runTest(
                changeCells(1, { A1: { u: true }, A2: { u: true }, A3: { u: true, v: 42 }, A4: 42,                       A5: 42 }),
                changeCells(1, { A1: { u: true }, A2: 42,          A3: 42,                 A4: { u: true, e: "#NAME?" }, A5: { u: true, v: 42 } }),
                changeCells(1, {                  A2: { u: true }, A3: { u: true, v: 42 }, A4: 42 }),
                changeCells(1, {                                                           A4: { u: true, v: 42 },       A5: { u: true, v: 42 } })
            );
        });
        it('should fail to transform the "sr" property', () => {
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", si: 1, sr: "A1:B2" } }), changeCells(1, { A1: { f: "1" } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", si: 1, sr: "A1:B2" } }), changeCells(1, { A1: { f: null } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", si: 1, sr: "A1:B2" } }), changeCells(1, { A1: { si: 2 } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", si: 1, sr: "A1:B2" } }), changeCells(1, { A1: { si: null } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", si: 1, sr: "A1:B2" } }), changeCells(1, { A1: { sr: "A1:C3" } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", si: 1, sr: "A1:B2" } }), changeCells(1, { A1: { sr: null } }));
        });
        it('should fail to transform the "mr" property', () => {
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:B2" } }), changeCells(1, { B2: { f: "1" } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:B2" } }), changeCells(1, { B2: { f: null } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:B2" } }), changeCells(1, { B2: { si: 1 } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:B2" } }), changeCells(1, { B2: { si: null } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:B2" } }), changeCells(1, { B2: { sr: "B2" } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:B2" } }), changeCells(1, { B2: { sr: null } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:B2" } }), changeCells(1, { B2: { mr: "B2:C3" } }));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:B2" } }), changeCells(1, { B2: { mr: null } }));
        });
    });

    describe('"changeCells" and "mergeCells"', () => {
        it("should skip different sheets", () => {
            testRunner().runBidiTest(changeCells(1, { B2: { f: "1", mr: "B2:C3" } }), mergeCells(0, "A1:B2", MM_MERGE));
        });
        it("should fail to merge over matrix formula", () => {
            testRunner().expectBidiError(changeCells(1, { B2: { f: "1", mr: "B2:C3" } }), mergeCells(1, "A1:B2", MM_MERGE));
            testRunner().expectBidiError(changeCells(1, { B2: { f: "1", mr: "B2:C3" } }), mergeCells(1, "A1:B2", MM_HORIZONTAL));
            testRunner().expectBidiError(changeCells(1, { B2: { f: "1", mr: "B2:C3" } }), mergeCells(1, "A1:B2", MM_VERTICAL));
        });
        it("should allow to unmerge over matrix formula", () => {
            testRunner().runBidiTest(changeCells(1, { B2: { f: "1", mr: "B2:C3" } }), mergeCells(1, "A1:B2", MM_UNMERGE));
        });
    });

    describe('"changeCells" and "insertTable"', () => {
        it("should fail to change values of a table header", () => {
            testRunner().runBidiTest(changeCells(1, { A2: "a" }), insertTable(1, "Table3", "A1:D4"));
            testRunner().runBidiTest(changeCells(1, { A1: { s: "a1" } }), insertTable(1, "Table3", "A1:D4"));
            testRunner().expectBidiError(changeCells(1, { A1: "a" }), insertTable(1, "Table3", "A1:D4"));
            testRunner().expectBidiError(changeCells(1, { B1: { v: 42 } }), insertTable(1, "Table3", "A1:D4"));
            testRunner().expectBidiError(changeCells(1, { C1: { f: "1" } }), insertTable(1, "Table3", "A1:D4"));
        });
        it("should fail to insert matrix formula over a table range", () => {
            testRunner().runBidiTest(changeCells(1, { A1: { f: "1", mr: "A1:D4" } }), insertTable(1, "Table3", "E5:H8"));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:D4" } }), insertTable(1, "Table3", "A1:D4"));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:D4" } }), insertTable(1, "Table3", "D4:G7"));
        });
        it("should skip different sheets", () => {
            testRunner().runBidiTest(changeCells(1, { A1: "a" }), insertTable(2, "Table3", "A1:D4"));
            testRunner().runBidiTest(changeCells(1, { A1: { f: "1", mr: "A1:D4" } }), insertTable(2, "Table3", "A1:D4"));
        });
    });

    describe('"changeCells" and "changeTable"', () => {
        it("should fail to change values of a table header", () => {
            testRunner().runBidiTest(changeCells(1, { A2: "a" }), changeTable(1, "Table1", "A1:D4"));
            testRunner().runBidiTest(changeCells(1, { A1: "a" }), changeTable(1, "Table1", { newName: "Table3" }));
            testRunner().runBidiTest(changeCells(1, { A1: { s: "a1" } }), changeTable(1, "Table1", "A1:D4"));
            testRunner().expectBidiError(changeCells(1, { A1: "a" }), changeTable(1, "Table1", "A1:D4"));
            testRunner().expectBidiError(changeCells(1, { B1: { v: 42 } }), changeTable(1, "Table1", "A1:D4"));
            testRunner().expectBidiError(changeCells(1, { C1: { f: "1" } }), changeTable(1, "Table1", "A1:D4"));
        });
        it("should fail to insert matrix formula over a table range", () => {
            testRunner().runBidiTest(changeCells(1, { A1: { f: "1", mr: "A1:D4" } }), changeTable(1, "Table1", "E5:H8"));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:D4" } }), changeTable(1, "Table1", "A1:D4"));
            testRunner().expectBidiError(changeCells(1, { A1: { f: "1", mr: "A1:D4" } }), changeTable(1, "Table1", "D4:G7"));
        });
        it("should skip different sheets", () => {
            testRunner().runBidiTest(changeCells(1, { A1: "a" }), changeTable(2, "Table2", "A1:D4"));
            testRunner().runBidiTest(changeCells(1, { A1: { f: "1", mr: "A1:D4" } }), changeTable(2, "Table2", "A1:D4"));
        });
    });

    // moveCells --------------------------------------------------------------

    describe('"moveCells" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(moveCells(1, "A1:B2", "down"), [
                GLOBAL_OPS, STYLESHEET_OPS,
                deleteName("a", 1), changeTableCol(1, "Table1", 1, { attrs: ATTRS }), deleteTable(1, "Table1")
            ]);
        });
    });

    // mergeCells -------------------------------------------------------------

    describe('"mergeCells" and independent operations', () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(mergeCells(1, "A1:B2"), [
                GLOBAL_OPS, STYLESHEET_OPS, hlinkOps(1), nameOps(1),
                changeTableCol(1, "Table1", 1, { attrs: ATTRS }), deleteTable(1, "Table1"),
                dvRuleOps(1), cfRuleOps(1), noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    describe('"mergeCells" and "mergeCells"', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(mergeCells(1, "A1:B2", MM_MERGE), mergeCells(2, "B2:C3", MM_MERGE));
        });
        it("should remove external MERGE ranges covered by local ranges", () => {
            testRunner().runTest(
                opSeries2(mergeCells, 1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", MM_ALL), mergeCells(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", MM_MERGE),
                opSeries2(mergeCells, 1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", MM_ALL), mergeCells(1, "C4:D6 A4:B6",                   MM_MERGE)
            );
        });
        it("should reduce external HORIZONTAL ranges", () => {
            testRunner().runTest(
                opSeries2(mergeCells, 1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", MM_ALL), mergeCells(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", MM_HORIZONTAL),
                opSeries2(mergeCells, 1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", MM_ALL), mergeCells(1, "C4:D6 H5:K6 T1:Y2 T5:Y6 A4:B6", MM_HORIZONTAL)
            );
            testRunner().runTest(
                opSeries2(mergeCells, 1, "B2 D4 F6", MM_ALL), mergeCells(1, "A1:G7", MM_HORIZONTAL),
                opSeries2(mergeCells, 1, "B2 D4 F6", MM_ALL), mergeCells(1, "A1:G1 A3:G3 A5:G5 A7:G7", MM_HORIZONTAL)
            );
        });
        it("should reduce external VERTICAL ranges", () => {
            testRunner().runTest(
                opSeries2(mergeCells, 1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", MM_ALL), mergeCells(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", MM_VERTICAL),
                opSeries2(mergeCells, 1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", MM_ALL), mergeCells(1, "C4:D6 J3:K6 T1:U6 X1:Y6 A4:B6", MM_VERTICAL)
            );
            testRunner().runTest(
                opSeries2(mergeCells, 1, "B2 D4 F6", MM_ALL), mergeCells(1, "A1:G7", MM_VERTICAL),
                opSeries2(mergeCells, 1, "B2 D4 F6", MM_ALL), mergeCells(1, "A1:A7 C1:C7 E1:E7 G1:G7", MM_VERTICAL)
            );
        });
        it("should reduce external UNMERGE ranges", () => {
            testRunner().runTest(
                opSeries2(mergeCells, 1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", MM_ALL), mergeCells(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6",                   MM_UNMERGE),
                opSeries2(mergeCells, 1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", MM_ALL), mergeCells(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6", MM_UNMERGE)
            );
        });
        it('should set no-op to "removed" state', () => {
            testRunner().runTest(mergeCells(1, "B4:C5", MM_MERGE), mergeCells(1, "C4:D5", MM_MERGE), null, []);
        });
    });

    describe('"mergeCells" and "insertTable"', () => {
        it("should merge outside the table range", () => {
            testRunner().runBidiTest(opSeries2(mergeCells, 1, "A1:D4", MM_ALL), insertTable(1, "Table3", "E5:H8"));
        });
        it("should unmerge before inserting overlapping table", () => {
            testRunner().runBidiTest(
                mergeCells(1, "A1:D4", MM_MERGE), insertTable(1, "Table3", "D4:G7"),
                [], [mergeCells(1, "D4:G7", MM_UNMERGE), insertTable(1, "Table3", "D4:G7")]
            );
            testRunner().runBidiTest(
                mergeCells(1, "A1:D4", MM_HORIZONTAL), insertTable(1, "Table3", "D4:G7"),
                mergeCells(1, "A1:D3", MM_HORIZONTAL), [mergeCells(1, "D4:G7", MM_UNMERGE), insertTable(1, "Table3", "D4:G7")]
            );
            testRunner().runBidiTest(
                mergeCells(1, "A1:D4", MM_VERTICAL), insertTable(1, "Table3", "D4:G7"),
                mergeCells(1, "A1:C4", MM_VERTICAL), [mergeCells(1, "D4:G7", MM_UNMERGE), insertTable(1, "Table3", "D4:G7")]
            );
        });
        it("should allow to unmerge over a table range", () => {
            testRunner().runBidiTest(mergeCells(1, "A1:D4", MM_UNMERGE), opSeries1(insertTable, [1, "Table3", "A1:D4"], [1, "Table4", "D4:G7"]));
        });
        it("should skip different sheets", () => {
            testRunner().runBidiTest(mergeCells(1, "A1:D4", MM_MERGE), insertTable(2, "Table4", "A1:D4"));
        });
    });

    describe('"mergeCells" and "changeTable"', () => {
        it("should merge outside the table range", () => {
            testRunner().runBidiTest(opSeries2(mergeCells, 1, "A1:D4", MM_ALL), changeTable(1, "Table1", "E5:H8"));
            testRunner().runBidiTest(opSeries2(mergeCells, 1, "A1:D4", MM_ALL), changeTable(1, "Table1", { newName: "Table3" }));
        });
        it("should unmerge before moving table", () => {
            testRunner().runBidiTest(
                mergeCells(1, "A1:D4", MM_MERGE), changeTable(1, "Table1", "D4:G7"),
                [], [mergeCells(1, "D4:G7", MM_UNMERGE), changeTable(1, "Table1", "D4:G7")]
            );
            testRunner().runBidiTest(
                mergeCells(1, "A1:D4", MM_HORIZONTAL), changeTable(1, "Table1", "D4:G7"),
                mergeCells(1, "A1:D3", MM_HORIZONTAL), [mergeCells(1, "D4:G7", MM_UNMERGE), changeTable(1, "Table1", "D4:G7")]
            );
            testRunner().runBidiTest(
                mergeCells(1, "A1:D4", MM_VERTICAL), changeTable(1, "Table1", "D4:G7"),
                mergeCells(1, "A1:C4", MM_VERTICAL), [mergeCells(1, "D4:G7", MM_UNMERGE), changeTable(1, "Table1", "D4:G7")]
            );
        });
        it("should allow to unmerge over a table range", () => {
            testRunner().runBidiTest(mergeCells(1, "A1:D4", MM_UNMERGE), changeTable(1, "Table1", "D4:G7"));
        });
        it("should skip different sheets", () => {
            testRunner().runBidiTest(mergeCells(1, "A1:D4", MM_MERGE), changeTable(2, "Table2", "A1:D4"));
        });
    });

    // hyperlinks -------------------------------------------------------------

    describe("hyperlinks and independent operations", () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(hlinkOps(1), [
                GLOBAL_OPS, STYLESHEET_OPS,
                nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    // insertHyperlink --------------------------------------------------------

    describe('"insertHyperlink" and "insertHyperlink"', () => {
        const URL2 = "https://example.org";
        it("should skip different sheets", () => {
            testRunner().runTest(insertHlink(1, "A1:B2", URL), insertHlink(0, "A1:B2", URL2));
        });
        it("should reduce external hyperlink ranges", () => {
            testRunner().runTest(
                insertHlink(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", URL), insertHlink(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6",                   URL2),
                insertHlink(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", URL), insertHlink(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6", URL2)
            );
        });
        it('should set no-op to "removed" state', () => {
            testRunner().runTest(insertHlink(1, "A1:D4", URL), insertHlink(1, "B2:C3", URL2), null, []);
        });
    });

    describe('"insertHyperlink" and "deleteHyperlink"', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(insertHlink(1, "A1:B2", URL), deleteHlink(0, "A1:B2"));
        });
        it("should reduce external hyperlink ranges", () => {
            testRunner().runTest(
                insertHlink(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", URL), deleteHlink(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6"),
                insertHlink(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", URL), deleteHlink(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6")
            );
        });
        it('should set no-op to "removed" state', () => {
            testRunner().runTest(insertHlink(1, "A1:D4", URL), deleteHlink(1, "B2:C3"), null, []);
        });
    });

    // deleteHyperlink --------------------------------------------------------

    describe('"deleteHyperlink" and "insertHyperlink"', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(deleteHlink(1, "A1:B2"), insertHlink(0, "A1:B2", URL));
        });
        it("should reduce external hyperlink ranges", () => {
            testRunner().runTest(
                deleteHlink(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3"), insertHlink(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6",                   URL),
                deleteHlink(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3"), insertHlink(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6", URL)
            );
        });
        it('should set no-op to "removed" state', () => {
            testRunner().runTest(deleteHlink(1, "A1:D4"), insertHlink(1, "B2:C3", URL), null, []);
        });
    });

    describe('"deleteHyperlink" and "deleteHyperlink"', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(deleteHlink(1, "A1:B2"), deleteHlink(0, "A1:B2"));
        });
        it("should reduce external hyperlink ranges", () => {
            testRunner().runTest(
                deleteHlink(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3"), deleteHlink(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6"),
                deleteHlink(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3"), deleteHlink(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6")
            );
        });
        it('should set no-op to "removed" state', () => {
            testRunner().runTest(deleteHlink(1, "A1:D4"), deleteHlink(1, "B2:C3"), null, []);
        });
    });

    // data validation --------------------------------------------------------

    describe("data validation and independent operations", () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(dvRuleOps(1), [
                GLOBAL_OPS, STYLESHEET_OPS, cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    // insertDVRule -----------------------------------------------------------

    describe('"insertDVRule" and "insertDVRule"', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(insertDVRule(1, 1, "A1:D4"), insertDVRule(2, 1, "A1:D4"));
        });
        it("should shift rule index", () => {
            testRunner().runTest(insertDVRule(1, 2, "A1:D4"), insertDVRule(1, 0, "E5:H8"), insertDVRule(1, 3, "A1:D4"), insertDVRule(1, 0, "E5:H8"));
            testRunner().runTest(insertDVRule(1, 2, "A1:D4"), insertDVRule(1, 1, "E5:H8"), insertDVRule(1, 3, "A1:D4"), insertDVRule(1, 1, "E5:H8"));
            testRunner().runTest(insertDVRule(1, 2, "A1:D4"), insertDVRule(1, 2, "E5:H8"), insertDVRule(1, 3, "A1:D4"), insertDVRule(1, 2, "E5:H8"));
            testRunner().runTest(insertDVRule(1, 2, "A1:D4"), insertDVRule(1, 3, "E5:H8"), insertDVRule(1, 2, "A1:D4"), insertDVRule(1, 4, "E5:H8"));
            testRunner().runTest(insertDVRule(1, 2, "A1:D4"), insertDVRule(1, 4, "E5:H8"), insertDVRule(1, 2, "A1:D4"), insertDVRule(1, 5, "E5:H8"));
        });
        it("should fail to create overlapping rules", () => {
            testRunner().expectError(insertDVRule(1, 0, "A1:D4"), insertDVRule(1, 2, "B2:E5"));
        });
    });

    describe('"insertDVRule" and "deleteDVRule"', () => {
        it("should skip different sheets", () => {
            testRunner().runBidiTest(insertDVRule(1, 1, "A1:D4"), deleteDVRule(2, 1));
        });
        it("should shift rule index", () => {
            testRunner().runBidiTest(insertDVRule(1, 2, "A1:D4"), deleteDVRule(1, 0), insertDVRule(1, 1, "A1:D4"), deleteDVRule(1, 0));
            testRunner().runBidiTest(insertDVRule(1, 2, "A1:D4"), deleteDVRule(1, 1), insertDVRule(1, 1, "A1:D4"), deleteDVRule(1, 1));
            testRunner().runBidiTest(insertDVRule(1, 2, "A1:D4"), deleteDVRule(1, 2), insertDVRule(1, 2, "A1:D4"), deleteDVRule(1, 3));
            testRunner().runBidiTest(insertDVRule(1, 2, "A1:D4"), deleteDVRule(1, 3), insertDVRule(1, 2, "A1:D4"), deleteDVRule(1, 4));
            testRunner().runBidiTest(insertDVRule(1, 2, "A1:D4"), deleteDVRule(1, 4), insertDVRule(1, 2, "A1:D4"), deleteDVRule(1, 5));
        });
    });

    describe('"insertDVRule" and "changeDVRule"', () => {
        it("should skip different sheets", () => {
            testRunner().runBidiTest(insertDVRule(1, 1, "A1:D4"), changeDVRule(2, 1, "A1:D4"));
        });
        it("should shift rule index", () => {
            testRunner().runBidiTest(insertDVRule(1, 2, "A1:D4"), changeDVRule(1, 0, "E5:H8"), insertDVRule(1, 2, "A1:D4"), changeDVRule(1, 0, "E5:H8"));
            testRunner().runBidiTest(insertDVRule(1, 2, "A1:D4"), changeDVRule(1, 1, "E5:H8"), insertDVRule(1, 2, "A1:D4"), changeDVRule(1, 1, "E5:H8"));
            testRunner().runBidiTest(insertDVRule(1, 2, "A1:D4"), changeDVRule(1, 2, "E5:H8"), insertDVRule(1, 2, "A1:D4"), changeDVRule(1, 3, "E5:H8"));
            testRunner().runBidiTest(insertDVRule(1, 2, "A1:D4"), changeDVRule(1, 3, "E5:H8"), insertDVRule(1, 2, "A1:D4"), changeDVRule(1, 4, "E5:H8"));
            testRunner().runBidiTest(insertDVRule(1, 2, "A1:D4"), changeDVRule(1, 4, "E5:H8"), insertDVRule(1, 2, "A1:D4"), changeDVRule(1, 5, "E5:H8"));
        });
        it("should fail to create overlapping rules", () => {
            testRunner().expectBidiError(insertDVRule(1, 0, "A1:D4"), changeDVRule(1, 2, "B2:E5"));
        });
    });

    // deleteDVRule -----------------------------------------------------------

    describe('"deleteDVRule" and "deleteDVRule"', () => {
        it("should skip different sheets", () => {
            testRunner().runTest(deleteDVRule(1, 1), deleteDVRule(2, 1));
        });
        it("should shift rule index", () => {
            testRunner().runTest(deleteDVRule(1, 2), deleteDVRule(1, 0), deleteDVRule(1, 1), deleteDVRule(1, 0));
            testRunner().runTest(deleteDVRule(1, 2), deleteDVRule(1, 1), deleteDVRule(1, 1), deleteDVRule(1, 1));
            testRunner().runTest(deleteDVRule(1, 2), deleteDVRule(1, 2), [],                 []);
            testRunner().runTest(deleteDVRule(1, 2), deleteDVRule(1, 3), deleteDVRule(1, 2), deleteDVRule(1, 2));
            testRunner().runTest(deleteDVRule(1, 2), deleteDVRule(1, 4), deleteDVRule(1, 2), deleteDVRule(1, 3));
        });
    });

    describe('"deleteDVRule" and "changeDVRule"', () => {
        it("should skip different sheets", () => {
            testRunner().runBidiTest(deleteDVRule(1, 1), changeDVRule(2, 1, "A1:D4"));
        });
        it("should shift rule index", () => {
            testRunner().runBidiTest(deleteDVRule(1, 2), changeDVRule(1, 0, "A1:D4"), null, changeDVRule(1, 0, "A1:D4"));
            testRunner().runBidiTest(deleteDVRule(1, 2), changeDVRule(1, 1, "A1:D4"), null, changeDVRule(1, 1, "A1:D4"));
            testRunner().runBidiTest(deleteDVRule(1, 2), changeDVRule(1, 2, "A1:D4"), null, []);
            testRunner().runBidiTest(deleteDVRule(1, 2), changeDVRule(1, 3, "A1:D4"), null, changeDVRule(1, 2, "A1:D4"));
            testRunner().runBidiTest(deleteDVRule(1, 2), changeDVRule(1, 4, "A1:D4"), null, changeDVRule(1, 3, "A1:D4"));
        });
    });

    // changeDVRule -----------------------------------------------------------

    describe('"changeDVRule" and "changeDVRule"', () => {
        it("should skip different sheets and rules", () => {
            testRunner().runTest(changeDVRule(1, 1, "A1:D4"), changeDVRule(2, 1, "A1:D4"));
            testRunner().runTest(changeDVRule(1, 1, "A1:D4"), changeDVRule(1, 2, "E5:H8"));
        });
        it("should reduce various properties", () => {
            testRunner().runTest(changeDVRule(1, 1, "A1:D4"),                changeDVRule(1, 1, "B2:E5"),                 null, []);
            testRunner().runTest(changeDVRule(1, 1, "A1:D4"),                changeDVRule(1, 1, "A1:D4"),                 [],   []);
            testRunner().runTest(changeDVRule(1, 1, { showInfo: true }),     changeDVRule(1, 1, { showInfo: false }),     null, []);
            testRunner().runTest(changeDVRule(1, 1, { showInfo: true }),     changeDVRule(1, 1, { showInfo: true }),      [],   []);
            testRunner().runTest(changeDVRule(1, 1, { infoTitle: "a" }),     changeDVRule(1, 1, { infoTitle: "b" }),      null, []);
            testRunner().runTest(changeDVRule(1, 1, { infoText: "a" }),      changeDVRule(1, 1, { infoText: "b" }),       null, []);
            testRunner().runTest(changeDVRule(1, 1, { showError: true }),    changeDVRule(1, 1, { showError: false }),    null, []);
            testRunner().runTest(changeDVRule(1, 1, { errorTitle: "a" }),    changeDVRule(1, 1, { errorTitle: "b" }),     null, []);
            testRunner().runTest(changeDVRule(1, 1, { errorText: "a" }),     changeDVRule(1, 1, { errorText: "b" }),      null, []);
            testRunner().runTest(changeDVRule(1, 1, { errorType: "a" }),     changeDVRule(1, 1, { errorType: "b" }),      null, []);
            testRunner().runTest(changeDVRule(1, 1, { showDropDown: true }), changeDVRule(1, 1, { showDropDown: false }), null, []);
            testRunner().runTest(changeDVRule(1, 1, { ignoreEmpty: true }),  changeDVRule(1, 1, { ignoreEmpty: false }),  null, []);
        });
        it("should fail to create overlapping rules", () => {
            testRunner().expectError(changeDVRule(1, 0, "A1:D4"), changeDVRule(1, 2, "B2:E5"));
        });
        it("should fail to reduce type/value properties", () => {
            testRunner().expectError(changeDVRule(1, 1, { type: "all" }), changeDVRule(1, 1, { type: "list" }));
            testRunner().expectBidiError(changeDVRule(1, 1, { type: "all" }), changeDVRule(1, 1, { compare: "between" }));
            testRunner().expectBidiError(changeDVRule(1, 1, { type: "all" }), changeDVRule(1, 1, { value1: "A1" }));
            testRunner().expectBidiError(changeDVRule(1, 1, { type: "all" }), changeDVRule(1, 1, { value2: "A1" }));
        });
    });

    // conditional formatting -------------------------------------------------

    describe("conditional formatting and independent operations", () => {
        it("should skip transformations", () => {
            testRunner().runBidiTest(cfRuleOps(1), [
                GLOBAL_OPS, STYLESHEET_OPS, noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });

    // insertCFRule -----------------------------------------------------------

    describe('"insertCFRule" and "insertCFRule"', () => {
        it("should skip different rules", () => {
            testRunner().runTest(insertCFRule(1, "R1", "A1:D4"), insertCFRule(2, "R1", "A1:D4"));
            testRunner().runTest(insertCFRule(1, "R1", "A1:D4"), insertCFRule(1, "R2", "E5:H8"));
        });
        it("should update rules when inserting the same rule", () => {
            testRunner().runTest(
                insertCFRule(1, "R1", "A1:D4"), insertCFRule(1, "R1", "B2:E5", { stop: true }),
                changeCFRule(1, "R1", "A1:D4"), changeCFRule(1, "R1", { stop: true })
            );
            testRunner().runTest(
                insertCFRule(1, "R1", "A1:D4"), insertCFRule(1, "R1", "B2:E5"),
                changeCFRule(1, "R1", "A1:D4"), []
            );
        });
        it("should succeed to create overlapping rules", () => {
            testRunner().runTest(insertCFRule(1, "R1", "A1:D4"), insertCFRule(1, "R2", "B2:E5"));
        });
        it("should shift rule indexes (indexed mode)", () => {
            odfTestRunner().runTest(insertCFRule(1, "0", "A1:D4"), insertCFRule(1, "2", "B2:E5"), insertCFRule(1, "0", "A1:D4"), insertCFRule(1, "3", "B2:E5"));
            odfTestRunner().runTest(insertCFRule(1, "1", "A1:D4"), insertCFRule(1, "2", "B2:E5"), insertCFRule(1, "1", "A1:D4"), insertCFRule(1, "3", "B2:E5"));
            odfTestRunner().runTest(insertCFRule(1, "2", "A1:D4"), insertCFRule(1, "2", "B2:E5"), insertCFRule(1, "3", "A1:D4"), insertCFRule(1, "2", "B2:E5"));
            odfTestRunner().runTest(insertCFRule(1, "3", "A1:D4"), insertCFRule(1, "2", "B2:E5"), insertCFRule(1, "4", "A1:D4"), insertCFRule(1, "2", "B2:E5"));
            odfTestRunner().runTest(insertCFRule(1, "4", "A1:D4"), insertCFRule(1, "2", "B2:E5"), insertCFRule(1, "5", "A1:D4"), insertCFRule(1, "2", "B2:E5"));
        });
        it("should fail for malformed rule indexes (indexed mode)", () => {
            odfTestRunner().expectError(insertCFRule(1, "R1", "A1:D4"), insertCFRule(1, "2", "B2:E5"));
            odfTestRunner().expectError(insertCFRule(1, "1", "A1:D4"), insertCFRule(1, "R2", "B2:E5"));
        });
    });

    describe('"insertCFRule" and "deleteCFRule"', () => {
        it("should skip different rules", () => {
            testRunner().runBidiTest(insertCFRule(1, "R1", "A1:D4"), deleteCFRule(2, "R1"));
            testRunner().runBidiTest(insertCFRule(1, "R1", "A1:D4"), deleteCFRule(1, "R2"));
        });
        it("should fail to insert an existing rule", () => {
            testRunner().expectBidiError(insertCFRule(1, "R1", "A1:D4"), deleteCFRule(1, "R1"));
        });
        it("should shift rule indexes (indexed mode)", () => {
            odfTestRunner().runBidiTest(insertCFRule(1, "0", "A1:D4"), deleteCFRule(1, "2"), insertCFRule(1, "0", "A1:D4"), deleteCFRule(1, "3"));
            odfTestRunner().runBidiTest(insertCFRule(1, "1", "A1:D4"), deleteCFRule(1, "2"), insertCFRule(1, "1", "A1:D4"), deleteCFRule(1, "3"));
            odfTestRunner().runBidiTest(insertCFRule(1, "2", "A1:D4"), deleteCFRule(1, "2"), insertCFRule(1, "2", "A1:D4"), deleteCFRule(1, "3"));
            odfTestRunner().runBidiTest(insertCFRule(1, "3", "A1:D4"), deleteCFRule(1, "2"), insertCFRule(1, "2", "A1:D4"), deleteCFRule(1, "2"));
            odfTestRunner().runBidiTest(insertCFRule(1, "4", "A1:D4"), deleteCFRule(1, "2"), insertCFRule(1, "3", "A1:D4"), deleteCFRule(1, "2"));
        });
        it("should fail for malformed rule indexes (indexed mode)", () => {
            odfTestRunner().expectBidiError(insertCFRule(1, "R1", "A1:D4"), deleteCFRule(1, "2"));
            odfTestRunner().expectBidiError(insertCFRule(1, "1", "A1:D4"), deleteCFRule(1, "R2"));
        });
    });

    describe('"insertCFRule" and "changeCFRule"', () => {
        it("should skip different rules", () => {
            testRunner().runBidiTest(insertCFRule(1, "R1", "A1:D4"), changeCFRule(2, "R1", "A1:D4"));
            testRunner().runBidiTest(insertCFRule(1, "R1", "A1:D4"), changeCFRule(1, "R2", "E5:H8"));
        });
        it("should fail to insert an existing rule", () => {
            testRunner().expectBidiError(insertCFRule(1, "R1", "A1:D4"), changeCFRule(1, "R1", "E5:H8"));
        });
        it("should succeed to create overlapping rules", () => {
            testRunner().runBidiTest(insertCFRule(1, "R1", "A1:D4"), changeCFRule(1, "R2", "B2:E5"));
        });
        it("should shift rule indexes (indexed mode)", () => {
            odfTestRunner().runBidiTest(insertCFRule(1, "0", "A1:D4"), changeCFRule(1, "2", "B2:E5"), null, changeCFRule(1, "3", "B2:E5"));
            odfTestRunner().runBidiTest(insertCFRule(1, "1", "A1:D4"), changeCFRule(1, "2", "B2:E5"), null, changeCFRule(1, "3", "B2:E5"));
            odfTestRunner().runBidiTest(insertCFRule(1, "2", "A1:D4"), changeCFRule(1, "2", "B2:E5"), null, changeCFRule(1, "3", "B2:E5"));
            odfTestRunner().runBidiTest(insertCFRule(1, "3", "A1:D4"), changeCFRule(1, "2", "B2:E5"), null, changeCFRule(1, "2", "B2:E5"));
            odfTestRunner().runBidiTest(insertCFRule(1, "4", "A1:D4"), changeCFRule(1, "2", "B2:E5"), null, changeCFRule(1, "2", "B2:E5"));
        });
        it("should fail for malformed rule indexes (indexed mode)", () => {
            odfTestRunner().expectBidiError(insertCFRule(1, "R1", "A1:D4"), changeCFRule(1, "2", "B2:E5"));
            odfTestRunner().expectBidiError(insertCFRule(1, "1", "A1:D4"), changeCFRule(1, "R2", "B2:E5"));
        });
    });

    // deleteCFRule -----------------------------------------------------------

    describe('"deleteCFRule" and "deleteCFRule"', () => {
        it("should skip different rules", () => {
            testRunner().runTest(deleteCFRule(1, "R1"), deleteCFRule(2, "R1"));
            testRunner().runTest(deleteCFRule(1, "R1"), deleteCFRule(1, "R2"));
        });
        it('should set both operations to "removed" state', () => {
            testRunner().runTest(deleteCFRule(1, "R1"), deleteCFRule(1, "R1"), [], []);
        });
        it("should shift rule indexes (indexed mode)", () => {
            odfTestRunner().runTest(deleteCFRule(1, "0"), deleteCFRule(1, "2"), deleteCFRule(1, "0"), deleteCFRule(1, "1"));
            odfTestRunner().runTest(deleteCFRule(1, "1"), deleteCFRule(1, "2"), deleteCFRule(1, "1"), deleteCFRule(1, "1"));
            odfTestRunner().runTest(deleteCFRule(1, "2"), deleteCFRule(1, "2"), [],                   []);
            odfTestRunner().runTest(deleteCFRule(1, "3"), deleteCFRule(1, "2"), deleteCFRule(1, "2"), deleteCFRule(1, "2"));
            odfTestRunner().runTest(deleteCFRule(1, "4"), deleteCFRule(1, "2"), deleteCFRule(1, "3"), deleteCFRule(1, "2"));
        });
        it("should fail for malformed rule indexes (indexed mode)", () => {
            odfTestRunner().expectError(deleteCFRule(1, "R1"), deleteCFRule(1, "2"));
            odfTestRunner().expectError(deleteCFRule(1, "1"), deleteCFRule(1, "R2"));
        });
    });

    describe('"deleteCFRule" and "changeCFRule', () => {
        it("should skip different rules", () => {
            testRunner().runBidiTest(deleteCFRule(2, "R1"), changeCFRule(1, "R1", "A1:D4"));
            testRunner().runBidiTest(deleteCFRule(1, "R2"), changeCFRule(1, "R1", "A1:D4"));
        });
        it('should set change operation to "removed" state', () => {
            testRunner().runBidiTest(deleteCFRule(1, "R1"), changeCFRule(1, "R1", "A1:D4"), null, []);
        });
        it("should shift rule indexes (indexed mode)", () => {
            odfTestRunner().runBidiTest(deleteCFRule(1, "0"), changeCFRule(1, "2", "B2:E5"), null, changeCFRule(1, "1", "B2:E5"));
            odfTestRunner().runBidiTest(deleteCFRule(1, "1"), changeCFRule(1, "2", "B2:E5"), null, changeCFRule(1, "1", "B2:E5"));
            odfTestRunner().runBidiTest(deleteCFRule(1, "2"), changeCFRule(1, "2", "B2:E5"), null, []);
            odfTestRunner().runBidiTest(deleteCFRule(1, "3"), changeCFRule(1, "2", "B2:E5"), null, changeCFRule(1, "2", "B2:E5"));
            odfTestRunner().runBidiTest(deleteCFRule(1, "4"), changeCFRule(1, "2", "B2:E5"), null, changeCFRule(1, "2", "B2:E5"));
        });
        it("should fail for malformed rule indexes (indexed mode)", () => {
            odfTestRunner().expectBidiError(deleteCFRule(1, "R1"), changeCFRule(1, "2", "B2:E5"));
            odfTestRunner().expectBidiError(deleteCFRule(1, "1"), changeCFRule(1, "R2", "B2:E5"));
        });
    });

    // changeCFRule -----------------------------------------------------------

    describe('"changeCFRule" and "changeCFRule"', () => {
        it("should skip different rules", () => {
            testRunner().runTest(changeCFRule(1, "R1", "A1:D4"), changeCFRule(2, "R1", "A1:D4"));
            testRunner().runTest(changeCFRule(1, "R1", "A1:D4"), changeCFRule(1, "R2", "E5:H8"));
        });
        it("should reduce various properties", () => {
            testRunner().runTest(changeCFRule(1, "R1", "A1:D4"),                changeCFRule(1, "R1", "B2:E5"),                 null, []);
            testRunner().runTest(changeCFRule(1, "R1", "A1:D4"),                changeCFRule(1, "R1", "A1:D4"),                 [],   []);
            testRunner().runTest(changeCFRule(1, "R1", { type: "formula" }),    changeCFRule(1, "R1", { type: "formula" }),     [],   []);
            testRunner().runTest(changeCFRule(1, "R1", { priority: 1 }),        changeCFRule(1, "R1", { priority: 2 }),         null, []);
            testRunner().runTest(changeCFRule(1, "R1", { priority: 1 }),        changeCFRule(1, "R1", { priority: 1 }),         [],   []);
            testRunner().runTest(changeCFRule(1, "R1", { stop: true }),         changeCFRule(1, "R1", { stop: false }),         null, []);
            testRunner().runTest(changeCFRule(1, "R1", { stop: true }),         changeCFRule(1, "R1", { stop: true }),          [],   []);
            testRunner().runTest(changeCFRule(1, "R1", { attrs: ATTRS_I1 }),    changeCFRule(1, "R1", { attrs: ATTRS_I2 }),     null, []);
            testRunner().runTest(changeCFRule(1, "R1", { attrs: ATTRS_I1 }),    changeCFRule(1, "R1", { attrs: ATTRS_I1 }),     [],   []);
        });
        it("should succeed to create overlapping rules", () => {
            testRunner().runTest(changeCFRule(1, "R1", "A1:D4"), changeCFRule(1, "R2", "B2:E5"));
        });
        it("should fail to reduce type/value properties", () => {
            testRunner().expectError(changeCFRule(1, "R1", { type: "formula" }), changeCFRule(1, "R1", { type: "between" }));
            testRunner().expectBidiError(changeCFRule(1, "R1", { type: "formula" }), changeCFRule(1, "R1", { value1: "A1" }));
            testRunner().expectBidiError(changeCFRule(1, "R1", { type: "formula" }), changeCFRule(1, "R1", { value2: "A1" }));
            testRunner().expectBidiError(changeCFRule(1, "R1", { type: "formula" }), changeCFRule(1, "R1", { colorScale: {} }));
        });
    });
});
