/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { generateCurrencyCode } from "@/io.ox/office/editframework/model/formatter/currencycodes";
import { NumberFormatter } from "@/io.ox/office/spreadsheet/model/numberformatter";

import { createSpreadsheetApp } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/numberformatter", () => {

    // class NumberFormatter --------------------------------------------------

    describe("class NumberFormatter", () => {

        it("should exist", () => {
            expect(NumberFormatter).toBeFunction();
        });

        let docModel, numberFormatter;
        createSpreadsheetApp("ooxml").done(function (docApp) {
            docModel = docApp.docModel;
            numberFormatter = docModel.numberFormatter;
        });

        describe("method formatValue", () => {
            it("should exist", () => {
                expect(NumberFormatter).toHaveMethod("formatValue");
            });
        });

        describe("method parseScalarValue", () => {
            it("should exist", () => {
                expect(NumberFormatter).toHaveMethod("parseScalarValue");
            });
            it("should parse text", () => {
                expect(numberFormatter.parseScalarValue("text")).toEqual({ value: "text", format: 0 });
                expect(numberFormatter.parseScalarValue("")).toEqual({ value: "", format: 0 });
                expect(numberFormatter.parseScalarValue("  ")).toEqual({ value: "  ", format: 0 });
                expect(numberFormatter.parseScalarValue("", { blank: true })).toEqual({ value: null, format: 0 });
                expect(numberFormatter.parseScalarValue("  ", { blank: true })).toEqual({ value: "  ", format: 0 });
            });
            it("should parse booleans", () => {
                expect(numberFormatter.parseScalarValue("WAHR")).toEqual({ value: true, format: 0 });
                expect(numberFormatter.parseScalarValue("falsch")).toEqual({ value: false, format: 0 });
                expect(numberFormatter.parseScalarValue(" falsch")).toEqual({ value: " falsch", format: 0 });
                expect(numberFormatter.parseScalarValue("false")).toEqual({ value: "false", format: 0 });
            });
            it("should parse error codes", () => {
                expect(numberFormatter.parseScalarValue("#BEZUG!")).toEqual({ value: ErrorCode.REF, format: 0 });
                expect(numberFormatter.parseScalarValue("#name?")).toEqual({ value: ErrorCode.NAME, format: 0 });
                expect(numberFormatter.parseScalarValue("#name!")).toEqual({ value: "#name!", format: 0 });
                expect(numberFormatter.parseScalarValue(" #name?")).toEqual({ value: " #name?", format: 0 });
            });
            it("should handle apostrophes", () => {
                expect(numberFormatter.parseScalarValue("'0")).toEqual({ value: "0", format: 0 });
                expect(numberFormatter.parseScalarValue("'wahr")).toEqual({ value: "wahr", format: 0 });
                expect(numberFormatter.parseScalarValue("'#name?")).toEqual({ value: "#name?", format: 0 });
                expect(numberFormatter.parseScalarValue("'0", { keepApos: true })).toEqual({ value: "'0", format: 0 });
            });
            it("should parse numbers", () => {
                expect(numberFormatter.parseScalarValue("1")).toEqual({ value: 1, format: 0 });
                expect(numberFormatter.parseScalarValue("- 1.234")).toEqual({ value: -1234, format: 3 });
                expect(numberFormatter.parseScalarValue("(1.234,5)")).toEqual({ value: -1234.5, format: 4 });
                expect(numberFormatter.parseScalarValue("42,5e-03")).toEqual({ value: 0.0425, format: 11 });
                expect(numberFormatter.parseScalarValue("1.234 4/10")).toEqual({ value: 1234.4, format: 13 });
                expect(numberFormatter.parseScalarValue("- %123e2")).toEqual({ value: -123, format: 10 });
            });
            it("should parse currency values", () => {
                const EUR_INT_CODE = generateCurrencyCode({ locale: "de-DE", patternType: "currency", negativeRed: true, groupSep: true, fracDigits: 0 });
                expect(numberFormatter.parseScalarValue("1€")).toEqual({ value: 1, format: EUR_INT_CODE });
                const USD_DEC_CODE = generateCurrencyCode({ locale: "en-US", patternType: "currency", negativeRed: true, groupSep: true });
                expect(numberFormatter.parseScalarValue("$(1,2)")).toEqual({ value: -1.2, format: USD_DEC_CODE });
            });
        });
    });
});
