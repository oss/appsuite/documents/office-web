/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Color } from "@/io.ox/office/editframework/utils/color";
import { MergeMode, Address } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { UsedRangeCollection } from "@/io.ox/office/spreadsheet/model/usedrangecollection";
import { MergeCollection } from "@/io.ox/office/spreadsheet/model/mergecollection";

import { a, r, ra, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/mergecollection", () => {

    // the operations to be applied by the document model
    const charAttrs1   = { fontName: "Arial", fontSize: 13, bold: true, italic: false, underline: false, strike: "none", color: Color.DARK1 },
        charAttrs2   = { fontName: "Arial", fontSize: 11, bold: false, italic: false, underline: false, strike: "none", color: Color.DARK1 },
        noBorder     = { style: "none" },
        singleBorder = { style: "single", width: 26, color: Color.AUTO },
        doubleBorder = { style: "double", width: 78, color: Color.AUTO },
        applyAttrs1  = { font: true, fill: true, border: false, align: false, number: false, protect: false },
        applyAttrs2  = { font: true, fill: true, border: true, align: false, number: false, protect: false };

    function borders(lb, rb, tb, bb) {
        return { borderLeft: lb, borderRight: rb, borderTop: tb, borderBottom: bb, borderDown: noBorder, borderUp: noBorder };
    }

    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),

        op.insertAutoStyle("a0", true),
        op.insertAutoStyle("a1",  { apply: applyAttrs1, character: charAttrs1, cell: borders(noBorder, noBorder, noBorder, noBorder) }),
        op.insertAutoStyle("a2",  { apply: applyAttrs2, character: charAttrs1, cell: borders(noBorder, noBorder, noBorder, noBorder) }),
        op.insertAutoStyle("a3",  { apply: applyAttrs2, character: charAttrs2, cell: borders(singleBorder, noBorder, singleBorder, noBorder) }),   // border top/left corner
        op.insertAutoStyle("a4",  { apply: applyAttrs2, character: charAttrs2, cell: borders(noBorder, singleBorder, singleBorder, noBorder) }),   // border top/right corner
        op.insertAutoStyle("a5",  { apply: applyAttrs2, character: charAttrs2, cell: borders(singleBorder, noBorder, noBorder, singleBorder) }),   // border bottom/left corner
        op.insertAutoStyle("a6",  { apply: applyAttrs2, character: charAttrs2, cell: borders(noBorder, singleBorder, noBorder, singleBorder) }),   // border bottom/right corner
        op.insertAutoStyle("a7",  { apply: applyAttrs2, character: charAttrs2, cell: borders(noBorder, noBorder, singleBorder, noBorder) }),       // border top
        op.insertAutoStyle("a8",  { apply: applyAttrs2, character: charAttrs2, cell: borders(noBorder, singleBorder, noBorder, noBorder) }),       // border right
        op.insertAutoStyle("a9",  { apply: applyAttrs2, character: charAttrs2, cell: borders(noBorder, noBorder, noBorder, singleBorder) }),       // border bottom
        op.insertAutoStyle("a10", { apply: applyAttrs2, character: charAttrs2, cell: borders(singleBorder, noBorder, noBorder, noBorder) }),       // border left
        op.insertAutoStyle("a11", { apply: applyAttrs2, character: charAttrs2, cell: borders(noBorder, noBorder, noBorder, noBorder) }),           // no border
        op.insertAutoStyle("a12", { apply: applyAttrs2, character: charAttrs2, cell: borders(noBorder, noBorder, doubleBorder, noBorder) }),       // special border top

        op.insertSheet(0, "Sheet1"),
        op.changeCells(0, "A5", [
            [{ v: 1, s: "a1" }, 4, "a"],
            [2, { v: 5, s: "a2" }, "b"],
            [3, 6, { v: "c", s: "a1" }]
        ]),
        op.changeCells(0, "E5", [
            [{ s: "a1" }, null, "a"],
            [null, { v: 5, s: "a2" }, "b"],
            [3, 6, { v: "c", s: "a1" }]
        ]),

        op.insertSheet(1, "Sheet2"),
        op.mergeCells(1, "A5:C7 E5:G7"),
        op.changeCells(1, { "E5:G7": { s: "a1" } }),

        op.insertSheet(2, "Sheet4"),
        op.changeCells(2, "E5", [
            [{ v: 1, s: "a3"  }, { v: 4, s: "a7" }, { v: "a", s: "a4" }],
            [{ v: 2, s: "a10" }, { v: 5          }, { v: "b", s: "a8" }],
            [{ v: 3, s: "a5"  }, { v: 6, s: "a9" }, { v: "c", s: "a6" }]
        ]),

        op.insertSheet(3, "Sheet5"),
        op.changeCells(3, "E5", [
            [{ v: 1, s: "a3"  }, { v: 4, s: "a12" }, { v: "a", s: "a4" }],
            [{ v: 2, s: "a10" }, { v: 5           }, { v: "b", s: "a8" }],
            [{ v: 3, s: "a5"  }, { v: 6, s: "a9"  }, { v: "c", s: "a6" }]
        ]),

        op.insertSheet(4, "Sheet6"),
        op.changeCells(4, "E5", [
            [{ v: 1, s: "a3"  }, { v: 4, s: "a7"  }, { v: "a", s: "a4" }],
            [{ v: 2, s: "a10" }, { v: 5           }, { v: "b", s: "a8" }],
            [{ v: 3, s: "a10" }, { v: 6, s: "a10" }, { v: "c", s: "a8" }],
            [{ v: 4, s: "a10" }, { v: 7           }, { v: "d", s: "a8" }],
            [{ v: 5, s: "a5"  }, { v: 8, s: "a9"  }, { v: "e", s: "a6" }]
        ])
    ];

    // initialize test document
    let docModel;
    createSpreadsheetApp("ooxml", OPERATIONS).done(function (docApp) {
        docModel = docApp.docModel;
    });

    function getMergeCollection(sheet) {
        return docModel.getSheetModel(sheet).mergeCollection;
    }

    function expectMergedRanges(sheet, ranges) {
        const mergedRanges = getMergeCollection(sheet).getMergedRanges(r("A1:XFD1048576")).sort();
        expect(mergedRanges).toStringifyTo(ranges);
    }

    function getCellCollection(sheet) {
        return docModel.getSheetModel(sheet).cellCollection;
    }

    function expectValueMatrix(sheet, start, values) {
        const cellCollection = getCellCollection(sheet);
        start = a(start);
        values.forEach(function (vector, row) {
            vector.forEach(function (value, col) {
                const address = new Address(start.c + col, start.r + row);
                expect(cellCollection.getValue(address)).toBe(value);
            });
        });
    }

    function expectStyleMatrix(sheet, start, styles) {
        const cellCollection = getCellCollection(sheet);
        start = a(start);
        styles.forEach(function (vector, row) {
            vector.forEach(function (style, col) {
                const address = new Address(start.c + col, start.r + row);
                expect(cellCollection.getStyleId(address)).toBe(style);
            });
        });
    }

    // class MergeCollection --------------------------------------------------

    describe("class MergeCollection", () => {

        it("should subclass UsedRangeCollection", () => {
            expect(MergeCollection).toBeSubClassOf(UsedRangeCollection);
        });

        describe("method generateMergeCellsOperations", () => {
            it("should exist", () => {
                expect(MergeCollection).toHaveMethod("generateMergeCellsOperations");
            });

            it("should merge cell ranges and move first value cells", async () => {
                const sheetModel = docModel.getSheetModel(0);
                await sheetModel.createAndApplyOperations(generator => {
                    return sheetModel.mergeCollection.generateMergeCellsOperations(generator, ra("A5:C7 E5:G7"), MergeMode.MERGE);
                });
                expectMergedRanges(0, "A5:C7,E5:G7");
                expectValueMatrix(0, "A5", [[1, null, null], [null, null, null], [null, null, null]]);
                expectStyleMatrix(0, "A5", [["a1", "a1", "a1"], ["a1", "a1", "a1"], ["a1", "a1", "a1"]]);
                expectValueMatrix(0, "E5", [["a", null, null], [null, null, null], [null, null, null]]);
                expectStyleMatrix(0, "E5", [["a0", "a0", "a0"], ["a0", "a0", "a0"], ["a0", "a0", "a0"]]);
                await docModel.undoManager.undo();
                expectMergedRanges(0, "");
                expectValueMatrix(0, "A5", [[1, 4, "a"], [2, 5, "b"], [3, 6, "c"]]);
                expectStyleMatrix(0, "A5", [["a1", "a0", "a0"], ["a0", "a2", "a0"], ["a0", "a0", "a1"]]);
                expectValueMatrix(0, "E5", [[null, null, "a"], [null, 5, "b"], [3, 6, "c"]]);
                expectStyleMatrix(0, "E5", [["a1", "a0", "a0"], ["a0", "a2", "a0"], ["a0", "a0", "a1"]]);
            });

            it("should merge cell ranges horizontally and move first value cells", async () => {
                const sheetModel = docModel.getSheetModel(0);
                await sheetModel.createAndApplyOperations(generator => {
                    return sheetModel.mergeCollection.generateMergeCellsOperations(generator, ra("A5:C7 E5:G7"), MergeMode.HORIZONTAL);
                });
                expectMergedRanges(0, "A5:C5,E5:G5,A6:C6,E6:G6,A7:C7,E7:G7");
                expectValueMatrix(0, "A5", [[1, null, null], [2, null, null], [3, null, null]]);
                expectStyleMatrix(0, "A5", [["a1", "a1", "a1"], ["a0", "a0", "a0"], ["a0", "a0", "a0"]]);
                expectValueMatrix(0, "E5", [["a", null, null], [5, null, null], [3, null, null]]);
                expectStyleMatrix(0, "E5", [["a0", "a0", "a0"], ["a2", "a2", "a2"], ["a0", "a0", "a0"]]);
                await docModel.undoManager.undo();
                expectMergedRanges(0, "");
                expectValueMatrix(0, "A5", [[1, 4, "a"], [2, 5, "b"], [3, 6, "c"]]);
                expectStyleMatrix(0, "A5", [["a1", "a0", "a0"], ["a0", "a2", "a0"], ["a0", "a0", "a1"]]);
                expectValueMatrix(0, "E5", [[null, null, "a"], [null, 5, "b"], [3, 6, "c"]]);
                expectStyleMatrix(0, "E5", [["a1", "a0", "a0"], ["a0", "a2", "a0"], ["a0", "a0", "a1"]]);
            });

            it("should merge cell ranges vertically and move first value cells", async () => {
                const sheetModel = docModel.getSheetModel(0);
                await sheetModel.createAndApplyOperations(generator => {
                    return sheetModel.mergeCollection.generateMergeCellsOperations(generator, ra("A5:C7 E5:G7"), MergeMode.VERTICAL);
                });
                expectMergedRanges(0, "A5:A7,B5:B7,C5:C7,E5:E7,F5:F7,G5:G7");
                expectValueMatrix(0, "A5", [[1, 4, "a"], [null, null, null], [null, null, null]]);
                expectStyleMatrix(0, "A5", [["a1", "a0", "a0"], ["a1", "a0", "a0"], ["a1", "a0", "a0"]]);
                expectValueMatrix(0, "E5", [[3, 5, "a"], [null, null, null], [null, null, null]]);
                expectStyleMatrix(0, "E5", [["a0", "a2", "a0"], ["a0", "a2", "a0"], ["a0", "a2", "a0"]]);
                await docModel.undoManager.undo();
                expectMergedRanges(0, "");
                expectValueMatrix(0, "A5", [[1, 4, "a"], [2, 5, "b"], [3, 6, "c"]]);
                expectStyleMatrix(0, "A5", [["a1", "a0", "a0"], ["a0", "a2", "a0"], ["a0", "a0", "a1"]]);
                expectValueMatrix(0, "E5", [[null, null, "a"], [null, 5, "b"], [3, 6, "c"]]);
                expectStyleMatrix(0, "E5", [["a1", "a0", "a0"], ["a0", "a2", "a0"], ["a0", "a0", "a1"]]);
            });

            it("should unmerge cell ranges", async () => {
                const sheetModel = docModel.getSheetModel(1);
                await sheetModel.createAndApplyOperations(generator => {
                    return sheetModel.mergeCollection.generateMergeCellsOperations(generator, r("D6:F6"), MergeMode.UNMERGE);
                });
                expectMergedRanges(1, "A5:C7");
                expectStyleMatrix(1, "E5", [["a1", "a1", "a1"], ["a1", "a1", "a1"], ["a1", "a1", "a1"]]);
                await docModel.undoManager.undo();
                expectMergedRanges(1, "A5:C7,E5:G7");
                expectStyleMatrix(1, "E5", [["a1", "a1", "a1"], ["a1", "a1", "a1"], ["a1", "a1", "a1"]]);
            });

            it("should unmerge cell ranges implicitly when merging", async () => {
                const sheetModel = docModel.getSheetModel(1);
                await sheetModel.createAndApplyOperations(generator => {
                    return sheetModel.mergeCollection.generateMergeCellsOperations(generator, r("D6:F6"), MergeMode.MERGE);
                });
                expectMergedRanges(1, "A5:C7,D6:F6");
                expectStyleMatrix(1, "E5", [["a1", "a1", "a1"], ["a0", "a0", "a1"], ["a1", "a1", "a1"]]);
                await docModel.undoManager.undo();
                expectMergedRanges(1, "A5:C7,E5:G7");
                expectStyleMatrix(1, "E5", [["a1", "a1", "a1"], ["a1", "a1", "a1"], ["a1", "a1", "a1"]]);
            });

            it("should merge with framed border", async () => {
                const sheetModel = docModel.getSheetModel(2);
                await sheetModel.createAndApplyOperations(generator => {
                    return sheetModel.mergeCollection.generateMergeCellsOperations(generator, r("E5:G7"), MergeMode.MERGE);
                }, { applyImmediately: true });
                expectMergedRanges(2, "E5:G7");
                expectValueMatrix(2, "E5", [[1, null, null], [null, null, null], [null, null, null]]);
                expectStyleMatrix(2, "E5", [["a3", "a7", "a4"], ["a10", "a11", "a8"], ["a5", "a9", "a6"]]);
                await docModel.undoManager.undo();
                expectMergedRanges(2, "");
                expectValueMatrix(2, "E5", [[1, 4, "a"], [2, 5, "b"], [3, 6, "c"]]);
                expectStyleMatrix(2, "E5", [["a3", "a7", "a4"], ["a10", "a0", "a8"], ["a5", "a9", "a6"]]);
            });

            it("should merge with framed border except the top one", async () => {
                const sheetModel = docModel.getSheetModel(3);
                await sheetModel.createAndApplyOperations(generator => {
                    return sheetModel.mergeCollection.generateMergeCellsOperations(generator, r("E5:G7"), MergeMode.MERGE);
                }, { applyImmediately: true });
                expectMergedRanges(3, "E5:G7");
                expectValueMatrix(3, "E5", [[1, null, null], [null, null, null], [null, null, null]]);
                expectStyleMatrix(3, "E5", [["a10", "a11", "a8"], ["a10", "a11", "a8"], ["a5", "a9", "a6"]]);
                await docModel.undoManager.undo();
                expectMergedRanges(3, "");
                expectValueMatrix(3, "E5", [[1, 4, "a"], [2, 5, "b"], [3, 6, "c"]]);
                expectStyleMatrix(3, "E5", [["a3", "a12", "a4"], ["a10", "a0", "a8"], ["a5", "a9", "a6"]]);
            });

            it("should merge with framed border and reset the middle cells", async () => {
                const sheetModel = docModel.getSheetModel(4);
                await sheetModel.createAndApplyOperations(function (generator) {
                    return sheetModel.mergeCollection.generateMergeCellsOperations(generator, r("E5:G9"), MergeMode.MERGE);
                }, { applyImmediately: true });
                expectMergedRanges(4, "E5:G9");
                expectValueMatrix(4, "E5", [[1, null, null], [null, null, null], [null, null, null], [null, null, null], [null, null, null]]);
                expectStyleMatrix(4, "E5", [["a3", "a7", "a4"], ["a10", "a11", "a8"], ["a10", "a11", "a8"], ["a10", "a11", "a8"], ["a5", "a9", "a6"]]);
                await docModel.undoManager.undo();
                expectMergedRanges(4, "");
                expectValueMatrix(4, "E5", [[1, 4, "a"], [2, 5, "b"], [3, 6, "c"], [4, 7, "d"], [5, 8, "e"]]);
                expectStyleMatrix(4, "E5", [["a3", "a7", "a4"], ["a10", "a0", "a8"], ["a10", "a10", "a8"], ["a10", "a0", "a8"], ["a5", "a9", "a6"]]);
            });
        });
    });
});
