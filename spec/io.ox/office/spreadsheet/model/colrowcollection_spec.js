/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { convertLength } from "@/io.ox/office/tk/dom";
import { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { SheetChildModel } from "@/io.ox/office/spreadsheet/model/sheetchildmodel";
import { ColModel } from "@/io.ox/office/spreadsheet/model/colrowmodel";
import { ColRowCollection } from "@/io.ox/office/spreadsheet/model/colrowcollection";

import { i } from "~/spreadsheet/sheethelper";
import { createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/colrowcollection", () => {

    // the operations to be applied by the document model
    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertAutoStyle("a1"),
        op.insertAutoStyle("a2"),
        op.insertSheet(0, "Sheet1", { column: { width: 9 } }),
        op.changeRows(0, "1", "a1", { row: { customFormat: true } }),
        op.changeRows(0, "2:3", "a2", { row: { customFormat: true } }),
        op.changeRows(0, "4:5 7", "a1", { row: { customFormat: true } })
    ];

    // initialize test document
    let docModel, sheetModel, colCollection, rowCollection, colFactor = 1;
    createSpreadsheetApp("ooxml", OPERATIONS).done(function (app) {
        docModel = app.docModel;
        sheetModel = docModel.getSheetModel(0);
        colCollection = sheetModel.colCollection;
        rowCollection = sheetModel.rowCollection;
        colFactor = convertLength(docModel.fontMetrics.getDefaultDigitWidth(), "px", "mm") * 100;
    });

    function getModels(collection) {
        return Array.from(collection.yieldModels(i("1:10000")));
    }

    function getColOffset(index) {
        return index * Math.ceil(9 * colFactor);
    }

    function expectInterval(entryModel, expInt) {
        expect(entryModel).toHaveProperty("interval");
        expect(entryModel.interval.toOpStr(/^[A-Z]/.test(expInt))).toBe(expInt);
    }

    function expectStyleId(entryModel, expStyleId) {
        const styleUid = docModel.autoStyles.resolveStyleUid(expStyleId);
        expect(entryModel).toHaveProperty("suid", styleUid);
    }

    function expectModelData(entryModel, expInt, expStyleId) {
        expectInterval(entryModel, expInt);
        expectStyleId(entryModel, expStyleId);
    }

    // class ColRowCollection -------------------------------------------------

    describe("class ColRowCollection", () => {

        it("should subclass SheetChildModel", () => {
            expect(ColRowCollection).toBeSubClassOf(SheetChildModel);
            expect(colCollection).toBeInstanceOf(ColRowCollection);
            expect(rowCollection).toBeInstanceOf(ColRowCollection);
        });

        describe("method applyChangeOperation", () => {
            it("should exist", () => {
                expect(ColRowCollection).toHaveMethod("applyChangeOperation");
            });
            const changeCols = (...args) => new SheetOperationContext(docModel, op.changeCols(0, ...args));
            it("should insert a new column model with an auto-style", () => {
                colCollection.applyChangeOperation(changeCols("C:G", "a1"));
                const models = getModels(colCollection);
                expect(models).toHaveLength(1);
                const colModel1 = models[0];
                expect(colModel1).toBeInstanceOf(ColModel);
                expectModelData(colModel1, "C:G", "a1");
                expect(colModel1.size).toBeNumber();
                expect(colModel1).toHaveProperty("sizeHmm", getColOffset(1));
                expect(colModel1.offset).toBeNumber();
                expect(colModel1).toHaveProperty("offsetHmm", getColOffset(2));
                expect(colModel1.getExplicitEntryAttributes()).toEqual({});
            });
            it("should shorten column model at the beginning", () => {
                colCollection.applyChangeOperation(changeCols("B:C", "a2"));
                const models = getModels(colCollection);
                expect(models).toHaveLength(2);
                expectModelData(models[0], "B:C", "a2");
                expectModelData(models[1], "D:G", "a1");
            });
            it("should shorten column model at the end", () => {
                colCollection.applyChangeOperation(changeCols("G:H", "a2"));
                const models = getModels(colCollection);
                expect(models).toHaveLength(3);
                expectModelData(models[1], "D:F", "a1");
                expectModelData(models[2], "G:H", "a2");
            });
            it("should split column model in the middle", () => {
                colCollection.applyChangeOperation(changeCols("E", "a2"));
                const models = getModels(colCollection);
                expect(models).toHaveLength(5);
                expectModelData(models[1], "D", "a1");
                expectModelData(models[2], "E", "a2");
                expectModelData(models[3], "F", "a1");
            });
            it("should merge column models before", () => {
                colCollection.applyChangeOperation(changeCols("A", "a2"));
                const models = getModels(colCollection);
                expect(models).toHaveLength(5);
                expectModelData(models[0], "A:C", "a2");
            });
            it("should merge column models in the middle", () => {
                colCollection.applyChangeOperation(changeCols("D", "a2"));
                const models = getModels(colCollection);
                expect(models).toHaveLength(3);
                expectModelData(models[0], "A:E", "a2");
            });
            it("should merge column models after", () => {
                colCollection.applyChangeOperation(changeCols("I", "a2"));
                const models = getModels(colCollection);
                expect(models).toHaveLength(3);
                expectModelData(models[2], "G:I", "a2");
            });
            it("should remove column models with default style", () => {
                colCollection.applyChangeOperation(changeCols("A:B", ""));
                const models = getModels(colCollection);
                expect(models).toHaveLength(3);
                expectModelData(models[0], "C:E", "a2");
            });
            it("should insert a new column model with attributes", () => {
                colCollection.applyChangeOperation(changeCols("M:Q", { column: { visible: false } }));
                const models = getModels(colCollection);
                expect(models).toHaveLength(4);
                const colModel1 = models[3];
                expectModelData(colModel1, "M:Q", "a0");
                expect(colModel1.getExplicitEntryAttributes()).toEqual({ visible: false });
                expect(colModel1).toHaveProperty("sizeHmm", 0);
                expect(colModel1).toHaveProperty("offsetHmm", getColOffset(12));
            });
            it("should merge attributes of existing column models", () => {
                colCollection.applyChangeOperation(changeCols("P:S", { column: { width: 42 } }));
                const models = getModels(colCollection);
                expect(models).toHaveLength(6);
                const colModel1 = models[3];
                expectInterval(colModel1, "M:O");
                expect(colModel1.getExplicitEntryAttributes()).toEqual({ visible: false });
                expect(colModel1).toHaveProperty("offsetHmm", getColOffset(12));
                const colModel2 = models[4];
                expectInterval(colModel2, "P:Q");
                expect(colModel2.getExplicitEntryAttributes()).toEqual({ width: 42, visible: false });
                expect(colModel1).toHaveProperty("offsetHmm", getColOffset(12));
                const colModel3 = models[5];
                expectInterval(colModel3, "R:S");
                expect(colModel3.getExplicitEntryAttributes()).toEqual({ width: 42 });
                expect(colModel1).toHaveProperty("offsetHmm", getColOffset(12));
            });
            it("should remove column models with default attributes", () => {
                colCollection.applyChangeOperation(changeCols("O", { column: { visible: null } }));
                const models = getModels(colCollection);
                expect(models).toHaveLength(6);
                const colModel1 = models[3];
                expectInterval(colModel1, "M:N");
                expect(colModel1.getExplicitEntryAttributes()).toEqual({ visible: false });
                expect(colModel1).toHaveProperty("offsetHmm", getColOffset(12));
                const colModel2 = models[4];
                expectInterval(colModel2, "P:Q");
                expect(colModel2).toHaveProperty("offsetHmm", getColOffset(13));
                const colModel3 = models[5];
                expectInterval(colModel3, "R:S");
                expect(colModel3).toHaveProperty("offsetHmm", getColOffset(13));
            });
        });

        describe("method applyInsertOperation", () => {
            it("should exist", () => {
                expect(ColRowCollection).toHaveMethod("applyInsertOperation");
            });
        });

        describe("method applyDeleteOperation", () => {
            it("should exist", () => {
                expect(ColRowCollection).toHaveMethod("applyDeleteOperation");
            });
        });
    });
});
