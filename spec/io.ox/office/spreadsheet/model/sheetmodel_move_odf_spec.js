/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Color } from "@/io.ox/office/editframework/utils/color";
import { Direction } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { SheetModel } from "@/io.ox/office/spreadsheet/model/sheetmodel";

import { ia, borders, createSpreadsheetApp, defineModelTestWithUndo } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// constants ==================================================================

// color attribute values
const { AUTO, RED, BLUE } = Color;

// border attribute values
const NONE = { style: "none" };
const THIN = { style: "solid", width: 26, color: AUTO };
const THIN_RED = { style: "solid", width: 26, color: RED };

// hyperlink URLs
const URL1 = "http://example.org/1";
const URL2 = "http://example.org/2";

// test formula expressions
const FMLA = "SUM([.E5:.F6];[.L12:.M13];[$Sheet1.E5:.F6];[$Sheet1.L12:.M13];[$Sheet2.E5:.F6];[$Sheet2.L12:.M13])";
const NMFMLA = "SUM([.L12:.$M$13];[$Sheet1.L12:.$M$13];[$Sheet2.L12:.$M$13])";
const SHFMLA = "SUM([.J10:.K11];[$Sheet1.J10:.K11];[$Sheet2.J10:.K11])";
const TBFMLA = "SUM(Table11;Table21)";

// tests ======================================================================

describe("module spreadsheet/model/sheetmodel_move_odf", () => {

    // initialize test document
    const appPromise = createSpreadsheetApp("odf", [
        op.changeConfig({ document: { cols: 1024, rows: 1048576 } }),
        op.insertAutoStyle("ce00", true),
        op.insertAutoStyle("ce01", { cell: { fillColor: RED } }),
        op.insertAutoStyle("ce02", { cell: { foreColor: BLUE } }),
        op.insertAutoStyle("ce03", { cell: borders("tbl", THIN) }),
        op.insertAutoStyle("ce04", { cell: borders("tbr", THIN) }),
        op.insertAutoStyle("ce05", { cell: borders("tbl", THIN_RED) }),
        op.insertAutoStyle("ce06", { cell: borders("tbr", THIN_RED) }),
        op.insertAutoStyle("ce07", { cell: borders("tlr", THIN) }),
        op.insertAutoStyle("ce08", { cell: borders("blr", THIN) }),
        op.insertAutoStyle("ce09", { cell: borders("tlr", THIN_RED) }),
        op.insertAutoStyle("ce10", { cell: borders("blr", THIN_RED) }),

        // global names
        op.insertName("global", { formula: NMFMLA, ref: "[$Sheet1.$A$1]" }),

        // test sheet for move operations (insert/delete columns/rows)
        op.insertSheet(0, "Sheet1"),
        op.insertName("local", 0, { formula: NMFMLA, ref: "[$Sheet1.$A$1]" }),
        // column/row styles
        op.changeCols(0, "G", "ce01", { column: { width: 2 } }),
        op.changeCols(0, "H", "ce02", { column: { width: 4 } }),
        op.changeRows(0, "7", "ce01", { row: { height: 100, customFormat: true } }),
        op.changeRows(0, "8", "ce02", { row: { height: 200, customFormat: true } }),
        // cell styles
        op.changeCells(0, "E2", [
            [{ s: "ce01" }, { s: "ce01" }, { s: "ce03" }, { s: "ce04" }],
            [{ s: "ce01" }, { s: "ce02" }, { s: "ce03" }, { s: "ce06" }],
            [{ s: "ce02" }, { s: "ce02" }, { s: "ce05" }, { s: "ce06" }]
        ]),
        op.changeCells(0, "B5", [
            [{ s: "ce01" }, { s: "ce01" },  { s: "ce02" }],
            [{ s: "ce01" }, { s: "ce02" },  { s: "ce02" }],
            [{ s: "ce07" }, { s: "ce07" },  { s: "ce09" }],
            [{ s: "ce08" }, { s: "ce10" }, { s: "ce10" }]
        ]),
        // cell formulas
        op.changeCells(0, { A6: { v: 0, f: FMLA }, F1: { v: 0, f: FMLA } }),
        // matrix formulas
        op.changeCells(0, "A1", [[{ v: 0, f: FMLA, mr: "A1:B2" }, 0], [0, 0]]),
        op.changeCells(0, "F6", [[{ v: 0, f: FMLA, mr: "F6:G7" }, 0], [0, 0]]),
        op.changeCells(0, "J10", [[{ v: 0, f: FMLA, mr: "J10:K11" }, 0], [0, 0]]),
        // merged ranges
        op.mergeCells(0, "D11:E12 E13:F14 F15:G16 B15:D15 B16:D16 O1:O1048576"),
        // hyperlinks
        op.insertHlink(0, "L3:L4 N5:N6", URL1),
        op.insertHlink(0, "M4:M5 O1:O1048576", URL2),
        // table ranges
        op.insertTable(0, "Table11", "C3:D4", { table: { headerRow: true } }),
        op.changeCells(0, { C3: "Col1", D3: "Col2", C4: 42, D4: 42 }),
        op.insertTable(0, "Table12", "E1:H2", { table: { headerRow: true } }),
        op.changeCells(0, { E1: "Col1", F1: "Col2", G1: "X", H1: "Col4" }),
        op.insertTable(0, "Table13", "B5:E9", { table: { headerRow: true } }),
        op.changeCells(0, { B5: "Col1", C5: "Col2", D5: "X", E5: "Col4" }),
        op.insertTable(0, "Table14", "I10:I11", { table: { headerRow: true } }),
        op.insertTable(0, "Table15", "M10:M11", { table: { headerRow: true } }),
        op.insertTable(0, "Table16", "M14:M15", { table: { headerRow: true } }),
        op.insertTable(0, "Table17", "I14:I15", { table: { headerRow: true } }),
        op.changeCells(0, { I10: "Col1", M10: "Col1", I14: "Col1", M14: "Col1" }),
        // auto-filter
        op.insertTable(0, null, "A7:A16", { table: { headerRow: true } }),
        op.changeTableCol(0, null, 0, { attrs: { filter: { type: "discrete", entries: ["2", "4", "6", "8"] } } }),
        op.changeCells(0, "A7", [["Col1"], [1], [2], [3], [4], [5], [6], [7], [8], [9]]),
        op.changeRows(0, "8 10 12 14 16", { row: { visible: false, filtered: true } }),
        // data validation
        op.insertDVRule(0, "E5:F6", { type: "between", value1: SHFMLA, value2: SHFMLA, ref: "[$Sheet1.$E$5]" }),
        op.insertDVRule(0, "C3:D4", { type: "between", value1: SHFMLA, value2: SHFMLA, ref: "[$Sheet1.$C$3]" }),
        op.insertDVRule(0, "G7:H8", { type: "between", value1: SHFMLA, value2: SHFMLA, ref: "[$Sheet1.$G$7]" }),
        op.insertDVRule(0, "G6 F7", { type: "list", value1: '"a"', ref: "[$Sheet1.$F$6]" }),
        op.insertDVRule(0, "I9 M13", { type: "list", value1: '"a"', ref: "[$Sheet1.$I$9]" }),
        op.insertDVRule(0, "M9 I13", { type: "list", value1: '"a"', ref: "[$Sheet1.$I$9]" }),
        // conditional formatting
        op.insertCFRule(0, 0, "E5:F6", { type: "between", value1: SHFMLA, value2: SHFMLA }),
        op.insertCFRule(0, 1, "C3:D4", { type: "between", value1: SHFMLA, value2: SHFMLA }),
        op.insertCFRule(0, 2, "G7:H8", { type: "between", value1: SHFMLA, value2: SHFMLA }),
        op.insertCFRule(0, 3, "G6 F7", { type: "iconset" }),
        op.insertCFRule(0, 4, "I9 M13", { type: "iconset" }),
        op.insertCFRule(0, 5, "M9 I13", { type: "iconset" }),
        // shape objects
        op.insertShape(0, 0, { drawing: { anchor: "2 A1 0 0 C3 0 0" } }),
        op.insertPara(0, "0 0"),
        op.insertText(0, "0 0 0", "abc"),
        op.insertShape(0, 1, { drawing: { anchor: "2 F6 0 0 H8 0 0" } }),
        op.insertPara(0, "1 0"),
        op.insertText(0, "1 0 0", "abc"),
        op.insertShape(0, 2, { drawing: { anchor: "2 I9 0 0 K11 0 0" }, text: { link: FMLA } }),
        op.insertPara(0, "2 0"),
        op.insertText(0, "2 0 0", "abc"),
        // chart objects
        op.insertChart(0, 3, { attrs: { drawing: { anchor: "2 A1 0 0 B2 0 0" } } }),
        op.insertChSeries(0, 3, 0, { series: { type: "column2d", title: FMLA, names: FMLA, values: FMLA } }),
        op.changeChAxis(0, 3, 0, { axis: { axPos: "b", crossAx: 1, label: true } }),
        op.changeChAxis(0, 3, 1, { axis: { axPos: "l", crossAx: 0, label: true } }),
        op.changeChTitle(0, 3, { text: { link: FMLA } }),
        op.changeChTitle(0, 3, 0, { text: { link: FMLA } }),
        // cell notes
        op.insertNote(0, "A1", "note1"),
        op.insertNote(0, "F6", "note2"),
        op.insertNote(0, "I9", "note3"),
        // comment threads
        op.insertComment(0, "B2", 0, { text: "comment1" }),
        op.insertComment(0, "G7", 0, { text: "comment2" }),
        op.insertComment(0, "J10", 0, { text: "comment3" }),

        // target sheet for formulas
        op.insertSheet(1, "Sheet2"),
        op.insertName("local", 1, { formula: NMFMLA, ref: "[$Sheet2.$A$1]" }),
        // cell formulas
        op.changeCells(1, { A6: { v: 0, f: FMLA }, F1: { v: 0, f: FMLA }, A1: { v: 0, f: TBFMLA } }),
        // matrix formulas
        op.changeCells(1, "J10", [[{ v: 0, f: FMLA, mr: "J10:K11" }, 0], [0, 0]]),
        // table range
        op.insertTable(1, "Table21", "C3:D4", { table: { headerRow: true } }),
        op.changeCells(1, "C3", [["Col1", "Col2"], [42, 42]]),
        // data validation
        op.insertDVRule(1, "C3:D4", { type: "between", value1: SHFMLA, value2: SHFMLA, ref: "[$Sheet2.$C$3]" }),
        // conditional formatting
        op.insertCFRule(1, 0, "C3:D4", { type: "between", value1: SHFMLA, value2: SHFMLA }),
        // chart objects
        op.insertChart(1, 0, { attrs: { drawing: { anchor: "2 A1 0 0 B2 0 0" } } }),
        op.insertChSeries(1, 0, 0, { series: { type: "column2d", title: FMLA, names: FMLA, values: FMLA } }),
        op.changeChAxis(1, 0, 0, { axis: { axPos: "b", crossAx: 1, label: true } }),
        op.changeChAxis(1, 0, 1, { axis: { axPos: "l", crossAx: 0, label: true } }),
        op.changeChTitle(1, 0, { text: { link: FMLA } }),
        op.changeChTitle(1, 0, 0, { text: { link: FMLA } })
    ]);

    // expects that the entire document is in initial state as after import
    function expectInitialState(docModel) {

        // global contents
        docModel.expect.defName("global", NMFMLA);

        // Sheet1 (moved contents)
        docModel.expect.sheet(0)
            .cols("A:F I:U", "ce00")
            .cols("G", "ce01", { column: { width: 2 } })
            .cols("H", "ce02", { column: { width: 4 } })
            .rows("1:6 9:21", "ce00", { row: { customFormat: false } })
            .rows("7", "ce01", { row: { height: 100, customFormat: true } })
            .rows("8", "ce02", { row: { height: 200, customFormat: true } })
            .rows("1:7 9 11 13 15 17:20", { row: { visible: true, filtered: false } })
            .rows("8 10 12 14 16", { row: { visible: false, filtered: true } })
            .styles("A1:D4", "ce00")
            .styles("E2", [
                ["ce01", "ce01", "ce03", "ce04", "ce00", "ce00", "ce00", "ce00", "ce00", "ce00"],
                ["ce01", "ce02", "ce03", "ce06", "ce00", "ce00", "ce00", "ce00", "ce00", "ce00"],
                ["ce02", "ce02", "ce05", "ce06", "ce00", "ce00", "ce00", "ce00", "ce00", "ce00"]
            ])
            .styles("B5", [
                ["ce01", "ce01", "ce02"],
                ["ce01", "ce02", "ce02"],
                ["ce07", "ce07", "ce09"],
                ["ce08", "ce10", "ce10"],
                ["ce00", "ce00", "ce00"],
                ["ce00", "ce00", "ce00"],
                ["ce00", "ce00", "ce00"],
                ["ce00", "ce00", "ce00"],
                ["ce00", "ce00", "ce00"],
                ["ce00", "ce00", "ce00"]
            ])
            .formulas({
                A6: FMLA,
                F1: FMLA,
                A1: { f: FMLA, mr: "A1:B2" },
                F6: { f: FMLA, mr: "F6:G7" },
                J10: { f: FMLA, mr: "J10:K11" }
            })
            .merged("D11:E12 E13:F14 F15:G16 B15:D15 B16:D16 O1:O1048576")
            .hlink(URL1, "L3:L4 N5:N6")
            .hlink(URL2, "M4:M5 O1:O1048576")
            .defName("local", NMFMLA)
            .tableCount(7)
            .table("Table11", "C3:D4")
            .values("C3", [["Col1", "Col2"]])
            .table("Table12", "E1:H2")
            .values("E1", [["Col1", "Col2", "X", "Col4"]])
            .table("Table13", "B5:E9")
            .values("B5", [["Col1", "Col2", "X", "Col4"]])
            .table("Table14", "I10:I11")
            .table("Table15", "M10:M11")
            .table("Table16", "M14:M15")
            .table("Table17", "I14:I15")
            .autoFilter("A7:A16")
            .dvRuleCount(6)
            .dvRule(0, "E5:F6", { dvrule: { value1: SHFMLA, value2: SHFMLA } })
            .dvRule(1, "C3:D4", { dvrule: { value1: SHFMLA, value2: SHFMLA } })
            .dvRule(2, "G7:H8", { dvrule: { value1: SHFMLA, value2: SHFMLA } })
            .dvRule(3, "G6 F7", { dvrule: { type: "list", value1: '"a"' } })
            .dvRule(4, "I9 M13", { dvrule: { type: "list", value1: '"a"' } })
            .dvRule(5, "M9 I13", { dvrule: { type: "list", value1: '"a"' } })
            .cfRuleCount(6)
            .cfRule(0, "E5:F6", { cfrule: { value1: SHFMLA, value2: SHFMLA } })
            .cfRule(1, "C3:D4", { cfrule: { value1: SHFMLA, value2: SHFMLA } })
            .cfRule(2, "G7:H8", { cfrule: { value1: SHFMLA, value2: SHFMLA } })
            .cfRule(3, "G6 F7", { cfrule: { type: "iconset" } })
            .cfRule(4, "I9 M13", { cfrule: { type: "iconset" } })
            .cfRule(5, "M9 I13", { cfrule: { type: "iconset" } })
            .drawingCount(4)
            .drawing(0, { drawing: { anchor: "2 A1 0 0 C3 0 0" } })
            .drawing(1, { drawing: { anchor: "2 F6 0 0 H8 0 0" } })
            .drawing(2, { drawing: { anchor: "2 I9 0 0 K11 0 0" }, text: { link: FMLA } })
            .chart(3)
                .series(0, { series: { title: FMLA, names: FMLA, values: FMLA } })
                .title(null, { text: { link: FMLA } })
                .title(0, { text: { link: FMLA } })
                .end()
            .noteCount(3)
            .note("A1", "note1")
            .note("F6", "note2")
            .note("I9", "note3")
            .commentCount(3)
            .comment("B2", 0, "comment1")
            .comment("G7", 0, "comment2")
            .comment("J10", 0, "comment3");

        // Sheet2 (dependent formulas)
        docModel.expect.sheet(1)
            .formulas({
                A6: FMLA,
                F1: FMLA,
                A1: TBFMLA,
                J10: { f: FMLA, mr: "J10:K11" }
            })
            .defName("local", NMFMLA)
            .table("Table21", "C3:D4")
            .dvRule(0, "C3:D4", { dvrule: { value1: SHFMLA, value2: SHFMLA } })
            .cfRule(0, "C3:D4", { cfrule: { value1: SHFMLA, value2: SHFMLA } })
            .chart(0)
                .series(0, { series: { title: FMLA, names: FMLA, values: FMLA } })
                .title(null, { text: { link: FMLA } })
                .title(0, { text: { link: FMLA } });
    }

    // invokes the method `SheetModel.generateMoveOperations()` on the first sheet
    function generateMoveOperations(docModel, intervals, direction) {
        return docModel.buildOperations(builder => {
            const sheetModel = docModel.getSheetModel(0);
            return sheetModel.generateMoveOperations(builder, ia(intervals), direction);
        });
    }

    // define a test for a move operation (insert/delet columns/rows)
    function defineMoveIntervalsTest(intervals, direction, expectStateFn) {
        defineModelTestWithUndo(appPromise, function (docModel) {
            return generateMoveOperations(docModel, intervals, direction);
        }, expectStateFn, expectInitialState, { redo: true });
    }

    // define a test for a failing move operation
    async function defineFailingMoveIntervalsTest(intervals, direction, expErrCode) {
        const { docModel } = await appPromise;
        try {
            await generateMoveOperations(docModel, intervals, direction);
            throw new Error("operation is expected to fail");
        } catch (err) {
            expect(err).toHaveProperty("cause", expErrCode);
        }
    }

    // class SheetModel -------------------------------------------------------

    describe("method generateMoveOperations() (ODF)", () => {
        it("should exist", () => {
            expect(SheetModel).toHaveMethod("generateMoveOperations");
        });

        it("should fail to split matrix formulas", () => {
            defineFailingMoveIntervalsTest("K:L", Direction.RIGHT, "formula:matrix:insert");
        });

        it("should fail to shrink matrix formulas", () => {
            defineFailingMoveIntervalsTest("K:L", Direction.LEFT, "formula:matrix:delete");
        });

        describe("should generate operations for inserted columns", () => {
            defineMoveIntervalsTest("F:H H:J", Direction.RIGHT, docModel => {

                // global names (no transformation of 2D references and relative references)
                docModel.expect.defName("global", "SUM([.L12:.$M$13];[$Sheet1.L12:.$S$13];[$Sheet2.L12:.$M$13])");

                // Sheet1 (moved contents)
                const at1 = { cell: { fillColor: AUTO, ...borders("tb", THIN, "lr", NONE) } };
                const at2 = { cell: { fillColor: AUTO, ...borders("tb", THIN_RED, "lr", NONE) } };
                const FMLA0 = "SUM([.E5:.I6];[.R12:.S13];[$Sheet1.E5:.I6];[$Sheet1.R12:.S13];[$Sheet2.E5:.F6];[$Sheet2.L12:.M13])";
                const SHFMLA0 = "SUM([.P10:.Q11];[$Sheet1.P10:.Q11];[$Sheet2.J10:.K11])";
                docModel.expect.sheet(0)
                    .cols("A:I O:U", "ce00")
                    .cols("J:M", "ce01", { column: { width: 2 } })
                    .cols("N", "ce02", { column: { width: 4 } })
                    .styles("E2", [
                        ["ce01", "ce01", "ce01", "ce01", "ce01", "ce03", at1,    at1,    at1,    "ce04"],
                        ["ce01", "ce01", "ce01", "ce01", "ce02", "ce03", "ce01", "ce01", "ce01", "ce06"],
                        ["ce02", "ce02", "ce02", "ce02", "ce02", "ce05", at2,    at2,    at2,    "ce06"]
                    ])
                    .formulas({
                        A6: FMLA0,
                        I1: FMLA0,
                        A1: { f: FMLA0, mr: "A1:B2" },
                        I6: { f: FMLA0, mr: "I6:J7" },
                        P10: { f: FMLA0, mr: "P10:Q11" }
                    })
                    .merged("D11:E12 E13:I14 I15:J16 B15:D15 B16:D16 U1:U1048576")
                    .hlink(URL1, "R3:R4 T5:T6")
                    .hlink(URL2, "S4:S5 U1:U1048576")
                    .defName("local", "SUM([.L12:.$S$13];[$Sheet1.L12:.$S$13];[$Sheet2.L12:.$M$13])")
                    .table("Table11", "C3:D4")
                    .table("Table12", "E1:N2")
                    .values("E1", [["Col1", "Col3", "Col5", "Col6", "Col2", "X", "Column1", "Column2", "Column3", "Col4"]])
                    .table("Table13", "B5:E9")
                    .table("Table14", "O10:O11")
                    .table("Table15", "S10:S11")
                    .table("Table16", "S14:S15")
                    .table("Table17", "O14:O15")
                    .dvRule(0, "E5:I6", { dvrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .dvRule(1, "C3:D4", { dvrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .dvRule(2, "J7:N8", { dvrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .dvRule(3, "J6:M6 I7", { dvrule: { type: "list", value1: '"a"' } })
                    .dvRule(4, "O9 S13", { dvrule: { type: "list", value1: '"a"' } })
                    .dvRule(5, "S9 O13", { dvrule: { type: "list", value1: '"a"' } })
                    .cfRule(0, "E5:I6", { cfrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .cfRule(1, "C3:D4", { cfrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .cfRule(2, "J7:N8", { cfrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .cfRule(3, "J6:M6 I7", { cfrule: { type: "iconset" } })
                    .cfRule(4, "O9 S13", { cfrule: { type: "iconset" } })
                    .cfRule(5, "S9 O13", { cfrule: { type: "iconset" } })
                    .drawing(0, { drawing: { anchor: "2 A1 0 0 C3 0 0" } })
                    .drawing(1, { drawing: { anchor: "2 I6 0 0 K8 0 0" } })
                    .drawing(2, { drawing: { anchor: "2 O9 0 0 Q11 0 0" }, text: { link: FMLA0 } })
                    .chart(3)
                        .series(0, { series: { title: FMLA0, names: FMLA0, values: FMLA0 } })
                        .title(null, { text: { link: FMLA0 } })
                        .title(0, { text: { link: FMLA0 } })
                        .end()
                    .note("A1", "note1")
                    .note("I6", "note2")
                    .note("O9", "note3")
                    .comment("B2", 0, "comment1")
                    .comment("J7", 0, "comment2")
                    .comment("P10", 0, "comment3");

                // Sheet2 (dependent formulas)
                const FMLA1 = "SUM([.E5:.F6];[.L12:.M13];[$Sheet1.E5:.I6];[$Sheet1.R12:.S13];[$Sheet2.E5:.F6];[$Sheet2.L12:.M13])";
                const SHFMLA1 = "SUM([.J10:.K11];[$Sheet1.P10:.Q11];[$Sheet2.J10:.K11])";
                docModel.expect.sheet(1)
                    .formulas({
                        A6: FMLA1,
                        F1: FMLA1,
                        A1: TBFMLA,
                        J10: { f: FMLA1, mr: "J10:K11" }
                    })
                    .defName("local", "SUM([.L12:.$M$13];[$Sheet1.L12:.$S$13];[$Sheet2.L12:.$M$13])")
                    .table("Table21", "C3:D4")
                    .dvRule(0, "C3:D4", { dvrule: { value1: SHFMLA1, value2: SHFMLA1 } })
                    .cfRule(0, "C3:D4", { cfrule: { value1: SHFMLA1, value2: SHFMLA1 } })
                    .chart(0)
                        .series(0, { series: { title: FMLA1, names: FMLA1, values: FMLA1 } })
                        .title(null, { text: { link: FMLA1 } })
                        .title(0, { text: { link: FMLA1 } });
            });
        });

        describe("should generate operations for inserted rows", () => {
            defineMoveIntervalsTest("6:8 8:10", Direction.DOWN, docModel => {

                // global contents (no transformation of 2D references and relative references in names)
                docModel.expect.defName("global", "SUM([.L12:.$M$13];[$Sheet1.L12:.$M$19];[$Sheet2.L12:.$M$13])");

                // Sheet1 (moved contents)
                const at1 = { cell: { fillColor: AUTO, ...borders("lr", THIN, "tb", NONE) } };
                const at2 = { cell: { fillColor: AUTO, ...borders("lr", THIN_RED, "tb", NONE) } };
                const FMLA0 = "SUM([.E5:.F9];[.L18:.M19];[$Sheet1.E5:.F9];[$Sheet1.L18:.M19];[$Sheet2.E5:.F6];[$Sheet2.L12:.M13])";
                const SHFMLA0 = "SUM([.J16:.K17];[$Sheet1.J16:.K17];[$Sheet2.J10:.K11])";
                docModel.expect.sheet(0)
                    .rows("1:9 15:21", "ce00", { row: { customFormat: false } })
                    .rows("10:13", "ce01", { row: { height: 100, customFormat: true } })
                    .rows("14", "ce02", { row: { height: 200, customFormat: true } })
                    .styles("B5", [
                        ["ce01", "ce01",  "ce02"],
                        ["ce01", "ce01",  "ce02"],
                        ["ce01", "ce01",  "ce02"],
                        ["ce01", "ce01",  "ce02"],
                        ["ce01", "ce02",  "ce02"],
                        ["ce07", "ce07",  "ce09"],
                        [at1,    "ce01",  at2],
                        [at1,    "ce01",  at2],
                        [at1,    "ce01",  at2],
                        ["ce08", "ce10", "ce10"]
                    ])
                    .formulas({
                        A9: FMLA0,
                        F1: FMLA0,
                        A1: { f: FMLA0, mr: "A1:B2" },
                        F9: { f: FMLA0, mr: "F9:G10" },
                        J16: { f: FMLA0, mr: "J16:K17" }
                    })
                    .merged("D17:E18 E19:F20 F21:G22 B21:D21 B22:D22 O1:O1048576")
                    .hlink(URL1, "L3:L4 N5:N9")
                    .hlink(URL2, "M4:M5 O1:O1048576")
                    .defName("local", "SUM([.L12:.$M$19];[$Sheet1.L12:.$M$19];[$Sheet2.L12:.$M$13])")
                    .table("Table11", "C3:D4")
                    .table("Table12", "E1:H2")
                    .table("Table13", "B5:E15")
                    .table("Table14", "I16:I17")
                    .table("Table15", "M16:M17")
                    .table("Table16", "M20:M21")
                    .table("Table17", "I20:I21")
                    .autoFilter("A10:A22")
                    .dvRule(0, "E5:F9", { dvrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .dvRule(1, "C3:D4", { dvrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .dvRule(2, "G10:H14", { dvrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .dvRule(3, "G9 F10:F13", { dvrule: { type: "list", value1: '"a"' } })
                    .dvRule(4, "I15 M19", { dvrule: { type: "list", value1: '"a"' } })
                    .dvRule(5, "M15 I19", { dvrule: { type: "list", value1: '"a"' } })
                    .cfRule(0, "E5:F9", { cfrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .cfRule(1, "C3:D4", { cfrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .cfRule(2, "G10:H14", { cfrule: { value1: SHFMLA0, value2: SHFMLA0 } })
                    .cfRule(3, "G9 F10:F13", { cfrule: { type: "iconset" } })
                    .cfRule(4, "I15 M19", { cfrule: { type: "iconset" } })
                    .cfRule(5, "M15 I19", { cfrule: { type: "iconset" } })
                    .drawing(0, { drawing: { anchor: "2 A1 0 0 C3 0 0" } })
                    .drawing(1, { drawing: { anchor: "2 F9 0 0 H11 0 0" } })
                    .drawing(2, { drawing: { anchor: "2 I15 0 0 K17 0 0" }, text: { link: FMLA0 } })
                    .chart(3)
                        .series(0, { series: { title: FMLA0, names: FMLA0, values: FMLA0 } })
                        .title(null, { text: { link: FMLA0 } })
                        .title(0, { text: { link: FMLA0 } })
                        .end()
                    .note("A1", "note1")
                    .note("F9", "note2")
                    .note("I15", "note3")
                    .comment("B2", 0, "comment1")
                    .comment("G10", 0, "comment2")
                    .comment("J16", 0, "comment3");

                // Sheet2 (dependent formulas)
                const FMLA1 = "SUM([.E5:.F6];[.L12:.M13];[$Sheet1.E5:.F9];[$Sheet1.L18:.M19];[$Sheet2.E5:.F6];[$Sheet2.L12:.M13])";
                const SHFMLA1 = "SUM([.J10:.K11];[$Sheet1.J16:.K17];[$Sheet2.J10:.K11])";
                docModel.expect.sheet(1)
                    .formulas({
                        A6: FMLA1,
                        F1: FMLA1,
                        A1: TBFMLA,
                        J10: { f: FMLA1, mr: "J10:K11" }
                    })
                    .defName("local", "SUM([.L12:.$M$13];[$Sheet1.L12:.$M$19];[$Sheet2.L12:.$M$13])")
                    .table("Table21", "C3:D4")
                    .dvRule(0, "C3:D4", { dvrule: { value1: SHFMLA1, value2: SHFMLA1 } })
                    .cfRule(0, "C3:D4", { cfrule: { value1: SHFMLA1, value2: SHFMLA1 } })
                    .chart(0)
                        .series(0, { series: { title: FMLA1, names: FMLA1, values: FMLA1 } })
                        .title(null, { text: { link: FMLA1 } })
                        .title(0, { text: { link: FMLA1 } });
            });
        });

        describe("should generate operations for deleted columns", () => {
            defineMoveIntervalsTest("C:D F:G", Direction.LEFT, docModel => {

                // global contents (no transformation of 2D references and relative references in names)
                docModel.expect.defName("global", "SUM([.L12:.$M$13];[$Sheet1.$I12:.L$13];[$Sheet2.L12:.$M$13])");

                // Sheet1 (moved contents)
                const FMLA0 = "SUM([.C5:.C6];[.H12:.I13];[$Sheet1.C5:.C6];[$Sheet1.H12:.I13];[$Sheet2.E5:.F6];[$Sheet2.L12:.M13])";
                const SHFMLA00 = "SUM([.F10:.G11];[$Sheet1.F10:.G11];[$Sheet2.J10:.K11])";
                const SHFMLA01 = "SUM([.G10:.H11];[$Sheet1.G10:.H11];[$Sheet2.K10:.L11])";
                docModel.expect.sheet(0)
                    .cols("A:C E:K", "ce00")
                    .cols("D", "ce02", { column: { width: 4 } })
                    .styles("B2", [
                        ["ce00", "ce01", "ce04", "ce00"],
                        ["ce00", "ce01", "ce06", "ce00"],
                        ["ce00", "ce02", "ce06", "ce00"]
                    ])
                    .formulas({
                        A6: FMLA0,
                        F1: null,
                        A1: { f: FMLA0, mr: "A1:B2" },
                        F6: null,
                        F10: { f: FMLA0, mr: "F10:G11" }
                    })
                    .merged("C11:C12 C13:C14 K1:K1048576")
                    .notMerged("A15:J16")
                    .hlink(URL1, "H3:H4 J5:J6")
                    .hlink(URL2, "I4:I5 K1:K1048576")
                    .defName("local", "SUM([.$I12:.L$13];[$Sheet1.$I12:.L$13];[$Sheet2.L12:.$M$13])")
                    .tableCount(6)
                    .noTable("Table11")
                    .table("Table12", "C1:D2")
                    .table("Table13", "B5:C9")
                    .table("Table14", "E10:E11")
                    .table("Table15", "I10:I11")
                    .table("Table16", "I14:I15")
                    .table("Table17", "E14:E15")
                    .dvRuleCount(4)
                    .dvRule(0, "C5:C6", { dvrule: { value1: SHFMLA00, value2: SHFMLA00 } })
                    .dvRule(1, "D7:D8", { dvrule: { value1: SHFMLA01, value2: SHFMLA01 } })
                    .dvRule(2, "E9 I13", { dvrule: { type: "list", value1: '"a"' } })
                    .dvRule(3, "I9 E13", { dvrule: { type: "list", value1: '"a"' } })
                    .cfRuleCount(4)
                    .cfRule(0, "C5:C6", { cfrule: { value1: SHFMLA00, value2: SHFMLA00 } })
                    .cfRule(1, "D7:D8", { cfrule: { value1: SHFMLA01, value2: SHFMLA01 } })
                    .cfRule(2, "E9 I13", { cfrule: { type: "iconset" } })
                    .cfRule(3, "I9 E13", { cfrule: { type: "iconset" } })
                    .drawingCount(3)
                    .drawing(0, { drawing: { anchor: "2 A1 0 0 C3 0 0" } })
                    .drawing(1, { drawing: { anchor: "2 E9 0 0 G11 0 0" }, text: { link: FMLA0 } })
                    .chart(2)
                        .series(0, { series: { title: FMLA0, names: FMLA0, values: FMLA0 } })
                        .title(null, { text: { link: FMLA0 } })
                        .title(0, { text: { link: FMLA0 } })
                        .end()
                    .noteCount(2)
                    .note("A1", "note1")
                    .note("E9", "note3")
                    .commentCount(2)
                    .comment("B2", 0, "comment1")
                    .comment("F10", 0, "comment3");

                // Sheet2 (dependent formulas)
                const FMLA1 = "SUM([.E5:.F6];[.L12:.M13];[$Sheet1.C5:.C6];[$Sheet1.H12:.I13];[$Sheet2.E5:.F6];[$Sheet2.L12:.M13])";
                const SHFMLA1 = "SUM([.J10:.K11];[$Sheet1.F10:.G11];[$Sheet2.J10:.K11])";
                const TBFMLA1 = "SUM(#REF!;Table21)";
                docModel.expect.sheet(1)
                    .formulas({
                        A6: FMLA1,
                        F1: FMLA1,
                        A1: TBFMLA1,
                        J10: { f: FMLA1, mr: "J10:K11" }
                    })
                    .defName("local", "SUM([.L12:.$M$13];[$Sheet1.$I12:.L$13];[$Sheet2.L12:.$M$13])")
                    .table("Table21", "C3:D4")
                    .dvRule(0, "C3:D4", { dvrule: { value1: SHFMLA1, value2: SHFMLA1 } })
                    .cfRule(0, "C3:D4", { cfrule: { value1: SHFMLA1, value2: SHFMLA1 } })
                    .chart(0)
                        .series(0, { series: { title: FMLA1, names: FMLA1, values: FMLA1 } })
                        .title(null, { text: { link: FMLA1 } })
                        .title(0, { text: { link: FMLA1 } });
            });
        });

        describe("should generate operations for deleted rows", () => {
            defineMoveIntervalsTest("3:4 6:7", Direction.UP, docModel => {

                // global contents (no transformation of 2D references and relative references in names)
                docModel.expect.defName("global", "SUM([.L12:.$M$13];[$Sheet1.L$9:.$M12];[$Sheet2.L12:.$M$13])");

                // Sheet1 (moved contents)
                const FMLA0 = "SUM([.E3:.F3];[.L8:.M9];[$Sheet1.E3:.F3];[$Sheet1.L8:.M9];[$Sheet2.E5:.F6];[$Sheet2.L12:.M13])";
                const SHFMLA00 = "SUM([.J6:.K7];[$Sheet1.J6:.K7];[$Sheet2.J10:.K11])";
                const SHFMLA01 = "SUM([.J7:.K8];[$Sheet1.J7:.K8];[$Sheet2.J11:.K12])";
                docModel.expect.sheet(0)
                    .rows("1:3 5:11", "ce00", { row: { customFormat: false } })
                    .rows("4", "ce02", { row: { height: 200, customFormat: true } })
                    .styles("B2", [
                        ["ce00", "ce00", "ce00"],
                        ["ce01", "ce01", "ce02"],
                        ["ce08", "ce10", "ce10"],
                        ["ce00", "ce00", "ce00"]
                    ])
                    .formulas({
                        A6: null,
                        F1: FMLA0,
                        A1: { f: FMLA0, mr: "A1:B2" },
                        F6: null,
                        J6: { f: FMLA0, mr: "J6:K7" }
                    })
                    .merged("D7:E8 E9:F10 F11:G12 B11:D11 B12:D12 O1:O1048576")
                    .hlink(URL1, "N3")
                    .hlink(URL2, "M3 O1:O1048576")
                    .defName("local", "SUM([.L$9:.$M12];[$Sheet1.L$9:.$M12];[$Sheet2.L12:.$M$13])")
                    .tableCount(6)
                    .noTable("Table11")
                    .table("Table12", "E1:H2")
                    .table("Table13", "B3:E5")
                    .table("Table14", "I6:I7")
                    .table("Table15", "M6:M7")
                    .table("Table16", "M10:M11")
                    .table("Table17", "I10:I11")
                    .noAutoFilter()
                    .rows("1:20", { row: { visible: true, filtered: false } })
                    .dvRuleCount(4)
                    .dvRule(0, "E3:F3", { dvrule: { value1: SHFMLA00, value2: SHFMLA00 } })
                    .dvRule(1, "G4:H4", { dvrule: { value1: SHFMLA01, value2: SHFMLA01 } })
                    .dvRule(2, "I5 M9", { dvrule: { type: "list", value1: '"a"' } })
                    .dvRule(3, "M5 I9", { dvrule: { type: "list", value1: '"a"' } })
                    .cfRuleCount(4)
                    .cfRule(0, "E3:F3", { cfrule: { value1: SHFMLA00, value2: SHFMLA00 } })
                    .cfRule(1, "G4:H4", { cfrule: { value1: SHFMLA01, value2: SHFMLA01 } })
                    .cfRule(2, "I5 M9", { cfrule: { type: "iconset" } })
                    .cfRule(3, "M5 I9", { cfrule: { type: "iconset" } })
                    .drawingCount(3)
                    .drawing(0, { drawing: { anchor: "2 A1 0 0 C3 0 0" } })
                    .drawing(1, { drawing: { anchor: "2 I5 0 0 K7 0 0" }, text: { link: FMLA0 } })
                    .chart(2)
                        .series(0, { series: { title: FMLA0, names: FMLA0, values: FMLA0 } })
                        .title(null, { text: { link: FMLA0 } })
                        .title(0, { text: { link: FMLA0 } })
                        .end()
                    .noteCount(2)
                    .note("A1", "note1")
                    .note("I5", "note3")
                    .commentCount(2)
                    .comment("B2", 0, "comment1")
                    .comment("J6", 0, "comment3");

                // Sheet2 (dependent formulas)
                const FMLA1 = "SUM([.E5:.F6];[.L12:.M13];[$Sheet1.E3:.F3];[$Sheet1.L8:.M9];[$Sheet2.E5:.F6];[$Sheet2.L12:.M13])";
                const SHFMLA1 = "SUM([.J10:.K11];[$Sheet1.J6:.K7];[$Sheet2.J10:.K11])";
                const TBFMLA1 = "SUM(#REF!;Table21)";
                docModel.expect.sheet(1)
                    .formulas({
                        A6: FMLA1,
                        F1: FMLA1,
                        A1: TBFMLA1,
                        J10: { f: FMLA1, mr: "J10:K11" }
                    })
                    .defName("local", "SUM([.L12:.$M$13];[$Sheet1.L$9:.$M12];[$Sheet2.L12:.$M$13])")
                    .table("Table21", "C3:D4")
                    .dvRule(0, "C3:D4", { dvrule: { value1: SHFMLA1, value2: SHFMLA1 } })
                    .cfRule(0, "C3:D4", { cfrule: { value1: SHFMLA1, value2: SHFMLA1 } })
                    .chart(0)
                        .series(0, { series: { title: FMLA1, names: FMLA1, values: FMLA1 } })
                        .title(null, { text: { link: FMLA1 } })
                        .title(0, { text: { link: FMLA1 } });
            });
        });
    });
});
