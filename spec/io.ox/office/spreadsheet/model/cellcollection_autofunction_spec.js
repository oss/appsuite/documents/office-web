/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { CellCollection } from "@/io.ox/office/spreadsheet/model/cellcollection";

import { a, r, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/cellcollection", () => {

    // the operations to be applied by the document model
    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertSheet(0, "Sheet1")
    ];

    // initialize test document
    let docModel, sheetTester, sheetModel, cellCollection;
    createSpreadsheetApp("ooxml", OPERATIONS).done(function (docApp) {
        docModel = docApp.docModel;
        sheetTester = docModel.expect.sheet(0);
        sheetModel = sheetTester.sheetModel;
        cellCollection = sheetModel.cellCollection;
    });

    async function testAutoFunctionRange(contents, range, expValues, expRange, expFormulas) {
        docModel.applyOperations(op.changeCells(0, contents));
        const targetRange = await sheetModel.createAndApplyOperations(function (generator) {
            return cellCollection.generateAutoFormulaOperations(generator, "SUM", r(range));
        });
        expect(targetRange).toStringifyTo(expRange);
        sheetTester.values(expValues).formulas(expFormulas);
        await docModel.undoManager.undo();
        sheetTester.noCells(Object.keys(expFormulas).join(" "));
        docModel.applyOperations(op.changeCells(0, { "A1:Z99": { u: true } }));
    }

    function testAutoFormulaSingle(contents, address, expected) {
        docModel.applyOperations(op.changeCells(0, contents));
        const range = cellCollection.findAutoFormulaRange(a(address));
        expect(range).toStringifyTo(expected);
        docModel.applyOperations(op.changeCells(0, { "A1:Z99": { u: true } }));
    }

    // class CellCollection ---------------------------------------------------

    describe("class CellCollection", () => {

        describe("method generateAutoFormulaOperations", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("generateAutoFormulaOperations");
            });

            it("describes autosum example 1 (column range) with expanding", async () => {

                const data = { B2: 2, B3: 3 };
                const range = "B2:B6";

                const expectedData = { B6: 5 };
                const expectedRange = "B2:B6";
                const expectedFormulas = { B6: "SUM(B2:B5)" };

                await testAutoFunctionRange(data, range, expectedData, expectedRange, expectedFormulas);
            });

            it("describes autosum example 1 (column range) without expanding", async () => {

                const data = { B2: 2, B3: 3 };
                const range = "B2:B3";

                const expectedData = { B4: 5 };
                const expectedRange = "B2:B4";
                const expectedFormulas = { B4: "SUM(B2:B3)" };

                await testAutoFunctionRange(data, range, expectedData, expectedRange, expectedFormulas);
            });

            it("describes autosum example 2 (row range) with expanding", async () => {

                const data = { B2: 2, C2: 3 };
                const range = "B2:E2";

                const expectedData = { E2: 5 };
                const expectedRange = "B2:E2";
                const expectedFormulas = { E2: "SUM(B2:D2)" };

                await testAutoFunctionRange(data, range, expectedData, expectedRange, expectedFormulas);
            });

            it("describes autosum example 2 (row range) without expanding", async () => {

                const data = { B2: 2, C2: 3 };
                const range = "B2:C2";

                const expectedData = { D2: 5 };
                const expectedRange = "B2:D2";
                const expectedFormulas = { D2: "SUM(B2:C2)" };

                await testAutoFunctionRange(data, range, expectedData, expectedRange, expectedFormulas);
            });

            it("describes autosum example 3 (column range) with auto expanding", async () => {

                const data = { B2: 2, B3: 3, B4: 4 };
                const range = "B2:B3";

                const expectedData = { B5: 5 };
                const expectedRange = "B2:B4";
                const expectedFormulas = { B5: "SUM(B2:B3)" };

                await testAutoFunctionRange(data, range, expectedData, expectedRange, expectedFormulas);
            });

            it("describes autosum example 4 (row range) without auto expanding", async () => {

                const data = { B2: 2, C2: 3, D2: 4 };
                const range = "B2:C2";

                const expectedData = { E2: 5 };
                const expectedRange = "B2:D2";
                const expectedFormulas = { E2: "SUM(B2:C2)" };

                await testAutoFunctionRange(data, range, expectedData, expectedRange, expectedFormulas);
            });

            it("describes autosum example 5 (single cell) with strings in active range (column)", () => {
                testAutoFormulaSingle({ B2: 2, B3: "str", B4: 3, B5: 4 }, "B6", "B4:B5");
            });

            it("describes autosum example 6 (single cell) with strings in active range (row)", () => {
                testAutoFormulaSingle({ B2: 2, C2: "str", D2: 3, E2: 4 }, "F2", "D2:E2");
            });

            it("describes autosum example 7 (range) with auto expanding", async () => {

                const data = { B2: 5, C2: 5, D2: 5, B3: 1, C3: 2, D3: 5, B4: 1, D4: 5 };
                const range = "B2:D4";

                const expectedData = { B5: 7, C5: 7, D5: 15 };
                const expectedRange = "B2:D5";
                const expectedFormulas = { B5: "SUM(B2:B4)" };

                await testAutoFunctionRange(data, range, expectedData, expectedRange, expectedFormulas);
            });

            it("describes autosum example 8 (single cell) with priority for column on top", () => {
                testAutoFormulaSingle({ D2: 3, D3: 7, B4: 2, C4: 3 }, "D4", "D2:D3");
            });

            it("describes autosum example 9 (single cell) with priority for closest number group", () => {
                testAutoFormulaSingle({ D2: 3, D3: 2, B5: -2, C5: 10 }, "D5", "B5:C5");
            });

            it("describes autosum example 10 (single cell) with priority for number group instad of string group", () => {
                testAutoFormulaSingle({ E2: 3, E3: -10, B5: 2, C5: 3, D5: "str" }, "E5", "E2:E4");
            });

            it("describes autosum example 11 (range) one row with no content inside", async () => {

                const data = { B2: 2, B3: 4, C2: 3, C3: 5 };
                const range = "B4:C4";

                const expectedData = { B4: 6, C4: 8 };
                const expectedRange = "B4:C4";
                const expectedFormulas = { B4: "SUM(B2:B3)" };

                await testAutoFunctionRange(data, range, expectedData, expectedRange, expectedFormulas);
            });

            it("describes special scenarion described in Bug 46938", async () => {

                const data = { A1: 3, A2: 5, A3: 2, A4: 3 };
                const range = "A2:A3";

                const expectedData = { A5: 7 };
                const expectedRange = "A2:A4";
                const expectedFormulas = { A5: "SUM(A2:A3)" };

                await testAutoFunctionRange(data, range, expectedData, expectedRange, expectedFormulas);
            });
        });
    });
});
