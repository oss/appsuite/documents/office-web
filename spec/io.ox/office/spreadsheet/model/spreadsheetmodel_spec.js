/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import TextBaseModel from "@/io.ox/office/textframework/model/editor";
import { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import { NumberFormatter } from "@/io.ox/office/spreadsheet/model/numberformatter";
import { SortListCollection } from "@/io.ox/office/spreadsheet/model/sortlistcollection";
import { NameCollection } from "@/io.ox/office/spreadsheet/model/namecollection";
import { SheetCollection } from "@/io.ox/office/spreadsheet/model/sheetcollection";
import { FormulaResource } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { FormulaParser } from "@/io.ox/office/spreadsheet/model/formula/parser/formulaparser";
import { RangeListParser } from "@/io.ox/office/spreadsheet/model/formula/parser/rangelistparser";
import { FormulaCompiler } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacompiler";
import { FormulaInterpreter } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulainterpreter";
import { DependencyManager } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencymanager";
import { SpreadsheetModel } from "@/io.ox/office/spreadsheet/model/spreadsheetmodel";

import { createSpreadsheetApp } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/spreadsheetmodel", () => {

    // class SpreadsheetModel -------------------------------------------------

    describe("class SpreadsheetModel", () => {

        it("should subclass TextBaseModel", () => {
            expect(SpreadsheetModel).toBeSubClassOf(TextBaseModel);
        });

        // initialize test document
        let docModel;
        createSpreadsheetApp("ooxml").done(docApp => {
            docModel = docApp.docModel;
        });

        describe("property addressFactory", () => {
            it("should exist", () => {
                expect(docModel.addressFactory).toBeInstanceOf(AddressFactory);
            });
        });

        describe("property numberFormatter", () => {
            it("should exist", () => {
                expect(docModel.numberFormatter).toBeInstanceOf(NumberFormatter);
            });
        });

        describe("property formulaResource", () => {
            it("should exist", () => {
                expect(docModel.formulaResource).toBeInstanceOf(FormulaResource);
                expect(docModel.formulaResource.fileFormat).toBe("ooxml");
            });
        });

        describe("property formulaGrammarOP", () => {
            it("should exist", () => {
                expect(docModel.formulaGrammarOP).toBeInstanceOf(FormulaGrammar);
            });
        });

        describe("property formulaGrammarUI", () => {
            it("should exist", () => {
                expect(docModel.formulaGrammarUI).toBeInstanceOf(FormulaGrammar);
            });
        });

        describe("property formulaParserOP", () => {
            it("should exist", () => {
                expect(docModel.formulaParserOP).toBeInstanceOf(FormulaParser);
            });
        });

        describe("property rangeListParserOP", () => {
            it("should exist", () => {
                expect(docModel.rangeListParserOP).toBeInstanceOf(RangeListParser);
            });
        });

        describe("property formulaCompiler", () => {
            it("should exist", () => {
                expect(docModel.formulaCompiler).toBeInstanceOf(FormulaCompiler);
            });
        });

        describe("property formulaInterpreter", () => {
            it("should exist", () => {
                expect(docModel.formulaInterpreter).toBeInstanceOf(FormulaInterpreter);
            });
        });

        describe("property nameCollection", () => {
            it("should exist", () => {
                expect(docModel.nameCollection).toBeInstanceOf(NameCollection);
            });
        });

        describe("property sortListCollection", () => {
            it("should exist", () => {
                expect(docModel.sortListCollection).toBeInstanceOf(SortListCollection);
            });
        });

        describe("property sheetCollection", () => {
            it("should exist", () => {
                expect(docModel.sheetCollection).toBeInstanceOf(SheetCollection);
            });
        });

        describe("property dependencyManager", () => {
            it("should exist", () => {
                expect(docModel.dependencyManager).toBeInstanceOf(DependencyManager);
            });
        });

        describe("method getFormulaGrammar", () => {
            it("should exist", () => {
                expect(SpreadsheetModel).toHaveMethod("getFormulaGrammar");
            });
            it("should return the formula grammar", () => {
                const opGrammar = docModel.getFormulaGrammar("op");
                expect(opGrammar).toBeInstanceOf(FormulaGrammar);
                expect(opGrammar).toHaveProperty("OF", false);
                expect(opGrammar).toHaveProperty("UI", false);
                expect(opGrammar).toHaveProperty("RC", false);
                const ooxGrammar = docModel.getFormulaGrammar("op:oox");
                expect(ooxGrammar).toBeInstanceOf(FormulaGrammar);
                expect(ooxGrammar).toBe(opGrammar);
                const a1Grammar = docModel.getFormulaGrammar("ui:a1");
                expect(a1Grammar).toBeInstanceOf(FormulaGrammar);
                expect(a1Grammar).toHaveProperty("OF", false);
                expect(a1Grammar).toHaveProperty("UI", true);
                expect(a1Grammar).toHaveProperty("RC", false);
                const rcGrammar = docModel.getFormulaGrammar("ui:rc");
                expect(rcGrammar).toBeInstanceOf(FormulaGrammar);
                expect(rcGrammar).toHaveProperty("OF", false);
                expect(rcGrammar).toHaveProperty("UI", true);
                expect(rcGrammar).toHaveProperty("RC", true);
                const enGrammar = docModel.getFormulaGrammar("en:a1");
                expect(enGrammar).toBeInstanceOf(FormulaGrammar);
                expect(enGrammar).toHaveProperty("OF", false);
                expect(enGrammar).toHaveProperty("UI", true);
                expect(enGrammar).toHaveProperty("RC", false);
                const enRcGrammar = docModel.getFormulaGrammar("en:rc");
                expect(enRcGrammar).toBeInstanceOf(FormulaGrammar);
                expect(enRcGrammar).toHaveProperty("OF", false);
                expect(enRcGrammar).toHaveProperty("UI", true);
                expect(enRcGrammar).toHaveProperty("RC", true);
            });
            it("should return the operation formula grammar", () => {
                const opGrammar = docModel.getFormulaGrammar("op");
                expect(opGrammar).toBe(docModel.formulaGrammarOP);
            });
            it("should return the UI formula grammar", () => {
                const uiGrammar = docModel.getFormulaGrammar("ui:a1");
                expect(uiGrammar).toBe(docModel.formulaGrammarUI);
            });
        });
    });
});
