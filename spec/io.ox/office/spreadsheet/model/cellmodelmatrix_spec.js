/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { EObject } from "@/io.ox/office/tk/objects";
import { CellModel } from "@/io.ox/office/spreadsheet/model/cellmodel";
import { CellModelMatrix } from "@/io.ox/office/spreadsheet/model/cellmodelmatrix";

import { i, a, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/cellmodelmatrix", () => {

    // class CellModelMatrix --------------------------------------------------

    describe("class CellModelMatrix", () => {

        it("should subclass EObject", () => {
            expect(CellModelMatrix).toBeSubClassOf(EObject);
        });

        // the operations to be applied by the document model
        const OPERATIONS = [
            op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
            op.insertAutoStyle("a0", true),
            op.insertSheet(0, "Sheet1")
        ];

        // initialize test document
        let docModel = null, sheetModel = null, colMatrix = null, rowMatrix = null;
        createSpreadsheetApp("ooxml", OPERATIONS).done(function (app) {
            docModel = app.getModel();
            sheetModel = docModel.getSheetModel(0);
            colMatrix = new CellModelMatrix(sheetModel, true);
            rowMatrix = new CellModelMatrix(sheetModel, false);
        });

        describe("method getUsedInterval", () => {
            it("should exist", () => {
                expect(CellModelMatrix).toHaveMethod("getUsedInterval");
            });
            it("should return null for empty matrix", () => {
                expect(colMatrix.getUsedInterval()).toBeUndefined();
                expect(rowMatrix.getUsedInterval()).toBeUndefined();
            });
        });

        describe("method yieldVectors", () => {
            it("should exist", () => {
                expect(CellModelMatrix).toHaveMethod("yieldVectors");
            });
        });

        describe("method yieldIndexes", () => {
            it("should exist", () => {
                expect(CellModelMatrix).toHaveMethod("yieldIndexes");
            });
        });

        describe("method yieldModels", () => {
            it("should exist", () => {
                expect(CellModelMatrix).toHaveMethod("yieldModels");
            });
        });

        describe("method insertModel", () => {
            it("should exist", () => {
                expect(CellModelMatrix).toHaveMethod("insertModel");
            });
            const mA1 = new CellModel(a("A1"));
            it("should insert a cell model", () => {
                colMatrix.insertModel(mA1);
                const colVectors = Array.from(colMatrix.yieldVectors(i("A:ZZZ")));
                expect(colVectors).toHaveLength(1);
                expect(Array.from(colVectors[0])).toEqual([mA1]);
                expect(colMatrix.getUsedInterval()).toStringifyTo("1:1");
                rowMatrix.insertModel(mA1);
                const rowVectors = Array.from(rowMatrix.yieldVectors(i("1:10000")));
                expect(rowVectors).toHaveLength(1);
                expect(Array.from(rowVectors[0])).toEqual([mA1]);
                expect(rowMatrix.getUsedInterval()).toStringifyTo("1:1");
            });
            it("should insert more cell models in column A", () => {
                const mA3 = new CellModel(a("A3"));
                colMatrix.insertModel(mA3);
                const colVectors = Array.from(colMatrix.yieldVectors(i("A:ZZZ")));
                expect(colVectors).toHaveLength(1);
                expect(Array.from(colVectors[0])).toEqual([mA1, mA3]);
                const mA2 = new CellModel(a("A2"));
                colMatrix.insertModel(mA2);
                expect(Array.from(colVectors[0])).toEqual([mA1, mA2, mA3]);
                expect(colMatrix.getUsedInterval()).toStringifyTo("1:1");
            });
            it("should insert more cell models in row 1", () => {
                const mC1 = new CellModel(a("C1"));
                const mB1 = new CellModel(a("B1"));
                colMatrix.insertModel(mC1);
                colMatrix.insertModel(mB1);
                const colVectors = Array.from(colMatrix.yieldVectors(i("A:ZZZ")));
                expect(colVectors).toHaveLength(3);
                expect(Array.from(colVectors[1])).toEqual([mB1]);
                expect(Array.from(colVectors[2])).toEqual([mC1]);
                expect(colMatrix.getUsedInterval()).toStringifyTo("1:3");
            });
        });
    });
});
