/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { SheetCollection } from "@/io.ox/office/spreadsheet/model/sheetcollection";

import { waitForRecalcEnd, createSpreadsheetApp, defineModelTestWithUndo } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/sheetcollection", () => {

    const FMLA1 = "1+Sheet1!A1+Sheet2!A1";
    const FMLA2 = "1+Sheet1!A1+'Sheet 9'!A1";
    const FMLA3 = "1+Sheet1!A1+#REF!";
    const FMLA3_UI = "1+Sheet1!A1+#BEZUG!";

    // initialize test document
    const appPromise = createSpreadsheetApp("ooxml", [

        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertName("global", { formula: FMLA1 }),

        op.insertSheet(0, "Sheet1"),
        op.insertName("local", 0, { formula: FMLA1 }),
        op.changeCells(0, { A1: 2, A2: { v: 6, f: FMLA1 }, A3: { v: "=" + FMLA1, f: "_xlfn.FORMULATEXT(A2)" } }),
        op.changeCells(0, { B1: "Col1", C1: "Col2", B2: 1, C2: 2 }),
        op.changeCells(0, { A4: { v: 6, f: FMLA1, si: 0, sr: "A4:A5" }, A5: { v: 6, si: 0 } }),
        op.insertTable(0, "T1", "B1:C2", { headerRow: true }),
        op.insertDVRule(0, 0, "B1:B3", { type: "between", value1: FMLA1, value2: FMLA1 }),
        op.insertCFRule(0, "R0", "B1:B3", { type: "between", value1: FMLA1, value2: FMLA1 }),
        op.insertShape(0, 0, { drawing: { anchor: "2 B1 0 0 C4 0 0" }, text: { link: FMLA1 } }),
        op.insertChart(0, 1, { drawing: { anchor: "2 B1 0 0 C4 0 0" } }),
        op.insertChSeries(0, 1, 0, { series: { type: "column2d", title: FMLA1, names: FMLA1, values: FMLA1 } }),
        op.changeChAxis(0, 1, 0, { axis: { axPos: "b", crossAx: 1, label: true } }),
        op.changeChAxis(0, 1, 1, { axis: { axPos: "l", crossAx: 0, label: true } }),
        op.changeChTitle(0, 1, { text: { link: FMLA1 } }),
        op.changeChTitle(0, 1, 0, { text: { link: FMLA1 } }),

        op.insertSheet(1, "Sheet2"),
        op.insertName("local", 1, { formula: FMLA1 }),
        op.changeCells(1, { A1: 3, A2: { v: 6, f: FMLA1 }, A3: { v: "=" + FMLA1, f: "_xlfn.FORMULATEXT(A2)" } }),
        op.changeCells(1, { B1: "Col1", C1: "Col2", B2: 1, C2: 2 }),
        op.changeCells(1, { A4: { v: 6, f: FMLA1, si: 0, sr: "A4:A5" }, A5: { v: 6, si: 0 } }),
        op.insertTable(1, "T2", "B1:C2", { headerRow: true }),
        op.insertDVRule(1, 0, "B1:B3", { type: "between", value1: FMLA1, value2: FMLA1 }),
        op.insertCFRule(1, "R0", "B1:B3", { type: "between", value1: FMLA1, value2: FMLA1 }),
        op.insertShape(1, 0, { drawing: { anchor: "2 B1 0 0 C4 0 0" }, text: { link: FMLA1 } }),
        op.insertChart(1, 1, { drawing: { anchor: "2 B1 0 0 C4 0 0" } }),
        op.insertChSeries(1, 1, 0, { series: { type: "column2d", title: FMLA1, names: FMLA1, values: FMLA1 } }),
        op.changeChAxis(1, 1, 0, { axis: { axPos: "b", crossAx: 1, label: true } }),
        op.changeChAxis(1, 1, 1, { axis: { axPos: "l", crossAx: 0, label: true } }),
        op.changeChTitle(1, 1, { text: { link: FMLA1 } }),
        op.changeChTitle(1, 1, 0, { text: { link: FMLA1 } }),

        op.insertSheet(2, "Sheet3")
    ]);

    // expects that the entire document is in initial state as after import
    async function expectInitialState(docModel) {

        // global contents
        docModel.expect
            .sheetCount(3)
            .defName("global", FMLA1)
            .table("T1", 0)
            .table("T2", 1)
            .noTable("T3");

        // Sheet1
        docModel.expect.sheet(0)
            .formulas("A2", FMLA1)
            .sharedFormula(0, FMLA1, "A4:A5")
            .defName("local", FMLA1)
            .dvRule(0, "B1:B3", { dvrule: { value1: FMLA1, value2: FMLA1 } })
            .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA1, value2: FMLA1 } })
            .drawing(0, "shape", { text: { link: FMLA1 } })
            .chart(1)
                .series(0, { series: { title: FMLA1, names: FMLA1, values: FMLA1 } })
                .title(null, { text: { link: FMLA1 } })
                .title(0, { text: { link: FMLA1 } });

        // Sheet2
        docModel.expect.sheet(1)
            .formulas("A2", FMLA1)
            .sharedFormula(0, FMLA1, "A4:A5")
            .defName("local", FMLA1)
            .dvRule(0, "B1:B3", { dvrule: { value1: FMLA1, value2: FMLA1 } })
            .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA1, value2: FMLA1 } })
            .drawing(0, "shape", { text: { link: FMLA1 } })
            .chart(1)
                .series(0, { series: { title: FMLA1, names: FMLA1, values: FMLA1 } })
                .title(null, { text: { link: FMLA1 } })
                .title(0, { text: { link: FMLA1 } });

        // DOCS-1667: formulas must be recalculated after renaming the sheet
        await waitForRecalcEnd(docModel);
        docModel.expect.sheet(0).values("A3", "=" + FMLA1);
        docModel.expect.sheet(1).values("A3", "=" + FMLA1);
    }

    // class SheetCollection --------------------------------------------------

    describe("class SheetCollection", () => {

        it("should exist", () => {
            expect(SheetCollection).toBeFunction();
        });

        describe("method generateUpdateTaskOperations", () => {
            it("should exist", () => {
                expect(SheetCollection).toHaveMethod("generateUpdateTaskOperations");
            });
        });

        describe("method generateInsertSheetOperations", () => {
            it("should exist", () => {
                expect(SheetCollection).toHaveMethod("generateInsertSheetOperations");
            });
        });

        describe("method generateRenameSheetOperations", () => {
            it("should exist", () => {
                expect(SheetCollection).toHaveMethod("generateRenameSheetOperations");
            });

            describe("should update formulas after renaming a sheet", () => {

                function buildOperations(docModel) {
                    return docModel.buildOperations(builder => {
                        return docModel.sheetCollection.generateRenameSheetOperations(builder, 1, "Sheet 9");
                    });
                }

                async function expectChangedState(docModel) {
                    docModel.expect.defName("global", FMLA2);
                    docModel.expect.sheet(0)
                        .formulas("A2", FMLA2)
                        .sharedFormula(0, FMLA2, "A4:A5")
                        .defName("local", FMLA2)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA2, value2: FMLA2 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA2, value2: FMLA2 } })
                        .drawing(0, "shape", { text: { link: FMLA2 } })
                        .chart(1)
                            .series(0, { series: { title: FMLA2, names: FMLA2, values: FMLA2 } })
                            .title(null, { text: { link: FMLA2 } })
                            .title(0, { text: { link: FMLA2 } });
                    docModel.expect.sheet(1)
                        .formulas("A2", FMLA2)
                        .sharedFormula(0, FMLA2, "A4:A5")
                        .defName("local", FMLA2)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA2, value2: FMLA2 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA2, value2: FMLA2 } })
                        .drawing(0, "shape", { text: { link: FMLA2 } })
                        .chart(1)
                            .series(0, { series: { title: FMLA2, names: FMLA2, values: FMLA2 } })
                            .title(null, { text: { link: FMLA2 } })
                            .title(0, { text: { link: FMLA2 } });
                    await waitForRecalcEnd(docModel);
                    // DOCS-1667: formulas must be recalculated after renaming the sheet
                    docModel.expect.sheet(0).values("A3", "=" + FMLA2);
                    docModel.expect.sheet(1).values("A3", "=" + FMLA2);
                }

                defineModelTestWithUndo(appPromise, buildOperations, expectChangedState, expectInitialState, { redo: true });
            });
        });

        describe("method generateMoveSheetOperations", () => {
            it("should exist", () => {
                expect(SheetCollection).toHaveMethod("generateMoveSheetOperations");
            });

            describe("should update formulas after moving a sheet", () => {

                function buildOperations(docModel) {
                    return docModel.buildOperations(builder => {
                        return docModel.sheetCollection.generateMoveSheetOperations(builder, 0, 2);
                    });
                }

                async function expectChangedState(docModel) {
                    docModel.expect.sheet(0).name("Sheet2").formulas("A2", FMLA1);
                    docModel.expect.sheet(1).name("Sheet3");
                    docModel.expect.sheet(2).name("Sheet1").formulas("A2", FMLA1);
                    await waitForRecalcEnd(docModel);
                    docModel.expect.sheet(0).values("A3", "=" + FMLA1);
                    docModel.expect.sheet(2).values("A3", "=" + FMLA1);
                }

                defineModelTestWithUndo(appPromise, buildOperations, expectChangedState, expectInitialState, { redo: true });
            });
        });

        describe("method generateMoveSheetsOperations", () => {
            it("should exist", () => {
                expect(SheetCollection).toHaveMethod("generateMoveSheetsOperations");
            });

            describe("should update formulas after moving sheets", () => {

                function buildOperations(docModel) {
                    return docModel.buildOperations(builder => {
                        return docModel.sheetCollection.generateMoveSheetsOperations(builder, [2, 1, 0]);
                    });
                }

                async function expectChangedState(docModel) {
                    docModel.expect.sheet(0).name("Sheet3");
                    docModel.expect.sheet(1).name("Sheet2").formulas("A2", FMLA1);
                    docModel.expect.sheet(2).name("Sheet1").formulas("A2", FMLA1);
                    await waitForRecalcEnd(docModel);
                    docModel.expect.sheet(1).values("A3", "=" + FMLA1);
                    docModel.expect.sheet(2).values("A3", "=" + FMLA1);
                }

                defineModelTestWithUndo(appPromise, buildOperations, expectChangedState, expectInitialState, { redo: true });
            });
        });

        describe("method generateCopySheetOperations", () => {
            it("should exist", () => {
                expect(SheetCollection).toHaveMethod("generateCopySheetOperations");
            });

            describe("should update formulas after copying a sheet", () => {

                function buildOperations(docModel) {
                    return docModel.buildOperations(builder => {
                        return docModel.sheetCollection.generateCopySheetOperations(builder, 1, 2, "Sheet 9");
                    });
                }

                async function expectChangedState(docModel) {
                    docModel.expect
                        .defName("global", FMLA1)
                        .table("T1", 0)
                        .table("T2", 1)
                        .table("T3", 2);
                    docModel.expect.sheet(0)
                        .formulas("A2", FMLA1)
                        .sharedFormula(0, FMLA1, "A4:A5")
                        .defName("local", FMLA1)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA1, value2: FMLA1 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA1, value2: FMLA1 } })
                        .drawing(0, "shape", { text: { link: FMLA1 } })
                        .chart(1)
                            .series(0, { series: { title: FMLA1, names: FMLA1, values: FMLA1 } })
                            .title(null, { text: { link: FMLA1 } })
                            .title(0, { text: { link: FMLA1 } });
                    docModel.expect.sheet(1)
                        .formulas("A2", FMLA1)
                        .sharedFormula(0, FMLA1, "A4:A5")
                        .defName("local", FMLA1)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA1, value2: FMLA1 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA1, value2: FMLA1 } })
                        .drawing(0, "shape", { text: { link: FMLA1 } })
                        .chart(1)
                            .series(0, { series: { title: FMLA1, names: FMLA1, values: FMLA1 } })
                            .title(null, { text: { link: FMLA1 } })
                            .title(0, { text: { link: FMLA1 } });
                    docModel.expect.sheet(2)
                        .formulas("A2", FMLA2)
                        .sharedFormula(0, FMLA2, "A4:A5")
                        .defName("local", FMLA2)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA2, value2: FMLA2 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA2, value2: FMLA2 } })
                        .drawing(0, "shape", { text: { link: FMLA2 } })
                        .chart(1)
                            .series(0, { series: { title: FMLA2, names: FMLA2, values: FMLA2 } })
                            .title(null, { text: { link: FMLA2 } })
                            .title(0, { text: { link: FMLA2 } });
                    await waitForRecalcEnd(docModel);
                    // DOCS-1667: formulas must be recalculated after copying the sheet
                    docModel.expect.sheet(0).values("A3", "=" + FMLA1);
                    docModel.expect.sheet(1).values("A3", "=" + FMLA1);
                    docModel.expect.sheet(2).values("A3", "=" + FMLA2);
                }

                defineModelTestWithUndo(appPromise, buildOperations, expectChangedState, expectInitialState, { redo: true });
            });
        });

        // this test must run last, "deleteSheet" is currently not undoable
        describe("method generateDeleteSheetOperations", () => {
            it("should exist", () => {
                expect(SheetCollection).toHaveMethod("generateDeleteSheetOperations");
            });

            describe("should update formulas after deleting a sheet", () => {

                function buildOperations(docModel) {
                    return docModel.buildOperations(builder => {
                        return docModel.sheetCollection.generateDeleteSheetOperations(builder, 1);
                    });
                }

                async function expectChangedState(docModel) {
                    docModel.expect
                        .sheetCount(2)
                        .defName("global", FMLA3)
                        .noTable("T2");
                    docModel.expect.sheet(0)
                        .formulas("A2", FMLA3)
                        .sharedFormula(0, FMLA3, "A4:A5")
                        .defName("local", FMLA3)
                        .dvRule(0, "B1:B3", { dvrule: { value1: FMLA3, value2: FMLA3 } })
                        .cfRule("R0", "B1:B3", { cfrule: { value1: FMLA3, value2: FMLA3 } })
                        .drawing(0, "shape", { text: { link: FMLA3 } })
                        .chart(1)
                            .series(0, { series: { title: FMLA3, names: FMLA3, values: FMLA3 } })
                            .title(null, { text: { link: FMLA3 } })
                            .title(0, { text: { link: FMLA3 } });
                    await waitForRecalcEnd(docModel);
                    docModel.expect.sheet(0).values("A3", "=" + FMLA3_UI);
                }

                // currently, no undo available for "deleteSheet"
                function expectRestoredState(docModel) {
                    docModel.expect.sheetCount(2);
                }

                defineModelTestWithUndo(appPromise, buildOperations, expectChangedState, expectRestoredState, { redo: false });
            });
        });
    });
});
