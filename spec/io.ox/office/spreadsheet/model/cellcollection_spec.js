/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { Color } from "@/io.ox/office/editframework/utils/color";
import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { SubtotalResult } from "@/io.ox/office/spreadsheet/utils/subtotalresult";
import { SheetOperationContext } from "@/io.ox/office/spreadsheet/model/operation/context";
import { CellModel } from "@/io.ox/office/spreadsheet/model/cellmodel";
import { UsedRangeCollection } from "@/io.ox/office/spreadsheet/model/usedrangecollection";
import { CellCollection } from "@/io.ox/office/spreadsheet/model/cellcollection";

import * as op from "~/spreadsheet/apphelper";
import { a, r, ra, borders, mergedRangesMatcher as rangesMatcher, createSpreadsheetApp } from "~/spreadsheet/apphelper";

// constants ==================================================================

// color attribute values
const { AUTO, RED, GREEN, BLUE } = Color;

// border attribute values
const NONE = { style: "none" };
const THIN = { style: "solid", width: 26, color: AUTO };
const DBL = { style: "double", width: 79, color: AUTO };
const THIN_RED = { style: "solid", width: 26, color: RED };
const DBL_RED = { style: "double", width: 79, color: RED };

// tests ======================================================================

describe("module spreadsheet/model/cellcollection", () => {

    // the operations to be applied by the document model
    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertAutoStyle("a1", { cell: borders("tbl", THIN) }),
        op.insertAutoStyle("a2", { cell: borders("tbr", THIN) }),
        op.insertAutoStyle("a3", { cell: borders("tbl", THIN_RED) }),
        op.insertAutoStyle("a4", { cell: borders("tbr", THIN_RED) }),
        op.insertAutoStyle("a5", { cell: { fillColor: RED } }),
        op.insertAutoStyle("a6", { cell: { foreColor: BLUE } }),

        // test sheet for operation generator methods
        op.insertSheet(0, "Sheet1"),
        op.changeCells(0, { A1: { s: "a5" }, B1: { s: "a6" }, A99: 42 }),

        // test data for `CellCollection#hasHeaderRow`
        op.insertSheet(1, "Sheet2"),
        op.changeCells(1, "A1", [
            ["headline", "headline", "headline", 9],
            [2, 2, 2, "text1"],
            [3, 2, 5, 12],
            [4, 2, 4, 0],
            [5, 2, 1, "text2"]
        ]),
        op.changeCells(1, "A11", [
            ["headline", 2, 2, 4, 5],
            ["headline", 2, 5, 12, 4],
            ["headline", 2, 4, 0, 4],
            [9, "text1", 1, 5, "text2"]
        ]),

        // test data for `CellCollection#generateSubtotals`
        op.insertSheet(2, "Sheet3"),
        op.changeCells(2, "A1", [
            [1,    2,    null, "a",  4],
            [8,    16,   null, "b",  32],
            [null, null, null, "c",  null],
            ["d",  "e",  "f",  "g",  null],
            [64,   128,  null, null, 256]
        ]),

        // test data for `CellCollection#getUsedRange`
        op.insertSheet(3, "Sheet4"),
        op.changeCells(3, { C9: 1, H2: 1 }),

        // test sheet for moveInterval operations
        op.insertSheet(4, "Sheet5"),
        op.changeCols(4, "A", "a1"),
        op.changeCols(4, "B", "a2"),
        op.changeCells(4, { A2: { s: "a3" }, B2: { s: "a4" } }),
        op.insertShape(4, 0, { drawing: { anchor: "2 A1 0 0 C3 0 0" } }),
        op.insertShape(4, 1, { drawing: { anchor: "2 D4 0 0 F6 0 0" } }),
        op.insertShape(4, 2, { drawing: { anchor: "2 G7 0 0 I9 0 0" } }),
        op.insertNote(4, "A1", "note1"),
        op.insertNote(4, "D4", "note2"),
        op.insertNote(4, "G7", "note3"),
        op.insertComment(4, "B2", 0, { text: "comment1" }),
        op.insertComment(4, "E5", 0, { text: "comment2" }),
        op.insertComment(4, "H8", 0, { text: "comment3" }),

        // test sheet for `CellCollection#getUsedRange` (empty sheet)
        op.insertSheet(5, "Sheet6"),

        // test sheet for `CellCollection#applyChangeCellsOperation`
        op.insertSheet(6, "Sheet7"),

        // DOCS-4185: test sheet for hidden columns/rows with filter
        op.insertSheet(7, "Sheet8"),
        op.changeCols(7, "C:D", { column: { visible: false } }),
        op.changeRows(7, "7:8", { row: { visible: false } }),
        op.changeCells(7, "A1", [["A"], [1], [2]]),
        op.insertTable(7, null, "A1:A3", { table: { headerRow: true } })
    ];

    // initialize test document
    let docModel, sheetTester0, sheetModel0, cellCollection0;
    createSpreadsheetApp("ooxml", OPERATIONS).done(function (docApp) {
        docModel = docApp.docModel;
        sheetTester0 = docModel.expect.sheet(0);
        sheetModel0 = sheetTester0.sheetModel;
        cellCollection0 = sheetModel0.cellCollection;
    });

    // creates a new operation context
    function opc(json) {
        return new SheetOperationContext(docModel, json);
    }

    // creates a matcher function for `toSatisfy` assertion that matches attribute values
    function expectAttributes(attrs, expected) {
        expect(attrs).toBeObject();
        _.each(expected, (value, name) => {
            expect(attrs[name]).toEqual(value);
        });
    }

    function getCellCollection(sheet) {
        return docModel.getSheetModel(sheet).cellCollection;
    }

    function expectCellAttributes(sheet, ranges, expected) {
        const cellCollection = getCellCollection(sheet);
        for (const range of ra(ranges)) {
            for (const address = range.a1.clone(); address.r <= range.a2.r; address.r += 1) {
                for (address.c = range.a1.c; address.c <= range.a2.c; address.c += 1) {
                    const cellAttrs = cellCollection.getAttributeSet(address).cell;
                    expectAttributes(cellAttrs, expected);
                }
            }
        }
    }

    function expectCellAutoStyle(sheet, ranges, expected) {
        const cellCollection = getCellCollection(sheet);
        for (const range of ra(ranges)) {
            for (const address = range.a1.clone(); address.r <= range.a2.r; address.r += 1) {
                for (address.c = range.a1.c; address.c <= range.a2.c; address.c += 1) {
                    expect(cellCollection.getStyleId(address)).toBe(expected);
                }
            }
        }
    }

    // changes the value of a single cell
    function changeCell(sheet, address, value) {
        const contents = {};
        contents[address] = value;
        docModel.invokeOperationHandlers(op.changeCells(sheet, contents));
    }

    // class CellCollection ---------------------------------------------------

    describe("class CellCollection", () => {

        it("should subclass UsedRangeCollection", () => {
            expect(CellCollection).toBeSubClassOf(UsedRangeCollection);
        });

        describe("method getSheetIndex", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("getSheetIndex");
            });
        });

        describe("method getDefaultStyleId", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("getDefaultStyleId");
            });
        });

        describe("method isVisibleCell", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("isVisibleCell");
            });
        });

        describe("method getCellModel", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("getCellModel");
            });
            it("should return a cell model", () => {
                const cellModel = cellCollection0.getCellModel(a("A99"));
                expect(cellModel).toBeInstanceOf(CellModel);
                expect(cellModel.a).toStringifyTo("A99");
                expect(cellModel.v).toBe(42);
            });
            it("should return null for undefined cells", () => {
                expect(cellCollection0.getCellModel(a("ZZ1000"))).toBeNull();
            });
        });

        describe("method isBlankCell", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("isBlankCell");
            });
            it("should return false for value cells", () => {
                expect(cellCollection0.isBlankCell(a("A99"))).toBeFalse();
            });
            it("should return true for blank cells", () => {
                expect(cellCollection0.isBlankCell(a("A1"))).toBeTrue();
                expect(cellCollection0.isBlankCell(a("ZZ1000"))).toBeTrue();
            });
        });

        describe("method getValue", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("getValue");
            });
            it("should return value of cell", () => {
                expect(cellCollection0.getValue(a("A99"))).toBe(42);
            });
            it("should return null for blank cells", () => {
                expect(cellCollection0.getValue(a("A1"))).toBeNull();
                expect(cellCollection0.getValue(a("ZZ1000"))).toBeNull();
            });
        });

        describe("method getUsedRange", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("getUsedRange");
            });
            it("should return the used area", () => {
                const cellCollection5 = getCellCollection(5);
                expect(cellCollection5.getUsedRange()).toBeUndefined();
                const cellCollection3 = getCellCollection(3);
                expect(cellCollection3.getUsedRange()).toStringifyTo("C2:H9");
            });
        });

        describe("method hasHeaderRow", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("hasHeaderRow");
            });
            it("should detect headlines", () => {
                const cellCollection = getCellCollection(1);
                expect(cellCollection.hasHeaderRow(r("A1:D5"))).toBeTrue();
                expect(cellCollection.hasHeaderRow(r("A2:D5"))).toBeFalse();
                expect(cellCollection.hasHeaderRow(r("A11:D15"))).toBeFalse();
                expect(cellCollection.hasHeaderRow(r("B11:D15"))).toBeFalse();
            });
        });

        describe("method cellEntries", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("cellEntries");
            });
        });

        describe("method linearCellEntries", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("linearCellEntries");
            });
        });

        describe("method styleRanges", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("styleRanges");
            });
        });

        describe("method generateSubtotals", () => {

            let cellCollection = null;
            beforeAll(() => { cellCollection = getCellCollection(2); });

            function expectSubtotals(ranges, expSum, expCells) {
                const iterator = cellCollection.generateSubtotals(ranges);
                const result = new SubtotalResult();
                for (const next of iterator) { result.add(next); }
                expect(result.sum).toBe(expSum);
                expect(result.cells).toBe(expCells);
            }

            it("should exist", () => {
                expect(CellCollection).toHaveMethod("generateSubtotals");
            });
            it("should return the correct subtotal results", () => {
                expectSubtotals(r("A1:A1"), 1, 1);
                expectSubtotals(r("A1:B2"), 27, 4);
                expectSubtotals(r("A1:C3"), 27, 4);
                expectSubtotals(r("A1:D4"), 27, 11);
                expectSubtotals(r("A1:E5"), 511, 16);
                expectSubtotals(r("B2:E5"), 432, 9);
                expectSubtotals(r("C3:E5"), 256, 4);
                expectSubtotals(r("D4:E5"), 256, 2);
                expectSubtotals(r("E5:E5"), 256, 1);
                expectSubtotals(ra("A1:D4 B2:E5"), 443, 14);
                expectSubtotals(ra("A1:C1048576 C3:E5"), 475, 12);
                expectSubtotals(ra("A1:XFD3 C3:E5"), 319, 12);
                expectSubtotals(ra("A1:C1048576 A1:XFD3"), 255, 14);
            });
            it("should return the correct subtotal results with hidden columns/rows", () => {
                docModel.invokeOperationHandlers([
                    op.changeRows(2, "2", { row: { visible: false } }),
                    op.changeCols(2, "B", { column: { visible: false } })
                ]);
                expectSubtotals(r("A1:A1"), 1, 1);
                expectSubtotals(r("A1:B2"), 1, 1);
                expectSubtotals(r("A1:C3"), 1, 1);
                expectSubtotals(r("A1:D4"), 1, 6);
                expectSubtotals(r("A1:E5"), 325, 9);
                expectSubtotals(r("B2:E5"), 256, 4);
                expectSubtotals(r("C3:E5"), 256, 4);
                expectSubtotals(r("D4:E5"), 256, 2);
                expectSubtotals(r("E5:E5"), 256, 1);
                expectSubtotals(ra("A1:D4 B2:E5"), 257, 7);
                expectSubtotals(ra("A1:C1048576 C3:E5"), 321, 7);
                expectSubtotals(ra("A1:XFD3 C3:E5"), 261, 7);
                expectSubtotals(ra("A1:C1048576 A1:XFD3"), 69, 7);
            });
            it("should return the correct subtotal results after changing a cell", () => {
                changeCell(2, "E4", 0.5);
                expectSubtotals(r("A1:A1"), 1, 1);
                expectSubtotals(r("A1:B2"), 1, 1);
                expectSubtotals(r("A1:C3"), 1, 1);
                expectSubtotals(r("A1:D4"), 1, 6);
                expectSubtotals(r("A1:E5"), 325.5, 10);
                expectSubtotals(r("B2:E5"), 256.5, 5);
                expectSubtotals(r("C3:E5"), 256.5, 5);
                expectSubtotals(r("D4:E5"), 256.5, 3);
                expectSubtotals(r("E5:E5"), 256, 1);
                expectSubtotals(ra("A1:D4 B2:E5"), 257.5, 8);
                expectSubtotals(ra("A1:C1048576 C3:E5"), 321.5, 8);
                expectSubtotals(ra("A1:XFD3 C3:E5"), 261.5, 8);
                expectSubtotals(ra("A1:C1048576 A1:XFD3"), 69, 7);
            });
        });

        describe("method applyChangeCellsOperation", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("applyChangeCellsOperation");
            });
            it("should apply simple value changes", () => {
                const cellCollection = getCellCollection(6);
                cellCollection.applyChangeCellsOperation(opc({ contents: { A1: 42, B2: "abc", "C3:D4": true } }));
                expect(cellCollection.getValue(a("A1"))).toBe(42);
                expect(cellCollection.getValue(a("B2"))).toBe("abc");
                expect(cellCollection.getValue(a("C3"))).toBeTrue();
                expect(cellCollection.getValue(a("D3"))).toBeTrue();
                expect(cellCollection.getValue(a("C4"))).toBeTrue();
                expect(cellCollection.getValue(a("D4"))).toBeTrue();
            });
            it("should apply error codes", () => {
                const cellCollection = getCellCollection(6);
                cellCollection.applyChangeCellsOperation(opc({ contents: { C3: { e: "#DIV/0!" }, D4: "#DIV/0!" } }));
                expect(cellCollection.getValue(a("C3"))).toBe(ErrorCode.DIV0);
                expect(cellCollection.getValue(a("D3"))).toBeTrue();
                expect(cellCollection.getValue(a("C4"))).toBeTrue();
                expect(cellCollection.getValue(a("D4"))).toBe("#DIV/0!");
            });
            it("should undefine a cell", () => {
                const cellCollection = getCellCollection(6);
                expect(cellCollection.getCellModel(a("A1"))).toBeInstanceOf(CellModel);
                cellCollection.applyChangeCellsOperation(opc({ contents: { A1: { u: true } } }));
                expect(cellCollection.getCellModel(a("A1"))).toBeNull();
            });
            it("should redefine a cell", () => {
                const cellCollection = getCellCollection(6);
                expect(cellCollection.getValue(a("B2"))).toBe("abc");
                cellCollection.applyChangeCellsOperation(opc({ contents: { B2: { s: "a1", u: true } } }));
                expect(cellCollection.getCellModel(a("B2"))).toBeInstanceOf(CellModel);
                expect(cellCollection.getValue(a("B2"))).toBeNull();
            });
        });

        describe("method parseCellValue", () => {

            function expectParseResult(parseText, exp) {
                const parseResult = cellCollection0.parseCellValue(parseText, "val", a("A1"));
                expect(parseResult).toHaveProperty("f", exp.f || null);
                if (exp.type) {
                    expect(parseResult).toHaveProperty("v", (exp.v === null) ? 0 : exp.v);
                    expect(parseResult.result).toBeObject();
                    expect(parseResult.result).toHaveProperty("type", exp.type);
                    expect(parseResult.result).toHaveProperty("value", exp.v);
                    if (exp.code) {
                        expect(parseResult.result).toHaveProperty("code", exp.code);
                    } else {
                        expect(parseResult.result).not.toHaveProperty("code");
                    }
                    if (exp.format) {
                        expect(parseResult.result.format).toBeObject();
                        expect(parseResult.result.format).toHaveProperty("formatCode", exp.format);
                    } else {
                        expect(parseResult.result).not.toHaveProperty("format");
                    }
                } else {
                    expect(parseResult).toHaveProperty("v", exp.v);
                    expect(parseResult).toHaveProperty("result", null);
                }
                if (exp.format && (exp.format !== "General")) {
                    expect(parseResult).toHaveProperty("format", exp.format);
                } else {
                    expect(parseResult).not.toHaveProperty("format");
                }
            }

            it("should exist", () => {
                expect(CellCollection).toHaveMethod("parseCellValue");
            });
            it("should parse literal values", () => {
                expectParseResult("", { v: null });
                expectParseResult("0", { v: 0 });
                expectParseResult("42,5", { v: 42.5 });
                expectParseResult("-42", { v: -42 });
                expectParseResult("wahr", { v: true });
                expectParseResult("Falsch", { v: false });
                expectParseResult("#Wert!", { v: ErrorCode.VALUE });
                expectParseResult("abc", { v: "abc" });
                expectParseResult("0a", { v: "0a" });
                expectParseResult(" wahr", { v: " wahr" });
                expectParseResult("true", { v: "true" });
                expectParseResult("#value!", { v: "#value!" });
            });
            it("should parse formatted numbers", () => {
                expectParseResult("1e3", { v: 1000, format: "0.00E+00" });
                expectParseResult("-100 %", { v: -1, format: "0%" });
                expectParseResult("24.4.1977", { v: 28239, format: "DD.MM.YYYY" });
            });
            it("should parse formula expressions", () => {
                expectParseResult("=1+1", { v: 2, f: "1+1", type: "valid" });
                expectParseResult("=SUMME(1;2;3)", { v: 6, f: "SUM(1,2,3)", type: "valid" });
                expectParseResult("=DATUM(2015;1;1)", { v: 42005, f: "DATE(2015,1,1)", type: "valid", format: "DD.MM.YYYY" });
                expectParseResult("+2*3", { v: 6, f: "+2*3", type: "valid" });
                expectParseResult("-2*3", { v: -6, f: "-2*3", type: "valid" });
                expectParseResult("=1+", { v: ErrorCode.NA, f: "1+", type: "error", code: "missing" });
                expectParseResult("=ZZ9999", { v: null, f: "ZZ9999", type: "valid" });
            });
            it("should handle leading apostrophes", () => {
                expectParseResult("'0", { v: "0" });
                expectParseResult("'WAHR", { v: "WAHR" });
                expectParseResult("'#WERT!", { v: "#WERT!" });
                expectParseResult("'=1+1", { v: "=1+1" });
            });
            it("should not parse special repeated characters as formulas", () => {
                expectParseResult("=", { v: "=" });
                expectParseResult("+", { v: "+" });
                expectParseResult("-", { v: "-" });
                expectParseResult("===", { v: "===" });
                expectParseResult("+++", { v: "+++" });
                expectParseResult("---", { v: "---" });
            });
        });

        describe("method generateCellOperations", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("generateCellOperations");
            });
            it("should generate correct cell operations", async () => {
                sheetTester0.blankCells("A1");
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateCellOperations(generator, a("A1"), { v: 42 });
                });
                expect(changed).toEqual(ra("A1:A1"));
                sheetTester0.values("A1", 42);
                await docModel.undoManager.undo();
                sheetTester0.blankCells("A1");
            });
        });

        describe("method generateCellsOperations", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("generateCellsOperations");
            });
            it("should generate correct cell operations", async () => {
                sheetTester0.blankCells("A1 B2 C3");
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateCellsOperations(generator, [
                        { address: a("A1"), v: 42 }, { address: a("C3"), v: "42" }, { address: a("B2"), v: true }
                    ]);
                });
                expect(changed).toEqual(ra("A1 B2 C3"));
                sheetTester0.values({ A1: 42, B2: true, C3: "42" });
                await docModel.undoManager.undo();
                sheetTester0.blankCells("A1 B2 C3");
            });
        });

        describe("method generateRangeOperations", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("generateRangeOperations");
            });
        });

        describe("method generateFillOperations", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("generateFillOperations");
            });
            it("should generate cell operations for an auto-style", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateFillOperations(generator, r("A1:B2"), { s: "a6" });
                });
                expect(changed).toStringifyTo("A1:A1,A2:B2");
                expectCellAutoStyle(0, "A1:B2", "a6");
                await docModel.undoManager.undo();
                expectCellAutoStyle(0, "A1", "a5");
                expectCellAutoStyle(0, "B1", "a6");
                expectCellAutoStyle(0, "A2:B2", "a0");
            });
            it("should generate cell operations for explicit attribute equal to auto-style", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateFillOperations(generator, r("A1:B2"), { a: { cell: { foreColor: BLUE } } });
                });
                expect(changed).toStringifyTo("A1:A1,A2:B2");
                expectCellAttributes(0, "A1", { fillColor: RED, foreColor: BLUE });
                expectCellAutoStyle(0, "B1 A2:B2", "a6");
                await docModel.undoManager.undo();
                expectCellAutoStyle(0, "A1", "a5");
                expectCellAutoStyle(0, "B1", "a6");
                expectCellAutoStyle(0, "A2:B2", "a0");
            });
            it("should generate cell operations for explicit attribute resulting in new auto-style", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateFillOperations(generator, r("A1:B2"), { a: { cell: { foreColor: GREEN } } });
                });
                expect(changed).toStringifyTo("A1:B2");
                expectCellAttributes(0, "A1", { fillColor: RED, foreColor: GREEN });
                expectCellAttributes(0, "B1 A2:B2", { fillColor: AUTO, foreColor: GREEN });
                await docModel.undoManager.undo();
                expectCellAutoStyle(0, "A1", "a5");
                expectCellAutoStyle(0, "B1", "a6");
                expectCellAutoStyle(0, "A2:B2", "a0");
            });
            it("should generate cell operations for an auto-style in entire column", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateFillOperations(generator, r("A1:B1048576"), { s: "a6" });
                });
                expect(changed).toStringifyTo("A1:B1048576");
                expectCellAutoStyle(0, "A1:B3 A1048576:B1048576", "a6");
                await docModel.undoManager.undo();
                expectCellAutoStyle(0, "A1", "a5");
                expectCellAutoStyle(0, "B1", "a6");
                expectCellAutoStyle(0, "A2:B3 A1048576:B1048576", "a0");
            });
            it("should generate cell operations for explicit attribute equal to auto-style in entire column", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateFillOperations(generator, r("A1:B1048576"), { a: { cell: { foreColor: BLUE } } });
                });
                expect(changed).toStringifyTo("A1:B1048576");
                expectCellAttributes(0, "A1", { fillColor: RED, foreColor: BLUE });
                expectCellAutoStyle(0, "B1 A2:B3 A1048576:B1048576", "a6");
                await docModel.undoManager.undo();
                expectCellAutoStyle(0, "A1", "a5");
                expectCellAutoStyle(0, "B1", "a6");
                expectCellAutoStyle(0, "A2:B3 A1048576:B1048576", "a0");
            });
            it("should generate cell operations for explicit attribute resulting in new auto-style in entire column", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateFillOperations(generator, r("A1:B1048576"), { a: { cell: { foreColor: GREEN } } });
                });
                expect(changed).toStringifyTo("A1:B1048576");
                expectCellAttributes(0, "A1", { fillColor: RED, foreColor: GREEN });
                expectCellAttributes(0, "B1 A2:B3 A1048576:B1048576", { fillColor: AUTO, foreColor: GREEN });
                await docModel.undoManager.undo();
                expectCellAutoStyle(0, "A1", "a5");
                expectCellAutoStyle(0, "B1", "a6");
                expectCellAutoStyle(0, "A2:B3 A1048576:B1048576", "a0");
            });
            // DOCS-4185
            it("should not skip hidden rows and columns in unfiltered sheets", async () => {
                const sheetModel = docModel.getSheetModel(7);
                await sheetModel.createAndApplyOperations(generator => {
                    return sheetModel.cellCollection.generateFillOperations(generator, r("B6:E9"), { v: "ab" });
                });
                const sheetTester = docModel.expect.sheet(7);
                sheetTester.values("B6:E9", "ab");
                await docModel.undoManager.undo();
                sheetTester.values("B6:E9", null);
            });
            it("should skip hidden rows and columns in filtered sheets", async () => {
                docModel.invokeOperationHandlers(op.changeTableCol(7, null, 0, { attrs: { filter: { type: "discrete" } } }));
                const sheetModel = docModel.getSheetModel(7);
                await sheetModel.createAndApplyOperations(generator => {
                    return sheetModel.cellCollection.generateFillOperations(generator, r("B6:E9"), { v: "ab" });
                });
                const sheetTester = docModel.expect.sheet(7);
                sheetTester.values("B6:E9", [
                    ["ab", null, null, "ab"],
                    [null, null, null, null],
                    [null, null, null, null],
                    ["ab", null, null, "ab"]
                ]);
                await docModel.undoManager.undo();
                sheetTester.values("B6:E9", null);
            });
        });

        describe("method generateBorderOperations", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("generateBorderOperations");
            });
            it("should create a border around a range of cells", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateBorderOperations(generator, r("B2:E5"), borders("tblr", THIN));
                });
                expect(changed).toSatisfy(rangesMatcher("B2:E2 B3:B4 E3:E4 B5:E5"));
                expectCellAttributes(0, "B2:B2", borders("tl", THIN, "br", NONE));
                expectCellAttributes(0, "C2:D2", borders("t", THIN, "blr", NONE));
                expectCellAttributes(0, "E2:E2", borders("tr", THIN, "bl", NONE));
                expectCellAttributes(0, "B3:B4", borders("l", THIN, "tbr", NONE));
                expectCellAttributes(0, "E3:E4", borders("r", THIN, "tbl", NONE));
                expectCellAttributes(0, "B5:B5", borders("bl", THIN, "tr", NONE));
                expectCellAttributes(0, "C5:D5", borders("b", THIN, "tlr", NONE));
                expectCellAttributes(0, "E5:E5", borders("br", THIN, "tl", NONE));
                await docModel.undoManager.undo();
                expectCellAttributes(0, "B2:E5", borders("tblr", NONE));
                await docModel.undoManager.redo();
            });
            it("should merge a new border over an existing border", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateBorderOperations(generator, ra("E4:F6 C2:C2 C6:C6"), borders("tblr", DBL));
                });
                expect(changed).toSatisfy(rangesMatcher("E4:F6 C2:C2 C5:C6"));
                expectCellAttributes(0, "E4:E4", borders("tl", DBL, "r", THIN, "b", NONE));
                expectCellAttributes(0, "F4:F4", borders("tr", DBL, "bl", NONE));
                expectCellAttributes(0, "E5:E5", borders("l", DBL, "br", THIN, "t", NONE));
                expectCellAttributes(0, "F5:F5", borders("r", DBL, "tbl", NONE));
                expectCellAttributes(0, "E6:E6", borders("bl", DBL, "tr", NONE));
                expectCellAttributes(0, "F6:F6", borders("br", DBL, "tl", NONE));
                expectCellAttributes(0, "C1:C1", borders("tblr", NONE));
                expectCellAttributes(0, "C2:C2", borders("tblr", DBL));
                expectCellAttributes(0, "C5:C5", borders("tblr", NONE));
                expectCellAttributes(0, "C6:C6", borders("tblr", DBL));
                await docModel.undoManager.undo();
                expectCellAttributes(0, "E4:E4", borders("r", THIN, "tbl", NONE));
                expectCellAttributes(0, "E5:E5", borders("br", THIN, "tl", NONE));
                expectCellAttributes(0, "E6:E6", borders("tblr", NONE));
                expectCellAttributes(0, "F4:F6", borders("tblr", NONE));
                expectCellAttributes(0, "C2:C2", borders("t", THIN, "blr", NONE));
                expectCellAttributes(0, "C5:C5", borders("b", THIN, "tlr", NONE));
                expectCellAttributes(0, "C6:C6", borders("tblr", NONE));
                await docModel.undoManager.redo();
            });
            it("should fill a range with inner borders", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateBorderOperations(generator, r("A10:E12"), borders("tblrhv", THIN));
                });
                expect(changed).toSatisfy(rangesMatcher("A10:E12"));
                expectCellAttributes(0, "A10:E12", borders("tblr", THIN));
                await docModel.undoManager.undo();
                expectCellAttributes(0, "A10:E12", borders("tblr", NONE));
                await docModel.undoManager.redo();
            });
            it("should remove adjacent borders when deleting borders", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateBorderOperations(generator, r("B11:B11"), borders("tblr", NONE));
                });
                expect(changed).toSatisfy(rangesMatcher("B10:B10 A11:C11 B12:B12"));
                expectCellAttributes(0, "B10:B10", borders("tlr", THIN, "b", NONE));
                expectCellAttributes(0, "A11:A11", borders("tbl", THIN, "r", NONE));
                expectCellAttributes(0, "B11:B11", borders("tblr", NONE));
                expectCellAttributes(0, "C11:C11", borders("tbr", THIN, "l", NONE));
                expectCellAttributes(0, "B12:B12", borders("blr", THIN, "t", NONE));
                await docModel.undoManager.undo();
                expectCellAttributes(0, "A10:C12", borders("tblr", THIN));
                await docModel.undoManager.redo();
            });
            it("should remove adjacent borders when setting different borders", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateBorderOperations(generator, r("D11:D11"), borders("tblr", DBL));
                });
                expect(changed).toSatisfy(rangesMatcher("D10:D10 C11:E11 D12:D12"));
                expectCellAttributes(0, "D10:D10", borders("tlr", THIN, "b", NONE));
                expectCellAttributes(0, "C11:C11", borders("tb", THIN, "lr", NONE));
                expectCellAttributes(0, "D11:D11", borders("tblr", DBL));
                expectCellAttributes(0, "E11:E11", borders("tbr", THIN, "l", NONE));
                expectCellAttributes(0, "D12:D12", borders("blr", THIN, "t", NONE));
                await docModel.undoManager.undo();
                expectCellAttributes(0, "D10:E12", borders("tblr", THIN));
                expectCellAttributes(0, "C11:C11", borders("tbr", THIN, "l", NONE));
                await docModel.undoManager.redo();
            });
            // bug 66003: check correct order of undo operations
            it("should create borders for entire columns, and succeed undoing", async () => {
                await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateBorderOperations(generator, r("C1:C1048576"), borders("tblr", DBL));
                }, { applyImmediately: true });
                await docModel.undoManager.undo();
            });
        });

        describe("method generateVisibleBorderOperations", () => {
            it("should exist", () => {
                expect(CellCollection).toHaveMethod("generateVisibleBorderOperations");
            });
            it("should change border color of existing borders", async () => {
                const changed = await sheetModel0.createAndApplyOperations(generator => {
                    return cellCollection0.generateVisibleBorderOperations(generator, r("B1:D5"), { color: RED });
                });
                expect(changed).toSatisfy(rangesMatcher("B2:D2 B3:B5 D5:D5 C6:C6 E4:E5"));
                expectCellAttributes(0, "B2:B2", borders("tl", THIN_RED, "br", NONE));
                expectCellAttributes(0, "C2:C2", borders("tblr", DBL_RED));
                expectCellAttributes(0, "D2:D2", borders("t", THIN_RED, "blr", NONE));
                expectCellAttributes(0, "B3:B4", borders("l", THIN_RED, "tbr", NONE));
                expectCellAttributes(0, "B5:B5", borders("bl", THIN_RED, "tr", NONE));
                expectCellAttributes(0, "D5:D5", borders("b", THIN_RED, "tlr", NONE));
                expectCellAttributes(0, "C6:C6", borders("t", DBL_RED, "blr", DBL));
                expectCellAttributes(0, "E4:E4", borders("l", DBL_RED, "t", DBL, "r", THIN, "b", NONE));
                expectCellAttributes(0, "E5:E5", borders("l", DBL_RED, "br", THIN, "t", NONE));
                await docModel.undoManager.undo();
                expectCellAttributes(0, "B2:B2", borders("tl", THIN, "br", NONE));
                expectCellAttributes(0, "C2:C2", borders("tblr", DBL));
                expectCellAttributes(0, "D2:D2", borders("t", THIN, "blr", NONE));
                expectCellAttributes(0, "B3:B4", borders("l", THIN, "tbr", NONE));
                expectCellAttributes(0, "B5:B5", borders("bl", THIN, "tr", NONE));
                expectCellAttributes(0, "D5:D5", borders("b", THIN, "tlr", NONE));
                expectCellAttributes(0, "C6:C6", borders("tblr", DBL));
                expectCellAttributes(0, "E4:E4", borders("tl", DBL, "r", THIN, "b", NONE));
                expectCellAttributes(0, "E5:E5", borders("l", DBL, "br", THIN, "t", NONE));
                await docModel.undoManager.redo();
            });
        });
    });
});
