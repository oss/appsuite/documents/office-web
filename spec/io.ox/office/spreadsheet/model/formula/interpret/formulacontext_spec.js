/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { FormatCategory } from "@/io.ox/office/editframework/model/formatter/parsedsection";
import { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";
import { combineParsedFormats, FormulaContext } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulacontext";

// tests ======================================================================

describe("module spreadsheet/model/formula/interpret/formulacontext", () => {

    // functions --------------------------------------------------------------

    describe("function combineParsedFormats", () => {

        var stdFormat = new ParsedFormat("", FormatCategory.STANDARD);
        var percentFormat = new ParsedFormat("", FormatCategory.PERCENT);
        var currFormat = new ParsedFormat("", FormatCategory.CURRENCY);
        var fracFormat = new ParsedFormat("", FormatCategory.FRACTION);
        var dateFormat = new ParsedFormat("", FormatCategory.DATE);
        var timeFormat = new ParsedFormat("", FormatCategory.TIME);
        var dateTimeFormat = new ParsedFormat("", FormatCategory.DATETIME);

        it("should exist", () => {
            expect(combineParsedFormats).toBeFunction();
        });
        it("should return null without if any format is null", () => {
            expect(combineParsedFormats(null, null)).toBeNull();
            expect(combineParsedFormats(stdFormat, null)).toBeNull();
            expect(combineParsedFormats(percentFormat, null)).toBeNull();
            expect(combineParsedFormats(currFormat, null)).toBeNull();
            expect(combineParsedFormats(fracFormat, null)).toBeNull();
            expect(combineParsedFormats(dateFormat, null)).toBeNull();
            expect(combineParsedFormats(timeFormat, null)).toBeNull();
            expect(combineParsedFormats(dateTimeFormat, null)).toBeNull();
            expect(combineParsedFormats(null, stdFormat)).toBeNull();
            expect(combineParsedFormats(null, percentFormat)).toBeNull();
            expect(combineParsedFormats(null, currFormat)).toBeNull();
            expect(combineParsedFormats(null, fracFormat)).toBeNull();
            expect(combineParsedFormats(null, dateFormat)).toBeNull();
            expect(combineParsedFormats(null, timeFormat)).toBeNull();
            expect(combineParsedFormats(null, dateTimeFormat)).toBeNull();
        });
        it("should skip standard category", () => {
            expect(combineParsedFormats(stdFormat, stdFormat)).toBe(stdFormat);

            expect(combineParsedFormats(percentFormat, stdFormat)).toBe(percentFormat);
            expect(combineParsedFormats(currFormat, stdFormat)).toBe(currFormat);
            expect(combineParsedFormats(fracFormat, stdFormat)).toBe(fracFormat);
            expect(combineParsedFormats(dateFormat, stdFormat)).toBe(dateFormat);
            expect(combineParsedFormats(timeFormat, stdFormat)).toBe(timeFormat);
            expect(combineParsedFormats(dateTimeFormat, stdFormat)).toBe(dateTimeFormat);

            expect(combineParsedFormats(stdFormat, percentFormat)).toBe(percentFormat);
            expect(combineParsedFormats(stdFormat, currFormat)).toBe(currFormat);
            expect(combineParsedFormats(stdFormat, fracFormat)).toBe(fracFormat);
            expect(combineParsedFormats(stdFormat, dateFormat)).toBe(dateFormat);
            expect(combineParsedFormats(stdFormat, timeFormat)).toBe(timeFormat);
            expect(combineParsedFormats(stdFormat, dateTimeFormat)).toBe(dateTimeFormat);
        });
        it("should add different category commbinations", () => {
            expect(combineParsedFormats(percentFormat, percentFormat)).toBe(percentFormat);
            expect(combineParsedFormats(percentFormat, currFormat)).toBe(percentFormat);
            expect(combineParsedFormats(percentFormat, fracFormat)).toBe(percentFormat);
            expect(combineParsedFormats(percentFormat, dateFormat)).toBe(dateFormat);
            expect(combineParsedFormats(percentFormat, timeFormat)).toBe(timeFormat);
            expect(combineParsedFormats(percentFormat, dateTimeFormat)).toBe(dateTimeFormat);

            expect(combineParsedFormats(currFormat, percentFormat)).toBe(currFormat);
            expect(combineParsedFormats(currFormat, currFormat)).toBe(currFormat);
            expect(combineParsedFormats(currFormat, fracFormat)).toBe(currFormat);
            expect(combineParsedFormats(currFormat, dateFormat)).toBe(dateFormat);
            expect(combineParsedFormats(currFormat, timeFormat)).toBe(timeFormat);
            expect(combineParsedFormats(currFormat, dateTimeFormat)).toBe(dateTimeFormat);

            expect(combineParsedFormats(fracFormat, percentFormat)).toBe(fracFormat);
            expect(combineParsedFormats(fracFormat, currFormat)).toBe(fracFormat);
            expect(combineParsedFormats(fracFormat, fracFormat)).toBe(fracFormat);
            expect(combineParsedFormats(fracFormat, dateFormat)).toBe(dateFormat);
            expect(combineParsedFormats(fracFormat, timeFormat)).toBe(timeFormat);
            expect(combineParsedFormats(fracFormat, dateTimeFormat)).toBe(dateTimeFormat);

            expect(combineParsedFormats(dateFormat, percentFormat)).toBe(dateFormat);
            expect(combineParsedFormats(dateFormat, currFormat)).toBe(dateFormat);
            expect(combineParsedFormats(dateFormat, fracFormat)).toBe(dateFormat);
            expect(combineParsedFormats(dateFormat, dateFormat)).toBeNull();
            expect(combineParsedFormats(dateFormat, timeFormat)).toBeNull();
            expect(combineParsedFormats(dateFormat, dateTimeFormat)).toBeNull();

            expect(combineParsedFormats(timeFormat, percentFormat)).toBe(timeFormat);
            expect(combineParsedFormats(timeFormat, currFormat)).toBe(timeFormat);
            expect(combineParsedFormats(timeFormat, fracFormat)).toBe(timeFormat);
            expect(combineParsedFormats(timeFormat, dateFormat)).toBeNull();
            expect(combineParsedFormats(timeFormat, timeFormat)).toBeNull();
            expect(combineParsedFormats(timeFormat, dateTimeFormat)).toBeNull();

            expect(combineParsedFormats(dateTimeFormat, percentFormat)).toBe(dateTimeFormat);
            expect(combineParsedFormats(dateTimeFormat, currFormat)).toBe(dateTimeFormat);
            expect(combineParsedFormats(dateTimeFormat, fracFormat)).toBe(dateTimeFormat);
            expect(combineParsedFormats(dateTimeFormat, dateFormat)).toBeNull();
            expect(combineParsedFormats(dateTimeFormat, timeFormat)).toBeNull();
            expect(combineParsedFormats(dateTimeFormat, dateTimeFormat)).toBeNull();
        });
    });

    // class FormulaContext ---------------------------------------------------

    describe("class FormulaContext", () => {
        it("should exist", () => {
            expect(FormulaContext).toBeClass();
        });
    });
});
