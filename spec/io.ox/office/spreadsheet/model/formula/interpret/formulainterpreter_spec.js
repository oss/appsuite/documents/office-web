/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { InternalErrorCode } from "@/io.ox/office/spreadsheet/model/formula/formulautils";
import { FormulaInterpreter } from "@/io.ox/office/spreadsheet/model/formula/interpret/formulainterpreter";

import { ErrorCode, createSpreadsheetApp } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/interpret/formulainterpreter", () => {

    // initialize test document
    let interpreter = null;
    createSpreadsheetApp("ooxml").done(function (docApp) {
        interpreter = new FormulaInterpreter(docApp.docModel);
    });

    // class FormulaInterpreter -----------------------------------------------

    describe("class FormulaInterpreter", () => {

        it("should exist", () => {
            expect(FormulaInterpreter).toBeFunction();
        });

        describe("constructor", () => {
            it("should create an instance", () => {
                expect(interpreter).toBeInstanceOf(FormulaInterpreter);
            });
        });

        describe("method equalScalars", () => {
            it("should exist", () => {
                expect(FormulaInterpreter).toHaveMethod("equalScalars");
            });
            it("should compare scalars", () => {
                expect(interpreter.equalScalars(0, 0)).toBeTrue();
                expect(interpreter.equalScalars(0, 42)).toBeFalse();
                expect(interpreter.equalScalars("", "")).toBeTrue();
                expect(interpreter.equalScalars("", "abc")).toBeFalse();
                expect(interpreter.equalScalars("abc", "ABC")).toBeTrue();
                expect(interpreter.equalScalars(false, false)).toBeTrue();
                expect(interpreter.equalScalars(false, true)).toBeFalse();
                expect(interpreter.equalScalars(ErrorCode.NULL, ErrorCode.NULL)).toBeTrue();
                expect(interpreter.equalScalars(ErrorCode.NULL, ErrorCode.VALUE)).toBeFalse();
                expect(interpreter.equalScalars(0, "")).toBeFalse();
                expect(interpreter.equalScalars(0, false)).toBeFalse();
                expect(interpreter.equalScalars("", false)).toBeFalse();
            });
        });

        describe("method compareScalars", () => {
            it("should exist", () => {
                expect(FormulaInterpreter).toHaveMethod("compareScalars");
            });
            it("should compare scalars", () => {
                expect(interpreter.compareScalars(0, 0)).toBe(0);
                expect(interpreter.compareScalars(0, 42)).toBe(-1);
                expect(interpreter.compareScalars(42, 0)).toBe(1);
                expect(interpreter.compareScalars("", "")).toBe(0);
                expect(interpreter.compareScalars("", "abc")).toBe(-1);
                expect(interpreter.compareScalars("abc", "")).toBe(1);
                expect(interpreter.compareScalars("abc", "ABC")).toBe(0);
                expect(interpreter.compareScalars(false, false)).toBe(0);
                expect(interpreter.compareScalars(false, true)).toBe(-1);
                expect(interpreter.compareScalars(true, false)).toBe(1);
                expect(interpreter.compareScalars(ErrorCode.NULL, ErrorCode.NULL)).toBe(0);
                expect(interpreter.compareScalars(ErrorCode.NULL, ErrorCode.VALUE)).toBe(-1);
                expect(interpreter.compareScalars(ErrorCode.VALUE, ErrorCode.NULL)).toBe(1);
                expect(interpreter.compareScalars(0, "")).toBe(-1);
                expect(interpreter.compareScalars(0, false)).toBe(-1);
                expect(interpreter.compareScalars("", false)).toBe(-1);
            });
        });

        describe("method createWarnResult", () => {
            it("should exist", () => {
                expect(FormulaInterpreter).toHaveMethod("createWarnResult");
            });
            it("should create a warning result", () => {
                expect(interpreter.createWarnResult("val", ErrorCode.NULL)).toEqual({ type: "warn", code: "NULL", value: ErrorCode.NULL });
                expect(interpreter.createWarnResult("ref", InternalErrorCode.CIRCULAR)).toEqual({ type: "warn", code: "circular", value: 0 });
                expect(interpreter.createWarnResult("any", InternalErrorCode.UNSUPPORTED)).toEqual({ type: "warn", code: "unsupported", value: ErrorCode.NA });
            });
            it("should create a matrix result for matrix context", () => {
                expect(interpreter.createWarnResult("mat", ErrorCode.NULL).value).toStringifyTo("{#NULL}");
                expect(interpreter.createWarnResult("mat", InternalErrorCode.CIRCULAR).value).toStringifyTo("{0}");
                expect(interpreter.createWarnResult("mat", InternalErrorCode.UNSUPPORTED).value).toStringifyTo("{#NA}");
            });
        });

        describe("method createErrorResult", () => {
            it("should exist", () => {
                expect(FormulaInterpreter).toHaveMethod("createErrorResult");
            });
            it("should create a warning result", () => {
                expect(interpreter.createErrorResult("val", "missing")).toEqual({ type: "error", code: "missing", value: ErrorCode.NA });
            });
            it("should create a matrix result for matrix context", () => {
                expect(interpreter.createErrorResult("mat", "missing").value).toStringifyTo("{#NA}");
            });
        });

        describe("method interpretTokens", () => {
            it("should exist", () => {
                expect(FormulaInterpreter).toHaveMethod("interpretTokens");
            });
        });
    });
});
