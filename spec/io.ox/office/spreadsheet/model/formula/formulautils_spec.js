/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as dimension from "@/io.ox/office/spreadsheet/model/formula/utils/dimension";
import * as complex from "@/io.ox/office/spreadsheet/model/formula/utils/complex";
import * as matrix from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";
import * as formulaerror from "@/io.ox/office/spreadsheet/model/formula/utils/formulaerror";
import * as cellref from "@/io.ox/office/spreadsheet/model/formula/utils/cellref";
import * as sheetref from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";

import * as formulautils from "@/io.ox/office/spreadsheet/model/formula/formulautils";

// tests ======================================================================

describe("module spreadsheet/model/formula/formulautils", () => {

    // re-exports -------------------------------------------------------------

    it("re-exports should exist", () => {
        expect(formulautils).toContainProps(dimension);
        expect(formulautils).toContainProps(complex);
        expect(formulautils).toContainProps(matrix);
        expect(formulautils).toContainProps(formulaerror);
        expect(formulautils).toContainProps(cellref);
        expect(formulautils).toContainProps(sheetref);
    });
});
