/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { DObject } from "@/io.ox/office/tk/objects";
import { LOCALE_DATA, localeDataRegistry } from "@/io.ox/office/tk/locale";
import { Resource, ResourceMap } from "@/io.ox/office/spreadsheet/model/formula/resource/resourcemap";
import { FunctionResource } from "@/io.ox/office/spreadsheet/model/formula/resource/functionresource";
import { FormulaResource, FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";

// tests ======================================================================

describe("module spreadsheet/model/formula/resource/formularesource", () => {

    let deOdfFactory = null; // grammar factory for ODF, de_DE
    let enOoxFactory = null; // grammar factory for OOXML, en_US

    let deOoxResource = null; // formula resource for OOXML, de_DE
    let enOoxResource = null; // formula resource for OOXML, en_US
    let deOdfResource = null; // formula resource for ODF, de_DE
    let enOdfResource = null; // formula resource for ODF, en_US

    // class FormulaResourceFactory -------------------------------------------

    describe("class FormulaResourceFactory", () => {

        it("should subclass DObject", () => {
            expect(FormulaResourceFactory).toBeSubClassOf(DObject);
        });

        describe("static function create", () => {
            it("should exist", () => {
                expect(FormulaResourceFactory).toHaveStaticMethod("create");
            });
            it("should create a resource factory for ODF/de_DE", async () => {
                const factory = await FormulaResourceFactory.create("odf", LOCALE_DATA);
                expect(factory).toBeInstanceOf(FormulaResourceFactory);
                deOdfFactory = factory;
            });
            it("should create a grammar factory for OOXML/en_US", async () => {
                const factory = await FormulaResourceFactory.create("ooxml", localeDataRegistry.getLocaleData("en_US"));
                expect(factory).toBeInstanceOf(FormulaResourceFactory);
                expect(factory).not.toBe(deOdfFactory);
                enOoxFactory = factory;
            });
            it("should create a grammar factory for OOXML/de_DE", async () => {
                const factory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
                expect(factory).toBeInstanceOf(FormulaResourceFactory);
                expect(factory).not.toBe(deOdfFactory);
                expect(factory).not.toBe(enOoxFactory);
            });
            it("should return cached factory", async () => {
                const factory = await FormulaResourceFactory.create("odf", LOCALE_DATA);
                expect(factory).toBe(deOdfFactory);
            });
        });

        describe("method getResource", () => {
            it("should exist", () => {
                expect(FormulaResourceFactory).toHaveMethod("getResource");
            });
            it("should return the resources for all grammars for ODF/de_DE", () => {
                // grammar OP (ODF, de_DE)
                deOdfResource = deOdfFactory.getResource("std");
                expect(deOdfResource).toBeInstanceOf(FormulaResource);
                // grammar OOX (OOXML, de_DE)
                deOoxResource = deOdfFactory.getResource("oox");
                expect(deOoxResource).toBeInstanceOf(FormulaResource);
                expect(deOoxResource).not.toBe(deOdfResource);
                // grammars EN_* (ODF, en_US)
                enOdfResource = deOdfFactory.getResource("en");
                expect(enOdfResource).toBeInstanceOf(FormulaResource);
                expect(enOdfResource).not.toBe(deOdfResource);
                expect(enOdfResource).not.toBe(deOoxResource);
            });
            it("should return the resources for all grammars for OOXML/en_US", () => {
                // grammar OP (OOXML, en_US)
                enOoxResource = enOoxFactory.getResource("en");
                expect(enOoxResource).toBeInstanceOf(FormulaResource);
                expect(enOoxResource).not.toBe(deOdfResource);
                expect(enOoxResource).not.toBe(deOoxResource);
                expect(enOoxResource).not.toBe(enOdfResource);
                // all other grammars also (OOXML, en_US)
                expect(enOoxFactory.getResource("std")).toBe(enOoxResource);
                expect(enOoxFactory.getResource("oox")).toBe(enOoxResource);
            });
        });
    });

    // class FormulaResource --------------------------------------------------

    describe("class FormulaResource", () => {

        it("should subclass DObject", () => {
            expect(FormulaResource).toBeSubClassOf(DObject);
        });

        describe("property fileFormat", () => {
            it("should contain the file format", () => {
                expect(deOoxResource.fileFormat).toBe("ooxml");
                expect(enOoxResource.fileFormat).toBe("ooxml");
                expect(deOdfResource.fileFormat).toBe("odf");
                expect(enOdfResource.fileFormat).toBe("odf");
            });
        });

        describe("property locale", () => {
            it("should contain the locale identifier", () => {
                expect(deOoxResource.locale).toBe(LOCALE_DATA.lc);
                expect(enOoxResource.locale).toBe("en_US");
                expect(deOdfResource.locale).toBe(LOCALE_DATA.lc);
                expect(enOdfResource.locale).toBe("en_US");
            });
        });

        describe("property booleanMap", () => {
            it("should exist", () => {
                expect(deOoxResource.booleanMap).toBeInstanceOf(ResourceMap);
            });
            it("should return entries for boolean literals", () => {
                const map = deOoxResource.booleanMap;
                expect(map.get("f")).toBeInstanceOf(Resource);
                expect(map.get("t")).toBeInstanceOf(Resource);
                expect(map.get("F")).toBeUndefined();
                expect(map.get("true")).toBeUndefined();
                expect(map.get("1")).toBeUndefined();
            });
        });

        describe("property errorMap", () => {
            it("should exist", () => {
                expect(deOoxResource.errorMap).toBeInstanceOf(ResourceMap);
            });
            it("should return entries for error code literals", () => {
                const map = deOoxResource.errorMap;
                expect(map.get("NULL")).toBeInstanceOf(Resource);
                expect(map.get("DIV0")).toBeInstanceOf(Resource);
                expect(map.get("VALUE")).toBeInstanceOf(Resource);
                expect(map.get("REF")).toBeInstanceOf(Resource);
                expect(map.get("NAME")).toBeInstanceOf(Resource);
                expect(map.get("NUM")).toBeInstanceOf(Resource);
                expect(map.get("NA")).toBeInstanceOf(Resource);
                expect(map.get("DATA")).toBeInstanceOf(Resource);
                expect(map.get("null")).toBeUndefined();
                expect(map.get("#NULL!")).toBeUndefined();
                expect(map.get("add")).toBeUndefined();
                expect(map.get("ABS")).toBeUndefined();
            });
        });

        describe("property regionMap", () => {
            it("should exist", () => {
                expect(deOoxResource.regionMap).toBeInstanceOf(ResourceMap);
            });
            it("should return entries for table regions", () => {
                const map = deOoxResource.regionMap;
                expect(map.get("ALL")).toBeInstanceOf(Resource);
                expect(map.get("HEADERS")).toBeInstanceOf(Resource);
                expect(map.get("DATA")).toBeInstanceOf(Resource);
                expect(map.get("TOTALS")).toBeInstanceOf(Resource);
                expect(map.get("ROW")).toBeInstanceOf(Resource);
                expect(map.get("all")).toBeUndefined();
                expect(map.get("#ALL")).toBeUndefined();
                expect(map.get("add")).toBeUndefined();
                expect(map.get("ABS")).toBeUndefined();
            });
        });

        describe("property cellParamMap", () => {
            it("should exist", () => {
                expect(deOoxResource.cellParamMap).toBeInstanceOf(ResourceMap);
            });
            it("should return entries for CELL function parameters", () => {
                const map = deOoxResource.cellParamMap;
                expect(map.get("ADDRESS")).toBeInstanceOf(Resource);
                expect(map.get("COL")).toBeInstanceOf(Resource);
                expect(map.get("COLOR")).toBeInstanceOf(Resource);
                expect(map.get("CONTENTS")).toBeInstanceOf(Resource);
                expect(map.get("FILENAME")).toBeInstanceOf(Resource);
                expect(map.get("FORMAT")).toBeInstanceOf(Resource);
                expect(map.get("PARENTHESES")).toBeInstanceOf(Resource);
                expect(map.get("PREFIX")).toBeInstanceOf(Resource);
                expect(map.get("PROTECT")).toBeInstanceOf(Resource);
                expect(map.get("ROW")).toBeInstanceOf(Resource);
                expect(map.get("TYPE")).toBeInstanceOf(Resource);
                expect(map.get("WIDTH")).toBeInstanceOf(Resource);
                expect(map.get("address")).toBeUndefined();
                expect(map.get("add")).toBeUndefined();
                expect(map.get("ABS")).toBeUndefined();
            });
        });

        describe("property operatorMap", () => {
            it("should exist", () => {
                expect(deOoxResource.operatorMap).toBeInstanceOf(ResourceMap);
            });
            it("should return entries for operators", () => {
                const map = deOoxResource.operatorMap;
                expect(map.get("lt")).toBeInstanceOf(FunctionResource);
                expect(map.get("le")).toBeInstanceOf(FunctionResource);
                expect(map.get("gt")).toBeInstanceOf(FunctionResource);
                expect(map.get("ge")).toBeInstanceOf(FunctionResource);
                expect(map.get("eq")).toBeInstanceOf(FunctionResource);
                expect(map.get("ne")).toBeInstanceOf(FunctionResource);
                expect(map.get("uplus")).toBeInstanceOf(FunctionResource);
                expect(map.get("uminus")).toBeInstanceOf(FunctionResource);
                expect(map.get("upct")).toBeInstanceOf(FunctionResource);
                expect(map.get("add")).toBeInstanceOf(FunctionResource);
                expect(map.get("sub")).toBeInstanceOf(FunctionResource);
                expect(map.get("mul")).toBeInstanceOf(FunctionResource);
                expect(map.get("div")).toBeInstanceOf(FunctionResource);
                expect(map.get("pow")).toBeInstanceOf(FunctionResource);
                expect(map.get("con")).toBeInstanceOf(FunctionResource);
                expect(map.get("list")).toBeInstanceOf(FunctionResource);
                expect(map.get("isect")).toBeInstanceOf(FunctionResource);
                expect(map.get("range")).toBeInstanceOf(FunctionResource);
                expect(map.get("+")).toBeUndefined();
            });
            it("should provide differences between file formats", () => {
                const deOoxMap = deOoxResource.operatorMap;
                const enOoxMap = enOoxResource.operatorMap;
                const deOdfMap = deOdfResource.operatorMap;
                const enOdfMap = enOdfResource.operatorMap;
                expect(deOoxMap.getName("list", false)).toBe(",");
                expect(enOoxMap.getName("list", false)).toBe(",");
                expect(deOdfMap.getName("list", false)).toBe("~");
                expect(enOdfMap.getName("list", false)).toBe("~");
                expect(deOoxMap.getName("list", true)).toBe(";");
                expect(enOoxMap.getName("list", true)).toBe(",");
                expect(deOdfMap.getName("list", true)).toBe(";");
                expect(enOdfMap.getName("list", true)).toBe(",");
                expect(deOoxMap.getName("isect", false)).toBe(" ");
                expect(enOoxMap.getName("isect", false)).toBe(" ");
                expect(deOdfMap.getName("isect", false)).toBe("!");
                expect(enOdfMap.getName("isect", false)).toBe("!");
                expect(deOoxMap.getName("isect", true)).toBe(" ");
                expect(enOoxMap.getName("isect", true)).toBe(" ");
                expect(deOdfMap.getName("isect", true)).toBe(" ");
                expect(enOdfMap.getName("isect", true)).toBe(" ");
            });
        });

        describe("property functionMap", () => {
            it("should exist", () => {
                expect(deOoxResource.functionMap).toBeInstanceOf(ResourceMap);
            });
            it("should return entries for functions", () => {
                const map = deOoxResource.functionMap;
                expect(map.get("ABS")).toBeInstanceOf(FunctionResource);
                expect(map.get("SUM")).toBeInstanceOf(FunctionResource);
                expect(map.get("ERROR.TYPE")).toBeInstanceOf(FunctionResource);
                expect(map.get("FORMULATEXT")).toBeInstanceOf(FunctionResource);
                expect(map.get("sum")).toBeUndefined();
                expect(map.get("SUMME")).toBeUndefined();
                expect(map.getName("SUM", false)).toBe("SUM");
                expect(map.getKey("SUM", false)).toBe("SUM");
                expect(map.getKey("sum", false)).toBe("SUM");
                expect(map.getKey("SUMME", false)).toBeNull();
            });
            it("should return correct entries for SUM function", () => {
                const deOoxMap = deOoxResource.functionMap;
                const enOoxMap = enOoxResource.functionMap;
                const deOdfMap = deOdfResource.functionMap;
                const enOdfMap = enOdfResource.functionMap;
                expect(deOoxMap.getName("SUM", true)).toBe("SUMME");
                expect(enOoxMap.getName("SUM", true)).toBe("SUM");
                expect(deOdfMap.getName("SUM", true)).toBe("SUMME");
                expect(enOdfMap.getName("SUM", true)).toBe("SUM");
                expect(deOoxMap.getKey("SUMME", true)).toBe("SUM");
                expect(enOoxMap.getKey("SUMME", true)).toBeNull();
                expect(deOdfMap.getKey("SUMME", true)).toBe("SUM");
                expect(enOdfMap.getKey("SUMME", true)).toBeNull();
                expect(deOoxMap.getKey("summe", true)).toBe("SUM");
                expect(enOoxMap.getKey("summe", true)).toBeNull();
                expect(deOdfMap.getKey("summe", true)).toBe("SUM");
                expect(enOdfMap.getKey("summe", true)).toBeNull();
                expect(deOoxMap.getKey("SUM", true)).toBeNull();
                expect(enOoxMap.getKey("SUM", true)).toBe("SUM");
                expect(deOdfMap.getKey("SUM", true)).toBeNull();
                expect(enOdfMap.getKey("SUM", true)).toBe("SUM");
            });
            it("should provide differences between file formats", () => {
                const deOoxMap = deOoxResource.functionMap;
                const enOoxMap = enOoxResource.functionMap;
                const deOdfMap = deOdfResource.functionMap;
                const enOdfMap = enOdfResource.functionMap;
                expect(deOoxMap.get("ERRORTYPE.ODF")).toBeUndefined();
                expect(enOoxMap.get("ERRORTYPE.ODF")).toBeUndefined();
                expect(deOdfMap.get("ERRORTYPE.ODF")).toBeInstanceOf(FunctionResource);
                expect(enOdfMap.get("ERRORTYPE.ODF")).toBeInstanceOf(FunctionResource);
                expect(deOoxMap.get("PHONETIC")).toBeInstanceOf(FunctionResource);
                expect(enOoxMap.get("PHONETIC")).toBeInstanceOf(FunctionResource);
                expect(deOdfMap.get("PHONETIC")).toBeUndefined();
                expect(enOdfMap.get("PHONETIC")).toBeUndefined();
                expect(deOoxMap.getName("FORMULATEXT", false)).toBe("_xlfn.FORMULATEXT");
                expect(enOoxMap.getName("FORMULATEXT", false)).toBe("_xlfn.FORMULATEXT");
                expect(deOdfMap.getName("FORMULATEXT", false)).toBe("FORMULA");
                expect(enOdfMap.getName("FORMULATEXT", false)).toBe("FORMULA");
                expect(deOoxMap.getName("FORMULATEXT", true)).toBe("FORMELTEXT");
                expect(enOoxMap.getName("FORMULATEXT", true)).toBe("FORMULATEXT");
                expect(deOdfMap.getName("FORMULATEXT", true)).toBe("FORMEL");
                expect(enOdfMap.getName("FORMULATEXT", true)).toBe("FORMULA");
            });
        });

        describe("method getDec", () => {
            it("should exist", () => {
                expect(FormulaResource).toHaveMethod("getDec");
            });
            it("should return the correct decimal separator", () => {
                expect(deOoxResource.getDec(false)).toBe(".");
                expect(deOoxResource.getDec(true)).toBe(",");
                expect(enOoxResource.getDec(false)).toBe(".");
                expect(enOoxResource.getDec(true)).toBe(".");
                expect(deOdfResource.getDec(false)).toBe(".");
                expect(deOdfResource.getDec(true)).toBe(",");
                expect(enOdfResource.getDec(false)).toBe(".");
                expect(enOdfResource.getDec(true)).toBe(".");
            });
        });

        describe("method getGroup", () => {
            it("should exist", () => {
                expect(FormulaResource).toHaveMethod("getGroup");
            });
            it("should return the correct group separator", () => {
                expect(deOoxResource.getGroup(false)).toBe(",");
                expect(deOoxResource.getGroup(true)).toBe(".");
                expect(enOoxResource.getGroup(false)).toBe(",");
                expect(enOoxResource.getGroup(true)).toBe(",");
                expect(deOdfResource.getGroup(false)).toBe(",");
                expect(deOdfResource.getGroup(true)).toBe(".");
                expect(enOdfResource.getGroup(false)).toBe(",");
                expect(enOdfResource.getGroup(true)).toBe(",");
            });
        });

        describe("method getSeparator", () => {
            it("should exist", () => {
                expect(FormulaResource).toHaveMethod("getSeparator");
            });
            it("should return the correct list separator", () => {
                expect(deOoxResource.getSeparator(false)).toBe(",");
                expect(deOoxResource.getSeparator(true)).toBe(";");
                expect(enOoxResource.getSeparator(false)).toBe(",");
                expect(enOoxResource.getSeparator(true)).toBe(",");
                expect(deOdfResource.getSeparator(false)).toBe(";");
                expect(deOdfResource.getSeparator(true)).toBe(";");
                expect(enOdfResource.getSeparator(false)).toBe(";");
                expect(enOdfResource.getSeparator(true)).toBe(",");
            });
        });

        describe("method getRCPrefixChars", () => {
            it("should exist", () => {
                expect(FormulaResource).toHaveMethod("getRCPrefixChars");
            });
            it("should return the correct characters", () => {
                expect(deOoxResource.getRCPrefixChars(false)).toBe("RC");
                expect(deOoxResource.getRCPrefixChars(true)).toBe("ZS");
                expect(enOoxResource.getRCPrefixChars(false)).toBe("RC");
                expect(enOoxResource.getRCPrefixChars(true)).toBe("RC");
                expect(deOdfResource.getRCPrefixChars(false)).toBe("RC");
                expect(deOdfResource.getRCPrefixChars(true)).toBe("RC");
                expect(enOdfResource.getRCPrefixChars(false)).toBe("RC");
                expect(enOdfResource.getRCPrefixChars(true)).toBe("RC");
            });
        });

        describe("method getFunctionHelp", () => {
            let resources;
            beforeAll(() => {
                resources = [deOoxResource, enOoxResource, deOdfResource, enOdfResource];
            });
            it("should exist", () => {
                expect(FormulaResource).toHaveMethod("getFunctionHelp");
            });
            it("should return a function help descriptor", () => {
                resources.forEach(function (resource) {
                    expect(resource.getFunctionHelp("ABS")).toBeObject();
                    expect(resource.getFunctionHelp("SUM")).toBeObject();
                });
            });
            it("should return null for invalid keys", () => {
                resources.forEach(function (resource) {
                    expect(resource.getFunctionHelp("123")).toBeUndefined();
                    expect(resource.getFunctionHelp("abc")).toBeUndefined();
                    expect(resource.getFunctionHelp("sum")).toBeUndefined();
                    expect(resource.getFunctionHelp("SUMME")).toBeUndefined();
                });
            });
        });
    });
});
