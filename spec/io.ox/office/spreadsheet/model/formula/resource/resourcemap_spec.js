/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Resource, ResourceMap } from "@/io.ox/office/spreadsheet/model/formula/resource/resourcemap";

// private functions ==========================================================

function *generator() {
    const source = [["key1", "name1"], ["key2", "name3"], ["key3", "name2"], ["key4", "name4"]];
    for (const [key, name] of source) {
        if (key === "key4") { continue; }
        const resource = new Resource(key, "native." + name, "local." + name);
        if (key === "key3") {
            resource.altNativeNames.push("alt." + resource.nativeName);
            resource.altLocalNames.push("alt." + resource.localName);
            resource.property = "custom";
        }
        yield [key, resource];
    }
}

// tests ======================================================================

describe("module spreadsheet/model/formula/resource/resourcemap", () => {

    // class Resource ---------------------------------------------------------

    describe("class Resource", () => {

        it("should exist", () => {
            expect(Resource).toBeFunction();
        });

        it("should create element properties", () => {
            const resource = new Resource("key", "native", "local");
            expect(resource).toHaveProperty("key", "key");
            expect(resource).toHaveProperty("nativeName", "native");
            expect(resource).toHaveProperty("localName", "local");
            expect(resource.altNativeNames).toBeArrayOfSize(0);
            expect(resource.altLocalNames).toBeArrayOfSize(0);
        });
    });

    // class ResourceMap ------------------------------------------------------

    let resMap = null;

    describe("class ResourceMap", () => {

        it("should be a class", () => {
            expect(ResourceMap).toBeClass();
        });

        describe("constructor", () => {
            it("should create a new collection", () => {
                resMap = new ResourceMap(generator());
                expect(resMap).toBeInstanceOf(ResourceMap);
            });
        });

        describe("method get", () => {
            it("should exist", () => {
                expect(ResourceMap).toHaveMethod("get");
            });
            it("should return existing resources", () => {
                expect(resMap.get("key1")).toHaveProperty("key", "key1");
                expect(resMap.get("key2")).toHaveProperty("key", "key2");
                expect(resMap.get("key3")).toHaveProperty("key", "key3");
            });
            it("should return undefined for invalid keys", () => {
                expect(resMap.get("")).toBeUndefined();
                expect(resMap.get("__invalid__")).toBeUndefined();
                expect(resMap.get("KEY1")).toBeUndefined();
                expect(resMap.get("key4")).toBeUndefined();
            });
        });

        describe("method @@iterator", () => {
            it("should exist", () => {
                expect(ResourceMap).toHaveMethod(Symbol.iterator);
            });
            it("should visit all resources", () => {
                const iterator = resMap[Symbol.iterator]();
                expect(iterator).toBeIterator();
                expect(iterator.next()).toEqual({ done: false, value: ["key1", resMap.get("key1")] });
                expect(iterator.next()).toEqual({ done: false, value: ["key2", resMap.get("key2")] });
                expect(iterator.next()).toEqual({ done: false, value: ["key3", resMap.get("key3")] });
                expect(iterator.next()).toEqual({ done: true, value: undefined });
            });
        });

        describe("method getName", () => {
            it("should exist", () => {
                expect(ResourceMap).toHaveMethod("getName");
            });
            it("should return native names", () => {
                expect(resMap.getName("key1", false)).toBe("native.name1");
                expect(resMap.getName("key2", false)).toBe("native.name3");
                expect(resMap.getName("key3", false)).toBe("native.name2");
            });
            it("should return localized names", () => {
                expect(resMap.getName("key1", true)).toBe("local.name1");
                expect(resMap.getName("key2", true)).toBe("local.name3");
                expect(resMap.getName("key3", true)).toBe("local.name2");
            });
            it("should return null for invalid keys", () => {
                expect(resMap.getName("__invalid__", false)).toBeNull();
                expect(resMap.getName("__invalid__", true)).toBeNull();
                expect(resMap.getName("KEY1", false)).toBeNull();
                expect(resMap.getName("key4", true)).toBeNull();
                expect(resMap.getName("native.name1", false)).toBeNull();
                expect(resMap.getName("native.name1", true)).toBeNull();
                expect(resMap.getName("local.name1", false)).toBeNull();
                expect(resMap.getName("local.name1", true)).toBeNull();
            });
        });

        describe("method getKey", () => {
            it("should exist", () => {
                expect(ResourceMap).toHaveMethod("getKey");
            });
            it("should return keys for native names", () => {
                expect(resMap.getKey("native.name1", false)).toBe("key1");
                expect(resMap.getKey("NATIVE.NAME1", false)).toBe("key1");
                expect(resMap.getKey("native.name2", false)).toBe("key3");
                expect(resMap.getKey("native.name3", false)).toBe("key2");
                expect(resMap.getKey("alt.native.name2", false)).toBe("key3");
                expect(resMap.getKey("ALT.NATIVE.NAME2", false)).toBe("key3");
            });
            it("should return keys for localized names", () => {
                expect(resMap.getKey("local.name1", true)).toBe("key1");
                expect(resMap.getKey("LOCAL.NAME1", true)).toBe("key1");
                expect(resMap.getKey("local.name2", true)).toBe("key3");
                expect(resMap.getKey("local.name3", true)).toBe("key2");
                expect(resMap.getKey("alt.local.name2", true)).toBe("key3");
                expect(resMap.getKey("ALT.LOCAL.NAME2", true)).toBe("key3");
            });
            it("should return null for invalid keys", () => {
                expect(resMap.getKey("__invalid__", false)).toBeNull();
                expect(resMap.getKey("__invalid__", true)).toBeNull();
                expect(resMap.getKey("key1", false)).toBeNull();
                expect(resMap.getKey("key4", true)).toBeNull();
                expect(resMap.getKey("local.name1", false)).toBeNull();
                expect(resMap.getKey("native.name1", true)).toBeNull();
            });
        });

        describe("method getAllNames", () => {
            it("should exist", () => {
                expect(ResourceMap).toHaveMethod("getAllNames");
            });
            it("should return native names", () => {
                expect(resMap.getAllNames(false)).toEqual(["alt.native.name2", "native.name1", "native.name2", "native.name3"]);
            });
            it("should return localized names", () => {
                expect(resMap.getAllNames(true)).toEqual(["alt.local.name2", "local.name1", "local.name2", "local.name3"]);
            });
        });

        describe("method getAllPattern", () => {
            it("should exist", () => {
                expect(ResourceMap).toHaveMethod("getAllPattern");
            });
            it("should return pattern for native names", () => {
                expect(resMap.getAllPattern(false)).toBe("(alt\\.native\\.name2|native\\.name1|native\\.name2|native\\.name3)");
            });
            it("should return pattern for localized names", () => {
                expect(resMap.getAllPattern(true)).toBe("(alt\\.local\\.name2|local\\.name1|local\\.name2|local\\.name3)");
            });
        });
    });
});
