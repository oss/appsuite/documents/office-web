/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { DocRef } from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { BaseToken } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import { TableToken } from "@/io.ox/office/spreadsheet/model/formula/token/tabletoken";

import { a, r, MockDocumentAccess } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/token/tabletoken", () => {

    const docAccess = new MockDocumentAccess({
        cols: 16384,
        rows: 1048576,
        sheets: ["Sheet1", "Sheet2", "Sheet3", "Sheet 4"],
        names: [
            { label: "name", sheet: null },
            { label: "name", sheet: 1 }
        ],
        tables: [
            { name: "Table1", sheet: 0, range: r("B2:D5"), header: true, footer: true, columns: ["COL1", "COL2", "COL3"] }
        ]
    });

    let opGrammar, uiGrammar;
    beforeAll(async () => {
        const resFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        const resource = resFactory.getResource("std");
        opGrammar = FormulaGrammar.create(resource);
        uiGrammar = FormulaGrammar.create(resource, { localized: true });
    });

    // class TableToken -------------------------------------------------------

    describe("class TableToken", () => {
        it("should exist", () => {
            expect(TableToken).toBeSubClassOf(BaseToken);
        });

        const tableToken = new TableToken(docAccess, "Table1");
        const extDoc0Token = new TableToken(docAccess, "Table1", { docRef: new DocRef(0) });
        const extDoc1Token = new TableToken(docAccess, "Table1", { docRef: new DocRef(1) });

        describe("property type", () => {
            it("should exist", () => {
                expect(tableToken.type).toBe("table");
            });
        });

        describe("property tableName", () => {
            it("should exist", () => {
                expect(tableToken.tableName).toBe("Table1");
            });
        });

        describe("method getText", () => {
            it("should exist", () => {
                expect(TableToken).toHaveMethod("getText");
            });
            function testToken(tableName, tableConfig, textOptions, expOp, expUi) {
                const token = new TableToken(docAccess, tableName, tableConfig);
                expect(token.getText(opGrammar, textOptions)).toBe(expOp);
                expect(token.getText(uiGrammar, textOptions)).toBe(expUi);
            }
            it("should return the string representation", () => {
                testToken("Table1", null, null, "Table1[]", "Table1");
                testToken("Table1", { regionSpec: "ALL" }, null, "Table1[#All]", "Table1[#Alle]");
                testToken("Table1", { regionSpec: "HEADERS,DATA" }, null, "Table1[[#Headers],[#Data]]", "Table1[[#Kopfzeilen];[#Daten]]");
                testToken("Table1", { regionSpec: "DATA,TOTALS" }, null, "Table1[[#Data],[#Totals]]", "Table1[[#Daten];[#Ergebnisse]]");
                testToken("Table1", { col1Name: "Col 1" }, null, "Table1[Col 1]", "Table1[Col 1]");
                testToken("Table1", { col1Name: " Col " }, null, "Table1[[ Col ]]", "Table1[[ Col ]]");
                testToken("Table1", { col1Name: " " }, null, "Table1[[ ]]", "Table1[[ ]]");
                testToken("Table1", { col1Name: "Col1", col2Name: "Col2" }, null, "Table1[[Col1]:[Col2]]", "Table1[[Col1]:[Col2]]");
                testToken("Table1", { regionSpec: "ALL", col1Name: "Col1" }, null, "Table1[[#All],[Col1]]", "Table1[[#Alle];[Col1]]");
                testToken("Table1", { regionSpec: "HEADERS,DATA", col1Name: "Col1", col2Name: "Col2" }, null, "Table1[[#Headers],[#Data],[Col1]:[Col2]]", "Table1[[#Kopfzeilen];[#Daten];[Col1]:[Col2]]");
                testToken("Table1", { regionSpec: "ROW" }, null, "Table1[#This Row]", "Table1[@]");
                testToken("Table1", { regionSpec: "ROW", col1Name: "Col1" }, null, "Table1[[#This Row],[Col1]]", "Table1[@Col1]");
                testToken("Table1", { regionSpec: "ROW", col1Name: "Col 1" }, null, "Table1[[#This Row],[Col 1]]", "Table1[@[Col 1]]");
                testToken("Table1", { regionSpec: "ROW", col1Name: "Col:1" }, null, "Table1[[#This Row],[Col:1]]", "Table1[@[Col:1]]");
                testToken("Table1", { regionSpec: "ROW", col1Name: "Col1", col2Name: "Col2" }, null, "Table1[[#This Row],[Col1]:[Col2]]", "Table1[@[Col1]:[Col2]]");
            });
            it("should add arbitrary whitespace characters", () => {
                testToken("Table1", { openWs: true }, null, "Table1[ ]", "Table1");
                testToken("Table1", { regionSpec: "ALL", openWs: true, closeWs: true }, null, "Table1[ [#All] ]", "Table1[ [#Alle] ]");
                testToken("Table1", { regionSpec: "HEADERS,DATA", col1Name: "Col1", openWs: true }, null, "Table1[ [#Headers],[#Data],[Col1]]", "Table1[ [#Kopfzeilen];[#Daten];[Col1]]");
                testToken("Table1", { regionSpec: "HEADERS,DATA", col1Name: "Col1", closeWs: true }, null, "Table1[[#Headers],[#Data],[Col1] ]", "Table1[[#Kopfzeilen];[#Daten];[Col1] ]");
                testToken("Table1", { regionSpec: "HEADERS,DATA", col1Name: "Col1", sepWs: true }, null, "Table1[[#Headers], [#Data], [Col1]]", "Table1[[#Kopfzeilen]; [#Daten]; [Col1]]");
                testToken("Table1", { regionSpec: "HEADERS,DATA", col1Name: "Col1", openWs: true, closeWs: true, sepWs: true }, null, "Table1[ [#Headers], [#Data], [Col1] ]", "Table1[ [#Kopfzeilen]; [#Daten]; [Col1] ]");
                testToken("Table1", { col1Name: "Col 1", openWs: true, closeWs: true }, null, "Table1[ [Col 1] ]", "Table1[ [Col 1] ]");
                testToken("Table1", { col1Name: " Col ", openWs: true, closeWs: true }, null, "Table1[ [ Col ] ]", "Table1[ [ Col ] ]");
                testToken("Table1", { col1Name: " ", openWs: true, closeWs: true }, null, "Table1[ [ ] ]", "Table1[ [ ] ]");
                testToken("Table1", { col1Name: "Col1", col2Name: "Col2", openWs: true, closeWs: true }, null, "Table1[ [Col1]:[Col2] ]", "Table1[ [Col1]:[Col2] ]");
                testToken("Table1", { regionSpec: "ROW", openWs: true, closeWs: true }, null, "Table1[ [#This Row] ]", "Table1[ @ ]");
                testToken("Table1", { regionSpec: "ROW", col1Name: "Col1", openWs: true, closeWs: true, sepWs: true }, null, "Table1[ [#This Row], [Col1] ]", "Table1[ @Col1 ]");
            });
            it("should omit table name in specific situations", () => {
                testToken("Table1", { col1Name: "Col1" }, { refAddress: a("B3"), targetAddress: a("B3"), refSheet: 0 }, "Table1[Col1]", "Table1[Col1]");
                testToken("Table1", { col1Name: "Col1" }, { refAddress: a("B3"), targetAddress: a("B3"), refSheet: 0, unqualifiedTables: true }, "Table1[Col1]", "[Col1]");
                testToken("Table1", { col1Name: "Col1" }, { refAddress: a("A1"), targetAddress: a("A1"), refSheet: 0, unqualifiedTables: true }, "Table1[Col1]", "Table1[Col1]");
                testToken("Table1", { col1Name: "Col1" }, { refAddress: a("B3"), targetAddress: a("B3"), refSheet: 1, unqualifiedTables: true }, "Table1[Col1]", "Table1[Col1]");
                testToken("Table1", { regionSpec: "ROW", col1Name: "Col1" }, { refAddress: a("B3"), targetAddress: a("B3"), refSheet: 0, unqualifiedTables: true }, "Table1[[#This Row],[Col1]]", "[@Col1]");
            });
            it("should support document references", () => {
                testToken("Table1", { docRef: new DocRef(0) }, null, "[0]!Table1[]", "[0]!Table1");
                testToken("Table1", { docRef: new DocRef(1), regionSpec: "ALL" }, null, "[1]!Table1[#All]", "[1]!Table1[#Alle]");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(TableToken).toHaveMethod("toString");
            });
            it("should return the string representation", () => {
                expect(tableToken.toString()).toBe("table[Table1]");
            });
        });

        describe("method isExtDoc", () => {
            it("should exist", () => {
                expect(TableToken).toHaveMethod("isExtDoc");
            });
            it("should return correct result", () => {
                expect(tableToken.isExtDoc()).toBeFalse();
                expect(extDoc0Token.isExtDoc()).toBeFalse();
                expect(extDoc1Token.isExtDoc()).toBeTrue();
            });
        });

        describe("method resolveTable", () => {
            it("should exist", () => {
                expect(TableToken).toHaveMethod("resolveTable");
            });
            it("should return the table reference", () => {
                const tableRef = tableToken.resolveTable();
                expect(tableRef).toContainProps({ name: "Table1", sheet: 0, header: true, footer: true });
                expect(tableRef.range).toEqual(r("B2:D5"));
                expect(tableRef.columns).toEqual(["COL1", "COL2", "COL3"]);
            });
            it("should handle document references", () => {
                expect(extDoc0Token.resolveTable()).toHaveProperty("name", "Table1");
                expect(extDoc1Token.resolveTable()).toBeUndefined();
            });
            it("should return undefined for invalid table name", () => {
                expect(new TableToken(docAccess, "__invalid__").resolveTable()).toBeUndefined();
            });
        });

        describe("method getRange3D", () => {
            it("should exist", () => {
                expect(TableToken).toHaveMethod("getRange3D");
            });
            it("should return the range", () => {
                expect(tableToken.getRange3D(a("A1"))).toStringifyTo("0:0!B3:D4");
                expect(tableToken.getRange3D(a("B2"))).toStringifyTo("0:0!B3:D4");
            });
            it("should return the correct table region", () => {
                expect(new TableToken(docAccess, "Table1", { regionSpec: "ALL" }).getRange3D(a("B2"))).toStringifyTo("0:0!B2:D5");
                expect(new TableToken(docAccess, "Table1", { regionSpec: "HEADERS" }).getRange3D(a("B2"))).toStringifyTo("0:0!B2:D2");
                expect(new TableToken(docAccess, "Table1", { regionSpec: "DATA" }).getRange3D(a("B2"))).toStringifyTo("0:0!B3:D4");
                expect(new TableToken(docAccess, "Table1", { regionSpec: "TOTALS" }).getRange3D(a("B2"))).toStringifyTo("0:0!B5:D5");
                expect(new TableToken(docAccess, "Table1", { regionSpec: "HEADERS,DATA" }).getRange3D(a("B2"))).toStringifyTo("0:0!B2:D4");
                expect(new TableToken(docAccess, "Table1", { regionSpec: "DATA,TOTALS" }).getRange3D(a("B2"))).toStringifyTo("0:0!B3:D5");
                expect(new TableToken(docAccess, "Table1", { regionSpec: "ROW" }).getRange3D(a("B1"))).toBeNull();
                expect(new TableToken(docAccess, "Table1", { regionSpec: "ROW" }).getRange3D(a("B2"))).toBeNull();
                expect(new TableToken(docAccess, "Table1", { regionSpec: "ROW" }).getRange3D(a("B3"))).toStringifyTo("0:0!B3:D3");
                expect(new TableToken(docAccess, "Table1", { regionSpec: "ROW" }).getRange3D(a("B4"))).toStringifyTo("0:0!B4:D4");
                expect(new TableToken(docAccess, "Table1", { regionSpec: "ROW" }).getRange3D(a("B5"))).toBeNull();
                expect(new TableToken(docAccess, "Table1", { regionSpec: "ROW" }).getRange3D(a("B6"))).toBeNull();
            });
            it("should return the correct table columns", () => {
                expect(new TableToken(docAccess, "Table1", { col1Name: "Col1" }).getRange3D(a("B2"))).toStringifyTo("0:0!B3:B4");
                expect(new TableToken(docAccess, "Table1", { col1Name: "Col2", col2Name: "Col3" }).getRange3D(a("B2"))).toStringifyTo("0:0!C3:D4");
                expect(new TableToken(docAccess, "Table1", { col1Name: "Col3", col2Name: "Col1" }).getRange3D(a("B2"))).toStringifyTo("0:0!B3:D4");
            });
            it("should return the correct combined ranges", () => {
                expect(new TableToken(docAccess, "Table1", { regionSpec: "ALL", col1Name: "Col1" }).getRange3D(a("B2"))).toStringifyTo("0:0!B2:B5");
                expect(new TableToken(docAccess, "Table1", { regionSpec: "ROW", col1Name: "Col1", col2Name: "Col2" }).getRange3D(a("B2"))).toBeNull();
                expect(new TableToken(docAccess, "Table1", { regionSpec: "ROW", col1Name: "Col1", col2Name: "Col2" }).getRange3D(a("B4"))).toStringifyTo("0:0!B4:C4");
            });
            it("should handle document references", () => {
                expect(extDoc0Token.getRange3D(a("B2"))).toStringifyTo("0:0!B3:D4");
                expect(extDoc1Token.getRange3D(a("B2"))).toBeNull();
            });
            it("should return null for invalid table name", () => {
                expect(new TableToken(docAccess, "__invalid__").getRange3D(a("B2"))).toBeNull();
            });
        });
    });
});
