/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { BaseToken } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import { FixedToken } from "@/io.ox/office/spreadsheet/model/formula/token/fixedtoken";

// tests ======================================================================

describe("module spreadsheet/model/formula/token/fixedtoken", () => {

    let opGrammar, uiGrammar;
    beforeAll(async () => {
        const resFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        const resource = resFactory.getResource("std");
        opGrammar = FormulaGrammar.create(resource);
        uiGrammar = FormulaGrammar.create(resource, { localized: true });
    });

    // class FixedToken -------------------------------------------------------

    describe("class FixedToken", () => {
        it("should subclass BaseToken", () => {
            expect(FixedToken).toBeSubClassOf(BaseToken);
        });

        const token = new FixedToken("type", "text");

        describe("property type", () => {
            it("should exist", () => {
                expect(token.type).toBe("type");
            });
        });

        describe("property value", () => {
            it("should exist", () => {
                expect(token.value).toBe("text");
            });
        });

        describe("method isType", () => {
            it("should exist", () => {
                expect(FixedToken).toHaveMethod("isType");
            });
            it("should test the token type", () => {
                expect(token.isType("type")).toBeTrue();
                expect(token.isType("other")).toBeFalse();
                expect(token.isType("type", "other")).toBeTrue();
                expect(token.isType("other", "type")).toBeTrue();
                expect(token.isType()).toBeFalse();
            });
        });

        describe("method getText", () => {
            it("should exist", () => {
                expect(FixedToken).toHaveMethod("getText");
            });
            it("should return the string representation", () => {
                expect(token.getText(opGrammar)).toBe("text");
                expect(token.getText(uiGrammar)).toBe("text");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(FixedToken).toHaveMethod("toString");
            });
            it("should return the string representation", () => {
                expect(token.toString()).toBe("type[text]");
            });
        });

        describe("method setValue", () => {
            it("should exist", () => {
                expect(FixedToken).toHaveMethod("setValue");
            });
            it("should return whether the value changes", () => {
                const token1 = new FixedToken("type", "text");
                expect(token1.setValue("text")).toBeFalse();
                expect(token1.value).toBe("text");
                expect(token1.setValue("other")).toBeTrue();
                expect(token1.value).toBe("other");
                expect(token1.getText(opGrammar)).toBe("other");
                expect(token1.getText(uiGrammar)).toBe("other");
            });
        });

        describe("method appendValue", () => {
            it("should exist", () => {
                expect(FixedToken).toHaveMethod("appendValue");
            });
            it("should return whether the value changes", () => {
                const token1 = new FixedToken("type", "other");
                expect(token1.appendValue("")).toBeFalse();
                expect(token1.value).toBe("other");
                expect(token1.appendValue("text")).toBeTrue();
                expect(token1.value).toBe("othertext");
                expect(token1.getText(opGrammar)).toBe("othertext");
                expect(token1.getText(uiGrammar)).toBe("othertext");
            });
        });
    });
});
