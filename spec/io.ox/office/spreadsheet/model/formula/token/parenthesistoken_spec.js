/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { FixedToken } from "@/io.ox/office/spreadsheet/model/formula/token/fixedtoken";
import { ParenthesisToken } from "@/io.ox/office/spreadsheet/model/formula/token/parenthesistoken";

// tests ======================================================================

describe("module spreadsheet/model/formula/token/parenthesistoken", () => {

    let opGrammar, uiGrammar;
    beforeAll(async () => {
        const resFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        const resource = resFactory.getResource("std");
        opGrammar = FormulaGrammar.create(resource);
        uiGrammar = FormulaGrammar.create(resource, { localized: true });
    });

    // class ParenthesisToken -------------------------------------------------

    describe("class ParenthesisToken", () => {
        it("should subclass FixedToken", () => {
            expect(ParenthesisToken).toBeSubClassOf(FixedToken);
        });

        const openToken = new ParenthesisToken(true);
        const closeToken = new ParenthesisToken(false);

        describe("property type", () => {
            it("should exist", () => {
                expect(openToken.type).toBe("open");
                expect(closeToken.type).toBe("close");
            });
        });

        describe("method getText", () => {
            it("should exist", () => {
                expect(ParenthesisToken).toHaveMethod("getText");
            });
            it("should return the string representation", () => {
                expect(openToken.getText(opGrammar)).toBe("(");
                expect(openToken.getText(uiGrammar)).toBe("(");
                expect(closeToken.getText(opGrammar)).toBe(")");
                expect(closeToken.getText(uiGrammar)).toBe(")");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(ParenthesisToken).toHaveMethod("toString");
            });
            it("should return the string representation", () => {
                expect(openToken.toString()).toBe("open");
                expect(closeToken.toString()).toBe("close");
            });
        });
    });
});
