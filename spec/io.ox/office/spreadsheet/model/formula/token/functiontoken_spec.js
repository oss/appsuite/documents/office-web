/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { BaseToken } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import { FunctionToken } from "@/io.ox/office/spreadsheet/model/formula/token/functiontoken";

// tests ======================================================================

describe("module spreadsheet/model/formula/token/functiontoken", () => {

    let opGrammar, uiGrammar;
    beforeAll(async () => {
        const resFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        const resource = resFactory.getResource("std");
        opGrammar = FormulaGrammar.create(resource);
        uiGrammar = FormulaGrammar.create(resource, { localized: true });
    });

    // class FunctionToken ----------------------------------------------------

    describe("class FunctionToken", () => {
        it("should subclass BaseToken", () => {
            expect(FunctionToken).toBeSubClassOf(BaseToken);
        });

        const token = new FunctionToken("SUM");

        describe("property type", () => {
            it("should exist", () => {
                expect(token.type).toBe("func");
            });
        });

        describe("property value", () => {
            it("should exist", () => {
                expect(token.value).toBe("SUM");
            });
        });

        describe("method getText", () => {
            it("should exist", () => {
                expect(FunctionToken).toHaveMethod("getText");
            });
            it("should return the string representation", () => {
                expect(token.getText(opGrammar)).toBe("SUM");
                expect(token.getText(uiGrammar)).toBe("SUMME");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(FunctionToken).toHaveMethod("toString");
            });
            it("should return the string representation", () => {
                expect(token.toString()).toBe("func[SUM]");
            });
        });
    });
});
