/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { NameRefToken } from "@/io.ox/office/spreadsheet/model/formula/token/namereftoken";
import { DefinedNameToken } from "@/io.ox/office/spreadsheet/model/formula/token/definednametoken";

import { r, srefs, MockDocumentAccess } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/token/definednametoken", () => {

    const docAccess = new MockDocumentAccess({
        cols: 16384,
        rows: 1048576,
        sheets: ["Sheet1", "Sheet2", "Sheet3", "Sheet 4"],
        names: [
            { label: "name", sheet: null },
            { label: "name", sheet: 1 }
        ],
        tables: [
            { name: "Table1", sheet: 0, range: r("B2:D5"), header: true, footer: true, columns: ["COL1", "COL2", "COL3"] }
        ]
    });

    let opGrammar, uiGrammar;
    beforeAll(async () => {
        const resFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        const resource = resFactory.getResource("std");
        opGrammar = FormulaGrammar.create(resource);
        uiGrammar = FormulaGrammar.create(resource, { localized: true });
    });

    // class DefinedNameToken -------------------------------------------------

    describe("class DefinedNameToken", () => {
        it("should subclass NameRefToken", () => {
            expect(DefinedNameToken).toBeSubClassOf(NameRefToken);
        });

        const nameToken = new DefinedNameToken(docAccess, "name");
        const sheet2NameToken = new DefinedNameToken(docAccess, "Name", srefs("$1"));
        const extDoc0NameToken = new DefinedNameToken(docAccess, "name", srefs(null, 0));
        const extDoc1NameToken = new DefinedNameToken(docAccess, "name", srefs(null, 1));
        const errorNameToken = new DefinedNameToken(docAccess, "NAME", srefs("$-1"));
        const tableToken = new DefinedNameToken(docAccess, "table1");
        const sheet1TableToken = new DefinedNameToken(docAccess, "Table1", srefs("$0"));
        const sheet2TableToken = new DefinedNameToken(docAccess, "TABLE1", srefs("$1"));
        const errorTableToken = new DefinedNameToken(docAccess, "Table1", srefs("$-1"));

        describe("property type", () => {
            it("should exist", () => {
                expect(nameToken.type).toBe("name");
            });
        });

        describe("property value", () => {
            it("should exist", () => {
                expect(nameToken.value).toBe("name");
                expect(sheet2NameToken.value).toBe("Name");
                expect(errorNameToken.value).toBe("NAME");
                expect(tableToken.value).toBe("table1");
                expect(sheet1TableToken.value).toBe("Table1");
                expect(sheet2TableToken.value).toBe("TABLE1");
                expect(errorTableToken.value).toBe("Table1");
            });
        });

        describe("method getText", () => {
            it("should exist", () => {
                expect(DefinedNameToken).toHaveMethod("getText");
            });
            it("should return the string representation", () => {
                expect(nameToken.getText(opGrammar)).toBe("name");
                expect(nameToken.getText(uiGrammar)).toBe("name");
                expect(sheet2NameToken.getText(opGrammar)).toBe("Sheet2!Name");
                expect(sheet2NameToken.getText(uiGrammar)).toBe("Sheet2!Name");
                expect(extDoc0NameToken.getText(opGrammar)).toBe("[0]!name");
                expect(extDoc0NameToken.getText(uiGrammar)).toBe("[0]!name");
                expect(extDoc1NameToken.getText(opGrammar)).toBe("[1]!name");
                expect(extDoc1NameToken.getText(uiGrammar)).toBe("[1]!name");
                expect(errorNameToken.getText(opGrammar)).toBe("#REF!");
                expect(errorNameToken.getText(uiGrammar)).toBe("#BEZUG!");
                expect(tableToken.getText(opGrammar)).toBe("table1");
                expect(tableToken.getText(uiGrammar)).toBe("table1");
                expect(sheet1TableToken.getText(opGrammar)).toBe("Sheet1!Table1");
                expect(sheet1TableToken.getText(uiGrammar)).toBe("Sheet1!Table1");
                expect(sheet2TableToken.getText(opGrammar)).toBe("Sheet2!TABLE1");
                expect(sheet2TableToken.getText(uiGrammar)).toBe("Sheet2!TABLE1");
                expect(errorTableToken.getText(opGrammar)).toBe("#REF!");
                expect(errorTableToken.getText(uiGrammar)).toBe("#BEZUG!");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(DefinedNameToken).toHaveMethod("toString");
            });
            it("should return the string representation", () => {
                expect(nameToken.toString()).toBe("name[name]");
                expect(sheet2NameToken.toString()).toBe("name[$1!Name]");
                expect(nameToken.toString()).toBe("name[name]");
                expect(extDoc0NameToken.toString()).toBe("name[[0]!name]");
                expect(extDoc1NameToken.toString()).toBe("name[[1]!name]");
                expect(tableToken.toString()).toBe("name[table1]");
                expect(sheet1TableToken.toString()).toBe("name[$0!Table1]");
                expect(sheet2TableToken.toString()).toBe("name[$1!TABLE1]");
                expect(errorTableToken.toString()).toBe("name[#REF!Table1]");
            });
        });

        describe("method isExtDoc", () => {
            it("should exist", () => {
                expect(DefinedNameToken).toHaveMethod("isExtDoc");
            });
            it("should return the correct result", () => {
                expect(nameToken.isExtDoc()).toBeFalse();
                expect(sheet2NameToken.isExtDoc()).toBeFalse();
                expect(extDoc0NameToken.isExtDoc()).toBeFalse();
                expect(extDoc1NameToken.isExtDoc()).toBeTrue();
                expect(errorNameToken.isExtDoc()).toBeFalse();
                expect(tableToken.isExtDoc()).toBeFalse();
                expect(sheet1TableToken.isExtDoc()).toBeFalse();
                expect(sheet2TableToken.isExtDoc()).toBeFalse();
                expect(errorTableToken.isExtDoc()).toBeFalse();
            });
        });

        describe("method hasSheetRef", () => {
            it("should exist", () => {
                expect(DefinedNameToken).toHaveMethod("hasSheetRef");
            });
            it("should return the correct result", () => {
                expect(nameToken.hasSheetRef()).toBeFalse();
                expect(sheet2NameToken.hasSheetRef()).toBeTrue();
                expect(extDoc0NameToken.hasSheetRef()).toBeFalse();
                expect(extDoc1NameToken.hasSheetRef()).toBeFalse();
                expect(errorNameToken.hasSheetRef()).toBeTrue();
                expect(tableToken.hasSheetRef()).toBeFalse();
                expect(sheet1TableToken.hasSheetRef()).toBeTrue();
                expect(sheet2TableToken.hasSheetRef()).toBeTrue();
                expect(errorTableToken.hasSheetRef()).toBeTrue();
            });
        });

        describe("method hasSheetError", () => {
            it("should exist", () => {
                expect(DefinedNameToken).toHaveMethod("hasSheetError");
            });
            it("should return the correct result", () => {
                expect(nameToken.hasSheetError()).toBeFalse();
                expect(sheet2NameToken.hasSheetError()).toBeFalse();
                expect(extDoc0NameToken.hasSheetError()).toBeFalse();
                expect(extDoc1NameToken.hasSheetError()).toBeFalse();
                expect(errorNameToken.hasSheetError()).toBeTrue();
                expect(tableToken.hasSheetError()).toBeFalse();
                expect(sheet1TableToken.hasSheetError()).toBeFalse();
                expect(sheet2TableToken.hasSheetError()).toBeFalse();
                expect(errorTableToken.hasSheetError()).toBeTrue();
            });
        });

        describe("method resolveName", () => {
            it("should exist", () => {
                expect(DefinedNameToken).toHaveMethod("resolveName");
            });
            it("should return globally defined name", () => {
                expect(nameToken.resolveName(null)).toContainProps({ sheet: null, label: "name" });
                expect(nameToken.resolveName(0)).toContainProps({ sheet: null, label: "name" });
                expect(extDoc0NameToken.resolveName(null)).toContainProps({ sheet: null, label: "name" });
                expect(extDoc0NameToken.resolveName(0)).toContainProps({ sheet: null, label: "name" });

            });
            it("should return sheet-local defined name", () => {
                expect(nameToken.resolveName(1)).toContainProps({ sheet: 1, label: "name" });
                expect(sheet2NameToken.resolveName(null)).toContainProps({ sheet: 1, label: "name" });
                expect(sheet2NameToken.resolveName(0)).toContainProps({ sheet: 1, label: "name" });
                expect(sheet2NameToken.resolveName(1)).toContainProps({ sheet: 1, label: "name" });
            });
            it("should return undefined for unknown global name", () => {
                const invalidToken = new DefinedNameToken(docAccess, "invalid");
                expect(invalidToken.resolveName(null)).toBeUndefined();
                expect(invalidToken.resolveName(0)).toBeUndefined();
                expect(invalidToken.resolveName(1)).toBeUndefined();
            });
            it("should return undefined for unknown sheet-local name", () => {
                const extNameToken = new DefinedNameToken(docAccess, "name", srefs("$0"));
                expect(extNameToken.resolveName(null)).toBeUndefined();
                expect(extNameToken.resolveName(0)).toBeUndefined();
                expect(extNameToken.resolveName(1)).toBeUndefined();
            });
            it("should return undefined for token with sheet error", () => {
                expect(errorNameToken.resolveName(null)).toBeUndefined();
                expect(errorNameToken.resolveName(0)).toBeUndefined();
            });
            it("should return undefined for token with external name", () => {
                expect(extDoc1NameToken.resolveName(null)).toBeUndefined();
                expect(extDoc1NameToken.resolveName(0)).toBeUndefined();
            });
            it("should return undefined for table tokens", () => {
                expect(tableToken.resolveName(null)).toBeUndefined();
                expect(sheet1TableToken.resolveName(null)).toBeUndefined();
                expect(sheet2TableToken.resolveName(null)).toBeUndefined();
                expect(errorTableToken.resolveName(null)).toBeUndefined();
            });
        });

        describe("method resolveTable", () => {
            it("should exist", () => {
                expect(DefinedNameToken).toHaveMethod("resolveTable");
            });
            it("should return table name for valid table tokens", () => {
                const tableRef1 = tableToken.resolveTable();
                expect(tableRef1).toContainProps({ name: "Table1", sheet: 0, header: true, footer: true });
                expect(tableRef1.range).toEqual(r("B2:D5"));
                expect(tableRef1.columns).toEqual(["COL1", "COL2", "COL3"]);
                const tableRef2 = sheet1TableToken.resolveTable();
                expect(tableRef2).toContainProps({ name: "Table1", sheet: 0, header: true, footer: true });
                expect(tableRef2.range).toEqual(r("B2:D5"));
                expect(tableRef2.columns).toEqual(["COL1", "COL2", "COL3"]);
            });
            it("should return undefined for invalid table tokens", () => {
                expect(sheet2TableToken.resolveTable()).toBeUndefined();
                expect(errorTableToken.resolveTable()).toBeUndefined();
            });
            it("should return undefined for name tokens", () => {
                expect(nameToken.resolveTable()).toBeUndefined();
                expect(sheet2NameToken.resolveTable()).toBeUndefined();
                expect(errorNameToken.resolveTable()).toBeUndefined();
            });
        });

        describe("method transformSheets", () => {
            it("should exist", () => {
                expect(DefinedNameToken).toHaveMethod("transformSheets");
            });
            it("should do nothing for tokens without sheet", () => {
                expect(nameToken.transformSheets([null, 0])).toBeFalse();
                expect(nameToken).toStringifyTo("name[name]");
            });
            it("should transform sheet index", () => {
                const token = new DefinedNameToken(docAccess, "name", srefs("$1"));
                expect(token).toStringifyTo("name[$1!name]");
                expect(token.transformSheets([1, 2, 3, 4, 5])).toBeTrue();
                expect(token).toStringifyTo("name[$2!name]");
                expect(token.transformSheets([0, 1, null, 2])).toBeTrue();
                expect(token).toStringifyTo("name[#REF!name]");
            });
            it("should do nothing for token with sheet error", () => {
                expect(errorNameToken.transformSheets([1, 2, 3, 4])).toBeFalse();
                expect(errorNameToken).toStringifyTo("name[#REF!NAME]");
            });
        });
    });
});
