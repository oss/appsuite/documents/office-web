/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { Direction } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import { AddressTransformer } from "@/io.ox/office/spreadsheet/model/addresstransformer";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { SheetRefToken } from "@/io.ox/office/spreadsheet/model/formula/token/sheetreftoken";
import { ReferenceToken } from "@/io.ox/office/spreadsheet/model/formula/token/referencetoken";

import { i, a, r, cref, crefs, srefs, MockDocumentAccess } from "~/spreadsheet/apphelper";

const { LEFT, RIGHT, UP, DOWN } = Direction;

// tests ======================================================================

describe("module spreadsheet/model/formula/token/referencetoken", () => {

    const A1 = a("A1");

    const docAccess = new MockDocumentAccess({
        cols: 16384,
        rows: 1048576,
        sheets: ["Sheet1", "Sheet2", "Sheet3", "Sheet 4"]
    });

    let opGrammar, uiGrammar;
    beforeAll(async () => {
        const resFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        const resource = resFactory.getResource("std");
        opGrammar = FormulaGrammar.create(resource);
        uiGrammar = FormulaGrammar.create(resource, { localized: true });
    });

    // class ReferenceToken ---------------------------------------------------

    describe("class ReferenceToken", () => {
        it("should subclass SheetRefToken", () => {
            expect(ReferenceToken).toBeSubClassOf(SheetRefToken);
        });

        const cellToken = new ReferenceToken(docAccess, cref("B$3"));
        const rangeToken = new ReferenceToken(docAccess, crefs("B$3:$D5"));
        const errToken = new ReferenceToken(docAccess);
        const sheet1Token = new ReferenceToken(docAccess, cref("B$3"), srefs("$1"));
        const sheet2Token = new ReferenceToken(docAccess, cref("B$3"), srefs("$1:$3"));
        const sheetErrToken = new ReferenceToken(docAccess, cref("B$3"), srefs("$-1"));

        describe("property type", () => {
            it("should exist", () => {
                expect(cellToken.type).toBe("ref");
            });
        });

        describe("method getText", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("getText");
            });
            it("should return the string representation", () => {
                expect(cellToken.getText(opGrammar)).toBe("B$3");
                expect(cellToken.getText(uiGrammar)).toBe("B$3");
                expect(rangeToken.getText(opGrammar)).toBe("B$3:$D5");
                expect(rangeToken.getText(uiGrammar)).toBe("B$3:$D5");
                expect(errToken.getText(opGrammar)).toBe("#REF!");
                expect(errToken.getText(uiGrammar)).toBe("#BEZUG!");
                expect(sheet1Token.getText(opGrammar)).toBe("Sheet2!B$3");
                expect(sheet1Token.getText(uiGrammar)).toBe("Sheet2!B$3");
                expect(sheet2Token.getText(opGrammar)).toBe("'Sheet2:Sheet 4'!B$3");
                expect(sheet2Token.getText(uiGrammar)).toBe("'Sheet2:Sheet 4'!B$3");
                expect(sheetErrToken.getText(opGrammar)).toBe("#REF!");
                expect(sheetErrToken.getText(uiGrammar)).toBe("#BEZUG!");
            });
            it("should handle simple/complex sheet names correctly", () => {
                const token1 = new ReferenceToken(docAccess, cref("A1"), srefs("$Sheet1"));
                expect(token1.getText(opGrammar)).toBe("Sheet1!A1");
                expect(token1.getText(uiGrammar)).toBe("Sheet1!A1");
                const token2 = new ReferenceToken(docAccess, cref("A1"), srefs("$Sheet 2"));
                expect(token2.getText(opGrammar)).toBe("'Sheet 2'!A1");
                expect(token2.getText(uiGrammar)).toBe("'Sheet 2'!A1");
                const token3 = new ReferenceToken(docAccess, cref("A1"), srefs("$1Sheet"));
                expect(token3.getText(opGrammar)).toBe("'1Sheet'!A1");
                expect(token3.getText(uiGrammar)).toBe("'1Sheet'!A1");
                const token4 = new ReferenceToken(docAccess, cref("A1"), srefs("$Sheet+1"));
                expect(token4.getText(opGrammar)).toBe("'Sheet+1'!A1");
                expect(token4.getText(uiGrammar)).toBe("'Sheet+1'!A1");
                const token5 = new ReferenceToken(docAccess, cref("A1"), srefs("$Sheet1:$Sheet2"));
                expect(token5.getText(opGrammar)).toBe("Sheet1:Sheet2!A1");
                expect(token5.getText(uiGrammar)).toBe("Sheet1:Sheet2!A1");
                const token6 = new ReferenceToken(docAccess, cref("A1"), srefs("$Sheet1:$Sheet 2"));
                expect(token6.getText(opGrammar)).toBe("'Sheet1:Sheet 2'!A1");
                expect(token6.getText(uiGrammar)).toBe("'Sheet1:Sheet 2'!A1");
                const token7 = new ReferenceToken(docAccess, cref("A1"), srefs("$Sheet 1:$Sheet2"));
                expect(token7.getText(opGrammar)).toBe("'Sheet 1:Sheet2'!A1");
                expect(token7.getText(uiGrammar)).toBe("'Sheet 1:Sheet2'!A1");
            });
            it("should treat booleans as complex sheet names", () => {
                const token1 = new ReferenceToken(docAccess, cref("A1"), srefs("$TRUE"));
                expect(token1.getText(opGrammar)).toBe("'TRUE'!A1");
                expect(token1.getText(uiGrammar)).toBe("TRUE!A1");
                const token2 = new ReferenceToken(docAccess, cref("A1"), srefs("$WAHR"));
                expect(token2.getText(opGrammar)).toBe("WAHR!A1");
                expect(token2.getText(uiGrammar)).toBe("'WAHR'!A1");
            });
            it("should treat cell addresses as complex sheet names", () => {
                const token1 = new ReferenceToken(docAccess, cref("A1"), srefs("$A1"));
                expect(token1.getText(opGrammar)).toBe("'A1'!A1");
                expect(token1.getText(uiGrammar)).toBe("'A1'!A1");
                const token2 = new ReferenceToken(docAccess, cref("A1"), srefs("$ZZZZ1"));
                expect(token2.getText(opGrammar)).toBe("ZZZZ1!A1");
                expect(token2.getText(uiGrammar)).toBe("ZZZZ1!A1");
                const token3 = new ReferenceToken(docAccess, cref("A1"), srefs("$R1C1"));
                expect(token3.getText(opGrammar)).toBe("'R1C1'!A1");
                expect(token3.getText(uiGrammar)).toBe("'R1C1'!A1");
                const token4 = new ReferenceToken(docAccess, cref("A1"), srefs("$Z1S1"));
                expect(token4.getText(opGrammar)).toBe("Z1S1!A1");
                expect(token4.getText(uiGrammar)).toBe("'Z1S1'!A1");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("toString");
            });
            it("should return the string representation", () => {
                expect(cellToken.toString()).toBe("ref[B$3]");
                expect(rangeToken.toString()).toBe("ref[B$3:$D5]");
                expect(errToken.toString()).toBe("ref[#REF]");
                expect(sheet1Token.toString()).toBe("ref[$1!B$3]");
                expect(sheet2Token.toString()).toBe("ref[$1:$3!B$3]");
                expect(sheetErrToken.toString()).toBe("ref[#REF!B$3]");
            });
        });

        describe("method hasSheetRef", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("hasSheetRef");
            });
            it("should return the correct result", () => {
                expect(cellToken.hasSheetRef()).toBeFalse();
                expect(rangeToken.hasSheetRef()).toBeFalse();
                expect(errToken.hasSheetRef()).toBeFalse();
                expect(sheet1Token.hasSheetRef()).toBeTrue();
                expect(sheet2Token.hasSheetRef()).toBeTrue();
                expect(sheetErrToken.hasSheetRef()).toBeTrue();
            });
        });

        describe("method hasSheetError", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("hasSheetError");
            });
            it("should return the correct result", () => {
                expect(cellToken.hasSheetError()).toBeFalse();
                expect(rangeToken.hasSheetError()).toBeFalse();
                expect(errToken.hasSheetError()).toBeFalse();
                expect(sheet1Token.hasSheetError()).toBeFalse();
                expect(sheet2Token.hasSheetError()).toBeFalse();
                expect(sheetErrToken.hasSheetError()).toBeTrue();
            });
        });

        describe("method getRange3D", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("getRange3D");
            });
            it("should return the correct range address", () => {
                expect(cellToken.getRange3D(A1, A1, 0)).toStringifyTo("0:0!B3:B3");
                expect(cellToken.getRange3D(A1, A1, 1)).toStringifyTo("1:1!B3:B3");
                expect(rangeToken.getRange3D(A1, A1, 0)).toStringifyTo("0:0!B3:D5");
                expect(rangeToken.getRange3D(A1, A1, 1)).toStringifyTo("1:1!B3:D5");
                expect(errToken.getRange3D(A1, A1, 0)).toBeNull();
                expect(errToken.getRange3D(A1, A1, 1)).toBeNull();
                expect(sheet1Token.getRange3D(A1, A1, 0)).toStringifyTo("1:1!B3:B3");
                expect(sheet1Token.getRange3D(A1, A1, 1)).toStringifyTo("1:1!B3:B3");
                expect(sheet2Token.getRange3D(A1, A1)).toStringifyTo("1:3!B3:B3");
                expect(sheet2Token.getRange3D(A1, A1, 0)).toStringifyTo("1:3!B3:B3");
                expect(sheet2Token.getRange3D(A1, A1, 1)).toStringifyTo("1:3!B3:B3");
                expect(sheetErrToken.getRange3D(A1, A1, 0)).toBeNull();
                expect(sheetErrToken.getRange3D(A1, A1, 1)).toBeNull();
            });
            it("should return the correct range address without reference sheet", () => {
                expect(cellToken.getRange3D(A1, A1)).toBeNull();
                expect(rangeToken.getRange3D(A1, A1)).toBeNull();
                expect(errToken.getRange3D(A1, A1)).toBeNull();
                expect(sheet1Token.getRange3D(A1, A1)).toStringifyTo("1:1!B3:B3");
                expect(sheetErrToken.getRange3D(A1, A1)).toBeNull();
            });
            it("should relocate cell range without wrapping", () => {
                expect(cellToken.getRange3D(A1, a("E6"), 0)).toStringifyTo("0:0!F3:F3");
                expect(cellToken.getRange3D(a("B2"), A1, 0)).toStringifyTo("0:0!A3:A3");
                expect(cellToken.getRange3D(a("C2"), A1, 0)).toBeNull();
                expect(cellToken.getRange3D(a("B2"), a("F9"), 0)).toStringifyTo("0:0!F3:F3");
                expect(cellToken.getRange3D(a("F9"), a("B2"), 0)).toBeNull();
                expect(rangeToken.getRange3D(A1, a("E6"), 0)).toStringifyTo("0:0!D3:F10");
                expect(rangeToken.getRange3D(a("B5"), A1, 0)).toStringifyTo("0:0!A1:D3");
                expect(rangeToken.getRange3D(a("C5"), A1, 0)).toBeNull();
                expect(rangeToken.getRange3D(a("B6"), A1, 0)).toBeNull();
                expect(rangeToken.getRange3D(a("B2"), a("F9"), 0)).toStringifyTo("0:0!D3:F12");
                expect(rangeToken.getRange3D(a("F9"), a("B2"), 0)).toBeNull();
                expect(errToken.getRange3D(A1, a("E6"), 0)).toBeNull();
            });
            it("should relocate cell range with wrapping", () => {
                expect(cellToken.getRange3D(A1, a("E6"), 0, true)).toStringifyTo("0:0!F3:F3");
                expect(cellToken.getRange3D(A1, a("XFD1"), 0, true)).toStringifyTo("0:0!A3:A3");
                expect(cellToken.getRange3D(a("B2"), A1, 0, true)).toStringifyTo("0:0!A3:A3");
                expect(cellToken.getRange3D(a("C2"), A1, 0, true)).toStringifyTo("0:0!XFD3:XFD3");
                expect(cellToken.getRange3D(a("B2"), a("F9"), 0, true)).toStringifyTo("0:0!F3:F3");
                expect(cellToken.getRange3D(a("F9"), a("B2"), 0, true)).toStringifyTo("0:0!XFB3:XFB3");
                expect(rangeToken.getRange3D(A1, a("E6"), 0, true)).toStringifyTo("0:0!D3:F10");
                expect(rangeToken.getRange3D(a("B5"), A1, 0, true)).toStringifyTo("0:0!A1:D3");
                expect(rangeToken.getRange3D(a("C5"), A1, 0, true)).toStringifyTo("0:0!D1:XFD3");
                expect(rangeToken.getRange3D(a("B6"), A1, 0, true)).toStringifyTo("0:0!A3:D1048576");
                expect(rangeToken.getRange3D(a("B2"), a("F9"), 0, true)).toStringifyTo("0:0!D3:F12");
                expect(rangeToken.getRange3D(a("F9"), a("B2"), 0, true)).toStringifyTo("0:0!D3:XFB1048574");
                expect(errToken.getRange3D(A1, a("E6"), 0, true)).toBeNull();
            });
        });

        describe("method hasRelCol", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("hasRelCol");
            });
            it("should return true for references with relative column", () => {
                expect(cellToken.hasRelCol()).toBeTrue();
                expect(rangeToken.hasRelCol()).toBeTrue();
                expect(errToken.hasRelCol()).toBeFalse();
                const relToken = new ReferenceToken(docAccess, crefs("B3:D5"));
                expect(relToken.hasRelCol()).toBeTrue();
                const absToken = new ReferenceToken(docAccess, crefs("$B$3:$D$5"));
                expect(absToken.hasRelCol()).toBeFalse();
            });
        });

        describe("method hasRelRow", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("hasRelRow");
            });
            it("should return true for references with relative row", () => {
                expect(cellToken.hasRelRow()).toBeFalse();
                expect(rangeToken.hasRelRow()).toBeTrue();
                expect(errToken.hasRelRow()).toBeFalse();
                const relToken = new ReferenceToken(docAccess, crefs("B3:D5"));
                expect(relToken.hasRelRow()).toBeTrue();
                const absToken = new ReferenceToken(docAccess, crefs("$B$3:$D$5"));
                expect(absToken.hasRelRow()).toBeFalse();
            });
        });

        describe("method setRange", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("setRange");
            });
            it("should change range address", () => {
                const token1 = new ReferenceToken(docAccess, cref("B$3"));
                expect(token1).toStringifyTo("ref[B$3]");
                expect(token1.setRange(r("B3:B3"))).toBeFalse();
                expect(token1).toStringifyTo("ref[B$3]");
                expect(token1.setRange(r("D5:D5"))).toBeTrue();
                expect(token1).toStringifyTo("ref[D$5]");
                expect(token1.setRange(r("B3:D5"))).toBeTrue();
                expect(token1).toStringifyTo("ref[B$3:D$5]");
                const token2 = new ReferenceToken(docAccess, crefs("B$3:$D5"));
                expect(token2.setRange(r("B3:D5"))).toBeFalse();
                expect(token2).toStringifyTo("ref[B$3:$D5]");
                expect(token2.setRange(r("C4:D5"))).toBeTrue();
                expect(token2).toStringifyTo("ref[C$4:$D5]");
                expect(token2.setRange(r("C4:E6"))).toBeTrue();
                expect(token2).toStringifyTo("ref[C$4:$E6]");
                expect(token2.setRange(r("C4:C4"))).toBeTrue();
                expect(token2).toStringifyTo("ref[C$4:$C4]");
            });
            it("should convert reference error to valid reference", () => {
                const token = new ReferenceToken(docAccess);
                expect(token).toStringifyTo("ref[#REF]");
                expect(token.setRange(r("B3:D5"))).toBeTrue();
                expect(token).toStringifyTo("ref[$B$3:$D$5]");
            });
        });

        describe("method relocateRange", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("relocateRange");
            });
            it("should relocate cell range without wrapping", () => {
                const token = new ReferenceToken(docAccess, crefs("B$3:$D5"), srefs("$1"));
                expect(token).toStringifyTo("ref[$1!B$3:$D5]");
                expect(token.relocateRange(a("B2"), a("B2"), false)).toBeFalse();
                expect(token).toStringifyTo("ref[$1!B$3:$D5]");
                expect(token.relocateRange(a("A2"), a("B4"), false)).toBeTrue();
                expect(token).toStringifyTo("ref[$1!C$3:$D7]");
                expect(token.relocateRange(a("B4"), a("A2"), false)).toBeTrue();
                expect(token).toStringifyTo("ref[$1!B$3:$D5]");
                expect(token.relocateRange(a("C1"), a("A1"), false)).toBeTrue();
                expect(token).toStringifyTo("ref[$1!#REF]");
            });
            it("should relocate cell range with wrapping", () => {
                const token = new ReferenceToken(docAccess, crefs("B$3:$D5"), srefs("$1"));
                expect(token).toStringifyTo("ref[$1!B$3:$D5]");
                expect(token.relocateRange(a("B2"), a("B2"), true)).toBeFalse();
                expect(token).toStringifyTo("ref[$1!B$3:$D5]");
                expect(token.relocateRange(a("A2"), a("B4"), true)).toBeTrue();
                expect(token).toStringifyTo("ref[$1!C$3:$D7]");
                expect(token.relocateRange(a("B4"), a("A2"), true)).toBeTrue();
                expect(token).toStringifyTo("ref[$1!B$3:$D5]");
                expect(token.relocateRange(a("C1"), a("A1"), true)).toBeTrue();
                expect(token).toStringifyTo("ref[$1!$D$3:XFD5]");
                expect(token.relocateRange(a("A6"), a("A1"), true)).toBeTrue();
                expect(token).toStringifyTo("ref[$1!$D$3:XFD1048576]");
            });
        });

        describe("method transformSheets", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("transformSheets");
            });
            it("should do nothing for tokens without sheet", () => {
                expect(cellToken.transformSheets([null, 0])).toBeFalse();
                expect(cellToken).toStringifyTo("ref[B$3]");
                expect(rangeToken.transformSheets([null, 0])).toBeFalse();
                expect(rangeToken).toStringifyTo("ref[B$3:$D5]");
            });
            it("should transform sheet index", () => {
                const token1 = new ReferenceToken(docAccess, cref("B$3"), srefs("$1"));
                expect(token1.transformSheets([0, 2, 3])).toBeTrue();
                expect(token1).toStringifyTo("ref[$2!B$3]");
                expect(token1.transformSheets([0, 1, 2, 4])).toBeFalse();
                expect(token1).toStringifyTo("ref[$2!B$3]");
                expect(token1.transformSheets([0, null, 1, 2, 3])).toBeTrue();
                expect(token1).toStringifyTo("ref[$1!B$3]");
                expect(token1.transformSheets([0, 1, null, 2])).toBeFalse();
                expect(token1).toStringifyTo("ref[$1!B$3]");
                const token2 = new ReferenceToken(docAccess, cref("B$3"), srefs("$1:$3"));
                expect(token2.transformSheets([1, 2, 3, 4])).toBeTrue();
                expect(token2).toStringifyTo("ref[$2:$4!B$3]");
                expect(token2.transformSheets([0, 1, 2, 4, 5])).toBeTrue();
                expect(token2).toStringifyTo("ref[$2:$5!B$3]");
                expect(token2.transformSheets([0, 1, 2, 3, 4, 5])).toBeFalse();
                expect(token2).toStringifyTo("ref[$2:$5!B$3]");
            });
            it("should do nothing for token with sheet error", () => {
                expect(sheetErrToken.transformSheets([1, 2, 3, 4])).toBeFalse();
                expect(sheetErrToken).toStringifyTo("ref[#REF!B$3]");
            });
        });

        describe("method resolveMovedCells", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("resolveMovedCells");
            });

            const factory = new AddressFactory(16384, 1048576);
            function makeTransformer(interval, dir) {
                return AddressTransformer.fromIntervals(factory, i(interval), dir);
            }

            it("should transform single cell address", () => {
                const token = new ReferenceToken(docAccess, cref("C$3"));
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("A:A", RIGHT), A1, A1, 0)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("A:A", RIGHT), A1, A1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("A:A", RIGHT), A1, A1, 1)).toBe("D$3");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("B:C", RIGHT), A1, A1, 1)).toBe("E$3");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("D:D", RIGHT), A1, A1, 1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("1:1", DOWN), A1, A1, 0)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("1:1", DOWN), A1, A1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("1:1", DOWN), A1, A1, 1)).toBe("C$4");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("2:3", DOWN), A1, A1, 1)).toBe("C$5");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("4:4", DOWN), A1, A1, 1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("A:A", LEFT), A1, A1, 0)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("A:A", LEFT), A1, A1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("A:A", LEFT), A1, A1, 1)).toBe("B$3");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("B:C", LEFT), A1, A1, 1)).toBe("#REF!");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("D:D", LEFT), A1, A1, 1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("1:1", UP), A1, A1, 0)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("1:1", UP), A1, A1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("1:1", UP), A1, A1, 1)).toBe("C$2");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("2:3", UP), A1, A1, 1)).toBe("#REF!");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("4:4", UP), A1, A1, 1)).toBeNull();
            });
            it("should transform range address", () => {
                const token = new ReferenceToken(docAccess, crefs("C$3:$E5"));
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("A:A", RIGHT), A1, A1, 1)).toBe("D$3:$F5");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("D:F", RIGHT), A1, A1, 1)).toBe("C$3:$H5");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("F:F", RIGHT), A1, A1, 1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("1:1", DOWN), A1, A1, 1)).toBe("C$4:$E6");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("4:6", DOWN), A1, A1, 1)).toBe("C$3:$E8");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("6:6", DOWN), A1, A1, 1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("A:A", LEFT), A1, A1, 1)).toBe("B$3:$D5");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("D:F", LEFT), A1, A1, 1)).toBe("C$3:$C5");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("C:E", LEFT), A1, A1, 1)).toBe("#REF!");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("F:F", LEFT), A1, A1, 1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("1:1", UP), A1, A1, 1)).toBe("C$2:$E4");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("4:6", UP), A1, A1, 1)).toBe("C$3:$E3");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("3:5", UP), A1, A1, 1)).toBe("#REF!");
                expect(token.resolveMovedCells(opGrammar, 1, makeTransformer("6:6", UP), A1, A1, 1)).toBeNull();
            });
            it("should transform references with sheet correctly", () => {
                const token = new ReferenceToken(docAccess, cref("C$3"), srefs("$1"));
                const transformer = makeTransformer("A:A", RIGHT);
                expect(token.resolveMovedCells(opGrammar, 0, transformer, A1, A1, 0)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 0, transformer, A1, A1, 1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 0, transformer, A1, A1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, transformer, A1, A1, 0)).toBe("Sheet2!D$3");
                expect(token.resolveMovedCells(opGrammar, 1, transformer, A1, A1, 1)).toBe("Sheet2!D$3");
                expect(token.resolveMovedCells(opGrammar, 1, transformer, A1, A1)).toBe("Sheet2!D$3");
            });
            it("should not transform references with sheet range", () => {
                const token = new ReferenceToken(docAccess, cref("C$3"), srefs("$1:$3"));
                const transformer = makeTransformer("A:A", RIGHT);
                expect(token.resolveMovedCells(opGrammar, 0, transformer, A1, A1, 0)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 0, transformer, A1, A1, 1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 0, transformer, A1, A1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, transformer, A1, A1, 0)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, transformer, A1, A1, 1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 1, transformer, A1, A1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 2, transformer, A1, A1, 0)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 2, transformer, A1, A1, 1)).toBeNull();
                expect(token.resolveMovedCells(opGrammar, 2, transformer, A1, A1)).toBeNull();
            });
            it("should not transform error references", () => {
                const transformer = makeTransformer("A:A", RIGHT);
                expect(errToken.resolveMovedCells(opGrammar, 0, transformer, A1, A1, 0)).toBeNull();
                expect(sheetErrToken.resolveMovedCells(opGrammar, 0, transformer, A1, A1, 0)).toBeNull();
            });
        });

        describe("method resolveCutPasteCells", () => {
            it("should exist", () => {
                expect(ReferenceToken).toHaveMethod("resolveCutPasteCells");
            });

            it("should transform single cell address", () => {
                const token1 = new ReferenceToken(docAccess, cref("B2"));
                expect(token1.resolveCutPasteCells(opGrammar, 0, r("A1:C3"), r("B2:D4"), 0)).toBe("C3");
                expect(token1.resolveCutPasteCells(opGrammar, 0, r("D4:F6"), r("G4:I6"), 0)).toBeNull();

                const token2 = new ReferenceToken(docAccess, cref("$B$2"));
                expect(token2.resolveCutPasteCells(opGrammar, 0, r("A1:C3"), r("B2:D4"), 0)).toBe("$B$2");
                expect(token2.resolveCutPasteCells(opGrammar, 0, r("D4:F6"), r("G4:I6"), 0)).toBeNull();

                const token3 = new ReferenceToken(docAccess, cref("$B2"));
                expect(token3.resolveCutPasteCells(opGrammar, 0, r("A1:C3"), r("B2:D4"), 0)).toBe("$B3");
                expect(token3.resolveCutPasteCells(opGrammar, 0, r("D4:F6"), r("G4:I6"), 0)).toBeNull();

                const token4 = new ReferenceToken(docAccess, cref("B$2"));
                expect(token4.resolveCutPasteCells(opGrammar, 0, r("A1:C3"), r("B2:D4"), 0)).toBe("C$2");
                expect(token4.resolveCutPasteCells(opGrammar, 0, r("D4:F6"), r("G4:I6"), 0)).toBeNull();

                const token5 = new ReferenceToken(docAccess, cref("B2"), srefs("$0"));
                expect(token5.resolveCutPasteCells(opGrammar, 0, r("A1:C3"), r("B2:D4"), 0)).toBe("Sheet1!C3");
                expect(token5.resolveCutPasteCells(opGrammar, 0, r("A1:C3"), r("B2:D4"), 1)).toBe("Sheet1!C3");
                expect(token5.resolveCutPasteCells(opGrammar, 1, r("A1:C3"), r("B2:D4"), 0)).toBeNull();
                expect(token5.resolveCutPasteCells(opGrammar, 1, r("A1:C3"), r("B2:D4"), 1)).toBeNull();
                expect(token5.resolveCutPasteCells(opGrammar, 0, r("D4:F6"), r("G4:I6"), 0)).toBeNull();
            });

            it("should transform cell range (one single col; up/down/other)", () => {
                const token = new ReferenceToken(docAccess, crefs("C10:C15"));

                // complete
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C10:C15"), r("D20:D25"), 0)).toBe("D20:D25");

                // inside
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C11:C14"), r("D20:D25"), 0)).toBeNull();

                // start to top (expand range)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C10:C11"), r("C8:C9"), 0)).toBe("C8:C15");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C9:C10"), r("C1:C2"), 0)).toBe("C2:C15");

                // start to bottom (shrink range)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C10:C11"), r("C12:C13"), 0)).toBe("C12:C15");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C10:C11"), r("C14:C15"), 0)).toBe("C12:C15");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C10:C11"), r("C15:C16"), 0)).toBe("C12:C16");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C10:C11"), r("C25:C26"), 0)).toBeNull();

                // start to ? (do nothing)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C10:C11"), r("D25:D26"), 0)).toBeNull();

                // end to top (shrink range)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C14:C15"), r("C11:C12"), 0)).toBe("C10:C13");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C14:C15"), r("C10:C11"), 0)).toBe("C10:C13");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C14:C15"), r("C9:C10"), 0)).toBe("C9:C13");         // the calculated Range must be bigger than 1, if the original Range was bigger than 1 (don't ask ;)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C14:C15"), r("C1:C2"), 0)).toBeNull();

                // end to bottom (expand range)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C14:C15"), r("C17:C18"), 0)).toBe("C10:C18");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C15:C16"), r("C17:C18"), 0)).toBe("C10:C17");

                // end to ? (do nothing)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("C14:C15"), r("A1:A2"), 0)).toBeNull();
            });

            it("should transform cell range (one single row; left/right/other)", () => {
                const token = new ReferenceToken(docAccess, crefs("E10:J10"));

                // complete
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D10:K10"), r("H20:O20"), 0)).toBe("I20:N20");

                // inside
                expect(token.resolveCutPasteCells(opGrammar, 0, r("F10:I10"), r("H20:K20"), 0)).toBeNull();

                // start to right (shrink range)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:F10"), r("G10:H10"), 0)).toBe("G10:J10");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:F10"), r("I10:J10"), 0)).toBe("G10:J10");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:F10"), r("J10:K10"), 0)).toBe("G10:K10");        // the calculated Range must be bigger than 1, if the original Range was bigger than 1 (don't ask ;)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:F10"), r("M10:N10"), 0)).toBeNull();

                // start to left (expand range)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:F10"), r("A10:B10"), 0)).toBe("A10:J10");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D10:E10"), r("B10:C10"), 0)).toBe("C10:J10");

                // start to ? (do nothing)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:F10"), r("C20:D20"), 0)).toBeNull();

                // end to left (shrink range)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:J10"), r("F10:G10"), 0)).toBe("E10:H10");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:J10"), r("E10:F10"), 0)).toBe("E10:H10");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:J10"), r("D10:E10"), 0)).toBe("D10:H10");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:J10"), r("A10:B10"), 0)).toBeNull();

                // end to right (expand range)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:J10"), r("M10:N10"), 0)).toBe("E10:N10");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("J10:K10"), r("M10:N10"), 0)).toBe("E10:M10");

                // end to ? (do nothing)
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:J10"), r("M11:N11"), 0)).toBeNull();
            });

            it("should transform cell range (top/right/bottom/left/other)", () => {
                const token = new ReferenceToken(docAccess, crefs("E10:I14"));

                // complete
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:J15"), r("G25:L30"), 0)).toBe("G25:K29");

                // inside
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G11:H12"), r("K18:L19"), 0)).toBeNull();

                // top/left-edge ------------------------------------------------------------------------------------------------------------
                // top/left -> up
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:G11"), r("E7:G8"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:G11"), r("E9:G10"), 0)).toBeNull();
                // top/left -> left
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:G11"), r("A10:C11"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:G11"), r("D10:F11"), 0)).toBeNull();
                // top/left -> down
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:G11"), r("E12:G13"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:G11"), r("E22:G23"), 0)).toBeNull();
                // top/left -> right
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:G11"), r("G10:I11"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:G11"), r("L10:N11"), 0)).toBeNull();
                // top/left -> ???
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:G11"), r("A1:C2"), 0)).toBeNull();

                // top/right-edge ------------------------------------------------------------------------------------------------------------
                // top/right -> up
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G10:I11"), r("G7:I8"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G10:I11"), r("G9:I10"), 0)).toBeNull();
                // top/right -> left
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G10:I11"), r("E10:G11"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G10:I11"), r("A10:C11"), 0)).toBeNull();
                // top/right -> down
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G10:I11"), r("G12:I13"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G10:I11"), r("G22:I23"), 0)).toBeNull();
                // top/right -> right
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G10:I11"), r("L10:N11"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G10:I11"), r("H10:J11"), 0)).toBeNull();
                // top/right -> ???
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G10:I11"), r("A1:C2"), 0)).toBeNull();

                // bottom/left-edge ------------------------------------------------------------------------------------------------------------
                // bottom/left -> up
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E13:G14"), r("E11:G12"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E13:G14"), r("E3:G4"), 0)).toBeNull();
                // bottom/left -> left
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E13:G14"), r("D13:F14"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E13:G14"), r("A13:C14"), 0)).toBeNull();
                // bottom/left -> down
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E13:G14"), r("E14:G15"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E13:G14"), r("E22:G23"), 0)).toBeNull();
                // bottom/left -> right
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E13:G14"), r("G13:I14"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E13:G14"), r("L13:N14"), 0)).toBeNull();
                // bottom/left -> ???
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E13:G14"), r("A1:C2"), 0)).toBeNull();

                // bottom/right-edge ------------------------------------------------------------------------------------------------------------
                // bottom/right -> up
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G13:I14"), r("G11:I12"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G13:I14"), r("G3:I4"), 0)).toBeNull();
                // bottom/right -> left
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G13:I14"), r("D13:F14"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G13:I14"), r("A13:C14"), 0)).toBeNull();
                // bottom/right -> down
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G13:I14"), r("G14:I15"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G13:I14"), r("G22:I23"), 0)).toBeNull();
                // bottom/right -> right
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G13:I14"), r("I13:K14"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G13:I14"), r("L13:N14"), 0)).toBeNull();
                // bottom/right -> ???
                expect(token.resolveCutPasteCells(opGrammar, 0, r("G13:I14"), r("A1:C2"), 0)).toBeNull();

                // top-edge ------------------------------------------------------------------------------------------------------------
                // top -> up
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:I10"), r("E8:I8"), 0)).toBe("E8:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D9:J11"),  r("D4:J6"), 0)).toBe("E5:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("F10:H10"), r("F9:H9"), 0)).toBeNull();
                // top -> left
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:I10"), r("D10:H10"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D9:J11"),  r("A9:G11"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("F10:H10"), r("C10:E10"), 0)).toBeNull();
                // top -> down
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:I10"), r("E11:I11"), 0)).toBe("E11:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:I10"), r("E13:I13"), 0)).toBe("E11:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D9:J10"), r("D12:J13"), 0)).toBe("E13:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E9:I10"), r("E12:I13"), 0)).toBe("E13:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E9:I10"), r("E13:I14"), 0)).toBe("E11:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E9:I10"), r("E14:I15"), 0)).toBe("E11:I15");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E9:I10"), r("E13:I14"), 0)).toBe("E11:I14");

                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:I10"), r("E14:I14"), 0)).toBe("E11:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:I10"), r("E17:I17"), 0)).toBeNull();
                // top -> right
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:I10"), r("G10:K10"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:I10"), r("L10:P10"), 0)).toBeNull();
                // top -> ???
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:I10"), r("A1:E5"), 0)).toBeNull();

                // right-edge ------------------------------------------------------------------------------------------------------------
                // right -> up
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:I14"), r("I7:I11"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:I14"), r("I2:I7"), 0)).toBeNull();
                // right -> left
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:I14"), r("H10:H14"), 0)).toBe("E10:H14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:I14"), r("F10:F14"), 0)).toBe("E10:H14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I9:J15"),  r("G9:H15"), 0)).toBe("E10:G14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:I14"), r("E10:E14"), 0)).toBe("E10:H14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:I14"), r("B10:B14"), 0)).toBeNull();
                // right -> down
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:I14"), r("I12:I16"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:I14"), r("I22:I26"), 0)).toBeNull();
                // right -> right
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:I14"), r("J10:J14"), 0)).toBe("E10:J14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:I14"), r("L10:L14"), 0)).toBe("E10:L14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I9:J15"),  r("K9:L15"), 0)).toBe("E10:K14");
                // right -> ???
                expect(token.resolveCutPasteCells(opGrammar, 0, r("I10:I14"),  r("A1:A5"), 0)).toBeNull();

                // bottom-edge ------------------------------------------------------------------------------------------------------------
                // bottom -> up
                expect(token.resolveCutPasteCells(opGrammar, 0, r("F14:H14"), r("F12:H12"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("E13:I13"), 0)).toBe("E10:I13");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("E11:I11"), 0)).toBe("E10:I13");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("E10:I10"), 0)).toBe("E10:I13");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("E2:I2"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D14:J15"), r("D13:J14"), 0)).toBe("E10:I13");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D14:J15"), r("D11:J12"), 0)).toBe("E10:I11");

                expect(token.resolveCutPasteCells(opGrammar, 0, r("D14:J15"), r("D10:J11"), 0)).toBe("E10:I13");

                // bottom -> left
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("C14:G14"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("A14:E14"), 0)).toBeNull();
                // bottom -> down
                expect(token.resolveCutPasteCells(opGrammar, 0, r("F14:H14"), r("F15:H15"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("E15:I15"), 0)).toBe("E10:I15");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("E18:I18"), 0)).toBe("E10:I18");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D14:J15"), r("D15:J16"), 0)).toBe("E10:I15");

                expect(token.resolveCutPasteCells(opGrammar, 0, r("E12:I13"), r("E11:I12"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E13:I14"), r("E11:I12"), 0)).toBe("E10:I12");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("E11:I11"), 0)).toBe("E10:I13");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I15"), r("E11:I12"), 0)).toBe("E10:I11");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I15"), r("E10:I11"), 0)).toBe("E10:I13");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I15"), r("E9:I10"), 0)).toBe("E9:I13");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I15"), r("E8:I9"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I15"), r("E9:I10"), 0)).toBe("E9:I13");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I15"), r("E8:I9"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D12:J14"), r("D9:J11"), 0)).toBe("E9:I11");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E12:I14"), r("E9:I11"), 0)).toBe("E9:I11");

                // bottom -> right
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("G14:K14"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("K14:O14"), 0)).toBeNull();
                // bottom -> ???
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E14:I14"), r("A1:E1"), 0)).toBeNull();

                // left-edge ------------------------------------------------------------------------------------------------------------
                // left -> up
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:E14"), r("E8:E12"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:E14"), r("E2:E7"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D9:E15"),  r("D2:E8"), 0)).toBeNull();
                // left -> left
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E11:E13"), r("D11:D13"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:E14"), r("D10:D14"), 0)).toBe("D10:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:E14"), r("A10:A14"), 0)).toBe("A10:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D9:E15"),  r("A9:B15"), 0)).toBe("B10:I14");
                // left -> down
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:E14"), r("E11:E15"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:E14"), r("E21:E25"), 0)).toBeNull();
                // left -> right
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E11:E13"), r("F9:F13"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:E14"), r("F10:F14"), 0)).toBe("F10:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:E14"), r("H10:H14"), 0)).toBe("F10:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:E14"), r("I10:I14"), 0)).toBe("F10:I14");
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:E14"), r("M10:M14"), 0)).toBeNull();
                expect(token.resolveCutPasteCells(opGrammar, 0, r("D9:E15"),  r("G9:H14"), 0)).toBe("H10:I14");
                // left -> ???
                expect(token.resolveCutPasteCells(opGrammar, 0, r("E10:E14"), r("A1:A5"), 0)).toBeNull();
            });
        });
    });
});
