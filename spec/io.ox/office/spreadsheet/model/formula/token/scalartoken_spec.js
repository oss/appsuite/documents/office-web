/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { BaseToken } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import { ScalarToken } from "@/io.ox/office/spreadsheet/model/formula/token/scalartoken";

// tests ======================================================================

describe("module spreadsheet/model/formula/token/scalartoken", () => {

    let opGrammar, uiGrammar;
    beforeAll(async () => {
        const resFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        const resource = resFactory.getResource("std");
        opGrammar = FormulaGrammar.create(resource);
        uiGrammar = FormulaGrammar.create(resource, { localized: true });
    });

    // class ScalarToken ------------------------------------------------------

    describe("class ScalarToken", () => {
        it("should subclass BaseToken", () => {
            expect(ScalarToken).toBeSubClassOf(BaseToken);
        });

        const numToken = new ScalarToken(12.5);
        const strToken = new ScalarToken('a"b"c');
        const falseToken = new ScalarToken(false);
        const trueToken = new ScalarToken(true);
        const errToken = new ScalarToken(ErrorCode.REF);
        const nullToken = new ScalarToken(null);

        describe("property type", () => {
            it("should exist", () => {
                expect(numToken.type).toBe("lit");
            });
        });

        describe("property value", () => {
            it("should exist", () => {
                expect(numToken.value).toBe(12.5);
                expect(strToken.value).toBe('a"b"c');
                expect(falseToken.value).toBeFalse();
                expect(trueToken.value).toBeTrue();
                expect(errToken.value).toBe(ErrorCode.REF);
                expect(nullToken.value).toBeNull();
            });
        });

        describe("method getText", () => {
            it("should exist", () => {
                expect(ScalarToken).toHaveMethod("getText");
            });
            it("should return the string representation", () => {
                expect(numToken.getText(opGrammar)).toBe("12.5");
                expect(numToken.getText(uiGrammar)).toBe("12,5");
                expect(strToken.getText(opGrammar)).toBe('"a""b""c"');
                expect(strToken.getText(uiGrammar)).toBe('"a""b""c"');
                expect(falseToken.getText(opGrammar)).toBe("FALSE");
                expect(falseToken.getText(uiGrammar)).toBe("FALSCH");
                expect(trueToken.getText(opGrammar)).toBe("TRUE");
                expect(trueToken.getText(uiGrammar)).toBe("WAHR");
                expect(errToken.getText(opGrammar)).toBe("#REF!");
                expect(errToken.getText(uiGrammar)).toBe("#BEZUG!");
                expect(nullToken.getText(opGrammar)).toBe("");
                expect(nullToken.getText(uiGrammar)).toBe("");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(ScalarToken).toHaveMethod("toString");
            });
            it("should return the string representation", () => {
                expect(numToken.toString()).toBe("lit[12.5]");
                expect(strToken.toString()).toBe('lit["a""b""c"]');
                expect(falseToken.toString()).toBe("lit[FALSE]");
                expect(trueToken.toString()).toBe("lit[TRUE]");
                expect(errToken.toString()).toBe("lit[#REF]");
                expect(nullToken.toString()).toBe("lit[null]");
            });
        });

        describe("method setValue", () => {
            it("should exist", () => {
                expect(ScalarToken).toHaveMethod("setValue");
            });
            it("should return whether the value changes", () => {
                const token = new ScalarToken(12.5);
                expect(token.setValue(12.5)).toBeFalse();
                expect(token.value).toBe(12.5);
                expect(token.getText(uiGrammar)).toBe("12,5");
                expect(token.setValue(1)).toBeTrue();
                expect(token.value).toBe(1);
                expect(token.getText(uiGrammar)).toBe("1");
                expect(token.setValue('a"b"c')).toBeTrue();
                expect(token.value).toBe('a"b"c');
                expect(token.getText(uiGrammar)).toBe('"a""b""c"');
                expect(token.setValue(true)).toBeTrue();
                expect(token.value).toBeTrue();
                expect(token.getText(uiGrammar)).toBe("WAHR");
                expect(token.setValue(ErrorCode.REF)).toBeTrue();
                expect(token.value).toBe(ErrorCode.REF);
                expect(token.getText(uiGrammar)).toBe("#BEZUG!");
                expect(token.setValue(null)).toBeTrue();
                expect(token.value).toBeNull();
                expect(token.getText(uiGrammar)).toBe("");
            });
        });
    });
});
