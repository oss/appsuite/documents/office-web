/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Matrix } from "@/io.ox/office/spreadsheet/model/formula/formulautils";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { BaseToken } from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import { MatrixToken } from "@/io.ox/office/spreadsheet/model/formula/token/matrixtoken";

// tests ======================================================================

describe("module spreadsheet/model/formula/token/matrixtoken", () => {

    let opGrammar, uiGrammar;
    beforeAll(async () => {
        const resFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        const resource = resFactory.getResource("std");
        opGrammar = FormulaGrammar.create(resource);
        uiGrammar = FormulaGrammar.create(resource, { localized: true });
    });

    // class MatrixToken ------------------------------------------------------

    describe("class MatrixToken", () => {
        it("should subclass BaseToken", () => {
            expect(MatrixToken).toBeSubClassOf(BaseToken);
        });

        const matrix = new Matrix([[12.5, 'a"b"c'], [true, ErrorCode.REF]]);
        const token = new MatrixToken(matrix);

        describe("property type", () => {
            it("should exist", () => {
                expect(token.type).toBe("mat");
            });
        });

        describe("property value", () => {
            it("should exist", () => {
                expect(token.value).toBe(matrix);
            });
        });

        describe("method getText", () => {
            it("should exist", () => {
                expect(MatrixToken).toHaveMethod("getText");
            });
            it("should return the string representation", () => {
                expect(token.getText(opGrammar)).toBe('{12.5,"a""b""c";TRUE,#REF!}');
                expect(token.getText(uiGrammar)).toBe('{12,5;"a""b""c"|WAHR;#BEZUG!}');
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(MatrixToken).toHaveMethod("toString");
            });
            it("should return the string representation", () => {
                expect(token.toString()).toBe('mat[{12.5;"a""b""c"|TRUE;#REF}]');
            });
        });
    });
});
