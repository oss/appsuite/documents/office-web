/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { NameRefToken } from "@/io.ox/office/spreadsheet/model/formula/token/namereftoken";
import { MacroToken } from "@/io.ox/office/spreadsheet/model/formula/token/macrotoken";

import { srefs, MockDocumentAccess } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/token/macrotoken", () => {

    const docAccess = new MockDocumentAccess({
        cols: 16384,
        rows: 1048576,
        sheets: ["Sheet1", "Sheet2", "Sheet3", "Sheet 4"]
    });

    let opGrammar, uiGrammar;
    beforeAll(async () => {
        const resFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        const resource = resFactory.getResource("std");
        opGrammar = FormulaGrammar.create(resource);
        uiGrammar = FormulaGrammar.create(resource, { localized: true });
    });

    // class MacroToken -------------------------------------------------------

    describe("class MacroToken", () => {
        it("should subclass NameRefToken", () => {
            expect(MacroToken).toBeSubClassOf(NameRefToken);
        });

        const macroToken = new MacroToken(docAccess, "Sum");
        const sheetToken = new MacroToken(docAccess, "Macro", srefs("$3"));
        const errorToken = new MacroToken(docAccess, "Macro", srefs("$-1"));

        describe("property type", () => {
            it("should exist", () => {
                expect(macroToken.type).toBe("macro");
            });
        });

        describe("property value", () => {
            it("should exist", () => {
                expect(macroToken.value).toBe("Sum");
                expect(sheetToken.value).toBe("Macro");
                expect(errorToken.value).toBe("Macro");
            });
        });

        describe("method getText", () => {
            it("should exist", () => {
                expect(MacroToken).toHaveMethod("getText");
            });
            it("should return the string representation", () => {
                expect(macroToken.getText(opGrammar)).toBe("_xludf.Sum");
                expect(macroToken.getText(uiGrammar)).toBe("Sum");
                expect(sheetToken.getText(opGrammar)).toBe("'Sheet 4'!Macro");
                expect(sheetToken.getText(uiGrammar)).toBe("'Sheet 4'!Macro");
                expect(errorToken.getText(opGrammar)).toBe("#REF!");
                expect(errorToken.getText(uiGrammar)).toBe("#BEZUG!");
                const udfToken = new MacroToken(docAccess, "_xludf.SUM");
                expect(udfToken.getText(opGrammar)).toBe("_xludf.SUM");
                expect(udfToken.getText(uiGrammar)).toBe("SUM");
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(MacroToken).toHaveMethod("toString");
            });
            it("should return the string representation", () => {
                expect(macroToken.toString()).toBe("macro[Sum]");
                expect(sheetToken.toString()).toBe("macro[$3!Macro]");
                expect(errorToken.toString()).toBe("macro[#REF!Macro]");
            });
        });

        describe("method hasSheetRef", () => {
            it("should exist", () => {
                expect(MacroToken).toHaveMethod("hasSheetRef");
            });
            it("should return the correct result", () => {
                expect(macroToken.hasSheetRef()).toBeFalse();
                expect(sheetToken.hasSheetRef()).toBeTrue();
                expect(errorToken.hasSheetRef()).toBeTrue();
            });
        });

        describe("method hasSheetError", () => {
            it("should exist", () => {
                expect(MacroToken).toHaveMethod("hasSheetError");
            });
            it("should return the correct result", () => {
                expect(macroToken.hasSheetError()).toBeFalse();
                expect(sheetToken.hasSheetError()).toBeFalse();
                expect(errorToken.hasSheetError()).toBeTrue();
            });
        });

        describe("method transformSheets", () => {
            it("should exist", () => {
                expect(MacroToken).toHaveMethod("transformSheets");
            });
            it("should do nothing for tokens without sheet", () => {
                expect(macroToken.transformSheets([null, 0])).toBeFalse();
                expect(macroToken).toStringifyTo("macro[Sum]");
            });
            it("should transform sheet index", () => {
                const token = new MacroToken(docAccess, "Sum", srefs("$1"));
                expect(token).toStringifyTo("macro[$1!Sum]");
                expect(token.transformSheets([1, 2, 3, 4, 5])).toBeTrue();
                expect(token).toStringifyTo("macro[$2!Sum]");
                expect(token.transformSheets([0, 1, null, 2])).toBeTrue();
                expect(token).toStringifyTo("macro[#REF!Sum]");
            });
            it("should do nothing for token with sheet error", () => {
                expect(errorToken.transformSheets([1, 2, 3, 4])).toBeFalse();
                expect(errorToken).toStringifyTo("macro[#REF!Macro]");
            });
        });
    });
});
