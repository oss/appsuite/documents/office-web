/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { Direction, ErrorCode } from "@/io.ox/office/spreadsheet/utils/sheetutils";
import { DependencyManager } from "@/io.ox/office/spreadsheet/model/formula/deps/dependencymanager";

import { waitForEvent } from "~/asynchelper";
import { i, waitForRecalcEnd, createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// private functions ==========================================================

// changes the value of a single cell
function execChangeCells(docModel, sheet, values) {
    docModel.invokeOperationHandlers(op.changeCells(sheet, values));
}

// changes the formula expression of a single cell
function execChangeFormula(docModel, sheet, address, formula) {
    docModel.invokeOperationHandlers(op.changeCells(sheet, { [address]: { f: formula } }));
}

// changes the formula expression of a defined name
function execChangeNameFormula(docModel, label, formula) {
    docModel.invokeOperationHandlers(op.changeName(label, { formula }));
}

// inserts a defined name
function execInsertName(docModel, label, formula) {
    docModel.invokeOperationHandlers(op.insertName(label, { formula }));
}

// deletes a defined name
function execDeleteName(docModel, label) {
    docModel.invokeOperationHandlers(op.deleteName(label));
}

async function expectRecalcWithCycles(docModel, ...expCycles) {
    const cycles = await waitForRecalcEnd(docModel);
    expect(cycles).toBeArrayOfSize(expCycles.length);
    for (const [idx1, cycle] of cycles.entries()) {
        const expCycle = expCycles[idx1];
        expect(cycle).toBeArrayOfSize(expCycle.length / 2);
        for (const [idx2, entry] of cycle.entries()) {
            const sheet = expCycle[idx2 * 2];
            const addr = expCycle[idx2 * 2 + 1];
            expect(entry).toHaveProperty("sheet", sheet);
            expect(entry).toHaveProperty("sheetUid", docModel.getSheetModel(sheet).uid);
            expect(entry.address).toStringifyTo(addr);
        }
    }
}

// tests ======================================================================

describe("module spreadsheet/model/formula/deps/dependencymanager", () => {

    // initialize test document
    const appPromise = createSpreadsheetApp("ooxml", [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertName("volatile", { formula: "RAND()" }),
        op.insertName("range1", { formula: "DefinedNames!$B$6:$E$9" }),
        op.insertName("indirect1", { formula: "range1" }),
        op.insertName("range2", { formula: "DefinedNames!$B$33:$E$33" }),
        op.insertName("cycle1", { formula: "CircularReferences!$B$33" }),

        op.insertSheet(0, "CellFormulas"),
        op.changeCells(0, {
            B6: 0,
            A9: { v: 0, f: "B6*2" }, A10: { v: 1, f: "A9+1" }, A11: { v: 1, f: "B6+A10" }, A12: { v: 2, f: "SUM(A9:A11)" },
            A22: { v: 0, f: "RAND()" }, B22: { v: 0, f: "volatile" },
            A31: { v: 0, f: "SUM(O:O,Q:T,V:AC,36:36,38:41,43:50)" },
            A100: 1, B100: 2, A101: 3, B101: 4, // target cells of CF rule
            // DOCS-650: recalculate formulas with wrong result types after import
            A300: { v: null, f: "1+1" }, B300: { v: "2", f: "1+1" },
            // DOCS-5295: shared formulas update after deleting row
            A400: { v: 3, f: "A401+A402", si: 0, sr: "A400:C400" }, C400: { v: 7, si: 0 },
            A401: 1, C401: 3,
            A402: 2, C402: 4,
        }),

        op.insertSheet(1, "DefinedNames"),
        op.insertName("local1", 1, { formula: "DefinedNames!$B$6:$E$9" }),
        op.changeCells(1, { A12: { v: 0, f: "SUM(range1,local1)" } }),
        op.changeCells(1, { A23: { v: 0, f: "PRODUCT(indirect1)" } }),
        op.changeCells(1, "B33", [[3, 3, 3, 37]]),
        op.changeCells(1, { A42: { e: "#NAME!", f: "missing+1" } }),

        // bug 51455: recalculate formulas with table references after undoing deleted table
        op.insertSheet(2, "TableRanges"),
        op.changeCells(2, "A6", [
            ["Col1", "Col2", "Col3", "Col4"],
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, { v: 10, f: "B2" }, 11, 12],
            [{ v: 15, f: "SUM(Table1[Col1])" }, { v: 18, f: "SUM(Table1[Col2])" }, { v: 21, f: "SUM(Table1[Col3])" }, { v: 24, f: "SUM(Table1[Col4])" }]
        ]),
        op.insertTable(2, "Table1", "A6:D10", { table: { headerRow: true, footerRow: true } }),
        op.changeCells(2, {
            B2: 10,
            F7: { v: 10, f: "SUM(Table1[#This Row])" },
            F8: { v: 6, f: "Table1[[#This Row],[Col2]]" },
            F9: { v: 30, f: "SUM(Table1[[#This Row],[Col1]:[Col3]])" },
            A13: { v: 66, f: "SUM(Table1[])" },
            A14: { v: "Col1", f: "Table1[#Headers]" },
            A15: { v: 18, f: "SUM(Table1[Col2])" },
            A16: { v: 54, f: "SUM(Table1[[Col1]:[Col3]])" },
            A17: { v: 18, f: "Table1[[#Totals],[Col2]]" },
            A18: { v: 54, f: "SUM(Table1[[#Totals],[Col1]:[Col3]])" }
        }),

        op.insertSheet(3, "CircularReferences"),
        op.changeCells(3, { B20: { v: 1, f: "B17+1" }, B33: { v: 1, f: "B30+1" } }),

        op.insertSheet(4, "FormattingRules"),
        op.insertCFRule(4, "R0", "A100:B101", { type: "equal", value1: "CellFormulas!A100", priority: 1, attrs: { character: { bold: true } } }),
        op.changeCells(4, { A100: 1, B100: 2, A101: 3, B101: 4 })
    ]);

    // class DependencyManager ------------------------------------------------

    describe("class DependencyManager", () => {

        it("should subclass ModelObject", () => {
            expect(DependencyManager).toBeSubClassOf(ModelObject);
        });

        it("should be contained in a document", async () => {
            const { docModel } = await appPromise;
            expect(docModel.dependencyManager).toBeInstanceOf(DependencyManager);
        });

        describe("method recalcFormulas", () => {
            it("should exist", () => {
                expect(DependencyManager).toHaveMethod("recalcFormulas");
            });
            it("should wait for initial update cycle", async () => {
                const { docModel } = await appPromise;
                await docModel.dependencyManager.recalcFormulas({ volatile: false, timeout: 1000 });
                // volatile functions
                docModel.expect.sheet(0).values({ A22: v => v !== 0, A23: v => v !== 0 });
                // DOCS-650: recalculate formulas with wrong cell value type
                docModel.expect.sheet(0).values({ A300: 2, B300: 2 });
            });
        });

        describe("background update task", () => {

            it("should update formula results after changing a cell", async () => {
                const { docModel } = await appPromise;
                const spy = jest.fn(); // check for the "recalc:stage" events
                docModel.dependencyManager.on("recalc:stage", spy);
                execChangeCells(docModel, 0, { B6: 1 });
                await waitForRecalcEnd(docModel);
                expect(spy).toHaveBeenCalledTimes(3);
                expect(spy).toHaveBeenNthCalledWith(1, "collect");
                expect(spy).toHaveBeenNthCalledWith(2, "calculate");
                expect(spy).toHaveBeenNthCalledWith(3, "send");
                docModel.dependencyManager.off("recalc:stage", spy);
                docModel.expect.sheet(0).values({ A9: 2, A10: 3, A11: 4, A12: 9 });
            });

            it("should update formula results after changing cells in large target ranges", async () => {
                const { docModel } = await appPromise;
                execChangeCells(docModel, 0, { O36: 1, R39: 3, X45: 5 });
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(0).values({ A31: 18 });
            });

            it("should update all shared formula results after changing formula expression", async () => { // DOCS-5295
                const { docModel } = await appPromise;
                execChangeCells(docModel, 0, { A400: { f: "A401*A402" } });
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(0).values({ A400: 2, C400: 12 });
            });

            it("should update all formula results after restarting worker", async () => { // DOCS-5295
                const { docModel } = await appPromise;
                // change cell B6 to value 2 initially
                execChangeCells(docModel, 0, { B6: 2 });
                // during recalculation, change cell B6 again to value 3
                docModel.dependencyManager.on("recalc:stage", stage => {
                    if (stage === "calculate") {
                        execChangeCells(docModel, 0, { B6: 3 });
                        docModel.dependencyManager.off("recalc:stage");
                    }
                });
                // expect an "recalc:cancel" to be triggered
                await waitForEvent(docModel.dependencyManager, "recalc:cancel");
                // expect following restarted recalc cycle to succeed
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(0).values({ A9: 6, A10: 7, A11: 10, A12: 23 });
            });

            it("should update formula results after changing target cells of defined names", async () => {
                const { docModel } = await appPromise;
                execChangeCells(docModel, 1, { B6: 2, E9: 3 });
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(1).values({ A12: 10, A23: 6 });
            });

            it("should update formula results after changing a defined name", async () => { // DOCS-4396
                const { docModel } = await appPromise;
                execChangeNameFormula(docModel, "indirect1", "range2");
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(1).values({ A23: 999 });
            });

            it("should update formula results after deleting a defined name", async () => { // DOCS-4396
                const { docModel } = await appPromise;
                execDeleteName(docModel, "range2");
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(1).values({ A23: ErrorCode.NAME });
            });

            it("should update formula results after inserting a defined name", async () => {
                const { docModel } = await appPromise;
                execInsertName(docModel, "missing", "DefinedNames!$A$12");
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(1).values({ A42: 11 });
            });

            it("should update formulas after changing table range contents", async () => { // bug 51455
                const { docModel } = await appPromise;

                // directly inside table range
                execChangeCells(docModel, 2, { B7: 1, B8: 5 });
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(2).values({
                    B10: 16, F7: 9, F8: 5, F9: 30,
                    A13: 76, A14: "Col1", A15: 16, A16: 52, A17: 16, A18: 52
                });

                // indirectly via cell reference
                execChangeCells(docModel, 2, { B2: 9 });
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(2).values({
                    B10: 15, F7: 9, F8: 5, F9: 29,
                    A13: 75, A14: "Col1", A15: 15, A16: 51, A17: 15, A18: 51
                });
            });

            it("should update formulas after undoing a deleted table range", async () => { // bug 51455
                const { docModel } = await appPromise;

                const sheetModel = docModel.getSheetModel(2);
                await docModel.buildOperations(builder => sheetModel.generateMoveOperations(builder, i("6:10"), Direction.UP));
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(2).values("A8", ErrorCode.REF);

                await docModel.undoManager.undo();
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(2).values({ A13: 75 });
            });

            it("should detect circular references", async () => {
                const { docModel } = await appPromise;

                execChangeFormula(docModel, 3, "B6", "B6+1");
                await expectRecalcWithCycles(docModel, [3, "B6"]);
                docModel.expect.sheet(3).values({ B6: 0 });

                execChangeFormula(docModel, 3, "B6", "SUM(B6)");
                await expectRecalcWithCycles(docModel, [3, "B6"]);
                docModel.expect.sheet(3).values({ B6: 0 });

                execChangeFormula(docModel, 3, "B6", 'INDIRECT("B"&6)');
                await expectRecalcWithCycles(docModel, [3, "B6"]);
                docModel.expect.sheet(3).values({ B6: 0 });

                execChangeFormula(docModel, 3, "B6", "ROW(B6)");
                await expectRecalcWithCycles(docModel);
                docModel.expect.sheet(3).values({ B6: 6 });

                execChangeFormula(docModel, 3, "B17", "B20");
                await expectRecalcWithCycles(docModel, [3, "B17", 3, "B20", 3, "B17"]);
                docModel.expect.sheet(3).values({ B17: 0 });

                execChangeFormula(docModel, 3, "B17", 'INDIRECT("B"&20)');
                await expectRecalcWithCycles(docModel, [3, "B17", 3, "B20", 3, "B17"]);
                docModel.expect.sheet(3).values({ B17: 0 });

                execChangeFormula(docModel, 3, "B17", "ROW(B20)");
                await expectRecalcWithCycles(docModel);
                docModel.expect.sheet(3).values({ B17: 20 });

                execChangeFormula(docModel, 3, "B30", "cycle1");
                await expectRecalcWithCycles(docModel, [3, "B30", 3, "B33", 3, "B30"]);
                docModel.expect.sheet(3).values({ B30: 0 });

                execChangeFormula(docModel, 3, "B30", 'INDIRECT("cycle"&1)');
                await expectRecalcWithCycles(docModel, [3, "B30", 3, "B33", 3, "B30"]);
                docModel.expect.sheet(3).values({ B30: 0 });

                execChangeFormula(docModel, 3, "B30", "ROW(cycle1)");
                await expectRecalcWithCycles(docModel);
                docModel.expect.sheet(3).values({ B30: 33 });
            });

            it("should update formatting rules after changing a cell", async () => {
                const { docModel } = await appPromise;
                const [arg] = await waitForEvent(docModel.getSheetModel(4), "refresh:ranges", () => {
                    execChangeCells(docModel, 0, { A100: 2 });
                });
                expect(arg.ranges).toStringifyTo("A100:B101");
            });

            it("should update formulas in a copied sheet", async () => { // bug 51453
                const { docModel } = await appPromise;
                const toSheet = docModel.getSheetCount();
                docModel.invokeOperationHandlers(op.copySheet(0, toSheet, "CopyOfSheet"));
                execChangeCells(docModel, toSheet, { B6: 0 });
                await waitForRecalcEnd(docModel);
                docModel.expect.sheet(toSheet, "CopyOfSheet").values({ A9: 0, A10: 1, A11: 1, A12: 2 });
            });
        });
    });
});
