/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as basetoken from "@/io.ox/office/spreadsheet/model/formula/token/basetoken";
import * as fixedtoken from "@/io.ox/office/spreadsheet/model/formula/token/fixedtoken";
import * as parenthesistoken from "@/io.ox/office/spreadsheet/model/formula/token/parenthesistoken";
import * as separatortoken from "@/io.ox/office/spreadsheet/model/formula/token/separatortoken";
import * as operatortoken from "@/io.ox/office/spreadsheet/model/formula/token/operatortoken";
import * as functiontoken from "@/io.ox/office/spreadsheet/model/formula/token/functiontoken";
import * as scalartoken from "@/io.ox/office/spreadsheet/model/formula/token/scalartoken";
import * as matrixtoken from "@/io.ox/office/spreadsheet/model/formula/token/matrixtoken";
import * as sheetreftoken from "@/io.ox/office/spreadsheet/model/formula/token/sheetreftoken";
import * as referencetoken from "@/io.ox/office/spreadsheet/model/formula/token/referencetoken";
import * as namereftoken from "@/io.ox/office/spreadsheet/model/formula/token/namereftoken";
import * as definednametoken from "@/io.ox/office/spreadsheet/model/formula/token/definednametoken";
import * as macrotoken from "@/io.ox/office/spreadsheet/model/formula/token/macrotoken";
import * as tabletoken from "@/io.ox/office/spreadsheet/model/formula/token/tabletoken";

import * as tokens from "@/io.ox/office/spreadsheet/model/formula/tokens";

// tests ======================================================================

describe("module spreadsheet/model/formula/tokens", () => {

    // re-exports -------------------------------------------------------------

    it("re-exports should exist", () => {
        expect(tokens).toContainProps(basetoken);
        expect(tokens).toContainProps(fixedtoken);
        expect(tokens).toContainProps(parenthesistoken);
        expect(tokens).toContainProps(separatortoken);
        expect(tokens).toContainProps(operatortoken);
        expect(tokens).toContainProps(functiontoken);
        expect(tokens).toContainProps(scalartoken);
        expect(tokens).toContainProps(matrixtoken);
        expect(tokens).toContainProps(sheetreftoken);
        expect(tokens).toContainProps(referencetoken);
        expect(tokens).toContainProps(namereftoken);
        expect(tokens).toContainProps(definednametoken);
        expect(tokens).toContainProps(macrotoken);
        expect(tokens).toContainProps(tabletoken);
    });

    // public functions -------------------------------------------------------

    describe("function isOpToken", () => {
        it("should exist", () => {
            expect(tokens.isOpToken).toBeFunction();
        });
        it("should recognize operator tokens", () => {
            const opToken = new tokens.OperatorToken("mul");
            expect(tokens.isOpToken(opToken, "mul")).toBeTrue();
            expect(tokens.isOpToken(opToken, "add")).toBeFalse();
        });
        it("should reject other tokens", () => {
            expect(tokens.isOpToken(new tokens.FixedToken("mul"), "mul")).toBeFalse();
            expect(tokens.isOpToken(42, "add")).toBeFalse();
            expect(tokens.isOpToken(undefined, "add")).toBeFalse();
        });
    });

    describe("function isWsToken", () => {
        it("should exist", () => {
            expect(tokens.isWsToken).toBeFunction();
        });
        it("should recognize whitespace tokens", () => {
            const wsToken = new tokens.FixedToken("ws", " ");
            expect(tokens.isWsToken(wsToken)).toBeTrue();
        });
        it("should reject other tokens", () => {
            expect(tokens.isWsToken(new tokens.FixedToken("bad", " "))).toBeFalse();
            expect(tokens.isWsToken(new tokens.OperatorToken("mul"))).toBeFalse();
            expect(tokens.isWsToken(42)).toBeFalse();
            expect(tokens.isWsToken(undefined)).toBeFalse();
        });
    });
});
