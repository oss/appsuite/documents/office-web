/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Operand } from "@/io.ox/office/spreadsheet/model/formula/interpret/operand";
import WebFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/webfuncs";

import { createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/funcs/webfuncs", () => {

    // initialize the test
    const appPromise = createSpreadsheetApp("ooxml");
    const moduleTester = createFunctionModuleTester(WebFuncs, appPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("HYPERLINK", function (HYPERLINK) {
        const url = "http://www.example.com";
        it("should return the URL as value, and as 'url' operand option", () => {
            const result = HYPERLINK.invoke(url);
            expect(result).toBeInstanceOf(Operand);
            expect(result.getRawValue()).toBe(url);
            expect(result.url).toBe(url);
        });
        it("should return the second parameter as value", () => {
            const result = HYPERLINK.invoke(url, 42);
            expect(result).toBeInstanceOf(Operand);
            expect(result.getRawValue()).toBe(42);
            expect(result.url).toBe(url);
        });
    });
});
