/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, dict } from "@/io.ox/office/tk/algorithms";

import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";

import { FunctionSpecFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/functionspec";
import { FUNCTION_CATEGORY_LABELS } from "@/io.ox/office/spreadsheet/view/labels";

import OperatorsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/operators";
import ComplexFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/complexfuncs";
import ConversionFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/conversionfuncs";
import DatabaseFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/databasefuncs";
import DatetimeFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/datetimefuncs";
import EngineeringFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/engineeringfuncs";
import FinancialFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/financialfuncs";
import InformationFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/informationfuncs";
import LogicalFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/logicalfuncs";
import MathFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/mathfuncs";
import MatrixFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/matrixfuncs";
import ReferenceFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/referencefuncs";
import StatisticalFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/statisticalfuncs";
import TextFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/textfuncs";
import WebFuncsModule from "@/io.ox/office/spreadsheet/model/formula/funcs/webfuncs";

// constants ==================================================================

const FUNCTION_MODULES = [
    ComplexFuncsModule,
    ConversionFuncsModule,
    DatabaseFuncsModule,
    DatetimeFuncsModule,
    EngineeringFuncsModule,
    FinancialFuncsModule,
    InformationFuncsModule,
    LogicalFuncsModule,
    MathFuncsModule,
    MatrixFuncsModule,
    ReferenceFuncsModule,
    StatisticalFuncsModule,
    TextFuncsModule,
    WebFuncsModule
];

// identifiers of all supported file formats
const FILE_FORMATS = Array.from(dict.values(FileFormatType));

// keys of all supported function categories
const FUNCTION_CATEGORIES = Object.keys(FUNCTION_CATEGORY_LABELS);

// regular expressions for valid property values
const PARAM_TYPE_RE = /^(val(:(num|int|bint|date|day|str|bool|comp|any))?|mat(:(num|str|bool|any))?|ref(:(sheet|single|multi|val))?|any(:lazy)?)$/;

// private functions ==========================================================

function makeMsg(name, prop) {
    return name + JSON.stringify([prop]);
}

function testSpecModule(module) {
    expect(module).toBeObject();
    if (module.category !== undefined) {
        expect(module.category).toBeOneOf(FUNCTION_CATEGORIES);
    }
    expect(module.register).toBeFunction();
    module.map = new Map(new FunctionSpecFactory(module));
    expect(module.map.size).toBeGreaterThan(0);
}

function testPropertyNames(desc, key, re) {
    dict.forEach(desc, (value, prop) => {
        const msg = makeMsg(key, prop);
        expect(prop, msg).toMatch(re);
        if (is.dict(value)) {
            dict.forEachKey(value, format => {
                expect(format, msg).toBeOneOf(FILE_FORMATS);
            });
        }
    });
}

function resolveSpec(desc, format) {
    const result = {};
    dict.forEach(desc, (value, key) => {
        if (is.dict(value)) {
            if (format in value) {
                result[key] = value[format];
            }
        } else {
            result[key] = value;
        }
    });
    return result;
}

function testFunctionCategory(desc, key) {
    if (!("category" in desc)) { return; }
    var msg = makeMsg(key, "category");
    if (is.string(desc.category)) {
        expect(desc.category, msg).toBeOneOf(FUNCTION_CATEGORIES);
    } else {
        expect(desc.category, msg).toBeArray();
        for (const category of desc.category) {
            expect(category, msg).toBeOneOf(FUNCTION_CATEGORIES);
        }
    }
}

function testFormatCategory(desc, key) {
    if (!("format" in desc)) { return; }
    expect(desc.type, makeMsg(key, "type")).toMatch(/^(val|mat)/);
    expect(desc.format, makeMsg(key, "format")).toMatch(/^(percent|fraction|currency|date|time|datetime|infer)$/);
}

function testOperatorNameProperty(desc, key, format, outMap) {
    const msg = makeMsg(key, "name");
    const outArray = outMap[format] ??= [];
    const names = is.string(desc.name) ? [desc.name] : desc.name;
    expect(names, msg).toBeArray();
    expect(names.length, msg).toBeGreaterThan(0);
    for (const name of names) {
        expect(name, msg).toMatch(/^[^A-Z0-9._]{1,2}$/i);
        outArray.push(name);
    }
}

function testFunctionNameProperty(desc, key, format, outMap) {
    const msg = makeMsg(key, "name");
    let outArray = outMap[format] ??= [];
    if ("name" in desc) {
        if (desc.name === null) { return; }
        const names = is.string(desc.name) ? [desc.name] : desc.name;
        expect(names, msg).toBeArray();
        expect(names.length, msg).toBeGreaterThan(0);
        for (const name of names) {
            expect(name, msg).toMatch(/^(_xlfn\.)?[A-Z][A-Z0-9]*([._][A-Z0-9]+)*$/);
        }
        outArray = outArray.concat(names);
    } else {
        outArray.push(key);
    }
    outMap[format] = outArray;
}

function testTypeProperty(desc, key, re) {
    expect(desc, makeMsg(key, "type")).toHaveProperty("type");
    expect(desc.type, makeMsg(key, "type")).toMatch(re);
}

function testRecalcProperty(desc, key) {
    if ("recalc" in desc) {
        expect(desc.recalc, makeMsg(key, "recalc")).toMatch(/^(always|once)$/);
    }
}

function testSignatureProperty(desc, key) {
    const msg = makeMsg(key, "signature");
    const sig = desc.signature;
    expect(sig, msg).toBeArray();
    sig.forEach((param, index) => {
        const msg1 = makeMsg(msg, index);
        if (is.string(param)) {
            expect(param, msg1).toMatch(PARAM_TYPE_RE);
        } else {
            expect(param, msg1).toBeObject();
            expect(param, msg1).toHaveProperty("type");
            dict.forEach(param, (val, key2) => {
                switch (key2) {
                    case "type": expect(val, msg1).toMatch(PARAM_TYPE_RE); break;
                    case "dep": expect(val, msg1).toMatch(/^(resolve|skip|pass)$/); break;
                    case "mat": expect(val, msg1).toMatch(/^(none|force|pass|forward)$/); break;
                    default: throw new Error(`unexpected parameter signature property "${key2}"`);
                }
            });
        }
    });
}

function forEachOperator(callback) {
    for (const format of FILE_FORMATS) {
        for (const [key, spec] of OperatorsModule.map) {
            callback(spec, key, format);
        }
    }
}

function forEachFunction(callback) {
    for (const format of FILE_FORMATS) {
        for (const module of FUNCTION_MODULES) {
            for (const [key, spec] of module.map) {
                callback(spec, key, format);
            }
        }
    }
}

// tests ======================================================================

describe("modules io.ox/office/spreadsheet/model/formula/funcs/*", () => {

    describe("operator specification module", () => {
        it("should exist", () => {
            testSpecModule(OperatorsModule);
        });
    });

    describe("function specification modules", () => {
        it("should exist", () => {
            for (const specModule of FUNCTION_MODULES) {
                testSpecModule(specModule);
            }
        });
    });

    describe("operator keys", () => {
        it("should have valid resource keys", () => {
            for (const key of OperatorsModule.map.keys()) {
                expect(key).toMatch(/^[a-z]+$/);
            }
        });
    });

    describe("function keys", () => {
        it("should have valid resource keys", () => {
            for (const module of FUNCTION_MODULES) {
                for (const key of module.map.keys()) {
                    expect(key).toMatch(/^[A-Z][A-Z0-9]*(\.[A-Z0-9]+)*$/);
                }
            }
        });
        it("should have unique resource keys", () => {
            const keys = FUNCTION_MODULES.flatMap(module => Array.from(module.map.keys()));
            expect(keys).toHaveLength(new Set(keys).size);
        });
    });

    describe("operator specifications", () => {
        it("should be objects", () => {
            for (const [key, spec] of OperatorsModule.map) {
                expect(spec, key).toBeObject();
            }
        });
        it("should not contain unknown properties", () => {
            for (const [key, spec] of OperatorsModule.map) {
                testPropertyNames(spec, key, /^(category|name|(min|max)Params|type|format|recalc|signature|resolve)$/);
            }
        });
        const namesMap = {};
        it("should contain correct properties", () => {
            forEachOperator((spec, key, format) => {
                const desc = resolveSpec(spec, format);
                testFunctionCategory(desc, key);
                testOperatorNameProperty(desc, key, format, namesMap);
                expect(desc.minParams, makeMsg(key, "minParams")).toBeIntegerInInterval(1, 2);
                expect(desc.maxParams, makeMsg(key, "maxParams")).toBeIntegerInInterval(desc.minParams, 2);
                testTypeProperty(desc, key, /^(val:(num|str|bool|any)|ref)$/);
                testFormatCategory(desc, key);
                testRecalcProperty(desc, key);
                testSignatureProperty(desc, key);
                const msg1 = makeMsg(key, "resolve");
                if ("resolve" in desc) {
                    expect(is.function(desc.resolve) || is.string(desc.resolve), msg1).toBeTrue();
                }
                if (is.string(desc.resolve)) {
                    expect(desc.resolve, msg1).not.toBe(key);
                    expect(OperatorsModule.map.has(desc.resolve), msg1).toBeTrue();
                }
            });
        });
        it("should have unique names", () => {
            dict.forEach(namesMap, names => {
                expect(names).toHaveLength(new Set(names).size + 2); // unary and binary plus/minus
            });
        });
    });

    describe("function specifications", () => {
        it("should be objects", () => {
            for (const module of FUNCTION_MODULES) {
                for (const [key, spec] of module.map) {
                    expect(spec, key).toBeObject();
                }
            }
        });
        it("should not contain unknown properties", () => {
            for (const module of FUNCTION_MODULES) {
                for (const [key, spec] of module.map) {
                    testPropertyNames(spec, key, /^(category|name|hidden|(min|max|repeat)Params|type|format|recalc|signature|resolve|relColRef|relRowRef)$/);
                }
            }
        });
        const namesMap = {};
        it("should contain correct properties", () => {
            const keys = FUNCTION_MODULES.flatMap(module => Array.from(module.map.keys()));
            forEachFunction((spec, key, format) => {
                const desc = resolveSpec(spec, format);
                testFunctionCategory(desc, key);
                testFunctionNameProperty(desc, key, format, namesMap);
                if ("hidden" in desc) { expect(desc.hidden, makeMsg(key, "hidden")).toBeBoolean(); }
                expect(desc.minParams, makeMsg(key, "minParams")).toBeIntegerInInterval(0, 10);
                if ("maxParams" in desc) { expect(desc.maxParams, makeMsg(key, "maxParams")).toBeIntegerInInterval(desc.minParams, 10); }
                if ("repeatParams" in desc) { expect(desc.repeatParams, makeMsg(key, "repeatParams")).toBeIntegerInInterval(1, 10); }
                expect(!("maxParams" in desc) || !("repeatParams" in desc), makeMsg(key, "maxParams")).toBeTrue();
                testTypeProperty(desc, key, /^(val:(num|bint|str|bool|err|date|comp|any)|mat:(num|str|bool|any)|ref|any)$/);
                testFormatCategory(desc, key);
                testRecalcProperty(desc, key);
                testSignatureProperty(desc, key);
                const msg1 = makeMsg(key, "resolve");
                if ("resolve" in desc) {
                    expect(is.function(desc.resolve) || is.string(desc.resolve), msg1).toBeTrue();
                    if (desc.maxParams !== 0) { expect(desc, key).toHaveProperty("signature"); }
                }
                if (is.string(desc.resolve)) {
                    expect(desc.resolve, msg1).not.toBe(key);
                    expect(desc.resolve, msg1).toBeOneOf(keys);
                }
                if ("relColRef" in desc) { expect(desc.relColRef, makeMsg(key, "relColRef")).toBeFunction(); }
                if ("relRowRef" in desc) { expect(desc.relRowRef, makeMsg(key, "relRowRef")).toBeFunction(); }
            });
        });
        it("should have unique names", () => {
            dict.forEach(namesMap, names => {
                expect(names).toHaveLength(new Set(names).size);
            });
        });
    });
});
