/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ReferenceFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/referencefuncs";

import { r3d, r3da, mat, ErrorCode, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/funcs/referencefuncs", () => {

    // the operations to be applied at the test document
    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),

        op.insertSheet(0, "Sheet1"),
        op.changeCells(0, "A2", [
            [2, { v: 3, f: "SUM(1,2)" }, "abc"],
            [{ v: 2, f: "A2", si: 0, sr: "A3:B3" }, { v: 3, si: 0 }],
            [{ v: 4, f: "A3:B3*2", mr: "A4:B4" }, 6],
            [{ v: 4, f: "A3:B3*2", mr: "A5:B5", md: true }, 6]
        ]),

        //horizontal data
        op.insertSheet(1, "Sheet2"),
        op.changeCells(1, "A1", [
            [4.14, 4.19, 5.17, 5.77, 6.39],
            [],
            ["Rot", "Orange", "Gelb", "Gruen", "Blau"],
            [],
            [4.14, null, null, 4.19, 5.17, 5.77, 6.39],
            [],
            ["Rot", null, null, "Orange", "Gelb", "Gruen", "Blau"]
        ]),

        //vertical data
        op.insertSheet(2, "Sheet3"),
        op.changeCells(2, "A1", [
            [4.14, null, "Rot"],
            [4.19, null, "Orange"],
            [5.17, null, "Gelb"],
            [5.77, null, "Gruen"],
            [6.39, null, "Blau"]
        ]),

        //indirect data
        op.insertSheet(3, "Sheet4"),
        op.insertName("name1", { formula: "Sheet4!$C$2:$D$3" }),
        op.changeCells(3, "A1", [
            ["B1",     null, 1333],
            ["B2",     null, 45],
            ["George", null, 10],
            [5,        null, 62]
        ]),

        // MATCH/LOOKUP functions
        op.insertSheet(4, "Sheet5"),
        op.changeCells(4, "K1", [
            [10, 20, 30,   40, null, null, null, 50, 60, 70],
            [70, 60, 50,   40, null, null, null, 30, 20, 10],
            [10, 20, 30,   99, null, null, null, 50, 60, 70],
            [10, 20, 30, null, null, null,   99, 50, 60, 70],
            [70, 60, 50,  -99, null, null, null, 30, 20, 10],
            [70, 60, 50, null, null, null,  -99, 30, 20, 10]
        ]),
        op.changeCells(4, "A100", [
            [10, 20, 1],
            [20, 30, 2],
            [30, 40, 3],
            [40, 50, 4],
            [50, 60, 5]
        ]),
        op.changeCells(4, "E100", [
            [10, 20, 30, 40, 50],
            [20, 30, 40, 50, 60],
            [1, 2, 3, 4, 5]
        ]),
        op.changeCells(4, "K100", [
            [10, 20, 30],
            [20,  0,  2],
            [30,  1,  0]
        ])
    ];

    // initialize the test
    const appPromise = createSpreadsheetApp("ooxml", OPERATIONS);
    const moduleTester = createFunctionModuleTester(ReferenceFuncs, appPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("CHOOSE", { toOperand: [1, 2, 3, 4, 5, 6, 7] }, CHOOSE => {
        const mat1 = mat([[1]]);
        const ranges = r3da("0:0!A1:A1");
        it("should return selected parameter", () => {
            expect(CHOOSE(1, 2, "a", false, ErrorCode.DIV0, null, mat1, ranges)).toBe(2);
            expect(CHOOSE(2, 2, "a", false, ErrorCode.DIV0, null, mat1, ranges)).toBe("a");
            expect(CHOOSE(3, 2, "a", false, ErrorCode.DIV0, null, mat1, ranges)).toBeFalse();
            expect(CHOOSE(4, 2, "a", false, ErrorCode.DIV0, null, mat1, ranges)).toBe(ErrorCode.DIV0);
            expect(CHOOSE(5, 2, "a", false, ErrorCode.DIV0, null, mat1, ranges)).toBeNull();
            expect(CHOOSE(6, 2, "a", false, ErrorCode.DIV0, null, mat1, ranges)).toEqual(mat1);
            expect(CHOOSE(7, 2, "a", false, ErrorCode.DIV0, null, mat1, ranges)).toBe(ranges);
        });
        it("should return #VALUE! for invalid index", () => {
            expect(CHOOSE(-1, 2, "a", false, ErrorCode.DIV0, null, mat1, ranges)).toBe(ErrorCode.VALUE);
            expect(CHOOSE(0, 2, "a", false, ErrorCode.DIV0, null, mat1, ranges)).toBe(ErrorCode.VALUE);
            expect(CHOOSE(8, 2, "a", false, ErrorCode.DIV0, null, mat1, ranges)).toBe(ErrorCode.VALUE);
            expect(CHOOSE(9, 2, "a", false, ErrorCode.DIV0, null, mat1, ranges)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("COLUMN", { targetAddress: "E6" }, COLUMN => {
        it("should return the column index", () => {
            expect(COLUMN(r3d("0:0!B3:D5"))).toBe(2);
            expect(COLUMN()).toBe(5);
        });
    });

    moduleTester.testFunction("COLUMNS", { toOperand: 0 }, COLUMNS => {
        it("should return the width of the operand", () => {
            expect(COLUMNS(42)).toBe(1);
            expect(COLUMNS(mat([[1, 2, 3], [4, 5, 6]]))).toBe(3);
            expect(COLUMNS(r3d("0:0!B2:C4"))).toBe(2);
        });
        it("should throw errors for complex ranges", () => {
            expect(COLUMNS(r3d("0:1!B2:C4"))).toBe(ErrorCode.VALUE);
            expect(COLUMNS(r3da("0:0!B2:C4 0:0!B2:C4"))).toBe(ErrorCode.REF);
        });
    });

    moduleTester.testFunction("FORMULATEXT", FORMULATEXT => {
        it("should return the formula expression for normal formula cells", () => {
            expect(FORMULATEXT(r3d("0!B2"))).toBe("=SUMME(1;2)");
        });
        it("should return the formula expression for shared formulas", () => {
            expect(FORMULATEXT(r3d("0!A3"))).toBe("=A2");
            expect(FORMULATEXT(r3d("0!B3"))).toBe("=B2");
        });
        it("should return the formula expression for matrix formulas", () => {
            expect(FORMULATEXT(r3d("0!A4"))).toBe("{=A3:B3*2}");
            expect(FORMULATEXT(r3d("0!B4"))).toBe("{=A3:B3*2}");
        });
        it("should return the formula expression for dynamic matrix formulas", () => {
            expect(FORMULATEXT(r3d("0!A5"))).toBe("=A3:B3*2");
            expect(FORMULATEXT(r3d("0!B5"))).toBe("=A3:B3*2");
        });
        it("should return #N/A for simple cells", () => {
            expect(FORMULATEXT(r3d("0!A2"))).toBe(ErrorCode.NA);
            expect(FORMULATEXT(r3d("0!C2"))).toBe(ErrorCode.NA);
            expect(FORMULATEXT(r3d("0!IV99"))).toBe(ErrorCode.NA);
        });
        it("should use the start address of a range", () => {
            expect(FORMULATEXT(r3d("0!B2:C3"))).toBe("=SUMME(1;2)");
        });
    });

    moduleTester.testFunction("HLOOKUP", { refSheet: 1, targetAddress: "A4", toOperand: 1 }, HLOOKUP => {

        const stringMatrix = mat([["Achsen", "Getriebe", "Schrauben"], [4, 4, 9], [5, 7, 10], [6, 8, 11]]);
        const numberMatrix = mat([[1, 2, 3], ["a", "b", "c"], ["d", "e", "f"]]);

        it("should return the result of the horizontal search", () => {
            expect(HLOOKUP("Achsen", stringMatrix, 2, true)).toBe(4);
            expect(HLOOKUP("Getriebe", stringMatrix, 3, false)).toBe(7);
            expect(HLOOKUP("B", stringMatrix, 3, true)).toBe(5);
            expect(HLOOKUP("Schrauben", stringMatrix, 4)).toBe(11);

            expect(HLOOKUP(3, numberMatrix, 2, true)).toBe("c");
            expect(HLOOKUP(2.5, numberMatrix, 2, true)).toBe("b");
            expect(HLOOKUP(4, numberMatrix, 2, true)).toBe("c");

            expect(HLOOKUP("A", stringMatrix, 3, true)).toBe(ErrorCode.NA);
            expect(HLOOKUP("Achsen", stringMatrix, 0, true)).toBe(ErrorCode.NA);
            expect(HLOOKUP("Achsen", stringMatrix, 100, true)).toBe(ErrorCode.NA);
        });
        it("should return the result of the horizontal search with wildcards", () => {
            expect(HLOOKUP("Schra?ben", stringMatrix, 4)).toBe(8);
            expect(HLOOKUP("Schra?ben", stringMatrix, 4, false)).toBe(11);
            expect(HLOOKUP("G*e", stringMatrix, 4, false)).toBe(8);
            expect(HLOOKUP("G?e", stringMatrix, 4, false)).toBe(ErrorCode.NA);
        });

        it("should return the result of the horizontal search with references", () => {
            expect(HLOOKUP(4.19, r3d("1:1!A1:E3"), 3)).toBe("Orange");
            expect(HLOOKUP(5.75, r3d("1:1!A1:E3"), 3)).toBe("Gelb");
            expect(HLOOKUP(6.00, r3d("1:1!A1:E3"), 3)).toBe("Gruen");
            expect(HLOOKUP(7.66, r3d("1:1!A1:E3"), 3)).toBe("Blau");
        });
    });

    moduleTester.testFunction("INDEX", { refSheet: 1, targetAddress: "Z26", toOperand: 0 }, INDEX => {

        describe("with scalar", () => {
            it("should return result", () => {
                expect(INDEX(42, 1)).toBe(42);
                expect(INDEX(42, 0)).toBe(42);
                expect(INDEX(42, 1, 1)).toBe(42);
                expect(INDEX(42, 1, 0)).toBe(42);
                expect(INDEX(42, 0, 1)).toBe(42);
                expect(INDEX(42, 0, 0)).toBe(42);
                expect(INDEX(42, 1, 1, 1)).toBe(42);
                expect(INDEX(42, 0, 0, 1)).toBe(42);
            });
            it("should return REF error", () => {
                expect(INDEX(42, -1)).toBe(ErrorCode.REF);
                expect(INDEX(42, 2)).toBe(ErrorCode.REF);
                expect(INDEX(42, 1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(42, 1, 2)).toBe(ErrorCode.REF);
                expect(INDEX(42, 2, 1)).toBe(ErrorCode.REF);
                expect(INDEX(42, -1, 1)).toBe(ErrorCode.REF);
                expect(INDEX(42, 1, 1, 0)).toBe(ErrorCode.REF);
                expect(INDEX(42, 1, 1, 2)).toBe(ErrorCode.REF);
            });
        });

        describe("with 1x1 matrix", () => {
            const m1 = mat([[2]]);
            it("should return result", () => {
                expect(INDEX(m1, 1)).toBe(2);
                expect(INDEX(m1, 0)).toEqual(m1);
                expect(INDEX(m1, 1, 1)).toBe(2);
                expect(INDEX(m1, 1, 0)).toEqual(m1);
                expect(INDEX(m1, 0, 1)).toEqual(m1);
                expect(INDEX(m1, 0, 0)).toEqual(m1);
                expect(INDEX(m1, 1, 1, 1)).toBe(2);
                expect(INDEX(m1, 0, 0, 1)).toEqual(m1);
            });
            it("should return REF error", () => {
                expect(INDEX(m1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 2)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, 2)).toBe(ErrorCode.REF);
                expect(INDEX(m1, -1, 1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 2, 1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, 1, 0)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, 1, 2)).toBe(ErrorCode.REF);
            });
        });

        describe("with row vector matrix", () => {
            const m1 = mat([[2, 4]]);
            it("should return result", () => {
                expect(INDEX(m1, 1)).toBe(2);
                expect(INDEX(m1, 2)).toBe(4);
                expect(INDEX(m1, 0)).toEqual(m1);
                expect(INDEX(m1, 1, 1)).toBe(2);
                expect(INDEX(m1, 1, 2)).toBe(4);
                expect(INDEX(m1, 1, 0)).toEqual(m1);
                expect(INDEX(m1, 0, 1)).toEqual(mat([[2]]));
                expect(INDEX(m1, 0, 2)).toEqual(mat([[4]]));
                expect(INDEX(m1, 0, 0)).toEqual(m1);
                expect(INDEX(m1, 1, 2, 1)).toBe(4);
                expect(INDEX(m1, 0, 0, 1)).toEqual(m1);
            });
            it("should return REF error", () => {
                expect(INDEX(m1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 3)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, 3)).toBe(ErrorCode.REF);
                expect(INDEX(m1, -1, 1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 2, 1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, 2, 0)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, 2, 2)).toBe(ErrorCode.REF);
            });
        });

        describe("with column vector matrix", () => {
            const m1 = mat([[2], [4]]);
            it("should return result", () => {
                expect(INDEX(m1, 1)).toEqual(mat([[2]]));
                expect(INDEX(m1, 2)).toEqual(mat([[4]]));
                expect(INDEX(m1, 0)).toEqual(m1);
                expect(INDEX(m1, 1, 1)).toBe(2);
                expect(INDEX(m1, 2, 1)).toBe(4);
                expect(INDEX(m1, 0, 1)).toEqual(m1);
                expect(INDEX(m1, 1, 0)).toEqual(mat([[2]]));
                expect(INDEX(m1, 2, 0)).toEqual(mat([[4]]));
                expect(INDEX(m1, 0, 0)).toEqual(m1);
                expect(INDEX(m1, 2, 1, 1)).toBe(4);
                expect(INDEX(m1, 0, 0, 1)).toEqual(m1);
            });
            it("should return REF error", () => {
                expect(INDEX(m1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 3)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, 2)).toBe(ErrorCode.REF);
                expect(INDEX(m1, -1, 1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 3, 1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 2, 1, 0)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 2, 1, 2)).toBe(ErrorCode.REF);
            });
        });

        describe("with 2x2 matrix", () => {
            const m1 = mat([[2, 4], [6, 8]]);
            it("should return result", () => {
                expect(INDEX(m1, 1)).toEqual(mat([[2, 4]]));
                expect(INDEX(m1, 2)).toEqual(mat([[6, 8]]));
                expect(INDEX(m1, 0)).toEqual(m1);
                expect(INDEX(m1, 1, 1)).toBe(2);
                expect(INDEX(m1, 1, 2)).toBe(4);
                expect(INDEX(m1, 1, 0)).toEqual(mat([[2, 4]]));
                expect(INDEX(m1, 2, 1)).toBe(6);
                expect(INDEX(m1, 2, 2)).toBe(8);
                expect(INDEX(m1, 2, 0)).toEqual(mat([[6, 8]]));
                expect(INDEX(m1, 0, 1)).toEqual(mat([[2], [6]]));
                expect(INDEX(m1, 0, 2)).toEqual(mat([[4], [8]]));
                expect(INDEX(m1, 0, 0)).toEqual(m1);
                expect(INDEX(m1, 1, 2, 1)).toBe(4);
                expect(INDEX(m1, 0, 0, 1)).toEqual(m1);
            });
            it("should return REF error", () => {
                expect(INDEX(m1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 3)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, 3)).toBe(ErrorCode.REF);
                expect(INDEX(m1, -1, 1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 3, 1)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, 2, 0)).toBe(ErrorCode.REF);
                expect(INDEX(m1, 1, 2, 2)).toBe(ErrorCode.REF);
            });
        });

        describe("with 1x1 range", () => {
            const r1 = r3d("1:1!B3:B3");
            it("should return result", () => {
                expect(INDEX(r1, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 0)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 1, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 1, 0)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 0, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 0, 0)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 1, 1, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 0, 0, 1)).toStringifyTo("1:1!B3:B3");
            });
            it("should return REF error", () => {
                expect(INDEX(r1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 2)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, 2)).toBe(ErrorCode.REF);
                expect(INDEX(r1, -1, 1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 2, 1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, 1, 0)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, 1, 2)).toBe(ErrorCode.REF);
            });
        });

        describe("with row vector range", () => {
            const r1 = r3d("1:1!B3:C3");
            it("should return result", () => {
                expect(INDEX(r1, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 2)).toStringifyTo("1:1!C3:C3");
                expect(INDEX(r1, 0)).toStringifyTo("1:1!B3:C3");
                expect(INDEX(r1, 1, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 1, 2)).toStringifyTo("1:1!C3:C3");
                expect(INDEX(r1, 1, 0)).toStringifyTo("1:1!B3:C3");
                expect(INDEX(r1, 0, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 0, 2)).toStringifyTo("1:1!C3:C3");
                expect(INDEX(r1, 0, 0)).toStringifyTo("1:1!B3:C3");
                expect(INDEX(r1, 1, 2, 1)).toStringifyTo("1:1!C3:C3");
                expect(INDEX(r1, 0, 0, 1)).toStringifyTo("1:1!B3:C3");
            });
            it("should return REF error", () => {
                expect(INDEX(r1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 3)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, 3)).toBe(ErrorCode.REF);
                expect(INDEX(r1, -1, 1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 2, 1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, 2, 0)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, 2, 2)).toBe(ErrorCode.REF);
            });
        });

        describe("with column vector range", () => {
            const r1 = r3d("1:1!B3:B4");
            it("should return result", () => {
                expect(INDEX(r1, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 2)).toStringifyTo("1:1!B4:B4");
                expect(INDEX(r1, 0)).toStringifyTo("1:1!B3:B4");
                expect(INDEX(r1, 1, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 2, 1)).toStringifyTo("1:1!B4:B4");
                expect(INDEX(r1, 0, 1)).toStringifyTo("1:1!B3:B4");
                expect(INDEX(r1, 1, 0)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 2, 0)).toStringifyTo("1:1!B4:B4");
                expect(INDEX(r1, 0, 0)).toStringifyTo("1:1!B3:B4");
                expect(INDEX(r1, 2, 1, 1)).toStringifyTo("1:1!B4:B4");
                expect(INDEX(r1, 0, 0, 1)).toStringifyTo("1:1!B3:B4");
            });
            it("should return REF error", () => {
                expect(INDEX(r1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 3)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, 2)).toBe(ErrorCode.REF);
                expect(INDEX(r1, -1, 1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 3, 1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 2, 1, 0)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 2, 1, 2)).toBe(ErrorCode.REF);
            });
        });

        describe("with 2x2 range", () => {
            const r1 = r3d("1:1!B3:C4");
            it("should return result", () => {
                expect(INDEX(r1, 1)).toStringifyTo("1:1!B3:C3");
                expect(INDEX(r1, 2)).toStringifyTo("1:1!B4:C4");
                expect(INDEX(r1, 0)).toStringifyTo("1:1!B3:C4");
                expect(INDEX(r1, 1, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(r1, 1, 2)).toStringifyTo("1:1!C3:C3");
                expect(INDEX(r1, 1, 0)).toStringifyTo("1:1!B3:C3");
                expect(INDEX(r1, 2, 1)).toStringifyTo("1:1!B4:B4");
                expect(INDEX(r1, 2, 2)).toStringifyTo("1:1!C4:C4");
                expect(INDEX(r1, 2, 0)).toStringifyTo("1:1!B4:C4");
                expect(INDEX(r1, 0, 1)).toStringifyTo("1:1!B3:B4");
                expect(INDEX(r1, 0, 2)).toStringifyTo("1:1!C3:C4");
                expect(INDEX(r1, 0, 0)).toStringifyTo("1:1!B3:C4");
                expect(INDEX(r1, 1, 2, 1)).toStringifyTo("1:1!C3:C3");
                expect(INDEX(r1, 0, 0, 1)).toStringifyTo("1:1!B3:C4");
            });
            it("should return REF error", () => {
                expect(INDEX(r1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 3)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, -1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, 3)).toBe(ErrorCode.REF);
                expect(INDEX(r1, -1, 1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 3, 1)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, 2, 0)).toBe(ErrorCode.REF);
                expect(INDEX(r1, 1, 2, 2)).toBe(ErrorCode.REF);
            });
        });

        describe("with range list", () => {
            const ra1 = r3da("1:1!B3:C4 2:2!C4:E6");
            it("should return result", () => {
                expect(INDEX(ra1, 1, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(ra1, 1, 1, 1)).toStringifyTo("1:1!B3:B3");
                expect(INDEX(ra1, 1, 1, 2)).toStringifyTo("2:2!C4:C4");
            });
            it("should return REF error", () => {
                expect(INDEX(ra1, 1, 1, 0)).toBe(ErrorCode.REF);
                expect(INDEX(ra1, 1, 1, 3)).toBe(ErrorCode.REF);
            });
        });

        describe("with multi-sheet range", () => {
            const r1 = r3d("1:2!B3:C4");
            it("should return VALUE error", () => {
                expect(INDEX(r1, 1, 1)).toBe(ErrorCode.VALUE);
            });
        });
    });

    moduleTester.testFunction("INDIRECT", { refSheet: 3, targetAddress: "B2" }, INDIRECT => {
        it("should return the result for A1 notation", () => {
            expect(INDIRECT("A1")).toEqual(r3d("3:3!A1:A1"));
            expect(INDIRECT("A2", true)).toEqual(r3d("3:3!A2:A2"));
            expect(INDIRECT("A:A")).toEqual(r3d("3:3!A1:A1048576"));
            expect(INDIRECT("1:1")).toEqual(r3d("3:3!A1:XFD1"));
            expect(INDIRECT("Sheet2!A3")).toEqual(r3d("1:1!A3:A3"));
            expect(INDIRECT("A2:B3")).toEqual(r3d("3:3!A2:B3"));
            expect(INDIRECT("name1")).toEqual(r3d("3:3!C2:D3"));
        });
        it("should return the result for R1C1 notation", () => {
            expect(INDIRECT("Z2S1", false)).toEqual(r3d("3:3!A2:A2"));
            expect(INDIRECT("ZS", false)).toEqual(r3d("3:3!B2:B2"));
            expect(INDIRECT("Z[1]S[-1]", false)).toEqual(r3d("3:3!A3:A3"));
            expect(INDIRECT("Z1:Z2", false)).toEqual(r3d("3:3!A1:XFD2"));
            expect(INDIRECT("Z", false)).toEqual(r3d("3:3!A2:XFD2"));
            expect(INDIRECT("S1:S2", false)).toEqual(r3d("3:3!A1:B1048576"));
            expect(INDIRECT("S", false)).toEqual(r3d("3:3!B1:B1048576"));
            expect(INDIRECT("Sheet2!Z3S1", false)).toEqual(r3d("1:1!A3:A3"));
            expect(INDIRECT("Z2S1:Z3S2", false)).toEqual(r3d("3:3!A2:B3"));
            expect(INDIRECT("name1", false)).toEqual(r3d("3:3!C2:D3"));
        });
        it("should throw #REF! error for invalid parameter", () => {
            expect(INDIRECT("1", true)).toBe(ErrorCode.REF);
            expect(INDIRECT("1", false)).toBe(ErrorCode.REF);
            expect(INDIRECT("A", true)).toBe(ErrorCode.REF);
            expect(INDIRECT("A", false)).toBe(ErrorCode.REF);
            expect(INDIRECT("", true)).toBe(ErrorCode.REF);
            expect(INDIRECT("", false)).toBe(ErrorCode.REF);
            expect(INDIRECT("Z2S1", true)).toBe(ErrorCode.REF);
            expect(INDIRECT("A1", false)).toBe(ErrorCode.REF);
            expect(INDIRECT("InvalidSheet!A1")).toBe(ErrorCode.REF);
            expect(INDIRECT("Sheet1:Sheet2!A1")).toBe(ErrorCode.REF);
        });
    });

    moduleTester.testFunction("LOOKUP", { toOperand: [1, 2] }, LOOKUP => {

        it("should fail for blank ranges", () => {
            expect(LOOKUP(1, r3d("4:4!A1:B1"))).toBe(ErrorCode.NA);
        });

        // result from lookup range
        describe("(matrix mode)", () => {
            it("should find entry in ascending data", () => {
                const lookup = mat([[2, 4, 6, "bb", "DD", true]]);
                expect(LOOKUP(1, lookup)).toBe(ErrorCode.NA);
                expect(LOOKUP(2, lookup)).toBe(2);
                expect(LOOKUP(3, lookup)).toBe(2);
                expect(LOOKUP(4, lookup)).toBe(4);
                expect(LOOKUP(5, lookup)).toBe(4);
                expect(LOOKUP(6, lookup)).toBe(6);
                expect(LOOKUP(7, lookup)).toBe(6);
                expect(LOOKUP(1e100, lookup)).toBe(6);
                expect(LOOKUP("a", lookup)).toBe(6);
                expect(LOOKUP("b", lookup)).toBe(6);
                expect(LOOKUP("B", lookup)).toBe(6);
                expect(LOOKUP("ba", lookup)).toBe(6);
                expect(LOOKUP("bb", lookup)).toBe("bb");
                expect(LOOKUP("BB", lookup)).toBe("bb");
                expect(LOOKUP("c", lookup)).toBe("bb");
                expect(LOOKUP("c", lookup)).toBe("bb");
                expect(LOOKUP("d", lookup)).toBe("bb");
                expect(LOOKUP("dc", lookup)).toBe("bb");
                expect(LOOKUP("dd", lookup)).toBe("DD");
                expect(LOOKUP("DD", lookup)).toBe("DD");
                expect(LOOKUP("zz", lookup)).toBe("DD");
                expect(LOOKUP(false, lookup)).toBe("DD");
                expect(LOOKUP(true, lookup)).toBeTrue();
                expect(LOOKUP(80, r3d("4:4!K1:T1"))).toBe(70);
                expect(LOOKUP(80, r3d("4:4!A1:AD1"))).toBe(70);
            });
            it("should pick value from last column", () => {
                expect(LOOKUP(30, mat([[10, 0, 1], [20, 0, 2], [30, 0, 3], [40, 0, 4], [50, 0, 5]]))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!A100:C104"))).toBe(3);
            });
            it("should pick value from last row", () => {
                expect(LOOKUP(30, mat([[10, 20, 30, 40, 50], [0, 0, 0, 0, 0], [1, 2, 3, 4, 5]]))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!E100:I102"))).toBe(3);
            });
            it("should prefer columns in square range", () => {
                expect(LOOKUP(20, mat([[10, 20, 30], [20, 0, 2], [30, 0, 0]]))).toBe(2);
                expect(LOOKUP(20, r3d("4:4!K100:M102"))).toBe(2);
            });
            it("should accept scalar value as lookup source", () => {
                expect(LOOKUP(30, 30)).toBe(30);
                expect(LOOKUP(30, ErrorCode.NULL)).toBe(ErrorCode.NULL);
            });
            it("should match last entry", () => {
                expect(LOOKUP(20, mat([[10, 20, 20, 20, 30], [1, 2, 3, 4, 5]]))).toBe(4);
            });
            it("should use binary search algorithm", () => {
                expect(LOOKUP(80, mat([[10, 99, 30, 40, 50, 60, 70]]))).toBe(70);
                expect(LOOKUP(80, mat([[10, 20, 30, 99, 50, 60, 70]]))).toBe(30);
                expect(LOOKUP(80, mat([[10, 20, 30, 40, 50, 99, 70]]))).toBe(50);
                expect(LOOKUP(80, mat([[10, 99, 30, 99, 50, 60, 70]]))).toBe(10);
                expect(LOOKUP(80, r3d("4:4!K3:T3"))).toBe(70);
                expect(LOOKUP(80, r3d("4:4!K4:T4"))).toBe(30);
            });
        });

        // result from explicit result range
        describe("(result mode)", () => {
            it("should pick value from result", () => {
                expect(LOOKUP(30, mat([[10, 20, 30, 40, 50]]), mat([[1, 2, 3, 4, 5]]))).toBe(3);
                expect(LOOKUP(30, mat([[10, 20, 30, 40, 50]]), mat([[1], [2], [3], [4], [5]]))).toBe(3);
                expect(LOOKUP(30, mat([[10], [20], [30], [40], [50]]), mat([[1, 2, 3, 4, 5]]))).toBe(3);
                expect(LOOKUP(30, mat([[10], [20], [30], [40], [50]]), mat([[1], [2], [3], [4], [5]]))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!A100:A104"), r3d("4:4!C100:C104"))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!A100:A104"), r3d("4:4!E102:I102"))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!E100:I100"), r3d("4:4!E102:I102"))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!E100:I100"), r3d("4:4!C100:C104"))).toBe(3);
            });
            it("should fail for two-dimensional results", () => {
                expect(LOOKUP(20, mat([[10, 20]]), mat([[1, 2], [3, 4]]))).toBe(ErrorCode.NA);
                expect(LOOKUP(20, r3d("4:4!A100:A101"), r3d("4:4!B100:C101"))).toBe(ErrorCode.NA);
            });
            it("should fail for insufficient matrix size", () => {
                expect(LOOKUP(30, mat([[10, 20, 30, 40, 50]]), mat([[1, 2]]))).toBe(ErrorCode.NA);
                expect(LOOKUP(30, mat([[10, 20, 30, 40, 50]]), mat([[1], [2]]))).toBe(ErrorCode.NA);
                expect(LOOKUP(30, mat([[10, 20, 30, 40, 50]]), mat([[1]]))).toBe(ErrorCode.NA);
            });
            it("should accept insufficient reference size", () => {
                expect(LOOKUP(30, r3d("4:4!A100:A104"), r3d("4:4!C100:C101"))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!A100:A104"), r3d("4:4!E102:F102"))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!E100:I100"), r3d("4:4!E102:F102"))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!E100:I100"), r3d("4:4!C100:C101"))).toBe(3);
            });
            it("should should pick correct direction for single cell", () => {
                expect(LOOKUP(30, r3d("4:4!A100:A104"), r3d("4:4!C100"))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!E100:I100"), r3d("4:4!E102"))).toBe(3);
            });
            it("should search in columns", () => {
                expect(LOOKUP(30, mat([[10, 20], [20, 30], [30, 40], [40, 50], [50, 60]]), r3d("4:4!C100:C104"))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!A100:B104"), r3d("4:4!C100:C104"))).toBe(3);
            });
            it("should search in rows", () => {
                expect(LOOKUP(30, mat([[10, 20, 30, 40, 50], [20, 30, 40, 50, 60]]), r3d("4:4!C100:C104"))).toBe(3);
                expect(LOOKUP(30, r3d("4:4!E100:I101"), r3d("4:4!C100:C104"))).toBe(3);
            });
            it("should accept scalar value as lookup source", () => {
                expect(LOOKUP(30, 30)).toBe(30);
                expect(LOOKUP(30, ErrorCode.NULL)).toBe(ErrorCode.NULL);
            });
        });
    });

    moduleTester.testFunction("MATCH", { toOperand: 1 }, MATCH => {

        it("should reject 2D ranges", () => {
            expect(MATCH(1, r3d("4:4!K1:L2"), 1)).toBe(ErrorCode.NA);
            expect(MATCH(1, r3d("4:4!K1:L2"), 0)).toBe(ErrorCode.NA);
            expect(MATCH(1, r3d("4:4!K1:L2"), -1)).toBe(ErrorCode.NA);
        });
        it("should fail for blank ranges", () => {
            expect(MATCH(1, r3d("4:4!A1:B1"), 1)).toBe(ErrorCode.NA);
            expect(MATCH(1, r3d("4:4!A1:B1"), 0)).toBe(ErrorCode.NA);
            expect(MATCH(1, r3d("4:4!A1:B1"), -1)).toBe(ErrorCode.NA);
        });

        // exact match
        describe("(search mode 0)", () => {
            it("should find exact values", () => {
                expect(MATCH(0, mat([[false, true, "0", "1", "", 0, 1]]), 0)).toBe(6);
                expect(MATCH(1, mat([[false, true, "0", "1", "", 0, 1]]), 0)).toBe(7);
                expect(MATCH(2, mat([[false, true, "0", "1", "", 0, 1]]), 0)).toBe(ErrorCode.NA);
                expect(MATCH(null, mat([[false, true, "0", "1", "", 0, 1]]), 0)).toBe(6);
                expect(MATCH(false, mat([[0, 1, "false", "true", "", false, true]]), 0)).toBe(6);
                expect(MATCH(true, mat([[0, 1, "false", "true", "", false, true]]), 0)).toBe(7);
                expect(MATCH("0", mat([[0, 1, false, true, "", "0", "1"]]), 0)).toBe(6);
                expect(MATCH("1", mat([[0, 1, false, true, "", "0", "1"]]), 0)).toBe(7);
                expect(MATCH("2", mat([[0, 1, false, true, "", "0", "1"]]), 0)).toBe(ErrorCode.NA);
                expect(MATCH("", mat([[0, 1, false, true, "", "0", "1"]]), 0)).toBe(5);
            });
            it("should ignore error codes", () => {
                expect(MATCH(1, mat([[ErrorCode.NULL, 1]]), 0)).toBe(2);
            });
            it("should find strings ignoring case", () => {
                expect(MATCH("B", mat([["a", "b", "B", "c"]]), 0)).toBe(2);
            });
            it("should use search patterns", () => {
                const data = mat([["cba", "bac", "abc", "acb", 10, "10"]]);
                expect(MATCH("a*", data, 0)).toBe(3);
                expect(MATCH("B*", data, 0)).toBe(2);
                expect(MATCH("c*", data, 0)).toBe(1);
                expect(MATCH("?c?", data, 0)).toBe(4);
                expect(MATCH("?c", data, 0)).toBe(ErrorCode.NA);
                expect(MATCH("1*", data, 0)).toBe(6);
            });
            it("should match first entry", () => {
                expect(MATCH(2, mat([[1, 2, 2, 2, 3]]), 0)).toBe(2);
            });
        });

        // binary search in ascending data
        describe("(search mode 1)", () => {
            it("should find entry", () => {
                const lookup = mat([[2, 4, 6, "bb", "DD", true]]);
                expect(MATCH(1, lookup, 1)).toBe(ErrorCode.NA);
                expect(MATCH(2, lookup, 1)).toBe(1);
                expect(MATCH(3, lookup, 1)).toBe(1);
                expect(MATCH(4, lookup, 1)).toBe(2);
                expect(MATCH(5, lookup, 1)).toBe(2);
                expect(MATCH(6, lookup, 1)).toBe(3);
                expect(MATCH(7, lookup, 1)).toBe(3);
                expect(MATCH(1e100, lookup, 1)).toBe(3);
                expect(MATCH("a", lookup, 1)).toBe(3);
                expect(MATCH("b", lookup, 1)).toBe(3);
                expect(MATCH("B", lookup, 1)).toBe(3);
                expect(MATCH("ba", lookup, 1)).toBe(3);
                expect(MATCH("bb", lookup, 1)).toBe(4);
                expect(MATCH("BB", lookup, 1)).toBe(4);
                expect(MATCH("c", lookup, 1)).toBe(4);
                expect(MATCH("c", lookup, 1)).toBe(4);
                expect(MATCH("d", lookup, 1)).toBe(4);
                expect(MATCH("dc", lookup, 1)).toBe(4);
                expect(MATCH("dd", lookup, 1)).toBe(5);
                expect(MATCH("DD", lookup, 1)).toBe(5);
                expect(MATCH("zz", lookup, 1)).toBe(5);
                expect(MATCH(false, lookup, 1)).toBe(5);
                expect(MATCH(true, lookup, 1)).toBe(6);
                expect(MATCH(80, r3d("4:4!K1:T1"), 1)).toBe(10);
                expect(MATCH(80, r3d("4:4!A1:AD1"), 1)).toBe(20);
            });
            it("should accept any positive number", () => {
                expect(MATCH(50, mat([[20, 40, 60]]), 1e100)).toBe(2);
                expect(MATCH(50, mat([[20, 40, 60]]), 1e-99)).toBe(2);
            });
            it("should default to ascending search", () => {
                expect(MATCH(50, mat([[20, 40, 60]]))).toBe(2);
            });
            it("should match last entry", () => {
                expect(MATCH(20, mat([[10, 20, 20, 20, 30]]), 1)).toBe(4);
            });
            it("should use binary search algorithm", () => {
                expect(MATCH(80, mat([[10, 99, 30, 40, 50, 60, 70]]), 1)).toBe(7);
                expect(MATCH(80, mat([[10, 20, 30, 99, 50, 60, 70]]), 1)).toBe(3);
                expect(MATCH(80, mat([[10, 20, 30, 40, 50, 99, 70]]), 1)).toBe(5);
                expect(MATCH(80, mat([[10, 99, 30, 99, 50, 60, 70]]), 1)).toBe(1);
                expect(MATCH(80, r3d("4:4!K3:T3"), 1)).toBe(10);
                expect(MATCH(80, r3d("4:4!K4:T4"), 1)).toBe(3);
            });
        });

        // binary search in descending data
        describe("(search mode -1)", () => {
            it("should find entry", () => {
                const data = mat([[false, "DD", "bb", 6, 4, 2]]);
                expect(MATCH(1, data, -1)).toBe(6);
                expect(MATCH(2, data, -1)).toBe(6);
                expect(MATCH(3, data, -1)).toBe(5);
                expect(MATCH(4, data, -1)).toBe(5);
                expect(MATCH(5, data, -1)).toBe(4);
                expect(MATCH(6, data, -1)).toBe(4);
                expect(MATCH(7, data, -1)).toBe(3);
                expect(MATCH(1e100, data, -1)).toBe(3);
                expect(MATCH("a", data, -1)).toBe(3);
                expect(MATCH("b", data, -1)).toBe(3);
                expect(MATCH("B", data, -1)).toBe(3);
                expect(MATCH("ba", data, -1)).toBe(3);
                expect(MATCH("bb", data, -1)).toBe(3);
                expect(MATCH("BB", data, -1)).toBe(3);
                expect(MATCH("c", data, -1)).toBe(2);
                expect(MATCH("c", data, -1)).toBe(2);
                expect(MATCH("d", data, -1)).toBe(2);
                expect(MATCH("dc", data, -1)).toBe(2);
                expect(MATCH("dd", data, -1)).toBe(2);
                expect(MATCH("DD", data, -1)).toBe(2);
                expect(MATCH("zz", data, -1)).toBe(1);
                expect(MATCH(false, data, -1)).toBe(1);
                expect(MATCH(true, data, -1)).toBe(ErrorCode.NA);
                expect(MATCH(0, r3d("4:4!K2:T2"), -1)).toBe(10);
                expect(MATCH(0, r3d("4:4!A2:AD2"), -1)).toBe(20);
            });
            it("should accept any positive number", () => {
                expect(MATCH(50, mat([[20, 40, 60]]), 1e100)).toBe(2);
                expect(MATCH(50, mat([[20, 40, 60]]), 1e-99)).toBe(2);
            });
            it("should match last entry", () => {
                expect(MATCH(20, mat([[30, 20, 20, 20, 10]]), -1)).toBe(4);
            });
            it("should use binary search algorithm", () => {
                expect(MATCH(0, mat([[70, -99, 50,  40, 30,  20, 10]]), -1)).toBe(7);
                expect(MATCH(0, mat([[70,  60, 50, -99, 30,  20, 10]]), -1)).toBe(3);
                expect(MATCH(0, mat([[70,  60, 50,  40, 30, -99, 10]]), -1)).toBe(5);
                expect(MATCH(0, mat([[70, -99, 50, -99, 30,  20, 10]]), -1)).toBe(1);
                expect(MATCH(0, r3d("4:4!K5:T5"), -1)).toBe(10);
                expect(MATCH(0, r3d("4:4!K6:T6"), -1)).toBe(3);
            });
        });
    });

    moduleTester.testFunction("ROW", { targetAddress: "E6" }, ROW => {
        it("should return the row index", () => {
            expect(ROW(r3d("0:0!B3:D5"))).toBe(3);
            expect(ROW()).toBe(6);
        });
    });

    moduleTester.testFunction("ROWS", { toOperand: 0 }, ROWS => {
        it("should return the width of the operand", () => {
            expect(ROWS(42)).toBe(1);
            expect(ROWS(mat([[1, 2, 3], [4, 5, 6]]))).toBe(2);
            expect(ROWS(r3d("0:0!B2:C4"))).toBe(3);
        });
        it("should throw errors for complex ranges", () => {
            expect(ROWS(r3d("0:1!B2:C4"))).toBe(ErrorCode.VALUE);
            expect(ROWS(r3da("0:0!B2:C4 0:0!B2:C4"))).toBe(ErrorCode.REF);
        });
    });

    moduleTester.testFunction("SWITCH", { toOperand: [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15] }, SWITCH => {
        const mat1 = mat([[1]]);
        const ranges = r3da("0:0!A1:A1");
        it("should return selected parameter", () => {
            expect(SWITCH(42, 42, 2, 43, "a", 44, false, 45, ErrorCode.DIV0, 46, null, 47, mat1, 41, ranges)).toBe(2);
            expect(SWITCH(43, 42, 2, 43, "a", 44, false, 45, ErrorCode.DIV0, 46, null, 47, mat1, 41, ranges)).toBe("a");
            expect(SWITCH(44, 42, 2, 43, "a", 44, false, 45, ErrorCode.DIV0, 46, null, 47, mat1, 41, ranges)).toBeFalse();
            expect(SWITCH(45, 42, 2, 43, "a", 44, false, 45, ErrorCode.DIV0, 46, null, 47, mat1, 41, ranges)).toBe(ErrorCode.DIV0);
            expect(SWITCH(46, 42, 2, 43, "a", 44, false, 45, ErrorCode.DIV0, 46, null, 47, mat1, 41, ranges)).toBeNull();
            expect(SWITCH(47, 42, 2, 43, "a", 44, false, 45, ErrorCode.DIV0, 46, null, 47, mat1, 41, ranges)).toEqual(mat1);
            expect(SWITCH(41, 42, 2, 43, "a", 44, false, 45, ErrorCode.DIV0, 46, null, 47, mat1, 41, ranges)).toBe(ranges);
        });
        it("should match different data types", () => {
            expect(SWITCH("b", "a", 2, "b", ranges, true, mat1)).toBe(ranges);
            expect(SWITCH("B", "a", 2, "b", ranges, true, mat1)).toBe(ranges);
            expect(SWITCH(true, "a", 2, "b", ranges, true, mat1)).toEqual(mat1);
        });
        it("should return default value", () => {
            expect(SWITCH("a", 1, 2, ranges)).toBe(ranges);
        });
        it("should return #N/A for invalid index without default value", () => {
            expect(SWITCH(40, 42, 2, 43, "a", 44, false, 45, ErrorCode.DIV0, 46, null, 47, mat1, 41, ranges)).toBe(ErrorCode.NA);
            expect(SWITCH("1", 1, 2)).toBe(ErrorCode.NA);
            expect(SWITCH(true, 1, 2)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("VLOOKUP", { refSheet: 2, targetAddress: "D1", toOperand: 1 }, VLOOKUP => {

        const stringMatrix = mat([["Achsen", 4, 5, 6], ["Getriebe", 4, 7, 8], ["Schrauben", 9, 10, 11]]);
        const numberMatrix = mat([[1, "a", "d"], [2, "b", "e"], [3, "c", "f"]]);

        it("should return the result of the vertical search", () => {
            expect(VLOOKUP("Achsen", stringMatrix, 2, true)).toBe(4);
            expect(VLOOKUP("Getriebe", stringMatrix, 3, false)).toBe(7);
            expect(VLOOKUP("B", stringMatrix, 3, true)).toBe(5);
            expect(VLOOKUP("Schrauben", stringMatrix, 4)).toBe(11);

            expect(VLOOKUP(3, numberMatrix, 2, true)).toBe("c");
            expect(VLOOKUP(2.5, numberMatrix, 2, true)).toBe("b");
            expect(VLOOKUP(4, numberMatrix, 2, true)).toBe("c");

            expect(VLOOKUP("A", stringMatrix, 3, true)).toBe(ErrorCode.NA);
            expect(VLOOKUP("Achsen", stringMatrix, 0, true)).toBe(ErrorCode.NA);
            expect(VLOOKUP("Achsen", stringMatrix, 100, true)).toBe(ErrorCode.NA);
        });

        it("should return the result of the vertical search with wildcards", () => {
            expect(VLOOKUP("Schra?ben", stringMatrix, 4)).toBe(8);
            expect(VLOOKUP("Schra?ben", stringMatrix, 4, false)).toBe(11);
            expect(VLOOKUP("G*e", stringMatrix, 4, false)).toBe(8);
            expect(VLOOKUP("G?e", stringMatrix, 4, false)).toBe(ErrorCode.NA);
        });

        it("should return the result of the vertical search with references", () => {
            expect(VLOOKUP(4.19, r3d("2:2!A1:C5"), 3)).toBe("Orange");
            expect(VLOOKUP(5.75, r3d("2:2!A1:C5"), 3)).toBe("Gelb");
            expect(VLOOKUP(6.00, r3d("2:2!A1:C5"), 3)).toBe("Gruen");
            expect(VLOOKUP(7.66, r3d("2:2!A1:C5"), 3)).toBe("Blau");
        });
    });
});
