/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import DateTimeFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/datetimefuncs";

import { dt, dtmat, ErrorCode, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/funcs/datetimefuncs", () => {

    // initialize the test
    const appPromise = createSpreadsheetApp("ooxml");
    const moduleTester = createFunctionModuleTester(DateTimeFuncs, appPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("DATEVALUE", DATEVALUE => {
        it("should return correct results", () => {
            expect(DATEVALUE("22.08.2011")).toBe(40777);
            expect(DATEVALUE("22. Mai. 2011")).toBe(40685);
            expect(DATEVALUE("11/3/2011")).toBe(40613);
            expect(DATEVALUE("2:24")).toBe(0);
            expect(DATEVALUE("22.08.2011 6:35")).toBe(40777);
        });
        it("should throw error", () => {
            expect(DATEVALUE("29.12.1899")).toBe(ErrorCode.VALUE);
            expect(DATEVALUE("1.1.10000")).toBe(ErrorCode.VALUE);
            expect(DATEVALUE("")).toBe(ErrorCode.VALUE);
            expect(DATEVALUE("hallo")).toBe(ErrorCode.VALUE);
            expect(DATEVALUE("22.08 2011")).toBe(ErrorCode.VALUE);
            expect(DATEVALUE("2011")).toBe(ErrorCode.VALUE);
            expect(DATEVALUE("5. 13.")).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("DAYS360", DAYS360 => {
        it("should return correct results", () => {
            expect(DAYS360(dt("2011-02-28"), dt("2011-03-31"), false)).toBe(30);
            expect(DAYS360(dt("2011-02-28"), dt("2011-03-31"), true)).toBe(32);
            expect(DAYS360(dt("2011-02-28"), dt("2011-03-31"))).toBe(30);
            // TODO: more tests
        });
    });

    moduleTester.testFunction("MONTHS", MONTHS => {
        it("should return correct results months between two assigned dates", () => {
            expect(MONTHS(dt("2010-04-03"), dt("2011-06-17"), 0)).toBe(14);
            expect(MONTHS(dt("2010-03-31"), dt("2010-04-30"), 0)).toBe(0);
            expect(MONTHS(dt("2010-03-31"), dt("2010-06-30"), 0)).toBe(2);
            expect(MONTHS(dt("2010-04-03"), dt("2011-06-17"), 1)).toBe(14);
            expect(MONTHS(dt("2010-03-31"), dt("2010-04-01"), 1)).toBe(1);
        });
    });

    moduleTester.testFunction("NETWORKDAYS", NETWORKDAYS => {
        it("should return correct results netto workdays between two assigned dates", () => {
            expect(NETWORKDAYS(dt("2015-10-05"), dt("2015-10-06"))).toBe(2);
            expect(NETWORKDAYS(dt("2015-10-06"), dt("2015-10-05"))).toBe(-2);
            expect(NETWORKDAYS(dt("2015-10-05"), dt("2015-10-09"))).toBe(5);
            expect(NETWORKDAYS(dt("2015-10-05"), dt("2015-10-12"))).toBe(6);
            expect(NETWORKDAYS(dt("2015-10-02"), dt("2015-10-12"))).toBe(7);

            expect(NETWORKDAYS(dt("1983-06-23"), dt("2015-10-02"))).toBe(8422);
            expect(NETWORKDAYS(dt("2015-10-02"), dt("2015-10-05"))).toBe(2);
            expect(NETWORKDAYS(dt("2015-10-05"), dt("1983-06-23"))).toBe(-8423);
            expect(NETWORKDAYS(dt("2015-10-05"), dt("2915-12-07"))).toBe(234845);
        });
    });

    moduleTester.testFunction("NETWORKDAYS.INTL", NETWORKDAYS_INTL => {
        it("should return correct results netto workdays between two assigned dates", () => {
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"))).toBe(5);
        });
        it("should return correct results netto workdays between two assigned dates and optional weekenddays", () => {
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 1)).toBe(5);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 2)).toBe(4);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 3)).toBe(3);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 4)).toBe(3);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 5)).toBe(3);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 5)).toBe(3);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 7)).toBe(4);

            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), true)).toBe(5); //true is interpreted as 1
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 0)).toBe(ErrorCode.NUM);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 8)).toBe(ErrorCode.NUM);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), "000")).toBe(ErrorCode.VALUE);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), false)).toBe(ErrorCode.NUM);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), "hello")).toBe(ErrorCode.VALUE);
        });

        it("should return correct results netto workdays between two assigned dates and optional weekenddays & holidays", () => {
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 1, dt("2015-10-07"))).toBe(4);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 2, dt("2015-10-07"))).toBe(3);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 3, dt("2015-10-07"))).toBe(2);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 4, dt("2015-10-07"))).toBe(3);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 5, dt("2015-10-07"))).toBe(3);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 6, dt("2015-10-07"))).toBe(2);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-09"), 7, dt("2015-10-07"))).toBe(3);

            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-11"), "0000000", dtmat([["2015-10-05", "2015-10-06"]]))).toBe(5);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-11"), "0000000", dtmat([["2015-10-09", "2015-10-10"]]))).toBe(5);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-11"), "0000000", dtmat([["2015-10-09", "2015-10-10", "2015-10-11"]]))).toBe(4);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2015-10-11"), "0000000", dtmat([["2015-10-11", "2015-10-09"]]))).toBe(5);

            expect(NETWORKDAYS_INTL(dt("1983-06-23"), dt("2015-10-02"), 4, dt("2015-10-02"))).toBe(8421);
            expect(NETWORKDAYS_INTL(dt("2015-10-02"), dt("2015-10-05"), 4, dt("2015-10-02"))).toBe(3);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("1983-06-23"), 4, dt("2015-10-02"))).toBe(-8424);
            expect(NETWORKDAYS_INTL(dt("2015-10-05"), dt("2915-12-07"), 4, dt("2015-10-02"))).toBe(234844);
        });
    });

    moduleTester.testFunction("TIME", TIME => {
        it("should return correct result", () => {
            expect(TIME(0, 0, 0)).toEqual(dt("00:00:00"));
            expect(TIME(0, 0, 1)).toEqual(dt("00:00:01"));
            expect(TIME(0, 0, 60)).toEqual(dt("00:01:00"));
            expect(TIME(0, 1, 0)).toEqual(dt("00:01:00"));
            expect(TIME(0, 1, 1)).toEqual(dt("00:01:01"));
            expect(TIME(0, 1, 60)).toEqual(dt("00:02:00"));
            expect(TIME(0, 1, -1)).toEqual(dt("00:00:59"));
            expect(TIME(0, 2, 0)).toEqual(dt("00:02:00"));
            expect(TIME(0, 60, 0)).toEqual(dt("01:00:00"));
            expect(TIME(1, 0, 0)).toEqual(dt("01:00:00"));
            expect(TIME(1, -1, 0)).toEqual(dt("00:59:00"));
            expect(TIME(1, 0, -1)).toEqual(dt("00:59:59"));
            expect(TIME(2, 60, 3600)).toEqual(dt("04:00:00"));
            expect(TIME(-1, 60, 0)).toEqual(dt("00:00:00"));
        });
        it("should truncate large results", () => {
            expect(TIME(24, 0, 0)).toEqual(dt("00:00:00"));
            expect(TIME(36, 0, 0)).toEqual(dt("12:00:00"));
            expect(TIME(48, 0, 0)).toEqual(dt("00:00:00"));
            expect(TIME(23, 60, 0)).toEqual(dt("00:00:00"));
        });
        it("should return #NUM! error for negative results", () => {
            expect(TIME(0, 0, -1)).toBe(ErrorCode.NUM);
            expect(TIME(0, 1, -61)).toBe(ErrorCode.NUM);
            expect(TIME(1, -60, -1)).toBe(ErrorCode.NUM);
            expect(TIME(-1, 0, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("TIMEVALUE", TIMEVALUE => {
        it("should return correct results", () => {
            expect(TIMEVALUE("2:24")).toBeAlmost(0.1);
            expect(TIMEVALUE("2 : 24")).toBeAlmost(0.1);
            expect(TIMEVALUE("22.08.2011 6:00")).toBeAlmost(0.25);
            expect(TIMEVALUE("23. Mai. 2011")).toBe(0);
        });
        it("should throw error", () => {
            expect(TIMEVALUE("29.12.1899")).toBe(ErrorCode.VALUE);
            expect(TIMEVALUE("1.1.10000")).toBe(ErrorCode.VALUE);
            expect(TIMEVALUE("15.59")).toBe(ErrorCode.VALUE);
            expect(TIMEVALUE("")).toBe(ErrorCode.VALUE);
            expect(TIMEVALUE("hallo")).toBe(ErrorCode.VALUE);
            expect(TIMEVALUE("20")).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("WEEKS", WEEKS => {
        it("should return correct number of weeks between two dates", () => {
            expect(WEEKS(dt("2010-05-03"), dt("2010-05-17"), 0)).toBe(2);
            expect(WEEKS(dt("2010-05-04"), dt("2010-05-27"), 0)).toBe(3);
            expect(WEEKS(dt("2010-05-06"), dt("2010-05-17"), 1)).toBe(2);
            expect(WEEKS(dt("2010-05-09"), dt("2010-05-10"), 1)).toBe(1);
            expect(WEEKS(dt("2010-05-03"), dt("2010-05-09"), 1)).toBe(0);
        });
    });

    moduleTester.testFunction("WORKDAY", WORKDAY => {
        it("should return correct date", () => {
            expect(WORKDAY(dt("2015-10-05"), 2)).toEqual(dt("2015-10-07"));
            expect(WORKDAY(dt("2015-10-06"), -2)).toEqual(dt("2015-10-02"));
            expect(WORKDAY(dt("2015-10-05"), 5)).toEqual(dt("2015-10-12"));
            expect(WORKDAY(dt("2015-10-05"), 6)).toEqual(dt("2015-10-13"));
            expect(WORKDAY(dt("2015-10-02"), 7)).toEqual(dt("2015-10-13"));

            expect(WORKDAY(dt("1983-06-23"), 8422)).toEqual(dt("2015-10-05"));
            expect(WORKDAY(dt("2015-10-02"), 2)).toEqual(dt("2015-10-06"));
            expect(WORKDAY(dt("2015-10-05"), -8423)).toEqual(dt("1983-06-22"));
            expect(WORKDAY(dt("2015-10-05"), 234845)).toEqual(dt("2915-12-09"));
        });
    });

    moduleTester.testFunction("WORKDAY.INTL", WORKDAY_INTL => {
        it("should return correct date", () => {
            expect(WORKDAY_INTL(dt("2015-10-05"), 5)).toEqual(dt("2015-10-12"));
        });
        it("should return correct results next workday between assigned date, day count and optional weekenddays", () => {
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, 1)).toEqual(dt("2015-10-12"));

            //first day is a weekend day
            expect(WORKDAY_INTL(dt("2015-10-04"), 5, 1)).toEqual(dt("2015-10-09"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, "1000000")).toEqual(dt("2015-10-10"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, 2)).toEqual(dt("2015-10-10"));

            expect(WORKDAY_INTL(dt("2015-10-05"), 5, 3)).toEqual(dt("2015-10-11"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, 4)).toEqual(dt("2015-10-12"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, 5)).toEqual(dt("2015-10-12"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, 6)).toEqual(dt("2015-10-12"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, 7)).toEqual(dt("2015-10-12"));

            expect(WORKDAY_INTL(dt("2015-10-05"), 5, true)).toEqual(dt("2015-10-12")); //true is interpreted as 1
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, 0)).toBe(ErrorCode.NUM);
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, 8)).toBe(ErrorCode.NUM);
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, "000")).toBe(ErrorCode.VALUE);
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, false)).toBe(ErrorCode.NUM);
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, "hello")).toBe(ErrorCode.VALUE);
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, "1111111")).toBe(ErrorCode.VALUE);
        });

        it("should return correct results next workday between assigned date, day count and optional weekenddays & holidays", () => {
            expect(WORKDAY_INTL(dt("2015-10-05"), 4, 1, dt("2015-10-07"))).toEqual(dt("2015-10-12"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 3, 2, dt("2015-10-07"))).toEqual(dt("2015-10-09"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 2, 3, dt("2015-10-07"))).toEqual(dt("2015-10-09"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 3, 4, dt("2015-10-07"))).toEqual(dt("2015-10-10"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 3, 5, dt("2015-10-07"))).toEqual(dt("2015-10-10"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 2, 6, dt("2015-10-07"))).toEqual(dt("2015-10-10"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 3, 7, dt("2015-10-07"))).toEqual(dt("2015-10-11"));

            expect(WORKDAY_INTL(dt("2015-10-05"), 5, "0000000", dtmat([["2015-10-05", "2015-10-06"]]))).toEqual(dt("2015-10-11"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, "0000000", dtmat([["2015-10-09", "2015-10-10"]]))).toEqual(dt("2015-10-12"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 4, "0000000", dtmat([["2015-10-09", "2015-10-10", "2015-10-11"]]))).toEqual(dt("2015-10-12"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 5, "0000000", dtmat([["2015-10-11", "2015-10-09"]]))).toEqual(dt("2015-10-12"));

            expect(WORKDAY_INTL(dt("1983-06-23"), 8421, 4, dt("2015-10-02"))).toEqual(dt("2015-10-03"));
            expect(WORKDAY_INTL(dt("2015-10-02"), 3, 4, dt("2015-10-02"))).toEqual(dt("2015-10-05"));
            expect(WORKDAY_INTL(dt("2015-10-05"), 234844, 4, dt("2015-10-02"))).toEqual(dt("2915-12-08"));
        });

        it("should return correct results last workday (negative workdays) between assigned date, day count and optional weekenddays & holidays", () => {
            expect(WORKDAY_INTL(dt("2015-10-05"), -8424, 1)).toEqual(dt("1983-06-21"));
            expect(WORKDAY_INTL(dt("2015-10-05"), -8424, "0000000")).toEqual(dt("1992-09-11"));
            expect(WORKDAY_INTL(dt("2015-10-05"), -8424, "0000000", dt("2015-10-02"))).toEqual(dt("1992-09-10"));
            expect(WORKDAY_INTL(dt("2015-10-05"), -8424, 1, dt("2015-10-02"))).toEqual(dt("1983-06-20"));
            expect(WORKDAY_INTL(dt("2015-10-05"), -8424, 2, dt("2015-10-02"))).toEqual(dt("1983-06-21"));
            expect(WORKDAY_INTL(dt("2015-10-05"), -8424, 3, dt("2015-10-02"))).toEqual(dt("1983-06-22"));
            expect(WORKDAY_INTL(dt("2015-10-05"), -8424, 4, dt("2015-10-02"))).toEqual(dt("1983-06-20"));
        });
    });

    moduleTester.testFunction("YEARFRAC", YEARFRAC => {
        // TODO: more test cases to cover all execution paths
        it("should return correct results for date mode 0 (30/360 US)", () => {
            expect(YEARFRAC(dt("2011-01-01"), dt("2011-01-01"), 0)).toBe(0);
            expect(YEARFRAC(dt("2011-02-28"), dt("2012-02-29"), 0)).toBe(1);
            expect(YEARFRAC(dt("2011-02-28"), dt("2011-03-31"), 0)).toBe(31 / 360);
        });
        it("should return correct results for date mode 1 (actual/actual)", () => {
            expect(YEARFRAC(dt("2011-01-01"), dt("2011-01-01"), 1)).toBe(0);
            expect(YEARFRAC(dt("2011-02-28"), dt("2012-02-29"), 1)).toBe(732 / 731);
            expect(YEARFRAC(dt("2011-02-28"), dt("2011-03-31"), 1)).toBe(31 / 365);
        });
        it("should return correct results for date mode 2 (actual/360)", () => {
            expect(YEARFRAC(dt("2011-01-01"), dt("2011-01-01"), 2)).toBe(0);
            expect(YEARFRAC(dt("2011-02-28"), dt("2012-02-29"), 2)).toBe(61 / 60);
            expect(YEARFRAC(dt("2011-02-28"), dt("2011-03-31"), 2)).toBe(31 / 360);
        });
        it("should return correct results for date mode 3 (actual/365)", () => {
            expect(YEARFRAC(dt("2011-01-01"), dt("2011-01-01"), 3)).toBe(0);
            expect(YEARFRAC(dt("2011-02-28"), dt("2012-02-29"), 3)).toBe(366 / 365);
            expect(YEARFRAC(dt("2011-02-28"), dt("2011-03-31"), 3)).toBe(31 / 365);
        });
        it("should return correct results for date mode 4 (30E/360)", () => {
            expect(YEARFRAC(dt("2011-01-01"), dt("2011-01-01"), 4)).toBe(0);
            expect(YEARFRAC(dt("2011-02-28"), dt("2012-02-29"), 4)).toBe(361 / 360);
            expect(YEARFRAC(dt("2011-02-28"), dt("2011-03-31"), 4)).toBe(32 / 360);
        });
        it("should default to date mode 0 (30/360 US)", () => {
            expect(YEARFRAC(dt("2011-01-01"), dt("2011-01-01"))).toBe(0);
            expect(YEARFRAC(dt("2011-02-28"), dt("2012-02-29"))).toBe(1);
            expect(YEARFRAC(dt("2011-02-28"), dt("2011-03-31"))).toBe(31 / 360);
        });
    });

    moduleTester.testFunction("YEARS", YEARS => {
        it("should return correct number of years between two dates", () => {
            expect(YEARS(dt("2009-04-03"), dt("2011-11-17"), 0)).toBe(2);
            expect(YEARS(dt("2011-02-28"), dt("2012-02-28"), 0)).toBe(1);
            expect(YEARS(dt("2012-02-29"), dt("2013-02-28"), 0)).toBe(0);
            expect(YEARS(dt("2009-04-03"), dt("2011-11-17"), 1)).toBe(2);
            expect(YEARS(dt("2009-12-31"), dt("2010-01-01"), 1)).toBe(1);
        });
    });
});
