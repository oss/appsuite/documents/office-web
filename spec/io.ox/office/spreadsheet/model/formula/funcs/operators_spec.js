/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { POW } from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";
import Operators from "@/io.ox/office/spreadsheet/model/formula/funcs/operators";

import { r3d, r3da, ErrorCode, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module io.ox/office/spreadsheet/model/formula/funcs/operators", () => {

    // initialize the test
    const appPromise = createSpreadsheetApp("ooxml");
    const moduleTester = createFunctionModuleTester(Operators, appPromise);

    // operator implementations -----------------------------------------------

    moduleTester.testFunction("uplus", (UPLUS, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 1);
            expect(descriptor).toHaveProperty("maxParams", 1);
        });
        it("should return the unconverted value", () => {
            expect(UPLUS(10)).toBe(10);
            expect(UPLUS(true)).toBeTrue();
            expect(UPLUS("10")).toBe("10");
            expect(UPLUS("abc")).toBe("abc");
        });
    });

    moduleTester.testFunction("uminus", (UMINUS, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 1);
            expect(descriptor).toHaveProperty("maxParams", 1);
        });
        it("should return the negated number", () => {
            expect(UMINUS(10)).toBe(-10);
            expect(UMINUS(-10)).toBe(10);
            expect(UMINUS(0)).toBe(-0);
        });
    });

    moduleTester.testFunction("upct", (PERCENT, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 1);
            expect(descriptor).toHaveProperty("maxParams", 1);
        });
        it("should return a hundreth of the argument", () => {
            expect(PERCENT(1)).toBe(0.01);
            expect(PERCENT(123)).toBe(1.23);
            expect(PERCENT(-1)).toBe(-0.01);
            expect(PERCENT(-123)).toBe(-1.23);
            expect(PERCENT(0)).toBe(0);
        });
    });

    moduleTester.testFunction("add", (ADD, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should return the sum", () => {
            expect(ADD(10, 2)).toBe(12);
        });
    });

    moduleTester.testFunction("sub", (SUB, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should return the difference", () => {
            expect(SUB(10, 2)).toBe(8);
        });
    });

    moduleTester.testFunction("mul", (MUL, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should return the product", () => {
            expect(MUL(10, 2)).toBe(20);
        });
    });

    moduleTester.testFunction("div", (DIV, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should return the quotient", () => {
            expect(DIV(10, 2)).toBe(5);
        });
        it("should throw #DIV/0! for zero divisor", () => {
            expect(DIV(10, 0)).toBe(ErrorCode.DIV0);
        });
    });

    moduleTester.testFunction("pow", (_POW, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should provide the correct resolver", () => {
            expect(descriptor).toHaveProperty("resolve", POW);
        });
    });

    moduleTester.testFunction("con", (CONCAT, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should return the concatenated strings", () => {
            expect(CONCAT("ab", "cd")).toBe("abcd");
            expect(CONCAT("ab", "")).toBe("ab");
            expect(CONCAT("", "cd")).toBe("cd");
            expect(CONCAT("", "")).toBe("");
        });
    });

    moduleTester.testFunction("lt", (LT, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should compare the passed values", () => {
            expect(LT(1, 2)).toBeTrue();
            expect(LT(2, 1)).toBeFalse();
            expect(LT(2, 2)).toBeFalse();
            expect(LT("a", "b")).toBeTrue();
            expect(LT("b", "a")).toBeFalse();
            expect(LT("b", "b")).toBeFalse();
            expect(LT(1, "1")).toBeTrue();
            expect(LT("1", 1)).toBeFalse();
        });
    });

    moduleTester.testFunction("le", (LE, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should compare the passed values", () => {
            expect(LE(1, 2)).toBeTrue();
            expect(LE(2, 1)).toBeFalse();
            expect(LE(2, 2)).toBeTrue();
            expect(LE("a", "b")).toBeTrue();
            expect(LE("b", "a")).toBeFalse();
            expect(LE("b", "b")).toBeTrue();
            expect(LE(1, "1")).toBeTrue();
            expect(LE("1", 1)).toBeFalse();
        });
    });

    moduleTester.testFunction("gt", (GT, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should compare the passed values", () => {
            expect(GT(1, 2)).toBeFalse();
            expect(GT(2, 1)).toBeTrue();
            expect(GT(2, 2)).toBeFalse();
            expect(GT("a", "b")).toBeFalse();
            expect(GT("b", "a")).toBeTrue();
            expect(GT("b", "b")).toBeFalse();
            expect(GT(1, "1")).toBeFalse();
            expect(GT("1", 1)).toBeTrue();
        });
    });

    moduleTester.testFunction("ge", (GE, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should compare the passed values", () => {
            expect(GE(1, 2)).toBeFalse();
            expect(GE(2, 1)).toBeTrue();
            expect(GE(2, 2)).toBeTrue();
            expect(GE("a", "b")).toBeFalse();
            expect(GE("b", "a")).toBeTrue();
            expect(GE("b", "b")).toBeTrue();
            expect(GE(1, "1")).toBeFalse();
            expect(GE("1", 1)).toBeTrue();
        });
    });

    moduleTester.testFunction("eq", (EQ, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should compare the passed values", () => {
            expect(EQ(1, 2)).toBeFalse();
            expect(EQ(2, 1)).toBeFalse();
            expect(EQ(2, 2)).toBeTrue();
            expect(EQ("a", "b")).toBeFalse();
            expect(EQ("b", "a")).toBeFalse();
            expect(EQ("b", "b")).toBeTrue();
            expect(EQ(1, "1")).toBeFalse();
            expect(EQ("1", 1)).toBeFalse();
        });
    });

    moduleTester.testFunction("ne", (NE, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should compare the passed values", () => {
            expect(NE(1, 2)).toBeTrue();
            expect(NE(2, 1)).toBeTrue();
            expect(NE(2, 2)).toBeFalse();
            expect(NE("a", "b")).toBeTrue();
            expect(NE("b", "a")).toBeTrue();
            expect(NE("b", "b")).toBeFalse();
            expect(NE(1, "1")).toBeTrue();
            expect(NE("1", 1)).toBeTrue();
        });
    });

    moduleTester.testFunction("list", (LIST, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should concatenate range lists", () => {
            expect(LIST(r3da("0:0!A1:A1"), r3da("1:1!A1:A1"))).toEqual(r3da("0:0!A1:A1 1:1!A1:A1"));
            expect(LIST(r3da("0:0!A1:B2 0:0!B2:C3"), r3da("1:1!A1:A1"))).toEqual(r3da("0:0!A1:B2 0:0!B2:C3 1:1!A1:A1"));
        });
    });

    moduleTester.testFunction("isect", (ISECT, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should return the intersection", () => {
            expect(ISECT(r3da("0:0!A1:B2"), r3da("0:0!B2:C3"))).toEqual(r3da("0:0!B2:B2"));
            expect(ISECT(r3da("0:0!A1:C3 0:0!B2:D4"), r3da("0:0!C3:D5 0:0!D4:E6"))).toEqual(r3da("0:0!C3:C3 0:0!C3:D4 0:0!D4:D4"));
        });
        it("should throw #VALUE! error code for ranges from different sheets", () => {
            expect(ISECT(r3da("0:0!A1:A1"), r3da("1:1!A1:A1"))).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("range", (RANGE, descriptor) => {
        it("should provide correct parameter count", () => {
            expect(descriptor).toHaveProperty("minParams", 2);
            expect(descriptor).toHaveProperty("maxParams", 2);
        });
        it("should return the union", () => {
            expect(RANGE(r3da("0:0!A1:B2"), r3da("0:0!B2:C3"))).toEqual(r3d("0:0!A1:C3"));
            expect(RANGE(r3da("0:0!A1:C3 0:0!B2:D4"), r3da("0:0!C3:D5 0:0!D4:E6"))).toEqual(r3d("0:0!A1:E6"));
        });
        it("should throw #VALUE! error code for ranges from different sheets", () => {
            expect(RANGE(r3da("0:0!A1:A1"), r3da("1:1!A1:A1"))).toBe(ErrorCode.VALUE);
        });
    });
});
