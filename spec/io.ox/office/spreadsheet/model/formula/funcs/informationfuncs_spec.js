/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import InformationFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/informationfuncs";

import { r3d, ErrorCode, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/funcs/informationfuncs", () => {

    // the operations to be applied at the test document
    const OPERATIONS = [
        { name: "setDocumentAttributes", attrs: { document: { cols: 16384, rows: 1048576 } } },
        { name: "insertSheet", sheet: 0, sheetName: "Sheet1" },
        { name: "changeCells", sheet: 0, start: "A2", contents: [
            [2, { v: 3, f: "=SUM(1,2)" }, "abc"],
            [{ v: 2, f: "A2", si: 0, sr: "A3:B3" }, { v: 3, si: 0 }],
            [{ v: 4, f: "A3:B3*2", mr: "A4:B4" }, 6]
        ] },
        { name: "insertSheet", sheet: 1, sheetName: "Sheet2" },
        { name: "insertSheet", sheet: 2, sheetName: "Sheet 3" }
    ];

    // initialize the test
    const appPromise = createSpreadsheetApp("ooxml", OPERATIONS);
    const moduleTester = createFunctionModuleTester(InformationFuncs, appPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("CELL", CELL => {
        const r1 = r3d("0:0!B20:B20");
        const r2 = r3d("1:1!B20:B20");
        const r3 = r3d("2:2!B20:B20");
        const rx = r3d("3:3!B20:B20");
        it("should return information about the formatting, location, or contents of a cell", () => {
            expect(CELL("address", r1)).toBe("$B$20");
            expect(CELL("address", r2)).toBe("Sheet2!$B$20");
            expect(CELL("address", r3)).toBe("'Sheet 3'!$B$20");
            expect(CELL("col", r1)).toBe(2);
            expect(CELL("color", r1)).toBe(0);
            expect(CELL("contents", r1)).toBeNull();
            expect(CELL("filename", r1)).toBe("/path/to/[testfile.xyz]Sheet1");
            expect(CELL("filename", r2)).toBe("/path/to/[testfile.xyz]Sheet2");
            expect(CELL("filename", r3)).toBe("/path/to/[testfile.xyz]Sheet 3");
            expect(CELL("format", r1)).toBe("G");
            //expect(CELL("parentheses", r1)).toBe(0);
            expect(CELL("prefix", r1)).toBe("");
            expect(CELL("protect", r1)).toBe(1);
            expect(CELL("row", r1)).toBe(20);
            expect(CELL("type", r1)).toBe("b");
            //expect(CELL("width", r1)).toBe(8);
        });
        it("should throw #REF! for invalid sheet index", () => {
            expect(CELL("address", rx)).toBe(ErrorCode.REF);
            expect(CELL("col", rx)).toBe(ErrorCode.REF);
            expect(CELL("color", rx)).toBe(ErrorCode.REF);
            expect(CELL("contents", rx)).toBe(ErrorCode.REF);
            expect(CELL("filename", rx)).toBe(ErrorCode.REF);
            expect(CELL("format", rx)).toBe(ErrorCode.REF);
            expect(CELL("parentheses", rx)).toBe(ErrorCode.REF);
            expect(CELL("prefix", rx)).toBe(ErrorCode.REF);
            expect(CELL("protect", rx)).toBe(ErrorCode.REF);
            expect(CELL("row", rx)).toBe(ErrorCode.REF);
            expect(CELL("type", rx)).toBe(ErrorCode.REF);
            expect(CELL("width", rx)).toBe(ErrorCode.REF);
        });
        it("should throw #VAL for non-recognized values", () => {
            expect(CELL("__invalid__", r1)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("ERROR.TYPE", ERROR_TYPE => {
        it("should return the numeric index of an error", () => {
            expect(ERROR_TYPE(ErrorCode.NULL)).toBe(1);
            expect(ERROR_TYPE(ErrorCode.DIV0)).toBe(2);
            expect(ERROR_TYPE(ErrorCode.VALUE)).toBe(3);
            expect(ERROR_TYPE(ErrorCode.REF)).toBe(4);
            expect(ERROR_TYPE(ErrorCode.NAME)).toBe(5);
            expect(ERROR_TYPE(ErrorCode.NUM)).toBe(6);
            expect(ERROR_TYPE(ErrorCode.NA)).toBe(7);
            expect(ERROR_TYPE(ErrorCode.DATA)).toBe(8);
        });
        it("should throw #N/A for non-error values", () => {
            expect(ERROR_TYPE(1)).toBe(ErrorCode.NA);
            expect(ERROR_TYPE("abc")).toBe(ErrorCode.NA);
            expect(ERROR_TYPE(true)).toBe(ErrorCode.NA);
            expect(ERROR_TYPE(null)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("ISFORMULA", ISFORMULA => {
        it("should return whether a cell is a formula cell", () => {
            expect(ISFORMULA(r3d("0:0!A2:A2"))).toBeFalse(); // number cell
            expect(ISFORMULA(r3d("0:0!B2:B2"))).toBeTrue(); // formula cell
            expect(ISFORMULA(r3d("0:0!C2:C2"))).toBeFalse(); // string cell
            expect(ISFORMULA(r3d("0:0!IV99:IV99"))).toBeFalse(); // blank cell
        });
        it("should return true for all shared formula cells", () => {
            expect(ISFORMULA(r3d("0:0!A3:A3"))).toBeTrue(); // shared formula (anchor)
            expect(ISFORMULA(r3d("0:0!B3:B3"))).toBeTrue(); // shared formula
        });
        it("should return true for all matrix formula cells", () => {
            expect(ISFORMULA(r3d("0:0!A4:A4"))).toBeTrue(); // matrix formula (anchor)
            expect(ISFORMULA(r3d("0:0!B4:B4"))).toBeTrue(); // matrix formula
        });
        it("should return true for the reference address of the formula context", () => {
            expect(ISFORMULA(r3d("0:0!A1:A1"))).toBeTrue();
        });
        it("should use the start address of a range", () => {
            expect(ISFORMULA(r3d("0:0!B2:C3"))).toBeTrue();
        });
    });
});
