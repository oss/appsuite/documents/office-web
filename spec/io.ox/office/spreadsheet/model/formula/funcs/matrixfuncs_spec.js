/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { InternalErrorCode, MAX_MATRIX_SIZE } from "@/io.ox/office/spreadsheet/model/formula/formulautils";
import MatrixFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/matrixfuncs";

import { mat as m, nmat as nm, ErrorCode, matrixMatcher, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module io.ox/office/spreadsheet/model/formula/funcs/matrixfuncs", () => {

    // initialize the test
    const appPromise = createSpreadsheetApp("ooxml");
    const moduleTester = createFunctionModuleTester(MatrixFuncs, appPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("MDETERM", MDETERM => {
        it("should return the matrix determinant", () => {
            expect(MDETERM(nm([[1]]))).toBe(1);
            expect(MDETERM(nm([[3, 6], [1, 1]]))).toBe(-3);
            expect(MDETERM(nm([[0, 1, 2], [3, 4, 5], [6, 7, 8]]))).toBe(0);
            expect(MDETERM(nm([[3, 6, 1], [1, 1, 0], [3, 10, 2]]))).toBe(1);
            expect(MDETERM(nm([[-4, -2.5, 3], [5, 6, 4], [9, 10, -9]]))).toBe(161.5);
            expect(MDETERM(nm([[1, 3, 8, 5], [1, 3, 6, 1], [1, 1, 1, 0], [7, 3, 10, 2]]))).toBe(88);
            expect(MDETERM(nm([[0, 1, 1, 1], [1, 3, 6, 1], [5, 3, 8, 1], [2, 3, 10, 7]]))).toBe(88);
            expect(MDETERM(nm([[1, 3, 8, 5, 1], [1, 3, 6, 1, 1], [1, 1, 1, 0, 1], [7, 3, 10, 2, 1], [1, 1, 1, 1, 1]]))).toBe(-24);
            expect(MDETERM(nm([[1, 0, 0], [0, 1, 0], [0, 0, 0]]))).toBe(0);
            expect(MDETERM(nm([[1, 1, 1], [1, 1, 1], [1, 1, 1]]))).toBe(0);
        });
        it("should throw #VALUE! error code for non-square matrix", () => {
            expect(MDETERM(nm([[1, 1], [1, 1], [1, 1]]))).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("MINVERSE", MINVERSE => {
        it("should return the inverse matrix", () => {
            expect(MINVERSE(nm([[1, 0, 0], [0, 1, 0], [0, 0, 1]]))).toSatisfy(matrixMatcher([[1, 0, 0], [0, 1, 0], [0, 0, 1]]));
            expect(MINVERSE(nm([[0, 1, 0], [0, 0, 1], [1, 0, 0]]))).toSatisfy(matrixMatcher([[0, 0, 1], [1, 0, 0], [0, 1, 0]]));
            expect(MINVERSE(nm([[1, 2, 1], [3, 4, -1], [0, 2, 0]]))).toSatisfy(matrixMatcher([[0.25, 0.25, -0.75], [0, 0, 0.5], [0.75, -0.25, -0.25]]));
        });
        it("should throw #VALUE! error code for non-square matrix", () => {
            expect(MINVERSE(nm([[1, 1], [1, 1], [1, 1]]))).toBe(ErrorCode.VALUE);
        });
        it("should throw #NUM! error code for non-invertible matrix", () => {
            expect(MINVERSE(nm([[1, 0, 0], [0, 1, 0], [0, 0, 0]]))).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("MMULT", MMULT => {
        it("should return the matrix product", () => {
            expect(MMULT(nm([[2]]), nm([[3]]))).toSatisfy(matrixMatcher([[6]]));
            expect(MMULT(nm([[2]]), nm([[3, 4]]))).toSatisfy(matrixMatcher([[6, 8]]));
            expect(MMULT(nm([[2], [3]]), nm([[4]]))).toSatisfy(matrixMatcher([[8], [12]]));
            expect(MMULT(nm([[2, 3]]), nm([[4], [5]]))).toSatisfy(matrixMatcher([[23]]));
            expect(MMULT(nm([[2], [3]]), nm([[4, 5]]))).toSatisfy(matrixMatcher([[8, 10], [12, 15]]));
            expect(MMULT(nm([[2, 3], [4, 5]]), nm([[6, 7], [8, 9]]))).toSatisfy(matrixMatcher([[36, 41], [64, 73]]));
        });
        it("should throw the #VALUE! error, if matrix sizes do not match", () => {
            expect(MMULT(nm([[2]]), nm([[3], [4]]))).toBe(ErrorCode.VALUE);
            expect(MMULT(nm([[2, 3]]), nm([[4, 5]]))).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("MUNIT", MUNIT => {
        it("should return the unit matrix", () => {
            expect(MUNIT(1)).toSatisfy(matrixMatcher([[1]]));
            expect(MUNIT(2)).toSatisfy(matrixMatcher([[1, 0], [0, 1]]));
            expect(MUNIT(3)).toSatisfy(matrixMatcher([[1, 0, 0], [0, 1, 0], [0, 0, 1]]));
            expect(MUNIT(4)).toSatisfy(matrixMatcher([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]));
        });
        it("should throw the #VALUE! error, if passed dimension is too small", () => {
            expect(MUNIT(0)).toBe(ErrorCode.VALUE);
            expect(MUNIT(-1)).toBe(ErrorCode.VALUE);
        });
        it("should throw the UNSUPPORTED error, if passed dimension is too large", () => {
            const maxSize = Math.floor(Math.sqrt(MAX_MATRIX_SIZE));
            expect(MUNIT(maxSize + 1)).toBe(InternalErrorCode.UNSUPPORTED);
        });
    });

    moduleTester.testFunction("SUMPRODUCT", SUMPRODUCT => {
        it("should return the sum product of its arguments", () => {
            expect(SUMPRODUCT(1, 2, m([[3]]))).toBe(6);
            expect(SUMPRODUCT(m([[1, 2], [3, 4], [5, 6]]))).toBe(21);
            expect(SUMPRODUCT(m([[1, 2]]), m([[3, 4]]), m([[5, 6]]))).toBe(63);
        });
        it("should skip invalid values in matrixes", () => {
            expect(SUMPRODUCT(m([[1, 2]]), m([["a", 4]]), m([[5, 6]]))).toBe(48);
        });
        it("should throw #VALUE! error code for invalid scalar values", () => {
            expect(SUMPRODUCT(1, 2, "abc")).toBe(ErrorCode.VALUE);
            expect(SUMPRODUCT(1, 2, "3")).toBe(ErrorCode.VALUE);
            expect(SUMPRODUCT(1, 2, true)).toBe(ErrorCode.VALUE);
            expect(SUMPRODUCT(1, 2, null)).toBe(ErrorCode.VALUE);
        });
        it("should throw #VALUE! error code for non-matching matrix sizes", () => {
            expect(SUMPRODUCT(m([[3, 4, 1], [8, 6, 1], [1, 9, 1]]), m([[2, 7], [6, 7], [5, 3]]))).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("SUMX2MY2", SUMX2MY2 => {
        it("should return the correct result", () => {
            expect(SUMX2MY2(1, m([[2]]))).toBe(-3);
            expect(SUMX2MY2(m([[1, 2, 4]]), m([[2, 5, 1]]))).toBe(-9);
            expect(SUMX2MY2(m([[1, 2, 4]]), m([[2, "a", 1]]))).toBe(12);
        });
        it("should ignore matrix orientation", () => {
            expect(SUMX2MY2(m([[1, 2, 4]]), m([[2], [5], [1]]))).toBe(-9);
        });
        it("should return the #N/A error code for non-matching matrix sizes", () => {
            expect(SUMX2MY2(m([[1, 1], [1, 1]]), m([[1, 1]]))).toBe(ErrorCode.NA);
        });
        it("should throw #VALUE! error code for invalid parameters", () => {
            expect(SUMX2MY2(1, true)).toBe(ErrorCode.VALUE);
            expect(SUMX2MY2(1, "abc")).toBe(ErrorCode.VALUE);
            expect(SUMX2MY2(1, null)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("SUMX2PY2", SUMX2PY2 => {
        it("should return the correct result", () => {
            expect(SUMX2PY2(m([[1, 2, 4]]), m([[2, 5, 1]]))).toBe(51);
            expect(SUMX2PY2(m([[1, 2, 4]]), m([[2, "a", 1]]))).toBe(22);
        });
        it("should ignore matrix orientation", () => {
            expect(SUMX2PY2(m([[1, 2, 4]]), m([[2], [5], [1]]))).toBe(51);
        });
        it("should return the #N/A error code for non-matching matrix sizes", () => {
            expect(SUMX2PY2(m([[1, 1], [1, 1]]), m([[1, 1]]))).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("SUMXMY2", SUMXMY2 => {
        it("should return the correct result", () => {
            expect(SUMXMY2(m([[1, 2, 4]]), m([[2, 5, 1]]))).toBe(19);
            expect(SUMXMY2(m([[1, 2, 4]]), m([[2, "a", 1]]))).toBe(10);
        });
        it("should ignore matrix orientation", () => {
            expect(SUMXMY2(m([[1, 2, 4]]), m([[2], [5], [1]]))).toBe(19);
        });
        it("should return the #N/A error code for non-matching matrix sizes", () => {
            expect(SUMXMY2(m([[1, 1], [1, 1]]), m([[1, 1]]))).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("TRANSPOSE", TRANSPOSE => {
        it("should return the transposed matrix", () => {
            expect(TRANSPOSE(m([[1]]))).toSatisfy(matrixMatcher([[1]]));
            expect(TRANSPOSE(m([[1, 2]]))).toSatisfy(matrixMatcher([[1], [2]]));
            expect(TRANSPOSE(m([[1], [2]]))).toSatisfy(matrixMatcher([[1, 2]]));
            expect(TRANSPOSE(m([[1, 2], [3, 4]]))).toSatisfy(matrixMatcher([[1, 3], [2, 4]]));
            expect(TRANSPOSE(m([[1, 2, 3], [4, 5, 6]]))).toSatisfy(matrixMatcher([[1, 4], [2, 5], [3, 6]]));
        });
    });
});
