/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { fmt } from "@/io.ox/office/tk/algorithms";
import TextFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/textfuncs";

import { r3d, mat, ErrorCode, createSpreadsheetApp, createFunctionModuleTester, changeConfig, insertSheet, changeCells } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/funcs/textfuncs", () => {

    // the operations to be applied at the test document
    const OPERATIONS = [
        changeConfig({ document: { cols: 16384, rows: 1048576, activeSheet: 0 } }),
        insertSheet(0, "Sheet1"),
        changeCells(0, "A2", [["abc", "", 123], [null, true]])
    ];

    // initialize the test
    const appPromise = createSpreadsheetApp("ooxml", OPERATIONS);
    const moduleTester = createFunctionModuleTester(TextFuncs, appPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("BAHTTEXT", BAHTTEXT => {

        // pattern map for BAHTTEXT tests
        const BAHTTEXT_MAP = {
            0: "\u0E28\u0E39\u0E19\u0E22\u0E4C",
            1: "\u0E2B\u0E19\u0E36\u0E48\u0E07",
            2: "\u0E2A\u0E2D\u0E07",
            3: "\u0E2A\u0E32\u0E21",
            4: "\u0E2A\u0E35\u0E48",
            5: "\u0E2B\u0E49\u0E32",
            6: "\u0E2B\u0E01",
            7: "\u0E40\u0E08\u0E47\u0E14",
            8: "\u0E41\u0E1B\u0E14",
            9: "\u0E40\u0E01\u0E49\u0E32",
            d: "\u0E2A\u0E34\u0E1A",
            I: "\u0E40\u0E2D\u0E47\u0E14",              // trailing one
            T: "\u0E22\u0E35\u0E48",                    // "two" for twenty
            h: "\u0E23\u0E49\u0E2D\u0E22",              // hundred
            k: "\u0E1E\u0E31\u0E19",                    // thousand
            D: "\u0E2B\u0E21\u0E37\u0E48\u0E19",        // ten thousand
            H: "\u0E41\u0E2A\u0E19",                    // hundred thousand
            M: "\u0E25\u0E49\u0E32\u0E19",              // million
            B: "\u0E1A\u0E32\u0E17",                    // Baht
            S: "\u0E2A\u0E15\u0E32\u0E07\u0E04\u0E4C",  // Satang
            "~": "\u0E16\u0E49\u0E27\u0E19",            // missing (zero) Satang
            "!": "\u0E16\u0E49",                        // missing (zero) Satang for negative zero
            "-": "\u0E25\u0E1A"                         // minus
        };

        function getExpectedBahtText(pattern) {
            return pattern.split("").reduce((exp, char) => exp + BAHTTEXT_MAP[char], "");
        }

        function getEscapeSeq(string) {
            return string.split("").reduce((text, char) => `${text}\\u${fmt.formatInt(char.charCodeAt(0), 16, { digits: 4, lower: true })}`, "");
        }

        function check(...args) {
            for (let i = 0; i < args.length; i += 2) {
                const number = args[i];
                const expected = getExpectedBahtText(args[i + 1]);
                const result = BAHTTEXT(number);
                if (result !== expected) {
                    console.warn("BAHTTEXT failed for input value " + number);
                    console.warn("  exp=" + getEscapeSeq(expected));
                    console.warn("  got=" + getEscapeSeq(result));
                }
                expect(result).toBe(expected);
            }
        }

        it("should return the correct result for integers between 0 and 9", () => {
            check(0, "0B~", 1, "1B~", 2, "2B~", 3, "3B~", 4, "4B~", 5, "5B~", 6, "6B~", 7, "7B~", 8, "8B~", 9, "9B~");
        });
        it("should return the correct result for integers between 10 and 99", () => {
            check(10, "dB~", 11, "dIB~", 12, "d2B~", 13, "d3B~", 14, "d4B~", 15, "d5B~", 16, "d6B~", 17, "d7B~", 18, "d8B~", 19, "d9B~");
            check(20, "TdB~", 21, "TdIB~", 22, "Td2B~", 23, "Td3B~", 24, "Td4B~", 25, "Td5B~", 26, "Td6B~", 27, "Td7B~", 28, "Td8B~", 29, "Td9B~");
            check(30, "3dB~", 31, "3dIB~", 32, "3d2B~", 39, "3d9B~");
            check(40, "4dB~", 41, "4dIB~", 42, "4d2B~", 49, "4d9B~");
            check(50, "5dB~", 60, "6dB~", 70, "7dB~", 80, "8dB~", 90, "9dB~", 99, "9d9B~");
        });
        it("should return the correct result for integers between 100 and 999", () => {
            check(100, "1hB~", 101, "1hIB~", 102, "1h2B~", 109, "1h9B~");
            check(110, "1hdB~", 111, "1hdIB~", 112, "1hd2B~", 119, "1hd9B~");
            check(120, "1hTdB~", 121, "1hTdIB~", 122, "1hTd2B~", 129, "1hTd9B~");
            check(130, "1h3dB~", 131, "1h3dIB~", 132, "1h3d2B~", 139, "1h3d9B~");
            check(140, "1h4dB~", 141, "1h4dIB~", 142, "1h4d2B~", 149, "1h4d9B~");
            check(200, "2hB~", 201, "2hIB~", 202, "2h2B~", 209, "2h9B~", 210, "2hdB~", 211, "2hdIB~", 299, "2h9d9B~");
            check(300, "3hB~", 401, "4hIB~", 502, "5h2B~", 609, "6h9B~", 710, "7hdB~", 811, "8hdIB~", 999, "9h9d9B~");
        });
        it("should return the correct result for integers between 1,000 and 9,999", () => {
            check(1000, "1kB~", 1001, "1kIB~", 1002, "1k2B~", 1009, "1k9B~");
            check(1010, "1kdB~", 1011, "1kdIB~", 1012, "1kd2B~", 1019, "1kd9B~");
            check(1020, "1kTdB~", 1021, "1kTdIB~", 1022, "1kTd2B~", 1029, "1kTd9B~");
            check(1030, "1k3dB~", 1031, "1k3dIB~", 1032, "1k3d2B~", 1039, "1k3d9B~");
            check(1100, "1k1hB~", 1101, "1k1hIB~", 1102, "1k1h2B~", 1109, "1k1h9B~");
            check(1210, "1k2hdB~", 2311, "2k3hdIB~", 3422, "3k4hTd2B~", 9999, "9k9h9d9B~");
        });
        it("should return the correct result for integers between 10,000 and 99,999", () => {
            check(10000, "1DB~", 10001, "1DIB~", 10002, "1D2B~", 10009, "1D9B~");
            check(10010, "1DdB~", 10011, "1DdIB~", 10012, "1Dd2B~", 10019, "1Dd9B~");
            check(10120, "1D1hTdB~", 10221, "1D2hTdIB~", 10322, "1D3hTd2B~", 10929, "1D9hTd9B~");
            check(11210, "1D1k2hdB~", 23411, "2D3k4hdIB~", 34522, "3D4k5hTd2B~", 99999, "9D9k9h9d9B~");
        });
        it("should return the correct result for integers between 100,000 and 999,999", () => {
            check(100000, "1HB~", 100001, "1HIB~", 100002, "1H2B~", 100009, "1H9B~");
            check(100010, "1HdB~", 100011, "1HdIB~", 100012, "1Hd2B~", 100019, "1Hd9B~");
            check(100120, "1H1hTdB~", 100221, "1H2hTdIB~", 100322, "1H3hTd2B~", 100929, "1H9hTd9B~");
            check(101120, "1H1k1hTdB~", 101221, "1H1k2hTdIB~", 101322, "1H1k3hTd2B~", 101929, "1H1k9hTd9B~");
            check(111210, "1H1D1k2hdB~", 234511, "2H3D4k5hdIB~", 345622, "3H4D5k6hTd2B~", 999999, "9H9D9k9h9d9B~");
        });
        it("should return the correct result for integers between 1,000,000 and 9,999,999", () => {
            check(1000000, "1MB~", 1000001, "1MIB~", 1000002, "1M2B~", 1000009, "1M9B~", 1999999, "1M9H9D9k9h9d9B~");
            check(2000000, "2MB~", 3000001, "3MIB~", 4567890, "4M5H6D7k8h9dB~", 9999999, "9M9H9D9k9h9d9B~");
        });
        it("should return the correct result for integers between 10,000,000 and 99,999,999", () => {
            check(10000000, "dMB~", 10000001, "dMIB~", 11000002, "dIM2B~", 20000009, "TdM9B~", 99999999, "9d9M9H9D9k9h9d9B~");
        });
        it("should return the correct result for integers starting from 100,000,000", () => {
            check(100000000, "1hMB~", 100000001, "1hMIB~", 101000000, "1hIMB~", 1000000000000, "1MMB~", 1000001000000, "1MIMB~", 1000001000001, "1MIMIB~");
        });
        it("should return the correct result for negative integers", () => {
            check(-1, "-1B~", -2, "-2B~", -9999999, "-9M9H9D9k9h9d9B~");
        });
        it("should return the correct result for fractionals between -1 and 1", () => {
            check(0.01, "1S", 0.02, "2S", 0.10, "dS", 0.11, "dIS", 0.20, "TdS", 0.21, "TdIS", 0.99, "9d9S");
            check(-0.01, "-1S", -0.99, "-9d9S");
            check(0.014, "1S", 0.015, "2S", -0.014, "-1S", -0.015, "-2S");
            check(0.004, "0B~", 0.005, "1S", -0.004, "-0B!", -0.005, "-1S");
        });
    });

    moduleTester.testFunction("CHAR", CHAR => {
        it("should convert ANSI codes to characters", () => {
            expect(CHAR(1)).toBe("\x01");
            expect(CHAR(63)).toBe("?");
            expect(CHAR(65)).toBe("A");
            expect(CHAR(97)).toBe("a");
            expect(CHAR(128)).toBe("\u20ac");
            expect(CHAR(255)).toBe("\xff");
        });
        it("should return #VALUE! error for invalid codes", () => {
            expect(CHAR(0)).toBe(ErrorCode.VALUE);
            expect(CHAR(-1)).toBe(ErrorCode.VALUE);
            expect(CHAR(256)).toBe(ErrorCode.VALUE);
            expect(CHAR(257)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("CODE", CODE => {
        it("should convert characters to ANSI codes", () => {
            expect(CODE("\x01")).toBe(1);
            expect(CODE("?")).toBe(63);
            expect(CODE("ABC")).toBe(65);
            expect(CODE("abc")).toBe(97);
            expect(CODE("\x80")).toBe(128);
            expect(CODE("\u20ac")).toBe(128);
            expect(CODE("\xff")).toBe(255);
        });
        it("should return question mark for unknown characters", () => {
            expect(CODE("\u0100")).toBe(63);
            expect(CODE("\u1111")).toBe(63);
            expect(CODE("\ufffd")).toBe(63);
            expect(CODE("\u20aa")).toBe(63);
        });
        it("should return #VALUE! error for empty string", () => {
            expect(CODE("")).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("CONCAT", CONCAT => {
        it("should return the concatenation of the operands", () => {
            expect(CONCAT("abc", 123, null, true)).toBe("abc123WAHR");
        });
        it("should concatenate matrix elements", () => {
            expect(CONCAT(mat([["abc", 123], ["", true]]))).toBe("abc123WAHR");
        });
        it("should concatenate cell contents", () => {
            expect(CONCAT(r3d("0:0!A2:C3"))).toBe("abc123WAHR");
        });
    });

    moduleTester.testFunction("CONCATENATE", CONCATENATE => {
        it("should return the concatenation of the operands", () => {
            expect(CONCATENATE("abc", "123", "", "WAHR")).toBe("abc123WAHR");
        });
    });

    moduleTester.testFunction("DOLLAR", DOLLAR => {
        it("should return the currency formatted string for a number", () => {
            expect(DOLLAR(1234.567, 2)).toBe("1.234,57 \u20AC");
            expect(DOLLAR(1234.567, -2)).toBe("1.200 \u20AC");
            expect(DOLLAR(-1234.567, -2)).toBe("-1.200 \u20AC");
            expect(DOLLAR(-0.123, 4)).toBe("-0,1230 \u20AC");
            expect(DOLLAR(99.888)).toBe("99,89 \u20AC");

            expect(DOLLAR(1234.567, 1)).toBe("1.234,6 \u20AC");
            expect(DOLLAR(1234.567, -1)).toBe("1.230 \u20AC");

            expect(DOLLAR(4444.332)).toBe("4.444,33 \u20AC");
            expect(DOLLAR(44.332)).toBe("44,33 \u20AC");

            expect(DOLLAR(1234567.332)).toBe("1.234.567,33 \u20AC");

            const number = 123456789.987;
            expect(DOLLAR(number, 5)).toBe("123.456.789,98700 \u20AC");
            expect(DOLLAR(number, 2)).toBe("123.456.789,99 \u20AC");
            expect(DOLLAR(number, 1)).toBe("123.456.790,0 \u20AC");
            expect(DOLLAR(number, 0)).toBe("123.456.790 \u20AC");
            expect(DOLLAR(number, -1)).toBe("123.456.790 \u20AC");
            expect(DOLLAR(number, -2)).toBe("123.456.800 \u20AC");
            expect(DOLLAR(number, -5)).toBe("123.500.000 \u20AC");
            expect(DOLLAR(number, -8)).toBe("100.000.000 \u20AC");
            expect(DOLLAR(number, -9)).toBe("0 \u20AC");
            expect(DOLLAR(number, -1000)).toBe("0 \u20AC");
        });
        it("should throw #VALUE! error for wrong second argument", () => {
            expect(DOLLAR(10, 128)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("FIXED", FIXED => {
        it("should return the fixed formated string of assigned number", () => {
            expect(FIXED(1234.567, 1)).toBe("1.234,6");
            expect(FIXED(1234.567, -1)).toBe("1.230");
            expect(FIXED(1234.567, -1, true)).toBe("1230");
            expect(FIXED(-1234.567, -1, true)).toBe("-1230");

            expect(FIXED(44.332, 2, true)).toBe("44,33");
            expect(FIXED(4444.332)).toBe("4.444,33");
            expect(FIXED(44.332)).toBe("44,33");

            expect(FIXED(1234567.332, 2, true)).toBe("1234567,33");
            expect(FIXED(1234567.332)).toBe("1.234.567,33");

            const number = 123456789.987;
            expect(FIXED(number, 5)).toBe("123.456.789,98700");
            expect(FIXED(number, 2)).toBe("123.456.789,99");
            expect(FIXED(number, 1)).toBe("123.456.790,0");
            expect(FIXED(number, 0)).toBe("123.456.790");
            expect(FIXED(number, -1)).toBe("123.456.790");
            expect(FIXED(number, -2)).toBe("123.456.800");
            expect(FIXED(number, -5)).toBe("123.500.000");
            expect(FIXED(number, -8)).toBe("100.000.000");
            expect(FIXED(number, -9)).toBe("0");
            expect(FIXED(number, -1000)).toBe("0");
        });
        it("should throw #VALUE! error for wrong second argument", () => {
            expect(FIXED(10, 128)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("LEFT", LEFT => {
        it("should return the left part", () => {
            expect(LEFT("\0\ud83d\udd95\ufffc", 0)).toBe("");
            expect(LEFT("\0\ud83d\udd95\ufffc", 1)).toBe("\0");
            expect(LEFT("\0\ud83d\udd95\ufffc", 2)).toBe("\0\ud83d\udd95");
            expect(LEFT("\0\ud83d\udd95\ufffc", 3)).toBe("\0\ud83d\udd95\ufffc");
            expect(LEFT("\0\ud83d\udd95\ufffc", 4)).toBe("\0\ud83d\udd95\ufffc");
        });
        it("should return one character", () => {
            expect(LEFT("abc")).toBe("a");
            expect(LEFT("\ud83d\udd95\ufffc")).toBe("\ud83d\udd95");
        });
        it("should return #VALUE! for invalid length", () => {
            expect(LEFT("abc", -1)).toBe(ErrorCode.VALUE);
            expect(LEFT("abc", -2)).toBe(ErrorCode.VALUE);
            expect(LEFT("abc", 32767)).toBe("abc");
            expect(LEFT("abc", 32768)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testAliasFunction("LEFTB", "LEFT");

    moduleTester.testFunction("LEN", LEN => {
        it("should return the code unit count", () => {
            expect(LEN("")).toBe(0);
            expect(LEN("abc")).toBe(3);
            // no reduction to full Unicode characters!
            expect(LEN("\0\ud83d\udd95\ufffc")).toBe(4);
        });
    });

    moduleTester.testAliasFunction("LENB", "LEN");

    moduleTester.testFunction("NUMBERVALUE", NUMBERVALUE => {
        it("should return the detected number assigned string", () => {
            expect(NUMBERVALUE("3,5%")).toBe(0.035);
            expect(NUMBERVALUE("2,500a27", "a", ",")).toBe(2500.27);
            expect(NUMBERVALUE("2,500a27", "abc", ",")).toBe(2500.27);
            expect(NUMBERVALUE("1.1.2007")).toBe(112007);
            expect(NUMBERVALUE("1.1.2007", "d", "g")).toBe(39083);
            expect(NUMBERVALUE("1.1,1")).toBe(11.1);

            expect(NUMBERVALUE("3,5%%")).toBeAlmost(3.5e-4);
            expect(NUMBERVALUE("3,5%%%")).toBeAlmost(3.5e-6);
            expect(NUMBERVALUE("-3,5%%%%")).toBeAlmost(-3.5e-8);
        });
        it("should throw #VALUE! if no valid number was passed", () => {
            expect(NUMBERVALUE("1.1d2007", "d", "g")).toBe(ErrorCode.VALUE);
            expect(NUMBERVALUE("1,1,1")).toBe(ErrorCode.VALUE);
            expect(NUMBERVALUE("1,1.1")).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("MID", MID => {
        it("should return a substring", () => {
            expect(MID("abcdef", 1, 0)).toBe("");
            expect(MID("abcdef", 1, 1)).toBe("a");
            expect(MID("abcdef", 1, 2)).toBe("ab");
            expect(MID("abcdef", 1, 5)).toBe("abcde");
            expect(MID("abcdef", 1, 6)).toBe("abcdef");
            expect(MID("abcdef", 1, 7)).toBe("abcdef");
            expect(MID("abcdef", 3, 1)).toBe("c");
            expect(MID("abcdef", 3, 2)).toBe("cd");
            expect(MID("abcdef", 6, 1)).toBe("f");
            expect(MID("abcdef", 7, 1)).toBe("");
            expect(MID("abcdef", 8, 1)).toBe("");
        });
        it("should repair spliced surrogate pairs", () => {
            expect(MID("\0\ud83d\udd95\ufffc", 1, 1)).toBe("\0");
            expect(MID("\0\ud83d\udd95\ufffc", 1, 2)).toBe("\0\ufffd");
            expect(MID("\0\ud83d\udd95\ufffc", 1, 3)).toBe("\0\ud83d\udd95");
            expect(MID("\0\ud83d\udd95\ufffc", 1, 4)).toBe("\0\ud83d\udd95\ufffc");
            expect(MID("\0\ud83d\udd95\ufffc", 2, 3)).toBe("\ud83d\udd95\ufffc");
            expect(MID("\0\ud83d\udd95\ufffc", 3, 2)).toBe("\ufffd\ufffc");
            expect(MID("\0\ud83d\udd95\ufffc", 4, 1)).toBe("\ufffc");
            expect(MID("\0\ud83d\udd95\ufffc", 2, 1)).toBe("\ufffd");
            expect(MID("\0\ud83d\udd95\ufffc", 3, 1)).toBe("\ufffd");
            expect(MID("\0\ud83d\udd95\ufffc", 4, 1)).toBe("\ufffc");
        });
        it("should throw #VALUE! for invalid index or string length", () => {
            expect(MID("abc", 0, 1)).toBe(ErrorCode.VALUE);
            expect(MID("abc", 32768, 1)).toBe(ErrorCode.VALUE);
            expect(MID("abc", 1, -1)).toBe(ErrorCode.VALUE);
            expect(MID("abc", 1, 32768)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("REPT", REPT => {
        it("should return the repeated string", () => {
            expect(REPT("abc", 0)).toBe("");
            expect(REPT("abc", 1)).toBe("abc");
            expect(REPT("abc", 2)).toBe("abcabc");
            expect(REPT("abc", 3)).toBe("abcabcabc");
            expect(REPT("abc", 4)).toBe("abcabcabcabc");
        });
        it("should throw #VALUE! for invalid string length", () => {
            expect(REPT("abc", -1)).toBe(ErrorCode.VALUE);
            expect(REPT("a", 32767)).toBeString();
            expect(REPT("a", 32768)).toBe(ErrorCode.VALUE);
            expect(REPT("abcd", 8191)).toBeString();
            expect(REPT("abcd", 8192)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("RIGHT", RIGHT => {
        it("should return the right part", () => {
            expect(RIGHT("\0\ud83d\udd95\ufffc", 0)).toBe("");
            expect(RIGHT("\0\ud83d\udd95\ufffc", 1)).toBe("\ufffc");
            expect(RIGHT("\0\ud83d\udd95\ufffc", 2)).toBe("\ud83d\udd95\ufffc");
            expect(RIGHT("\0\ud83d\udd95\ufffc", 3)).toBe("\0\ud83d\udd95\ufffc");
            expect(RIGHT("\0\ud83d\udd95\ufffc", 4)).toBe("\0\ud83d\udd95\ufffc");
        });
        it("should return one character", () => {
            expect(RIGHT("abc")).toBe("c");
            expect(RIGHT("\0\ud83d\udd95")).toBe("\ud83d\udd95");
        });
        it("should return #VALUE! for invalid length", () => {
            expect(RIGHT("abc", -1)).toBe(ErrorCode.VALUE);
            expect(RIGHT("abc", -2)).toBe(ErrorCode.VALUE);
            expect(RIGHT("abc", 32767)).toBe("abc");
            expect(RIGHT("abc", 32768)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testAliasFunction("RIGHTB", "RIGHT");

    moduleTester.testFunction("ROT13", ROT13 => {
        it("should return the ROW13 encoding of a string", () => {
            expect(ROT13("hello")).toBe("uryyb");
            expect(ROT13("HELLO")).toBe("URYYB");
            expect(ROT13("uryyb")).toBe("hello");
            expect(ROT13("123")).toBe("123");
        });
    });

    moduleTester.testFunction("TEXT", TEXT => {
        it("should return the particular wrong formated Text in English", () => {
            expect(TEXT(2800, "$0.00")).toBe("$2.800");
            expect(TEXT(0.4, "0%")).toBe("40%");
            expect(TEXT(39300.63, "yyyy-MM-dd")).toBe("yyyy-08-dd");
            expect(TEXT(39300.63, "m/d/yyyy h:mm AM/PM")).toBe("7/d/yyyy 3:07 PM");
            expect(TEXT(39300.63, "0.000E+00")).toBe("0.004E+04");
            expect(TEXT(39300.63, "$#,##0.00")).toBe("$39300,63000");
        });
        it("should return the correct formated Text in German", () => {
            expect(TEXT(2800, "0,00 \u20AC")).toBe("2800,00 \u20AC");
            expect(TEXT(0.4, "0%")).toBe("40%");
            expect(TEXT(39300.63, "h:MM")).toBe("15:07");
            expect(TEXT(39300.63, "h:mm")).toBe("15:07");
            expect(TEXT(39300.63, "MM:ss")).toBe("07:12");
            expect(TEXT(39300.63, "mm:ss")).toBe("07:12");
            expect(TEXT(39300.63, "MM")).toBe("08");
            expect(TEXT(39300.63, "mm")).toBe("07");
            expect(TEXT(39300.63, "jjjj-MM-tt")).toBe("2007-08-06");
            expect(TEXT(39300.63, "jjjj-mm-tt")).toBe("2007-07-06");
            expect(TEXT(39300.63, "t.M.jjjj h:mm AM/PM")).toBe("6.8.2007 3:07 PM");
            expect(TEXT(39300.63, "t.m.jjjj h:mm a/p")).toBe("6.7.2007 3:07 p");
            expect(TEXT(39300.63, "0,00E+00")).toBe("3,93E+04");
            expect(TEXT(39300.63, "#.##0,00 \u20ac")).toBe("39.300,63 \u20ac");
        });
    });

    moduleTester.testFunction("TEXTJOIN", TEXTJOIN => {
        it("should return the concatenation of the operands", () => {
            expect(TEXTJOIN("", true, "abc", "", 123, null, true)).toBe("abc123WAHR");
            expect(TEXTJOIN(",", true, "abc", "", 123, null, true)).toBe("abc,123,WAHR");
            expect(TEXTJOIN(",", false, "abc", "", 123, null, true)).toBe("abc,,123,,WAHR");
        });
        it("should concatenate matrix elements", () => {
            expect(TEXTJOIN("", true, mat([["abc", 123], ["", true]]))).toBe("abc123WAHR");
            expect(TEXTJOIN(",", true, mat([["abc", 123], ["", true]]))).toBe("abc,123,WAHR");
            expect(TEXTJOIN(",", false, mat([["abc", 123], ["", true]]))).toBe("abc,123,,WAHR");
        });
        it("should concatenate cell contents", () => {
            expect(TEXTJOIN("", true, r3d("0:0!A2:C3"))).toBe("abc123WAHR");
            expect(TEXTJOIN(",", true, r3d("0:0!A2:C3"))).toBe("abc,123,WAHR");
            expect(TEXTJOIN(",", false, r3d("0:0!A2:C3"))).toBe("abc,,123,,WAHR,");
            expect(TEXTJOIN(",", true, r3d("0:0!A8:B9"))).toBe("");
            expect(TEXTJOIN(",", false, r3d("0:0!A8:B9"))).toBe(",,,");
        });
    });

    moduleTester.testFunction("UNICHAR", UNICHAR => {
        it("should return a Unicode character", () => {
            expect(UNICHAR(1)).toBe("\x01");
            expect(UNICHAR(10)).toBe("\n");
            expect(UNICHAR(65)).toBe("A");
            expect(UNICHAR(0xD7FF)).toBe("\ud7ff");
            expect(UNICHAR(0xE000)).toBe("\ue000");
            expect(UNICHAR(0xFDCF)).toBe("\ufdcf");
            expect(UNICHAR(0xFDF0)).toBe("\ufdf0");
            expect(UNICHAR(0xFFFD)).toBe("\ufffd");
            expect(UNICHAR(0x10000)).toBe("\ud800\udc00");
            expect(UNICHAR(0x11111)).toBe("\ud804\udd11");
            expect(UNICHAR(0x1FFFD)).toBe("\ud83f\udffd");
            expect(UNICHAR(0x20000)).toBe("\ud840\udc00");
            expect(UNICHAR(0x22222)).toBe("\ud848\ude22");
            expect(UNICHAR(0xFFFFD)).toBe("\udbbf\udffd");
            expect(UNICHAR(0x100000)).toBe("\udbc0\udc00");
            expect(UNICHAR(0x10FFFD)).toBe("\udbff\udffd");
        });
        it("should return #N/A! for reserved codes", () => {
            expect(UNICHAR(0xD800)).toBe(ErrorCode.NA);
            expect(UNICHAR(0xDBFF)).toBe(ErrorCode.NA);
            expect(UNICHAR(0xDC00)).toBe(ErrorCode.NA);
            expect(UNICHAR(0xDFFF)).toBe(ErrorCode.NA);
            expect(UNICHAR(0xFDD0)).toBe(ErrorCode.NA);
            expect(UNICHAR(0xFDEF)).toBe(ErrorCode.NA);
            expect(UNICHAR(0xFFFE)).toBe(ErrorCode.NA);
            expect(UNICHAR(0xFFFF)).toBe(ErrorCode.NA);
            expect(UNICHAR(0x1FFFE)).toBe(ErrorCode.NA);
            expect(UNICHAR(0x1FFFF)).toBe(ErrorCode.NA);
            expect(UNICHAR(0x2FFFE)).toBe(ErrorCode.NA);
            expect(UNICHAR(0x2FFFF)).toBe(ErrorCode.NA);
            expect(UNICHAR(0x10FFFE)).toBe(ErrorCode.NA);
            expect(UNICHAR(0x10FFFF)).toBe(ErrorCode.NA);
        });
        it("should return #VALUE! for invalid codes", () => {
            expect(UNICHAR(0)).toBe(ErrorCode.VALUE);
            expect(UNICHAR(-1)).toBe(ErrorCode.VALUE);
            expect(UNICHAR(0x110000)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("UNICODE", UNICODE => {
        it("should return a Unicode character", () => {
            expect(UNICODE("\x01")).toBe(1);
            expect(UNICODE("\n")).toBe(10);
            expect(UNICODE("ABCDEF")).toBe(65);
            expect(UNICODE("\ud7ff")).toBe(0xD7FF);
            expect(UNICODE("\ue000")).toBe(0xE000);
            expect(UNICODE("\ufdcf")).toBe(0xFDCF);
            expect(UNICODE("\ufdf0")).toBe(0xFDF0);
            expect(UNICODE("\ufffd")).toBe(0xFFFD);
            expect(UNICODE("\ud800\udc00")).toBe(0x10000);
            expect(UNICODE("\ud804\udd11")).toBe(0x11111);
            expect(UNICODE("\ud83f\udffd")).toBe(0x1FFFD);
            expect(UNICODE("\ud840\udc00")).toBe(0x20000);
            expect(UNICODE("\ud848\ude22")).toBe(0x22222);
            expect(UNICODE("\udbbf\udffd")).toBe(0xFFFFD);
            expect(UNICODE("\udbc0\udc00")).toBe(0x100000);
            expect(UNICODE("\udbff\udffd")).toBe(0x10FFFD);
        });
        it("should return #VALUE! for empty string", () => {
            expect(UNICODE("")).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("VALUE", VALUE => {
        it("should return numbes", () => {
            expect(VALUE(0)).toBe(0);
            expect(VALUE(-42)).toBe(-42);
        });
        it("should convert strings to numbes", () => {
            expect(VALUE("0")).toBe(0);
            expect(VALUE("-42")).toBe(-42);
            expect(VALUE("200%")).toBe(2);
        });
        it("should throw #VALUE! error code", () => {
            expect(VALUE("")).toBe(ErrorCode.VALUE);
            expect(VALUE(true)).toBe(ErrorCode.VALUE);
        });
    });
});
