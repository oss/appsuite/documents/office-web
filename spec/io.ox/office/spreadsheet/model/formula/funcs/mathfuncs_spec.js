/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import MathFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/mathfuncs";

import { r3d, mat, ErrorCode, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/funcs/mathfuncs", () => {

    // the operations to be applied at the test document
    const OPERATIONS = [
        { name: "setDocumentAttributes", attrs: { document: { cols: 16384, rows: 1048576 } } },
        { name: "insertSheet", sheet: 0, sheetName: "Sheet1" },
        { name: "changeCells", sheet: 0, start: "A2", contents: [
            [1, 2, 3, "abc", "4", true, null, null, "",  5,   ErrorCode.DIV0],
            [1, 2, 4, 8,     16,  32,   64,   128,  256, 512, 1024]
        ] },

        // sheet with auto-filter with active filter rule and manually hidden row (test for SUBTOTAL, bug 49357)
        { name: "insertSheet", sheet: 1, sheetName: "Sheet2" },
        { name: "changeRows", sheet: 1, intervals: "3 8", attrs: { row: { visible: false } } },
        { name: "insertTable", sheet: 1, range: "A1:A4", attrs: { table: { headerRow: true, filtered: true } } },
        { name: "changeTableColumn", sheet: 1, col: 0, attrs: { filter: { type: "discrete", entries: ["1", "4"] } } },
        { name: "changeCells", sheet: 1, start: "A1", contents: [["head"], [1], [2], [4]] },
        { name: "changeCells", sheet: 1, start: "A6", contents: [["head"], [1], [2], [4]] },

        // sheet with auto-filter without active filter rule and manually hidden row (test for SUBTOTAL, bug 49357)
        { name: "insertSheet", sheet: 2, sheetName: "Sheet3" },
        { name: "changeRows", sheet: 2, intervals: "8", attrs: { row: { visible: false } } },
        { name: "insertTable", sheet: 2, range: "A1:A4", attrs: { table: { headerRow: true, filtered: true } } },
        { name: "changeCells", sheet: 2, start: "A1", contents: [["head"], [1], [2], [4]] },
        { name: "changeCells", sheet: 2, start: "A6", contents: [["head"], [1], [2], [4]] }
    ];

    // initialize the test
    const appPromise = createSpreadsheetApp("ooxml", OPERATIONS);
    const moduleTester = createFunctionModuleTester(MathFuncs, appPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("AGGREGATE", AGGREGATE => {
        const mat1 = mat([[3, 2, ErrorCode.NUM, 4, 5, 6]]);
        const mat2 = mat([[3, 2, 4, 5, 6]]);
        const mat3 = mat([[3, 2, 4, 5, 6, ErrorCode.NUM, 2]]);
        it("should return the aggregate of its arguments", () => {
            // AVERAGE
            expect(AGGREGATE(1, 4, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(1, 1, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(1, null, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(1, 2, mat1)).toBe(4);
            expect(AGGREGATE(1, 3, mat1)).toBe(4);
            expect(AGGREGATE(1, 6, mat1)).toBe(4);
            expect(AGGREGATE(1, 7, mat1)).toBe(4);
            // COUNT
            expect(AGGREGATE(2, 6, mat1)).toBe(5);
            // COUNTA
            expect(AGGREGATE(3, 6, mat1)).toBe(5);
            expect(AGGREGATE(3, 4, mat1)).toBe(6);
            // MAX
            expect(AGGREGATE(4, 4, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(4, 6, mat1)).toBe(6);
            // MIN
            expect(AGGREGATE(5, 4, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(5, 6, mat1)).toBe(2);
            // PRODUCT
            expect(AGGREGATE(6, 4, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(6, 4, mat2)).toBe(720);
            expect(AGGREGATE(6, 6, mat1)).toBe(720);
            // STDEV.S
            expect(AGGREGATE(7, 4, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(7, 6, mat1)).toBeCloseTo(1.58113883, 5);
            // STDEV.P
            expect(AGGREGATE(8, 4, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(8, 6, mat1)).toBeCloseTo(1.414213562, 5);
            // SUM
            expect(AGGREGATE(9, 4, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(9, 4, mat2)).toBe(20);
            expect(AGGREGATE(9, 6, mat1)).toBe(20);
            // VAR.S
            expect(AGGREGATE(10, 4, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(10, 4, mat2)).toBe(2.5);
            expect(AGGREGATE(10, 6, mat1)).toBe(2.5);
            // VAR.P
            expect(AGGREGATE(11, 4, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(11, 4, mat2)).toBe(2);
            expect(AGGREGATE(11, 6, mat1)).toBe(2);
            // MEDIAN
            expect(AGGREGATE(12, 4, mat1)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(12, 4, mat2)).toBe(4);
            expect(AGGREGATE(12, 6, mat1)).toBe(4);
            // MODE.SNGL
            expect(AGGREGATE(13, 4, mat3)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(13, 6, mat3)).toBe(2);
            // LARGE
            expect(AGGREGATE(14, 4, mat1, 3)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(14, 6, mat1, 3)).toBe(4);
            // SMALL
            expect(AGGREGATE(15, 4, mat1, 3)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(15, 6, mat1, 3)).toBe(4);
            // PERCENTILE.INC
            expect(AGGREGATE(16, 4, mat1, 0.4)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(16, 6, mat1, 0.4)).toBe(3.6);
            // QUARTILE.INC
            expect(AGGREGATE(17, 4, mat1, 1.5)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(17, 6, mat1, 1.5)).toBe(3);
            // PERCENTILE.EXC
            expect(AGGREGATE(18, 4, mat1, 0.4)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(18, 6, mat1, 0.4)).toBeCloseTo(3.4, 5);
            // QUARTILE.EXC
            expect(AGGREGATE(19, 4, mat1, 1.5)).toBe(ErrorCode.NUM);
            expect(AGGREGATE(19, 6, mat1, 1.5)).toBe(2.5);
        });
    });

    moduleTester.testFunction("CEILING", CEILING => {
        it("should return the rounded number for positive multiplier", () => {
            expect(CEILING(-6, 4)).toBe(-8);
            expect(CEILING(-5, 4)).toBe(-8);
            expect(CEILING(-4, 4)).toBe(-4);
            expect(CEILING(-3, 4)).toBe(-4);
            expect(CEILING(-2, 4)).toBe(-4);
            expect(CEILING(-1, 4)).toBe(-4);
            expect(CEILING(0, 4)).toBe(0);
            expect(CEILING(1, 4)).toBe(4);
            expect(CEILING(2, 4)).toBe(4);
            expect(CEILING(3, 4)).toBe(4);
            expect(CEILING(4, 4)).toBe(4);
            expect(CEILING(5, 4)).toBe(8);
            expect(CEILING(6, 4)).toBe(8);
            expect(CEILING(2.05, 0.2)).toBe(2.2);
        });
        it("should return the rounded number for negative multiplier", () => {
            expect(CEILING(-6, -4)).toBe(-8);
            expect(CEILING(-5, -4)).toBe(-8);
            expect(CEILING(-4, -4)).toBe(-4);
            expect(CEILING(-3, -4)).toBe(-4);
            expect(CEILING(-2, -4)).toBe(-4);
            expect(CEILING(-1, -4)).toBe(-4);
            expect(CEILING(0, -4)).toBe(0);
            expect(CEILING(-2.05, -0.2)).toBe(-2.2);
        });
        it("should return zero, if multiplier is zero", () => {
            expect(CEILING(0, 0)).toBe(0);
            expect(CEILING(10, 0)).toBe(0);
            expect(CEILING(-10, 0)).toBe(0);
        });
        it("should return #NUM! for positive number and negative multiplier", () => {
            expect(CEILING(10, -1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("CEILING.MATH", CEILING_MATH => {
        it("should return the rounded number for positive multiplier", () => {
            expect(CEILING_MATH(-6, 4)).toBe(-4);
            expect(CEILING_MATH(-5, 4)).toBe(-4);
            expect(CEILING_MATH(-4, 4)).toBe(-4);
            expect(CEILING_MATH(-3, 4)).toBe(-0);
            expect(CEILING_MATH(-2, 4)).toBe(-0);
            expect(CEILING_MATH(-1, 4)).toBe(-0);
            expect(CEILING_MATH(0, 4)).toBe(0);
            expect(CEILING_MATH(1, 4)).toBe(4);
            expect(CEILING_MATH(2, 4)).toBe(4);
            expect(CEILING_MATH(3, 4)).toBe(4);
            expect(CEILING_MATH(4, 4)).toBe(4);
            expect(CEILING_MATH(5, 4)).toBe(8);
            expect(CEILING_MATH(6, 4)).toBe(8);
            expect(CEILING_MATH(2.05, 0.2)).toBe(2.2);
        });
        it("should return the rounded number for negative multiplier", () => {
            expect(CEILING_MATH(-6, -4)).toBe(-4);
            expect(CEILING_MATH(-5, -4)).toBe(-4);
            expect(CEILING_MATH(-4, -4)).toBe(-4);
            expect(CEILING_MATH(-3, -4)).toBe(-0);
            expect(CEILING_MATH(-2, -4)).toBe(-0);
            expect(CEILING_MATH(-1, -4)).toBe(-0);
            expect(CEILING_MATH(0, -4)).toBe(0);
            expect(CEILING_MATH(1, -4)).toBe(4);
            expect(CEILING_MATH(2, -4)).toBe(4);
            expect(CEILING_MATH(3, -4)).toBe(4);
            expect(CEILING_MATH(4, -4)).toBe(4);
            expect(CEILING_MATH(5, -4)).toBe(8);
            expect(CEILING_MATH(6, -4)).toBe(8);
            expect(CEILING_MATH(2.05, -0.2)).toBe(2.2);
        });
        it("should round down negative numbers if specified", () => {
            expect(CEILING_MATH(1, 4, 0)).toBe(4);
            expect(CEILING_MATH(1, 4, 1)).toBe(4);
            expect(CEILING_MATH(-1, 4, 0)).toBe(-0);
            expect(CEILING_MATH(-1, 4, 1)).toBe(-4);
        });
        it("should return zero, if multiplier is zero", () => {
            expect(CEILING_MATH(0, 0)).toBe(0);
            expect(CEILING_MATH(10, 0)).toBe(0);
            expect(CEILING_MATH(-10, 0)).toBe(0);
        });
    });

    moduleTester.testFunction("EVEN", EVEN => {
        it("should return the rounded number", () => {
            expect(EVEN(-3.5)).toBe(-4);
            expect(EVEN(-3)).toBe(-4);
            expect(EVEN(-2.5)).toBe(-4);
            expect(EVEN(-2)).toBe(-2);
            expect(EVEN(-1.5)).toBe(-2);
            expect(EVEN(-1)).toBe(-2);
            expect(EVEN(-0.5)).toBe(-2);
            expect(EVEN(0)).toBe(0);
            expect(EVEN(0.5)).toBe(2);
            expect(EVEN(1)).toBe(2);
            expect(EVEN(1.5)).toBe(2);
            expect(EVEN(2)).toBe(2);
            expect(EVEN(2.5)).toBe(4);
            expect(EVEN(3)).toBe(4);
            expect(EVEN(3.5)).toBe(4);
        });
    });

    moduleTester.testFunction("FLOOR", FLOOR => {
        it("should return the rounded number for positive multiplier", () => {
            expect(FLOOR(-6, 4)).toBe(-4);
            expect(FLOOR(-5, 4)).toBe(-4);
            expect(FLOOR(-4, 4)).toBe(-4);
            expect(FLOOR(-3, 4)).toBe(-0);
            expect(FLOOR(-2, 4)).toBe(-0);
            expect(FLOOR(-1, 4)).toBe(-0);
            expect(FLOOR(0, 4)).toBe(0);
            expect(FLOOR(1, 4)).toBe(0);
            expect(FLOOR(2, 4)).toBe(0);
            expect(FLOOR(3, 4)).toBe(0);
            expect(FLOOR(4, 4)).toBe(4);
            expect(FLOOR(5, 4)).toBe(4);
            expect(FLOOR(6, 4)).toBe(4);
            expect(FLOOR(2.35, 0.2)).toBe(2.2);
        });
        it("should return the rounded number for negative multiplier", () => {
            expect(FLOOR(-6, -4)).toBe(-4);
            expect(FLOOR(-5, -4)).toBe(-4);
            expect(FLOOR(-4, -4)).toBe(-4);
            expect(FLOOR(-3, -4)).toBe(-0);
            expect(FLOOR(-2, -4)).toBe(-0);
            expect(FLOOR(-1, -4)).toBe(-0);
            expect(FLOOR(0, -4)).toBe(0);
            expect(FLOOR(-2.35, -0.2)).toBe(-2.2);
        });
        it("should return zero, if multiplier is zero", () => {
            expect(FLOOR(0, 0)).toBe(0);
            expect(FLOOR(10, 0)).toBe(0);
            expect(FLOOR(-10, 0)).toBe(0);
        });
        it("should return #NUM! for positive number and negative multiplier", () => {
            expect(FLOOR(10, -1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("FLOOR.MATH", FLOOR_MATH => {
        it("should return the rounded number for positive multiplier", () => {
            expect(FLOOR_MATH(-6, 4)).toBe(-8);
            expect(FLOOR_MATH(-5, 4)).toBe(-8);
            expect(FLOOR_MATH(-4, 4)).toBe(-4);
            expect(FLOOR_MATH(-3, 4)).toBe(-4);
            expect(FLOOR_MATH(-2, 4)).toBe(-4);
            expect(FLOOR_MATH(-1, 4)).toBe(-4);
            expect(FLOOR_MATH(0, 4)).toBe(0);
            expect(FLOOR_MATH(1, 4)).toBe(0);
            expect(FLOOR_MATH(2, 4)).toBe(0);
            expect(FLOOR_MATH(3, 4)).toBe(0);
            expect(FLOOR_MATH(4, 4)).toBe(4);
            expect(FLOOR_MATH(5, 4)).toBe(4);
            expect(FLOOR_MATH(6, 4)).toBe(4);
            expect(FLOOR_MATH(2.35, 0.2)).toBe(2.2);
        });
        it("should return the rounded number for negative multiplier", () => {
            expect(FLOOR_MATH(-6, -4)).toBe(-8);
            expect(FLOOR_MATH(-5, -4)).toBe(-8);
            expect(FLOOR_MATH(-4, -4)).toBe(-4);
            expect(FLOOR_MATH(-3, -4)).toBe(-4);
            expect(FLOOR_MATH(-2, -4)).toBe(-4);
            expect(FLOOR_MATH(-1, -4)).toBe(-4);
            expect(FLOOR_MATH(0, -4)).toBe(0);
            expect(FLOOR_MATH(1, -4)).toBe(0);
            expect(FLOOR_MATH(2, -4)).toBe(0);
            expect(FLOOR_MATH(3, -4)).toBe(0);
            expect(FLOOR_MATH(4, -4)).toBe(4);
            expect(FLOOR_MATH(5, -4)).toBe(4);
            expect(FLOOR_MATH(6, -4)).toBe(4);
            expect(FLOOR_MATH(2.35, -0.2)).toBe(2.2);
        });
        it("should round up negative numbers if specified", () => {
            expect(FLOOR_MATH(1, 4, 0)).toBe(0);
            expect(FLOOR_MATH(1, 4, 1)).toBe(0);
            expect(FLOOR_MATH(-1, 4, 0)).toBe(-4);
            expect(FLOOR_MATH(-1, 4, 1)).toBe(-0);
        });
        it("should return zero, if multiplier is zero", () => {
            expect(FLOOR_MATH(0, 0)).toBe(0);
            expect(FLOOR_MATH(10, 0)).toBe(0);
            expect(FLOOR_MATH(-10, 0)).toBe(0);
        });
    });

    moduleTester.testFunction("MROUND", MROUND => {
        it("should return the positive rounded number", () => {
            expect(MROUND(0, 4)).toBe(0);
            expect(MROUND(1, 4)).toBe(0);
            expect(MROUND(2, 4)).toBe(4);
            expect(MROUND(3, 4)).toBe(4);
            expect(MROUND(4, 4)).toBe(4);
            expect(MROUND(5, 4)).toBe(4);
            expect(MROUND(6, 4)).toBe(8);
            expect(MROUND(2.15, 0.2)).toBe(2.2);
            expect(MROUND(2.25, 0.2)).toBe(2.2);
        });
        it("should return the negative rounded number", () => {
            expect(MROUND(0, -4)).toBe(0);
            expect(MROUND(-1, -4)).toBe(-0);
            expect(MROUND(-2, -4)).toBe(-4);
            expect(MROUND(-3, -4)).toBe(-4);
            expect(MROUND(-4, -4)).toBe(-4);
            expect(MROUND(-5, -4)).toBe(-4);
            expect(MROUND(-6, -4)).toBe(-8);
            expect(MROUND(-2.15, -0.2)).toBe(-2.2);
            expect(MROUND(-2.25, -0.2)).toBe(-2.2);
        });
        it("should return zero, if multiplier is zero", () => {
            expect(MROUND(0, 0)).toBe(0);
            expect(MROUND(10, 0)).toBe(0);
            expect(MROUND(-10, 0)).toBe(0);
        });
        it("should throw #NUM! error code for mismatching signs", () => {
            expect(MROUND(1, -1)).toBe(ErrorCode.NUM);
            expect(MROUND(-1, 1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("ODD", ODD => {
        it("should return the rounded number", () => {
            expect(ODD(-3.5)).toBe(-5);
            expect(ODD(-3)).toBe(-3);
            expect(ODD(-2.5)).toBe(-3);
            expect(ODD(-2)).toBe(-3);
            expect(ODD(-1.5)).toBe(-3);
            expect(ODD(-1)).toBe(-1);
            expect(ODD(-0.5)).toBe(-1);
            expect(ODD(0)).toBe(1);
            expect(ODD(0.5)).toBe(1);
            expect(ODD(1)).toBe(1);
            expect(ODD(1.5)).toBe(3);
            expect(ODD(2)).toBe(3);
            expect(ODD(2.5)).toBe(3);
            expect(ODD(3)).toBe(3);
            expect(ODD(3.5)).toBe(5);
        });
    });

    moduleTester.testFunction("ROUND", ROUND => {
        it("should return the positive rounded number", () => {
            expect(ROUND(123.456, 3)).toBeAlmost(123.456);
            expect(ROUND(123.456, 2)).toBeAlmost(123.46);
            expect(ROUND(123.456, 1)).toBeAlmost(123.5);
            expect(ROUND(123.456, 0)).toBe(123);
            expect(ROUND(123.456, -1)).toBe(120);
            expect(ROUND(123.456, -2)).toBe(100);
            expect(ROUND(123.456, -3)).toBe(0);
            expect(ROUND(123.456, -400)).toBe(0);
        });
        it("should return the negative rounded number", () => {
            expect(ROUND(-123.456, 3)).toBeAlmost(-123.456);
            expect(ROUND(-123.456, 2)).toBeAlmost(-123.46);
            expect(ROUND(-123.456, 1)).toBeAlmost(-123.5);
            expect(ROUND(-123.456, 0)).toBe(-123);
            expect(ROUND(-123.456, -1)).toBe(-120);
            expect(ROUND(-123.456, -2)).toBe(-100);
            expect(ROUND(-123.456, -3)).toBe(0);
            expect(ROUND(-123.456, -400)).toBe(0);
        });
        it("should accept zero value", () => {
            expect(ROUND(0, 1)).toBe(0);
            expect(ROUND(0, 0)).toBe(0);
            expect(ROUND(0, -1)).toBe(0);
        });
    });

    moduleTester.testFunction("ROUNDDOWN", ROUNDDOWN => {
        it("should return the positive rounded number", () => {
            expect(ROUNDDOWN(123.456, 3)).toBeAlmost(123.456);
            expect(ROUNDDOWN(123.456, 2)).toBeAlmost(123.45);
            expect(ROUNDDOWN(123.456, 1)).toBeAlmost(123.4);
            expect(ROUNDDOWN(123.456, 0)).toBe(123);
            expect(ROUNDDOWN(123.456, -1)).toBe(120);
            expect(ROUNDDOWN(123.456, -2)).toBe(100);
            expect(ROUNDDOWN(123.456, -3)).toBe(0);
            expect(ROUNDDOWN(123.456, -400)).toBe(0);
        });
        it("should return the negative rounded number", () => {
            expect(ROUNDDOWN(-123.456, 3)).toBeAlmost(-123.456);
            expect(ROUNDDOWN(-123.456, 2)).toBeAlmost(-123.45);
            expect(ROUNDDOWN(-123.456, 1)).toBeAlmost(-123.4);
            expect(ROUNDDOWN(-123.456, 0)).toBe(-123);
            expect(ROUNDDOWN(-123.456, -1)).toBe(-120);
            expect(ROUNDDOWN(-123.456, -2)).toBe(-100);
            expect(ROUNDDOWN(-123.456, -3)).toBe(0);
            expect(ROUNDDOWN(-123.456, -400)).toBe(0);
        });
        it("should accept zero value", () => {
            expect(ROUNDDOWN(0, 1)).toBe(0);
            expect(ROUNDDOWN(0, 0)).toBe(0);
            expect(ROUNDDOWN(0, -1)).toBe(0);
        });
    });

    moduleTester.testFunction("ROUNDUP", ROUNDUP => {
        it("should return the positive rounded number", () => {
            expect(ROUNDUP(123.456, 3)).toBeAlmost(123.456);
            expect(ROUNDUP(123.456, 2)).toBeAlmost(123.46);
            expect(ROUNDUP(123.456, 1)).toBeAlmost(123.5);
            expect(ROUNDUP(123.456, 0)).toBe(124);
            expect(ROUNDUP(123.456, -1)).toBe(130);
            expect(ROUNDUP(123.456, -2)).toBe(200);
            expect(ROUNDUP(123.456, -3)).toBe(1e3);
            expect(ROUNDUP(123.456, -4)).toBe(1e4);
            expect(ROUNDUP(123.456, -307)).toBeAlmost(1e307);
        });
        it("should return the negative rounded number", () => {
            expect(ROUNDUP(-123.456, 3)).toBeAlmost(-123.456);
            expect(ROUNDUP(-123.456, 2)).toBeAlmost(-123.46);
            expect(ROUNDUP(-123.456, 1)).toBeAlmost(-123.5);
            expect(ROUNDUP(-123.456, 0)).toBe(-124);
            expect(ROUNDUP(-123.456, -1)).toBe(-130);
            expect(ROUNDUP(-123.456, -2)).toBe(-200);
            expect(ROUNDUP(-123.456, -3)).toBe(-1e3);
            expect(ROUNDUP(-123.456, -4)).toBe(-1e4);
            expect(ROUNDUP(-123.456, -307)).toBeAlmost(-1e307);
        });
        it("should accept zero value", () => {
            expect(ROUNDUP(0, 1)).toBe(0);
            expect(ROUNDUP(0, 0)).toBe(0);
            expect(ROUNDUP(0, -1)).toBe(0);
        });
    });

    moduleTester.testFunction("SERIESSUM", SERIESSUM => {
        it("should return the sum of a power series", () => {
            expect(SERIESSUM(2, 2, 1, 1)).toBe(4);
            expect(SERIESSUM(2, 2, 1, "1")).toBe(4);
            expect(SERIESSUM(2, 2, 1, mat([[1]]))).toBe(4);
            expect(SERIESSUM(2, 2, 1, mat([[1, 1]]))).toBe(12);
            expect(SERIESSUM(2, 2, 1, mat([[1, 1, 1]]))).toBe(28);
            expect(SERIESSUM(2, 2, 1, mat([[1, 1], [1, 1]]))).toBe(60);
            expect(SERIESSUM(2, 2, 1, mat([[1, 1], [2, 1]]))).toBe(76);
            expect(SERIESSUM(2, 2, 2, mat([[1, 1], [1, 1]]))).toBe(340);
            expect(SERIESSUM(2, 2, 2, mat([[1, 2], [3, 4]]))).toBe(1252);
        });
        it("should throw #VALUE! error code for invalid data", () => {
            expect(SERIESSUM(2, 2, 1, "abc")).toBe(ErrorCode.VALUE);
            expect(SERIESSUM(2, 2, 1, mat([[1, "abc"]]))).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("SUBTOTAL", { toOperand: 1 }, SUBTOTAL => {
        const mat1 = mat([[3, 2, 4, 5, 6]]);
        const mat2 = mat([[3, 2, ErrorCode.NUM, 4, 5, 6]]);
        it("should return the subtotal of its arguments", () => {
            // AVERAGE
            expect(SUBTOTAL(1, mat1)).toBe(4);
            expect(SUBTOTAL(101, mat1)).toBe(4);
            // COUNT
            expect(SUBTOTAL(2, mat1)).toBe(5);
            expect(SUBTOTAL(102, mat1)).toBe(5);
            // COUNTA
            expect(SUBTOTAL(3, mat1)).toBe(5);
            expect(SUBTOTAL(103, mat1)).toBe(5);
            // MAX
            expect(SUBTOTAL(4, mat1)).toBe(6);
            expect(SUBTOTAL(104, mat1)).toBe(6);
            // MIN
            expect(SUBTOTAL(5, mat1)).toBe(2);
            expect(SUBTOTAL(105, mat1)).toBe(2);
            // PRODUCT
            expect(SUBTOTAL(6, mat1)).toBe(720);
            expect(SUBTOTAL(106, mat1)).toBe(720);
            // STDEV.S
            expect(SUBTOTAL(7, mat1)).toBeCloseTo(1.58113883, 5);
            expect(SUBTOTAL(107, mat1)).toBeCloseTo(1.58113883, 5);
            // STDEV.P
            expect(SUBTOTAL(8, mat1)).toBeCloseTo(1.414213562, 5);
            expect(SUBTOTAL(108, mat1)).toBeCloseTo(1.414213562, 5);
            // SUM
            expect(SUBTOTAL(9, mat1)).toBe(20);
            expect(SUBTOTAL(109, mat1)).toBe(20);
            // VAR.S
            expect(SUBTOTAL(10, mat1)).toBe(2.5);
            expect(SUBTOTAL(110, mat1)).toBe(2.5);
            // VAR.P
            expect(SUBTOTAL(11, mat1)).toBe(2);
            expect(SUBTOTAL(111, mat1)).toBe(2);
        });
        it("should skip all hidden rows in a sheet with active auto-filter", () => {
            expect(SUBTOTAL(9, r3d("1:1!A2:A4"))).toBe(5);
            expect(SUBTOTAL(109, r3d("1:1!A2:A4"))).toBe(5);
            expect(SUBTOTAL(9, r3d("1:1!A7:A9"))).toBe(5);
            expect(SUBTOTAL(109, r3d("1:1!A7:A9"))).toBe(5);
        });
        it("should include hidden rows in a sheet without active auto-filter", () => {
            expect(SUBTOTAL(9, r3d("2:2!A2:A4"))).toBe(7);
            expect(SUBTOTAL(109, r3d("2:2!A2:A4"))).toBe(7);
            expect(SUBTOTAL(9, r3d("2:2!A7:A9"))).toBe(7);
            expect(SUBTOTAL(109, r3d("2:2!A7:A9"))).toBe(5);
        });
        it("should return #NUM! for invalid function ID", () => {
            expect(SUBTOTAL(55, mat2)).toBe(ErrorCode.VALUE);
        });
        it("should return embedded error code", () => {
            expect(SUBTOTAL(1, mat2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("SUM", SUM => {
        it("should return the sum of its arguments", () => {
            expect(SUM(1, 2, 3)).toBe(6);
            expect(SUM(1, true, 3)).toBe(5);
            expect(SUM(1, "2", 3)).toBe(6);
            expect(SUM(1, null, 3)).toBe(4);
        });
        it("should return the sum of a matrix", () => {
            expect(SUM(mat([[1, 2], [3, 4]]))).toBe(10);
            expect(SUM(mat([[1, true], [3, 4]]))).toBe(8);
            expect(SUM(mat([[1, "2"], [3, 4]]))).toBe(8);
            expect(SUM(mat([[1, "abc"], [3, 4]]))).toBe(8);
            expect(SUM(mat([[1, ""], [3, 4]]))).toBe(8);
        });
        it("should throw #VALUE! error for invalid arguments", () => {
            expect(SUM(1, "abc", 3)).toBe(ErrorCode.VALUE);
            expect(SUM(1, "", 3)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("SUMIF", SUMIF => {
        it("should return the sum of the matching cells", () => {
            expect(SUMIF(r3d("0:0!A2:J2"), ">2")).toBe(8);
        });
        it("should return the sum of another range", () => {
            expect(SUMIF(r3d("0:0!A2:K2"), ">2", r3d("0:0!A3:K3"))).toBe(516);
        });
        it("should ignore the size of the data range", () => {
            expect(SUMIF(r3d("0:0!A2:K2"), ">2", r3d("0:0!A3:A4"))).toBe(516);
        });
    });

    moduleTester.testFunction("SUMIFS", SUMIFS => {
        it("should return the sum of the matching cells", () => {
            expect(SUMIFS(r3d("0:0!A3:J3"), r3d("0:0!A2:J2"), ">2")).toBe(516);
            expect(SUMIFS(r3d("0:0!A3:J3"), r3d("0:0!A2:J2"), ">2", r3d("0:0!A3:J3"), "<32")).toBe(4);
        });
    });

    moduleTester.testFunction("TRUNC", TRUNC => {
        it("should return the positive rounded number", () => {
            expect(TRUNC(123.456, 3)).toBeAlmost(123.456);
            expect(TRUNC(123.456, 2)).toBeAlmost(123.45);
            expect(TRUNC(123.456, 1)).toBeAlmost(123.4);
            expect(TRUNC(123.456, 0)).toBe(123);
            expect(TRUNC(123.456)).toBe(123);
            expect(TRUNC(123.456, -1)).toBe(120);
            expect(TRUNC(123.456, -2)).toBe(100);
            expect(TRUNC(123.456, -3)).toBe(0);
            expect(TRUNC(123.456, -400)).toBe(0);
        });
        it("should return the negative rounded number", () => {
            expect(TRUNC(-123.456, 3)).toBeAlmost(-123.456);
            expect(TRUNC(-123.456, 2)).toBeAlmost(-123.45);
            expect(TRUNC(-123.456, 1)).toBeAlmost(-123.4);
            expect(TRUNC(-123.456, 0)).toBe(-123);
            expect(TRUNC(-123.456)).toBe(-123);
            expect(TRUNC(-123.456, -1)).toBe(-120);
            expect(TRUNC(-123.456, -2)).toBe(-100);
            expect(TRUNC(-123.456, -3)).toBe(0);
            expect(TRUNC(-123.456, -400)).toBe(0);
        });
        it("should accept zero value", () => {
            expect(TRUNC(0, 1)).toBe(0);
            expect(TRUNC(0, 0)).toBe(0);
            expect(TRUNC(0, -1)).toBe(0);
        });
    });
});
