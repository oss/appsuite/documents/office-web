/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ConversionFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/conversionfuncs";

import { ErrorCode, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/funcs/conversionfuncs", () => {

    // initialize the test
    const appPromise = createSpreadsheetApp("ooxml");
    const moduleTester = createFunctionModuleTester(ConversionFuncs, appPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("ARABIC", ARABIC => {
        it("should convert roman to decimal", () => {
            expect(ARABIC("")).toBe(0);
            expect(ARABIC("I")).toBe(1);
            expect(ARABIC("V")).toBe(5);
            expect(ARABIC("X")).toBe(10);
            expect(ARABIC("L")).toBe(50);
            expect(ARABIC("C")).toBe(100);
            expect(ARABIC("D")).toBe(500);
            expect(ARABIC("M")).toBe(1000);
            expect(ARABIC("II")).toBe(2);
            expect(ARABIC("III")).toBe(3);
            expect(ARABIC("IIII")).toBe(4);
            expect(ARABIC("IIIII")).toBe(5);
            expect(ARABIC("MMMDDDCCCLLLXXXVVVIII")).toBe(4998);
            expect(ARABIC("DMCDLCXLVXIV")).toBe(999);
            expect(ARABIC("MDCLXVI")).toBe(1666);
            expect(ARABIC("IVXLCDM")).toBe(334);
            expect(ARABIC("M".repeat(255))).toBe(255000);
        });
        it("should accept lower-case strings", () => {
            expect(ARABIC("mdclxvi")).toBe(1666);
            expect(ARABIC("mDcLxVi")).toBe(1666);
            expect(ARABIC("MdClXvI")).toBe(1666);
        });
        it("should trim white-space", () => {
            expect(ARABIC("  XLII  ")).toBe(42);
        });
        it("should accept leading minus sign", () => {
            expect(ARABIC("-XLII")).toBe(-42);
            expect(ARABIC("  -XLII  ")).toBe(-42);
            expect(ARABIC("-" + "M".repeat(254))).toBe(-254000);
        });
        it("should throw #VALUE! error code for invalid parameter", () => {
            expect(ARABIC("abc")).toBe(ErrorCode.VALUE);
            expect(ARABIC("I I")).toBe(ErrorCode.VALUE);
            expect(ARABIC("- I")).toBe(ErrorCode.VALUE);
            expect(ARABIC("I-")).toBe(ErrorCode.VALUE);
            expect(ARABIC("42")).toBe(ErrorCode.VALUE);
            expect(ARABIC("M".repeat(256))).toBe(ErrorCode.VALUE);
            expect(ARABIC("-" + "M".repeat(255))).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("BASE", BASE => {
        it("should convert any base to decimal", () => {
            expect(BASE(0, 2)).toBe("0");
            expect(BASE(42, 2)).toBe("101010");
            expect(BASE(42, 3)).toBe("1120");
            expect(BASE(42, 4)).toBe("222");
            expect(BASE(42, 5)).toBe("132");
            expect(BASE(42, 6)).toBe("110");
            expect(BASE(42, 7)).toBe("60");
            expect(BASE(42, 8)).toBe("52");
            expect(BASE(42, 10)).toBe("42");
            expect(BASE(42, 15)).toBe("2C");
            expect(BASE(42, 16)).toBe("2A");
            expect(BASE(42, 22)).toBe("1K");
            expect(BASE(1295, 36)).toBe("ZZ");
            expect(BASE(Math.pow(2, 53) - 1, 2)).toMatch(/^1{53}$/);
            expect(BASE(Math.pow(2, 53) - 1, 5)).toBe("33421042423033203202431");
            expect(BASE(Math.pow(2, 53) - 1, 9)).toBe("47664754584305344");
            expect(BASE(Math.pow(2, 53) - 1, 17)).toBe("F7DED8C9E1F8E");
            expect(BASE(Math.pow(2, 53) - 1, 32)).toBe("7VVVVVVVVVV");
            expect(BASE(Math.pow(2, 53) - 1, 36)).toBe("2GOSA7PA2GV");
        });
        it("should expand result with zeros", () => {
            expect(BASE(3, 2, 0)).toBe("11");
            expect(BASE(3, 2, 1)).toBe("11");
            expect(BASE(3, 2, 2)).toBe("11");
            expect(BASE(3, 2, 4)).toBe("0011");
            expect(BASE(3, 2, 10)).toBe("0000000011");
            expect(BASE(3, 2, 255)).toMatch(/^0{253}11$/);
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(BASE(-1, 2)).toBe(ErrorCode.NUM);
            expect(BASE(-1, Math.pow(2, 53))).toBe(ErrorCode.NUM);
            expect(BASE(1, 1)).toBe(ErrorCode.NUM);
            expect(BASE(1, 37)).toBe(ErrorCode.NUM);
            expect(BASE(1, 2, -1)).toBe(ErrorCode.NUM);
            expect(BASE(1, 2, 256)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("BIN2DEC", BIN2DEC => {
        it("should convert binary to decimal", () => {
            expect(BIN2DEC("")).toBe(0);
            expect(BIN2DEC("0")).toBe(0);
            expect(BIN2DEC("101010")).toBe(42);
            expect(BIN2DEC("0111111111")).toBe(511);
            expect(BIN2DEC("1000000000")).toBe(-512);
            expect(BIN2DEC("1111111111")).toBe(-1);
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(BIN2DEC("2")).toBe(ErrorCode.NUM);
            expect(BIN2DEC("abc")).toBe(ErrorCode.NUM);
            expect(BIN2DEC("10000000000")).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("BIN2OCT", BIN2OCT => {
        it("should convert binary to octal", () => {
            expect(BIN2OCT("")).toBe("0");
            expect(BIN2OCT("0")).toBe("0");
            expect(BIN2OCT("100010")).toBe("42");
            expect(BIN2OCT("0111111111")).toBe("777");
            expect(BIN2OCT("1000000000")).toBe("7777777000");
            expect(BIN2OCT("1111111111")).toBe("7777777777");
        });
        it("should expand the result with leading zeros", () => {
            expect(BIN2OCT("100010", 2)).toBe("42");
            expect(BIN2OCT("100010", 4)).toBe("0042");
            expect(BIN2OCT("100010", 10)).toBe("0000000042");
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(BIN2OCT("abc")).toBe(ErrorCode.NUM);
            expect(BIN2OCT("10000000000")).toBe(ErrorCode.NUM);
            expect(BIN2OCT("100010", 1)).toBe(ErrorCode.NUM);
            expect(BIN2OCT("100010", 11)).toBe(ErrorCode.NUM);
            expect(BIN2OCT("0", 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("BIN2HEX", BIN2HEX => {
        it("should convert binary to hexadecimal", () => {
            expect(BIN2HEX("")).toBe("0");
            expect(BIN2HEX("0")).toBe("0");
            expect(BIN2HEX("1000010")).toBe("42");
            expect(BIN2HEX("0111111111")).toBe("1FF");
            expect(BIN2HEX("1000000000")).toBe("FFFFFFFE00");
            expect(BIN2HEX("1111111111")).toBe("FFFFFFFFFF");
        });
        it("should expand the result with leading zeros", () => {
            expect(BIN2HEX("1000010", 2)).toBe("42");
            expect(BIN2HEX("1000010", 4)).toBe("0042");
            expect(BIN2HEX("1000010", 10)).toBe("0000000042");
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(BIN2HEX("efg")).toBe(ErrorCode.NUM);
            expect(BIN2HEX("10000000000")).toBe(ErrorCode.NUM);
            expect(BIN2HEX("1000010", 1)).toBe(ErrorCode.NUM);
            expect(BIN2HEX("1000010", 11)).toBe(ErrorCode.NUM);
            expect(BIN2HEX("0", 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("COLOR", COLOR => {
        it("should return RGB color values", () => {
            expect(COLOR(0, 0, 0)).toBe(0);
            expect(COLOR(1, 2, 3)).toBe(0x00010203);
            expect(COLOR(252, 253, 254)).toBe(0x00fcfdfe);
        });
        it("should return RGBA color values", () => {
            expect(COLOR(0, 0, 0, 0)).toBe(0);
            expect(COLOR(1, 2, 3, 4)).toBe(0x04010203);
            expect(COLOR(252, 253, 254, 255)).toBe(0xfffcfdfe);
        });
        it("should throw error code for invalid arguments", () => {
            expect(COLOR(-1, 0, 0, 0)).toBe(ErrorCode.VALUE);
            expect(COLOR(256, 0, 0, 0)).toBe(ErrorCode.VALUE);
            expect(COLOR(0, -1, 0, 0)).toBe(ErrorCode.VALUE);
            expect(COLOR(0, 256, 0, 0)).toBe(ErrorCode.VALUE);
            expect(COLOR(0, 0, -1, 0)).toBe(ErrorCode.VALUE);
            expect(COLOR(0, 0, 256, 0)).toBe(ErrorCode.VALUE);
            expect(COLOR(0, 0, 0, -1)).toBe(ErrorCode.VALUE);
            expect(COLOR(0, 0, 0, 256)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("CONVERT", CONVERT => {
        it("should return correct results for length", () => {
            expect(CONVERT(1, "m", "m")).toBe(1);
            expect(CONVERT(1, "m", "pm")).toBe(1e12);
            expect(CONVERT(1, "pm", "m")).toBe(1e-12);
            expect(CONVERT(1, "mi", "m")).toBe(1609.344);
            expect(CONVERT(1, "Nmi", "m")).toBe(1852);
            expect(CONVERT(1, "in", "m")).toBe(0.0254);
            expect(CONVERT(1, "ft", "m")).toBe(0.3048);
            expect(CONVERT(1, "yd", "m")).toBe(0.9144);
            expect(CONVERT(1, "ang", "m")).toBe(1E-10);
            expect(CONVERT(1, "kang", "m")).toBeAlmost(1E-7);
            expect(CONVERT(1, "Pica", "m")).toBe(0.003175 / 9);
            expect(CONVERT(1, "ell", "m")).toBe(1.143);
            expect(CONVERT(1, "parsec", "m")).toBeAlmost(3.08567758128155E+16);
            expect(CONVERT(1, "ly", "m")).toBeAlmost(9.4607304725808E+15);
            expect(CONVERT(1, "survey_mi", "m")).toBeAlmost(1609.3472186944375);
            expect(CONVERT(100, "ft", "m")).toBe(30.48);
            expect(CONVERT(30.48, "ft", "m")).toBe(9.290304);
            // conflict: "pc" can be Parsec and picocalorie
            expect(CONVERT(1, "pc", "Pm")).toBeAlmost(30.8567758128155);
            expect(CONVERT(1, "Pm", "pc")).toBeAlmost(1 / 30.8567758128155);
        });
        it("should return correct results for area", () => {
            expect(CONVERT(1, "m2", "m2")).toBe(1);
            expect(CONVERT(1, "mi2", "m2")).toBe(2589988.110336);
            expect(CONVERT(1, "Nmi2", "m2")).toBe(3429904);
            expect(CONVERT(1, "in2", "m2")).toBe(0.00064516);
            expect(CONVERT(1, "ft2", "m2")).toBe(0.09290304);
            expect(CONVERT(1, "yd2", "m2")).toBeAlmost(0.83612736);
            expect(CONVERT(1, "ang2", "m2")).toBe(1E-20);
            expect(CONVERT(1, "Pica2", "m2")).toBeAlmost(1.2445216049382715E-07);
            expect(CONVERT(1, "ly2", "m2")).toBeAlmost(8.95054210748189E+31);
        });
        it("should return correct results for volume", () => {
            expect(CONVERT(1, "m3", "l")).toBe(1000);
            expect(CONVERT(1, "mi3", "l")).toBeAlmost(4.16818182544058E+12);
            expect(CONVERT(1, "Nmi3", "l")).toBeAlmost(6.352182208E+12);
            expect(CONVERT(1, "in3", "l")).toBeAlmost(0.016387064);
            expect(CONVERT(1, "ft3", "l")).toBeAlmost(28.316846592);
            expect(CONVERT(1, "yd3", "l")).toBeAlmost(764.554857984);
            expect(CONVERT(1, "ang3", "l")).toBe(1E-27);
            expect(CONVERT(1, "Pica3", "l")).toBeAlmost(4.39039566186557E-08);
            expect(CONVERT(1, "ly3", "l")).toBeAlmost(8.46786664623715E+50);
        });
        it("should return correct results for temperatures", () => {
            expect(CONVERT(1000, "g", "kg")).toBe(1);
            expect(CONVERT(1, "kg", "g")).toBe(1000);
            expect(CONVERT(1, "km", "m")).toBe(1000);
            expect(CONVERT(1000, "m", "km")).toBe(1);
            expect(CONVERT(1000000, "m2", "km2")).toBe(1);
            expect(CONVERT(1000000000, "m3", "km3")).toBe(1);
            expect(CONVERT(1, "lbm", "kg")).toBeCloseTo(0.45359237, 5);
            expect(CONVERT(0, "C", "K")).toBe(273.15);
            expect(CONVERT(0, "K", "C")).toBe(-273.15);
            expect(CONVERT(68, "F", "K")).toBeAlmost(293.15);
        });
        it("should return correct results for energy", () => {
            // conflict: "pc" can be Parsec and picocalorie
            expect(CONVERT(1, "pJ", "pc")).toBe(0.2390057361376673);
            expect(CONVERT(1, "pc", "pJ")).toBe(4.184);
        });
        it("should throw #N/A error for invalid parameters", () => {
            expect(CONVERT(1, "ly", "m2")).toBe(ErrorCode.NA);
            expect(CONVERT(68, "f", "c")).toBe(ErrorCode.NA);
            expect(CONVERT(2.5, "ft", "sec")).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("DEC2BIN", DEC2BIN => {
        it("should convert decimal to binary", () => {
            expect(DEC2BIN(0)).toBe("0");
            expect(DEC2BIN(42)).toBe("101010");
            expect(DEC2BIN(511)).toBe("111111111");
            expect(DEC2BIN(-512)).toBe("1000000000");
            expect(DEC2BIN(-1)).toBe("1111111111");
        });
        it("should expand the result with leading zeros", () => {
            expect(DEC2BIN(3, 2)).toBe("11");
            expect(DEC2BIN(3, 4)).toBe("0011");
            expect(DEC2BIN(3, 10)).toBe("0000000011");
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(DEC2BIN(512)).toBe(ErrorCode.NUM);
            expect(DEC2BIN(-513)).toBe(ErrorCode.NUM);
            expect(DEC2BIN(3, 1)).toBe(ErrorCode.NUM);
            expect(DEC2BIN(3, 11)).toBe(ErrorCode.NUM);
            expect(DEC2BIN(0, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("DEC2HEX", DEC2HEX => {
        it("should convert decimal to binary", () => {
            expect(DEC2HEX(0)).toBe("0");
            expect(DEC2HEX(42)).toBe("2A");
            expect(DEC2HEX(0x7FFFFFFFFF)).toBe("7FFFFFFFFF");
            expect(DEC2HEX(-0x8000000000)).toBe("8000000000");
            expect(DEC2HEX(-1)).toBe("FFFFFFFFFF");
        });
        it("should expand the result with leading zeros", () => {
            expect(DEC2HEX(42, 2)).toBe("2A");
            expect(DEC2HEX(42, 4)).toBe("002A");
            expect(DEC2HEX(42, 10)).toBe("000000002A");
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(DEC2HEX(0x8000000000)).toBe(ErrorCode.NUM);
            expect(DEC2HEX(-0x8000000001)).toBe(ErrorCode.NUM);
            expect(DEC2HEX(42, 1)).toBe(ErrorCode.NUM);
            expect(DEC2HEX(42, 11)).toBe(ErrorCode.NUM);
            expect(DEC2HEX(0, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("DEC2OCT", DEC2OCT => {
        it("should convert decimal to binary", () => {
            expect(DEC2OCT(0)).toBe("0");
            expect(DEC2OCT(42)).toBe("52");
            expect(DEC2OCT(0x1FFFFFFF)).toBe("3777777777");
            expect(DEC2OCT(-0x20000000)).toBe("4000000000");
            expect(DEC2OCT(-1)).toBe("7777777777");
        });
        it("should expand the result with leading zeros", () => {
            expect(DEC2OCT(42, 2)).toBe("52");
            expect(DEC2OCT(42, 4)).toBe("0052");
            expect(DEC2OCT(42, 10)).toBe("0000000052");
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(DEC2OCT(0x20000000)).toBe(ErrorCode.NUM);
            expect(DEC2OCT(-0x20000001)).toBe(ErrorCode.NUM);
            expect(DEC2OCT(42, 1)).toBe(ErrorCode.NUM);
            expect(DEC2OCT(42, 11)).toBe(ErrorCode.NUM);
            expect(DEC2OCT(0, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("DECIMAL", DECIMAL => {
        it("should convert a value to the decimal value", () => {
            expect(DECIMAL(1, 36)).toBe(1);
            expect(DECIMAL("FF", 16)).toBe(255);
            expect(DECIMAL("111", 2)).toBe(7);
            expect(DECIMAL("zap", 36)).toBe(45745);
            expect(DECIMAL("f".repeat(198), 36)).toBeAlmost(6.02446334927868e+307);
        });
        it("should throw #NUM! for invalid parameters", () => {
            expect(DECIMAL(1, 37)).toBe(ErrorCode.NUM);
            expect(DECIMAL(-1, 36)).toBe(ErrorCode.NUM);
            expect(DECIMAL("f".repeat(199), 36)).toBe(ErrorCode.NUM);
            expect(DECIMAL("123h", 8)).toBe(ErrorCode.NUM);
            expect(DECIMAL("ff", 15)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("HEX2BIN", HEX2BIN => {
        it("should convert hexadecimal to binary", () => {
            expect(HEX2BIN("")).toBe("0");
            expect(HEX2BIN("0")).toBe("0");
            expect(HEX2BIN("2A")).toBe("101010");
            expect(HEX2BIN("02a")).toBe("101010");
            expect(HEX2BIN("1FF")).toBe("111111111");
            expect(HEX2BIN("FFFFFFFE00")).toBe("1000000000");
            expect(HEX2BIN("FFFFFFFFFF")).toBe("1111111111");
        });
        it("should expand the result with leading zeros", () => {
            expect(HEX2BIN("3", 2)).toBe("11");
            expect(HEX2BIN("3", 4)).toBe("0011");
            expect(HEX2BIN("3", 10)).toBe("0000000011");
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(HEX2BIN("efg")).toBe(ErrorCode.NUM);
            expect(HEX2BIN("200")).toBe(ErrorCode.NUM);
            expect(HEX2BIN("FFFFFFFDFF")).toBe(ErrorCode.NUM);
            expect(HEX2BIN("3", 1)).toBe(ErrorCode.NUM);
            expect(HEX2BIN("3", 11)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("HEX2DEC", HEX2DEC => {
        it("should convert hexadecimal to decimal", () => {
            expect(HEX2DEC("")).toBe(0);
            expect(HEX2DEC("0")).toBe(0);
            expect(HEX2DEC("2A")).toBe(42);
            expect(HEX2DEC("02a")).toBe(42);
            expect(HEX2DEC("7FFFFFFFFF")).toBe(0x7fffffffff);
            expect(HEX2DEC("8000000000")).toBe(-0x8000000000);
            expect(HEX2DEC("FFFFFFFFFF")).toBe(-1);
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(HEX2DEC("efg")).toBe(ErrorCode.NUM);
            expect(HEX2DEC("10000000000")).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("HEX2OCT", HEX2OCT => {
        it("should convert hexadecimal to octal", () => {
            expect(HEX2OCT("")).toBe("0");
            expect(HEX2OCT("0")).toBe("0");
            expect(HEX2OCT("2A")).toBe("52");
            expect(HEX2OCT("02a")).toBe("52");
            expect(HEX2OCT("1FFFFFFF")).toBe("3777777777");
            expect(HEX2OCT("FFE0000000")).toBe("4000000000");
            expect(HEX2OCT("FFFFFFFFFF")).toBe("7777777777");
        });
        it("should expand the result with leading zeros", () => {
            expect(HEX2OCT("22", 2)).toBe("42");
            expect(HEX2OCT("22", 4)).toBe("0042");
            expect(HEX2OCT("22", 10)).toBe("0000000042");
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(HEX2OCT("efg")).toBe(ErrorCode.NUM);
            expect(HEX2OCT("20000000")).toBe(ErrorCode.NUM);
            expect(HEX2OCT("FFDFFFFFFF")).toBe(ErrorCode.NUM);
            expect(HEX2OCT("22", 1)).toBe(ErrorCode.NUM);
            expect(HEX2OCT("22", 11)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("OCT2BIN", OCT2BIN => {
        it("should convert octal to binary", () => {
            expect(OCT2BIN("")).toBe("0");
            expect(OCT2BIN("0")).toBe("0");
            expect(OCT2BIN("42")).toBe("100010");
            expect(OCT2BIN("042")).toBe("100010");
            expect(OCT2BIN("777")).toBe("111111111");
            expect(OCT2BIN("7777777000")).toBe("1000000000");
            expect(OCT2BIN("7777777777")).toBe("1111111111");
        });
        it("should expand the result with leading zeros", () => {
            expect(OCT2BIN("3", 2)).toBe("11");
            expect(OCT2BIN("3", 4)).toBe("0011");
            expect(OCT2BIN("3", 10)).toBe("0000000011");
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(OCT2BIN("678")).toBe(ErrorCode.NUM);
            expect(OCT2BIN("1000")).toBe(ErrorCode.NUM);
            expect(OCT2BIN("7777776777")).toBe(ErrorCode.NUM);
            expect(OCT2BIN("3", 1)).toBe(ErrorCode.NUM);
            expect(OCT2BIN("3", 11)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("OCT2DEC", OCT2DEC => {
        it("should convert octal to decimal", () => {
            expect(OCT2DEC("")).toBe(0);
            expect(OCT2DEC("0")).toBe(0);
            expect(OCT2DEC("42")).toBe(34);
            expect(OCT2DEC("042")).toBe(34);
            expect(OCT2DEC("3777777777")).toBe(0x1FFFFFFF);
            expect(OCT2DEC("4000000000")).toBe(-0x20000000);
            expect(OCT2DEC("7777777777")).toBe(-1);
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(OCT2DEC("678")).toBe(ErrorCode.NUM);
            expect(OCT2DEC("10000000000")).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("OCT2HEX", OCT2HEX => {
        it("should convert octal to hexadecimal", () => {
            expect(OCT2HEX("")).toBe("0");
            expect(OCT2HEX("0")).toBe("0");
            expect(OCT2HEX("42")).toBe("22");
            expect(OCT2HEX("042")).toBe("22");
            expect(OCT2HEX("3777777777")).toBe("1FFFFFFF");
            expect(OCT2HEX("4000000000")).toBe("FFE0000000");
            expect(OCT2HEX("7777777777")).toBe("FFFFFFFFFF");
        });
        it("should expand the result with leading zeros", () => {
            expect(OCT2HEX("42", 2)).toBe("22");
            expect(OCT2HEX("42", 4)).toBe("0022");
            expect(OCT2HEX("42", 10)).toBe("0000000022");
        });
        it("should throw #NUM! error code for invalid parameter", () => {
            expect(OCT2HEX("678")).toBe(ErrorCode.NUM);
            expect(OCT2HEX("10000000000")).toBe(ErrorCode.NUM);
            expect(OCT2HEX("42", 1)).toBe(ErrorCode.NUM);
            expect(OCT2HEX("42", 11)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("ROMAN", ROMAN => {
        it("should convert decimal to roman", () => {
            expect(ROMAN(1)).toBe("I");
            expect(ROMAN(2)).toBe("II");
            expect(ROMAN(3)).toBe("III");
            expect(ROMAN(4)).toBe("IV");
            expect(ROMAN(499)).toBe("CDXCIX");
            expect(ROMAN(499, 0)).toBe("CDXCIX");
            expect(ROMAN(499, 1)).toBe("LDVLIV");
            expect(ROMAN(499, 2)).toBe("XDIX");
            expect(ROMAN(499, 3)).toBe("VDIV");
            expect(ROMAN(499, 4)).toBe("ID");
            expect(ROMAN(0)).toBe("");
        });
        it("should throw #VALUE! error code for invalid parameter", () => {
            expect(ROMAN(-1)).toBe(ErrorCode.VALUE);
            expect(ROMAN(4000)).toBe(ErrorCode.VALUE);
            expect(ROMAN(1, -1)).toBe(ErrorCode.VALUE);
            expect(ROMAN(1, 5)).toBe(ErrorCode.VALUE);
        });
    });
});
