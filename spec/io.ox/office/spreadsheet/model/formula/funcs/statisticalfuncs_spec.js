/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import StatisticalFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/statisticalfuncs";

import { r3d, r3da, mat, ErrorCode, matrixMatcher, assertMatrixCloseTo, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/funcs/statisticalfuncs", () => {

    // the operations to be applied at the test document
    const OPERATIONS = [
        { name: "setDocumentAttributes", attrs: { document: { cols: 16384, rows: 1048576 } } },
        { name: "insertSheet", sheet: 0, sheetName: "Sheet1" },
        { name: "changeCells", sheet: 0, start: "A2", contents: [
            [1, 2, 3, "abc", "5", true, null, null, "",  6,   ErrorCode.DIV0],
            [1, 2, 4, 8,     16,  32,   64,   128,  256, 512, 1024]
        ] }
    ];

    // initialize the test
    const ooxAppPromise = createSpreadsheetApp("ooxml", OPERATIONS);
    const odfAppPromise = createSpreadsheetApp("odf", OPERATIONS);
    const moduleTester = createFunctionModuleTester(StatisticalFuncs, ooxAppPromise, odfAppPromise);

    // function implementations -------------------------------------------

    moduleTester.testFunction("AVERAGE", AVERAGE => {
        it("should return the average of its scalar arguments", () => {
            expect(AVERAGE(1)).toBe(1);
            expect(AVERAGE(1, 2)).toBe(1.5);
            expect(AVERAGE(1, 2, 6)).toBe(3);
            expect(AVERAGE(true, 2, 6)).toBe(3);
            expect(AVERAGE(1, "2", 6)).toBe(3);
        });
        it("should handle empty arguments as zero", () => {
            expect(AVERAGE(1, null, 5)).toBe(2);
        });
        it("should fail for non-numeric arguments", () => {
            expect(AVERAGE(1, "abc", 6)).toBe(ErrorCode.VALUE);
        });
        it("should return the average of its matrix arguments", () => {
            expect(AVERAGE(mat([[1]]))).toBe(1);
            expect(AVERAGE(mat([[1, 2]]))).toBe(1.5);
            expect(AVERAGE(mat([[1, 2], [2, 5]]))).toBe(2.5);
        });
        it("should skip non-numeric matrix elements", () => {
            expect(AVERAGE(mat([[true, 1, 2]]))).toBe(1.5);
            expect(AVERAGE(mat([[1, "a", 2]]))).toBe(1.5);
            expect(AVERAGE(mat([[1, "2", 2]]))).toBe(1.5);
        });
        it("should return the average of its reference arguments", () => {
            expect(AVERAGE(r3d("0:0!A2:J2"))).toBe(3);
        });
    });

    moduleTester.testFunction("BETA.DIST", (BETADIST, BETADIST_ODF) => {
        it("should return the statistical BETA distribution", () => {
            expect(BETADIST(2, 8, 10, true, 1, 3)).toBeCloseTo(0.6854706, 6);
            expect(BETADIST(2, 8, 10, false, 1, 3)).toBeCloseTo(1.4837646, 6);
            expect(BETADIST(0.5, 9, 10, true, 0, 1)).toBeCloseTo(0.592735291, 6);
            expect(BETADIST(0.5, 9, 10, false, 0, 1)).toBeCloseTo(3.338470459, 6);

            expect(BETADIST(2, -8, 10, true, 1, 3)).toBe(ErrorCode.NUM);
            expect(BETADIST(2, 8, -10, true, 1, 3)).toBe(ErrorCode.NUM);
            expect(BETADIST(1.4, 8, -10, true, 1.5, 2)).toBe(ErrorCode.NUM); // x < a
            expect(BETADIST(2.4, 8, -10, true, 1.5, 2)).toBe(ErrorCode.NUM); // x > b
            expect(BETADIST(2, 8, -10, true, 2, 2)).toBe(ErrorCode.NUM); // a === b
        });
        it("should return the statistical BETA distribution for ODF", () => {
            expect(BETADIST_ODF(1, 8, 10, true, 2, 3)).toBe(0); // x < a , cumulative
            expect(BETADIST_ODF(4, 8, 10, true, 2, 3)).toBe(1); // x > b , cumulative
            expect(BETADIST_ODF(1, 8, 10, false, 2, 3)).toBe(0); // x < a , not cumulative
            expect(BETADIST_ODF(4, 8, 10, false, 2, 3)).toBe(0); // x > b , not cumulative
            expect(BETADIST_ODF(1, -8, 10, true, 1, 1)).toBe(ErrorCode.NUM); // b - a <= 0
        });
    });

    moduleTester.testFunction("BINOM.DIST", BINOMDIST => {
        it("should return the statistical binomial distribution", () => {
            expect(BINOMDIST(6, 10, 0.5, false)).toBeCloseTo(0.2050781, 6);
            expect(BINOMDIST(6, 10, 0.5, true)).toBeCloseTo(0.828125, 6);
            expect(BINOMDIST(6, 10, 0, true)).toBe(1); // p === 0, cumulative === true
            expect(BINOMDIST(6, 10, 0, false)).toBe(0); // p === 0, cumulative === false
        });
        it("should return error code for invalid data", () => {
            expect(BINOMDIST(6, -0.1, 0.5, true)).toBe(ErrorCode.NUM); // n < 0
            expect(BINOMDIST(-1, 0.1, 0.5, true)).toBe(ErrorCode.NUM); // x < 0
            expect(BINOMDIST(6, 5, 0.5, true)).toBe(ErrorCode.NUM); // x > n
            expect(BINOMDIST(6, 10, 1.1, true)).toBe(ErrorCode.NUM); // p > 1
        });
    });

    moduleTester.testFunction("BINOM.INV", BINOMINV => {
        it("should return the inverse statistical binomial distribution", () => {
            expect(BINOMINV(6, 0.5, 0.75)).toBe(4);
            expect(BINOMINV(6.5, 0.5, 0.75)).toBe(4);
            expect(BINOMINV(50, 0.333, 0.75)).toBe(19);
        });
        it("should return error code for invalid data", () => {
            expect(BINOMINV(50, 0.5, 1)).toBe(ErrorCode.NA);
            expect(BINOMINV(50, 1.1, 0.5)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("BINOM.DIST.RANGE", BINOMDISTRANGE => {
        it("should return the inverse statistical binomial distribution", () => {
            expect(BINOMDISTRANGE(60, 0.75, 48)).toBeCloseTo(0.083974967, 6);
            expect(BINOMDISTRANGE(60, 0.75, 45, 50)).toBeCloseTo(0.523629793, 6);
            expect(BINOMDISTRANGE(60, 1, 45, 50)).toBe(0);
            expect(BINOMDISTRANGE(60, 0, 45, 50)).toBe(0);
        });
        it("should return error code for invalid data", () => {
            expect(BINOMDISTRANGE(0.9, 0.75, 48)).toBe(ErrorCode.NA);
            expect(BINOMDISTRANGE(50, 0.1, 55)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("CHISQ.DIST", CHISQDIST => {
        it("should return the correct result", () => {
            expect(CHISQDIST(0.5, 1, true)).toBeCloseTo(0.52049988, 6);
            expect(CHISQDIST(2, 3, false)).toBeCloseTo(0.20755375, 6);
            expect(CHISQDIST(1, 1, true)).toBeCloseTo(0.682689492, 6);
            expect(CHISQDIST(1, 1, false)).toBeCloseTo(0.241970725, 6);
        });
        it("should return error code for invalid data", () => {
            expect(CHISQDIST(1, 0.5, true)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("CHISQ.DIST.RT", (CHISQDISTRT, CHISQDISTRT_ODF) => {
        it("should return the ChiSq right tail distribution", () => {
            expect(CHISQDISTRT(18.307, 10)).toBeCloseTo(0.0500006, 5);
            expect(CHISQDISTRT(18, 11)).toBeCloseTo(0.081580614, 5);
            expect(CHISQDISTRT(18, 101)).toBe(1);
        });
        it("should return the ChiSq right tail distribution for ODF", () => {
            expect(CHISQDISTRT_ODF(-1, 8)).toBe(1);
        });
    });

    moduleTester.testFunction("CHISQ.INV", CHISQINV => {
        it("should return the correct result", () => {
            expect(CHISQINV(0.93, 1)).toBeCloseTo(3.283020287, 6);
            expect(CHISQINV(0.6, 2)).toBeCloseTo(1.832581464, 6);
        });
        it("should return error code for invalid data", () => {
            expect(CHISQINV(1, 5)).toBe(ErrorCode.NA);
            expect(CHISQINV(0.2, 0.5)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("CHISQ.INV.RT", CHISQINVRT => {
        it("should return the correct result", () => {
            expect(CHISQINVRT(0.050001, 10)).toBeCloseTo(18.306973, 5);
            expect(CHISQINVRT(0.15, 20)).toBeCloseTo(26.49758019, 5);
            expect(CHISQINVRT(0.75, 100)).toBeCloseTo(90.13321975, 5);
        });
        it("should return error code for invalid data", () => {
            expect(CHISQINVRT(0, 10)).toBe(ErrorCode.NA);
            expect(CHISQINVRT(0.6, 0.9)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("CHISQ.TEST", CHISQTEST => {
        const mat1 = mat([[58, 11, 10], [35, 25, 23]]);
        const mat2 = mat([[45.35, 17.56, 16.09], [47.65, 18.44, 16.91]]);
        const mat3 = mat([[1, 2], [1, 2]]);
        it("should return the correct result", () => {
            expect(CHISQTEST(mat1, mat2)).toBeCloseTo(0.0003082, 6);
        });
        it("should return error code for invalid data", () => {
            expect(CHISQTEST(mat1, mat3)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("CONFIDENCE.NORM", CONFIDENCENORM => {
        it("should return the correct result", () => {
            expect(CONFIDENCENORM(0.05, 2.5, 50)).toBeCloseTo(0.692951912, 7);
            expect(CONFIDENCENORM(0.05, 2.5, 100)).toBeCloseTo(0.489990996, 7);
        });
        it("should return error code for invalid data", () => {
            expect(CONFIDENCENORM(-0.05, 2.5, 100)).toBe(ErrorCode.NA); // alpha <= 0
            expect(CONFIDENCENORM(0.05, 2.5, 0.5)).toBe(ErrorCode.NA); // n < 1
        });
    });

    moduleTester.testFunction("CONFIDENCE.T", CONFIDENCET => {
        it("should return the correct result", () => {
            expect(CONFIDENCET(0.05, 1, 50)).toBeCloseTo(0.284196855, 7);
            expect(CONFIDENCET(0.1, 1, 50)).toBeCloseTo(0.237100101, 7);
        });
        it("should return error code for invalid data", () => {
            expect(CONFIDENCET(-0.05, 2.5, 100)).toBe(ErrorCode.NA); // alpha <= 0
            expect(CONFIDENCET(0.05, 2.5, 0.5)).toBe(ErrorCode.NA); // n < 1
        });
    });

    moduleTester.testAliasFunction("CORREL", "PEARSON");

    moduleTester.testFunction("COUNT", { toOperand: true }, COUNT => {
        it("should return the count of scalar numbers", () => {
            expect(COUNT(1)).toBe(1);
            expect(COUNT(1, 2, 3, "abc", "4", true, null, null, null, 5, ErrorCode.DIV0)).toBe(9);
        });
        it("should return the count of numbers in a matrix", () => {
            expect(COUNT(mat([[1]]))).toBe(1);
            expect(COUNT(mat([[1, 2, 3], ["abc", "4", true], [5, 6, ErrorCode.DIV0]]))).toBe(5);
        });
        it("should return the count of numbers in a cell range", () => {
            expect(COUNT(r3d("0:0!A2:K2"))).toBe(4);
        });
    });

    moduleTester.testFunction("COUNTA", { toOperand: true }, COUNTA => {
        it("should return the count of scalar values", () => {
            expect(COUNTA(1)).toBe(1);
            expect(COUNTA(1, 2, 3, "abc", "4", true, null, null, null, 5, ErrorCode.DIV0)).toBe(11);
        });
        it("should return the count of values in a matrix", () => {
            expect(COUNTA(mat([[1]]))).toBe(1);
            expect(COUNTA(mat([[1, 2, 3], ["abc", "4", true], [5, 6, ErrorCode.DIV0]]))).toBe(9);
        });
        it("should return the count of values in a cell range", () => {
            expect(COUNTA(r3d("0:0!A2:K2"))).toBe(9);
        });
    });

    moduleTester.testFunction("COUNTBLANK", COUNTBLANK => {
        it("should return the number of blank cells", () => {
            expect(COUNTBLANK(r3da("0:0!A2:K2"))).toBe(3);
            expect(COUNTBLANK(r3da("0:0!A2:K10000"))).toBe(109970);
        });
    });

    moduleTester.testFunction("COUNTIF", COUNTIF => {
        it("should return the number of matching cells", () => {
            expect(COUNTIF(r3d("0:0!A2:K2"), ">2")).toBe(2);
        });
        it("should count empty cells and empty string results", () => {
            expect(COUNTIF(r3d("0:0!A2:K2"), "")).toBe(3);
            expect(COUNTIF(r3d("0:0!A2:K2"), "=")).toBe(2);
            expect(COUNTIF(r3d("0:0!ZZ1:ZZ1000000"), "")).toBe(1000000);
            expect(COUNTIF(r3d("0:0!ZZ1:ZZ1000000"), "=")).toBe(1000000);
        });
    });

    moduleTester.testFunction("COUNTIFS", COUNTIFS => {
        it("should return the number of matching cells", () => {
            expect(COUNTIFS(r3d("0:0!A2:K2"), "<4", r3d("0:0!A3:K3"), ">1")).toBe(2);
        });
        it("should count empty cells and empty string results", () => {
            expect(COUNTIFS(r3d("0:0!A2:K2"), "")).toBe(3);
            expect(COUNTIFS(r3d("0:0!ZZ1:ZZ1000000"), "", r3d("0:0!ZZ1:ZZ1000000"), "=")).toBe(1000000);
        });
    });

    moduleTester.testAliasFunction("COVAR", "COVARIANCE.P");

    moduleTester.testFunction("COVARIANCE.P", COVARIANCE_P => {
        const mat1 = mat([[3, 2, 4, 5, 6]]);
        const mat2 = mat([[9, 7, 12, 15, 17]]);
        const mat5 = mat([[10]]);
        it("should return covariance, the average of the products of paired deviations", () => {
            expect(COVARIANCE_P(mat1, mat2)).toBe(5.2);
        });
        it("should return error code for invalid data", () => {
            expect(COVARIANCE_P(mat1, mat5)).toBe(ErrorCode.NA); // not same dimensions of matrices
        });
    });

    moduleTester.testFunction("COVARIANCE.S", COVARIANCE_S => {
        const mat1 = mat([[2, 4, 8]]);
        const mat2 = mat([[5, 11, 12]]);
        const mat5 = mat([[10]]);
        it("should return covariance, the average of the products of paired deviations", () => {
            expect(COVARIANCE_S(mat1, mat2)).toBeCloseTo(9.666666667, 6);
        });
        it("should return error code for invalid data", () => {
            expect(COVARIANCE_S(mat1, mat5)).toBe(ErrorCode.NA); // not same dimensions of matrices
        });
    });

    moduleTester.testFunction("EXPON.DIST", EXPONDIST => {
        it("should return the correct result", () => {
            expect(EXPONDIST(0.2, 10, true)).toBeCloseTo(0.864664717, 7);
            expect(EXPONDIST(0.2, 10, false)).toBeCloseTo(1.353352832, 7);
        });
        it("should return error code for invalid data", () => {
            expect(EXPONDIST(0.5, 0, true)).toBe(ErrorCode.NA); // lambda <= 0
        });
    });

    moduleTester.testFunction("F.DIST", FDIST => {
        it("should return the correct result", () => {
            expect(FDIST(15.2069, 6, 4, true)).toBeCloseTo(0.99, 6);
            expect(FDIST(15.2069, 6, 4, false)).toBeCloseTo(0.001223792, 7);
        });
        it("should return error code for invalid data", () => {
            expect(FDIST(15.2069, 0.6, 4, true)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("F.DIST.RT", FDISTRT => {
        it("should return the correct result", () => {
            expect(FDISTRT(15.2069, 6, 4)).toBeCloseTo(0.01, 2);
            expect(FDISTRT(10, 8, 6)).toBeCloseTo(0.005784045, 7);
        });
        it("should return error code for invalid data", () => {
            expect(FDISTRT(15.2069, 0.6, 4)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("F.INV", FINV => {
        it("should return the correct result", () => {
            expect(FINV(0.01, 6, 4)).toBeCloseTo(0.10930991, 6);
            expect(FINV(0.6, 5, 7)).toBeCloseTo(1.194251702, 6);
        });
        it("should return error code for invalid data", () => {
            expect(FINV(0.01, 0.6, 4)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("F.INV.RT", FINVRT => {
        it("should return the correct result", () => {
            expect(FINVRT(0.01, 6, 4)).toBeCloseTo(15.20686486, 6);
            expect(FINVRT(0.6, 5, 7)).toBeCloseTo(0.769927426, 6);
        });
        it("should return error code for invalid data", () => {
            expect(FINVRT(0.01, 0.6, 4)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("F.TEST", FTEST => {
        const mat1 = mat([[6, 7, 9, 15, 21]]);
        const mat2 = mat([[20, 28, 31, 38, 40]]);
        const mat3 = mat([[10, 31, 31, 15, 20]]);
        it("should return the correct result", () => {
            expect(FTEST(mat1, mat2)).toBeCloseTo(0.64831785, 6);
            expect(FTEST(mat1, mat3)).toBeCloseTo(0.453048722, 6);
        });
    });

    moduleTester.testFunction("FISHER", FISHER => {
        it("should return the Fisher statistical transformation of x", () => {
            expect(FISHER(0.75)).toBeCloseTo(0.972955075, 8);
            expect(FISHER(0.55)).toBeCloseTo(0.618381314, 8);
            expect(FISHER(-0.75)).toBeCloseTo(-0.972955075, 8);
            expect(FISHER(-0.55)).toBeCloseTo(-0.618381314, 8);
        });
        it("should throw #NUM! error for invalid arguments", () => {
            expect(FISHER(-1)).toBe(ErrorCode.NUM);
            expect(FISHER(1)).toBe(ErrorCode.NUM);
            expect(FISHER(5.0)).toBe(ErrorCode.NUM);
            expect(FISHER(-5.0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testAliasFunction("FISHERINV", "TANH");

    moduleTester.testAliasFunction("FORECAST", "FORECAST.LINEAR");

    moduleTester.testFunction("FORECAST.LINEAR", FORECAST_LINEAR => {
        const mat1 = mat([[6, 7, 9, 15, 21]]);
        const mat2 = mat([[20, 28, 31, 38, 40]]);
        const mat3 = mat([[20, 28]]);
        it("should return a value along a linear trend", () => {
            expect(FORECAST_LINEAR(30, mat1, mat2)).toBeCloseTo(10.607253, 5);
            expect(FORECAST_LINEAR(20, mat1, mat2)).toBeCloseTo(3.5162037, 5);
        });
        it("should return error code for invalid data", () => {
            expect(FORECAST_LINEAR(30, mat1, mat3)).toBe(ErrorCode.NA); // not same dimensions of matrices
        });
    });

    moduleTester.testFunction("FREQUENCY", FREQUENCY => {
        const data = mat([[79, 85, 78, 85, 50, 81, 95, 88, 97]]);
        it("should return correct frequencies", () => {
            expect(FREQUENCY(data, mat([[70, 79, 89]]))).toSatisfy(matrixMatcher([[1], [2], [4], [2]]));
            expect(FREQUENCY(data, mat([[70], [79], [89]]))).toSatisfy(matrixMatcher([[1], [2], [4], [2]]));
            expect(FREQUENCY(data, mat([[70, 75, 79, 89]]))).toSatisfy(matrixMatcher([[1], [0], [2], [4], [2]]));
            expect(FREQUENCY(data, mat([[79, 75, 70, 89]]))).toSatisfy(matrixMatcher([[2], [0], [1], [4], [2]]));
            expect(FREQUENCY(data, mat([[1, 75, 79, 500]]))).toSatisfy(matrixMatcher([[0], [1], [2], [6], [0]]));
            expect(FREQUENCY(data, mat([[500, 75, 79, 1]]))).toSatisfy(matrixMatcher([[6], [1], [2], [0], [0]]));
            expect(FREQUENCY(data, mat([[500, 75, "79", 1]]))).toSatisfy(matrixMatcher([[8], [1], [0], [0]]));
            // bug 54466: duplicate entries in "bins" causes invalid matrix
            expect(FREQUENCY(data, mat([[70, 79, 79, 89]]))).toSatisfy(matrixMatcher([[1], [2], [0], [4], [2]]));
        });
    });

    moduleTester.testFunction("GAMMA", GAMMA => {
        it("should return the correct result", () => {
            expect(GAMMA(2.5)).toBeCloseTo(1.329340388, 6);
            expect(GAMMA(-3.75)).toBeCloseTo(0.267866129, 6);
        });
        it("should return error code for invalid data", () => {
            expect(GAMMA(0)).toBe(ErrorCode.NUM);
            expect(GAMMA(-2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("GAMMA.DIST", GAMMADIST => {
        it("should return the correct result", () => {
            expect(GAMMADIST(10.00001131, 9, 2, false)).toBeCloseTo(0.03263913, 6);
            expect(GAMMADIST(10.00001131, 9, 2, true)).toBeCloseTo(0.068094004, 6);
        });
        it("should return error code for invalid data", () => {
            expect(GAMMADIST(10, -9, 2, false)).toBe(ErrorCode.NUM);
            expect(GAMMADIST(10, 9, 0, false)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("GAMMA.INV", GAMMAINV => {
        it("should return the correct result", () => {
            expect(GAMMAINV(0.068094, 9, 2)).toBeCloseTo(10.00001119, 6);
        });
        it("should return error code for invalid data", () => {
            expect(GAMMAINV(0.5, -9, 2)).toBe(ErrorCode.NUM);
            expect(GAMMAINV(0.5, 9, 0)).toBe(ErrorCode.NUM);
            expect(GAMMAINV(1, 9, 1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testAliasFunction("GAMMALN", "GAMMALN.PRECISE");

    moduleTester.testFunction("GAMMALN.PRECISE", GAMMALN_PRECISE => {
        it("should return the correct result", () => {
            expect(GAMMALN_PRECISE(4)).toBeCloseTo(1.791759469, 7);
            expect(GAMMALN_PRECISE(2.2)).toBeCloseTo(0.096947467, 7);
        });
        it("should return error code for invalid data", () => {
            expect(GAMMALN_PRECISE(0)).toBe(ErrorCode.NUM);
            expect(GAMMALN_PRECISE(-1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("GAUSS", GAUSS => {
        it("should return the Gauss statistical probability of x", () => {
            expect(GAUSS(2)).toBeCloseTo(0.477249868, 8);
            expect(GAUSS(2.22)).toBeCloseTo(0.486790616, 8);
            expect(GAUSS(-0.5)).toBeCloseTo(-0.191462461, 8);
        });
    });

    moduleTester.testFunction("GROWTH", { toOperand: [0, 1, 2] }, GROWTH => {
        it("should return the GROWTH statistical result", () => {
            const knownY = mat([[33100, 47300, 69000, 102000, 150000, 220000]]);
            const knownX = mat([[11, 12, 13, 14, 15, 16]]);
            const newX =  mat([[17, 18]]);

            const expectedResult = mat([[32618.2037735397], [47729.4226147478], [69841.3008562175], [102197.0733788320], [149542.4867400460], [218821.8762145950]]);

            assertMatrixCloseTo(GROWTH(knownY), expectedResult, 6);
            assertMatrixCloseTo(GROWTH(knownY, knownX), expectedResult, 6);
            assertMatrixCloseTo(GROWTH(knownY, knownX, newX), mat([[320196.718363472], [468536.054184048]]), 6);
            assertMatrixCloseTo(GROWTH(knownY, knownX, knownX), expectedResult, 6);

            expect(GROWTH(knownY, newX)).toBe(ErrorCode.REF);
            expect(GROWTH(mat([[17, 0]]))).toBe(ErrorCode.NUM);
            expect(GROWTH(mat([[17, "18"]]))).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("HYPGEOM.DIST", HYPGEOMDIST => {
        it("should return the correct result", () => {
            expect(HYPGEOMDIST(1, 4, 8, 20, true)).toBeCloseTo(0.465428277, 6);
            expect(HYPGEOMDIST(1, 4, 8, 20, false)).toBeCloseTo(0.363261094, 6);
            expect(HYPGEOMDIST(12, 45, 80000, 200000, true)).toBeCloseTo(0.044612892, 6);
        });
        it("should return error code for invalid data", () => {
            expect(HYPGEOMDIST(-1, 4, 8, 20, true)).toBe(ErrorCode.NUM); // x < 0
            expect(HYPGEOMDIST(4, 1, 8, 20, true)).toBe(ErrorCode.NUM); // n < x
            expect(HYPGEOMDIST(1, 40, 8, 20, true)).toBe(ErrorCode.NUM); // x < n - N + M
        });
    });

    moduleTester.testFunction("INTERCEPT", INTERCEPT => {
        const mat1 = mat([[2, 3, 9, 1, 8]]);
        const mat2 = mat([[6, 5, 11, 7, 5]]);
        const mat3 = mat([["a"]]);
        it("should return the Intercept of the linear regression line", () => {
            expect(INTERCEPT(mat1, mat2)).toBeCloseTo(0.048387097, 6);
        });
        it("should return error code for invalid data", () => {
            expect(INTERCEPT(mat3, mat3)).toBe(ErrorCode.DIV0);
        });
    });

    moduleTester.testFunction("KURT", KURT => {
        const mat1 = mat([[3, 4, 5, 2, 3, 4, 5, 6, 4, 7]]);
        const mat2 = mat([[6, 5, 11, 7, 5, 4, 4]]);
        const mat3 = mat([["a"]]);
        it("should return the correct result", () => {
            expect(KURT(mat1)).toBeCloseTo(-0.151799637, 6);
            expect(KURT(mat2)).toBeCloseTo(3.155555556, 6);
        });
        it("should return error code for invalid data", () => {
            expect(KURT(mat3)).toBe(ErrorCode.DIV0);
        });
    });

    moduleTester.testFunction("LARGE", { toOperand: 0 }, LARGE => {
        const mat1 = mat([[3, 5, 3, 5, 4], [4, 2, 4, 6, 7]]);
        it("should return the 3rd and 7th largest numbers from set", () => {
            expect(LARGE(mat1, 3)).toBe(5);
            expect(LARGE(mat1, 7)).toBe(4);
        });
        it("should return error code for invalid data", () => {
            expect(LARGE(mat1, 0)).toBe(ErrorCode.NUM); // k <= 0
            expect(LARGE(mat1, 11)).toBe(ErrorCode.NUM); // k > mat1.length
        });
    });

    moduleTester.testFunction("LINEST", LINEST => {
        it("should return the correct result", () => {
            expect(LINEST(mat([[3100, 3100]]))).toSatisfy(matrixMatcher([[0, 3100]]));
            expect(LINEST(mat([[3100, 4500]]), null, false)).toSatisfy(matrixMatcher([[2420, 0]]));
            expect(LINEST(mat([[3100, 4500]]), mat([[1, 2]]), true)).toSatisfy(matrixMatcher([[1400, 1700]]));
            expect(LINEST(mat([[3100], [4500], [4400], [5400], [7500], [8100]]))).toSatisfy(matrixMatcher([[1000, 2000]]));
            expect(LINEST(mat([[3100], [4500], [4400], [5400], [7500], [8100]]), mat([[1], [2], [3], [4], [5], [6]]))).toSatisfy(matrixMatcher([[1000, 2000]]));
        });
        it("should return the correct result with special 1D knownX", () => {
            const knownY = mat([[142000], [144000]]);
            assertMatrixCloseTo(LINEST(knownY, null, false, false), mat([[86000, 0]]), 1e-7);
            assertMatrixCloseTo(LINEST(knownY, mat([[2], [3]]), false, false), mat([[55076.92308, 0]]), 4);
            assertMatrixCloseTo(LINEST(knownY, mat([[2310], [2333]]), false, false), mat([[61.598726930698234, 0]]), 6);
        });
        it("should return the correct result with special 2D knownX", () => {
            assertMatrixCloseTo(LINEST(mat([[0], [0]]), mat([[1, 2], [3, 4]]), false, false), mat([[0, 0, 0]]), 4);
            assertMatrixCloseTo(LINEST(mat([[0], [0]]), mat([[1, 0], [0, 1]]), false, false), mat([[0, 0, 0]]), 4);
            assertMatrixCloseTo(LINEST(mat([[1], [1]]), mat([[1, 0], [0, 1]]), false, false), mat([[1, 1, 0]]), 4);
            assertMatrixCloseTo(LINEST(mat([[1], [66]]), mat([[1, 0], [0, 1]]), false, false), mat([[66, 1, 0]]), 4);
            assertMatrixCloseTo(LINEST(mat([[66], [1]]), mat([[1, 0], [0, 1]]), false, false), mat([[1, 66, 0]]), 4);

            assertMatrixCloseTo(LINEST(mat([[1], [1]]), mat([[1, 2], [3, 4]]), false, false), mat([[1, -1, 0]]), 4);
            assertMatrixCloseTo(LINEST(mat([[66], [1]]), mat([[1, 2], [3, 4]]), false, false), mat([[98.5, -131, 0]]), 4);

            assertMatrixCloseTo(LINEST(mat([[1], [1], [1]]), mat([[1, 2], [3, 4], [5, 0]]), true, false), mat([[0, 0, 1]]), 4);
            assertMatrixCloseTo(LINEST(mat([[66], [1], [1]]), mat([[1, 2], [3, 4], [5, 0]]), true, false), mat([[-10.83333333, -21.66666667, 109.3333333]]), 4);

            assertMatrixCloseTo(LINEST(mat([[142000], [144000], [151000]]), mat([[2310], [2333], [2356]]), false, false), mat([[62.44612203, 0]]), 4);
            assertMatrixCloseTo(LINEST(mat([[142000], [144000], [151000]]), mat([[2310, 2], [2333, 2], [2356, 3]]), false, false), mat([[6041.800495, 56.39534008, 0]]), 4);
            assertMatrixCloseTo(LINEST(mat([[142000], [144000]]), mat([[1, 2], [3, 4]]), false, false), mat([[141000, -140000, 0]]), 4);

            assertMatrixCloseTo(LINEST(mat([[10], [20]]), mat([[30, 2], [40, 2]]), false, false), mat([[-10, 1, 0]]), 6);
            assertMatrixCloseTo(LINEST(mat([[142000], [144000]]), mat([[2310, 2], [2333, 2]]), false, false), mat([[-29434.782608695226, 86.95652173913008, 0]]), 6);
        });
        it("should return the correct result with stats", () => {

            const knownY = mat([[142000], [144000], [151000], [150000], [139000], [169000], [126000], [142900], [163000], [169000], [149000]]);

            const knownX = mat([
                [2310, 2, 2,   20],
                [2333, 2, 2,   12],
                [2356, 3, 1.5, 33],
                [2379, 3, 2,   43],
                [2402, 2, 3,   53],
                [2425, 4, 2,   23],
                [2448, 2, 1.5, 99],
                [2471, 2, 2,   34],
                [2494, 3, 3,   23],
                [2517, 4, 4,   55],
                [2540, 2, 3,   22]
            ]);

            const resultWithStats = mat([
                [-234.2371645,           2553.210660391537,     12529.768167086753,    27.64138737,    52317.830507291335],
                [13.26801148,            530.6691519,           400.0668382,           5.429374042,    12237.361602862342],
                [0.996747993,            970.5784629,           ErrorCode.NA,          ErrorCode.NA,   ErrorCode.NA],
                [459.7536742,            6,                     ErrorCode.NA,          ErrorCode.NA,   ErrorCode.NA],
                [1732393319.2292507,     5652135.316203966,     ErrorCode.NA,          ErrorCode.NA,   ErrorCode.NA]
            ]);

            const resultWithoutStats = mat([[-234.2371645, 2553.210660391537, 12529.768167086753, 27.64138737, 52317.830507291335]]);
            const resultWithoutStatsNotConstant = mat([[-250.1350324,  1248.795225, 12540.261860152552, 50.71193750640334, 0]]);

            assertMatrixCloseTo(LINEST(knownY, null, false, false), mat([[19788.932806324112, 0]]), 6);
            assertMatrixCloseTo(LINEST(knownY, knownX, false, false), resultWithoutStatsNotConstant, 6);
            assertMatrixCloseTo(LINEST(knownY, knownX, true, false), resultWithoutStats, 6);
            assertMatrixCloseTo(LINEST(knownY, knownX, true, true), resultWithStats, 6);
        });
        it("should return the correct result original calcengine did not support", () => {
            assertMatrixCloseTo(LINEST(mat([[3100]])), mat([[0, 3100]]), 6);
            assertMatrixCloseTo(LINEST(mat([[3100]]), undefined, false, false), mat([[3100, 0]]), 6);
            // still no support
            //assertMatrixCloseTo(LINEST(mat([[1], [500]]), mat([[0, 0], [0, 1]]), false, false), mat([[500, 0, 0]]), 4);
            //assertMatrixCloseTo(LINEST(mat([[1], [1]]), mat([[0, 0], [0, 0]]), false, false), mat([[0, 0, 0]]), 4);
        });
    });

    moduleTester.testFunction("LOGEST", LOGEST => {
        // LOGEST does not need so many tests, because its just a variation of LINEST
        it("should return the correct result with stats", () => {

            const knownY = mat([[142000], [144000], [151000], [150000], [139000], [169000], [126000], [142900], [163000], [169000], [149000]]);
            const knownX = mat([
                [2310, 2, 2,   20],
                [2333, 2, 2,   12],
                [2356, 3, 1.5, 33],
                [2379, 3, 2,   43],
                [2402, 2, 3,   53],
                [2425, 4, 2,   23],
                [2448, 2, 1.5, 99],
                [2471, 2, 2,   34],
                [2494, 3, 3,   23],
                [2517, 4, 4,   55],
                [2540, 2, 3,   22]
            ]);

            const resultWithStats = mat([
                [0.998307311, 1.018106214, 1.085434734,  1.000174374,  80387.1472131758],
                [8.22132E-05, 0.003288211, 0.002478954,  3.36423E-05,  0.075826957],
                [0.997224229, 0.006014042, ErrorCode.NA, ErrorCode.NA, ErrorCode.NA],
                [538.8903983, 6,           ErrorCode.NA, ErrorCode.NA, ErrorCode.NA],
                [0.077963872, 0.000217012, ErrorCode.NA, ErrorCode.NA, ErrorCode.NA]
            ]);

            assertMatrixCloseTo(LOGEST(knownY, knownX, true, true), resultWithStats, 6);
        });
    });

    moduleTester.testFunction("LOGNORM.DIST", LOGNORMDIST => {
        it("should return the 3rd and 7th largest numbers from set", () => {
            expect(LOGNORMDIST(4, 3.5, 1.2, true)).toBeCloseTo(0.0390836, 6);
            expect(LOGNORMDIST(4, 3.5, 1.2, false)).toBeCloseTo(0.0176176, 6);
        });
        it("should return error code for invalid data", () => {
            expect(LOGNORMDIST(0, 3.5, 1.2, false)).toBe(ErrorCode.NUM); // x <= 0
            expect(LOGNORMDIST(4, 3.5, 0, false)).toBe(ErrorCode.NUM); // sigma <= 0
        });
    });

    moduleTester.testFunction("LOGNORM.INV", LOGNORMINV => {
        it("should return the 3rd and 7th largest numbers from set", () => {
            expect(LOGNORMINV(0.039084, 3.5, 1.2)).toBeCloseTo(4.0000252, 6);
        });
        it("should return error code for invalid data", () => {
            expect(LOGNORMINV(0, 3.5, 1.2, false)).toBe(ErrorCode.NUM); // p <= 0
            expect(LOGNORMINV(4, 3.5, 0, false)).toBe(ErrorCode.NUM); // sigma <= 0
        });
    });

    moduleTester.testAliasFunction("MODE", "MODE.SNGL");

    moduleTester.testFunction("MODE.SNGL", MODE_SNGL => {
        it("should return the correct result", () => {
            expect(MODE_SNGL(mat([[4, 2, 1, 3, 4, 3, 2, 1, 2, 3, 5, 6, 1]]))).toBe(2);
        });
        it("should throw #N/A for distinct numbers", () => {
            expect(MODE_SNGL(mat([[1, 3, 2, 4]]))).toBe(ErrorCode.NA);
        });
        it("should throw #N/A for invalid data", () => {
            expect(MODE_SNGL(mat([["a"]]))).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("MODE.MULT", MODE_MULT => {
        it("should return the correct result", () => {
            expect(MODE_MULT(mat([[4, 2, 1, 3, 4, 3, 2, 1, 2, 3, 5, 6, 1]]))).toSatisfy(matrixMatcher([[2], [1], [3]]));
        });
        it("should throw #N/A for distinct numbers", () => {
            expect(MODE_MULT(mat([[1, 3, 2, 4]]))).toBe(ErrorCode.NA);
        });
        it("should throw #N/A for invalid data", () => {
            expect(MODE_MULT(mat([["a"]]))).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("NEGBINOM.DIST", NEGBINOMDIST => {
        it("should return the negative binomial distribution", () => {
            expect(NEGBINOMDIST(10, 5, 0.25, true)).toBeCloseTo(0.3135141, 6);
            expect(NEGBINOMDIST(10, 5, 0.25, false)).toBeCloseTo(0.0550487, 6);
        });
        it("should return error code for invalid data", () => {
            expect(NEGBINOMDIST(10, 5, -0.1, false)).toBe(ErrorCode.NUM);
            expect(NEGBINOMDIST(-1, 5, 0.25, false)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("NORM.DIST", NORMDIST => {
        it("should return the statistical normal distribution", () => {
            expect(NORMDIST(42, 40, 1.5, true)).toBeCloseTo(0.9087888, 6);
            expect(NORMDIST(42, 40, 1.5, false)).toBeCloseTo(0.10934, 6);
        });
        it("should return error code for invalid data", () => {
            expect(NORMDIST(1, 1, -1, false)).toBe(ErrorCode.NUM);
            expect(NORMDIST(1, 1, -0.1, false)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("NORM.S.DIST", NORMSDIST => {
        it("should return the statistical standard normal distribution", () => {
            expect(NORMSDIST(1.333333, true)).toBeCloseTo(0.908788726, 6);
            expect(NORMSDIST(1.333333, false)).toBeCloseTo(0.164010148, 6);
        });
    });

    moduleTester.testFunction("NORMSDIST", NORMSDIST => {
        it("should return the statistical standard normal distribution", () => {
            expect(NORMSDIST(1.333333)).toBeCloseTo(0.908788726, 6);
        });
    });

    moduleTester.testFunction("NORM.INV", NORMINV => {
        it("should return the statistical normal distribution", () => {
            expect(NORMINV(0.908789, 40, 1.5)).toBeCloseTo(42.000002, 7);
            expect(NORMINV(0.908789, 40, 1.5)).not.toBeCloseTo(41.900002, 7);
            expect(NORMINV(0.555555, 20, 0.5)).toBeCloseTo(20.06985445, 7);
        });
        it("should return error code for invalid data", () => {
            expect(NORMINV(1, 1, 0.5)).toBe(ErrorCode.NUM);
            expect(NORMINV(-1, 1, 0.5)).toBe(ErrorCode.NUM);
            expect(NORMINV(2, 1, 0.5)).toBe(ErrorCode.NUM);
            expect(NORMINV(0, 1, 0.5)).toBe(ErrorCode.NUM);
            expect(NORMINV(1, 1, 0.5)).toBe(ErrorCode.NUM);
            expect(NORMINV(0.5, 1, -0.5)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("NORM.S.INV", NORMSINV => {
        it("should return the statistical inverse normal distribution", () => {
            expect(NORMSINV(0.908789)).toBeCloseTo(1.3333347, 6);
        });
        it("should return error code for invalid data", () => {
            expect(NORMSINV(0)).toBe(ErrorCode.NUM);
            expect(NORMSINV(1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("PEARSON", PEARSON => {
        const mat1 = mat([[9, 7, 5, 3, 1]]);
        const mat2 = mat([[10, 6, 1, 5, 3]]);
        const mat3 = mat([[1, 3, 5, 7, 9]]);
        const mat5 = mat([[10]]);
        it("should return the Pearson product moment correlation coefficient", () => {
            expect(PEARSON(mat1, mat2)).toBeCloseTo(0.699379, 5);
            expect(PEARSON(mat3, mat2)).toBeCloseTo(-0.699379, 5);
            expect(PEARSON(mat1, mat3)).toBe(-1);
        });
        it("should return error code for invalid data", () => {
            expect(PEARSON(mat1, mat5)).toBe(ErrorCode.NA); // not same dimensions of matrices
        });
    });

    moduleTester.testFunction("PERMUT", PERMUT => {
        it("should return the permutation of assigned integers", () => {
            expect(PERMUT(100, 3)).toBe(970200);
            expect(PERMUT(3, 2)).toBeAlmost(6);
        });
        it("should return error code for invalid data", () => {
            expect(PERMUT(0, 1)).toBe(ErrorCode.NUM);
            expect(PERMUT(2, -1)).toBe(ErrorCode.NUM);
            expect(PERMUT(1, 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("PERMUTATIONA", PERMUTATIONA => {
        it("should return the permutation of assigned integers", () => {
            expect(PERMUTATIONA(4, 3)).toBe(64);
            expect(PERMUTATIONA(0, 0)).toBeAlmost(1);
        });
        it("should return error code for invalid data", () => {
            expect(PERMUTATIONA(-1, 1)).toBe(ErrorCode.NUM);
            expect(PERMUTATIONA(1, -1)).toBe(ErrorCode.NUM);
            expect(PERMUTATIONA(-1, -1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("PHI", PHI => {
        it("should return the value of the density function for a standard normal distribution", () => {
            expect(PHI(0.75)).toBeCloseTo(0.301137432, 8);
            expect(PHI(0.55)).toBeCloseTo(0.342943855, 8);
        });
    });

    moduleTester.testAliasFunction("POISSON", "POISSON.DIST");

    moduleTester.testFunction("POISSON.DIST", POISSON_DIST => {
        it("should return the value of the Poisson distribution", () => {
            expect(POISSON_DIST(2, 5, true)).toBeCloseTo(0.124652019, 8);
            expect(POISSON_DIST(2, 5, false)).toBeCloseTo(0.084224337, 8);
        });
        it("should return error code for invalid data", () => {
            expect(POISSON_DIST(1, -1, true)).toBe(ErrorCode.NUM);
            expect(POISSON_DIST(-1, 1, true)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("PROB", PROB => {
        const mat1 = mat([[0, 1, 2, 3]]);
        const mat2 = mat([[0.2, 0.3, 0.1, 0.4]]);
        const mat3 = mat([[0.2, 0.3, 0.1, 0.1]]);
        it("should return the probability that values in a range are between two limits", () => {
            expect(PROB(mat1, mat2, 2)).toBe(0.1);
            expect(PROB(mat1, mat2, 1, 3)).toBe(0.8);
        });
        it("should return error code for invalid data", () => {
            expect(PROB(mat1, mat3, 2)).toBe(ErrorCode.VALUE); // sum p < 1
        });
    });

    moduleTester.testAliasFunction("PERCENTILE", "PERCENTILE.INC");

    moduleTester.testFunction("PERCENTILE.EXC", PERCENTILE_EXC => {
        const set1 = mat([[1, 2, 3, 6, 6, 6, 7, 8, 9]]);
        it("should return the correct result", () => {
            expect(PERCENTILE_EXC(set1, 0.25)).toBe(2.5);
        });
        it("should return error code for invalid data", () => {
            expect(PERCENTILE_EXC(set1, 0)).toBe(ErrorCode.NUM);
            expect(PERCENTILE_EXC(set1, 0.01)).toBe(ErrorCode.NUM);
            expect(PERCENTILE_EXC(set1, 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("PERCENTILE.INC", PERCENTILE_INC => {
        const set1 = mat([[1, 3, 2, 4]]);
        it("should return the correct result", () => {
            expect(PERCENTILE_INC(set1, 0.3)).toBe(1.9);
        });
        it("should return error code for invalid data", () => {
            expect(PERCENTILE_INC(set1, 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testAliasFunction("PERCENTRANK", "PERCENTRANK.INC");

    moduleTester.testFunction("PERCENTRANK.EXC", PERCENTRANK_EXC => {
        const mat1 = mat([[1, 2, 3, 6, 6, 6, 7, 8, 9]]);
        it("should return the correct result", () => {
            expect(PERCENTRANK_EXC(mat1, 7)).toBe(0.7);
            expect(PERCENTRANK_EXC(mat1, 5.43)).toBe(0.381);
            expect(PERCENTRANK_EXC(mat1, 5.43, 1)).toBe(0.4);
        });
        it("should return error code for invalid data", () => {
            expect(PERCENTRANK_EXC(mat1, 7, 0.9)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("PERCENTRANK.INC", PERCENTRANK_INC => {
        const mat1 = mat([[13, 12, 11, 8, 4, 3, 2, 1, 1, 1]]);
        it("should return the correct result", () => {
            expect(PERCENTRANK_INC(mat1, 2)).toBe(0.3333);
            expect(PERCENTRANK_INC(mat1, 4)).toBe(0.556);
            expect(PERCENTRANK_INC(mat1, 8)).toBe(0.667);
            expect(PERCENTRANK_INC(mat1, 5)).toBe(0.583);
        });
        it("should return error code for invalid data", () => {
            expect(PERCENTRANK_INC(mat1, 2, 0.9)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testAliasFunction("QUARTILE", "QUARTILE.INC");

    moduleTester.testFunction("QUARTILE.EXC", QUARTILE_EXC => {
        const set1 = mat([[6, 7, 15, 36, 39, 40, 41, 42, 43, 47, 49]]);
        it("should return the correct result", () => {
            expect(QUARTILE_EXC(set1, 1)).toBe(15);
            expect(QUARTILE_EXC(set1, 3)).toBe(43);
            expect(QUARTILE_EXC(set1, 2)).toBe(40);
        });
        it("should return error code for invalid data", () => {
            expect(QUARTILE_EXC(set1, 0)).toBe(ErrorCode.NUM);
            expect(QUARTILE_EXC(set1, 4)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("QUARTILE.INC", QUARTILE_INC => {
        const set1 = mat([[1, 2, 4, 7, 8, 9, 10, 12]]);
        it("should return the correct result", () => {
            expect(QUARTILE_INC(set1, 1)).toBe(3.5);
            expect(QUARTILE_INC(set1, 2)).toBe(7.5);
        });
        it("should return error code for invalid data", () => {
            expect(QUARTILE_INC(set1, -1)).toBe(ErrorCode.NUM);
            expect(QUARTILE_INC(set1, 5)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("RANK.AVG", RANK_AVG => {
        const set = mat([[89, 88, 92, 101, 94, 97, 95]]);
        it("should return the correct result", () => {
            expect(RANK_AVG(94, set)).toBe(4);
            expect(RANK_AVG(94, set, 1)).toBe(4);
            expect(RANK_AVG(94, set, 2)).toBe(4);
            expect(RANK_AVG(101, set)).toBe(1);
            expect(RANK_AVG(101, set, 1)).toBe(7);
        });
        it("should return error code for invalid data", () => {
            expect(RANK_AVG(93, set)).toBe(ErrorCode.NA); // not in the set
        });
    });

    moduleTester.testFunction("RANK.EQ", RANK_EQ => {
        const set = mat([[7, 3.5, 3.5, 1, 2]]);
        it("should return the correct result", () => {
            expect(RANK_EQ(7, set, 1)).toBe(5);
            expect(RANK_EQ(2, set)).toBe(4);
            expect(RANK_EQ(3.5, set, 1)).toBe(3);
        });
        it("should return error code for invalid data", () => {
            expect(RANK_EQ(9, set)).toBe(ErrorCode.NA); // not in the set
        });
    });

    moduleTester.testFunction("RSQ", RSQ => {
        const set1 = mat([[2, 3, 9, 1, 8, 7, 5]]);
        const set2 = mat([[6, 5, 11, 7, 5, 4, 4]]);
        it("should return the probability that values in a range are between two limits", () => {
            expect(RSQ(set1, set2)).toBeCloseTo(0.057950192, 6);
        });
    });

    moduleTester.testFunction("SLOPE", SLOPE => {
        const mat1 = mat([[3, 4, 10, 2, 9, 8, 6]]);
        const mat2 = mat([[6, 5, 11, 7, 5, 4, 4]]);
        const mat3 = mat([["a"]]);
        it("should return the Slope of the linear regression line", () => {
            expect(SLOPE(mat1, mat2)).toBeCloseTo(0.305555556, 6);
        });
        it("should return error code for invalid data", () => {
            expect(SLOPE(mat3, mat3)).toBe(ErrorCode.DIV0);
        });
    });

    moduleTester.testFunction("SKEW", SKEW => {
        const set1 = mat([[3, 4, 5, 2, 3, 4, 5, 6, 4, 7]]);
        const set2 = mat([["a"]]);
        it("should return the Slope of the linear regression line", () => {
            expect(SKEW(set1)).toBeCloseTo(0.359543071, 6);
        });
        it("should return error code for invalid data", () => {
            expect(SKEW(set2)).toBe(ErrorCode.DIV0);
        });
    });

    moduleTester.testFunction("SKEW.P", SKEW_P => {
        const set1 = mat([[3, 4, 5, 2, 3, 4, 5, 6, 4, 7]]);
        const set2 = mat([["a"]]);
        it("should return the Slope of the linear regression line", () => {
            expect(SKEW_P(set1)).toBeCloseTo(0.303193339, 6);
        });
        it("should return error code for invalid data", () => {
            expect(SKEW_P(set2)).toBe(ErrorCode.DIV0);
        });
    });

    moduleTester.testFunction("STANDARDIZE", STANDARDIZE => {
        it("should return the Slope of the linear regression line", () => {
            expect(STANDARDIZE(42, 40, 1.5)).toBeCloseTo(1.3333333, 6);
        });
        it("should return error code for invalid data", () => {
            expect(STANDARDIZE(42, 40, -0.5)).toBe(ErrorCode.NUM);
            expect(STANDARDIZE(42, 40, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("SMALL", { toOperand: 0 }, SMALL => {
        const mat1 = mat([[3, 5, 3, 5, 4], [4, 2, 4, 6, 7]]);
        it("should return the 3rd and 7th smallest numbers from set", () => {
            expect(SMALL(mat1, 1)).toBe(2);
            expect(SMALL(mat1, 5)).toBe(4);
        });
        it("should return error code for invalid data", () => {
            expect(SMALL(mat1, 0)).toBe(ErrorCode.NUM); // k <= 0
            expect(SMALL(mat1, 11)).toBe(ErrorCode.NUM); // k > mat1.length
        });
    });

    moduleTester.testFunction("STEYX", STEYX => {
        const mat1 = mat([[2, 3, 9, 1, 8, 7, 5]]);
        const mat2 = mat([[6, 5, 11, 7, 5, 4, 4]]);
        const mat3 = mat([[5, 10]]);
        it("should return the standard error of the predicted y-value for each x in the regression", () => {
            expect(STEYX(mat1, mat2)).toBeCloseTo(3.30571895, 6);
        });
        it("should return error code for invalid data", () => {
            expect(STEYX(mat1, mat3)).toBe(ErrorCode.NA); // not same dimensions of matrices
            expect(STEYX(mat3, mat3)).toBe(ErrorCode.DIV0); //  elem count < 3
        });
    });

    moduleTester.testFunction("TREND", { toOperand: [0, 1, 2] }, TREND => {
        const knownY = mat([[33100, 47300, 69000, 102000, 150000, 220000]]);
        const knownX = mat([[11, 12, 13, 14, 15, 16]]);
        const newX =  mat([[17, 18]]);
        it("should return the GROWTH statistical result", () => {
            const expectedResult =  mat([[12452.3809523809], [48898.0952380952], [85343.8095238095], [121789.5238095240], [158235.2380952380], [194680.9523809520]]);
            assertMatrixCloseTo(TREND(knownY), expectedResult, 6);
            assertMatrixCloseTo(TREND(knownY, knownX), expectedResult, 6);
            assertMatrixCloseTo(TREND(knownY, knownX, newX), mat([[231126.666666667], [267572.380952381]]), 6);
            assertMatrixCloseTo(TREND(knownY, knownX, knownX), expectedResult, 6);
            expect(TREND(mat([[1, 0]]))).toSatisfy(matrixMatcher([[1], [0]]));
        });
        it("should return error code for invalid data", () => {
            expect(TREND(knownY, newX)).toBe(ErrorCode.REF);
            expect(TREND(mat([[17, "18"]]))).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("TRIMMEAN", TRIMMEAN => {
        const set1 = mat([[4, 5, 6, 7, 2, 3, 4, 5, 1, 2, 3]]);
        const set2 = mat([[3, 6, 7, 8, 6, 5, 4, 2, 1, 9]]);
        const set3 = mat([["a"]]);
        it("should return the correct result", () => {
            expect(TRIMMEAN(set1, 0.2)).toBeCloseTo(3.7777777, 6);
            expect(TRIMMEAN(set2, 0.2)).toBe(5.125);
        });
        it("should return error code for invalid data", () => {
            expect(TRIMMEAN(set3, 0.1)).toBe(ErrorCode.VALUE); //  empty
        });
    });

    moduleTester.testFunction("T.DIST", TDIST => {
        it("should return the Students left-tailed t-distribution", () => {
            expect(TDIST(60, 1, true)).toBeCloseTo(0.99469533, 6);
            expect(TDIST(8, 3, false)).toBeCloseTo(0.00073691, 6);
        });
        it("should return error code for invalid data", () => {
            expect(TDIST(60, 0, true)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("T.DIST.2T", TDIST2T => {
        it("should return the two-tailed Students t-distribution", () => {
            expect(TDIST2T(1.959999998, 60)).toBeCloseTo(0.054645, 6);
        });
        it("should return error code for invalid data", () => {
            expect(TDIST2T(60, 0)).toBe(ErrorCode.NUM);
            expect(TDIST2T(-0.5, 60)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("T.DIST.RT", TDISTRT => {
        it("should return the right-tailed Students t-distribution", () => {
            expect(TDISTRT(1.959999998, 60)).toBeCloseTo(0.02732246, 6);
        });
        it("should return error code for invalid data", () => {
            expect(TDISTRT(60, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("T.INV", TINV => {
        it("should return the left-tailed inverse of the Students t-distribution", () => {
            expect(TINV(0.75, 2)).toBeCloseTo(0.8164966, 6);
        });
        it("should return error code for invalid data", () => {
            expect(TINV(60, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("T.INV.2T", TINV2T => {
        it("should return the two-tailed inverse of the Students t-distribution", () => {
            expect(TINV2T(0.546449, 60)).toBeCloseTo(0.606533, 5);
        });
        it("should return error code for invalid data", () => {
            expect(TINV2T(-0.54, 20)).toBe(ErrorCode.NUM);
            expect(TINV2T(0, 20)).toBe(ErrorCode.NUM);
            expect(TINV2T(1.1, 20)).toBe(ErrorCode.NUM);
            expect(TINV2T(0.54, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("T.TEST", TTEST => {
        const data1 = mat([[3, 4, 5, 8, 9, 1, 2, 4, 5]]);
        const data2 = mat([[6, 19, 3, 2, 14, 4, 5, 17, 1]]);
        it("should return the probability associated with a Students t-Test", () => {
            expect(TTEST(data1, data2, 2, 1)).toBeCloseTo(0.196015785, 6);
            expect(TTEST(data1, data2, 2, 2)).toBeCloseTo(0.191995887, 6);
            expect(TTEST(data1, data2, 2, 3)).toBeCloseTo(0.202293923, 6);
        });
        it("should return error code for invalid data", () => {
            expect(TTEST(data1, data2, 0, 1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testAliasFunction("WEIBULL", "WEIBULL.DIST");

    moduleTester.testFunction("WEIBULL.DIST", WEIBULLDIST => {
        it("should return the correct result", () => {
            expect(WEIBULLDIST(105, 20, 100, true)).toBeCloseTo(0.929581, 5);
            expect(WEIBULLDIST(105, 20, 100, 1)).toBeCloseTo(0.929581, 5);
            expect(WEIBULLDIST(105, 20, 100, false)).toBeCloseTo(0.035589, 5);
            expect(WEIBULLDIST(105, 20, 100, 0)).toBeCloseTo(0.035589, 5);
        });
        it("should return error code for invalid data", () => {
            expect(WEIBULLDIST(-5, 20, 100, false)).toBe(ErrorCode.NUM);
            expect(WEIBULLDIST(105, 0, 100, false)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("Z.TEST", ZTEST => {
        const set = mat([[3, 6, 7, 8, 6, 5, 4, 2, 1, 9]]);
        it("should return the standard error of the predicted y-value for each x in the regression", () => {
            expect(ZTEST(set, 4)).toBeCloseTo(0.090574, 5);
            expect(ZTEST(set, 6)).toBeCloseTo(0.863043, 5);
        });
    });

    moduleTester.testAliasFunction("ZTEST", "Z.TEST");
});
