/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { erf, erfc } from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";
import EngineeringFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/engineeringfuncs";

import { ErrorCode, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/funcs/engineeringfuncs", () => {

    // initialize the test
    const ooxAppPromise = createSpreadsheetApp("ooxml");
    const odfAppPromise = createSpreadsheetApp("odf");
    const moduleTester = createFunctionModuleTester(EngineeringFuncs, ooxAppPromise, odfAppPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("BITAND", BITAND => {
        it("should return the binary AND of the numbers", () => {
            expect(BITAND(12, 10)).toBe(8);
            expect(BITAND(0xFFF_FFFFF_FFFFn, 0x1234_5678_9ABCn)).toBe(0x1234_5678_9ABCn);
        });
        it("should throw #NUM! for invalid numbers", () => {
            expect(BITAND(-1n, 1n)).toBe(ErrorCode.NUM);
            expect(BITAND(1n, -1n)).toBe(ErrorCode.NUM);
            expect(BITAND(0x1_0000_0000_0000n, 1n)).toBe(ErrorCode.NUM);
            expect(BITAND(1n, 0x1_0000_0000_0000n)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("BITLSHIFT", BITLSHIFT => {
        it("should return the number shifted to the left", () => {
            expect(BITLSHIFT(9n, 0)).toBe(9n);
            expect(BITLSHIFT(9n, 1)).toBe(18n);
            expect(BITLSHIFT(9n, 2)).toBe(36n);
            expect(BITLSHIFT(9n, 10)).toBe(0x2400n);
            expect(BITLSHIFT(9n, 44)).toBe(0x9000_0000_0000n);
        });
        it("should return the number shifted to the right", () => {
            expect(BITLSHIFT(36n, -1)).toBe(18n);
            expect(BITLSHIFT(36n, -2)).toBe(9n);
            expect(BITLSHIFT(36n, -3)).toBe(4n);
            expect(BITLSHIFT(36n, -4)).toBe(2n);
            expect(BITLSHIFT(36n, -5)).toBe(1n);
            expect(BITLSHIFT(36n, -6)).toBe(0n);
            expect(BITLSHIFT(36n, -48)).toBe(0n);
        });
        it("should throw #NUM! for invalid parameters", () => {
            expect(BITLSHIFT(9n, 45)).toBe(ErrorCode.NUM);
            expect(BITLSHIFT(9n, 46)).toBe(ErrorCode.NUM);
            expect(BITLSHIFT(9n, 47)).toBe(ErrorCode.NUM);
            expect(BITLSHIFT(9n, 48)).toBe(ErrorCode.NUM);
            expect(BITLSHIFT(9n, 54)).toBe(ErrorCode.NUM);
            expect(BITLSHIFT(9n, -54)).toBe(ErrorCode.NUM);
        });
        it("should throw zero for specific shift counts", () => {
            expect(BITLSHIFT(9n, 49)).toBe(0n);
            expect(BITLSHIFT(9n, 53)).toBe(0n);
            expect(BITLSHIFT(36n, -49)).toBe(0n);
            expect(BITLSHIFT(36n, -53)).toBe(0n);
        });
        it("should truncate bit count", () => {
            expect(BITLSHIFT(36n, 1.9)).toBe(72n);
            expect(BITLSHIFT(36n, -1.9)).toBe(18n);
        });
    });

    moduleTester.testFunction("BITOR", BITOR => {
        it("should return the binary OR of the numbers", () => {
            expect(BITOR(12n, 10n)).toBe(14n);
            expect(BITOR(0xFFFF_FFFF_FFFFn, 0x1234_5678_9ABCn)).toBe(0xFFFF_FFFF_FFFFn);
        });
        it("should throw #NUM! for invalid numbers", () => {
            expect(BITOR(-1n, 1n)).toBe(ErrorCode.NUM);
            expect(BITOR(1n, -1n)).toBe(ErrorCode.NUM);
            expect(BITOR(0x1_0000_0000_0000n, 1n)).toBe(ErrorCode.NUM);
            expect(BITOR(1n, 0x1_0000_0000_0000n)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("BITRSHIFT", BITRSHIFT => {
        it("should return the number shifted to the right", () => {
            expect(BITRSHIFT(36n, 0)).toBe(36n);
            expect(BITRSHIFT(36n, 1)).toBe(18n);
            expect(BITRSHIFT(36n, 2)).toBe(9n);
            expect(BITRSHIFT(36n, 3)).toBe(4n);
            expect(BITRSHIFT(36n, 4)).toBe(2n);
            expect(BITRSHIFT(36n, 5)).toBe(1n);
            expect(BITRSHIFT(36n, 6)).toBe(0n);
            expect(BITRSHIFT(36n, 48)).toBe(0n);
        });
        it("should return the number shifted to the left", () => {
            expect(BITRSHIFT(9n, -1)).toBe(18n);
            expect(BITRSHIFT(9n, -2)).toBe(36n);
            expect(BITRSHIFT(9n, -10)).toBe(0x2400n);
            expect(BITRSHIFT(9n, -44)).toBe(0x9000_0000_0000n);
        });
        it("should throw #NUM! for invalid parameters", () => {
            expect(BITRSHIFT(9n, 54)).toBe(ErrorCode.NUM);
            expect(BITRSHIFT(9n, -45)).toBe(ErrorCode.NUM);
            expect(BITRSHIFT(9n, -46)).toBe(ErrorCode.NUM);
            expect(BITRSHIFT(9n, -47)).toBe(ErrorCode.NUM);
            expect(BITRSHIFT(9n, -48)).toBe(ErrorCode.NUM);
            expect(BITRSHIFT(9n, -54)).toBe(ErrorCode.NUM);
        });
        it("should throw zero for specific shift counts", () => {
            expect(BITRSHIFT(36n, 49)).toBe(0n);
            expect(BITRSHIFT(36n, 53)).toBe(0n);
            expect(BITRSHIFT(9n, -49)).toBe(0n);
            expect(BITRSHIFT(9n, -53)).toBe(0n);
        });
        it("should truncate bit count", () => {
            expect(BITRSHIFT(36n, 1.9)).toBe(18n);
            expect(BITRSHIFT(36n, -1.9)).toBe(72n);
        });
    });

    moduleTester.testFunction("BITXOR", BITXOR => {
        it("should return the binary XOR of the numbers", () => {
            expect(BITXOR(12n, 10n)).toBe(6n);
            expect(BITXOR(0xFFFF_FFFF_FFFFn, 0x1234_5678_9ABCn)).toBe(0xEDCB_A987_6543n);
        });
        it("should throw #NUM! for invalid numbers", () => {
            expect(BITXOR(-1n, 1n)).toBe(ErrorCode.NUM);
            expect(BITXOR(1n, -1n)).toBe(ErrorCode.NUM);
            expect(BITXOR(0x1_0000_0000_0000n, 1n)).toBe(ErrorCode.NUM);
            expect(BITXOR(1n, 0x1_0000_0000_0000n)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("DELTA", DELTA => {
        it("should return the correct result", () => {
            expect(DELTA(5, 4)).toBe(0);
            expect(DELTA(5, 5)).toBe(1);
            expect(DELTA(0.5, 0)).toBe(0);
        });
    });

    moduleTester.testFunction("ERF", (ERF_OOX, ERF_ODF) => {
        it("should return the result of the error function", () => {
            expect(ERF_OOX(0)).toBe(0);
            expect(ERF_OOX(1)).toBe(erf(1));
            expect(ERF_OOX(2)).toBe(erf(2));
            expect(ERF_ODF(0)).toBe(0);
            expect(ERF_ODF(1)).toBe(erf(1));
            expect(ERF_ODF(2)).toBe(erf(2));
        });
        it("should return the difference of two function results", () => {
            expect(ERF_OOX(1, 1)).toBe(0);
            expect(ERF_OOX(1, 2)).toBe(erf(2) - erf(1));
            expect(ERF_OOX(2, 1)).toBe(erf(1) - erf(2));
            expect(ERF_ODF(1, 1)).toBe(0);
            expect(ERF_ODF(1, 2)).toBe(erf(2) - erf(1));
            expect(ERF_ODF(2, 1)).toBe(erf(1) - erf(2));
        });
        it("should throw #NUM! error code for negative numbers in OOXML mode", () => {
            expect(ERF_OOX(-1)).toBe(ErrorCode.NUM);
            expect(ERF_OOX(-2)).toBe(ErrorCode.NUM);
            expect(ERF_OOX(-1, 2)).toBe(ErrorCode.NUM);
            expect(ERF_OOX(2, -2)).toBe(ErrorCode.NUM);
        });
        it("should return a result for negative numbers in ODF mode", () => {
            expect(ERF_ODF(-1)).toBe(erf(-1));
            expect(ERF_ODF(-2)).toBe(erf(-2));
            expect(ERF_ODF(-1, 2)).toBe(erf(2) + erf(1));
            expect(ERF_ODF(2, -2)).toBe(-2 * erf(2));
        });
    });

    moduleTester.testFunction("ERF.PRECISE", ERF_PRECISE => {
        it("should return the result of the error function", () => {
            expect(ERF_PRECISE(0)).toBe(0);
            expect(ERF_PRECISE(1)).toBe(erf(1));
            expect(ERF_PRECISE(2)).toBe(erf(2));
        });
        it("should return a result for negative numbers", () => {
            expect(ERF_PRECISE(-1)).toBe(erf(-1));
            expect(ERF_PRECISE(-2)).toBe(erf(-2));
        });
    });

    moduleTester.testFunction("ERFC", (ERFC_OOX, ERFC_ODF) => {
        it("should return the complementary error function", () => {
            expect(ERFC_OOX(0)).toBe(1);
            expect(ERFC_OOX(1)).toBe(erfc(1));
            expect(ERFC_OOX(2)).toBe(erfc(2));
            expect(ERFC_ODF(0)).toBe(1);
            expect(ERFC_ODF(1)).toBe(erfc(1));
            expect(ERFC_ODF(2)).toBe(erfc(2));
        });
        it("should throw #NUM! error for negative numbers in OOXML mode", () => {
            expect(ERFC_OOX(-1)).toBe(ErrorCode.NUM);
            expect(ERFC_OOX(-2)).toBe(ErrorCode.NUM);
        });
        it("should return a result for negative numbers in ODF mode", () => {
            expect(ERFC_ODF(-1)).toBe(erfc(-1));
            expect(ERFC_ODF(-2)).toBe(erfc(-2));
        });
    });

    moduleTester.testFunction("ERFC.PRECISE", ERFC_PRECISE => {
        it("should return the complementary error function", () => {
            expect(ERFC_PRECISE(0)).toBe(1);
            expect(ERFC_PRECISE(1)).toBe(erfc(1));
            expect(ERFC_PRECISE(2)).toBe(erfc(2));
        });
        it("should return a result for negative numbers", () => {
            expect(ERFC_PRECISE(-1)).toBe(erfc(-1));
            expect(ERFC_PRECISE(-2)).toBe(erfc(-2));
        });
    });

    moduleTester.testFunction("GESTEP", GESTEP => {
        it("should return the correct result", () => {
            expect(GESTEP(5, 4)).toBe(1);
            expect(GESTEP(5, 5)).toBe(1);
            expect(GESTEP(-4, -5)).toBe(1);
            expect(GESTEP(-1)).toBe(0);
            expect(GESTEP(1)).toBe(1);
        });
    });
});
