/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import LogicalFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/logicalfuncs";

import { r3da, mat, ErrorCode, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests =====================================================================

describe("module spreadsheet/model/formula/funcs/logicalfuncs", () => {

    // initialize the test
    const appPromise = createSpreadsheetApp("ooxml");
    const moduleTester = createFunctionModuleTester(LogicalFuncs, appPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("AND", AND => {
        it("should return the logical AND of the arguments", () => {
            expect(AND(false)).toBeFalse();
            expect(AND(true)).toBeTrue();
            expect(AND(false, false)).toBeFalse();
            expect(AND(false, true)).toBeFalse();
            expect(AND(true, false)).toBeFalse();
            expect(AND(true, true)).toBeTrue();
            expect(AND(false, false, false)).toBeFalse();
            expect(AND(false, false, true)).toBeFalse();
            expect(AND(false, true, false)).toBeFalse();
            expect(AND(false, true, true)).toBeFalse();
            expect(AND(true, false, false)).toBeFalse();
            expect(AND(true, false, true)).toBeFalse();
            expect(AND(true, true, false)).toBeFalse();
            expect(AND(true, true, true)).toBeTrue();
        });
    });

    moduleTester.testFunction("FALSE", FALSE => {
        it("should return FALSE", () => {
            expect(FALSE()).toBeFalse();
        });
    });

    moduleTester.testFunction("IF", { toOperand: [1, 2] }, IF => {
        const mat1 = mat([[1]]);
        const ranges = r3da("0:0!A1:A1");
        it("should return first parameter, if condition is TRUE", () => {
            expect(IF(true, 2, "x")).toBe(2);
            expect(IF(true, "a", "x")).toBe("a");
            expect(IF(true, false, "x")).toBeFalse();
            expect(IF(true, ErrorCode.DIV0, "x")).toBe(ErrorCode.DIV0);
            expect(IF(true, null, "x")).toBeNull();
            expect(IF(true, mat1, "x")).toEqual(mat1);
            expect(IF(true, ranges, "x")).toBe(ranges);
        });
        it("should return second parameter, if condition is FALSE", () => {
            expect(IF(false, "x", 2)).toBe(2);
            expect(IF(false, "x", "a")).toBe("a");
            expect(IF(false, "x", true)).toBeTrue();
            expect(IF(false, "x", ErrorCode.DIV0)).toBe(ErrorCode.DIV0);
            expect(IF(false, "x", null)).toBeNull();
            expect(IF(false, "x", mat1)).toEqual(mat1);
            expect(IF(false, "x", ranges)).toBe(ranges);
        });
        it("should return FALSE, if second parameter is missing, and condition is FALSE", () => {
            expect(IF(true, 2)).toBe(2);
            expect(IF(false, 2)).toBeFalse();
        });
        it("should ignore error codes in unused parameter", () => {
            expect(IF(true, 2, ErrorCode.DIV0)).toBe(2);
            expect(IF(false, ErrorCode.DIV0, 2)).toBe(2);
        });
    });

    moduleTester.testFunction("IFERROR", IFERROR => {
        it("should return the first parameter, if not an error code", () => {
            expect(IFERROR(2, "x")).toBe(2);
            expect(IFERROR("a", "x")).toBe("a");
            expect(IFERROR(false, "x")).toBeFalse();
        });
        it("should return the second parameter, if first is an error code", () => {
            expect(IFERROR(ErrorCode.DIV0, 2)).toBe(2);
            expect(IFERROR(ErrorCode.NA, "a")).toBe("a");
            expect(IFERROR(ErrorCode.NAME, false)).toBeFalse();
            expect(IFERROR(ErrorCode.NULL, 2)).toBe(2);
            expect(IFERROR(ErrorCode.NUM, "a")).toBe("a");
            expect(IFERROR(ErrorCode.REF, false)).toBeFalse();
            expect(IFERROR(ErrorCode.VALUE, 2)).toBe(2);
            expect(IFERROR(ErrorCode.DIV0, ErrorCode.NAME)).toBe(ErrorCode.NAME);
        });
    });

    moduleTester.testFunction("IFNA", IFNA => {
        it("should return the first parameter, if not the #N/A error code", () => {
            expect(IFNA(2, "x")).toBe(2);
            expect(IFNA("a", "x")).toBe("a");
            expect(IFNA(false, "x")).toBeFalse();
            expect(IFNA(ErrorCode.DIV0, "x")).toBe(ErrorCode.DIV0);
            expect(IFNA(ErrorCode.NAME, "x")).toBe(ErrorCode.NAME);
            expect(IFNA(ErrorCode.NULL, "x")).toBe(ErrorCode.NULL);
            expect(IFNA(ErrorCode.NUM, "x")).toBe(ErrorCode.NUM);
            expect(IFNA(ErrorCode.REF, "x")).toBe(ErrorCode.REF);
            expect(IFNA(ErrorCode.VALUE, "x")).toBe(ErrorCode.VALUE);
        });
        it("should return the second parameter, if first is the #N/A error code", () => {
            expect(IFNA(ErrorCode.NA, 2)).toBe(2);
            expect(IFNA(ErrorCode.NA, "a")).toBe("a");
            expect(IFNA(ErrorCode.NA, false)).toBeFalse();
            expect(IFNA(ErrorCode.NA, ErrorCode.NAME)).toBe(ErrorCode.NAME);
        });
    });

    moduleTester.testFunction("IFS", { toOperand: [1, 3, 5, 7, 9, 11, 13] }, IFS => {
        const mat1 = mat([[1]]);
        const ranges = r3da("0:0!A1:A1");
        it("should return parameter following the first TRUE condition", () => {
            expect(IFS(true, 2, true, "a", true, false, true, ErrorCode.DIV0, true, null, true, mat1, true, ranges)).toBe(2);
            expect(IFS(false, 2, true, "a", true, false, true, ErrorCode.DIV0, true, null, true, mat1, true, ranges)).toBe("a");
            expect(IFS(false, 2, false, "a", true, false, true, ErrorCode.DIV0, true, null, true, mat1, true, ranges)).toBeFalse();
            expect(IFS(false, 2, false, "a", false, false, true, ErrorCode.DIV0, true, null, true, mat1, true, ranges)).toBe(ErrorCode.DIV0);
            expect(IFS(false, 2, false, "a", false, false, false, ErrorCode.DIV0, true, null, true, mat1, true, ranges)).toBeNull();
            expect(IFS(false, 2, false, "a", false, false, false, ErrorCode.DIV0, false, null, true, mat1, true, ranges)).toEqual(mat1);
            expect(IFS(false, 2, false, "a", false, false, false, ErrorCode.DIV0, false, null, false, mat1, true, ranges)).toBe(ranges);
        });
        it("should return #N/A if all conditions are FALSE", () => {
            expect(IFS(false, 2, false, "a", false, false, false, ErrorCode.DIV0, false, null, false, mat1, false, ranges)).toBe(ErrorCode.NA);
        });
    });

    moduleTester.testFunction("NOT", NOT => {
        it("should return the negated argument", () => {
            expect(NOT(false)).toBeTrue();
            expect(NOT(true)).toBeFalse();
        });
    });

    moduleTester.testFunction("OR", OR => {
        it("should return the logical OR of the arguments", () => {
            expect(OR(false)).toBeFalse();
            expect(OR(true)).toBeTrue();
            expect(OR(false, false)).toBeFalse();
            expect(OR(false, true)).toBeTrue();
            expect(OR(true, false)).toBeTrue();
            expect(OR(true, true)).toBeTrue();
            expect(OR(false, false, false)).toBeFalse();
            expect(OR(false, false, true)).toBeTrue();
            expect(OR(false, true, false)).toBeTrue();
            expect(OR(false, true, true)).toBeTrue();
            expect(OR(true, false, false)).toBeTrue();
            expect(OR(true, false, true)).toBeTrue();
            expect(OR(true, true, false)).toBeTrue();
            expect(OR(true, true, true)).toBeTrue();
        });
    });

    moduleTester.testFunction("TRUE", TRUE => {
        it("should return TRUE", () => {
            expect(TRUE()).toBeTrue();
        });
    });

    moduleTester.testFunction("XOR", XOR => {
        it("should return the logical XOR of the arguments", () => {
            expect(XOR(false)).toBeFalse();
            expect(XOR(true)).toBeTrue();
            expect(XOR(false, false)).toBeFalse();
            expect(XOR(false, true)).toBeTrue();
            expect(XOR(true, false)).toBeTrue();
            expect(XOR(true, true)).toBeFalse();
            expect(XOR(false, false, false)).toBeFalse();
            expect(XOR(false, false, true)).toBeTrue();
            expect(XOR(false, true, false)).toBeTrue();
            expect(XOR(false, true, true)).toBeFalse();
            expect(XOR(true, false, false)).toBeTrue();
            expect(XOR(true, false, true)).toBeFalse();
            expect(XOR(true, true, false)).toBeFalse();
            expect(XOR(true, true, true)).toBeTrue();
        });
    });
});
