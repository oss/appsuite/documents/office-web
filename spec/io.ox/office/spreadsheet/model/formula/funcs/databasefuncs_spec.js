/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import DatabaseFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/databasefuncs";

import { r3d, ErrorCode, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/funcs/databasefuncs", () => {

    function TestContainer(ooxResolver, odfResolver, database, field) {
        this.testValue = (criteria, ooxResult, odfResult) => {
            expect(ooxResolver(database, field, criteria)).toBe(ooxResult);
            expect(odfResolver(database, field, criteria)).toBe(odfResult || ooxResult);
        };
    }

    // the operations to be applied at the test document
    const OPERATIONS = [
        { name: "setDocumentAttributes", attrs: { document: { cols: 16384, rows: 1048576, activeSheet: 0 } } },

        { name: "insertSheet", sheet: 0, sheetName: "DAverageSheet" },
        { name: "changeCells", sheet: 0, start: "A1", contents: [
            ["Tree", "Height", "Height", "Age", "Profit", "Height", "Height", "Height", "Height"],
            ["Apple", {}, ">8", null, null, "<16", {}, ">8", "<8"],
            ["Pear", null, null, ">15", {}, ">8", ">8"],
            ["Tree", "Height", "Age", "Yield", "Profit"],
            ["Apple", 18, 20, 40, 105],
            ["Apple", 12, 12, 31, 96],
            ["Cherry", 13, 14, 9, 105],
            ["Apple", 14, 15, 5, 75],
            ["Pear", 9, 8, 8, 76.8],
            ["Apple", 8, 9, 6, 45]
        ] },

        { name: "insertSheet", sheet: 1, sheetName: "DCountSheet" },
        { name: "changeCells", sheet: 1, start: "A1", contents: [
            ["Tree", "Height", "Age", "Yield", "Profit", "Height", "Height"],
            ["=Apple", ">10", { r: 3 }, "<16", ">30"],
            ["*e*"],
            [],
            ["Tree", "Height", "Age", "Yield", "Profit"],
            ["Apple", 18, 20, 14, "$105"],
            ["Pear", 12, "\xfc", 10, "$96"],
            ["Cherry", 13, 14, 9, "$105"],
            ["Apple", 14, "N/A", 10, "$75"],
            ["Pear", 9, 8, 8, "$77"],
            ["Apple", 12, 11, 6, "$45"]
        ] },

        { name: "insertSheet", sheet: 2, sheetName: "DCountASheet" },
        { name: "changeCells", sheet: 2, start: "A1", contents: [
            ["Tree", "Height", "Age", "Yield", 1, "Height", "Height"],
            ["=Apple", ">10", ">10", {}, ">70", "<16", ">30"],
            ["*e*"],
            [],
            ["Tree", "Height", "Age", "Yield", 1],
            ["Apple", 18, 20, 14, 105],
            ["Pear", 12, "\xfc", 10, 96],
            ["Cherry", 13, 14, 9, 105],
            ["Apple", 14, "N/A", 10, 75],
            ["Pear", 9, 8, 8, 77],
            ["Apple", 12, 11, 6, 45]
        ] },

        { name: "insertSheet", sheet: 3, sheetName: "DGetSheet" },
        { name: "changeCells", sheet: 3, start: "A1", contents: [
            ["Tree", "Height", "Age", "Yield", "Profit", "Height", "Yield"],
            ["=Apple", ">10", { r: 3 }, "<16", ">100"],
            ["Pear", ">12"],
            [],
            ["Tree", "Height", "Age", "Yield", "Profit"],
            ["Apple", 18, 20, 14, "$105"],
            ["Pear", 12, 12, 10, "$96"],
            ["Cherry", 13, 14, 9, "$105"],
            ["Apple", 14, 15, 10, "$75"],
            ["Pear", 9, 8, 8, "$77"],
            ["Apple", 8, 9, 6, "$45"]
        ] },

        { name: "insertSheet", sheet: 4, sheetName: "DMaxSheet" },
        { name: "changeCells", sheet: 4, start: "A1", contents: [
            ["Tree", "Height", "Age", "Yield", "Profit", "Height", "Height"],
            ["=Apple", ">10", { r: 3 }, "<16", ">100"],
            ["=Pear"],
            [],
            ["Tree", "Height", "Age", "Yield", "Profit", "Empty"],
            ["Apple", 18, 20, 14, 105],
            ["Pear", 12, 12, 10, 96],
            ["Cherry", 13, 14, 9, 105],
            ["Apple", 14, 15, 10, 75],
            ["Pear", 9, 8, 8, 77],
            ["Apple", 8, 9, 6, 45]
        ] },

        { name: "insertSheet", sheet: 5, sheetName: "DMinSheet" },
        { name: "changeCells", sheet: 5, start: "A1", contents: [
            ["Tree", "Height", "Age", "Yield", "Profit", "Height", "Height"],
            ["=Apple", ">10", { r: 3 }, "<16", ">100"],
            ["=Pear"],
            [],
            ["Tree", "Height", "Age", "Yield", "Profit", "Empty"],
            ["Apple", 18, 20, 14, 105],
            ["Pear", 12, 12, 10, 96],
            ["Cherry", 13, 14, 9, 105],
            ["Apple", 14, 15, 10, 75],
            ["Pear", 9, 8, 8, 77],
            ["Apple", 8, 9, 6, 45]
        ] },

        { name: "insertSheet", sheet: 6, sheetName: "DProductSheet" },
        { name: "changeCells", sheet: 6, start: "A1", contents: [
            ["Tree", "Height", "Age", "Yield", "Profit", "Height", "Height"],
            ["Apple", ">10", { r: 3 }, "<16", 8],
            ["Pear"],
            [],
            ["Tree", "Height", "Age", "Yield", "Profit", "Width"],
            ["Apple", 18, 20, 14, 105, 100],
            ["Pear", 12, {}, -10, 96, 0],
            ["Cherry", 13, {}, 9, -1, 88],
            ["Apple", 14, 15, 10, 75, 10],
            ["Pear", 9, 10, 8, 77, 20],
            ["Apple", 8, 9, 6, 45, 52]
        ] },

        { name: "insertSheet", sheet: 7, sheetName: "DefaultSheet" },
        { name: "changeCells", sheet: 7, start: "A1", contents: [
            ["Tree", "Height", "Age", "Yield", "Profit", "Height"],
            ["=Apple", ">10", { r: 3 }, "<16"],
            ["=Pear"],
            [],
            ["Tree", "Height", "Age", "Yield", "Profit"],
            ["Apple", 18, 20, 14, 105],
            ["Pear", 12, 12, "A", 96],
            ["Cherry", 13, 14, 9, 105],
            ["Apple", 14, 15, 10, 75],
            ["Pear", 9, 8, 8, 77],
            ["Apple", 8, 9, 6, 45]
        ] }
    ];

    // initialize the test
    const ooxAppPromise = createSpreadsheetApp("ooxml", OPERATIONS);
    const odfAppPromise = createSpreadsheetApp("odf", OPERATIONS);
    const moduleTester = createFunctionModuleTester(DatabaseFuncs, ooxAppPromise, odfAppPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("DAVERAGE", (DAVERAGE, DAVERAGE_ODF) => {
        const database = r3d("0:0!A4:E10");
        const testContainer = new TestContainer(DAVERAGE, DAVERAGE_ODF, database, 4);
        it("should return the averageof the defined column with the criteria", () => {
            testContainer.testValue(r3d("0:0!B1:B3"), 16.5);
            testContainer.testValue(r3d("0:0!C1:C2"), 18.6);
            testContainer.testValue(r3d("0:0!C1:C3"), 16.5, 18.6);
            testContainer.testValue(r3d("0:0!B1:C2"), 18.6);
            testContainer.testValue(r3d("0:0!B1:F2"), 13.25);
            testContainer.testValue(r3d("0:0!F1:F3"), 16.5);
            testContainer.testValue(r3d("0:0!G1:G3"), 16.5, 18.6);
            testContainer.testValue(r3d("0:0!C1:D3"), 18.6);
        });
        it("should return error codes", () => {
            // result for average is empty
            testContainer.testValue(r3d("0:0!H1:I2"), ErrorCode.DIV0);
            // database column labels are incorrect
            expect(DAVERAGE(r3d("0:0!A5:E10"), "Yield", r3d("0:0!A1:F3"))).toBe(ErrorCode.VALUE);
            // criteria column labels are incorrect
            testContainer.testValue(r3d("0:0!A2:G3"), ErrorCode.DIV0);
            // the field or db column name for average is not valid
            expect(DAVERAGE(database, "WrongFieldName", r3d("0:0!B1:B3"))).toBe(ErrorCode.VALUE);
            // criteria row size is lower than 2, first row are the labels, second row is the criteria
            testContainer.testValue(r3d("0:0!A1:A1"), ErrorCode.VALUE);
            // database row size is lower than 2, first row are the labels, second row is the criteria
            expect(DAVERAGE(r3d("0:0!A5:E5"), 4, r3d("0:0!A1:F3"))).toBe(ErrorCode.VALUE);
            // test null field name
            expect(DAVERAGE(database, null, r3d("0:0!B1:B3"))).toBe(ErrorCode.NAME);
        });
    });

    moduleTester.testFunction("DCOUNT", (DCOUNT, DCOUNT_ODF) => {
        const database = r3d("1:1!A5:E11");
        const testContainer = new TestContainer(DCOUNT, DCOUNT_ODF, database, "age");
        it("should return the count the defined column with the criteria", () => {
            testContainer.testValue(r3d("1:1!A1:F2"), 1);
            testContainer.testValue(r3d("1:1!G1:G2"), 0);
            // if the odf regex feature is implemented change the test to testValue(r3d("0:0!A1:A3"), 4, 2);
            testContainer.testValue(r3d("1:1!A1:A3"), 4);
            // test null field name
            expect(DCOUNT(database, null, r3d("1:1!A1:F2"))).toBe(2);
        });
    });

    moduleTester.testFunction("DCOUNTA", (DCOUNTA, DCOUNTA_ODF) => {
        const database = r3d("2:2!A5:E11");
        const testContainer = new TestContainer(DCOUNTA, DCOUNTA_ODF, database, "age");
        it("should return the count the defined column with the criteria", () => {
            testContainer.testValue(r3d("2:2!A1:F2"), 0);
            testContainer.testValue(r3d("2:2!G1:G2"), 0);
            // without field value count all rows
            expect(DCOUNTA(database, null, r3d("2:2!B1:C2"))).toBe(3);
            // Labels are numbers
            expect(DCOUNTA(database, 1, r3d("2:2!E1:E2"))).toBe(5);
        });
    });

    moduleTester.testFunction("DGET", (DGET, DGET_ODF) => {
        const database = r3d("3:3!A5:E11");
        const testContainer = new TestContainer(DGET, DGET_ODF, database, "yieLd");
        it("should return the count the defined column with the criteria", () => {
            // More than one match
            testContainer.testValue(r3d("3:3!A1:A3"), ErrorCode.NUM);
            // Exactly one match
            testContainer.testValue(r3d("3:3!A1:F2"), 10);
            // Without a match
            testContainer.testValue(r3d("3:3!G1:G2"), ErrorCode.VALUE);
            // One match with word
            expect(DGET(database, 1, r3d("3:3!A1:F3"))).toBe("Apple");
        });
    });

    moduleTester.testFunction("DMAX", (DMAX, DMAX_ODF) => {
        const database = r3d("4:4!A5:E11");
        const testContainer = new TestContainer(DMAX, DMAX_ODF, database, 5);
        it("should return the count the defined column with the criteria", () => {
            testContainer.testValue(r3d("4:4!A1:F3"), 96);
            // No match
            testContainer.testValue(r3d("4:4!G1:G2"), 0);
            // Only Text
            expect(DMAX(database, "TRee", r3d("4:4!A1:F3"))).toBe(0);
            // Only empty cells
            expect(DMAX(r3d("4:4!A5:F11"), "EmPty", r3d("4:4!A1:F3"))).toBe(0);
        });
    });

    moduleTester.testFunction("DMIN", (DMIN, DMIN_ODF) => {
        const database = r3d("5:5!A5:E11");
        const testContainer = new TestContainer(DMIN, DMIN_ODF, database, 5);
        it("should return the count the defined column with the criteria", () => {
            testContainer.testValue(r3d("5:5!A1:F3"), 75);
            // No match
            testContainer.testValue(r3d("5:5!G1:G2"), 0);
            // Only Text
            expect(DMIN(database, "TRee", r3d("5:5!A1:F3"))).toBe(0);
            // Only empty cells
            expect(DMIN(r3d("5:5!A5:F11"), "EmPty", r3d("5:5!A1:F3"))).toBe(0);
        });
    });

    moduleTester.testFunction("DPRODUCT", (DPRODUCT, DPRODUCT_ODF) => {
        const database = r3d("6:6!A5:F11");
        const testContainer = new TestContainer(DPRODUCT, DPRODUCT_ODF, database, "width");
        it("should return the count the defined column with the criteria", () => {
            expect(DPRODUCT(database, "Yield", r3d("6:6!A1:F3"))).toBe(-800);
            expect(DPRODUCT(database, "Profit", r3d("6:6!A1:F3"))).toBe(554400);
            expect(DPRODUCT(database, "age", r3d("6:6!A1:F3"))).toBe(150);
            expect(DPRODUCT(database, 1, r3d("6:6!A1:F3"))).toBe(0);
            testContainer.testValue(r3d("6:6!A1:F3"), 0);
            testContainer.testValue(r3d("6:6!G1:G2"), 52);
        });
    });

    moduleTester.testFunction("DSTDEV", DSTDEV => {
        const database = r3d("7:7!A5:F11");
        it("should return the count the defined column with the criteria", () => {
            expect(DSTDEV(database, "yield", r3d("7:7!A1:A3"))).toBeCloseTo(3.415650255, 9);
            expect(DSTDEV(database, 1, r3d("7:7!B1:B2"))).toBe(ErrorCode.DIV0);
            expect(DSTDEV(database, "yield", r3d("7:7!B1:B2"))).toBeCloseTo(2.645751311, 9);
        });
    });

    moduleTester.testFunction("DSTDEVP", DSTDEVP => {
        const database = r3d("7:7!A5:F11");
        it("should return the count the defined column with the criteria", () => {
            expect(DSTDEVP(database, "yield", r3d("7:7!A1:A3"))).toBeCloseTo(2.958039892, 9);
            expect(DSTDEVP(database, 1, r3d("7:7!B1:B2"))).toBe(ErrorCode.DIV0);
            expect(DSTDEVP(database, "yield", r3d("7:7!B1:B2"))).toBeCloseTo(2.160246899, 9);
        });
    });

    moduleTester.testFunction("DSUM", (DSUM, DSUM_ODF) => {
        const database = r3d("7:7!A5:F11");
        const testContainer = new TestContainer(DSUM, DSUM_ODF, database, "yield");
        it("should return the count the defined column with the criteria", () => {
            testContainer.testValue(r3d("7:7!A1:A3"), 38);
            expect(DSUM(database, 1, r3d("7:7!B1:B2"))).toBe(0);
            testContainer.testValue(r3d("7:7!B1:B2"), 33);
        });
    });

    moduleTester.testFunction("DVAR", DVAR => {
        const database = r3d("7:7!A5:F11");
        it("should return the count the defined column with the criteria", () => {
            expect(DVAR(database, "yield", r3d("7:7!A1:A3"))).toBeCloseTo(11.66666667, 7);
            expect(DVAR(database, 1, r3d("7:7!B1:B2"))).toBe(ErrorCode.DIV0);
            expect(DVAR(database, "yield", r3d("7:7!B1:B2"))).toBe(7);
        });
    });

    moduleTester.testFunction("DVARP", DVARP => {
        const database = r3d("7:7!A5:F11");
        it("should return the count the defined column with the criteria", () => {
            expect(DVARP(database, "yield", r3d("7:7!A1:A3"))).toBe(8.75);
            expect(DVARP(database, 1, r3d("7:7!B1:B2"))).toBe(ErrorCode.DIV0);
            expect(DVARP(database, "yield", r3d("7:7!B1:B2"))).toBeCloseTo(4.666666667, 9);
        });
    });
});
