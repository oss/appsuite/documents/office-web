/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import FinancialFuncs from "@/io.ox/office/spreadsheet/model/formula/funcs/financialfuncs";

import { dt, mat, dtmat, ErrorCode, createSpreadsheetApp, createFunctionModuleTester } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/funcs/financialfuncs", () => {

    // initialize the test
    const appPromise = createSpreadsheetApp("ooxml");
    const moduleTester = createFunctionModuleTester(FinancialFuncs, appPromise);

    // function implementations -----------------------------------------------

    moduleTester.testFunction("ACCRINT", ACCRINT => {
        it("should return correct results", () => {
            expect(ACCRINT(dt("2008-03-01"), dt("2008-08-31"), dt("2008-05-01"), 0.1, 1000, 2, 0)).toBeAlmost(16 + 2 / 3);

            expect(ACCRINT(dt("2008-04-01"), dt("2010-04-01"), dt("2018-04-01"), 1, 1, 1, 0, true)).toBe(10);
            //expect(ACCRINT(d("2008-04-01"), d("2010-04-01"), d("2018-04-01"), 1, 1, 1, 0, false)).toBe(9);

            expect(ACCRINT(dt("2008-04-05"), dt("2008-08-31"), dt("2008-05-01"), 0.1, 1000, 2, 0, true)).toBeAlmost(7 + 2 / 9);
            //expect(ACCRINT(d("2008-03-05"), d("2008-08-31"), d("2008-05-01"), 0.1, 1000, 2, 0, false)).toBeAlmost(15 + 5 / 9);

            expect(ACCRINT(dt("2008-03-01"), dt("2010-03-01"), dt("2009-05-01"), 0.1, 1000, 2, 0, true)).toBeAlmost(116 + 2 / 3);
            //expect(ACCRINT(d("2008-03-01"), d("2010-03-01"), d("2009-05-01"), 0.1, 1000, 2, 0, false)).toBeAlmost(-33 + 1 / 3);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(ACCRINT(dt("2009-05-02"), dt("2010-03-01"), dt("2009-05-01"), 0.1, 1000, 2, 0, true)).toBe(ErrorCode.NUM);
            expect(ACCRINT(dt("2008-03-01"), dt("2010-03-01"), dt("2009-05-01"), 0.1, -1000, 2, 0, true)).toBe(ErrorCode.NUM);
            expect(ACCRINT(dt("2008-03-01"), dt("2010-03-01"), dt("2009-05-01"), -0.1, 1000, 2, 0, true)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("ACCRINTM", ACCRINTM => {
        it("should return correct results", () => {
            expect(ACCRINTM(dt("2008-04-01"), dt("2008-06-15"), 0.1, 1000, 3)).toBeCloseTo(20.54794521, 5);
            expect(ACCRINTM(dt("2008-04-01"), dt("2009-04-01"), 0.1, 1000, 0)).toBe(100);
            expect(ACCRINTM(dt("2008-04-01"), dt("2009-04-01"), 0.1, 1000, 1)).toBe(100);
            expect(ACCRINTM(dt("2008-04-01"), dt("2009-04-01"), 0.1, 1000, 3)).toBe(100);
            expect(ACCRINTM(dt("2008-04-01"), dt("2009-04-01"), 0.1, 1000, 4)).toBe(100);
            expect(ACCRINTM(dt("2008-04-06"), dt("2009-04-01"), 0.1, 1000, 2)).toBe(100);
        });
        it("should return correct results within leapyears", () => {
            expect(ACCRINTM(dt("2008-02-15"), dt("2009-02-15"), 0.1, 1000, 0)).toBe(100);
            expect(ACCRINTM(dt("2008-02-15"), dt("2009-02-15"), 0.1, 1000, 1)).toBe(100);
            expect(ACCRINTM(dt("2008-02-15"), dt("2009-02-09"), 0.1, 1000, 2)).toBe(100);
            expect(ACCRINTM(dt("2008-02-15"), dt("2009-02-14"), 0.1, 1000, 3)).toBe(100);
            expect(ACCRINTM(dt("2008-02-15"), dt("2009-02-15"), 0.1, 1000, 4)).toBe(100);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(ACCRINTM(dt("2009-04-01"), dt("2008-04-06"), 0.1, 1000, 2)).toBe(ErrorCode.NUM);
            expect(ACCRINTM(dt("2008-04-01"), dt("2008-06-15"), 0, 1000, 2)).toBe(ErrorCode.NUM);
            expect(ACCRINTM(dt("2008-04-01"), dt("2008-06-15"), 0.1, 0, 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("AMORDEGRC", AMORDEGRC => {
        it("should return correct results", () => {
            expect(AMORDEGRC(2400, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 0)).toBe(776);
            expect(AMORDEGRC(2400, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 1)).toBe(776);
            expect(AMORDEGRC(2400, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 3)).toBe(776);
            expect(AMORDEGRC(2400, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 4)).toBe(777);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(AMORDEGRC(2400, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 2)).toBe(ErrorCode.NUM);
            expect(AMORDEGRC(0, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 0)).toBe(ErrorCode.NUM);
            expect(AMORDEGRC(2400, dt("2008-08-19"), dt("2008-12-31"), -1, 1, 0.15, 0)).toBe(ErrorCode.NUM);
            expect(AMORDEGRC(299, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("AMORLINC", AMORLINC => {
        it("should return correct results", () => {
            expect(AMORLINC(2400, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 0)).toBe(360);
            expect(AMORLINC(2400, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 1)).toBe(360);
            expect(AMORLINC(2400, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 3)).toBe(360);
            expect(AMORLINC(2400, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 4)).toBe(360);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(AMORLINC(2400, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 2)).toBe(ErrorCode.NUM);
            expect(AMORLINC(0, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 0)).toBe(ErrorCode.NUM);
            expect(AMORLINC(2400, dt("2008-08-19"), dt("2008-12-31"), -1, 1, 0.15, 0)).toBe(ErrorCode.NUM);
            expect(AMORLINC(299, dt("2008-08-19"), dt("2008-12-31"), 300, 1, 0.15, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("COUPDAYBS", COUPDAYBS => {
        it("should return correct results", () => {
            expect(COUPDAYBS(dt("2011-01-25"), dt("2011-11-15"), 2)).toBe(70);
            expect(COUPDAYBS(dt("2011-01-25"), dt("2011-11-15"), 2, 1)).toBe(71);
            expect(COUPDAYBS(dt("2011-01-25"), dt("2011-11-15"), 2, 2)).toBe(71);
            expect(COUPDAYBS(dt("2011-01-25"), dt("2011-11-15"), 2, 3)).toBe(71);
            expect(COUPDAYBS(dt("2011-01-25"), dt("2011-11-15"), 2, 4)).toBe(70);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(COUPDAYBS(dt("2011-11-15"), dt("2011-01-25"), 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("COUPDAYS", COUPDAYS => {
        it("should return correct results", () => {
            expect(COUPDAYS(dt("2011-01-25"), dt("2011-11-15"), 2)).toBe(180);
            expect(COUPDAYS(dt("2011-01-25"), dt("2011-11-15"), 2, 1)).toBe(181);
            expect(COUPDAYS(dt("2011-01-25"), dt("2011-11-15"), 2, 2)).toBe(180);
            expect(COUPDAYS(dt("2011-01-25"), dt("2011-11-15"), 2, 3)).toBe(182.5);
            expect(COUPDAYS(dt("2011-01-25"), dt("2011-11-15"), 2, 4)).toBe(180);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(COUPDAYS(dt("2011-11-15"), dt("2011-01-25"), 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("COUPDAYSNC", COUPDAYSNC => {
        it("should return correct results", () => {
            expect(COUPDAYSNC(dt("2011-01-25"), dt("2011-11-15"), 2)).toBe(110);
            expect(COUPDAYSNC(dt("2011-01-25"), dt("2011-11-15"), 2, 0)).toBe(110);
            expect(COUPDAYSNC(dt("2011-01-25"), dt("2011-11-15"), 2, 4)).toBe(110);

            expect(COUPDAYSNC(dt("2011-01-25"), dt("2011-11-15"), 2, 1)).toBe(110);
            expect(COUPDAYSNC(dt("2011-01-25"), dt("2011-11-15"), 2, 2)).toBe(110);
            expect(COUPDAYSNC(dt("2011-01-25"), dt("2011-11-15"), 2, 3)).toBe(110);

            expect(COUPDAYSNC(dt("2011-01-25"), dt("2012-01-25"), 2, 1)).toBe(181);
            expect(COUPDAYSNC(dt("2011-01-25"), dt("2012-01-25"), 2, 2)).toBe(181);
            expect(COUPDAYSNC(dt("2011-01-25"), dt("2012-01-25"), 2, 3)).toBe(181);
            expect(COUPDAYSNC(dt("2011-01-25"), dt("2012-01-25"), 2, 4)).toBe(180);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(COUPDAYSNC(dt("2011-11-15"), dt("2011-01-25"), 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("COUPNCD", COUPNCD => {
        it("should return correct results", () => {
            expect(COUPNCD(dt("2011-01-25"), dt("2011-11-15"), 2, 1)).toEqual(dt("2011-05-15"));
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(COUPNCD(dt("2011-11-15"), dt("2011-01-25"), 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("COUPNUM", COUPNUM => {
        it("should return correct results", () => {
            expect(COUPNUM(dt("2007-01-01"), dt("2008-01-01"), 1)).toBe(1);
            expect(COUPNUM(dt("2007-01-01"), dt("2008-01-01"), 2)).toBe(2);
            expect(COUPNUM(dt("2007-01-01"), dt("2008-01-01"), 4)).toBe(4);
            expect(COUPNUM(dt("2007-01-01"), dt("2007-04-01"), 4)).toBe(1);
            expect(COUPNUM(dt("2007-01-01"), dt("2007-04-02"), 4)).toBe(2);
            expect(COUPNUM(dt("2011-01-01"), dt("2012-01-01"), 4, 1)).toBe(4);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(COUPNUM(dt("2011-11-15"), dt("2011-01-25"), 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("COUPPCD", COUPPCD => {
        it("should return correct results", () => {
            expect(COUPPCD(dt("2011-01-25"), dt("2011-11-15"), 2, 0)).toEqual(dt("2010-11-15"));
            expect(COUPPCD(dt("2011-01-25"), dt("2011-11-15"), 2, 1)).toEqual(dt("2010-11-15"));
            expect(COUPPCD(dt("2011-01-25"), dt("2011-11-15"), 2, 2)).toEqual(dt("2010-11-15"));
            expect(COUPPCD(dt("2011-01-25"), dt("2011-11-15"), 2, 3)).toEqual(dt("2010-11-15"));
            expect(COUPPCD(dt("2011-01-25"), dt("2011-11-15"), 2, 4)).toEqual(dt("2010-11-15"));

            expect(COUPPCD(dt("2012-02-29"), dt("2050-08-08"), 1)).toEqual(dt("2011-08-08"));
            expect(COUPPCD(dt("2012-02-29"), dt("2050-08-08"), 2)).toEqual(dt("2012-02-08"));
            expect(COUPPCD(dt("2012-02-29"), dt("2050-08-08"), 4)).toEqual(dt("2012-02-08"));
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(COUPPCD(dt("2011-11-15"), dt("2011-01-25"), 2, 1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("CUMIPMT", CUMIPMT => {
        it("should return correct results", () => {
            expect(CUMIPMT(0.09 / 12, 30 * 12, 125000, 13, 24, 0)).toBeCloseTo(-11135.23213, 5);
            expect(CUMIPMT(0.09 / 12, 30 * 12, 125000, 1, 1, 0)).toBe(-937.5);
            expect(CUMIPMT(0.09 / 12, 30 * 12, 125000, 13, 24, 1)).toBeCloseTo(-11052.33958, 4);
            expect(CUMIPMT(0.09 / 12, 30 * 12, 125000, 1, 1, 1)).toBe(0);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(CUMIPMT(0.09 / 12, 30 * 12, 125000, 13, 24, 2)).toBe(ErrorCode.NUM);
            expect(CUMIPMT(0.09 / 12, 30 * 12, 125000, 13, 24, -1)).toBe(ErrorCode.NUM);
            expect(CUMIPMT(0.09 / 12, 30 * 12, 125000, 24, 13, 1)).toBe(ErrorCode.NUM);
            expect(CUMIPMT(0.09 / 12, 24, 125000, 13, 360, 1)).toBe(ErrorCode.NUM);
            expect(CUMIPMT(0.09 / 12, 30 * 12, 125000, 0, 24, 1)).toBe(ErrorCode.NUM);

            expect(CUMIPMT(0, 30 * 12, 125000, 13, 24, 1)).toBe(ErrorCode.NUM);
            expect(CUMIPMT(0.09 / 12, 0, 125000, 13, 24, 1)).toBe(ErrorCode.NUM);
            expect(CUMIPMT(0.09 / 12, 30 * 12, 0, 13, 24, 1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("CUMPRINC", CUMPRINC => {
        it("should return correct results", () => {
            expect(CUMPRINC(0.09 / 12, 30 * 12, 125000, 13, 24, 0)).toBeCloseTo(-934.1071234, 5);
            expect(CUMPRINC(0.09 / 12, 30 * 12, 125000, 1, 1, 0)).toBeCloseTo(-68.27827118, 5);

            expect(CUMPRINC(0.09 / 12, 30 * 12, 125000, 13, 24, 1)).toBeCloseTo(-927.1534724, 5);
            expect(CUMPRINC(0.09 / 12, 30 * 12, 125000, 1, 1, 1)).toBeCloseTo(-998.291088, 5);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(CUMPRINC(0.09 / 12, 30 * 12, 125000, 13, 24, 2)).toBe(ErrorCode.NUM);
            expect(CUMPRINC(0.09 / 12, 30 * 12, 125000, 13, 24, -1)).toBe(ErrorCode.NUM);
            expect(CUMPRINC(0.09 / 12, 30 * 12, 125000, 24, 13, 1)).toBe(ErrorCode.NUM);
            expect(CUMPRINC(0.09 / 12, 24, 125000, 13, 360, 1)).toBe(ErrorCode.NUM);
            expect(CUMPRINC(0.09 / 12, 30 * 12, 125000, 0, 24, 1)).toBe(ErrorCode.NUM);

            expect(CUMPRINC(0, 30 * 12, 125000, 13, 24, 1)).toBe(ErrorCode.NUM);
            expect(CUMPRINC(0.09 / 12, 0, 125000, 13, 24, 1)).toBe(ErrorCode.NUM);
            expect(CUMPRINC(0.09 / 12, 30 * 12, 0, 13, 24, 1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("DB", DB => {
        it("should return correct results", () => {
            expect(DB(1000000, 100000, 6, 1, 7)).toBeCloseTo(186083.333333, 5);
            expect(DB(1000000, 100000, 6, 2, 7)).toBeCloseTo(259639.416666, 5);
            expect(DB(1000000, 100000, 6, 3, 7)).toBeCloseTo(176814.442750, 5);
            expect(DB(1000000, 100000, 6, 4, 7)).toBeCloseTo(120410.635512, 5);
            expect(DB(1000000, 100000, 6, 5, 7)).toBeCloseTo(81999.642784, 5);
            expect(DB(1000000, 100000, 6, 6, 7)).toBeCloseTo(55841.756736, 5);
            expect(DB(1000000, 100000, 6, 7, 7)).toBeCloseTo(15845.098473, 5);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(DB(-1, 100000, 6, 1, 7)).toBe(ErrorCode.NUM);
            expect(DB(1000000, -1, 6, 1, 7)).toBe(ErrorCode.NUM);
            expect(DB(1000000, 100000, -1, 1, 7)).toBe(ErrorCode.NUM);
            expect(DB(1000000, 100000, 6, -1, 7)).toBe(ErrorCode.NUM);
            expect(DB(1000000, 100000, 6, 1, 0)).toBe(ErrorCode.NUM);
            expect(DB(1000000, 100000, 6, 1, 13)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("DDB", DDB => {
        it("should return correct results", () => {
            expect(DDB(2400, 300, 3650, 1)).toBeCloseTo(1.315068493, 5);
            expect(DDB(2400, 300, 120, 1, 2)).toBe(40);
            expect(DDB(2400, 300, 10, 1, 2)).toBe(480);
            expect(DDB(2400, 300, 10, 2, 1.5)).toBeCloseTo(306, 5);
            expect(DDB(2400, 300, 10, 10)).toBeCloseTo(22.1225472, 5);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(DDB(-1, 300, 10, 0, 0.875)).toBe(ErrorCode.NUM);
            expect(DDB(2400, -1, 10, 0, 0.875)).toBe(ErrorCode.NUM);
            expect(DDB(2400, 300, -1, 0, 0.875)).toBe(ErrorCode.NUM);
            expect(DDB(2400, 300, 10, -1, 0.875)).toBe(ErrorCode.NUM);
            expect(DDB(2400, 300, 10, 0, -1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("DISC", DISC => {
        it("should return correct results", () => {
            expect(DISC(dt("2007-01-25"), dt("2007-06-15"), 97.975, 100, 1)).toBeCloseTo(0.052420213, 5);
            expect(DISC(dt("2007-01-25"), dt("2008-01-25"), 1000, 10, 0)).toBe(-99);
            expect(DISC(dt("2007-01-25"), dt("2008-01-25"), 1000, 10, 1)).toBe(-99);
            expect(DISC(dt("2007-01-25"), dt("2008-01-25"), 1000, 10, 3)).toBe(-99);
            expect(DISC(dt("2007-01-25"), dt("2008-01-25"), 1000, 10, 4)).toBe(-99);
            expect(DISC(dt("2007-01-25"), dt("2008-01-23"), 1000, 10, 2)).toBe(-(98 + 2 / 11));
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(DISC(dt("2007-06-15"), dt("2007-01-25"), 97.975, 100, 1)).toBe(ErrorCode.NUM);
            expect(DISC(dt("2007-01-25"), dt("2007-06-15"), 0, 100, 1)).toBe(ErrorCode.NUM);
            expect(DISC(dt("2007-01-25"), dt("2007-06-15"), 97.975, 0, 1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("DOLLARDE", DOLLARDE => {
        it("should return correct results", () => {
            expect(DOLLARDE(1.02, 16)).toBeAlmost(1.125);
            expect(DOLLARDE(1.1, 32)).toBeAlmost(1.3125);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(DOLLARDE(1.1, -1)).toBe(ErrorCode.NUM);
            expect(DOLLARDE(1.1, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("DOLLARFR", DOLLARFR => {
        it("should return correct results", () => {
            expect(DOLLARFR(1.125, 16)).toBeAlmost(1.02);
            expect(DOLLARFR(1.125, 32)).toBeAlmost(1.04);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(DOLLARFR(1.125, -1)).toBe(ErrorCode.NUM);
            expect(DOLLARFR(1.125, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("DURATION", DURATION => {
        it("should return correct results", () => {
            expect(DURATION(dt("2008-01-01"), dt("2016-01-01"), 0.08, 0.09, 2, 1)).toBeCloseTo(5.99195, 4);
            expect(DURATION(dt("2008-01-01"), dt("2016-01-01"), 0, 0.09, 2, 1)).toBeCloseTo(8, 1);
            expect(DURATION(dt("2008-01-01"), dt("2016-01-01"), 0.1, 0, 2, 1)).toBeCloseTo(6.3315, 4);

            expect(DURATION(dt("2008-01-01"), dt("2008-06-01"), 1, 0, 4, 0)).toBeCloseTo(0.375, 4);
            expect(DURATION(dt("2008-01-01"), dt("2008-06-01"), 1, 0, 4, 1)).toBeCloseTo(0.373168498, 2);
            expect(DURATION(dt("2008-01-01"), dt("2008-06-01"), 1, 0, 4, 2)).toBeCloseTo(0.38, 2);
            expect(DURATION(dt("2008-01-01"), dt("2008-06-01"), 1, 0, 4, 3)).toBeCloseTo(0.37477, 2);
            expect(DURATION(dt("2008-01-01"), dt("2008-06-01"), 1, 0, 4, 4)).toBeCloseTo(0.375, 4);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(DURATION(dt("2016-01-01"), dt("2008-01-01"), 0.08, 0.09, 2, 1)).toBe(ErrorCode.NUM);
            expect(DURATION(dt("2008-01-01"), dt("2016-01-01"), -0.1, 0.09, 2, 1)).toBe(ErrorCode.NUM);
            expect(DURATION(dt("2008-01-01"), dt("2016-01-01"), 0.08, -0.1, 2, 1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("EFFECT", EFFECT => {
        it("should return correct results", () => {
            expect(EFFECT(0.0525, 4)).toBeCloseTo(0.053542667, 4);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(EFFECT(0.0525, 0)).toBe(ErrorCode.NUM);
            expect(EFFECT(0, 4)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("FV", FV => {
        it("should return correct results", () => {
            expect(FV(0.06 / 12, 12, -100, -1000, 1)).toBeCloseTo(2301.40183, 5);
            expect(FV(0.06 / 12, 10, -200, -500, 1)).toBeCloseTo(2581.403374, 5);
            expect(FV(0.12 / 12, 12, -1000)).toBeCloseTo(12682.503013, 5);
            expect(FV(0.11 / 12, 35, -2000, null, 1)).toBeCloseTo(82846.246371, 5);
        });
    });

    moduleTester.testFunction("FVSCHEDULE", FVSCHEDULE => {
        it("should return correct results", () => {
            expect(FVSCHEDULE(1, mat([[0.09, 0.11, 0.1]]))).toBeCloseTo(1.33089, 9);
            expect(FVSCHEDULE(1, mat([[1, 2, 3]]))).toBe(24);
            expect(FVSCHEDULE(1, mat([[0, 1, 2, 3]]))).toBe(24);
            expect(FVSCHEDULE(1, mat([[0, 1, -2, 3]]))).toBe(-8);

            expect(FVSCHEDULE(1, "22")).toBe(23);
        });
        it("should throw #VALUE! error code for wrong input data", () => {
            expect(FVSCHEDULE(1, mat([[1, "hello", 3]]))).toBe(ErrorCode.VALUE);
            expect(FVSCHEDULE(1, mat([[1, true, 3]]))).toBe(ErrorCode.VALUE);
            expect(FVSCHEDULE(1, true)).toBe(ErrorCode.VALUE);
        });
    });

    moduleTester.testFunction("INTRATE", INTRATE => {
        it("should return correct results", () => {
            expect(INTRATE(dt("2008-02-15"), dt("2008-05-15"), 1000000, 1014420, 2)).toBeCloseTo(0.05768, 9);

            expect(INTRATE(dt("2008-02-15"), dt("2009-02-15"), 1000000, 2000000, 0)).toBeCloseTo(1, 9);
            expect(INTRATE(dt("2008-02-15"), dt("2009-02-15"), 1000000, 2000000, 1)).toBeCloseTo(1, 3);
            expect(INTRATE(dt("2008-02-15"), dt("2009-02-15"), 1000000, 2000000, 4)).toBeCloseTo(1, 9);

            expect(INTRATE(dt("2008-02-15"), dt("2009-02-14"), 1000000, 2000000, 3)).toBeCloseTo(1, 2);

            expect(INTRATE(dt("2008-04-06"), dt("2009-04-01"), 1000000, 2000000, 2)).toBeCloseTo(1, 9);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(INTRATE(dt("2008-02-15"), dt("2008-05-15"), 0, 1014420, 2)).toBe(ErrorCode.NUM);
            expect(INTRATE(dt("2008-02-15"), dt("2008-05-15"), 1000000, 0, 2)).toBe(ErrorCode.NUM);
            expect(INTRATE(dt("2008-02-15"), dt("2008-02-15"), 1000000, 1014420, 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("IPMT", IPMT => {
        it("should return correct results", () => {
            expect(IPMT(1 / 120, 1, 36, 8000)).toBeCloseTo(-200 / 3, 5);
            expect(IPMT(1 / 10, 3, 3, 8000)).toBeCloseTo(-292.4471299, 5);
            expect(IPMT(1 / 120, 1, 36, 8000, 500, 0)).toBeCloseTo(-200 / 3, 5);
            expect(IPMT(1 / 120, 1, 36, 8000, 500, 1)).toBeAlmost(0);
            expect(IPMT(1 / 120, 1, 36, 8000, 500, 666)).toBeAlmost(0);
        });
        it("should throw #NUM! error on wrong input data", () => {
            expect(IPMT(3, 0.1, 3, 8000)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("IRR", IRR => {
        it("should return correct results", () => {
            expect(IRR(mat([[-70000, 12000, 15000, 18000, 21000]]))).toBeCloseTo(-0.02124485, 5);
            expect(IRR(mat([[-70000, 12000, 15000, 18000, 21000, 26000]]))).toBeCloseTo(0.08663095, 5);
            expect(IRR(mat([[-70000, 12000, 15000]]), -0.1)).toBeCloseTo(-0.44350694, 5);
        });
        it("should throw #NUM! error on wrong input data", () => {
            expect(IRR(mat([[70000, 12000, 15000]]))).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("ISPMT", ISPMT => {
        it("should return correct results", () => {
            expect(ISPMT(1 / 120, 1, 36, 8000000)).toBeCloseTo(-64814.814814, 5);
            expect(ISPMT(0.1, 1, 3, 8000000)).toBeCloseTo(-533333.333333, 5);
        });
        it("should throw #NUM! error on wrong input data", () => {
            expect(ISPMT(1 / 120, 3, 1, 8000000)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("MDURATION", MDURATION => {
        it("should return correct results", () => {
            //MDURATION is almost the same as DURATION
            expect(MDURATION(dt("2008-01-01"), dt("2016-01-01"), 0.08, 0.09, 2, 1)).toBeCloseTo(5.733, 2);
        });
    });

    moduleTester.testFunction("MIRR", MIRR => {
        it("should return correct results", () => {
            expect(MIRR(mat([[-120000, 39000, 30000, 21000, 37000, 46000]]), 0.1, 0.12)).toBeCloseTo(0.12609413, 5);
            expect(MIRR(mat([[-120000, 39000, 30000, 21000]]), 0.1, 0.12)).toBeCloseTo(-0.048044655, 5);
            expect(MIRR(mat([[-120000, 39000, 30000, 21000, 37000, 46000]]), 0.1, 0.14)).toBeCloseTo(0.134759111, 5);
        });
        it("should throw #NUM! error on wrong input data", () => {
            expect(MIRR(mat([[-120000, 39000, 30000, 21000, 37000, 46000]]), 0.1, -1)).toBe(ErrorCode.DIV0);
        });
    });

    moduleTester.testFunction("NOMINAL", NOMINAL => {
        it("should return correct results", () => {
            expect(NOMINAL(1, 1)).toBeCloseTo(1, 5);
            expect(NOMINAL(0.053543, 4)).toBeCloseTo(0.05250032, 5);
        });
        it("should throw #NUM! error on wrong input data", () => {
            expect(NOMINAL(1, 0)).toBe(ErrorCode.NUM);
            expect(NOMINAL(0, 1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("NPER", NPER => {
        it("should return correct results", () => {
            expect(NPER(0.01, -100, -1000, 10000, 1)).toBeCloseTo(59.67386567, 5);
            expect(NPER(0.01, -100, -1000, 10000)).toBeCloseTo(60.08212285, 5);
            expect(NPER(0.01, -100, -1000)).toBeCloseTo(-9.57859404, 5);
        });
        it("should throw #NUM! error on wrong input data", () => {
            expect(NPER(-0.01, -100, -1000, 10000, 1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("NPV", NPV => {
        it("should return correct results", () => {
            expect(NPV(1, 1, 6)).toBe(2);
            expect(NPV(0.08, mat([[8000, 9200, 10000, 12000, 14500]]))).toBeCloseTo(41922.061555, 5);
            expect(NPV(0.08, mat([[8000, 9200, 10000, 12000, 14500, -9000]]))).toBeCloseTo(36250.534913, 5);
            expect(NPV(0.1, mat([[-10000, 3000, 4200, 6800]]))).toBeCloseTo(1188.443412, 5);
        });
        it("should throw #DIV/0! error on wrong input data", () => {
            expect(NPV(-1, mat([[-10000, 3000, 4200, 6800]]))).toBe(ErrorCode.DIV0);
        });
    });

    moduleTester.testFunction("ODDLYIELD", ODDLYIELD => {
        it("should return correct results", () => {
            expect(ODDLYIELD(dt("2008-04-20"), dt("2008-06-15"), dt("2007-12-24"), 0.0375, 99.875, 100, 2, 0)).toBeCloseTo(0.0452, 3);
        });
        it("with whole years should return correct results", () => {
            expect(ODDLYIELD(dt("2008-01-01"), dt("2009-01-01"), dt("2007-01-01"), 1, 100, 100, 1)).toBe(0.5);
            expect(ODDLYIELD(dt("2008-01-01"), dt("2009-01-01"), dt("2007-01-01"), 1, 100, 100, 1, 0)).toBe(0.5);
            expect(ODDLYIELD(dt("2008-01-01"), dt("2009-01-01"), dt("2007-01-01"), 1, 100, 100, 1, 1)).toBeCloseTo(0.5, 2);
            expect(ODDLYIELD(dt("2008-01-01"), dt("2009-01-01"), dt("2007-01-01"), 1, 100, 100, 1, 2)).toBeCloseTo(0.5, 1);
            expect(ODDLYIELD(dt("2008-01-01"), dt("2009-01-01"), dt("2007-01-01"), 1, 100, 100, 1, 3)).toBe(0.5);
            expect(ODDLYIELD(dt("2008-01-01"), dt("2009-01-01"), dt("2007-01-01"), 1, 100, 100, 1, 4)).toBe(0.5);
        });
        it("with whole leapyears should return correct results", () => {
            expect(ODDLYIELD(dt("2011-03-01"), dt("2012-02-29"), dt("2010-03-01"), 1, 100, 100, 1, 1)).toBeCloseTo(0.5, 2);
            expect(ODDLYIELD(dt("2011-03-01"), dt("2012-02-29"), dt("2010-03-01"), 1, 100, 100, 1, 2)).toBeCloseTo(0.5, 1);
            expect(ODDLYIELD(dt("2011-03-01"), dt("2012-02-29"), dt("2010-03-01"), 1, 100, 100, 1, 3)).toBe(0.5);
            expect(ODDLYIELD(dt("2011-03-01"), dt("2012-02-29"), dt("2010-03-01"), 1, 100, 100, 1, 4)).toBe(0.5);

            expect(ODDLYIELD(dt("2011-03-01"), dt("2012-02-29"), dt("2010-03-01"), 1, 100, 100, 1)).toBeCloseTo(366 / (365 * 2), 1);
            expect(ODDLYIELD(dt("2011-03-01"), dt("2012-02-29"), dt("2010-03-01"), 1, 100, 100, 1, 0)).toBeCloseTo(366 / (365 * 2), 1);
        });
        it("should throw correct errors", () => {
            expect(ODDLYIELD(dt("2008-06-15"), dt("2008-04-20"), dt("2007-12-24"), 5.62, 100, 100, 2)).toBe(ErrorCode.NUM);
            expect(ODDLYIELD(dt("2008-04-20"), dt("2007-12-24"), dt("2008-06-15"), 5.62, 100, 100, 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("PDURATION", PDURATION => {
        it("should return correct results", () => {
            expect(PDURATION(0.025, 2000, 2200)).toBeCloseTo(3.859866163, 5);
            expect(PDURATION(0.025 / 12, 1000, 1200)).toBeCloseTo(87.60547642, 5);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(PDURATION(0, 2000, 2200)).toBe(ErrorCode.NUM);
            expect(PDURATION(0.025, 0, 2200)).toBe(ErrorCode.NUM);
            expect(PDURATION(0.025, 2000, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("PMT", PMT => {
        it("should return correct results", () => {
            expect(PMT(0.08 / 12, 10, 10000)).toBeCloseTo(-1037.032089, 3);
            expect(PMT(0.08 / 12, 10, 10000, 0, 1)).toBeCloseTo(-1030.164327, 3);
            expect(PMT(0.06 / 12, 18 * 12, 0, 50000)).toBeCloseTo(-129.0811609, 3);
            expect(PMT(0.06 / 12, 18 * 12, 0, 50000, 1)).toBeCloseTo(-128.438966, 3);
        });
    });

    moduleTester.testFunction("PPMT", PPMT => {
        it("should return correct results", () => {
            expect(PPMT(0.08, 10, 10, 200000)).toBeCloseTo(-27598.05346, 3);
            expect(PPMT(0.1 / 12, 1, 2 * 12, 2000)).toBeCloseTo(-75.62318601, 3);
        });
    });

    moduleTester.testFunction("PRICE", PRICE => {
        it("should return correct results", () => {
            expect(PRICE(dt("2008-02-15"), dt("2017-05-15"), 0.065, 0.065, 100, 2, 0)).toBeCloseTo(100, 0);
            expect(PRICE(dt("2008-02-15"), dt("2017-05-15"), 0.1, 0, 100, 2, 0)).toBeCloseTo(192.5, 1);
            expect(PRICE(dt("2007-02-15"), dt("2008-02-15"), 0, 1, 100, 1, 0)).toBeCloseTo(50, 1);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(PRICE(dt("2017-05-15"), dt("2008-02-15"), 0.0575, 0.065, 100, 2, 0)).toBe(ErrorCode.NUM);
            expect(PRICE(dt("2008-02-15"), dt("2017-05-15"), 0.0575, 0.065, 0, 2, 0)).toBe(ErrorCode.NUM);
            expect(PRICE(dt("2008-02-15"), dt("2017-05-15"), -1, 0.065, 100, 2, 0)).toBe(ErrorCode.NUM);
            expect(PRICE(dt("2008-02-15"), dt("2017-05-15"), 0.0575, -1, 100, 2, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("PRICEDISC", PRICEDISC => {
        it("should return correct results", () => {
            expect(PRICEDISC(dt("2008-03-01"), dt("2009-03-01"), 0.1, 99, 0)).toBeCloseTo(89.1, 4);
            expect(PRICEDISC(dt("2008-03-01"), dt("2009-03-01"), 0.1, 99, 2)).toBeCloseTo(88.9625, 4);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(PRICEDISC(dt("2009-03-01"), dt("2008-03-01"), 0.1, 99)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("PRICEMAT", PRICEMAT => {
        it("should return correct results", () => {
            expect(PRICEMAT(dt("2009-02-15"), dt("2010-02-15"), dt("2007-11-11"), 0, 0, 0)).toBeCloseTo(100, 4);
            expect(PRICEMAT(dt("2009-02-15"), dt("2010-02-15"), dt("2007-11-11"), 0.2, 0, 0)).toBeCloseTo(120, 4);
            expect(PRICEMAT(dt("2009-02-15"), dt("2010-02-15"), dt("2007-11-11"), 0, 1, 0)).toBeCloseTo(50, 4);
        });
    });

    moduleTester.testFunction("PV", PV => {
        it("should return correct results", () => {
            expect(PV(0.08 / 12, 12 * 20, 500, undefined, 0)).toBeCloseTo(-59777.14585, 4);
            expect(PV(0.08 / 12, 12 * 20, 500, undefined, 1)).toBeCloseTo(-60175.66016, 4);
        });
    });

    moduleTester.testFunction("RATE", RATE => {
        it("should return correct results", () => {
            expect(RATE(4 * 12, -200, 8000)).toBeCloseTo(0.007701472, 5);
            expect(RATE(0, -200, 8000)).toBe(ErrorCode.NUM);
            expect(RATE(4 * 12, -200, 0)).toBe(ErrorCode.NUM);
            expect(RATE(4 * 12, -200, 0, -500, 1, -1)).toBe(ErrorCode.NUM);
        });
        it("should have fixed behavior", () => {
            //important because of a bug in open office!
            expect(RATE(4 * 12, -200, 0, 0.0001, -500)).toBeCloseTo(-1, 5);
            expect(RATE(4 * 12, -200, 0, 0.00002, -500)).toBeCloseTo(-1, 5);
            expect(RATE(4 * 12, -200, 0, 0.00001, -500)).toBe(ErrorCode.NUM);

            expect(RATE(1, -200, 8000)).toBeCloseTo(-0.975, 5);
            expect(RATE(1.5, -200, 8000)).toBeCloseTo(-0.91062293265641, 5);
            expect(RATE(0.5, -200, 8000)).toBe(ErrorCode.NUM);
        });
        it("should return correct results with guess is zero", () => {
            expect(RATE(4, -200, 8000, -500, 1, 0)).toBeCloseTo(-0.452187382445176, 9);
        });
        it("should return correct results with guess is null", () => {
            expect(RATE(4, -200, 8000, -500, 1, null)).toBeCloseTo(-0.452187382445176, 9);
        });
        it("should return correct results with guess is assigned with numbers", () => {
            expect(RATE(4, -200, 8000, -500, 1, 50)).toBeCloseTo(-0.452187382445177, 9);
            expect(RATE(4, -200, 8000, -500, 1, 5)).toBeCloseTo(-0.452187382445171, 9);
            expect(RATE(4, -200, 8000, -500, 1, 1)).toBeCloseTo(-0.452187382441825, 9);
        });
        it("should return correct results with guess is not assigned", () => {
            expect(RATE(4, -200, 8000, -500, 1)).toBeCloseTo(-0.452187382443900, 9);
        });
        it("should return correct results with guess is default (0.1)", () => {
            expect(RATE(4, -200, 8000, -500, 1, 0.1)).toBeCloseTo(-0.452187382443900, 9);
        });
    });

    moduleTester.testFunction("RECEIVED", RECEIVED => {
        it("should return correct results", () => {
            expect(RECEIVED(dt("2008-02-15"), dt("2008-05-15"), 1000000, 0.0575)).toBeCloseTo(1014584.6543, 2);

            expect(RECEIVED(dt("2009-02-15"), dt("2010-02-15"), 900000, 0.1, 0)).toBeAlmost(1000000);
            expect(RECEIVED(dt("2009-02-15"), dt("2010-02-15"), 900000, 0.1, 1)).toBeAlmost(1000000);
            expect(RECEIVED(dt("2009-02-15"), dt("2010-02-15"), 900000, 0.1, 4)).toBeAlmost(1000000);

            expect(RECEIVED(dt("2009-02-15"), dt("2010-02-10"), 900000, 0.1, 2)).toBeAlmost(1000000);
            expect(RECEIVED(dt("2009-02-15"), dt("2010-02-15"), 900000, 0.1, 3)).toBeAlmost(1000000);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(RECEIVED(dt("2010-05-15"), dt("2009-02-15"), 900000, 0.1)).toBe(ErrorCode.NUM);
            expect(RECEIVED(dt("2009-02-15"), dt("2010-05-15"), 900000, 0)).toBe(ErrorCode.NUM);
            expect(RECEIVED(dt("2009-02-15"), dt("2010-05-15"), 0, 0.1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("RRI", RRI => {
        it("should return correct results", () => {
            expect(RRI(96, 10000, 11000)).toBeCloseTo(0.00099330737, 9);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(RRI(0, 10000, 11000)).toBe(ErrorCode.NUM);
            expect(RRI(96, 0, 11000)).toBe(ErrorCode.NUM);
            expect(RRI(96, 10000, -1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("SLN", SLN => {
        it("should return correct results", () => {
            expect(SLN(30000, 7500, 10)).toBeCloseTo(2250, 9);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(SLN(30000, -1, 10)).toBe(ErrorCode.NUM);
            expect(SLN(30000, 7500, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("SYD", SYD => {
        it("should return correct results", () => {
            expect(SYD(30000, 7500, 10, 1)).toBeCloseTo(4090.9091, 2);
            expect(SYD(30000, 7500, 10, 10)).toBeCloseTo(409.09091, 2);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(SYD(30000, 7500, 0, 10)).toBe(ErrorCode.NUM);
            expect(SYD(30000, 7500, 10, 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("TBILLEQ", TBILLEQ => {
        it("should return correct results", () => {
            expect(TBILLEQ(dt("2008-03-31"), dt("2008-06-01"), 0.09)).toBeCloseTo(0.092686643, 7);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(TBILLEQ(dt("2009-04-01"), dt("2008-03-31"), 0.09)).toBe(ErrorCode.NUM);
            expect(TBILLEQ(dt("2008-03-31"), dt("2008-06-01"), 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("TBILLPRICE", TBILLPRICE => {
        it("should return correct results", () => {
            expect(TBILLPRICE(dt("2008-03-31"), dt("2008-06-01"), 0.09)).toBeAlmost(98.45);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(TBILLPRICE(dt("2009-04-01"), dt("2008-03-31"), 0.09)).toBe(ErrorCode.NUM);
            expect(TBILLPRICE(dt("2008-03-31"), dt("2008-06-01"), 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("TBILLYIELD", TBILLYIELD => {
        it("should return correct results", () => {
            expect(TBILLYIELD(dt("2008-03-31"), dt("2008-06-01"), 98.45)).toBeAlmost(0.09141696292534233);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(TBILLYIELD(dt("2008-03-31"), dt("2009-04-01"), 98.45)).toBe(ErrorCode.NUM);
            expect(TBILLYIELD(dt("2008-03-31"), dt("2008-06-01"), 0)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("VDB", VDB => {
        it("should return correct results", () => {
            expect(VDB(2400, 300, 3650, 0, 1)).toBeCloseTo(1.315068493, 5);
            expect(VDB(2400, 300, 120, 0, 1)).toBe(40);
            expect(VDB(2400, 300, 10, 0, 1)).toBe(480);
            expect(VDB(2400, 300, 120, 6, 18)).toBeCloseTo(396.3060533, 5);
            expect(VDB(2400, 300, 120, 6, 18, 1.5)).toBeCloseTo(311.8089367, 5);
            expect(VDB(2400, 300, 10, 0, 0.875, 1.5)).toBe(315);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(VDB(-1, 300, 10, 0, 0.875, 1.5)).toBe(ErrorCode.NUM);
            expect(VDB(2400, -1, 10, 0, 0.875, 1.5)).toBe(ErrorCode.NUM);
            expect(VDB(2400, 300, -1, 0, 0.875, 1.5)).toBe(ErrorCode.NUM);
            expect(VDB(2400, 300, 10, -1, 0.875, 1.5)).toBe(ErrorCode.NUM);
            expect(VDB(2400, 300, 10, 0, -1, 1.5)).toBe(ErrorCode.NUM);
            expect(VDB(2400, 300, 10, 0, 0.875, -1)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("XIRR", XIRR => {
        it("should return correct results", () => {
            expect(XIRR(mat([[-10000, 2750, 4250, 3250, 2750]]), dtmat([["2008-01-01", "2008-03-01", "2008-10-30", "2009-02-15", "2009-04-01"]]))).toBeCloseTo(0.373362535, 5);
        });
        it("should throw #NUM! error on wrong input data", () => {
            expect(XIRR(mat([[-10000, 2750, 4250, 3250]]), dtmat([["2008-01-01", "2008-03-01", "2008-10-30", "2009-02-15", "2009-04-01"]]))).toBe(ErrorCode.NUM);
            expect(XIRR(mat([[-10000, 2750, 4250, 3250, 2750]]), dtmat([["2008-01-01", "2008-03-01", "2008-10-30", "2009-02-15", "2009-04-01"]]), -2)).toBe(ErrorCode.NUM);
            expect(XIRR(mat([[-10000]]), dtmat([["2008-01-01"]]))).toBe(ErrorCode.NUM);
            //TODO: test floats!!!
        });
    });

    moduleTester.testFunction("XNPV", XNPV => {
        it("should return correct results", () => {
            expect(XNPV(0.09, mat([[-10000, 2750, 4250, 3250, 2750]]), dtmat([["2008-01-01", "2008-03-01", "2008-10-30", "2009-02-15", "2009-04-01"]]))).toBeCloseTo(2086.647602, 5);
        });
        it("should throw #NUM! error on wrong input data", () => {
            expect(XNPV(0.09, mat([[-10000, 2750, 4250, 3250]]), dtmat([["2008-01-01", "2008-03-01", "2008-10-30", "2009-02-15", "2009-04-01"]]))).toBe(ErrorCode.NUM);
            expect(XNPV(0.09, mat([[-10000]]), dtmat([["2008-01-01"]]))).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("YIELD", YIELD => {
        it("should return correct results", () => {
            expect(YIELD(dt("2008-02-15"), dt("2016-11-15"), 0.0575, 95.04287, 100, 2, 0)).toBeAlmost(0.06500000688075479);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(YIELD(dt("2008-02-16"), dt("2008-03-01"), -1, 100, 2)).toBe(ErrorCode.NUM);
            expect(YIELD(dt("2008-02-16"), dt("2008-03-01"), 99.795, -1, 2)).toBe(ErrorCode.NUM);
            expect(YIELD(dt("2008-03-01"), dt("2008-02-16"), 99.795, 100, 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("YIELDDISC", YIELDDISC => {
        it("should return correct results", () => {
            expect(YIELDDISC(dt("2008-02-16"), dt("2008-03-01"), 99.795, 100, 2)).toBeAlmost(0.052822571986860085);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(YIELDDISC(dt("2008-02-16"), dt("2008-03-01"), -1, 100, 2)).toBe(ErrorCode.NUM);
            expect(YIELDDISC(dt("2008-02-16"), dt("2008-03-01"), 99.795, -1, 2)).toBe(ErrorCode.NUM);
            expect(YIELDDISC(dt("2008-03-01"), dt("2008-02-16"), 99.795, 100, 2)).toBe(ErrorCode.NUM);
        });
    });

    moduleTester.testFunction("YIELDMAT", YIELDMAT => {
        it("should return correct results", () => {
            expect(YIELDMAT(dt("2008-03-15"), dt("2008-11-03"), dt("2007-11-08"), 0.0625, 100.0123, 0)).toBeAlmost(0.060954333691538576);
        });
        it("should throw #NUM! error code for wrong input data", () => {
            expect(YIELDMAT(dt("2008-03-15"), dt("2008-11-03"), dt("2007-11-08"), -1, 100.0123, 0)).toBe(ErrorCode.NUM);
            expect(YIELDMAT(dt("2008-03-15"), dt("2008-11-03"), dt("2007-11-08"), 0.0625, 0, 0)).toBe(ErrorCode.NUM);
            expect(YIELDMAT(dt("2008-11-03"), dt("2008-03-15"), dt("2007-11-08"), 0.0625, 100.0123, 0)).toBe(ErrorCode.NUM);
        });
    });
});
