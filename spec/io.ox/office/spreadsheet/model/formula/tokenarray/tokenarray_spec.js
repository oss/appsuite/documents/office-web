/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { BaseTokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray/basetokenarray";
import { TokenArray } from "@/io.ox/office/spreadsheet/model/formula/tokenarray/tokenarray";

import { a, r, ErrorCode, createSpreadsheetApp } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/tokenarray/tokenarray", () => {

    // the operations to be applied by the document model
    const OPERATIONS = [
        { name: "setDocumentAttributes", attrs: { document: { cols: 16384, rows: 1048576 } } },
        { name: "insertAutoStyle", styleId: "a0", attrs: {}, default: true },
        { name: "insertName", label: "ColumnA", formula: "Sheet1!$A:$A" },
        { name: "insertSheet", sheet: 0, sheetName: "Sheet1" },
        { name: "changeCells", sheet: 0, start: "B2", contents: [[1, 2], [4, 8]] },
        { name: "changeCells", sheet: 0, start: "E2", contents: [[true, false], [false, true]] },
        { name: "changeCells", sheet: 0, start: "H2", contents: [[1, 2], [2, 1]] },
        { name: "changeCells", sheet: 0, start: "A10", contents: [["a", "b", "c", "a", "b", "b"]] }
    ];

    // initialize test document
    let docModel = null, tokenArray = null;
    createSpreadsheetApp("ooxml", OPERATIONS).done(function (app) {
        docModel = app.getModel();
        tokenArray = new TokenArray(docModel.getSheetModel(0));
    });

    function expectScalarResult(formula, ref, exp) {
        ref = a(ref);
        tokenArray.parseFormula("op", formula, ref, { refSheet: 0 });
        const result = tokenArray.interpretFormula("val", ref, ref, { refSheet: 0 });
        expect(result).toHaveProperty("type", "valid");
        expect(result).toHaveProperty("value", exp);
    }

    function expectMatrixResult(formula, range, exp) {
        range = r(range);
        tokenArray.parseFormula("op", formula, range.a1, { refSheet: 0 });
        const result = tokenArray.interpretFormula("mat", range.a1, range.a1, { refSheet: 0, matrixRange: range });
        expect(result).toHaveProperty("type", "valid");
        expect(result.value).toStringifyTo(exp);
    }

    function expectWarning(formula, ref, code, value) {
        ref = a(ref);
        tokenArray.parseFormula("op", formula, ref, { refSheet: 0 });
        const result = tokenArray.interpretFormula("val", ref, ref, { refSheet: 0 });
        expect(result).toEqual({ type: "warn", code, value });
    }

    // class TokenArray -------------------------------------------------------

    describe("class TokenArray", () => {

        it("should exist", () => {
            expect(TokenArray).toBeSubClassOf(BaseTokenArray);
        });

        describe("method interpretFormula", () => {
            it("should exist", () => {
                expect(TokenArray).toHaveMethod("interpretFormula");
            });
            it("should calculate simple scalar formulas", () => {
                expectScalarResult("B2", "A1", 1);
                expectScalarResult("B2:C3", "A1", ErrorCode.VALUE);
                expectScalarResult("B2:C3", "B1", ErrorCode.VALUE);
                expectScalarResult("B3:C3", "B1", 4);
                expectScalarResult("B2:C3", "A2", ErrorCode.VALUE);
                expectScalarResult("C2:C3", "A2", 2);
                expectScalarResult("SUM(B2:C3)", "A1", 15);
                expectScalarResult("SUM(B2:C3+1)", "A1", ErrorCode.VALUE);
                expectScalarResult("SUM({1,2;4,8})", "A1", 15);
                expectScalarResult("SUM({1,2;4,8}+1)", "A1", 19);
                expectScalarResult("SUM(B:C)", "A1", 15);
                expectScalarResult("LARGE(B2:C3,{1;3})", "A1", 8);
                expectScalarResult("SUM(LARGE(B2:C3,{1;3}))", "A1", 8);
                expectScalarResult("SUMPRODUCT(B2:C3*B2:C3)", "A1", 85); // bug 47280: forced matrix context in cell formulas
            });
            it("should calculate simple matrix formulas", () => {
                expectMatrixResult("B2:C3", "D2:E3", "{1;2|4;8}");
                expectMatrixResult("B2:C3*2", "D2:E3", "{2;4|8;16}");
                expectMatrixResult("LOG(B2:C3,2)", "D2:E3", "{0;1|2;3}");
                expectMatrixResult("LOG(B2:C3,2)*2", "D2:E3", "{0;2|4;6}");
                expectMatrixResult("LOG(B2:C3*2,2)", "D2:E3", "{1;2|3;4}");
                expectMatrixResult("SUM(B2:C3)", "A1:A1", "{15}");
                expectMatrixResult("SUM(B2:C3+1)", "A1:A1", "{19}");
                expectMatrixResult("SUM((B2:C3>2)+0)", "A1:A1", "{2}");
                expectMatrixResult("SUM(B:C)", "A1:A1", "{15}");
                expectMatrixResult("LARGE(B2:C3,{1,3})", "A1:B1", "{8;2}");
                expectMatrixResult("SUM(LARGE(B2:C3,{1;3}))", "A1:A1", "{10}");
                expectMatrixResult("SUMPRODUCT(B2:C3*B2:C3)", "A1:A1", "{85}");
                expectMatrixResult("MMULT({1,2;3,4},B2:C3)", "D2:E3", "{9;18|19;38}");
                expectMatrixResult("MMULT({1,2;3,4}*2,B2:C3*2)", "D2:E3", "{36;72|76;152}");
                // bug 49476: forward error codes as matrix elements without failing entire formula
                expectMatrixResult("IFERROR(ABS({1,-2;3,#NULL!}),4)", "D2:E3", "{1;2|3;4}");
            });

            // IF function

            it("should calculate scalar formulas with IF function", () => {
                expectScalarResult("IF(E2:F3,B2:C3,10)", "A1", ErrorCode.VALUE);
                expectScalarResult("SUM(IF(E2:F3,B2:C3,10)", "A1", ErrorCode.VALUE);
                expectScalarResult("IF(NOT(E2:F3),B2:C3+1,LOG(B2:C3,2))", "A1", ErrorCode.VALUE);
                expectScalarResult("SUM(IF(NOT(E2:F3),B2:C3+1,LOG(B2:C3,2)))", "A1", ErrorCode.VALUE);
                expectScalarResult("ISREF(IF(1,B2:C3))", "A1", true);
                expectScalarResult("ISREF(IF({1},B2:C3))", "A1", false);
                expectScalarResult("IF({1,0;0,1},10,B2:C3)", "A1", 10);
                expectScalarResult("IF({1,0;0,1},B2:C3,10)", "A1", ErrorCode.VALUE);
                expectScalarResult("SUM(IF({1,0;0,1},10,B2:C3))", "A1", 26);
            });
            it("should calculate matrix formulas with IF function", () => {
                expectMatrixResult("IF(E2:F3,B2:C3,10)", "A5:B6", "{1;10|10;8}");
                expectMatrixResult("SUM(IF(E2:F3,B2:C3,10)", "A1:A1", "{29}");
                expectMatrixResult("IF(NOT(E2:F3),LOG(B2:C3,2),B2:C3+1)", "A5:B6", "{2;1|2;9}");
                expectMatrixResult("SUM(IF(NOT(E2:F3),LOG(B2:C3,2),B2:C3+1))", "A1:A1", "{14}");
                expectMatrixResult("ISREF(IF(1,B2:C3))", "A1:A1", "{TRUE}");
                expectMatrixResult("ISREF(IF({1},B2:C3))", "A1:A1", "{FALSE}");
                expectMatrixResult("IF({1,0;0,1},10,B2:C3)", "A5:B6", "{10;2|4;10}");
                expectMatrixResult("IF({1,0;0,1},B2:C3,10)", "A5:B6", "{1;10|10;8}");
                expectMatrixResult("SUM(IF({1,0;0,1},10,B2:C3))", "A1:A1", "{26}");
            });

            // CHOOSE function

            it("should calculate scalar formulas with CHOOSE function", () => {
                expectScalarResult("CHOOSE(H2:I3,B2:C3,10)", "A1", ErrorCode.VALUE);
                expectScalarResult("SUM(CHOOSE(H2:I3,B2:C3,10))", "A1", ErrorCode.VALUE);
                expectScalarResult("CHOOSE(3-H2:I3,LOG(B2:C3,2),B2:C3+1)", "A1", ErrorCode.VALUE);
                expectScalarResult("SUM(CHOOSE(3-H2:I3,LOG(B2:C3,2),B2:C3+1))", "A1", ErrorCode.VALUE);
                expectScalarResult("ISREF(CHOOSE(1,B2:C3))", "A1", true);
                expectScalarResult("ISREF(CHOOSE({1},B2:C3))", "A1", false);
                expectScalarResult("CHOOSE({1,2;2,1},10,B2:C3)", "A1", 10);
                expectScalarResult("CHOOSE({1,2;2,1},B2:C3,10)", "A1", ErrorCode.VALUE);
                expectScalarResult("SUM(CHOOSE({1,2;2,1},10,B2:C3))", "A1", 26);
            });
            it("should calculate matrix formulas with CHOOSE function", () => {
                expectMatrixResult("CHOOSE(H2:I3,B2:C3,10)", "A5:B6", "{1;10|10;8}");
                expectMatrixResult("SUM(CHOOSE(H2:I3,B2:C3,10))", "A1:A1", "{29}");
                expectMatrixResult("CHOOSE(3-H2:I3,LOG(B2:C3,2),B2:C3+1)", "A5:B6", "{2;1|2;9}");
                expectMatrixResult("SUM(CHOOSE(3-H2:I3,LOG(B2:C3,2),B2:C3+1))", "A1:A1", "{14}");
                expectMatrixResult("ISREF(CHOOSE(1,B2:C3))", "A1:A1", "{TRUE}");
                expectMatrixResult("ISREF(CHOOSE({1},B2:C3))", "A1:A1", "{FALSE}");
                expectMatrixResult("CHOOSE({1,2;2,1},10,B2:C3)", "A5:B6", "{10;2|4;10}");
                expectMatrixResult("CHOOSE({1,2;2,1},B2:C3,10)", "A5:B6", "{1;10|10;8}");
                expectMatrixResult("SUM(CHOOSE({1,2;2,1},10,B2:C3))", "A1:A1", "{26}");
            });

            // COLUMN/ROW functions

            it("should calculate scalar formulas with COLUMN/ROW functions", () => {
                expectScalarResult("SUM(COLUMN()+ROW()+B2:C3", "A1", ErrorCode.VALUE);
                expectScalarResult("SUM(B2:C3+COLUMN()+ROW()", "A1", ErrorCode.VALUE);
                expectScalarResult("SUM(COLUMN(B5:C6),ROW(B5:C6))", "A1", 7);
                expectScalarResult("SUM(COLUMN(B5:C6)+ROW(B5:C6))", "A1", 7);
                expectScalarResult("SUM(COLUMN(B5:C6),ROW(B5:C6),B2:C3)", "A1", 22);
                expectScalarResult("SUM(COLUMN(B5:C6)+ROW(B5:C6)+B2:C3)", "A1", ErrorCode.VALUE);
            });
            it("should calculate matrix formulas with COLUMN/ROW functions", () => {
                expectMatrixResult("COLUMN()&ROW()&B2", "D2:E3", '{"421";"521"|"431";"531"}');
                expectMatrixResult("COLUMN()&ROW()&B2:C3", "D2:E3", '{"421";"522"|"434";"538"}');
                expectMatrixResult("COLUMN(B5:C6)&ROW(B5:C6)&B2:C3", "D2:E3", '{"251";"352"|"264";"368"}');
                expectMatrixResult("SUM(COLUMN()+ROW()+B2:C3)", "A1:A1", "{23}");
                expectMatrixResult("SUM(B2:C3+COLUMN()+ROW()", "A1:A1", "{23}");
                // expectMatrixResult("SUM(COLUMN(B5:C6),ROW(B5:C6))", "A1:A1", "{32}");
                // expectMatrixResult("SUM(COLUMN(B5:C6)+ROW(B5:C6))", "A1:A1", "{32}");
                // expectMatrixResult("SUM(COLUMN(B5:C6),ROW(B5:C6),B2:C3)", "A1:A1", "{42}");
                // expectMatrixResult("SUM(COLUMN(B5:C6)+ROW(B5:C6)+B2:C3)", "A1:A1", "{42}");
            });

            // COUNTIF function

            it("should calculate scalar formulas with COUNTIF function", () => {
                expectScalarResult("COUNTIF(A10:F10,A10:F10)", "A1", 2);
                expectScalarResult("MAX(COUNTIF(A10:F10,A10:F10))", "A1", 2);
            });
            it("should calculate matrix formulas with COUNTIF function", () => {
                expectMatrixResult("COUNTIF(A10:F10,A10:F10)", "A1:F1", "{2;3;1;2;3;3}");
                expectMatrixResult("MAX(COUNTIF(A10:F10,A10:F10))", "A1:A1", "{3}");
            });

            // bug 55707: INDEX/IF functions

            it("should calculate INDEX function in IF function as matrix", () => {
                expectMatrixResult('IF(E2:F3,INDEX({1,2,3,4,5,6,7,8},1,B2:C3),"")', "A5:B6", '{1;""|"";8}');
            });

            // SUM function with defined name

            it("should calculate scalar formulas with SUM function and defined name", () => {
                expectScalarResult("SUM(A:A)", "B1", 0);
                expectScalarResult("SUM(ColumnA)", "B1", 0);
            });
            it("should calculate matrix formulas with SUM function and defined name", () => {
                expectMatrixResult("SUM(A:A)", "B1:B1", "{0}");
                expectMatrixResult("SUM(ColumnA)", "B1:B1", "{0}");
            });

            // unsupported function

            it("should return #NA for unsupported functions", () => {
                expectWarning("ODDFPRICE(1,1,1,1,1,1,1,1)", "A1", "unsupported", ErrorCode.NA);
            });
        });
    });
});
