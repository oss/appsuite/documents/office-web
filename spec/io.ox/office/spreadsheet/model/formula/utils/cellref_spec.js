/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Address } from "@/io.ox/office/spreadsheet/utils/address";
import { AddressFactory } from "@/io.ox/office/spreadsheet/model/addressfactory";
import { CellRef } from "@/io.ox/office/spreadsheet/model/formula/utils/cellref";

import { a, cref } from "~/spreadsheet/sheethelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/utils/cellref", () => {

    // dummy address factory for a document with 10x20 cells per sheet
    const addressFactory = new AddressFactory(10, 20);

    // class CellRef ----------------------------------------------------------

    describe("class CellRef", () => {

        it("should exist", () => {
            expect(CellRef).toBeFunction();
        });

        describe("constructor", () => {
            it("should create a cell reference", () => {
                const r1 = new CellRef(1, 2, false, true);
                expect(r1).toHaveProperty("col", 1);
                expect(r1).toHaveProperty("row", 2);
                expect(r1).toHaveProperty("absCol", false);
                expect(r1).toHaveProperty("absRow", true);
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("toString");
            });
            it("should return an internal string representation", () => {
                expect(new CellRef(1, 2, false, false).toString()).toBe("B3");
                expect(new CellRef(1, 2, false, true).toString()).toBe("B$3");
                expect(new CellRef(1, 2, true, false).toString()).toBe("$B3");
                expect(new CellRef(1, 2, true, true).toString()).toBe("$B$3");
                // helper self test
                expect(cref("B3")).toStringifyTo("B3");
                expect(cref("B$3")).toStringifyTo("B$3");
                expect(cref("$B3")).toStringifyTo("$B3");
                expect(cref("$B$3")).toStringifyTo("$B$3");
            });
        });

        describe("static function create", () => {
            it("should exist", () => {
                expect(CellRef).toHaveStaticMethod("create");
            });
            it("should create a cell reference", () => {
                const r1 = CellRef.create(a("B3"), false, true);
                expect(r1).toBeInstanceOf(CellRef);
                expect(r1).toHaveProperty("col", 1);
                expect(r1).toHaveProperty("row", 2);
                expect(r1).toHaveProperty("absCol", false);
                expect(r1).toHaveProperty("absRow", true);
            });
        });

        describe("static function equal", () => {
            it("should exist", () => {
                expect(CellRef).toHaveStaticMethod("equal");
            });
            it("should compare cell references", () => {
                const r1 = cref("B$3");
                expect(CellRef.equal(r1, r1)).toBeTrue();
                expect(CellRef.equal(r1, cref("B$3"))).toBeTrue();
                expect(CellRef.equal(r1, cref("C$3"))).toBeFalse();
                expect(CellRef.equal(r1, cref("$B$3"))).toBeFalse();
                expect(CellRef.equal(r1, cref("B3"))).toBeFalse();
                expect(CellRef.equal(r1, cref("B$2"))).toBeFalse();
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("clone");
            });
            it("should create a clone", () => {
                const r1 = cref("B$3"), r2 = r1.clone();
                expect(r2).toBeInstanceOf(CellRef);
                expect(r2).not.toBe(r1);
                expect(r2).toStringifyTo("B$3");
            });
        });

        describe("method equals", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("equals");
            });
            const r1 = cref("B$3");
            it("should return true for equal cell references", () => {
                expect(r1.equals(cref("B$3"))).toBeTrue();
            });
            it("should return false for different cell references", () => {
                expect(r1.equals(cref("C$3"))).toBeFalse();
                expect(r1.equals(cref("$B$3"))).toBeFalse();
                expect(r1.equals(cref("B3"))).toBeFalse();
                expect(r1.equals(cref("B$2"))).toBeFalse();
            });
        });

        describe("method toAddress", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("toAddress");
            });
            it("should return the address", () => {
                expect(cref("B3").toAddress()).toBeInstanceOf(Address);
                expect(cref("B3").toAddress()).toStringifyTo("B3");
                expect(cref("$B3").toAddress()).toStringifyTo("B3");
                expect(cref("B$3").toAddress()).toStringifyTo("B3");
                expect(cref("$B$3").toAddress()).toStringifyTo("B3");
            });
        });

        describe("method isAbs", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("isAbs");
            });
            it("should return the absolute flag", () => {
                expect(cref("B3").isAbs(false)).toBeFalse();
                expect(cref("B3").isAbs(true)).toBeFalse();
                expect(cref("$B3").isAbs(false)).toBeFalse();
                expect(cref("$B3").isAbs(true)).toBeTrue();
                expect(cref("B$3").isAbs(false)).toBeTrue();
                expect(cref("B$3").isAbs(true)).toBeFalse();
                expect(cref("$B$3").isAbs(false)).toBeTrue();
                expect(cref("$B$3").isAbs(true)).toBeTrue();
            });
        });

        describe("method getIndex", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("getIndex");
            });
            it("should return the specified index", () => {
                expect(cref("B3").getIndex(false)).toBe(2);
                expect(cref("B3").getIndex(true)).toBe(1);
                expect(cref("$B3").getIndex(false)).toBe(2);
                expect(cref("$B3").getIndex(true)).toBe(1);
                expect(cref("B$3").getIndex(false)).toBe(2);
                expect(cref("B$3").getIndex(true)).toBe(1);
                expect(cref("$B$3").getIndex(false)).toBe(2);
                expect(cref("$B$3").getIndex(true)).toBe(1);
            });
        });

        describe("method relocate", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("relocate");
            });
            it("should relocate relative components without wrapping", () => {
                const r1 = cref("B3");
                expect(r1.relocate(addressFactory, a("A2"), a("C5"), false)).toBeTrue();
                expect(r1).toStringifyTo("D6");
                expect(r1.relocate(addressFactory, a("C5"), a("A2"), false)).toBeTrue();
                expect(r1).toStringifyTo("B3");
                expect(cref("A1").relocate(addressFactory, a("B2"), a("A2"), false)).toBeFalse();
                expect(cref("A1").relocate(addressFactory, a("B2"), a("B1"), false)).toBeFalse();
                expect(cref("J20").relocate(addressFactory, a("A1"), a("B1"), false)).toBeFalse();
                expect(cref("J20").relocate(addressFactory, a("A1"), a("A2"), false)).toBeFalse();
            });
            it("should relocate relative components with wrapping", () => {
                const r1 = cref("B3");
                expect(r1.relocate(addressFactory, a("A2"), a("C5"), true)).toBeTrue();
                expect(r1).toStringifyTo("D6");
                expect(r1.relocate(addressFactory, a("C5"), a("A2"), true)).toBeTrue();
                expect(r1).toStringifyTo("B3");
                expect(r1.relocate(addressFactory, a("E6"), a("B2"), true)).toBeTrue();
                expect(r1).toStringifyTo("I19");
                expect(r1.relocate(addressFactory, a("B2"), a("E6"), true)).toBeTrue();
                expect(r1).toStringifyTo("B3");
            });
            it("should not relocate absolute components", () => {
                const r1 = cref("$B$3");
                expect(r1.relocate(addressFactory, a("A2"), a("C5"), false)).toBeTrue();
                expect(r1).toStringifyTo("$B$3");
                const r2 = cref("$B$3");
                expect(r2.relocate(addressFactory, a("A2"), a("C5"), true)).toBeTrue();
                expect(r2).toStringifyTo("$B$3");
            });
        });

        describe("method colText", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("colText");
            });
            it("should return the column text", () => {
                expect(cref("B$3").colText()).toBe("B");
                expect(cref("$B$3").colText()).toBe("$B");
            });
        });

        describe("method rowText", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("rowText");
            });
            it("should return the row text", () => {
                expect(cref("B$3").rowText()).toBe("$3");
                expect(cref("B3").rowText()).toBe("3");
            });
        });

        describe("method refText", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("refText");
            });
            it("should return the reference text", () => {
                expect(cref("$B$3").refText()).toBe("$B$3");
                expect(cref("B$3").refText()).toBe("B$3");
                expect(cref("$B3").refText()).toBe("$B3");
                expect(cref("B3").refText()).toBe("B3");
            });
        });

        describe("method colTextRC", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("colTextRC");
            });
            it("should return the column text for R1C1", () => {
                expect(cref("$C$4").colTextRC("RC", a("A1"))).toBe("C3");
                expect(cref("$C$4").colTextRC("ZS", a("E1"))).toBe("S3");
                expect(cref("C$4").colTextRC("RC", a("B1"))).toBe("C[1]");
                expect(cref("C$4").colTextRC("RC", a("C1"))).toBe("C");
                expect(cref("C$4").colTextRC("RC", a("D1"))).toBe("C[-1]");
            });
        });

        describe("method rowTextRC", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("rowTextRC");
            });
            it("should return the column text for R1C1", () => {
                expect(cref("$C$4").rowTextRC("RC", a("A1"))).toBe("R4");
                expect(cref("$C$4").rowTextRC("ZS", a("A5"))).toBe("Z4");
                expect(cref("$C4").rowTextRC("RC", a("A2"))).toBe("R[2]");
                expect(cref("$C4").rowTextRC("RC", a("A4"))).toBe("R");
                expect(cref("$C4").rowTextRC("RC", a("A6"))).toBe("R[-2]");
            });
        });

        describe("method refTextRC", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("refTextRC");
            });
            it("should return the column text for R1C1", () => {
                expect(cref("$C$4").refTextRC("RC", a("A1"))).toBe("R4C3");
                expect(cref("$C$4").refTextRC("ZS", a("E5"))).toBe("Z4S3");
                expect(cref("C$4").refTextRC("RC", a("B2"))).toBe("R4C[1]");
                expect(cref("C$4").refTextRC("RC", a("C4"))).toBe("R4C");
                expect(cref("C$4").refTextRC("RC", a("D6"))).toBe("R4C[-1]");
                expect(cref("$C4").refTextRC("RC", a("B2"))).toBe("R[2]C3");
                expect(cref("$C4").refTextRC("RC", a("C4"))).toBe("RC3");
                expect(cref("$C4").refTextRC("RC", a("D6"))).toBe("R[-2]C3");
                expect(cref("C4").refTextRC("RC", a("B2"))).toBe("R[2]C[1]");
                expect(cref("C4").refTextRC("RC", a("C4"))).toBe("RC");
                expect(cref("C4").refTextRC("RC", a("D6"))).toBe("R[-2]C[-1]");
            });
        });

        describe("method parseCol", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("parseCol");
            });
            it("should parse the column text", () => {
                function testParseCol(colText, absFlag, expRes, expCol, expAbs) {
                    const cellRef = new CellRef(0, 0, true, true);
                    expect(cellRef.parseCol(colText, absFlag, 255)).toBe(expRes);
                    if (expRes) {
                        expect(cellRef.col).toBe(expCol);
                        expect(cellRef.absCol).toBe(expAbs);
                    }
                }
                testParseCol("A", "$", true, 0, true);
                testParseCol("A", "", true, 0, false);
                testParseCol("A", null, true, 0, false);
                testParseCol("IV", "$", true, 255, true);
                testParseCol("IW", "$", false);
                testParseCol("1", "$", false);
            });
        });

        describe("method parseRow", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("parseRow");
            });
            it("should parse the row text", () => {
                function testParseRow(rowText, absFlag, expRes, expRow, expAbs) {
                    const cellRef = new CellRef(0, 0, true, true);
                    expect(cellRef.parseRow(rowText, absFlag, 255)).toBe(expRes);
                    if (expRes) {
                        expect(cellRef.row).toBe(expRow);
                        expect(cellRef.absRow).toBe(expAbs);
                    }
                }
                testParseRow("1", "$", true, 0, true);
                testParseRow("1", "", true, 0, false);
                testParseRow("1", null, true, 0, false);
                testParseRow("256", "$", true, 255, true);
                testParseRow("257", "$", false);
                testParseRow("A", "$", false);
            });
        });

        describe("method parseColRC", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("parseColRC");
            });
            it("should parse the column text for R1C1", () => {
                function testParseCol(absText, relText, wrap, expRes, expCol, expAbs) {
                    const cellRef = new CellRef(0, 0, true, true);
                    expect(cellRef.parseColRC(absText, relText, 255, 15, wrap)).toBe(expRes);
                    if (expRes) {
                        expect(cellRef.col).toBe(expCol);
                        expect(cellRef.absCol).toBe(expAbs);
                    }
                }
                testParseCol("1", null, false, true, 0, true);
                testParseCol("1", null, true, true, 0, true);
                testParseCol("256", null, false, true, 255, true);
                testParseCol("256", null, true, true, 255, true);
                testParseCol("257", null, false, false);
                testParseCol("257", null, true, false);
                testParseCol("A", null, false, false);
                testParseCol(null, null, false, true, 15, false);
                testParseCol(null, null, true, true, 15, false);
                testParseCol(null, "", false, true, 15, false);
                testParseCol(null, "0", false, true, 15, false);
                testParseCol(null, "-1", false, true, 14, false);
                testParseCol(null, "-1", true, true, 14, false);
                testParseCol(null, "-15", false, true, 0, false);
                testParseCol(null, "-15", true, true, 0, false);
                testParseCol(null, "-16", false, false);
                testParseCol(null, "-16", true, true, 255, false);
                testParseCol(null, "-255", false, false);
                testParseCol(null, "-255", true, true, 16, false);
                testParseCol(null, "-256", false, false);
                testParseCol(null, "-256", true, false);
                testParseCol(null, "1", false, true, 16, false);
                testParseCol(null, "1", true, true, 16, false);
                testParseCol(null, "240", false, true, 255, false);
                testParseCol(null, "240", true, true, 255, false);
                testParseCol(null, "241", false, false);
                testParseCol(null, "241", true, true, 0, false);
                testParseCol(null, "255", false, false);
                testParseCol(null, "255", true, true, 14, false);
                testParseCol(null, "256", false, false);
                testParseCol(null, "256", true, false);
                testParseCol(null, "A", false, false);
            });
        });

        describe("method parseRowRC", () => {
            it("should exist", () => {
                expect(CellRef).toHaveMethod("parseRowRC");
            });
            it("should parse the row text for R1C1", () => {
                function testParseRow(absText, relText, wrap, expRes, expRow, expAbs) {
                    const cellRef = new CellRef(0, 0, true, true);
                    expect(cellRef.parseRowRC(absText, relText, 255, 15, wrap)).toBe(expRes);
                    if (expRes) {
                        expect(cellRef.row).toBe(expRow);
                        expect(cellRef.absRow).toBe(expAbs);
                    }
                }
                testParseRow("1", null, false, true, 0, true);
                testParseRow("1", null, true, true, 0, true);
                testParseRow("256", null, false, true, 255, true);
                testParseRow("256", null, true, true, 255, true);
                testParseRow("257", null, false, false);
                testParseRow("257", null, true, false);
                testParseRow("A", null, false, false);
                testParseRow(null, null, false, true, 15, false);
                testParseRow(null, null, true, true, 15, false);
                testParseRow(null, "", false, true, 15, false);
                testParseRow(null, "0", false, true, 15, false);
                testParseRow(null, "-1", false, true, 14, false);
                testParseRow(null, "-1", true, true, 14, false);
                testParseRow(null, "-15", false, true, 0, false);
                testParseRow(null, "-15", true, true, 0, false);
                testParseRow(null, "-16", false, false);
                testParseRow(null, "-16", true, true, 255, false);
                testParseRow(null, "-255", false, false);
                testParseRow(null, "-255", true, true, 16, false);
                testParseRow(null, "-256", false, false);
                testParseRow(null, "-256", true, false);
                testParseRow(null, "1", false, true, 16, false);
                testParseRow(null, "1", true, true, 16, false);
                testParseRow(null, "240", false, true, 255, false);
                testParseRow(null, "240", true, true, 255, false);
                testParseRow(null, "241", false, false);
                testParseRow(null, "241", true, true, 0, false);
                testParseRow(null, "255", false, false);
                testParseRow(null, "255", true, true, 14, false);
                testParseRow(null, "256", false, false);
                testParseRow(null, "256", true, false);
                testParseRow(null, "A", false, false);
            });
        });
    });
});
