/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ErrorLiteral, ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import * as types from "@/io.ox/office/spreadsheet/model/formula/utils/types";

// tests ======================================================================

describe("module spreadsheet/model/formula/utils/types", () => {

    // types ------------------------------------------------------------------

    const { InternalErrorCode } = types;
    describe("enum InternalErrorCode", () => {
        it("should exist", () => {
            expect(InternalErrorCode).toBeObject();
        });
        it("should provide error code #UNSUPPORTED", () => {
            const UNSUPPORTED = InternalErrorCode.UNSUPPORTED;
            expect(UNSUPPORTED).toBeInstanceOf(ErrorLiteral);
            expect(UNSUPPORTED.key).toBe("unsupported");
            expect(UNSUPPORTED.num).toBeNaN();
            expect(UNSUPPORTED.internal).toBeTrue();
            expect(UNSUPPORTED.value).toBe(ErrorCode.NA);
        });
        it("should provide error code #CIRCULAR", () => {
            const CIRCULAR = InternalErrorCode.CIRCULAR;
            expect(CIRCULAR).toBeInstanceOf(ErrorLiteral);
            expect(CIRCULAR.key).toBe("circular");
            expect(CIRCULAR.num).toBeNaN();
            expect(CIRCULAR.internal).toBeTrue();
            expect(CIRCULAR.value).toBe(0);
        });
        it("should provide error code #TIMEOUT", () => {
            const TIMEOUT = InternalErrorCode.TIMEOUT;
            expect(TIMEOUT).toBeInstanceOf(ErrorLiteral);
            expect(TIMEOUT.key).toBe("timeout");
            expect(TIMEOUT.num).toBeNaN();
            expect(TIMEOUT.internal).toBeTrue();
            expect(TIMEOUT.value).toBe(ErrorCode.NA);
        });
        it("should provide error code #ENGINE", () => {
            const ENGINE = InternalErrorCode.ENGINE;
            expect(ENGINE).toBeInstanceOf(ErrorLiteral);
            expect(ENGINE.key).toBe("engine");
            expect(ENGINE.num).toBeNaN();
            expect(ENGINE.internal).toBeTrue();
            expect(ENGINE.value).toBe(ErrorCode.NA);
        });
    });

    // constants --------------------------------------------------------------

    describe("constant MAX_PARAM_COUNT", () => {
        const { MAX_PARAM_COUNT } = types;
        it("should exist", () => {
            expect(MAX_PARAM_COUNT).toBeIntegerInInterval(1, 1e9);
        });
    });

    describe("constant MAX_REF_LIST_SIZE", () => {
        const { MAX_REF_LIST_SIZE } = types;
        it("should exist", () => {
            expect(MAX_REF_LIST_SIZE).toBeIntegerInInterval(1, 1e9);
        });
    });

    describe("constant MAX_EVAL_TIME", () => {
        const { MAX_EVAL_TIME } = types;
        it("should exist", () => {
            expect(MAX_EVAL_TIME).toBeIntegerInInterval(1, 1e9);
        });
    });

    // public functions -------------------------------------------------------

    describe("function isInternalError", () => {
        const { isInternalError } = types;
        it("should exist", () => {
            expect(isInternalError).toBeFunction();
        });
        it("should return true for internal error codes", () => {
            expect(isInternalError(InternalErrorCode.UNSUPPORTED)).toBeTrue();
            expect(isInternalError(InternalErrorCode.CIRCULAR)).toBeTrue();
            expect(isInternalError(InternalErrorCode.TIMEOUT)).toBeTrue();
            expect(isInternalError(InternalErrorCode.ENGINE)).toBeTrue();
        });
        it("should return false for other values", () => {
            expect(isInternalError(null)).toBeFalse();
            expect(isInternalError(42)).toBeFalse();
            expect(isInternalError(ErrorCode.VALUE)).toBeFalse();
        });
    });

    describe("function throwEngineError", () => {
        const { throwEngineError } = types;
        it("should exist", () => {
            expect(throwEngineError).toBeFunction();
        });
        it("should always throw", () => {
            expect(throwEngineError).toThrowValue(InternalErrorCode.ENGINE);
        });
    });

    describe("function getRecalcMode", () => {
        const { getRecalcMode } = types;
        it("should exist", () => {
            expect(getRecalcMode).toBeFunction();
        });
        it("should return stronger recalc mode", () => {
            expect(getRecalcMode("always", "always")).toBe("always");
            expect(getRecalcMode("always", "once")).toBe("always");
            expect(getRecalcMode("always", "normal")).toBe("always");
            expect(getRecalcMode("once", "always")).toBe("always");
            expect(getRecalcMode("once", "once")).toBe("once");
            expect(getRecalcMode("once", "normal")).toBe("once");
            expect(getRecalcMode("normal", "always")).toBe("always");
            expect(getRecalcMode("normal", "once")).toBe("once");
            expect(getRecalcMode("normal", "normal")).toBe("normal");
        });
    });

    describe("function resolveParentModel", () => {
        const { resolveParentModel } = types;
        it("should exist", () => {
            expect(resolveParentModel).toBeFunction();
        });
    });
});
