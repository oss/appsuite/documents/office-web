/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { MIN_NORM_NUMBER } from "@/io.ox/office/tk/algorithms/math";
import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { Complex } from "@/io.ox/office/spreadsheet/model/formula/utils/complex";

// private functions ==========================================================

// convenience shortcut to create an instance of Complex
function c(r, i, u) {
    return new Complex(r, i, u);
}

// complex zero and one
const C0 = c(0, 0);
const C1 = c(1, 0);

/**
 * Wrapper for Jest's "expect" function with two new pseudo-assertions.
 *
 * - `expect(c1).toBeComplexEqual(c2)` - Checks strict equality of two complex
 *   numbers (using `toBe` for real and imaginary part). Checks equality of the
 *   imaginary unit.
 *
 * - `expect(c1).toBeComplexAlmost(c2)` - Checks almost equality of two complex
 *   numbers (using `toBeAlmost` for real and imaginary part). Checks equality
 *   of the imaginary unit.
 */
function expect(received) {
    const assertion = globalThis.expect(received);
    assertion.toBeComplexEqual = function (expected) {
        globalThis.expect(received.real).toBe(expected.real);
        globalThis.expect(received.imag).toBe(expected.imag);
        globalThis.expect(received.unit).toBe(expected.unit);
    };
    assertion.toBeComplexAlmost = function (expected) {
        globalThis.expect(received.real).toBeAlmost(expected.real);
        globalThis.expect(received.imag).toBeAlmost(expected.imag);
        globalThis.expect(received.unit).toBe(expected.unit);
    };
    return assertion;
}

// tests ======================================================================

describe("module spreadsheet/model/formula/utils/complex", () => {

    // class Complex ----------------------------------------------------------

    describe("class Complex", () => {

        it("should exist", () => {
            expect(Complex).toBeFunction();
        });

        describe("constructor", () => {
            it("should create a complex number", () => {
                const c1 = new Complex(2, -1, "i");
                expect(c1).toHaveProperty("real", 2);
                expect(c1).toHaveProperty("imag", -1);
                expect(c1).toHaveProperty("unit", "i");
                const c2 = new Complex(-1, 0, "j");
                expect(c2).toHaveProperty("real", -1);
                expect(c2).toHaveProperty("imag", 0);
                expect(c2).toHaveProperty("unit", "j");
                const c3 = new Complex(0, 0);
                expect(c3).toHaveProperty("real", 0);
                expect(c3).toHaveProperty("imag", 0);
                expect(c3).toHaveProperty("unit", undefined);
            });
        });

        describe("method add", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("add");
            });
            it("should return a new complex number", () => {
                expect(C0.add(C0)).toBeInstanceOf(Complex);
                expect(C0.add(C0)).not.toBe(C0);
            });
            it("should return sum of two numbers", () => {
                expect(c(0, 0).add(c(0, 0))).toBeComplexEqual(c(0, 0));
                expect(c(1, 2).add(c(3, 4))).toBeComplexEqual(c(4, 6));
                expect(c(1, 2).add(c(-1, -2))).toBeComplexEqual(c(0, 0));
            });
            it("should combine units", () => {
                expect(c(0, 0, "i").add(c(0, 0))).toBeComplexEqual(c(0, 0, "i"));
                expect(c(0, 0).add(c(0, 0, "j"))).toBeComplexEqual(c(0, 0, "j"));
                expect(c(0, 0, "i").add(c(0, 0, "i"))).toBeComplexEqual(c(0, 0, "i"));
                expect(c(0, 0, "j").add(c(0, 0, "j"))).toBeComplexEqual(c(0, 0, "j"));
            });
            it("should fail on different units", () => {
                expect(() => c(0, 0, "i").add(c(0, 0, "j"))).toThrowValue(ErrorCode.VALUE);
                expect(() => c(0, 0, "j").add(c(0, 0, "i"))).toThrowValue(ErrorCode.VALUE);
            });
        });

        describe("method sub", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("sub");
            });
            it("should return a new complex number", () => {
                expect(C0.sub(C0)).toBeInstanceOf(Complex);
                expect(C0.sub(C0)).not.toBe(C0);
            });
            it("should return difference of two numbers", () => {
                expect(c(0, 0).sub(c(0, 0))).toBeComplexEqual(c(0, 0));
                expect(c(1, 2).sub(c(3, 4))).toBeComplexEqual(c(-2, -2));
                expect(c(1, 2).sub(c(-1, -2))).toBeComplexEqual(c(2, 4));
            });
            it("should combine units", () => {
                expect(c(0, 0, "i").sub(c(0, 0))).toBeComplexEqual(c(0, 0, "i"));
                expect(c(0, 0).sub(c(0, 0, "j"))).toBeComplexEqual(c(0, 0, "j"));
                expect(c(0, 0, "i").sub(c(0, 0, "i"))).toBeComplexEqual(c(0, 0, "i"));
                expect(c(0, 0, "j").sub(c(0, 0, "j"))).toBeComplexEqual(c(0, 0, "j"));
            });
            it("should fail on different units", () => {
                expect(() => c(0, 0, "i").sub(c(0, 0, "j"))).toThrowValue(ErrorCode.VALUE);
                expect(() => c(0, 0, "j").sub(c(0, 0, "i"))).toThrowValue(ErrorCode.VALUE);
            });
        });

        describe("method mul", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("mul");
            });
            it("should return a new complex number", () => {
                expect(C0.mul(C0)).toBeInstanceOf(Complex);
                expect(C0.mul(C0)).not.toBe(C0);
            });
            it("should return product of two numbers", () => {
                expect(c(0, 0).mul(c(0, 0))).toBeComplexEqual(c(0, 0));
                expect(c(1, 2).mul(c(3, 4))).toBeComplexEqual(c(-5, 10));
                expect(c(1, 2).mul(c(-1, -2))).toBeComplexEqual(c(3, -4));
            });
            it("should combine units", () => {
                expect(c(0, 0, "i").mul(c(0, 0))).toBeComplexEqual(c(0, 0, "i"));
                expect(c(0, 0).mul(c(0, 0, "j"))).toBeComplexEqual(c(0, 0, "j"));
                expect(c(0, 0, "i").mul(c(0, 0, "i"))).toBeComplexEqual(c(0, 0, "i"));
                expect(c(0, 0, "j").mul(c(0, 0, "j"))).toBeComplexEqual(c(0, 0, "j"));
            });
            it("should fail on different units", () => {
                expect(() => c(0, 0, "i").mul(c(0, 0, "j"))).toThrowValue(ErrorCode.VALUE);
                expect(() => c(0, 0, "j").mul(c(0, 0, "i"))).toThrowValue(ErrorCode.VALUE);
            });
        });

        describe("method div", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("div");
            });
            it("should return a new complex number", () => {
                expect(C1.div(C1)).toBeInstanceOf(Complex);
                expect(C1.div(C1)).not.toBe(C1);
            });
            it("should return quotient of two numbers", () => {
                expect(c(0, 0).div(c(1, 0))).toBeComplexEqual(c(0, 0));
                expect(c(1, 2).div(c(3, 4))).toBeComplexEqual(c(0.44, 0.08));
                expect(c(1, 2).div(c(-1, -2))).toBeComplexEqual(c(-1, 0));
            });
            it("should combine units", () => {
                expect(c(0, 0, "i").div(c(1, 0))).toBeComplexEqual(c(0, 0, "i"));
                expect(c(0, 0).div(c(1, 0, "j"))).toBeComplexEqual(c(0, 0, "j"));
                expect(c(0, 0, "i").div(c(1, 0, "i"))).toBeComplexEqual(c(0, 0, "i"));
                expect(c(0, 0, "j").div(c(1, 0, "j"))).toBeComplexEqual(c(0, 0, "j"));
            });
            it("should fail on division by zero", () => {
                expect(() => c(1, 1).div(c(0, 0))).toThrowValue(ErrorCode.NUM);
                expect(() => c(0, 0).div(c(0, 0))).toThrowValue(ErrorCode.NUM);
            });
            it("should fail on different units", () => {
                expect(() => c(0, 0, "i").div(c(0, 0, "j"))).toThrowValue(ErrorCode.VALUE);
                expect(() => c(0, 0, "j").div(c(0, 0, "i"))).toThrowValue(ErrorCode.VALUE);
            });
        });

        describe("method isZero", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("isZero");
            });
            it("should return true for zero", () => {
                expect(c(0, 0).isZero()).toBeTrue();
                expect(c(MIN_NORM_NUMBER / 2, 0).isZero()).toBeTrue();
                expect(c(0, MIN_NORM_NUMBER / 2).isZero()).toBeTrue();
            });
            it("should return false for non-zero", () => {
                expect(c(1, 0).isZero()).toBeFalse();
                expect(c(0, 1).isZero()).toBeFalse();
                expect(c(MIN_NORM_NUMBER, 0).isZero()).toBeFalse();
                expect(c(0, MIN_NORM_NUMBER).isZero()).toBeFalse();
            });
        });

        describe("method abs", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("abs");
            });
            it("should return the absolute value", () => {
                expect(c(3, 4).abs()).toBe(5);
                expect(c(-3, 4).abs()).toBe(5);
                expect(c(3, -4).abs()).toBe(5);
                expect(c(-3, -4).abs()).toBe(5);
                expect(c(3, 0).abs()).toBe(3);
                expect(c(-3, 0).abs()).toBe(3);
                expect(c(0, 4).abs()).toBe(4);
                expect(c(0, -4).abs()).toBe(4);
                expect(c(0, 0).abs()).toBe(0);
            });
        });

        describe("method arg", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("arg");
            });
            it("should return the argument", () => {
                expect(c(3, 0).arg()).toBe(0);
                expect(c(3, 3).arg()).toBeAlmost(Math.PI * 0.25);
                expect(c(0, 3).arg()).toBeAlmost(Math.PI * 0.5);
                expect(c(-3, 3).arg()).toBeAlmost(Math.PI * 0.75);
                expect(c(-3, 0).arg()).toBeAlmost(Math.PI);
                expect(c(-3, -3).arg()).toBeAlmost(Math.PI * -0.75);
                expect(c(0, -3).arg()).toBeAlmost(Math.PI * -0.5);
                expect(c(3, -3).arg()).toBeAlmost(Math.PI * -0.25);
            });
            it("should throw #DIV/0! error code for complex zero", () => {
                expect(() => C0.arg()).toThrowValue(ErrorCode.DIV0);
            });
        });

        describe("method conj", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("conj");
            });
            it("should return a new complex number", () => {
                expect(C0.conj()).toBeInstanceOf(Complex);
                expect(C0.conj()).not.toBe(C0);
            });
            it("should copy the imaginary unit", () => {
                expect(c(0, 0, "j").conj()).toHaveProperty("unit", "j");
            });
            it("should return the conjugate", () => {
                expect(c(2, 3).conj()).toBeComplexEqual(c(2, -3));
                expect(c(0, 3).conj()).toBeComplexEqual(c(0, -3));
                expect(c(-2, 3).conj()).toBeComplexEqual(c(-2, -3));
                expect(c(2, 0).conj()).toBeComplexEqual(c(2, -0));
                expect(c(0, 0).conj()).toBeComplexEqual(c(0, -0));
                expect(c(-2, 0).conj()).toBeComplexEqual(c(-2, -0));
                expect(c(2, -3).conj()).toBeComplexEqual(c(2, 3));
                expect(c(0, -3).conj()).toBeComplexEqual(c(0, 3));
                expect(c(-2, -3).conj()).toBeComplexEqual(c(-2, 3));
            });
        });

        describe("method exp", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("exp");
            });
            it("should return a new complex number", () => {
                expect(C0.exp()).toBeInstanceOf(Complex);
                expect(C0.exp()).not.toBe(C0);
            });
            it("should copy the imaginary unit", () => {
                expect(c(0, 0, "j").exp()).toHaveProperty("unit", "j");
            });
            it("should return the complex exponential", () => {
                expect(c(0, 0).exp()).toBeComplexEqual(c(1, 0));
                expect(c(4, 3).exp()).toBeComplexAlmost(c(Math.exp(4) * Math.cos(3), Math.exp(4) * Math.sin(3)));
                expect(c(-4, -3).exp()).toBeComplexAlmost(c(Math.exp(-4) * Math.cos(-3), Math.exp(-4) * Math.sin(-3)));
            });
        });

        describe("method log", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("log");
            });
            it("should return a new complex number", () => {
                expect(C1.log(1)).toBeInstanceOf(Complex);
                expect(C1.log(1)).not.toBe(C1);
            });
            it("should copy the imaginary unit", () => {
                expect(c(1, 0, "j").log(1)).toHaveProperty("unit", "j");
            });
            it("should return the complex logarithm", () => {
                expect(c(3, 4).log(2)).toBeComplexAlmost(c(Math.log2(5), Math.atan2(4, 3) / Math.LN2));
            });
            it("should throw #DIV/0! error code for complex zero", () => {
                expect(() => C0.log(1)).toThrowValue(ErrorCode.DIV0);
            });
        });

        describe("method ln", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("ln");
            });
            it("should return a new complex number", () => {
                expect(C1.ln()).toBeInstanceOf(Complex);
                expect(C1.ln()).not.toBe(C1);
            });
            it("should copy the imaginary unit", () => {
                expect(c(1, 0, "j").ln()).toHaveProperty("unit", "j");
            });
            it("should return the natural complex logarithm", () => {
                expect(c(3, 4).ln()).toBeComplexAlmost(c(Math.log(5), Math.atan2(4, 3)));
            });
            it("should throw #DIV/0! error code for complex zero", () => {
                expect(() => C0.ln()).toThrowValue(ErrorCode.DIV0);
            });
        });

        describe("method log10", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("log10");
            });
            it("should return a new complex number", () => {
                expect(C1.log10()).toBeInstanceOf(Complex);
                expect(C1.log10()).not.toBe(C1);
            });
            it("should copy the imaginary unit", () => {
                expect(c(1, 0, "j").log10()).toHaveProperty("unit", "j");
            });
            it("should return the decimal complex logarithm", () => {
                expect(c(3, 4).log10()).toBeComplexAlmost(c(Math.log10(5), Math.atan2(4, 3) / Math.LN10));
            });
            it("should throw #DIV/0! error code for complex zero", () => {
                expect(() => C0.log10()).toThrowValue(ErrorCode.DIV0);
            });
        });

        describe("method log2", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("log2");
            });
            it("should return a new complex number", () => {
                expect(C1.log2()).toBeInstanceOf(Complex);
                expect(C1.log2()).not.toBe(C1);
            });
            it("should copy the imaginary unit", () => {
                expect(c(1, 0, "j").log2()).toHaveProperty("unit", "j");
            });
            it("should return the binary complex logarithm", () => {
                expect(c(3, 4).log2()).toBeComplexAlmost(c(Math.log2(5), Math.atan2(4, 3) / Math.LN2));
            });
            it("should throw #DIV/0! error code for complex zero", () => {
                expect(() => C0.log2()).toThrowValue(ErrorCode.DIV0);
            });
        });

        describe("method pow", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("pow");
            });
            it("should return a new complex number", () => {
                expect(C0.pow(1)).toBeInstanceOf(Complex);
                expect(C0.pow(1)).not.toBe(C0);
            });
            it("should copy the imaginary unit", () => {
                expect(c(0, 0, "j").pow(1)).toHaveProperty("unit", "j");
            });
            it("should return the complex power", () => {
                expect(c(0, 0).pow(1)).toBeComplexEqual(c(0, 0));
                expect(c(4, -3).pow(2)).toBeComplexAlmost(c(7, -24));
                expect(c(-7, 24).pow(0.5)).toBeComplexAlmost(c(3, 4));
                expect(c(3, 4).pow(-1)).toBeComplexAlmost(c(0.12, -0.16));
            });
            it("should throw #NUM! error code for complex zero and non-positive exponent", () => {
                expect(() => C0.pow(0)).toThrowValue(ErrorCode.NUM);
                expect(() => C0.pow(-1)).toThrowValue(ErrorCode.NUM);
            });
        });

        describe("method sqrt", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("sqrt");
            });
            it("should return a new complex number", () => {
                expect(C0.sqrt()).toBeInstanceOf(Complex);
                expect(C0.sqrt()).not.toBe(C0);
            });
            it("should copy the imaginary unit", () => {
                expect(c(0, 0, "j").sqrt()).toHaveProperty("unit", "j");
            });
            it("should return the complex square root", () => {
                expect(c(0, 0).sqrt()).toBeComplexEqual(c(0, 0));
                expect(c(-7, 24).sqrt()).toBeComplexEqual(c(3, 4));
            });
        });

        describe("method sin", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("sin");
            });
            it("should return a new complex number", () => {
                expect(C0.sin()).toBeInstanceOf(Complex);
                expect(C0.sin()).not.toBe(C0);
            });
            it("should copy the imaginary unit", () => {
                expect(c(0, 0, "j").sin()).toHaveProperty("unit", "j");
            });
        });

        describe("method cos", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("cos");
            });
            it("should return a new complex number", () => {
                expect(C0.cos()).toBeInstanceOf(Complex);
                expect(C0.cos()).not.toBe(C0);
            });
            it("should copy the imaginary unit", () => {
                expect(c(0, 0, "j").cos()).toHaveProperty("unit", "j");
            });
        });

        describe("method sec", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("sec");
            });
            it("should return a new complex number", () => {
                expect(C0.sec()).toBeInstanceOf(Complex);
                expect(C0.sec()).not.toBe(C0);
            });
            it("should copy the imaginary unit", () => {
                expect(c(0, 0, "j").sec()).toHaveProperty("unit", "j");
            });
            it("should throw #NUM! error code for complex zero", () => {
                expect(() => c(Math.PI / 2, 0).sec()).toThrowValue(ErrorCode.NUM);
            });
        });

        describe("method csc", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("csc");
            });
            it("should return a new complex number", () => {
                expect(C1.csc()).toBeInstanceOf(Complex);
                expect(C1.csc()).not.toBe(C1);
            });
            it("should copy the imaginary unit", () => {
                expect(c(1, 0, "j").csc()).toHaveProperty("unit", "j");
            });
            it("should throw #NUM! error code for complex zero", () => {
                expect(() => C0.csc()).toThrowValue(ErrorCode.NUM);
            });
        });

        describe("method tan", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("tan");
            });
            it("should return a new complex number", () => {
                expect(C0.tan()).toBeInstanceOf(Complex);
                expect(C0.tan()).not.toBe(C0);
            });
            it("should copy the imaginary unit", () => {
                expect(c(0, 0, "j").tan()).toHaveProperty("unit", "j");
            });
            it("should throw #NUM! error code for complex zero", () => {
                expect(() => c(Math.PI / 2, 0).tan()).toThrowValue(ErrorCode.NUM);
            });
        });

        describe("method cot", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("cot");
            });
            it("should return a new complex number", () => {
                expect(C1.cot()).toBeInstanceOf(Complex);
                expect(C1.cot()).not.toBe(C1);
            });
            it("should copy the imaginary unit", () => {
                expect(c(1, 0, "j").cot()).toHaveProperty("unit", "j");
            });
            it("should throw #NUM! error code for complex zero", () => {
                expect(() => C0.cot()).toThrowValue(ErrorCode.NUM);
            });
        });

        describe("method sinh", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("sinh");
            });
            it("should return a new complex number", () => {
                expect(C0.sinh()).toBeInstanceOf(Complex);
                expect(C0.sinh()).not.toBe(C0);
            });
            it("should copy the imaginary unit", () => {
                expect(c(0, 0, "j").sinh()).toHaveProperty("unit", "j");
            });
        });

        describe("method cosh", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("cosh");
            });
            it("should return a new complex number", () => {
                expect(C0.cosh()).toBeInstanceOf(Complex);
                expect(C0.cosh()).not.toBe(C0);
            });
            it("should copy the imaginary unit", () => {
                expect(c(0, 0, "j").cosh()).toHaveProperty("unit", "j");
            });
        });

        describe("method sech", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("sech");
            });
            it("should return a new complex number", () => {
                expect(C0.sech()).toBeInstanceOf(Complex);
                expect(C0.sech()).not.toBe(C0);
            });
            it("should copy the imaginary unit", () => {
                expect(c(0, 0, "j").sech()).toHaveProperty("unit", "j");
            });
            it("should throw #NUM! error code for complex zero", () => {
                expect(() => c(0, Math.PI / 2).sech()).toThrowValue(ErrorCode.NUM);
            });
        });

        describe("method csch", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("csch");
            });
            it("should return a new complex number", () => {
                expect(C1.csch()).toBeInstanceOf(Complex);
                expect(C1.csch()).not.toBe(C1);
            });
            it("should copy the imaginary unit", () => {
                expect(c(1, 0, "j").csch()).toHaveProperty("unit", "j");
            });
            it("should throw #NUM! error code for complex zero", () => {
                expect(() => C0.csch()).toThrowValue(ErrorCode.NUM);
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(Complex).toHaveMethod("toString");
            });
            it("should return the string representation", () => {
                expect(new Complex(3, 4, "i").toString()).toBe("3+4i");
                expect(new Complex(-3, 4, "j").toString()).toBe("-3+4j");
                expect(new Complex(3, -4, "i").toString()).toBe("3-4i");
                expect(new Complex(-3, -4, "j").toString()).toBe("-3-4j");
                expect(new Complex(3, 4).toString()).toBe("3+4i");
                expect(new Complex(3, 0, "i").toString()).toBe("3+0i");
                expect(new Complex(-3, 0, "j").toString()).toBe("-3+0j");
                expect(new Complex(0, 4, "i").toString()).toBe("0+4i");
                expect(new Complex(0, -4, "j").toString()).toBe("0-4j");
                expect(new Complex(0, 0).toString()).toBe("0+0i");
            });
        });
    });
});
