/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { json } from "@/io.ox/office/tk/algorithms";

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import { InternalErrorCode } from "@/io.ox/office/spreadsheet/model/formula/utils/types";
import { Dimension } from "@/io.ox/office/spreadsheet/model/formula/utils/dimension";
import * as matrix from "@/io.ox/office/spreadsheet/model/formula/utils/matrix";

// constants ==================================================================

const a1 = [[1]];
const a2 = [[1, 2], ["a", "b"], [false, true]];

// tests ======================================================================

describe("module spreadsheet/model/formula/utils/matrix", () => {

    // constants --------------------------------------------------------------

    const { MAX_MATRIX_SIZE } = matrix;
    describe("constant MAX_MATRIX_SIZE", () => {
        it("should exist", () => {
            expect(MAX_MATRIX_SIZE).toBeIntegerInInterval(1, 1e9);
        });
    });

    // public functions -------------------------------------------------------

    describe("function isValidMatrixDim", () => {
        const { isValidMatrixDim } = matrix;
        it("should exist", () => {
            expect(isValidMatrixDim).toBeFunction();
        });
        const rows = Math.floor(Math.sqrt(MAX_MATRIX_SIZE));
        const cols = Math.floor(MAX_MATRIX_SIZE / rows);
        it("should return true for valid matrix sizes", () => {
            expect(isValidMatrixDim(1, 1)).toBeTrue();
            expect(isValidMatrixDim(MAX_MATRIX_SIZE, 1)).toBeTrue();
            expect(isValidMatrixDim(1, MAX_MATRIX_SIZE)).toBeTrue();
            expect(isValidMatrixDim(rows, cols)).toBeTrue();
            expect(isValidMatrixDim(new Dimension(rows, cols))).toBeTrue();
        });
        it("should return false for invalid matrix sizes", () => {
            expect(isValidMatrixDim(0, 1)).toBeFalse();
            expect(isValidMatrixDim(1, 0)).toBeFalse();
            expect(isValidMatrixDim(MAX_MATRIX_SIZE + 1, 1)).toBeFalse();
            expect(isValidMatrixDim(1, MAX_MATRIX_SIZE + 1)).toBeFalse();
            expect(isValidMatrixDim(rows + 1, cols)).toBeFalse();
            expect(isValidMatrixDim(rows, cols + 1)).toBeFalse();
            expect(isValidMatrixDim(new Dimension(rows, cols + 1))).toBeFalse();
        });
    });

    describe("function ensureMatrixDim", () => {
        const { ensureMatrixDim } = matrix;
        it("should exist", () => {
            expect(ensureMatrixDim).toBeFunction();
        });
        it("should do nothing for valid matrix sizes", () => {
            // simply expect that the function does not throw
            ensureMatrixDim(1, 1);
            ensureMatrixDim(MAX_MATRIX_SIZE, 1);
            ensureMatrixDim(1, MAX_MATRIX_SIZE);
            ensureMatrixDim(new Dimension(1, MAX_MATRIX_SIZE));
        });
        it("should throw for invalid matrix sizes", () => {
            expect(() => ensureMatrixDim(0, 1)).toThrowValue(InternalErrorCode.UNSUPPORTED);
            expect(() => ensureMatrixDim(1, 0)).toThrowValue(InternalErrorCode.UNSUPPORTED);
            expect(() => ensureMatrixDim(MAX_MATRIX_SIZE + 1, 1)).toThrowValue(InternalErrorCode.UNSUPPORTED);
            expect(() => ensureMatrixDim(1, MAX_MATRIX_SIZE + 1)).toThrowValue(InternalErrorCode.UNSUPPORTED);
            expect(() => ensureMatrixDim(new Dimension(1, MAX_MATRIX_SIZE + 1))).toThrowValue(InternalErrorCode.UNSUPPORTED);
        });
    });

    describe("function getMatrixAddress", () => {
        const { getMatrixAddress } = matrix;
        it("should exist", () => {
            expect(getMatrixAddress).toBeFunction();
        });
    });

    // class Matrix -----------------------------------------------------------

    const { Matrix } = matrix;
    describe("class Matrix", () => {

        it("should exist", () => {
            expect(Matrix).toBeFunction();
        });

        describe("constructor()", () => {
            it("should create a matrix from arrays", () => {
                const m1 = new Matrix(a2);
                expect(m1.raw()).toBe(a2);
            });
            it("should fill a matrix", () => {
                const m1 = new Matrix(1, 1, 1);
                expect(m1).toBeInstanceOf(Matrix);
                expect(m1.raw()).toEqual([[1]]);
                const m2 = new Matrix(3, 2, "a");
                expect(m2).toBeInstanceOf(Matrix);
                expect(m2.raw()).toEqual([["a", "a"], ["a", "a"], ["a", "a"]]);
            });
            it("should generate a matrix", () => {
                const m1 = new Matrix(1, 1, () => 1);
                expect(m1).toBeInstanceOf(Matrix);
                expect(m1.raw()).toEqual([[1]]);
                const spy = jest.fn((row, col) => a2[row][col]);
                const m2 = new Matrix(3, 2, spy, a1);
                expect(m2).toBeInstanceOf(Matrix);
                expect(m2.raw()).toEqual(a2);
                expect(spy).toHaveBeenCalledTimes(6);
                expect(spy).toHaveBeenAlwaysCalledOn(a1);
            });
        });

        describe("method raw", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("raw");
            });
            const m2 = new Matrix(a2);
            it("should return the internal arrays", () => {
                expect(m2.raw()).toBe(a2);
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("clone");
            });
            it("should return a deep clone of a matrix", () => {
                const m1 = new Matrix(a1);
                const m2 = m1.clone();
                expect(m2).toBeInstanceOf(Matrix);
                expect(m2.raw()).toEqual(a1);
                expect(m2.raw()).not.toBe(a1);
            });
        });

        describe("method rows", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("rows");
            });
            it("should return the number of rows", () => {
                expect(new Matrix(a1).rows()).toBe(1);
                expect(new Matrix(a2).rows()).toBe(3);
            });
        });

        describe("method cols", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("cols");
            });
            it("should return the number of rows", () => {
                expect(new Matrix(a1).cols()).toBe(1);
                expect(new Matrix(a2).cols()).toBe(2);
            });
        });

        describe("method size", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("size");
            });
            it("should return the number of rows", () => {
                expect(new Matrix(a1).size()).toBe(1);
                expect(new Matrix(a2).size()).toBe(6);
            });
        });

        describe("method get", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("get");
            });
            const m1 = new Matrix(a1), m2 = new Matrix(a2), m3 = new Matrix([[2, 3]]), m4 = new Matrix([[2], [3]]);
            it("should return the specified element", () => {
                expect(m1.get(0, 0)).toBe(a1[0][0]);
                expect(m2.get(0, 0)).toBe(a2[0][0]);
                expect(m2.get(0, 1)).toBe(a2[0][1]);
                expect(m2.get(1, 0)).toBe(a2[1][0]);
                expect(m2.get(1, 1)).toBe(a2[1][1]);
                expect(m2.get(2, 0)).toBe(a2[2][0]);
                expect(m2.get(2, 1)).toBe(a2[2][1]);
            });
            it("should ignore row index for single-row vector", () => {
                expect(m3.get(0, 0)).toBe(2);
                expect(m3.get(0, 1)).toBe(3);
                expect(m3.get(1, 0)).toBe(2);
                expect(m3.get(1, 1)).toBe(3);
                expect(m3.get(9, 0)).toBe(2);
                expect(m3.get(9, 1)).toBe(3);
            });
            it("should ignore column index for single-column vector", () => {
                expect(m4.get(0, 0)).toBe(2);
                expect(m4.get(0, 1)).toBe(2);
                expect(m4.get(0, 9)).toBe(2);
                expect(m4.get(1, 0)).toBe(3);
                expect(m4.get(1, 1)).toBe(3);
                expect(m4.get(1, 9)).toBe(3);
            });
            it("should return undefined on invalid indexes", () => {
                expect(m2.get(2, 2)).toBeUndefined();
                expect(m2.get(3, 1)).toBeUndefined();
                expect(m2.get(3, 2)).toBeUndefined();
                expect(m3.get(0, 2)).toBeUndefined();
                expect(m3.get(9, 2)).toBeUndefined();
                expect(m4.get(2, 0)).toBeUndefined();
                expect(m4.get(2, 9)).toBeUndefined();
            });
            it("should return default value on invalid indexes", () => {
                expect(m2.get(2, 2, ErrorCode.NA)).toBe(ErrorCode.NA);
            });
        });

        describe("method toRowVector", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("toRowVector");
            });
            const m2 = new Matrix(a2);
            it("should return row vector", () => {
                expect(m2.toRowVector(0)).toBeInstanceOf(Matrix);
                expect(m2.toRowVector(0).raw()).toEqual([[1, 2]]);
                expect(m2.toRowVector(1).raw()).toEqual([["a", "b"]]);
                expect(m2.toRowVector(2).raw()).toEqual([[false, true]]);
            });
        });

        describe("method toColVector", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("toColVector");
            });
            const m2 = new Matrix(a2);
            it("should return column vector", () => {
                expect(m2.toColVector(0)).toBeInstanceOf(Matrix);
                expect(m2.toColVector(0).raw()).toEqual([[1], ["a"], [false]]);
                expect(m2.toColVector(1).raw()).toEqual([[2], ["b"], [true]]);
            });
        });

        describe("method getByIndex", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("getByIndex");
            });
            const m2 = new Matrix(a2);
            it("should return the specified element", () => {
                expect(m2.getByIndex(0)).toBe(a2[0][0]);
                expect(m2.getByIndex(1)).toBe(a2[0][1]);
                expect(m2.getByIndex(2)).toBe(a2[1][0]);
                expect(m2.getByIndex(3)).toBe(a2[1][1]);
                expect(m2.getByIndex(4)).toBe(a2[2][0]);
                expect(m2.getByIndex(5)).toBe(a2[2][1]);
            });
        });

        describe("method set", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("set");
            });
            const m1 = new Matrix(3, 2, 1);
            it("should change the specified element", () => {
                m1.set(0, 0, "a");
                expect(m1.raw()[0][0]).toBe("a");
                m1.set(2, 1, true);
                expect(m1.raw()[2][1]).toBeTrue();
            });
            it("should return itself", () => {
                expect(m1.set(0, 0, 1)).toBe(m1);
            });
        });

        describe("method setByIndex", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("setByIndex");
            });
            const m1 = new Matrix(3, 2, 1);
            it("should change the specified element", () => {
                m1.setByIndex(0, "a");
                expect(m1.raw()[0][0]).toBe("a");
                m1.setByIndex(5, true);
                expect(m1.raw()[2][1]).toBeTrue();
            });
            it("should return itself", () => {
                expect(m1.setByIndex(0, 1)).toBe(m1);
            });
        });

        describe("method update", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("update");
            });
            const m1 = new Matrix(3, 2, 1);
            it("should update the specified element", () => {
                m1.update(0, 0, function (n) { return n + 1; });
                expect(m1.raw()[0][0]).toBe(2);
                m1.update(2, 1, function (n) { return n - 1; });
                expect(m1.raw()[2][1]).toBe(0);
            });
            it("should return itself", () => {
                expect(m1.update(0, 0, n => n)).toBe(m1);
            });
        });

        describe("method updateByIndex", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("updateByIndex");
            });
            const m1 = new Matrix(3, 2, 1);
            it("should update the specified element", () => {
                m1.updateByIndex(0, function (n) { return n + 1; });
                expect(m1.raw()[0][0]).toBe(2);
                m1.updateByIndex(5, function (n) { return n - 1; });
                expect(m1.raw()[2][1]).toBe(0);
            });
            it("should return itself", () => {
                expect(m1.updateByIndex(0, n => n)).toBe(m1);
            });
        });

        describe("method fill", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("fill");
            });
            const m1 = new Matrix(4, 4, 1);
            it("should change the specified elements", () => {
                m1.fill(0, 1, 2, 3, 42);
                expect(m1.raw()).toEqual([[1, 42, 42, 42], [1, 42, 42, 42], [1, 42, 42, 42], [1, 1, 1, 1]]);
            });
            it("should return itself", () => {
                expect(m1.fill(0, 0, 0, 0, 1)).toBe(m1);
            });
        });

        describe("method values", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("values");
            });
            it("should create an iterator that visits all matrix elements", () => {
                const m = new Matrix(a2), it = m.values();
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([1, 2, "a", "b", false, true]);
            });
        });

        describe("method entries", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("entries");
            });
            it("should create an iterator that visits all matrix elements", () => {
                const m = new Matrix(a2), it = m.entries();
                expect(it).toBeIterator();
                expect(Array.from(it)).toEqual([
                    { row: 0, col: 0, value: 1 },
                    { row: 0, col: 1, value: 2 },
                    { row: 1, col: 0, value: "a" },
                    { row: 1, col: 1, value: "b" },
                    { row: 2, col: 0, value: false },
                    { row: 2, col: 1, value: true }
                ]);
            });
        });

        describe("method forEach", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("forEach");
            });
            it("should visit all matrix elements", () => {
                const m = new Matrix(a2), spy = jest.fn();
                expect(m.forEach(spy, a1)).toBe(m);
                expect(spy).toHaveBeenCalledTimes(6);
                expect(spy).toHaveBeenAlwaysCalledOn(a1);
                expect(spy).toHaveBeenNthCalledWith(1, a2[0][0], 0, 0);
                expect(spy).toHaveBeenNthCalledWith(2, a2[0][1], 0, 1);
                expect(spy).toHaveBeenNthCalledWith(3, a2[1][0], 1, 0);
                expect(spy).toHaveBeenNthCalledWith(4, a2[1][1], 1, 1);
                expect(spy).toHaveBeenNthCalledWith(5, a2[2][0], 2, 0);
                expect(spy).toHaveBeenNthCalledWith(6, a2[2][1], 2, 1);
            });
        });

        describe("method map", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("map");
            });
            it("should create a new matrix", () => {
                const m1 = new Matrix([[1, 2], [3, 4]]), m2 = m1.map(function (e) { return e + 1; });
                expect(m2).toBeInstanceOf(Matrix);
                expect(m1.raw()).not.toBe(m2.raw());
                expect(m2.raw()).toEqual([[2, 3], [4, 5]]);
            });
        });

        describe("method reduce", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("reduce");
            });
            it("should reduce all matrix elements to a value", () => {
                const m = new Matrix([[1, 2], [3, 4]]);
                const spy = jest.fn((a, e) => a + e);
                expect(m.reduce(0, spy, a1)).toBe(10);
                expect(spy).toHaveBeenCalledTimes(4);
                expect(spy).toHaveBeenAlwaysCalledOn(a1);
                expect(spy).toHaveBeenNthCalledWith(1, 0, 1, 0, 0);
                expect(spy).toHaveBeenNthCalledWith(2, 1, 2, 0, 1);
                expect(spy).toHaveBeenNthCalledWith(3, 3, 3, 1, 0);
                expect(spy).toHaveBeenNthCalledWith(4, 6, 4, 1, 1);
            });
        });

        describe("method transform", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("transform");
            });
            it("should modify all matrix elements", () => {
                const m = new Matrix([[1, 2], [3, 4]]);
                const spy = jest.fn(e => e * e);
                expect(m.transform(spy, a1)).toBe(m);
                expect(m.raw()).toEqual([[1, 4], [9, 16]]);
                expect(spy).toHaveBeenCalledTimes(4);
                expect(spy).toHaveBeenAlwaysCalledOn(a1);
                expect(spy).toHaveBeenNthCalledWith(1, 1, 0, 0);
                expect(spy).toHaveBeenNthCalledWith(2, 2, 0, 1);
                expect(spy).toHaveBeenNthCalledWith(3, 3, 1, 0);
                expect(spy).toHaveBeenNthCalledWith(4, 4, 1, 1);
            });
        });

        describe("method swapRows", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("swapRows");
            });
            it("should swap the row elements of a matrix", () => {
                const m = new Matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
                expect(m.swapRows(1, 2)).toBe(m);
                expect(m.raw()).toEqual([[1, 2, 3], [7, 8, 9], [4, 5, 6]]);
            });
        });

        describe("method transpose", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("transpose");
            });
            it("should transpose a matrix", () => {
                const m1 = new Matrix(json.deepClone(a2)), m2 = m1.transpose();
                expect(m2).toBeInstanceOf(Matrix);
                expect(m2).not.toBe(m1);
                expect(m1.raw()).toEqual(a2);
                expect(m2.raw()).toEqual([[1, "a", false], [2, "b", true]]);
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(Matrix).toHaveMethod("toString");
            });
            it("should return the string representation", () => {
                const m = new Matrix(a2);
                expect(m.toString()).toBe('{1;2|"a";"b"|FALSE;TRUE}');
                expect("<" + m + ">").toBe('<{1;2|"a";"b"|FALSE;TRUE}>');
            });
        });
    });

    // class NumberMatrix -----------------------------------------------------

    describe("class NumberMatrix", () => {

        const { NumberMatrix } = matrix;
        it("should subclass Matrix", () => {
            expect(NumberMatrix).toBeSubClassOf(Matrix);
        });

        describe("static function identity", () => {
            it("should exist", () => {
                expect(NumberMatrix).toHaveStaticMethod("identity");
            });
            it("should create an identity matrix", () => {
                const m1 = NumberMatrix.identity(1);
                expect(m1).toBeInstanceOf(NumberMatrix);
                expect(m1.raw()).toEqual([[1]]);
                const m3 = NumberMatrix.identity(3);
                expect(m3).toBeInstanceOf(NumberMatrix);
                expect(m3.raw()).toEqual([[1, 0, 0], [0, 1, 0], [0, 0, 1]]);
            });
        });

        describe("method add", () => {
            it("should exist", () => {
                expect(NumberMatrix).toHaveMethod("add");
            });
            const m1 = new NumberMatrix(3, 2, 1);
            it("should update the specified element", () => {
                m1.add(0, 0, 1);
                expect(m1.raw()[0][0]).toBe(2);
                m1.add(2, 1, -1);
                expect(m1.raw()[2][1]).toBe(0);
            });
            it("should return itself", () => {
                expect(m1.add(0, 0, 0)).toBe(m1);
            });
        });

        describe("method addByIndex", () => {
            it("should exist", () => {
                expect(NumberMatrix).toHaveMethod("addByIndex");
            });
            const m1 = new NumberMatrix(3, 2, 1);
            it("should update the specified element", () => {
                m1.addByIndex(0, 1);
                expect(m1.raw()[0][0]).toBe(2);
                m1.addByIndex(5, -1);
                expect(m1.raw()[2][1]).toBe(0);
            });
            it("should return itself", () => {
                expect(m1.addByIndex(0, 0)).toBe(m1);
            });
        });

        describe("method scaleRow", () => {
            it("should exist", () => {
                expect(NumberMatrix).toHaveMethod("scaleRow");
            });
            it("should scale the row elements of a matrix", () => {
                const m = new NumberMatrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
                expect(m.scaleRow(1, 2)).toBe(m);
                expect(m.raw()).toEqual([[1, 2, 3], [8, 10, 12], [7, 8, 9]]);
            });
        });

        describe("method addScaledRow", () => {
            it("should exist", () => {
                expect(NumberMatrix).toHaveMethod("addScaledRow");
            });
            it("should add the scaled row elements of a matrix", () => {
                const m = new NumberMatrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
                expect(m.addScaledRow(1, 2, -2)).toBe(m);
                expect(m.raw()).toEqual([[1, 2, 3], [-10, -11, -12], [7, 8, 9]]);
            });
        });
    });
});
