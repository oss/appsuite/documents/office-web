/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ErrorCode } from "@/io.ox/office/spreadsheet/utils/scalar";
import * as mathutils from "@/io.ox/office/spreadsheet/model/formula/utils/mathutils";

const { log } = Math;

// tests ======================================================================

describe("module spreadsheet/model/formula/utils/mathutils", () => {

    // functions --------------------------------------------------------------

    describe("function add", () => {
        const { add } = mathutils;
        it("should exist", () => {
            expect(add).toBeFunction();
        });
        it("should return the sum of two numbers", () => {
            expect(add(3, 2)).toBe(5);
            expect(add(0, 2)).toBe(2);
        });
        it("should return the concatenation of two strings", () => {
            expect(add("a", "b")).toBe("ab");
        });
    });

    describe("function sub", () => {
        const { sub } = mathutils;
        it("should exist", () => {
            expect(sub).toBeFunction();
        });
        it("should return the difference of two numbers", () => {
            expect(sub(3, 2)).toBe(1);
            expect(sub(0, 2)).toBe(-2);
        });
    });

    describe("function mul", () => {
        const { mul } = mathutils;
        it("should exist", () => {
            expect(mul).toBeFunction();
        });
        it("should return the product of two numbers", () => {
            expect(mul(3, 2)).toBe(6);
            expect(mul(-3, 2)).toBe(-6);
            expect(mul(3, -2)).toBe(-6);
            expect(mul(-3, -2)).toBe(6);
            expect(mul(0, 2)).toBe(0);
            expect(mul(3, 0)).toBe(0);
        });
    });

    describe("function div", () => {
        const { div } = mathutils;
        it("should exist", () => {
            expect(div).toBeFunction();
        });
        it("should return the quotient of two numbers", () => {
            expect(div(3, 2)).toBe(1.5);
            expect(div(0, 2)).toBe(0);
        });
        it("should throw the #DIV/0! error, if divisor is 0", () => {
            expect(() => div(3, 0)).toThrowValue(ErrorCode.DIV0);
            expect(() => div(0, 0)).toThrowValue(ErrorCode.DIV0);
        });
    });

    describe("function mod", () => {
        const { mod } = mathutils;
        it("should exist", () => {
            expect(mod).toBeFunction();
        });
        it("should return the remainder, if divisor is positive", () => {
            expect(mod(-5, 3)).toBe(1);
            expect(mod(-4, 3)).toBe(2);
            expect(mod(-3, 3)).toBe(-0);
            expect(mod(-2, 3)).toBe(1);
            expect(mod(-1, 3)).toBe(2);
            expect(mod(0, 3)).toBe(0);
            expect(mod(1, 3)).toBe(1);
            expect(mod(2, 3)).toBe(2);
            expect(mod(3, 3)).toBe(0);
            expect(mod(4, 3)).toBe(1);
            expect(mod(5, 3)).toBe(2);
        });
        it("should return the remainder, if divisor is negative", () => {
            expect(mod(-5, -3)).toBe(-2);
            expect(mod(-4, -3)).toBe(-1);
            expect(mod(-3, -3)).toBe(-0);
            expect(mod(-2, -3)).toBe(-2);
            expect(mod(-1, -3)).toBe(-1);
            expect(mod(0, -3)).toBe(0);
            expect(mod(1, -3)).toBe(-2);
            expect(mod(2, -3)).toBe(-1);
            expect(mod(3, -3)).toBe(0);
            expect(mod(4, -3)).toBe(-2);
            expect(mod(5, -3)).toBe(-1);
        });
        it("should throw the #DIV/0! error, if divisor is 0", () => {
            expect(() => mod(3, 0)).toThrowValue(ErrorCode.DIV0);
            expect(() => mod(0, 0)).toThrowValue(ErrorCode.DIV0);
        });
    });

    describe("function powNaN", () => {
        const { powNaN } = mathutils;
        it("should exist", () => {
            expect(powNaN).toBeFunction();
        });
        it("should return the power of two numbers", () => {
            expect(powNaN(3, 0)).toBe(1);
            expect(powNaN(3, 1)).toBe(3);
            expect(powNaN(3, 2)).toBe(9);
            expect(powNaN(3, 3)).toBe(27);
            expect(powNaN(9, 0.5)).toBeAlmost(3);
            expect(powNaN(81, 0.25)).toBeAlmost(3);
            expect(powNaN(4, -1)).toBeAlmost(0.25);
            expect(powNaN(2, -2)).toBeAlmost(0.25);
            expect(powNaN(0, 1)).toBe(0);
            expect(powNaN(0, 2)).toBe(0);
            expect(powNaN(0, 0.5)).toBe(0);
        });
        it("should throw the #NUM! error, if both numbers are 0", () => {
            expect(() => powNaN(0, 0)).toThrowValue(ErrorCode.NUM);
        });
        it("should throw the #DIV/0! error, if base is 0, and exponent is negative", () => {
            expect(() => powNaN(0, -1)).toThrowValue(ErrorCode.DIV0);
        });
    });

    describe("function pow1", () => {
        const { pow1 } = mathutils;
        it("should exist", () => {
            expect(pow1).toBeFunction();
        });
        it("should return the power of two numbers", () => {
            expect(pow1(3, 0)).toBe(1);
            expect(pow1(3, 1)).toBe(3);
            expect(pow1(3, 2)).toBe(9);
            expect(pow1(3, 3)).toBe(27);
            expect(pow1(9, 0.5)).toBeAlmost(3);
            expect(pow1(81, 0.25)).toBeAlmost(3);
            expect(pow1(4, -1)).toBeAlmost(0.25);
            expect(pow1(2, -2)).toBeAlmost(0.25);
            expect(pow1(0, 1)).toBe(0);
            expect(pow1(0, 2)).toBe(0);
            expect(pow1(0, 0.5)).toBe(0);
        });
        it("should return 1, if both numbers are 0", () => {
            expect(pow1(0, 0)).toBe(1);
        });
        it("should throw the #NUM! error, if base is 0, and exponent is negative", () => {
            expect(() => pow1(0, -1)).toThrowValue(ErrorCode.NUM);
        });
    });

    describe("constant POW", () => {
        const { POW, powNaN, pow1 } = mathutils;
        it("should exist", () => {
            expect(POW).toBeObject();
        });
        it("should contain resolvers for file formats", () => {
            expect(POW).toHaveProperty("ooxml", powNaN);
            expect(POW).toHaveProperty("odf", pow1);
        });
    });

    describe("function atanxy", () => {
        const { atanxy } = mathutils;
        it("should exist", () => {
            expect(atanxy).toBeFunction();
        });
        it("should return the arc tangent of two numbers", () => {
            expect(atanxy(1, 0)).toBeAlmost(0);
            expect(atanxy(1, 1)).toBeAlmost(0.25 * Math.PI);
            expect(atanxy(0, 1)).toBeAlmost(0.5 * Math.PI);
            expect(atanxy(-1, 1)).toBeAlmost(0.75 * Math.PI);
            expect(atanxy(-1, 0)).toBeAlmost(Math.PI);
            expect(atanxy(-1, -1)).toBeAlmost(-0.75 * Math.PI);
            expect(atanxy(0, -1)).toBeAlmost(-0.5 * Math.PI);
            expect(atanxy(1, -1)).toBeAlmost(-0.25 * Math.PI);
        });
        it("should throw the #DIV/0! error, if both numbers are 0", () => {
            expect(() => atanxy(0, 0)).toThrowValue(ErrorCode.DIV0);
        });
    });

    describe("function acoth", () => {
        const { acoth } = mathutils;
        it("should exist", () => {
            expect(acoth).toBeFunction();
        });
        it("should return the argument resulting in the hyperbolic tangent", () => {
            expect(acoth(-3)).toBeAlmost(log(1 / 2) / 2);
            expect(acoth(-2)).toBeAlmost(log(1 / 3) / 2);
            expect(acoth(-1)).not.toBeFinite();
            expect(acoth(0)).not.toBeFinite();
            expect(acoth(1)).not.toBeFinite();
            expect(acoth(2)).toBeAlmost(log(3) / 2);
            expect(acoth(3)).toBeAlmost(log(2) / 2);
        });
        it("should use the native implementation", () => {
            if (Math.acoth) { expect(acoth).toBe(Math.acoth); }
        });
    });

    describe("function sum", () => {
        const { sum } = mathutils;
        it("should exist", () => {
            expect(sum).toBeFunction();
        });
        it("should return the sum of the passed array", () => {
            expect(sum([])).toBe(0);
            expect(sum([1])).toBe(1);
            expect(sum([1, 2])).toBe(3);
            expect(sum([1, 2, 3])).toBe(6);
            expect(sum([1, 2, 3, 4])).toBe(10);
        });
    });

    describe("function product", () => {
        const { product } = mathutils;
        it("should exist", () => {
            expect(product).toBeFunction();
        });
        it("should return the product of the passed array", () => {
            expect(product([])).toBe(1);
            expect(product([1])).toBe(1);
            expect(product([1, 2])).toBe(2);
            expect(product([1, 2, 3])).toBe(6);
            expect(product([1, 2, 3, 4])).toBe(24);
        });
    });

    describe("function factorial", () => {
        const { factorial } = mathutils;
        it("should exist", () => {
            expect(factorial).toBeFunction();
        });
        it("should return the factorials of numbers", () => {
            expect(factorial(0)).toBe(1);
            expect(factorial(1)).toBe(1);
            expect(factorial(2)).toBe(2);
            expect(factorial(3)).toBe(6);
            expect(factorial(4)).toBe(24);
            expect(factorial(5)).toBe(120);
            expect(factorial(6)).toBe(720);
            expect(factorial(7)).toBe(5040);
        });
        it("should round the input parameter", () => {
            expect(factorial(4.5)).toBe(24);
        });
        it("should throw #NUM! error code for invalid numbers", () => {
            expect(() => factorial(-1)).toThrowValue(ErrorCode.NUM);
            expect(() => factorial(1000)).toThrowValue(ErrorCode.NUM);
        });
    });

    describe("function factorial2", () => {
        const { factorial2 } = mathutils;
        it("should exist", () => {
            expect(factorial2).toBeFunction();
        });
        it("should return the factorials of numbers", () => {
            expect(factorial2(0)).toBe(1);
            expect(factorial2(1)).toBe(1);
            expect(factorial2(2)).toBe(2);
            expect(factorial2(3)).toBe(3);
            expect(factorial2(4)).toBe(8);
            expect(factorial2(5)).toBe(15);
            expect(factorial2(6)).toBe(48);
            expect(factorial2(7)).toBe(105);
        });
        it("should round the input parameter", () => {
            expect(factorial2(4.5)).toBe(8);
        });
        it("should throw #NUM! error code for invalid numbers", () => {
            expect(() => factorial2(-1)).toThrowValue(ErrorCode.NUM);
            expect(() => factorial2(1000)).toThrowValue(ErrorCode.NUM);
        });
    });

    describe("function binomial", () => {
        const { binomial } = mathutils;
        it("should exist", () => {
            expect(binomial).toBeFunction();
        });
        it("should return the binomial coefficient", () => {
            expect(binomial(0, 0)).toBe(1);
            expect(binomial(1, 0)).toBe(1);
            expect(binomial(2, 0)).toBe(1);
            expect(binomial(3, 0)).toBe(1);
            expect(binomial(6, 0)).toBe(1);
            expect(binomial(6, 1)).toBe(6);
            expect(binomial(6, 2)).toBe(15);
            expect(binomial(6, 3)).toBe(20);
            expect(binomial(6, 4)).toBe(15);
            expect(binomial(6, 5)).toBe(6);
            expect(binomial(6, 6)).toBe(1);
            expect(binomial(20, 0)).toBe(1);
            expect(binomial(20, 1)).toBe(20);
            expect(binomial(20, 5)).toBe(15504);
            expect(binomial(20, 10)).toBe(184756);
            expect(binomial(20, 15)).toBe(15504);
            expect(binomial(20, 19)).toBe(20);
            expect(binomial(20, 20)).toBe(1);
            expect(binomial(1028, 514)).toBeGreaterThan(7.1e307);
            expect(binomial(1083, 400)).toBeGreaterThan(1.4e308);
        });
        it("should throw #NUM! error code for invalid parameters", () => {
            expect(() => binomial(-1, 0)).toThrowValue(ErrorCode.NUM);
            expect(() => binomial(0, -1)).toThrowValue(ErrorCode.NUM);
            expect(() => binomial(2, 3)).toThrowValue(ErrorCode.NUM);
            expect(() => binomial(1200, 600)).toThrowValue(ErrorCode.NUM);
        });
    });

    describe("function erf", () => {
        const { erf } = mathutils;
        it("should exist", () => {
            expect(erf).toBeFunction();
        });
        it("should return the result of the Gauss error function", () => {
            expect(erf(0.0)).toBe(0);
            expect(erf(0.5)).toBeCloseTo(0.520499877813046, 14);
            expect(erf(1.0)).toBeCloseTo(0.842700792949714, 14);
            expect(erf(1.5)).toBeCloseTo(0.96610514647531, 14);
            expect(erf(2.0)).toBeCloseTo(0.995322265019265, 14);
            expect(erf(2.5)).toBeCloseTo(0.999593047982555, 14);
            expect(erf(3.0)).toBeCloseTo(0.999977909503001, 14);
            expect(erf(3.5)).toBeCloseTo(0.999999256901627, 14);
            expect(erf(4.0)).toBeCloseTo(0.999999984582742, 14);
            expect(erf(4.5)).toBeCloseTo(0.999999999803383, 14);
            expect(erf(5.0)).toBeCloseTo(0.999999999998462, 14);
            expect(erf(5.5)).toBeCloseTo(0.9999999999999927, 14);
            expect(erf(6.0)).toBe(1);
        });
        it("should be an odd function", () => {
            expect(erf(-1)).toBe(-erf(1));
            expect(erf(-2)).toBe(-erf(2));
            expect(erf(-3)).toBe(-erf(3));
            expect(erf(-4)).toBe(-erf(4));
            expect(erf(-5)).toBe(-erf(5));
            expect(erf(-6)).toBe(-erf(6));
        });
    });

    describe("function erfc", () => {
        const { erf, erfc } = mathutils;
        it("should exist", () => {
            expect(erfc).toBeFunction();
        });
        it("should return the result of the complementary Gauss error function", () => {
            expect(erfc(0.0)).toBe(1);
            expect(erfc(0.5)).toBeCloseTo(1 - erf(0.5), 14);
            expect(erfc(1.0)).toBeCloseTo(1 - erf(1.0), 14);
            expect(erfc(1.5)).toBeCloseTo(1 - erf(1.5), 14);
            expect(erfc(2.0)).toBeCloseTo(1 - erf(2.0), 14);
            expect(erfc(2.5)).toBeCloseTo(1 - erf(2.5), 14);
            expect(erfc(3.0)).toBeCloseTo(1 - erf(3.0), 14);
            expect(erfc(3.5)).toBeCloseTo(1 - erf(3.5), 14);
            expect(erfc(4.0)).toBeCloseTo(1 - erf(4.0), 14);
            expect(erfc(4.5)).toBeCloseTo(1 - erf(4.5), 14);
            expect(erfc(5.0)).toBeCloseTo(1 - erf(5.0), 14);
            expect(erfc(5.5)).toBeCloseTo(1 - erf(5.5), 14);
            expect(erfc(6.0)).toBeCloseTo(1 - erf(6.0), 14);
        });
        it("should return non-zero results for large input parameters", () => {
            expect(erfc(6)).toBeGreaterThan(0); // erf(6) returns exactly 1; but erfc(6) must not return 0
            expect(erfc(7)).toBeGreaterThan(0);
            expect(erfc(8)).toBeGreaterThan(0);
            expect(erfc(9)).toBeGreaterThan(0);
        });
        it("should return correct results for negative numbers", () => {
            expect(erfc(-1)).toBe(1 - erf(-1));
            expect(erfc(-2)).toBe(1 - erf(-2));
            expect(erfc(-3)).toBe(1 - erf(-3));
            expect(erfc(-4)).toBe(1 - erf(-4));
            expect(erfc(-5)).toBe(1 - erf(-5));
            expect(erfc(-6)).toBe(1 - erf(-6));
        });
    });

    describe("function slope", () => {
        const { slope } = mathutils;
        it("should exist", () => {
            expect(slope).toBeFunction();
        });
        it("should return slope parameters", () => {
            expect(slope([3, 5])).toEqual({ sum: 8, mean: 4, slope: 2 });
        });
    });

    describe("function devSq", () => {
        const { devSq } = mathutils;
        it("should exist", () => {
            expect(devSq).toBeFunction();
        });
    });
});
