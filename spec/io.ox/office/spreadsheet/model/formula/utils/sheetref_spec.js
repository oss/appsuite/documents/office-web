/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as sheetref from "@/io.ox/office/spreadsheet/model/formula/utils/sheetref";

import { MockDocumentAccess } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/utils/sheetref", () => {

    const docAccess = new MockDocumentAccess({
        cols: 256,
        rows: 65536,
        sheets: ["Sheet1", "Sheet 2", "Sheet'3"]
    });

    // public functions -------------------------------------------------------

    describe("function encodeComplexSheetName", () => {
        const { encodeComplexSheetName } = sheetref;
        it("should exist", () => {
            expect(encodeComplexSheetName).toBeFunction();
        });
        it("should encode the passed sheet names", () => {
            expect(encodeComplexSheetName("Sheet'1")).toBe("'Sheet''1'");
        });
    });

    // class DocRef -----------------------------------------------------------

    const { DocRef } = sheetref;
    describe("class DocRef", () => {
        it("should exist", () => {
            expect(DocRef).toBeClass();
        });

        describe("constructor", () => {
            it("should create a document reference", () => {
                const r1 = new DocRef(0);
                expect(r1).toHaveProperty("index", 0);
                const r2 = new DocRef(1);
                expect(r2).toHaveProperty("index", 1);
            });
        });

        describe("method isExtDoc", () => {
            it("should exist", () => {
                expect(DocRef).toHaveMethod("isExtDoc");
            });
            it("should return true for external document", () => {
                expect(new DocRef(0).isExtDoc()).toBeFalse();
                expect(new DocRef(1).isExtDoc()).toBeTrue();
            });
        });
    });

    // class SheetRef ---------------------------------------------------------

    describe("class SheetRef", () => {

        const { SheetRef } = sheetref;
        it("should exist", () => {
            expect(SheetRef).toBeClass();
        });

        describe("constructor", () => {
            it("should create a sheet reference", () => {
                const r1 = new SheetRef(1, true);
                expect(r1).toHaveProperty("sheet", 1);
                expect(r1).toHaveProperty("abs", true);
                const r2 = new SheetRef("Sheet0", false);
                expect(r2).toHaveProperty("sheet", "Sheet0");
                expect(r2).toHaveProperty("abs", false);
            });
        });

        describe("method toString", () => {
            it("should exist", () => {
                expect(SheetRef).toHaveMethod("toString");
            });
            it("should return an internal string representation", () => {
                expect(new SheetRef(1, true).toString()).toBe("$1");
                expect(new SheetRef(2, false).toString()).toBe("2");
                expect(new SheetRef(-1, true).toString()).toBe("#REF");
                expect(new SheetRef(-1, false).toString()).toBe("#REF");
                expect(new SheetRef("Sheet1", true).toString()).toBe("$'Sheet1'");
                expect(new SheetRef("Sheet'2", false).toString()).toBe("'Sheet''2'");
            });
        });

        describe("method clone", () => {
            it("should exist", () => {
                expect(SheetRef).toHaveMethod("clone");
            });
            it("should create a clone", () => {
                const r1 = new SheetRef(2, true), r2 = r1.clone();
                expect(r2).toBeInstanceOf(SheetRef);
                expect(r2).not.toBe(r1);
                expect(r2).toStringifyTo("$2");
            });
        });

        describe("method valid", () => {
            it("should exist", () => {
                expect(SheetRef).toHaveMethod("valid");
            });
            it("should return true for valid sheet references", () => {
                expect(new SheetRef(0, true).valid()).toBeTrue();
                expect(new SheetRef(2, false).valid()).toBeTrue();
            });
            it("should return false for invalid sheet references", () => {
                expect(new SheetRef(-1, true).valid()).toBeFalse();
                expect(new SheetRef("Sheet0", false).valid()).toBeFalse();
            });
        });

        describe("method equals", () => {
            it("should exist", () => {
                expect(SheetRef).toHaveMethod("equals");
            });
            it("should return true for equal sheet references", () => {
                expect(new SheetRef(0, true).equals(new SheetRef(0, true))).toBeTrue();
                expect(new SheetRef("Sheet0", false).equals(new SheetRef("Sheet0", false))).toBeTrue();
            });
            it("should return false for different sheet references", () => {
                expect(new SheetRef(1, true).equals(new SheetRef(0, true))).toBeFalse();
                expect(new SheetRef(1, true).equals(new SheetRef(1, false))).toBeFalse();
                expect(new SheetRef(1, true).equals(new SheetRef("Sheet1", true))).toBeFalse();
                expect(new SheetRef("Sheet0", true).equals(new SheetRef("Sheet1", true))).toBeFalse();
                expect(new SheetRef("Sheet0", true).equals(new SheetRef("Sheet0", false))).toBeFalse();
            });
        });

        describe("method resolve", () => {
            it("should exist", () => {
                expect(SheetRef).toHaveMethod("resolve");
            });
            it("should resolve explicit sheet names", () => {
                expect(new SheetRef("Sheet", true).resolve(docAccess)).toBe("Sheet");
                expect(new SheetRef("Sheet", true).resolve(docAccess, new DocRef(0))).toBe("Sheet");
            });
            it("should resolve existing sheet names", () => {
                expect(new SheetRef(0, true).resolve(docAccess)).toBe("Sheet1");
                expect(new SheetRef(1, true).resolve(docAccess)).toBe("Sheet 2");
                expect(new SheetRef(2, true).resolve(docAccess)).toBe("Sheet'3");
                expect(new SheetRef(2, true).resolve(docAccess, new DocRef(0))).toBe("Sheet'3");
            });
            it("should fail for invalid sheet index", () => {
                expect(new SheetRef(99, true).resolve(docAccess)).toBeNull();
            });
            it("should handle external documents", () => {
                expect(new SheetRef("Sheet", true).resolve(docAccess, new DocRef(1))).toBe("Sheet");
                expect(new SheetRef(0, true).resolve(docAccess, new DocRef(1))).toBeNull();
            });
        });

        describe("method transform", () => {
            it("should exist", () => {
                expect(SheetRef).toHaveMethod("transform");
            });
            it("should adjust sheet index when inserting a sheet", () => {
                const r1 = new SheetRef(2, true);
                expect(r1.transform([0, 1, 2, 4])).toBeFalse();
                expect(r1).toStringifyTo("$2");
                const r2 = new SheetRef(2, true);
                expect(r2.transform([0, 1, 3, 4])).toBeTrue();
                expect(r2).toStringifyTo("$3");
                const r3 = new SheetRef(2, true);
                expect(r3.transform([0, 2, 3, 4])).toBeTrue();
                expect(r3).toStringifyTo("$3");
            });
            it("should adjust sheet index when deleting a sheet", () => {
                const r1 = new SheetRef(2, true);
                expect(r1.transform([0, 1, 2, null])).toBeFalse();
                expect(r1).toStringifyTo("$2");
                const r2 = new SheetRef(2, true);
                expect(r2.transform([0, 1, null, 2])).toBeTrue();
                expect(r2).toStringifyTo("#REF");
                const r3 = new SheetRef(2, true);
                expect(r3.transform([0, null, 1, 2])).toBeTrue();
                expect(r3).toStringifyTo("$1");
            });
            it("should adjust sheet index when moving a sheet", () => {
                const r1 = new SheetRef(2, true);
                expect(r1.transform([0, 2, 1, 3, 4])).toBeTrue();
                expect(r1).toStringifyTo("$1");
                const r2 = new SheetRef(2, true);
                expect(r2.transform([0, 3, 2, 1, 4])).toBeFalse();
                expect(r2).toStringifyTo("$2");
            });
            it("should not do anything for invalid sheet references", () => {
                expect(new SheetRef(-1, true).transform([1, 0])).toBeFalse();
                expect(new SheetRef("Sheet1", true).transform([1, 0])).toBeFalse();
            });
        });
    });

    // class SheetRefs --------------------------------------------------------

    describe("class SheetRefs", () => {

        const { SheetRefs } = sheetref;
        it("should exist", () => {
            expect(SheetRefs).toBeClass();
        });
    });
});
