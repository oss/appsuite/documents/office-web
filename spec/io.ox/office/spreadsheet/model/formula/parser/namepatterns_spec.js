/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { NameRefPatterns } from "@/io.ox/office/spreadsheet/model/formula/parser/namerefpatterns";

// tests ======================================================================

describe("module spreadsheet/model/formula/parser/tablepatterns", () => {

    let factory = null; // resource factory for OOXML

    let opPatterns = null; // patterns for "op"
    let dePatterns = null; // patterns for "ui:a1"
    let enPatterns = null; // patterns for "en:a1"

    beforeAll(async () => {
        factory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
    });

    // class NameRefPatterns --------------------------------------------------

    describe("class NameRefPatterns", () => {

        it("should exist", () => {
            expect(NameRefPatterns).toBeClass();
        });

        describe("static function create", () => {
            it("should exist", () => {
                expect(NameRefPatterns).toHaveStaticMethod("create");
            });
            it("should create instances", () => {
                opPatterns = NameRefPatterns.create(factory.getResource("std"), false);
                expect(opPatterns).toBeInstanceOf(NameRefPatterns);
                dePatterns = NameRefPatterns.create(factory.getResource("std"), true);
                expect(dePatterns).toBeInstanceOf(NameRefPatterns);
                enPatterns = NameRefPatterns.create(factory.getResource("en"), true);
                expect(enPatterns).toBeInstanceOf(NameRefPatterns);
            });
        });

        describe("method getTableRegionKey", () => {
            it("should exist", () => {
                expect(NameRefPatterns).toHaveMethod("getTableRegionKey");
            });
            it("should parse native region names", () => {
                expect(opPatterns.getTableRegionKey("#all")).toBe("ALL");
                expect(opPatterns.getTableRegionKey("#All")).toBe("ALL");
                expect(opPatterns.getTableRegionKey("#ALL")).toBe("ALL");
                expect(opPatterns.getTableRegionKey("#Headers")).toBe("HEADERS");
                expect(opPatterns.getTableRegionKey("#Data")).toBe("DATA");
                expect(opPatterns.getTableRegionKey("#Totals")).toBe("TOTALS");
                expect(opPatterns.getTableRegionKey("#This Row")).toBe("ROW");
            });
            it("should parse localized region names", () => {
                expect(dePatterns.getTableRegionKey("#alle")).toBe("ALL");
                expect(dePatterns.getTableRegionKey("#Alle")).toBe("ALL");
                expect(dePatterns.getTableRegionKey("#ALLE")).toBe("ALL");
                expect(dePatterns.getTableRegionKey("#Kopfzeilen")).toBe("HEADERS");
                expect(dePatterns.getTableRegionKey("#Daten")).toBe("DATA");
                expect(dePatterns.getTableRegionKey("#Ergebnisse")).toBe("TOTALS");
                expect(dePatterns.getTableRegionKey("#Diese Zeile")).toBe("ROW");
            });
            it("should parse English region names", () => {
                expect(enPatterns.getTableRegionKey("#all")).toBe("ALL");
                expect(enPatterns.getTableRegionKey("#All")).toBe("ALL");
                expect(enPatterns.getTableRegionKey("#ALL")).toBe("ALL");
                expect(enPatterns.getTableRegionKey("#Headers")).toBe("HEADERS");
                expect(enPatterns.getTableRegionKey("#Data")).toBe("DATA");
                expect(enPatterns.getTableRegionKey("#Totals")).toBe("TOTALS");
                expect(enPatterns.getTableRegionKey("#This Row")).toBe("ROW");
            });
            it("should return null for invalid names", () => {
                expect(opPatterns.getTableRegionKey("All")).toBeNull();
                expect(opPatterns.getTableRegionKey("#Alle")).toBeNull();
                expect(opPatterns.getTableRegionKey("#ThisRow")).toBeNull();
                expect(dePatterns.getTableRegionKey("Alle")).toBeNull();
                expect(dePatterns.getTableRegionKey("#All")).toBeNull();
                expect(dePatterns.getTableRegionKey("#DieseZeile")).toBeNull();
                expect(enPatterns.getTableRegionKey("All")).toBeNull();
                expect(enPatterns.getTableRegionKey("#Alle")).toBeNull();
                expect(enPatterns.getTableRegionKey("#ThisRow")).toBeNull();
            });
        });

        describe("method getTableRegionName", () => {
            it("should exist", () => {
                expect(NameRefPatterns).toHaveMethod("getTableRegionName");
            });
            it("should return native region names", () => {
                expect(opPatterns.getTableRegionName("ALL")).toBe("#All");
                expect(opPatterns.getTableRegionName("HEADERS")).toBe("#Headers");
                expect(opPatterns.getTableRegionName("DATA")).toBe("#Data");
                expect(opPatterns.getTableRegionName("TOTALS")).toBe("#Totals");
                expect(opPatterns.getTableRegionName("ROW")).toBe("#This Row");
            });
            it("should return localized region names", () => {
                expect(dePatterns.getTableRegionName("ALL")).toBe("#Alle");
                expect(dePatterns.getTableRegionName("HEADERS")).toBe("#Kopfzeilen");
                expect(dePatterns.getTableRegionName("DATA")).toBe("#Daten");
                expect(dePatterns.getTableRegionName("TOTALS")).toBe("#Ergebnisse");
                expect(dePatterns.getTableRegionName("ROW")).toBe("#Diese Zeile");
            });
            it("should return English region names", () => {
                expect(enPatterns.getTableRegionName("ALL")).toBe("#All");
                expect(enPatterns.getTableRegionName("HEADERS")).toBe("#Headers");
                expect(enPatterns.getTableRegionName("DATA")).toBe("#Data");
                expect(enPatterns.getTableRegionName("TOTALS")).toBe("#Totals");
                expect(enPatterns.getTableRegionName("ROW")).toBe("#This Row");
            });
            it("should return null for invalid keys", () => {
                expect(opPatterns.getTableRegionName("all")).toBeNull();
                expect(opPatterns.getTableRegionName("#All")).toBeNull();
                expect(opPatterns.getTableRegionName(" ")).toBeNull();
            });
        });

        describe("method isSimpleTableColumn", () => {
            it("should exist", () => {
                expect(NameRefPatterns).toHaveMethod("isSimpleTableColumn");
            });
            it("should return true for simple table columns", () => {
                expect(opPatterns.isSimpleTableColumn("Col1")).toBeTrue();
                expect(dePatterns.isSimpleTableColumn("Col1")).toBeTrue();
                expect(opPatterns.isSimpleTableColumn("Col\xA11")).toBeTrue();
                expect(dePatterns.isSimpleTableColumn("Col\xA11")).toBeTrue();
            });
            it("should return false for complex table columns", () => {
                expect(opPatterns.isSimpleTableColumn("Col 1")).toBeFalse();
                expect(dePatterns.isSimpleTableColumn("Col 1")).toBeFalse();
                expect(opPatterns.isSimpleTableColumn("Col!1")).toBeFalse();
                expect(dePatterns.isSimpleTableColumn("Col!1")).toBeFalse();
                expect(opPatterns.isSimpleTableColumn("Col_1")).toBeFalse();
                expect(dePatterns.isSimpleTableColumn("Col_1")).toBeFalse();
            });
            it("should return result for comma character according to grammar", () => {
                expect(opPatterns.isSimpleTableColumn("Col,1")).toBeFalse();
                expect(dePatterns.isSimpleTableColumn("Col,1")).toBeTrue();
                expect(enPatterns.isSimpleTableColumn("Col,1")).toBeFalse();
            });
        });

        describe("method encodeTableColumn", () => {
            it("should exist", () => {
                expect(NameRefPatterns).toHaveMethod("encodeTableColumn");
            });
            it("should return encoded table columns", () => {
                expect(opPatterns.encodeTableColumn("Col1")).toBe("Col1");
                expect(dePatterns.encodeTableColumn("Col1")).toBe("Col1");
                expect(opPatterns.encodeTableColumn(" !:*+{")).toBe(" !:*+{");
                expect(dePatterns.encodeTableColumn(" !:*+{")).toBe(" !:*+{");
                expect(opPatterns.encodeTableColumn("[]'#@")).toBe("'[']'''#@");
                expect(dePatterns.encodeTableColumn("[]'#@")).toBe("'[']'''#'@");
            });
        });

        describe("method decodeTableColumn", () => {
            it("should exist", () => {
                expect(NameRefPatterns).toHaveMethod("decodeTableColumn");
            });
            it("should return encoded table columns", () => {
                expect(opPatterns.decodeTableColumn("Col'''#'1")).toBe("Col'#1");
                expect(dePatterns.decodeTableColumn("Col'''#'1")).toBe("Col'#1");
            });
        });
    });
});
