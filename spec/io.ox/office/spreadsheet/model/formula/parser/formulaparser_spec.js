/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { ScalarToken, FixedToken, OperatorToken } from "@/io.ox/office/spreadsheet/model/formula/tokens";
import { FormulaParser } from "@/io.ox/office/spreadsheet/model/formula/parser/formulaparser";

import { a, r, MockDocumentAccess } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/parser/formulaparser", () => {

    // mock of an OOXML document model
    const ooxDocAccess = new MockDocumentAccess({
        cols: 16384,
        rows: 1048576,
        sheets: ["Sheet1", "Sheet2", "Sheet3", "Sheet 4"],
        names: [
            { label: "name", sheet: null },
            { label: "name", sheet: 1 }
        ],
        tables: [
            { name: "tbl", sheet: 0, range: r("A1:C3"), header: true, footer: true, columns: ["COL1", "COL2", "COL3"] }
        ]
    });

    // mock of an ODF document model
    const odfDocAccess = new MockDocumentAccess({
        cols: 1024,
        rows: 1048576,
        sheets: ["Sheet1", "Sheet2", "Sheet3", "Sheet 4"],
        names: [
            { label: "name", sheet: null }
        ],
        tables: [
            { name: "tbl", sheet: 0, range: r("A1:C3"), header: true, footer: true, columns: ["COL1", "COL2", "COL3"] }
        ]
    });

    // all parser instances, mapped by a unique key
    const parserMap = new Map();
    beforeAll(async () => {
        const ooxFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        const ooxResource = ooxFactory.getResource("std");
        const odfFactory = await FormulaResourceFactory.create("odf", LOCALE_DATA);
        const odfResource = odfFactory.getResource("std");
        parserMap.set("oox:op", FormulaParser.create(FormulaGrammar.create(ooxResource)));
        parserMap.set("oox:ui", FormulaParser.create(FormulaGrammar.create(ooxResource, { localized: true })));
        parserMap.set("odf:op", FormulaParser.create(FormulaGrammar.create(odfResource)));
        parserMap.set("odf:ui", FormulaParser.create(FormulaGrammar.create(odfResource, { localized: true })));
    });

    const REF = a("A1");

    // class FormulaParser ----------------------------------------------------

    describe("class FormulaParser", () => {
        it("should exist", () => {
            expect(FormulaParser).toBeClass();
        });

        describe("static function create", () => {
            it("should exist", () => {
                expect(FormulaParser).toHaveStaticMethod("create");
            });
        });

        describe("method parse", () => {
            it("should exist", () => {
                expect(FormulaParser).toHaveMethod("parse");
            });

            function expectParsed(grammarKey, docAccess, formula, options, expected) {
                if (is.string(options)) { expected = options; options = undefined; }
                const result = parserMap.get(grammarKey).parse(docAccess, formula, REF, options);
                const descr = `formula "${formula}"`;
                expect(result, descr).toBeArray();
                expect(result.map(e => e.token).join(" "), descr).toBe(expected);
            }

            const expectOoxOp = expectParsed.bind(null, "oox:op", ooxDocAccess);
            const expectOoxUi = expectParsed.bind(null, "oox:ui", ooxDocAccess);
            const expectOdfOp = expectParsed.bind(null, "odf:op", odfDocAccess);
            const expectOdfUi = expectParsed.bind(null, "odf:ui", odfDocAccess);

            it("should return an array of token descriptors", () => {
                const result = parserMap.get("oox:op").parse(ooxDocAccess, "42+012.50 *-.01", REF);
                expect(result).toBeArrayOfSize(6);
                expect(result[0]).toBeObject();
                expect(result[0].token).toBeInstanceOf(ScalarToken);
                expect(result[0].token.value).toBe(42);
                expect(result[0]).toHaveProperty("index", 0);
                expect(result[0]).toHaveProperty("text", "42");
                expect(result[0]).toHaveProperty("start", 0);
                expect(result[0]).toHaveProperty("end", 2);
                expect(result[1].token).toBeInstanceOf(OperatorToken);
                expect(result[1].token.value).toBe("add");
                expect(result[1]).toHaveProperty("index", 1);
                expect(result[1]).toHaveProperty("text", "+");
                expect(result[1]).toHaveProperty("start", 2);
                expect(result[1]).toHaveProperty("end", 3);
                expect(result[2].token).toBeInstanceOf(ScalarToken);
                expect(result[2].token.value).toBe(12.5);
                expect(result[2]).toHaveProperty("index", 2);
                expect(result[2]).toHaveProperty("text", "012.50");
                expect(result[2]).toHaveProperty("start", 3);
                expect(result[2]).toHaveProperty("end", 9);
                expect(result[3].token).toBeInstanceOf(FixedToken);
                expect(result[3].token.type).toBe("ws");
                expect(result[3].token.value).toBe(" ");
                expect(result[3]).toHaveProperty("index", 3);
                expect(result[3]).toHaveProperty("text", " ");
                expect(result[3]).toHaveProperty("start", 9);
                expect(result[3]).toHaveProperty("end", 10);
                expect(result[4].token).toBeInstanceOf(OperatorToken);
                expect(result[4].token.value).toBe("mul");
                expect(result[4]).toHaveProperty("index", 4);
                expect(result[4]).toHaveProperty("text", "*");
                expect(result[4]).toHaveProperty("start", 10);
                expect(result[4]).toHaveProperty("end", 11);
                expect(result[5].token).toBeInstanceOf(ScalarToken);
                expect(result[5].token.value).toBe(-0.01);
                expect(result[5]).toHaveProperty("index", 5);
                expect(result[5]).toHaveProperty("text", "-.01");
                expect(result[5]).toHaveProperty("start", 11);
                expect(result[5]).toHaveProperty("end", 15);
            });

            it("should parse number literals and numeric operators", () => {
                // native OOXML
                expectOoxOp("1+2%%", "lit[1] op[add] lit[2] op[upct] op[upct]");
                expectOoxOp("0042-4200", "lit[42] op[sub] lit[4200]");
                expectOoxOp(".010*010.", "lit[0.01] op[mul] lit[10]");
                expectOoxOp("077.880/0.0", "lit[77.88] op[div] lit[0]");
                expectOoxOp("1e30^001.100E-0030^0e0", "lit[1e+30] op[pow] lit[1.1e-30] op[pow] lit[0]");
                expectOoxOp(".01e+30+10.E-30-10.01e30", "lit[1e+28] op[add] lit[1e-29] op[sub] lit[1.001e+31]");
                expectOoxOp("1 2/4+0 0/2+02 03/04", "lit[1.5] op[add] lit[0] op[add] lit[2.75]");
                // localized OOXML
                expectOoxUi("1+2%%", "lit[1] op[add] lit[2] op[upct] op[upct]");
                expectOoxUi("0042-4200", "lit[42] op[sub] lit[4200]");
                expectOoxUi(",010*010,", "lit[0.01] op[mul] lit[10]");
                expectOoxUi("077,880/0,0", "lit[77.88] op[div] lit[0]");
                expectOoxUi("1e30^001,100E-0030^0e0", "lit[1e+30] op[pow] lit[1.1e-30] op[pow] lit[0]");
                expectOoxUi(",01e+30+10,E-30-10,01e30", "lit[1e+28] op[add] lit[1e-29] op[sub] lit[1.001e+31]");
                expectOoxUi("1 2/4+0 0/2+02 03/04", "lit[1.5] op[add] lit[0] op[add] lit[2.75]");
                // native ODF
                expectOdfOp("1+2%%", "lit[1] op[add] lit[2] op[upct] op[upct]");
                expectOdfOp("0042-4200", "lit[42] op[sub] lit[4200]");
                expectOdfOp(".010*010.", "lit[0.01] op[mul] lit[10]");
                expectOdfOp("077.880/0.0", "lit[77.88] op[div] lit[0]");
                expectOdfOp("1e30^001.100E-0030^0e0", "lit[1e+30] op[pow] lit[1.1e-30] op[pow] lit[0]");
                expectOdfOp(".01e+30+10.E-30-10.01e30", "lit[1e+28] op[add] lit[1e-29] op[sub] lit[1.001e+31]");
                expectOdfOp("1 2/4+0 0/2+02 03/04", "lit[1.5] op[add] lit[0] op[add] lit[2.75]");
                // localized ODF
                expectOdfUi("1+2%%", "lit[1] op[add] lit[2] op[upct] op[upct]");
                expectOdfUi("0042-4200", "lit[42] op[sub] lit[4200]");
                expectOdfUi(",010*010,", "lit[0.01] op[mul] lit[10]");
                expectOdfUi("077,880/0,0", "lit[77.88] op[div] lit[0]");
                expectOdfUi("1e30^001,100E-0030^0e0", "lit[1e+30] op[pow] lit[1.1e-30] op[pow] lit[0]");
                expectOdfUi(",01e+30+10,E-30-10,01e30", "lit[1e+28] op[add] lit[1e-29] op[sub] lit[1.001e+31]");
                expectOdfUi("1 2/4+0 0/2+02 03/04", "lit[1.5] op[add] lit[0] op[add] lit[2.75]");
            });

            it("should parse boolean literals and comparison operators", () => {
                // native OOXML
                expectOoxOp("true<false>True<>False", "lit[TRUE] op[lt] lit[FALSE] op[gt] lit[TRUE] op[ne] lit[FALSE]");
                expectOoxOp("tRUE<=fALSE>=TRUE=FALSE", "lit[TRUE] op[le] lit[FALSE] op[ge] lit[TRUE] op[eq] lit[FALSE]");
                // localized OOXML
                expectOoxUi("wahr<falsch>Wahr<>Falsch", "lit[TRUE] op[lt] lit[FALSE] op[gt] lit[TRUE] op[ne] lit[FALSE]");
                expectOoxUi("wAHR<=fALSCH>=WAHR=FALSCH", "lit[TRUE] op[le] lit[FALSE] op[ge] lit[TRUE] op[eq] lit[FALSE]");
                // native ODF
                expectOoxOp("true<false>True<>False", "lit[TRUE] op[lt] lit[FALSE] op[gt] lit[TRUE] op[ne] lit[FALSE]");
                expectOoxOp("tRUE<=fALSE>=TRUE=FALSE", "lit[TRUE] op[le] lit[FALSE] op[ge] lit[TRUE] op[eq] lit[FALSE]");
                // localized ODF
                expectOoxUi("wahr<falsch>Wahr<>Falsch", "lit[TRUE] op[lt] lit[FALSE] op[gt] lit[TRUE] op[ne] lit[FALSE]");
                expectOoxUi("wAHR<=fALSCH>=WAHR=FALSCH", "lit[TRUE] op[le] lit[FALSE] op[ge] lit[TRUE] op[eq] lit[FALSE]");
            });

            it("should parse string literals and string operators", () => {
                // native OOXML
                expectOoxOp('"abc"&"D""E""F"&"&""&"&""', 'lit["abc"] op[con] lit["D""E""F"] op[con] lit["&""&"] op[con] lit[""]');
                // localized OOXML
                expectOoxUi('"abc"&"D""E""F"&"&""&"&""', 'lit["abc"] op[con] lit["D""E""F"] op[con] lit["&""&"] op[con] lit[""]');
                // native ODF
                expectOdfOp('"abc"&"D""E""F"&"&""&"&""', 'lit["abc"] op[con] lit["D""E""F"] op[con] lit["&""&"] op[con] lit[""]');
                // localized ODF
                expectOdfUi('"abc"&"D""E""F"&"&""&"&""', 'lit["abc"] op[con] lit["D""E""F"] op[con] lit["&""&"] op[con] lit[""]');
            });

            it("should parse error code literals", () => {
                // native OOXML
                expectOoxOp("#NULL!#DIV/0!#ref!#NAME?", "lit[#NULL] lit[#DIV0] lit[#REF] lit[#NAME]");
                expectOoxOp("#VALUE!#NUM!#num!#N/A", "lit[#VALUE] lit[#NUM] lit[#NUM] lit[#NA]");
                // localized OOXML
                expectOoxUi("#NULL!#DIV/0!#bezug!#NAME?", "lit[#NULL] lit[#DIV0] lit[#REF] lit[#NAME]");
                expectOoxUi("#WERT!#ZAHL!#zahl!#NV", "lit[#VALUE] lit[#NUM] lit[#NUM] lit[#NA]");
                // native ODF
                expectOdfOp("#NULL!#DIV/0!#ref!#NAME?", "lit[#NULL] lit[#DIV0] lit[#REF] lit[#NAME]");
                expectOdfOp("#VALUE!#NUM!#num!#N/A", "lit[#VALUE] lit[#NUM] lit[#NUM] lit[#NA]");
                // localized ODF
                expectOdfUi("#NULL!#DIV/0!#bezug!#NAME?", "lit[#NULL] lit[#DIV0] lit[#REF] lit[#NAME]");
                expectOdfUi("#WERT!#ZAHL!#zahl!#NV", "lit[#VALUE] lit[#NUM] lit[#NUM] lit[#NA]");
            });

            it("should parse matrix literals", () => {
                // native OOXML
                expectOoxOp("{1,0042;+1,-0042}", "mat[{1;42|1;-42}]");
                expectOoxOp("{.010,-010.;077.880,+0.0}", "mat[{0.01;-10|77.88;0}]");
                expectOoxOp("{1e30,+001.100E-0030;-0e0,-.01e+30}", "mat[{1e+30;1.1e-30|0;-1e+28}]");
                expectOoxOp('{true,fAlSe;"abc","D""E";","";""|",""}', 'mat[{TRUE;FALSE|"abc";"D""E"|","";""|";""}]');
                expectOoxOp("{#VALUE!,#num!}", "mat[{#VALUE;#NUM}]");
                expectOoxOp("{ 1 , 2 ; 3 , 4 }", "mat[{1;2|3;4}]");
                expectOoxOp("{1,2;3,4", "bad[{1,2;3,4]");
                expectOoxOp("{1,2;3,4", { autoCorrect: true }, "mat[{1;2|3;4}]");
                // localized OOXML
                expectOoxUi("{1;0042|+1;-0042}", "mat[{1;42|1;-42}]");
                expectOoxUi("{,010;-010,|077,880;+0,0}", "mat[{0.01;-10|77.88;0}]");
                expectOoxUi("{1e30;+001,100E-0030|-0e0;-,01e+30}", "mat[{1e+30;1.1e-30|0;-1e+28}]");
                expectOoxUi("{1 2/5;+01 02/05|-1 2/5;- 1 2/5}", "mat[{1.4;1.4|-1.4;-1.4}]");
                expectOoxUi('{wahr;fAlScH|"abc";"D""E"|","";""|";""}', 'mat[{TRUE;FALSE|"abc";"D""E"|","";""|";""}]');
                expectOoxUi("{#WERT!;#zahl!}", "mat[{#VALUE;#NUM}]");
                expectOoxUi("{ 1 ; 2 | 3 ; 4 }", "mat[{1;2|3;4}]");
                expectOoxUi("{1;2|3;4", "bad[{1;2|3;4]");
                expectOoxUi("{1;2|3;4", { autoCorrect: true }, "mat[{1;2|3;4}]");
                // native ODF
                expectOdfOp("{1;0042|+1;-0042}", "mat[{1;42|1;-42}]");
                expectOdfOp("{.010;-010.|077.880;+0.0}", "mat[{0.01;-10|77.88;0}]");
                expectOdfOp("{1e30;+001.100E-0030|-0e0;-.01e+30}", "mat[{1e+30;1.1e-30|0;-1e+28}]");
                expectOdfOp('{true;fAlSe|"abc";"D""E"|","";""|";""}', 'mat[{TRUE;FALSE|"abc";"D""E"|","";""|";""}]');
                expectOdfOp("{#VALUE!;#num!}", "mat[{#VALUE;#NUM}]");
                expectOdfOp("{ 1 ; 2 | 3 ; 4 }", "mat[{1;2|3;4}]");
                expectOdfOp("{1;2|3;4", "bad[{1;2|3;4]");
                expectOdfOp("{1;2|3;4", { autoCorrect: true }, "mat[{1;2|3;4}]");
                // localized ODF
                expectOdfUi("{1;0042|+1;-0042}", "mat[{1;42|1;-42}]");
                expectOdfUi("{,010;-010,|077,880;+0,0}", "mat[{0.01;-10|77.88;0}]");
                expectOdfUi("{1e30;+001,100E-0030|-0e0;-,01e+30}", "mat[{1e+30;1.1e-30|0;-1e+28}]");
                expectOdfUi("{1 2/5;+01 02/05|-1 2/5;- 1 2/5}", "mat[{1.4;1.4|-1.4;-1.4}]");
                expectOdfUi('{wahr;fAlScH|"abc";"D""E"|","";""|";""}', 'mat[{TRUE;FALSE|"abc";"D""E"|","";""|";""}]');
                expectOdfUi("{#WERT!;#zahl!}", "mat[{#VALUE;#NUM}]");
                expectOdfUi("{ 1 ; 2 | 3 ; 4 }", "mat[{1;2|3;4}]");
                expectOdfUi("{1;2|3;4", "bad[{1;2|3;4]");
                expectOdfUi("{1;2|3;4", { autoCorrect: true }, "mat[{1;2|3;4}]");
            });

            it("should parse local cell references and separators", () => {
                // native OOXML
                expectOoxOp("A1,xfd001048576", "ref[A1] sep ref[XFD1048576]");
                expectOoxOp("B3,B$3 $B3\n  $B$3", "ref[B3] sep ref[B$3] op[isect] ref[$B3] ws[\n] op[isect] ws[ ] ref[$B$3]");
                // localized OOXML
                expectOoxUi("A1;xfd001048576", "ref[A1] sep ref[XFD1048576]");
                expectOoxUi("B3;B$3;$B3 $B$3", "ref[B3] sep ref[B$3] sep ref[$B3] op[isect] ref[$B$3]");
                // native ODF
                expectOdfOp("[.A1];[.amj001048576]", "ref[A1] sep ref[AMJ1048576]");
                expectOdfOp("[.B3];[.B$3]~[.$B3]![.$B$3]", "ref[B3] sep ref[B$3] op[list] ref[$B3] op[isect] ref[$B$3]");
                expectOdfOp("[.B#REF!];[.#REF!$3];[.$B3:.$#REF!$3]", "ref[#REF] sep ref[#REF] sep ref[#REF]");
                // localized ODF
                expectOdfUi("A1;amj001048576", "ref[A1] sep ref[AMJ1048576]");
                expectOdfUi("B3;B$3;$B3 $B$3", "ref[B3] sep ref[B$3] sep ref[$B3] op[isect] ref[$B$3]");
            });

            it("should parse local range references and range operators", () => {
                // native OOXML
                expectOoxOp("B3:D7:B7:D3:D3:B7:D7:B3", "ref[B3:D7] op[range] ref[B3:D7] op[range] ref[B3:D7] op[range] ref[B3:D7]");
                expectOoxOp("B$3:$D7:d7:$b$3:$B$3:$D$7", "ref[B$3:$D7] op[range] ref[$B$3:D7] op[range] ref[$B$3:$D$7]");
                expectOoxOp("B3:B3:$B3:B3", "ref[B3:B3] op[range] ref[$B3:B3]");
                expectOoxOp("B3:B3:B3,B3", "ref[B3:B3] op[range] ref[B3] sep ref[B3]");
                // localized OOXML
                expectOoxUi("B3:D7:B7:D3:D3:B7:D7:B3", "ref[B3:D7] op[range] ref[B3:D7] op[range] ref[B3:D7] op[range] ref[B3:D7]");
                expectOoxUi("B$3:$D7:d7:$b$3:$B$3:$D$7", "ref[B$3:$D7] op[range] ref[$B$3:D7] op[range] ref[$B$3:$D$7]");
                expectOoxUi("B3:B3:$B3:B3", "ref[B3:B3] op[range] ref[$B3:B3]");
                expectOoxUi("B3:B3:B3;B3", "ref[B3:B3] op[range] ref[B3] sep ref[B3]");
                // native ODF
                expectOdfOp("[.B3:.D7]:[.B7:.D3]:[.D3:.B7]:[.D7:.B3]", "ref[B3:D7] op[range] ref[B3:D7] op[range] ref[B3:D7] op[range] ref[B3:D7]");
                expectOdfOp("[.B$3:.$D7]:[.d7:.$b$3]:[.$B$3:.$D$7]", "ref[B$3:$D7] op[range] ref[$B$3:D7] op[range] ref[$B$3:$D$7]");
                expectOdfOp("[.B3:.B3]:[.$B3:.B3]", "ref[B3:B3] op[range] ref[$B3:B3]");
                expectOdfOp("[.B3:.B3]:[.B3];[.B3]", "ref[B3:B3] op[range] ref[B3] sep ref[B3]");
                // localized ODF
                expectOdfUi("B3:D7:B7:D3:D3:B7:D7:B3", "ref[B3:D7] op[range] ref[B3:D7] op[range] ref[B3:D7] op[range] ref[B3:D7]");
                expectOdfUi("B$3:$D7:d7:$b$3:$B$3:$D$7", "ref[B$3:$D7] op[range] ref[$B$3:D7] op[range] ref[$B$3:$D$7]");
                expectOdfUi("B3:B3:$B3:B3", "ref[B3:B3] op[range] ref[$B3:B3]");
                expectOdfUi("B3:B3:B3;B3", "ref[B3:B3] op[range] ref[B3] sep ref[B3]");
            });

            it("should parse local column/row references", () => {
                // native OOXML
                expectOoxOp("A:xfd:A:$B:$A:B:$b:$a", "ref[A$1:XFD$1048576] op[range] ref[A$1:$B$1048576] op[range] ref[$A$1:B$1048576] op[range] ref[$A$1:$B$1048576]");
                expectOoxOp("1:001048576:1:$2:$1:2:$002:$001", "ref[$A1:$XFD1048576] op[range] ref[$A1:$XFD$2] op[range] ref[$A$1:$XFD2] op[range] ref[$A$1:$XFD$2]");
                // localized OOXML
                expectOoxUi("A:xfd:A:$B:$A:B:$b:$a", "ref[A$1:XFD$1048576] op[range] ref[A$1:$B$1048576] op[range] ref[$A$1:B$1048576] op[range] ref[$A$1:$B$1048576]");
                expectOoxUi("1:001048576:1:$2:$1:2:$002:$001", "ref[$A1:$XFD1048576] op[range] ref[$A1:$XFD$2] op[range] ref[$A$1:$XFD2] op[range] ref[$A$1:$XFD$2]");
                // native ODF
                // ... not available
                // localized ODF
                expectOdfUi("A:amj:A:$B:$A:B:$b:$a", "ref[A$1:AMJ$1048576] op[range] ref[A$1:$B$1048576] op[range] ref[$A$1:B$1048576] op[range] ref[$A$1:$B$1048576]");
                expectOdfUi("1:001048576:1:$2:$1:2:$002:$001", "ref[$A1:$AMJ1048576] op[range] ref[$A1:$AMJ$2] op[range] ref[$A$1:$AMJ2] op[range] ref[$A$1:$AMJ$2]");
            });

            it("should parse cell and range references with sheets", () => {
                // native OOXML
                expectOoxOp("A1:Sheet1!A1:'Sheet 4'!xfd001048576", "ref[A1] op[range] ref[$0!A1] op[range] ref[$3!XFD1048576]");
                expectOoxOp("A1:Sheet1:Sheet3!A$1:$C2:'Sheet 4:Sheet1'!$a$1:$c$2", "ref[A1] op[range] ref[$0:$2!A$1:$C2] op[range] ref[$0:$3!$A$1:$C$2]");
                expectOoxOp("A1:A1!A1:Sheet1:A1!A1:A1:Sheet1:A1!A1", "ref[A1] op[range] ref[$'A1'!A1] op[range] ref[$0:$'A1'!A1:A1] op[range] ref[$0:$'A1'!A1]");
                expectOoxOp("TRUE!A1:WAHR!A1", "ref[$'TRUE'!A1] op[range] ref[$'WAHR'!A1]");
                expectOoxOp("[0]Sheet1!A1:'[0]Sheet 4'!A1:B2:'[0]Sheet1:Sheet 4'!A1:B2", "ref[[0]$0!A1] op[range] ref[[0]$3!A1:B2] op[range] ref[[0]$0:$3!A1:B2]");
                expectOoxOp("[1]Sheet1!A1:'[1]Sheet 4'!A1:B2:'[1]Sheet1:Sheet 4'!A1:B2", "ref[[1]$'Sheet1'!A1] op[range] ref[[1]$'Sheet 4'!A1:B2] op[range] ref[[1]$'Sheet1':$'Sheet 4'!A1:B2]");
                // localized OOXML
                expectOoxUi("A1:Sheet1!A1:'Sheet 4'!xfd001048576", "ref[A1] op[range] ref[$0!A1] op[range] ref[$3!XFD1048576]");
                expectOoxUi("A1:Sheet1:Sheet3!A$1:$C2:'Sheet 4:Sheet1'!$a$1:$c$2", "ref[A1] op[range] ref[$0:$2!A$1:$C2] op[range] ref[$0:$3!$A$1:$C$2]");
                expectOoxUi("A1:A1!A1:Sheet1:A1!A1:A1:Sheet1:A1!A1", "ref[A1] op[range] ref[$'A1'!A1] op[range] ref[$0:$'A1'!A1:A1] op[range] ref[$0:$'A1'!A1]");
                expectOoxUi("TRUE!A1:WAHR!A1", "ref[$'TRUE'!A1] op[range] ref[$'WAHR'!A1]");
                expectOoxUi("[0]Sheet1!A1:'[0]Sheet 4'!A1:B2:'[0]Sheet1:Sheet 4'!A1:B2", "ref[[0]$0!A1] op[range] ref[[0]$3!A1:B2] op[range] ref[[0]$0:$3!A1:B2]");
                expectOoxUi("[1]Sheet1!A1", "bad[[1]Sheet1!A1]");
                // native ODF
                expectOdfOp("[.A1]:[Sheet1.A1]:[$'Sheet 4'.amj001048576]", "ref[A1] op[range] ref[0!A1] op[range] ref[$3!AMJ1048576]");
                expectOdfOp("[.A1]:[$Sheet1.A$1:Sheet3.$C2]:['Sheet 4'.$a$1:$Sheet1.$c$2]", "ref[A1] op[range] ref[$0:2!A$1:$C2] op[range] ref[$0:3!$A$1:$C$2]");
                expectOdfOp("[.A1]:[A1.A1]:[Sheet1.A1:A1.A1]:[Sheet1.A1:A1.A1]", "ref[A1] op[range] ref['A1'!A1] op[range] ref[0:'A1'!A1:A1] op[range] ref[0:'A1'!A1:A1]");
                expectOdfOp("[TRUE.A1]:[WAHR.A1]", "ref['TRUE'!A1] op[range] ref['WAHR'!A1]");
                // localized ODF
                expectOdfUi("A1:Sheet1!A1:$'Sheet 4'!amj001048576", "ref[A1] op[range] ref[0!A1] op[range] ref[$3!AMJ1048576]");
                expectOdfUi("A1:$Sheet1:Sheet3!A$1:$C2:'Sheet 4':$Sheet1!$a$1:$c$2", "ref[A1] op[range] ref[$0:2!A$1:$C2] op[range] ref[$0:3!$A$1:$C$2]");
                expectOdfUi("A1:A1!A1:Sheet1:A1!A1:A1:Sheet1:A1!A1", "ref[A1] op[range] ref['A1'!A1] op[range] ref[0:'A1'!A1:A1] op[range] ref[0:'A1'!A1]");
                expectOdfUi("TRUE!A1:WAHR!A1", "ref['TRUE'!A1] op[range] ref['WAHR'!A1]");
                expectOdfUi("[0]Sheet1!A1", "bad[[0]Sheet1!A1]");
            });

            it("should parse column/row references with sheets", () => {
                // native OOXML
                expectOoxOp("A:XFD:Sheet1!A:$B:'Sheet 4:Sheet1'!$b:$a", "ref[A$1:XFD$1048576] op[range] ref[$0!A$1:$B$1048576] op[range] ref[$0:$3!$A$1:$B$1048576]");
                expectOoxOp("1:001048576+1:$2+$1:2+$002:$001", "ref[$A1:$XFD1048576] op[add] ref[$A1:$XFD$2] op[add] ref[$A$1:$XFD2] op[add] ref[$A$1:$XFD$2]");
                // localized OOXML
                expectOoxUi("A:XFD:Sheet1!A:$B:'Sheet 4:Sheet1'!$b:$a", "ref[A$1:XFD$1048576] op[range] ref[$0!A$1:$B$1048576] op[range] ref[$0:$3!$A$1:$B$1048576]");
                expectOoxUi("1:001048576+1:$2+$1:2+$002:$001", "ref[$A1:$XFD1048576] op[add] ref[$A1:$XFD$2] op[add] ref[$A$1:$XFD2] op[add] ref[$A$1:$XFD$2]");
                // native ODF
                // ... not available
                // localized ODF
                expectOdfUi("A:AMJ:Sheet1!A:$B:$'Sheet 4':Sheet1!$b:$a", "ref[A$1:AMJ$1048576] op[range] ref[0!A$1:$B$1048576] op[range] ref[0:$3!$A$1:$B$1048576]");
                expectOdfUi("1:001048576+1:$2+$1:2+$002:$001", "ref[$A1:$AMJ1048576] op[add] ref[$A1:$AMJ$2] op[add] ref[$A$1:$AMJ2] op[add] ref[$A$1:$AMJ$2]");
            });

            it("should not accept oversized cell addresses", () => {
                // native OOXML
                expectOoxOp("A1048757+XFE1+Sheet1!A1:XFE1", "name[A1048757] op[add] name[XFE1] op[add] ref[$0!A1] op[range] name[XFE1]");
                // localized OOXML
                expectOoxUi("A1048757+XFE1+Sheet1!A1:XFE1", "name[A1048757] op[add] name[XFE1] op[add] ref[$0!A1] op[range] name[XFE1]");
                // native ODF
                expectOdfOp("[.A1048757]+[.AMK1]+[Sheet1.A1:.XFE1]", "ref[#REF] op[add] ref[#REF] op[add] ref[0!#REF]");
                // localized ODF
                expectOdfUi("A1048757+AMK1+Sheet1!A1:AMK1", "name[A1048757] op[add] name[AMK1] op[add] ref[0!A1] op[range] name[AMK1]");
            });

            it("should parse function names", () => {
                // native OOXML
                expectOoxOp("true()+wahr()+my_func()", "func[TRUE] open close op[add] macro[wahr] open close op[add] macro[my_func] open close");
                expectOoxOp("Sheet1!true()+Sheet1!wahr()", "macro[$0!true] open close op[add] macro[$0!wahr] open close");
                expectOoxOp("Sheet1:Sheet2!my_func()+Sheet1!A1()", "name[Sheet1] op[range] macro[$1!my_func] open close op[add] macro[$0!A1] open close");
                expectOoxOp("_xlfn.FORMULATEXT()+FORMULATEXT()+FORMULA()", "func[FORMULATEXT] open close op[add] macro[FORMULATEXT] open close op[add] macro[FORMULA] open close");
                expectOoxOp("FORMELTEXT()+FORMEL()", "macro[FORMELTEXT] open close op[add] macro[FORMEL] open close");
                expectOoxOp("SUM (A1)", "name[SUM] op[isect] open ref[A1] close");
                expectOoxOp("SUMME()+_xludf.SUM()", "macro[SUMME] open close op[add] macro[_xludf.SUM] open close");
                // localized OOXML
                expectOoxUi("true()+wahr()+my_func()", "macro[true] open close op[add] func[TRUE] open close op[add] macro[my_func] open close");
                expectOoxUi("Sheet1!true()+Sheet1!wahr()", "macro[$0!true] open close op[add] macro[$0!wahr] open close");
                expectOoxUi("Sheet1:Sheet2!my_func()+Sheet1!A1()", "name[Sheet1] op[range] macro[$1!my_func] open close op[add] macro[$0!A1] open close");
                expectOoxUi("_xlfn.FORMULATEXT()+FORMULATEXT()+FORMULA()", "macro[_xlfn.FORMULATEXT] open close op[add] macro[FORMULATEXT] open close op[add] macro[FORMULA] open close");
                expectOoxUi("FORMELTEXT()+FORMEL()", "func[FORMULATEXT] open close op[add] macro[FORMEL] open close");
                expectOoxUi("SUMME (A1)", "name[SUMME] op[isect] open ref[A1] close");
                // native ODF
                expectOdfOp("true()+wahr()+my_func()", "func[TRUE] open close op[add] macro[wahr] open close op[add] macro[my_func] open close");
                expectOdfOp("_xlfn.FORMULATEXT()+FORMULATEXT()+FORMULA()", "macro[_xlfn.FORMULATEXT] open close op[add] macro[FORMULATEXT] open close op[add] func[FORMULATEXT] open close");
                expectOdfOp("FORMELTEXT()+FORMEL()", "macro[FORMELTEXT] open close op[add] macro[FORMEL] open close");
                expectOdfOp("SUM ([.A1])", "func[SUM] open ref[A1] close");
                // localized ODF
                expectOdfUi("true()+wahr()+my_func()", "macro[true] open close op[add] func[TRUE] open close op[add] macro[my_func] open close");
                expectOdfUi("_xlfn.FORMULATEXT()+FORMULATEXT()+FORMULA()", "macro[_xlfn.FORMULATEXT] open close op[add] macro[FORMULATEXT] open close op[add] macro[FORMULA] open close");
                expectOdfUi("FORMELTEXT()+FORMEL()", "macro[FORMELTEXT] open close op[add] func[FORMULATEXT] open close");
                expectOdfUi("SUMME (A1)", "name[SUMME] op[isect] open ref[A1] close");
            });

            it("should parse defined names", () => {
                // native OOXML
                expectOoxOp("true1+wahr1+a1a", "name[true1] op[add] name[wahr1] op[add] name[a1a]");
                expectOoxOp("Sheet1!name1+'Sheet 4'!name2+Sheet1:Sheet2!name3", "name[$0!name1] op[add] name[$3!name2] op[add] name[Sheet1] op[range] name[$1!name3]");
                expectOoxOp("[0]!name1+[0]Sheet1!name2+[1]!name3+[1]Sheet2!name4", "name[[0]!name1] op[add] name[[0]$0!name2] op[add] name[[1]!name3] op[add] name[[1]$'Sheet2'!name4]");
                // localized OOXML
                expectOoxUi("true1+wahr1+a1a", "name[true1] op[add] name[wahr1] op[add] name[a1a]");
                expectOoxUi("Sheet1!name1+'Sheet 4'!name2+Sheet1:Sheet2!name3", "name[$0!name1] op[add] name[$3!name2] op[add] name[Sheet1] op[range] name[$1!name3]");
                expectOoxUi("[0]!name1+[0]Sheet1!name2+[1]!name3", "name[[0]!name1] op[add] name[[0]$0!name2] op[add] bad[[1]!name3]");
                // native ODF
                expectOdfOp("true1+wahr1+a1a", "name[true1] op[add] name[wahr1] op[add] name[a1a]");
                expectOdfOp("Sheet1.name1+'Sheet 4'.name2+Sheet1:Sheet2.name3", "name[0!name1] op[add] name[3!name2] op[add] name[Sheet1] op[range] name[1!name3]");
                expectOdfOp("$Sheet1.name1+$'Sheet 4'.name2", "name[$0!name1] op[add] name[$3!name2]");
                // localized ODF
                expectOdfUi("true1+wahr1+a1a", "name[true1] op[add] name[wahr1] op[add] name[a1a]");
                expectOdfUi("Sheet1!name1+$Sheet1!name2", "name[0!name1] op[add] name[$0!name2]");
                expectOdfUi("[0]!name1", "bad[[0]!name1]");
            });

            it("should parse table references", () => {
                // native OOXML
                expectOoxOp("tbl[]+tbl", "table[tbl] op[add] table[tbl]");
                expectOoxOp("tbl[#data]+tbl[#all]+tbl[#this row]+tbl[@]", "table[tbl#DATA] op[add] table[tbl#ALL] op[add] table[tbl#ROW] op[add] table[tbl[@]]");
                expectOoxOp("tbl[Col1]+tbl[Col''1]+tbl[Col 1]+tbl[Col'#1]", "table[tbl[Col1]] op[add] table[tbl[Col'1]] op[add] table[tbl[Col 1]] op[add] table[tbl[Col#1]]");
                expectOoxOp("tbl[Col1:Col2]+tbl[[Col1]:[Col2]]+tbl[Col1:[Col2]]", "table[tbl[Col1:Col2]] op[add] table[tbl[Col1]:[Col2]] op[add] bad[tbl[Col1:[Col2]]]");
                expectOoxOp("tbl[[#data],[Col1]]+tbl[[#data],[Col1]:[Col2]]+tbl[[Col1]:[Col2],[#data],[#totals]]", "table[tbl#DATA[Col1]] op[add] table[tbl#DATA[Col1]:[Col2]] op[add] table[tbl#DATA#TOTALS[Col1]:[Col2]]");
                expectOoxOp("[0]!tbl[#data]+[1]!tbl[Col1]", "table[[0]!tbl#DATA] op[add] table[[1]!tbl[Col1]]");
                expectOoxOp("tbl[", "bad[tbl[]");
                expectOoxOp("tbl[", { autoCorrect: true }, "table[tbl]");
                // localized OOXML
                expectOoxUi("tbl[]+tbl", "table[tbl] op[add] table[tbl]");
                expectOoxUi("tbl[#daten]+tbl[#alle]+tbl[#diese zeile]+tbl[@]", "table[tbl#DATA] op[add] table[tbl#ALL] op[add] table[tbl#ROW] op[add] table[tbl#ROW]");
                expectOoxUi("tbl[Col1]+tbl[Col''1]+tbl[Col 1]+tbl[Col'#1]", "table[tbl[Col1]] op[add] table[tbl[Col'1]] op[add] table[tbl[Col 1]] op[add] table[tbl[Col#1]]");
                expectOoxUi("tbl[Col1:Col2]+tbl[[Col1]:[Col2]]+tbl[Col1:[Col2]]", "table[tbl[Col1:Col2]] op[add] table[tbl[Col1]:[Col2]] op[add] table[tbl[Col1]:[Col2]]");
                expectOoxUi("tbl[[#daten];Col1]+tbl[[#daten];Col1:Col2]+tbl[[Col1]:Col2;[#daten];[#ergebnisse]]", "table[tbl#DATA[Col1]] op[add] table[tbl#DATA[Col1]:[Col2]] op[add] table[tbl#DATA#TOTALS[Col1]:[Col2]]");
                expectOoxUi("tbl[@Col1]+tbl[@Col1:Col2]+tbl[@[Col1]:Col2]", "table[tbl#ROW[Col1]] op[add] table[tbl#ROW[Col1]:[Col2]] op[add] table[tbl#ROW[Col1]:[Col2]]");
                expectOoxUi("[0]!tbl[#daten]+[1]!tbl[Col1]", "table[[0]!tbl#DATA] op[add] bad[[1]!tbl[Col1]]");
                expectOoxUi("tbl[", "bad[tbl[]");
                expectOoxUi("tbl[", { autoCorrect: true }, "table[tbl]");
                // native ODF
                expectOdfOp("tbl", "table[tbl]");
                expectOdfOp("tbl[", "table[tbl] bad[[]");
                expectOdfOp("tbl[]", "table[tbl] bad[[]]");
                // localized ODF
                expectOdfUi("tbl", "table[tbl]");
                expectOdfUi("tbl[", "table[tbl] bad[[]");
                expectOdfUi("tbl[]", "table[tbl] bad[[]]");
            });

            it("should recognize negative numbers after operators", () => {
                // native OOXML
                expectOoxOp("-1-1--1-(-1)-1,-1", "lit[-1] op[sub] lit[1] op[sub] lit[-1] op[sub] open lit[-1] close op[sub] lit[1] sep lit[-1]");
                // localized OOXML
                expectOoxUi("-1-1--1-(-1)-1;-1", "lit[-1] op[sub] lit[1] op[sub] lit[-1] op[sub] open lit[-1] close op[sub] lit[1] sep lit[-1]");
                // native ODF
                expectOdfOp("-1-1--1-(-1)-1;-1", "lit[-1] op[sub] lit[1] op[sub] lit[-1] op[sub] open lit[-1] close op[sub] lit[1] sep lit[-1]");
                // localized ODF
                expectOdfUi("-1-1--1-(-1)-1;-1", "lit[-1] op[sub] lit[1] op[sub] lit[-1] op[sub] open lit[-1] close op[sub] lit[1] sep lit[-1]");
            });

            it("should fail for invalid matrix literals", () => {
                // native OOXML
                expectOoxOp("{1,{2}}", "bad[{1,{2}}]");
                expectOoxOp("1+2}", "lit[1] op[add] lit[2] bad[}]");
                expectOoxOp("{1*1}", "bad[{1*1}]");
                expectOoxOp("{1,A1}", "bad[{1,A1}]");
                // localized OOXML
                expectOoxUi("{1;{2}}", "bad[{1;{2}}]");
                expectOoxUi("1+2}", "lit[1] op[add] lit[2] bad[}]");
                expectOoxUi("{1*1}", "bad[{1*1}]");
                expectOoxUi("{1;A1}", "bad[{1;A1}]");
                // native ODF
                expectOdfOp("{1;{2}}", "bad[{1;{2}}]");
                expectOdfOp("1+2}", "lit[1] op[add] lit[2] bad[}]");
                expectOdfOp("{1*1}", "bad[{1*1}]");
                expectOdfOp("{1;A1}", "bad[{1;A1}]");
                // localized ODF
                expectOdfUi("{1;{2}}", "bad[{1;{2}}]");
                expectOdfUi("1+2}", "lit[1] op[add] lit[2] bad[}]");
                expectOdfUi("{1*1}", "bad[{1*1}]");
                expectOdfUi("{1;A1}", "bad[{1;A1}]");
            });

            it("should fail for tokens of other grammars", () => {
                // native OOXML
                expectOoxOp("1.2+3,4", "lit[1.2] op[add] lit[3] sep lit[4]");
                expectOoxOp("True=Wahr=False=Falsch", "lit[TRUE] op[eq] name[Wahr] op[eq] lit[FALSE] op[eq] name[Falsch]");
                expectOoxOp("#Ref!<>#Bezug!+1", "lit[#REF] op[ne] bad[#Bezug!+1]");
                expectOoxOp("{1,2;3,4}+{1;2|3;4}", "mat[{1;2|3;4}] op[add] bad[{1;2|3;4}]");
                expectOoxOp("SUMME(1,2;3)", "macro[SUMME] open lit[1] sep lit[2] bad[;3)]");
                // localized OOXML
                expectOoxUi("1.2+3,4", "lit[1] bad[.2+3,4]");
                expectOoxUi("True=Wahr=False=Falsch", "name[True] op[eq] lit[TRUE] op[eq] name[False] op[eq] lit[FALSE]");
                expectOoxUi("{1,2;3,4}+{1;2|3;4}", "mat[{1.2;3.4}] op[add] mat[{1;2|3;4}]");
                expectOoxUi("SUMME(1,2;3)", "func[SUM] open lit[1.2] sep lit[3] close");
                // native ODF
                expectOdfOp("1.2+3,4", "lit[1.2] op[add] lit[3] bad[,4]");
                expectOdfOp("True=Wahr=False=Falsch", "lit[TRUE] op[eq] name[Wahr] op[eq] lit[FALSE] op[eq] name[Falsch]");
                expectOdfOp("#Ref!<>#Bezug!+1", "lit[#REF] op[ne] bad[#Bezug!+1]");
                expectOdfOp("{1,2;3,4}+{1;2|3;4}", "bad[{1,2;3,4}+{1;2|3;4}]");
                expectOdfOp("SUMME(1,2;3)", "macro[SUMME] open lit[1] bad[,2;3)]");
                // localized ODF
                expectOdfUi("1.2+3,4", "lit[1] bad[.2+3,4]");
                expectOdfUi("True=Wahr=False=Falsch", "name[True] op[eq] lit[TRUE] op[eq] name[False] op[eq] lit[FALSE]");
                expectOdfUi("{1,2;3,4}+{1;2|3;4}", "mat[{1.2;3.4}] op[add] mat[{1;2|3;4}]");
                expectOdfUi("SUMME(1,2;3)", "func[SUM] open lit[1.2] sep lit[3] close");
            });
        });
    });
});
