/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";

import { a, cref, crefs, srefs, ErrorCode, MockDocumentAccess } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/parser/formulagrammar", () => {

    // simple mock of a document model, used for sheet names, and maximum column/row checks
    const docAccess = new MockDocumentAccess({
        cols: 256,
        rows: 65536,
        sheets: ["Sheet1", "Sheet2", "Sheet3", "Sheet 4"]
    });

    let ooxStdResource = null; // formula resource for OOXML
    let ooxOoxResource = null; // formula resource for OOXML
    let ooxEnResource = null;  // formula resource for OOXML

    let opOoxA1Grammar = null; // formula grammar for OOXML, "op"
    let deOoxA1Grammar = null; // formula grammar for OOXML, "ui:a1"
    let deOoxRCGrammar = null; // formula grammar for OOXML, "ui:rc"
    let enOoxA1Grammar = null; // formula grammar for OOXML, "en:a1"
    let enOoxRCGrammar = null; // formula grammar for OOXML, "en:rc"

    let odfStdResource = null; // formula resource for ODF
    let odfOoxResource = null; // formula resource for ODF
    let odfEnResource = null;  // formula resource for ODF

    let opOdfA1Grammar = null; // formula grammar for ODF, "op"
    let deOdfA1Grammar = null; // formula grammar for ODF, "ui:a1"
    let deOdfRCGrammar = null; // formula grammar for ODF, "ui:rc"
    let enOdfA1Grammar = null; // formula grammar for ODF, "en:a1"
    let enOdfRCGrammar = null; // formula grammar for ODF, "en:rc"

    beforeAll(async () => {
        const ooxFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        ooxStdResource = ooxFactory.getResource("std");
        ooxOoxResource = ooxFactory.getResource("oox");
        ooxEnResource = ooxFactory.getResource("en");
        const odfFactory = await FormulaResourceFactory.create("odf", LOCALE_DATA);
        odfStdResource = odfFactory.getResource("std");
        odfOoxResource = odfFactory.getResource("oox");
        odfEnResource = odfFactory.getResource("en");
    });

    // class FormulaGrammar ---------------------------------------------------

    describe("class FormulaGrammar", () => {

        it("should exist", () => {
            expect(FormulaGrammar).toBeFunction();
        });

        describe("static function create", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveStaticMethod("create");
            });
            it("should return all grammars for OOXML", () => {
                // grammar OP
                opOoxA1Grammar = FormulaGrammar.create(ooxStdResource);
                expect(opOoxA1Grammar).toBeInstanceOf(FormulaGrammar);
                // grammar OOX
                expect(FormulaGrammar.create(ooxOoxResource)).toBe(opOoxA1Grammar);
                // grammar UI_A1
                deOoxA1Grammar = FormulaGrammar.create(ooxStdResource, { localized: true });
                expect(deOoxA1Grammar).toBeInstanceOf(FormulaGrammar);
                expect(deOoxA1Grammar).not.toBe(opOoxA1Grammar);
                // grammar UI_RC
                deOoxRCGrammar = FormulaGrammar.create(ooxStdResource, { localized: true, rcStyle: true });
                expect(deOoxRCGrammar).toBeInstanceOf(FormulaGrammar);
                expect(deOoxRCGrammar).not.toBe(opOoxA1Grammar);
                expect(deOoxRCGrammar).not.toBe(deOoxA1Grammar);
                // grammar EN_A1
                enOoxA1Grammar = FormulaGrammar.create(ooxEnResource, { localized: true });
                expect(enOoxA1Grammar).toBeInstanceOf(FormulaGrammar);
                expect(enOoxA1Grammar).not.toBe(opOoxA1Grammar);
                expect(enOoxA1Grammar).not.toBe(deOoxA1Grammar);
                expect(enOoxA1Grammar).not.toBe(deOoxRCGrammar);
                // grammar EN_RC
                enOoxRCGrammar = FormulaGrammar.create(ooxEnResource, { localized: true, rcStyle: true });
                expect(enOoxRCGrammar).toBeInstanceOf(FormulaGrammar);
                expect(enOoxRCGrammar).not.toBe(opOoxA1Grammar);
                expect(enOoxRCGrammar).not.toBe(deOoxA1Grammar);
                expect(enOoxRCGrammar).not.toBe(deOoxRCGrammar);
                expect(enOoxRCGrammar).not.toBe(enOoxA1Grammar);
            });
            it("should return all grammars for ODF", () => {
                // grammar OP
                opOdfA1Grammar = FormulaGrammar.create(odfStdResource);
                expect(opOdfA1Grammar).toBeInstanceOf(FormulaGrammar);
                expect(opOdfA1Grammar).not.toBe(opOoxA1Grammar);
                // grammar OOX
                const ooxGrammar = FormulaGrammar.create(odfOoxResource);
                expect(ooxGrammar).toBeInstanceOf(FormulaGrammar);
                expect(ooxGrammar).not.toBe(opOdfA1Grammar);
                expect(ooxGrammar).toHaveProperty("OF", false);
                expect(ooxGrammar).toHaveProperty("UI", false);
                // grammar UI_A1
                deOdfA1Grammar = FormulaGrammar.create(odfStdResource, { localized: true });
                expect(deOdfA1Grammar).toBeInstanceOf(FormulaGrammar);
                expect(deOdfA1Grammar).not.toBe(ooxGrammar);
                expect(deOdfA1Grammar).not.toBe(opOdfA1Grammar);
                // grammar UI_RC
                deOdfRCGrammar = FormulaGrammar.create(odfStdResource, { localized: true, rcStyle: true });
                expect(deOdfRCGrammar).toBeInstanceOf(FormulaGrammar);
                expect(deOdfRCGrammar).not.toBe(ooxGrammar);
                expect(deOdfRCGrammar).not.toBe(opOdfA1Grammar);
                expect(deOdfRCGrammar).not.toBe(deOdfA1Grammar);
                // grammar EN_A1
                enOdfA1Grammar = FormulaGrammar.create(odfEnResource, { localized: true });
                expect(enOdfA1Grammar).toBeInstanceOf(FormulaGrammar);
                expect(enOdfA1Grammar).not.toBe(ooxGrammar);
                expect(enOdfA1Grammar).not.toBe(opOdfA1Grammar);
                expect(enOdfA1Grammar).not.toBe(deOdfA1Grammar);
                expect(enOdfA1Grammar).not.toBe(deOdfRCGrammar);
                // grammar EN_RC
                enOdfRCGrammar = FormulaGrammar.create(odfEnResource, { localized: true, rcStyle: true });
                expect(enOdfRCGrammar).toBeInstanceOf(FormulaGrammar);
                expect(enOdfRCGrammar).not.toBe(ooxGrammar);
                expect(enOdfRCGrammar).not.toBe(opOdfA1Grammar);
                expect(enOdfRCGrammar).not.toBe(deOdfA1Grammar);
                expect(enOdfRCGrammar).not.toBe(deOdfRCGrammar);
                expect(enOdfRCGrammar).not.toBe(enOdfA1Grammar);
            });
        });

        describe("property UI", () => {
            it("should exist", () => {
                expect(opOoxA1Grammar.UI).toBeFalse();
                expect(deOoxA1Grammar.UI).toBeTrue();
                expect(deOoxRCGrammar.UI).toBeTrue();
                expect(enOoxA1Grammar.UI).toBeTrue();
                expect(enOoxRCGrammar.UI).toBeTrue();
                expect(opOdfA1Grammar.UI).toBeFalse();
                expect(deOdfA1Grammar.UI).toBeTrue();
                expect(deOdfRCGrammar.UI).toBeTrue();
                expect(enOdfA1Grammar.UI).toBeTrue();
                expect(enOdfRCGrammar.UI).toBeTrue();
            });
        });

        describe("property RC", () => {
            it("should exist", () => {
                expect(opOoxA1Grammar.RC).toBeFalse();
                expect(deOoxA1Grammar.RC).toBeFalse();
                expect(deOoxRCGrammar.RC).toBeTrue();
                expect(enOoxA1Grammar.RC).toBeFalse();
                expect(enOoxRCGrammar.RC).toBeTrue();
                expect(opOdfA1Grammar.RC).toBeFalse();
                expect(deOdfA1Grammar.RC).toBeFalse();
                expect(deOdfRCGrammar.RC).toBeTrue();
                expect(enOdfA1Grammar.RC).toBeFalse();
                expect(enOdfRCGrammar.RC).toBeTrue();
            });
        });

        describe("property DEC", () => {
            it("should exist", () => {
                expect(opOoxA1Grammar.DEC).toBe(".");
                expect(deOoxA1Grammar.DEC).toBe(",");
                expect(deOoxRCGrammar.DEC).toBe(",");
                expect(enOoxA1Grammar.DEC).toBe(".");
                expect(enOoxRCGrammar.DEC).toBe(".");
                expect(opOdfA1Grammar.DEC).toBe(".");
                expect(deOdfA1Grammar.DEC).toBe(",");
                expect(deOdfRCGrammar.DEC).toBe(",");
                expect(enOdfA1Grammar.DEC).toBe(".");
                expect(enOdfRCGrammar.DEC).toBe(".");
            });
        });

        describe("property SEP", () => {
            it("should exist", () => {
                expect(opOoxA1Grammar.SEP).toBe(",");
                expect(deOoxA1Grammar.SEP).toBe(";");
                expect(deOoxRCGrammar.SEP).toBe(";");
                expect(enOoxA1Grammar.SEP).toBe(",");
                expect(enOoxRCGrammar.SEP).toBe(",");
                expect(opOdfA1Grammar.SEP).toBe(";");
                expect(deOdfA1Grammar.SEP).toBe(";");
                expect(deOdfRCGrammar.SEP).toBe(";");
                expect(enOdfA1Grammar.SEP).toBe(",");
                expect(enOdfRCGrammar.SEP).toBe(",");
            });
        });

        describe("property MAT_ROW", () => {
            it("should exist", () => {
                expect(opOoxA1Grammar.MAT_ROW).toBe(";");
                expect(deOoxA1Grammar.MAT_ROW).toBe("|");
                expect(deOoxRCGrammar.MAT_ROW).toBe("|");
                expect(enOoxA1Grammar.MAT_ROW).toBe("|");
                expect(enOoxRCGrammar.MAT_ROW).toBe("|");
                expect(opOdfA1Grammar.MAT_ROW).toBe("|");
                expect(deOdfA1Grammar.MAT_ROW).toBe("|");
                expect(deOdfRCGrammar.MAT_ROW).toBe("|");
                expect(enOdfA1Grammar.MAT_ROW).toBe("|");
                expect(enOdfRCGrammar.MAT_ROW).toBe("|");
            });
        });

        describe("property MAT_COL", () => {
            it("should exist", () => {
                expect(opOoxA1Grammar.MAT_COL).toBe(",");
                expect(deOoxA1Grammar.MAT_COL).toBe(";");
                expect(deOoxRCGrammar.MAT_COL).toBe(";");
                expect(enOoxA1Grammar.MAT_COL).toBe(";");
                expect(enOoxRCGrammar.MAT_COL).toBe(";");
                expect(opOdfA1Grammar.MAT_COL).toBe(";");
                expect(deOdfA1Grammar.MAT_COL).toBe(";");
                expect(deOdfRCGrammar.MAT_COL).toBe(";");
                expect(enOdfA1Grammar.MAT_COL).toBe(";");
                expect(enOdfRCGrammar.MAT_COL).toBe(";");
            });
        });

        describe("property ISECT_SPACE", () => {
            it("should exist", () => {
                expect(opOoxA1Grammar.ISECT_SPACE).toBeTrue();
                expect(opOdfA1Grammar.ISECT_SPACE).toBeFalse();
                expect(deOoxA1Grammar.ISECT_SPACE).toBeTrue();
                expect(deOdfA1Grammar.ISECT_SPACE).toBeTrue();
            });
        });

        describe("method isReservedSymbol", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("isReservedSymbol");
            });
            describe("should recognize boolean literal", () => {
                it("FALSE", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "FALSE")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "FALSE")).toBeFalse();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "FALSE")).toBeTrue();
                });
                it("TRUE", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "TRUE")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "TRUE")).toBeFalse();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "TRUE")).toBeTrue();
                });
                it("FALSCH", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "FALSCH")).toBeFalse();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "FALSCH")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "FALSCH")).toBeFalse();
                });
                it("WAHR", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "WAHR")).toBeFalse();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "WAHR")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "WAHR")).toBeFalse();
                });
            });
            describe("should recgonize A1 reference", () => {
                it("A1", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "A1")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "A1")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "A1")).toBeTrue();
                });
                it("iv65536", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "iv65536")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "iv65536")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "iv65536")).toBeTrue();
                });
                it("IW65536", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "IW65536")).toBeFalse();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "IW65536")).toBeFalse();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "IW65536")).toBeFalse();
                });
                it("IV65537", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "IV65537")).toBeFalse();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "IV65537")).toBeFalse();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "IV65537")).toBeFalse();
                });
            });
            describe("should recgonize R1C1 reference", () => {
                it("R1C1", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "R1C1")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "R1C1")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "R1C1")).toBeTrue();
                });
                it("R65536C256", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "R65536C256")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "R65536C256")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "R65536C256")).toBeTrue();
                });
                it("R65537C256", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "R65537C256")).toBeFalse();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "R65537C256")).toBeFalse();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "R65537C256")).toBeFalse();
                });
                it("R65536C257", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "R65536C257")).toBeFalse();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "R65536C257")).toBeFalse();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "R65536C257")).toBeFalse();
                });
                it("R1C", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "R1C")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "R1C")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "R1C")).toBeTrue();
                });
                it("RC1", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "RC1")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "RC1")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "RC1")).toBeTrue();
                });
                it("R1", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "R1")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "R1")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "R1")).toBeTrue();
                });
                it("C1", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "C1")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "C1")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "C1")).toBeTrue();
                });
                it("R", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "R")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "R")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "R")).toBeTrue();
                });
                it("C", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "C")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "C")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "C")).toBeTrue();
                });
                it("RC", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "RC")).toBeTrue();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "RC")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "RC")).toBeTrue();
                });
                // localized R1C1 references (not used in ODF)
                it("Z1S1", () => {
                    expect(opOoxA1Grammar.isReservedSymbol(docAccess, "Z1S1")).toBeFalse();
                    expect(deOoxA1Grammar.isReservedSymbol(docAccess, "Z1S1")).toBeTrue();
                    expect(enOoxA1Grammar.isReservedSymbol(docAccess, "Z1S1")).toBeFalse();
                    expect(opOdfA1Grammar.isReservedSymbol(docAccess, "Z1S1")).toBeFalse();
                    expect(deOdfA1Grammar.isReservedSymbol(docAccess, "Z1S1")).toBeFalse();
                    expect(enOdfA1Grammar.isReservedSymbol(docAccess, "Z1S1")).toBeFalse();
                });
            });
            it("should return false for other strings", () => {
                expect(opOoxA1Grammar.isReservedSymbol(docAccess, "A")).toBeFalse();
                expect(deOoxA1Grammar.isReservedSymbol(docAccess, "A")).toBeFalse();
                expect(enOoxA1Grammar.isReservedSymbol(docAccess, "A")).toBeFalse();
                expect(opOoxA1Grammar.isReservedSymbol(docAccess, "1")).toBeFalse();
                expect(deOoxA1Grammar.isReservedSymbol(docAccess, "1")).toBeFalse();
                expect(enOoxA1Grammar.isReservedSymbol(docAccess, "1")).toBeFalse();
                expect(opOoxA1Grammar.isReservedSymbol(docAccess, " A1")).toBeFalse();
                expect(deOoxA1Grammar.isReservedSymbol(docAccess, " A1")).toBeFalse();
                expect(enOoxA1Grammar.isReservedSymbol(docAccess, " A1")).toBeFalse();
                expect(opOoxA1Grammar.isReservedSymbol(docAccess, "A 1")).toBeFalse();
                expect(deOoxA1Grammar.isReservedSymbol(docAccess, "A 1")).toBeFalse();
                expect(enOoxA1Grammar.isReservedSymbol(docAccess, "A 1")).toBeFalse();
                expect(opOoxA1Grammar.isReservedSymbol(docAccess, "R1 C1")).toBeFalse();
                expect(deOoxA1Grammar.isReservedSymbol(docAccess, "R1 C1")).toBeFalse();
                expect(enOoxA1Grammar.isReservedSymbol(docAccess, "R1 C1")).toBeFalse();
                expect(opOoxA1Grammar.isReservedSymbol(docAccess, "C1R1")).toBeFalse();
                expect(deOoxA1Grammar.isReservedSymbol(docAccess, "C1R1")).toBeFalse();
                expect(enOoxA1Grammar.isReservedSymbol(docAccess, "C1R1")).toBeFalse();
                expect(opOoxA1Grammar.isReservedSymbol(docAccess, "S1Z1")).toBeFalse();
                expect(deOoxA1Grammar.isReservedSymbol(docAccess, "S1Z1")).toBeFalse();
                expect(enOoxA1Grammar.isReservedSymbol(docAccess, "S1Z1")).toBeFalse();
                expect(opOoxA1Grammar.isReservedSymbol(docAccess, "")).toBeFalse();
                expect(deOoxA1Grammar.isReservedSymbol(docAccess, "")).toBeFalse();
                expect(enOoxA1Grammar.isReservedSymbol(docAccess, "")).toBeFalse();
                expect(opOoxA1Grammar.isReservedSymbol(docAccess, " TRUE")).toBeFalse();
                expect(deOoxA1Grammar.isReservedSymbol(docAccess, " WAHR")).toBeFalse();
                expect(enOoxA1Grammar.isReservedSymbol(docAccess, " WAHR")).toBeFalse();
            });
        });

        describe("method isSimpleSheetName", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("isSimpleSheetName");
            });
            describe("should recognize simple sheet names", () => {
                it("without extras", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1")).toBeTrue();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1")).toBeTrue();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet1")).toBeTrue();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet1")).toBeTrue();
                });
                it("with whitespace", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet 1")).toBeFalse();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet 1")).toBeFalse();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet 1")).toBeFalse();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet 1")).toBeFalse();
                });
                it("with replacement character", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet\ufffd1")).toBeTrue();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet\ufffd1")).toBeTrue();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet\ufffd1")).toBeTrue();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet\ufffd1")).toBeTrue();
                });
                it("with line separator", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet\u20281")).toBeFalse();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet\u20281")).toBeFalse();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet\u20281")).toBeFalse();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet\u20281")).toBeFalse();
                });
                it("with surrogate pair", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet\ud800\udc001")).toBeFalse();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet\ud800\udc001")).toBeFalse();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet\ud800\udc001")).toBeFalse();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet\ud800\udc001")).toBeFalse();
                });
                it("as sheet range", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Sheet2")).toBeFalse();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Sheet2")).toBeFalse();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Sheet2")).toBeFalse();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Sheet2")).toBeFalse();
                });
                it("as boolean literal", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "TRUE")).toBeFalse();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "TRUE")).toBeTrue();
                    expect(enOoxA1Grammar.isSimpleSheetName(docAccess, "TRUE")).toBeFalse();
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "WAHR")).toBeTrue();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "WAHR")).toBeFalse();
                    expect(enOoxA1Grammar.isSimpleSheetName(docAccess, "WAHR")).toBeTrue();
                });
                it("with leading digits", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "1Sheet")).toBeFalse();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "1Sheet")).toBeFalse();
                    expect(enOoxA1Grammar.isSimpleSheetName(docAccess, "1Sheet")).toBeFalse();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "1Sheet")).toBeTrue();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "1Sheet")).toBeTrue();
                    expect(enOdfA1Grammar.isSimpleSheetName(docAccess, "1Sheet")).toBeTrue();
                });
                it("with digits only", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "123")).toBeFalse();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "123")).toBeFalse();
                    expect(enOoxA1Grammar.isSimpleSheetName(docAccess, "123")).toBeFalse();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "123")).toBeFalse();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "123")).toBeFalse();
                    expect(enOdfA1Grammar.isSimpleSheetName(docAccess, "123")).toBeFalse();
                });
                it("as A1 reference", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "A1")).toBeFalse();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "A1")).toBeFalse();
                    expect(enOoxA1Grammar.isSimpleSheetName(docAccess, "A1")).toBeFalse();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "A1")).toBeFalse();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "A1")).toBeFalse();
                    expect(enOdfA1Grammar.isSimpleSheetName(docAccess, "A1")).toBeFalse();
                });
                it("as native R1C1 reference", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "R1C1")).toBeFalse();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "R1C1")).toBeFalse();
                    expect(enOoxA1Grammar.isSimpleSheetName(docAccess, "R1C1")).toBeFalse();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "R1C1")).toBeFalse();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "R1C1")).toBeFalse();
                    expect(enOdfA1Grammar.isSimpleSheetName(docAccess, "R1C1")).toBeFalse();
                });
                it("as localized R1C1 reference", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Z1S1")).toBeTrue();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Z1S1")).toBeFalse();
                    expect(enOoxA1Grammar.isSimpleSheetName(docAccess, "Z1S1")).toBeTrue();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "Z1S1")).toBeTrue();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "Z1S1")).toBeTrue();
                    expect(enOdfA1Grammar.isSimpleSheetName(docAccess, "Z1S1")).toBeTrue();
                });
                it("with external path", () => {
                    expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1")).toBeFalse();
                    expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1")).toBeFalse();
                    expect(opOdfA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1")).toBeFalse();
                    expect(deOdfA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1")).toBeFalse();
                });
            });
            it("should recognize simple sheet ranges", () => {
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1", { range: true })).toBeTrue();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1", { range: true })).toBeTrue();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet 1", { range: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet 1", { range: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Sheet2", { range: true })).toBeTrue();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Sheet2", { range: true })).toBeTrue();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Sheet 2", { range: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Sheet 2", { range: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:TRUE", { range: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:TRUE", { range: true })).toBeTrue();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:WAHR", { range: true })).toBeTrue();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:WAHR", { range: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:A1", { range: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:A1", { range: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:R1C1", { range: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:R1C1", { range: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Z1S1", { range: true })).toBeTrue();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Z1S1", { range: true })).toBeFalse();
            });
            it("should recognize simple sheet names with document name", () => {
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1", { external: true })).toBeTrue();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1", { external: true })).toBeTrue();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet 1", { external: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet 1", { external: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1", { external: true })).toBeTrue();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1", { external: true })).toBeTrue();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet 1", { external: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet 1", { external: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1:Sheet2", { external: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1:Sheet2", { external: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc 1.xlsx]Sheet1", { external: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc 1.xlsx]Sheet1", { external: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "[path/doc1.xlsx]Sheet1", { external: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "[path/doc1.xlsx]Sheet1", { external: true })).toBeFalse();
            });
            it("should recognize simple sheet ranges with document name", () => {
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1", { range: true, external: true })).toBeTrue();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1", { range: true, external: true })).toBeTrue();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet 1", { range: true, external: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet 1", { range: true, external: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Sheet2", { range: true, external: true })).toBeTrue();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "Sheet1:Sheet2", { range: true, external: true })).toBeTrue();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1", { range: true, external: true })).toBeTrue();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1", { range: true, external: true })).toBeTrue();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet 1", { range: true, external: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet 1", { range: true, external: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1:Sheet2", { range: true, external: true })).toBeTrue();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1:Sheet2", { range: true, external: true })).toBeTrue();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1:Sheet 2", { range: true, external: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc.xlsx]Sheet1:Sheet 2", { range: true, external: true })).toBeFalse();
                expect(opOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc 1.xlsx]Sheet1:Sheet2", { range: true, external: true })).toBeFalse();
                expect(deOoxA1Grammar.isSimpleSheetName(docAccess, "[path\\doc 1.xlsx]Sheet1:Sheet2", { range: true, external: true })).toBeFalse();
            });
        });

        describe("method validateNameLabel", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("validateNameLabel");
            });
            it("should reject invalid strings", () => {
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "")).toBe("name:empty");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "")).toBe("name:empty");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "")).toBe("name:empty");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "")).toBe("name:empty");
                expect(opOoxA1Grammar.validateNameLabel(docAccess, " name")).toBe("name:invalid");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, " name")).toBe("name:invalid");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, " name")).toBe("name:invalid");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, " name")).toBe("name:invalid");
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "1name")).toBe("name:invalid");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "1name")).toBe("name:invalid");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "1name")).toBe("name:invalid");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "1name")).toBe("name:invalid");
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "/name")).toBe("name:invalid");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "/name")).toBe("name:invalid");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "/name")).toBe("name:invalid");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "/name")).toBe("name:invalid");
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "?name")).toBe("name:invalid");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "?name")).toBe("name:invalid");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "?name")).toBe("name:invalid");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "?name")).toBe("name:invalid");
                expect(opOoxA1Grammar.validateNameLabel(docAccess, ".name")).toBe("name:invalid");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, ".name")).toBe("name:invalid");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, ".name")).toBe("name:invalid");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, ".name")).toBe("name:invalid");
            });
            it("should handle boolean literals", () => {
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "true")).toBe("name:invalid");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "true")).toBe("");
                expect(enOoxA1Grammar.validateNameLabel(docAccess, "true")).toBe("name:invalid");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "true")).toBe("");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "true")).toBe("");
                expect(enOdfA1Grammar.validateNameLabel(docAccess, "true")).toBe("");
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "wahr")).toBe("");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "wahr")).toBe("name:invalid");
                expect(enOoxA1Grammar.validateNameLabel(docAccess, "wahr")).toBe("");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "wahr")).toBe("");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "wahr")).toBe("");
                expect(enOdfA1Grammar.validateNameLabel(docAccess, "wahr")).toBe("");
            });
            it("should reject A1 cell references", () => {
                // first cell in sheet
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "a1")).toBe("name:address");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "a1")).toBe("name:address");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "a1")).toBe("name:address");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "a1")).toBe("name:address");
                // last cell in sheet
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "iv65536")).toBe("name:address");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "iv65536")).toBe("name:address");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "iv65536")).toBe("name:address");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "iv65536")).toBe("name:address");
                // outside of sheet range
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "iw65536")).toBe("");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "iw65536")).toBe("");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "iw65536")).toBe("");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "iw65536")).toBe("");
            });
            it("should reject R1C1 cell references", () => {
                // native absolute R1C1
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "r1c1")).toBe("name:address");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "r1c1")).toBe("name:address");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "r1c1")).toBe("name:address");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "r1c1")).toBe("name:address");
                // native relative R1C1
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "rc")).toBe("name:address");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "rc")).toBe("name:address");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "rc")).toBe("name:address");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "rc")).toBe("name:address");
                // localized absolute R1C1
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "z1s1")).toBe("");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "z1s1")).toBe("name:address");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "z1s1")).toBe("");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "z1s1")).toBe("");
                // localized relative R1C1
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "zs")).toBe("");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "zs")).toBe("name:address");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "zs")).toBe("");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "zs")).toBe("");
            });
            it("should accept valid names", () => {
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "my_name")).toBe("");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "my_name")).toBe("");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "my_name")).toBe("");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "my_name")).toBe("");
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "true1")).toBe("");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "wahr1")).toBe("");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "true1")).toBe("");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "wahr1")).toBe("");
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "c1r1")).toBe("");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "c1r1")).toBe("");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "c1r1")).toBe("");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "c1r1")).toBe("");
            });
            it("should handle backslash and question mark", () => {
                expect(opOoxA1Grammar.validateNameLabel(docAccess, "\\name?name.name\\name")).toBe("");
                expect(deOoxA1Grammar.validateNameLabel(docAccess, "\\name?name.name\\name")).toBe("");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "\\name")).toBe("name:invalid");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "\\name")).toBe("name:invalid");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "name?a")).toBe("");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "name?a")).toBe("");
                expect(opOdfA1Grammar.validateNameLabel(docAccess, "name.a")).toBe("name:invalid");
                expect(deOdfA1Grammar.validateNameLabel(docAccess, "name.a")).toBe("name:invalid");
            });
        });

        describe("method formatScalar", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("formatScalar");
            });
            it("should convert values to native text", () => {
                expect(opOoxA1Grammar.formatScalar(42)).toBe("42");
                expect(opOoxA1Grammar.formatScalar(-12.5)).toBe("-12.5");
                expect(opOoxA1Grammar.formatScalar(1e300)).toBe("1E+300");
                expect(opOoxA1Grammar.formatScalar(1e-300)).toBe("1E-300");
                expect(opOoxA1Grammar.formatScalar(1e-310)).toBe("0");
                expect(opOoxA1Grammar.formatScalar(Infinity)).toBe("#NUM!");
                expect(opOoxA1Grammar.formatScalar(NaN)).toBe("#NUM!");
                expect(opOoxA1Grammar.formatScalar("abc")).toBe('"abc"');
                expect(opOoxA1Grammar.formatScalar('a"b"c')).toBe('"a""b""c"');
                expect(opOoxA1Grammar.formatScalar("")).toBe('""');
                expect(opOoxA1Grammar.formatScalar(false)).toBe("FALSE");
                expect(opOoxA1Grammar.formatScalar(true)).toBe("TRUE");
                expect(opOoxA1Grammar.formatScalar(ErrorCode.NULL)).toBe("#NULL!");
                expect(opOoxA1Grammar.formatScalar(ErrorCode.DIV0)).toBe("#DIV/0!");
                expect(opOoxA1Grammar.formatScalar(ErrorCode.VALUE)).toBe("#VALUE!");
                expect(opOoxA1Grammar.formatScalar(ErrorCode.REF)).toBe("#REF!");
                expect(opOoxA1Grammar.formatScalar(ErrorCode.NAME)).toBe("#NAME?");
                expect(opOoxA1Grammar.formatScalar(ErrorCode.NUM)).toBe("#NUM!");
                expect(opOoxA1Grammar.formatScalar(ErrorCode.NA)).toBe("#N/A");
                expect(opOoxA1Grammar.formatScalar(ErrorCode.DATA)).toBe("#GETTING_DATA");
                expect(opOoxA1Grammar.formatScalar(null)).toBe("");
            });
            it("should convert values to localized text", () => {
                expect(deOoxA1Grammar.formatScalar(42)).toBe("42");
                expect(deOoxA1Grammar.formatScalar(-12.5)).toBe("-12,5");
                expect(deOoxA1Grammar.formatScalar(1e300)).toBe("1E+300");
                expect(deOoxA1Grammar.formatScalar(1e-300)).toBe("1E-300");
                expect(deOoxA1Grammar.formatScalar(1e-310)).toBe("0");
                expect(deOoxA1Grammar.formatScalar(Infinity)).toBe("#ZAHL!");
                expect(deOoxA1Grammar.formatScalar(NaN)).toBe("#ZAHL!");
                expect(deOoxA1Grammar.formatScalar("abc")).toBe('"abc"');
                expect(deOoxA1Grammar.formatScalar('a"b"c')).toBe('"a""b""c"');
                expect(deOoxA1Grammar.formatScalar("")).toBe('""');
                expect(deOoxA1Grammar.formatScalar(ErrorCode.NULL)).toBe("#NULL!");
                expect(deOoxA1Grammar.formatScalar(ErrorCode.DIV0)).toBe("#DIV/0!");
                expect(deOoxA1Grammar.formatScalar(ErrorCode.VALUE)).toBe("#WERT!");
                expect(deOoxA1Grammar.formatScalar(ErrorCode.REF)).toBe("#BEZUG!");
                expect(deOoxA1Grammar.formatScalar(ErrorCode.NAME)).toBe("#NAME?");
                expect(deOoxA1Grammar.formatScalar(ErrorCode.NUM)).toBe("#ZAHL!");
                expect(deOoxA1Grammar.formatScalar(ErrorCode.NA)).toBe("#NV");
                expect(deOoxA1Grammar.formatScalar(ErrorCode.DATA)).toBe("#GETTING_DATA");
                expect(deOoxA1Grammar.formatScalar(null)).toBe("");
            });
            it("should convert values to English text", () => {
                expect(enOoxA1Grammar.formatScalar(42)).toBe("42");
                expect(enOoxA1Grammar.formatScalar(-12.5)).toBe("-12.5");
                expect(enOoxA1Grammar.formatScalar(1e300)).toBe("1E+300");
                expect(enOoxA1Grammar.formatScalar(1e-300)).toBe("1E-300");
                expect(enOoxA1Grammar.formatScalar(1e-310)).toBe("0");
                expect(enOoxA1Grammar.formatScalar(Infinity)).toBe("#NUM!");
                expect(enOoxA1Grammar.formatScalar(NaN)).toBe("#NUM!");
                expect(enOoxA1Grammar.formatScalar("abc")).toBe('"abc"');
                expect(enOoxA1Grammar.formatScalar('a"b"c')).toBe('"a""b""c"');
                expect(enOoxA1Grammar.formatScalar("")).toBe('""');
                expect(enOoxA1Grammar.formatScalar(ErrorCode.NULL)).toBe("#NULL!");
                expect(enOoxA1Grammar.formatScalar(ErrorCode.DIV0)).toBe("#DIV/0!");
                expect(enOoxA1Grammar.formatScalar(ErrorCode.VALUE)).toBe("#VALUE!");
                expect(enOoxA1Grammar.formatScalar(ErrorCode.REF)).toBe("#REF!");
                expect(enOoxA1Grammar.formatScalar(ErrorCode.NAME)).toBe("#NAME?");
                expect(enOoxA1Grammar.formatScalar(ErrorCode.NUM)).toBe("#NUM!");
                expect(enOoxA1Grammar.formatScalar(ErrorCode.NA)).toBe("#N/A");
                expect(enOoxA1Grammar.formatScalar(ErrorCode.DATA)).toBe("#GETTING_DATA");
                expect(enOoxA1Grammar.formatScalar(null)).toBe("");
            });
        });

        describe("method formatGenericRange", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("formatGenericRange");
            });
            it("should return the string representation of a cell address", () => {
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3")).toBe("B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3")).toBe("B3");
                expect(enOoxA1Grammar.formatGenericRange(docAccess, "B3")).toBe("B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3")).toBe("B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3")).toBe("B3");
                expect(enOdfA1Grammar.formatGenericRange(docAccess, "B3")).toBe("B3");
            });
            it("should return the string representation of a cell range address", () => {
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", "D5")).toBe("B3:D5");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", "D5")).toBe("B3:D5");
                expect(enOoxA1Grammar.formatGenericRange(docAccess, "B3", "D5")).toBe("B3:D5");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", "D5")).toBe("B3:D5");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", "D5")).toBe("B3:D5");
                expect(enOdfA1Grammar.formatGenericRange(docAccess, "B3", "D5")).toBe("B3:D5");
            });
            it("should return the string representation of a cell address with sheet", () => {
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1"))).toBe("Sheet2!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1"))).toBe("Sheet2!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1"))).toBe("$Sheet2.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1"))).toBe("$Sheet2!B3");
            });
            it("should return the string representation of a cell range address with sheet", () => {
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$1"))).toBe("Sheet2!B3:D5");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$1"))).toBe("Sheet2!B3:D5");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$1"))).toBe("$Sheet2.B3:D5");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$1"))).toBe("$Sheet2!B3:D5");
            });
            it("should return the string representation of a reference with sheet range", () => {
                // cell address
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1:$3"))).toBe("$Sheet2.B3:$'Sheet 4'.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1:$3"))).toBe("$Sheet2:$'Sheet 4'!B3");
                // cell range address
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!B3:D5");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!B3:D5");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$1:$3"))).toBe("$Sheet2.B3:$'Sheet 4'.D5");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$1:$3"))).toBe("$Sheet2:$'Sheet 4'!B3:D5");
            });
            it("should return the string representation of a sheet reference error", () => {
                // cell address
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$-1"))).toBe("#REF!");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$-1"))).toBe("#BEZUG!");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$-1"))).toBe("#REF!");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$-1"))).toBe("#BEZUG!");
                // cell range address
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$-1"))).toBe("#REF!");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$-1"))).toBe("#BEZUG!");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$-1"))).toBe("#REF!");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", "D5", srefs("$-1"))).toBe("#BEZUG!");
            });
            it("should handle simple/complex sheet names correctly", () => {
                // simple name
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1"))).toBe("Sheet1!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1"))).toBe("Sheet1!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1"))).toBe("$Sheet1.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1"))).toBe("$Sheet1!B3");
                // whitespace
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet 2"))).toBe("'Sheet 2'!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet 2"))).toBe("'Sheet 2'!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet 2"))).toBe("$'Sheet 2'.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet 2"))).toBe("$'Sheet 2'!B3");
                // leading digit
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1Sheet"))).toBe("'1Sheet'!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1Sheet"))).toBe("'1Sheet'!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1Sheet"))).toBe("$1Sheet.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1Sheet"))).toBe("$1Sheet!B3");
                // operatosr
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet+1"))).toBe("'Sheet+1'!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet+1"))).toBe("'Sheet+1'!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet+1"))).toBe("$'Sheet+1'.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet+1"))).toBe("$'Sheet+1'!B3");
                // simple sheet range
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1:$Sheet2"))).toBe("Sheet1:Sheet2!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1:$Sheet2"))).toBe("Sheet1:Sheet2!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1:$Sheet2"))).toBe("$Sheet1.B3:$Sheet2.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1:$Sheet2"))).toBe("$Sheet1:$Sheet2!B3");
                // complex first sheet
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet 1:$Sheet2"))).toBe("'Sheet 1:Sheet2'!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet 1:$Sheet2"))).toBe("'Sheet 1:Sheet2'!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet 1:$Sheet2"))).toBe("$'Sheet 1'.B3:$Sheet2.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet 1:$Sheet2"))).toBe("$'Sheet 1':$Sheet2!B3");
                // complex second sheet
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1:$Sheet 2"))).toBe("'Sheet1:Sheet 2'!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1:$Sheet 2"))).toBe("'Sheet1:Sheet 2'!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1:$Sheet 2"))).toBe("$Sheet1.B3:$'Sheet 2'.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1:$Sheet 2"))).toBe("$Sheet1:$'Sheet 2'!B3");
                // ODF: relative sheets
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet1"))).toBe("Sheet1.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet1"))).toBe("Sheet1!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet 2"))).toBe("'Sheet 2'.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet 2"))).toBe("'Sheet 2'!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet1:$Sheet2"))).toBe("Sheet1.B3:$Sheet2.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet1:$Sheet2"))).toBe("Sheet1:$Sheet2!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1:Sheet2"))).toBe("$Sheet1.B3:Sheet2.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Sheet1:Sheet2"))).toBe("$Sheet1:Sheet2!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet1:Sheet2"))).toBe("Sheet1.B3:Sheet2.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet1:Sheet2"))).toBe("Sheet1:Sheet2!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet 1:Sheet2"))).toBe("'Sheet 1'.B3:Sheet2.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet 1:Sheet2"))).toBe("'Sheet 1':Sheet2!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet1:Sheet 2"))).toBe("Sheet1.B3:'Sheet 2'.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("Sheet1:Sheet 2"))).toBe("Sheet1:'Sheet 2'!B3");
            });
            it("should treat booleans as complex sheet names", () => {
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$TRUE"))).toBe("'TRUE'!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$TRUE"))).toBe("TRUE!B3");
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$WAHR"))).toBe("WAHR!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$WAHR"))).toBe("'WAHR'!B3");
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$TRUE:$WAHR"))).toBe("'TRUE:WAHR'!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$TRUE:$WAHR"))).toBe("'TRUE:WAHR'!B3");
            });
            it("should treat cell addresses as complex sheet names", () => {
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$A1"))).toBe("'A1'!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$A1"))).toBe("'A1'!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$A1"))).toBe("$'A1'.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$A1"))).toBe("$'A1'!B3");
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$IW1"))).toBe("IW1!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$IW1"))).toBe("IW1!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$IW1"))).toBe("$IW1.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$IW1"))).toBe("$IW1!B3");
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$R1C1"))).toBe("'R1C1'!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$R1C1"))).toBe("'R1C1'!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$R1C1"))).toBe("$'R1C1'.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$R1C1"))).toBe("$'R1C1'!B3");
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Z1S1"))).toBe("Z1S1!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Z1S1"))).toBe("'Z1S1'!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Z1S1"))).toBe("$Z1S1.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$Z1S1"))).toBe("$Z1S1!B3");
                expect(opOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1:$A1"))).toBe("'Sheet2:A1'!B3");
                expect(deOoxA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1:$A1"))).toBe("'Sheet2:A1'!B3");
                expect(opOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1:$A1"))).toBe("$Sheet2.B3:$'A1'.B3");
                expect(deOdfA1Grammar.formatGenericRange(docAccess, "B3", null, srefs("$1:$A1"))).toBe("$Sheet2:$'A1'!B3");
            });
        });

        describe("method formatReference", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("formatReference");
            });
            const ref = a("C4"), c1 = cref("B$3"), c12 = crefs("B$3:$D5");
            it("should return the string representation of a cell address", () => {
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c1)).toBe("B$3");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c1)).toBe("B$3");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, c1)).toBe("Z3S[-1]");
                expect(enOoxA1Grammar.formatReference(docAccess, ref, c1)).toBe("B$3");
                expect(enOoxRCGrammar.formatReference(docAccess, ref, c1)).toBe("R3C[-1]");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c1)).toBe("[.B$3]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c1)).toBe("B$3");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, c1)).toBe("R3C[-1]");
                expect(enOdfA1Grammar.formatReference(docAccess, ref, c1)).toBe("B$3");
                expect(enOdfRCGrammar.formatReference(docAccess, ref, c1)).toBe("R3C[-1]");
            });
            it("should return the string representation of a cell range address", () => {
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c12)).toBe("B$3:$D5");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c12)).toBe("B$3:$D5");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, c12)).toBe("Z3S[-1]:Z[1]S4");
                expect(enOoxA1Grammar.formatReference(docAccess, ref, c12)).toBe("B$3:$D5");
                expect(enOoxRCGrammar.formatReference(docAccess, ref, c12)).toBe("R3C[-1]:R[1]C4");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c12)).toBe("[.B$3:.$D5]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c12)).toBe("B$3:$D5");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, c12)).toBe("R3C[-1]:R[1]C4");
                expect(enOdfA1Grammar.formatReference(docAccess, ref, c12)).toBe("B$3:$D5");
                expect(enOdfRCGrammar.formatReference(docAccess, ref, c12)).toBe("R3C[-1]:R[1]C4");
            });
            it("should return the string representation of a reference error", () => {
                expect(opOoxA1Grammar.formatReference(docAccess, ref)).toBe("#REF!");
                expect(deOoxA1Grammar.formatReference(docAccess, ref)).toBe("#BEZUG!");
                expect(deOoxRCGrammar.formatReference(docAccess, ref)).toBe("#BEZUG!");
                expect(enOoxA1Grammar.formatReference(docAccess, ref)).toBe("#REF!");
                expect(enOoxRCGrammar.formatReference(docAccess, ref)).toBe("#REF!");
                expect(opOdfA1Grammar.formatReference(docAccess, ref)).toBe("[#REF!]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref)).toBe("#BEZUG!");
                expect(deOdfRCGrammar.formatReference(docAccess, ref)).toBe("#BEZUG!");
                expect(enOdfA1Grammar.formatReference(docAccess, ref)).toBe("#REF!");
                expect(enOdfRCGrammar.formatReference(docAccess, ref)).toBe("#REF!");
            });
            it("should return the string representation of a cell address with sheet", () => {
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c1, srefs("$1"))).toBe("Sheet2!B$3");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c1, srefs("$1"))).toBe("Sheet2!B$3");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, c1, srefs("$1"))).toBe("Sheet2!Z3S[-1]");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c1, srefs("$1"))).toBe("[$Sheet2.B$3]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c1, srefs("$1"))).toBe("$Sheet2!B$3");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, c1, srefs("$1"))).toBe("$Sheet2!R3C[-1]");
            });
            it("should return the string representation of a cell range address with sheet", () => {
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c12, srefs("$1"))).toBe("Sheet2!B$3:$D5");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c12, srefs("$1"))).toBe("Sheet2!B$3:$D5");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, c12, srefs("$1"))).toBe("Sheet2!Z3S[-1]:Z[1]S4");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c12, srefs("$1"))).toBe("[$Sheet2.B$3:.$D5]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c12, srefs("$1"))).toBe("$Sheet2!B$3:$D5");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, c12, srefs("$1"))).toBe("$Sheet2!R3C[-1]:R[1]C4");
            });
            it("should return the string representation of a reference error with sheet", () => {
                expect(opOoxA1Grammar.formatReference(docAccess, ref, undefined, srefs("$1"))).toBe("Sheet2!#REF!");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, undefined, srefs("$1"))).toBe("Sheet2!#BEZUG!");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, undefined, srefs("$1"))).toBe("Sheet2!#BEZUG!");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, undefined, srefs("$1"))).toBe("[#REF!]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, undefined, srefs("$1"))).toBe("#BEZUG!");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, undefined, srefs("$1"))).toBe("#BEZUG!");
            });
            it("should return the string representation of a reference with sheet range", () => {
                // cell address
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c1, srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!B$3");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c1, srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!B$3");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c1, srefs("$1:$3"))).toBe("[$Sheet2.B$3:$'Sheet 4'.B$3]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c1, srefs("$1:$3"))).toBe("$Sheet2:$'Sheet 4'!B$3");
                // cell range address
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c12, srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!B$3:$D5");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c12, srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!B$3:$D5");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c12, srefs("$1:$3"))).toBe("[$Sheet2.B$3:$'Sheet 4'.$D5]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c12, srefs("$1:$3"))).toBe("$Sheet2:$'Sheet 4'!B$3:$D5");
                // reference error
                expect(opOoxA1Grammar.formatReference(docAccess, ref, undefined, srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!#REF!");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, undefined, srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!#BEZUG!");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, undefined, srefs("$1:$3"))).toBe("[#REF!]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, undefined, srefs("$1:$3"))).toBe("#BEZUG!");
            });
            it("should return the string representation of a sheet reference error", () => {
                // cell address
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c1, srefs("$-1"))).toBe("#REF!");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c1, srefs("$-1"))).toBe("#BEZUG!");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c1, srefs("$-1"))).toBe("[#REF!]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c1, srefs("$-1"))).toBe("#BEZUG!");
                // cell range address
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c12, srefs("$-1"))).toBe("#REF!");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c12, srefs("$-1"))).toBe("#BEZUG!");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c12, srefs("$-1"))).toBe("[#REF!]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c12, srefs("$-1"))).toBe("#BEZUG!");
                // reference error
                expect(opOoxA1Grammar.formatReference(docAccess, ref, undefined, srefs("$-1"))).toBe("#REF!");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, undefined, srefs("$-1"))).toBe("#BEZUG!");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, undefined, srefs("$-1"))).toBe("[#REF!]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, undefined, srefs("$-1"))).toBe("#BEZUG!");
            });
            it("should return string representation of a column range", () => {
                // relative->absolute
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("B$1:$D$65536"))).toBe("B:$D");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("B$1:$D$65536"))).toBe("B:$D");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, crefs("B$1:$D$65536"))).toBe("S[-1]:S4");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("B$1:$D$65536"))).toBe("[.B$1:.$D$65536]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("B$1:$D$65536"))).toBe("B:$D");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, crefs("B$1:$D$65536"))).toBe("C[-1]:C4");
                // absolute->relative
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:D$65536"))).toBe("$B:D");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:D$65536"))).toBe("$B:D");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, crefs("$B$1:D$65536"))).toBe("S2:S[1]");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:D$65536"))).toBe("[.$B$1:.D$65536]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:D$65536"))).toBe("$B:D");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, crefs("$B$1:D$65536"))).toBe("C2:C[1]");
            });
            it("should return string representation of a single column", () => {
                // absolute
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$B$65536"))).toBe("$B:$B");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$B$65536"))).toBe("$B:$B");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, crefs("$B$1:$B$65536"))).toBe("S2");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$B$65536"))).toBe("[.$B$1:.$B$65536]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$B$65536"))).toBe("$B:$B");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, crefs("$B$1:$B$65536"))).toBe("C2");
                // relative
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("B$1:B$65536"))).toBe("B:B");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("B$1:B$65536"))).toBe("B:B");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, crefs("B$1:B$65536"))).toBe("S[-1]");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("B$1:B$65536"))).toBe("[.B$1:.B$65536]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("B$1:B$65536"))).toBe("B:B");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, crefs("B$1:B$65536"))).toBe("C[-1]");
            });
            it("should return string representation of a column range with sheet", () => {
                // single sheet
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:B$65536"), srefs("$1"))).toBe("Sheet2!$B:B");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:B$65536"), srefs("$1"))).toBe("Sheet2!$B:B");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, crefs("$B$1:B$65536"), srefs("$1"))).toBe("Sheet2!S2:S[-1]");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:B$65536"), srefs("$1"))).toBe("[$Sheet2.$B$1:.B$65536]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:B$65536"), srefs("$1"))).toBe("$Sheet2!$B:B");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, crefs("$B$1:B$65536"), srefs("$1"))).toBe("$Sheet2!C2:C[-1]");
                // sheet range
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:B$65536"), srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!$B:B");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:B$65536"), srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!$B:B");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:B$65536"), srefs("$1:$3"))).toBe("[$Sheet2.$B$1:$'Sheet 4'.B$65536]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:B$65536"), srefs("$1:$3"))).toBe("$Sheet2:$'Sheet 4'!$B:B");
            });
            it("should not return string representation of a column range", () => {
                // missing absolute flags
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$D65536"))).toBe("$B$1:$D65536");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$D65536"))).toBe("$B$1:$D65536");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$D65536"))).toBe("[.$B$1:.$D65536]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$D65536"))).toBe("$B$1:$D65536");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$B1:$D$65536"))).toBe("$B1:$D$65536");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$B1:$D$65536"))).toBe("$B1:$D$65536");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$B1:$D$65536"))).toBe("[.$B1:.$D$65536]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$B1:$D$65536"))).toBe("$B1:$D$65536");
                // wrong indexes
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$2:$D$65536"))).toBe("$B$2:$D$65536");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$2:$D$65536"))).toBe("$B$2:$D$65536");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$2:$D$65536"))).toBe("[.$B$2:.$D$65536]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$2:$D$65536"))).toBe("$B$2:$D$65536");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$D$65535"))).toBe("$B$1:$D$65535");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$D$65535"))).toBe("$B$1:$D$65535");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$D$65535"))).toBe("[.$B$1:.$D$65535]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$1:$D$65535"))).toBe("$B$1:$D$65535");
            });
            it("should return string representation of a row range", () => {
                // relative->absolute
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$A2:$IV$4"))).toBe("2:$4");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$A2:$IV$4"))).toBe("2:$4");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, crefs("$A2:$IV$4"))).toBe("Z[-2]:Z4");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$A2:$IV$4"))).toBe("[.$A2:.$IV$4]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$A2:$IV$4"))).toBe("2:$4");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, crefs("$A2:$IV$4"))).toBe("R[-2]:R4");
                // absolute->relative
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV4"))).toBe("$2:4");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV4"))).toBe("$2:4");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, crefs("$A$2:$IV4"))).toBe("Z2:Z");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV4"))).toBe("[.$A$2:.$IV4]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV4"))).toBe("$2:4");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, crefs("$A$2:$IV4"))).toBe("R2:R");
            });
            it("should return string representation of a single row", () => {
                // absolute
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV$2"))).toBe("$2:$2");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV$2"))).toBe("$2:$2");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, crefs("$A$2:$IV$2"))).toBe("Z2");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV$2"))).toBe("[.$A$2:.$IV$2]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV$2"))).toBe("$2:$2");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, crefs("$A$2:$IV$2"))).toBe("R2");
                // relative
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$A2:$IV2"))).toBe("2:2");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$A2:$IV2"))).toBe("2:2");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, crefs("$A2:$IV2"))).toBe("Z[-2]");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$A2:$IV2"))).toBe("[.$A2:.$IV2]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$A2:$IV2"))).toBe("2:2");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, crefs("$A2:$IV2"))).toBe("R[-2]");
            });
            it("should return string representation of a row range with sheet", () => {
                // single sheet
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV2"), srefs("$1"))).toBe("Sheet2!$2:2");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV2"), srefs("$1"))).toBe("Sheet2!$2:2");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, crefs("$A$2:$IV2"), srefs("$1"))).toBe("Sheet2!Z2:Z[-2]");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV2"), srefs("$1"))).toBe("[$Sheet2.$A$2:.$IV2]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV2"), srefs("$1"))).toBe("$Sheet2!$2:2");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, crefs("$A$2:$IV2"), srefs("$1"))).toBe("$Sheet2!R2:R[-2]");
                // sheet range
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV2"), srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!$2:2");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV2"), srefs("$1:$3"))).toBe("'Sheet2:Sheet 4'!$2:2");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV2"), srefs("$1:$3"))).toBe("[$Sheet2.$A$2:$'Sheet 4'.$IV2]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IV2"), srefs("$1:$3"))).toBe("$Sheet2:$'Sheet 4'!$2:2");
            });
            it("should not return string representation of a row range", () => {
                // missing absolute flags
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:IV$4"))).toBe("$A$2:IV$4");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:IV$4"))).toBe("$A$2:IV$4");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:IV$4"))).toBe("[.$A$2:.IV$4]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:IV$4"))).toBe("$A$2:IV$4");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("A$2:$IV$4"))).toBe("A$2:$IV$4");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("A$2:$IV$4"))).toBe("A$2:$IV$4");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("A$2:$IV$4"))).toBe("[.A$2:.$IV$4]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("A$2:$IV$4"))).toBe("A$2:$IV$4");
                // wrong indexes
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$2:$IV$4"))).toBe("$B$2:$IV$4");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$B$2:$IV$4"))).toBe("$B$2:$IV$4");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$2:$IV$4"))).toBe("[.$B$2:.$IV$4]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$B$2:$IV$4"))).toBe("$B$2:$IV$4");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IU$4"))).toBe("$A$2:$IU$4");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IU$4"))).toBe("$A$2:$IU$4");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IU$4"))).toBe("[.$A$2:.$IU$4]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$2:$IU$4"))).toBe("$A$2:$IU$4");
            });
            it("should return string representation of a sheet range", () => {
                expect(opOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$1:$IV$65536"))).toBe("$1:$65536");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, crefs("$A$1:$IV$65536"))).toBe("$1:$65536");
                expect(deOoxRCGrammar.formatReference(docAccess, ref, crefs("$A$1:$IV$65536"))).toBe("Z1:Z65536");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$1:$IV$65536"))).toBe("[.$A$1:.$IV$65536]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, crefs("$A$1:$IV$65536"))).toBe("$1:$65536");
                expect(deOdfRCGrammar.formatReference(docAccess, ref, crefs("$A$1:$IV$65536"))).toBe("R1:R65536");
            });
            it("should handle simple/complex sheet names correctly", () => {
                const c2 = cref("A1");
                // simple name
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1"))).toBe("Sheet1!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1"))).toBe("Sheet1!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1"))).toBe("[$Sheet1.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1"))).toBe("$Sheet1!A1");
                // whitespace
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet 2"))).toBe("'Sheet 2'!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet 2"))).toBe("'Sheet 2'!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet 2"))).toBe("[$'Sheet 2'.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet 2"))).toBe("$'Sheet 2'!A1");
                // leading digit
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$1Sheet"))).toBe("'1Sheet'!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$1Sheet"))).toBe("'1Sheet'!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$1Sheet"))).toBe("[$1Sheet.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$1Sheet"))).toBe("$1Sheet!A1");
                // operator
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet+1"))).toBe("'Sheet+1'!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet+1"))).toBe("'Sheet+1'!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet+1"))).toBe("[$'Sheet+1'.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet+1"))).toBe("$'Sheet+1'!A1");
                // simple sheet range
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1:$Sheet2"))).toBe("Sheet1:Sheet2!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1:$Sheet2"))).toBe("Sheet1:Sheet2!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1:$Sheet2"))).toBe("[$Sheet1.A1:$Sheet2.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1:$Sheet2"))).toBe("$Sheet1:$Sheet2!A1");
                // complex first sheet
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet 1:$Sheet2"))).toBe("'Sheet 1:Sheet2'!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet 1:$Sheet2"))).toBe("'Sheet 1:Sheet2'!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet 1:$Sheet2"))).toBe("[$'Sheet 1'.A1:$Sheet2.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet 1:$Sheet2"))).toBe("$'Sheet 1':$Sheet2!A1");
                // complex second sheet
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1:$Sheet 2"))).toBe("'Sheet1:Sheet 2'!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1:$Sheet 2"))).toBe("'Sheet1:Sheet 2'!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1:$Sheet 2"))).toBe("[$Sheet1.A1:$'Sheet 2'.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1:$Sheet 2"))).toBe("$Sheet1:$'Sheet 2'!A1");
                // ODF: relative sheets
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet1"))).toBe("[Sheet1.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet1"))).toBe("Sheet1!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet 2"))).toBe("['Sheet 2'.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet 2"))).toBe("'Sheet 2'!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet1:$Sheet2"))).toBe("[Sheet1.A1:$Sheet2.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet1:$Sheet2"))).toBe("Sheet1:$Sheet2!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1:Sheet2"))).toBe("[$Sheet1.A1:Sheet2.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Sheet1:Sheet2"))).toBe("$Sheet1:Sheet2!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet1:Sheet2"))).toBe("[Sheet1.A1:Sheet2.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet1:Sheet2"))).toBe("Sheet1:Sheet2!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet 1:Sheet2"))).toBe("['Sheet 1'.A1:Sheet2.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet 1:Sheet2"))).toBe("'Sheet 1':Sheet2!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet1:Sheet 2"))).toBe("[Sheet1.A1:'Sheet 2'.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("Sheet1:Sheet 2"))).toBe("Sheet1:'Sheet 2'!A1");
            });
            it("should treat booleans as complex sheet names", () => {
                const c2 = cref("A1");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$TRUE"))).toBe("'TRUE'!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$TRUE"))).toBe("TRUE!A1");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$WAHR"))).toBe("WAHR!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$WAHR"))).toBe("'WAHR'!A1");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$TRUE:$WAHR"))).toBe("'TRUE:WAHR'!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$TRUE:$WAHR"))).toBe("'TRUE:WAHR'!A1");
            });
            it("should treat cell addresses as complex sheet names", () => {
                const c2 = cref("A1");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$A1"))).toBe("'A1'!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$A1"))).toBe("'A1'!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$A1"))).toBe("[$'A1'.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$A1"))).toBe("$'A1'!A1");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$IW1"))).toBe("IW1!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$IW1"))).toBe("IW1!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$IW1"))).toBe("[$IW1.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$IW1"))).toBe("$IW1!A1");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$R1C1"))).toBe("'R1C1'!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$R1C1"))).toBe("'R1C1'!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$R1C1"))).toBe("[$'R1C1'.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$R1C1"))).toBe("$'R1C1'!A1");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Z1S1"))).toBe("Z1S1!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$Z1S1"))).toBe("'Z1S1'!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Z1S1"))).toBe("[$Z1S1.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$Z1S1"))).toBe("$Z1S1!A1");
                expect(opOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$1:$A1"))).toBe("'Sheet2:A1'!A1");
                expect(deOoxA1Grammar.formatReference(docAccess, ref, c2, srefs("$1:$A1"))).toBe("'Sheet2:A1'!A1");
                expect(opOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$1:$A1"))).toBe("[$Sheet2.A1:$'A1'.A1]");
                expect(deOdfA1Grammar.formatReference(docAccess, ref, c2, srefs("$1:$A1"))).toBe("$Sheet2:$'A1'!A1");
            });
        });

        describe("method formatName", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("formatName");
            });
            it("should return the string representation", () => {
                expect(opOoxA1Grammar.formatName(docAccess, "name")).toBe("name");
                expect(deOoxA1Grammar.formatName(docAccess, "name")).toBe("name");
                expect(opOoxA1Grammar.formatName(docAccess, "Name", srefs("$1"))).toBe("Sheet2!Name");
                expect(deOoxA1Grammar.formatName(docAccess, "Name", srefs("$1"))).toBe("Sheet2!Name");
                expect(opOoxA1Grammar.formatName(docAccess, "NAME", srefs("$3"))).toBe("'Sheet 4'!NAME");
                expect(deOoxA1Grammar.formatName(docAccess, "NAME", srefs("$3"))).toBe("'Sheet 4'!NAME");
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs("$-1"))).toBe("#REF!");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs("$-1"))).toBe("#BEZUG!");
            });
            it("should return the string representation for explicitly global names", () => {
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs(null, 0))).toBe("[0]!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs(null, 0))).toBe("[0]!name");
                expect(opOoxA1Grammar.formatName(docAccess, "Name", srefs("$1", 0))).toBe("[0]Sheet2!Name");
                expect(deOoxA1Grammar.formatName(docAccess, "Name", srefs("$1", 0))).toBe("[0]Sheet2!Name");
                expect(opOoxA1Grammar.formatName(docAccess, "NAME", srefs("$3", 0))).toBe("'[0]Sheet 4'!NAME");
                expect(deOoxA1Grammar.formatName(docAccess, "NAME", srefs("$3", 0))).toBe("'[0]Sheet 4'!NAME");
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs("$-1", 0))).toBe("#REF!");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs("$-1", 0))).toBe("#BEZUG!");
            });
            it("should return the string representation for external names", () => {
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs(null, 1))).toBe("[1]!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs(null, 1))).toBe("[1]!name");
                expect(opOoxA1Grammar.formatName(docAccess, "Name", srefs("$1", 1))).toBe("#REF!");
                expect(deOoxA1Grammar.formatName(docAccess, "Name", srefs("$1", 1))).toBe("#BEZUG!");
                expect(opOoxA1Grammar.formatName(docAccess, "Name", srefs('$Sheet1', 1))).toBe("[1]Sheet1!Name");
                expect(deOoxA1Grammar.formatName(docAccess, "Name", srefs('$Sheet1', 1))).toBe("[1]Sheet1!Name");
                expect(opOoxA1Grammar.formatName(docAccess, "Name", srefs('$Sheet1', 1))).toBe("[1]Sheet1!Name");
                expect(deOoxA1Grammar.formatName(docAccess, "Name", srefs('$Sheet1', 1))).toBe("[1]Sheet1!Name");
            });
            it("should handle simple/complex sheet names correctly", () => {
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs('$Sheet1'))).toBe("Sheet1!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs('$Sheet1'))).toBe("Sheet1!name");
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs('$Sheet 2'))).toBe("'Sheet 2'!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs('$Sheet 2'))).toBe("'Sheet 2'!name");
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs('$1Sheet'))).toBe("'1Sheet'!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs('$1Sheet'))).toBe("'1Sheet'!name");
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs('$Sheet+1'))).toBe("'Sheet+1'!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs('$Sheet+1'))).toBe("'Sheet+1'!name");
            });
            it("should treat booleans as complex sheet names", () => {
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs('$TRUE'))).toBe("'TRUE'!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs('$TRUE'))).toBe("TRUE!name");
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs('$WAHR'))).toBe("WAHR!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs('$WAHR'))).toBe("'WAHR'!name");
            });
            it("should treat cell addresses as complex sheet names", () => {
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs("$A1"))).toBe("'A1'!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs("$A1"))).toBe("'A1'!name");
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs("$IW1"))).toBe("IW1!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs("$IW1"))).toBe("IW1!name");
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs("$R1C1"))).toBe("'R1C1'!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs("$R1C1"))).toBe("'R1C1'!name");
                expect(opOoxA1Grammar.formatName(docAccess, "name", srefs("$Z1S1"))).toBe("Z1S1!name");
                expect(deOoxA1Grammar.formatName(docAccess, "name", srefs("$Z1S1"))).toBe("'Z1S1'!name");
            });
        });

        describe("method formatMacro", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("formatMacro");
            });
            it("should return the string representation", () => {
                expect(opOoxA1Grammar.formatMacro(docAccess, "func")).toBe("func");
                expect(deOoxA1Grammar.formatMacro(docAccess, "func")).toBe("func");
                expect(opOoxA1Grammar.formatMacro(docAccess, "Func", srefs("$1"))).toBe("Sheet2!Func");
                expect(deOoxA1Grammar.formatMacro(docAccess, "Func", srefs("$1"))).toBe("Sheet2!Func");
                expect(opOoxA1Grammar.formatMacro(docAccess, "FUNC", srefs("$3"))).toBe("'Sheet 4'!FUNC");
                expect(deOoxA1Grammar.formatMacro(docAccess, "FUNC", srefs("$3"))).toBe("'Sheet 4'!FUNC");
                expect(opOoxA1Grammar.formatMacro(docAccess, "func", srefs("$-1"))).toBe("#REF!");
                expect(deOoxA1Grammar.formatMacro(docAccess, "func", srefs("$-1"))).toBe("#BEZUG!");
            });
            it("should omit UDF prefix in UI grammar", () => {
                expect(opOoxA1Grammar.formatMacro(docAccess, "_xludf.sum")).toBe("_xludf.sum");
                expect(deOoxA1Grammar.formatMacro(docAccess, "_xludf.sum")).toBe("sum");
                expect(opOoxA1Grammar.formatMacro(docAccess, "_xludf.Sum", srefs("$1"))).toBe("Sheet2!_xludf.Sum");
                expect(deOoxA1Grammar.formatMacro(docAccess, "_xludf.Sum", srefs("$1"))).toBe("Sheet2!Sum");
                expect(opOoxA1Grammar.formatMacro(docAccess, "_xludf.SUM", srefs("$3"))).toBe("'Sheet 4'!_xludf.SUM");
                expect(deOoxA1Grammar.formatMacro(docAccess, "_xludf.SUM", srefs("$3"))).toBe("'Sheet 4'!SUM");
                expect(opOoxA1Grammar.formatMacro(docAccess, "_xludf.sum", srefs("$-1"))).toBe("#REF!");
                expect(deOoxA1Grammar.formatMacro(docAccess, "_xludf.sum", srefs("$-1"))).toBe("#BEZUG!");
            });
            it("should add UDF prefix in operations", () => {
                expect(opOoxA1Grammar.formatMacro(docAccess, "sum")).toBe("_xludf.sum");
                expect(deOoxA1Grammar.formatMacro(docAccess, "sum")).toBe("sum");
                expect(opOoxA1Grammar.formatMacro(docAccess, "Sum", srefs("$1"))).toBe("Sheet2!_xludf.Sum");
                expect(deOoxA1Grammar.formatMacro(docAccess, "Sum", srefs("$1"))).toBe("Sheet2!Sum");
                expect(opOoxA1Grammar.formatMacro(docAccess, "SUM", srefs("$3"))).toBe("'Sheet 4'!_xludf.SUM");
                expect(deOoxA1Grammar.formatMacro(docAccess, "SUM", srefs("$3"))).toBe("'Sheet 4'!SUM");
                expect(opOoxA1Grammar.formatMacro(docAccess, "sum", srefs("$-1"))).toBe("#REF!");
                expect(deOoxA1Grammar.formatMacro(docAccess, "sum", srefs("$-1"))).toBe("#BEZUG!");
            });
        });

        describe("method getBooleanName", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("getBooleanName");
            });
            it("should return native boolean names", () => {
                expect(opOoxA1Grammar.getBooleanName(false)).toBe("FALSE");
                expect(opOoxA1Grammar.getBooleanName(true)).toBe("TRUE");
            });
            it("should return localized boolean names", () => {
                expect(deOoxA1Grammar.getBooleanName(false)).toBe("FALSCH");
                expect(deOoxA1Grammar.getBooleanName(true)).toBe("WAHR");
            });
            it("should return English boolean names", () => {
                expect(enOoxA1Grammar.getBooleanName(false)).toBe("FALSE");
                expect(enOoxA1Grammar.getBooleanName(true)).toBe("TRUE");
            });
        });

        describe("method getBooleanValue", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("getBooleanValue");
            });
            it("should parse native boolean names", () => {
                expect(opOoxA1Grammar.getBooleanValue("false")).toBeFalse();
                expect(opOoxA1Grammar.getBooleanValue("False")).toBeFalse();
                expect(opOoxA1Grammar.getBooleanValue("FALSE")).toBeFalse();
                expect(opOoxA1Grammar.getBooleanValue("true")).toBeTrue();
                expect(opOoxA1Grammar.getBooleanValue("True")).toBeTrue();
                expect(opOoxA1Grammar.getBooleanValue("TRUE")).toBeTrue();
            });
            it("should parse localized boolean names", () => {
                expect(deOoxA1Grammar.getBooleanValue("falsch")).toBeFalse();
                expect(deOoxA1Grammar.getBooleanValue("Falsch")).toBeFalse();
                expect(deOoxA1Grammar.getBooleanValue("FALSCH")).toBeFalse();
                expect(deOoxA1Grammar.getBooleanValue("wahr")).toBeTrue();
                expect(deOoxA1Grammar.getBooleanValue("Wahr")).toBeTrue();
                expect(deOoxA1Grammar.getBooleanValue("WAHR")).toBeTrue();
            });
            it("should parse English boolean names", () => {
                expect(enOoxA1Grammar.getBooleanValue("false")).toBeFalse();
                expect(enOoxA1Grammar.getBooleanValue("False")).toBeFalse();
                expect(enOoxA1Grammar.getBooleanValue("FALSE")).toBeFalse();
                expect(enOoxA1Grammar.getBooleanValue("true")).toBeTrue();
                expect(enOoxA1Grammar.getBooleanValue("True")).toBeTrue();
                expect(enOoxA1Grammar.getBooleanValue("TRUE")).toBeTrue();
            });
            it("should return null for invalid names", () => {
                expect(opOoxA1Grammar.getBooleanValue("1")).toBeNull();
                expect(opOoxA1Grammar.getBooleanValue("abc")).toBeNull();
                expect(opOoxA1Grammar.getBooleanValue("FALSCH")).toBeNull();
                expect(deOoxA1Grammar.getBooleanValue("1")).toBeNull();
                expect(deOoxA1Grammar.getBooleanValue("abc")).toBeNull();
                expect(deOoxA1Grammar.getBooleanValue("FALSE")).toBeNull();
                expect(enOoxA1Grammar.getBooleanValue("1")).toBeNull();
                expect(enOoxA1Grammar.getBooleanValue("abc")).toBeNull();
                expect(enOoxA1Grammar.getBooleanValue("FALSCH")).toBeNull();
            });
        });

        describe("method getErrorName", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("getErrorName");
            });
            it("should return native error names", () => {
                expect(opOoxA1Grammar.getErrorName(ErrorCode.NULL)).toBe("#NULL!");
                expect(opOoxA1Grammar.getErrorName(ErrorCode.DIV0)).toBe("#DIV/0!");
                expect(opOoxA1Grammar.getErrorName(ErrorCode.VALUE)).toBe("#VALUE!");
                expect(opOoxA1Grammar.getErrorName(ErrorCode.REF)).toBe("#REF!");
                expect(opOoxA1Grammar.getErrorName(ErrorCode.NAME)).toBe("#NAME?");
                expect(opOoxA1Grammar.getErrorName(ErrorCode.NUM)).toBe("#NUM!");
                expect(opOoxA1Grammar.getErrorName(ErrorCode.NA)).toBe("#N/A");
                expect(opOoxA1Grammar.getErrorName(ErrorCode.DATA)).toBe("#GETTING_DATA");
            });
            it("should return localized error names", () => {
                expect(deOoxA1Grammar.getErrorName(ErrorCode.NULL)).toBe("#NULL!");
                expect(deOoxA1Grammar.getErrorName(ErrorCode.DIV0)).toBe("#DIV/0!");
                expect(deOoxA1Grammar.getErrorName(ErrorCode.VALUE)).toBe("#WERT!");
                expect(deOoxA1Grammar.getErrorName(ErrorCode.REF)).toBe("#BEZUG!");
                expect(deOoxA1Grammar.getErrorName(ErrorCode.NAME)).toBe("#NAME?");
                expect(deOoxA1Grammar.getErrorName(ErrorCode.NUM)).toBe("#ZAHL!");
                expect(deOoxA1Grammar.getErrorName(ErrorCode.NA)).toBe("#NV");
                expect(deOoxA1Grammar.getErrorName(ErrorCode.DATA)).toBe("#GETTING_DATA");
            });
            it("should return English error names", () => {
                expect(enOoxA1Grammar.getErrorName(ErrorCode.NULL)).toBe("#NULL!");
                expect(enOoxA1Grammar.getErrorName(ErrorCode.DIV0)).toBe("#DIV/0!");
                expect(enOoxA1Grammar.getErrorName(ErrorCode.VALUE)).toBe("#VALUE!");
                expect(enOoxA1Grammar.getErrorName(ErrorCode.REF)).toBe("#REF!");
                expect(enOoxA1Grammar.getErrorName(ErrorCode.NAME)).toBe("#NAME?");
                expect(enOoxA1Grammar.getErrorName(ErrorCode.NUM)).toBe("#NUM!");
                expect(enOoxA1Grammar.getErrorName(ErrorCode.NA)).toBe("#N/A");
                expect(enOoxA1Grammar.getErrorName(ErrorCode.DATA)).toBe("#GETTING_DATA");
            });
        });

        describe("method getErrorCode", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("getErrorCode");
            });
            it("should parse native error names", () => {
                expect(opOoxA1Grammar.getErrorCode("#NULL!")).toBe(ErrorCode.NULL);
                expect(opOoxA1Grammar.getErrorCode("#null!")).toBe(ErrorCode.NULL);
                expect(opOoxA1Grammar.getErrorCode("#DIV/0!")).toBe(ErrorCode.DIV0);
                expect(opOoxA1Grammar.getErrorCode("#VALUE!")).toBe(ErrorCode.VALUE);
                expect(opOoxA1Grammar.getErrorCode("#REF!")).toBe(ErrorCode.REF);
                expect(opOoxA1Grammar.getErrorCode("#NAME?")).toBe(ErrorCode.NAME);
                expect(opOoxA1Grammar.getErrorCode("#NUM!")).toBe(ErrorCode.NUM);
                expect(opOoxA1Grammar.getErrorCode("#N/A")).toBe(ErrorCode.NA);
                expect(opOoxA1Grammar.getErrorCode("#GETTING_DATA")).toBe(ErrorCode.DATA);
            });
            it("should parse localized error names", () => {
                expect(deOoxA1Grammar.getErrorCode("#NULL!")).toBe(ErrorCode.NULL);
                expect(deOoxA1Grammar.getErrorCode("#DIV/0!")).toBe(ErrorCode.DIV0);
                expect(deOoxA1Grammar.getErrorCode("#WERT!")).toBe(ErrorCode.VALUE);
                expect(deOoxA1Grammar.getErrorCode("#wert!")).toBe(ErrorCode.VALUE);
                expect(deOoxA1Grammar.getErrorCode("#BEZUG!")).toBe(ErrorCode.REF);
                expect(deOoxA1Grammar.getErrorCode("#NAME?")).toBe(ErrorCode.NAME);
                expect(deOoxA1Grammar.getErrorCode("#ZAHL!")).toBe(ErrorCode.NUM);
                expect(deOoxA1Grammar.getErrorCode("#NV")).toBe(ErrorCode.NA);
                expect(deOoxA1Grammar.getErrorCode("#GETTING_DATA")).toBe(ErrorCode.DATA);
            });
            it("should parse English error names", () => {
                expect(enOoxA1Grammar.getErrorCode("#NULL!")).toBe(ErrorCode.NULL);
                expect(enOoxA1Grammar.getErrorCode("#null!")).toBe(ErrorCode.NULL);
                expect(enOoxA1Grammar.getErrorCode("#DIV/0!")).toBe(ErrorCode.DIV0);
                expect(enOoxA1Grammar.getErrorCode("#VALUE!")).toBe(ErrorCode.VALUE);
                expect(enOoxA1Grammar.getErrorCode("#REF!")).toBe(ErrorCode.REF);
                expect(enOoxA1Grammar.getErrorCode("#NAME?")).toBe(ErrorCode.NAME);
                expect(enOoxA1Grammar.getErrorCode("#NUM!")).toBe(ErrorCode.NUM);
                expect(enOoxA1Grammar.getErrorCode("#N/A")).toBe(ErrorCode.NA);
                expect(enOoxA1Grammar.getErrorCode("#GETTING_DATA")).toBe(ErrorCode.DATA);
            });
            it("should return null for invalid names", () => {
                expect(opOoxA1Grammar.getErrorCode("abc")).toBeNull();
                expect(opOoxA1Grammar.getErrorCode("#NULL")).toBeNull();
                expect(opOoxA1Grammar.getErrorCode("NULL")).toBeNull();
                expect(opOoxA1Grammar.getErrorCode("#WERT!")).toBeNull();
                expect(deOoxA1Grammar.getErrorCode("abc")).toBeNull();
                expect(deOoxA1Grammar.getErrorCode("#NULL")).toBeNull();
                expect(deOoxA1Grammar.getErrorCode("NULL")).toBeNull();
                expect(deOoxA1Grammar.getErrorCode("#VALUE!")).toBeNull();
                expect(enOoxA1Grammar.getErrorCode("abc")).toBeNull();
                expect(enOoxA1Grammar.getErrorCode("#NULL")).toBeNull();
                expect(enOoxA1Grammar.getErrorCode("NULL")).toBeNull();
                expect(enOoxA1Grammar.getErrorCode("#WERT!")).toBeNull();
            });
        });

        describe("method getOperatorName", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("getOperatorName");
            });
            it("should return native operator names", () => {
                expect(opOoxA1Grammar.getOperatorName("add")).toBe("+");
                expect(opOoxA1Grammar.getOperatorName("le")).toBe("<=");
                expect(opOoxA1Grammar.getOperatorName("list")).toBe(",");
                expect(opOoxA1Grammar.getOperatorName("isect")).toBe(" ");
                expect(opOdfA1Grammar.getOperatorName("add")).toBe("+");
                expect(opOdfA1Grammar.getOperatorName("le")).toBe("<=");
                expect(opOdfA1Grammar.getOperatorName("list")).toBe("~");
                expect(opOdfA1Grammar.getOperatorName("isect")).toBe("!");
            });
            it("should return localized operator names", () => {
                expect(deOoxA1Grammar.getOperatorName("add")).toBe("+");
                expect(deOoxA1Grammar.getOperatorName("le")).toBe("<=");
                expect(deOoxA1Grammar.getOperatorName("list")).toBe(";");
                expect(deOoxA1Grammar.getOperatorName("isect")).toBe(" ");
                expect(deOdfA1Grammar.getOperatorName("add")).toBe("+");
                expect(deOdfA1Grammar.getOperatorName("le")).toBe("<=");
                expect(deOdfA1Grammar.getOperatorName("list")).toBe(";");
                expect(deOdfA1Grammar.getOperatorName("isect")).toBe(" ");
            });
            it("should return English operator names", () => {
                expect(enOoxA1Grammar.getOperatorName("add")).toBe("+");
                expect(enOoxA1Grammar.getOperatorName("le")).toBe("<=");
                expect(enOoxA1Grammar.getOperatorName("list")).toBe(",");
                expect(enOoxA1Grammar.getOperatorName("isect")).toBe(" ");
                expect(enOdfA1Grammar.getOperatorName("add")).toBe("+");
                expect(enOdfA1Grammar.getOperatorName("le")).toBe("<=");
                expect(enOdfA1Grammar.getOperatorName("list")).toBe(",");
                expect(enOdfA1Grammar.getOperatorName("isect")).toBe(" ");
            });
            it("should return null for invalid keys", () => {
                expect(opOoxA1Grammar.getOperatorName("abc")).toBeNull();
                expect(opOoxA1Grammar.getOperatorName("ADD")).toBeNull();
                expect(opOoxA1Grammar.getOperatorName("+")).toBeNull();
            });
        });

        describe("method getOperatorKey", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("getOperatorKey");
            });
            it("should parse native operator names", () => {
                expect(opOoxA1Grammar.getOperatorKey("+")).toBe("add");
                expect(opOoxA1Grammar.getOperatorKey("<=")).toBe("le");
                expect(opOoxA1Grammar.getOperatorKey(",")).toBe("list");
                expect(opOoxA1Grammar.getOperatorKey(" ")).toBe("isect");
                expect(opOdfA1Grammar.getOperatorKey("+")).toBe("add");
                expect(opOdfA1Grammar.getOperatorKey("<=")).toBe("le");
                expect(opOdfA1Grammar.getOperatorKey("~")).toBe("list");
                expect(opOdfA1Grammar.getOperatorKey("!")).toBe("isect");
            });
            it("should parse localized operator names", () => {
                expect(deOoxA1Grammar.getOperatorKey("+")).toBe("add");
                expect(deOoxA1Grammar.getOperatorKey("<=")).toBe("le");
                expect(deOoxA1Grammar.getOperatorKey(";")).toBe("list");
                expect(deOoxA1Grammar.getOperatorKey(" ")).toBe("isect");
                expect(deOdfA1Grammar.getOperatorKey("+")).toBe("add");
                expect(deOdfA1Grammar.getOperatorKey("<=")).toBe("le");
                expect(deOdfA1Grammar.getOperatorKey(";")).toBe("list");
                expect(deOdfA1Grammar.getOperatorKey(" ")).toBe("isect");
            });
            it("should parse English operator names", () => {
                expect(enOoxA1Grammar.getOperatorKey("+")).toBe("add");
                expect(enOoxA1Grammar.getOperatorKey("<=")).toBe("le");
                expect(enOoxA1Grammar.getOperatorKey(",")).toBe("list");
                expect(enOoxA1Grammar.getOperatorKey(" ")).toBe("isect");
                expect(enOdfA1Grammar.getOperatorKey("+")).toBe("add");
                expect(enOdfA1Grammar.getOperatorKey("<=")).toBe("le");
                expect(enOdfA1Grammar.getOperatorKey(",")).toBe("list");
                expect(enOdfA1Grammar.getOperatorKey(" ")).toBe("isect");
            });
            it("should return null for invalid names", () => {
                expect(opOoxA1Grammar.getOperatorKey("abc")).toBeNull();
                expect(opOoxA1Grammar.getOperatorKey("add")).toBeNull();
                expect(opOoxA1Grammar.getOperatorKey(";")).toBeNull();
                expect(deOoxA1Grammar.getOperatorKey("abc")).toBeNull();
                expect(deOoxA1Grammar.getOperatorKey("add")).toBeNull();
                expect(deOoxA1Grammar.getOperatorKey(",")).toBeNull();
                expect(enOoxA1Grammar.getOperatorKey("abc")).toBeNull();
                expect(enOoxA1Grammar.getOperatorKey("add")).toBeNull();
                expect(enOoxA1Grammar.getOperatorKey(";")).toBeNull();
            });
        });

        describe("method getFunctionName", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("getFunctionName");
            });
            it("should return native function names", () => {
                expect(opOoxA1Grammar.getFunctionName("ABS")).toBe("ABS");
                expect(opOoxA1Grammar.getFunctionName("SUM")).toBe("SUM");
                expect(opOoxA1Grammar.getFunctionName("AGGREGATE")).toBe("_xlfn.AGGREGATE");
                expect(opOdfA1Grammar.getFunctionName("ABS")).toBe("ABS");
                expect(opOdfA1Grammar.getFunctionName("SUM")).toBe("SUM");
                expect(opOdfA1Grammar.getFunctionName("AGGREGATE")).toBe("COM.MICROSOFT.AGGREGATE");
            });
            it("should return localized function names", () => {
                expect(deOoxA1Grammar.getFunctionName("ABS")).toBe("ABS");
                expect(deOoxA1Grammar.getFunctionName("SUM")).toBe("SUMME");
                expect(deOoxA1Grammar.getFunctionName("AGGREGATE")).toBe("AGGREGAT");
                expect(deOdfA1Grammar.getFunctionName("ABS")).toBe("ABS");
                expect(deOdfA1Grammar.getFunctionName("SUM")).toBe("SUMME");
                expect(deOdfA1Grammar.getFunctionName("AGGREGATE")).toBe("AGGREGAT");
            });
            it("should return English function names", () => {
                expect(enOoxA1Grammar.getFunctionName("ABS")).toBe("ABS");
                expect(enOoxA1Grammar.getFunctionName("SUM")).toBe("SUM");
                expect(enOoxA1Grammar.getFunctionName("AGGREGATE")).toBe("AGGREGATE");
                expect(enOdfA1Grammar.getFunctionName("ABS")).toBe("ABS");
                expect(enOdfA1Grammar.getFunctionName("SUM")).toBe("SUM");
                expect(enOdfA1Grammar.getFunctionName("AGGREGATE")).toBe("AGGREGATE");
            });
            it("should return null for invalid keys", () => {
                expect(opOoxA1Grammar.getFunctionName("123")).toBeNull();
                expect(opOoxA1Grammar.getFunctionName("abc")).toBeNull();
                expect(opOoxA1Grammar.getFunctionName("abs")).toBeNull();
                expect(opOoxA1Grammar.getFunctionName("ISLEAPYEAR")).toBeNull();
                expect(deOoxA1Grammar.getFunctionName("123")).toBeNull();
                expect(deOoxA1Grammar.getFunctionName("abc")).toBeNull();
                expect(deOoxA1Grammar.getFunctionName("abs")).toBeNull();
                expect(deOoxA1Grammar.getFunctionName("ISLEAPYEAR")).toBeNull();
                expect(enOoxA1Grammar.getFunctionName("123")).toBeNull();
                expect(enOoxA1Grammar.getFunctionName("abc")).toBeNull();
                expect(enOoxA1Grammar.getFunctionName("abs")).toBeNull();
                expect(enOoxA1Grammar.getFunctionName("ISLEAPYEAR")).toBeNull();
            });
        });

        describe("method getFunctionKey", () => {
            it("should exist", () => {
                expect(FormulaGrammar).toHaveMethod("getFunctionKey");
            });
            it("should parse native function names", () => {
                expect(opOoxA1Grammar.getFunctionKey("ABS")).toBe("ABS");
                expect(opOoxA1Grammar.getFunctionKey("abs")).toBe("ABS");
                expect(opOoxA1Grammar.getFunctionKey("SUM")).toBe("SUM");
                expect(opOoxA1Grammar.getFunctionKey("sum")).toBe("SUM");
                expect(opOoxA1Grammar.getFunctionKey("_xlfn.AGGREGATE")).toBe("AGGREGATE");
                expect(opOdfA1Grammar.getFunctionKey("ABS")).toBe("ABS");
                expect(opOdfA1Grammar.getFunctionKey("abs")).toBe("ABS");
                expect(opOdfA1Grammar.getFunctionKey("SUM")).toBe("SUM");
                expect(opOdfA1Grammar.getFunctionKey("sum")).toBe("SUM");
                expect(opOdfA1Grammar.getFunctionKey("COM.MICROSOFT.AGGREGATE")).toBe("AGGREGATE");
            });
            it("should parse localized function names", () => {
                expect(deOoxA1Grammar.getFunctionKey("ABS")).toBe("ABS");
                expect(deOoxA1Grammar.getFunctionKey("abs")).toBe("ABS");
                expect(deOoxA1Grammar.getFunctionKey("SUMME")).toBe("SUM");
                expect(deOoxA1Grammar.getFunctionKey("summe")).toBe("SUM");
                expect(deOoxA1Grammar.getFunctionKey("AGGREGAT")).toBe("AGGREGATE");
                expect(deOdfA1Grammar.getFunctionKey("ABS")).toBe("ABS");
                expect(deOdfA1Grammar.getFunctionKey("abs")).toBe("ABS");
                expect(deOdfA1Grammar.getFunctionKey("SUMME")).toBe("SUM");
                expect(deOdfA1Grammar.getFunctionKey("summe")).toBe("SUM");
                expect(deOdfA1Grammar.getFunctionKey("AGGREGAT")).toBe("AGGREGATE");
            });
            it("should parse English function names", () => {
                expect(enOoxA1Grammar.getFunctionKey("ABS")).toBe("ABS");
                expect(enOoxA1Grammar.getFunctionKey("abs")).toBe("ABS");
                expect(enOoxA1Grammar.getFunctionKey("SUM")).toBe("SUM");
                expect(enOoxA1Grammar.getFunctionKey("sum")).toBe("SUM");
                expect(enOoxA1Grammar.getFunctionKey("AGGREGATE")).toBe("AGGREGATE");
                expect(enOdfA1Grammar.getFunctionKey("ABS")).toBe("ABS");
                expect(enOdfA1Grammar.getFunctionKey("abs")).toBe("ABS");
                expect(enOdfA1Grammar.getFunctionKey("SUM")).toBe("SUM");
                expect(enOdfA1Grammar.getFunctionKey("sum")).toBe("SUM");
                expect(enOdfA1Grammar.getFunctionKey("AGGREGATE")).toBe("AGGREGATE");
            });
            it("should return null for invalid names", () => {
                expect(opOoxA1Grammar.getFunctionKey("123")).toBeNull();
                expect(opOoxA1Grammar.getFunctionKey("abc")).toBeNull();
                expect(opOoxA1Grammar.getFunctionKey("SUMME")).toBeNull();
                expect(opOoxA1Grammar.getFunctionKey("ISLEAPYEAR")).toBeNull();
                expect(opOoxA1Grammar.getFunctionKey("FORMULATEXT")).toBeNull();
                expect(opOoxA1Grammar.getFunctionKey("FORMELTEXT")).toBeNull();
                expect(deOoxA1Grammar.getFunctionKey("123")).toBeNull();
                expect(deOoxA1Grammar.getFunctionKey("abc")).toBeNull();
                expect(deOoxA1Grammar.getFunctionKey("SUM")).toBeNull();
                expect(deOoxA1Grammar.getFunctionKey("ISTSCHALTJAHR")).toBeNull();
            });
        });
    });
});
