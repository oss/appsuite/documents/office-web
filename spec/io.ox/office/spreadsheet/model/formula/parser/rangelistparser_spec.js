/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/objects";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { RangeArray } from "@/io.ox/office/spreadsheet/utils/rangearray";
import { FormulaResourceFactory } from "@/io.ox/office/spreadsheet/model/formula/resource/formularesource";
import { FormulaGrammar } from "@/io.ox/office/spreadsheet/model/formula/parser/formulagrammar";
import { RangeListParser } from "@/io.ox/office/spreadsheet/model/formula/parser/rangelistparser";

import { ra, MockDocumentAccess } from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/formula/parser/rangelistparser", () => {

    // mock of an OOXML document model
    const ooxDocAccess = new MockDocumentAccess({
        cols: 16384,
        rows: 1048576,
        sheets: ["Sheet1"]
    });

    // mock of an ODF document model
    const odfDocAccess = new MockDocumentAccess({
        cols: 1024,
        rows: 1048576,
        sheets: ["Sheet1"]
    });

    // all parser instances, mapped by a unique key
    const parserMap = new Map();
    beforeAll(async () => {
        const ooxFactory = await FormulaResourceFactory.create("ooxml", LOCALE_DATA);
        const ooxResource = ooxFactory.getResource("std");
        const odfFactory = await FormulaResourceFactory.create("odf", LOCALE_DATA);
        const odfResource = odfFactory.getResource("std");
        parserMap.set("oox:op", RangeListParser.create(FormulaGrammar.create(ooxResource)));
        parserMap.set("odf:op", RangeListParser.create(FormulaGrammar.create(odfResource)));
    });

    // class RangeListParser --------------------------------------------------

    describe("class RangeListParser", () => {
        it("should exist", () => {
            expect(RangeListParser).toBeSubClassOf(DObject);
        });

        describe("static function create", () => {
            it("should exist", () => {
                expect(RangeListParser).toHaveStaticMethod("create");
            });
        });

        describe("method parse", () => {
            it("should exist", () => {
                expect(RangeListParser).toHaveMethod("parse");
            });

            function expectParsed(grammarKey, docAccess, formula, options, expected) {
                if (is.string(options)) { expected = options; options = undefined; }
                const result = parserMap.get(grammarKey).parse(docAccess, formula, options);
                const descr = `formula "${formula}"`;
                if (expected) {
                    expect(result, descr).toBeInstanceOf(RangeArray);
                    expect(result).toStringifyTo(expected);
                } else {
                    expect(result, descr).toBeUndefined();
                }
            }

            const expectOoxOp = expectParsed.bind(null, "oox:op", ooxDocAccess);
            const expectOdfOp = expectParsed.bind(null, "odf:op", odfDocAccess);

            it("should parse simple range lists", () => {
                // native OOXML
                expectOoxOp("B3:C4", "B3:C4");
                expectOoxOp("B3:C4 c4:d5  \tD5:E6", "B3:C4,C4:D5,D5:E6");
                expectOoxOp("B3 c4", "B3:B3,C4:C4");
                expectOoxOp("B:c 3:4", "B1:C1048576,A3:XFD4");
                // native ODF
                expectOdfOp("B3:C4", "B3:C4");
                expectOdfOp("B3:C4 c4:d5  \tD5:E6", "B3:C4,C4:D5,D5:E6");
                expectOdfOp("B3 c4", "B3:B3,C4:C4");
            });

            it("should fail with invalid range lists", () => {
                // native OOXML
                expectOoxOp("abc", undefined);
                expectOoxOp("B3:ZZZZ4", undefined);
                expectOoxOp("B:3", undefined);
                expectOoxOp("", undefined);
                // native ODF
                expectOdfOp("abc", undefined);
                expectOdfOp("B3:ZZZZ4", undefined);
                expectOdfOp("B:3", undefined);
                expectOdfOp("B:C", undefined);
                expectOdfOp("3:4", undefined);
                expectOdfOp("", undefined);
            });

            it("should handle special separators", () => {
                expectOoxOp("B3:C4 C4:D5 D5:E6", "B3:C4,C4:D5,D5:E6");
                expectOoxOp("B3:C4,C4:D5,D5:E6", undefined);
                expectOoxOp("B3:C4;C4:D5;D5:E6", undefined);
                expectOoxOp("B3:C4 C4:D5 D5:E6", { extSep: true }, "B3:C4,C4:D5,D5:E6");
                expectOoxOp("B3:C4,C4:D5,D5:E6", { extSep: true }, "B3:C4,C4:D5,D5:E6");
                expectOoxOp("B3:C4;C4:D5;D5:E6", { extSep: true }, "B3:C4,C4:D5,D5:E6");
                expectOoxOp("B3:C4,C4:D5;D5:E6", { extSep: true }, undefined); // no mixed separators
                expectOoxOp("B3:C4 C4:D5;D5:E6", { extSep: true }, undefined); // no mixed separators
            });

            it("should ignore sheet names if specified", () => {
                // native OOXML
                expectOoxOp("A2 Sheet1!B3", undefined);
                expectOoxOp("A2:B3 Sheet1!B3:C4", undefined);
                expectOoxOp("A:B Sheet1!B:C", undefined);
                expectOoxOp("2:3 Sheet1!3:4", undefined);
                expectOoxOp("A2 Sheet1!B3", { skipSheets: true }, "A2:A2,B3:B3");
                expectOoxOp("A2:B3 Sheet1!B3:C4", { skipSheets: true }, "A2:B3,B3:C4");
                expectOoxOp("A:B Sheet1!B:C", { skipSheets: true }, "A1:B1048576,B1:C1048576");
                expectOoxOp("2:3 Sheet1!3:4", { skipSheets: true }, "A2:XFD3,A3:XFD4");
                expectOoxOp("'Sheet 1'!B3", { skipSheets: true }, "B3:B3");
                expectOoxOp("'Sheet''1'!B3:C4", { skipSheets: true }, "B3:C4");
                expectOoxOp("Sheet-1!B3", { skipSheets: true }, "B3:B3");   // DOCS-4710: accept invalid unquoted sheet names
                // native ODF (no support for column/row intervals)
                expectOdfOp("A2 Sheet1.B3", undefined);
                expectOdfOp("A2:B3 Sheet1.B3:C4", undefined);
                expectOdfOp("A:B Sheet1.B:C", undefined);
                expectOdfOp("2:3 Sheet1.3:4", undefined);
                expectOdfOp("A2 Sheet1.B3", { skipSheets: true }, "A2:A2,B3:B3");
                expectOdfOp("A2:B3 Sheet1.B3:C4 Sheet1.C4:Sheet2.D5", { skipSheets: true }, "A2:B3,B3:C4,C4:D5");
                expectOdfOp("A:B Sheet1.B:C Sheet1.C:Sheet2.D", { skipSheets: true }, undefined);
                expectOdfOp("2:3 Sheet1.3:4 Sheet1.4:Sheet2.5", { skipSheets: true }, undefined);
                expectOdfOp("'Sheet 1'.B3", { skipSheets: true }, "B3:B3");
                expectOdfOp("'Sheet''1'.B3:C4", { skipSheets: true }, "B3:C4");
                expectOdfOp("Sheet-1.B3", { skipSheets: true }, "B3:B3");   // DOCS-4710: accept invalid unquoted sheet names
            });
        });

        describe("method format", () => {
            it("should exist", () => {
                expect(RangeListParser).toHaveMethod("format");
            });

            function expectFormatted(grammarKey, docAccess, ranges, options, expected) {
                if (is.string(options)) { expected = options; options = undefined; }
                expect(parserMap.get(grammarKey).format(docAccess, ranges, options)).toBe(expected);
            }

            const expectOoxOp = expectFormatted.bind(null, "oox:op", ooxDocAccess);
            const expectOdfOp = expectFormatted.bind(null, "odf:op", odfDocAccess);

            it("should format range lists", () => {
                // native OOXML
                expectOoxOp(ra("B3:C4 C4:D5 D5:E6"), "B3:C4 C4:D5 D5:E6");
                expectOoxOp(ra("B3:B3 C4:C4"), "B3 C4");
                expectOoxOp(ra("B1:C1048576 A3:XFD4"), "B:C 3:4");
                // native ODF
                expectOdfOp(ra("B3:C4 C4:D5 D5:E6"), "B3:C4 C4:D5 D5:E6");
                expectOdfOp(ra("B3:B3 C4:C4"), "B3 C4");
                expectOdfOp(ra("B1:C1048576 A3:AMJ4"), "B1:C1048576 A3:AMJ4");
            });

            it("should add sheet names", () => {
                // native OOXML
                expectOoxOp(ra("B3:B3 C4:D5 B1:C1048576 A3:XFD4"), { sheetName: "Sheet1" }, "Sheet1!B3 Sheet1!C4:D5 Sheet1!B:C Sheet1!3:4");
                expectOoxOp(ra("C4:D5"), { sheetName: "Sheet 1" }, "'Sheet 1'!C4:D5");
                // native ODF
                expectOdfOp(ra("B3:B3 C4:D5 B1:C1048576 A3:AMJ4"), { sheetName: "Sheet1" }, "Sheet1.B3 Sheet1.C4:Sheet1.D5 Sheet1.B1:Sheet1.C1048576 Sheet1.A3:Sheet1.AMJ4");
                expectOdfOp(ra("C4:D5"), { sheetName: "Sheet 1" }, "'Sheet 1'.C4:'Sheet 1'.D5");
            });
        });
    });
});
