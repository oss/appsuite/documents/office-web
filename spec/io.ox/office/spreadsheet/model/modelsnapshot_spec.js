/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { DocumentSnapshot } from "@/io.ox/office/spreadsheet/model/modelsnapshot";

import { createSpreadsheetApp } from "~/spreadsheet/apphelper";
import * as op from "~/spreadsheet/apphelper";

// tests ======================================================================

describe("module spreadsheet/model/modelsnapshot", () => {

    // the operations to be applied by the document model
    const OPERATIONS = [
        op.changeConfig({ document: { cols: 16384, rows: 1048576 } }),
        op.insertAutoStyle("a0", true),
        op.insertName("name", { formula: "1" }),
        op.insertSheet(0, "Sheet1"),
        op.insertName("name", 0, { formula: "1" }),
        op.insertSheet(1, "Sheet2"),
        op.insertName("name", 1, { formula: "1" }),
        op.insertName("nameB", 1, { formula: "1" }),
        op.insertName("nameA", 1, { formula: "1" }),
        op.changeCells(1, { A1: "Col1", B1: "Col2" }),
        op.insertTable(1, "Table1", "A1:B2", { table: { headerRow: true, footerRow: true } }),
        op.insertSheet(2, "Sheet3"),
        op.changeCells(2, { B2: "Col4", C2: "Col3" }),
        op.insertTable(2, "Table2", "B2:C3", { table: { headerRow: true } }),
        op.insertSheet(3, "Sheet4")
    ];

    // initialize test document
    let docModel = null;
    createSpreadsheetApp("ooxml", OPERATIONS).done(function (docApp) { docModel = docApp.docModel; });

    // class DocumentSnapshot -------------------------------------------------

    describe("class DocumentSnapshot", () => {

        it("should exist", () => {
            expect(DocumentSnapshot).toBeFunction();
        });

        describe("static function fromModel", () => {
            it("should exist", () => {
                expect(DocumentSnapshot).toHaveStaticMethod("fromModel");
            });
            it("should create a document snapshot", () => {
                const snapshot = DocumentSnapshot.fromModel(docModel);
                expect(snapshot).toBeInstanceOf(DocumentSnapshot);
                expect(snapshot.toJSON()).toEqual({
                    names: ["name"],
                    sheets: [
                        { name: "Sheet1", names: ["name"] },
                        { name: "Sheet2", names: ["name", "nameA", "nameB"], tables: [
                            { name: "Table1", range: "A1:B2", header: true, footer: true, columns: ["COL1", "COL2"] }
                        ] },
                        { name: "Sheet3", tables: [
                            { name: "Table2", range: "B2:C3", header: true, footer: false, columns: ["COL4", "COL3"] }
                        ] },
                        { name: "Sheet4" }
                    ]
                });
            });
        });
    });
});
