/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import SearchHandler from '@/io.ox/office/textframework/components/searchhandler/searchhandler';
import * as Position from '@/io.ox/office/textframework/utils/position';

import { createTextApp } from '~/text/apphelper';

// class Searchhandler ====================================================

describe('Text class SearchHandler', function () {

    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 } } },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', start: [0, 0], text: 'Hello World!' },
        { name: 'insertParagraph', start: [1] },
        { name: 'insertText', start: [1, 0], text: 'Hello Earth!' }
    ];

    var model,
        searchHandler = null,
        firstParagraph,
        secondParagraph;

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        searchHandler = model.getSearchHandler();
        var node = model.getNode();
        firstParagraph = Position.getParagraphElement(node, [0]);
        secondParagraph = Position.getParagraphElement(node, [1]);
    });

    it('should subclass ModelObject', function () {
        expect(SearchHandler).toBeSubClassOf(ModelObject);
    });

    describe('method reset', function () {
        it('should exist', function () {
            expect(SearchHandler).toHaveMethod('reset');
        });
    });

    describe('method removeHighlighting', function () {
        it('should exist', function () {
            expect(SearchHandler).toHaveMethod('removeHighlighting');
        });
    });

    describe('method removeSpansHighlighting', function () {
        it('should exist', function () {
            expect(SearchHandler).toHaveMethod('removeSpansHighlighting');
        });
    });

    describe('method selectNextSearchResult', function () {
        it('should exist', function () {
            expect(SearchHandler).toHaveMethod('selectPreviousSearchResult');
        });
    });

    describe('method selectPreviousSearchResult', function () {
        it('should exist', function () {
            expect(SearchHandler).toHaveMethod('selectPreviousSearchResult');
        });
    });

    describe('method replaceSearchResult', function () {
        it('should exist', function () {
            expect(SearchHandler).toHaveMethod('replaceSearchResult');
        });
    });

    describe('method replaceSelectedSearchResult', function () {
        it('should exist', function () {
            expect(SearchHandler).toHaveMethod('replaceSelectedSearchResult');
        });
    });

    describe('method quickSearch', function () {
        it('should exist', function () {
            expect(SearchHandler).toHaveMethod('quickSearch');
            expect(SearchHandler).toHaveMethod('reset');
            searchHandler.reset();
        });

        it('without query should return false', function () {
            expect(searchHandler.quickSearch()).toContainProps({ count: 0 });
        });

        it('with query "x" should return false', function () {
            expect(searchHandler.quickSearch('x', 0)).toContainProps({ count: 0 });
        });
        it('with query "Hello" should return true', function () {
            expect(searchHandler.quickSearch('Hello', 0)).toContainProps({ count: 2 });
        });

        it('should highlight first span of the two paragraphs', function () {
            expect($(firstParagraph).children('.highlight')).toHaveLength(1);
            expect($(secondParagraph).children('.highlight')).toHaveLength(1);
        });

        it('should not highlight first span of the two paragraphs', function () {
            searchHandler.reset();
            expect($(firstParagraph).children('.highlight')).toHaveLength(0);
            expect($(secondParagraph).children('.highlight')).toHaveLength(0);
        });

        it('search "Hello" and replace it with "001100"', async function () {
            await searchHandler.searchAndReplaceAll('Hello', '001100');
            expect(searchHandler.quickSearch('Hello', 0)).toContainProps({ count: 0 });
            expect(searchHandler.quickSearch('001100', 0)).toContainProps({ count: 2 });
            expect($(firstParagraph).children('.highlight')).toHaveLength(1);
            expect($(secondParagraph).children('.highlight')).toHaveLength(1);
        });
    });

    describe('method searchAndReplaceAll', function () {
        it('should exist', function () {
            expect(SearchHandler).toHaveMethod('searchAndReplaceAll');
        });
    });

    describe('method clearHighlighting', function () {
        it('should exist', function () {
            expect(SearchHandler).toHaveMethod('clearHighlighting');
        });
    });
});

// special test for 'searchAndReplaceAll' and header/footer (DOCS-2773)

describe('Text class Searchhandler (II)', function () {

    var model = null,
        searchHandler = null,
        page = null,
        headerText = 'Hello world in header.',
        para1Text = 'Hello world in document on page 1.',
        para2Text = 'Hello world in document on page 2.',
        searchString = 'world',
        replaceString = 'moon',
        headerTextAfter = 'Hello moon in header.',
        para1TextAfter = 'Hello moon in document on page 1.',
        para2TextAfter = 'Hello moon in document on page 2.',

        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 } } },
            { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' },
            { name: 'insertParagraph', start: [0], target: 'OX_rId100' },
            { name: 'insertText', start: [0, 0], target: 'OX_rId100', text: headerText },
            { name: 'insertParagraph', start: [0] },
            { name: 'insertText', start: [0, 0], text: para1Text },
            { name: 'insertParagraph', start: [1], attrs: { paragraph: { pageBreakBefore: true } } },
            { name: 'insertText', start: [1, 0], text: para2Text }
        ];

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        searchHandler = model.getSearchHandler();
    });

    describe('method searchAndReplaceAll', function () {
        it('should exist', function () {
            expect(SearchHandler).toHaveMethod('searchAndReplaceAll');
        });

        it('should search and replace all occurences of a specified word', async function () {

            page = model.getNode();
            let header1Paragraph = page.find('> .header-wrapper > .header > .marginalcontent > .p');
            let header2Paragraph = page.find('> .pagecontent .page-break > .header > .marginalcontent .p');
            let paragraphs = page.find('> .pagecontent > .p');
            let paragraph1 = $(paragraphs.get(0));
            let paragraph2 = $(paragraphs.get(1));

            expect(paragraph1.text()).toBe(para1Text);
            expect(paragraph2.text()).toBe(para2Text);
            expect(header1Paragraph.text()).toBe(headerText);
            expect(header2Paragraph.text()).toBe(headerText);

            await searchHandler.searchAndReplaceAll(searchString, replaceString);

            header1Paragraph = page.find('> .header-wrapper > .header > .marginalcontent > .p');
            header2Paragraph = page.find('> .pagecontent .page-break > .header > .marginalcontent .p');
            paragraphs = page.find('> .pagecontent > .p');
            paragraph1 = $(paragraphs.get(0));
            paragraph2 = $(paragraphs.get(1));

            expect(paragraph1.text()).toBe(para1TextAfter);
            expect(paragraph2.text()).toBe(para2TextAfter);
            expect(header1Paragraph.text()).toBe(headerTextAfter);
            expect(header2Paragraph.text()).toBe(headerTextAfter);
        });

        it('should not modify the results after one second (DOCS-2773)', async function () {

            await model.executeDelayed(() => null, 1000);

            const header1Paragraph = page.find('> .header-wrapper > .header > .marginalcontent > .p');
            const header2Paragraph = page.find('> .pagecontent .page-break > .header > .marginalcontent .p');
            const paragraphs = page.find('> .pagecontent > .p');
            const paragraph1 = $(paragraphs.get(0));
            const paragraph2 = $(paragraphs.get(1));

            expect(paragraph1.text()).toBe(para1TextAfter);
            expect(paragraph2.text()).toBe(para2TextAfter);
            expect(header1Paragraph.text()).toBe(headerTextAfter);
            expect(header2Paragraph.text()).toBe(headerTextAfter);
        });

        it('should be able to restore the previous document state by undo', async function () {

            await model.getUndoManager().undo();
            await model.executeDelayed(() => null, 1000); // waiting for asynchronous update of header in 'pageLayout.updateEditingHeaderFooter'

            const header1Paragraph = page.find('> .header-wrapper > .header > .marginalcontent > .p');
            const header2Paragraph = page.find('> .pagecontent .page-break > .header > .marginalcontent .p');
            const paragraphs = page.find('> .pagecontent > .p');
            const paragraph1 = $(paragraphs.get(0));
            const paragraph2 = $(paragraphs.get(1));

            expect(paragraph1.text()).toBe(para1Text);
            expect(paragraph2.text()).toBe(para2Text);
            expect(header1Paragraph.text()).toBe(headerText);
            expect(header2Paragraph.text()).toBe(headerText);
        });

        it('should again not modify the results after one more second', async function () {

            await model.executeDelayed(() => null, 1000);

            const header1Paragraph = page.find('> .header-wrapper > .header > .marginalcontent > .p');
            const header2Paragraph = page.find('> .pagecontent .page-break > .header > .marginalcontent .p');
            const paragraphs = page.find('> .pagecontent > .p');
            const paragraph1 = $(paragraphs.get(0));
            const paragraph2 = $(paragraphs.get(1));

            expect(paragraph1.text()).toBe(para1Text);
            expect(paragraph2.text()).toBe(para2Text);
            expect(header1Paragraph.text()).toBe(headerText);
            expect(header2Paragraph.text()).toBe(headerText);
        });

    });

});
