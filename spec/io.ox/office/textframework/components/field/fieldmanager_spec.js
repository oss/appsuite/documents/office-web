/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import { transformOpColor, Color } from '@/io.ox/office/editframework/utils/color';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';

import FieldManager from '@/io.ox/office/textframework/components/field/fieldmanager';
import * as Position from '@/io.ox/office/textframework/utils/position';
import * as DOM from '@/io.ox/office/textframework/utils/dom';

import { createTextApp } from '~/text/apphelper';

// class FieldManager =====================================================

describe('Text class FieldManager', function () {

    it('should subclass ModelObject', function () {
        expect(FieldManager).toBeSubClassOf(ModelObject);
    });

    // private helpers ----------------------------------------------------

    var FILENAMECONST = 'unnamed (1).docx';

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 } } },

        { name: 'insertParagraph', start: [0] },
        { name: 'insertRange', start: [0, 0], type: 'field', position: 'start' },
        { name: 'insertComplexField', start: [0, 1], instruction: 'FILENAME' },
        { name: 'insertText', start: [0, 2], text: FILENAMECONST },
        { name: 'insertRange', start: [0, 18], type: 'field', position: 'end' },
        // checkbox
        { name: 'insertRange', start: [0, 19], type: 'field', position: 'start' },
        { name: 'insertRange', start: [0, 20], type: 'field', position: 'end' },
        { name: 'setAttributes', start: [0, 19], attrs: { character: { field: { formFieldType: 'checkBox', checked: true, enabled: true } } } },
        { name: 'insertRange', start: [0, 21], type: 'field', position: 'start' },
        { name: 'insertRange', start: [0, 22], type: 'field', position: 'end' },
        { name: 'setAttributes', start: [0, 21], attrs: { character: { field: { formFieldType: 'checkBox', checked: false, enabled: true } } } },

        { name: 'insertParagraph', start: [1], attrs: { paragraph: { pageBreakBefore: true } } },
        { name: 'insertParagraph', start: [2], attrs: { paragraph: { pageBreakBefore: true } } }
    ];

    // the ODF operations to be applied by the document model
    var ODF_OPERATIONS = [
        { name: 'insertParagraph', start: [0] },
        { name: 'insertField', start: [0, 0], type: 'date', representation: '1/12/2016', attrs: { character: { field: { dateFormat: 'M/D/YYYY' } } } },
        { name: 'updateField', start: [0, 0], type: 'date', representation: '1/12/2016', attrs: { character: { field: { dateFormat: 'M/D/YYYY', 'text:fixed': 'true' } } } }
    ];

    var model, selection, fieldManager;
    createTextApp('ooxml', OPERATIONS).done(function (app) {
        app.getFullFileName = function () { return FILENAMECONST; };
        model = app.getModel();
        selection = model.getSelection();
        fieldManager = model.getFieldManager();
    });

    var odfModel, odfSelection, odfFieldManager;
    createTextApp('odf', ODF_OPERATIONS).done(function (app) {
        app.getFullFileName = function () { return FILENAMECONST; };
        odfModel = app.getModel();
        odfSelection = odfModel.getSelection();
        odfFieldManager = odfModel.getFieldManager();
    });

    // constructor --------------------------------------------------------

    describe('constructor', function () {
        it('should create a FieldManager class instance', function () {
            expect(fieldManager).toBeInstanceOf(FieldManager);
            expect(odfFieldManager).toBeInstanceOf(FieldManager);
        });
    });

    // public methods -----------------------------------------------------

    describe('method fieldsAreInDocument', function () {
        it('should exist', function () {
            expect(FieldManager).toHaveMethod('fieldsAreInDocument');
        });
        it('should return that there are fields in document', function () {
            expect(fieldManager.fieldsAreInDocument()).toBeTrue();
        });
        it('should return that there are odf fields in document', function () {
            expect(odfFieldManager.fieldsAreInDocument()).toBeTrue();
        });
    });

    // Simple fields tests
    describe('method getSimpleField', function () {
        it('should exist', function () {
            expect(FieldManager).toHaveMethod('getSimpleField');
        });
        it('should return simple odf field node', function () {
            const field = odfFieldManager.getSimpleField('sf0');
            expect(field).toBeInstanceOf($);
            expect(field.hasClass('field')).toBeTrue();
        });
    });

    describe('method isAutomaticDateField for odf', function () {
        it('should return simple odf field node', function () {
            expect(odfFieldManager.isFixedSimpleField(odfFieldManager.getSimpleField('sf0'), true)).toBeTrue();
        });
    });

    describe('testing changind date field to automatic from fixed, for odf', function () {
        beforeAll(function () {
            odfSelection.setTextSelection(odfSelection.getFirstDocumentPosition());
            odfFieldManager.setDateFieldToAutoUpdate(true);
        });
        it('should return that odf field node is not fixed anymore', function () {
            expect(odfFieldManager.isFixedSimpleField(odfFieldManager.getSimpleField('sf0'), true)).toBeFalse();
        });
    });

    // Complex field tests
    describe('method isComplexFieldId', function () {
        it('should exist', function () {
            expect(FieldManager).toHaveMethod('isComplexFieldId');
        });
        it('should return that asked id is id of complex field', function () {
            expect(fieldManager.isComplexFieldId('fld0')).toBeTrue();
        });
    });

    describe('method getComplexField', function () {
        it('should exist', function () {
            expect(FieldManager).toHaveMethod('getComplexField');
        });
        it('should return complex field node', function () {
            const field = fieldManager.getComplexField('fld0');
            expect(field).toBeInstanceOf($);
            expect(field.hasClass('complexfield')).toBeTrue();
        });
    });

    describe('method isHighlightState', function () {
        beforeAll(function () {
            selection.setTextSelection(selection.getFirstDocumentPosition());
        });
        it('should exist', function () {
            expect(FieldManager).toHaveMethod('isHighlightState');
        });
        it('should return that complex field is highlighted', function () {
            expect(fieldManager.isHighlightState()).toBeTrue();
        });
    });

    describe('method getSelectedFieldFormat', function () {
        beforeAll(function () {
            selection.setTextSelection(selection.getFirstDocumentPosition());
        });
        it('should exist', function () {
            expect(FieldManager).toHaveMethod('getSelectedFieldFormat');
        });
        it('should return default complex field format of Filename field', function () {
            expect(fieldManager.getSelectedFieldFormat()).toBe("default");
        });
    });

    describe('method updateFieldFormatting', function () {

        it('should exist', function () {
            expect(FieldManager).toHaveMethod('updateFieldFormatting');
        });

        it('should return new, changed complex field format', async function () {

            selection.setTextSelection(selection.getFirstDocumentPosition());
            await fieldManager.updateFieldFormatting('Upper'); // updating the field with new format
            selection.setTextSelection(selection.getFirstDocumentPosition());

            expect(fieldManager.getSelectedFieldFormat()).toBe("Upper");
        });

    });

    describe('Checking the rendering of checkboxes inside rangestart marker', function () {
        it('should render checkbox character with state "checked"', function () {
            var rangemarkerStartNodePoint = Position.getDOMPosition(model.getNode(), [0, 19], true);

            expect(rangemarkerStartNodePoint).not.toBeNull();
            expect(rangemarkerStartNodePoint.node).not.toBeNull();
            expect(DOM.isRangeStartWithCheckboxAttr(rangemarkerStartNodePoint.node)).toBeTrue();
            expect(DOM.isRangeStartWithCheckboxEnabledState(rangemarkerStartNodePoint.node)).toBeTrue();
        });

        it('should render checkbox character with state "unchecked"', function () {
            var rangemarkerStartNodePoint = Position.getDOMPosition(model.getNode(), [0, 21], true);

            expect(rangemarkerStartNodePoint).not.toBeNull();
            expect(rangemarkerStartNodePoint.node).not.toBeNull();
            expect(DOM.isRangeStartWithCheckboxAttr(rangemarkerStartNodePoint.node)).toBeTrue();
            expect(DOM.isRangeStartWithCheckboxEnabledState(rangemarkerStartNodePoint.node)).toBeFalse();
        });
    });

    describe('method dispatchInsertField for pageNum field on first page', function () {
        beforeAll(function () {
            selection.setTextSelection(selection.getFirstDocumentPosition());
            fieldManager.dispatchInsertField('page', 'default');
            selection.setTextSelection(selection.getFirstDocumentPosition());
        });
        it('should exist', function () {
            expect(FieldManager).toHaveMethod('dispatchInsertField');
        });
        it('should return newly inserted complex field with id fld3', function () {
            expect(fieldManager.getSelectedFieldFormat()).toBe("default");
            const field = fieldManager.getSelectedFieldNode();
            expect(field).toBeInstanceOf($);
            expect(field.hasClass('complexfield')).toBeTrue();
            expect(field.attr('data-field-id')).toBe("fld3");
            expect(field.next().text()).toBe("1");
        });
    });

    describe('method dispatchInsertField for pageNum field on last page', function () {
        beforeAll(function () {
            var lastPosCached = selection.getLastDocumentPosition();
            selection.setTextSelection(lastPosCached);
            fieldManager.dispatchInsertField('page', 'ALPHABETIC');
            selection.setTextSelection(lastPosCached);
        });
        it('should return newly inserted complex field with id fld4 and ALPHABETIC format', function () {
            expect(fieldManager.getSelectedFieldFormat()).toBe("ALPHABETIC");
            const field = fieldManager.getSelectedFieldNode();
            expect(field).toBeInstanceOf($);
            expect(field.hasClass('complexfield')).toBeTrue();
            expect(field.attr('data-field-id')).toBe("fld4");
            expect(field.next().text()).toBe("C"); // page 3 -> C in alphabetic upper format
        });
    });
});

describe('Text class FieldManager: update TOC.', function () {

    // private helpers ----------------------------------------------------

    // update toc field instructions
    var instruction1 = 'TOC \\o "1-3" \\h';
    var instruction2 = 'TOC \\o "5-7" \\n 6-7 \\p "_"';

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 } } },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading1', styleName: 'heading·1', attrs: { character: { bold: true, fontName: 'Times·New·Roman', fontSize: 14, color: transformOpColor(Color.ACCENT1, { shade: 74902 }) }, paragraph: { marginTop: 846, outlineLevel: 0, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading2', styleName: 'heading·2', attrs: { character: { bold: true, fontName: 'Times·New·Roman', fontSize: 13, color: Color.ACCENT1 }, paragraph: { marginTop: 352, outlineLevel: 1, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },

        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading4', styleName: 'heading·4', attrs: { character: { bold: true, italic: true, fontName: 'Times·New·Roman', color: Color.ACCENT1 }, paragraph: { marginTop: 352, outlineLevel: 3, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading5', styleName: 'heading·5', attrs: { character: { bold: true, italic: true, fontName: 'Times·New·Roman', color: Color.ACCENT1 }, paragraph: { marginTop: 352, outlineLevel: 4, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading6', styleName: 'heading·6', attrs: { character: { bold: true, italic: true, fontName: 'Times·New·Roman', color: Color.ACCENT1 }, paragraph: { marginTop: 352, outlineLevel: 5, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading7', styleName: 'heading·7', attrs: { character: { bold: true, italic: true, fontName: 'Times·New·Roman', color: Color.ACCENT1 }, paragraph: { marginTop: 352, outlineLevel: 6, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },

        // TOC field
        { name: 'insertParagraph', start: [0], attrs: { paragraph: { tabStops: [{ value: 'right', pos: 15984, fillChar: 'dot' }] } } },
        { name: 'insertRange', start: [0, 0], type: 'field', position: 'start' },
        { name: 'insertComplexField', start: [0, 1], instruction: instruction1 },
        { name: 'insertText', start: [0, 2], text: 'Test text 1' },
        { name: 'insertRange', start: [0, 13], type: 'field', position: 'end' },

        // content for toc
        { name: 'insertParagraph', start: [1], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading1' } },
        { name: 'insertText', start: [1, 0], text: 'Aaa' },
        { name: 'insertParagraph', start: [2], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading4' } },
        { name: 'insertText', start: [2, 0], text: 'Bbb' },
        { name: 'insertParagraph', start: [3], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading2' } },
        { name: 'insertText', start: [3, 0], text: 'Ccc' },

        // second TOC field
        { name: 'insertParagraph', start: [4] },
        { name: 'insertParagraph', start: [5], attrs: { paragraph: { tabStops: [{ value: 'right', pos: 15984, fillChar: 'dot' }] } } },
        { name: 'insertRange', start: [5, 0], type: 'field', position: 'start' },
        { name: 'insertComplexField', start: [5, 1], instruction: instruction2 },
        { name: 'insertText', start: [5, 2], text: 'Test text 1' },
        { name: 'insertRange', start: [5, 13], type: 'field', position: 'end' },

        // content for second toc
        { name: 'insertParagraph', start: [6], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading5' } },
        { name: 'insertText', start: [6, 0], text: 'Second Aaa' },
        { name: 'insertParagraph', start: [7], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading6' } },
        { name: 'insertText', start: [7, 0], text: 'Second Bbb' },
        { name: 'insertParagraph', start: [8], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading7' } },
        { name: 'insertText', start: [8, 0], text: 'Second Ccc' },

        { name: 'insertParagraph', start: [9] }
    ];

    var model, selection, fieldManager;
    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        selection = model.getSelection();
        fieldManager = model.getFieldManager();
    });

    // constructor --------------------------------------------------------

    describe('constructor', function () {
        it('should create a FieldManager class instance', function () {
            expect(fieldManager).toBeInstanceOf(FieldManager);
        });
    });

    // public methods -----------------------------------------------------

    describe('method updateTableOfContents for first TOC in document', function () {

        beforeAll(function () {
            selection.setTextSelection([0, 2]);
            var node = fieldManager.getSelectedFieldNode();
            return fieldManager.dispatchUpdateTOC(node, instruction1);
        });

        it('should update first TOC correctly', function () {
            var par1 = Position.getParagraphElement(model.getNode(), [0]);
            var par2 = Position.getParagraphElement(model.getNode(), [1]);
            expect($(par1).text()).toBe("Aaa2");
            expect($(par2).text()).not.toBe("Bbb3");
            expect($(par2).text()).toBe("Ccc4");
            var charAttrs = getExplicitAttributes($(par2).children('span').first(), 'character', true);
            expect(charAttrs.anchor).toMatch(/./);
        });
    });

    describe('method updateTableOfContents for second TOC in document', function () {

        beforeAll(function () {
            selection.setTextSelection([7, 2]);
            var node = fieldManager.getSelectedFieldNode();
            return fieldManager.dispatchUpdateTOC(node, instruction2);
        });

        it('should update second TOC correctly', function () {
            var par1 = Position.getParagraphElement(model.getNode(), [7]);
            var par2 = Position.getParagraphElement(model.getNode(), [8]);
            var par3 = Position.getParagraphElement(model.getNode(), [9]);
            expect($(par1).text()).toBe("Second Aaa_5");
            expect($(par2).text()).toBe("Second Bbb");
            expect($(par3).text()).toBe("Second Ccc");
            var charAttrs = getExplicitAttributes($(par2).children('span').first(), 'character', true);
            expect(charAttrs.anchor).toBeUndefined();
        });

        it('should update second TOC correctly after heading text deletion', async function () {

            selection.setTextSelection([13, 8], [13, 10]);
            await model.deleteSelected();

            selection.setTextSelection([7, 2]);
            await fieldManager.updateHighlightedField();

            const par = Position.getParagraphElement(model.getNode(), [9]);
            expect($(par).text()).toBe("Second C");
        });
    });
});

describe('Text class FieldManager: insert TOC.', function () {
    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 } } },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading1', styleName: 'heading·1', attrs: { character: { bold: true, fontName: 'Times·New·Roman', fontSize: 14, color: transformOpColor(Color.ACCENT1, { shade: 74902 }) }, paragraph: { marginTop: 846, outlineLevel: 0, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading2', styleName: 'heading·2', attrs: { character: { bold: true, fontName: 'Times·New·Roman', fontSize: 13, color: Color.ACCENT1 }, paragraph: { marginTop: 352, outlineLevel: 1, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading3', styleName: 'heading·3', attrs: { character: { bold: true, fontName: 'Times·New·Roman', color: Color.ACCENT1 }, paragraph: { marginTop: 352, outlineLevel: 2, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading4', styleName: 'heading·4', attrs: { character: { bold: true, italic: true, fontName: 'Times·New·Roman', color: Color.ACCENT1 }, paragraph: { marginTop: 352, outlineLevel: 3, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading5', styleName: 'heading·5', attrs: { character: { bold: true, italic: true, fontName: 'Times·New·Roman', color: Color.ACCENT1 }, paragraph: { marginTop: 352, outlineLevel: 4, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading6', styleName: 'heading·6', attrs: { character: { bold: true, italic: true, fontName: 'Times·New·Roman', color: Color.ACCENT1 }, paragraph: { marginTop: 352, outlineLevel: 5, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading7', styleName: 'heading·7', attrs: { character: { bold: true, italic: true, fontName: 'Times·New·Roman', color: Color.ACCENT1 }, paragraph: { marginTop: 352, outlineLevel: 6, nextStyleId: 'Normal' } }, parent: 'Normal', uiPriority: 9 },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertParagraph', start: [1] },

        // content for toc
        { name: 'insertParagraph', start: [2], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading1' } },
        { name: 'insertText', start: [2, 0], text: 'Aaa' },
        { name: 'insertParagraph', start: [3], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading4' } },
        { name: 'insertText', start: [3, 0], text: 'Bbb' },
        { name: 'insertParagraph', start: [4], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading2' } },
        { name: 'insertText', start: [4, 0], text: 'Ccc' },

        { name: 'insertParagraph', start: [5] },

        // content for second toc
        { name: 'insertParagraph', start: [6], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading5' } },
        { name: 'insertText', start: [6, 0], text: 'Second Aaa' },
        { name: 'insertParagraph', start: [7], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading6' } },
        { name: 'insertText', start: [7, 0], text: 'Second Bbb' },
        { name: 'insertParagraph', start: [8], attrs: { paragraph: { pageBreakBefore: true }, styleId: 'Heading7' } },
        { name: 'insertText', start: [8, 0], text: 'Second Ccc' },

        { name: 'insertParagraph', start: [9] }
    ];

    var model, selection, fieldManager, globalApp;
    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        selection = model.getSelection();
        fieldManager = model.getFieldManager();
        globalApp = app;
    });

    describe('method insertTableOfContents with page numbers and dots in tab stops', function () {

        beforeAll(function () {
            selection.setTextSelection(selection.getLastDocumentPosition());
            return fieldManager.dispatchInsertToc(1);
        });

        it('should exist', function () {
            expect(FieldManager).toHaveMethod('dispatchInsertToc');
        });

        it('should insert new Table of Contents with page numbers and dots in tab stops', function () {
            var toc1FieldNode = fieldManager.getComplexField('fld0');
            var firstTextSpanInField = toc1FieldNode.next();
            var secondTextSpanInField = firstTextSpanInField.next().next(); // tabStop node is in between
            var paraAttrs = model.paragraphStyles.getElementAttributes(toc1FieldNode.parent());

            expect(fieldManager.getCountOfAllFields()).toBe(1);
            expect(toc1FieldNode).toHaveLength(1);
            expect(DOM.getComplexFieldInstruction(toc1FieldNode)).toBe("TOC \\o '1-3' \\h \\u");
            expect(firstTextSpanInField.text()).toBe("Aaa");
            expect(paraAttrs).toBeObject();
            expect(paraAttrs.paragraph).toBeObject();
            expect(paraAttrs.paragraph.tabStops).toBeArray();
            expect(paraAttrs.paragraph.tabStops[0].fillChar).toBe("dot");
            expect(secondTextSpanInField.text()).toBe("2"); // Heading1 is on second page (one pageBreakBefore property before it)
        });
    });

    describe('method insertTableOfContents with page numbers and tab stops with fill type "none"', function () {

        beforeAll(function () {
            selection.setTextSelection(selection.getLastDocumentPosition());
            return fieldManager.dispatchInsertToc(2);
        });

        it('should insert new Table of Contents with page numbers and tab stops with fill type "none"', function () {
            var toc2FieldNode = fieldManager.getComplexField('fld1');
            var firstTextSpanInField = toc2FieldNode.next();
            var secondTextSpanInField = firstTextSpanInField.next().next(); // tabStop node is in between
            var paraAttrs = model.paragraphStyles.getElementAttributes(toc2FieldNode.parent());

            expect(fieldManager.getCountOfAllFields()).toBe(2);
            expect(toc2FieldNode).toHaveLength(1);
            expect(DOM.getComplexFieldInstruction(toc2FieldNode)).toBe("TOC \\o '1-3' \\h \\u");
            expect(firstTextSpanInField.text()).toBe("Aaa");
            expect(paraAttrs).toBeObject();
            expect(paraAttrs.paragraph).toBeObject();
            expect(paraAttrs.paragraph.tabStops).toBeArray();
            expect(paraAttrs.paragraph.tabStops[0].fillChar).toBe("none");
            expect(secondTextSpanInField.text()).toBe("2"); // Heading1 is on second page (one pageBreakBefore property before it)
        });
    });

    describe('method insertTableOfContents only with headings', function () {

        beforeAll(function () {
            selection.setTextSelection(selection.getLastDocumentPosition());
            return fieldManager.dispatchInsertToc(3);
        });

        it('should insert new Table of Contents only with headings', function () {
            var toc3FieldNode = fieldManager.getComplexField('fld2');
            var firstTextSpanInField = toc3FieldNode.next();
            var secondTextSpanInField = firstTextSpanInField.next().next(); // tabStop node is in between

            expect(fieldManager.getCountOfAllFields()).toBe(3);
            expect(toc3FieldNode).toHaveLength(1);
            expect(DOM.getComplexFieldInstruction(toc3FieldNode)).toBe("TOC \\o '1-3' \\n \\h \\u");
            expect(firstTextSpanInField.text()).toBe("Aaa");
            expect(secondTextSpanInField.text()).toBe(""); // Heading1 is on second page (one pageBreakBefore property before it)
        });
    });

    describe('method insertTableOfContents without headings', function () {

        beforeAll(function () {
            selection.setTextSelection(selection.getFirstDocumentPosition(), selection.getLastDocumentPosition());
            return model.deleteSelected().then(function () {
                selection.setTextSelection(selection.getLastDocumentPosition());
                return fieldManager.dispatchInsertToc(3);
            });
        });

        it('should insert new Table of Contents without headings', function () {
            var toc3FieldNode = fieldManager.getComplexField('fld3');

            // OT:
            // Synchronous validation of paragraph node required, because 'implInsertText'
            // calls 'implParagraphChanged' (and therefore 'validateParagraphNode')
            // asynchronously, if OT is enabled. In this case 'validateParagraphNode'
            // would not be called fast enough and therefore there would be an empty
            // text span in front of the span with the text 'No table of contents entries
            // in the document.'.
            if (globalApp.isOTEnabled()) { model.validateParagraphNode(toc3FieldNode.parent()); }

            var firstTextSpanInField = toc3FieldNode.next();

            expect(fieldManager.getCountOfAllFields()).toBe(1);
            expect(toc3FieldNode).toHaveLength(1);
            expect(DOM.getComplexFieldInstruction(toc3FieldNode)).toBe("TOC \\o '1-3' \\n \\h \\u");
            expect(firstTextSpanInField.text()).toBe("No table of contents entries in the document."); // TODO: It is the second span  (OT: .skip)
        });

    });
});
