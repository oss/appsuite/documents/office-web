/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { createPresentationApp } from '~/presentation/apphelper';
import { calculateCropOperation, drawCropFrame, getDrawingMoveCropOp, getDrawingResizeCropOp } from '@/io.ox/office/textframework/components/drawing/imagecropframe';
import { ACTIVE_CROPPING_CLASS } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';

// class ImageCropFrame ===================================================

describe('Textframework class ImageCropFrame', function () {

    // private helpers ----------------------------------------------------

    var //  the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { page: { width: 33866, height: 19050, orientation: 'landscape' } } },
            { name: 'insertMasterSlide', id: 'masterId_1' },
            { name: 'insertLayoutSlide', id: 'layoutId_1', target: 'masterId_1' },
            { name: 'insertSlide', start: [0], target: 'layoutId_1' },
            { name: 'insertDrawing', start: [0, 0], type: 'image', attrs: { drawing: { id: '4000010', name: 'image1', width: 6000, height: 4000, top: 0, left: 0, aspectLocked: true }, image: { imageUrl: '' }, line: { type: 'none' } } }
        ],

        model = null,
        selection = null;

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        selection = model.getSelection();
    });

    // public methods -----------------------------------------------------

    describe('method drawCropFrame', function () {

        it('should exist', function () {
            expect(drawCropFrame).toBeFunction();
        });

        it('should enter correct crop mode through models handleDrawingCrop, using drawCropFrame', function () {
            selection.setTextSelection([0, 0], [0, 1]);
            var drawing = selection.getSelectedDrawing();
            expect(drawing.hasClass(ACTIVE_CROPPING_CLASS)).toBeFalse();
            model.handleDrawingCrop(true);
            expect(drawing.hasClass(ACTIVE_CROPPING_CLASS)).toBeTrue();
            model.handleDrawingCrop(false);
            expect(drawing.hasClass(ACTIVE_CROPPING_CLASS)).toBeFalse();

        });
    });

    describe('method getDrawingMoveCropOp', function () {

        it('should exist', function () {
            expect(getDrawingMoveCropOp).toBeFunction();
        });

        it('should calculate correct crop values on drawing move event', function () {
            selection.setTextSelection([0, 0], [0, 1]);
            var drawing = selection.getSelectedDrawing();
            // simulating image node, as no url is provided with operation, and no url would work
            // 453px => 12000 hmm
            // 302px => 8000 hmm
            drawing.find('.placeholder').replaceWith($('<div class="content" style=""><div class="cropping-frame"><img src="" style="left: 0px; top: 0px; width: 453px; height: 302px; position: absolute;"></div></div>'));
            model.handleDrawingCrop(true);
            var oldAttrs = getExplicitAttributes(drawing, 'drawing');
            var newAttrs = { left: 0, top: 0 };
            var cropProps = getDrawingMoveCropOp(drawing, oldAttrs, newAttrs);
            expect(cropProps.cropLeft).toBe(-0);
            expect(cropProps.cropRight).toBe(50); // as the image is 50% bigger from the frame, width 12000 / 6000
            expect(cropProps.cropTop).toBe(-0);
            expect(cropProps.cropBottom).toBe(50); // as the image is 50% bigger from the frame, height 8000 / 4000

            newAttrs = { left: 1000, top: 1000 }; // simulate move 1000hmm to right and bottom
            cropProps = getDrawingMoveCropOp(drawing, oldAttrs, newAttrs);
            expect(cropProps.cropLeft).toBe(8); // 1000/6000 = 16% * 50% = 8%
            expect(cropProps.cropRight).toBe(42); // 50% - 8%
            expect(cropProps.cropTop).toBe(13); // 1000/4000 = 25%* 50% = ~13%
            expect(cropProps.cropBottom).toBe(37); // 50% - 13%

            newAttrs = { left: 2000, top: 2000 }; // simulate move 1000hmm to right and bottom
            cropProps = getDrawingMoveCropOp(drawing, oldAttrs, newAttrs);
            expect(cropProps.cropLeft).toBe(17); // 2000/6000 = 33% * 50% = ~17%
            expect(cropProps.cropRight).toBe(33); // 50% - 17%
            expect(cropProps.cropTop).toBe(25); // 2000/4000 = 50%* 50% = ~25%
            expect(cropProps.cropBottom).toBe(25); // 50% - 25%
        });
    });

    describe('method getDrawingResizeCropOp', function () {

        it('should exist', function () {
            expect(getDrawingResizeCropOp).toBeFunction();
        });

        it('should calculate correct crop values on drawing resize event', function () {
            selection.setTextSelection([0, 0], [0, 1]);
            var drawing = selection.getSelectedDrawing();
            // simulating image node, as no url is provided with operation, and no url would work
            drawing.find('.placeholder').replaceWith($('<div class="content" style=""><div class="cropping-frame"><img src="" style="left: 0px; top: 0px; width: 453px; height: 302px; position: absolute;"></div></div>'));
            model.handleDrawingCrop(true);

            var oldAttrs = getExplicitAttributes(drawing, 'drawing');
            var newAttrs = { left: 0, top: 0 };
            var useLeft = false, useTop = false;
            var cropProps = getDrawingResizeCropOp(drawing, oldAttrs, newAttrs, useLeft, useTop);
            expect(cropProps.cropLeft).toBe(-0);
            expect(cropProps.cropRight).toBe(50); // as the image is 50% bigger from the frame, width 12000 / 6000
            expect(cropProps.cropTop).toBe(-0);
            expect(cropProps.cropBottom).toBe(50); // as the image is 50% bigger from the frame, height 8000 / 4000

            newAttrs = { width: 9000 };
            cropProps = getDrawingResizeCropOp(drawing, oldAttrs, newAttrs, useLeft, useTop);
            expect(cropProps.cropLeft).toBe(-0);
            expect(cropProps.cropRight).toBe(25);
            expect(cropProps.cropTop).toBe(-0);
            expect(cropProps.cropBottom).toBe(50); // as the image is 50% bigger from the frame, height 8000 / 4000

            newAttrs = { width: 12000, height: 6000 };
            cropProps = getDrawingResizeCropOp(drawing, oldAttrs, newAttrs, useLeft, useTop);
            expect(cropProps.cropLeft).toBe(-0);
            expect(cropProps.cropRight).toBe(-0);
            expect(cropProps.cropTop).toBe(-0);
            expect(cropProps.cropBottom).toBe(25);

            newAttrs = { width: 12000, height: 8000 };
            cropProps = getDrawingResizeCropOp(drawing, oldAttrs, newAttrs, useLeft, useTop);
            expect(cropProps.cropLeft).toBe(-0);
            expect(cropProps.cropRight).toBe(-0);
            expect(cropProps.cropTop).toBe(-0);
            expect(cropProps.cropBottom).toBe(-0);
        });
    });

    describe('method calculateCropOperation', function () {

        it('should exist', function () {
            expect(calculateCropOperation).toBeFunction();
        });

        it('should calculate correct crop values basted on dialog fields', function () {
            // calculateCropOperation(drawingWidth, drawingHeight, frameWidth, frameHeight, offsetX, offsetY);
            var cropProps = calculateCropOperation(6000, 4000, 6000, 4000, 0, 0);

            expect(cropProps.cropLeft).toBe(0);
            expect(cropProps.cropRight).toBe(0);
            expect(cropProps.cropTop).toBe(0);
            expect(cropProps.cropBottom).toBe(0);

            cropProps = calculateCropOperation(6000, 4000, 12000, 8000, 0, 0);
            // dialog values are centered and with offset.
            // In this case img is 2x bigger from the frame and centered => 25% in all directions
            expect(cropProps.cropLeft).toBe(25);
            expect(cropProps.cropRight).toBe(25);
            expect(cropProps.cropTop).toBe(25);
            expect(cropProps.cropBottom).toBe(25);

            cropProps = calculateCropOperation(6000, 4000, 12000, 8000, 3000, 0);
            // dialog values are centered and with offset.
            // In this case img is 2x bigger from the frame and aligned to left => left 0, right 50%
            expect(cropProps.cropLeft).toBe(0);
            expect(cropProps.cropRight).toBe(50);
            expect(cropProps.cropTop).toBe(25);
            expect(cropProps.cropBottom).toBe(25);

            cropProps = calculateCropOperation(6000, 4000, 12000, 8000, -3000, 0);
            // dialog values are centered and with offset.
            // In this case img is 2x bigger from the frame and aligned to right => left 50%, right 0%
            expect(cropProps.cropLeft).toBe(50);
            expect(cropProps.cropRight).toBe(0);
            expect(cropProps.cropTop).toBe(25);
            expect(cropProps.cropBottom).toBe(25);

            cropProps = calculateCropOperation(6000, 4000, 12000, 8000, 3000, 2000);
            // dialog values are centered and with offset.
            // In this case img is 2x bigger from the frame
            // and aligned to left and top => left 0, right 50%, top: 0%, bottom: 50%
            expect(cropProps.cropLeft).toBe(0);
            expect(cropProps.cropRight).toBe(50);
            expect(cropProps.cropTop).toBe(0);
            expect(cropProps.cropBottom).toBe(50);

            cropProps = calculateCropOperation(6000, 4000, 12000, 8000, -1500, -1000);

            expect(cropProps.cropLeft).toBe(37.5);
            expect(cropProps.cropRight).toBe(12.5);
            expect(cropProps.cropTop).toBe(37.5);
            expect(cropProps.cropBottom).toBe(12.5);
        });
    });

});
