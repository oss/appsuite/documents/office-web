/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import PageLayout from '@/io.ox/office/textframework/components/pagelayout/pagelayout';

import { createTextApp } from '~/text/apphelper';

// class PageLayout =======================================================

describe('Text class PageLayout', function () {

    it('should subclass ModelObject', function () {
        expect(PageLayout).toBeSubClassOf(ModelObject);
    });

    // private helpers ----------------------------------------------------
    var REPETITIVE_TEXT1 = 'Repetitive text in first cell';
    var REPETITIVE_TEXT2 = 'Repetitive text in second cell';

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 } } },
        { name: 'insertParagraph', start: [0] },
        { name: 'splitParagraph', start: [0, 0] },
        { name: 'insertText', text: 'Hello World', start: [1, 0] },
        { name: 'splitParagraph', start: [1, 0] },
        { name: 'setAttributes', start: [2], attrs: { paragraph: { pageBreakBefore: true } } },
        { name: 'splitParagraph', start: [2, 0] },
        { name: 'setAttributes', start: [3], attrs: { paragraph: { pageBreakBefore: true } } },

        { name: 'insertTable', start: [4], attrs: { styleId: 'TableGrid', table: { tableGrid: [7958, 7958], width: 15919, exclude: ['lastRow', 'lastCol', 'bandsVert'] } } },
        { name: 'insertRows', start: [4, 0], attrs: { row: { headerRow: true, height: 10000 } } },
        { name: 'insertCells', start: [4, 0, 0] },
        { name: 'insertParagraph', start: [4, 0, 0, 0] },
        { name: 'insertText', start: [4, 0, 0, 0, 0], text: REPETITIVE_TEXT1 },
        { name: 'insertCells', start: [4, 0, 1] },
        { name: 'insertParagraph', start: [4, 0, 1, 0] },
        { name: 'insertText', start: [4, 0, 1, 0, 0], text: REPETITIVE_TEXT2 },
        { name: 'insertRows', start: [4, 1], attrs: { row: { height: 10052 } } },
        { name: 'insertCells', start: [4, 1, 0] },
        { name: 'insertParagraph', start: [4, 1, 0, 0] },
        { name: 'insertCells', start: [4, 1, 1] },
        { name: 'insertParagraph', start: [4, 1, 1, 0] },
        { name: 'insertRows', start: [4, 2], attrs: { row: { height: 6455 } } },
        { name: 'insertCells', start: [4, 2, 0] },
        { name: 'insertParagraph', start: [4, 2, 0, 0] },
        { name: 'insertCells', start: [4, 2, 1] },
        { name: 'insertParagraph', start: [4, 2, 1, 0] }

    ];

    var model, pageLayout;
    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        pageLayout = model.getPageLayout();
        // $('#io-ox-office-temp').append(model.getNode()); // appending '.page' to the DOM, for correct height computations -> superfluous for JSDom
    });

    describe('method getHeaderFooterPlaceHolder', function () {
        it('should exist', function () {
            expect(pageLayout).toRespondTo('getHeaderFooterPlaceHolder');
        });
        it('should return headerFooter placeholder node', function () {
            expect(pageLayout.getHeaderFooterPlaceHolder()).toBeInstanceOf($);
        });
    });

    describe('method getallHeadersAndFooters', function () {
        it('should exist', function () {
            expect(pageLayout).toRespondTo('getallHeadersAndFooters');
        });
        it('should return all headers and footers in document', function () {
            expect(pageLayout.getallHeadersAndFooters()).toBeInstanceOf($);
        });
    });

    describe('group of methods with pre-requirement of initial page breaks called', function () {
        beforeAll(function () {
            pageLayout.callInitialPageBreaks();
        });

        describe('method getPageNumber', function () {
            it('should exist', function () {
                expect(pageLayout).toRespondTo('getPageNumber');
            });
            it('should return the current page number of the selection', function () {
                expect(pageLayout.getPageNumber()).toBe(1);
            });
        });

        describe('existence of first and last header/footer wrappers', function () {
            it('should return that first header wrapper exists in document', function () {
                expect(pageLayout.isExistingFirstHeaderWrapper()).toBeTrue();
            });
            it('should return that last footer wrapper exists in document', function () {
                expect(pageLayout.isExistingLastFooterWrapper()).toBeTrue();
            });

        });

        describe('method getPageAttribute', function () {
            it('should exist', function () {
                expect(pageLayout).toRespondTo('getPageAttribute');
            });
            it('should return that first page header is not set in document', function () {
                expect(pageLayout.getPageAttribute('firstPage')).toBeFalse();
            });
            it('should return that even/odd pages headers are not set in document', function () {
                expect(pageLayout.getPageAttribute('evenOddPages')).toBeFalse();
            });
            it('should return that default document orientation is set to portrait mode', function () {
                expect(pageLayout.getPageAttribute('orientation')).toBe("portrait");
                expect(pageLayout.getPageAttribute('orientation')).not.toBe("landscape");
            });
        });

        describe('method getNumberOfDocumentPages', function () {
            it('should exist', function () {
                expect(pageLayout).toRespondTo('getNumberOfDocumentPages');
            });
        });

    });

    describe('method getPageAttribute', function () {
        it('should exist', function () {
            expect(pageLayout).toRespondTo('getPageAttribute');
        });
        it('should return that first page header is not set in document', function () {
            expect(pageLayout.getPageAttribute('firstPage')).toBeFalse();
        });
        it('should return that even/odd pages headers are not set in document', function () {
            expect(pageLayout.getPageAttribute('evenOddPages')).toBeFalse();
        });
        it('should return that default document orientation is set to portrait mode', function () {
            expect(pageLayout.getPageAttribute('orientation')).toBe("portrait");
            expect(pageLayout.getPageAttribute('orientation')).not.toBe("landscape");
        });
    });

});

describe('Text class PageLayout, simplified file', function () {

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 } } },
        { name: 'insertParagraph', start: [0] },
        { name: 'splitParagraph', start: [0, 0] },
        { name: 'insertText', text: 'Hello World', start: [1, 0] },
        { name: 'splitParagraph', start: [1, 0] },
        { name: 'setAttributes', start: [2], attrs: { paragraph: { pageBreakBefore: true } } },
        { name: 'splitParagraph', start: [2, 0] },
        { name: 'setAttributes', start: [3], attrs: { paragraph: { pageBreakBefore: true } } }
    ];

    var model, pageLayout;
    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        pageLayout = model.getPageLayout();
    });

    // public methods -----------------------------------------------------

    describe('group of methods with pre-requirement of initial page breaks called', function () {
        beforeAll(function () {
            pageLayout.callInitialPageBreaks();
        });

        describe('method getPageNumber', function () {
            it('should exist', function () {
                expect(pageLayout).toRespondTo('getPageNumber');
            });
            it('should return the current page number of the selection', function () {
                expect(pageLayout.getPageNumber()).toBe(1);
            });
        });

        describe('method getNumberOfDocumentPages', function () {
            it('should exist', function () {
                expect(pageLayout).toRespondTo('getNumberOfDocumentPages');
            });
            it('should return correct number of document pages', function () {
                expect(pageLayout.getNumberOfDocumentPages()).toBe(3); // only three pages in this simplified scenario without table
            });
        });
    });
});
