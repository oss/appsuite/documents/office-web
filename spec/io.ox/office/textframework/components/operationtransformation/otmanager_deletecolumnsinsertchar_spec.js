/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // insertText and local deleteColumns operation (handleDeleteColumnsInsertChar)
        // { name: deleteColumns, start: [2], startGrid: 1, endGrid: 2 }
        it('should calculate valid transformed insertText operation after local deleteColumns operation', function () {

            oneOperation = { name: 'insertText', start: [3, 4, 2, 2, 0], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 1, 2, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 2, 2, 0], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 0, 2, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 2, 2, 0], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 2, 2, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 2, 2, 0], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertText', start: [3, 4, 2, 2, 0], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 2, 0], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertText', start: [3, 4, 3, 2, 0], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 3, 2, 0]);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4]);

            oneOperation = { name: 'insertText', start: [3, 4, 3, 2, 0], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [4], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4, 3, 2, 0]);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [4], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4]);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 5, 4], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 8, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 4]);

            oneOperation = { name: 'insertText', start: [3, 6], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 5, 4], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 5, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 6]);

            oneOperation = { name: 'insertText', start: [4, 4], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 5, 4], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 5, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 4]);

            oneOperation = { name: 'insertText', start: [2, 2, 4, 6, 2], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 2, 3, 6, 2]);

            oneOperation = { name: 'insertText', start: [2, 6, 4, 6, 2], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 6, 3, 6, 2]);

            oneOperation = { name: 'insertText', start: [2, 1, 2, 3, 2], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 3, 2]);

            oneOperation = { name: 'insertText', start: [2, 1, 2, 1, 4, 3, 1, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 1, 4, 2, 1, 1]);

            oneOperation = { name: 'insertText', start: [2, 1, 2, 1, 4, 2, 1, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertText', start: [2, 1, 2, 1, 4, 3, 1, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 1, 4, 1, 1, 1]);

            oneOperation = { name: 'insertText', start: [2, 2, 2, 1, 4, 2, 1, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 2, 2, 1, 4, 2, 1, 1]);

            oneOperation = { name: 'insertText', start: [2, 1, 2, 2, 4, 2, 1, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 2, 4, 2, 1, 1]);

            oneOperation = { name: 'insertText', start: [2, 1, 2, 1, 4, 2, 1, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertText', start: [2, 2, 2, 3, 2], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 2, 2, 3, 2]);

            oneOperation = { name: 'insertText', start: [2, 1, 2, 1, 5, 0, 2, 4], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 1, 5, 0, 2, 4]);

            oneOperation = { name: 'insertText', start: [2, 1, 2, 1, 5, 0, 2, 4], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1], startGrid: 0, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertText', start: [2, 1, 2, 1, 5, 3, 2, 4], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 1, 5, 2, 2, 4]);

            oneOperation = { name: 'insertText', start: [2, 1, 2, 1, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 2, 1, 5, 0], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }]; // table in textframe in table
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 1, 8, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 1, 2]);

            oneOperation = { name: 'insertText', start: [2, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 6, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 9, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1]);

            oneOperation = { name: 'insertText', start: [2, 6], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 4, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 6]);

            oneOperation = { name: 'insertText', start: [1, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 6, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 6, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1]);
        });
    });

});
