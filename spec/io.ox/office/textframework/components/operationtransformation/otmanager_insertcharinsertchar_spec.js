/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        it('should not change the insertText operation, if there are no local operations without acknowledgement', function () {
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'o' };
            localActions = [];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation).toEqual({ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'o' });
        });

        // insert and local insert (handleInsertCharInsertChar)
        it('should calculate valid transformed insertText operation after internal insertText operation', function () {

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'o' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]); // not modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'o' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 5], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]); // not modified
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'o' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]); // not modified
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // not modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'o' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [2, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]); // not modified
            expect(localActions[0].operations[0].start).toEqual([2, 1]); // not modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'o' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 5]); // but the locally saved operation is modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ooo' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 7]); // but the locally saved operation is modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 2], text: 'o' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 1, 2, 2, 4]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 4], text: 'o' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1, 2, 2, 7]);
            expect(localActions[0].operations[0].start).toEqual([0, 1, 2, 2, 3]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 3, 4], text: 'o' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([0, 1, 2, 2, 3]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 1, 1, 2, 4], text: 'o' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1, 1, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([0, 1, 2, 2, 3]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'o' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]); // not modified
            expect(localActions[0].operations[0].start).toEqual([0, 1, 2, 2, 3]); // not modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2, 2, 2, 3], text: 'o' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2, 2, 3]); // not modified
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // not modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6, 1, 1], text: 'o' }; // inside a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 9, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2, 1, 1], text: 'o' }; // inside a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6, 1, 1], text: 'o' }; // inside a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 4], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([0, 4]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6, 1, 1], text: 'o' }; // inside a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4, 1, 0], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 1, 0]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6, 1, 1], text: 'o' }; // inside a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 6, 1, 0], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6, 1, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 6, 1, 0]);

            oneOperation = { name: 'insertTab', opl: 1, osn: 1, start: [1, 6] };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 9]);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);

            oneOperation = { name: 'insertTab', opl: 1, osn: 1, start: [1, 6] };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 8], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 9]);
        });

    });

});
