/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertRows and local setAttributes (handleSetAttrsInsertRows)
        it('should calculate valid transformed insertRows operation after local setAttributes operation', function () {

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 11]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 2 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 10, 4, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 10, 4, 5]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 22]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 12]);
            expect(localActions[0].operations[0].end).toEqual([1, 16]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 12]);
            expect(localActions[0].operations[0].end).toEqual([2, 16]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([1]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1, 2, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 1, 2, 6]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 0], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 0]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 7]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4, 5], end:  [3, 1, 2, 4, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 7, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 2, 7, 9]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            // local change inside one paragraph and several following paragraphs completely and an external insertRows in the middle of the paragraphs
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 4], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2, 4]); // external operation is not modified

            // local change of several paragraphs completely and an external insertRows before the first of the paragraphs
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1, 3]); // external operation is not modified

            // local change of several paragraphs completely and an external insertRows in the last of the paragraphs
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([3, 1]); // external operation is not modified

            // local change of several paragraphs and an external insertRows inside the table
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 4, 5, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3, 8]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2, 1, 4, 5, 2]); // external operation is not modified

            // local change of one complete paragraph and an external insertRows before this paragraph
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 5]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1, 2]); // external operation is not modified
        });

        // insertCells and local setAttributes (handleSetAttrsInsertRows)
        it('should calculate valid transformed insertCells operation after local setAttributes operation', function () {

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 11]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 8, 2], count: 2 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 8, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 6, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 6, 5]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 22]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 12]);
            expect(localActions[0].operations[0].end).toEqual([1, 16]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 3, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 12]);
            expect(localActions[0].operations[0].end).toEqual([2, 16]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 6]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 5]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 1]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 3]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([1]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2, 2, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 2, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 1, 2, 6]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 0, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 0, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 0, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3, 0, 2], end: [3, 1, 2, 3, 0, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 0, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 0, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 2, 3, 0, 7]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 0, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3, 0, 1], end: [3, 1, 2, 3, 0, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 0, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 0, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 2, 3, 0, 7]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 5]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3, 5], end:  [3, 1, 2, 3, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 8]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 2, 3, 12]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 4, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2, 4, 2]); // external operation is not modified

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 3, 3], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1, 3, 3]); // external operation is not modified

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([3, 2, 2]); // external operation is not modified

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 4, 2, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3, 8]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2, 1, 4, 2, 2, 2]); // external operation is not modified

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1);
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 2, 2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1);
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 2, 2]);
        });

        // updateField and local insertRows (handleSetAttrsInsertRows)
        it('should calculate valid transformed updateField operation after local insertRows operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 8, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 11, 4, 2, 3], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 5, 4, 2, 3], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }], transformedOps);
        });

        // updateField and external insertRows (handleSetAttrsInsertRows)
        it('should calculate valid transformed updateField operation after external insertRows operation', function () {

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 8, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 11, 4, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 5, 4, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }], transformedOps);
        });

        // updateField and local insertCells (handleSetAttrsInsertRows)
        it('should calculate valid transformed updateField operation after local insertCells operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 7, 2, 3], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }], transformedOps);
        });

        // updateField and external insertCells (handleSetAttrsInsertRows)
        it('should calculate valid transformed updateField operation after external insertCells operation', function () {

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 7, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }], transformedOps);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }], transformedOps);
        });
    });

});
