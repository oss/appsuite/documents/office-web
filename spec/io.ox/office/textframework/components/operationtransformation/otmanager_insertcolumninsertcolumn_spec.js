/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        // insertColumn and local insertColumn operation (handleInsertColumnInsertColumn)
        // { name: insertColumn, start: [2], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }
        it('should calculate valid transformed insertColumn operation after local insertColumn operation', function () {

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(3);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 1, 2, 2, 2]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 2, 2], gridPosition: 3, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 3, 3, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.gridPosition).toBe(4);
            expect(oneOperation.tableGrid).toEqual([1, 1, 3, 3, 2, 2]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3], gridPosition: 3, insertMode: 'before', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 3, 3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.gridPosition).toBe(4);
            expect(oneOperation.tableGrid).toEqual([1, 1, 3, 3, 3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3], gridPosition: 3, insertMode: 'before', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 6, 6], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 3, 3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.gridPosition).toBe(4);
            expect(oneOperation.tableGrid).toEqual([1, 1, 3, 3, 3]); // only use values from external grid

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 3, 3, 1, 1, 5, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.gridPosition).toBe(6);
            expect(oneOperation.tableGrid).toEqual([1, 1, 3, 3, 1, 1, 5, 5]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 3, 3, 1, 1, 5, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.gridPosition).toBe(6);
            expect(oneOperation.tableGrid).toEqual([1, 1, 3, 3, 1, 1, 5, 5]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(6);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 3, 3, 1, 1, 5, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 1, 3, 3, 1, 1, 5, 5]);

            oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(5);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 3, 1, 1, 5, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 1, 1]);

            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(5);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 3, 1, 1, 5, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 1, 1]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [2, 2, 4, 4], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.gridPosition).toBe(3);
            expect(oneOperation.tableGrid).toEqual([1, 1, 2, 2, 2]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(3);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 1, 2, 2, 2]);

            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 1, 1, 1]);

            oneOperation = { name: 'insertColumn', start: [3, 6, 1, 2], tableGrid: [2, 2, 3, 3], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [2, 2, 2, 3], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(1);
            expect(localActions[0].operations[0].tableGrid).toEqual([2, 2, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 6, 1, 2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([2, 2, 3, 3]);

            oneOperation = { name: 'insertColumn', start: [3, 6, 1, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 1, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(1);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 6, 2, 2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [3, 6, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(1);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 6, 3, 2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [3, 0, 1, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 1, 2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 2, 2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 3, 2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 2, 2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 2, 2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 5, 2, 2]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 0, 1]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 2, 2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 0, 1]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 0, 1]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 2, 3, 3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3, 0, 1]);
            expect(oneOperation.gridPosition).toBe(3);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 3, 3]);

            oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 0, 1]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [2, 3, 4, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3, 5, 1]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 4, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 5, 1]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 4, 2]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 2, 3, 3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3, 4, 2]);
            expect(oneOperation.gridPosition).toBe(3);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 3, 3]);

            oneOperation = { name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 4, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 4, 1]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3, 4, 2]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [2, 3, 1, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 0, 1]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3, 1, 1]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [2, 2, 0, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 0, 1]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 2, 0, 1]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'insertColumn', start: [2, 2, 0, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [1, 2, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 0, 1]);
            expect(localActions[0].operations[0].gridPosition).toBe(2);
            expect(localActions[0].operations[0].tableGrid).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 2, 0, 1]);
            expect(oneOperation.gridPosition).toBe(2);
            expect(oneOperation.tableGrid).toEqual([1, 2, 3, 4]);
        });
    });

});
