/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        // insertTable and local insertRows operation (handleInsertParaInsertRows)
        it('should calculate valid transformed insertTable operation after local insertRows operation', function () {

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertTable', start: [3, 1, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 1, 2]);

            oneOperation = { name: 'insertTable', start: [3, 0, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 1, 2]);

            oneOperation = { name: 'insertTable', start: [3, 2, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 1, 2]);

            oneOperation = { name: 'insertTable', start: [3, 2, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 1, 2]);

            oneOperation = { name: 'insertTable', start: [4, 2, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 2, 1, 2]);

            oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 1, 1], count: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertTable', start: [2, 1, 0, 0, 0, 1], opl: 1, osn: 1 }; // in a text frame in a table
            localActions = [{ operations: [{ name: 'insertRows', start: [2, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 4, 0, 0, 0, 1]);
        });

        // insertParagraph and local insertRows operation (handleInsertParaInsertRows)
        it('should calculate valid transformed insertParagraph operation after local insertRows operation', function () {

            oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertParagraph', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertParagraph', start: [3, 1, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 1, 2]);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 1, 2]);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 1, 2]);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 1, 2]);

            oneOperation = { name: 'insertParagraph', start: [4, 2, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 2, 1, 2]);

            oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 1, 1], count: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertParagraph', start: [2, 1, 0, 0, 0, 1], opl: 1, osn: 1 }; // in a text frame in a table
            localActions = [{ operations: [{ name: 'insertRows', start: [2, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 4, 0, 0, 0, 1]);
        });

        // insertTable and local insertCells operation (handleInsertParaInsertRows)
        it('should calculate valid transformed insertTable operation after local insertCells operation', function () {

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertTable', start: [3, 1, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 2]);

            oneOperation = { name: 'insertTable', start: [3, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 3, 2]);

            oneOperation = { name: 'insertTable', start: [3, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 5, 2]);

            oneOperation = { name: 'insertTable', start: [3, 0, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 2, 2]);

            oneOperation = { name: 'insertTable', start: [3, 2, 3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 2, 3], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 4, 2]);

            oneOperation = { name: 'insertTable', start: [3, 2, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 1, 2]);

            oneOperation = { name: 'insertTable', start: [4, 2, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 3], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 2, 1, 2]);

            oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 1, 1, 2], count: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 1, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertTable', start: [2, 1, 0, 0, 0, 1], opl: 1, osn: 1 }; // in a text frame in a table
            localActions = [{ operations: [{ name: 'insertCells', start: [2, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 0, 0, 0, 1]);

            oneOperation = { name: 'insertTable', start: [2, 1, 0, 0, 0, 1], opl: 1, osn: 1 }; // in a text frame in a table
            localActions = [{ operations: [{ name: 'insertCells', start: [2, 1, 0], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 3, 0, 0, 1]);

            oneOperation = { name: 'insertTable', start: [1, 1, 3], opl: 1, osn: 1 }; // text frame
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 1, 3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 4, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 3]);

            oneOperation = { name: 'insertTable', start: [1, 2, 3], opl: 1, osn: 1 }; // text frame
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 1, 3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 2, 3]);

            oneOperation = { name: 'insertTable', start: [1, 1, 4], opl: 1, osn: 1 }; // text frame
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 1, 3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 4]);

            oneOperation = { name: 'insertTable', start: [1, 1, 3, 1, 2, 2], opl: 1, osn: 1 }; // text frame
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 1, 3, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 3, 1, 5, 2]);
        });

        // insertParagraph and local insertCells operation (handleInsertParaInsertRows)
        it('should calculate valid transformed insertParagraph operation after local insertCells operation', function () {

            oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertParagraph', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertParagraph', start: [3, 1, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 2]);

            oneOperation = { name: 'insertParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 3, 2]);

            oneOperation = { name: 'insertParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 5, 2]);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 2, 2]);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 2, 3], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 4, 2]);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 1, 2]);

            oneOperation = { name: 'insertParagraph', start: [4, 2, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 3], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 2, 1, 2]);

            oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 1, 1, 2], count: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 1, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertParagraph', start: [2, 1, 0, 0, 0, 1], opl: 1, osn: 1 }; // in a text frame in a table
            localActions = [{ operations: [{ name: 'insertCells', start: [2, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 0, 0, 0, 1]);

            oneOperation = { name: 'insertParagraph', start: [2, 1, 0, 0, 0, 1], opl: 1, osn: 1 }; // in a text frame in a table
            localActions = [{ operations: [{ name: 'insertCells', start: [2, 1, 0], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 3, 0, 0, 1]);

            oneOperation = { name: 'insertParagraph', start: [1, 1, 3], opl: 1, osn: 1 }; // text frame
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 1, 3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 4, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 3]);

            oneOperation = { name: 'insertParagraph', start: [1, 2, 3], opl: 1, osn: 1 }; // text frame
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 1, 3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 2, 3]);

            oneOperation = { name: 'insertParagraph', start: [1, 1, 4], opl: 1, osn: 1 }; // text frame
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 1, 3, 1, 2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 4]);

            oneOperation = { name: 'insertParagraph', start: [1, 1, 3, 1, 2, 2], opl: 1, osn: 1 }; // text frame
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 1, 3, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 3, 1, 5, 2]);
        });
    });

});
