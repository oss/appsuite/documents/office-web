/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertRows and local delete (handleInsertRowsDelete)
        it('should calculate valid transformed insertRows operation after local delete operation', function () {

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(oneOperation.referenceRow).toBe(1);
            expect(localActions[0].operations[0].start).toEqual([1, 11]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4]);
            expect(oneOperation.referenceRow).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(oneOperation.referenceRow).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([1]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 5], referenceRow: 4, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(oneOperation.referenceRow).toBe(4);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 4]);
            expect(oneOperation.referenceRow).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(oneOperation.referenceRow).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(oneOperation.referenceRow).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 2, 3, 4]);
            expect(oneOperation.referenceRow).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before insertRows
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 2, 4]);
            expect(oneOperation.referenceRow).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 0]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind insert
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(oneOperation.referenceRow).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 5], referenceRow: 4, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 5]);
            expect(oneOperation.referenceRow).toBe(4);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 5], referenceRow: 4, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 5]);
            expect(oneOperation.referenceRow).toBe(4);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 1, 3, 3], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 4, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 3]);
            expect(oneOperation.referenceRow).toBe(2);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 3, 7, 6]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 1, 3, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 4]);
            expect(oneOperation.referenceRow).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 1, 3, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 4]);
            expect(oneOperation.referenceRow).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 8] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 11]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(2);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(oneOperation.referenceRow).toBe(1);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 7]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 1, 4, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 1, 5, 3], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2, 3]);
            expect(oneOperation.referenceRow).toBe(2);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], end: [2, 1, 1, 4, 2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2, 1]);
            expect(oneOperation.referenceRow).toBe(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4, 2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 2, 2, 1], end: [2, 1, 2, 4, 2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2, 1]);
            expect(oneOperation.referenceRow).toBe(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 2, 4, 2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [2, 3] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(oneOperation.referenceRow).toBe(0);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 5] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2, 8]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 4, 5], referenceRow: 4, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 8]);
            expect(localActions[0].operations).toHaveLength(2);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(oneOperation.referenceRow).toBe(3);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [0, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 4]);
            expect(oneOperation.referenceRow).toBe(3);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 7, 2, 2], referenceRow: 1, count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3, 2, 2], referenceRow: 1, count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 5], end: [1, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 7]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2, 2, 2], referenceRow: 1, count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2, 2, 2], referenceRow: 1, count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 7, 2, 4], referenceRow: 3, count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 7, 2, 4]);
            expect(oneOperation.referenceRow).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([0, 2]);

        });

        // insertRows and local delete (handleInsertRowsDelete)
        it('should calculate valid transformed insertRows operation after local delete operation with special handling of the reference row', function () {

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 3], referenceRow: 2, count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }], transformedOps);
            expect(transformedOps[0].referenceRow).toBeUndefined(); // the reference row is deleted

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }], transformedOps);
            expect(transformedOps[0].referenceRow).toBeUndefined(); // the reference row is deleted

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }], transformedOps);
            expect(transformedOps[0].referenceRow).toBeUndefined(); // the reference row is deleted

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 0, count: 3 }], transformedOps); // the reference row is not affected

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 7, 2], end: [1, 7, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 3, 2], end: [1, 3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 3, 2], end: [1, 3, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 2, 0, 0], end: [1, 4, 2, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 7, 2, 0, 0], end: [1, 7, 2, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 4], referenceRow: 3, count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 8], end: [1, 4, 9], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 4, 11], end: [1, 4, 12], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 2], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 2], end: [1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2, 6], referenceRow: 5, count: 3 }], transformedOps);
        });

        // delete and local insertRows (handleInsertRowsDelete)
        it('should calculate valid transformed delete operation after local insertRows operation', function () {

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [1, 11] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 12]);
            expect(oneOperation.end).toEqual([1, 13]);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 3]);
            expect(localActions[0].operations[0].referenceRow).toBe(2);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(oneOperation.end).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertRows', start: [2, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations[0].referenceRow).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 2, 8] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3, 2, 8]);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations[0].referenceRow).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 4, 1, 1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3]);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 1, 1, 4]);
            expect(localActions[0].operations[0].referenceRow).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 3]);
            expect(localActions[0].operations[0].referenceRow).toBe(2);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1, 3]);
            expect(localActions[0].operations[0].referenceRow).toBe(2);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertRows', start: [2, 1, 1, 1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 1, 1, 4]);
            expect(localActions[0].operations[0].referenceRow).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].referenceRow).toBe(2);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 3, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 10]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 2] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 2, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 1, 2] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 2, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 5, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 3, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].referenceRow).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [3, 6] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 12]);
            expect(oneOperation.end).toEqual([3, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertRows', start: [5, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([4, 3]);
            expect(localActions[0].operations[0].referenceRow).toBe(2);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [3] };
            localActions = [{ operations: [{ name: 'insertRows', start: [5, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.end).toEqual([3]);
            expect(localActions[0].operations[0].start).toEqual([2, 3]);
            expect(localActions[0].operations[0].referenceRow).toBe(2);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 6] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(oneOperation.end).toEqual([1, 8]);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'insertRows', start: [2, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [3, 18] };
            localActions = [{ operations: [{ name: 'insertRows', start: [0, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 14]);
            expect(oneOperation.end).toEqual([3, 18]);
            expect(localActions[0].operations[0].start).toEqual([0, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);
        });

        // delete and local insertRows (handleInsertRowsDelete)
        it('should calculate valid transformed delete operation after local insertRows operation with special handling of the reference row', function () {

            oneOperation = { name: 'delete', start: [1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 3], referenceRow: 2, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }], transformedOps);
            expect(localActions[0].operations[0].referenceRow).toBeUndefined(); // the reference row is deleted

            oneOperation = { name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 2, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }], transformedOps);
            expect(localActions[0].operations[0].referenceRow).toBeUndefined(); // the reference row is deleted

            oneOperation = { name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 1, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }], transformedOps);
            expect(localActions[0].operations[0].referenceRow).toBeUndefined(); // the reference row is deleted

            oneOperation = { name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 0, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 0, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }], transformedOps); // the reference row is not affected

            oneOperation = { name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 7, 2], end: [1, 7, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 3, 2], end: [1, 3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 3, 2], end: [1, 3, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 4, 2, 0, 0], end: [1, 4, 2, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 7, 2, 0, 0], end: [1, 7, 2, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 4], referenceRow: 3, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 4, 8], end: [1, 4, 9], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 4, 11], end: [1, 4, 12], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 2], end: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2, 6], referenceRow: 5, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 2], end: [1, 3], opl: 1, osn: 1 }], transformedOps);
        });

        // insertCells and local delete (handleInsertRowsDelete)
        it('should calculate valid transformed insertCells operation after local delete operation', function () {

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 2, 3], opl: 1, osn: 1 }] }]; // deleting a cell
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 6]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 2, 0], opl: 1, osn: 1 }] }]; // deleting a cell
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 0]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 5]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1, 0], opl: 1, osn: 1 }] }]; // deleting a cell
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 0]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 2, 0], opl: 1, osn: 1 }] }]; // deleting a cell
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 0]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([1]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 3]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 4]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 2, 3, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before insert
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 0]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind insert
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 0, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 0]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 4, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 3, 4, 6]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 1, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 3, 1, 10]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 3, 1, 1]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 1]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 4]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 0, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 3, 0]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 3, 2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 3, 1]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 8] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 8]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1, 0], end: [1, 1, 8] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([1, 1, 11]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2, 2], end: [1, 2, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 2, 7]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 4, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 5, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 3], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], end: [2, 1, 1, 2, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 4], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 0], end: [2, 1, 1, 2, 1, 2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2, 1, 0]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 2, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 4], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 6], end: [2, 1, 1, 2, 1, 7] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2, 1, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2, 1, 9]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 2, 1, 10]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 4], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 2], end: [2, 1, 1, 2, 1, 6] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 2, 1, 9]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 2, 2, 1], end: [2, 1, 2, 4, 2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 2, 4, 2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1, 2], end: [1, 1, 3] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 1, 6]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 5] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2, 5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 4, 5, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 8]);
            expect(localActions[0].operations).toHaveLength(2);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [0, 1, 2], count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 1, 2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 7, 2, 2, 2], count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 3, 2, 2, 2], count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 5], end: [1, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 7]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2, 2, 2], count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2, 2, 2], count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 7, 2, 2, 2], count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 7, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([0, 2]);
        });

        // delete and local insertCells (handleInsertRowsDelete)
        it('should calculate valid transformed delete operation after local insertCells operation', function () {

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [1, 11] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10]);
            expect(oneOperation.end).toEqual([1, 11]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2, 4, 2, 2], end: [1, 2, 4, 2, 6] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 6, 2, 2]);
            expect(oneOperation.end).toEqual([1, 2, 6, 2, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 1], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 1]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 1], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(oneOperation.end).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 1]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4, 0], end: [1, 4, 2] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 4], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4, 0]);
            expect(oneOperation.end).toEqual([1, 4, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 1]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertCells', start: [2, 4, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 2, 8] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3, 2, 8]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 1, 1, 1, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3]);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 1, 1, 1, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 4] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 3, 4], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 3, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertCells', start: [2, 1, 1, 1, 1, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 1, 1, 1, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 3, 1, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 1, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 2, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 2, 4] };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2, 6]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 2, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 2, 0] };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2, 0]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 2, 1]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 2] };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 2, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2, 2, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 1, 2] };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 2, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2, 2, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 5, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4, 2, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 3, 4, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [3, 6] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10]);
            expect(oneOperation.end).toEqual([3, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertCells', start: [5, 1, 1], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 1]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [3] };
            localActions = [{ operations: [{ name: 'insertCells', start: [5, 1, 1], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.end).toEqual([3]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 6] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 1], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(oneOperation.end).toEqual([1, 6]);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'insertCells', start: [2, 1, 1], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [3, 18] };
            localActions = [{ operations: [{ name: 'insertCells', start: [0, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 14]);
            expect(oneOperation.end).toEqual([3, 18]);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 8] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 10, 0, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(oneOperation.end).toEqual([1, 8]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 0, 2, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 4], end: [0, 8] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 10, 0, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 4]);
            expect(oneOperation.end).toEqual([0, 8]);
            expect(localActions[0].operations[0].start).toEqual([1, 10, 0, 2, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 14] };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 10, 0, 2, 2], count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(oneOperation.end).toEqual([1, 14]);
            expect(localActions[0].operations).toHaveLength(0);
        });
    });

});
