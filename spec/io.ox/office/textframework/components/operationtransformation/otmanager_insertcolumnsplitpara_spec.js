/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // insertColumn and local splitParagraph operation (handleInsertColumnSplitPara)
        it('should calculate valid transformed insertColumn operation after local splitParagraph operation', function () {

            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5]);

            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [5, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 3, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 3, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 4, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 3, 3, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 4, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 4, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 3, 1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 3, 3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 3, 2, 3, 1, 3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 1, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 3, 2, 3, 3, 3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 4, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 3, 2, 3, 3, 3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 3, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 3]);
        });

        // insertColumn and local splitTable operation without requirement of new insertColumn operation (handleInsertColumnSplitPara)
        it('should calculate valid transformed insertColumn operation after local splitTable operation without generating new insertColumn operation', function () {

            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5]);

            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [5, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 3, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 3, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 4, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 3, 3, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 4, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 4, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [0, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 3, 2, 3, 1, 3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 1, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 3, 2, 3, 2, 3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 3, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 3, 2, 3, 2, 3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 2, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 3, 2, 3, 3, 3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 4, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 3, 2, 3, 3, 3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 3, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 3]);
        });

        // // insertColumn and local splitTable operation with requirement of new insertColumn operation (handleInsertColumnSplitPara)
        it('should calculate valid transformed insertColumn operation after local splitTable operation and generate required insertColumn operation', function () {

            // the local split is in the same table: local splitTable [4, 2] and external insertColumn [4]
            // -> an additional external operation: 'insertColumn [5]' after 'insertColumn [4]' is required
            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [4, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].name).toBe("splitTable");
            expect(localActions[0].operations[0].start).toEqual([4, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(2); // one additional external insertColumn operation
            expect(transformedOps[0]).toEqual(oneOperation);
            expect(transformedOps[1].name).toBe("insertColumn");
            expect(transformedOps[1].start).toEqual([5]);

            // the local split is in the same table: local splitTable [1, 2, 2, 4, 2] and external insertColumn [1, 2, 2, 4]
            // -> an additional external operation: 'insertColumn [1, 2, 2, 5]' after 'insertColumn [1, 2, 2, 4]' is required
            oneOperation = { name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 2, 4, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].name).toBe("splitTable");
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 4, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(2); // one additional external insertColumn operation
            expect(transformedOps[0]).toEqual(oneOperation);
            expect(transformedOps[1].name).toBe("insertColumn");
            expect(transformedOps[1].start).toEqual([1, 2, 2, 5]);

            // the local split is in the previous table: local splitTable [3] and external insertColumn [4] -> no additional operation required
            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // no additional operation required
            expect(localActions[0].operations[0].name).toBe("splitTable");
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(transformedOps).toHaveLength(1); // no additional operation required
            expect(transformedOps[0].start).toEqual([5]);
        });

        // // splitTable and local insertColumn operation with requirement of new insertColumn operation (handleInsertColumnSplitPara)
        it('should calculate valid transformed splitTable operation after local insertColumn operation and generate required insertColumn operation', function () {

            // the merge is in the same table: local insertColumn [4] and external splitTable [4, 2]
            // -> an additional internal operation: 'insertColumn [5]' after 'insertColumn [4]' is required
            oneOperation = { name: 'splitTable', start: [4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("insertColumn");
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations[1].name).toBe("insertColumn");
            expect(localActions[0].operations[1].start).toEqual([5]);
            expect(transformedOps).toHaveLength(1); // no additional external operation
            expect(transformedOps[0]).toEqual(oneOperation);

            // the split is in the same table: local insertColumn [1, 2, 2, 4] and external splitTable [1, 2, 2, 4, 2]
            // -> an additional internal operation: 'insertColumn [1, 2, 2, 5]' after 'insertColumn [1, 2, 2, 4]' is required
            oneOperation = { name: 'splitTable', start: [1, 2, 2, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("insertColumn");
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 4]);
            expect(localActions[0].operations[1].name).toBe("insertColumn");
            expect(localActions[0].operations[1].start).toEqual([1, 2, 2, 5]);
            expect(transformedOps).toHaveLength(1); // no additional external operation
            expect(transformedOps[0]).toEqual(oneOperation);

            // the external split is in the previous table: local insertColumn [4] and external splitTable [3, 2] -> no additional operation required
            oneOperation = { name: 'splitTable', start: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // no additional internal operation
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(transformedOps).toHaveLength(1); // no additional external operation
            expect(transformedOps[0]).toEqual(oneOperation);
        });
    });

});
