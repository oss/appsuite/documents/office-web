/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertTable and local mergeParagraph operation (handleMergeParaInsertPara)
        it('should calculate valid transformed insertTable operation after local mergeParagraph operation', function () {

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'splitParagraph', start: [2, 5], opl: 1, osn: 1 }, { name: 'insertTable', start: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 }, { name: 'insertText', start: [2, 8], text: 'ooo', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [4, 3], text: 'ooo', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitParagraph', start: [2, 5], opl: 1, osn: 1 }, { name: 'insertTable', start: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 3, 3, 1], paralength: 5, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [5, 1, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 1]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [5, 1, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [5, 1, 1, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 6, 2]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 0, 1], opl: 1, osn: 1, paralength: 5 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 1, 0], opl: 1, osn: 1, paralength: 5 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 1]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 1, 4], opl: 1, osn: 1, paralength: 5 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);
        });

        // mergeParagraph and local insertTable operation (handleMergeParaInsertPara)
        it('should calculate valid transformed mergeParagraph operation after local insertTable operation', function () {

            oneOperation = { name: 'mergeParagraph', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'mergeParagraph', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5]);

            oneOperation = { name: 'mergeParagraph', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'mergeParagraph', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [4, 2, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("splitParagraph");
            expect(localActions[0].operations[0].start).toEqual([2, 5]);
            expect(localActions[0].operations[1].name).toBe("insertTable");
            expect(localActions[0].operations[1].start).toEqual([3]);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeParagraph', start: [2, 3, 3, 1], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3, 3, 1]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [2, 5], opl: 1, osn: 1 }, { name: 'insertTable', start: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }, { name: 'insertText', start: [4, 0], test: 'ooo', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [2, 5], opl: 1, osn: 1 }, { name: 'insertTable', start: [3], opl: 1, osn: 1 }, { name: 'insertText', start: [4, 0], test: 'ooo', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        // insertTable and local mergeTable operation (handleMergeParaInsertPara)
        it('should calculate valid transformed insertTable operation after local mergeTable operation', function () {

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([3]);
            expect(transformedOps).toHaveLength(2); // an additional external operation
            expect(transformedOps[0].name).toBe("splitTable"); // inserted before insertTable operation
            expect(transformedOps[0].start).toEqual([2, 5]);
            expect(transformedOps[1]).toEqual(oneOperation);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 1]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 1, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 6, 2]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 0, 1], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 1, 0], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 1]);

            oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 1, 4], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);
        });

        // mergeTable and local insertTable operation (handleMergeParaInsertPara)
        it('should calculate valid transformed mergeTable operation after local insertTable operation', function () {

            oneOperation = { name: 'mergeTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'mergeTable', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5]);

            oneOperation = { name: 'mergeTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'mergeTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [4, 2, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("splitTable");
            expect(localActions[0].operations[0].start).toEqual([2, 5]);
            expect(localActions[0].operations[1].name).toBe("insertTable");
            expect(localActions[0].operations[1].start).toEqual([3]);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3, 3, 1]);
            expect(transformedOps).toHaveLength(1);
        });

        // insertParagraph and local mergeTable operation (handleMergeParaInsertPara)
        it('should calculate valid transformed insertParagraph operation after local mergeTable operation', function () {

            oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertParagraph', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([3]);
            expect(transformedOps).toHaveLength(2); // an additional external operation
            expect(transformedOps[0].name).toBe("splitTable"); // inserted before insertTable operation
            expect(transformedOps[0].start).toEqual([2, 5]);
            expect(transformedOps[1]).toEqual(oneOperation);

            oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertParagraph', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 1, 2, 2]);

            oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 1]);

            oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 1, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 1, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 1, 2, 2]);

            oneOperation = { name: 'insertParagraph', start: [1, 1, 2, 1, 1, 2], opl: 1, osn: 1 }; // in a text frame in a cell
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 6, 2, 1, 1, 2]);

            oneOperation = { name: 'insertParagraph', start: [1, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2, 2]);

            oneOperation = { name: 'insertParagraph', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);

            oneOperation = { name: 'insertParagraph', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 0, 1], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);

            oneOperation = { name: 'insertParagraph', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 1, 0], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 1]);

            oneOperation = { name: 'insertParagraph', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 1, 4], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);
        });

        // mergeTable and local insertTable operation (handleMergeParaInsertPara)
        it('should calculate valid transformed mergeTable operation after local insertParagraph operation', function () {

            oneOperation = { name: 'mergeTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'mergeTable', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5]);

            oneOperation = { name: 'mergeTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'mergeTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [4, 2, 2, 0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("splitTable");
            expect(localActions[0].operations[0].start).toEqual([2, 5]);
            expect(localActions[0].operations[1].name).toBe("insertParagraph");
            expect(localActions[0].operations[1].start).toEqual([3]);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 3, 3, 1]);
            expect(transformedOps).toHaveLength(1);
        });

    });

});
