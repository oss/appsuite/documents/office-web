/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // local deleteColumns and external deleteColumns operation (handleDeleteColumnsDeleteColumns)
        // { name: deleteColumns, start: [2], startGrid: 1, endGrid: 2 }
        it('should calculate valid transformed deleteColumns operation after local deleteColumns operation', function () {

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'deleteColumns', start: [3, 2, 6, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 3, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [3, 2, 0, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 0, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [3, 2, 1, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', start: [3, 2, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', start: [3, 2, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', start: [3, 2, 4, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 1, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [3, 2, 5, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [3, 2, 5, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 5, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [3, 2, 5, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [4], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 5, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 0, 1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 2, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 5, 2, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 5, 2, 1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 5, 2, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 0, 1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 0, 1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 0, 1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [4, 2, 4, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 2, 4, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [2, 5, 4, 0, 0, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 5, 1, 0, 0, 1, 1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [2, 5, 3, 0, 0, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 0]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
        });

        // local deleteColumns and external deleteColumns operation (handleDeleteColumnsDeleteColumns)
        // { name: deleteColumns, start: [2], startGrid: 1, endGrid: 2 }
        it('should calculate valid values for startGrid and endGrid for deleteColumns operation after local deleteColumns operation', function () {

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'deleteColumns', start: [2, 1, 1, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 1, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 7, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(1);
            expect(oneOperation.endGrid).toBe(4);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 7, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(4);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 8, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(5);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(1);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 8, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(4);
            expect(localActions[0].operations[0].endGrid).toBe(7);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(1);
            expect(oneOperation.endGrid).toBe(1);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 8, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(5);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(2);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(4);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(4);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(4);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(1);
            expect(oneOperation.endGrid).toBe(1);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 3, endGrid: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(1);
            expect(oneOperation.endGrid).toBe(6);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 3, endGrid: 4, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(6);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(4);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(1);
            expect(oneOperation.endGrid).toBe(1);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(4);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(1);
            expect(oneOperation.endGrid).toBe(4);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(1);
            expect(oneOperation.endGrid).toBe(4);

            oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(1);
            expect(localActions[0].operations[0].endGrid).toBe(4);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(1);
            expect(oneOperation.endGrid).toBe(4);
        });
    });

});
