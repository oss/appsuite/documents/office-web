/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertRows and local insertRows operation (handleInsertRowsInsertRows)
        it('should calculate valid transformed insertRows operation after local insertRows operation', function () {

            oneOperation = { name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);

            oneOperation = { name: 'insertRows', start: [2, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1]);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [2, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 4], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);

            oneOperation = { name: 'insertRows', start: [3, 6], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 4], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 9]);

            oneOperation = { name: 'insertRows', start: [3, 6, 0, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 4], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 9, 0, 1, 1]);

            oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 4], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 1, 1]);

            oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 3, 0, 0, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 0, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 1, 1]);

            oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 1], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 0, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 1, 1]);

            oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 4], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 0, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 1, 7]);

            oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 3, 0, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 0, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 1, 1]);

            oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 3, 1, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 1, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 0, 1, 1]);

            oneOperation = { name: 'insertRows', start: [1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [0, 5, 1, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 5, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1]);

            oneOperation = { name: 'insertRows', start: [0, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 5, 1, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 1]);
        });

        // insertRows and local insertRows operation (handleInsertRowsInsertRows)
        it('should calculate valid transformed insertRows operation after local insertRows operation with special handling of property referenceRow', function () {

            oneOperation = { name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 2], count: 1, referenceRow: 0, opl: 1, osn: 1 }] }], localActions); // "referenceRow" does not change
            expectOp([{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 2], count: 1, referenceRow: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 3], count: 1, referenceRow: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 8], count: 2, referenceRow: 7, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', start: [3, 1, 1, 0, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 1, 1, 0, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', start: [3, 2], count: 1, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 3], count: 1, referenceRow: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', start: [3, 2], count: 2, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 8], count: 2, referenceRow: 7, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 1, 0, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 1, 1, 0, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }], transformedOps);
        });

        // insertCells and local insertCells operation (handleInsertRowsInsertRows)
        it('should calculate valid transformed insertCells operation after local insertCells operation', function () {

            oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1]);

            oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1]);

            oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1]);

            oneOperation = { name: 'insertCells', start: [2, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 1]);

            oneOperation = { name: 'insertCells', start: [2, 1, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 0], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2]);

            oneOperation = { name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [2, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2]);

            oneOperation = { name: 'insertCells', start: [3, 0, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 4], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 1]);

            oneOperation = { name: 'insertCells', start: [3, 0, 6], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 4], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 9]);

            oneOperation = { name: 'insertCells', start: [3, 0, 6, 0, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 4], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 9, 0, 1, 1]);

            oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 4], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 3, 0, 1, 1]);

            oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 3, 0, 0, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 3, 0, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 3, 0, 1, 1]);

            oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 3, 0, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 3, 0, 1, 1]);

            oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 4], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 3, 0, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 3, 0, 1, 7]);

            oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 3, 0, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 3, 0, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 3, 0, 1, 1]);

            oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 3, 1, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 3, 1, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 3, 0, 1, 1]);

            oneOperation = { name: 'insertCells', start: [1, 1, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [0, 5, 1, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 5, 1, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2]);

            oneOperation = { name: 'insertCells', start: [0, 1, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 5, 1, 1, 2], count: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 1, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 1, 2]);
        });
    });

});
