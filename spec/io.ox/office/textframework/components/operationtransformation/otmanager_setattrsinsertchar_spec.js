/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // setAttributes and local insert (handleSetAttrsInsertChar)
        it('should calculate valid transformed setAttributes operation after internal insertText operation', function () {

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], end: [1, 4], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(oneOperation.end).toEqual([1, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], end: [2, 4], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(oneOperation.end).toEqual([2, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 5], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [2, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 4], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 7]); // the insert must shift the setAttributes
            expect(localActions[0].operations[0].start).toEqual([1, 4]); // the insert must shift the setAttributes

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 4], end: [1, 4], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 7]); // the insert must shift the setAttributes
            expect(oneOperation.end).toEqual([1, 7]); // the insert must shift the setAttributes
            expect(localActions[0].operations[0].start).toEqual([1, 4]); // the insert must shift the setAttributes

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 4], end: [1, 6], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 7]);
            expect(oneOperation.end).toEqual([1, 9]);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [0, 1, 2, 2, 2], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 1, 2, 2, 3]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [0, 1, 2, 2, 4], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1, 2, 2, 7]);
            expect(localActions[0].operations[0].start).toEqual([0, 1, 2, 2, 3]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [0, 1, 2, 3, 4], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([0, 1, 2, 2, 3]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [0, 1, 1, 2, 4], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1, 1, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([0, 1, 2, 2, 3]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2, 2, 2], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2, 2], attrs: { character: { italic: true } } }; // in a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2, 2], end: [1, 2, 2, 4], attrs: { character: { italic: true } } }; // in a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2]);
            expect(oneOperation.end).toEqual([1, 5, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2, 2], end: [1, 2, 2, 4], attrs: { character: { italic: true } } }; // in a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 3], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2, 2]);
            expect(oneOperation.end).toEqual([1, 2, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 3]);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2, 2], end: [1, 2, 2, 4], attrs: { character: { italic: true } } }; // in a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1], text: 'ppp' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 2, 2]);
            expect(oneOperation.end).toEqual([1, 2, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
        });

        // updateField and local insert (handleSetAttrsInsertChar)
        it('should calculate valid transformed updateField operation after internal insertText operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 5], type: 'PAGE' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 0], type: 'PAGE' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 0], type: 'PAGE' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 1], type: 'PAGE' };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], type: 'PAGE' }], transformedOps);
        });

        // updateField and external insert (handleSetAttrsInsertChar)
        it('should calculate valid transformed updateField operation after external insertText operation', function () {

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 5], type: 'PAGE' }] }], localActions);
            expectOp([{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }], transformedOps);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 0], type: 'PAGE' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 0], type: 'PAGE' }] }], localActions);
            expectOp([{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }], transformedOps);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 1], type: 'PAGE' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], type: 'PAGE' }] }], localActions);
            expectOp([{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }], transformedOps);
        });
    });
});
