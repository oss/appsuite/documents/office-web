/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        // insert and local mergeParagraph (handleParaMergeInsertChar)
        it('should calculate valid transformed insertText operation after mergeParagraph operation', function () {

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 5], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 7 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6]);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].paralength).toBe(1);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 6], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 9]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 6, 3, 2], text: 'ooo' }; // inside text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 9, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 4, 2], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 7, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 5], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 4, 2], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 0, 2, 2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 0, 2, 2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 0, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 2, 1], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 2, 1, 5]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 1]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 2, 3, 4], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 3, 4]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3], text: 'ooo' }; // insert text in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 1], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3], text: 'ooo' }; // insert text in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 5, 0], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 1, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 0]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3], text: 'ooo' }; // insert text in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 5, 1], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 1, 6]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 1]);
            expect(localActions[0].operations[0].paralength).toBe(3);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 2], text: 'ooo' }; // insert text in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 5, 2], opl: 1, osn: 1, paralength: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 2]);
            expect(localActions[0].operations[0].paralength).toBe(6);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 8 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].paralength).toBe(11);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 1, 1], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 8 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].paralength).toBe(8); // the drawing at position [2, 5] was already there
        });

        // insert and local mergeTable (handleParaMergeInsertChar)
        it('should calculate valid transformed insertText operation after mergeTable operation', function () {

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2, 0, 0, 1], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 7 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 0, 0, 1]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(7);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6]);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].rowcount).toBe(1);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 6, 0, 0, 1], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 9, 0, 0, 1]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 6, 3, 2], text: 'ooo' }; // inside text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 6, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 6, 3, 2, 0, 0, 1], text: 'ooo' }; // inside text frame in table
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 9, 3, 2, 0, 0, 1]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 6, 3, 2, 0, 0, 1], text: 'ooo' }; // inside text frame in table
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 6, 3, 2, 0, 0, 1]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 4, 2], opl: 1, osn: 1, rowcount: 3 }] }]; // inside a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 7, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 5], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 4, 2], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 0, 2, 2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 0, 2, 2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 0, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 2, 2, 2, 2, 2, 2, 2], text: 'ooo' }; // table in table
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 2, 2, 2, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2, 2, 2, 2], text: 'ooo' }; // table in table
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 2, 2, 2, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2, 2, 2, 2], text: 'ooo' }; // table in table
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 2], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 2, 2, 2, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 4, 2, 2, 2, 2], text: 'ooo' }; // table in table
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 2], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 2, 3, 2, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 3, 2, 2, 2, 2], text: 'ooo' }; // table in table
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 2], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 2, 2, 5, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 1, 3, 2, 2, 2, 2], text: 'ooo' }; // table in table
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 2], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 1, 3, 2, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1, 2, 3, 2, 2, 2, 2], text: 'ooo' }; // table in table
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 2], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 2, 3, 2, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 1], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 1, 6, 0, 0, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 1]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 0], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 1, 3, 0, 0, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 0]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 2], opl: 1, osn: 1, rowcount: 4 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 2, 3, 0, 0, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 3], opl: 1, osn: 1, rowcount: 4 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 2, 3, 0, 0, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 3]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 4, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 3], opl: 1, osn: 1, rowcount: 4 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4, 2, 3, 0, 0, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 3]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 6, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 3], opl: 1, osn: 1, rowcount: 4 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 6, 2, 3, 0, 0, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 3]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 0], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 1, 3, 0, 0, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 0]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 1], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 1, 6, 0, 0, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 1]);
            expect(localActions[0].operations[0].rowcount).toBe(3);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 2, 0, 0, 3], text: 'ooo' }; // insert text in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 2], opl: 1, osn: 1, rowcount: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 2, 2, 0, 0, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 2]);
            expect(localActions[0].operations[0].rowcount).toBe(3);
        });
    });

});
