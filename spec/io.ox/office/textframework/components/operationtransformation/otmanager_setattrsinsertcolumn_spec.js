/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertColumn and local setAttributes (handleSetAttrsInsertColumn)
        it('should calculate valid transformed insertColumn operation after local setAttributes operation', function () {

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 5, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 5, 5]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 3, 2], end: [1, 8, 3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 4, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 4, 5]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 2, 2], end: [1, 8, 2, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 2, 5]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 2, 2], end: [1, 8, 2, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 3, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 3, 5]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 1, 2], end: [1, 8, 1, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 1, 5]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 4, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 4, 5]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 4, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 4, 5]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1, 8, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 8, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 4, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 4, 5]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1, 8, 4, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 8, 4, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 4, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 4, 5]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1, 22]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([2, 12]);
            expect(localActions[0].operations[0].end).toEqual([2, 16]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 5]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 5]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 4]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([1]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 1, 2, 6]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3, 3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 3, 6]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4, 5], end:  [3, 1, 2, 4, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 2, 4, 9]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2]); // external operation is not modified

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1]); // external operation is not modified

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([3]); // external operation is not modified

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2, 1, 4, 5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3, 8]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2, 1, 4, 5]); // external operation is not modified

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 4], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 5]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1]); // external operation is not modified

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 0, 4], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 5]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1]); // external operation is not modified
        });

        // updateField and local insertColumn (handleSetAttrsInsertColumn)
        it('should calculate valid transformed updateField operation after local insertColumn operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 4, 2, 2], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], representation: 'abc' }], transformedOps);
        });

        // updateField and external insertColumn (handleSetAttrsInsertColumn)
        it('should calculate valid transformed updateField operation after external insertColumn operation', function () {

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 4, 2, 2], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }], transformedOps);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }], transformedOps);

            oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }], transformedOps);
        });

        // setAttributes with tableGrid attribut and external insertColumn (handleSetAttrsInsertColumn)
        it('should calculate valid transformed setAttributes operation after external insertColumn operation', function () {
            oneOperation = { name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true }, table: { tableGrid: [1, 2, 3] } } }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }] }], localActions);
            expectOp([{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }], transformedOps);

            oneOperation = { name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true }, table: { width: 320, tableGrid: [1, 2, 3] } } }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true }, table: { width: 320 } } }] }], localActions);
            expectOp([{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [1], attrs: { character: { italic: true }, table: { tableGrid: [1, 2, 3] } } };
            localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }], transformedOps);

        });
    });

});
