/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // setAttributes and local mergeParagraph (handleSetAttrsParaMerge)
        it('should calculate valid transformed setAttributes operation after local mergeParagraph operation', function () {

            // the external setAttributes is (far) behind the mergeParagraph operation
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 4]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([2, 8]); // the position of the external operation is modified

            // the external setAttributes is directly behind the local merge
            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 9], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 6]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([1, 11]); // the position of the external operation is modified

            // the external setAttributes is exactly in the paragraph of the local merge
            oneOperation = { name: 'setAttributes', start: [1, 4], end: [1, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 8, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(8); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 4]); // not modified
            expect(oneOperation.end).toEqual([1, 6]); // not modified

            // the external setAttributes is in front of the local merge
            oneOperation = { name: 'setAttributes', start: [1, 4], end: [1, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 4]); // not modified
            expect(oneOperation.end).toEqual([1, 6]); // not modified

            // the external setAttributes is inside a table cell behind the top level local merge
            oneOperation = { name: 'setAttributes', start: [3, 2, 2, 1], end: [3, 2, 2, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 2, 2, 1]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([2, 2, 2, 5]); // the position of the external operation is modified

            // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
            oneOperation = { name: 'setAttributes', start: [1, 2, 2, 1], end: [1, 2, 2, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 1]); // not modified
            expect(oneOperation.end).toEqual([1, 2, 2, 5]); // not modified

            // the external setAttributes is top level and in front of the local merge inside a table cell -> no influence
            oneOperation = { name: 'setAttributes', start: [1, 1], end: [1, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 1]); // not modified
            expect(oneOperation.end).toEqual([1, 4]); // not modified

            // the external setAttributes is top level and behind the local merge inside a table cell -> no influence
            oneOperation = { name: 'setAttributes', start: [3, 1], end: [3, 6], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 1]); // not modified
            expect(oneOperation.end).toEqual([3, 6]); // not modified

            // the external setAttributes is (far) behind the local merge inside the same table cell
            oneOperation = { name: 'setAttributes', start: [1, 2, 2, 3, 2], end: [1, 2, 2, 3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 2, 2]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([1, 2, 2, 2, 6]); // the position of the external operation is modified

            // the external setAttributes is directly behind the local merge inside the same table cell
            oneOperation = { name: 'setAttributes', start: [1, 3, 2, 2, 4], end: [1, 3, 2, 2, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 1], paralength: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 3, 2, 1, 7]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([1, 3, 2, 1, 10]); // the position of the external operation is modified

            // the external setAttributes and the local merge are in different table cells -> no influence
            oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(5); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1, 2, 2]); // not modified
            expect(oneOperation.end).toEqual([2, 1, 2, 6]); // not modified

            oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], paralength: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2, 2]);
            expect(oneOperation.end).toEqual([1, 1, 2, 6]);

            oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 6, 2, 2]);
            expect(oneOperation.end).toEqual([1, 6, 2, 6]);

            oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 2]);
            expect(oneOperation.end).toEqual([2, 1, 2, 6]);

            oneOperation = { name: 'setAttributes', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [1], opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes and external mergeParagraph (handleSetAttrsParaMerge)
        it('should calculate valid transformed setAttributes operation after external mergeParagraph operation', function () {
            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes and local mergeTable (handleSetAttrsParaMerge)
        it('should calculate valid transformed setAttributes operation after local mergeTable operation', function () {

            // the external setAttributes is (far) behind the mergeParagraph operation
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 4]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([2, 8]); // the position of the external operation is modified

            oneOperation = { name: 'setAttributes', start: [3, 4, 0, 0, 2], end: [3, 4, 0, 0, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 4, 0, 0, 2]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([2, 4, 0, 0, 6]); // the position of the external operation is modified

            // the external setAttributes is directly behind the local merge
            oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 9], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 6]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([1, 11]); // the position of the external operation is modified

            oneOperation = { name: 'setAttributes', start: [2, 4, 0, 0, 2], end: [2, 4, 0, 0, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 6, 0, 0, 2]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([1, 6, 0, 0, 4]); // the position of the external operation is modified

            // the external setAttributes is exactly in the paragraph of the local merge
            oneOperation = { name: 'setAttributes', start: [1, 4], end: [1, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 8, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(8); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 4]); // not modified
            expect(oneOperation.end).toEqual([1, 6]); // not modified

            oneOperation = { name: 'setAttributes', start: [1, 4, 0, 0, 2], end: [1, 4, 0, 0, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 8, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(8); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 4, 0, 0, 2]); // not modified
            expect(oneOperation.end).toEqual([1, 4, 0, 0, 6]); // not modified

            // the external setAttributes is in front of the local merge
            oneOperation = { name: 'setAttributes', start: [1, 4], end: [1, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 4]); // not modified
            expect(oneOperation.end).toEqual([1, 6]); // not modified

            oneOperation = { name: 'setAttributes', start: [1, 4, 0, 0, 2], end: [1, 4, 0, 0, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 4, 0, 0, 2]); // not modified
            expect(oneOperation.end).toEqual([1, 4, 0, 0, 6]); // not modified

            // the external setAttributes is inside a table cell behind the top level local merge
            oneOperation = { name: 'setAttributes', start: [3, 2, 2, 1], end: [3, 2, 2, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 2, 2, 1]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([2, 2, 2, 5]); // the position of the external operation is modified

            // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
            oneOperation = { name: 'setAttributes', start: [1, 2, 2, 1], end: [1, 2, 2, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 1]); // not modified
            expect(oneOperation.end).toEqual([1, 2, 2, 5]); // not modified

            // the external setAttributes is top level and in front of the local merge inside a table cell -> no influence
            oneOperation = { name: 'setAttributes', start: [1, 1], end: [1, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 1]); // not modified
            expect(oneOperation.end).toEqual([1, 4]); // not modified

            // the external setAttributes is top level and behind the local merge inside a table cell -> no influence
            oneOperation = { name: 'setAttributes', start: [3, 1], end: [3, 6], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 1]); // not modified
            expect(oneOperation.end).toEqual([3, 6]); // not modified

            // the external setAttributes is (far) behind the local merge inside the same table cell
            oneOperation = { name: 'setAttributes', start: [1, 2, 2, 3, 2], end: [1, 2, 2, 3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 2, 2]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([1, 2, 2, 2, 6]); // the position of the external operation is modified

            // the external setAttributes is directly behind the local merge inside the same table cell
            oneOperation = { name: 'setAttributes', start: [1, 3, 2, 2, 4], end: [1, 3, 2, 2, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 1], rowcount: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 3, 2, 1, 7]); // the position of the external operation is modified
            expect(oneOperation.end).toEqual([1, 3, 2, 1, 10]); // the position of the external operation is modified

            // the external setAttributes and the local merge are in different table cells -> no influence
            oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1, 2, 2]); // not modified
            expect(oneOperation.end).toEqual([2, 1, 2, 6]); // not modified

            oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], rowcount: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 2, 2]);
            expect(oneOperation.end).toEqual([1, 1, 2, 6]);

            oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 6, 2, 2]);
            expect(oneOperation.end).toEqual([1, 6, 2, 6]);

            oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 2]);
            expect(oneOperation.end).toEqual([2, 1, 2, 6]);

            oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 2], rowcount: 5, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 2]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 1, 2, 2]);
            expect(oneOperation.end).toEqual([2, 1, 2, 6]);
        });

        // updateField and local mergeParagraph (handleSetAttrsMergeComp)
        it('should calculate valid transformed updateField operation after local mergeParagraph operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [2, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', start: [1, 4], representation: 'abc', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', start: [2, 2], representation: 'abc', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', start: [1, 1], representation: 'abc', opl: 1, osn: 1 }], transformedOps);
        });

        // updateField and local mergeTable (handleSetAttrsMergeComp)
        it('should calculate valid transformed updateField operation after local mergeTable operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', start: [1, 4, 0, 1, 1], representation: 'abc', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', start: [2, 2], representation: 'abc', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 1, 0, 1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', start: [1, 1, 0, 1, 1], representation: 'abc', opl: 1, osn: 1 }], transformedOps);
        });

        // updateField and external mergeParagraph (handleSetAttrsMergeComp)
        it('should calculate valid transformed updateField operation after external mergeParagraph operation', function () {

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [2, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', start: [2, 2], representation: 'abc', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 1], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', start: [1, 1], representation: 'abc', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);
        });

        // updateField and external mergeTable (handleSetAttrsMergeComp)
        it('should calculate valid transformed updateField operation after external mergeTable operation', function () {

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', start: [1, 4, 0, 1, 1], representation: 'abc', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', start: [2, 2], representation: 'abc', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 1, 0, 1, 1], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', start: [1, 1, 0, 1, 1], representation: 'abc', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }], transformedOps);
        });

        // Testing the automatically generated external setAttributes operations with noUndo for font size adaptions (handleSetAttrsMergeComp)
        it('should calculate valid transformed local mergeParagraph operation after external autogenerated setAttributes operation', function () {
            oneOperation = { name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 1, 4], paralength: 0, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeParagraph', start: [0, 1, 4], paralength: 0, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }], transformedOps);
        });

        // Testing the automatically generated internal setAttributes operations with noUndo for font size adaptions (handleSetAttrsMergeComp)
        it('should calculate valid transformed local mergeParagraph operation after internal autogenerated setAttributes operation', function () {
            oneOperation = { name: 'mergeParagraph', start: [0, 1, 4], paralength: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeParagraph', start: [0, 1, 4], paralength: 0, opl: 1, osn: 1 }], transformedOps);
        });

    });

});
