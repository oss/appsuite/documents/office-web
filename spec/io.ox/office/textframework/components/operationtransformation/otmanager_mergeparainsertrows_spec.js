/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;


    describe('method transformOperation', function () {

        // insertRows and local mergeParagraph operation (handleMergeCompInsertRows)
        it('should calculate valid transformed insertRows operation after local mergeParagraph operation', function () {

            oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);
            expect(oneOperation.referenceRow).toBe(0);

            oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [5], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 1]);
            expect(oneOperation.referenceRow).toBe(0);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);
            expect(oneOperation.referenceRow).toBe(0);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);
            expect(oneOperation.referenceRow).toBe(0);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);
            expect(oneOperation.referenceRow).toBe(0);

            oneOperation = { name: 'insertRows', start: [3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [3, 1, 1, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 9, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 6, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);
        });

        // insertRows and local mergeTable operation (handleMergeCompInsertRows)
        it('should calculate valid transformed insertRows operation after local mergeTable operation', function () {

            oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);
            expect(oneOperation.referenceRow).toBe(0);

            oneOperation = { name: 'insertRows', start: [2, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 7, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 8]);
            expect(oneOperation.referenceRow).toBe(7);

            oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 1]);
            expect(oneOperation.referenceRow).toBe(0);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);
            expect(oneOperation.referenceRow).toBe(0);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);
            expect(oneOperation.referenceRow).toBe(0);

            oneOperation = { name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1]);
            expect(oneOperation.referenceRow).toBe(0);

            oneOperation = { name: 'insertRows', start: [3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [3, 1, 1, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 9, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);

            oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 6, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2]);
            expect(oneOperation.referenceRow).toBe(1);
        });

        // insertRows and local mergeTable operation (handleMergeCompInsertRows)
        it('should calculate valid transformed insertRows operation after local mergeTable operation with special handling of property referenceRow', function () {

            oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 6, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [4], rowcount: 4, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'mergeTable', start: [4], rowcount: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }], transformedOps);
        });

        // mergeTable and local insertRows operation (handleMergeCompInsertRows)
        it('should calculate valid transformed mergeTable operation after local insertRows operation with special handling of property referenceRow', function () {

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 6, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'mergeTable', start: [4], rowcount: 4, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'mergeTable', start: [4], rowcount: 4, opl: 1, osn: 1 }], transformedOps);
        });

        // insertCells and local mergeParagraph operation (handleMergeCompInsertRows)
        it('should calculate valid transformed insertCells operation after local mergeParagraph operation', function () {

            oneOperation = { name: 'insertCells', start: [4, 1, 1], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1]);

            oneOperation = { name: 'insertCells', start: [4, 1, 1], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [5], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 1, 1]);

            oneOperation = { name: 'insertCells', start: [3, 1, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 5, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 0]);

            oneOperation = { name: 'insertCells', start: [3, 0, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 0]);

            oneOperation = { name: 'insertCells', start: [3, 2, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 0]);

            oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1]);

            oneOperation = { name: 'insertCells', start: [3, 3, 3], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 3]);

            oneOperation = { name: 'insertCells', start: [3, 2, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2]);

            oneOperation = { name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 2, 2, 2]);

            oneOperation = { name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2, 2]);

            oneOperation = { name: 'insertCells', start: [3, 1, 1, 3, 2, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 2, 2]);

            oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 9, 2, 2, 2]);

            oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2, 2]);

            oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2, 2]);

            oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2, 2]);

            oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 1], count: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 2, 1, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 2, 4, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2, 1]);

            oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 3], count: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 2, 2, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 2, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2, 3]);
        });

        // insertCells and local mergeTable operation (handleMergeCompInsertRows)
        it('should calculate valid transformed insertCells operation after local mergeTable operation', function () {

            oneOperation = { name: 'insertCells', start: [4, 1, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2]);

            oneOperation = { name: 'insertCells', start: [2, 1, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 7, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 8, 2]);

            oneOperation = { name: 'insertCells', start: [4, 1, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 1, 2]);

            oneOperation = { name: 'insertCells', start: [3, 1, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 5, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2]);

            oneOperation = { name: 'insertCells', start: [3, 1, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2]);

            oneOperation = { name: 'insertCells', start: [3, 3, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 3, 2]);

            oneOperation = { name: 'insertCells', start: [3, 2, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 2, 2]);

            oneOperation = { name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 2, 2, 2]);

            oneOperation = { name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2, 2]);

            oneOperation = { name: 'insertCells', start: [3, 1, 1, 3, 2, 2], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 3, 2, 2]);

            oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 9, 2, 2, 2]);

            oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2, 2]);

            oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2, 2]);

            oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2, 2]);

            oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 2, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 2, 5, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 2, 2]);
        });
    });

});
