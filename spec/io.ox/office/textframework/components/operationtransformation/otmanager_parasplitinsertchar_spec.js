/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        // insert and local splitParagraph (handleParaSplitInsertChar)
        it('should calculate valid transformed insertText operation after splitParagraph operation', function () {

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 10], text: 'o' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]); // not modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 11]); // modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 6], opl: 1, osn: 1 }] }]; // same position
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0]); // local split operation shifts insert into second paragraph
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 0, 2, 2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 0, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 6]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 0, 2, 2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 0, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2, 2, 1, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 2, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 1, 4]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2, 2, 3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 3, 4]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2, 2, 1, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 2, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 1, 4]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 3], text: 'ooo' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2, 2, 2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 2, 3, 0]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 2, 3]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 5, 2, 3], opl: 1, osn: 1 }] }]; // split in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 8, 2, 3]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 1, 2, 3], opl: 1, osn: 1 }] }]; // split in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 3]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ooo' };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 5, 2, 3], opl: 1, osn: 1 }] }]; // split in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 2, 3]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3], text: 'ooo' }; // insert text in a text frame
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 3, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
        });

        // splitParagraph and local insert (handleParaSplitInsertChar)
        it('should calculate valid transformed splitParagraph operation after insert operation', function () {

            oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 10], text: 'o' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 8]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]); // modified

            oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 11]);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified

            oneOperation = { name: 'splitParagraph', start: [1, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' }] }]; // same position
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6]);
            expect(localActions[0].operations[0].start).toEqual([2, 0]); // modified -> insert is shifted by the split

            oneOperation = { name: 'splitParagraph', start: [0, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 10], text: 'o' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 8]);
            expect(localActions[0].operations[0].start).toEqual([2, 10]); // modified

            oneOperation = { name: 'splitParagraph', start: [0, 2, 2, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 10], text: 'o' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2, 2, 4, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]); // not modified

            oneOperation = { name: 'splitParagraph', start: [0, 2, 2, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 2, 2, 4, 1], text: 'ooo' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2, 2, 4, 5]);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2, 4, 1]); // not modified

            oneOperation = { name: 'splitParagraph', start: [0, 2, 2, 3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 2, 1, 4, 2], text: 'ooo' }] }]; // neighbour cell
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2, 2, 3, 1]);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 1, 4, 2]); // not modified

            oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 10], text: 'o' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 8]);
            expect(localActions[0].operations[0].start).toEqual([0, 10]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2, 2, 2, 3], text: 'ooo' };
            localActions = [{ operations: [{ name: 'insertText', start: [2, 2, 2, 2, 3], text: 'ooo', opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 2, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 3, 0]);
        });

    });

});
