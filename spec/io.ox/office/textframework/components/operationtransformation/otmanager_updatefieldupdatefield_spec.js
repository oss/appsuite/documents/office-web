/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // updateField and local updateField (handleUpdateFieldUpdateField)
        it('should calculate valid transformed updateField operation after local updateField operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4], type: 'PAGE' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], type: 'PAGE' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', target: 'abc' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', target: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'DATE' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'abc' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'abc' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'def' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'def' }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'abc' };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);
        });
    });

});
