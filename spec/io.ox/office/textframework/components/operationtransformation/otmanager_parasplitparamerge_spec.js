/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        // splitParagraph and local mergeParagraph (handleParaSplitParaMerge)
        it('should calculate valid transformed splitParagraph operation after local mergeParagraph operation', function () {

            // split long before the local merge
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([4]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the paragraph length does not change

            // split in the previous paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 5] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([4]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the paragraph length does not change

            // split and merge in the same paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([3]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].paralength).toBe(8); // the paragraph length does change

            // split in the directly following paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 5] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 15]); // the external operation is modified because of local merge operation
            expect(localActions[0].operations[0].start).toEqual([2]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the paragraph length does not change

            // split far after the local merge
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [5, 5] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4, 5]); // the external operation is modified
            expect(localActions[0].operations[0].start).toEqual([3]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 2, 1, 4, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 1, 4, 2]); // the external operation is modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(5); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 3], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 2]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(5); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 2, 3], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([3, 2, 2, 3]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].paralength).toBe(5); // the paragraph length does not change

            // split and merge with in the same table cell
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 3, 2], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1, 4]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 3, 3]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].paralength).toBe(5); // the paragraph length does not change

            // split and merge with in different table cells
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 2], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1, 4]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 2]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(5); // the paragraph length does not change

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 11, 1], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 3]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 3]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 3]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 3]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 1] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 3]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 4]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 15, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].paralength).toBe(10);
        });

        // mergeParagraph and local splitParagraph (handleParaSplitParaMerge)
        it('should calculate valid transformed mergeParagraph operation after local splitParagraph operation', function () {

            // merge long before the local split
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 10 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]); // the external operation is not modified
            expect(oneOperation.paralength).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 5]); // the start position of the locally saved operation is modified

            // merge in the previous paragraph
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 10 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]); // the external operation is not modified
            expect(oneOperation.paralength).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 15]); // the start position of the locally saved operation is modified

            // merge and split in the same paragraph
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 10 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3]); // the external operation is modified
            expect(oneOperation.paralength).toBe(7); // the paragraph length does change
            expect(localActions[0].operations[0].start).toEqual([2, 3]); // the start position of the locally saved operation is not modified

            // merge in the directly following paragraph
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 10 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]); // the external operation is modified
            expect(oneOperation.paralength).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 3]); // the start position of the locally saved operation is not modified

            // split far after the local merge
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [4], paralength: 10 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([5]); // the external operation is modified
            expect(oneOperation.paralength).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 3]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3, 2, 1, 4], paralength: 6 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4, 2, 1, 4]); // the external operation is modified
            expect(oneOperation.paralength).toBe(6); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 3]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 6 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 2, 3, 1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3]); // the external operation is not modified
            expect(oneOperation.paralength).toBe(6); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3, 1]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 2 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 2, 2, 3, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]); // the external operation is not modified
            expect(oneOperation.paralength).toBe(2); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 3, 1]); // the start position of the locally saved operation is modified

            // split and merge with in the same table cell
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1], paralength: 4 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1]); // the external operation is not modified
            expect(oneOperation.paralength).toBe(4); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 3, 1, 5]); // the start position of the locally saved operation is modified

            // split and merge with in different table cells
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1], paralength: 4 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 2, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1]); // the external operation is not modified
            expect(oneOperation.paralength).toBe(4); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 2, 1]); // the start position of the locally saved operation is not modified
        });

        // splitTable and local mergeParagraph (handleParaSplitParaMerge)
        it('should calculate valid transformed splitTable operation after local mergeParagraph operation', function () {

            // split long before the local merge
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([4]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the paragraph length does not change

            // split in the previous paragraph
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 5] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([4]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the paragraph length does not change

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 5, 6], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 5, 6]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 1, 5, 6], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 5, 6]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 1, 1], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([4, 3, 1, 1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 5] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 0, 2] }; // inside a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 0, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 5, 0, 2] }; // inside a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 15, 0, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 5, 0, 2] }; // inside a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 0, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            // split far after the local merge
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [5, 5] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4, 5]); // the external operation is modified
            expect(localActions[0].operations[0].start).toEqual([3]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 2, 1, 4, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 1, 4, 2]); // the external operation is modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(5); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 3], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 2]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(5); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 2, 3], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([3, 2, 2, 3]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].paralength).toBe(5); // the paragraph length does not change

            // split and merge with in the same table cell
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 3, 2], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1, 4]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 3, 3]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].paralength).toBe(5); // the paragraph length does not change

            // split and merge with in different table cells
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 2], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1, 4]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 2]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(5); // the paragraph length does not change

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 11, 1, 3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 1, 3]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 7, 1, 3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 1, 3]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 7, 1, 3, 2, 1], opl: 1, osn: 1, paralength: 10 }] }]; // textframe in table
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 1, 3, 2, 1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 7, 1, 3, 2, 1], opl: 1, osn: 1, paralength: 10 }] }]; // textframe in table
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([2, 7, 1, 3, 2, 1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 6, 1, 3, 2, 1], opl: 1, osn: 1, paralength: 10 }] }]; // textframe in table
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([0, 6, 1, 3, 2, 1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 6, 1, 3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([0, 6, 1, 3]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 3]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 1] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 3]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 4]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].paralength).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 15, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].paralength).toBe(10);
        });

        // mergeParagraph and local splitTable (handleParaSplitParaMerge)
        it('should calculate valid transformed mergeParagraph operation after local splitTable operation', function () {

            // merge long before the local split
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 10 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]); // the external operation is not modified
            expect(oneOperation.paralength).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 5]); // the start position of the locally saved operation is modified

            // merge in the directly following paragraph
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 10 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]); // the external operation is modified
            expect(oneOperation.paralength).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 3]); // the start position of the locally saved operation is not modified

            // split far after the local merge
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [4], paralength: 10 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([5]); // the external operation is modified
            expect(oneOperation.paralength).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 3]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3, 2, 1, 4], paralength: 6 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4, 2, 1, 4]); // the external operation is modified
            expect(oneOperation.paralength).toBe(6); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 3]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 6 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 2, 3, 1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3]); // the external operation is not modified
            expect(oneOperation.paralength).toBe(6); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3, 1]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 2 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 2, 2, 3, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]); // the external operation is not modified
            expect(oneOperation.paralength).toBe(2); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 3, 1]); // the start position of the locally saved operation is modified

            // split and merge with in the same table cell
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 3, 0], paralength: 4 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 0]); // the external operation is not modified
            expect(oneOperation.paralength).toBe(4); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 3, 1, 1]); // the start position of the locally saved operation is modified

            // split and merge with in the same table cell
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4], paralength: 4 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 5]);
            expect(oneOperation.paralength).toBe(4);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 3, 2, 1]);

            // split and merge with in different table cells
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1], paralength: 4 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 2, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1]); // the external operation is not modified
            expect(oneOperation.paralength).toBe(4); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 2, 1]); // the start position of the locally saved operation is not modified
        });

        // splitTable and local mergeTable (handleParaSplitParaMerge)
        it('should calculate valid transformed splitTable operation after local mergeTable operation', function () {

            // split long before the local merge
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([4]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the paragraph length does not change

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([4]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the paragraph length does not change

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 9 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 5]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([4]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(4); // the paragraph length does not change

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 5, 6], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 5, 6]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 5, 6], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 5, 6]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 1, 1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([4, 3, 1, 1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 0, 2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 0, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 5, 0, 2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 15, 0, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 5, 0, 2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 0, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            // split far after the local merge
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [5, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4, 5]); // the external operation is modified
            expect(localActions[0].operations[0].start).toEqual([3]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 2, 1, 4, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 1, 4, 2]); // the external operation is modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 3], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 2]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 3], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([3, 2, 2, 3]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // the paragraph length does not change

            // split and merge with in the same table cell
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 3, 2], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1, 4]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 3, 3]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // the paragraph length does not change

            // split and merge with in different table cells
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 2], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1, 4]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 2]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // the paragraph length does not change

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0, 11, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 1, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0, 7, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 1, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0, 7, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 1, 3, 2, 1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 7, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([2, 7, 1, 3, 2, 1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0, 6, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([0, 6, 1, 3, 2, 1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0, 6, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([0, 6, 1, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 1] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 4]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 15, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].rowcount).toBe(10);
        });

        // mergeTable and local splitTable (handleParaSplitParaMerge)
        it('should calculate valid transformed mergeTable operation after local splitTable operation', function () {

            // merge long before the local split
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 10 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]); // the external operation is not modified
            expect(oneOperation.rowcount).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 5]); // the start position of the locally saved operation is modified

            // merge in the directly following paragraph
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 10 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]); // the external operation is modified
            expect(oneOperation.rowcount).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 3]); // the start position of the locally saved operation is not modified

            // split far after the local merge
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [4], rowcount: 10 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([5]); // the external operation is modified
            expect(oneOperation.rowcount).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 3]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3, 2, 1, 4], rowcount: 6 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4, 2, 1, 4]); // the external operation is modified
            expect(oneOperation.rowcount).toBe(6); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 3]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 6 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 2, 3, 1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3]); // the external operation is not modified
            expect(oneOperation.rowcount).toBe(6); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3, 1]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 2 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 2, 2, 3, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]); // the external operation is not modified
            expect(oneOperation.rowcount).toBe(2); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 3, 1]); // the start position of the locally saved operation is modified

            // split and merge with in the same table cell
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 0], rowcount: 4 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 0]); // the external operation is not modified
            expect(oneOperation.rowcount).toBe(4); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 3, 1, 1]); // the start position of the locally saved operation is modified

            // split and merge with in the same table cell
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 4], rowcount: 4 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 5]);
            expect(oneOperation.rowcount).toBe(4);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 3, 2, 1]);

            // split and merge with in different table cells
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 1], rowcount: 4 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 2, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1]); // the external operation is not modified
            expect(oneOperation.rowcount).toBe(4); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 2, 1]); // the start position of the locally saved operation is not modified
        });

        // splitParagraph and local mergeTable (handleParaSplitParaMerge)
        it('should calculate valid transformed splitParagraph operation after local mergeTable operation', function () {

            // split long before the local merge
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([4]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the paragraph length does not change

            // split in the previous paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([4]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the paragraph length does not change

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 9 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 5]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([4]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(4); // the paragraph length does not change

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 5, 6], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 5, 6]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 5, 6], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 5, 6]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 1, 1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([4, 3, 1, 1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 0, 2] }; // inside a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 0, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 5, 0, 2] }; // inside a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 15, 0, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 5, 0, 2] }; // inside a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5, 0, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            // split far after the local merge
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [5, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4, 5]); // the external operation is modified
            expect(localActions[0].operations[0].start).toEqual([3]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 2, 1, 4, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 1, 4, 2]); // the external operation is modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 3], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 2]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // the paragraph length does not change

            // split and merge on different paragraph levels
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 3], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([3, 2, 2, 3]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // the paragraph length does not change

            // split and merge with in the same table cell
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 3, 2], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1, 4]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 3, 3]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // the paragraph length does not change

            // split and merge with in different table cells
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 2], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1, 4]); // the external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 2]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // the paragraph length does not change

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0, 11, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 1, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0, 7, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 1, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0, 7, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 1, 3, 2, 1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 7, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([2, 7, 1, 3, 2, 1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0, 6, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([0, 6, 1, 3, 2, 1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0, 6, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([0, 6, 1, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 1] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 4]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 15, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].rowcount).toBe(10);
        });

        // mergeTable and local splitParagraph (handleParaSplitParaMerge)
        it('should calculate valid transformed mergeTable operation after local splitParagraph operation', function () {

            // merge long before the local split
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 10 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]); // the external operation is not modified
            expect(oneOperation.rowcount).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 5]); // the start position of the locally saved operation is modified

            // merge in the directly following paragraph
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 10 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]); // the external operation is modified
            expect(oneOperation.rowcount).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 3]); // the start position of the locally saved operation is not modified

            // split far after the local merge
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [4], rowcount: 10 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([5]); // the external operation is modified
            expect(oneOperation.rowcount).toBe(10); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 3]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3, 2, 1, 4], rowcount: 6 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4, 2, 1, 4]); // the external operation is modified
            expect(oneOperation.rowcount).toBe(6); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 3]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 6 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 2, 3, 1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3]); // the external operation is not modified
            expect(oneOperation.rowcount).toBe(6); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3, 1]); // the start position of the locally saved operation is not modified

            // split and merge on different paragraph levels
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 2 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 2, 2, 3, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]); // the external operation is not modified
            expect(oneOperation.rowcount).toBe(2); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 3, 1]); // the start position of the locally saved operation is modified

            // split and merge with in the same table cell
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 0], rowcount: 4 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 0]); // the external operation is not modified
            expect(oneOperation.rowcount).toBe(4); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 3, 1, 1]); // the start position of the locally saved operation is modified

            // split and merge with in the same table cell
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 4], rowcount: 4 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 5]);
            expect(oneOperation.rowcount).toBe(4);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 3, 2, 1]);

            // split and merge with in different table cells
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 1], rowcount: 4 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 2, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 3, 1]); // the external operation is not modified
            expect(oneOperation.rowcount).toBe(4); // the paragraph length does not change
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 2, 1]); // the start position of the locally saved operation is not modified
        });
    });

});
