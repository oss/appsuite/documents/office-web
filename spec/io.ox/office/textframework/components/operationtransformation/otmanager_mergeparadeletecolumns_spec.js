/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // deleteColumns and local mergeParagraph operation (handleMergeParaDeleteColumns)
        it('should calculate valid transformed deleteColumns operation after local mergeParagraph operation', function () {

            oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [5], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 0, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 0, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 1, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 3, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 4, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 8, 0, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 8, 0, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 8, 1, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 8, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 8, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 8, 3, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 8, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 2]);

            oneOperation = { name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'deleteColumns', start: [3, 1, 1, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 9, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 2, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 4, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 3]);
        });

        // deleteColumns and local mergeTable operation without requirement of new deleteColumns operation (handleMergeParaDeleteColumns)
        it('should calculate valid transformed deleteColumns operation after local mergeTable operation without generating new deleteColumns operation', function () {

            oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 0, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 0, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 1, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 3, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 4, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 3, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 2]);

            oneOperation = { name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'deleteColumns', start: [3, 1, 1, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 9, 2, 3]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 3]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 3]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 0, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 0, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 4, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'deleteColumns', start: [1, 3, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 3]);
        });

        // deleteColumns and local mergeTable operation with requirement of new deleteColumns operation (handleMergeParaDeleteColumns)
        it('should calculate valid transformed deleteColumns operation after local mergeTable operation and generate required deleteColumns operation', function () {

            // the local merge is in the same table: local mergeTable [4] and external deleteColumns [4]
            // -> an additional internal operation: 'deleteColumns [5]' before 'mergeTable [4]' is required
            oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("deleteColumns");
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations[1].name).toBe("mergeTable");
            expect(localActions[0].operations[1].start).toEqual([4]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0]).toEqual(oneOperation);

            // the local merge is in the same table: local mergeTable [1, 2, 2, 4] and external deleteColumns [1, 2, 2, 4]
            // -> an additional internal operation: 'deleteColumns [1, 2, 2, 5]' before 'mergeTable [1, 2, 2, 4]' is required
            oneOperation = { name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("deleteColumns");
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 5]);
            expect(localActions[0].operations[1].name).toBe("mergeTable");
            expect(localActions[0].operations[1].start).toEqual([1, 2, 2, 4]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0]).toEqual(oneOperation);

            // the local merge is in the previous table: local mergeTable [3] and external deleteColumns [4]
            // 1. the external deleteColumns operation must be modified, because of the locally already executed mergeTable [3] operation
            // 2. an additional internal operation: 'deleteColumns [3]' before 'mergeTable [3]' is required
            oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 6, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("deleteColumns");
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[1].name).toBe("mergeTable");
            expect(localActions[0].operations[1].start).toEqual([3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            // the local merge is in the previous table: local mergeTable [1, 2, 2, 3] and external deleteColumns [1, 2, 2, 4]
            // 1. the external deleteColumns operation must be modified, because of the locally already executed mergeTable [1, 2, 2, 3] operation
            // 2. an additional internal operation: 'deleteColumns [1, 2, 2, 3]' before 'mergeTable [1, 2, 2, 3]' is required
            oneOperation = { name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("deleteColumns");
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3]);
            expect(localActions[0].operations[1].name).toBe("mergeTable");
            expect(localActions[0].operations[1].start).toEqual([1, 2, 2, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1, 2, 2, 3]);
        });

        // mergeTable and local deleteColumns operation with requirement of new deleteColumns operation (handleMergeParaDeleteColumns)
        it('should calculate valid transformed mergeTable operation after local deleteColumns operation and generate required deleteColumns operation', function () {

            // the merge is in the same table: local deleteColumns [3] and external mergeTable [3]
            // -> an additional external operation: 'deleteColumns [4]' before 'mergeTable [3]' is required
            oneOperation = { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].name).toBe("deleteColumns");
            expect(transformedOps[0].start).toEqual([5]);
            expect(transformedOps[1].name).toBe("mergeTable");
            expect(transformedOps[1].start).toEqual([4]);

            // the merge is in the same table: local deleteColumns [1, 2, 2, 3] and external mergeTable [1, 2, 2, 3]
            // -> an additional external operation: 'deleteColumns [1, 2, 2, 4]' before 'mergeTable [1, 2, 2, 3]' is required
            oneOperation = { name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].name).toBe("deleteColumns");
            expect(transformedOps[0].start).toEqual([1, 2, 2, 5]);
            expect(transformedOps[1].name).toBe("mergeTable");
            expect(transformedOps[1].start).toEqual([1, 2, 2, 4]);

            // the external merge is in the previous table: local deleteColumns [4] and external mergeTable [3]
            // 1. the internal deleteColumns operation must be modified, because of the external executed mergeTable [3] operation
            // 2. an additional external operation: 'deleteColumns [3]' before 'mergeTable [3]' is required
            oneOperation = { name: 'mergeTable', start: [3], rowcount: 6, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].name).toBe("deleteColumns");
            expect(transformedOps[0].start).toEqual([3]);
            expect(transformedOps[1].name).toBe("mergeTable");
            expect(transformedOps[1].start).toEqual([3]);

            // the external merge is in the previous table: local deleteColumns [1, 2, 2, 4] and external mergeTable [1, 2, 2, 3]
            // 1. the internal deleteColumns operation must be modified, because of the external executed mergeTable [1, 2, 2, 3] operation
            // 2. an additional external operation: 'deleteColumns [1, 2, 2, 3]' before 'mergeTable [1, 2, 2, 3]' is required
            oneOperation = { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].name).toBe("deleteColumns");
            expect(transformedOps[0].start).toEqual([1, 2, 2, 3]);
            expect(transformedOps[1].name).toBe("mergeTable");
            expect(transformedOps[1].start).toEqual([1, 2, 2, 3]);
        });
    });

});
