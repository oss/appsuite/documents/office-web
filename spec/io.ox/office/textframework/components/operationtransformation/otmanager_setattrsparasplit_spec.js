/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        generatedOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // splitParagraph and local setAttributes (handleSetAttrsParaSplit)
        it('should calculate valid transformed splitParagraph operation after local setAttributes operation', function () {

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 10] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 20]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 30]);
            expect(localActions[0].operations[0].start).toEqual([1, 12]);
            expect(localActions[0].operations[0].end).toEqual([1, 16]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 8] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 8]);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 8]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 30]);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 40]);
            expect(localActions[0].operations[0].start).toEqual([3]);

            // Generating new operation for splitted paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 40]);
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations[1].start).toEqual([2]);
            expect(localActions[0].operations[1].end).toBeUndefined();

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 40]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 40]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([3]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1, 2, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 40]);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 2, 6]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 50] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 50]);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, before split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 0]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, but behind split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 5]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3], end: [1, 4, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 4]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 3, 6]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 0, 3, 6]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([1, 4, 3, 6]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 0] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([1, 4, 3, 6]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4, 3, 6]);

            // local change inside one paragraph and an external splitParagraph in this paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [0, 1], end: [0, 12], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is a one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([0, 4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation generated
            generatedOperation = localActions[0].operations[1];
            expect(generatedOperation.name).toBe("setAttributes");
            expect(generatedOperation.start).toEqual([1, 0]);
            expect(generatedOperation.end).toEqual([1, 7]);
            expect(oneOperation.start).toEqual([0, 5]); // external operation is not modified

            // local change inside one paragraph and a following paragraph completely and an external splitParagraph in the completely the removed paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [2], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1, 3]); // external operation is not modified

            // local change inside one paragraph and several following paragraphs completely and an external splitParagraph in the middle of the completely removed paragraphs
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2, 3]); // external operation is not modified

            // local change of several paragraphs completely and an external splitParagraph in the first of the completely removed paragraphs
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1, 3]); // external operation is not modified

            // local change of several paragraphs completely and an external splitParagraph in the last of the completely removed paragraphs
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([3, 3]); // external operation is not modified

            // local change of several paragraphs and an external splitParagraph inside the table
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1, 4, 5, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3, 8]); // the end position of the locally saved operation is also NOT modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2, 1, 4, 5, 3]); // external operation is not modified

            // local change of one complete paragraph and an external splitParagraph inside this paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations[1].start).toEqual([2]);
            expect(localActions[0].operations[1].end).toBeUndefined();
            expect(oneOperation.start).toEqual([1, 2]); // external operation is not modified

            // local change of one complete paragraph and an external splitParagraph inside the following paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toBeUndefined(); // the end position of the locally saved operation is not generated
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2, 2]); // the start position of the external operation is modified

            // local change of one complete paragraph and an external splitParagraph inside the previous paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].end).toBeUndefined(); // the end position of the locally saved operation is not generated
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([0, 2]); // the start position of the external operation is not modified

        });

        // setAttributes and local splitParagraph (handleSetAttrsParaSplit)
        it('should calculate valid transformed setAttributes operation after local splitParagraph operation', function () {

            oneOperation = { name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 10] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 8]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);

            oneOperation = { name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 20]);

            oneOperation = { name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 12]);
            expect(oneOperation.end).toEqual([1, 16]);
            expect(localActions[0].operations[0].start).toEqual([1, 30]);

            oneOperation = { name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 8] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4]);
            expect(oneOperation.end).toEqual([2, 8]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(localActions[0].operations[0].start).toEqual([1, 30]);

            oneOperation = { name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3]);
            expect(localActions[0].operations[0].start).toEqual([1, 40]);

            oneOperation = { name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 40]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(2); // an additional external operation
            expect(transformedOps[0]).toEqual(oneOperation);
            expect(transformedOps[1].name).toBe("setAttributes");
            expect(transformedOps[1].start).toEqual([2]);
            expect(transformedOps[1].end).toBeUndefined();

            oneOperation = { name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.end).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1, 40]);

            oneOperation = { name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.end).toEqual([3]);
            expect(localActions[0].operations[0].start).toEqual([1, 40]);

            oneOperation = { name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1, 2, 2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.end).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 2, 2]);

            oneOperation = { name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 2, 1, 2, 2]);
            expect(oneOperation.end).toEqual([3, 2, 1, 2, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 40]);

            oneOperation = { name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 50] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 3]);
            expect(oneOperation.end).toEqual([0, 8]);
            expect(localActions[0].operations[0].start).toEqual([1, 50]);

            oneOperation = { name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4]);

            oneOperation = { name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] }] }]; // changing paragraph in same cell, before split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 0]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4]);

            oneOperation = { name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] }] }]; // changing paragraph in same cell, but behind split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 5]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4]);

            oneOperation = { name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4]);

            // external change inside one paragraph and local splitParagraph in this paragraph
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [0, 1], end: [0, 12], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [0, 5] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is a one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 5]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations).toHaveLength(1); // there is no new local operation generated
            expect(oneOperation.start).toEqual([0, 1]);
            expect(oneOperation.end).toEqual([0, 4]);
            expect(transformedOps).toHaveLength(2); // an additional external operation
            expect(transformedOps[0]).toEqual(oneOperation);
            expect(transformedOps[1].name).toBe("setAttributes");
            expect(transformedOps[1].start).toEqual([1, 0]);
            expect(transformedOps[1].end).toEqual([1, 7]);

            // external change inside one paragraph and a following paragraph completely and an intenal splitParagraph in the completely the removed paragraph
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [2], end: [3], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1);

            // external change inside one paragraph and several following paragraphs completely and an internal splitParagraph in the middle of the completely removed paragraphs
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [2, 3] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.end).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1);

            // external change of several paragraphs completely and an internal splitParagraph in the first of the completely removed paragraphs
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1);

            // external change of several paragraphs completely and an internal splitParagraph in the last of the completely removed paragraphs
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [3, 3] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1);

            // external change of several paragraphs and an internal splitParagraph inside the table
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1, 4, 5, 3] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 4, 5, 3]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([3, 4]);
            expect(oneOperation.end).toEqual([3, 8]);
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1);

            // external change of one complete paragraph and an internal splitParagraph inside this paragraph
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(2); // an additional external operation
            expect(transformedOps[0]).toEqual(oneOperation);
            expect(transformedOps[1].name).toBe("setAttributes");
            expect(transformedOps[1].start).toEqual([2]);
            expect(transformedOps[1].end).toBeUndefined();

            // local change of one complete paragraph and an external splitParagraph inside the following paragraph
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.end).toBeUndefined(); // not generated
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1);

            // local change of one complete paragraph and an external splitParagraph inside the previous paragraph
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [0, 2] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.end).toBeUndefined(); // not generated
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1);
        });

        // updateField and local splitParagraph (handleSetAttrsParaSplit)
        it('should calculate valid transformed updateField operation after local splitParagraph operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4], representation: 'abc' };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [2, 2], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4], representation: 'abc' };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4], representation: 'abc' };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [2, 0], representation: 'abc' }], transformedOps);
        });

        // updateField and external splitParagraph (handleSetAttrsParaSplit)
        it('should calculate valid transformed updateField operation after external splitParagraph operation', function () {

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [2, 2], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] }], transformedOps);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] }], transformedOps);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [2, 0], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] }], transformedOps);
        });

        // Testing the automatically generated external setAttributes operations with noUndo for font size adaptions (handleSetAttrsSplitPara)
        it('should calculate valid transformed local splitParagraph operation after external autogenerated setAttributes operation on drawing', function () {
            oneOperation = { name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }], transformedOps);
        });

        // Testing the automatically generated internal setAttributes operations with noUndo for font size adaptions (handleSetAttrsSplitPara)
        it('should calculate valid transformed local splitParagraph operation after internal autogenerated setAttributes operation on drawing', function () {
            oneOperation = { name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed local splitParagraph operation after external setAttributes operation on toplevel node', function () {
            oneOperation = { name: 'setAttributes', start: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [0], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed local splitParagraph operation after internal setAttributes operation on toplevel node', function () {
            oneOperation = { name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed local splitParagraph operation after external setAttributes operation on splitted paragraph in drawing', function () {
            oneOperation = { name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }, { name: 'setAttributes', start: [0, 1, 1], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed local splitParagraph operation after internal setAttributes operation on splitted paragraph in drawing', function () {
            oneOperation = { name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }, { name: 'setAttributes', start: [0, 1, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed local splitParagraph operation after external setAttributes operation behind splitted paragraph in drawing', function () {
            oneOperation = { name: 'setAttributes', start: [0, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [0, 1, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed local splitParagraph operation after internal setAttributes operation behind splitted paragraph in drawing', function () {
            oneOperation = { name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed local splitParagraph operation after external setAttributes operation before splitted paragraph in drawing', function () {
            oneOperation = { name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 1, 1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [0, 1, 1, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed local splitParagraph operation after internal setAttributes operation before splitted paragraph in drawing', function () {
            oneOperation = { name: 'splitParagraph', start: [0, 1, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitParagraph', start: [0, 1, 1, 2], opl: 1, osn: 1 }], transformedOps);
        });
    });

});
