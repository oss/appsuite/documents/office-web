/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // splitTable and local insertRows (handleInsertRowsSplitTable)
        it('should calculate valid transformed splitTable operation after insertRows operation', function () {

            oneOperation = { name: 'splitTable', start: [1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 1], count: 2, referenceRow: 0 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].referenceRow).toBe(0);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 2, referenceRow: 0 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].referenceRow).toBe(0);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 0], count: 2, referenceRow: 0 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 0]);
            expect(localActions[0].operations[0].referenceRow).toBe(0);

            oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 6], count: 2, referenceRow: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].referenceRow).toBe(2);

            oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].referenceRow).toBeUndefined();

            oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 0, 1, 0, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 1, 0, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 5, 1, 0, 1], count: 2, referenceRow: 0 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 1, 0, 1]);
            expect(localActions[0].operations[0].referenceRow).toBe(0);

            oneOperation = { name: 'splitTable', start: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2, 1, 0, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 1, 0, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 4], opl: 1, osn: 1 }; // table in drawing
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 8, 1, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 6]);
            expect(localActions[0].operations[0].start).toEqual([2, 8, 1, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], count: 2, referenceRow: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations[0].referenceRow).toBe(3);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 10, 1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], count: 2, referenceRow: 3 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations[0].referenceRow).toBe(3);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 2], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 1], count: 2, referenceRow: 0 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 3, 0, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].referenceRow).toBe(0);

            oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 2], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 0, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 2], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 0, 2, 0, 1], count: 2, referenceRow: 0 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 0, 1]);
            expect(localActions[0].operations[0].referenceRow).toBe(0);

            oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 0, 2, 1, 4, 1, 0, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 2, 3, 1, 0, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 0, 2, 1, 0, 1, 0, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 1, 0, 1, 0, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [2, 0, 1, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 0, 2, 1, 2, 1, 0, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 1, 2, 1, 0, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [4, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([5, 2]);
            expect(localActions[0].operations[0].referenceRow).toBe(1);

            oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [4, 2], count: 2, referenceRow: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 0, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([4, 2]); // -> not modified
            expect(localActions[0].operations[0].referenceRow).toBe(1);
        });

        // splitTable and local insertRows (handleInsertRowsSplitTable)
        it('should calculate valid transformed splitTable operation after insertRows operation with special handling of property referenceRow', function () {

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 1 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 0 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 0 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2 }] }], localActions);
            expect(localActions[0].operations[0].referenceRow).toBeUndefined();
            expectOp([{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 4 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 3 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'splitTable', start: [2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'splitTable', start: [2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 4 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2 }] }], localActions);
            expect(localActions[0].operations[0].referenceRow).toBeUndefined();
            expectOp([{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'splitTable', start: [2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 5 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2 }] }], localActions);
            expect(localActions[0].operations[0].referenceRow).toBeUndefined();
            expectOp([{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }], transformedOps);
        });

        // insertRows and local splitTable (handleInsertRowsSplitTable)
        it('should calculate valid transformed insertRows operation after splitTable operation with special handling of property referenceRow', function () {

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 1 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 0 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 0 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2 }], transformedOps);
            expect(transformedOps[0].referenceRow).toBeUndefined();

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 4 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 3 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 }], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 4 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2 }], transformedOps);
            expect(transformedOps[0].referenceRow).toBeUndefined();

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 5 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2 }], transformedOps);
            expect(transformedOps[0].referenceRow).toBeUndefined();
        });

        // splitTable and local insertCells (handleInsertRowsSplitTable)
        it('should calculate valid transformed splitTable operation after insertCells operation', function () {

            oneOperation = { name: 'splitTable', start: [1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 2]);

            oneOperation = { name: 'splitTable', start: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 1], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 1]);

            oneOperation = { name: 'splitTable', start: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1]);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 1]);

            oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 3]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 3, 3], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 3]);

            oneOperation = { name: 'splitTable', start: [1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 0, 1, 0, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 1, 0, 2]);

            oneOperation = { name: 'splitTable', start: [1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 1, 0, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 1, 0, 2]);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 5, 1, 0, 1, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 1, 0, 1, 2]);

            oneOperation = { name: 'splitTable', start: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 1, 0, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 1, 0, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 2, 3, 2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 1], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 5, 2, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1]);

            oneOperation = { name: 'splitTable', start: [2, 2, 3, 2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 3], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 5, 2, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 3]);

            oneOperation = { name: 'splitTable', start: [2, 2, 3, 2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 4], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 3, 2, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 4]);

            oneOperation = { name: 'splitTable', start: [2, 2, 3, 2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 3, 2, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1]);

            oneOperation = { name: 'splitTable', start: [2, 2, 3, 2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 1], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 3, 2, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 1]);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 4], opl: 1, osn: 1 }; // table in drawing
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 8, 1, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 8, 1, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 2], opl: 1, osn: 1 }; // table in drawing
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 8, 1, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 8, 2, 0, 2]);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 3], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 3]);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [3, 2, 3], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 3]);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 8, 1], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 3, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 8, 1]);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 2]);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [3, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 2], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 0], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 0]);

            oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 2], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 0, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 2], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 0, 2, 0, 1, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 0, 1, 2]);

            oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 0, 2, 1, 4, 1, 0, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 2, 3, 1, 0, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 0, 2, 1, 0, 1, 0, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 1, 0, 1, 0, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 0, 1, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 0, 2, 1, 2, 1, 0, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 1, 2, 1, 0, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [4, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([5, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [4, 2, 2], count: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 0, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([4, 2, 2]); // -> not modified
        });
    });

});
