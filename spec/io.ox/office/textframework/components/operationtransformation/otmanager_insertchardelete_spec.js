/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insert and local delete (handleInsertCharDelete)
        it('should calculate valid transformed insertText operation after internal delete operation', function () {

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'o' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'o' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 7] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 8]);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'o' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]); // the local delete operation must not shift the insert operation
            expect(localActions[0].operations[0].start).toEqual([1, 5]); // the locally saved operation is modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'oooo' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 5] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 3]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([1, 9]); // the end position of the locally saved operation is modified

            // local delete inside one paragraph
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 4], text: 'a' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [0, 8] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([0, 9]); // the end position of the locally saved operation is modified

            // local delete over several paragraphs (includes a mergeParagraph operation) and an external insertText into first paragraph
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 4], text: 'a' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2, 1] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 1]); // the end position of the locally saved operation is also NOT modified
            expect(localActions[0].operations).toHaveLength(2); // there is no new local operation generated

            // local delete over several paragraphs (includes a mergeParagraph operation) and an external insertText into a middle paragraph
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'a' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 2 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 2]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 2]); // the end position of the locally saved operation is also NOT modified
            expect(localActions[0].operations).toHaveLength(2); // there is no new local operation generated

            // local delete over several paragraphs (includes a mergeParagraph operation) and an external insertText in the last paragraph
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2], text: 'a' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 3], end: [2, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 3]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(2); // there is no new local operation generated

            // local delete over several paragraphs (includes a mergeParagraph operation) and an external insertText in the last paragraph behind the delete range
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 4], text: 'a' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 6 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 2]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(2); // there is no new local operation generated
            // the external insertText operation has a new position:
            // - 6 positions are specified from the paragraph length of paragraph 0
            // - in the second paragraph the first three chars are deleted -> one char remains before the inserted 'a'
            // - in sum there are 7 chars before the inserted 'a', so that it is inserted at position [0,7]
            expect(oneOperation.start).toEqual([0, 7]);

            // local delete over several paragraphs (without a following mergeParagraph operation) and an external insertText in the last paragraph behind the delete range
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 7], text: 'a' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2], end: [2, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 4]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no new local operation generated
            expect(oneOperation.start).toEqual([1, 2]); // the following (local) merge is not included here (one paragraph is removed completely)

            // local delete over several paragraphs (includes a mergeParagraph operation) and an external insertText in the following paragraph behind the delete range
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 4], text: 'a' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 2]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(2); // there is no new local operation generated
            expect(oneOperation.start).toEqual([1, 4]); // the external insertText operation has a new position

            // local delete of one complete paragraph (no additional mergeParagraph operation) and an external insertText in the following paragraph behind the delete paragraph
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 4], text: 'a' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations).toHaveLength(1); // there is no new local operation generated
            expect(oneOperation.start).toEqual([1, 4]); // the external insertText operation has a new position

            // local delete of several complete paragraphs (no additional mergeParagraph operation) and an external insertText in the following paragraph behind the last delete paragraph
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [4, 4], text: 'a' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // the end position of the locally saved operation is not not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no new local operation generated
            expect(oneOperation.start).toEqual([1, 4]); // the external insertText operation has a new position

            // local delete of one complete paragraph (no additional mergeParagraph operation) and an external insertText in this deleted paragraph
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 4], text: 'a' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations).toHaveLength(1); // there is no new local operation generated

            // local delete of several complete paragraphs (no additional mergeParagraph operation) and an external insertText inside this deleted paragraphs
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [4, 4], text: 'a' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [5] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no new local operation generated

            // local delete of text range and following merge (example: selection from [2, 2] to [3, 0] and following merge) and an external insertText before the deleted range
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1], text: 'ooo' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 2], end: [2, 5] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [2], paralength: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 5]); // the start position of the locally saved delete operation is modified
            expect(localActions[0].operations[0].end).toEqual([2, 8]); // the end position of the locally saved delete operation is also modified
            expect(localActions[0].operations[1].start).toEqual([2]); // the start position of the locally saved merge operation is NOT modified
            expect(localActions[0].operations[1].paralength).toBe(5); // the paragraph length of the locally saved merge operation is modified(!)
            expect(localActions[0].operations).toHaveLength(2); // there is no new local operation generated
            expect(oneOperation.start).toEqual([2, 1]); // the external insertText operation is not modified

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1], text: 'ooo' };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1, 4, 2], text: 'ooo' }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1, 4, 2], text: 'ooo' }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1, 4, 2], text: 'ooo' }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 4]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1, 4, 2], text: 'ooo' }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 1, 4, 2]);
        });

        // delete and local insert (handleInsertCharDelete)
        it('should calculate valid transformed delete operation after internal insert operation', function () {

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'o' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(oneOperation.end).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 6], end: [1, 8] };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ooo' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 9]);
            expect(oneOperation.end).toEqual([1, 11]);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 3], text: 'o' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 3]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 12], end: [1, 14] };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 12], text: 'ooo' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 15]);
            expect(oneOperation.end).toEqual([1, 17]);
            expect(localActions[0].operations[0].start).toEqual([1, 12]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 7] };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ooo' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [1, 3], end: [1, 10], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 5, 2, 3], text: 'ooo' }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 2, 3]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 4, 5] }; // in a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ooo' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6, 4, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 4] };
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 5, 2, 3], text: 'ooo' }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3]);
            expect(oneOperation.end).toEqual([1, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 4, 5], end: [1, 3, 4, 6] }; // in a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ooo' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6, 4, 5]);
            expect(oneOperation.end).toEqual([1, 6, 4, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 4] }; // in a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4, 2, 3], text: 'ooo' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [1, 3], end: [1, 4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0], end: [5, 10] }; // in a text frame
            localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [2, 7], text: '1' }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.end).toEqual([5, 10]);
            expect(localActions[0].operations).toHaveLength(0);
        });

    });

});
