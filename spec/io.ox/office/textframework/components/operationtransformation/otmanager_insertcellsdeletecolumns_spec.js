/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null,

        // shortcuts for the utils functions
        expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertCells and local deleteColumns operation (handleInsertCellsDeleteColumns)
        // { name: deleteColumns, start: [2], startGrid: 2, endGrid: 3 }
        it('should calculate valid transformed insertCells operation after local deleteColumns operation', function () {

            oneOperation = { name: 'insertCells', start: [3, 2, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [3, 2, 0], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 2, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [3, 2, 1], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 2, 3], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 2, 4], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [3, 2, 2], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 1, 1], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 1, 1], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [4, 1, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [4, 1, 2], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 6, 0, 2, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [3, 6, 0, 2, 2, 2], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 6, 1, 2, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 6, 2, 2, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 6, 3, 2, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [3, 6, 1, 2, 2, 2], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 0, 1, 2, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [3, 0, 1, 2, 2, 2], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 0, 2, 2, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 0, 3, 2, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [3, 0, 2, 2, 2, 2], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 5, 2, 2, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 5, 3, 2, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [3, 5, 2, 2, 2, 2], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 5, 2, 2, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [3, 5, 2, 2, 2, 2], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 5, 2, 2, 2, 2], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [3, 5, 2, 2, 2, 2], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 2, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 2, 0], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 3, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 3, 3, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 3, 0], count: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 3, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 3, 3, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 3, 0], count: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [1, 3, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [1, 3, 0], count: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 3, 1], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 3, 1], count: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 4, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 4, 0], count: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [1, 2, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [1, 2, 0], count: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [1, 3, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [1, 3, 0], count: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [3, 1, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [3, 1, 0], count: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [4, 2, 4, 2, 1, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [4, 2, 4, 2, 1, 0], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 1, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 1, 0], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 5, 3, 0, 0, 1, 1, 2, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 5, 1, 0, 0, 1, 1, 2, 0], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 5, 2, 0, 0, 1, 1, 2, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 5, 1, 0, 0, 1, 1, 2, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 5, 0, 0, 0, 1, 1, 2, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 5, 0, 0, 0, 1, 1, 2, 0], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 1, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 1, 0], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 1, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 1, 0], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 1, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 2, 0, 2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 2, 0, 2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 1, 0], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 2, 0], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 2, 0, 2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 2, 1, 2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 2, 0], count: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertCells', start: [2, 2, 0], count: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 2, 0, 2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 2, 2, 2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertCells', start: [2, 2, 0], count: 2, opl: 1, osn: 1 }], transformedOps);

        });
    });

});
