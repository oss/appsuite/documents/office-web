/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // mergeParagraph and local delete (handleParaMergeDelete)
        it('should calculate valid transformed mergeParagraph operation using local delete operation', function () {

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.paralength).toBe(9);
            expect(localActions[0].operations[0].start).toEqual([1, 8]); // the locally saved operation is not modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 22], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([1, 22]); // the locally saved operation is not modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 12], end: [1, 16], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 22]); // the locally saved delete operation is modified
            expect(localActions[0].operations[0].end).toEqual([0, 26]); // the locally saved delete operation is modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved delete operation is modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [4], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [0], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0]); // the locally saved delete operation is not modified
            expect(localActions[0].operations[0].end).toEqual([1]); // the locally saved delete operation is not modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [4], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 4]); // the locally saved delete operation is not modified
            expect(localActions[0].operations[0].end).toEqual([1, 3]); // the locally saved delete operation is not modified
            expect(transformedOps).toHaveLength(1);

            // this test includes a following mergeParagraph operation
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [4], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [1, 3], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [0], op2: 1, osn: 1, paralength: 4 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3]); // the external operation is modified
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 4]); // the locally saved delete operation is not modified
            expect(localActions[0].operations[0].end).toEqual([1, 3]); // the locally saved delete operation is not modified
            expect(localActions[0].operations[1].start).toEqual([0]);
            expect(localActions[0].operations[1].paralength).toBe(4);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 2, 3]);
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([2]); // the locally saved delete operation is not modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before merge
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 2]);
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 0]); // the locally saved delete operation is not modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 5], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [4, 5, 6, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5, 6, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [3, 1, 2, 3], paralength: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 3, 5], opl: 1, osn: 1 }, { name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([4]); // the locally saved delete operation is not modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 12 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([1, 4]); // the locally saved operation is not modified
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.paralength).toBe(8);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 6]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.paralength).toBe(5);
            expect(transformedOps).toHaveLength(1);

            // the merge is inside the deleted range (that covers more than one paragraph)
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 2 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the deleted range (that covers more than one paragraph)
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 2 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 3]);
            expect(transformedOps).toHaveLength(0);

            // the merge is behind the deleted range (that covers more than one paragraph)
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]); // only paragraph [2] is removed completely
            expect(oneOperation.paralength).toBe(1); // from the 3 characters in paragraph [3], the first 2 are removed
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1]);
            expect(transformedOps).toHaveLength(1);

            // the merge is inside the deleted range (that covers more than one paragraph)
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 2 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the deleted range (that covers more than one paragraph)
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 2 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the deleted range in a completely removed paragraph
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 2 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1]); // the end position becomes a text position (2 characters are removed)
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the only completely removed paragraph
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 4 };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 0]); // the end position becomes a text position (4 characters are removed)
            expect(localActions[0].operations[0].end).toEqual([1, 3]); // the end position becomes a text position (4 characters are removed)
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the first of two completely removed paragraphs
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 4 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([1]); // start and end position are the same
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the second of three completely removed paragraphs
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 4 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 4 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 1]);
            expect(transformedOps).toHaveLength(0);

            // the merge is directly behind the deleted range (that goes to the end of the paragraph)
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.paralength).toBe(1); // from the 5 characters in paragraph [1], all except the first are removed
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 4]);
            expect(transformedOps).toHaveLength(1);

            // the merge is inside a removed range, but no complete paragraph is removed
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([0, 4]);
            expect(transformedOps).toHaveLength(1);
            // the external mergeParagraph operation has NOT the marker for ignoring, because a delete operation
            // from [0,1] to [1,1] does not remove any paragraph. So it is wrong to reduce the delete operation
            // and therefore ignore the merge operation -> this merge is still required!

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // this test includes no following mergeParagraph operation
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 6], end: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 6]);
            expect(localActions[0].operations[0].end).toEqual([0, 14]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);

            // this test includes a following local mergeParagraph operation
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [1, 4], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [0], op2: 1, osn: 1, paralength: 4 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 4]);
            expect(localActions[0].operations[0].end).toEqual([0, 14]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);
            // the external mergeParagraph operation got the marker from the mergeParagraph-mergeParagraph handler

            // this test includes no following mergeParagraph operation, but really removes one paragraph locally, so that merge can be ignored
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 6], end: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.paralength).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 6]);
            expect(localActions[0].operations[0].end).toEqual([1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2], paralength: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [0], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2], paralength: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [0], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2], paralength: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2], paralength: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 4 };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 0]);
            expect(localActions[0].operations[0].end).toEqual([1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 6, 4], paralength: 4 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 2], end: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 4]);
            expect(oneOperation.start).toEqual([1, 3, 4]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 6, 4], paralength: 4 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 7], end: [1, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 7]);
            expect(localActions[0].operations[0].end).toEqual([1, 8]);
            expect(oneOperation.start).toEqual([1, 6, 4]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 6, 4], paralength: 4 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [0, 7], end: [0, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 7]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);
            expect(oneOperation.start).toEqual([1, 6, 4]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4], end: [5, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 4], end: [4, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4], end: [4, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 4], end: [3, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        // delete and local mergeParagraph (handleParaMergeDelete)
        it('should calculate valid transformed delete operation after local mergeParagraph operation', function () {

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 7] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 17]);
            expect(localActions[0].operations[0].start).toEqual([0]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 12 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10]);
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(11); // the length is modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 12 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10]);
            expect(oneOperation.end).toEqual([1, 14]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].paralength).toBe(12);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the length is modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]); // the position is not modified
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 3], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]); // the position is not modified
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 3]); // the locally saved operation is modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 2, 18]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 6], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4]); // the position is not modified
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 5]); // the locally saved operation is modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 8]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 12], end: [4, 6] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 22]); // the position is modified
            expect(oneOperation.end).toEqual([3, 6]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 14], end: [2, 18] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 24]); // the position is modified
            expect(oneOperation.end).toEqual([1, 28]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 22 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 14]); // the position is not modified
            expect(oneOperation.end).toEqual([1, 18]); // the position is not modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved start position of the operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(17); // the length is modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 14], end: [2, 18] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 14]); // the position is modified
            expect(oneOperation.end).toEqual([1, 18]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([0]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).toBe(10); // the length is not modified

            // the paragraph is merged directly behind the last deleted position
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 4] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]); // no modification
            expect(oneOperation.end).toEqual([1, 4]); // no modification
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].paralength).toBe(1); // the length is modified

            oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 4], end: [5, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [1, 4], end: [4, 4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 4], end: [4, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [1, 4], end: [3, 7], opl: 1, osn: 1 }], transformedOps);
        });

        // mergeTable and local delete (handleParaMergeDelete)
        it('should calculate valid transformed mergeTable operation using local delete operation', function () {

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.rowcount).toBe(9);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8, 6, 4, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 6, 4, 2]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [2], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 22], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([1, 22]); // the locally saved operation is not modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 12], end: [1, 16], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 22]);
            expect(localActions[0].operations[0].end).toEqual([0, 26]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 2, 0, 0, 6], end: [1, 2, 0, 0, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 12, 0, 0, 6]);
            expect(localActions[0].operations[0].end).toEqual([0, 12, 0, 0, 8]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 2, 0, 0, 6], end: [2, 2, 0, 0, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 0, 0, 6]);
            expect(localActions[0].operations[0].end).toEqual([1, 2, 0, 0, 8]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 9, 0, 0, 6], end: [0, 9, 0, 0, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 9, 0, 0, 6]);
            expect(localActions[0].operations[0].end).toEqual([0, 9, 0, 0, 8]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved delete operation is modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [4], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [0], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0]); // the locally saved delete operation is not modified
            expect(localActions[0].operations[0].end).toEqual([1]); // the locally saved delete operation is not modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [4], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 4]); // the locally saved delete operation is not modified
            expect(localActions[0].operations[0].end).toEqual([1, 3]); // the locally saved delete operation is not modified
            expect(transformedOps).toHaveLength(1);

            // // this test includes a following mergeTable operation (TODO)
            // oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [4], rowcount: 10 };
            // localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [1, 3], opl: 1, osn: 1 }, { name: 'mergeTable', start: [0], op2: 1, osn: 1, rowcount: 4 }] }];
            // transformedOps = otManager.transformOperation(oneOperation, localActions);
            // expect(oneOperation.start).toEqual([3]); // the external operation is modified
            // expect(oneOperation.rowcount).toBe(10);
            // expect(localActions[0].operations[0].start).toEqual([0, 4]); // the locally saved delete operation is not modified
            // expect(localActions[0].operations[0].end).toEqual([1, 3]); // the locally saved delete operation is not modified
            // expect(localActions[0].operations[1].start).toEqual([0]);
            // expect(localActions[0].operations[1].rowcount).toBe(4);
            // expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3, 1, 2, 3], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 2, 3]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3, 1, 2, 3], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 2]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 0]); // the locally saved delete operation is not modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [4, 5, 6, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5, 6, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeTable', start: [3, 1, 2, 3], rowcount: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [3, 1, 2, 3, 5], opl: 1, osn: 1 }, { name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3, 1, 2, 3], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([4]); // the locally saved delete operation is not modified
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 12 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([1, 4]); // the locally saved operation is not modified
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.rowcount).toBe(8);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 6]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.rowcount).toBe(5);
            expect(transformedOps).toHaveLength(1);

            // the merge is inside the deleted range (that covers more than one paragraph)
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 2 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1]);
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the deleted range (that covers more than one paragraph)
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [2], rowcount: 2 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 3]);
            expect(transformedOps).toHaveLength(0);

            // the merge is behind the deleted range (that covers more than one paragraph)
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]); // only paragraph [2] is removed completely
            expect(oneOperation.rowcount).toBe(1); // from the 3 characters in paragraph [3], the first 2 are removed
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1]);
            expect(transformedOps).toHaveLength(1);

            // the merge is inside the deleted range (that covers more than one paragraph)
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 2 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the deleted range (that covers more than one paragraph)
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [2], rowcount: 2 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the deleted range in a completely removed paragraph
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 2 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1]); // the end position becomes a row position (2 rows are removed)
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the only completely removed paragraph
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 4 };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 0]); // the end position becomes a text position (4 characters are removed)
            expect(localActions[0].operations[0].end).toEqual([1, 3]); // the end position becomes a text position (4 characters are removed)
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the first of two completely removed paragraphs
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 4 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([1]); // start and end position are the same
            expect(transformedOps).toHaveLength(0);

            // the merge is inside the second of three completely removed paragraphs
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [2], rowcount: 4 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 4 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 1]);
            expect(transformedOps).toHaveLength(0);

            // the merge is directly behind the deleted range (that goes to the end of the paragraph)
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.rowcount).toBe(1); // from the 5 characters in paragraph [1], all except the first are removed
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 4]);
            expect(transformedOps).toHaveLength(1);

            // the merge is inside a removed range, but no complete paragraph is removed
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [1, 1], opl: 1, osn: 1 }] }]; // without merge
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([0, 4]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // this test includes no following mergeTable operation
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 6], end: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 6]);
            expect(localActions[0].operations[0].end).toEqual([0, 14]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);

            // // this test includes a following local mergeTable operation (TODO)
            // oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
            // localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [1, 4], opl: 1, osn: 1 }, { name: 'mergeTable', start: [0], op2: 1, osn: 1, rowcount: 4 }] }];
            // transformedOps = otManager.transformOperation(oneOperation, localActions);
            // expect(oneOperation.start).toEqual([0]);
            // expect(oneOperation.rowcount).toBe(10);
            // expect(localActions[0].operations[0].start).toEqual([0, 4]);
            // expect(localActions[0].operations[0].end).toEqual([0, 14]);
            // expect(localActions[0].operations).toHaveLength(1);
            // expect(transformedOps).toHaveLength(0);
            // // the external mergeTable operation got the marker from the mergeTable-mergeTable handler

            // this test includes no following mergeTable operation, but really removes one paragraph locally, so that merge can be ignored
            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 6], end: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.rowcount).toBe(10);
            expect(localActions[0].operations[0].start).toEqual([0, 6]);
            expect(localActions[0].operations[0].end).toEqual([1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 2, 2], rowcount: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [0], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 2, 2], rowcount: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [0], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 2, 2], rowcount: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 2, 2], rowcount: 5 };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 4 };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 0]);
            expect(localActions[0].operations[0].end).toEqual([1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(0);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 6, 4], rowcount: 4 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 2], end: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 4]);
            expect(oneOperation.start).toEqual([1, 3, 4]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 6, 4], rowcount: 4 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 7], end: [1, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 7]);
            expect(localActions[0].operations[0].end).toEqual([1, 8]);
            expect(oneOperation.start).toEqual([1, 6, 4]);
            expect(transformedOps).toHaveLength(1);

            oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 6, 4], rowcount: 4 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [0, 7], end: [0, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 7]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);
            expect(oneOperation.start).toEqual([1, 6, 4]);
            expect(transformedOps).toHaveLength(1);
        });

        // delete and local mergeTable (handleParaMergeDelete)
        it('should calculate valid transformed delete operation after local mergeTable operation', function () {

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 7] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 17]);
            expect(localActions[0].operations[0].start).toEqual([0]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 12 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10]);
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(11); // the length is modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 12 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10]);
            expect(oneOperation.end).toEqual([1, 14]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(12);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the length is modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]); // the position is not modified
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 3], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]); // the position is not modified
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 3]); // the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 2, 18]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 6], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4]); // the position is not modified
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 5]); // the locally saved operation is modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 8]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8, 0, 0, 3] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 8, 0, 0, 3]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 12], end: [4, 6] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 22]); // the position is modified
            expect(oneOperation.end).toEqual([3, 6]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(10); // the length is not modified

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 4], end: [2, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 14]); // the position is modified
            expect(oneOperation.end).toEqual([1, 15]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(10);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 8] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 12 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]); // the position is not modified
            expect(oneOperation.end).toEqual([1, 8]); // the position is not modified
            expect(localActions[0].operations[0].start).toEqual([1]); // the locally saved start position of the operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(7);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 4], end: [2, 8] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]); // the position is modified
            expect(oneOperation.end).toEqual([1, 8]); // the position is modified
            expect(localActions[0].operations[0].start).toEqual([0]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].rowcount).toBe(10);

            // the table is merged directly behind the last deleted position
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 4] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 5 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]); // no modification
            expect(oneOperation.end).toEqual([1, 4]); // no modification
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].rowcount).toBe(1);

            oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);
        });

        // delete and local mergeParagraph (handleParaMergeDelete)
        it('should calculate valid transformed delete operation of table cell after local mergeParagraph operation inside cell', function () {
            oneOperation = { name: 'delete', start: [2, 1, 0], opl: 1, osn: 1 }; // deleting table cell
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 1, 0, 4], paralength: 3, opl: 1, osn: 1 }] }]; // merging paragraph inside table cell
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [2, 1, 0], opl: 1, osn: 1 }], transformedOps);
        });

        // delete and external mergeParagraph (handleParaMergeDelete)
        it('should calculate valid transformed delete operation of table cell after external mergeParagraph operation inside cell', function () {
            oneOperation = { name: 'mergeParagraph', start: [2, 1, 0, 4], paralength: 3, opl: 1, osn: 1 }; // deleting table cell
            localActions = [{ operations: [{ name: 'delete', start: [2, 1, 0], opl: 1, osn: 1 }] }]; // merging paragraph inside table cell
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 1, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });
    });

});
