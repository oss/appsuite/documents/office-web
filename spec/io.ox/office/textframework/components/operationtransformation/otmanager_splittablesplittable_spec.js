/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        // splitTable and local splitTable (handleSplitTableSplitTable)
        it('should calculate valid transformed splitTable operation after splitTable operation', function () {

            oneOperation = { name: 'splitTable', start: [1, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 10] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 8]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]); // modified

            oneOperation = { name: 'splitTable', start: [1, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified

            oneOperation = { name: 'splitTable', start: [1, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }] }]; // same position
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6]); // external operation is not modified
            expect(localActions[0].operations[0].start).toEqual([2, 0]); // local operation is modified

            oneOperation = { name: 'splitTable', start: [0, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 10] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 8]);
            expect(localActions[0].operations[0].start).toEqual([2, 10]); // modified

            oneOperation = { name: 'splitTable', start: [0, 2, 2, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 10] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2, 2, 4, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]); // not modified

            oneOperation = { name: 'splitTable', start: [0, 2, 2, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [0, 2, 2, 4, 1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2, 2, 5, 1]);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2, 4, 1]); // not modified

            oneOperation = { name: 'splitTable', start: [0, 2, 1, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [0, 2, 2, 3, 1] }] }]; // neighbour cell
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2, 1, 4, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2, 3, 1]); // not modified

            oneOperation = { name: 'splitTable', start: [1, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [0, 10] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8]);
            expect(localActions[0].operations[0].start).toEqual([0, 10]);

            oneOperation = { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 2, 2]);

            oneOperation = { name: 'splitTable', start: [1, 6, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 2, 2]);

            oneOperation = { name: 'splitTable', start: [0, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 2, 2]);

            oneOperation = { name: 'splitTable', start: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 2, 2]);
        });

    });

});
