/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null,
        // shortcuts for the utils functions
        expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // delete and local delete in separated delete ranges (handleDeleteDelete)
        it('should calculate valid transformed delete operation after local delete operation in separated delete ranges', function () {

            // the external delete is in front of the local delete in the same paragraph (no ranges)
            oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [3, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]); // modified
            expect(localActions[0].operations[0].end).toEqual([3, 6]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 4]); // not modified
            expect(oneOperation.end).toEqual([3, 4]); // not modified

            // the external delete is in front of the local delete in the same paragraph (with ranges)
            oneOperation = { name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [3, 9], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // modified
            expect(localActions[0].operations[0].end).toEqual([3, 6]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 2]); // not modified
            expect(oneOperation.end).toEqual([3, 4]); // not modified

            // the external delete is behind the local delete in the same paragraph (no ranges)
            oneOperation = { name: 'delete', start: [3, 6], end: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 2]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 5]); // modified
            expect(oneOperation.end).toEqual([3, 5]); // modified

            // the external delete is behind the local delete in the same paragraph (with ranges)
            oneOperation = { name: 'delete', start: [3, 6], end: [3, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 3]); // modified
            expect(oneOperation.end).toEqual([3, 5]); // modified

            // the external delete is in a paragraph before the local delete
            oneOperation = { name: 'delete', start: [2, 6], end: [2, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 6]); // not modified
            expect(oneOperation.end).toEqual([2, 8]); // not modified

            // the external delete is in a paragraph behind the local delete
            oneOperation = { name: 'delete', start: [5, 6], end: [5, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([5, 6]); // not modified
            expect(oneOperation.end).toEqual([5, 8]); // not modified

            // the external delete is in a paragraph behind the local delete over several paragraphs (without following merge)
            oneOperation = { name: 'delete', start: [5, 6], end: [5, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 6]); // modified
            expect(oneOperation.end).toEqual([4, 8]); // modified

            // the external delete is in a paragraph behind the local delete over several paragraphs (with following merge)
            oneOperation = { name: 'delete', start: [5, 6], end: [5, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(2); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 6]); // modified
            expect(oneOperation.end).toEqual([3, 8]); // modified

            // the external delete is in a paragraph behind the local delete over several complete paragraphs (without following merge)
            oneOperation = { name: 'delete', start: [5, 6], end: [5, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 6]); // not modified
            expect(oneOperation.end).toEqual([2, 8]); // not modified

            // the external delete is in a paragraph behind the local delete over several complete paragraphs (with following merge)
            oneOperation = { name: 'delete', start: [5, 6], end: [5, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 3], end: [3], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // not modified
            expect(localActions[0].operations[1].start).toEqual([1]); // not modified
            expect(localActions[0].operations[1].paralength).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(2); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 6]); // not modified
            expect(oneOperation.end).toEqual([2, 8]); // not modified

            // the external delete is inside the last paragraph behind the local delete over several paragraphs (without following merge)
            oneOperation = { name: 'delete', start: [3, 6], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1]); // modified (5 characters inside the paragraph deleted)
            expect(oneOperation.end).toEqual([4]); // modified

            // the external delete is in a paragraph behind the local delete over several paragraphs (with following merge)
            oneOperation = { name: 'delete', start: [3, 6], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations[1].start).toEqual([1]); // not modified
            expect(localActions[0].operations[1].paralength).toBe(6); // not modified
            expect(localActions[0].operations).toHaveLength(2); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 7]); // modified (deleting the 8th character after merge)
            expect(oneOperation.end).toEqual([3]); // modified

            // the external delete ends in the first paragraph of the local delete (without following merge)
            oneOperation = { name: 'delete', start: [2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4]); // modified
            expect(localActions[0].operations[0].end).toEqual([5]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3, 2]); // not modified

            // the external delete removes several paragraphs before the local delete
            oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7], end: [6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 7]); // modified
            expect(localActions[0].operations[0].end).toEqual([4]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified

            // the external delete removes several top level paragraphs before the local delete inside a table
            oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 7, 2, 2, 1]); // modified
            expect(localActions[0].operations[0].end).toEqual([2, 7, 2, 2, 6]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified

            // the external delete removes several top level paragraphs behind the local delete inside a table
            oneOperation = { name: 'delete', start: [5], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 2, 2, 6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([5]); // not modified
            expect(oneOperation.end).toEqual([6]); // not modified

            // the external delete removes several paragraphs inside a table in front of the local deleted top level paragraphs
            oneOperation = { name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [5], end: [6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]); // not modified
            expect(localActions[0].operations[0].end).toEqual([6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 2]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 2, 6]); // not modified

            // the external delete removes several paragraphs inside a table in front of the local deleted paragraphs in the same table cell
            oneOperation = { name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 2, 2, 2]); // modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 2, 2, 8]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 2]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 2, 3]); // not modified

            // the external delete removes several paragraphs inside a table in front of the local deleted paragraphs in another cell
            oneOperation = { name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 3, 4, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 3, 4, 8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 2]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 2, 3]); // not modified

            oneOperation = { name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [4, 2], end: [4, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 2]);
            expect(localActions[0].operations[0].end).toEqual([4, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 5, 2, 2]);
            expect(oneOperation.end).toEqual([4, 5, 2, 3]);

            oneOperation = { name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [4, 2], end: [4, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 2]);
            expect(localActions[0].operations[0].end).toEqual([4, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 1, 2, 2]);
            expect(oneOperation.end).toEqual([4, 1, 2, 3]);

            oneOperation = { name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [4, 1], end: [4, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([4, 1]);
            expect(localActions[0].operations[0].end).toEqual([4, 3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 2]);
            expect(oneOperation.end).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [3, 1], end: [3, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 1, 2, 2]);
            expect(oneOperation.end).toEqual([4, 1, 2, 3]);
        });

        // delete and local delete with identical delete ranges (handleDeleteDelete)
        it('should calculate valid transformed delete operation after local delete operation in identical delete ranges', function () {

            oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0); // there is no additional local operation generated

            oneOperation = { name: 'delete', start: [4], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0); // there is no additional local operation generated

            oneOperation = { name: 'delete', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 3, 2, 2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0); // there is no additional local operation generated

            oneOperation = { name: 'delete', start: [4, 3, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 3, 2, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0); // there is no additional local operation generated
        });

        // delete and local delete in surrounding delete ranges (handleDeleteDelete)
        it('should calculate valid transformed delete operation after local delete operation in surrounding delete ranges', function () {

            // the external delete is surrounded by the local delete in the same paragraph
            oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated

            // the external delete surrounds the local delete in the same paragraph
            oneOperation = { name: 'delete', start: [3, 5], end: [3, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 5], end: [3, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 5]); // modified
            expect(oneOperation.end).toEqual([3, 5]); // modified

            // the external delete surrounds the local delete because it is an ancestor node
            oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3]); // not modified
            expect(oneOperation.end).toBeUndefined(); // not modified

            // the external delete surrounds the local delete because it is an ancestor node
            oneOperation = { name: 'delete', start: [3], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified

            // the external delete surrounds the local delete because its final paragraph is an ancestor node
            oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified

            // the external delete surrounds the local delete because its final table is an ancestor node
            oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified

            // the external delete is surrounded by the local delete because several paragraphs are removed
            oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated

            // the external delete of paragraphs is surrounded by the local delete because several paragraphs are removed
            oneOperation = { name: 'delete', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated

            // the external delete surrounds the local delete because that is an ancestor table
            oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 5, 2, 1], end: [3, 5, 2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3]); // not modified
            expect(oneOperation.end).toBeUndefined(); // not modified
        });

        // delete and local delete in overlapping delete ranges (handleDeleteDelete)
        it('should calculate valid transformed delete operation after local delete operation in overlapping delete ranges', function () {

            // the two delete ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'delete', start: [3, 4], end: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 3]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 3]); // modified, only one character must be removed by the external operation
            expect(oneOperation.end).toEqual([3, 3]); // modified

            // the two delete ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'delete', start: [3, 4], end: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1], end: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 3]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 1]); // modified, only one character must be removed by the external operation
            expect(oneOperation.end).toEqual([3, 1]); // modified

            // the two delete ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([0, 2]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([0, 1]); // modified, only three characters must be removed by the external operation
            expect(oneOperation.end).toEqual([0, 3]); // modified

            // the two delete ranges overlap on paragraph level (local operation before external operation)
            oneOperation = { name: 'delete', start: [4], end: [9], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // modified, 5 instead of 6 paragraphs must be removed by the external operation
            expect(oneOperation.end).toEqual([5]); // modified

            // the two delete ranges overlap each other inside the same paragraph (local operation after external operation)
            oneOperation = { name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 2]); // modified, only one character must be removed by the external operation
            expect(oneOperation.end).toEqual([3, 2]); // modified

            // the two delete ranges overlap each other inside the same paragraph (local operation after external operation)
            oneOperation = { name: 'delete', start: [3, 1], end: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 10], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 5]);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 1]); // modified, only three characters must be removed by the external operation
            expect(oneOperation.end).toEqual([3, 3]); // modified

            // the two delete ranges overlap on paragraph level (local operation after external operation)
            oneOperation = { name: 'delete', start: [1], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3], end: [8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // modified, 2 instead of 4 paragraphs must be removed by the external operation
            expect(oneOperation.end).toEqual([2]); // modified

            // the two delete ranges overlap each other inside the same paragraph (local operation after external operation)
            oneOperation = { name: 'delete', start: [0, 2], end: [0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]);
            expect(localActions[0].operations[0].end).toEqual([0, 4]);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([0, 2]); // modified, only two characters must be removed by the external operation
            expect(oneOperation.end).toEqual([0, 3]); // modified

            // the two delete ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'delete', start: [3, 3], end: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 3]); // modified, only one character must be removed by the external operation
            expect(oneOperation.end).toEqual([3, 3]); // modified
        });

        // OX Presentation: User A deletes a tables while external user B deleted a row in this table (DOCS-3113)
        it('should calculate valid transformed delete operation for rows after local delete operation of table', function () {

            oneOperation = { name: 'delete', start: [0, 2, 0], opl: 1, osn: 1 }; // deleting a row in OX Presentation
            localActions = [{ operations: [{ name: 'delete', start: [0, 2], opl: 1, osn: 1 }] }]; // deleting a table in OX Presentation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'delete', start: [0, 2], opl: 1, osn: 1 }; // deleting a table in OX Presentation
            localActions = [{ operations: [{ name: 'delete', start: [0, 2, 0], opl: 1, osn: 1 }] }]; // deleting a table row in OX Presentation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [0, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0, 3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0, 2, 0], opl: 1, osn: 1 }], transformedOps);
        });

    });

});
