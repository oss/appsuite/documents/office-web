/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';
import { BaseOTManager } from '@/io.ox/office/editframework/model/operation/otmanager';

// class OTManager ========================================================

describe('TextFramework class OTManager', function () {

    // existence check ----------------------------------------------------

    it('should subclass BaseOTManager', function () {
        expect(OTManager).toBeSubClassOf(BaseOTManager);
    });

    // private helpers ----------------------------------------------------

    var otManager = new OTManager();
    var localActions = null;
    var pos = null;
    var oneOperation = null;
    var transformedOps = null;

    // the test runner
    var TestRunner = Utils.TestRunner;
    var testRunner = new TestRunner(otManager);

    // shortcuts for the utils functions
    var expectActions = Utils.expectActions;
    var expectAction = Utils.expectAction;
    var expectOp = Utils.expectOp;

    // public methods -----------------------------------------------------

    describe('method hasAliasName', function () {
        it('should contain operations for alias "insertChar"', function () {
            expect(otManager.hasAliasName({ name: 'insertText' }, 'insertChar')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'insertTab' }, 'insertChar')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'insertHardBreak' }, 'insertChar')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'insertField' }, 'insertChar')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'insertRange' }, 'insertChar')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'insertBookmark' }, 'insertChar')).toBeTrue();
        });
        it('should contain operations for alias "insertComp"', function () {
            expect(otManager.hasAliasName({ name: 'insertParagraph' }, 'insertComp')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'insertTable' }, 'insertComp')).toBeTrue();
        });
        it('should contain operations for alias "mergeComp"', function () {
            expect(otManager.hasAliasName({ name: 'mergeParagraph' }, 'mergeComp')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'mergeTable' }, 'mergeComp')).toBeTrue();
        });
        it('should contain operations for alias "splitComp"', function () {
            expect(otManager.hasAliasName({ name: 'splitParagraph' }, 'splitComp')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'splitTable' }, 'splitComp')).toBeTrue();
        });
    });

    describe('method transformPosition', function () {

        it('should not change the position, if there are no local operations without acknowledgement', function () {
            pos = [1, 1];
            localActions = [];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1]);
        });

        it('should calculate valid transformed position after insertText operation', function () {
            pos = [1, 9];
            localActions = [{ operations: [{ name: 'insertText', start: [1, 8], text: 'ooo', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 12]);
        });

        it('should transform position when position of insertText operation is the same as the specified position', function () {
            pos = [1, 9];
            localActions = [{ operations: [{ name: 'insertText', start: [1, 9], text: 'ooo', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 12]);
        });

        it('should not transform position when position of insertText operation is behind specified position', function () {
            pos = [1, 9];
            localActions = [{ operations: [{ name: 'insertText', start: [1, 10], text: 'ooo', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 9]);
        });

        it('should not transform position when position of insertText operation is inside previous paragraph', function () {
            pos = [1, 9];
            localActions = [{ operations: [{ name: 'insertText', start: [0, 2], text: 'ooo', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 9]);
        });

        it('should transform position when it is inside a text frame in the same paragraph', function () {
            pos = [1, 6, 1, 3];
            localActions = [{ operations: [{ name: 'insertText', start: [1, 5], text: 'ooo', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 9, 1, 3]);
        });

        it('should not transform position when it is inside a text frame in the same paragraph before the inserted text', function () {
            pos = [1, 3, 1, 3];
            localActions = [{ operations: [{ name: 'insertText', start: [1, 5], text: 'ooo', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 3, 1, 3]);
        });

        it('should not transform position when it is inside a text frame  and the text is inserted in the previous paragraph', function () {
            pos = [1, 3, 1, 3];
            localActions = [{ operations: [{ name: 'insertText', start: [0, 2], text: 'ooo', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 3, 1, 3]);
        });

        it('should not transform position when it is inside a text frame  and the text is inserted in another text frame', function () {
            pos = [1, 3, 1, 3];
            localActions = [{ operations: [{ name: 'insertText', start: [1, 2, 1, 1], text: 'ooo', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 3, 1, 3]);
        });

        it('should not transform position when position of insertText operation is inside drawing', function () {
            pos = [1, 9];
            localActions = [{ operations: [{ name: 'insertText', start: [1, 8, 1, 0], text: 'ooo', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 9]);
        });

        it('should change position behind a delete range', function () {
            pos = [5, 2];
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3, 2]);
        });

        it('should not change position behind a delete range, if no paragraphs are removed', function () {
            pos = [5, 2];
            localActions = [{ operations: [{ name: 'delete', start: [1, 3], end: [2, 4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([5, 2]);
        });

        it('should change position behind a delete range, if no paragraphs are removed but a mergeParagraph operation follows', function () {
            pos = [5, 2];
            localActions = [{ operations: [{ name: 'delete', start: [1, 3], end: [2, 4], opl: 1, osn: 1 }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 3 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([4, 2]);
        });

        it('should change position behind a delete range, if no paragraphs are removed but a mergeParagraph operation follows and position is inside paragraph', function () {
            pos = [2, 6];
            localActions = [{ operations: [{ name: 'delete', start: [1, 3], end: [2, 4], opl: 1, osn: 1 }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 3 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 4]);
        });

        it('should not change position behind a delete range, if row inside table are removed', function () {
            pos = [5, 2];
            localActions = [{ operations: [{ name: 'delete', start: [1, 3], end: [1, 5], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([5, 2]);
        });

        it('should set position as undefined, if the paragraph is removed completely', function () {
            pos = [3, 2];
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined();
        });

        it('should set paragraph position as undefined, if the paragraph is removed completely', function () {
            pos = [3];
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined();
        });

        it('should set position as undefined, if the paragraph is removed with end position', function () {
            pos = [3, 2];
            localActions = [{ operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined();
        });

        it('should set position as undefined, if the paragraph range is removed and position is in first paragraph', function () {
            pos = [3, 2];
            localActions = [{ operations: [{ name: 'delete', start: [3], end: [4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined();
        });

        it('should set position as undefined, if the paragraph range is removed and position is in last paragraph', function () {
            pos = [3, 2];
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined();
        });

        it('should modify text position, if the text range is removed in front of the position', function () {
            pos = [3, 6];
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3, 3]);
        });

        it('should not modify text position, if the text range is removed in front of the position, but in previous paragraph', function () {
            pos = [4, 6];
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([4, 6]);
        });

        it('should not modify text position, if the text range is removed behind the position', function () {
            pos = [3, 1];
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3, 1]);
        });

        it('should set position as undefined, if the text range covers the position', function () {
            pos = [3, 3];
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined();
        });

        it('should not change position, if the text range covers the position at the start point', function () {
            pos = [3, 2];
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined(); // TODO: Maybe [3, 2] is more valid than undefined, but equality fails for paragraph selection
        });

        it('should set position as undefined, if the text range covers the position at the end point', function () {
            pos = [3, 4];
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined();
        });

        it('should set position as undefined, if the paragraph range is removed and position is in middle paragraph', function () {
            pos = [3, 2];
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined();
        });

        it('should change position inside a text frame behind a delete range', function () {
            pos = [4, 2, 3, 4];
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2, 3, 4]);
        });

        it('should change position inside a text frame behind a delete range in the same paragraph with selection', function () {
            pos = [4, 2, 3, 4];
            localActions = [{ operations: [{ name: 'delete', start: [4, 0], end: [4, 1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([4, 0, 3, 4]);
        });

        it('should change position inside a text frame behind a delete range in the same paragraph', function () {
            pos = [4, 2, 3, 4];
            localActions = [{ operations: [{ name: 'delete', start: [4, 1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([4, 1, 3, 4]);
        });

        it('should not change position inside a text frame in front of a delete range in the same paragraph with selection', function () {
            pos = [4, 2, 3, 4];
            localActions = [{ operations: [{ name: 'delete', start: [4, 3], end: [4, 4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([4, 2, 3, 4]);
        });

        it('should not change position inside a text frame in front of a delete range in the same paragraph without selection', function () {
            pos = [4, 2, 3, 4];
            localActions = [{ operations: [{ name: 'delete', start: [4, 4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([4, 2, 3, 4]);
        });

        it('should calculate valid transformed position after splitParagraph operation', function () {
            pos = [1, 10];
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2]);
        });

        it('should calculate valid transformed position after splitParagraph operation in same paragraph', function () {
            pos = [1, 4];
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 4]);
        });

        it('should calculate valid transformed position after splitParagraph operation in paragraph and drawing in paragraph', function () {
            pos = [1, 4, 2, 2];
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2, 2, 2]);
        });

        it('should calculate valid transformed position after splitParagraph operation in paragraph directly before drawing', function () {
            pos = [1, 4, 2, 2];
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 4], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 0, 2, 2]);
        });

        it('should calculate valid transformed position after splitParagraph operation in paragraph directly behind drawing', function () {
            pos = [1, 4, 2, 2];
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 5], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 4, 2, 2]);
        });

        it('should change position behind a merged paragraph', function () {
            pos = [5, 2];
            localActions = [{ operations: [{ name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 3 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([4, 2]);
        });

        it('should change position directly behind a mergeParagraph', function () {
            pos = [2, 2];
            localActions = [{ operations: [{ name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 3 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 5]);
        });

        it('should not change position in front of a mergeParagraph', function () {
            pos = [0, 2];
            localActions = [{ operations: [{ name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 3 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 2]);
        });

        it('should change position behind a mergeParagraph inside a table', function () {
            pos = [3, 2, 2, 3, 4];
            localActions = [{ operations: [{ name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 3 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2, 2, 3, 4]);
        });

        it('should not change position behind a mergeParagraph that happens inside a table', function () {
            pos = [3, 2];
            localActions = [{ operations: [{ name: 'mergeParagraph', opl: 1, osn: 2, start: [1, 2, 3, 2], paralength: 3 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3, 2]);
        });

        it('should not change position behind a mergeParagraph that happens inside a drawing', function () {
            pos = [3, 2];
            localActions = [{ operations: [{ name: 'mergeParagraph', opl: 1, osn: 2, start: [1, 2, 2], paralength: 3 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3, 2]);
        });

        it('should change position behind a mergeParagraph that happens directly in front of a drawing containing the selection', function () {
            pos = [3, 2, 4, 3];
            localActions = [{ operations: [{ name: 'mergeParagraph', opl: 1, osn: 2, start: [2], paralength: 3 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 5, 4, 3]);
        });

        it('should change position behind a mergeParagraph that happens in front of a drawing containing the selection', function () {
            pos = [3, 2, 4, 3];
            localActions = [{ operations: [{ name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 3 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2, 4, 3]);
        });

        it('should calculate valid transformed position after insertParagraph operation', function () {
            pos = [1, 10];
            localActions = [{ operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 10]);
        });

        it('should not modify position after insertParagraph operation', function () {
            pos = [1, 10];
            localActions = [{ operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 10]);
        });

        it('should modify position inside table after insertParagraph operation before table', function () {
            pos = [2, 2, 1, 2, 6];
            localActions = [{ operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3, 2, 1, 2, 6]);
        });

        it('should modify position inside textframe after insertParagraph operation before text frame', function () {
            pos = [1, 2, 1, 2];
            localActions = [{ operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2, 1, 2]);
        });

        it('should modify position inside textframe after insertParagraph operation before the selection in the text frame', function () {
            pos = [1, 2, 1, 2];
            localActions = [{ operations: [{ name: 'insertParagraph', start: [1, 2, 0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2, 2, 2]);
        });

        it('should not modify position inside textframe after insertParagraph operation behind the selection in the text frame', function () {
            pos = [1, 2, 1, 2];
            localActions = [{ operations: [{ name: 'insertParagraph', start: [1, 2, 2], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2, 1, 2]);
        });

        it('should calculate valid transformed position after insertRows operation', function () {
            pos = [1, 2, 3, 4, 5];
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 2], count: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 3, 3, 4, 5]);
        });

        it('should calculate valid transformed position after insertRows operation with more than one row', function () {
            pos = [1, 2, 3, 4, 5];
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 2], count: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 5, 3, 4, 5]);
        });

        it('should not change position for insertRows operation behind current position', function () {
            pos = [1, 2, 3, 4, 5];
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 3], count: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2, 3, 4, 5]);
        });

        it('should not change position for insertRows operation, if position is behind the table', function () {
            pos = [2, 2];
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 3], count: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2]);
        });

        it('should not change position after insertColumn operation, if the column is inserted behind the current column', function () {
            pos = [1, 1, 1, 4, 5];
            localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 1, 4, 5]);
        });

        it('should change position after insertColumn operation, if the column is inserted before the current column', function () {
            pos = [1, 1, 1, 4, 5];
            localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'before', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 2, 4, 5]);
        });

        it('should change position after insertColumn operation, if the gridposition is smaller than the current column position', function () {
            pos = [1, 1, 1, 4, 5];
            localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 2, 4, 5]);
        });

        it('should not change position after insertColumn operation, if the gridposition is larger than the current column position', function () {
            pos = [1, 1, 1, 4, 5];
            localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 1, 4, 5]);
        });

        it('should not change position for insertColumn operation, if position is behind the table', function () {
            pos = [2, 2];
            localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2]);
        });

        it('should not change position for insertColumn operation, if position is in front of the table', function () {
            pos = [0, 2];
            localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 2]);
        });

        it('should not change position for insertColumn operation, if the table itself is selected', function () {
            pos = [1];
            localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1]);
        });

        it('should not change position for insertColumn operation, if a row in the table is selected', function () {
            pos = [1, 2];
            localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2]);
        });

        it('should calculate valid transformed position after insertCells operation', function () {
            pos = [1, 2, 3, 4, 5];
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 2], count: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2, 4, 4, 5]);
        });

        it('should calculate valid transformed position after insertCells operation at same cell position', function () {
            pos = [1, 2, 3, 4, 5];
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 3], count: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2, 4, 4, 5]);
        });

        it('should not change position after insertCells operation at following cell position', function () {
            pos = [1, 2, 3, 4, 5];
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 4], count: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2, 3, 4, 5]);
        });

        it('should calculate valid transformed position after insertCells operation with more than one cell', function () {
            pos = [1, 2, 3, 4, 5];
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 2], count: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2, 6, 4, 5]);
        });

        it('should not change position for insertCells operation in a different row', function () {
            pos = [1, 2, 3, 4, 5];
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 1, 1], count: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2, 3, 4, 5]);
        });

        it('should not change position for insertCells operation, if position is behind the table', function () {
            pos = [2, 2];
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 3], count: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2]);
        });

        it('should not change position for insertCells operation, if position is in front of the table', function () {
            pos = [0, 2];
            localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 3], count: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 2]);
        });

        it('should not change position for setAttributes operation, if attributes are set before the position', function () {
            pos = [1, 8];
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2], end: [1, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 8]);
        });

        it('should not change position for setAttributes operation, if attributes are set behind the position', function () {
            pos = [1, 3];
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 5], end: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 3]);
        });

        it('should not change position for setAttributes operation, if attribute range includes the position', function () {
            pos = [1, 5];
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 3], end: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 5]);
        });

        it('should change position behind table after splitTable operation', function () {
            pos = [3];
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }, { name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([5]);
        });

        it('should not change position in front of table after splitTable operation', function () {
            pos = [1];
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }, { name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1]);
        });

        it('should not change position behind table after splitTable operation in inner table', function () {
            pos = [3];
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1, 1, 2, 3], opl: 1, osn: 1 }, { name: 'insertParagraph', start: [2, 1, 1, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3]);
        });

        it('should change position inside table behind the split after splitTable operation', function () {
            pos = [2, 2, 3, 4, 2];
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }, { name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([4, 1, 3, 4, 2]);
        });

        it('should change position inside table directly behind the split after splitTable operation', function () {
            pos = [2, 1, 3, 4, 2];
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }, { name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([4, 0, 3, 4, 2]);
        });

        it('should not change position inside table directly in front of the split after splitTable operation', function () {
            pos = [2, 0, 3, 4, 2];
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }, { name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 0, 3, 4, 2]);
        });

        it('should change position far behind table after mergeTable operation', function () {
            pos = [5, 2];
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }, { name: 'mergeTable', start: [2], rowcount: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3, 2]);
        });

        it('should not find valid position after mergeTable operation for position inside deleted paragraph', function () {
            pos = [3, 0];
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }, { name: 'mergeTable', start: [2], rowcount: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined();
        });

        it('should not change position in front of table after mergeTable operation', function () {
            pos = [1];
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }, { name: 'mergeTable', start: [2], rowcount: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1]);
        });

        it('should not change position behind table after mergeTable operation in inner table', function () {
            pos = [3];
            localActions = [{ operations: [{ name: 'delete', start: [2, 3, 3, 3], opl: 1, osn: 1 }, { name: 'mergeTable', start: [2, 3, 3, 2], rowcount: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3]);
        });

        it('should change position inside table directly behind the merge after mergeTable operation', function () {
            pos = [4, 0, 3, 4, 2];
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }, { name: 'mergeTable', start: [2], rowcount: 2, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2, 3, 4, 2]);
        });

        it('should not change position inside table directly in front of the merge after mergeTable operation', function () {
            pos = [2, 0, 3, 4, 2];
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }, { name: 'mergeTable', start: [2], rowcount: 2, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 0, 3, 4, 2]);
        });

        it('should set position as undefined, if the column containing the selection is removed', function () {
            pos = [1, 1, 1, 4, 5];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined();
        });

        it('should change position after deleteColumns operation, if the column behind the column with the selection is removed', function () {
            pos = [1, 1, 0, 4, 5];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 0, 4, 5]);
        });

        it('should change position after deleteColumns operation, if the column before the column with the selection is removed', function () {
            pos = [1, 1, 2, 4, 5];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 1, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 1, 4, 5]);
        });

        it('should set position as undefined, if some columns including that one containing the selection is removed', function () {
            pos = [1, 1, 2, 4, 5];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toBeUndefined();
        });

        it('should not change position after deleteColumns operation, if the columns are removed behind the current column in same table', function () {
            pos = [1, 1, 0, 4, 5];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 0, 4, 5]);
        });

        it('should change position after deleteColumns operation, if the columns are removed in front of the current column in same table', function () {
            pos = [1, 1, 3, 4, 5];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 1, 4, 5]);
        });

        it('should change position after deleteColumns operation, if the startGrid is smaller than the current column position', function () {
            pos = [1, 1, 3, 4, 5];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 2, 4, 5]);
        });

        it('should change position of table in table after deleteColumns operation, if the startGrid is smaller than the current column position', function () {
            pos = [1, 1, 3, 1, 1, 1, 4, 5];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 2, 1, 1, 1, 4, 5]);
        });

        it('should change position of table in table after deleteColumns operation for inner table, if the startGrid is smaller than the current column position', function () {
            pos = [1, 1, 3, 1, 1, 4, 4, 5];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 1, 3, 1], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 3, 1, 1, 3, 4, 5]);
        });

        it('should not change position of table in table after deleteColumns operation for different inner table, if the startGrid is smaller than the current column position', function () {
            pos = [1, 1, 3, 1, 1, 4, 4, 5];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 1, 3, 0], startGrid: 2, endGrid: 2, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 3, 1, 1, 4, 4, 5]);
        });

        it('should not change position after deleteColumns operation, if the startGrid is larger than the current column position', function () {
            pos = [1, 1, 1, 4, 5];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 1, 4, 5]);
        });

        it('should not change position for deleteColumns operation, if position is behind the table', function () {
            pos = [2, 2];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2]);
        });

        it('should not change position for deleteColumns operation, if position is behind the table inside a following table', function () {
            pos = [3, 2, 2, 2, 2];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3, 2, 2, 2, 2]);
        });

        it('should not change position for deleteColumns operation, if position is in front of the table', function () {
            pos = [0, 2];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 2]);
        });

        it('should not change position for deleteColumns operation, if position is in front of the table in another table', function () {
            pos = [0, 2, 3, 2, 1];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 2, 3, 2, 1]);
        });

        it('should not change position for deleteColumns operation, if the table itself is selected', function () {
            pos = [1];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1]);
        });

        it('should not change position for deleteColumns operation, if a row in the table is selected', function () {
            pos = [1, 2];
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2]);
        });
    });

    // checking that no basic handler is missing
    describe('method transformOperation() handles all operations', function () {

        it('should transform the specified external action with the specified local actions', function () {

            var externalActions = [
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [1, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [1, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [1], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [1, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitTable', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [2], end: [2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeTable', start: [1], rowcount: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }
            ];
            localActions = [
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [3, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [3, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [3], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [3, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitTable', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeTable', start: [3], rowcount: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [3, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [3, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [3], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [3, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitTable', start: [3, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeTable', start: [3], rowcount: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] }
            ], localActions);
            expectActions([
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [1, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [1, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [1], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [1, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitTable', start: [1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [2], end: [2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeTable', start: [1], rowcount: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }
            ], transformedActions);
        });

    });

    describe('method transformOperation', function () {

        it('should not use external removed operations for further OT of local operations', function () {

            // an insertText operation that follows a delete operation -> the delete operation includes the remotely inserted text and therefore marks the remote operation as removed.
            oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 5], text: 'remote' };
            localActions = [{ operations: [
                { name: 'delete', start: [0, 4], end: [0, 6], opl: 1, osn: 1 },
                { name: 'insertText', opl: 1, osn: 1, start: [0, 7], text: 'local' }
            ] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [
                { name: 'delete', start: [0, 4], end: [0, 12], opl: 1, osn: 1 },
                { name: 'insertText', opl: 1, osn: 1, start: [0, 7], text: 'local' }
            ] }], localActions);
            expectOp([], transformedOps);

            // a mergeParagraph operation that follows a delete operation -> the delete operation includes the remotely splitted paragraph and therefore marks the remote operation as removed.
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [
                { name: 'delete', start: [1, 2], end: [2, 8], opl: 1, osn: 1 },
                { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 7 }
            ] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [
                { name: 'delete', start: [1, 2], end: [3, 8], opl: 1, osn: 1 },
                { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 7 }
            ] }], localActions);
            expectOp([], transformedOps);
        });

        // unit tests, that check that new generated external operations are transformed by all following local operations (handleSetAttrsParaSplit)
        it('should update all new generated external operations with following local operations', function () {

            // external change inside one paragraph and local splitParagraph in this paragraph
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } };
            localActions = [{ operations: [
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] }
            ] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] }
            ] }], localActions);
            expectOp([
                { name: 'setAttributes', opl: 1, osn: 1, start: [2, 4], end: [2, 5], attrs: { character: { italic: true } } },
                { name: 'setAttributes', opl: 1, osn: 1, start: [3, 0], end: [3, 2], attrs: { character: { italic: true } } }
            ], transformedOps);

            // external change inside one paragraph and local splitParagraph in this paragraph
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } };
            localActions = [{ operations: [
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] },
                { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }
            ] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] },
                { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }
            ] }], localActions);
            expectOp([
                { name: 'setAttributes', opl: 1, osn: 1, start: [2, 4], end: [2, 5], attrs: { character: { italic: true } } },
                { name: 'setAttributes', opl: 1, osn: 1, start: [3, 0], end: [3, 5], attrs: { character: { italic: true } } }
            ], transformedOps);

            // external change inside one paragraph and local splitParagraph in this paragraph
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } };
            localActions = [{ operations: [
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 23] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] },
                { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }
            ] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 23] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] },
                { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }
            ] }], localActions);
            expectOp([
                { name: 'setAttributes', opl: 1, osn: 1, start: [2, 4], end: [2, 5], attrs: { character: { italic: true } } },
                { name: 'setAttributes', opl: 1, osn: 1, start: [3, 0], end: [3, 5], attrs: { character: { italic: true } } }
            ], transformedOps);

            // external change inside one paragraph and local splitParagraph in this paragraph
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } };
            localActions = [{ operations: [
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 23] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 20] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 14] },
                { name: 'insertText', opl: 1, osn: 1, start: [4, 1], text: 'bbb' }
            ] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [
                { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 23] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 20] },
                { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 14] },
                { name: 'insertText', opl: 1, osn: 1, start: [4, 1], text: 'bbb' }
            ] }], localActions);
            expectOp([
                { name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 5], attrs: { character: { italic: true } } },
                { name: 'setAttributes', opl: 1, osn: 1, start: [4, 0], end: [4, 5], attrs: { character: { italic: true } } }
            ], transformedOps);
        });

        // multi action test
        it('should transform the specified external action with the specified local actions', function () {

            var externalActions = [
                { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }
            ];
            localActions = [
                { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [4, 1], count: 3, opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                    { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                ] },
                { operations: [
                    { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                    { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                ] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [
                    { name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                    { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [4, 1], count: 3, opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                    { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                ] },
                { operations: [
                    { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                    { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                ] }
            ], localActions);
            expectActions([
                { operations: [{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }
            ], transformedActions);
        });

        it('should transform all specified external actions with the specified local actions', function () {

            var externalActions = [
                { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertBookmark', start: [5, 1, 4, 0, 0], opl: 1, osn: 1 },
                    { name: 'insertTab', start: [5, 1, 4, 0, 1], opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 0] }] }
            ];
            localActions = [
                { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [4, 1], count: 3, opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                    { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                ] },
                { operations: [
                    { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                    { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                ] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [
                    { name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                    { name: 'mergeTable', start: [6], rowcount: 6, opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'insertText', start: [6, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [6, 1], count: 3, opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                    { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                ] },
                { operations: [
                    { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                    { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                ] }
            ], localActions);
            expectActions([
                { operations: [{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [7, 10, 5, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                { operations: [
                    { name: 'insertBookmark', start: [7, 10, 5, 0, 0], opl: 1, osn: 1 },
                    { name: 'insertTab', start: [7, 10, 5, 0, 1], opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [4, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [5, 0] }] }
            ], transformedActions);
        });
    });
});
