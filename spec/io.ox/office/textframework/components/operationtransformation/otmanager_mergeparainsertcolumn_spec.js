/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // the test runner
    var TestRunner = Utils.TestRunner;
    var testRunner = new TestRunner(otManager);

    // shortcuts for the utils functions
    var expectActions = Utils.expectActions,
        expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertColumn and local mergeParagraph operation (handleMergeParaInsertColumn)
        it('should calculate valid transformed insertColumn operation after local mergeParagraph operation', function () {

            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [5], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 3, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 4, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 3, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 4, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 2]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 9, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 4, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 3]);
        });

        // insertColumn and local mergeTable operation without requirement of new insertColumn operation (handleMergeParaInsertColumn)
        it('should calculate valid transformed insertColumn operation after local mergeTable operation without generating new insertColumn operation', function () {

            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [5], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 3, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 4, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 3, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 4, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 2]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 9, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2, 3]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 4, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 2]);

            oneOperation = { name: 'insertColumn', start: [1, 3, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 3, 3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3, 3]);
        });

        // insertColumn and local mergeTable operation with requirement of new insertColumn operation (handleMergeParaInsertColumn)
        it('should calculate valid transformed insertColumn operation after local mergeTable operation and generate required insertColumn operation', function () {

            // the local merge is in the same table: local mergeTable [4] and external insertColumn [4]
            // -> an additional internal operation: 'insertColumn [5]' before 'mergeTable [4]' is required
            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [
                { name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }
            ] }], localActions);
            expectOp([{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }], transformedOps);

            // the local merge is in the same table: local mergeTable [4] and external insertColumn [4]
            // -> an additional internal operation: 'insertColumn [5]' before 'mergeTable [4]' is required
            // -> also the insertText position must be adapted, even though a new operation is inserted before
            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [
                { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 },
                { name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }
            ] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [
                { name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 },
                { name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }
            ] }], localActions);
            expectOp([{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }], transformedOps);

            // the local merge is in the same table: local mergeTable [4] and external insertColumn [4]
            // -> an additional internal operation: 'insertColumn [5]' before 'mergeTable [4]' is required
            // -> the insertText position must not be adapted
            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [
                { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 },
                { name: 'insertText', start: [4, 4, 1, 0, 0], text: 'ooo', opl: 1, osn: 1 }
            ] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [
                { name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 },
                { name: 'insertText', start: [4, 4, 1, 0, 0], text: 'ooo', opl: 1, osn: 1 }
            ] }], localActions);
            expectOp([{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }], transformedOps);

            // the local merge is in the same table: local mergeTable [4] and external insertColumn [4]
            // -> an additional internal operation: 'insertColumn [5]' before 'mergeTable [4]' is required
            // -> also the insertText position in a following action must be increased
            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [
                { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] }
            ];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectActions([
                { operations: [
                    { name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                    { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] }
            ], localActions);
            expectOp([{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }], transformedOps);

            // the local merge is in the same table: local mergeTable [4] and external insertColumn [4]
            // -> an additional internal operation: 'insertColumn [5]' before 'mergeTable [4]' is required
            // -> also the insertText position in a following action must be adapted
            var externalActions = [
                { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp', opl: 1, osn: 1 }] }
            ];
            localActions = [
                { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [
                    { name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                    { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }
                ] },
                { operations: [{ name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] }
            ], localActions);
            expectActions([
                { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [4, 7, 5, 0, 0], text: 'ppp', opl: 1, osn: 1 }] }
            ], transformedActions);

            // the local merge is in the same table: local mergeTable [1, 2, 2, 4] and external insertColumn [1, 2, 2, 4]
            // -> an additional internal operation: 'insertColumn [1, 2, 2, 5]' before 'mergeTable [1, 2, 2, 4]' is required
            oneOperation = { name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("insertColumn");
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 5]);
            expect(localActions[0].operations[1].name).toBe("mergeTable");
            expect(localActions[0].operations[1].start).toEqual([1, 2, 2, 4]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0]).toEqual(oneOperation);

            // the local merge is in the previous table: local mergeTable [3] and external insertColumn [4]
            // 1. the external insertColumn operation must be modified, because of the locally already executed mergeTable [3] operation
            // 2. an additional internal operation: 'insertColumn [3]' before 'mergeTable [3]' is required
            oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 6, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("insertColumn");
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[1].name).toBe("mergeTable");
            expect(localActions[0].operations[1].start).toEqual([3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            // the local merge is in the previous table: local mergeTable [1, 2, 2, 3] and external insertColumn [1, 2, 2, 4]
            // 1. the external insertColumn operation must be modified, because of the locally already executed mergeTable [1, 2, 2, 3] operation
            // 2. an additional internal operation: 'insertColumn [1, 2, 2, 3]' before 'mergeTable [1, 2, 2, 3]' is required
            oneOperation = { name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(2);
            expect(localActions[0].operations[0].name).toBe("insertColumn");
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3]);
            expect(localActions[0].operations[1].name).toBe("mergeTable");
            expect(localActions[0].operations[1].start).toEqual([1, 2, 2, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1, 2, 2, 3]);
        });

        // mergeTable and local insertColumn operation with requirement of new insertColumn operation (handleMergeParaInsertColumn)
        it('should calculate valid transformed mergeTable operation after local insertColumn operation and generate required insertColumn operation', function () {

            // the merge is in the same table: local insertColumn [3] and external mergeTable [3]
            // -> an additional external operation: 'insertColumn [4]' before 'mergeTable [3]' is required
            oneOperation = { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].name).toBe("insertColumn");
            expect(transformedOps[0].start).toEqual([5]);
            expect(transformedOps[1].name).toBe("mergeTable");
            expect(transformedOps[1].start).toEqual([4]);

            // the merge is in the same table: local insertColumn [1, 2, 2, 3] and external mergeTable [1, 2, 2, 3]
            // -> an additional external operation: 'insertColumn [1, 2, 2, 4]' before 'mergeTable [1, 2, 2, 3]' is required
            oneOperation = { name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].name).toBe("insertColumn");
            expect(transformedOps[0].start).toEqual([1, 2, 2, 5]);
            expect(transformedOps[1].name).toBe("mergeTable");
            expect(transformedOps[1].start).toEqual([1, 2, 2, 4]);

            // the external merge is in the previous table: local insertColumn [4] and external mergeTable [3]
            // 1. the internal insertColumn operation must be modified, because of the external executed mergeTable [3] operation
            // 2. an additional external operation: 'insertColumn [3]' before 'mergeTable [3]' is required
            oneOperation = { name: 'mergeTable', start: [3], rowcount: 6, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].name).toBe("insertColumn");
            expect(transformedOps[0].start).toEqual([3]);
            expect(transformedOps[1].name).toBe("mergeTable");
            expect(transformedOps[1].start).toEqual([3]);

            // the external merge is in the previous table: local insertColumn [1, 2, 2, 4] and external mergeTable [1, 2, 2, 3]
            // 1. the internal insertColumn operation must be modified, because of the external executed mergeTable [1, 2, 2, 3] operation
            // 2. an additional external operation: 'insertColumn [1, 2, 2, 3]' before 'mergeTable [1, 2, 2, 3]' is required
            oneOperation = { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].name).toBe("insertColumn");
            expect(transformedOps[0].start).toEqual([1, 2, 2, 3]);
            expect(transformedOps[1].name).toBe("mergeTable");
            expect(transformedOps[1].start).toEqual([1, 2, 2, 3]);
        });
    });

});
