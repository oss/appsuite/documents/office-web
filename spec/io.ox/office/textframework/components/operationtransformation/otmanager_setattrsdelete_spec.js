/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // setAttributes and local delete in separated ranges (handleSetAttrsDelete)
        it('should calculate valid transformed setAttributes operation after local delete operation in separated ranges', function () {

            // the external setAttributes is in front of the local delete in the same paragraph (no ranges)
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [3, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 7]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 7]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 4]); // not modified
            expect(oneOperation.end).toEqual([3, 4]); // not modified

            // the external setAttributes is in front of the local delete in the same paragraph (with ranges)
            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [3, 9], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 7]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 9]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 2]); // not modified
            expect(oneOperation.end).toEqual([3, 4]); // not modified

            // the external setAttributes is behind the local delete in the same paragraph (no ranges)
            oneOperation = { name: 'setAttributes', start: [3, 6], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 2]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 5]); // modified
            expect(oneOperation.end).toEqual([3, 5]); // modified

            // the external setAttributes is behind the local delete in the same paragraph (with ranges)
            oneOperation = { name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 3]); // modified
            expect(oneOperation.end).toEqual([3, 5]); // modified

            // the external setAttributes is in a paragraph before the local delete
            oneOperation = { name: 'setAttributes', start: [2, 6], end: [2, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 6]); // not modified
            expect(oneOperation.end).toEqual([2, 8]); // not modified

            // the external setAttributes is in a paragraph behind the local delete
            oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([5, 6]); // not modified
            expect(oneOperation.end).toEqual([5, 8]); // not modified

            // the external setAttributes is in a paragraph behind the local delete over several paragraphs (without following merge)
            oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 6]); // modified
            expect(oneOperation.end).toEqual([4, 8]); // modified

            // the external setAttributes is in a paragraph behind the local delete over several paragraphs (with following merge)
            oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(2); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 6]); // modified
            expect(oneOperation.end).toEqual([3, 8]); // modified

            // the external setAttributes is in a paragraph behind the local delete over several complete paragraphs (without following merge)
            oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 6]); // not modified
            expect(oneOperation.end).toEqual([2, 8]); // not modified

            // the external setAttributes is in a paragraph behind the local delete over several complete paragraphs (with following merge)
            oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 3], end: [3], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // not modified
            expect(localActions[0].operations[1].start).toEqual([1]); // not modified
            expect(localActions[0].operations[1].paralength).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(2); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 6]); // not modified
            expect(oneOperation.end).toEqual([2, 8]); // not modified

            // the external setAttributes is inside the last paragraph behind the local delete over several paragraphs (without following merge)
            oneOperation = { name: 'setAttributes', start: [3, 6], end: [5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1]); // modified (5 characters inside the paragraph deleted)
            expect(oneOperation.end).toEqual([4]); // modified

            // the external setAttributes is in a paragraph behind the local delete over several paragraphs (with following merge)
            oneOperation = { name: 'setAttributes', start: [3, 6], end: [5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations[1].start).toEqual([1]); // not modified
            expect(localActions[0].operations[1].paralength).toBe(6); // not modified
            expect(localActions[0].operations).toHaveLength(2); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 7]); // modified (deleting the 8th character after merge)
            expect(oneOperation.end).toEqual([3]); // modified

            // the external setAttributes ends in the first paragraph of the local delete (without following merge)
            oneOperation = { name: 'setAttributes', start: [2], end: [3, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 7]); // not modified
            expect(localActions[0].operations[0].end).toEqual([6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3, 2]); // not modified

            // the external setAttributes changes several paragraphs before the local delete
            oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7], end: [6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7]); // not modified
            expect(localActions[0].operations[0].end).toEqual([6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified

            // the external setAttributes changes several paragraphs behind the local delete
            oneOperation = { name: 'setAttributes', start: [8], end: [9], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7], end: [6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7]); // not modified
            expect(localActions[0].operations[0].end).toEqual([6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([6]); // modified
            expect(oneOperation.end).toEqual([7]); // modified

            // the external setAttributes changes several top level paragraphs before the local delete inside a table
            oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 2, 2, 6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified

            // the external setAttributes changes content inside a table behind the local removal of paragraphs before the table
            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 7, 2, 2, 1]); // modified
            expect(oneOperation.end).toEqual([2, 7, 2, 2, 6]); // modified

            // the external setAttributes changes several top level paragraphs behind the local delete inside a table
            oneOperation = { name: 'setAttributes', start: [5], end: [6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 2, 2, 6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([5]); // not modified
            expect(oneOperation.end).toEqual([6]); // not modified

            // the external setAttributes changes several paragraphs inside a table in front of the local deleted top level paragraphs
            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [5], end: [6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5]); // not modified
            expect(localActions[0].operations[0].end).toEqual([6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 2]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 2, 6]); // not modified

            // the external setAttributes changes several paragraphs inside a table in front of the local deleted paragraphs in the same table cell
            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 2, 4, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 2, 4, 8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 2]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 2, 3]); // not modified

            // the external setAttributes changes several paragraphs inside a table in front of the local deleted paragraphs in another cell
            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 3, 4, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 3, 4, 8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 2]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 2, 3]); // not modified

            // the external setAttributes changes several paragraphs inside a table behind the local deleted paragraphs in the same table cell
            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 2, 4, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 2, 4, 8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 6, 4]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 2, 6, 8]); // not modified

            // the external setAttributes changes several paragraphs inside a table behind the local deleted paragraphs in another cell
            oneOperation = { name: 'setAttributes', start: [4, 7, 4, 1, 2], end: [4, 7, 4, 1, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 3, 4, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 3, 4, 8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 4, 1, 2]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 4, 1, 5]); // not modified

            // the external setAttributes changes several paragraphs inside a table behind the local deleted paragraphs in the same table cell
            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 2, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 2, 3]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 4, 4]); // modified
            expect(oneOperation.end).toEqual([4, 7, 2, 4, 8]); // modified

            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].end).toEqual([3]);
            expect(oneOperation.start).toEqual([2, 7, 2, 2]);
            expect(oneOperation.end).toEqual([2, 7, 2, 4]);

            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [4, 2], end: [4, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 2]);
            expect(localActions[0].operations[0].end).toEqual([4, 3]);
            expect(oneOperation.start).toEqual([4, 5, 2, 2]);
            expect(oneOperation.end).toEqual([4, 5, 2, 4]);

            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [4, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 2]);
            expect(oneOperation.start).toEqual([4, 6, 2, 2]);
            expect(oneOperation.end).toEqual([4, 6, 2, 4]);

            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [4, 8], end: [4, 9], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 8]);
            expect(localActions[0].operations[0].end).toEqual([4, 9]);
            expect(oneOperation.start).toEqual([4, 7, 2, 2]);
            expect(oneOperation.end).toEqual([4, 7, 2, 4]);

            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [4, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 8]);
            expect(oneOperation.start).toEqual([4, 7, 2, 2]);
            expect(oneOperation.end).toEqual([4, 7, 2, 4]);

            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [3, 1], end: [3, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(oneOperation.start).toEqual([4, 7, 2, 2]);
            expect(oneOperation.end).toEqual([4, 7, 2, 4]);
        });

        // setAttributes and local delete with identical ranges (handleSetAttrsDelete)
        it('should calculate valid transformed setAttributes operation after local delete operation in identical ranges', function () {

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated

            oneOperation = { name: 'setAttributes', start: [4], end: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated

            oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 3, 2, 2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated

            oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 3, 2, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
        });

        // setAttributes and local delete in surrounding ranges (handleSetAttrsDelete)
        it('should calculate valid transformed setAttributes operation after local delete operation in surrounding ranges', function () {

            // the external setAttributes is surrounded by the local delete in the same paragraph
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated

            // the external setAttributes surrounds the local delete in the same paragraph
            oneOperation = { name: 'setAttributes', start: [3, 5], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 5], end: [3, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 5]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 7]); // not modified
            expect(oneOperation.start).toEqual([3, 5]); // modified
            expect(oneOperation.end).toEqual([3, 5]); // modified

            // the external setAttributes surrounds the local delete because it is an ancestor node
            oneOperation = { name: 'setAttributes', start: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(oneOperation.start).toEqual([3]); // not modified
            expect(oneOperation.end).toBeUndefined(); // not modified

            // the external setAttributes surrounds the local delete because it is an ancestor node
            oneOperation = { name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(oneOperation.start).toEqual([3]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified

            // the external setAttributes surrounds the local delete because its final paragraph is an ancestor node
            oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified

            // the external setAttributes surrounds the local delete because its final table is an ancestor node
            oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 4, 2, 2, 0]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4, 2, 2, 6]); // not modified
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified

            // the external setAttributes is surrounded by the local delete because several paragraphs are removed
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated

            // the external setAttributes inside a table is surrounded by the local delete because several paragraphs are removed
            oneOperation = { name: 'setAttributes', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // not modified

            // the external setAttributes of paragraphs is surrounded by the local delete because several paragraphs are removed
            oneOperation = { name: 'setAttributes', start: [3], end: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated

            // the external setAttributes surrounds the local delete because that is an ancestor table
            oneOperation = { name: 'setAttributes', start: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 5, 2, 1], end: [3, 5, 2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 5, 2, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5, 2, 4]); // not modified
            expect(oneOperation.start).toEqual([3]); // not modified
            expect(oneOperation.end).toBeUndefined(); // not modified
        });

        // setAttributes and local delete in overlapping ranges (handleSetAttrsDelete)
        it('should calculate valid transformed setAttributes operation after local delete operation in overlapping ranges', function () {

            // the two ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 3]); // modified, only one character must be changed by the external operation
            expect(oneOperation.end).toEqual([3, 3]); // modified

            // the two ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1], end: [3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 1]); // modified, only one character must be changed by the external operation
            expect(oneOperation.end).toEqual([3, 1]); // modified

            // the two ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([0, 5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([0, 1]); // modified, only three characters must be changed by the external operation
            expect(oneOperation.end).toEqual([0, 3]); // modified

            // the two ranges overlap on paragraph level (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [4], end: [9], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // modified, 5 instead of 6 paragraphs must be changed by the external operation
            expect(oneOperation.end).toEqual([5]); // modified

            // the two ranges overlap each other inside the same paragraph (local operation after external operation)
            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 2]); // modified, only one character must be changed by the external operation
            expect(oneOperation.end).toEqual([3, 2]); // modified

            // the two ranges overlap each other inside the same paragraph (local operation after external operation)
            oneOperation = { name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 10], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 10]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 1]); // modified, only three characters must be changed by the external operation
            expect(oneOperation.end).toEqual([3, 3]); // modified

            // the two ranges overlap on paragraph level (local operation after external operation)
            oneOperation = { name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3], end: [8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // modified, 2 instead of 4 paragraphs must be changed by the external operation
            expect(oneOperation.end).toEqual([2]); // modified

            // the two ranges overlap each other inside the same paragraph (local operation after external operation)
            oneOperation = { name: 'setAttributes', start: [0, 2], end: [0, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([0, 8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([0, 2]); // modified, only two characters must be changed by the external operation
            expect(oneOperation.end).toEqual([0, 3]); // modified
        });

        it('should calculate valid transformed setAttributes operation after local delete operation even for invalid setAttributes operations', function () {

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [2, 10], end: [9, 10] }], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [9, 10] }], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
            localActions = [{ operations: [{ name: 'delete', start: [11], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [11], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] }], transformedOps);

            // Info: New valid start position is [3] (this is fine, because the setAttributes operation is not supported)
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [9, 10] }], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
            localActions = [{ operations: [{ name: 'delete', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [8, 10] }], transformedOps);

            // Info: New valid end position is [9, 10] (this is fine, because the setAttributes operation is not supported)
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
            localActions = [{ operations: [{ name: 'delete', start: [10], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [10], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [9, 10] }], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
            localActions = [{ operations: [{ name: 'delete', start: [4], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [8, 10] }], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [9] }], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [9] }], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
            localActions = [{ operations: [{ name: 'delete', start: [4], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [8] }], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
            localActions = [{ operations: [{ name: 'delete', start: [5], end: [11], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [5], end: [11], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [4] }], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
            localActions = [{ operations: [{ name: 'delete', start: [4], end: [11], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [4], end: [11], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [3] }], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
            localActions = [{ operations: [{ name: 'delete', start: [3], end: [11], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3], end: [11], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [11], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2], end: [11], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
            localActions = [{ operations: [{ name: 'delete', start: [2], end: [6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2], end: [6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [2], end: [5] }], transformedOps);
        });

        // updateField and local delete (handleSetAttrsDelete)
        it('should calculate valid transformed updateField operation after local delete operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 6], representation: 'abc' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 5], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 1], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 6], representation: 'abc' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 4, 1, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 3, 1, 2], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [4, 4], representation: 'abc' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [4, 4], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        // updateField and external delete (handleSetAttrsDelete)
        it('should calculate valid transformed updateField operation after external delete operation', function () {

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 6], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 5], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 1], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 1], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 6], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 4, 1, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 3, 1, 2], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [4, 4], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [4, 4], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3], opl: 1, osn: 1 }], transformedOps);
        });
    });

});
