/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertTable and local delete (handleInsertParaDelete)
        it('should calculate valid transformed insertTable operation after local delete operation', function () {

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([2, 8]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([1]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3] };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([3]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([3]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before insert
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 0]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind insert
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 5]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 1, 3] };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 1, 3] };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 8] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 8]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 3]);
            expect(localActions[0].operations).toHaveLength(2);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([4, 3]);
            expect(localActions[0].operations).toHaveLength(2);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 2 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[1].start).toEqual([2]);
            expect(localActions[0].operations[1].paralength).toBe(2);
            expect(localActions[0].operations).toHaveLength(2);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 1, 4] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 1, 5] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 1, 2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 5]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 1, 2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 2, 2], end: [2, 1, 2, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 3] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 7] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([3, 7]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 4, 5] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 8]);
            expect(localActions[0].operations).toHaveLength(2);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [0] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 1, 4, 5] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [2, 18] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 14]);
            expect(localActions[0].operations[0].end).toEqual([3, 18]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 4, 0, 8]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([1, 4, 0, 8]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [0] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 4, 0, 8]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 7, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 3, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 5], end: [1, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 7]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 7, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 7, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([0, 2]);
        });

        // delete and local insertTable (handleInsertParaDelete)
        it('should calculate valid transformed delete operation after local insertTable operation', function () {

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10] };
            localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 10]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [1, 14] };
            localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 10]);
            expect(oneOperation.end).toEqual([2, 14]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3] };
            localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
            localActions = [{ operations: [{ name: 'insertTable', start: [3, 1, 2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
            localActions = [{ operations: [{ name: 'insertTable', start: [3, 1, 2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
            localActions = [{ operations: [{ name: 'insertTable', start: [3, 1, 2, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'insertTable', start: [3, 1, 2, 3], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 5, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [3, 6] };
            localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 10]);
            expect(oneOperation.end).toEqual([4, 6]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertTable', start: [5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [3] };
            localActions = [{ operations: [{ name: 'insertTable', start: [5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.end).toEqual([3]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [2, 18] };
            localActions = [{ operations: [{ name: 'insertTable', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 14]);
            expect(oneOperation.end).toEqual([3, 18]);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [3, 18] };
            localActions = [{ operations: [{ name: 'insertTable', start: [0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 14]);
            expect(oneOperation.end).toEqual([4, 18]);
            expect(localActions[0].operations[0].start).toEqual([0]);
        });

        // insertParagraph and local delete (handleInsertParaDelete)
        it('should calculate valid transformed insertParagraph operation after local delete operation', function () {

            oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }], transformedOps);

            oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [3] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [3] }], transformedOps);

            oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [3] };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [2] }], transformedOps);

            oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [2, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] }], transformedOps);

            oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [3, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] }], transformedOps);

            oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [3, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        // insertParagraph and external delete (handleInsertParaDelete)
        it('should calculate valid transformed insertParagraph operation after external delete operation', function () {

            oneOperation = { name: 'delete', start: [1, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }] }], localActions);
            expectOp([{ name: 'delete', start: [2, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [3] }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [2] }] }], localActions);
            expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [2, 2, 3, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] }] }], localActions);
            expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [3, 2, 3, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] }] }], localActions);
            expectOp([{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [3, 2, 3, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }], transformedOps);
        });
    });

});
