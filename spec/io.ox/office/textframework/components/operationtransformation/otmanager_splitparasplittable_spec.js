/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        // splitTable and local splitParagraph (handleSplitParaSplitTable)
        it('should calculate valid transformed splitTable operation after splitParagraph operation', function () {

            oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [2, 10] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 10]);

            oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [0, 6] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8]);
            expect(localActions[0].operations[0].start).toEqual([0, 6]);

            oneOperation = { name: 'splitParagraph', start: [1, 6, 0, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 0, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);

            oneOperation = { name: 'splitParagraph', start: [0, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 10] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 8]);
            expect(localActions[0].operations[0].start).toEqual([2, 10]);

            oneOperation = { name: 'splitParagraph', start: [1, 2, 0, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 0, 3, 1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 0, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 0, 4, 1]);

            oneOperation = { name: 'splitParagraph', start: [1, 2, 0, 2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 3, 0, 3, 1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2, 0, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 0, 3, 1]);

            oneOperation = { name: 'splitParagraph', start: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 3, 0, 3, 1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 0, 3, 1]);

            oneOperation = { name: 'splitParagraph', start: [0, 2, 2, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 10] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2, 2, 4, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);

            oneOperation = { name: 'splitParagraph', start: [0, 2, 2, 5, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [0, 2, 2, 4, 1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2, 2, 6, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2, 4, 1]);

            oneOperation = { name: 'splitParagraph', start: [0, 2, 1, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [0, 2, 2, 3, 1] }] }]; // neighbour cell
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2, 1, 4, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2, 3, 1]);

            oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [0, 10] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8]);
            expect(localActions[0].operations[0].start).toEqual([0, 10]);

            oneOperation = { name: 'splitParagraph', start: [1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 3, 2, 2]);

            oneOperation = { name: 'splitParagraph', start: [1, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 7]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 2, 2]);

            oneOperation = { name: 'splitParagraph', start: [1, 6, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 6, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 2, 2]);

            oneOperation = { name: 'splitParagraph', start: [0, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 7]);
            expect(localActions[0].operations[0].start).toEqual([2, 5, 2, 2]);

            oneOperation = { name: 'splitParagraph', start: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0]);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 2, 2]);
        });

    });

});
