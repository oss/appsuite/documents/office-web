/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // mergeParagraph and local mergeParagraph (handleParaMergeParaMerge)
        it('should calculate valid transformed mergeParagraph operation after local mergeParagraph operation', function () {

            // the external merge is (far) behind the local merge
            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, nextparalength: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(2); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // the position of the external operation is modified
            expect(oneOperation.paralength).toBe(3); // the paragraph length does not change
            expect(oneOperation.nextparalength).toBe(3); // the next paragraph length does not change

            // the external merge is directly behind the local merge
            oneOperation = { name: 'mergeParagraph', start: [2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, nextparalength: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(2); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // the position of the external operation is modified
            expect(oneOperation.paralength).toBe(5); // the paragraph length is the sum of the paralength values
            expect(oneOperation.nextparalength).toBe(3); // the next paragraph length does not change

            // the external merge is exactly the local merge -> simply ignoring the external merge
            oneOperation = { name: 'mergeParagraph', start: [2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(0);

            // the external merge is directly in front of the local merge
            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 3, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // modified
            expect(localActions[0].operations[0].paralength).toBe(9); // the sum of both paralength values
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // not modified
            expect(oneOperation.paralength).toBe(6); // not modified
            expect(oneOperation.nextparalength).toBe(3); // not modified

            // the external merge is far in front of the local merge
            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // not modified
            expect(oneOperation.paralength).toBe(6); // not modified
            expect(oneOperation.nextparalength).toBe(3); // not modified

            // the external merge is inside a table cell behind the top level local merge
            oneOperation = { name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 2, 2, 1]); // modified
            expect(oneOperation.paralength).toBe(6); // not modified
            expect(oneOperation.nextparalength).toBe(3); // not modified

            // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
            oneOperation = { name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 1]); // modified
            expect(oneOperation.paralength).toBe(6); // not modified
            expect(oneOperation.nextparalength).toBe(3); // not modified

            // the external merge is top level and in front of the local merge inside a table cell
            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 3, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 1]); // modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // not modified
            expect(oneOperation.paralength).toBe(6); // not modified
            expect(oneOperation.nextparalength).toBe(3); // not modified

            // the external merge is top level and behind the local merge inside a table cell -> both operations do not influence each other
            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3]); // modified
            expect(oneOperation.paralength).toBe(6); // not modified
            expect(oneOperation.nextparalength).toBe(3); // not modified

            // the external merge is (far) behind the local merge inside the same table cell
            oneOperation = { name: 'mergeParagraph', start: [1, 2, 2, 3], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2, nextparalength: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(2); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 2]); // the position of the external operation is modified
            expect(oneOperation.paralength).toBe(3); // the paragraph length does not change
            expect(oneOperation.nextparalength).toBe(3); // the next paragraph length does not change

            // the external merge is directly behind the local merge inside the same table cell
            oneOperation = { name: 'mergeParagraph', start: [1, 3, 2, 2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 1], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(5); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 3, 2, 1]); // the position of the external operation is modified
            expect(oneOperation.paralength).toBe(8); // the paragraph length is the sum of the paralength values
            expect(oneOperation.nextparalength).toBe(3); // the next paragraph length does not change

            // the external merge is exactly the local merge inside the same table cell -> simply ignoring the external merge
            oneOperation = { name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(0);

            // the external and the local merge are in different table cells -> both merges do not influence each other
            oneOperation = { name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(5); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1, 2, 2]); // not modified
            expect(oneOperation.paralength).toBe(3); // the paragraph length is the sum of the paralength values
            expect(oneOperation.nextparalength).toBe(3); // the next paragraph length does not change

            // the external and the local merge are identical -> both merges need to get the remove flag
            oneOperation = { name: 'mergeParagraph', start: [2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'mergeParagraph', start: [0], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 3]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.paralength).toBe(3);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 7, 3]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.paralength).toBe(3);

            oneOperation = { name: 'mergeParagraph', start: [2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 3]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.paralength).toBe(3);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 3]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(3);

            oneOperation = { name: 'mergeParagraph', start: [2, 4, 0], paralength: 3, nextparalength: 3, opl: 1, osn: 1 }; // in text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 2]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(oneOperation.start).toEqual([2, 4, 0]);
            expect(oneOperation.paralength).toBe(3);
        });

        // mergeTable and local mergeTable (handleParaMergeParaMerge)
        it('should calculate valid transformed mergeTable operation after local mergeTable operation', function () {

            // the external merge is (far) behind the local merge
            oneOperation = { name: 'mergeTable', start: [3], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, nextrowcount: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(2); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // the position of the external operation is modified
            expect(oneOperation.rowcount).toBe(3); // the paragraph length does not change
            expect(oneOperation.nextrowcount).toBe(3); // the next paragraph length does not change

            // the external merge is directly behind the local merge
            oneOperation = { name: 'mergeTable', start: [2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, nextrowcount: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(2); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // the position of the external operation is modified
            expect(oneOperation.rowcount).toBe(5); // the paragraph length is the sum of the rowcount values
            expect(oneOperation.nextrowcount).toBe(3); // the next paragraph length does not change

            // the external merge is exactly the local merge -> simply ignoring the external merge
            oneOperation = { name: 'mergeTable', start: [2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(0);

            // the external merge is directly in front of the local merge
            oneOperation = { name: 'mergeTable', start: [1], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 3, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // modified
            expect(localActions[0].operations[0].rowcount).toBe(9); // the sum of both rowcount values
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // not modified
            expect(oneOperation.rowcount).toBe(6); // not modified
            expect(oneOperation.nextrowcount).toBe(3); // not modified

            // the external merge is far in front of the local merge
            oneOperation = { name: 'mergeTable', start: [1], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 3, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // not modified
            expect(oneOperation.rowcount).toBe(6); // not modified
            expect(oneOperation.nextrowcount).toBe(3); // not modified

            // the external merge is inside a table cell behind the top level local merge
            oneOperation = { name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 2, 2, 1]); // modified
            expect(oneOperation.rowcount).toBe(6); // not modified
            expect(oneOperation.nextrowcount).toBe(3); // not modified

            // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
            oneOperation = { name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 3, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 1]); // modified
            expect(oneOperation.rowcount).toBe(6); // not modified
            expect(oneOperation.nextrowcount).toBe(3); // not modified

            // the external merge is top level and in front of the local merge inside a table cell
            oneOperation = { name: 'mergeTable', start: [1], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 3, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 1]); // modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // not modified
            expect(oneOperation.rowcount).toBe(6); // not modified
            expect(oneOperation.nextrowcount).toBe(3); // not modified

            // the external merge is top level and behind the local merge inside a table cell -> both operations do not influence each other
            oneOperation = { name: 'mergeTable', start: [3], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3]); // modified
            expect(oneOperation.rowcount).toBe(6); // not modified
            expect(oneOperation.nextrowcount).toBe(3); // not modified

            // the external merge is (far) behind the local merge inside the same table cell
            oneOperation = { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2, nextrowcount: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(2); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 2]); // the position of the external operation is modified
            expect(oneOperation.rowcount).toBe(3); // the paragraph length does not change
            expect(oneOperation.nextrowcount).toBe(3); // the next paragraph length does not change

            // the external merge is directly behind the local merge inside the same table cell
            oneOperation = { name: 'mergeTable', start: [1, 3, 2, 2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 1], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 2, 1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(3); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 3, 2, 1]); // the position of the external operation is modified
            expect(oneOperation.rowcount).toBe(8); // the paragraph length is the sum of the rowcount values
            expect(oneOperation.nextrowcount).toBe(3); // the next paragraph length does not change

            // the external merge is exactly the local merge inside the same table cell -> simply ignoring the external merge
            oneOperation = { name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(0);

            // the external and the local merge are in different table cells -> both merges do not influence each other
            oneOperation = { name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1, 2, 2]); // not modified
            expect(oneOperation.rowcount).toBe(3); // the paragraph length is the sum of the rowcount values
            expect(oneOperation.nextrowcount).toBe(3); // the next paragraph length does not change

            // the external and the local merge are identical -> both merges need to get the remove flag
            oneOperation = { name: 'mergeTable', start: [2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'mergeTable', start: [0], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.rowcount).toBe(3);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 1], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 2, 1]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.rowcount).toBe(3);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }]; // in a text frame in a table
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 1, 1, 2, 2]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.rowcount).toBe(3);

            oneOperation = { name: 'mergeTable', start: [2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }]; // in a text frame in a table
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1, 1, 2, 2]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.rowcount).toBe(3);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(3);

            oneOperation = { name: 'mergeTable', start: [2, 4, 0], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 }; // in text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 2]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([2, 4, 0]);
            expect(oneOperation.rowcount).toBe(3);
        });

        // mergeTable and local mergeParagraph (handleParaMergeParaMerge)
        it('should calculate valid transformed mergeTable operation after local mergeParagraph operation', function () {

            // the external merge is (far) behind the local merge
            oneOperation = { name: 'mergeTable', start: [3], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, nextparalength: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(2); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // the position of the external operation is modified
            expect(oneOperation.rowcount).toBe(3); // the paragraph length does not change
            expect(oneOperation.nextrowcount).toBe(3); // the next paragraph length does not change

            // the external merge is far in front of the local merge
            oneOperation = { name: 'mergeTable', start: [1], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // not modified
            expect(oneOperation.rowcount).toBe(6); // not modified
            expect(oneOperation.nextrowcount).toBe(3); // not modified

            // the external merge is inside a table cell behind the top level local merge
            oneOperation = { name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 2, 2, 1]); // modified
            expect(oneOperation.rowcount).toBe(6); // not modified
            expect(oneOperation.nextrowcount).toBe(3); // not modified

            // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
            oneOperation = { name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 1]); // modified
            expect(oneOperation.rowcount).toBe(6); // not modified
            expect(oneOperation.nextrowcount).toBe(3); // not modified

            // the external merge is top level and in front of the local merge inside a table cell
            oneOperation = { name: 'mergeTable', start: [1], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 3, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 1]); // modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // not modified
            expect(oneOperation.rowcount).toBe(6); // not modified
            expect(oneOperation.nextrowcount).toBe(3); // not modified

            // the external merge is top level and behind the local merge inside a table cell -> both operations do not influence each other
            oneOperation = { name: 'mergeTable', start: [3], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(3); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3]); // modified
            expect(oneOperation.rowcount).toBe(6); // not modified
            expect(oneOperation.nextrowcount).toBe(3); // not modified

            // the external merge is (far) behind the local merge inside the same table cell
            oneOperation = { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2, nextparalength: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(2); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 2]); // the position of the external operation is modified
            expect(oneOperation.rowcount).toBe(3); // the paragraph length does not change
            expect(oneOperation.nextrowcount).toBe(3); // the next paragraph length does not change

            // the external and the local merge are in different table cells -> both merges do not influence each other
            oneOperation = { name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5, nextparalength: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3]); // not modified
            expect(localActions[0].operations[0].paralength).toBe(5); // not modified
            expect(localActions[0].operations[0].nextparalength).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1, 2, 2]); // not modified
            expect(oneOperation.rowcount).toBe(3); // the paragraph length is the sum of the rowcount values
            expect(oneOperation.nextrowcount).toBe(3); // the next paragraph length does not change

            oneOperation = { name: 'mergeTable', start: [0], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 3]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.rowcount).toBe(3);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 2, 1], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 2, 1]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.rowcount).toBe(3);

            oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 1, 1, 2, 2], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }]; // in a text frame in a table
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 1, 1, 2, 2]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.rowcount).toBe(3);

            oneOperation = { name: 'mergeTable', start: [2], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 1, 1, 2, 2], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }]; // in a text frame in a table
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1, 1, 2, 2]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.rowcount).toBe(3);

            oneOperation = { name: 'mergeTable', start: [3], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 3]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.rowcount).toBe(3);

            oneOperation = { name: 'mergeTable', start: [2, 4, 0], rowcount: 3, nextrowcount: 3, opl: 1, osn: 1 }; // in text frame
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, nextparalength: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 2]);
            expect(localActions[0].operations[0].paralength).toBe(5);
            expect(oneOperation.start).toEqual([2, 4, 0]);
            expect(oneOperation.rowcount).toBe(3);
        });

        // mergeParagraph and local mergeTable (handleParaMergeParaMerge)
        it('should calculate valid transformed mergeParagraph operation after local mergeTable operation', function () {

            // the external merge is (far) behind the local merge
            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, nextrowcount: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(2); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // the position of the external operation is modified
            expect(oneOperation.paralength).toBe(3); // the paragraph length does not change
            expect(oneOperation.nextparalength).toBe(3); // the next paragraph length does not change

            // the external merge is far in front of the local merge
            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 3, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // not modified
            expect(oneOperation.paralength).toBe(6); // not modified
            expect(oneOperation.nextparalength).toBe(3); // not modified

            // the external merge is inside a table cell behind the top level local merge
            oneOperation = { name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 2, 2, 1]); // modified
            expect(oneOperation.paralength).toBe(6); // not modified
            expect(oneOperation.nextparalength).toBe(3); // not modified

            // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
            oneOperation = { name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 3, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 1]); // modified
            expect(oneOperation.paralength).toBe(6); // not modified
            expect(oneOperation.nextparalength).toBe(3); // not modified

            // the external merge is top level and in front of the local merge inside a table cell
            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 3, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 2, 1]); // modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1]); // not modified
            expect(oneOperation.paralength).toBe(6); // not modified
            expect(oneOperation.nextparalength).toBe(3); // not modified

            // the external merge is top level and behind the local merge inside a table cell -> both operations do not influence each other
            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(3); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3]); // modified
            expect(oneOperation.paralength).toBe(6); // not modified
            expect(oneOperation.nextparalength).toBe(3); // not modified

            // the external merge is (far) behind the local merge inside the same table cell
            oneOperation = { name: 'mergeParagraph', start: [1, 2, 2, 3], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2, nextrowcount: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(2); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(2); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([1, 2, 2, 2]); // the position of the external operation is modified
            expect(oneOperation.paralength).toBe(3); // the paragraph length does not change
            expect(oneOperation.nextparalength).toBe(3); // the next paragraph length does not change

            // the external and the local merge are in different table cells -> both merges do not influence each other
            oneOperation = { name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5, nextrowcount: 4, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3]); // not modified
            expect(localActions[0].operations[0].rowcount).toBe(5); // not modified
            expect(localActions[0].operations[0].nextrowcount).toBe(4); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1, 2, 2]); // not modified
            expect(oneOperation.paralength).toBe(3); // the paragraph length is the sum of the rowcount values
            expect(oneOperation.nextparalength).toBe(3); // the next paragraph length does not change

            oneOperation = { name: 'mergeParagraph', start: [0], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.paralength).toBe(3);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 2]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.paralength).toBe(3);

            oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }]; // in a text frame in a table
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 5, 1, 1, 2, 2]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.paralength).toBe(3);

            oneOperation = { name: 'mergeParagraph', start: [2], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }]; // in a text frame in a table
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 1, 1, 2, 2]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.paralength).toBe(3);

            oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, nextparalength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 3]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.paralength).toBe(3);

            oneOperation = { name: 'mergeParagraph', start: [2, 4, 0], paralength: 3, nextparalength: 3, opl: 1, osn: 1 }; // in text frame
            localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, nextrowcount: 3, opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 2]);
            expect(localActions[0].operations[0].rowcount).toBe(5);
            expect(oneOperation.start).toEqual([2, 4, 0]);
            expect(oneOperation.paralength).toBe(3);
        });

    });

});
