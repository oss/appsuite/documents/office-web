/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null;

    describe('method transformOperation', function () {

        // splitTable and local insertParagraph/insertTable (handleTableSplitInsertPara)
        it('should calculate valid transformed splitTable operation after insertParagraph or insertTable operation', function () {

            oneOperation = { name: 'splitTable', start: [1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(localActions[0].operations[0].start).toEqual([3]);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2, 2, 1, 0] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 0]);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2, 5, 1, 0] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 1, 0]);

            oneOperation = { name: 'splitTable', start: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2, 2, 1, 0] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 1, 0]);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1], opl: 1, osn: 1 }; // table in drawing
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 8, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 8, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'splitTable', start: [2, 8, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [3] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([3]); // -> not modified

            oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 2], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2, 0, 2, 2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 3]);

            oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 2], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2, 0, 2, 0] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 0]);

            oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2, 0, 2, 1, 4, 1, 0] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 2, 3, 1, 0]);

            oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2, 0, 2, 1, 0, 1, 0] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 1, 0, 1, 0]);

            oneOperation = { name: 'splitTable', start: [2, 0, 1, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2, 0, 2, 1, 2, 1, 0] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 1, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 1, 2, 1, 0]);

            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([5]);

            oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 1], opl: 1, osn: 1 }; // table in table
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 0, 1, 1]);
            expect(localActions[0].operations[0].start).toEqual([4]); // -> not modified
        });

    });

});
