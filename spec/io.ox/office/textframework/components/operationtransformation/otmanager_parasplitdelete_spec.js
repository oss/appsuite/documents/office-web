/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        generatedOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // splitParagraph and local delete (handleParaSplitDelete)
        it('should calculate valid transformed splitParagraph operation after local delete operation', function () {

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 10] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 9]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 22], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 20]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 12], end: [1, 16], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 25]);
            expect(localActions[0].operations[0].start).toEqual([1, 12]);
            expect(localActions[0].operations[0].end).toEqual([1, 16]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] };
            localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 30]);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 40]);
            expect(localActions[0].operations[0].start).toEqual([3]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 50] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 50]);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 0]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 5]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            // local delete inside one paragraph (without a mergeParagraph operation) and an external splitParagraph in this paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [0, 8] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([1, 3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation generated
            generatedOperation = localActions[0].operations[1];
            expect(generatedOperation.name).toBe("mergeParagraph");
            expect(generatedOperation.start).toEqual([0]);
            expect(generatedOperation.paralength).toBe(1);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 5] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 2]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation generated that will be sent to the server
            generatedOperation = localActions[0].operations[1];
            expect(generatedOperation.name).toBe("mergeParagraph");
            expect(generatedOperation.start).toEqual([1]);
            expect(generatedOperation.paralength).toBe(2);

            // local delete inside one paragraph (without a mergeParagraph operation) and an external splitParagraph in this paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1, 1, 0, 5] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 0, 1], end: [2, 1, 1, 0, 8] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 0, 1]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 1, 3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation generated
            generatedOperation = localActions[0].operations[1];
            expect(generatedOperation.name).toBe("mergeParagraph");
            expect(generatedOperation.start).toEqual([2, 1, 1, 0]);
            expect(generatedOperation.paralength).toBe(1);

            // local delete inside one paragraph (without a mergeParagraph operation) and an external splitParagraph in this paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1, 1, 3, 5] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 8] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3, 1]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4, 3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation generated
            generatedOperation = localActions[0].operations[1];
            expect(generatedOperation.name).toBe("mergeParagraph");
            expect(generatedOperation.start).toEqual([2, 1, 1, 3]);
            expect(generatedOperation.paralength).toBe(1);

            // local delete inside several paragraphs (including a mergeParagraph operation) and an external splitParagraph in the first of the removed paragraphs
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 4] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 3]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(2); // there is no additional local operation generated

            // local delete inside several paragraphs (including a mergeParagraph operation) and an external splitParagraph in the last of the removed paragraphs
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([2, 2]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(2); // there is no additional local operation generated

            // local delete inside several paragraphs (including a mergeParagraph operation) and an external splitParagraph in the middle of the removed paragraphs
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4, 3]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(2); // there is no additional local operation generated

            // local delete inside several paragraphs and of full paragraphs (without a mergeParagraph operation) and an external splitParagraph in the middle of the removed paragraphs
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated

            // local delete inside one paragraph (until its end with a mergeParagraph operation) and an external splitParagraph in the first of the removed paragraphs
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 2 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 2]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([2, 1]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(3); // there is an additional local operation generated
            generatedOperation = localActions[0].operations[2];
            expect(generatedOperation.name).toBe("mergeParagraph");
            expect(generatedOperation.start).toEqual([1]);
            expect(generatedOperation.paralength).toBe(2);

            // local delete inside one paragraph and a following paragraph completely and an external splitParagraph in the completely the removed paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([2]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            // local delete inside one paragraph and several following paragraphs completely and an external splitParagraph in the middle of the completely removed paragraphs
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            // local delete of several paragraphs completely and an external splitParagraph in the first of the completely removed paragraphs
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            // local delete of several paragraphs completely and an external splitParagraph in the last of the completely removed paragraphs
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            // local delete of several paragraphs completely and partly a final paragraph and an external splitParagraph in the completely removed paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3, 2]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            // local delete of several paragraphs completely and partly a final paragraph and an external splitParagraph in the partly removed paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 7] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            // local delete of several paragraphs (including a table and a following merge operation) and an external splitParagraph inside the table
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1, 4, 5, 3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 4]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3, 8]); // the end position of the locally saved operation is also NOT modified
            expect(localActions[0].operations).toHaveLength(2); // there is still only one local operation

            // local delete of one complete paragraph and an external splitParagraph inside this paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([2]); // the end position of the locally saved operation is generated, so that both paragraphs are deleted
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            // local delete of one complete paragraph and an external splitParagraph inside the following paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toBeUndefined(); // the end position of the locally saved operation is not generated
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1, 2]); // the start position of the external operation is modified

            // local delete of one complete paragraph and an external splitParagraph inside the previous paragraph
            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].end).toBeUndefined(); // the end position of the locally saved operation is not generated
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([0, 2]); // the start position of the external operation is not modified

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 0, 8]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 0, 0, 8]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([1, 4, 0, 8]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([1, 4, 0, 8]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 4, 0, 8]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 7, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 7, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 7, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([0, 2]);
        });

        // delete and local splitParagraph (handleParaSplitDelete)
        it('should calculate valid transformed delete operation after local splitParagraph operation', function () {

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10] };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3] };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 5, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 5, 4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 5, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [3, 6] };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(oneOperation.end).toEqual([4, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 10], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4]);
            expect(oneOperation.end).toEqual([2, 8]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 20], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 14]);
            expect(oneOperation.end).toEqual([1, 18]);
            expect(localActions[0].operations[0].start).toEqual([1, 15]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 20], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 14]);
            expect(oneOperation.end).toEqual([2, 18]);
            expect(localActions[0].operations[0].start).toEqual([0, 20]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 5] };
            localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0].name).toBe("delete");
            expect(transformedOps[0].start).toEqual([1, 2]);
            expect(transformedOps[0].end).toEqual([2, 2]);
            expect(transformedOps[1].name).toBe("mergeParagraph");
            expect(transformedOps[1].start).toEqual([1]);
            expect(transformedOps[1].paralength).toBe(2);
        });

        // splitTable and local delete (handleParaSplitDelete)
        it('should calculate valid transformed splitTable operation after local delete operation', function () {

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 10] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 9]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 22], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 20]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 30] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 12], end: [1, 16], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 25]);
            expect(localActions[0].operations[0].start).toEqual([1, 12]);
            expect(localActions[0].operations[0].end).toEqual([1, 16]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 30] };
            localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 30]);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 40] };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 40]);
            expect(localActions[0].operations[0].start).toEqual([3]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 50] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 50]);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 0]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 5]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 4]);
            expect(localActions[0].operations[0].start).toEqual([0, 4]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 0]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([2, 6]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 4, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([0, 4, 2, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 5, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 2, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 6, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 2, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 6, 2, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([2, 6, 2, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 4, 2, 2, 2], end: [0, 4, 2, 2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([0, 4, 2, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([0, 4, 2, 2, 6]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 5, 2, 2, 2], end: [0, 5, 2, 2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 0, 2, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 0, 2, 2, 6]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [0, 6, 2, 2, 2], end: [0, 6, 2, 2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 2, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 1, 2, 2, 6]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 6, 2, 2, 2], end: [1, 6, 2, 2, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([2, 6, 2, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 6, 2, 2, 6]);

            // local delete inside one table and an external splitTable in this table
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [0, 8] }] }]; // -> no such range in delete operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([1, 3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation generated
            generatedOperation = localActions[0].operations[1];
            expect(generatedOperation.name).toBe("mergeTable");
            expect(generatedOperation.start).toEqual([0]);
            expect(generatedOperation.rowcount).toBe(1);

            // local delete inside one table and an external splitTable in this table
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1, 1, 0, 5] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 0, 1], end: [2, 1, 1, 0, 8] }] }]; // -> no such range in delete operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 0, 1]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 1, 3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation generated
            generatedOperation = localActions[0].operations[1];
            expect(generatedOperation.name).toBe("mergeTable");
            expect(generatedOperation.start).toEqual([2, 1, 1, 0]);
            expect(generatedOperation.rowcount).toBe(1);

            // local delete inside one table and an external splitTable in this table
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1, 1, 3, 5] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 8] }] }]; // -> no such range in delete operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3, 1]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4, 3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation generated
            generatedOperation = localActions[0].operations[1];
            expect(generatedOperation.name).toBe("mergeTable");
            expect(generatedOperation.start).toEqual([2, 1, 1, 3]);
            expect(generatedOperation.rowcount).toBe(1);

            // local delete inside one table and an external splitTable in this table
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1, 1, 3, 5] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 3, 3], end: [2, 1, 1, 3, 8] }] }]; // -> no such range in delete operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3, 3]); // the start position of the locally saved operation is NOT modified
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4, 3]); // the end position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation generated
            generatedOperation = localActions[0].operations[1];
            expect(generatedOperation.name).toBe("mergeTable");
            expect(generatedOperation.start).toEqual([2, 1, 1, 3]);
            expect(generatedOperation.rowcount).toBe(3);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([2]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 3] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 1] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3, 2]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 7] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([2]); // the end position of the locally saved operation is generated, so that both paragraphs are deleted
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toBeUndefined(); // the end position of the locally saved operation is not generated
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1, 2]); // the start position of the external operation is modified

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 2] };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // the start position of the locally saved operation is modified
            expect(localActions[0].operations[0].end).toBeUndefined(); // the end position of the locally saved operation is not generated
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([0, 2]); // the start position of the external operation is not modified

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 0, 8]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3, 4], end: [1, 4, 0, 3, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 0, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 0, 3, 6]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 0, 0, 8]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3, 4], end: [1, 4, 0, 3, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 0, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 0, 0, 3, 8]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([1, 4, 0, 8]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3, 4], end: [1, 4, 0, 3, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 0, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([1, 4, 0, 3, 8]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([1, 4, 0, 8]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 0, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 4, 0, 8]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 7, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(oneOperation.start).toEqual([1, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(oneOperation.start).toEqual([1, 2, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 7, 2, 2] }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 7, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([0, 2]);
        });

        // delete and local splitTable (handleParaSplitDelete)
        it('should calculate valid transformed delete operation after local splitTable operation', function () {

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10] };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10, 3, 2, 1], end: [1, 10, 3, 2, 2] };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2, 3, 2, 1]);
            expect(oneOperation.end).toEqual([2, 2, 3, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3] };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 5, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 5, 4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 5, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3, 4]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [3, 6] };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 2]);
            expect(oneOperation.end).toEqual([4, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 10], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 4]);
            expect(oneOperation.end).toEqual([2, 8]);
            expect(localActions[0].operations[0].start).toEqual([1, 10]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 20], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 14]);
            expect(oneOperation.end).toEqual([1, 18]);
            expect(localActions[0].operations[0].start).toEqual([1, 15]);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
            localActions = [{ operations: [{ name: 'splitTable', start: [0, 20], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 14]);
            expect(oneOperation.end).toEqual([2, 18]);
            expect(localActions[0].operations[0].start).toEqual([0, 20]);

        });

    });

});
