/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // deleteColumns and local delete (handleDeleteColumnsDelete)
        it('should calculate valid transformed deleteColumns operation after local delete operation', function () {

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8, 2, 4], end: [1, 8, 2, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8, 3, 4], end: [1, 8, 3, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8, 4, 4], end: [1, 8, 4, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 2, 4]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 2, 5]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8, 2, 4], end: [1, 8, 2, 5], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 1, endGrid: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8, 2], end: [1, 8, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(1);
            expect(oneOperation.endGrid).toBe(1);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 3]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8, 2], end: [1, 8, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8, 0], end: [1, 8, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 0]);
            expect(localActions[0].operations[0].end).toEqual([1, 8, 1]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([1]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 3, 2, 2, 2], end: [0, 3, 2, 2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([0, 3, 2, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([0, 3, 2, 2, 4]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3, 1, 2, 3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 2, 3]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3, 1, 2, 3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before insert
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 0]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3, 1, 2, 3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind insert
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3, 1, 2, 3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3, 1, 1, 3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 4, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 3, 4, 4]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3, 1, 1, 3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 4, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3, 1, 1, 3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 4, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 1, 3, 4, 1]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3, 1, 1, 3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3, 1, 1, 3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 1, 3]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 8] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is a one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 8]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
            expect(localActions[0].operations).toHaveLength(2); // there are two local operations
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(2);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
            expect(localActions[0].operations).toHaveLength(1);
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }] }];
            expect(localActions[0].operations).toHaveLength(1);
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 4], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 5], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], end: [2, 1, 1, 4, 2] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2, 1]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 4, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 2], startGrid: 2, endGrid: 2 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 2, 3, 4], end: [2, 1, 1, 2, 2, 3, 6] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2, 2, 2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 2, 2, 2, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(2);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 2, 4] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 2, 3] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 2, 2] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations).toHaveLength(0);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 2, 1] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 2], end: [2, 1, 1, 2, 4] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 2, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 2, 5, 3, 2], end: [2, 1, 1, 2, 2, 5, 3, 8] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 2, 2, 3, 3, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 2, 2, 3, 3, 8]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 2, 2, 2, 5, 3, 2], end: [2, 1, 2, 2, 2, 5, 3, 8] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 2, 2, 5, 3, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 1, 2, 2, 2, 5, 3, 8]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 3] }] }];
            expect(localActions[0].operations).toHaveLength(1);
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [3], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
            expect(localActions[0].operations).toHaveLength(1);
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2] }] }];
            expect(localActions[0].operations).toHaveLength(1);
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
            expect(localActions[0].operations).toHaveLength(2);
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 8]);
            expect(localActions[0].operations).toHaveLength(2);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2, 1, 1, 0], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
            expect(localActions[0].operations).toHaveLength(2);
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 8]);
            expect(localActions[0].operations).toHaveLength(2, 1, 1, 0);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            expect(localActions[0].operations).toHaveLength(1);
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [4] }] }];
            expect(localActions[0].operations).toHaveLength(1);
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [2], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            expect(localActions[0].operations).toHaveLength(1);
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [0], startGrid: 2, endGrid: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            expect(localActions[0].operations).toHaveLength(1);
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1, 7, 2], startGrid: 2, endGrid: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1, 3, 2], startGrid: 2, endGrid: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 5], end: [1, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 5]);
            expect(localActions[0].operations[0].end).toEqual([1, 7]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1, 2, 2], startGrid: 2, endGrid: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1, 2, 2], startGrid: 2, endGrid: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(oneOperation.start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1, 7, 2, 2], startGrid: 2, endGrid: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 7, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([0, 2]);
            expect(oneOperation.startGrid).toBe(2);
            expect(oneOperation.endGrid).toBe(3);
        });

        // delete and local deleteColumns (handleDeleteColumnsDelete)
        it('should calculate valid transformed delete operation after local deleteColumns operation', function () {

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [1, 11] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10]);
            expect(oneOperation.end).toEqual([1, 11]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10, 5, 2], end: [1, 10, 5, 3] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10, 3, 2]);
            expect(oneOperation.end).toEqual([1, 10, 3, 3]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(oneOperation.end).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1]);
            expect(oneOperation.end).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1, 4], end: [1, 1, 4] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 2]);
            expect(oneOperation.end).toEqual([1, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1, 4, 6, 2], end: [1, 1, 4, 6, 8] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 2, 6, 2]);
            expect(oneOperation.end).toEqual([1, 1, 2, 6, 8]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1, 1, 6, 2], end: [1, 1, 1, 6, 8] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 1, 6, 2]);
            expect(oneOperation.end).toEqual([1, 1, 1, 6, 8]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1, 2, 6, 2], end: [1, 1, 2, 6, 8] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 4, 8] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3, 4, 8]);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 4, 8] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3, 2, 8]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 2, 8] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 1, 8] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3, 1, 8]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 4, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3]);
            expect(localActions[0].operations[0].start).toEqual([1, 3, 1, 1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 3] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 4, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(oneOperation.end).toEqual([1, 3]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 1, 1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 4, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(oneOperation.end).toEqual([1, 4]);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 1, 1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 1] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8, 4] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 8, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8, 3] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 2] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 1, 2] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 2]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 1] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2, 5], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 1], end: [3, 1, 2, 4] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2, 5], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 1]);
            expect(oneOperation.end).toEqual([3, 1, 2, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 4, 8]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 6] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 3]);
            expect(oneOperation.end).toEqual([1, 6]);
            expect(localActions[0].operations[0].start).toEqual([1, 2, 2]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 6] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(oneOperation.end).toEqual([1, 6]);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [5], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [5, 1, 1, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([4, 1, 1, 2]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [3] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [5], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.end).toEqual([3]);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 6] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(oneOperation.end).toEqual([1, 6]);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 2, 1, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [3, 18] };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 14]);
            expect(oneOperation.end).toEqual([3, 18]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].startGrid).toBe(2);
            expect(localActions[0].operations[0].endGrid).toBe(3);
        });

    });

});
