/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertTable and local setAttributes (handleSetAttrsInsertPara)
        it('should calculate valid transformed insertTable operation after local setAttributes operation', function () {

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([2, 8]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1, 22]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1, 12]);
            expect(localActions[0].operations[0].end).toEqual([1, 16]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([2, 12]);
            expect(localActions[0].operations[0].end).toEqual([2, 16]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([3]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([3]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 1, 2, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 2, 6]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1]);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, before split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 0]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, but behind split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 5]);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            // local change inside one paragraph and several following paragraphs completely and an external insertTable in the middle of the paragraphs
            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2]); // external operation is not modified

            // local change of several paragraphs completely and an external insertTable before the first of the paragraphs
            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1]); // external operation is not modified

            // local change of several paragraphs completely and an external insertTable in the last of the paragraphs
            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([3]); // external operation is not modified

            // local change of several paragraphs and an external insertTable inside the table
            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 4, 5] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([3, 8]); // the end position of the locally saved operation is also NOT modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2, 1, 4, 5]); // external operation is not modified

            // local change of one complete paragraph and an external insertTable before this paragraph
            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1]); // external operation is not modified
        });

        // setAttributes and local insertTable (handleSetAttrsInsertPara)
        it('should calculate valid transformed splitParagraph operation after local insertTable operation', function () {

            oneOperation = { name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 8]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 22]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 12]);
            expect(oneOperation.end).toEqual([2, 16]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 12]);
            expect(oneOperation.end).toEqual([1, 16]);
            expect(localActions[0].operations[0].start).toEqual([2]);

            oneOperation = { name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.end).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.end).toEqual([3]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1, 1, 2, 2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0]);
            expect(oneOperation.end).toEqual([2]);
            expect(localActions[0].operations[0].start).toEqual([1, 1, 2, 2]);

            oneOperation = { name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 2, 1, 2, 2]);
            expect(oneOperation.end).toEqual([3, 2, 1, 2, 6]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 3]);
            expect(oneOperation.end).toEqual([0, 8]);
            expect(localActions[0].operations[0].start).toEqual([1]);

            oneOperation = { name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] }] }]; // changing paragraph in same cell, before split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 0]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] }] }]; // changing paragraph in same cell, but behind split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 5]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);

            oneOperation = { name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 3]);

            // external change inside one paragraph and a following paragraph completely and an intenal insertTable in front of the paragraph range
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [2], end: [3], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([3]);
            expect(oneOperation.end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1); // no additional external operation

            // external change inside one paragraph and several following paragraphs completely and an internal insertTable in the middle of these paragraphs
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.end).toEqual([5]);
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1); // no additional external operation

            // external change of several paragraphs completely and an internal insertTable in the first paragraph
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([2]);
            expect(oneOperation.end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1); // no additional external operation

            // external change of several paragraphs completely and an internal insertTable in the last of the paragraphs
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [3] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([1]);
            expect(oneOperation.end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1); // no additional external operation

            // external change of several paragraphs and an internal insertTable inside the table
            oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 4, 5] }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 4, 5]); // the start position of the locally saved operation is not modified
            expect(oneOperation.start).toEqual([3, 4]);
            expect(oneOperation.end).toEqual([3, 8]);
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(transformedOps).toHaveLength(1); // no additional external operation
        });

        // updateField and local insertTable (handleSetAttrsInsertComp)
        it('should calculate valid transformed updateField operation after internal insertTable operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [2, 2], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' }], transformedOps);
        });

        // updateField and external insertTable (handleSetAttrsInsertComp)
        it('should calculate valid transformed updateField operation after external insertText operation', function () {

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [2, 2], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertTable', opl: 1, osn: 1, start: [1] }], transformedOps);

            oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertTable', opl: 1, osn: 1, start: [2] }], transformedOps);
        });

        // updateField and local insertParagraph (handleSetAttrsInsertComp)
        it('should calculate valid transformed updateField operation after internal insertParagraph operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [2, 2], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' };
            localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [2] }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' }], transformedOps);
        });

        // updateField and external insertParagraph (handleSetAttrsInsertComp)
        it('should calculate valid transformed updateField operation after external insertParagraph operation', function () {

            oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [2, 2], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }], transformedOps);

            oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [2] }], transformedOps);
        });

        // setAttributes and external insertParagraph (handleSetAttrsInsertComp)
        it('should calculate valid transformed setAttributes operation to table cell after external insertParagraph operation into cell', function () {
            oneOperation = { name: 'insertParagraph', start: [1, 2, 3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [1, 2, 3, 1], opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes and internal insertParagraph (handleSetAttrsInsertComp)
        it('should calculate valid transformed setAttributes operation to table cell after internal insertParagraph operation into cell', function () {
            oneOperation = { name: 'setAttributes', start: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [1, 2, 3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [1, 2, 3, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
        });

    });
});
