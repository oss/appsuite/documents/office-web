/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // insertText and local insertTable operation (handleInsertParaInsertChar)
        it('should calculate valid transformed insertText operation after local insertTable operation', function () {

            oneOperation = { name: 'insertText', start: [3, 4], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([4, 4]);

            // oneOperation = { name: 'insertText', start: [3, 4], text: 'o', opl: 1, osn: 1 };
            // localActions = [{ operations: [{ name: 'insertTable', start: [3], target: 'cmt0', opl: 1, osn: 1 }] }];
            // transformedOps = otManager.transformOperation(oneOperation, localActions);
            // expect(localActions[0].operations[0].start).toEqual([3]);
            // expect(localActions[0].operations).toHaveLength(1);
            // expect(transformedOps).toHaveLength(1);
            // expect(transformedOps[0].start).toEqual([3, 4]);

            // oneOperation = { name: 'insertText', start: [3, 4], text: 'o', target: 'cmt0', opl: 1, osn: 1 };
            // localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            // transformedOps = otManager.transformOperation(oneOperation, localActions);
            // expect(localActions[0].operations[0].start).toEqual([3]);
            // expect(localActions[0].operations).toHaveLength(1);
            // expect(transformedOps).toHaveLength(1);
            // expect(transformedOps[0].start).toEqual([3, 4]);

            // oneOperation = { name: 'insertText', start: [3, 4], text: 'o', target: 'cmt0', opl: 1, osn: 1 };
            // localActions = [{ operations: [{ name: 'insertTable', start: [3], target: 'cmt0', opl: 1, osn: 1 }] }];
            // transformedOps = otManager.transformOperation(oneOperation, localActions);
            // expect(localActions[0].operations[0].start).toEqual([3]);
            // expect(localActions[0].operations).toHaveLength(1);
            // expect(transformedOps).toHaveLength(1);
            // expect(transformedOps[0].start).toEqual([4, 4]);

            // oneOperation = { name: 'insertText', start: [3, 4], text: 'o', target: 'cmt0', opl: 1, osn: 1 };
            // localActions = [{ operations: [{ name: 'insertTable', start: [3], target: 'cmt1', opl: 1, osn: 1 }] }];
            // transformedOps = otManager.transformOperation(oneOperation, localActions);
            // expect(localActions[0].operations[0].start).toEqual([3]);
            // expect(localActions[0].operations).toHaveLength(1);
            // expect(transformedOps).toHaveLength(1);
            // expect(transformedOps[0].start).toEqual([3, 4]);

            oneOperation = { name: 'insertText', start: [3, 4], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4]);

            oneOperation = { name: 'insertText', start: [2, 2, 4, 6, 2], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [2, 2, 4, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 4, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 2, 4, 7, 2]);

            oneOperation = { name: 'insertText', start: [2, 2, 4, 6, 2], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [2, 2, 3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 2, 4, 6, 2]);

            oneOperation = { name: 'insertText', start: [2, 2, 4, 6, 2], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 4, 6, 2]);

            oneOperation = { name: 'insertText', start: [2, 2], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 2]);

            oneOperation = { name: 'insertText', start: [2, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [2, 5, 0], opl: 1, osn: 1 }] }]; // in a textframe
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 8, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 2]);

            oneOperation = { name: 'insertText', start: [2, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [2, 1, 0], opl: 1, osn: 1 }] }]; // in a textframe
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 2]);

            oneOperation = { name: 'insertText', start: [1, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [2, 1, 0], opl: 1, osn: 1 }] }]; // in a textframe
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1, 1]);

            oneOperation = { name: 'insertText', start: [2, 1, 3, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 3, 1]);

            oneOperation = { name: 'insertText', start: [1, 1, 3, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 1, 3, 1]);
        });
    });
});
