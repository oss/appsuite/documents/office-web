/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Color } from '@/io.ox/office/editframework/utils/color';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

import * as Utils from '~/othelper';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // setAttributes and local setAttributes in separated ranges with concurrent attributes (handleSetAttrsSetAttrs)
        it('should calculate valid transformed setAttributes operation after local delete setAttributes in separated ranges', function () {

            // the external setAttributes is in front of the local setAttributes in the same paragraph (no ranges)
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 7], end: [3, 7], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 7], end: [3, 7], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 }], transformedOps);

            // the external setAttributes is in front of the local setAttributes in the same paragraph (with ranges)
            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 7], end: [3, 9], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 7]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 9]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 2]); // not modified
            expect(oneOperation.end).toEqual([3, 4]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes is behind the local setAttributes in the same paragraph (no ranges)
            oneOperation = { name: 'setAttributes', start: [3, 6], end: [3, 6], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 2], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 2]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 6]); // not modified
            expect(oneOperation.end).toEqual([3, 6]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes is in a paragraph before the local setAttributes
            oneOperation = { name: 'setAttributes', start: [2, 6], end: [2, 8], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 6]); // not modified
            expect(oneOperation.end).toEqual([2, 8]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes is in a paragraph behind the local setAttributes over several paragraphs
            oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 6], end: [3, 4], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([5, 6]); // not modified
            expect(oneOperation.end).toEqual([5, 8]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes is in a paragraph behind the local setAttributes over several complete paragraphs
            oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([5, 6]); // not modified
            expect(oneOperation.end).toEqual([5, 8]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes is inside the last paragraph behind the local setAttributes over several paragraphs
            oneOperation = { name: 'setAttributes', start: [3, 6], end: [5], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 6], end: [3, 4], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 6]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 6]); // not modified
            expect(oneOperation.end).toEqual([5]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes changes several paragraphs behind the local setAttributes
            oneOperation = { name: 'setAttributes', start: [8], end: [9], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7], end: [6], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7]); // not modified
            expect(localActions[0].operations[0].end).toEqual([6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([8]); // not modified
            expect(oneOperation.end).toEqual([9]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes changes several top level paragraphs before the local setAttributes inside a table
            oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 2, 2, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 2, 2, 6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes changes content inside a table behind the local change of paragraphs before the table
            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 2, 1]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 2, 2, 6]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes changes several paragraphs inside a table in front of the local changed paragraphs in the same table cell
            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 2, 4, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 2, 4, 8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 2]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 2, 3]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes changes several paragraphs inside a table behind the local changed paragraphs in the same table cell
            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 2, 4, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 2, 4, 8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 6, 4]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 2, 6, 8]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes changes several paragraphs inside a table behind the local changed paragraphs in another cell
            oneOperation = { name: 'setAttributes', start: [4, 7, 4, 1, 2], end: [4, 7, 4, 1, 5], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 3, 4, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 3, 4, 8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 4, 1, 2]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 4, 1, 5]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes changes several paragraphs inside a table behind the local changed paragraphs in the same table cell
            oneOperation = { name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 7, 2, 2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4, 7, 2, 3]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4, 7, 2, 6, 4]); // not modified
            expect(oneOperation.end).toEqual([4, 7, 2, 6, 8]); // not modified
            expect(transformedOps).toHaveLength(1);
        });

        // setAttributes and local setAttributes with identical ranges (handleSetAttrsSetAttrs)
        it('should calculate valid transformed setAttributes operation after local setAttributes operation in identical ranges', function () {

            // concurrent attributes
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character:  { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(transformedOps).toHaveLength(0);

            // non concurrent attributes
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character:  { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4]); // not modified
            expect(oneOperation.start).toEqual([3, 4]); // not modified
            expect(oneOperation.end).toEqual([3, 4]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            oneOperation = { name: 'setAttributes', start: [4], end: [4], attrs: { character:  { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([4]); // not modified
            expect(transformedOps).toHaveLength(0);

            // non concurrent attributes
            oneOperation = { name: 'setAttributes', start: [4], end: [4], attrs: { character:  { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([4]); // not modified
            expect(oneOperation.start).toEqual([4]); // not modified
            expect(oneOperation.end).toEqual([4]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4], attrs: { character:  { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4, 3, 2, 2, 4], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([4, 3, 2, 2, 4]); // not modified
            expect(transformedOps).toHaveLength(0);

            // non concurrent attributes
            oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4], attrs: { character:  { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4, 3, 2, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([4, 3, 2, 2, 4]); // not modified
            expect(oneOperation.start).toEqual([4, 3, 2, 2, 4]); // not modified
            expect(oneOperation.end).toEqual([4, 3, 2, 2, 4]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character:  { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([4, 3, 2, 2]); // not modified
            expect(transformedOps).toHaveLength(0);

            // non concurrent attributes
            oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character:  { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([4, 3, 2, 2]); // not modified
            expect(oneOperation.start).toEqual([4, 3, 2, 2]); // not modified
            expect(transformedOps).toHaveLength(1);
        });

        // setAttributes and local setAttributes in surrounding ranges (handleSetAttrsSetAttrs)
        it('should calculate valid transformed setAttributes operation after local setAttributes operation in surrounding ranges', function () {

            // concurrent attributes
            // the external setAttributes is surrounded by the local setAttributes in the same paragraph
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character:  { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(transformedOps).toHaveLength(0);

            // non concurrent attributes
            // the external setAttributes is surrounded by the local setAttributes in the same paragraph
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character:  { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 4]); // not modified
            expect(oneOperation.end).toEqual([3, 4]); // not modified
            expect(transformedOps).toHaveLength(1); // no additional external operations

            // concurrent attributes
            // the external setAttributes surrounds the local setAttributes in the same paragraph at both sides -> a new external operation is required
            oneOperation = { name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 5], end: [3, 7], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 5]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 7]); // not modified
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0]).toEqual(oneOperation);
            expect(transformedOps[0].start).toEqual([3, 3]);
            expect(transformedOps[0].end).toEqual([3, 4]);
            expect(transformedOps[1].name).toBe("setAttributes");
            expect(transformedOps[1].start).toEqual([3, 8]);
            expect(transformedOps[1].end).toEqual([3, 9]);

            // non concurrent attributes
            // the external setAttributes surrounds the local setAttributes in the same paragraph
            oneOperation = { name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 5], end: [3, 7], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 5]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 7]); // not modified
            expect(oneOperation.start).toEqual([3, 3]); // not modified
            expect(oneOperation.end).toEqual([3, 9]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the external setAttributes surrounds the local setAttributes in the same paragraph only at the beginning -> no new external operation required
            oneOperation = { name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 5], end: [3, 9], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 5]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 9]); // not modified
            expect(oneOperation.start).toEqual([3, 3]); // modified
            expect(oneOperation.end).toEqual([3, 4]); // modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the external setAttributes surrounds the local setAttributes in the same paragraph only at the end -> no new external operation required
            oneOperation = { name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 7], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 7]); // not modified
            expect(oneOperation.start).toEqual([3, 8]); // modified
            expect(oneOperation.end).toEqual([3, 9]); // modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the external setAttributes surrounds the local setAttributes because it is an ancestor node
            // If a text attribute is set at the paragraph [3], the more specific setting of a concurrent attribute
            // from [3,2] to [3,6] will overwrite it -> no further operation required.
            oneOperation = { name: 'setAttributes', start: [3], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // not modified
            expect(oneOperation.start).toEqual([3]); // not modified
            expect(oneOperation.end).toBeUndefined(); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the external setAttributes surrounds the local setAttributes because it is an ancestor node
            oneOperation = { name: 'setAttributes', start: [3], end: [3], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // not modified
            expect(oneOperation.start).toEqual([3]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the external setAttributes surrounds the local setAttributes because its final paragraph is an ancestor node
            oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the external setAttributes surrounds the local setAttributes because its final table is an ancestor node
            oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 4, 2, 2, 0]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 4, 2, 2, 6]); // not modified
            expect(oneOperation.start).toEqual([2]); // not modified
            expect(oneOperation.end).toEqual([3]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the external setAttributes is surrounded by the local setAttributes because several paragraphs are changed
            // -> not removing the more specific external operation
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 4]); // not modified
            expect(oneOperation.end).toEqual([3, 5]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the external setAttributes is surrounded by the local setAttributes because several paragraphs are changed
            // -> not removing the more specific external operation
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 4]); // not modified
            expect(oneOperation.end).toEqual([3, 5]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the external setAttributes is surrounded by the local setAttributes because several paragraphs are changed
            // -> not removing the more specific external operation
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 4]); // not modified
            expect(oneOperation.end).toEqual([3, 5]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the external setAttributes inside a table is surrounded by the local setAttributes because several paragraphs are removed
            // -> not removing the more specific external operation
            oneOperation = { name: 'setAttributes', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the external setAttributes of paragraphs is surrounded by the local setAttributes because several paragraphs are removed
            oneOperation = { name: 'setAttributes', start: [3], end: [4], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(transformedOps).toHaveLength(0); // no operation returned

            // non concurrent attributes
            // the external setAttributes of paragraphs is surrounded by the local setAttributes because several paragraphs are removed
            oneOperation = { name: 'setAttributes', start: [3], end: [4], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]); // not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3]); // not modified
            expect(oneOperation.end).toEqual([4]); // not modified
            expect(transformedOps).toHaveLength(1);

            // the external setAttributes surrounds the local setAttributes because that is an ancestor table
            // -> no modification required, because local operation is more specific in position
            oneOperation = { name: 'setAttributes', start: [3], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 5, 2, 1], end: [3, 5, 2, 4], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(localActions[0].operations[0].start).toEqual([3, 5, 2, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5, 2, 4]); // not modified
            expect(oneOperation.start).toEqual([3]); // not modified
            expect(oneOperation.end).toBeUndefined(); // not modified
            expect(transformedOps).toHaveLength(1);
        });

        // setAttributes and local setAttributes in overlapping ranges (handleSetAttrsSetAttrs)
        it('should calculate valid transformed setAttributes operation after local setAttributes operation in overlapping ranges', function () {

            // concurrent attributes
            // the two ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 5], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 6]); // modified, only one character must be changed by the external operation
            expect(oneOperation.end).toEqual([3, 6]); // modified
            expect(transformedOps).toHaveLength(1);

            // non concurrent attributes
            // the two ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 4]); // not modified
            expect(oneOperation.end).toEqual([3, 6]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the two ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 6]); // modified, only one character must be changed by the external operation
            expect(oneOperation.end).toEqual([3, 6]); // modified
            expect(transformedOps).toHaveLength(1);

            // non concurrent attributes
            // the two ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 4]); // not modified
            expect(oneOperation.end).toEqual([3, 6]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the two ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1], end: [0, 5], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([0, 5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([0, 6]); // modified, only three characters must be changed by the external operation
            expect(oneOperation.end).toEqual([0, 8]); // modified
            expect(transformedOps).toHaveLength(1);

            // non concurrent attributes
            // the two ranges overlap each other inside the same paragraph (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1], end: [0, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([0, 5]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([0, 3]); // not modified
            expect(oneOperation.end).toEqual([0, 8]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the two ranges overlap on paragraph level (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [4], end: [9], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([5]); // modified, 5 instead of 6 paragraphs must be changed by the external operation
            expect(oneOperation.end).toEqual([9]); // modified
            expect(transformedOps).toHaveLength(1);

            // non concurrent attributes
            // the two ranges overlap on paragraph level (local operation before external operation)
            oneOperation = { name: 'setAttributes', start: [4], end: [9], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([4]); // not modified
            expect(oneOperation.end).toEqual([9]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the two ranges overlap each other inside the same paragraph (local operation after external operation)
            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 2]); // modified, only one character must be changed by the external operation
            expect(oneOperation.end).toEqual([3, 2]); // modified
            expect(transformedOps).toHaveLength(1);

            // non concurrent attributes
            // the two ranges overlap each other inside the same paragraph (local operation after external operation)
            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([3, 6]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([3, 2]); // not modified
            expect(oneOperation.end).toEqual([3, 5]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the two ranges overlap each other inside the same paragraph in a table cell (local operation after external operation)
            oneOperation = { name: 'setAttributes', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 5], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 1, 1, 3, 4], end: [2, 1, 1, 3, 10], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 3, 10]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1, 1, 3, 1]); // modified, only three characters must be changed by the external operation
            expect(oneOperation.end).toEqual([2, 1, 1, 3, 3]); // modified
            expect(transformedOps).toHaveLength(1);

            // non concurrent attributes
            // the two ranges overlap each other inside the same paragraph in a table cell (local operation after external operation)
            oneOperation = { name: 'setAttributes', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 5], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 1, 1, 3, 4], end: [2, 1, 1, 3, 10], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3, 4]); // not modified
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 3, 10]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1, 1, 3, 1]); // not modified
            expect(oneOperation.end).toEqual([2, 1, 1, 3, 5]); // not modified
            expect(transformedOps).toHaveLength(1);

            // concurrent attributes
            // the two ranges overlap on paragraph level in a table cell (local operation after external operation)
            oneOperation = { name: 'setAttributes', start: [2, 1, 1, 1], end: [2, 1, 1, 4], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 1, 1, 3], end: [2, 1, 1, 8], attrs: { character: { color: Color.GREEN } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1, 1, 1]); // modified, 2 instead of 4 paragraphs must be changed by the external operation
            expect(oneOperation.end).toEqual([2, 1, 1, 2]); // modified
            expect(transformedOps).toHaveLength(1);

            // non concurrent attributes
            // the two ranges overlap on paragraph level in a table cell (local operation after external operation)
            oneOperation = { name: 'setAttributes', start: [2, 1, 1, 1], end: [2, 1, 1, 4], attrs: { character: { color: Color.RED } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 1, 1, 3], end: [2, 1, 1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1, 3]); // not modified
            expect(localActions[0].operations[0].end).toEqual([2, 1, 1, 8]); // not modified
            expect(localActions[0].operations).toHaveLength(1); // there is no additional local operation generated
            expect(oneOperation.start).toEqual([2, 1, 1, 1]); // not modified
            expect(oneOperation.end).toEqual([2, 1, 1, 4]); // not modified
            expect(transformedOps).toHaveLength(1);
        });

        // setAttributes and local setAttributes with different attribute sets (handleSetAttrsSetAttrs)
        it('should not calculate valid transformed setAttributes operation for separated ranges', function () {

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [2, 2], end: [2, 5], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [2, 2], end: [2, 5], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);

            // oneOperation = { name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { italic: false } }, target: 'cmt0', opl: 1, osn: 1 };
            // localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            // transformedOps = otManager.transformOperation(oneOperation, localActions);
            // expectAction([{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            // expectOp([{ name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { italic: false } }, target: 'cmt0', opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes and local setAttributes with different attribute sets (handleSetAttrsSetAttrs)
        it('should calculate valid transformed setAttributes operation after local setAttributes operation and adapt the attribute set for identical position ranges', function () {

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true, bold: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: false }, family2: { propA: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { family2: { propA: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false, color: { propA: true } } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false, color: { propA: true } }, family2: { propA: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { family2: { propA: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false, color: { propA: true } } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: true } } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: true } } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { color: { propA: true } } }, opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes and local setAttributes with different attribute sets (handleSetAttrsSetAttrs)
        it('should calculate valid transformed setAttributes operation after local setAttributes operation and adapt the attribute set for local operations surrounding external operations', function () {

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false }, family2: { propA: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { family2: { propA: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes and local setAttributes with different attribute sets (handleSetAttrsSetAttrs)
        it('should calculate valid transformed setAttributes operation after local setAttributes operation and adapt the attribute set for external operations surrounding local operations', function () {

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 7], end: [3, 8], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 }
            ], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 7], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 }
            ], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 5], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false }, family2: { propA: 1 } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { family2: { propA: 1 } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 5], end: [3, 8], attrs: { character: { italic: false }, family2: { propA: 1 } }, opl: 1, osn: 1 }
            ], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false }, family2: { propA: 1 } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false }, family2: { propA: 1 } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { family2: { propA: 1 } }, opl: 1, osn: 1 }
            ], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3], end: [3], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes and local setAttributes with different attribute sets (handleSetAttrsSetAttrs)
        it('should calculate valid transformed setAttributes operation after local setAttributes operation and adapt the attribute set for external operations overlapping with local operations', function () {

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } }, opl: 1, osn: 1 }
            ], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 7], end: [3, 9], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 }
            ], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { bold: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { bold: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { bold: true } }, opl: 1, osn: 1 }
            ], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true, bold: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: true, bold: true } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { bold: true } }, opl: 1, osn: 1 }
            ], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { bold: true } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 }
            ], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false }, family2: { propA: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { family2: { propA: true } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { italic: false }, family2: { propA: true } }, opl: 1, osn: 1 }
            ], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 2 } } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 2 } } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { family2: { propA: true } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } }, opl: 1, osn: 1 }
            ], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1, propC: 2 } }, family2: { propA: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([
                { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { family2: { propA: true } }, opl: 1, osn: 1 },
                { name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { propA: { propB: 1, propC: 2 } }, family2: { propA: true } }, opl: 1, osn: 1 }
            ], transformedOps);
        });
    });

});
