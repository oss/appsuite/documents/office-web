/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertTable and local insertTable operation (handleInsertParaInsertPara)
        it('should calculate valid transformed insertTable operation after local insertTable operation', function () {

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2]);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4]);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3, 0, 1, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [4, 0, 1, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([5, 0, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [2, 0, 1, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3]);

            oneOperation = { name: 'insertTable', start: [3, 0, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([4, 0, 1, 1]);

            oneOperation = { name: 'insertTable', start: [4, 0, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([5, 0, 1, 1]);

            oneOperation = { name: 'insertTable', start: [2, 0, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 1, 1]);

            oneOperation = { name: 'insertTable', start: [2, 0, 1], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'insertTable', start: [1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 1]);

            oneOperation = { name: 'insertTable', start: [2, 0, 1], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'insertTable', start: [2], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([3, 0, 1]);

            oneOperation = { name: 'insertTable', start: [2, 0, 1], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'insertTable', start: [3], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 1]);

            oneOperation = { name: 'insertTable', start: [2, 0, 1], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'insertTable', start: [2, 1, 1], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 1]);

            oneOperation = { name: 'insertTable', start: [2, 0, 1], opl: 1, osn: 1 }; // in a text frame
            localActions = [{ operations: [{ name: 'insertTable', start: [2, 0, 0], count: 1, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([2, 0, 2]);
        });

        // insertTable and local insertTable operation (handleInsertParaInsertPara)
        it('should calculate valid transformed insertParagraph operation after local insertTable operation', function () {

            oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertTable', start: [3], count: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertTable', start: [4], count: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }], transformedOps);
        });

        // insertTable and external insertParagraph operation (handleInsertParaInsertPara)
        it('should calculate valid transformed insertParagraph operation after external insertTable operation', function () {

            oneOperation = { name: 'insertTable', start: [3], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertTable', start: [3], count: 1, opl: 1, osn: 1 }], transformedOps);
        });

        // insertParagraph and local insertParagraph operation (handleInsertParaInsertPara)
        it('should calculate valid transformed insertParagraph operation after external insertParagraph operation', function () {

            oneOperation = { name: 'insertParagraph', start: [3], count: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3], count: 1, opl: 1, osn: 1 }], transformedOps);
        });

    });

});
