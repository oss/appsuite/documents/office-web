/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager as OTManager } from '@/io.ox/office/textframework/model/otmanager';

// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new OTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // splitTable and local setAttributes (handleSetAttrsSplitTable)
        it('should calculate valid transformed splitTable operation after local setAttributes operation', function () {

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 10] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { row: { height: 2000 } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10]);
            expect(localActions[0].operations[0].start).toEqual([1, 8]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 10] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 2, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 10]);
            expect(localActions[0].operations[0].start).toEqual([1, 8, 2, 2, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { row: { height: 2000 } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 20]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22, 4, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 20]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 4, 2, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 20]);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 20]);
            expect(localActions[0].operations[0].start).toEqual([0, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 20]);
            expect(localActions[0].operations[0].start).toEqual([3]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 0, 2, 1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 0, 2, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 3, 0]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 0, 2, 1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 0, 2, 2, 0, 3, 2, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 2, 3, 0, 3, 2, 1]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 0, 2, 1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 1, 2, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0, 2, 1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 1, 2, 2, 0]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 30] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 30]);
            expect(localActions[0].operations[0].start).toEqual([0]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 40] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 40]);
            expect(localActions[0].operations[0].start).toEqual([3]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations[1].start).toEqual([2]);
            expect(localActions[0].operations[1].end).toBeUndefined();

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations).toHaveLength(2); // there is a new local operation
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toEqual([1]);
            expect(localActions[0].operations[1].start).toEqual([2]);
            expect(localActions[0].operations[1].end).toEqual([2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([3]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 1, 2, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 1, 2, 2]);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations[0].end).toEqual([2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1, 2, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2, 1, 2, 6]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([0, 3]);
            expect(localActions[0].operations[0].end).toEqual([0, 8]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, before split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 0]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, but behind split
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 2, 5]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].start).toEqual([4]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([5]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3], end: [1, 4, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 4]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 2, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 2, 3, 6]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 4]);
            expect(localActions[0].operations[0].start).toEqual([2, 0, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 0, 3, 6]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([1, 5]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([1, 4, 3, 6]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 0] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([2, 0]);
            expect(localActions[0].operations[0].start).toEqual([1, 4, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([1, 4, 3, 6]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 2]);
            expect(localActions[0].operations[0].start).toEqual([2, 4, 3, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4, 3, 6]);

            // -> this is not handled correctly. But there is no setAttributes operation covering several rows
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [0, 1], end: [0, 7], attrs: { row: { height: 2000 } } }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).toEqual([0, 5]);
            expect(localActions[0].operations[0].start).toEqual([0, 1]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);

            // -> this is not handled correctly. But there is no setAttributes operation covering several tables/paragraphs
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [2], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1);
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].end).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([1, 3]);

            // -> this is not handled correctly. But there is no setAttributes operation covering several tables/paragraphs
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([5]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([2, 3]); // external operation is not modified

            // -> this is not handled correctly. But there is no setAttributes operation covering several tables/paragraphs
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([1, 3]); // external operation is not modified

            // -> this is not handled correctly. But there is no setAttributes operation covering several tables/paragraphs
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1); // there is one local operation
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]); // the start position of the locally saved operation is not modified
            expect(localActions[0].operations[0].end).toEqual([4]); // the end position of the locally saved operation is modified
            expect(localActions[0].operations).toHaveLength(1); // there is still only one local operation
            expect(oneOperation.start).toEqual([3, 3]); // external operation is not modified

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1, 4, 5, 3] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 8]);
            expect(oneOperation.start).toEqual([2, 1, 4, 5, 3]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1);
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[1].start).toEqual([2]);
            expect(localActions[0].operations).toHaveLength(2);
            expect(oneOperation.start).toEqual([1, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(oneOperation.start).toEqual([2, 2]);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 2] };
            localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
            expect(localActions[0].operations).toHaveLength(1);
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(oneOperation.start).toEqual([0, 2]);

            // testing, that external operation is added correctly (making setAttributes to external operation)
            oneOperation = { name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0]).toEqual(oneOperation);
            expect(transformedOps[1].name).toBe("setAttributes");
            expect(transformedOps[1].start).toEqual([2]); // generated by the splitTable operation
            expect(transformedOps[1].end).toBeUndefined();

            // testing, that external operation is added correctly (making setAttributes to external operation)
            oneOperation = { name: 'setAttributes', start: [1], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(transformedOps).toHaveLength(2);
            expect(transformedOps[0]).toEqual(oneOperation);
            expect(transformedOps[1].name).toBe("setAttributes");
            expect(transformedOps[1].start).toEqual([2]); // generated by the splitTable operation
            expect(transformedOps[1].end).toEqual([2]);
        });

        // updateField and local splitTable (handleSetAttrsSplitTable)
        it('should calculate valid transformed updateField operation after local splitTable operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [2, 0, 0, 1, 1], representation: 'abc' }], transformedOps);
        });

        // updateField and external splitTable (handleSetAttrsSplitTable)
        it('should calculate valid transformed updateField operation after external splitParagraph operation', function () {

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }], transformedOps);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 6] };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }], transformedOps);

            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [2, 0, 0, 1, 1], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }], transformedOps);
        });

        // setAttributes and local splitTable (handleSetAttrsSplitTable)
        it('should calculate valid transformed setAttributes operation for table after local splitTable operation (split in previous table)', function () {
            oneOperation = { name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for table after local splitTable operation (split in same table)', function () {
            oneOperation = { name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }, { name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for table after local splitTable operation (split in following table)', function () {
            oneOperation = { name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for child table after local splitTable operation (split in previous table)', function () {
            oneOperation = { name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for child table after local splitTable operation (split in same table)', function () {
            oneOperation = { name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 1, 1, 2, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 1, 1, 2, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }, { name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for child table after local splitTable operation (split in following table)', function () {
            oneOperation = { name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 1, 1, 3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 1, 1, 3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for table cell after local splitTable operation (split in table inside cell)', function () {
            oneOperation = { name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for table cell after local splitTable operation (split in table inside cell in previous row)', function () {
            oneOperation = { name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 0, 1, 1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [1, 0, 1, 1, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes and external splitTable (handleSetAttrsSplitTable)
        it('should calculate valid transformed setAttributes operation for table after external splitTable operation (split in previous table)', function () {
            oneOperation = { name: 'splitTable', start: [1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [1, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for table after external splitTable operation (split in same table)', function () {
            oneOperation = { name: 'splitTable', start: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }, { name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [2, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for table after external splitTable operation (split in following table)', function () {
            oneOperation = { name: 'splitTable', start: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [3, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for child table after external splitTable operation (split in previous table)', function () {
            oneOperation = { name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for child table after external splitTable operation (split in same table)', function () {
            oneOperation = { name: 'splitTable', start: [1, 1, 1, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }, { name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [1, 1, 1, 2, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for child table after external splitTable operation (split in following table)', function () {
            oneOperation = { name: 'splitTable', start: [1, 1, 1, 3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [1, 1, 1, 3, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for table cell after external splitTable operation (split in table inside cell)', function () {
            oneOperation = { name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed setAttributes operation for table cell after external splitTable operation (split in table inside cell in previous row)', function () {
            oneOperation = { name: 'splitTable', start: [1, 0, 1, 1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [1, 0, 1, 1, 2], opl: 1, osn: 1 }], transformedOps);
        });
    });

});
