/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { createTextApp } from '~/text/apphelper';
import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import ChangeTrack from '@/io.ox/office/textframework/components/changetrack/changetrack';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Position from '@/io.ox/office/textframework/utils/position';
import { getMSFormatIsoDateString } from '@/io.ox/office/editframework/utils/dateutils';
import { resolveUserId } from '@/io.ox/office/editframework/utils/operationutils';

// class ChangeTrack ======================================================

describe('Text class ChangeTrack', function () {

    // private helpers ----------------------------------------------------

    //  the operations to be applied by the document model
    var model = null,
        pageNode = null,
        changeTrack = null,
        firstParagraph = null,
        startText = 'Hello World!',
        insertText1 = ' big',
        fullText1 = 'Hello big World!',
        ctSelection = null, // a full change track selection object
        testAuthor = 'author1',
        testUid = '1111',

        ox_userId = '1227', // in ox.js the user_id is set to 12 -> this results after transformation in '1227'
        ox_author = 'author', // returned from 'getClientOperationName()' in unit tests in OX Text

        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
            { name: 'insertParagraph', start: [0] },
            { name: 'insertText', start: [0, 0], text: startText }
        ];

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        pageNode = model.getNode();
    });

    // existence check ----------------------------------------------------

    it('should subclass ModelObject', function () {
        expect(ChangeTrack).toBeSubClassOf(ModelObject);
    });

    // public methods -----------------------------------------------------

    describe('method model.getChangeTrack', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getChangeTrack');
        });
        it('should return the one change track object used by the application', function () {
            changeTrack = model.getChangeTrack(); // this is the change track object in the model that can be used by the tests
            expect(changeTrack).toBeInstanceOf(ChangeTrack);
        });
    });

    describe('method isActiveChangeTracking', function () {
        it('should exist', function () {
            expect(ChangeTrack).toHaveMethod('isActiveChangeTracking');
        });
        it('should return that change tracking is not activated in the current context', function () {
            expect(changeTrack.isActiveChangeTracking()).toBeFalse();
        });
    });

    describe('method isActiveChangeTrackingInDocAttributes', function () {
        it('should exist', function () {
            expect(ChangeTrack).toHaveMethod('isActiveChangeTrackingInDocAttributes');
        });
        it('should return that change tracking is not activated in document attributes', function () {
            expect(changeTrack.isActiveChangeTrackingInDocAttributes()).toBeFalse();
        });
    });

    describe('method setActiveChangeTracking', function () {
        it('should exist', function () {
            expect(ChangeTrack).toHaveMethod('setActiveChangeTracking');
        });
        it('should activate change tracking in the document successfully', function () {
            // successfully applying operation
            expect(changeTrack.setActiveChangeTracking(true)).toBeTrue();
            // checking if change tracking is active in current context (for example not inside comments)
            expect(changeTrack.isActiveChangeTracking()).toBeTrue();
            // checking if change tracking is actived in the document attributes
            expect(changeTrack.isActiveChangeTrackingInDocAttributes()).toBeTrue();
        });
    });

    describe('method getNextChangeTrack', function () {
        it('should exist', function () {
            expect(ChangeTrack).toHaveMethod('getNextChangeTrack');
        });
        it('should not select any change track node in the document', function () {
            var firstPara = Position.getParagraphElement(pageNode, [0]);
            expect($(firstPara).text()).toBe(startText);
            // no change tracked nodes in the document yet
            expect(changeTrack.getNextChangeTrack()).toBeFalse();
        });
    });

    describe('method updateSideBar', function () {
        it('should exist', function () {
            expect(ChangeTrack).toHaveMethod('updateSideBar');
        });
        it('should not draw any lines in the side bar of the document', function () {
            firstParagraph = Position.getParagraphElement(pageNode, [0]);
            expect($(firstParagraph).text()).toBe(startText);
            // no change tracked nodes in the document yet
            expect(changeTrack.updateSideBar()).toBeFalse();
        });
    });

    describe('method getChangeTrackSelection', function () {
        it('should exist', function () {
            expect(ChangeTrack).toHaveMethod('getChangeTrackSelection');
        });
        it('should not find any valid change track selection in the document', function () {
            firstParagraph = Position.getParagraphElement(pageNode, [0]);
            expect($(firstParagraph).text()).toBe(startText);
            // no change tracked nodes in the document yet
            expect(changeTrack.getChangeTrackSelection()).toBeNull();
        });
    });

    describe('method resolveChangeTracking', function () {
        it('should exist', function () {
            expect(ChangeTrack).toHaveMethod('resolveChangeTracking');
        });
        it('should return a resolved promise because there are no valid change track nodes in the document', function () {
            firstParagraph = Position.getParagraphElement(pageNode, [0]);
            expect($(firstParagraph).text()).toBe(startText);
            // no change tracked nodes in the document yet
            var resolvePromise = changeTrack.resolveChangeTracking();
            expect(resolvePromise.state()).toBe("resolved");
        });
    });

    // preparations for inserting text
    describe('method getChangeTrackInfo', function () {
        it('should exist', function () {
            expect(ChangeTrack).toHaveMethod('getChangeTrackInfo');
        });
        it('should fail on direct call', function () {
            // user_id is specified in fake ox object and set to '12'
            var changeTrackInfo = changeTrack.getChangeTrackInfo();
            expect(changeTrackInfo).toContainProps({ author: ox_author, userId: ox_userId });
        });
    });

    // inserting some text, so that change track nodes exist
    describe('method getChangeTrackSelection #2', function () {
        let getCtInfoStub = null;

        beforeAll(function () {
            getCtInfoStub = jest.spyOn(changeTrack, 'getChangeTrackInfo').mockReturnValue({
                author: testAuthor,
                date: getMSFormatIsoDateString({ useSeconds: false }),
                userId: testUid
            });
        });

        afterAll(function () {
            getCtInfoStub.mockRestore();
        });

        it('should find a valid change track selection in the document', function () {
            // using defined values for:
            // insertText1 = ' big'
            // fullText1 = 'Hello big World!'

            model.insertText(insertText1, [0, 5]);

            expect(getCtInfoStub).toHaveBeenCalledOnce();

            firstParagraph = Position.getParagraphElement(pageNode, [0]);
            expect($(firstParagraph).text()).toBe(fullText1);

            // this one change track can be selected now
            expect(changeTrack.getNextChangeTrack()).toBeTrue();
        });

        it('should return a valid change track selection object', function () {

            // the change track selection was set in the function changeTrack.getNextChangeTrack()
            ctSelection = changeTrack.getChangeTrackSelection();
            expect(ctSelection).toBeObject();

            // ChangeTrack selection object consists of:
            // -> activeNode: HTMLElement, the one currently node used by the iterator
            // -> activeNodeType: String, the one currently used node type, one of 'inserted', 'removed', 'modified'
            // -> activeNodeTypesOrdered: String[], ordered list of strings, corresponding to priority of node types
            // -> selectedNodes: HTMLElement[], sorted list of HTMLElements of the group belonging to 'activeNode' and 'activeNodeType
            // -> allChangeTrackNodes: jQuery, optional, cache for performance reasons, contains all change track elements in the document

            expect(ctSelection.activeNode).toBeInstanceOf(HTMLElement);
            expect($(ctSelection.activeNode).text()).toBe(insertText1);
            expect(ctSelection.activeNodeType).toBe("inserted");
            expect(ctSelection.activeNodeTypesOrdered).toHaveLength(1);
            expect(ctSelection.activeNodeTypesOrdered[0]).toBe("inserted");
            expect(ctSelection.selectedNodes).toHaveLength(1);
            expect(ctSelection.allChangeTrackNodes).toHaveLength(1);
            expect(ctSelection.selectedNodes[0]).toBe(ctSelection.activeNode);
        });
    });

    // checking the selected changetrack node (must be a text span)
    describe('method DOM.isTextSpan', function () {
        it('should exist', function () {
            expect(DOM.isTextSpan).toBeFunction();
        });
        it('should identify the selected node as text span node', function () {
            expect(DOM.isTextSpan(ctSelection.activeNode)).toBeTrue();
        });
    });

    describe('method DOM.isChangeTrackNode', function () {
        it('should exist', function () {
            expect(DOM.isChangeTrackNode).toBeFunction();
        });
        it('should detect a selected node as change tracked node', function () {
            expect(DOM.isChangeTrackNode(ctSelection.activeNode)).toBeTrue();
            expect(DOM.isChangeTrackNode(ctSelection.activeNode.previousSibling)).toBeFalse();
            expect(DOM.isChangeTrackNode(ctSelection.activeNode.nextSibling)).toBeFalse();
        });
    });

    describe('method DOM.isChangeTrackInsertNode', function () {
        it('should exist', function () {
            expect(DOM.isChangeTrackInsertNode).toBeFunction();
        });
        it('should detect a selected node as inserted change tracked node', function () {
            expect(DOM.isChangeTrackInsertNode(ctSelection.activeNode)).toBeTrue();
            expect(DOM.isChangeTrackInsertNode(ctSelection.activeNode.previousSibling)).toBeFalse();
            expect(DOM.isChangeTrackInsertNode(ctSelection.activeNode.nextSibling)).toBeFalse();
        });
    });

    // side bar must contain an element
    describe('method updateSideBar #2', function () {
        it('should draw any lines in the side bar of the document', function () {
            // one change tracked node exists in the document
            expect(changeTrack.updateSideBar()).toBeTrue();
        });
    });

    // accepting or rejecting the change track
    describe('method resolveChangeTracking #2', function () {

        it('should accept the existing change track', async function () {
            firstParagraph = Position.getParagraphElement(pageNode, [0]);
            expect($(firstParagraph).text()).toBe(fullText1);

            await changeTrack.resolveChangeTracking();

            // the full text should still be there
            expect($(firstParagraph).text()).toBe(fullText1);
            // no change tracked nodes in the document yet
            expect(changeTrack.getNextChangeTrack()).toBeFalse();
        });

        it('should undo the acceptance of the change track', async function () {

            firstParagraph = Position.getParagraphElement(pageNode, [0]);
            expect($(firstParagraph).text()).toBe(fullText1);

            await model.getUndoManager().undo();

            // the full text should still be there
            expect($(firstParagraph).text()).toBe(fullText1);
            // one change tracked node in the document again
            expect(changeTrack.getNextChangeTrack()).toBeTrue();
            // checking the change track selection object
            ctSelection = changeTrack.getChangeTrackSelection();
            expect(ctSelection).toBeObject();
            expect(ctSelection.activeNode).toBeInstanceOf(HTMLElement);
            expect($(ctSelection.activeNode).text()).toBe(insertText1);
            expect(ctSelection.activeNodeType).toBe("inserted");
            expect(ctSelection.activeNodeTypesOrdered).toHaveLength(1);
            expect(ctSelection.activeNodeTypesOrdered[0]).toBe("inserted");
            expect(ctSelection.selectedNodes).toHaveLength(1);
            expect(ctSelection.allChangeTrackNodes).toHaveLength(1);
            expect(ctSelection.selectedNodes[0]).toBe(ctSelection.activeNode);
        });

        it('should reject the existing change track', async function () {
            firstParagraph = Position.getParagraphElement(pageNode, [0]);
            expect($(firstParagraph).text()).toBe(fullText1);

            await changeTrack.resolveChangeTracking(null, { accepted: false });

            // only the start text should still be there
            expect($(firstParagraph).text()).toBe(startText);
            // no change tracked nodes in the document yet
            expect(changeTrack.getNextChangeTrack()).toBeFalse();
        });

        it('should undo the rejecting of the change track', async function () {

            firstParagraph = Position.getParagraphElement(pageNode, [0]);
            expect($(firstParagraph).text()).toBe(startText);

            await model.getUndoManager().undo();

            // the full text should again be there
            expect($(firstParagraph).text()).toBe(fullText1);
            // one change tracked node in the document again
            expect(changeTrack.getNextChangeTrack()).toBeTrue();
            // checking the change track selection object
            ctSelection = changeTrack.getChangeTrackSelection();
            expect(ctSelection).toBeObject();
            expect(ctSelection.activeNode).toBeInstanceOf(HTMLElement);
            expect($(ctSelection.activeNode).text()).toBe(insertText1);
            expect(ctSelection.activeNodeType).toBe("inserted");
            expect(ctSelection.activeNodeTypesOrdered).toHaveLength(1);
            expect(ctSelection.activeNodeTypesOrdered[0]).toBe("inserted");
            expect(ctSelection.selectedNodes).toHaveLength(1);
            expect(ctSelection.allChangeTrackNodes).toHaveLength(1);
            expect(ctSelection.selectedNodes[0]).toBe(ctSelection.activeNode);
        });
    });

    // investigating the change tracked node
    describe('method getChangeTrackInfoFromNode', function () {
        it('should exist', function () {
            expect(ChangeTrack).toHaveMethod('getChangeTrackInfoFromNode');
        });
        it('should return the change tracking information from the change tracked node', function () {
            // parameter: node, info, type
            // allowed infos: CHANGE_TRACK_INFOS = ['author', 'date', 'userId'],
            // allowed types: CHANGE_TRACK_TYPES = ['inserted', 'removed', 'modified']
            expect(changeTrack.getChangeTrackInfoFromNode(ctSelection.activeNode, 'author', 'inserted')).toBe(testAuthor);
            expect(changeTrack.getChangeTrackInfoFromNode(ctSelection.activeNode, 'author', 'removed')).toBeNull();
            expect(changeTrack.getChangeTrackInfoFromNode(ctSelection.activeNode, 'author', 'modified')).toBeNull();
            expect(changeTrack.getChangeTrackInfoFromNode(ctSelection.activeNode, 'author', 'test')).toBeNull(); // invalid type

            expect(changeTrack.getChangeTrackInfoFromNode(ctSelection.activeNode, 'name', 'inserted')).toBeNull(); // 'name' is not valid

            // the userId is modified using function OperationUtils.resolveUserId(testUid)
            var transferUid = resolveUserId(parseInt(testUid, 10));
            expect(changeTrack.getChangeTrackInfoFromNode(ctSelection.activeNode, 'userId', 'inserted')).toBe(transferUid);
            expect(changeTrack.getChangeTrackInfoFromNode(ctSelection.activeNode, 'userId', 'removed')).toBeNull();
            expect(changeTrack.getChangeTrackInfoFromNode(ctSelection.activeNode, 'userId', 'modified')).toBeNull();
            expect(changeTrack.getChangeTrackInfoFromNode(ctSelection.activeNode, 'userId', 'test')).toBeNull(); // invalid type
        });
    });
});
