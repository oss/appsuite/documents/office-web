/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { createTextApp } from '~/text/apphelper';
import {  getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';
import { EditModel } from '@/io.ox/office/editframework/model/editmodel';
import * as Position from '@/io.ox/office/textframework/utils/position';
import { XTableOperation } from '@/io.ox/office/textframework/model/tableoperationmixin';


// class TableOperationMixin ==============================================

describe('TextFramework class TableOperationMixin', function () {

    // private helpers ----------------------------------------------------

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 } } },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', start: [0, 0], text: 'Hello World!' },
        { name: 'insertTable', start: [1], attrs: { table: { tableGrid: [1000, 1000], width: 'auto', exclude: ['lastRow', 'lastCol', 'bandsVert'] }, styleId: 'TableGrid' } },
        { name: 'insertRows', start: [1, 0], count: 1, insertDefaultCells: true },
        { name: 'insertParagraph', start: [1, 0, 0, 0] },
        { name: 'insertText', start: [1, 0, 0, 0, 0], text: 'Hello Earth!' },
        { name: 'setAttributes', start: [1, 0, 0, 0, 0], attrs: { character: { fontSize: 15, bold: true } }, end: [1, 0, 0, 0, 11] },
        { name: 'setAttributes', start: [1, 0, 1, 0], attrs: { character: { underline: true } } }
    ];

    var model;
    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // existence check ----------------------------------------------------

    describe('TextFramework class XTableOperation', function () {

        it("should exist", () => {
            expect(XTableOperation).toBeObject();
        });

        // public functions -------------------------------------------------------

        describe("function XTableOperation.mixin", () => {

            it("should exist", () => {
                expect(XTableOperation).toRespondTo("mixin");
            });

            it("should create a subclass", () => {
                expect(XTableOperation.mixin(EditModel)).toBeSubClassOf(EditModel);
            });
        });
    });

    // public methods -----------------------------------------------------

    describe('method insertRowHandler', function () {
        it('should exist', function () {
            expect(model).toRespondTo('insertRowHandler');
        });
        it('should insert a row', function () {
            expect(model.insertRowHandler({ name: 'insertRow', start: [2, 0], count: 1, insertDefaultCells: false, referenceRow: 0, attrs: {} })).toBeFalse();
            expect(model.insertRowHandler({ name: 'insertRow', start: [1, 0], count: 1, insertDefaultCells: false, referenceRow: 0, attrs: {} })).toBeTrue();
        });
    });

    describe('method insertTableHandler', function () {
        it('should exist', function () {
            expect(model).toRespondTo('insertTableHandler');
        });
        it('should insert a table', function () {
            expect(model.insertTableHandler({ name: 'insertTable', start: [3] })).toBeFalse();
            expect(model.insertTableHandler({ name: 'insertTable', start: [2] })).toBeTrue();
        });
    });

    // DOCS-797 Inherit text and paragraph attributes on insert row/column
    describe('method insertRow', function () {
        beforeAll(function () {
            model.getSelection().setTextSelection([1, 1, 0, 0, 0]);
            model.insertRow();
        });

        it('should exist', function () {
            expect(model).toRespondTo('insertRow');
        });

        it('should insert new row with inherited attributes', function () {
            var par1 = Position.getParagraphElement(model.getNode(), [1, 1, 0, 0]);
            var par1Child = par1 ? par1.firstChild : null;
            var charAttrs1 = getExplicitAttributes(par1Child, 'character', true);
            var par2 = Position.getParagraphElement(model.getNode(), [1, 2, 0, 0]);
            var par2Child = par2 ? par2.firstChild : null;
            var charAttrs2 = getExplicitAttributes(par2Child, 'character', true);

            var par3 = Position.getParagraphElement(model.getNode(), [1, 1, 1, 0]);
            var par3Child = par3 ? par3.firstChild : null;
            var charAttrs3 = getExplicitAttributes(par3Child, 'character', true);
            var par4 = Position.getParagraphElement(model.getNode(), [1, 2, 1, 0]);
            var par4Child = par4 ? par4.firstChild : null;
            var charAttrs4 = getExplicitAttributes(par4Child, 'character', true);

            expect(charAttrs1.bold).toBe(charAttrs2.bold);
            expect(charAttrs1.fontSize).toBe(charAttrs2.fontSize);
            expect(charAttrs3.underline).toBe(charAttrs4.underline);
        });
    });

    // DOCS-797 Inherit text and paragraph attributes on insert row/column
    describe('method insertColumn', function () {
        beforeAll(function () {
            model.getSelection().setTextSelection([1, 0, 0, 0, 0]);
            model.insertColumn();
        });

        it('should exist', function () {
            expect(model).toRespondTo('insertColumn');
        });

        it('should insert new column with inherited attributes', function () {
            var par1 = Position.getParagraphElement(model.getNode(), [1, 2, 0, 0]);
            var par1Child = par1 ? par1.firstChild : null;
            var charAttrs1 = getExplicitAttributes(par1Child, 'character', true);
            var par2 = Position.getParagraphElement(model.getNode(), [1, 2, 1, 0]);
            var par2Child = par2 ? par2.firstChild : null;
            var charAttrs2 = getExplicitAttributes(par2Child, 'character', true);
            expect(charAttrs1.bold).toBe(charAttrs2.bold);
            expect(charAttrs1.fontSize).toBe(charAttrs2.fontSize);
        });
    });
});
