/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { createTextApp } from '~/text/apphelper';

import { OperationError } from '@/io.ox/office/editframework/utils/operationerror';
import { OperationContext } from '@/io.ox/office/editframework/model/operation/context';
import { EditModel } from '@/io.ox/office/editframework/model/editmodel';
import { XParagraph } from '@/io.ox/office/textframework/model/paragraphmixin';

// class ParagraphMixin ===================================================

describe('TextFramework class ParagraphMixin', function () {

    // private helpers ----------------------------------------------------

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 } } },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', start: [0, 0], text: 'Hello World!' },
        { name: 'insertParagraph', start: [1] },
        { name: 'insertText', start: [1, 0], text: 'Hello Earth!' }
    ];

    var model;
    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    function op(name, properties) {
        return new OperationContext(model, { name, ...properties });
    }

    // existence check ----------------------------------------------------

    describe('TextFramework class XParagraph', function () {

        it("should exist", () => {
            expect(XParagraph).toBeObject();
        });

        // public functions -------------------------------------------------------

        describe("function XParagraph.mixin", () => {

            it("should exist", () => {
                expect(XParagraph).toRespondTo("mixin");
            });

            it("should create a subclass", () => {
                expect(XParagraph.mixin(EditModel)).toBeSubClassOf(EditModel);
            });
        });
    });

    // public methods -----------------------------------------------------

    describe('method insertParagraphHandler', function () {
        it('should exist', function () {
            expect(model).toRespondTo('insertParagraphHandler');
        });
        it('should insert a paragraph', function () {
            expect(model.insertParagraphHandler.bind(model, op('insertParagraph', { start: [3] }))).toThrow(OperationError);
            expect(model.insertParagraphHandler(op('insertParagraph', { start: [2] }))).toBeUndefined();
        });
    });

    describe('method mergeParagraphHandler', function () {
        it('should exist', function () {
            expect(model).toRespondTo('mergeParagraphHandler');
        });
        it('should merge two paragraphs', function () {
            expect(model.mergeParagraphHandler({ name: 'mergeParagraph', start: [2] })).toBeFalse();
            expect(model.mergeParagraphHandler({ name: 'mergeParagraph', start: [1] })).toBeTrue();
        });
    });

    describe('method splitParagraphHandler', function () {
        it('should exist', function () {
            expect(model).toRespondTo('splitParagraphHandler');
        });
        it('should split a paragraph', function () {
            expect(model.splitParagraphHandler({ name: 'splitParagraph', start: [2, 0] })).toBeFalse();
            expect(model.splitParagraphHandler({ name: 'splitParagraph', start: [1, 13] })).toBeFalse();
            expect(model.splitParagraphHandler({ name: 'splitParagraph', start: [1, 12] })).toBeTrue();
            expect(model.splitParagraphHandler({ name: 'splitParagraph', start: [0, 0] })).toBeTrue();
        });
    });
});
