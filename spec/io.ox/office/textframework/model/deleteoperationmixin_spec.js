/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { transformOpColor, Color } from '@/io.ox/office/editframework/utils/color';
import { EditModel } from '@/io.ox/office/editframework/model/editmodel';
import { XDeleteOperation } from '@/io.ox/office/textframework/model/deleteoperationmixin';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Operations from '@/io.ox/office/textframework/utils/operations';
import * as Position from '@/io.ox/office/textframework/utils/position';

import { createTextApp } from '~/text/apphelper';

// class DeleteOperationMixin =============================================

describe('TextFramework class DeleteOperationMixin', function () {

    var globalApp = null;
    var model = null;
    var selection = null;
    var pageNode = null;
    var pageContentNode = null;

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes',
            attrs: {
                document:  { defaultTabStop: 1270, zoom: { value: 100 } },
                page:      { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 },
                character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' },
                paragraph: { lineHeight: { type: 'percent', value: 119 }, marginBottom: 352 }
            }
        },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading1', styleName: 'heading 1',
            attrs: {
                character: { bold: true, fontName: 'Times New Roman', fontSize: 14, color: transformOpColor(Color.ACCENT1, { shade: 74902 }) },
                paragraph: { marginTop: 846, outlineLevel: 0, nextStyleId: 'Normal' }
            },
            parent: 'Normal',
            uiPriority: 9
        },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', start: [0, 0], text: 'abcde' },

        { name: 'insertParagraph', start: [1] },
        { name: 'insertText', start: [1, 0], text: 'fghij' },

        { name: 'insertParagraph', start: [2] },
        { name: 'insertText', start: [2, 0], text: 'klmno' },

        { name: 'insertParagraph', start: [3] },
        { name: 'insertText', start: [3, 0], text: 'pqrst' },

        { name: 'insertTable', start: [4], attrs: { styleId: 'TableGrid', table: { tableGrid: [8255, 8255], width: 16510, exclude: ['lastRow', 'lastCol', 'bandsVert'] } } },
        { name: 'insertRows', start: [4, 0] },
        { name: 'insertCells', start: [4, 0, 0] },
        { name: 'insertParagraph', start: [4, 0, 0, 0] },
        { name: 'insertText', start: [4, 0, 0, 0, 0], text: 'Hello' },
        { name: 'insertParagraph', start: [4, 0, 0, 1] },
        { name: 'insertText', start: [4, 0, 0, 1, 0], text: 'World' },
        { name: 'insertCells', start: [4, 0, 1] },
        { name: 'insertParagraph', start: [4, 0, 1, 0] },
        { name: 'insertText', start: [4, 0, 1, 0, 0], text: 'Hello' },
        { name: 'insertParagraph', start: [4, 0, 1, 1] },
        { name: 'insertText', start: [4, 0, 1, 1, 0], text: 'Earth' },
        { name: 'insertRows', start: [4, 1] },
        { name: 'insertCells', start: [4, 1, 0] },
        { name: 'insertParagraph', start: [4, 1, 0, 0] },
        { name: 'insertText', start: [4, 1, 0, 0, 0], text: 'Hello' },
        { name: 'insertParagraph', start: [4, 1, 0, 1] },
        { name: 'insertText', start: [4, 1, 0, 1, 0], text: 'Moon' },
        { name: 'insertCells', start: [4, 1, 1] },
        { name: 'insertParagraph', start: [4, 1, 1, 0] },
        { name: 'insertText', start: [4, 1, 1, 0, 0], text: 'Hello' },
        { name: 'insertParagraph', start: [4, 1, 1, 1] },
        { name: 'insertText', start: [4, 1, 1, 1, 0], text: 'Jupiter' },

        { name: 'insertParagraph', start: [5] },
        { name: 'insertText', start: [5, 0], text: 'uvwxy' },

        { name: 'insertParagraph', start: [6] },
        { name: 'insertText', start: [6, 0], text: 'zabcd' }
    ];

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        globalApp = app;
    });

    // existence check ----------------------------------------------------

    describe('TextFramework class XDeleteOperation', function () {

        it("should exist", () => {
            expect(XDeleteOperation).toBeObject();
        });

        // public functions -------------------------------------------------------

        describe("function XDeleteOperation.mixin", () => {

            it("should exist", () => {
                expect(XDeleteOperation).toRespondTo("mixin");
            });

            it("should create a subclass", () => {
                expect(XDeleteOperation.mixin(EditModel)).toBeSubClassOf(EditModel);
            });
        });
    });

    it('should contain a function deleteHandler', function () {
        expect(model).toRespondTo('deleteHandler');
    });

    it('should contain a function selectAll', function () {
        expect(model).toRespondTo('selectAll');
    });

    // public methods -----------------------------------------------------

    describe('method getNode', function () {

        it('should exist', function () {
            expect(model).toRespondTo('getNode');
        });

        it('should exist also as getCurrentRootNode', function () {
            expect(model).toRespondTo('getCurrentRootNode');
        });

        it('should return the same node as getCurrentRootNode', function () {
            expect(model.getNode()[0]).toBe(model.getCurrentRootNode()[0]);
        });

        it('should have a correct prepared document', function () {

            selection = model.getSelection();
            pageNode = model.getCurrentRootNode();
            pageContentNode = DOM.getPageContentNode(pageNode);
            const allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);

            expect(allParagraphs).toHaveLength(7);

            const para1 = $(allParagraphs[0]);
            const para2 = $(allParagraphs[1]);
            const para3 = $(allParagraphs[2]);
            const para4 = $(allParagraphs[3]);
            const table1 = $(allParagraphs[4]);
            const para5 = $(allParagraphs[5]);
            const para6 = $(allParagraphs[6]);

            expect(DOM.isParagraphNode(para1)).toBeTrue();
            expect(DOM.isParagraphNode(para2)).toBeTrue();
            expect(DOM.isParagraphNode(para3)).toBeTrue();
            expect(DOM.isParagraphNode(para4)).toBeTrue();
            expect(DOM.isTableNode(table1)).toBeTrue();
            expect(DOM.isParagraphNode(para5)).toBeTrue();
            expect(DOM.isParagraphNode(para6)).toBeTrue();

            expect(table1.find(DOM.TABLE_CELLNODE_SELECTOR)).toHaveLength(4); // 4 table cells inside the table
            expect(table1.find(DOM.CONTENT_NODE_SELECTOR)).toHaveLength(8); // 8 paragraphs inside the table

            expect(para1.text()).toBe("abcde");
            expect(para1.children()).toHaveLength(1); // only 1 text span

            expect(para6.text()).toBe("zabcd");
            expect(para6.children()).toHaveLength(1); // only 1 text span

            // checking the text content of one paragraph inside the table
            var tableParagraph  = $(Position.getParagraphElement(model.getCurrentRootNode(), [4, 0, 1, 1]));
            expect(tableParagraph.text()).toBe("Earth");
            expect(tableParagraph.children()).toHaveLength(1); // only 1 text span
        });
    });

    describe('method deleteRange', function () {
        it('should exist', function () {
            expect(model).toRespondTo('deleteRange');
        });

        it('should delete one character when the delete button is pressed', async function () {

            var pos = [0, 1];
            var opts = { setTextSelection: false };

            const allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(7);
            const para1 = $(allParagraphs[0]);

            expect(para1.text()).toHaveLength(5);
            expect(para1.children()).toHaveLength(1); // only 1 text span

            model.getSelection().setTextSelection(pos);
            await model.deleteRange(pos, pos, opts);

            expect(para1.text()).toBe("acde");
            expect(para1.children()).toHaveLength(1); // only 1 text span

            expect(selection.getStartPosition()).toEqual(pos);
            expect(selection.getEndPosition()).toEqual(pos);

            expect(selection.isTextCursor()).toBeTrue();

            // undo has to insert the 'b' again
            await model.getUndoManager().undo();

            expect(para1.text()).toBe("abcde");
            expect(para1.children()).toHaveLength(1); // only 1 text span

            // the old selection must be restored after undo
            expect(selection.getStartPosition()).toEqual(pos);
            expect(selection.getEndPosition()).toEqual(pos);
            expect(selection.isTextCursor()).toBeTrue();
        });
    });

    describe('method deleteSelected', function () {

        it('should exist', function () {
            expect(model).toRespondTo('deleteSelected');
        });

        it('should delete a selection range over more than one paragraph with only one delete operation and restore it correctly', async function () {

            var startPos = [0, 1];
            var endPos = [3, 3];
            var deleteEndPos = Position.decreaseLastIndex(endPos);

            var localOps = null;

            // Registering an event handler for 'operations:success' to check the generated operations.
            // Info: Using a jest.spyOn around 'model.deleteHandler' does not work. The function that is called, is
            //       already registered during the creation of the model. This happens in:
            //       this.registerPlainOperationHandler(Operations.DELETE, self.deleteHandler);
            //       Therefore generating a spy with:
            //       spy = jest.spyOn(model, 'deleteHandler');
            //       is useless here.
            model.on('operations:success', function (operations) { localOps = operations; });

            let allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            const para1 = $(allParagraphs[0]);

            expect(para1.text()).toHaveLength(5);
            expect(para1.children()).toHaveLength(1); // only 1 text span

            model.getSelection().setTextSelection(startPos, endPos);

            expect(selection.isTextCursor()).toBeFalse();

            await model.deleteSelected({ deleteKey: true }); // triggered by pressing 'delete' or 'backspace'

            allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(4); // only one remaining paragraph before the table (and two following paragraphs remain)

            expect(DOM.isParagraphNode(allParagraphs[0])).toBeTrue();
            expect(DOM.isTableNode(allParagraphs[1])).toBeTrue();
            expect(DOM.isParagraphNode(allParagraphs[2])).toBeTrue();
            expect(DOM.isParagraphNode(allParagraphs[3])).toBeTrue();

            expect(para1.text()).toBe("ast");
            expect(para1.children()).toHaveLength(1); // only 1 text span

            expect(selection.getStartPosition()).toEqual(startPos);
            expect(selection.getEndPosition()).toEqual(startPos);

            expect(selection.isTextCursor()).toBeTrue();

            // checking the operations (delete and merge)
            expect(localOps).toHaveLength(2);
            expect(localOps[0].name).toBe(Operations.DELETE); // only one delete operation!
            expect(localOps[0].start).toEqual(startPos);
            expect(localOps[0].end).toEqual(deleteEndPos);

            expect(localOps[1].name).toBe(Operations.PARA_MERGE);
            expect(localOps[1].start).toEqual([0]);

            // undo has to insert the paragraphs again
            await model.getUndoManager().undo();

            expect(para1.text()).toBe("abcde");
            expect(para1.children()).toHaveLength(1); // only 1 text span

            allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(7); // all 7 paragraphs and tables restored

            // the old selection must be restored after undo
            expect(selection.getStartPosition()).toEqual(startPos);
            expect(selection.getEndPosition()).toEqual(endPos);
            expect(selection.isTextCursor()).toBeFalse();
        });

        it('should delete a selection range from paragraph into table with one delete operation for every cell and restore it correctly', async function () {

            var startPos = [0, 1];
            var endPos = [4, 1, 1, 1, 1];

            var localOps = null;

            var deleteOps = [
                { name: Operations.DELETE, start: [4, 1, 1, 0], end: [4, 1, 1, 1, 0] },
                { name: Operations.DELETE, start: [4, 1, 0, 0], end: [4, 1, 0, 1, 3] },
                { name: Operations.DELETE, start: [4, 0, 1, 0], end: [4, 0, 1, 1, 4] },
                { name: Operations.DELETE, start: [4, 0, 0, 0], end: [4, 0, 0, 1, 4] },
                { name: Operations.DELETE, start: [0, 1], end: [3] }
            ];

            // Registering an event handler for 'operations:success' to check the generated operations.
            // Info: Using a jest.spyOn around 'model.deleteHandler' does not work. The function that is called, is
            //       already registered during the creation of the model. This happens in:
            //       this.registerPlainOperationHandler(Operations.DELETE, self.deleteHandler);
            //       Therefore generating a spy with:
            //       spy = jest.spyOn(model, 'deleteHandler');
            //       is useless here.
            model.on('operations:success', function (operations) { localOps = operations; });

            let allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            const para1 = $(allParagraphs[0]);

            expect(para1.text()).toHaveLength(5);
            expect(para1.children()).toHaveLength(1); // only 1 text span

            model.getSelection().setTextSelection(startPos, endPos);

            expect(selection.isTextCursor()).toBeFalse();

            // checking the text content of one paragraph inside the table
            let tableParagraph  = $(Position.getParagraphElement(model.getCurrentRootNode(), [4, 0, 1, 1]));
            expect(tableParagraph.text()).toBe("Earth");
            expect(tableParagraph.children()).toHaveLength(1); // only 1 text span

            await model.deleteSelected({ deleteKey: true }); // triggered by pressing 'delete' or 'backspace'

            allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(4); // only one remaining paragraph before the table (and two following paragraphs remain)

            expect(DOM.isParagraphNode(allParagraphs[0])).toBeTrue();
            expect(DOM.isTableNode(allParagraphs[1])).toBeTrue();
            expect(DOM.isParagraphNode(allParagraphs[2])).toBeTrue();
            expect(DOM.isParagraphNode(allParagraphs[3])).toBeTrue();

            expect(para1.text()).toBe("a");
            expect(para1.children()).toHaveLength(1); // only 1 text span

            expect(selection.getStartPosition()).toEqual(startPos);
            expect(selection.getEndPosition()).toEqual(startPos);

            expect(selection.isTextCursor()).toBeTrue(); // the cursor has to be at the start position

            // checking the operations (5 delete operations: one for every affected cell plus all content before the table in one operation)
            expect(localOps).toHaveLength(5);

            localOps.forEach((op, index) => {
                expect(op.name).toBe(deleteOps[index].name);
                expect(op.start).toEqual(deleteOps[index].start);
                expect(op.end).toEqual(deleteOps[index].end);
            });

            // checking the text content of one paragraph inside the table
            expect(Position.getParagraphElement(model.getCurrentRootNode(), [1, 0, 1, 1])).toBeNull(); // there is no paragraph at the specified position
            tableParagraph  = $(Position.getParagraphElement(model.getCurrentRootNode(), [1, 0, 1, 0])); // using the first paragraph in the cell
            expect(tableParagraph.text()).toBe("");
            expect(tableParagraph.children()).toHaveLength(2); // <br> element is create synchronously after pressing 'delete' or 'backspace'
            expect(tableParagraph.children('span')).toHaveLength(1);
            expect(tableParagraph.children('br')).toHaveLength(1);

            expect(DOM.isParagraphNode(tableParagraph)).toBeTrue();
            // expect(DOM.isEmptyParagraph(tableParagraph)).toBeTrue(); // validateParagraph might not have run yet

            // undo has to insert the paragraphs again and the content into the table
            await model.getUndoManager().undo();

            expect(para1.text()).toBe("abcde");
            expect(para1.children()).toHaveLength(1); // only 1 text span

            allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(7); // all 7 paragraphs and tables restored

            // checking the text content of one paragraph inside the table
            tableParagraph  = $(Position.getParagraphElement(model.getCurrentRootNode(), [4, 0, 1, 1]));
            expect(tableParagraph.text()).toBe("Earth");

            expect(tableParagraph.children('br')).toHaveLength(1); // <br> will be removed later, validateParagraphNode follows async after undo
            expect(tableParagraph.children('span')).toHaveLength(1); // only 1 text span

            // TODO: This line sometimes fails because there is only 1 child in the tableParagraph. New order of "expects" allows
            //       determining, whether this is a "span" or a "br" (see lines above).
            expect(tableParagraph.children()).toHaveLength(2);

            // the old selection must be restored after undo
            expect(selection.getStartPosition()).toEqual(startPos);
            expect(selection.getEndPosition()).toEqual(endPos);
            expect(selection.isTextCursor()).toBeFalse();

            await model.executeDelayed(() => null, 500);
            expect(tableParagraph.text()).toBe("Earth");
            expect(tableParagraph.children()).toHaveLength(1);
            expect(tableParagraph.children('span')).toHaveLength(1); // only 1 text span, <br> element was removed in the meantime
        });

        it('should delete the complete document content with only one operation and restore it correctly', async function () {

            var firstPos = [0, 0];
            var lastPos = [6, 5];

            model.selectAll();

            expect(selection.getStartPosition()).toEqual(firstPos);
            expect(selection.getEndPosition()).toEqual(lastPos);

            expect(selection.isTextCursor()).toBeFalse();

            var localOps = null;

            var deleteOps = [
                { name: Operations.DELETE, start: [0], end: [6, 4] }
            ];

            // Registering an event handler for 'operations:success' to check the generated operations.
            // Info: Using a jest.spyOn around 'model.deleteHandler' does not work. The function that is called, is
            //       already registered during the creation of the model. This happens in:
            //       this.registerPlainOperationHandler(Operations.DELETE, self.deleteHandler);
            //       Therefore generating a spy with:
            //       spy = jest.spyOn(model, 'deleteHandler');
            //       is useless here.
            model.on('operations:success', function (operations) { localOps = operations; });

            await model.deleteSelected({ deleteKey: true }); // triggered by pressing 'delete' or 'backspace'

            const allParagraphs = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs).toHaveLength(1); // only one remaining paragraph before the table (and two following paragraphs remain)

            const para1 = $(allParagraphs[0]); // this is yet a new dom node

            expect(DOM.isParagraphNode(para1)).toBeTrue();

            expect(para1.text()).toBe("");
            expect(para1.children()).toHaveLength(2); // only 1 text span and <br> element

            expect(DOM.isEmptyParagraph(para1)).toBeTrue();

            expect(selection.getStartPosition()).toEqual(firstPos);
            expect(selection.getEndPosition()).toEqual(firstPos);

            expect(selection.isTextCursor()).toBeTrue(); // the cursor has to be at the first position

            // checking the operations -> only 1 operation to delete the complete document
            expect(localOps).toHaveLength(1);

            expect(localOps[0].name).toBe(deleteOps[0].name);
            expect(localOps[0].start).toEqual(deleteOps[0].start);
            expect(localOps[0].end).toEqual(deleteOps[0].end);

            // undo has to insert the paragraphs and the table again
            await model.getUndoManager().undo();

            const allParagraphs2 = pageContentNode.children(DOM.CONTENT_NODE_SELECTOR);
            expect(allParagraphs2).toHaveLength(7); // all 7 paragraphs and tables restored

            const para1_2 = $(allParagraphs2[0]);
            const table1 = $(allParagraphs2[4]);
            const para6 = $(allParagraphs2[6]);

            expect(DOM.isParagraphNode(para1_2)).toBeTrue();
            expect(DOM.isParagraphNode(allParagraphs2[1])).toBeTrue();
            expect(DOM.isParagraphNode(allParagraphs2[2])).toBeTrue();
            expect(DOM.isParagraphNode(allParagraphs2[3])).toBeTrue();
            expect(DOM.isTableNode(table1)).toBeTrue();
            expect(DOM.isParagraphNode(allParagraphs2[5])).toBeTrue();
            expect(DOM.isParagraphNode(para6)).toBeTrue();

            expect(table1.find(DOM.TABLE_CELLNODE_SELECTOR)).toHaveLength(4); // 4 table cells inside the table
            expect(table1.find(DOM.CONTENT_NODE_SELECTOR)).toHaveLength(8); // 8 paragraphs inside the table

            // OT:
            // Synchronous validation of paragraph node required, because 'implInsertText'
            // calls 'implParagraphChanged' (and therefore 'validateParagraphNode')
            // asynchronously, if OT is enabled. In this case 'validateParagraphNode'
            // would not be called fast enough and therefore there would be an
            // additional <br> element inside the paragraph.
            if (globalApp.isOTEnabled()) { model.validateParagraphNode(para1_2); }
            expect(para1_2.text()).toBe("abcde");
            expect(para1_2.children()).toHaveLength(1);

            if (globalApp.isOTEnabled()) { model.validateParagraphNode(para6); }
            expect(para6.text()).toBe("zabcd");
            expect(para6.children()).toHaveLength(1);

            // checking the text content of one paragraph inside the table
            const tableParagraph  = $(Position.getParagraphElement(model.getCurrentRootNode(), [4, 0, 1, 1]));
            if (globalApp.isOTEnabled()) { model.validateParagraphNode(tableParagraph); }
            expect(tableParagraph.text()).toBe("Earth");
            expect(tableParagraph.children()).toHaveLength(1);

            // the old selection must be restored after undo
            expect(selection.getStartPosition()).toEqual(firstPos);
            expect(selection.getEndPosition()).toEqual(lastPos);
            expect(selection.isTextCursor()).toBeFalse();
        });
    });
});
