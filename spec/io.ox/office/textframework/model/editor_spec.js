/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { EditModel } from '@/io.ox/office/editframework/model/editmodel';
import Editor from '@/io.ox/office/textframework/model/editor';

import { createTextApp } from '~/text/apphelper';

// class Editor ===========================================================

describe('Text class Editor', function () {

    // private helpers ----------------------------------------------------

    var editor;
    createTextApp('ooxml').done(function (app) {
        editor = app.getModel();
    });

    // existence check -----------------------------------------------------

    it('should subclass EditModel', function () {
        expect(Editor).toBeSubClassOf(EditModel);
    });

    // public methods -----------------------------------------------------

    describe('method getNode', function () {
        it('should exist', function () {
            expect(Editor).toHaveMethod('getNode');
        });
        it('should return jQuery node', function () {
            expect(editor.getNode()).toBeInstanceOf($);
        });
    });

    describe('method getDrawingLayer', function () {
        it('should exist', function () {
            expect(Editor).toHaveMethod('getDrawingLayer');
        });
        it('should return object', function () {
            expect(editor.getDrawingLayer()).toBeInstanceOf(Object);
        });
    });

    describe('method getCommentLayer', function () {
        it('should exist', function () {
            expect(Editor).toHaveMethod('getCommentLayer');
        });
        it('should return object', function () {
            expect(editor.getCommentLayer()).toBeInstanceOf(Object);
        });
    });

    describe('method getRangeMarker', function () {
        it('should exist', function () {
            expect(Editor).toHaveMethod('getRangeMarker');
        });
        it('should return object', function () {
            expect(editor.getRangeMarker()).toBeInstanceOf(Object);
        });
    });

    describe('method getFieldManager', function () {
        it('should exist', function () {
            expect(Editor).toHaveMethod('getFieldManager');
        });
        it('should return object', function () {
            expect(editor.getFieldManager()).toBeInstanceOf(Object);
        });
    });

    describe('method getChangeTrack', function () {
        it('should exist', function () {
            expect(Editor).toHaveMethod('getChangeTrack');
        });
        it('should return object', function () {
            expect(editor.getChangeTrack()).toBeInstanceOf(Object);
        });
    });

    describe('method getPageLayout', function () {
        it('should exist', function () {
            expect(Editor).toHaveMethod('getPageLayout');
        });
        it('should return object', function () {
            expect(editor.getPageLayout()).toBeInstanceOf(Object);
        });
    });

    describe('property numberFormatter', function () {
        it('should exist', function () {
            expect(editor.numberFormatter).toBeInstanceOf(Object);
        });
    });

    describe('method getLastOperationEnd', function () {
        beforeAll(function () {
            editor.setLastOperationEnd([1, 1]);
        });

        it('should exist', function () {
            expect(Editor).toHaveMethod('getLastOperationEnd');
        });
        it('should return correct last operation end', function () {
            expect(editor.getLastOperationEnd()).toEqual([1, 1]); // deep equal - array comparison
        });
        it('should not return incorrect last operation end', function () {
            expect(editor.getLastOperationEnd()).not.toEqual([1, 0]);
        });
    });

    describe('method getValidImplicitParagraphNode', function () {
        it('should exist', function () {
            expect(Editor).toHaveMethod('getValidImplicitParagraphNode');
        });
        it('should return object', function () {
            const node = editor.getValidImplicitParagraphNode();
            expect(node).toBeInstanceOf($);
            expect(node.hasClass('p')).toBeTrue();
        });
    });
});
