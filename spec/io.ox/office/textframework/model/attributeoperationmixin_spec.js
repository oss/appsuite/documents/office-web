/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import * as Utils from '@/io.ox/office/tk/utils';
import { EditModel } from '@/io.ox/office/editframework/model/editmodel';
import { transformOpColor, Color } from '@/io.ox/office/editframework/utils/color';
import { LineHeight } from '@/io.ox/office/editframework/utils/lineheight';
import { XAttributeOperation } from '@/io.ox/office/textframework/model/attributeoperationmixin';

import * as Position from '@/io.ox/office/textframework/utils/position';

import { createTextApp } from '~/text/apphelper';

// class AttributeOperationMixin ==========================================

describe('TextFramework class AttributeOperationMixin', function () {

    const PARAGRAPHSPACING__NONE   = 0;
    const PARAGRAPHSPACING__NORMAL = 1;
    const PARAGRAPHSPACING__WIDE   = 2;

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes',
            attrs: {
                document:  { defaultTabStop: 1270, zoom: { value: 100 } },
                page:      { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 },
                character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' },
                paragraph: { lineHeight: { type: 'percent', value: 119 }, marginBottom: 352 }
            }
        },
        { name: 'insertStyleSheet', type: 'paragraph', styleId: 'Heading1', styleName: 'heading 1',
            attrs: {
                character: { bold: true, fontName: 'Times New Roman', fontSize: 14, color: transformOpColor(Color.ACCENT1, { shade: 74902 }) },
                paragraph: { marginTop: 846, outlineLevel: 0, nextStyleId: 'Normal' }
            },
            parent: 'Normal',
            uiPriority: 9
        },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', text: 'Hello World', start: [0, 0] },

        { name: 'splitParagraph', start: [0, 11] },
        { name: 'insertText', start: [1, 0], text: [
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet',
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet',
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet.'
        ].join('') },

        { name: 'splitParagraph', start: [1, 240] },
        { name: 'insertText', start: [2, 0], text: [
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet',
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet',
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet.'
        ].join('') },

        { name: 'splitParagraph', start: [2, 240] },
        { name: 'insertText', start: [3, 0], text: [
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet',
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet',
            'lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet.'
        ].join('') },

        { name: 'splitParagraph', start: [3, 240] }
    ];

    var model, firstParagraph, secondParagraph, defaultMarginBottom;
    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        var node = model.getNode();
        firstParagraph  = Position.getParagraphElement(node, [0]);
        secondParagraph = Position.getParagraphElement(node, [1]);
        defaultMarginBottom = model.getDefaultMarginBottom();
    });

    // existence check ----------------------------------------------------

    describe('TextFramework class XGroupOperation', function () {

        it("should exist", () => {
            expect(XAttributeOperation).toBeObject();
        });

        // public functions -------------------------------------------------------

        describe("function XTableOperation.mixin", () => {

            it("should exist", () => {
                expect(XAttributeOperation).toRespondTo("mixin");
            });

            it("should create a subclass", () => {
                expect(XAttributeOperation.mixin(EditModel)).toBeSubClassOf(EditModel);
            });
        });
    });

    // public methods -----------------------------------------------------

    describe('method setAttributes', function () {
        it('should exist', function () {
            expect(model).toRespondTo('setAttributes');
        });
        // DOM layout (lineHeight calculation) required -> replaced with e2e test
        it('should set the specified paragraph style', function () {

            function compareEMWithPT(elementEM, comparePT) {
                var compare = Utils.convertToEMValue(comparePT, 'pt');
                expect(parseFloat(elementEM, 10)).toBeCloseTo(compare, 2);
            }

            function expectParagraphSpacing(position, exp) {
                model.getSelection().setTextSelection(position);
                var attributes = model.getAttributes('paragraph').paragraph;
                expect(model.getParagraphSpacing(attributes)).toBe(exp);
            }

            expect($(firstParagraph).hasClass('p')).toBeTrue();
            compareEMWithPT($(firstParagraph).children().first().css('font-size'), 11);

            // setting styleId 'Heading1' to one and only paragraph
            model.getSelection().setTextSelection([0, 2]); // not necessary
            model.setAttributes('paragraph', { styleId: 'Heading1' }, { clear: true });

            compareEMWithPT($(firstParagraph).children().first().css('font-size'), 14);

            /**
             *  'Paragraph and LineHeight spacing' test - User Story #99693312:
             *  https://www.pivotaltracker.com/projects/534975/stories/99693312
             */
            // setting both the paragraph spacing and the line height, covering the whole selection ...
            //
            // ... that should be equal to the following operations ...
            // { name: 'setAttributes', start: [1], attrs: { paragraph: { marginBottom: 500 }} },
            // { name: 'setAttributes', start: [2], attrs: { paragraph: { marginBottom: 500 }} },
            // { name: 'setAttributes', start: [3], attrs: { paragraph: { marginBottom: 500 }} }

            //
            // 1st COMBINED ROUND (lineHeight + marginBottom)
            //
            model.getSelection().setTextSelection([1, 0], [3, 240]);
            model.setAttribute('paragraph', 'lineHeight', LineHeight._115);
            model.setAttribute('paragraph', 'marginBottom', (PARAGRAPHSPACING__WIDE * defaultMarginBottom));

            // expect(parseInt($(secondParagraph).find('span')[0].style.lineHeight, 10)).toBe(139);
            // expect(parseInt($(thirdParagraph).find('span')[0].style.lineHeight, 10)).toBe(139);
            // expect(parseInt($(fourthParagraph).find('span')[0].style.lineHeight, 10)).toBe(139);

            expectParagraphSpacing([1, 0], PARAGRAPHSPACING__WIDE);
            expectParagraphSpacing([2, 0], PARAGRAPHSPACING__WIDE);
            expectParagraphSpacing([3, 0], PARAGRAPHSPACING__WIDE);

            //
            // 2nd COMBINED ROUND (lineHeight + marginBottom)
            //
            model.getSelection().setTextSelection([1, 0], [2, 240]);
            model.setAttribute('paragraph', 'lineHeight', LineHeight.ONE_HALF);
            model.setAttribute('paragraph', 'marginBottom', (PARAGRAPHSPACING__NONE * defaultMarginBottom));

            // expect(parseInt($(secondParagraph).find('span')[0].style.lineHeight, 10)).toBe(180);
            // expect(parseInt($(thirdParagraph).find('span')[0].style.lineHeight, 10)).toBe(180);
            // expect(parseInt($(fourthParagraph).find('span')[0].style.lineHeight, 10)).toBe(139);

            expectParagraphSpacing([1, 0], PARAGRAPHSPACING__NONE);
            expectParagraphSpacing([2, 0], PARAGRAPHSPACING__NONE);
            expectParagraphSpacing([3, 0], PARAGRAPHSPACING__WIDE);

            //
            // 3rd COMBINED ROUND (lineHeight + marginBottom)
            //
            model.getSelection().setTextSelection([2, 0], [3, 240]);
            model.setAttribute('paragraph', 'lineHeight', LineHeight.SINGLE);
            model.setAttribute('paragraph', 'marginBottom', (PARAGRAPHSPACING__NORMAL * defaultMarginBottom));

            // expect(parseInt($(secondParagraph).find('span')[0].style.lineHeight, 10)).toBe(180);
            // // fontCollection.getNormalLineHeight() in characterStyles.updateElementLineHeight() causes 119 or 126 to be possible (1pt difference)
            // expect(parseInt($(thirdParagraph).find('span')[0].style.lineHeight, 10)).toBeOneOf([119, 126]);
            // expect(parseInt($(fourthParagraph).find('span')[0].style.lineHeight, 10)).toBeOneOf([119, 126]);

            expectParagraphSpacing([1, 0], PARAGRAPHSPACING__NONE);
            expectParagraphSpacing([2, 0], PARAGRAPHSPACING__NORMAL);
            expectParagraphSpacing([3, 0], PARAGRAPHSPACING__NORMAL);

            //
            // 4th COMBINED ROUND (lineHeight + marginBottom)
            //
            model.getSelection().setTextSelection([1, 5]);
            model.setAttribute('paragraph', 'lineHeight', LineHeight.DOUBLE);
            model.setAttribute('paragraph', 'marginBottom', (PARAGRAPHSPACING__WIDE * defaultMarginBottom));

            // fontCollection.getNormalLineHeight() in characterStyles.updateElementLineHeight() causes 119 or 126 (235 or 242) to be possible (1pt difference)
            // expect(parseInt($(secondParagraph).find('span')[0].style.lineHeight, 10)).toBeOneOf([235, 242]);
            // expect(parseInt($(thirdParagraph).find('span')[0].style.lineHeight, 10)).toBeOneOf([119, 126]);
            // expect(parseInt($(fourthParagraph).find('span')[0].style.lineHeight, 10)).toBeOneOf([119, 126]);

            expectParagraphSpacing([1, 0], PARAGRAPHSPACING__WIDE);
            expectParagraphSpacing([2, 0], PARAGRAPHSPACING__NORMAL);
            expectParagraphSpacing([3, 0], PARAGRAPHSPACING__NORMAL);

            //
            // 5th COMBINED ROUND (lineHeight + marginBottom)
            //
            model.getSelection().setTextSelection([1, 5], [2, 12]);
            model.setAttribute('paragraph', 'lineHeight', LineHeight.NORMAL);
            model.setAttribute('paragraph', 'marginBottom', (PARAGRAPHSPACING__NONE * defaultMarginBottom));

            // fontCollection.getNormalLineHeight() in characterStyles.updateElementLineHeight() causes 119 or 126 to be possible (1pt difference)
            // expect(parseInt($(secondParagraph).find('span')[0].style.lineHeight, 10)).toBeOneOf([119, 126]);
            // expect(parseInt($(thirdParagraph).find('span')[0].style.lineHeight, 10)).toBeOneOf([119, 126]);
            // expect(parseInt($(fourthParagraph).find('span')[0].style.lineHeight, 10)).toBeOneOf([119, 126]);

            expectParagraphSpacing([1, 0], PARAGRAPHSPACING__NONE);
            expectParagraphSpacing([2, 0], PARAGRAPHSPACING__NONE);
            expectParagraphSpacing([3, 0], PARAGRAPHSPACING__NORMAL);
            //
            // END OF 'Paragraph and LineHeight spacing' test
        });

        it('should avoid setting character attributes to trailing whitespace(s)', function () {
            expect($(firstParagraph).find('span')[0]).toHaveStyle({ textDecoration: "none" });

            model.getSelection().setTextSelection([0, 0], [0, 6]);
            model.setAttribute('character', 'underline', true);

            expect($(firstParagraph).find('span')[0]).toHaveStyle({ textDecoration: "underline" });
            expect($(firstParagraph).find('span').first().text()).toBe("Hello");
        });

        it('should expand character attributes to whole surrounding word', function () {
            expect($(firstParagraph).find('span')[1]).toHaveStyle({ textDecoration: "none" });

            // first setting cursor at the begining of word - should not expand
            model.getSelection().setTextSelection([0, 6]);
            model.setAttribute('character', 'underline', true);

            expect($(firstParagraph).find('span')[1]).toHaveStyle({ textDecoration: "none" });

            // now setting cursor inside word - should expand
            model.getSelection().setTextSelection([0, 7]);
            model.setAttribute('character', 'underline', true);

            expect($(firstParagraph).find('span').last()[0]).toHaveStyle({ textDecoration: "underline" });
            expect($(firstParagraph).find('span').last().text()).toBe("World");
        });

        it('should set small caps to a specific word', function () {

            var allParagraphChildren = $(secondParagraph).children();

            expect(allParagraphChildren).toHaveLength(1); // only one span in the second paragraph
            // Text in second paragraph: 'lorem ipsum dolor ...'
            model.getSelection().setTextSelection([1, 6], [1, 11]); // selecting 'ipsum'
            model.setAttribute('character', 'caps', 'small'); // setting small caps to the word ipsum

            allParagraphChildren = $(secondParagraph).children();  // refresh after applying the operation
            expect(allParagraphChildren).toHaveLength(3);

            const secondSpan = $(allParagraphChildren[1]);

            expect(secondSpan.text()).toBe("ipsum");
            // checking the data object at the span node
            expect(secondSpan.data().attributes.character.caps).toBe("small");
            // checking if the correct class has been set
            expect(secondSpan.hasClass('lowercase')).toBeTrue();
        });
    });

    // checking positions of setAttributes operation
    // -> directly calling 'setAttributesHandler' with test positions. This function
    //    returns whether the operation could be successfully applied (using a boolean)
    describe('method setAttributesHandler', function () {

        it('should exist', function () {
            expect(model).toRespondTo('setAttributesHandler');
            expect(model).toRespondTo('getCurrentRootNode'); // required function
        });
        it('should successfully apply operation with valid logical positions', function () {

            var paraPos = 0; // using the first paragraph
            var length = Position.getParagraphLength(model.getCurrentRootNode(), [paraPos]);

            expect(length).toBe(11); // 'Hello World'

            var operation = {};
            operation.name = 'setAttributes';
            operation.start = [0, 0];
            operation.end = [0, length]; // using paragraph length as end position (behind last character)
            operation.attrs = { character: { bold: true } };

            // not allowed operation: the end position is behind the last character
            expect(model.setAttributesHandler(operation, false)).toBeFalse();

            operation.end = [0, length - 1]; // using (length - 1) as end position (position of last character)
            operation.attrs = { character: { bold: true } };

            // allowed operation: the end position is the position of the last character
            expect(model.setAttributesHandler(operation, false)).toBeTrue();
        });
    });
});
