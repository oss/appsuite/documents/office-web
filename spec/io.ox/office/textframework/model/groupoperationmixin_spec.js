/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Color } from '@/io.ox/office/editframework/utils/color';
import { EditModel } from '@/io.ox/office/editframework/model/editmodel';
import { isGroupDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { XGroupOperation } from '@/io.ox/office/textframework/model/groupoperationmixin';
import * as DOM from '@/io.ox/office/textframework/utils/dom';

import { createPresentationApp } from '~/presentation/apphelper';

// class PresentationModel ================================================

describe('Presentation class GroupOperationMixin', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        layoutId_1 = 'layout1',
        layoutId_2 = 'layout2',
        masterId_1 = 'master1',
        slide1DefaultName = 'slide_1',
        // slide2DefaultName = 'slide_2',
        activeSlide = null,

        // the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { page: { width: 33866, height: 19050, orientation: 'landscape' } } },
            { name: 'insertMasterSlide', id: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_2, target: masterId_1 },
            { name: 'insertSlide', start: [0], target: layoutId_1, attrs: { fill: { type: 'solid', color: Color.RED } } },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Title', noGroup: true } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertText', text: 'Title', start: [0, 0, 0, 0] },
            { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Subtitle', noGroup: true } } },
            { name: 'insertParagraph', start: [0, 1, 0] },
            { name: 'insertText', text: 'SubTitle', start: [0, 1, 0, 0] },
            { name: 'insertDrawing', start: [0, 2], type: 'shape', attrs: { drawing: { name: 'rect', width: 2000, height: 3000, left: 1000, top: 1000 }, geometry: { presetShape: 'rect', avList: {} } } },
            { name: 'insertParagraph', start: [0, 2, 0] },
            { name: 'insertText', text: 'Shape 1', start: [0, 2, 0, 0] },
            { name: 'insertDrawing', start: [0, 3], type: 'shape', attrs: { drawing: { name: 'rect', width: 2000, height: 2000, left: 3000, top: 3000 }, geometry: { presetShape: 'rect', avList: {} } } },
            { name: 'insertParagraph', start: [0, 3, 0] },
            { name: 'insertText', text: 'Shape 2', start: [0, 3, 0, 0] },
            { name: 'insertDrawing', start: [0, 4], type: 'shape', attrs: { drawing: { name: 'rect', width: 2000, height: 3000, left: 6000, top: 6000 }, geometry: { presetShape: 'rect', avList: {} } } },
            { name: 'insertParagraph', start: [0, 4, 0] },
            { name: 'insertText', text: 'Shape 4', start: [0, 4, 0, 0] },
            { name: 'insertSlide', start: [1], target: layoutId_1, attrs: { fill: { type: 'solid', color: Color.RED } } },
            { name: 'insertDrawing', start: [1, 0], type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', noGroup: true } } },
            { name: 'insertParagraph', start: [1, 0, 0] },
            { name: 'insertText', text: 'Slide 2', start: [1, 0, 0, 0] }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // existence check ----------------------------------------------------

    describe('TextFramework class XGroupOperation', function () {

        it("should exist", () => {
            expect(XGroupOperation).toBeObject();
        });

        // public functions -------------------------------------------------------

        describe("function XTableOperation.mixin", () => {

            it("should exist", () => {
                expect(XGroupOperation).toRespondTo("mixin");
            });

            it("should create a subclass", () => {
                expect(XGroupOperation.mixin(EditModel)).toBeSubClassOf(EditModel);
            });
        });
    });

    // public methods -----------------------------------------------------

    describe('method getSlideById', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getSlideById');
        });
        it('should return the standard IDs for the active slides', function () {
            // checking if the document is correctly prepared
            // model.getSelection().setTextSelection([1, 0, 0, 0]);  // selecting the second slide
            // expect(model.getActiveSlideId()).toBe(slide2DefaultName);
            // activeSlide = model.getSlideById(model.getActiveSlideId());
            // expect(activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(1);

            model.getSelection().setTextSelection([0, 0, 0, 0]);  // selecting the first slide
            activeSlide = model.getSlideById(model.getActiveSlideId());
            expect(model.getActiveSlideId()).toBe(slide1DefaultName);
            expect(activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(5);
        });
    });

    describe('method setMultiDrawingSelectionByPosition', function () {
        it('should exist', function () {
            expect(model.getSelection()).toRespondTo('setMultiDrawingSelectionByPosition');
        });
        it('should allow to set a multi drawing selection with the specified logical positions', function () {
            // checking if the document is correctly prepared
            model.getSelection().setMultiDrawingSelectionByPosition([[0, 1], [0, 2], [0, 3]]);  // selecting three drawings by position
            expect(model.getSelection().getMultiSelectionCount()).toBe(3);

            model.getSelection().setMultiDrawingSelectionByPosition([[0, 1], [0, 2]]);  // selecting two drawings by position
            expect(model.getSelection().getMultiSelectionCount()).toBe(2);

            model.getSelection().clearMultiSelection();  // selecting three drawings by position
            expect(model.getSelection().getMultiSelectionCount()).toBe(0);

            model.getSelection().setMultiDrawingSelectionByPosition([[0, 1], [0, 2], [0, 4]]);  // selecting three drawings by position
            expect(model.getSelection().getMultiSelectionCount()).toBe(3);
        });
    });

    describe('method setMultiDrawingSelection', function () {
        it('should exist', function () {
            expect(model.getSelection()).toRespondTo('setMultiDrawingSelection');
        });
        it('should allow to set a multi drawing selection with the specified drawing nodes', function () {
            activeSlide = model.getSlideById(model.getActiveSlideId());
            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);

            expect(allDrawings).toHaveLength(5);

            model.getSelection().setMultiDrawingSelection([allDrawings[0], allDrawings[1], allDrawings[3]]);  // selecting three drawings by node
            expect(model.getSelection().getMultiSelectionCount()).toBe(3);
        });
    });

    describe('method isGroupableSelection', function () {
        it('should exist', function () {
            expect(model.getSelection()).toRespondTo('isGroupableSelection');
        });
        it('should return that a multi drawing selection with place holder drawing is not groupable', function () {
            model.getSelection().setMultiDrawingSelectionByPosition([[0, 1], [0, 2], [0, 4]]);
            expect(model.getSelection().isGroupableSelection()).toBeFalse();
        });
        it('should return that a multi drawing selection without place holder drawing is groupable', function () {
            model.getSelection().setMultiDrawingSelectionByPosition([[0, 2], [0, 3], [0, 4]]);
            expect(model.getSelection().isGroupableSelection()).toBeTrue();
        });
    });

    describe('method groupDrawings', function () {
        it('should exist', function () {
            expect(model).toRespondTo('groupDrawings');
        });
        it('should group the selected drawings', function () {
            model.groupDrawings();
            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(allDrawings).toHaveLength(3); // only the remaining children
            expect(isGroupDrawingFrame(allDrawings[2])).toBeTrue(); // at position [0, 2] must be a group
        });
    });

    describe('method isUngroupableSelection', function () {
        it('should exist', function () {
            expect(model.getSelection()).toRespondTo('isUngroupableSelection');
        });
        it('should return that a multi drawing selection with place holder drawing is not groupable', function () {
            // model.getSelection().setMultiDrawingSelectionByPosition([[0, 1], [0, 2], [0, 3]]);
            model.getSelection().selectAll();
            expect(model.getSelection().isUngroupableSelection()).toBeTrue();
        });
    });

    describe('method ungroupDrawings', function () {
        it('should exist', function () {
            expect(model).toRespondTo('ungroupDrawings');
        });
        it('should ungroup the selected drawings', function () {
            model.getSelection().selectAll();
            expect(model.getSelection().getMultiSelectionCount()).toBe(3);
            expect(model.getSelection().isUngroupableSelection()).toBeTrue();
            model.ungroupDrawings();
            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(allDrawings).toHaveLength(5); // only the remaining children
            expect(isGroupDrawingFrame(allDrawings[2])).toBeFalse(); // at position [0, 2] must no longer be a group
            model.getSelection().selectAll();
            expect(model.getSelection().getMultiSelectionCount()).toBe(5);
            expect(model.getSelection().isUngroupableSelection()).toBeFalse();
        });
    });

});
