/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';
import _ from '$/underscore';

import { EditModel } from '@/io.ox/office/editframework/model/editmodel';
import { opRgbColor, Color } from '@/io.ox/office/editframework/utils/color';

import { XClipBoardHandler } from '@/io.ox/office/textframework/model/clipboardhandlermixin';
import * as Position from '@/io.ox/office/textframework/utils/position';

import { createTextApp } from '~/text/apphelper';

// class ClipBoardHandlerMixin =============================================

describe('TextFramework class ClipBoardHandlerMixin', function () {

    // private helpers ----------------------------------------------------

    var firstParagraph = null,
        secondParagraph = null,
        commentLayer = null,
        firstParaTextBefore = 'Hello new world',
        firstParaTextAfter = 'Hello 12345 world',
        secondParaText = 'More text in paragraph',
        secondParaTextAfter = 'More 111222 behind in paragraph',
        pasteText = '12345',
        clipboardOperations = null,
        clipboardOperations1 = [{ name: 'insertText', start: [0, 0], text: pasteText }],
        clipboardOperations2 = [
            { name: 'insertText', start: [0, 0], text: '111' },
            { name: 'insertRange', start: [0, 3], id: 'cmt1234_1', type: 'comment', position: 'start' },
            { name: 'insertText', start: [0, 4], text: '222' },
            { name: 'insertRange', start: [0, 7], id: 'cmt1234_1', type: 'comment', position: 'end' },
            { name: 'insertComment', start: [0, 8], text: 'abcdef', id: 'cmt1234_1', author: 'author', userId: '1227', date: '2016-01-08T12:13:00Z' }, // defining userId causes problems
            { name: 'insertText', start: [0, 9], text: ' behind' }
        ],

        // the operations to be applied by the document model
        OPERATIONS = [
            { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
            { name: 'insertParagraph', start: [0] },
            { name: 'insertText', text: firstParaTextBefore, start: [0, 0] },
            { name: 'insertParagraph', start: [1] },
            { name: 'insertText', text: secondParaText, start: [1, 0] }
        ];

    var model;
    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        model.getClipboardOperations = function () { return clipboardOperations; };
        model.setClipboardOperations = function (ops) { clipboardOperations = ops; };
        model.hasInternalClipboard = _.constant(true);
        model.checkSetClipboardPasteInProgress = _.constant(false);
        model.isPasteToSameDocumentType = _.constant(true);
        commentLayer = model.getCommentLayer();
    });

    // existence check ----------------------------------------------------

    describe('TextFramework class XClipBoardHandler', function () {

        it("should exist", () => {
            expect(XClipBoardHandler).toBeObject();
        });

        // public functions -------------------------------------------------------

        describe("function XClipBoardHandler.mixin", () => {

            it("should exist", () => {
                expect(XClipBoardHandler).toRespondTo("mixin");
            });

            it("should create a subclass", () => {
                expect(XClipBoardHandler.mixin(EditModel)).toBeSubClassOf(EditModel);
            });
        });
    });

    // public methods -----------------------------------------------------

    describe('method pasteInternalClipboard', function () {
        it('should exist', function () {
            expect(model).toRespondTo('pasteInternalClipboard');
        });
        it('should contain the correct text in the first paragraph before pasting', function () {
            firstParagraph = Position.getParagraphElement(model.getNode(), [0]);
            expect($(firstParagraph).text()).toBe(firstParaTextBefore);
            expect(Position.getParagraphNodeLength(firstParagraph)).toBe(firstParaTextBefore.length);
        });
        it('should contain the correct text in the first paragraph after pasting', async function () {
            model.getSelection().setTextSelection([0, 6], [0, 9]); // selecting the word 'new'
            clipboardOperations = clipboardOperations1;
            await model.pasteInternalClipboard();
            expect($(firstParagraph).text()).toBe(firstParaTextAfter);
            expect(Position.getParagraphNodeLength(firstParagraph)).toBe(firstParaTextAfter.length);
        });
        it('should paste comment place holders and range markers into the main document', async function () {
            model.getSelection().setTextSelection([1, 5], [1, 9]); // selecting the word 'text' in second paragraph
            secondParagraph = Position.getParagraphElement(model.getNode(), [1]);
            expect(Position.getParagraphNodeLength(secondParagraph)).toBe(secondParaText.length);
            clipboardOperations = clipboardOperations2;
            await model.pasteInternalClipboard();
            expect($(secondParagraph).text()).toBe(secondParaTextAfter);
            expect(Position.getParagraphNodeLength(secondParagraph)).toBe(secondParaTextAfter.length + 3); // 3 additional place holder and range marker nodes
            expect(commentLayer.getCommentNumber()).toBe(1);
        });
    });

});

// ========================================================================
//     Test ODF specific stuff
// ========================================================================

describe('TextFramework class ClipBoardHandlerMixin II', function () {

    // private helpers ----------------------------------------------------

    var firstParagraph      = null;
    var firstParaTextBefore = 'Hello new world';
    var firstParaTextAfter  = 'Hello 12345 world';
    var secondParaText      = 'More text in paragraph';
    var pasteText           = '12345';

    var colorBefore     = { ...Color.ACCENT4, fallbackValue: 'FFC000' };
    var fillColorBefore = { ...Color.ACCENT5, fallbackValue: '4472C4' };
    var colorAfter      = opRgbColor('FFC000');
    var fillColorAfter  = opRgbColor('4472C4');

    var clipboardOperations  = null;
    var clipboardOperations1 = [{
        name: 'insertText',
        start: [0, 0],
        text: pasteText,
        attrs: { character: { color: colorBefore, fillColor: fillColorBefore } }
    }];

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', text: firstParaTextBefore, start: [0, 0] },
        { name: 'insertParagraph', start: [1] },
        { name: 'insertText', text: secondParaText, start: [1, 0] }
    ];

    var model    = null;
    var rootNode = null;
    createTextApp('odf', OPERATIONS).done(function (app) {
        model = app.getModel();
        model.getClipboardOperations = function () { return clipboardOperations; };
        model.setClipboardOperations = function (ops) { clipboardOperations = ops; };
        model.hasInternalClipboard = _.constant(true);
        model.checkSetClipboardPasteInProgress = _.constant(false);

        rootNode = model.getCurrentRootNode(model.getActiveTarget());
    });

    // public methods -----------------------------------------------------

    describe('method pasteInternalClipboard', function () {
        it('should exist', function () {
            expect(model).toRespondTo('pasteInternalClipboard');
        });

        it('should contain the correct text in the first paragraph before pasting', function () {
            firstParagraph = Position.getParagraphElement(model.getNode(), [0]);
            expect($(firstParagraph).text()).toBe(firstParaTextBefore);
            expect(Position.getParagraphNodeLength(firstParagraph)).toBe(firstParaTextBefore.length);
        });

        it('should replace theme colors with the fallback color for ODF documents after pasting', async function () {

            model.getSelection().setTextSelection([0, 6], [0, 9]); // selecting the word 'new'
            let textNode = Position.getSelectedElement(rootNode, [0, 6], 'span');
            let textAttrs = model.characterStyles.getElementAttributes(textNode);

            // before pasting the default should be 'auto' color
            expect(textAttrs.character.color).toBeObject();
            expect(textAttrs.character.color).toHaveProperty('type', 'auto');

            expect(textAttrs.character.fillColor).toBeObject();
            expect(textAttrs.character.fillColor).toHaveProperty('type', 'auto');

            clipboardOperations = clipboardOperations1;
            await model.pasteInternalClipboard();

            textNode = Position.getSelectedElement(rootNode, [0, 6], 'span');
            textAttrs = model.characterStyles.getElementAttributes(textNode);

            // after pasting the theme colors should have been replaced with RGB colors
            expect(textAttrs.character.color).toEqual(colorAfter);
            expect(textAttrs.character.fillColor).toEqual(fillColorAfter);

            expect($(firstParagraph).text()).toBe(firstParaTextAfter);
            expect(Position.getParagraphNodeLength(firstParagraph)).toBe(firstParaTextAfter.length);
        });

        it('should not have extended the RGB colors with a fallback color for ODF documents after copy', function () {
            model.getSelection().setTextSelection([0, 6], [0, 11]); // selecting the word '12345'

            // setting  'forceOperationGeneration' is required in test environment (Unit test and Testrunner), because there is
            // no event for the 'copy' function specified (the first parameter).
            model.copy(null, { forceOperationGeneration: true });

            const operations = model.getClipboardOperations();

            expect(operations).toBeArrayOfSize(2);

            // after copy the RGB colors should not have been extended with a fallback color
            expect(operations).not.toHaveProperty('[1].attrs.character.color.fallbackValue');
            expect(operations).not.toHaveProperty('[1].attrs.character.fillColor.fallbackValue');
        });
    });
});

// ========================================================================
//     Test OOXML specific stuff
// ========================================================================

describe('TextFramework class ClipBoardHandlerMixin III', function () {

    // private helpers ----------------------------------------------------

    var firstParagraph      = null;
    var firstParaTextBefore = 'Hello new world';
    var firstParaTextAfter  = 'Hello 12345 world';
    var secondParaText      = 'More text in paragraph';
    var pasteText           = '12345';

    var color     = Color.ACCENT4;
    var fillColor = Color.ACCENT5;

    var clipboardOperations  = null;
    var clipboardOperations1 = [{
        name: 'insertText',
        start: [0, 0],
        text: pasteText,
        attrs: { character: { color, fillColor } }
    }];

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', text: firstParaTextBefore, start: [0, 0] },
        { name: 'insertParagraph', start: [1] },
        { name: 'insertText', text: secondParaText, start: [1, 0] }
    ];

    var model    = null;
    var rootNode = null;
    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        model.getClipboardOperations = function () { return clipboardOperations; };
        model.setClipboardOperations = function (ops) { clipboardOperations = ops; };
        model.hasInternalClipboard = _.constant(true);
        model.checkSetClipboardPasteInProgress = _.constant(false);

        rootNode = model.getCurrentRootNode(model.getActiveTarget());
    });

    // public methods -----------------------------------------------------

    describe('method pasteInternalClipboard', function () {
        it('should exist', function () {
            expect(model).toRespondTo('pasteInternalClipboard');
        });

        it('should contain the correct text in the first paragraph before pasting', function () {
            firstParagraph = Position.getParagraphElement(model.getNode(), [0]);
            expect($(firstParagraph).text()).toBe(firstParaTextBefore);
            expect(Position.getParagraphNodeLength(firstParagraph)).toBe(firstParaTextBefore.length);
        });

        it('should not have modified the theme colors for OOXML documents after pasting', async function () {

            model.getSelection().setTextSelection([0, 6], [0, 9]); // selecting the word 'new'
            let textNode = Position.getSelectedElement(rootNode, [0, 6], 'span');
            let textAttrs = model.characterStyles.getElementAttributes(textNode);

            // before pasting the default should be 'auto' color
            expect(textAttrs.character.color).toBeObject();
            expect(textAttrs.character.color).toHaveProperty('type', 'auto');

            expect(textAttrs.character.fillColor).toBeObject();
            expect(textAttrs.character.fillColor).toHaveProperty('type', 'auto');

            clipboardOperations = clipboardOperations1;
            await model.pasteInternalClipboard();

            textNode = Position.getSelectedElement(rootNode, [0, 6], 'span');
            textAttrs = model.characterStyles.getElementAttributes(textNode);

            // after pasting the theme colors should be unchanged
            expect(textAttrs.character.color).toEqual(color);
            expect(textAttrs.character.fillColor).toEqual(fillColor);

            expect($(firstParagraph).text()).toBe(firstParaTextAfter);
            expect(Position.getParagraphNodeLength(firstParagraph)).toBe(firstParaTextAfter.length);
        });

        it('should have extended the theme colors with the fallback color for OOXML documents after copy', function () {

            model.getSelection().setTextSelection([0, 6], [0, 11]); // selecting the word '12345'

            // setting  'forceOperationGeneration' is required in test environment (Unit test and Testrunner), because there is
            // no event for the 'copy' function specified (the first parameter).
            model.copy(null, { forceOperationGeneration: true });

            const operations = model.getClipboardOperations();

            expect(operations).toBeArrayOfSize(2); // docs-2323, character attributes are no longer copy & pasted at the paragraph

            // after copy the theme colors should have been extended with the fallback color
            expect(operations).toHaveProperty('[1].attrs.character.color.fallbackValue');
            expect(operations[1].attrs.character.color.fallbackValue).toMatch(/./);
            expect(operations).toHaveProperty('[1].attrs.character.fillColor.fallbackValue');
            expect(operations[1].attrs.character.fillColor.fallbackValue).toMatch(/./);
        });
    });
});
