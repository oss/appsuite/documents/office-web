/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { Color } from '@/io.ox/office/editframework/utils/color';
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';

import ListHandlerMixin from '@/io.ox/office/textframework/model/listhandlermixin';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import { DEFAULT_LIST_STYLE_ID } from '@/io.ox/office/textframework/utils/listutils';

import { waitForEvent } from '~/asynchelper';
import { createPresentationApp } from '~/presentation/apphelper';

// mix-in class UpdateListsMixin ==========================================

describe('Textframework mix-in class ListHandlerMixin', function () {

    // private helpers ----------------------------------------------------

    var layoutId_1 = 'layout1',
        masterId_1 = 'master1',
        slideDefaultName = 'slide_1',
        firstStandardDrawing = null, // the first drawing in the standard slide
        firstStandardParagraphs = null, // the paragraphs in the first drawing in the standard slide
        secondStandardDrawing = null, // the second drawing in the standard slide
        secondStandardParagraphs = null, // the paragraphs in the second drawing in the standard slide
        explicitAttributes = null,  // the explicit attributes of a paragraph
        textFrameDrawing = null, // the text frame drawing in the standard slide
        textFrameParagraphs = null, // the paragraphs in the text frame drawing in the standard slide
        para1 = null, // the first paragraph in second drawing on standard slide
        para2 = null, // the second paragraph in second drawing on standard slide
        para3 = null, // the third paragraph in second drawing on standard slide
        para4 = null, // the fourth paragraph in second drawing on standard slide
        para5 = null, // the fifth paragraph in second drawing on standard slide
        para6 = null, // the sixth paragraph in second drawing on standard slide

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' },
                    defaultTextListStyles: {
                        l1: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 0 } },
                        l2: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 1270 } },
                        l3: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 2540 } }
                    }
                }
            },
            {
                name: 'insertMasterSlide', id: masterId_1, attrs: {
                    listStyles: {
                        title: {
                            l1: {
                                character: { fontSize: 44, fontName: '+mj-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'percent', value: 0 }, defaultTabSize: 2540, bullet: { type: 'none' }, alignment: 'left' }
                            }
                        },
                        body: {
                            l1: {
                                character: { fontSize: 28, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 35 }, defaultTabSize: 2540, bullet: { type: 'character', character: '-' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 635, indentFirstLine: -634 }
                            },
                            l2: {
                                character: { fontSize: 24, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 1905, indentFirstLine: -634 }
                            },
                            l3: {
                                character: { fontSize: 20, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 3175, indentFirstLine: -634 }
                            },
                            l4: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 4445, indentFirstLine: -634 }
                            },
                            l5: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 5715, indentFirstLine: -634 }
                            },
                            l6: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 6985, indentFirstLine: -634 }
                            },
                            l7: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 8255, indentFirstLine: -634 }
                            },
                            l8: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 9525, indentFirstLine: -634 }
                            },
                            l9: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 10795, indentFirstLine: -634 }
                            }
                        },
                        other: {
                            l1: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 0 }
                            },
                            l2: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 1270 }
                            },
                            l3: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 2540 }
                            },
                            l4: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 3810 }
                            },
                            l5: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 5080 }
                            },
                            l6: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 6350 }
                            },
                            l7: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 7620 }
                            },
                            l8: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 8890 }
                            },
                            l9: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 10160 }
                            }
                        }
                    }
                }
            },
            // { name: 'insertDrawing', start: [0, 0], target: masterId_1, type: 'shape', attrs: { presentation: { phType: 'title' }, shape: { anchor: 'centered', paddingLeft: 254, paddingRight: 254, paddingTop: 127, paddingBottom: 127 }, drawing: { name: 'T1', left: 2328, top: 1014, width: 29210, height: 3682 }, geometry: { presetShape: 'rect', avList: {} }}},
            // { name: 'insertParagraph', start: [0, 0, 0], target: masterId_1 },
            // { name: 'insertText', start: [0, 0, 0, 0], target: masterId_1, text: 'Master title format' },
            // { name: 'insertDrawing', start: [0, 1], target: masterId_1, type: 'shape', attrs: { presentation: { phType: 'body', phIndex: 1 }, drawing: { name: 'T2', left: 2328, top: 5071, width: 29210, height: 12087 }, geometry: { presetShape: 'rect', avList: {} }}},
            // { name: 'insertParagraph', start: [0, 1, 0], target: masterId_1, attrs: { paragraph: { level: 0 }}},
            // { name: 'insertText', start: [0, 1, 0, 0], target: masterId_1,text: 'Format templates' },
            // { name: 'insertParagraph', start: [0, 1, 1], target: masterId_1, attrs: { paragraph: { level: 1 }}},
            // { name: 'insertText', start: [0, 1, 1, 0], target: masterId_1,text: 'Second level' },
            // { name: 'insertParagraph', start: [0, 1, 2], target: masterId_1, attrs: { paragraph: { level: 2 }}},
            // { name: 'insertText', start: [0, 1, 2, 0], target: masterId_1,text: 'Third level' },
            // { name: 'insertParagraph', start: [0, 1, 3], target: masterId_1, attrs: { paragraph: { level: 3 }}},
            // { name: 'insertText', start: [0, 1, 3, 0], target: masterId_1, text: 'Fourth level' },
            // { name: 'insertParagraph', start: [0, 1, 4], target: masterId_1, attrs: { paragraph: { level: 4 }}},
            // { name: 'insertText', start: [0, 1, 4, 0], target: masterId_1, text: 'Fifth level' },
            // { name: 'insertDrawing', start: [0, 2],target: masterId_1, type: 'shape', attrs: { presentation: { phType: 'dt', phSize: 'half', phIndex: 2 }, listStyle: { l1: { character: { fontSize: 12, type: 'solid', color: opSchemeColor('text1', { tint: 75000 }) }, paragraph: { alignment: 'left' }}}, shape: { anchor: 'centered', paddingLeft: 254, paddingRight: 254, paddingTop: 127, paddingBottom: 127 }, drawing: { name: 'D3', left: 2328, top: 17657, width: 7620, height: 1014 }, geometry: { presetShape: 'rect', avList: {} }}},
            // { name: 'insertParagraph', start: [0, 2, 0], target: masterId_1 },
            // { name: 'insertDrawing', start: [0, 3], target: masterId_1, type: 'shape', attrs: { presentation: { phType: 'ftr', phSize: 'quarter', phIndex: 3 }, listStyle: { l1: { character: { fontSize: 12, type: 'solid', color: opSchemeColor('text1', { tint: 75000 }) }, paragraph: { alignment: 'center' }}}, shape: { anchor: 'centered', paddingLeft: 254, paddingRight: 254, paddingTop: 127, paddingBottom: 127 }, drawing: { name: 'F4', left: 11218, top: 17657, width: 11430, height: 1014 }, geometry: { presetShape: 'rect', avList: {} }}},
            // { name: 'insertParagraph', start: [0, 3, 0], target: masterId_1 },
            // { name: 'insertDrawing', start: [0, 4], target: masterId_1, type: 'shape', attrs: { presentation: { phType: 'sldNum', phSize: 'quarter', phIndex: 4 }, listStyle: { l1: { character: { fontSize: 12, type: 'solid', color: opSchemeColor('text1', { tint: 75000 }) }, paragraph: { alignment: 'right' }}}, shape: { anchor: 'centered', paddingLeft: 254, paddingRight: 254, paddingTop: 127, paddingBottom: 127 }, drawing: { name: 'F5', left: 23918, top: 17657, width: 7620, height: 1014 }, geometry: { presetShape: 'rect', avList: {} }}},
            // { name: 'insertParagraph', start: [0, 4, 0], target: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            // { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'title' }, drawing: { name: 'Title 1' }}},
            // { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            // { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: 'Title master format' },
            // { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phIndex: 1 }, drawing: { name: 'I2' }}},
            // { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1, attrs: { paragraph: { level: 0 }}},
            // { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_1, text: 'Format templates' },
            // { name: 'insertParagraph', start: [0, 1, 1], target: layoutId_1, attrs: { paragraph: { level: 1 }}},
            // { name: 'insertText', start: [0, 1, 1, 0], target: layoutId_1, text: 'Second level' },
            // { name: 'insertParagraph', start: [0, 1, 2], target: layoutId_1, attrs: { paragraph: { level: 2 }}},
            // { name: 'insertText', start: [0, 1, 2, 0], target: layoutId_1, text: 'Third level' },
            // { name: 'insertParagraph', start: [0, 1, 3], target: layoutId_1, attrs: { paragraph: { level: 3 }}},
            // { name: 'insertText', start: [0, 1, 3, 0], target: layoutId_1, text: 'Fourth level' },
            // { name: 'insertParagraph', start: [0, 1, 4], target: layoutId_1, attrs: { paragraph: { level: 4 }}},
            // { name: 'insertText', start: [0, 1, 4, 0], target: layoutId_1, text: 'Fifth level' },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'dt', phSize: 'half', phIndex: 10 }, drawing: { name: 'D3' } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ftr', phSize: 'quarter', phIndex: 11 }, drawing: { name: 'F4' } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1 },
            { name: 'insertDrawing', start: [0, 2], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'sldNum', phSize: 'quarter', phIndex: 12 }, drawing: { name: 'F5' } } },
            { name: 'insertParagraph', start: [0, 2, 0], target: layoutId_1 },
            { name: 'insertSlide', start: 0, target: layoutId_1 },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { phType: 'title' }, drawing: { name: 'Title 1' } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertText', start: [0, 0, 0, 0], text: 'Title' },
            { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { presentation: { phIndex: 1 }, drawing: { name: 'I2' } } },
            { name: 'insertParagraph', start: [0, 1, 0] },
            { name: 'insertText', start: [0, 1, 0, 0], text: 'Item 1' },
            { name: 'insertParagraph', start: [0, 1, 1] },
            { name: 'insertText', start: [0, 1, 1, 0], text: 'Item 2' },
            { name: 'insertParagraph', start: [0, 1, 2] },
            { name: 'insertText', start: [0, 1, 2, 0], text: 'Item 3' },
            { name: 'insertParagraph', start: [0, 1, 3] },
            { name: 'insertText', start: [0, 1, 3, 0], text: 'Item 4' },
            { name: 'insertParagraph', start: [0, 1, 4] },
            { name: 'insertText', start: [0, 1, 4, 0], text: 'Item 5' },
            { name: 'insertParagraph', start: [0, 1, 5] },
            { name: 'insertText', start: [0, 1, 5, 0], text: 'Item 6' },
            { name: 'insertDrawing', attrs: { drawing: { width: 8000, left: 5000, top: 3000 }, shape: { autoResizeHeight: true }, line: { color: Color.BLACK, style: 'single', type: 'none', width: 26 }, fill: { color: Color.AUTO, type: 'solid' } }, start: [0, 2], type: 'shape' },
            { name: 'insertParagraph', start: [0, 2, 0] },
            { name: 'insertText', start: [0, 2, 0, 0], text: 'Text frame text in paragraph 1' },
            { name: 'insertParagraph', start: [0, 2, 1] },
            { name: 'insertText', start: [0, 2, 1, 0], text: 'Text frame text in paragraph 2' },
            { name: 'insertParagraph', start: [0, 2, 2] },
            { name: 'insertText', start: [0, 2, 2, 0], text: 'Text frame text in paragraph 3' }
        ];

    var model;
    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(ListHandlerMixin).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('method isStandardSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isStandardSlideId');
        });
        it('should return whether the specified ID is the ID of a normal (standard) slide', function () {
            expect(model.isStandardSlideId(layoutId_1)).toBeFalse();
            expect(model.isStandardSlideId(masterId_1)).toBeFalse();
            expect(model.isStandardSlideId(slideDefaultName)).toBeTrue();
        });
    });

    describe('method getActiveSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getActiveSlideId');
        });
        it('should return the ID of the active slide', function () {
            expect(model.getActiveSlideId()).toBe(slideDefaultName);
        });
    });

    describe('method DOM.isListParagraphNode', function () {
        it('should exist', function () {
            expect(DOM.isListParagraphNode).toBeFunction();
        });
        it('should check whether the specified node is a list paragraph', function () {
            model.getSelection().setTextSelection([0, 0, 0, 0]);
            firstStandardDrawing = model.getSelection().getSelectedTextFrameDrawing();
            expect(firstStandardDrawing).toHaveLength(1);
            firstStandardParagraphs = firstStandardDrawing.find(DOM.PARAGRAPH_NODE_SELECTOR);
            expect(firstStandardParagraphs).toHaveLength(1);
            model.getSelection().setTextSelection([0, 1, 0, 0]);
            secondStandardDrawing = model.getSelection().getSelectedTextFrameDrawing();
            expect(secondStandardDrawing).toHaveLength(1);
            secondStandardParagraphs = secondStandardDrawing.find(DOM.PARAGRAPH_NODE_SELECTOR);
            expect(secondStandardParagraphs).toHaveLength(6);
            expect(DOM.isListParagraphNode(firstStandardParagraphs[0])).toBeFalse();
            expect(DOM.isListParagraphNode(secondStandardParagraphs[0])).toBeTrue();
        });
    });

    describe('method DOM.isListLabelNode', function () {
        it('should exist', function () {
            expect(DOM.isListLabelNode).toBeFunction();
        });
        it('should check whether the specified node is a list label in a list paragraph', function () {
            expect(DOM.isListLabelNode(firstStandardParagraphs[0].childNodes[0])).toBeFalse();
            expect(DOM.isListLabelNode(secondStandardParagraphs[0].childNodes[0])).toBeTrue();
        });
    });

    describe('method isListIndentChangeable', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isListIndentChangeable');
        });
        it('should check whether the list level at the currently selected paragraph is changeable', function () {
            model.getSelection().setTextSelection([0, 0, 0, 0]);
            expect(model.isListIndentChangeable()).toBeFalse();
            model.getSelection().setTextSelection([0, 1, 0, 0]);
            expect(model.isListIndentChangeable()).toBeTrue();
        });
    });

    describe('method getMaxListLevel', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getMaxListLevel');
        });
        it('should receive the maximum list level for the drawing that contains the selection', function () {
            model.getSelection().setTextSelection([0, 0, 0, 0]);
            expect(model.getMaxListLevel()).toBe(0);
            model.getSelection().setTextSelection([0, 1, 0, 0]);
            expect(model.getMaxListLevel()).toBe(8);
        });
    });

    describe('method changeListIndent', function () {
        it('should exist', function () {
            expect(model).toRespondTo('changeListIndent');
        });
        it('should change the level of list indent for the specified paragraph', function () {

            expect($(firstStandardParagraphs[0].childNodes[0]).text()).toBe("Title");

            expect(secondStandardParagraphs[0].childNodes).toHaveLength(2);
            expect($(secondStandardParagraphs[0].childNodes[0]).text()).toBe("-");
            expect($(secondStandardParagraphs[0].childNodes[1]).text()).toBe("Item 1");

            model.getSelection().setTextSelection([0, 0, 0, 0]);

            explicitAttributes = getExplicitAttributeSet(firstStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph).toBeUndefined();

            model.changeListIndent(); // increasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(firstStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph).toBeUndefined();  // no effect in title

            model.changeListIndent({ increase: false }); // decreasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(firstStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph).toBeUndefined();  // no effect in title

            model.getSelection().setTextSelection([0, 1, 0, 0]);

            model.changeListIndent(); // increasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(secondStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.level).toBe(1);

            model.changeListIndent(); // increasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(secondStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.level).toBe(2);

            model.changeListIndent({ increase: false }); // decreasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(secondStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.level).toBe(1);

            model.changeListIndent({ increase: false }); // decreasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(secondStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.level).toBe(0);

        });
    });

    describe('method setListStyleId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('setListStyleId');
        });
        it('should set the specified list style to the selected paragraph', function () {

            model.getSelection().setTextSelection([0, 0, 0, 0]);

            explicitAttributes = getExplicitAttributeSet(firstStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph).toBeUndefined();

            model.setListStyleId('bullet', DEFAULT_LIST_STYLE_ID, '');

            explicitAttributes = getExplicitAttributeSet(firstStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("character");

            model.setListStyleId('bullet', DEFAULT_LIST_STYLE_ID, ListHandlerMixin.DEFAULT_BULLET_LISTSTYLE);

            explicitAttributes = getExplicitAttributeSet(firstStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("none");
        });
    });

    describe('method getListStyleId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getListStyleId');
        });
        it('should get the list style to a selected paragraph', function () {

            var newListStyleId = 'L20014';

            model.getSelection().setTextSelection([0, 0, 0, 0]);

            explicitAttributes = getExplicitAttributeSet(firstStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("none");

            model.setListStyleId('bullet', newListStyleId, '');

            explicitAttributes = getExplicitAttributeSet(firstStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("character");

            expect(model.getListStyleId('bullet', explicitAttributes.paragraph)).toBe(newListStyleId);
        });
    });

    describe('method getMaxListLevel #2', function () {
        it('should receive the maximum list level of a paragraph inside a text frame', function () {
            model.getSelection().setTextSelection([0, 2, 0, 0]);
            expect(model.getMaxListLevel()).toBe(2);
        });
    });

    describe('method setListStyleId #2', function () {
        it('should set the default numbering list style over all selected paragraphs', function () {

            model.getSelection().setTextSelection([0, 2, 0, 0], [0, 2, 2, 2]); // selecting three paragraphs

            textFrameDrawing = model.getSelection().getSelectedTextFrameDrawing();
            expect(textFrameDrawing).toHaveLength(1);
            textFrameParagraphs = textFrameDrawing.find(DOM.PARAGRAPH_NODE_SELECTOR);
            expect(textFrameParagraphs).toHaveLength(3);

            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[0], true);
            expect(explicitAttributes.paragraph).toBeUndefined();
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[1], true);
            expect(explicitAttributes.paragraph).toBeUndefined();
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[2], true);
            expect(explicitAttributes.paragraph).toBeUndefined();

            model.setListStyleId('numbering', ListHandlerMixin.DEFAULT_NUMBERING_TYPE, '');

            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[0], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(ListHandlerMixin.DEFAULT_NUMBERING_TYPE);
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[1], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(ListHandlerMixin.DEFAULT_NUMBERING_TYPE);
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[2], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(ListHandlerMixin.DEFAULT_NUMBERING_TYPE);
        });

        it('should expand the selection of a new numbering list style automatically', function () {

            var newNumberingListStyle = 'arabicParenBoth';

            model.getSelection().setTextSelection([0, 2, 1, 1], [0, 2, 1, 2]); // selecting only one paragraph

            model.setListStyleId('numbering', newNumberingListStyle, ListHandlerMixin.DEFAULT_NUMBERING_TYPE);

            // all three paragraphs have the new numbering style assigned
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[0], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(newNumberingListStyle);
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[1], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(newNumberingListStyle);
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[2], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(newNumberingListStyle);
        });

        it('should not expand the selection automatically, if more than one paragraph is selected', function () {

            var oldNumberingListStyle = 'arabicParenBoth',
                newNumberingListStyle = 'romanLcPeriod';

            model.getSelection().setTextSelection([0, 2, 1, 1], [0, 2, 2, 2]); // selecting two paragraphs

            model.setListStyleId('numbering', newNumberingListStyle, oldNumberingListStyle);

            // all three paragraphs have the new numbering style assigned
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[0], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(oldNumberingListStyle);  // old style remains
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[1], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(newNumberingListStyle);
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[2], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(newNumberingListStyle);
        });
    });

    describe('method changeListIndent #2', function () {
        it('should find the correct numbering type after increase and decrease of list level', function () {

            var oldNumberingListStyle = 'arabicParenBoth',
                newNumberingListStyle = 'romanLcPeriod';

            model.getSelection().setTextSelection([0, 2, 0, 0]);  // selecting the first paragraph

            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[0], true);
            expect(explicitAttributes.paragraph.bullet.numType).toBe(oldNumberingListStyle); // old value

            model.changeListIndent(); // increasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[0], true);
            expect(explicitAttributes.paragraph.bullet.numType).toBe(oldNumberingListStyle); // still old value

            model.changeListIndent({ increase: false }); // decreasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[0], true);
            expect(explicitAttributes.paragraph.bullet.numType).toBe(newNumberingListStyle); // new value
        });

        it('should not modify the numbering type after increase and decrease of list level, if character bullet list follows', function () {

            var oldNumberingListStyle = 'arabicParenBoth',
                newNumberingListStyle = 'romanLcPeriod',
                bulletListStyleId = 'L20014';

            model.getSelection().setTextSelection([0, 2, 1, 0]);  // selecting the second paragraph
            model.setListStyleId('bullet', bulletListStyleId, ''); // second paragraph gets character bullet list
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[1], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("character");

            model.getSelection().setTextSelection([0, 2, 0, 0]);  // selecting the first paragraph
            model.setListStyleId('numbering', oldNumberingListStyle, newNumberingListStyle);  // setting list style to 'oldNumberingListStyle'
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[0], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(oldNumberingListStyle);

            // now increasing and decreasing level of third paragraph
            model.getSelection().setTextSelection([0, 2, 2, 0]);  // selecting the third paragraph
            model.changeListIndent(); // increasing indent of third paragraph
            model.changeListIndent({ increase: false }); // decreasing indent of third paragraph

            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[2], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(newNumberingListStyle);  // -> not switched to oldNumberingListStyle
        });

        it('should modify the numbering type after increase and decrease of list level, if no bullet list follows', function () {

            var oldNumberingListStyle = 'arabicParenBoth',
                newNumberingListStyle = 'romanLcPeriod';

            model.getSelection().setTextSelection([0, 2, 1, 0]);  // selecting the second paragraph
            model.setListStyleId('numbering', oldNumberingListStyle, ''); // second paragraph gets numbering list again
            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[1], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");

            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[2], true);  // third paragraph
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(newNumberingListStyle);  // -> check of existing numbering list style

            // now increasing and decreasing level of third paragraph
            model.getSelection().setTextSelection([0, 2, 2, 0]);  // selecting the third paragraph
            model.changeListIndent(); // increasing indent of third paragraph
            model.changeListIndent({ increase: false }); // decreasing indent of third paragraph

            explicitAttributes = getExplicitAttributeSet(textFrameParagraphs[2], true);
            expect(explicitAttributes.paragraph.bullet.type).toBe("numbering");
            expect(explicitAttributes.paragraph.bullet.numType).toBe(oldNumberingListStyle);  // -> switched to oldNumberingListStyle
        });
    });

    // checks with empty paragraphs (using asynchronous list update via 'updateListsDebounced')

    describe('method updateLists', function () {

        it('should exist', function () {
            expect(model).toRespondTo('updateLists');
        });

        it('should have a correct prepared drawing on standard slide with 6 paragraphs', function () {

            expect(secondStandardParagraphs).toHaveLength(6);

            para1 = $(secondStandardParagraphs[0]);
            para2 = $(secondStandardParagraphs[1]);
            para3 = $(secondStandardParagraphs[2]);
            para4 = $(secondStandardParagraphs[3]);
            para5 = $(secondStandardParagraphs[4]);
            para6 = $(secondStandardParagraphs[5]);

            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeTrue();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
        });
    });

    describe('method isNumberedListParagraph', function () {

        it('should exist', function () {
            expect(model).toRespondTo('isNumberedListParagraph');
        });

        it('should check that paragraphs are correctly switched from bullet list to numbered list', async function () {

            await waitForEvent(model, 'listformatting:done', () => {

                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para1).paragraph)).toBeFalse();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para2).paragraph)).toBeFalse();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para3).paragraph)).toBeFalse();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para4).paragraph)).toBeFalse();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para5).paragraph)).toBeFalse();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para6).paragraph)).toBeFalse();

                model.getSelection().setTextSelection([0, 1, 0, 0], [0, 1, 5, 1]); // selecting all six paragraphs

                model.setListStyleId('numbering', ListHandlerMixin.DEFAULT_NUMBERING_TYPE, ''); // setting numbering list style to all paragraphs

                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para1).paragraph)).toBeTrue();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para2).paragraph)).toBeTrue();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para3).paragraph)).toBeTrue();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para4).paragraph)).toBeTrue();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para5).paragraph)).toBeTrue();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para6).paragraph)).toBeTrue();

                // debounced list update -> synchronous check: still the old list bullets
                expect(para1.children().first().text()).toBe("-");
                expect(para2.children().first().text()).toBe("-");
                expect(para3.children().first().text()).toBe("-");
                expect(para4.children().first().text()).toBe("-");
                expect(para5.children().first().text()).toBe("-");
                expect(para6.children().first().text()).toBe("-");
            });

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4.");
            expect(para5.children().first().text()).toBe("5.");
            expect(para6.children().first().text()).toBe("6.");
        });
    });

    describe('empty list paragraphs', function () {

        it('should not be counted in numbered lists', async function () {

            model.getSelection().setTextSelection([0, 1, 1, 0], [0, 1, 1, 6]); // selecting all content of second paragraph

            await waitForEvent(model, 'listformatting:done', () => model.deleteSelected());

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // empty paragraph gets number increased by 1, but stops further increase
            expect(para3.children().first().text()).toBe("2.");
            expect(para4.children().first().text()).toBe("3.");
            expect(para5.children().first().text()).toBe("4.");
            expect(para6.children().first().text()).toBe("5.");
        });

        it('should have the class attributes that handle the opacity, if the selection is inside the paragraph', function () {
            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeTrue();
            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS)).toBeTrue();
        });

        it('should not have the class attributes that handle the opacity, if the selection is outside the paragraph', function () {
            model.getSelection().setTextSelection([0, 1, 0, 0]); // setting cursor into another paragraph
            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeTrue();
            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS)).toBeFalse();
        });

        it('should not be counted in numbered lists if text is deleted (preparation for undo)', async function () {

            model.getSelection().setTextSelection([0, 1, 3, 0], [0, 1, 3, 6]); // selecting all content of fourth paragraph

            await waitForEvent(model, 'listformatting:done', () => model.deleteSelected());

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // empty paragraph
            expect(para3.children().first().text()).toBe("2.");
            expect(para4.children().first().text()).toBe("3."); // empty paragraph
            expect(para5.children().first().text()).toBe("3.");
            expect(para6.children().first().text()).toBe("4.");

            expect(para4.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeTrue();
            expect(para4.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS)).toBeTrue();
        });

        it('should be counted in numbered lists if text is inserted via undo', async function () {

            await waitForEvent(model, 'listformatting:done', () => model.getUndoManager().undo());

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // empty paragraph
            expect(para3.children().first().text()).toBe("2.");
            expect(para4.children().first().text()).toBe("3."); // no longer empty paragraph
            expect(para5.children().first().text()).toBe("4.");
            expect(para6.children().first().text()).toBe("5.");

            expect(para4.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeFalse();
            expect(para4.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS)).toBeFalse();
        });

        it('should be counted in numbered lists if text is inserted into paragraph', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([0, 1, 1, 0]); // setting cursor into empty paragraph
                model.insertText('c', [0, 1, 1, 0]);  // inserting one character into empty paragraph
                model.getSelection().setTextSelection([0, 1, 1, 1]); // setting cursor behind the new text
            });

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // no longer empty paragraph
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4.");
            expect(para5.children().first().text()).toBe("5.");
            expect(para6.children().first().text()).toBe("6.");

            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeFalse();
            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS)).toBeFalse();
        });

        // additional checks for synchronous list update (calling model.updateLists() directly)

        it('should not be counted in numbered lists if text is deleted (synchronous list update)', async function () {

            model.getSelection().setTextSelection([0, 1, 1, 0], [0, 1, 1, 1]); // selecting all content of second paragraph

            await model.deleteSelected();

            // updating lists synchronously
            model.updateLists(null, secondStandardParagraphs);

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // empty paragraph
            expect(para3.children().first().text()).toBe("2.");
            expect(para4.children().first().text()).toBe("3.");
            expect(para5.children().first().text()).toBe("4.");
            expect(para6.children().first().text()).toBe("5.");

            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeTrue();
            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS)).toBeTrue();
        });

        it('should be counted in numbered lists if text is inserted into paragraph (synchronous list update)', function () {

            model.getSelection().setTextSelection([0, 1, 1, 0]); // setting cursor into empty paragraph
            model.insertText('c', [0, 1, 1, 0]);  // inserting one character into empty paragraph
            model.getSelection().setTextSelection([0, 1, 1, 1]); // setting cursor behind the new text

            // updating lists synchronously (this is done debounced during working with document)
            model.updateLists(null, secondStandardParagraphs);

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // no longer empty paragraph
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4.");
            expect(para5.children().first().text()).toBe("5.");
            expect(para6.children().first().text()).toBe("6.");

            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeFalse();
            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS)).toBeFalse();
        });

    });

});

// ========================================================================

describe('Presentation mix-in class ListHandlerMixin, ODF', function () {

    // private helpers ----------------------------------------------------

    var masterId_1 = 'master1',
        slideDefaultName = 'slide_1',
        firstStandardDrawing = null, // the first drawing in the standard slide
        firstStandardParagraphs = null, // the paragraphs in the first drawing in the standard slide
        secondStandardDrawing = null, // the second drawing in the standard slide
        secondStandardParagraphs = null, // the paragraphs in the second drawing in the standard slide
        explicitAttributes = null,  // the explicit attributes of a paragraph
        para1 = null, // the first paragraph in second drawing on standard slide
        para2 = null, // the second paragraph in second drawing on standard slide
        para3 = null, // the third paragraph in second drawing on standard slide
        para4 = null, // the fourth paragraph in second drawing on standard slide
        para5 = null, // the fifth paragraph in second drawing on standard slide
        para6 = null, // the sixth paragraph in second drawing on standard slide
        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { marginRight: 0, width: 28000, marginBottom: 0, marginTop: 0, marginLeft: 0, height: 21000 }
                }
            },
            {
                name: 'insertMasterSlide', id: masterId_1, attrs: {
                    listStyles: {
                        title: {
                            l1: { paragraph: { bulletFont: { followText: false, name: 'StarSymbol' }, alignment: 'center', indentLeft: 600 }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 44, fontSizeAsian: 44, boldAsian: false, fontSizeComplex: 44, italicAsian: false } }
                        },
                        body: {
                            l1: { paragraph: { indentFirstLine: -600, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 500 }, bullet: { character: '-', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 500 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 32, fontSizeAsian: 32, boldAsian: false, fontSizeComplex: 32, italicAsian: false } },
                            l2: { paragraph: { indentFirstLine: 600, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l3: { paragraph: { indentFirstLine: 1200, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l4: { paragraph: { indentFirstLine: 1800, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l5: { paragraph: { indentFirstLine: 2400, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l6: { paragraph: { indentFirstLine: 3000, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l7: { paragraph: { indentFirstLine: 3600, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l8: { paragraph: { indentFirstLine: 4200, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l9: { paragraph: { indentFirstLine: 4800, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } }
                        }
                    },
                    fill: { type: 'solid' }
                }
            },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { phType: 'title' }, drawing: { top: 837, left: 1400, width: 25199, height: 3506 } }, target: masterId_1 },
            { name: 'insertParagraph', start: [0, 0, 0], target: masterId_1, attrs: { paragraph: { bullet: { type: 'none' } } } },
            { name: 'insertText', start: [0, 0, 0, 0], target: masterId_1, text: 'Click to edit Master title style' },
            { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { presentation: { phType: 'body' }, drawing: { top: 4914, left: 1400, width: 25199, height: 12179 } }, target: masterId_1 },
            { name: 'insertParagraph', start: [0, 1, 0], target: masterId_1, attrs: { paragraph: { level: 0 } } },
            { name: 'insertText', start: [0, 1, 0, 0], target: masterId_1, text: 'Edit Master text styles' },
            { name: 'insertParagraph', start: [0, 1, 1], target: masterId_1, attrs: { paragraph: { level: 1 } } },
            { name: 'insertText', start: [0, 1, 1, 0], target: masterId_1, text: 'Second level' },
            { name: 'insertSlide', start: [0], target: masterId_1, attrs: { slide: { isSlideNum: false, isDate: false, isFooter: false } } },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { userTransformed: true, phType: 'title' }, drawing: { top: 837, left: 1400, width: 25199, height: 3506 } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertText', start: [0, 0, 0, 0], text: 'Title' },
            { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { presentation: { phType: 'body' }, drawing: { top: 4914, left: 1400, width: 25199, height: 12179 } } },
            { name: 'insertParagraph', start: [0, 1, 0], attrs: { paragraph: { level: 0, listStyleId: 'L1' } } },
            { name: 'insertText', start: [0, 1, 0, 0], text: 'Item 1' },
            { name: 'insertParagraph', start: [0, 1, 1], attrs: { paragraph: { level: 0, listStyleId: 'L1' } } },
            { name: 'insertText', start: [0, 1, 1, 0], text: 'Item 2' },
            { name: 'insertParagraph', start: [0, 1, 2], attrs: { paragraph: { level: 0, listStyleId: 'L1' } } },
            { name: 'insertText', start: [0, 1, 2, 0], text: 'Item 3' },
            { name: 'insertParagraph', start: [0, 1, 3], attrs: { paragraph: { level: 0, listStyleId: 'L1' } } },
            { name: 'insertText', start: [0, 1, 3, 0], text: 'Item 4' },
            { name: 'insertParagraph', start: [0, 1, 4], attrs: { paragraph: { level: 0, listStyleId: 'L1' } } },
            { name: 'insertText', start: [0, 1, 4, 0], text: 'Item 5' },
            { name: 'insertParagraph', start: [0, 1, 5], attrs: { paragraph: { level: 0, listStyleId: 'L1' } } },
            { name: 'insertText', start: [0, 1, 5, 0], text: 'Item 6' }
        ];

    var model;
    createPresentationApp('odf', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(ListHandlerMixin).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('method isStandardSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isStandardSlideId');
        });
        it('should return whether the specified ID is the ID of a normal (standard) slide', function () {
            expect(model.isStandardSlideId(masterId_1)).toBeFalse();
            expect(model.isStandardSlideId(slideDefaultName)).toBeTrue();
        });
    });

    describe('method getActiveSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getActiveSlideId');
        });
        it('should return the ID of the active slide', function () {
            expect(model.getActiveSlideId()).toBe(slideDefaultName);
        });
    });

    describe('method DOM.isListParagraphNode', function () {
        it('should exist', function () {
            expect(DOM.isListParagraphNode).toBeFunction();
        });
        it('should check whether the specified node is a list paragraph', function () {
            model.getSelection().setTextSelection([0, 0, 0, 0]);
            firstStandardDrawing = model.getSelection().getSelectedTextFrameDrawing();
            expect(firstStandardDrawing).toHaveLength(1);
            firstStandardParagraphs = firstStandardDrawing.find(DOM.PARAGRAPH_NODE_SELECTOR);
            expect(firstStandardParagraphs).toHaveLength(1);
            model.getSelection().setTextSelection([0, 1, 0, 0]);
            secondStandardDrawing = model.getSelection().getSelectedTextFrameDrawing();
            expect(secondStandardDrawing).toHaveLength(1);
            secondStandardParagraphs = secondStandardDrawing.find(DOM.PARAGRAPH_NODE_SELECTOR);
            expect(secondStandardParagraphs).toHaveLength(6);
            expect(DOM.isListParagraphNode(firstStandardParagraphs[0])).toBeFalse();
            expect(DOM.isListParagraphNode(secondStandardParagraphs[0])).toBeTrue();
        });
    });

    describe('method DOM.isListLabelNode', function () {
        it('should exist', function () {
            expect(DOM.isListLabelNode).toBeFunction();
        });
        it('should check whether the specified node is a list label in a list paragraph', function () {
            expect(DOM.isListLabelNode(firstStandardParagraphs[0].childNodes[0])).toBeFalse();
            expect(DOM.isListLabelNode(secondStandardParagraphs[0].childNodes[0])).toBeTrue();
        });
    });

    describe('method isListIndentChangeable', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isListIndentChangeable');
        });
        it('should check whether the list level at the currently selected paragraph is changeable', function () {
            model.getSelection().setTextSelection([0, 0, 0, 0]);
            expect(model.isListIndentChangeable()).toBeFalse();
            model.getSelection().setTextSelection([0, 1, 0, 0]);
            expect(model.isListIndentChangeable()).toBeTrue();
        });
    });

    describe('method getMaxListLevel', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getMaxListLevel');
        });
        it('should receive the maximum list level for the drawing that contains the selection', function () {
            model.getSelection().setTextSelection([0, 0, 0, 0]);
            expect(model.getMaxListLevel()).toBe(0);
            model.getSelection().setTextSelection([0, 1, 0, 0]);
            expect(model.getMaxListLevel()).toBe(8);
        });
    });

    describe('method changeListIndent', function () {
        it('should exist', function () {
            expect(model).toRespondTo('changeListIndent');
        });
        it('should change the level of list indent for the specified paragraph', function () {

            expect($(firstStandardParagraphs[0].childNodes[0]).text()).toBe("Title");

            expect(secondStandardParagraphs[0].childNodes).toHaveLength(2);
            expect($(secondStandardParagraphs[0].childNodes[0]).text()).toBe("-");
            expect($(secondStandardParagraphs[0].childNodes[1]).text()).toBe("Item 1");

            model.getSelection().setTextSelection([0, 0, 0, 0]);

            explicitAttributes = getExplicitAttributeSet(firstStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph).toBeUndefined();

            model.changeListIndent(); // increasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(firstStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph).toBeUndefined();  // no effect in title

            model.changeListIndent({ increase: false }); // decreasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(firstStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph).toBeUndefined();  // no effect in title

            model.getSelection().setTextSelection([0, 1, 0, 0]);

            model.changeListIndent(); // increasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(secondStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.level).toBe(1);

            model.changeListIndent(); // increasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(secondStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.level).toBe(2);

            model.changeListIndent({ increase: false }); // decreasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(secondStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.level).toBe(1);

            model.changeListIndent({ increase: false }); // decreasing indent of first paragraph

            explicitAttributes = getExplicitAttributeSet(secondStandardParagraphs[0], true);
            expect(explicitAttributes.paragraph.level).toBe(0);

        });
    });

    // checks with empty paragraphs (using asynchronous list update via 'updateListsDebounced')

    describe('method updateLists', function () {

        it('should exist', function () {
            expect(model).toRespondTo('updateLists');
        });

        it('should have a correct prepared drawing on standard slide with 6 paragraphs', function () {

            expect(secondStandardParagraphs).toHaveLength(6);

            para1 = $(secondStandardParagraphs[0]);
            para2 = $(secondStandardParagraphs[1]);
            para3 = $(secondStandardParagraphs[2]);
            para4 = $(secondStandardParagraphs[3]);
            para5 = $(secondStandardParagraphs[4]);
            para6 = $(secondStandardParagraphs[5]);

            expect(DOM.isListParagraphNode(para1)).toBeTrue();
            expect(DOM.isListParagraphNode(para2)).toBeTrue();
            expect(DOM.isListParagraphNode(para3)).toBeTrue();
            expect(DOM.isListParagraphNode(para4)).toBeTrue();
            expect(DOM.isListParagraphNode(para5)).toBeTrue();
            expect(DOM.isListParagraphNode(para6)).toBeTrue();
        });
    });

    describe('method isNumberedListParagraph', function () {

        it('should exist', function () {
            expect(model).toRespondTo('isNumberedListParagraph');
        });

        it('should check that paragraphs are correctly switched from bullet list to numbered list', async function () {

            await waitForEvent(model, 'listformatting:done', () => {

                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para1).paragraph)).toBeFalse();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para2).paragraph)).toBeFalse();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para3).paragraph)).toBeFalse();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para4).paragraph)).toBeFalse();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para5).paragraph)).toBeFalse();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para6).paragraph)).toBeFalse();

                model.getSelection().setTextSelection([0, 1, 0, 0], [0, 1, 5, 1]); // selecting all six paragraphs

                model.setListStyleId('numbering', ListHandlerMixin.DEFAULT_NUMBERING_TYPE, ''); // setting numbering list style to all paragraphs

                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para1).paragraph)).toBeTrue();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para2).paragraph)).toBeTrue();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para3).paragraph)).toBeTrue();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para4).paragraph)).toBeTrue();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para5).paragraph)).toBeTrue();
                expect(model.isNumberedListParagraph(model.paragraphStyles.getElementAttributes(para6).paragraph)).toBeTrue();

                // debounced list update -> synchronous check: still the old list bullets
                expect(para1.children().first().text()).toBe("-");
                expect(para2.children().first().text()).toBe("-");
                expect(para3.children().first().text()).toBe("-");
                expect(para4.children().first().text()).toBe("-");
                expect(para5.children().first().text()).toBe("-");
                expect(para6.children().first().text()).toBe("-");
            });

            // checking state after formatting of list
            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2.");
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4.");
            expect(para5.children().first().text()).toBe("5.");
            expect(para6.children().first().text()).toBe("6.");
        });
    });

    describe('empty list paragraphs', function () {

        it('should be counted in numbered lists, not like in OOXML', async function () {

            model.getSelection().setTextSelection([0, 1, 1, 0], [0, 1, 1, 6]); // selecting all content of second paragraph

            await waitForEvent(model, 'listformatting:done', () => model.deleteSelected());

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // empty paragraph gets number increased by 1 and does NOT stop further increase
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4.");
            expect(para5.children().first().text()).toBe("5.");
            expect(para6.children().first().text()).toBe("6.");
        });

        it('should have the class attributes that mark the paragraph as empty list paragraph', function () {
            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeTrue();
        });

        it('should trigger list formatting, if the paragraph selection is changed', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([0, 1, 3, 0]); // selecting all content of second paragraph
            });

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // empty paragraph
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4."); // selected paragraph
            expect(para5.children().first().text()).toBe("5.");
            expect(para6.children().first().text()).toBe("6.");
        });

        it('should have not the class attributes that mark the selected paragraph as empty list paragraph', function () {
            var selectedParagraph = model.getSelectedParagraph();
            expect(selectedParagraph[0]).toBe(para4[0]);
            expect(para4.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeFalse();
        });

        it('should be counted in numbered lists if text is deleted (preparation for undo), not like in OOXML', async function () {

            model.getSelection().setTextSelection([0, 1, 3, 0], [0, 1, 3, 6]); // selecting all content of fourth paragraph

            await waitForEvent(model, 'listformatting:done', () => model.deleteSelected());

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // empty paragraph
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4."); // empty paragraph
            expect(para5.children().first().text()).toBe("5.");
            expect(para6.children().first().text()).toBe("6.");
        });

        it('should be counted in numbered lists if text is inserted via undo', async function () {

            await waitForEvent(model, 'listformatting:done', () => model.getUndoManager().undo());

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // empty paragraph
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4."); // no longer empty paragraph
            expect(para5.children().first().text()).toBe("5.");
            expect(para6.children().first().text()).toBe("6.");

            expect(para4.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeFalse();
        });

        it('should be counted in numbered lists if text is inserted into paragraph', async function () {

            await waitForEvent(model, 'listformatting:done', () => {
                model.getSelection().setTextSelection([0, 1, 1, 0]); // setting cursor into empty paragraph
                model.insertText('c', [0, 1, 1, 0]);  // inserting one character into empty paragraph
                model.getSelection().setTextSelection([0, 1, 1, 1]); // setting cursor behind the new text
            });

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // no longer empty paragraph
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4.");
            expect(para5.children().first().text()).toBe("5.");
            expect(para6.children().first().text()).toBe("6.");

            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeFalse();
        });

        // additional checks for synchronous list update (calling model.updateLists() directly)

        it('should be counted in numbered lists if text is deleted (synchronous list update), not like in OOXML', async function () {

            model.getSelection().setTextSelection([0, 1, 1, 0], [0, 1, 1, 1]); // selecting all content of second paragraph

            await model.deleteSelected();

            // updating lists synchronously
            model.updateLists(null, secondStandardParagraphs);

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // empty paragraph
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4.");
            expect(para5.children().first().text()).toBe("5.");
            expect(para6.children().first().text()).toBe("6.");

            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeTrue();
        });

        it('should be counted in numbered lists if text is inserted into paragraph (synchronous list update)', function () {

            model.getSelection().setTextSelection([0, 1, 1, 0]); // setting cursor into empty paragraph
            model.insertText('c', [0, 1, 1, 0]);  // inserting one character into empty paragraph
            model.getSelection().setTextSelection([0, 1, 1, 1]); // setting cursor behind the new text

            // updating lists synchronously (this is done debounced during working with document)
            model.updateLists(null, secondStandardParagraphs);

            expect(para1.children().first().text()).toBe("1.");
            expect(para2.children().first().text()).toBe("2."); // no longer empty paragraph
            expect(para3.children().first().text()).toBe("3.");
            expect(para4.children().first().text()).toBe("4.");
            expect(para5.children().first().text()).toBe("5.");
            expect(para6.children().first().text()).toBe("6.");

            expect(para2.hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)).toBeFalse();
        });

    });

});
