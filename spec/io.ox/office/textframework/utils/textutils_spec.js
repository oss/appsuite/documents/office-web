/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { indexOfValuesInsideTextSpan } from '@/io.ox/office/textframework/utils/textutils';

// static class TextUtils =================================================

describe('Text module TextUtils', function () {

    it('should exist', function () {
        expect(indexOfValuesInsideTextSpan).toBeFunction();
    });

    // static methods -----------------------------------------------------

    describe('method indexOfValuesInsideTextSpan', function () {
        it('should find the correct index in forward direction', function () {
            expect(indexOfValuesInsideTextSpan('Hello World', 0, [' ', 'W', 'd'])).toBe(5);
            expect(indexOfValuesInsideTextSpan('Hello World', 5, [' ', 'W', 'd'])).toBe(5);
            expect(indexOfValuesInsideTextSpan('Hello World', 0, [' ', 'H', 'W', 'd'])).toBe(0);
            expect(indexOfValuesInsideTextSpan('Hello World', 2, [' ', 'H', 'W', 'd'])).toBe(5);
            expect(indexOfValuesInsideTextSpan('Hello World', 6, [' ', 'H', 'W', 'd'])).toBe(6);
            expect(indexOfValuesInsideTextSpan('Hello World', 7, [' ', 'H', 'W', 'd'])).toBe(10);
            expect(indexOfValuesInsideTextSpan('Hello World', 10, [' ', 'H', 'W', 'd'])).toBe(10);
        });
        it('should find the correct index in backward direction', function () {
            expect(indexOfValuesInsideTextSpan('Hello World', 8, [' ', 'H', 'W', 'd'], { reverse: true })).toBe(6);
            expect(indexOfValuesInsideTextSpan('Hello World', 5, [' ', 'H', 'W', 'd'], { reverse: true })).toBe(5);
            expect(indexOfValuesInsideTextSpan('Hello World', 4, [' ', 'H', 'W', 'd'], { reverse: true })).toBe(0);
            expect(indexOfValuesInsideTextSpan('Hello World', 8, [' ', 'H', 'W', 'd'], { reverse: true })).toBe(6);
            expect(indexOfValuesInsideTextSpan('Hello World', 10, [' ', 'H', 'W', 'd'], { reverse: true })).toBe(10);
            expect(indexOfValuesInsideTextSpan('Hello World', 20, [' ', 'H', 'W', 'd'], { reverse: true })).toBe(10);
        });
        it('should find no valid index in forward direction', function () {
            expect(indexOfValuesInsideTextSpan('Hello World', 8, [' ', 'H', 'W'])).toBe(-1);
            expect(indexOfValuesInsideTextSpan('Hello World', 20, [' ', 'H', 'W'])).toBe(-1);
            expect(indexOfValuesInsideTextSpan('Hello World', 0, ['A', 'B', 'C', 'h', 'w'])).toBe(-1);
        });
        it('should find no valid index in backward direction', function () {
            expect(indexOfValuesInsideTextSpan('Hello World', 4, [' ', 'W'], { reverse: true })).toBe(-1);
            expect(indexOfValuesInsideTextSpan('Hello World', 10, ['A', 'B', 'C', 'h', 'w'], { reverse: true })).toBe(-1);
        });
    });
});
