/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { createTextApp } from '~/text/apphelper';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Position from '@/io.ox/office/textframework/utils/position';

// static class Position ==================================================

describe('Text module Position', function () {

    // private helpers ----------------------------------------------------

    var paraNode1 = $('<div>').addClass('p'),
        paraNode2 = $('<div>').addClass('p'),
        span1 = $('<span>').text('Hello'),
        span2 = $('<span>').text(' World'),
        span3 = $('<span>').text(' Test'),
        span4 = $('<span>').text('Hello'),
        span5 = $('<span>').text(' World'),
        span6 = $('<span>').text('Test'),
        listLabel1 = $('<div>').addClass('helper list-label').append($('<span>').text('123.)')),
        hardBreak = $('<div>').addClass('inline hardbreak'),
        para2Text = 'Hello Earth';

    paraNode1.append(span1);
    paraNode1.append(span2);
    paraNode1.append(span3);

    paraNode2.append(listLabel1);
    paraNode2.append(span4);
    paraNode2.append(span5);
    paraNode2.append(hardBreak);
    paraNode2.append(span6);

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 } } },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', start: [0, 0], text: 'Hello World!' },
        { name: 'insertRange', start: [0, 2], id: 'cmt0', type: 'comment', position: 'start' },
        { name: 'insertRange', start: [0, 5], id: 'cmt0', type: 'comment', position: 'end' },
        { name: 'insertComment', start: [0, 6], author: 'a b', date: '2015-11-28T22:10:00Z', /*userId: 111, */ id: 'cmt0' },
        { name: 'insertParagraph', start: [1] },
        { name: 'insertText', start: [1, 0], text: para2Text }
    ];

    var model, rootNode;
    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        rootNode = model.getNode();
    });

    // static methods -----------------------------------------------------

    describe('method appendNewIndex', function () {
        it('should exist', function () {
            expect(Position.appendNewIndex).toBeFunction();
        });
        it('should append index to logical position', function () {
            expect(Position.appendNewIndex([1, 0], 7)).toEqual([1, 0, 7]);
        });
    });

    describe('method increaseLastIndex', function () {
        it('should exist', function () {
            expect(Position.increaseLastIndex).toBeFunction();
        });
        it('should increase the last index with default value', function () {
            expect(Position.increaseLastIndex([1, 0])).toEqual([1, 1]);
        });
        it('should increase the last index with specified value', function () {
            expect(Position.increaseLastIndex([1, 0], 3)).toEqual([1, 3]);
        });
    });

    describe('method decreaseLastIndex', function () {
        it('should exist', function () {
            expect(Position.decreaseLastIndex).toBeFunction();
        });
        it('should decrease the last index with default value', function () {
            expect(Position.decreaseLastIndex([1, 5])).toEqual([1, 4]);
        });
        it('should decrease the last index with specified value', function () {
            expect(Position.decreaseLastIndex([1, 5], 3)).toEqual([1, 2]);
        });
        it('should not decrease the last index to negative values', function () {
            expect(Position.decreaseLastIndex([1, 1], 3)).toEqual([1, 0]);
        });
    });

    describe('method getOxoPosition', function () {
        it('should exist', function () {
            expect(Position.getOxoPosition).toBeFunction();
        });
        it('should find the correct span position inside a paragraph', function () {
            expect(Position.getOxoPosition(paraNode1, span2)).toEqual([5]);
            expect(Position.getOxoPosition(paraNode1, span3)).toEqual([11]);
        });
        it('should find the correct span position inside a paragraph with offset', function () {
            expect(Position.getOxoPosition(paraNode1, span2, 3)).toEqual([8]);
        });
        it('should not fail even with invalid offset inside node', function () {
            expect(Position.getOxoPosition(paraNode1, span3, 9)).toEqual([20]);
        });
        it('should find the correct span position and ignore list labels', function () {
            expect(Position.getOxoPosition(paraNode2, span5, 2)).toEqual([7]);
        });
        it('should find the correct span position and use length 1 for hard break nodes', function () {
            expect(Position.getOxoPosition(paraNode2, span6, 2)).toEqual([14]);
        });
    });

    describe('method getDOMPosition', function () {
        it('should exist', function () {
            expect(Position.getDOMPosition).toBeFunction();
        });
        it('should find the correct dom point position inside a paragraph', function () {
            const pos = Position.getDOMPosition(paraNode1, [8]);
            expect(pos).toBeObject();
            expect(pos).toHaveProperty('offset', 3);
            expect(pos).toHaveProperty('node');
            expect(pos.node.nodeValue).toBe(" World");
        });
        it('should not find a valid dom point because of big offset', function () {
            expect(Position.getDOMPosition(paraNode1, [100])).toBeUndefined();
        });
        it('should find the correct dom point including list label in paragraph', function () {
            const pos = Position.getDOMPosition(paraNode2, [8]);
            expect(pos).toBeObject();
            expect(pos).toHaveProperty('offset', 3);
            expect(pos).toHaveProperty('node');
            expect(pos.node.nodeValue).toBe(" World");
        });
        it('should find the correct hard break dom point using forcePositionCounting', function () {
            const pos = Position.getDOMPosition(paraNode2, [11], true);
            expect(pos).toBeObject();
            expect(pos).toHaveProperty('offset', 0);
            expect(pos).toHaveProperty('node');
            expect(pos.node).toHaveClass('hardbreak');
        });
        it('should find the correct text dom point before a hard break node in a paragraph', function () {
            const pos = Position.getDOMPosition(paraNode2, [11]);
            expect(pos).toBeObject();
            expect(pos).toHaveProperty('offset', 6);
            expect(pos).toHaveProperty('node');
            expect(pos.node.nodeValue).toBe(" World");
        });
    });

    describe('method getWordSelection', function () {
        it('should exist', function () {
            expect(Position.getWordSelection).toBeFunction();
        });
        it('should find the correct word inside a paragraph with default boundaries', function () {
            expect(Position.getWordSelection(paraNode1, 0)).toEqual({ start: 0, end: 5, text: 'Hello', node: null, nodes: null });
            expect(Position.getWordSelection(paraNode1, 5)).toEqual({ start: 0, end: 5, text: 'Hello', node: null, nodes: null });
            expect(Position.getWordSelection(paraNode1, 6)).toEqual({ start: 6, end: 11, text: 'World', node: null, nodes: null });
            expect(Position.getWordSelection(paraNode1, 16)).toEqual({ start: 12, end: 16, text: 'Test', node: null, nodes: null });
        });
        it('should find the correct word inside a paragraph with user specified boundaries', function () {
            expect(Position.getWordSelection(paraNode1, 1, ['n', 'o', 'p'])).toEqual({ start: 0, end: 4, text: 'Hell', node: null, nodes: null });
            expect(Position.getWordSelection(paraNode1, 5, ['n', 'o', 'p'])).toEqual({ start: 5, end: 7, text: ' W', node: null, nodes: null });
            expect(Position.getWordSelection(paraNode1, 1, ['x', 'y', 'z'])).toEqual({ start: 0, end: 16, text: 'Hello World Test', node: null, nodes: null });
        });
        it('should not find the searched word in the paragraph', function () {
            expect(Position.getWordSelection(paraNode1, 17)).toBeNull();
        });
        it('should evalute option "addFinalSpaces" correctly', function () {
            expect(Position.getWordSelection(paraNode1, 8, undefined, { addFinalSpaces: true })).toEqual({ start: 6, end: 12, text: 'World ', node: null, nodes: null });
            expect(Position.getWordSelection(paraNode1, 14, undefined, { addFinalSpaces: true })).toEqual({ start: 12, end: 16, text: 'Test', node: null, nodes: null });
        });
        it('should evalute option "ignoreText" correctly', function () {
            expect(Position.getWordSelection(paraNode1, 8, undefined, { ignoreText: true })).toEqual({ start: 6, end: 11, text: '', node: null, nodes: null });
            expect(Position.getWordSelection(paraNode1, 14, undefined, { ignoreText: true })).toEqual({ start: 12, end: 16, text: '', node: null, nodes: null });
        });
        it('should evalute option "onlyLeft" correctly', function () {
            expect(Position.getWordSelection(paraNode1, 8, undefined, { onlyLeft: true })).toEqual({ start: 6, end: 8, text: 'Wo', node: null, nodes: null });
            expect(Position.getWordSelection(paraNode1, 14, undefined, { onlyLeft: true })).toEqual({ start: 12, end: 14, text: 'Te', node: null, nodes: null });
        });
    });

    describe('method getParagraphElement', function () {
        it('should exist', function () {
            expect(Position.getParagraphElement).toBeFunction();
        });
        it('should find a paragraph node', function () {
            const paraNode = Position.getParagraphElement(rootNode, [0]);
            expect(paraNode).toBeInstanceOf(HTMLDivElement);
            expect(paraNode).toHaveClass('p');
        });
    });

    describe('method getParagraphLength', function () {
        it('should exist', function () {
            expect(Position.getParagraphLength).toBeFunction();
        });
        it('should return the paragraph length of paragraph specified by logical position', function () {
            expect(Position.getParagraphLength(rootNode, [0])).toBe(15);
        });
    });

    describe('method getParagraphNodeLength', function () {
        it('should exist', function () {
            expect(Position.getParagraphNodeLength).toBeFunction();
        });
        it('should return the paragraph length of paragraph specified by paragraph node', function () {
            expect(Position.getParagraphNodeLength(Position.getParagraphElement(rootNode, [0]))).toBe(15);
        });
    });

    // testing word boundaries on second paragraph
    describe('method getWordBoundaries', function () {
        it('should exist', function () {
            expect(Position.getWordBoundaries).toBeFunction();
        });
        it('should return true for a range of positions', function () {
            var paraLength = para2Text.length; // para2Text is 'Hello Earth'
            var secondPara = Position.getParagraphElement(rootNode, [1]);
            expect(DOM.isParagraphNode(secondPara)).toBeTrue();
            expect(Position.getParagraphLength(rootNode, [1])).toBe(paraLength); // counting length (value is 11)

            var lastLogicalPos = [1, paraLength]; // the logical position behind the word 'Earth'
            // searching the last word
            var wordBoundaries = Position.getWordBoundaries(rootNode, lastLogicalPos);
            expect(wordBoundaries).toBeArray();
            expect(wordBoundaries).toHaveLength(2);

            // comparing the logical border positions of the word earth: [1,6] and [1,11].
            // -> the end position is the position behind the word.
            expect(wordBoundaries[0][0]).toBe(1);
            expect(wordBoundaries[0][1]).toBe(6);
            expect(wordBoundaries[1][0]).toBe(1);
            expect(wordBoundaries[1][1]).toBe(11);

            // comparing value with function 'Position.getWordSelection'
            expect(Position.getWordSelection(secondPara, paraLength)).toEqual({ start: 6, end: 11, text: 'Earth', node: null, nodes: null });
        });
    });

    // testing valid selections on second paragraph (checking the word 'Earth' at the end of the paragraph)
    // -> valid element ranges can be used for setAttributes operation, where the final position inside a
    //    a paragraph behind the last character is invalid.
    describe('method isValidElementRange', function () {
        it('should exist', function () {
            expect(Position.isValidElementRange).toBeFunction();
            expect(Position.isValidElementPosition).toBeFunction(); // used by function 'isValidElementRange'
        });
        it('should return true for a range of positions', function () {
            var paraPos = 1;
            var secondPara = Position.getParagraphElement(rootNode, [paraPos]);
            var paraLength = para2Text.length; // para2Text is 'Hello Earth'
            var lastWordStartPos = 6; // the start position of the last word inside the paragraph

            expect(Position.getParagraphLength(rootNode, [paraPos])).toBe(paraLength); // counting length
            // checking that paragraph ends with 'Earth'
            expect(Position.getWordSelection(secondPara, paraLength)).toEqual({ start: lastWordStartPos, end: paraLength, text: 'Earth', node: null, nodes: null });

            // checking isValidElementRange -> this is not valid behind the last character in the paragraph!
            // checking missing end position (start and end position are equal)
            expect(Position.isValidElementRange(rootNode, [paraPos, (paraLength - 1)])).toBeTrue(); // position of last character
            expect(Position.isValidElementRange(rootNode, [paraPos, paraLength])).toBeFalse(); // position behind last character
            // checking with range from logical start to end position
            expect(Position.isValidElementRange(rootNode, [paraPos, lastWordStartPos], [paraPos, (paraLength - 1)])).toBeTrue(); // position of last character
            expect(Position.isValidElementRange(rootNode, [paraPos, lastWordStartPos], [paraPos, paraLength])).toBeFalse(); // position behind last character
            // checking with range from logical start to end position with parameter 'allowFinalPosition'
            expect(Position.isValidElementRange(rootNode, [paraPos, lastWordStartPos], [paraPos, (paraLength - 1)], { allowFinalPosition: true })).toBeTrue(); // position of last character
            expect(Position.isValidElementRange(rootNode, [paraPos, lastWordStartPos], [paraPos, paraLength], { allowFinalPosition: true })).toBeTrue(); // position behind last character
            expect(Position.isValidElementRange(rootNode, [paraPos, lastWordStartPos], [paraPos, paraLength + 1], { allowFinalPosition: true })).toBeFalse(); // one more position behind the character
        });
    });

});
