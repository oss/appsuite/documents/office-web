/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { createTextApp } from '~/text/apphelper';
import * as DOM from '@/io.ox/office/textframework/utils/dom';


// static class DOM =======================================================

describe('Text module DOM', function () {

    // static methods -----------------------------------------------------

    describe('method isTableRowNode', function () {
        it('should exist', function () {
            expect(DOM.isTableRowNode).toBeFunction();
        });
        it('should find valid table row nodes', function () {
            expect(DOM.isTableRowNode($('<tr>'))).toBeTrue();
            expect(DOM.isTableRowNode($('<td>'))).not.toBeTrue();
        });
    });

    describe('method isListLabelNode', function () {
        it('should exist', function () {
            expect(DOM.isListLabelNode).toBeFunction();
        });
        it('should find valid list label nodes', function () {
            expect(DOM.isListLabelNode($('<div>').addClass('list-label'))).toBeTrue();
            expect(DOM.isListLabelNode($('<span>').addClass('list-label'))).not.toBeTrue();
            expect(DOM.isListLabelNode($('<div>'))).not.toBeTrue();
        });
    });
});

// class Point --------------------------------------------------------

describe("class DOM.Point", function () {

    // private helpers ----------------------------------------------------

    //  the operations to be applied by the document model
    let model;
    let pageNode;
    let pageContentNode;
    let firstParagraph;
    let secondParagraph;
    let firstTextNode;
    let secondTextNode;
    const startText1 = 'Hello Moon';
    const startText2 = 'Hello Earth!';

    const OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', start: [0, 0], text: startText1 },
        { name: 'insertParagraph', start: [1] },
        { name: 'insertText', start: [1, 0], text: startText2 }
    ];

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        pageNode = model.getNode();
        pageContentNode = DOM.getPageContentNode(pageNode)[0];
        firstParagraph = pageContentNode.firstElementChild;
        secondParagraph = pageContentNode.lastElementChild;
        firstTextNode = firstParagraph.firstChild.firstChild;
        secondTextNode = secondParagraph.firstChild.firstChild;
    });

    it("should work with document with valid nodes", function () {
        expect(pageContentNode).toHaveClass('pagecontent');
        expect(pageContentNode.childNodes).toHaveLength(2);
        expect(firstParagraph).toHaveClass('p');
        expect(secondParagraph).toHaveClass('p');
        expect(firstParagraph.childNodes).toHaveLength(1);
        expect(secondParagraph.childNodes).toHaveLength(1);
        expect(firstParagraph.firstChild.nodeName).toBe("SPAN");
        expect(secondParagraph.firstChild.nodeName).toBe("SPAN");
        expect(firstParagraph.firstChild.childNodes).toHaveLength(1);
        expect(secondParagraph.firstChild.childNodes).toHaveLength(1);

        expect(firstTextNode.nodeType).toBe(3);
        expect(secondTextNode.nodeType).toBe(3);
        expect(firstTextNode.nodeValue).toBe(startText1);
        expect(secondTextNode.nodeValue).toBe(startText2);
    });

    it("should exist", function () {
        expect(DOM.Point).toBeFunction();
    });

    describe("constructor", function () {
        it("is constructed with a node and an offset", function () {
            const domPoint = new DOM.Point(firstTextNode, 4);
            expect(domPoint.node).toBe(firstTextNode);
            expect(domPoint.node.nodeType).toBe(3);
            expect(domPoint.node.nodeValue).toBe(startText1);
            expect(domPoint.offset).toBe(4);
        });
    });

    describe("method clone", function () {
        it("should create a clone of the point", function () {
            const domPoint = new DOM.Point(firstTextNode, 2);
            const clone = domPoint.clone();
            expect(clone.node).toBe(firstTextNode);
            expect(clone.node.nodeType).toBe(3);
            expect(domPoint.node.nodeValue).toBe(startText1);
            expect(clone.offset).toBe(2);
        });
    });

    describe("method validate", function () {
        it("should reduce the offset if required", function () {
            const domPoint = new DOM.Point(firstTextNode, 25);
            domPoint.validate();
            expect(domPoint.node).toBe(firstTextNode);
            expect(domPoint.node.nodeType).toBe(3);
            expect(domPoint.node.nodeValue).toBe(startText1);
            expect(domPoint.offset).toBe(startText1.length);
        });

        it("should increase the offset if required", function () {
            const domPoint = new DOM.Point(firstTextNode, -2);
            domPoint.validate();
            expect(domPoint.node).toBe(firstTextNode);
            expect(domPoint.node.nodeType).toBe(3);
            expect(domPoint.node.nodeValue).toBe(startText1);
            expect(domPoint.offset).toBe(0);
        });

        it("should reduce the offset for a paragraph node to the number of children, if required", function () {
            const domPoint = new DOM.Point(secondParagraph, 5);
            domPoint.validate();
            expect(domPoint.node).toBe(secondParagraph);
            expect(domPoint.node.nodeType).toBe(1);
            expect(domPoint.offset).toBe(1);
        });
    });

    describe("method toString", function () {
        it("should create a valid text description", function () {
            const domPoint = new DOM.Point(firstTextNode, 2);
            expect(domPoint.toString()).toBe(`span:0>#text"${startText1}":2`);
        });
    });

    describe("static method createPointForNode", function () {

        it("should create a point for a specified text node", function () {
            const domPoint = DOM.Point.createPointForNode(firstTextNode);
            expect(domPoint.node).toBe(firstTextNode);
            expect(domPoint.node.nodeType).toBe(3);
            expect(domPoint.node.nodeValue).toBe(startText1);
            expect(domPoint.offset).toBe(0);
        });

        it("should create a point for a specified first paragraph node", function () {
            const domPoint = DOM.Point.createPointForNode(firstParagraph);
            expect(domPoint.node).toBe(pageContentNode);
            expect(domPoint.node.nodeType).toBe(1);
            expect(domPoint.offset).toBe(0);
        });

        it("should create a point for a specified second paragraph node", function () {
            const domPoint = DOM.Point.createPointForNode(secondParagraph);
            expect(domPoint.node).toBe(pageContentNode);
            expect(domPoint.node.nodeType).toBe(1);
            expect(domPoint.offset).toBe(1);
        });
    });

    describe("static method equalPoints", function () {

        it("should return true, when two points are equal", function () {
            const domPoint1 = DOM.Point.createPointForNode(firstTextNode);
            const domPoint2 = DOM.Point.createPointForNode(firstTextNode);
            expect(DOM.Point.equalPoints(domPoint1, domPoint2)).toBeTrue();
        });

        it("should return false, when two points are not equal", function () {
            const domPoint1 = DOM.Point.createPointForNode(firstTextNode);
            const domPoint2 = DOM.Point.createPointForNode(secondTextNode);
            expect(DOM.Point.equalPoints(domPoint1, domPoint2)).toBeFalse();
        });
    });

    describe("static method comparePoints", function () {

        it("should return 0, when two points are equal (text node)", function () {
            const domPoint1 = DOM.Point.createPointForNode(firstTextNode);
            const domPoint2 = DOM.Point.createPointForNode(firstTextNode);
            expect(DOM.Point.comparePoints(domPoint1, domPoint2)).toBe(0);
        });

        it("should return 0, when two points are equal (paragraph node)", function () {
            const domPoint1 = DOM.Point.createPointForNode(secondParagraph);
            const domPoint2 = DOM.Point.createPointForNode(secondParagraph);
            expect(DOM.Point.comparePoints(domPoint1, domPoint2)).toBe(0);
        });

        it("should return a negative number, when point1 precedes point2 (text node)", function () {
            const domPoint1 = DOM.Point.createPointForNode(firstTextNode);
            const domPoint2 = DOM.Point.createPointForNode(secondTextNode);
            expect(DOM.Point.comparePoints(domPoint1, domPoint2)).toBeLessThan(0);
        });

        it("should return a negative number, when point1 precedes point2 (inside text node)", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 1);
            const domPoint2 = new DOM.Point(firstTextNode, 4);
            expect(DOM.Point.comparePoints(domPoint1, domPoint2)).toBeLessThan(0);
        });

        it("should return a negative number, when point1 precedes point2 (paragraph node)", function () {
            const domPoint1 = DOM.Point.createPointForNode(firstParagraph);
            const domPoint2 = DOM.Point.createPointForNode(secondParagraph);
            expect(DOM.Point.comparePoints(domPoint1, domPoint2)).toBeLessThan(0);
        });

        it("should return a positive number, when point1 follows point2 (text node)", function () {
            const domPoint1 = DOM.Point.createPointForNode(secondTextNode);
            const domPoint2 = DOM.Point.createPointForNode(firstTextNode);
            expect(DOM.Point.comparePoints(domPoint1, domPoint2)).toBeGreaterThan(0);
        });

        it("should return a positive number, when point1 follows point2 (inside text node)", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 4);
            const domPoint2 = new DOM.Point(firstTextNode, 1);
            expect(DOM.Point.comparePoints(domPoint1, domPoint2)).toBeGreaterThan(0);
        });

        it("should return a positive number, when point1 follows point2 (paragraph node)", function () {
            const domPoint1 = DOM.Point.createPointForNode(secondParagraph);
            const domPoint2 = DOM.Point.createPointForNode(firstParagraph);
            expect(DOM.Point.comparePoints(domPoint1, domPoint2)).toBeGreaterThan(0);
        });
    });

});

// class Point --------------------------------------------------------

describe("class DOM.Range", function () {

    // private helpers ----------------------------------------------------

    //  the operations to be applied by the document model
    let model;
    let pageNode;
    let pageContentNode;
    let firstParagraph;
    let secondParagraph;
    let firstTextNode;
    let secondTextNode;
    const startText1 = 'Hello Moon';
    const startText2 = 'Hello Earth!';

    const OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 }, character: { fontName: 'Arial', fontSize: 11, language: 'en-US', languageEa: 'en-US', languageBidi: 'ar-SA' }, paragraph: { lineHeight: { type: 'percent', value: 115 }, marginBottom: 352 } } },
        { name: 'insertParagraph', start: [0] },
        { name: 'insertText', start: [0, 0], text: startText1 },
        { name: 'insertParagraph', start: [1] },
        { name: 'insertText', start: [1, 0], text: startText2 }
    ];

    createTextApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        pageNode = model.getNode();
        pageContentNode = DOM.getPageContentNode(pageNode)[0];
        firstParagraph = pageContentNode.firstElementChild;
        secondParagraph = pageContentNode.lastElementChild;
        firstTextNode = firstParagraph.firstChild.firstChild;
        secondTextNode = secondParagraph.firstChild.firstChild;
    });

    it("should work with document with valid nodes", function () {
        expect(pageContentNode).toHaveClass('pagecontent');
        expect(pageContentNode.childNodes).toHaveLength(2);
        expect(firstParagraph).toHaveClass('p');
        expect(secondParagraph).toHaveClass('p');
        expect(firstParagraph.childNodes).toHaveLength(1);
        expect(secondParagraph.childNodes).toHaveLength(1);
        expect(firstParagraph.firstChild.nodeName).toBe("SPAN");
        expect(secondParagraph.firstChild.nodeName).toBe("SPAN");
        expect(firstParagraph.firstChild.childNodes).toHaveLength(1);
        expect(secondParagraph.firstChild.childNodes).toHaveLength(1);

        expect(firstTextNode.nodeType).toBe(3);
        expect(secondTextNode.nodeType).toBe(3);
        expect(firstTextNode.nodeValue).toBe(startText1);
        expect(secondTextNode.nodeValue).toBe(startText2);
    });

    it("should exist", function () {
        expect(DOM.Range).toBeFunction();
    });

    describe("constructor", function () {

        it("is constructed with two DOM Points", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 1);
            const domPoint2 = new DOM.Point(firstTextNode, 4);
            const range = new DOM.Range(domPoint1, domPoint2);
            expect(range.start.node).toBe(firstTextNode);
            expect(range.start.node.nodeType).toBe(3);
            expect(range.start.node.nodeValue).toBe(startText1);
            expect(range.start.offset).toBe(1);
            expect(range.end.node).toBe(firstTextNode);
            expect(range.end.node.nodeType).toBe(3);
            expect(range.end.node.nodeValue).toBe(startText1);
            expect(range.end.offset).toBe(4);
        });

        it("is constructed with one DOM Point", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 1);
            const range = new DOM.Range(domPoint1);
            expect(range.start.node).toBe(firstTextNode);
            expect(range.start.node.nodeType).toBe(3);
            expect(range.start.node.nodeValue).toBe(startText1);
            expect(range.start.offset).toBe(1);
            expect(range.end.node).toBe(firstTextNode);
            expect(range.end.node.nodeType).toBe(3);
            expect(range.end.node.nodeValue).toBe(startText1);
            expect(range.end.offset).toBe(1);
        });
    });

    describe("method clone", function () {
        it("should create a clone of the range", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 1);
            const domPoint2 = new DOM.Point(firstTextNode, 4);
            const range = new DOM.Range(domPoint1, domPoint2);
            const cloneRange = range.clone();
            expect(cloneRange.start.node).toBe(firstTextNode);
            expect(cloneRange.start.node.nodeType).toBe(3);
            expect(cloneRange.start.node.nodeValue).toBe(startText1);
            expect(cloneRange.start.offset).toBe(1);
            expect(cloneRange.end.node).toBe(firstTextNode);
            expect(cloneRange.end.node.nodeType).toBe(3);
            expect(cloneRange.end.node.nodeValue).toBe(startText1);
            expect(cloneRange.end.offset).toBe(4);
        });
    });

    describe("method validate", function () {
        it("should validate both Points inside the range", function () {
            const domPoint1 = new DOM.Point(firstTextNode, -5);
            const domPoint2 = new DOM.Point(firstTextNode, 40);
            const range = new DOM.Range(domPoint1, domPoint2);
            range.validate();
            expect(range.start.node).toBe(firstTextNode);
            expect(range.start.node.nodeType).toBe(3);
            expect(range.start.node.nodeValue).toBe(startText1);
            expect(range.start.offset).toBe(0);
            expect(range.end.node).toBe(firstTextNode);
            expect(range.end.node.nodeType).toBe(3);
            expect(range.end.node.nodeValue).toBe(startText1);
            expect(range.end.offset).toBe(10);
        });
    });

    describe("method adjust", function () {

        it("should not switch both Points inside the range, if the order is correct", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 5);
            const domPoint2 = new DOM.Point(secondTextNode, 2);
            const range = new DOM.Range(domPoint1, domPoint2);
            range.adjust();
            expect(range.start.node).toBe(firstTextNode);
            expect(range.start.node.nodeType).toBe(3);
            expect(range.start.node.nodeValue).toBe(startText1);
            expect(range.start.offset).toBe(5);
            expect(range.end.node).toBe(secondTextNode);
            expect(range.end.node.nodeType).toBe(3);
            expect(range.end.node.nodeValue).toBe(startText2);
            expect(range.end.offset).toBe(2);
        });

        it("should switch both Points inside the range, if the order is not correct", function () {
            const domPoint1 = new DOM.Point(secondTextNode, 2);
            const domPoint2 = new DOM.Point(firstTextNode, 5);
            const range = new DOM.Range(domPoint1, domPoint2);
            range.adjust();
            expect(range.start.node).toBe(firstTextNode);
            expect(range.start.node.nodeType).toBe(3);
            expect(range.start.node.nodeValue).toBe(startText1);
            expect(range.start.offset).toBe(5);
            expect(range.end.node).toBe(secondTextNode);
            expect(range.end.node.nodeType).toBe(3);
            expect(range.end.node.nodeValue).toBe(startText2);
            expect(range.end.offset).toBe(2);
        });
    });

    describe("method isCollapsed", function () {

        it("should be true, if range is created with one Point", function () {
            const domPoint = new DOM.Point(secondTextNode, 5);
            const range = new DOM.Range(domPoint);
            expect(range.isCollapsed()).toBeTrue();
        });

        it("should be true, if start and end Point are equal", function () {
            const domPoint1 = new DOM.Point(secondTextNode, 5);
            const domPoint2 = new DOM.Point(secondTextNode, 5);
            const range = new DOM.Range(domPoint1, domPoint2);
            expect(range.isCollapsed()).toBeTrue();
        });

        it("should be false, if start and end Point are not equal inside a text node", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 2);
            const domPoint2 = new DOM.Point(firstTextNode, 5);
            const range = new DOM.Range(domPoint1, domPoint2);
            expect(range.isCollapsed()).toBeFalse();
        });

        it("should not be false, if start and end Point have different nodes", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 5);
            const domPoint2 = new DOM.Point(secondTextNode, 5);
            const range = new DOM.Range(domPoint1, domPoint2);
            expect(range.isCollapsed()).toBeFalse();
        });
    });

    describe("method toString", function () {
        it("should return a valid string describing a specified range inside a text node", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 2);
            const domPoint2 = new DOM.Point(firstTextNode, 5);
            const range = new DOM.Range(domPoint1, domPoint2);
            expect(range.toString()).toBe(`[start=span:0>#text"${startText1}":2, end=span:0>#text"${startText1}":5]`);
        });

        it("should return a valid string describing a specified collapsed range", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 2);
            const range = new DOM.Range(domPoint1);
            expect(range.toString()).toBe(`[start=span:0>#text"${startText1}":2, end=span:0>#text"${startText1}":2]`);
        });

        it("should return a valid string describing a specified paragraph range", function () {
            const domPoint1 = new DOM.Point(firstParagraph, 0);
            const domPoint2 = new DOM.Point(firstParagraph, 1);
            const range = new DOM.Range(domPoint1, domPoint2);
            expect(range.toString()).toBe("[start=div.pagecontent:0>div.p:0, end=div.pagecontent:0>div.p:1]");
        });
    });

    describe("static method equalRanges", function () {

        it("should return true, if the Points of the ranges are equal", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 5);
            const domPoint2 = new DOM.Point(secondTextNode, 2);
            const range1 = new DOM.Range(domPoint1, domPoint2);
            const range2 = new DOM.Range(domPoint1, domPoint2);
            expect(DOM.Range.equalRanges(range1, range2)).toBeTrue();
        });

        it("should return true, if the Points of two collapsed ranges are equal", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 5);
            const domPoint2 = new DOM.Point(firstTextNode, 5);
            const range1 = new DOM.Range(domPoint1);
            const range2 = new DOM.Range(domPoint1, domPoint2);
            expect(DOM.Range.equalRanges(range1, range2)).toBeTrue();
        });

        it("should return false, if the Points of the ranges have only different offsets", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 2);
            const domPoint2 = new DOM.Point(firstTextNode, 3);
            const domPoint3 = new DOM.Point(firstTextNode, 4);
            const range1 = new DOM.Range(domPoint1, domPoint2);
            const range2 = new DOM.Range(domPoint1, domPoint3);
            expect(DOM.Range.equalRanges(range1, range2)).toBeFalse();
        });

        it("should return false, if the Points of the ranges have only different nodes", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 2);
            const domPoint2 = new DOM.Point(firstTextNode, 3);
            const domPoint3 = new DOM.Point(secondTextNode, 2);
            const domPoint4 = new DOM.Point(secondTextNode, 3);
            const range1 = new DOM.Range(domPoint1, domPoint2);
            const range2 = new DOM.Range(domPoint3, domPoint4);
            expect(DOM.Range.equalRanges(range1, range2)).toBeFalse();
        });

        it("should return false, if the Points of the ranges have a different order", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 5);
            const domPoint2 = new DOM.Point(secondTextNode, 2);
            const range1 = new DOM.Range(domPoint1, domPoint2);
            const range2 = new DOM.Range(domPoint2, domPoint1);
            expect(DOM.Range.equalRanges(range1, range2)).toBeFalse();
        });
    });

    describe("static method equalRangeOffsets", function () {

        it("should return true, if the Points of the ranges have same node and same offset", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 5);
            const domPoint2 = new DOM.Point(secondTextNode, 2);
            const range1 = new DOM.Range(domPoint1, domPoint2);
            const range2 = new DOM.Range(domPoint1, domPoint2);
            expect(DOM.Range.equalRangeOffsets(range1, range2)).toBeTrue();
        });

        it("should return true, if the Points of the ranges have different node but same offset", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 5);
            const domPoint2 = new DOM.Point(secondTextNode, 5);
            const range1 = new DOM.Range(domPoint2, domPoint1);
            const range2 = new DOM.Range(domPoint1, domPoint2);
            expect(DOM.Range.equalRangeOffsets(range1, range2)).toBeTrue();
        });

        it("should return false, if the Points of the ranges have only different offsets", function () {
            const domPoint1 = new DOM.Point(firstTextNode, 2);
            const domPoint2 = new DOM.Point(firstTextNode, 3);
            const domPoint3 = new DOM.Point(firstTextNode, 4);
            const range1 = new DOM.Range(domPoint1, domPoint2);
            const range2 = new DOM.Range(domPoint1, domPoint3);
            expect(DOM.Range.equalRangeOffsets(range1, range2)).toBeFalse();
        });
    });

    describe("static method createRange", function () {

        it("should create a new range from two specified text nodes and offsets", function () {
            const range = DOM.Range.createRange(firstTextNode, 5, secondTextNode, 2);
            expect(range.start.node).toBe(firstTextNode);
            expect(range.start.node.nodeType).toBe(3);
            expect(range.start.node.nodeValue).toBe(startText1);
            expect(range.start.offset).toBe(5);
            expect(range.end.node).toBe(secondTextNode);
            expect(range.end.node.nodeType).toBe(3);
            expect(range.end.node.nodeValue).toBe(startText2);
            expect(range.end.offset).toBe(2);
        });

        it("should create a collapsed range from one specified text node and offset", function () {
            const range = DOM.Range.createRange(firstTextNode, 5);
            expect(range.start.node).toBe(firstTextNode);
            expect(range.start.node.nodeType).toBe(3);
            expect(range.start.node.nodeValue).toBe(startText1);
            expect(range.start.offset).toBe(5);
            expect(range.end.node).toBe(firstTextNode);
            expect(range.end.node.nodeType).toBe(3);
            expect(range.end.node.nodeValue).toBe(startText1);
            expect(range.end.offset).toBe(5);
        });
    });

    describe("static method createRangeForNode", function () {

        it("should create a new range from a specified text node", function () {
            const range = DOM.Range.createRangeForNode(firstTextNode);
            expect(range.start.node).toBe(firstTextNode);
            expect(range.start.node.nodeType).toBe(3);
            expect(range.start.node.nodeValue).toBe(startText1);
            expect(range.start.offset).toBe(0);
            expect(range.end.node).toBe(firstTextNode);
            expect(range.end.node.nodeType).toBe(3);
            expect(range.end.node.nodeValue).toBe(startText1);
            expect(range.end.offset).toBe(10);
        });

        it("should create a new range from the specified first paragraph node", function () {
            const range = DOM.Range.createRangeForNode(firstParagraph);
            expect(range.start.node).toBe(pageContentNode);
            expect(range.start.node.nodeType).toBe(1);
            expect(range.start.offset).toBe(0);
            expect(range.end.node).toBe(pageContentNode);
            expect(range.end.node.nodeType).toBe(1);
            expect(range.end.offset).toBe(1);
        });

        it("should create a new range from the specified second paragraph node", function () {
            const range = DOM.Range.createRangeForNode(secondParagraph);
            expect(range.start.node).toBe(pageContentNode);
            expect(range.start.node.nodeType).toBe(1);
            expect(range.start.offset).toBe(1);
            expect(range.end.node).toBe(pageContentNode);
            expect(range.end.node.nodeType).toBe(1);
            expect(range.end.offset).toBe(2);
        });
    });

});
