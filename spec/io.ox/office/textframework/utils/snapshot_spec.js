/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Color } from '@/io.ox/office/editframework/utils/color';
import Snapshot from '@/io.ox/office/textframework/utils/snapshot';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Utils from '@/io.ox/office/tk/utils';

import { createPresentationApp } from '~/presentation/apphelper';

// class Snapshot =========================================================

describe('Text framework module Snapshot', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        snapshot = null,
        layoutId_1 = 'layout1',
        layoutId_2 = 'layout2',
        masterId_1 = 'master1',
        slideId_1 = 'slide_1',
        slideId_2 = 'slide_2',
        ctrTitleLeft = 1900,
        subTitleLeft = 3800,
        ctrTitleTop = 5900,
        subTitleTop = 10800,
        ctrTitleHeight = 4000,
        subTitleHeight = 5000,
        ctrTitleWidth = 21600,
        subTitleWidth = 17800,

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: { page: { width: 33866, height: 19050, orientation: 'landscape' } }
            },
            {
                name: 'insertMasterSlide', id: masterId_1, attrs: {
                    listStyles: {
                        title: { l1: {
                            character: { fontSize: 44, fontName: '+mj-lt', type: 'solid', color: Color.TEXT1 },
                            paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'percent', value: 0 }, defaultTabSize: 2540, bullet: { type: 'none' }, alignment: 'left' } }
                        },
                        body: {
                            l1: {
                                character: { fontSize: 28, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 35 }, defaultTabSize: 2540, bullet: { type: 'character', character: '-' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 635, indentFirstLine: -634 }
                            },
                            l2: {
                                character: { fontSize: 24, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 1905, indentFirstLine: -634 }
                            }
                        },
                        other: {
                            l1: {
                                character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 0 }
                            }
                        }
                    }
                }
            },
            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1, attrs: { slide: { type: 'title' } } },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: ctrTitleLeft, top: ctrTitleTop, width: ctrTitleWidth, height: ctrTitleHeight }, listStyle: { l1: { character: { fontSize: 40, bold: true }, paragraph: { alignment: 'left' } } } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: 'Mastertitelformat bearbeiten' },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: subTitleLeft, top: subTitleTop, width: subTitleWidth, height: subTitleHeight } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_1, text: 'Master-Untertitelformat bearbeiten' },

            { name: 'insertLayoutSlide', id: layoutId_2, target: masterId_1, attrs: { slide: { type: 'obj' } } },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_2, type: 'shape', attrs: { presentation: { phType: 'title' }, drawing: { name: 'Titel 1' }, listStyle: { l1: { character: { fontSize: 40, bold: true }, paragraph: { alignment: 'left' } } } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_2 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_2, text: 'Mastertitelformat bearbeiten' },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_2, type: 'shape', attrs: { presentation: { phIndex: 1 }, drawing: { name: 'Drawing 2', left: 1270, top: 4445, width: 11218, height: 12572 } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_2, attrs: { paragraph: { level: 0 } } },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_2, text: 'Mastertextformat bearbeiten' },
            { name: 'insertDrawing', start: [0, 2], target: layoutId_2, type: 'shape', attrs: { presentation: { phIndex: 2 }, drawing: { name: 'Drawing 3', left: 12912, top: 4445, width: 11218, height: 12572 } } },
            { name: 'insertParagraph', start: [0, 2, 0], target: layoutId_2, attrs: { paragraph: { level: 0 } } },
            { name: 'insertText', start: [0, 2, 0, 0], target: layoutId_2, text: 'Mastertextformat bearbeiten' },

            { name: 'insertSlide', start: [0], target: layoutId_1, attrs: { slide: { followMasterShapes: true, isDate: true, isSlideNum: true, isHeader: false, isFooter: true } } },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1' } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertText', start: [0, 0, 0, 0], text: 'Hello paragraph 1 in drawing 1 on slide 1' },

            { name: 'insertSlide', start: [1], target: layoutId_1, attrs: { slide: { followMasterShapes: false, isDate: true, isSlideNum: true, isHeader: false, isFooter: false } } },
            { name: 'insertDrawing', start: [1, 0], type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 2' } } },
            { name: 'insertParagraph', start: [1, 0, 0] },
            { name: 'insertText', start: [1, 0, 0, 0], text: 'Hello paragraph 1 in drawing 1 on slide 2' }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        snapshot = new Snapshot(model);
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(Snapshot).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('constructor function', function () {
        it('should create a new object', function () {
            expect(snapshot).toBeInstanceOf(Snapshot);
        });
    });

    describe('method apply', function () {

        it('should exist', function () {
            expect(Snapshot).toHaveMethod('apply');
        });

        it('should exchange the top level nodes of the page and restore the model', function () {

            // saving old children of the page node
            var oldPageContentNode = DOM.getPageContentNode(model.getNode())[0];
            var oldMasterSlideLayerNode = Utils.getDomNode(model.getMasterSlideLayerNode());
            var oldLayoutSlideLayerNode = Utils.getDomNode(model.getLayoutSlideLayerNode());

            var oldPageContentNodeParent = oldPageContentNode.parentNode;
            var oldMasterSlideLayerNodeParent = oldMasterSlideLayerNode.parentNode;
            var oldLayoutSlideLayerNodeParent = oldLayoutSlideLayerNode.parentNode;

            // saving the old slide nodes
            var oldMasterSlide1 = Utils.getDomNode(model.getSlideById(masterId_1));
            var oldLayoutSlide1 = Utils.getDomNode(model.getSlideById(layoutId_1));
            var oldLayoutSlide2 = Utils.getDomNode(model.getSlideById(layoutId_2));
            var oldSlide1 = Utils.getDomNode(model.getSlideById(slideId_1));
            var oldSlide2 = Utils.getDomNode(model.getSlideById(slideId_2));

            //  saving old model values
            var oldStandardSlideOrder = model.getStandardSlideOrder();
            var oldMasterSlideOrder = model.getMasterSlideOrder();

            var oldIdTypeConnectionModel = model.getIdTypeConnectionModel();
            var oldLayoutConnection = model.getLayoutConnection();
            var oldLayoutMasterConnection = model.getLayoutMasterConnection();

            var oldSlideAttributesMaster1 = model.getAllFamilySlideAttributes(masterId_1);
            var oldSlideAttributesLayout1 = model.getAllFamilySlideAttributes(layoutId_1);
            var oldSlideAttributesLayout2 = model.getAllFamilySlideAttributes(layoutId_2);

            var oldSlideAttributesSlide1 = model.getAllFamilySlideAttributes(slideId_1);
            var oldSlideAttributesSlide2 = model.getAllFamilySlideAttributes(slideId_2);

            var oldSlideListStyleAttributesTitle = model.getListStylesAttributesOfSlide(masterId_1, 'title');
            var oldSlideListStyleAttributesBody = model.getListStylesAttributesOfSlide(masterId_1, 'body');
            var oldSlideListStyleAttributesOther = model.getListStylesAttributesOfSlide(masterId_1, 'other');

            var oldPlaceHolderAttributesMaster1 = model.drawingStyles.getAllPlaceHolderAttributesForId(masterId_1);
            var oldPlaceHolderAttributesLayout1 = model.drawingStyles.getAllPlaceHolderAttributesForId(layoutId_1);
            var oldPlaceHolderAttributesLayout2 = model.drawingStyles.getAllPlaceHolderAttributesForId(layoutId_2);

            var oldDrawingListStyles = model.drawingStyles.getAllDrawingListStyleAttributes();

            snapshot.apply();  // applying the snapshot

            const newPageContentNode = DOM.getPageContentNode(model.getNode())[0];
            const newMasterSlideLayerNode = Utils.getDomNode(model.getMasterSlideLayerNode());
            const newLayoutSlideLayerNode = Utils.getDomNode(model.getLayoutSlideLayerNode());

            // comparing the children of the page node
            expect(oldPageContentNode).not.toBe(newPageContentNode);
            expect(oldMasterSlideLayerNode).not.toBe(newMasterSlideLayerNode);
            expect(oldLayoutSlideLayerNode).not.toBe(newLayoutSlideLayerNode);

            expect(oldPageContentNodeParent).toBe(newPageContentNode.parentNode);
            expect(oldMasterSlideLayerNodeParent).toBe(newMasterSlideLayerNode.parentNode);
            expect(oldLayoutSlideLayerNodeParent).toBe(newLayoutSlideLayerNode.parentNode);

            // expecting that the old nodes are no longer in the DOM
            expect(oldPageContentNode.parentNode).toBeNull();
            expect(oldMasterSlideLayerNode.parentNode).toBeNull();
            expect(oldLayoutSlideLayerNode.parentNode).toBeNull();

            // comparing the slide nodes
            const newMasterSlide1 = Utils.getDomNode(model.getSlideById(masterId_1));
            const newLayoutSlide1 = Utils.getDomNode(model.getSlideById(layoutId_1));
            const newLayoutSlide2 = Utils.getDomNode(model.getSlideById(layoutId_2));
            const newSlide1 = Utils.getDomNode(model.getSlideById(slideId_1));
            const newSlide2 = Utils.getDomNode(model.getSlideById(slideId_2));

            expect(DOM.isSlideNode(oldMasterSlide1) && DOM.isSlideNode(newMasterSlide1) && oldMasterSlide1 !== newMasterSlide1).toBeTrue();
            expect(DOM.isSlideNode(oldLayoutSlide1) && DOM.isSlideNode(newLayoutSlide1) && oldLayoutSlide1 !== newLayoutSlide1).toBeTrue();
            expect(DOM.isSlideNode(oldLayoutSlide2) && DOM.isSlideNode(newLayoutSlide2) && oldLayoutSlide2 !== newLayoutSlide2).toBeTrue();
            expect(DOM.isSlideNode(oldSlide1) && DOM.isSlideNode(newSlide1) && oldSlide1 !== newSlide1).toBeTrue();
            expect(DOM.isSlideNode(oldSlide2) && DOM.isSlideNode(newSlide2) && oldSlide2 !== newSlide2).toBeTrue();

            // comparing the model
            const newStandardSlideOrder = model.getStandardSlideOrder();
            expect(oldStandardSlideOrder).not.toBe(newStandardSlideOrder);
            expect(oldStandardSlideOrder).toEqual(newStandardSlideOrder);

            const newMasterSlideOrder = model.getMasterSlideOrder();
            expect(oldMasterSlideOrder).not.toBe(newMasterSlideOrder);
            expect(oldMasterSlideOrder).toEqual(newMasterSlideOrder);

            expect(oldIdTypeConnectionModel).toEqual(model.getIdTypeConnectionModel());
            expect(oldLayoutConnection).toEqual(model.getLayoutConnection());
            expect(oldLayoutMasterConnection).toEqual(model.getLayoutMasterConnection());

            expect(oldSlideAttributesMaster1).toEqual(model.getAllFamilySlideAttributes(masterId_1));
            expect(oldSlideAttributesLayout1).toEqual(model.getAllFamilySlideAttributes(layoutId_1));
            expect(oldSlideAttributesLayout2).toEqual(model.getAllFamilySlideAttributes(layoutId_2));

            expect(oldSlideAttributesSlide1).toEqual(model.getAllFamilySlideAttributes(slideId_1));
            expect(oldSlideAttributesSlide2).toEqual(model.getAllFamilySlideAttributes(slideId_2));

            expect(oldSlideListStyleAttributesTitle).toEqual(model.getListStylesAttributesOfSlide(masterId_1, 'title'));
            expect(oldSlideListStyleAttributesBody).toEqual(model.getListStylesAttributesOfSlide(masterId_1, 'body'));
            expect(oldSlideListStyleAttributesOther).toEqual(model.getListStylesAttributesOfSlide(masterId_1, 'other'));

            expect(oldPlaceHolderAttributesMaster1).toEqual(model.drawingStyles.getAllPlaceHolderAttributesForId(masterId_1));
            expect(oldPlaceHolderAttributesLayout1).toEqual(model.drawingStyles.getAllPlaceHolderAttributesForId(layoutId_1));
            expect(oldPlaceHolderAttributesLayout2).toEqual(model.drawingStyles.getAllPlaceHolderAttributesForId(layoutId_2));

            expect(oldDrawingListStyles).toEqual(model.drawingStyles.getAllDrawingListStyleAttributes());
        });

    });

});
