/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { transformOpColor, Color } from '@/io.ox/office/editframework/utils/color';
import { getDrawingType } from '@/io.ox/office/drawinglayer/view/drawingframe';
import Selection from '@/io.ox/office/textframework/selection/selection';
import { ABSOLUTE_DRAWING_SELECTOR } from '@/io.ox/office/textframework/utils/dom';

import * as TextHelper from '~/text/apphelper';
import * as PresHelper from '~/presentation/apphelper';

// class Selection ========================================================

describe('Textframework class Selection', function () {

    // private helpers ----------------------------------------------------

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { document: { defaultTabStop: 1270, zoom: { value: 100 } }, page: { width: 21590, height: 27940, marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540, marginHeader: 1248, marginFooter: 1248 } } },
        { name: 'insertParagraph', start: [0] },
        { name: 'splitParagraph', start: [0, 0] },
        { name: 'insertText', text: 'Hello World', start: [1, 0] },
        { name: 'splitParagraph', start: [1, 0] },
        { name: 'setAttributes', start: [2], attrs: { paragraph: { pageBreakBefore: true } } },
        { name: 'splitParagraph', start: [2, 0] },
        { name: 'setAttributes', start: [3], attrs: { paragraph: { pageBreakBefore: true } } }
    ];

    const appPromise = TextHelper.createTextApp('ooxml', OPERATIONS);

    let selection;
    beforeAll(async () => {
        const { docModel } = await appPromise;
        docModel.getPageLayout().callInitialPageBreaks();
        selection = docModel.getSelection();
    });

    // constructor --------------------------------------------------------

    it('should subclass ModelObject', function () {
        expect(Selection).toBeSubClassOf(ModelObject);
    });

    describe('constructor', function () {
        it('should create a Selection class instance', function () {
            expect(selection).toBeInstanceOf(Selection);
        });
    });

    // public methods -----------------------------------------------------

    describe('method getFirstDocumentPosition', function () {
        it('should return correct first oxo position in document', function () {
            expect(selection.getFirstDocumentPosition()).toEqual([0, 0]);
        });
    });

    describe('method getLastDocumentPosition', function () {
        it('should return correct last oxo position in document', function () {
            expect(selection.getLastDocumentPosition()).toEqual([3, 11]);
        });
    });

    describe('method isAllSelected', function () {
        it('should not be true when not all is selected in document', function () {
            expect(selection.isAllSelected()).toBeFalse();
        });
    });

    describe('method selectAll', function () {
        beforeAll(function () {
            selection.selectAll();
        });

        it('should be true when all is selected in document', function () {
            expect(selection.isAllSelected()).toBeTrue();
        });
    });
});

// ========================================================================

describe('Presentation class Selection', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        selection = null,
        layoutId_1 = 'layout1',
        masterId_1 = 'master1',
        activeSlide = null,
        activeSlideId = null,
        drawings = null,
        slide_1_id = 'slide_1', // the ID of the first slide in document view
        text_para1_drawing1 = 'Paragraph 1 in the shape smileyFace',

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' },
                    defaultTextListStyles: {
                        l1: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 0 } },
                        l2: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 1270 } },
                        l3: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 2540 } }
                    }
                }
            },
            { name: 'insertMasterSlide', id: masterId_1 },

            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: 1905, top: 5918, width: 21590, height: 4083 } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: 'Title' },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: 3810, top: 10795, width: 17780, height: 4868 } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_1, text: 'Subtitle' },

            { name: 'insertSlide', start: [0], target: layoutId_1 },

            // inserting a drawing of type 'shape', preset shape attribute is 'smileyFace'
            { name: 'insertDrawing', attrs: { fill: { type: 'solid', color: Color.ACCENT1 }, line: { type: 'solid', style: 'solid', width: 26, color: transformOpColor(Color.ACCENT1, { shade: 50000 }) }, geometry: { presetShape: 'smileyFace' }, character: { color: Color.LIGHT1 }, drawing: { name: 'smileyFace', left: 4286, top: 8969, width: 6853, height: 5874 }, shape: { anchor: 'centered', anchorCentered: false, wordWrap: true, horzOverflow: 'overflow', vertOverflow: 'overflow', paddingRight: 685, paddingLeft: 685, paddingBottom: 587, paddingTop: 587 } }, start: [0, 0], type: 'shape' },
            { name: 'setAttributes', attrs: { shape: { anchor: 'centered' } }, start: [0, 0] },
            { name: 'insertParagraph', start: [0, 0, 0], attrs: { character: { color: Color.LIGHT1 }, paragraph: { alignment: 'center', bullet: { type: 'none' }, indentFirstLine: 0 } } },
            { name: 'insertText', text: text_para1_drawing1, start: [0, 0, 0, 0] },

            // inserting a drawing of type 'connector', preset shape attribute is 'line'
            { name: 'insertDrawing', attrs: { line: { type: 'solid', style: 'solid', width: 26, color: transformOpColor(Color.ACCENT1, { shade: 50000 }) }, geometry: { presetShape: 'line' }, character: { color: Color.LIGHT1 }, drawing: { name: 'line', left: 14287, top: 4762, width: 8229, height: 4657 }, shape: { anchor: 'centered', anchorCentered: false, wordWrap: true, horzOverflow: 'overflow', vertOverflow: 'overflow', paddingRight: 823, paddingLeft: 823, paddingBottom: 466, paddingTop: 466 } }, start: [0, 1], type: 'connector' },
            { name: 'setAttributes', attrs: { shape: { anchor: 'centered' } }, start: [0, 1] }
        ];

    PresHelper.createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        selection = model.getSelection();
    });

    // constructor --------------------------------------------------------

    it('should exist', function () {
        expect(Selection).toBeFunction();
    });

    describe('drawing selection preparations', function () {

        it('should create a Selection class instance', function () {
            expect(selection).toBeInstanceOf(Selection);
        });

        it('should have the first slide activated', function () {
            activeSlideId = model.getActiveSlideId();
            expect(activeSlideId).toBe(slide_1_id);
        });

        it('should find two drawings on the active slide', function () {
            activeSlide = model.getSlideById(activeSlideId);
            expect(activeSlide).toHaveLength(1);
            drawings = activeSlide.children(ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(2);
        });

        it('should have the correct drawing types specified at the drawing nodes', function () {
            expect(getDrawingType(drawings[0])).toBe("shape");  // the smiley face
            expect(getDrawingType(drawings[1])).toBe("connector");  // the line
        });
    });

    describe('method isTextCursor', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('isTextCursor');
        });

        it('should be a text cursor selection after loading the document', function () {
            expect(selection.isTextCursor()).toBeTrue(); // in a slide selection
        });
    });

    describe('method isTopLevelTextCursor', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('isTopLevelTextCursor');
        });

        it('should be a text cursor selection after loading the document', function () {
            expect(selection.isTopLevelTextCursor()).toBeTrue(); // a slide selection
        });
    });

    describe('method isSlideSelection', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('isSlideSelection');
        });

        it('should be a slide selection after loading the document', function () {
            expect(selection.isSlideSelection()).toBeTrue();
        });
    });

    describe('method isDrawingSelection', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('isDrawingSelection');
        });

        it('should not be a drawing selection after loading the document', function () {
            expect(selection.isDrawingSelection()).toBeFalse();
        });
    });

    describe('method getSelectedDrawing', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('getSelectedDrawing');
        });

        it('should return no drawing after loading the document (empty jQuery collection)', function () {
            expect(selection.getSelectedDrawing()).toHaveLength(0);
        });
    });

    describe('method getSelectedTextFrameDrawing', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('getSelectedTextFrameDrawing');
        });

        it('should return no drawing after loading the document (empty jQuery collection)', function () {
            expect(selection.getSelectedTextFrameDrawing()).toHaveLength(0);
        });
    });

    describe('method isAdditionalTextframeSelection', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('isAdditionalTextframeSelection');
        });

        // selecting the text in the drawing
        it('should be an additional text frame selection, if the cursor is set into the first drawing', function () {

            expect(selection.isAdditionalTextframeSelection()).toBeFalse();

            selection.setTextSelection([0, 0, 0, 0]);  // setting the cursor into the first drawing on the first slide

            expect(selection.isAdditionalTextframeSelection()).toBeTrue();
            expect(selection.isDrawingSelection()).toBeFalse();  // no drawing selection
            expect(selection.isSlideSelection()).toBeFalse(); // no longer a slide selection
            expect(selection.isTextCursor()).toBeTrue();  // a text cursor selection inside the drawing
            expect(selection.isTopLevelTextCursor()).toBeFalse(); // no longer a slide selection
        });

        it('should be the first drawing on the slide that is additionally selected', function () {
            var selectedDrawing = selection.getSelectedTextFrameDrawing();
            expect(selectedDrawing).toHaveLength(1);
            expect(selectedDrawing[0]).toBe(drawings[0]);  // checking that drawing nodes are identical

            selectedDrawing = selection.getSelectedDrawing();
            expect(selectedDrawing).toHaveLength(0); // no drawing selection
        });

        it('should have 8 resizer nodes at the additional drawing selection', function () {

            // getting the drawing node in the drawing selection overlay node
            var drawingSelectionNode = selection.getSelectedTextFrameDrawing().data('selection');
            expect(drawingSelectionNode).toHaveLength(1);

            // the 'selection' node is a child of the drawingSelectionNode
            var selectionNode = drawingSelectionNode.children('.selection');
            expect(selectionNode).toHaveLength(1);

            // the 'resizers' node is a child of the selection
            var resizersNode = selectionNode.children('.resizers');
            expect(resizersNode).toHaveLength(1);

            // the 'data-pos' nodes are children of the resizers node
            expect(resizersNode.children('div[data-pos]')).toHaveLength(8);  // 8 resizers
        });
    });

    // selecting the 'line' drawing
    describe('selecting the line drawing', function () {

        // selecting the drawing
        it('should be a drawing selection', function () {

            expect(selection.isAdditionalTextframeSelection()).toBeTrue();
            expect(selection.isDrawingSelection()).toBeFalse();  // no drawing selection

            selection.setTextSelection([0, 1], [0, 2]);  // selecting the line drawing on the first slide

            expect(selection.isAdditionalTextframeSelection()).toBeFalse();
            expect(selection.isDrawingSelection()).toBeTrue();  // drawing selection
            expect(selection.isSlideSelection()).toBeFalse(); // no longer a slide selection
            expect(selection.isTextCursor()).toBeFalse();  // a text cursor selection inside the drawing
            expect(selection.isTopLevelTextCursor()).toBeFalse(); // no longer a slide selection
        });

        it('should be the second drawing on the slide that is selected', function () {

            var selectedDrawing = selection.getSelectedTextFrameDrawing();
            expect(selectedDrawing).toHaveLength(0);

            selectedDrawing = selection.getSelectedDrawing();
            expect(selectedDrawing).toHaveLength(1); // no drawing selection
            expect(selectedDrawing[0]).toBe(drawings[1]);  // checking that drawing nodes are identical
        });

        it('should have only 2 resizer nodes at the additional drawing selection', function () {

            // getting the drawing node in the drawing selection overlay node
            var drawingSelectionNode = selection.getSelectedDrawing().data('selection');
            expect(drawingSelectionNode).toHaveLength(1);

            // the 'selection' node is a child of the drawingSelectionNode
            var selectionNode = drawingSelectionNode.children('.selection');
            expect(selectionNode).toHaveLength(1);

            // the 'resizers' node is a child of the selection
            var resizersNode = selectionNode.children('.resizers');
            expect(resizersNode).toHaveLength(1);

            // the 'data-pos' nodes are children of the resizers node
            expect(resizersNode.children('div[data-pos]')).toHaveLength(2);  // only 2 resizers
        });
    });

    // selecting the 'smileyFace' drawing
    describe('selecting the smiley drawing', function () {

        it('should be a drawing selection', function () {

            expect(selection.isAdditionalTextframeSelection()).toBeFalse();
            expect(selection.isDrawingSelection()).toBeTrue();  // drawing selection

            selection.setTextSelection([0, 0], [0, 1]);  // selecting the smiley face drawing on the first slide

            expect(selection.isAdditionalTextframeSelection()).toBeFalse();
            expect(selection.isDrawingSelection()).toBeTrue();  // drawing selection
            expect(selection.isSlideSelection()).toBeFalse(); // no longer a slide selection
            expect(selection.isTextCursor()).toBeFalse();  // a text cursor selection inside the drawing
            expect(selection.isTopLevelTextCursor()).toBeFalse(); // no longer a slide selection
        });

        it('should be the first drawing on the slide that is selected', function () {

            var selectedDrawing = selection.getSelectedTextFrameDrawing();
            expect(selectedDrawing).toHaveLength(0);

            selectedDrawing = selection.getSelectedDrawing();
            expect(selectedDrawing).toHaveLength(1); // no drawing selection
            expect(selectedDrawing[0]).toBe(drawings[0]);  // checking that drawing nodes are identical
        });

        it('should have 8 resizer nodes at the additional drawing selection', function () {

            // getting the drawing node in the drawing selection overlay node
            var drawingSelectionNode = selection.getSelectedDrawing().data('selection');
            expect(drawingSelectionNode).toHaveLength(1);

            // the 'selection' node is a child of the drawingSelectionNode
            var selectionNode = drawingSelectionNode.children('.selection');
            expect(selectionNode).toHaveLength(1);

            // the 'resizers' node is a child of the selection
            var resizersNode = selectionNode.children('.resizers');
            expect(resizersNode).toHaveLength(1);

            // the 'data-pos' nodes are children of the resizers node
            expect(resizersNode.children('div[data-pos]')).toHaveLength(8);  // 8 resizers
        });
    });

    // multi selection tests

    describe('method isMultiSelectionSupported', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('isMultiSelectionSupported');
        });

        it('should return true in presentation application', function () {
            expect(selection.isMultiSelectionSupported()).toBeTrue();
        });
    });

    describe('method isMultiSelection', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('isMultiSelection');
        });

        it('should return false, if only one drawing is selected', function () {
            expect(selection.isDrawingSelection()).toBeTrue();
            expect(selection.isMultiSelection()).toBeFalse();
        });
    });

    describe('method selectAllDrawingsOnSlide', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('selectAllDrawingsOnSlide');
        });

        it('should return the number of selected drawings on the slide', function () {
            expect(selection.selectAllDrawingsOnSlide()).toBe(2);
        });

        it('should be a multi selection and a drawing selection', function () {
            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.isDrawingSelection()).toBeTrue();
            expect(selection.isAdditionalTextframeSelection()).toBeFalse();
        });

        it('should generate two drawing selection nodes in the selection overlay node', function () {

            var drawingSelection1 = $(drawings[0]).data('selection');
            var drawingSelection2 = $(drawings[1]).data('selection');

            expect(drawingSelection1).toHaveLength(1);
            expect(drawingSelection2).toHaveLength(1);

            var selectionOverlayNode = drawingSelection1.parent();
            expect(selectionOverlayNode.hasClass('drawingselection-overlay')).toBeTrue();

            expect(drawingSelection1.parent()[0]).toBe(drawingSelection2.parent()[0]);

            expect(selectionOverlayNode.parent()[0]).toBe(model.getNode()[0]); // the parent node is the page

            expect(selectionOverlayNode.children()).toHaveLength(2); // two active drawing selections
        });

        it('should remove all drawing selections from the overlay node in a text selection', function () {

            selection.setTextSelection([0, 0, 0, 0]);  // setting the cursor into the first drawing on the first slide

            expect(selection.isAdditionalTextframeSelection()).toBeTrue();
            expect(selection.isDrawingSelection()).toBeFalse();  // no drawing selection
            expect(selection.isSlideSelection()).toBeFalse(); // no slide selection
            expect(selection.isTextCursor()).toBeTrue();  // a text cursor selection inside the drawing
            expect(selection.isMultiSelection()).toBeFalse(); // no multi selection

            // the first drawing is still selected as additional text frame selection
            var drawingSelection = $(drawings[0]).data('selection');
            expect(drawingSelection).toHaveLength(1);

            var selectionOverlayNode = drawingSelection.parent();
            expect(selectionOverlayNode.hasClass('drawingselection-overlay')).toBeTrue();

            expect(selectionOverlayNode.children()).toHaveLength(1); // one active drawing selection

            // the 'data-pos' nodes in the selection ndoe
            expect(drawingSelection.find('div[data-pos]')).toHaveLength(12); // 12 'data-pos' nodes at all

            // the 'data-pos' nodes are children of the resizers node
            expect(drawingSelection.find('.borders > div[data-pos]')).toHaveLength(4); // 4 'data-pos' nodes as borders

            // the 'data-pos' nodes are children of the resizers node
            expect(drawingSelection.find('.resizers > div[data-pos]')).toHaveLength(8); // 8 'data-pos' nodes as resizers
        });
    });
});
