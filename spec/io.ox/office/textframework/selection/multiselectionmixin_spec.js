/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { Color } from '@/io.ox/office/editframework/utils/color';
import { getDrawingRotationAngle } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { isBodyPlaceHolderDrawing, isTitleOrSubtitlePlaceHolderDrawing } from '@/io.ox/office/presentation/utils/placeholderutils';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import MultiSelectionMixin from '@/io.ox/office/textframework/selection/multiselectionmixin';

import { createPresentationApp } from '~/presentation/apphelper';

// class MultiSelectionMixin ==============================================

describe('Textframework class MultiSelectionMixin', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        selection = null,
        layoutId_1 = 'layout1',
        layoutId_2 = 'layout2',
        masterId_1 = 'master1',
        ctrTitleLeft = 1900,
        subTitleLeft = 3800,
        drawing1Left = 2000,
        ctrTitleTop = 5900,
        subTitleTop = 10800,
        drawing1Top = 6000,
        ctrTitleHeight = 4000,
        subTitleHeight = 5000,
        drawing1Height = 3600,
        ctrTitleWidth = 21600,
        subTitleWidth = 17800,
        drawing1Width = 6000,
        drawingDelta = 100, // shift in 1/100 mm
        drawingSizePercentage = 0.05, // 5% of increase or decrease

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: { page: { width: 33866, height: 19050, orientation: 'landscape' } }
            },
            { name: 'insertMasterSlide', id: masterId_1 },

            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: ctrTitleLeft, top: ctrTitleTop, width: ctrTitleWidth, height: ctrTitleHeight } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: 'Mastertitelformat bearbeiten' },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: subTitleLeft, top: subTitleTop, width: subTitleWidth, height: subTitleHeight } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_1, text: 'Master-Untertitelformat bearbeiten' },

            { name: 'insertLayoutSlide', id: layoutId_2, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_2, type: 'shape', attrs: { presentation: { phType: 'title' }, drawing: { name: 'Titel 1' } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_2 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_2, text: 'Mastertitelformat bearbeiten' },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_2, type: 'shape', attrs: { presentation: { phIndex: 1 }, drawing: { name: 'Drawing 2', left: 1270, top: 4445, width: 11218, height: 12572 } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_2, attrs: { paragraph: { level: 0 } } },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_2, text: 'Mastertextformat bearbeiten' },
            { name: 'insertDrawing', start: [0, 2], target: layoutId_2, type: 'shape', attrs: { presentation: { phIndex: 2 }, drawing: { name: 'Drawing 3', left: 12912, top: 4445, width: 11218, height: 12572 } } },
            { name: 'insertParagraph', start: [0, 2, 0], target: layoutId_2, attrs: { paragraph: { level: 0 } } },
            { name: 'insertText', start: [0, 2, 0, 0], target: layoutId_2, text: 'Mastertextformat bearbeiten' },

            { name: 'insertSlide', start: [0], target: layoutId_1 },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1' } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertText', start: [0, 0, 0, 0], text: 'Hello paragraph 1 in drawing 1' },
            { name: 'insertParagraph', start: [0, 0, 1] },
            { name: 'insertText', start: [0, 0, 1, 0], text: 'Hello paragraph 2 in drawing 1' },
            { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2' } } },
            { name: 'insertParagraph', start: [0, 1, 0] },
            { name: 'insertText', start: [0, 1, 0, 0], text: 'Hello paragraph 1 in drawing 2' },
            { name: 'insertDrawing', start: [0, 2], type: 'shape', attrs: { presentation: { phType: 'body', phIndex: 1 }, drawing: { name: 'Text body', left: drawing1Left, top: drawing1Top, width: drawing1Width, height: drawing1Height } } },
            { name: 'insertParagraph', start: [0, 2, 0] },
            { name: 'insertText', start: [0, 2, 0, 0], text: 'Hello paragraph 1 in drawing 3' }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        selection = model.getSelection();
    });

    function getDrawingsOnSlide() {
        const activeSlide = model.getSlideById(model.getActiveSlideId());
        return activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
    }

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(MultiSelectionMixin).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('method isMultiSelection', function () {
        it('should exist', function () {
            expect(selection).toRespondTo('isMultiSelection');
        });
    });

    describe('method getMultiSelection', function () {
        it('should exist', function () {
            expect(selection).toRespondTo('getMultiSelection');
        });
    });

    describe('method getMultiSelectionCount', function () {
        it('should exist', function () {
            expect(selection).toRespondTo('getMultiSelectionCount');
        });
    });

    describe('method selectAllDrawingsOnSlide', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('selectAllDrawingsOnSlide');
        });

        it('should select all drawings on the slide', function () {

            selection.setTextSelection([0, 0, 0, 0]);  // selecting the first slide

            expect(selection.isMultiSelection()).toBeFalse();

            expect(getDrawingsOnSlide()).toHaveLength(3);

            selection.selectAllDrawingsOnSlide();

            expect(selection.isMultiSelection()).toBeTrue();

            expect(selection.getMultiSelectionCount()).toBe(3);

            selection.setTextSelection([0, 0, 0, 0]);  // selecting the text position again -> no more multiple selection

            expect(selection.isMultiSelection()).toBeFalse();

            expect(selection.getMultiSelectionCount()).toBe(0);

            selection.selectAllDrawingsOnSlide();  // and switching back to multiple selection

            expect(selection.isMultiSelection()).toBeTrue();

            expect(selection.getMultiSelectionCount()).toBe(3);
        });
    });

    describe('method getListOfPositions', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('getListOfPositions');
        });

        it('should return the list of all start positions of the selected drawings as string', function () {
            expect(selection.getListOfPositions()).toBe("(0,0),(0,1),(0,2)");
        });
    });

    describe('method addOneDrawingIntoMultipleSelection', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('addOneDrawingIntoMultipleSelection');
        });

        it('should add one drawing into the list of multiple selections', function () {

            selection.setTextSelection([0, 0, 0, 0]);  // selecting the text position again -> no more multiple selection
            expect(selection.isMultiSelection()).toBeFalse();
            expect(selection.getMultiSelectionCount()).toBe(0);

            const drawingsOnSlide = getDrawingsOnSlide();
            selection.addOneDrawingIntoMultipleSelection(drawingsOnSlide[0]);
            selection.addOneDrawingIntoMultipleSelection(drawingsOnSlide[2]);

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(2);
            expect(selection.getListOfPositions()).toBe("(0,0),(0,2)");
        });
    });

    describe('method removeOneDrawingFromMultipleSelection', function () {

        it('should exist', function () {
            expect(selection).toRespondTo('removeOneDrawingFromMultipleSelection');
        });

        it('should remove one drawing from the list of multiple selections', function () {

            const drawingsOnSlide = getDrawingsOnSlide();
            selection.removeOneDrawingFromMultipleSelection(drawingsOnSlide[0]);
            selection.addOneDrawingIntoMultipleSelection(drawingsOnSlide[1]);

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(2);
            // expect(selection.getListOfPositions()).toBe("(0,1),(0,2)"); // TODO
        });
    });

    // setting character attributes to all selected drawings
    describe('method setAttribute', function () {

        it('should exist', function () {
            expect(model).toRespondTo('setAttribute');
        });

        it('should assign character attributes to all selected drawings in a multiple drawing selection', function () {

            const drawingsOnSlide = getDrawingsOnSlide();
            var spansInDrawing1 = $(drawingsOnSlide[0]).find('div.p > span'),
                spansInDrawing2 = $(drawingsOnSlide[1]).find('div.p > span'),
                spansInDrawing3 = $(drawingsOnSlide[2]).find('div.p > span');

            selection.setTextSelection([0, 0, 0, 0]);  // selecting the text position again -> no more multiple selection
            expect(selection.isMultiSelection()).toBeFalse();
            expect(selection.getMultiSelectionCount()).toBe(0);

            expect(spansInDrawing1).toHaveLength(2);
            expect(spansInDrawing2).toHaveLength(1);
            expect(spansInDrawing3).toHaveLength(1);

            expect($(spansInDrawing1[0]).data('attributes')).toBeUndefined();
            expect($(spansInDrawing1[1]).data('attributes')).toBeUndefined();
            expect($(spansInDrawing2[0]).data('attributes')).toBeUndefined();
            expect($(spansInDrawing3[0]).data('attributes')).toBeUndefined();

            selection.selectAllDrawingsOnSlide();
            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(3);

            model.setAttribute('character', 'bold', true); // setting character attribute 'bold' to all selected drawings

            expect($(spansInDrawing1[0]).data('attributes').character.bold).toBeTrue();
            expect($(spansInDrawing1[1]).data('attributes').character.bold).toBeTrue();
            expect($(spansInDrawing2[0]).data('attributes').character.bold).toBeTrue();
            expect($(spansInDrawing3[0]).data('attributes').character.bold).toBeTrue();
        });

    });

    // delete all selected drawings
    describe('method deleteSelected', function () {

        it('should exist', function () {
            expect(model).toRespondTo('deleteSelected');
        });

        it('should delete all selected drawings in a multiple drawing selection', async function () {

            selection.setTextSelection([0, 0, 0, 0]);  // selecting the text position again -> no more multiple selection
            expect(selection.isMultiSelection()).toBeFalse();
            expect(selection.getMultiSelectionCount()).toBe(0);

            selection.selectAllDrawingsOnSlide();

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(3);

            let drawingsOnSlide = getDrawingsOnSlide();
            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[0])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[1])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[2])).toBeFalse();

            await model.deleteSelected({ deleteKey: true }); // deleting all selected drawings with 'delete' or 'backspace'

            drawingsOnSlide = getDrawingsOnSlide();
            expect(drawingsOnSlide).toHaveLength(3);  // all non-empty place holder drawing are replaced by empty place holder drawings

            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[0])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[1])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[2])).toBeTrue();

            selection.selectAllDrawingsOnSlide();

            await model.deleteSelected({ deleteKey: true }); // deleting all selected drawings with 'delete' or 'backspace'

            drawingsOnSlide = getDrawingsOnSlide();
            expect(drawingsOnSlide).toHaveLength(0);  // all empty place holder drawing are removed

            // restoring the drawings again
            await model.getUndoManager().undo();

            drawingsOnSlide = getDrawingsOnSlide();
            expect(drawingsOnSlide).toHaveLength(3);

            selection.selectAllDrawingsOnSlide();
            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(3);

            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[0])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[1])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[2])).toBeTrue();

            // restoring the drawings again
            await model.getUndoManager().undo();

            drawingsOnSlide = getDrawingsOnSlide();
            expect(drawingsOnSlide).toHaveLength(3);

            selection.selectAllDrawingsOnSlide();
            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(3);

            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[0])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[1])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(drawingsOnSlide[2])).toBeFalse();
        });
    });

    // moving all selected drawings
    describe('method handleDrawingOperations', function () {

        it('should exist', function () {
            expect(model).toRespondTo('handleDrawingOperations');
        });

        it('should move all selected drawings in a multiple drawing selection', function () {

            selection.setTextSelection([0, 0, 0, 0]);  // selecting the text position again -> no more multiple selection
            expect(selection.isMultiSelection()).toBeFalse();
            expect(selection.getMultiSelectionCount()).toBe(0);

            selection.selectAllDrawingsOnSlide();

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(3);

            const drawingsOnSlide = getDrawingsOnSlide();
            expect(drawingsOnSlide).toHaveLength(3);

            // reading the drawing attributes for 'left' -> no explicit attributes set at drawing node (excepot for one drawing)
            expect($(drawingsOnSlide[0]).data('attributes').drawing.left).toBeUndefined();
            expect($(drawingsOnSlide[1]).data('attributes').drawing.left).toBeUndefined();
            expect($(drawingsOnSlide[2]).data('attributes').drawing.left).toBe(drawing1Left);

            model.handleDrawingOperations({ ctrlKey: false, shiftKey: false, metaKey: false, keyCode: 39 }); // cursor right

            // reading the drawing attributes for 'left' from the explicit drawing attributes
            expect($(drawingsOnSlide[0]).data('attributes').drawing.left).toBe(ctrTitleLeft + drawingDelta); // new values calculated with merged attributes from 'ctrTitle'
            expect($(drawingsOnSlide[1]).data('attributes').drawing.left).toBe(subTitleLeft + drawingDelta); // new values calculated with merged attributes from 'subTitle'
            expect($(drawingsOnSlide[2]).data('attributes').drawing.left).toBe(drawing1Left + drawingDelta);

            model.handleDrawingOperations({ ctrlKey: false, shiftKey: false, metaKey: false, keyCode: 39 }); // cursor right

            expect($(drawingsOnSlide[0]).data('attributes').drawing.left).toBe(ctrTitleLeft + 2 * drawingDelta); // reading explicit attributes at drawing
            expect($(drawingsOnSlide[1]).data('attributes').drawing.left).toBe(subTitleLeft + 2 * drawingDelta); // reading explicit attributes at drawing
            expect($(drawingsOnSlide[2]).data('attributes').drawing.left).toBe(drawing1Left + 2 * drawingDelta);

            // double velocity with 'ctrl'-key pressed
            model.handleDrawingOperations({ ctrlKey: true, shiftKey: false, metaKey: false, keyCode: 39 }); // cursor right + ctrl key

            expect($(drawingsOnSlide[0]).data('attributes').drawing.left).toBe(ctrTitleLeft + 4 * drawingDelta); // reading explicit attributes at drawing
            expect($(drawingsOnSlide[1]).data('attributes').drawing.left).toBe(subTitleLeft + 4 * drawingDelta); // reading explicit attributes at drawing
            expect($(drawingsOnSlide[2]).data('attributes').drawing.left).toBe(drawing1Left + 4 * drawingDelta);

            expect(selection.isMultiSelection()).toBeTrue();
            expect(drawingsOnSlide).toHaveLength(3);
        });

        it('should resize all selected drawings in a multiple drawing selection', function () {

            selection.setTextSelection([0, 0, 0, 0]);  // selecting the text position again -> no more multiple selection
            expect(selection.isMultiSelection()).toBeFalse();
            expect(selection.getMultiSelectionCount()).toBe(0);

            selection.selectAllDrawingsOnSlide();

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(3);

            const drawingsOnSlide = getDrawingsOnSlide();
            expect(drawingsOnSlide).toHaveLength(3);

            // reading the drawing attributes for 'top' and 'height' -> get explicit attributes because there position is not longer extended by their placeholder
            expect($(drawingsOnSlide[0]).data('attributes').drawing.top).toBe(ctrTitleTop);
            expect($(drawingsOnSlide[1]).data('attributes').drawing.top).toBe(subTitleTop);
            expect($(drawingsOnSlide[2]).data('attributes').drawing.top).toBe(drawing1Top);

            model.handleDrawingOperations({ ctrlKey: false, shiftKey: true, metaKey: false, keyCode: 38 }); // cursor top + shift key

            // reading the drawing attributes for 'top' and 'height' from the explicit drawing attributes
            expect($(drawingsOnSlide[0]).data('attributes').drawing.top).toBe(ctrTitleTop - 0.5 * drawingSizePercentage * ctrTitleHeight); // half of 5% of ctrTitleHeight
            expect($(drawingsOnSlide[0]).data('attributes').drawing.height).toBe(ctrTitleHeight + drawingSizePercentage * ctrTitleHeight); // 5% of ctrTitleHeight
            expect($(drawingsOnSlide[1]).data('attributes').drawing.top).toBe(subTitleTop - 0.5 * drawingSizePercentage * subTitleHeight); // half of 5% of subTitleHeight
            expect($(drawingsOnSlide[1]).data('attributes').drawing.height).toBe(subTitleHeight + drawingSizePercentage * subTitleHeight); // 5% of subTitleHeight
            expect($(drawingsOnSlide[2]).data('attributes').drawing.top).toBe(drawing1Top - 0.5 * drawingSizePercentage * drawing1Height); // half of 5% of drawing1Height
            expect($(drawingsOnSlide[2]).data('attributes').drawing.height).toBe(drawing1Height + drawingSizePercentage * drawing1Height); // 5% of drawing1Height

            // refresh global variables for following test
            ctrTitleLeft = $(drawingsOnSlide[0]).data('attributes').drawing.left;
            subTitleLeft = $(drawingsOnSlide[1]).data('attributes').drawing.left;
            drawing1Left = $(drawingsOnSlide[2]).data('attributes').drawing.left;

            model.handleDrawingOperations({ ctrlKey: false, shiftKey: true, metaKey: false, keyCode: 39 }); // cursor right + shift key

            // reading the drawing attributes for 'left' and 'width' from the explicit drawing attributes
            expect($(drawingsOnSlide[0]).data('attributes').drawing.left).toBe(ctrTitleLeft - 0.5 * drawingSizePercentage * ctrTitleWidth); // half of 5% of ctrTitleWidth
            expect($(drawingsOnSlide[0]).data('attributes').drawing.width).toBe(ctrTitleWidth + drawingSizePercentage * ctrTitleWidth); // 5% of ctrTitleWidth
            expect($(drawingsOnSlide[1]).data('attributes').drawing.left).toBe(subTitleLeft - 0.5 * drawingSizePercentage * subTitleWidth); // half of 5% of subTitleWidth
            expect($(drawingsOnSlide[1]).data('attributes').drawing.width).toBe(subTitleWidth + drawingSizePercentage * subTitleWidth); // 5% of subTitleWidth
            expect($(drawingsOnSlide[2]).data('attributes').drawing.left).toBe(drawing1Left - 0.5 * drawingSizePercentage * drawing1Width); // half of 5% of drawing1Width
            expect($(drawingsOnSlide[2]).data('attributes').drawing.width).toBe(drawing1Width + drawingSizePercentage * drawing1Width); // 5% of drawing1Width

            expect(selection.isMultiSelection()).toBeTrue();
            expect(drawingsOnSlide).toHaveLength(3);
        });

        it('should rotate all selected drawings in a multiple drawing selection', function () {

            selection.setTextSelection([0, 0, 0, 0]);  // selecting the text position again -> no more multiple selection
            expect(selection.isMultiSelection()).toBeFalse();
            expect(selection.getMultiSelectionCount()).toBe(0);

            selection.selectAllDrawingsOnSlide();

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(3);

            const drawingsOnSlide = getDrawingsOnSlide();
            expect(drawingsOnSlide).toHaveLength(3);

            expect($(drawingsOnSlide[0]).data('attributes').drawing.rotation).toBe(0);
            expect($(drawingsOnSlide[1]).data('attributes').drawing.rotation).toBe(0);
            expect($(drawingsOnSlide[2]).data('attributes').drawing.rotation).toBe(0);

            model.handleDrawingOperations({ ctrlKey: false, shiftKey: false, metaKey: false, altKey: true, keyCode: 39 }); // alt + right

            // reading the drawing attributes for 'rotate' from the explicit drawing attributes
            expect($(drawingsOnSlide[0]).data('attributes').drawing.rotation).toBe(15);
            expect($(drawingsOnSlide[1]).data('attributes').drawing.rotation).toBe(15);
            expect($(drawingsOnSlide[2]).data('attributes').drawing.rotation).toBe(15);

            model.handleDrawingOperations({ ctrlKey: false, shiftKey: false, metaKey: false, altKey: true, keyCode: 39 }); // alt + right

            // test also the getter function for rotation angle
            expect(getDrawingRotationAngle(model, drawingsOnSlide[0])).toBe(30);
            expect(getDrawingRotationAngle(model, drawingsOnSlide[1])).toBe(30);
            expect(getDrawingRotationAngle(model, drawingsOnSlide[2])).toBe(30);

            model.handleDrawingOperations({ ctrlKey: false, shiftKey: false, metaKey: false, altKey: true, keyCode: 37 }); // alt + left

            // reading the drawing attributes for 'rotate' from the explicit drawing attributes
            expect($(drawingsOnSlide[0]).data('attributes').drawing.rotation).toBe(15);
            expect($(drawingsOnSlide[1]).data('attributes').drawing.rotation).toBe(15);
            expect($(drawingsOnSlide[2]).data('attributes').drawing.rotation).toBe(15);

            model.handleDrawingOperations({ ctrlKey: false, shiftKey: false, metaKey: false, altKey: true, keyCode: 37 }); // alt + left

            // test also the getter function for rotation angle
            expect(getDrawingRotationAngle(model, drawingsOnSlide[0])).toBeNull();
            expect(getDrawingRotationAngle(model, drawingsOnSlide[1])).toBeNull();
            expect(getDrawingRotationAngle(model, drawingsOnSlide[2])).toBeNull();

            expect(selection.isMultiSelection()).toBeTrue();
            expect(drawingsOnSlide).toHaveLength(3);
        });

    });

    // getting and setting attributes to all selected drawings
    describe('applying attributes to all selected drawings', function () {

        it('should generate setAttributes operations for all affected drawings', function () {

            selection.setTextSelection([0, 0, 0, 0]);  // selecting the text position

            let drawingsOnSlide = getDrawingsOnSlide();
            expect(drawingsOnSlide).toHaveLength(3);

            expect(selection.isMultiSelection()).toBeFalse();
            expect(selection.getMultiSelectionCount()).toBe(0);

            selection.selectAllDrawingsOnSlide();

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(3);

            expect(drawingsOnSlide).toHaveLength(3);

            // ctrTitle, subTitle and body -> all support background color
            expect(model.isPlaceHolderDrawing(drawingsOnSlide[0])).toBeTrue();
            expect(model.isPlaceHolderDrawing(drawingsOnSlide[1])).toBeTrue();
            expect(model.isPlaceHolderDrawing(drawingsOnSlide[2])).toBeTrue();

            expect(isTitleOrSubtitlePlaceHolderDrawing(drawingsOnSlide[0])).toBeTrue();
            expect(isTitleOrSubtitlePlaceHolderDrawing(drawingsOnSlide[1])).toBeTrue();
            expect(isBodyPlaceHolderDrawing(drawingsOnSlide[2])).toBeTrue();

            // adding a line (not supporting background color)
            model.insertShape('line', { left: 1000, top: 1000, width: 1000, height: 1000 });

            drawingsOnSlide = getDrawingsOnSlide();
            expect(drawingsOnSlide).toHaveLength(4);

            selection.selectAllDrawingsOnSlide();
            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(4);

            let operationsCount = model.getOperationsCount();

            // setting background color (only three operations are generated, no operation for the line)
            model.setDrawingFillColor(Color.RED);
            expect(model.getOperationsCount()).toBe(operationsCount + 3); // check for 3 operations

            operationsCount = model.getOperationsCount();

            // setting border color (to all four drawings)
            model.setDrawingBorderColor(Color.BLUE);
            expect(model.getOperationsCount()).toBe(operationsCount + 4); // check for 4 operations

        });

        // getting drawing attributes from all selected drawings
        it('should get merged drawing attributes from selected drawings', function () {

            selection.setTextSelection([0, 0, 0, 0]);  // selecting the text position

            const drawingsOnSlide = getDrawingsOnSlide();
            expect(drawingsOnSlide).toHaveLength(4);

            // select three drawings
            selection.setMultiDrawingSelectionByPosition([[0, 0], [0, 1], [0, 2]]);

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(3);

            let allAttributes = model.getAttributes('drawing');
            expect(allAttributes.fill.type).toBe("solid");
            expect(allAttributes.fill.color).toEqual(Color.RED); // still red from previous test

            // even if the line is part of the selection, the fill color shall be red (ignoring the value of the line)
            selection.selectAllDrawingsOnSlide();

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(4);

            allAttributes = model.getAttributes('drawing');
            expect(allAttributes.fill.type).toBe("solid");
            expect(allAttributes.fill.color).toEqual(Color.RED); // still red from previous test

            // modifying the color of two selected drawings
            selection.setMultiDrawingSelectionByPosition([[0, 0], [0, 1]]);
            expect(selection.getMultiSelectionCount()).toBe(2);

            const operationsCount = model.getOperationsCount();

            // setting a different background color to two drawings
            model.setDrawingFillColor(Color.GREEN);
            expect(model.getOperationsCount()).toBe(operationsCount + 2); // check for 2 operations

            // select the three drawings again
            selection.setMultiDrawingSelectionByPosition([[0, 0], [0, 1], [0, 2]]);
            expect(selection.getMultiSelectionCount()).toBe(3);

            allAttributes = model.getAttributes('drawing');
            expect(allAttributes.fill.type).toBe("solid");
            expect(allAttributes.fill.color).toBeNull();
        });

        // getting character attributes from all selected drawings
        it('should get merged character attributes from selected drawings', function () {

            selection.setTextSelection([0, 0, 0, 0]);  // selecting the text position

            const drawingsOnSlide = getDrawingsOnSlide();
            expect(drawingsOnSlide).toHaveLength(4);

            // select three drawings
            selection.setMultiDrawingSelectionByPosition([[0, 0], [0, 1], [0, 2]]);

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(3);

            let allAttributes = model.getAttributes('character');

            expect(allAttributes.character.italic).toBeFalse(); // no text in italic

            // setting italic to two selected drawings
            selection.setMultiDrawingSelectionByPosition([[0, 0], [0, 1]]);
            expect(selection.getMultiSelectionCount()).toBe(2);

            const operationsCount = model.getOperationsCount();

            // setting character attribute italic to only two drawings
            model.setAttribute('character', 'italic', true);
            expect(model.getOperationsCount()).toBe(operationsCount + 3); // check for 3 operations in 2 drawings (3 paragraphs)

            // select the three drawings again
            selection.setMultiDrawingSelectionByPosition([[0, 0], [0, 1], [0, 2]]);
            expect(selection.getMultiSelectionCount()).toBe(3);

            allAttributes = model.getAttributes('character');

            expect(allAttributes.character.italic).toBeNull();  // italic is no longer the unique state
        });

    });

});
