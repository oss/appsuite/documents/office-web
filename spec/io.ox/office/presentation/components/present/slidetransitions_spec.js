/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Color } from '@/io.ox/office/editframework/utils/color';
import PresentationModeManager from '@/io.ox/office/presentation/components/present/presentationmodemanager';
import { SlideTransitions } from '@/io.ox/office/presentation/components/present/slidetransitions';

import { createPresentationApp } from '~/presentation/apphelper';

// class SlideTransitions =================================================

describe('Presentation class SlideTransitions', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        presentationModeManager = null,
        slideTransitions = null,

        layoutId_1 = 'layout1',
        masterId_1 = 'master1',
        text_slide_1 = 'Slide 1',
        text_slide_2 = 'Slide 2',

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' },
                    defaultTextListStyles: {
                        l1: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 0 } },
                        l2: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 1270 } },
                        l3: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 2540 } }
                    }
                }
            },
            { name: 'insertMasterSlide', id: masterId_1 },

            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: 1905, top: 5918, width: 21590, height: 4083 } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: 'Mastertitelformat bearbeiten' },

            { name: 'insertSlide', start: [0], target: layoutId_1 },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow left', left: 5027, top: 6725, width: 9419, height: 6509 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.RED } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertText', start: [0, 0, 0, 0], text: text_slide_1 },

            { name: 'insertSlide', start: [1], target: layoutId_1 },
            { name: 'insertDrawing', start: [1, 0], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow left', left: 5027, top: 6725, width: 9419, height: 6000 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.RED } } },
            { name: 'insertParagraph', start: [1, 0, 0] },
            { name: 'insertText', start: [1, 0, 0, 0], text: text_slide_2 }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // existence check ----------------------------------------------------

    it('should be a class', function () {
        expect(SlideTransitions).toBeClass();
    });

    it('should return a PresentationModeManager instance from the model', function () {
        presentationModeManager = model.getPresentationModeManager();
        expect(presentationModeManager).toBeInstanceOf(PresentationModeManager);
    });

    it('should return a SlideTransitions instance from the PresentationModeManager', function () {
        slideTransitions = presentationModeManager.getSlideTransitions();
        expect(slideTransitions).toBeInstanceOf(SlideTransitions);
    });

    // public methods -----------------------------------------------------

    describe('method getActiveSlideTransitionId', function () {

        it('should exist', function () {
            expect(SlideTransitions).toHaveMethod('getActiveSlideTransitionId');
        });

        it('should return the fast swiper transition as default transition for touch devices', function () {
            expect(slideTransitions.getActiveSlideTransitionId()).toBeNull(); // no touch device
        });

    });

    describe('method getActiveSlideTransition', function () {

        it('should exist', function () {
            expect(SlideTransitions).toHaveMethod('getActiveSlideTransition');
        });

        it('should return the fast swiper transition object as default transition for touch devices', function () {
            expect(slideTransitions.getActiveSlideTransition()).toBeNull(); // no touch device
        });

    });

    describe('method isActiveSlideTransition', function () {

        it('should exist', function () {
            expect(SlideTransitions).toHaveMethod('isActiveSlideTransition');
        });

        it('should return that there is an active transition as default transition for touch devices', function () {
            expect(slideTransitions.isActiveSlideTransition()).toBeFalse(); // no touch device
        });

    });

});
