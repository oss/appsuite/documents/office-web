/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { TOUCH_DEVICE, KeyCode } from '@/io.ox/office/tk/dom';
import { EObject } from '@/io.ox/office/tk/objects';

import { Color } from '@/io.ox/office/editframework/utils/color';
import PresentationModeManager from '@/io.ox/office/presentation/components/present/presentationmodemanager';
import * as DOM from '@/io.ox/office/textframework/utils/dom';

import { createPresentationApp } from '~/presentation/apphelper';

// class PresentationModeManager ==========================================

describe('Presentation class PresentationModeManager', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        docView = null,
        presentationModeManager = null,

        layoutId_1 = 'layout1',
        masterId_1 = 'master1',
        slide_1_id = 'slide_1',
        slide_2_id = 'slide_2',
        slide_3_id = 'slide_3',
        slide_4_id = 'slide_4',
        text_slide_1 = 'Slide 1',
        text_slide_2 = 'Slide 2',
        text_slide_3 = 'Slide 3',
        text_slide_4 = 'Slide 4',

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' },
                    defaultTextListStyles: {
                        l1: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 0 } },
                        l2: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 1270 } },
                        l3: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 2540 } }
                    }
                }
            },
            { name: 'insertMasterSlide', id: masterId_1 },

            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: 1905, top: 5918, width: 21590, height: 4083 } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: 'Mastertitelformat bearbeiten' },

            { name: 'insertSlide', start: [0], target: layoutId_1 },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow left', left: 5027, top: 6725, width: 9419, height: 6509 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.RED } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertText', start: [0, 0, 0, 0], text: text_slide_1 },

            { name: 'insertSlide', start: [1], target: layoutId_1 },
            { name: 'insertDrawing', start: [1, 0], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow left', left: 5027, top: 6725, width: 9419, height: 6000 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.RED } } },
            { name: 'insertParagraph', start: [1, 0, 0] },
            { name: 'insertText', start: [1, 0, 0, 0], text: text_slide_2 },

            { name: 'insertSlide', start: [2], target: layoutId_1 },
            { name: 'insertDrawing', start: [2, 0], type: 'shape', attrs: { presentation: { phType: 'ctrTitle', phIndex: 10 }, drawing: { name: 'Titel 1', left: 1000, top: 2000, width: 4000, height: 2000 } } },
            { name: 'insertParagraph', start: [2, 0, 0] },
            { name: 'insertText', start: [2, 0, 0, 0], text: text_slide_3 },

            { name: 'insertSlide', start: [3], target: layoutId_1 },
            { name: 'insertDrawing', start: [3, 0], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow left', left: 1027, top: 6725, width: 9419, height: 6000 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.RED } } },
            { name: 'insertParagraph', start: [3, 0, 0] },
            { name: 'insertText', start: [3, 0, 0, 0], text: text_slide_4 }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        docView = app.getView();
    });

    // existence check ----------------------------------------------------

    it('should be subclass of EObject', function () {
        expect(PresentationModeManager).toBeSubClassOf(EObject);
    });

    it('should return a PresentationModeManager instance from the model', function () {
        presentationModeManager = model.getPresentationModeManager();
        expect(presentationModeManager).toBeInstanceOf(PresentationModeManager);
    });

    // public methods -----------------------------------------------------

    describe('method isPresentationMode', function () {

        it('should exist', function () {
            expect(PresentationModeManager).toHaveMethod('isPresentationMode');
        });

        it('should return false after loading a document', function () {
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
        });

    });

    describe('method startPresentation', function () {

        it('should exist', function () {
            expect(PresentationModeManager).toHaveMethod('startPresentation');
        });

        it('should start the presentation mode', function () {
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
        });

        it('should activate the first slide', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });

    });

    describe('method handleKeydownInPresentationMode', function () {

        it('should exist', function () {
            expect(PresentationModeManager).toHaveMethod('handleKeydownInPresentationMode');
        });

        it('should switch to the second slide when right arrow was pressed', function () {
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_2_id);
        });

        it('should switch to the first slide when left arrow was pressed', function () {
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.LEFT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });

        it('should switch to the last slide when the "end" key was pressed', function () {
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.END });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
        });

        it('should switch to the first slide when the "home" key was pressed', function () {
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.HOME });
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });

        it('should not leave the presentation mode after right arrow key on the last slide', function () {
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.END });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id); // still active
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
        });

        it('should return to the last slide after left arrow key on the post-last slide', function () {
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.LEFT_ARROW });
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
        });

        it('should leave the presentation mode when pressing twice the right arrow key on the last slide', function () {
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
        });

        it('should leave the presentation mode at the current slide when pressing "Escape"', function () {
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_2_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_3_id);
        });

    });

    describe('method startPresentation without parameter"', function () {

        it('should start the presentation mode on the first slide', function () {
            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });

        it('should return to last active slide if presentation is completed', function () {
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.END });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_3_id);
        });

    });

    describe('method startPresentation with parameter "startMode"', function () {

        it('should start the presentation on the current slide', function () {
            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.startPresentation({ startMode: 'CURRENT' });
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_3_id);
        });

        it('should return to the start slide if presentation is completed', function () {
            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_3_id);
        });
    });

    describe('method showSlideAnimation', function () {

        it('should exist', function () {
            expect(PresentationModeManager).toHaveMethod('showSlideAnimation');
        });

        it('should have a default value of true for touch devices, otherwise false (after starting the presentation mode)', function () {

            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.startPresentation({ startMode: 'FIRST' });
            expect(presentationModeManager.isPresentationMode()).toBeTrue();

            if (TOUCH_DEVICE) {
                expect(presentationModeManager.showSlideAnimation()).toBeTrue();
            } else {
                expect(presentationModeManager.showSlideAnimation()).toBeFalse();
            }

            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });

    });

    describe('method getActiveSlideEffectId', function () {

        it('should exist', function () {
            expect(PresentationModeManager).toHaveMethod('getActiveSlideEffectId');
        });

        it('should return a specified id for touch devices, otherwise null', function () {
            if (TOUCH_DEVICE) {
                expect(presentationModeManager.getActiveSlideEffectId()).not.toBeNull();
            } else {
                expect(presentationModeManager.getActiveSlideEffectId()).toBeNull();
            }
        });

    });

    describe('method startPresentation without specified fullscreen option', function () {

        it('should start the presentation in full screen mode', async function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            await presentationModeManager.startPresentation();
            expect(presentationModeManager.useFullscreenPresentation()).toBeTrue(); // not changed in any previous test
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().hasClass('presentationmode')).toBeFalse();
        });
    });

    describe('method useFullscreenPresentation without modifications', function () {

        it('should exist', function () {
            expect(PresentationModeManager).toHaveMethod('useFullscreenPresentation');
        });

        it('should return true (if not modified)', function () {
            expect(presentationModeManager.useFullscreenPresentation()).toBeTrue();
        });

    });

    describe('method setFullscreenPresentation', function () {

        it('should exist', function () {
            expect(PresentationModeManager).toHaveMethod('setFullscreenPresentation');
        });

        it('should modify the full screen option', function () {
            expect(presentationModeManager.useFullscreenPresentation()).toBeTrue();
            presentationModeManager.setFullscreenPresentation(false);
            expect(presentationModeManager.useFullscreenPresentation()).toBeFalse();
            presentationModeManager.setFullscreenPresentation(true);
            expect(presentationModeManager.useFullscreenPresentation()).toBeTrue();
        });

    });

    describe('method startPresentation with parameter "startSlide"', function () {

        it('should start the presentation with the specified slide', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            presentationModeManager.startPresentation({ startInPresentationMode: true, startSlide: 2 });
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_3_id);
        });

        it('should return to the start slide if presentation is completed', function () {
            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });
    });

    describe.skip('method startPresentation with parameter "fullscreen" and user accepts full screen mode', function () {

        var showQueryDialogStub = null;

        beforeAll(function () {
            showQueryDialogStub = jest.spyOn(docView, 'showQueryDialog').mockImplementation(Promise.resolve);
        });

        afterAll(function () {
            showQueryDialogStub.mockRestore();
        });

        it('should start the presentation in full screen mode if specified', async function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            const queryUserPromise = presentationModeManager.startPresentation({ startInPresentationMode: true, fullscreen: true });
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);

            await queryUserPromise;
            expect(showQueryDialogStub).toHaveBeenCalledTimes(1); // first call
            expect(presentationModeManager.useFullscreenPresentation()).toBeTrue();
        });

        it('should return to the start slide if presentation is completed (1)', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_2_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });

        it('should start the presentation in full screen mode if not specified', async function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            const queryUserPromise = presentationModeManager.startPresentation({ startInPresentationMode: true });
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);

            await queryUserPromise;
            expect(showQueryDialogStub).toHaveBeenCalledTimes(2); // one further call
            expect(presentationModeManager.useFullscreenPresentation()).toBeTrue();
        });

        it('should return to the start slide if presentation is completed (2)', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_2_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });

        it('should not start the presentation in full screen mode if explicitely specified', async function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            const queryUserPromise = presentationModeManager.startPresentation({ startInPresentationMode: true, fullscreen: false });
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);

            await queryUserPromise;
            expect(showQueryDialogStub).toHaveBeenCalledTimes(2); // no further call
            expect(presentationModeManager.useFullscreenPresentation()).toBeFalse();
        });

        it('should return to the start slide if presentation is completed (3)', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_2_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });
    });

    describe.skip('method startPresentation with parameter "fullscreen" and user rejects full screen mode', function () {

        var showQueryDialogStub = null;

        function getRejectedPromise() {
            return new Promise((_resolve, reject) => {
                window.setTimeout(function () {
                    reject(new Error('No full screen'));
                }, 10);
            });
        }

        beforeAll(function () {
            showQueryDialogStub = jest.spyOn(docView, 'showQueryDialog').mockImplementation(getRejectedPromise); // rejecting
        });

        afterAll(function () {
            showQueryDialogStub.mockRestore();
        });

        it('should not start the presentation in full screen mode although specified', async function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            const queryUserPromise = presentationModeManager.startPresentation({ startInPresentationMode: true, fullscreen: true });
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);

            await expect(queryUserPromise).rejects.toThrow(Error);
            expect(showQueryDialogStub).toHaveBeenCalledTimes(1); // first call
            expect(presentationModeManager.useFullscreenPresentation()).toBeFalse();
        });

        it('should return to the start slide if presentation is completed (1)', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_2_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });

        it('should not start the presentation in full screen mode if not specified', async function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            const queryUserPromise = presentationModeManager.startPresentation({ startInPresentationMode: true });
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);

            await expect(queryUserPromise).rejects.toThrow(Error);
            expect(showQueryDialogStub).toHaveBeenCalledTimes(2); // one further call
            expect(presentationModeManager.useFullscreenPresentation()).toBeFalse();
        });

        it('should return to the start slide if presentation is completed (2)', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_2_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });

        it('should not start the presentation in full screen mode if explicitely specified', async function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            const queryUserPromise = presentationModeManager.startPresentation({ startInPresentationMode: true, fullscreen: false });
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);

            await expect(queryUserPromise).rejects.toThrow(Error);
            expect(showQueryDialogStub).toHaveBeenCalledTimes(2); // no further call
            expect(presentationModeManager.useFullscreenPresentation()).toBeFalse();
        });

        it('should return to the start slide if presentation is completed (3)', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_2_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_3_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(model.getActiveSlideId()).toBe(slide_4_id);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.RIGHT_ARROW });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });
    });

    describe('method startPresentation adapts the DOM in non-fullscreen mode', function () {

        it('should start in non-fullscreen mode after setting this explicitely', async function () {
            presentationModeManager.setFullscreenPresentation(false); // explicitely setting the fullscreen mode
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            await presentationModeManager.startPresentation();
            expect(presentationModeManager.useFullscreenPresentation()).toBeFalse(); // modified in a previous test
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().hasClass('presentationmode')).toBeFalse();
        });

        it('should set class "presentationmode" to the content root node', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().hasClass('presentationmode')).toBeFalse();
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().hasClass('presentationmode')).toBeTrue();
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().hasClass('presentationmode')).toBeFalse();
        });

        it('should set class "presentationmode" to the page node', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(model.getNode().hasClass('presentationmode')).toBeFalse();
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(model.getNode().hasClass('presentationmode')).toBeTrue();
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(model.getNode().hasClass('presentationmode')).toBeFalse();
        });

        it('should set class "presentationmode" to the page content node', function () {
            var pageContentNode = DOM.getPageContentNode(model.getNode());
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(pageContentNode.hasClass('presentationmode')).toBeFalse();
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(pageContentNode.hasClass('presentationmode')).toBeTrue();
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(pageContentNode.hasClass('presentationmode')).toBeFalse();
        });

        it('should add a node with class "presentationblocker" to the content root node', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().children('.presentationblocker')).toHaveLength(0);
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().children('.presentationblocker')).toHaveLength(1);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().children('.presentationblocker')).toHaveLength(0);
        });

        it('should add the required nodes below the "presentationblocker" node', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            const presentationBlockerNode = docView.getContentRootNode().children('.presentationblocker');
            expect(presentationBlockerNode).toHaveLength(1);
            expect(presentationBlockerNode.children()).toHaveLength(5);
            expect(presentationBlockerNode.children('.blockerinfonode')).toHaveLength(1);
            expect(presentationBlockerNode.children('.laserpointer')).toHaveLength(1);
            expect(presentationBlockerNode.children('.bigfullscreennode')).toHaveLength(1);
            expect(presentationBlockerNode.children('.borderparent')).toHaveLength(1);
            expect(presentationBlockerNode.children('.blockernavigation')).toHaveLength(1);
            expect(presentationBlockerNode.children('.blockernavigation').children('.navigationcontent').children('.navigationbutton')).toHaveLength(6);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });
    });

    describe('method startPresentation adapts the DOM in fullscreen mode', function () {

        it('should start in fullscreen mode after setting this explicitely', async function () {
            presentationModeManager.setFullscreenPresentation(true); // explicitely setting the fullscreen mode
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            await presentationModeManager.startPresentation();
            expect(presentationModeManager.useFullscreenPresentation()).toBeTrue();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().hasClass('presentationmode')).toBeFalse();
        });

        it('should set class "presentationmode" to the content root node', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().hasClass('presentationmode')).toBeFalse();
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().hasClass('presentationmode')).toBeTrue();
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().hasClass('presentationmode')).toBeFalse();
        });

        it('should set class "presentationmode" to the page node', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(model.getNode().hasClass('presentationmode')).toBeFalse();
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(model.getNode().hasClass('presentationmode')).toBeTrue();
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(model.getNode().hasClass('presentationmode')).toBeFalse();
        });

        it('should set class "presentationmode" to the page content node', function () {
            var pageContentNode = DOM.getPageContentNode(model.getNode());
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(pageContentNode.hasClass('presentationmode')).toBeFalse();
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(pageContentNode.hasClass('presentationmode')).toBeTrue();
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(pageContentNode.hasClass('presentationmode')).toBeFalse();
        });

        it('should add a node with class "presentationblocker" to the content root node', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().children('.presentationblocker')).toHaveLength(0);
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().children('.presentationblocker')).toHaveLength(1);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expect(docView.getContentRootNode().children('.presentationblocker')).toHaveLength(0);
        });

        it('should add the required nodes below the "presentationblocker" node', function () {
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            presentationModeManager.startPresentation();
            expect(presentationModeManager.isPresentationMode()).toBeTrue();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            const presentationBlockerNode = docView.getContentRootNode().children('.presentationblocker');
            expect(presentationBlockerNode).toHaveLength(1);
            expect(presentationBlockerNode.children()).toHaveLength(5);
            expect(presentationBlockerNode.children('.blockerinfonode')).toHaveLength(1);
            expect(presentationBlockerNode.children('.laserpointer')).toHaveLength(1);
            expect(presentationBlockerNode.children('.bigfullscreennode')).toHaveLength(1);
            expect(presentationBlockerNode.children('.borderparent')).toHaveLength(1);
            expect(presentationBlockerNode.children('.blockernavigation')).toHaveLength(1);
            expect(presentationBlockerNode.children('.blockernavigation').children('.navigationcontent').children('.navigationbutton')).toHaveLength(6);
            presentationModeManager.handleKeydownInPresentationMode({ keyCode: KeyCode.ESCAPE });
            expect(presentationModeManager.isPresentationMode()).toBeFalse();
            expect(model.getActiveSlideId()).toBe(slide_1_id);
        });

    });

});
