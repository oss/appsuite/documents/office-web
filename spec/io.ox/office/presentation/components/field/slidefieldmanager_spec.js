/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import { opSchemeColor, Color } from '@/io.ox/office/editframework/utils/color';

import SlideFieldManager from '@/io.ox/office/presentation/components/field/slidefieldmanager';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import Snapshot from '@/io.ox/office/textframework/utils/snapshot';

import { waitForEvent } from '~/asynchelper';
import { createPresentationApp } from '~/presentation/apphelper';

// class SlideFieldManager ====================================================

describe('Presentation class SlideFieldManager', function () {
    //  works as field manager

    it('should subclass ModelObject', function () {
        expect(SlideFieldManager).toBeSubClassOf(ModelObject);
    });

    // private helpers ----------------------------------------------------

    var model = null,
        selection = null,
        layoutId_1 = 'layout1',
        layoutId_2 = 'layout2',
        masterId_1 = 'master1',
        ctrTitleLeft = 1900,
        subTitleLeft = 3800,
        ctrTitleTop = 5900,
        subTitleTop = 10800,
        ctrTitleHeight = 4000,
        subTitleHeight = 5000,
        ctrTitleWidth = 21600,
        subTitleWidth = 17800,
        fieldManager = null,
        snapshot = null;

    // the operations to be applied by the document model
    var OPERATIONS = [
        {
            name: 'setDocumentAttributes',
            attrs: { page: { width: 33866, height: 19050, orientation: 'landscape' } }
        },
        { name: 'insertMasterSlide', id: masterId_1 },
        { name: 'insertDrawing', start: [0, 0], target: masterId_1, type: 'shape', attrs: { presentation: { phType: 'dt', phSize: 'half', phIndex: 2 }, listStyle: { l1: { character: { fontSize: 12, color: opSchemeColor('text1', { tint: 75000 }) }, paragraph: { alignment: 'left' } } }, shape: { anchor: 'centered', paddingLeft: 254, paddingRight: 254, paddingTop: 127, paddingBottom: 127 }, drawing: { name: 'Datumsplatzhalter 3', noGroup: true, left: 1270, top: 17657, width: 5927, height: 1014 }, geometry: { presetShape: 'rect', avList: {} } } },
        { name: 'insertParagraph', start: [0, 0, 0], target: masterId_1 },
        { name: 'insertField', start: [0, 0, 0, 0], type: 'datetimeFigureOut', representation: '30.11.15', target: masterId_1 },
        { name: 'insertDrawing', start: [0, 1], target: masterId_1, type: 'shape', attrs: { presentation: { phType: 'ftr', phSize: 'quarter', phIndex: 3 }, listStyle: { l1: { character: { fontSize: 12, color: opSchemeColor('text1', { tint: 75000 }) }, paragraph: { alignment: 'center' } } }, shape: { anchor: 'centered', paddingLeft: 254, paddingRight: 254, paddingTop: 127, paddingBottom: 127 }, drawing: { name: 'Fu\xdfzeilenplatzhalter 4', noGroup: true, left: 8678, top: 17657, width: 8043, height: 1014 }, geometry: { presetShape: 'rect', avList: {} } } },
        { name: 'insertParagraph', start: [0, 1, 0], target: masterId_1 },
        { name: 'insertDrawing', start: [0, 2], target: masterId_1, type: 'shape', attrs: { presentation: { phType: 'sldNum', phSize: 'quarter', phIndex: 4 }, listStyle: { l1: { character: { fontSize: 12, color: opSchemeColor('text1', { tint: 75000 }) }, paragraph: { alignment: 'right' } } }, shape: { anchor: 'centered', paddingLeft: 254, paddingRight: 254, paddingTop: 127, paddingBottom: 127 }, drawing: { name: 'Foliennummernplatzhalter 5', noGroup: true, left: 18203, top: 17657, width: 5927, height: 1014 }, geometry: { presetShape: 'rect', avList: {} } } },
        { name: 'insertParagraph', start: [0, 2, 0], target: masterId_1 },
        { name: 'insertField', start: [0, 2, 0, 0], type: 'slidenum', representation: '\u2039Nr.\u203a', target: masterId_1 },

        { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
        { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: ctrTitleLeft, top: ctrTitleTop, width: ctrTitleWidth, height: ctrTitleHeight } } },
        { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
        { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: 'Mastertitelformat bearbeiten' },
        { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: subTitleLeft, top: subTitleTop, width: subTitleWidth, height: subTitleHeight } } },
        { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1 },
        { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_1, text: 'Master-Untertitelformat bearbeiten' },

        { name: 'insertDrawing', start: [0, 2], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'dt', phSize: 'half', phIndex: 10 }, drawing: { name: 'Datumsplatzhalter 3', noGroup: true } } },
        { name: 'insertParagraph', start: [0, 2, 0], target: layoutId_1 },
        { name: 'insertField', start: [0, 2, 0, 0], type: 'datetimeFigureOut', representation: '30.11.15', target: layoutId_1 },
        { name: 'insertDrawing', start: [0, 3], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ftr', phSize: 'quarter', phIndex: 11 }, drawing: { name: 'Fu\xdfzeilenplatzhalter 4', noGroup: true } } },
        { name: 'insertParagraph', start: [0, 3, 0], target: layoutId_1 },
        { name: 'insertDrawing', start: [0, 4], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'sldNum', phSize: 'quarter', phIndex: 12 }, drawing: { name: 'Foliennummernplatzhalter 5', noGroup: true } } },
        { name: 'insertParagraph', start: [0, 4, 0], target: layoutId_1 },
        { name: 'insertField', start: [0, 4, 0, 0], type: 'slidenum', representation: '\u2039Nr.\u203a', target: layoutId_1 },

        { name: 'insertLayoutSlide', id: layoutId_2, target: masterId_1 },
        { name: 'insertDrawing', start: [0, 0], target: layoutId_2, type: 'shape', attrs: { presentation: { phType: 'title' }, drawing: { name: 'Titel 1' } } },
        { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_2 },
        { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_2, text: 'Mastertitelformat bearbeiten' },
        { name: 'insertDrawing', start: [0, 1], target: layoutId_2, type: 'shape', attrs: { presentation: { phIndex: 1 }, drawing: { name: 'Drawing 2', left: 1270, top: 4445, width: 11218, height: 12572 } } },
        { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_2, attrs: { paragraph: { level: 0 } } },
        { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_2, text: 'Mastertextformat bearbeiten' },
        { name: 'insertDrawing', start: [0, 2], target: layoutId_2, type: 'shape', attrs: { presentation: { phIndex: 2 }, drawing: { name: 'Drawing 3', left: 12912, top: 4445, width: 11218, height: 12572 } } },
        { name: 'insertParagraph', start: [0, 2, 0], target: layoutId_2, attrs: { paragraph: { level: 0 } } },
        { name: 'insertText', start: [0, 2, 0, 0], target: layoutId_2, text: 'Mastertextformat bearbeiten' },

        { name: 'insertSlide', start: [0], target: layoutId_1 },
        { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1' } } },
        { name: 'insertParagraph', start: [0, 0, 0] },
        { name: 'insertField', start: [0, 0, 0, 0], type: 'datetime1', representation: '11.04.2016' }
    ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        selection = model.getSelection();
        fieldManager = model.getFieldManager();
    });

    // constructor --------------------------------------------------------
    describe('constructor', function () {
        it('should create a SlideFieldManager class instance', function () {
            expect(fieldManager).toBeInstanceOf(SlideFieldManager);
        });
    });

    // public methods -----------------------------------------------------
    describe('method fieldsAreInDocument', function () {
        it('should exist', function () {
            expect(SlideFieldManager).toHaveMethod('fieldsAreInDocument');
        });
        it('should return that there are fields in document', function () {
            expect(fieldManager.fieldsAreInDocument()).toBeTrue();
        });
    });

    describe('method getSimpleField', function () {
        it('should exist', function () {
            expect(SlideFieldManager).toHaveMethod('getSimpleField');
        });
        it('should return simple odf field node', function () {
            const field = fieldManager.getSimpleField('sf0');
            expect(field).toBeInstanceOf($);
            expect(field.hasClass('field')).toBeTrue();
        });
    });

    describe('method isHighlightState', function () {
        beforeAll(function () {
            selection.setTextSelection([0, 0, 0, 0]);
        });
        it('should exist', function () {
            expect(SlideFieldManager).toHaveMethod('isHighlightState');
        });
        it('should return that simple slide field is highlighted', function () {
            expect(fieldManager.isHighlightState()).toBeTrue();
        });
    });

    describe('method dispatchInsertField', function () {

        it('should delete the previous content successfully', async function () {
            selection.setTextSelection([0, 0, 0, 0]);
            selection.selectAll();
            await model.deleteSelected();
            selection.setTextSelection([0, 0, 0, 0]);
            fieldManager.dispatchInsertField('slidenum');
            fieldManager.dispatchInsertField('slidenum');
            fieldManager.insertField('datetime'); // should be inserted as text, not as field, therefore should be only 2 fields in document
            selection.setTextSelection([0, 0, 0, 0]);
        });

        it('should exist', function () {
            expect(SlideFieldManager).toHaveMethod('dispatchInsertField');
            expect(SlideFieldManager).toHaveMethod('insertField');
        });

        it('should return that there are fields in document', function () {
            expect(fieldManager.fieldsAreInDocument()).toBeTrue();
        });

        it('should return that there are 2 fields on standard document slides', function () {
            expect(fieldManager.getAllNormalSlideFields()).toHaveLength(2);
        });

        it('should return newly inserted field with id sf1', function () {
            const field = fieldManager.getSelectedFieldNode();
            expect(field).toBeInstanceOf($);
            expect(field.hasClass('field')).toBeTrue();
            expect(field.attr('data-fid')).toBe("sf5");
        });
    });

    describe('method insertAllFooters', function () {

        it('should exist', function () {
            expect(SlideFieldManager).toHaveMethod('insertAllFooters');
        });

        it('should have been executed successfully', async function () {
            await fieldManager.insertAllFooters([{ type: 'ftr', ftrText: 'Footer' }, { type: 'dt', automatic: true, formatIndex: 1, representation: '12.05.2016' }]);
        });

        it('should return that there are fields in document', function () {
            expect(fieldManager.fieldsAreInDocument()).toBeTrue();
        });

        it('should return that there are 3 fields on standard document slides', function () {
            expect(fieldManager.getAllNormalSlideFields()).toHaveLength(3); // in footer placeholder is inserted text, therefore is not counted as field
        });

        it('should return that there are 2 types of footer fields on current standard slide', function () {
            expect(fieldManager.getFieldOptionsOnSlide()).toHaveLength(2);
        });
    });

    describe('testing snapshot for insertAllFooters method', function () {

        it('should have been executed successfully', async function () {
            snapshot = new Snapshot(model);
            await fieldManager.insertAllFooters([]); // this will delete footer fields
        });

        it('should have same number of fields as before delete', function () {
            snapshot.apply();  // applying the snapshot

            expect(fieldManager.fieldsAreInDocument()).toBeTrue();
            expect(fieldManager.getAllNormalSlideFields()).toHaveLength(3);
            expect(fieldManager.getFieldOptionsOnSlide()).toHaveLength(2);
        });
    });

});

// Tests for ODP footer fields

describe('Presentation class SlideFieldManager in ODP applications', function () {
    //  works as field manager

    it('should exist', function () {
        expect(SlideFieldManager).toBeFunction();
    });

    // private helpers ----------------------------------------------------

    var model = null,
        // selection = null,
        masterId_1 = 'Standard',
        masterSlide = null,
        allDrawings = null,
        fieldManager = null,
        slideNumDrawing = null,
        dateDrawing = null,
        footerDrawing = null,
        slide_1 = 'slide_1', // the ID of the first slide in document view
        slide_2 = 'slide_2', // the ID of the second slide in document view
        slide1FooterText = 'Footer on slide 1',
        slide2FooterText = 'Footer on slide 2';

    // the operations to be applied by the document model
    var OPERATIONS = [
        {
            name: 'setDocumentAttributes',
            attrs: { page: { width: 33866, height: 19050, orientation: 'landscape' } }
        },
        { name: 'insertMasterSlide', id: masterId_1 },
        { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { phType: 'title' }, drawing: { top: 837, left: 1400, width: 25199, height: 3506 } }, target: masterId_1 },
        { name: 'insertParagraph', start: [0, 0, 0], target: masterId_1, attrs: { paragraph: { bullet: { type: 'none' } } } },
        { name: 'insertText', start: [0, 0, 0, 0], target: masterId_1, text: 'Click to edit Master title style' },
        { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { presentation: { phType: 'body' }, drawing: { top: 4914, left: 1400, width: 25199, height: 12179 } }, target: masterId_1 },
        { name: 'insertParagraph', start: [0, 1, 0], target: masterId_1, attrs: { paragraph: { level: 0 } } },
        { name: 'insertText', start: [0, 1, 0, 0], target: masterId_1, text: 'Edit Master text styles' },
        { name: 'insertDrawing', start: [0, 2], type: 'shape', attrs: { presentation: { phType: 'dt' }, character: { fontSize: 14, fontSizeAsian: 14, fontSizeComplex: 14 }, line: { type: 'none' }, drawing: { top: 19131, left: 1400, width: 6523, height: 1448 }, fill: { color: Color.WHITE, type: 'none' } }, target: masterId_1 },
        { name: 'insertParagraph', start: [0, 2, 0], target: masterId_1, attrs: { paragraph: { bullet: { type: 'none' } }, character: { fontSize: 14, fontSizeAsian: 14, fontSizeComplex: 14 } } },
        { name: 'insertField', start: [0, 2, 0, 0], type: 'date-time', representation: '', target: masterId_1 },
        { name: 'insertDrawing', start: [0, 3], type: 'shape', attrs: { presentation: { phType: 'ftr' }, paragraph: { alignment: 'center' }, character: { fontSize: 14, fontSizeAsian: 14, fontSizeComplex: 14 }, line: { type: 'none' }, drawing: { top: 19131, left: 9576, width: 8875, height: 1448 }, fill: { color: Color.WHITE, type: 'none' } }, target: masterId_1 },
        { name: 'insertParagraph', start: [0, 3, 0], target: masterId_1, attrs: { paragraph: { alignment: 'center', bullet: { type: 'none' } }, character: { fontSize: 14, fontSizeAsian: 14, fontSizeComplex: 14 } } },
        { name: 'insertField', start: [0, 3, 0, 0], type: 'footer', representation: '', target: masterId_1 },
        { name: 'insertDrawing', start: [0, 4], type: 'shape', attrs: { presentation: { phType: 'sldNum' }, paragraph: { alignment: 'right' }, character: { fontSize: 14, fontSizeAsian: 14, fontSizeComplex: 14 }, line: { type: 'none' }, drawing: { top: 19131, left: 20076, width: 6523, height: 1448 }, fill: { color: Color.WHITE, type: 'none' } }, target: masterId_1 },
        { name: 'insertParagraph', start: [0, 4, 0], target: masterId_1, attrs: { paragraph: { alignment: 'right', bullet: { type: 'none' } }, character: { fontSize: 14, fontSizeAsian: 14, fontSizeComplex: 14 } } },
        { name: 'insertField', start: [0, 4, 0, 0], type: 'page-number', representation: '<#>', target: masterId_1 },
        { name: 'insertSlide', start: [0], target: masterId_1, attrs: { slide: { isSlideNum: false, isDate: true, isFooter: true, dateField: 'DD.MM.YYYY', footerText: slide1FooterText } } },
        { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { phType: 'title' }, drawing: { top: 837, left: 1400, width: 25199, height: 3506 } } },
        { name: 'insertParagraph', start: [0, 0, 0], attrs: { paragraph: { bullet: { type: 'none' } } } },
        { name: 'insertText', start: [0, 0, 0, 0], text: 'Test slide 1' },
        { name: 'insertSlide', start: [1], target: masterId_1, attrs: { slide: { isSlideNum: true, isDate: true, isFooter: true, dateField: 'DD.MM.YYYY', footerText: slide2FooterText } } },
        { name: 'insertDrawing', start: [1, 0], type: 'shape', attrs: { presentation: { phType: 'title' }, drawing: { top: 837, left: 1400, width: 25199, height: 3506 } } },
        { name: 'insertParagraph', start: [1, 0, 0], attrs: { paragraph: { bullet: { type: 'none' } } } },
        { name: 'insertText', start: [1, 0, 0, 0], text: 'Test slide 2' }
    ];

    createPresentationApp('odf', OPERATIONS).done(function (app) {
        model = app.getModel();
        fieldManager = model.getFieldManager();
    });

    // constructor --------------------------------------------------------

    describe('constructor', function () {
        it('should create a SlideFieldManager class instance', function () {
            expect(fieldManager).toBeInstanceOf(SlideFieldManager);
        });
    });

    // public methods -----------------------------------------------------

    describe('should contain specific nodes after load', function () {

        it('should have one master slide generated', function () {
            expect(model.getMasterSlideCount()).toBe(1);
        });

        it('should have one master slide with valid ID', function () {
            expect(model.getMasterSlideOrder()[0]).toBe(masterId_1);
        });

        it('should have one master slide with 5 drawings', function () {
            masterSlide = model.getSlideById(masterId_1);
            allDrawings = masterSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR); // getting all drawings on slide
            expect(allDrawings).toHaveLength(5);
        });

        it('should have one slide number footer drawing on the master slide', function () {
            slideNumDrawing = masterSlide.children('[data-placeholdertype=sldNum]');
            expect(slideNumDrawing).toHaveLength(1);
        });

        it('should have one date footer drawing on the master slide', function () {
            dateDrawing = masterSlide.children('[data-placeholdertype=dt]');
            expect(dateDrawing).toHaveLength(1);
        });

        it('should have one footer text drawing on the master slide', function () {
            footerDrawing = masterSlide.children('[data-placeholdertype=ftr]');
            expect(footerDrawing).toHaveLength(1);
        });

        it('should have two document slides', function () {
            expect(model.getStandardSlideCount()).toBe(2);
        });

        it('should have documents slides with valid IDs', function () {
            var allDocSlideIDs = model.getStandardSlideOrder();
            expect(allDocSlideIDs).toHaveLength(2);
            expect(allDocSlideIDs[0]).toBe(slide_1);
            expect(allDocSlideIDs[1]).toBe(slide_2);
        });

        it('should have the first slide as active slide', function () {
            expect(model.getActiveSlideId()).toBe(slide_1);
        });

    });

    describe('methods in model for checking slide attributes', function () {

        it('should exist', function () {
            expect(model).toRespondTo('isSlideNumberDrawingSlide');
            expect(model).toRespondTo('isFooterDrawingSlide');
            expect(model).toRespondTo('isDateDrawingSlide');
        });

        it('should have the correct slide attributes for each document slide', function () {

            // first document slide
            expect(model.isSlideNumberDrawingSlide(slide_1)).toBeFalse();
            expect(model.isFooterDrawingSlide(slide_1)).toBeTrue();
            expect(model.isDateDrawingSlide(slide_1)).toBeTrue();

            // second document slide
            expect(model.isSlideNumberDrawingSlide(slide_2)).toBeTrue();
            expect(model.isFooterDrawingSlide(slide_2)).toBeTrue();
            expect(model.isDateDrawingSlide(slide_2)).toBeTrue();

        });
    });

    describe('method updateMasterSlideFields', function () {

        it('should exist', function () {
            expect(SlideFieldManager).toHaveMethod('updateMasterSlideFields');
        });

        it('should have the correct content in fields in master slide footer drawings after activating slide 2', async function () {

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.setActiveSlideId(slide_2); // activating the second slide
            });

            expect(arg).toBe(slide_2);
            expect(slideNumDrawing.text()).toBe("2");
            expect(dateDrawing.text()).not.toBe(""); // not empty
            expect(/^[.\d]+$/.test(dateDrawing.text())).toBeTrue(); // only digits and dots
            expect(footerDrawing.text()).toBe(slide2FooterText); // the text was modified on the master slide
        });

        it('should have the correct content in fields in master slide footer drawings after changing back to slide 1', async function () {

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.setActiveSlideId(slide_1); // activating the first slide
            });

            expect(arg).toBe(slide_1);
            expect(slideNumDrawing.text()).toBe("1");
            expect(dateDrawing.text()).not.toBe(""); // not empty
            expect(/^[.\d]+$/.test(dateDrawing.text())).toBeTrue(); // only digits and dots
            expect(footerDrawing.text()).toBe(slide1FooterText); // the text was modified on the master slide
        });
    });

    // modifying the slide attributes (for one slide or for all slides)
    describe('method insertFooterODF', function () {

        it('should exist', function () {
            expect(SlideFieldManager).toHaveMethod('insertFooterODF');
        });

        it('should change the slide attributes for the active slide only', async function () {

            var newFooterText = 'New footer on slide 1';
            var newDateText = 'New date text';

            fieldManager.insertFooterODF([{ type: 'dt', automatic: false, representation: newDateText }, { type: 'sldNum' }, { type: 'ftr', ftrText: newFooterText }], false);

            expect(slideNumDrawing.text()).toBe("1");
            expect(dateDrawing.text()).toBe(newDateText);
            expect(footerDrawing.text()).toBe(newFooterText); // the text was modified on the master slide

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.setActiveSlideId(slide_2); // activating the first slide
            });

            expect(arg).toBe(slide_2);
            expect(slideNumDrawing.text()).toBe("2");
            expect(dateDrawing.text()).not.toBe(""); // not empty
            expect(/^[.\d]+$/.test(dateDrawing.text())).toBeTrue(); // only digits and dots
            expect(footerDrawing.text()).toBe(slide2FooterText); // the text was modified on the master slide
        });

        it('should change the slide attributes for all document slides', async function () {

            var newFooterText = 'New footer on all slides';
            var newDateText = 'New date text on all slides';

            fieldManager.insertFooterODF([{ type: 'dt', automatic: false, representation: newDateText }, { type: 'sldNum' }, { type: 'ftr', ftrText: newFooterText }], true);

            expect(slideNumDrawing.text()).toBe("2");
            expect(dateDrawing.text()).toBe(newDateText);
            expect(footerDrawing.text()).toBe(newFooterText); // the text was modified on the master slide

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.setActiveSlideId(slide_1); // activating the first slide
            });

            expect(arg).toBe(slide_1);
            expect(slideNumDrawing.text()).toBe("1");
            expect(dateDrawing.text()).toBe(newDateText);
            expect(footerDrawing.text()).toBe(newFooterText); // the text is modified for the first slide, too
        });
    });
});
