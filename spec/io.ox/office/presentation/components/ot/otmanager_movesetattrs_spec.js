/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ============================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // setAttributes and local move operation (handleMoveSetAttrs)
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 1] }
        it('should calculate valid transformed setAttributes operation after local move operation', function () {

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3]);
            expect(transformedOps[0].end).toEqual([3, 4]);

            oneOperation = { name: 'setAttributes', start: [3], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);
            expect(transformedOps[0].end).toEqual([3]);

            oneOperation = { name: 'setAttributes', start: [2], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2]);
            expect(transformedOps[0].end).toEqual([2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 1]);
            expect(transformedOps[0].end).toEqual([3, 3, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 1]);
            expect(transformedOps[0].end).toEqual([3, 4, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], target: '234567', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 1]);
            expect(transformedOps[0].end).toEqual([3, 4, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 1]);
            expect(transformedOps[0].end).toEqual([3, 3, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 6, 1]);
            expect(transformedOps[0].end).toEqual([3, 6, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 6, 1]);
            expect(transformedOps[0].end).toEqual([3, 6, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 5]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 1]);
            expect(transformedOps[0].end).toEqual([3, 4, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 1]);
            expect(transformedOps[0].end).toEqual([3, 3, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 1]);
            expect(transformedOps[0].end).toEqual([3, 5, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 1]);
            expect(transformedOps[0].end).toEqual([3, 5, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 1]);
            expect(transformedOps[0].end).toEqual([3, 4, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 1]);
            expect(transformedOps[0].end).toEqual([3, 4, 2]);

            oneOperation = { name: 'setAttributes', start: [4, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 1], to: [4, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [4, 1], to: [4, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [4, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [4, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [4, 0], to: [4, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [4, 0], to: [4, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
        });

        // move and local setAttributes operation (handleMoveSetAttrs)
        it('should calculate valid transformed move operation after local setAttributes operation', function () {

            oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 0]);
            expect(transformedOps[0].end).toEqual([3, 0]);
            expect(transformedOps[0].to).toEqual([3, 6]);

            oneOperation = { name: 'move', start: [3, 6], end: [3, 6], to: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 6]);
            expect(transformedOps[0].end).toEqual([3, 6]);
            expect(transformedOps[0].to).toEqual([3, 0]);

            oneOperation = { name: 'move', start: [3, 6], end: [3, 6], to: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 6]);
            expect(transformedOps[0].end).toEqual([3, 6]);
            expect(transformedOps[0].to).toEqual([3, 0]);
        });

        // updateField and local move (handleMoveSetAttrs)
        it('should calculate valid transformed updateField operation after local move operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 1, 1, 1], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 3, 1, 1], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 0, 1, 1], representation: 'abc' }], transformedOps);
        });

        // updateField and external move (handleMoveSetAttrs)
        it('should calculate valid transformed updateField operation after external move operation', function () {

            oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 1, 1, 1], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 3, 1, 1], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 0, 1, 1], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 }], transformedOps);
        });
    });

});
