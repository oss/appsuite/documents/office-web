/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class OTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertSlide and local moveSlide operation (handleMoveSlideInsertComp)
        it('should calculate valid transformed insertSlide operation after local moveSlide operation', function () {

            oneOperation = { name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }], transformedOps);
        });

        // insertSlide and external moveSlide operation (handleMoveSlideInsertComp)
        it('should calculate valid transformed insertSlide operation after external moveSlide operation', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);
        });

        // insertParagraph and local moveSlide operation (handleMoveSlideInsertComp)
        it('should calculate valid transformed insertParagraph operation after local moveSlide operation', function () {

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [4, 0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [4, 0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [0, 0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 0, 1], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 0, 0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [2, 0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [2, 0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [4, 0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [4, 0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [2, 0, 1], opl: 1, osn: 1 }], transformedOps);
        });

        // insertParagraph and external moveSlide operation (handleMoveSlideInsertComp)
        it('should calculate valid transformed insertParagraph operation after external moveSlide operation', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [4, 0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [4, 0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [2, 0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [2, 0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [4, 0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [4, 0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
        });

    });
});
