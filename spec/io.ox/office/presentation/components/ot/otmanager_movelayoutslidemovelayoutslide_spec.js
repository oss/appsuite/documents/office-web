/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    // moveLayoutSlide and local deleteTargetSlide operation (handleMoveLayoutSlideDeleteTargetSlide)
    // moveLayoutSlide: { id: "layout_1", start: 1, target: "master_1", oldtarget: "master_2", oldindex: 3 } }

    describe('method transformOperation', function () {

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of the same layout slide and move inside same master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of the same layout slide and move to another master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 5, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 5, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_2', oldindex: 5, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of a different layout slide and move inside same master', function () {

            // new move process
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            // new move process
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of a different layout slide and only external move to a different master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of a different layout slide and only local move to a different master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of a different layout slide and only external move from a different master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 6, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of a different layout slide and only local move from a different master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of a different layout slide and both operations have complete different master slides', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_4', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_4', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of a different layout slide and both operations move from same master to a different master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of a different layout slide and both operations move from different masters to the same master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of a different layout slide and both operations move from same masters to same master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local moveLayoutSlide operation of a different layout slide and both operations move in opposite direction', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
        });
    });

});
