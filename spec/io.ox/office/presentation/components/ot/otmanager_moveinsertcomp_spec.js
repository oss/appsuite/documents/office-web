/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // insertParagraph and local move operation (handleMoveInsertComp)
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 1] }
        it('should calculate valid transformed insertParagraph operation after local move operation', function () {

            oneOperation = { name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 0, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 1, 1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 1, 1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 1, 1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 0, 1]);
        });

        // move and local insertParagraph operation (handleMoveInsertComp)
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 0] }
        it('should calculate valid transformed move operation after local insertParagraph operation', function () {

            oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 0]);
            expect(transformedOps[0].end).toEqual([3, 0]);
            expect(transformedOps[0].to).toEqual([3, 4]);

            oneOperation = { name: 'move', start: [3, 1], end: [3, 1], to: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].end).toEqual([3, 1]);
            expect(transformedOps[0].to).toEqual([3, 4]);

            oneOperation = { name: 'move', start: [3, 1], to: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 4, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].end).toBeUndefined();
            expect(transformedOps[0].to).toEqual([3, 4]);

            oneOperation = { name: 'move', start: [3, 0], to: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 0]);
            expect(transformedOps[0].end).toBeUndefined();
            expect(transformedOps[0].to).toEqual([3, 4]);
        });

        // insertSlide and local move operation (handleMoveInsertComp)
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 0] }
        it('should calculate valid transformed insertSlide operation after local move operation', function () {

            oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 0]);
            expect(localActions[0].operations[0].end).toEqual([4, 0]);
            expect(localActions[0].operations[0].to).toEqual([4, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0]);

            oneOperation = { name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(localActions[0].operations[0].end).toEqual([3, 0]);
            expect(localActions[0].operations[0].to).toEqual([3, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([4]);
        });

        // move and local insertSlide operation (handleMoveInsertComp)
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 1] }
        it('should calculate valid transformed move operation after local insertSlide operation', function () {

            oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([4, 0]);
            expect(transformedOps[0].end).toEqual([4, 0]);
            expect(transformedOps[0].to).toEqual([4, 3]);

            oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([4, 0]);
            expect(transformedOps[0].end).toEqual([4, 0]);
            expect(transformedOps[0].to).toEqual([4, 3]);

            oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 0]);
            expect(transformedOps[0].end).toEqual([3, 0]);
            expect(transformedOps[0].to).toEqual([3, 3]);
        });
    });

});
