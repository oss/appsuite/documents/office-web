/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        localOperation = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    // the test runner
    var TestRunner = Utils.TestRunner;
    var testRunner = new TestRunner(otManagerPresentation);

    describe('method transformOperation', function () {

        // { name: ungroup, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed ungroup operation after local ungroup operation', function () {

            oneOperation = { name: 'ungroup', start: [2, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [2, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [4, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [4, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 2], drawings: [2, 3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 6], drawings: [6, 7, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 };
            localOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 6, 8], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 7], opl: 1, osn: 1 };
            localOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'ungroup', start: [3, 2], drawings: [2, 3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 6], drawings: [6, 7, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 7], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '234567', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
            localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });

        it('should ignore both ungroup operations, if they are identical', function () {
            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(transformedOps).toHaveLength(0);
            expect(localActions[0].operations).toHaveLength(0);
        });

        it('should not ignore the identical ungroup operations, if targets are differnet', function () {
            oneOperation = { name: 'ungroup', start: [0, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [0, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [0, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [0, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [0, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [0, 1], drawings: [1, 4, 5], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [0, 1], drawings: [1, 4, 5], target: '234567', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [0, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

    });
});
