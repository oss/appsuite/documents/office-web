/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class TextOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // move and local move operation (handleMoveMove)
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 1] }
        it('should calculate valid transformed move operation after local move operation', function () {

            oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 0], end: [1, 0], to: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [1, 0], end: [1, 0], to: [1, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 1], opl: 1, osn: 1 }], transformedOps);

            // new move process
            oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 6], opl: 1, osn: 1 }], transformedOps);

            // new move process
            oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [2, 5], to: [2, 3], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 5], to: [2, 3], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 3], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [2, 5], to: [2, 3], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 5], to: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 5], to: [2, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 5], to: [2, 3], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });
    });

});
