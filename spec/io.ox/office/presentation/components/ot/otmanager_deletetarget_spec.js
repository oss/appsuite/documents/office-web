/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        localOperation = null,
        oneOperation = null,
        externalActions = null,
        transformedOps = null,
        transformedActions = null;

    // the test runner
    var TestRunner = Utils.TestRunner;
    var testRunner = new TestRunner(otManagerPresentation);

    // shortcuts for the utils functions
    var expectActions = Utils.expectActions,
        expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // deleteTargetSlide and local deleteTargetSlide operation (handleDeleteTargetDeleteTarget)
        it('should calculate valid transformed deleteTargetSlide operation after local deleteTargetSlide operation', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }], transformedOps);
        });

        // insertText and local deleteTargetSlide and delete operation (handleDeleteTarget)
        it('should calculate valid transformed operation after local delete and deleteTargetSlide operation', function () {

            // using the combination of specific 'delete' and generic 'deleteTargetSlide' operation
            oneOperation = { name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [0, 1, 0, 0], text: 'abc', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', opl: 1, osn: 1 }], transformedOps);
        });

        // insertText and local deleteTargetSlide (handleDeleteTarget)
        it('should calculate valid transformed operation after local deleteTargetSlide operation', function () {

            // using the generic 'deleteTargetSlide' operation that indicates a removed target
            oneOperation = { name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // using the generic 'deleteTargetSlide' operation that indicates a removed target
            oneOperation = { name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 }], transformedOps);
        });

        // insertText and local delete
        it('should calculate valid transformed operation after local delete operation', function () {

            // using the specific 'delete' operation that also indicates the removal of the target slide
            oneOperation = { name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 2], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 2], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 }], transformedOps);
        });

        // insertParagraph and local deleteTargetSlide and delete operation (handleDeleteTarget)
        it('should calculate valid transformed insertParagraph operation after local delete and deleteTargetSlide operation', function () {

            // using the combination of specific 'delete' and generic 'deleteTargetSlide' operation
            oneOperation = { name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [0, 1, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [0, 1, 0], opl: 1, osn: 1 }], transformedOps);
        });

        // insertParagraph and local deleteTargetSlide (handleDeleteTarget)
        it('should calculate valid transformed insertParagraph operation after local deleteTargetSlide operation', function () {

            // using the generic 'deleteTargetSlide' operation that indicates a removed target
            oneOperation = { name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // using the generic 'deleteTargetSlide' operation that indicates a removed target
            oneOperation = { name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 }], transformedOps);
        });

        // insertParagraph and local delete
        it('should calculate valid transformed insertParagraph operation after local delete operation', function () {

            // using the specific 'delete' operation that also indicates the removal of the target slide
            oneOperation = { name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 2], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 2], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 }], transformedOps);
        });

        // insertSlide and local deleteTargetSlide and delete operation (handleDeleteTarget)
        it('should calculate valid transformed insertSlide operation after local delete and deleteTargetSlide operation', function () {

            oneOperation = { name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 }], transformedOps);
        });

        // insertSlide and local deleteTargetSlide (handleDeleteTarget)
        it('should calculate valid transformed insertSlide operation after local deleteTargetSlide operation', function () {

            // using the generic 'deleteTargetSlide' operation that indicates a removed target
            oneOperation = { name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 }], transformedOps);
        });

        // insertSlide and local deleteTargetSlide (handleDeleteTarget)
        it('should reload document after deleting required target slide for insertSlide operation', function () {

            // using the generic 'deleteTargetSlide' operation that indicates a removed target
            // -> reload test
            oneOperation = { name: 'insertSlide', start: [3], target: 'layout_1', opl: 1, osn: 1 };
            localOperation = { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });

        // insertSlide and local delete
        it('should calculate valid transformed insertSlide operation after local delete operation', function () {

            oneOperation = { name: 'insertSlide', start: [3], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [3], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 }], transformedOps);

            // using the specific 'delete' operation that also indicates the removal of the target slide
            // -> no changes here -> handling is done with deleteTargetSlide operation
            oneOperation = { name: 'insertSlide', start: [3], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [3], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);
        });

        // insertText and external deleteTargetSlide and delete operation (handleDeleteTarget)
        it('should calculate valid transformed operation after external delete and deleteTargetSlide operation', function () {

            // using the combination of specific 'delete' and generic 'deleteTargetSlide' operation
            externalActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([{ operations: [] }], localActions);
            expectActions([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], transformedActions);

            externalActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 }] }];
            transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectActions([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], transformedActions);

            externalActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', opl: 1, osn: 1 }] }];
            transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', opl: 1, osn: 1 }] }], localActions);
            expectActions([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], transformedActions);
        });

        // insertText and external deleteTargetSlide (handleDeleteTarget)
        it('should calculate valid transformed operation after external deleteTargetSlide operation', function () {

            // using the generic 'deleteTargetSlide' operation that indicates a removed target
            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            // using the generic 'deleteTargetSlide' operation that indicates a removed target
            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }], transformedOps);
        });

        // insertText and external delete
        it('should calculate valid transformed operation after external delete operation', function () {

            // using the specific 'delete' operation that also indicates the removal of the target slide
            oneOperation = { name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0, 2], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0, 2], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [0, 1, 0, 0], text: 'abc', target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);
        });

        // insertParagraph and external deleteTargetSlide and delete operation (handleDeleteTarget)
        it('should calculate valid transformed insertParagraph operation after external delete and deleteTargetSlide operation', function () {

            // using the combination of specific 'delete' and generic 'deleteTargetSlide' operation
            externalActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([{ operations: [] }], localActions);
            expectActions([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], transformedActions);

            externalActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 }] }];
            transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectActions([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], transformedActions);

            externalActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], opl: 1, osn: 1 }] }];
            transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], opl: 1, osn: 1 }] }], localActions);
            expectActions([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], transformedActions);
        });

        // insertParagraph and external deleteTargetSlide (handleDeleteTarget)
        it('should calculate valid transformed insertParagraph operation after external deleteTargetSlide operation', function () {

            // using the generic 'deleteTargetSlide' operation that indicates a removed target
            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }], transformedOps);
        });

        // insertParagraph and external delete
        it('should calculate valid transformed insertParagraph operation after external delete operation', function () {

            // using the specific 'delete' operation that also indicates the removal of the target slide
            oneOperation = { name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0, 2], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0, 2], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0, 0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [0, 0, 0], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0, 0], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [0, 1, 0], target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);
        });

        // insertSlide and external deleteTargetSlide and delete operation (handleDeleteTarget)
        it('should calculate valid transformed insertSlide operation after external delete and deleteTargetSlide operation', function () {

            externalActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }];
            localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 }] }];
            transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([{ operations: [{ name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectActions([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }, { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }] }], transformedActions);
        });

        // insertSlide and external deleteTargetSlide (handleDeleteTarget)
        it('should calculate valid transformed insertSlide operation after external deleteTargetSlide operation', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 }], transformedOps);
        });

        // insertSlide and external deleteTargetSlide (handleDeleteTarget)
        it('should reload document after removal of target slide for insertSlide operation', function () {

            // using the generic 'deleteTargetSlide' operation that indicates a removed target
            // -> in this process the insertSlide operation can be marked as removed (not with delete operation, see below)
            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', opl: 1, osn: 1 };
            localOperation = { name: 'insertSlide', start: [3], target: 'layout_1', opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });

        // insertSlide and external delete
        it('should calculate valid transformed insertSlide operation after external delete operation', function () {

            oneOperation = { name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0, 1], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            // using the specific 'delete' operation that also indicates the removal of the target slide
            // -> no changes here -> handling is done with deleteTargetSlide operation
            oneOperation = { name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);
        });
    });

});
