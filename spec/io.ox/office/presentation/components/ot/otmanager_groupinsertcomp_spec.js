/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // insertParagraph and local group operation (handleGroupInsertComp)
        // { name: group, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed insertParagraph operation after local group operation', function () {

            oneOperation = { name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 0, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 1, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 3, 5, 6, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 3, 5, 6, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 0, 1]);

            oneOperation = { name: 'insertParagraph', start: [4, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 3, 5, 6, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([4, 1, 1]);

            oneOperation = { name: 'insertParagraph', start: [2, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 3, 5, 6, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 1, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 1]);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 1, 1]);

        });

        // group and local insertParagraph operation (handleGroupInsertComp)
        // { name: group, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed group operation after local insertParagraph operation', function () {

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3]);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3]);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3]);
        });

        // insertSlide and local group operation (handleGroupInsertComp)
        // { name: group, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed insertSlide operation after local group operation', function () {

            oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0]);

            oneOperation = { name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([4]);
        });

        // group and local insertSlide operation (handleGroupInsertComp)
        // { name: group, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed group operation after local insertSlide operation', function () {

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([4, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3]);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([4, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3]);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3]);
        });
    });

});
