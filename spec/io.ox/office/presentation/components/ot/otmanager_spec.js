/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import { TextBaseOTManager } from '@/io.ox/office/textframework/model/otmanager';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ============================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var otManager = null;

    // the test runner
    var TestRunner = Utils.TestRunner;
    var testRunner = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction;
    var expectActions = Utils.expectActions;

    // existence check ----------------------------------------------------

    it('should subclass TextBaseOTManager', function () {
        expect(PresentationOTManager).toBeSubClassOf(TextBaseOTManager);
    });

    it('should construct successfully', function () {
        // constructing manager later, so that not already the require fails
        otManager = new PresentationOTManager();
        testRunner = new TestRunner(otManager);
    });

    // public methods -----------------------------------------------------

    describe('method hasAliasName', function () {
        it('should contain operations for alias "insertChar"', function () {
            expect(otManager.hasAliasName({ name: 'insertText' }, 'insertChar')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'insertDrawing' }, 'insertChar')).toBeTrue();
        });
        it('should contain operations for alias "insertComp"', function () {
            expect(otManager.hasAliasName({ name: 'insertParagraph' }, 'insertComp')).toBeTrue();
            expect(otManager.hasAliasName({ name: 'insertSlide' }, 'insertComp')).toBeTrue();
        });
    });

    // checking that no presentation handler is missing
    describe('method transformOperation() handles all operations', function () {

        it('should transform the specified external action with the specified local actions', function () {

            var externalActions = [
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [0, 0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [0, 0, 1], end: [0, 0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [0, 0, 1, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [0, 0, 1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [0, 0, 1, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [0, 0, 1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComment', start: [0, 0], text: '1111', date: '2020-01-07T09:35:46Z', pos: [529, 529], author: 'abc', initials: 'a', providerId: 'ox', userId: '12', is_new_thread: true }] },
                { operations: [{ name: 'insertComment', start: [0, 1], text: '2222', date: '2020-01-07T09:37:43Z', pos: [529, 529], author: 'def', initials: 'd', providerId: 'ox', userId: '14', parent: 0, is_new_comment: true }] },
                { operations: [{ name: 'changeComment', start: [0, 1], text: '3333', parent: 0 }] },
                { operations: [{ name: 'deleteComment', start: [0, 1], is_delete_as_thread: true }] },
                { operations: [{ name: 'deleteComment', start: [0, 0], is_delete_as_thread: true }] },
                { operations: [{ name: 'insertField', start: [0, 0, 1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [0, 0, 1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [0, 0, 1, 7], type: 'shape', opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [0, 0], end: [0, 0], to: [0, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [0, 2], end: [0, 2], to: [0, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'group', start: [0, 0], drawings: [0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'ungroup', start: [0, 0], drawings: [0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [0, 0, 1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [0, 0, 1], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [0, 0, 1, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [0, 0, 1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [0, 1], attrs: { tableGrid: [1, 1, 1] }, type: 'table', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [0, 1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [0, 1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [0, 1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertSlide', start: [4], target: '214' }] },
                { operations: [{ name: 'moveSlide', start: [4], end: [6], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [6], opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteTargetSlide', id: 'abcd', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertLayoutSlide', start: 3, id: 'new_layout_id_1', target: 'master_id_1', attrs: { name: 'Title·Slide', type: 'title' } }] },
                { operations: [{ name: 'moveLayoutSlide', start: 4, id: 'new_layout_id_1', target: 'master_id_1', oldindex: 2, oldtarget: 'master_id_1' }] },
                { operations: [{ name: 'delete', start: [0], target: 'new_layout_id_1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteTargetSlide', id: 'new_layout_id_1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeLayout', start: [0], target: 'changed_layout_id_1' }] },
                { operations: [{ name: 'changeMaster', start: [0], target: 'changed_master_id_1' }] }, // only ODF
                { operations: [{ name: 'insertMasterSlide', start: [0], id: 'new_master_id_1' }] } // generated by undo
            ];
            var localActions = [
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [1, 0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [1, 0, 1], end: [1, 0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [1, 0, 1, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [1, 0, 1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [1, 0, 1, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [1, 0, 1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComment', start: [1, 0], text: '1111', date: '2020-01-07T09:35:46Z', pos: [529, 529], author: 'abc', initials: 'a', providerId: 'ox', userId: '12', is_new_thread: true }] },
                { operations: [{ name: 'insertComment', start: [1, 1], text: '2222', date: '2020-01-07T09:37:43Z', pos: [529, 529], author: 'def', initials: 'd', providerId: 'ox', userId: '14', parent: 0, is_new_comment: true }] },
                { operations: [{ name: 'changeComment', start: [1, 1], text: '3333', parent: 0 }] },
                { operations: [{ name: 'deleteComment', start: [1, 1], is_delete_as_thread: true }] },
                { operations: [{ name: 'deleteComment', start: [1, 0], is_delete_as_thread: true }] },
                { operations: [{ name: 'insertField', start: [1, 0, 1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [1, 0, 1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [1, 0, 1, 7], type: 'shape', opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [1, 0], end: [1, 0], to: [1, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [1, 2], end: [1, 2], to: [1, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'group', start: [1, 0], drawings: [0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'ungroup', start: [1, 0], drawings: [0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [1, 0, 1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [1, 0, 1], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [1, 0, 1, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [1, 0, 1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [1, 1], attrs: { tableGrid: [1, 1, 1] }, type: 'table', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [1, 1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [1, 1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [1, 1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [1, 1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertSlide', start: [8], target: '214' }] },
                { operations: [{ name: 'moveSlide', start: [8], end: [9], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [9], opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteTargetSlide', id: 'bcde', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertLayoutSlide', start: 1, id: 'new_layout_id_2', target: 'master_id_2', attrs: { name: 'Title·Slide', type: 'title' } }] },
                { operations: [{ name: 'moveLayoutSlide', start: 2, id: 'new_layout_id_2', target: 'master_id_2', oldindex: 3, oldtarget: 'master_id_2' }] },
                { operations: [{ name: 'delete', start: [0], target: 'new_layout_id_2', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteTargetSlide', id: 'new_layout_id_2', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeLayout', start: [2], target: 'changed_layout_id_2' }] },
                { operations: [{ name: 'changeMaster', start: [2], target: 'changed_master_id_2' }] }, // only ODF
                { operations: [{ name: 'insertMasterSlide', start: [0], id: 'new_master_id_2' }] } // generated by undo
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [1, 0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [1, 0, 1], end: [1, 0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [1, 0, 1, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [1, 0, 1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [1, 0, 1, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [1, 0, 1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComment', start: [1, 0], text: '1111', date: '2020-01-07T09:35:46Z', pos: [529, 529], author: 'abc', initials: 'a', providerId: 'ox', userId: '12', is_new_thread: true }] },
                { operations: [{ name: 'insertComment', start: [1, 1], text: '2222', date: '2020-01-07T09:37:43Z', pos: [529, 529], author: 'def', initials: 'd', providerId: 'ox', userId: '14', parent: 0, is_new_comment: true }] },
                { operations: [{ name: 'changeComment', start: [1, 1], text: '3333', parent: 0 }] },
                { operations: [{ name: 'deleteComment', start: [1, 1], is_delete_as_thread: true }] },
                { operations: [{ name: 'deleteComment', start: [1, 0], is_delete_as_thread: true }] },
                { operations: [{ name: 'insertField', start: [1, 0, 1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [1, 0, 1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [1, 0, 1, 7], type: 'shape', opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [1, 0], end: [1, 0], to: [1, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [1, 2], end: [1, 2], to: [1, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'group', start: [1, 0], drawings: [0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'ungroup', start: [1, 0], drawings: [0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [1, 0, 1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [1, 0, 1], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [1, 0, 1, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [1, 0, 1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [1, 1], attrs: { tableGrid: [1, 1, 1] }, type: 'table', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [1, 1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [1, 1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [1, 1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [1, 1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertSlide', start: [8], target: '214' }] },
                { operations: [{ name: 'moveSlide', start: [8], end: [9], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [9], opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteTargetSlide', id: 'bcde', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertLayoutSlide', start: 1, id: 'new_layout_id_2', target: 'master_id_2', attrs: { name: 'Title·Slide', type: 'title' } }] },
                { operations: [{ name: 'moveLayoutSlide', start: 2, id: 'new_layout_id_2', target: 'master_id_2', oldindex: 3, oldtarget: 'master_id_2' }] },
                { operations: [{ name: 'delete', start: [0], target: 'new_layout_id_2', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteTargetSlide', id: 'new_layout_id_2', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeLayout', start: [2], target: 'changed_layout_id_2' }] },
                { operations: [{ name: 'changeMaster', start: [2], target: 'changed_master_id_2' }] }, // only ODF
                { operations: [{ name: 'insertMasterSlide', start: [0], id: 'new_master_id_2' }] } // generated by undo
            ], localActions);
            expectActions([
                { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertParagraph', start: [0, 0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [0, 0, 1], end: [0, 0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [0, 0, 1, 0], text: 'a', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertTab', start: [0, 0, 1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertBookmark', start: [0, 0, 1, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRange', start: [0, 0, 1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertComment', start: [0, 0], text: '1111', date: '2020-01-07T09:35:46Z', pos: [529, 529], author: 'abc', initials: 'a', providerId: 'ox', userId: '12', is_new_thread: true }] },
                { operations: [{ name: 'insertComment', start: [0, 1], text: '2222', date: '2020-01-07T09:37:43Z', pos: [529, 529], author: 'def', initials: 'd', providerId: 'ox', userId: '14', parent: 0, is_new_comment: true }] },
                { operations: [{ name: 'changeComment', start: [0, 1], text: '3333', parent: 0 }] },
                { operations: [{ name: 'deleteComment', start: [0, 1], is_delete_as_thread: true }] },
                { operations: [{ name: 'deleteComment', start: [0, 0], is_delete_as_thread: true }] },
                { operations: [{ name: 'insertField', start: [0, 0, 1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'updateField', start: [0, 0, 1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [0, 0, 1, 7], type: 'shape', opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [0, 0], end: [0, 0], to: [0, 2], opl: 1, osn: 1 }] },
                { operations: [{ name: 'move', start: [0, 2], end: [0, 2], to: [0, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'group', start: [0, 0], drawings: [0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'ungroup', start: [0, 0], drawings: [0, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'splitParagraph', start: [0, 0, 1, 1], opl: 1, osn: 1 }] },
                { operations: [{ name: 'mergeParagraph', start: [0, 0, 1], paralength: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertHardBreak', start: [0, 0, 1, 0], opl: 1, osn: 1 }] },
                { operations: [{ name: 'setAttributes', start: [0, 0, 1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertDrawing', start: [0, 1], attrs: { tableGrid: [1, 1, 1] }, type: 'table', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertRows', start: [0, 1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertColumn', start: [0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteColumns', start: [0, 1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertCells', start: [0, 1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertSlide', start: [4], target: '214' }] },
                { operations: [{ name: 'moveSlide', start: [4], end: [6], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [6], opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteTargetSlide', id: 'abcd', opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertLayoutSlide', start: 3, id: 'new_layout_id_1', target: 'master_id_1', attrs: { name: 'Title·Slide', type: 'title' } }] },
                { operations: [{ name: 'moveLayoutSlide', start: 4, id: 'new_layout_id_1', target: 'master_id_1', oldindex: 2, oldtarget: 'master_id_1' }] },
                { operations: [{ name: 'delete', start: [0], target: 'new_layout_id_1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'deleteTargetSlide', id: 'new_layout_id_1', opl: 1, osn: 1 }] },
                { operations: [{ name: 'changeLayout', start: [0], target: 'changed_layout_id_1' }] },
                { operations: [{ name: 'changeMaster', start: [0], target: 'changed_master_id_1' }] }, // only ODF
                { operations: [{ name: 'insertMasterSlide', start: [0], id: 'new_master_id_1' }] } // generated by undo
            ], transformedActions);
        });

    });

    // checking that insertSlide is handled correctly, although a target is specified
    describe('method transformOperation() handles insertText and insertSlide operation', function () {

        it('should transform the specified insertSlide and insertText operations correctly', function () {

            var externalActions = [
                { operations: [{ name: 'insertSlide', start: [4], target: '214' }] }
            ];
            var localActions = [
                { operations: [{ name: 'insertText', start: [8, 0, 1, 1], text: 'abc', opl: 1, osn: 1 }] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [{ name: 'insertText', start: [9, 0, 1, 1], text: 'abc', opl: 1, osn: 1 }] }
            ], localActions);
            expectActions([
                { operations: [{ name: 'insertSlide', start: [4], target: '214' }] }
            ], transformedActions);
        });

    });

    // checking that insertSlide is handled correctly, although a target is specified
    describe('method transformOperation() handles insertSlide and moveSlide operation', function () {

        it('should transform the specified insertSlide and moveSlide operations correctly', function () {

            var externalActions = [
                { operations: [{ name: 'insertSlide', start: [4], target: '214' }] }
            ];
            var localActions = [
                { operations: [{ name: 'moveSlide', start: [8], end: [9], opl: 1, osn: 1 }] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [{ name: 'moveSlide', start: [9], end: [10], opl: 1, osn: 1 }] }
            ], localActions);
            expectActions([
                { operations: [{ name: 'insertSlide', start: [4], target: '214' }] }
            ], transformedActions);
        });

    });

    // checking that insertSlide is handled correctly, although a target is specified in a group of operations
    describe('method transformOperation() handles group of slide operations with insertSlide operation', function () {

        it('should transform the specified group of operations correctly although insertSlide has a specified target', function () {

            var externalActions = [
                { operations: [{ name: 'insertSlide', start: [4], target: '214' }] },
                { operations: [{ name: 'moveSlide', start: [4], end: [6], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [6], opl: 1, osn: 1 }] }
            ];
            var localActions = [
                { operations: [{ name: 'insertSlide', start: [8], target: '214' }] },
                { operations: [{ name: 'moveSlide', start: [8], end: [9], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [9], opl: 1, osn: 1 }] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [{ name: 'insertSlide', start: [8], target: '214' }] },
                { operations: [{ name: 'moveSlide', start: [8], end: [9], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [9], opl: 1, osn: 1 }] }
            ], localActions);
            expectActions([
                { operations: [{ name: 'insertSlide', start: [4], target: '214' }] },
                { operations: [{ name: 'moveSlide', start: [4], end: [6], opl: 1, osn: 1 }] },
                { operations: [{ name: 'delete', start: [6], opl: 1, osn: 1 }] }
            ], transformedActions);
        });

    });

    // checking that no presentation handler is missing
    describe('method transformOperation() handles deleting slides', function () {

        it('should transform the specified external action with the specified local actions', function () {

            var externalActions = [
                { operations: [{ name: 'delete', start: [6], opl: 1, osn: 1 }] }
            ];
            var localActions = [
                { operations: [{ name: 'insertSlide', start: [8], target: '214' }] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [{ name: 'insertSlide', start: [7], target: '214' }] }
            ], localActions);
            expectActions([
                { operations: [{ name: 'delete', start: [6], opl: 1, osn: 1 }] }
            ], transformedActions);
        });

    });

    // Unit tests for task DOCS-2304
    describe('checks for task DOCS-2304', function () {

        it('should handle test 1 correctly', function () {

            var externalAction = [
                { operations: [
                    { name: 'insertDrawing', start: [0, 31], type: 'shape' },
                    { name: 'insertParagraph', start: [0, 31, 0] },
                    { name: 'insertText', start: [0, 31, 0, 0], text: '123' }
                ] }
            ];
            var localAction = [
                { operations: [
                    { name: 'changeLayout', start: [0], target: '123456' },
                    { name: 'insertDrawing', start: [0, 32], type: 'shape', drawing: { name: 'ctrTitle 1' } },
                    { name: 'insertParagraph', start: [0, 32, 0] }
                ] }
            ];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([
                { operations: [
                    { name: 'changeLayout', start: [0], target: '123456' },
                    { name: 'insertDrawing', start: [0, 33], type: 'shape', drawing: { name: 'ctrTitle 1' } },
                    { name: 'insertParagraph', start: [0, 33, 0] }
                ] }
            ], localAction);
            expectActions([
                { operations: [
                    { name: 'insertDrawing', start: [0, 31], type: 'shape' },
                    { name: 'insertParagraph', start: [0, 31, 0] },
                    { name: 'insertText', start: [0, 31, 0, 0], text: '123' }
                ] }
            ], transformedActions);
        });

        it('should handle test 2 correctly', function () {

            var externalAction = [
                { operations: [
                    { name: 'insertDrawing', start: [0, 31], type: 'shape' },
                    { name: 'insertParagraph', start: [0, 31, 0] },
                    { name: 'insertText', start: [0, 31, 0, 0], text: '123' }
                ] }
            ];
            var localAction = [
                { operations: [
                    { name: 'insertDrawing', start: [0, 32], type: 'shape', drawing: { name: 'ctrTitle 1' } },
                    { name: 'insertParagraph', start: [0, 32, 0] }
                ] }
            ];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([
                { operations: [
                    { name: 'insertDrawing', start: [0, 33], type: 'shape', drawing: { name: 'ctrTitle 1' } },
                    { name: 'insertParagraph', start: [0, 33, 0] }
                ] }
            ], localAction);
            expectActions([
                { operations: [
                    { name: 'insertDrawing', start: [0, 31], type: 'shape' },
                    { name: 'insertParagraph', start: [0, 31, 0] },
                    { name: 'insertText', start: [0, 31, 0, 0], text: '123' }
                ] }
            ], transformedActions);
        });

        it('should handle test 3 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertParagraph', start: [0, 31, 0] }] }];
            var localAction = [
                { operations: [
                    { name: 'insertDrawing', start: [0, 32], type: 'shape', drawing: { name: 'ctrTitle 1' } },
                    { name: 'insertParagraph', start: [0, 32, 0] }
                ] }
            ];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([
                { operations: [
                    { name: 'insertDrawing', start: [0, 32], type: 'shape', drawing: { name: 'ctrTitle 1' } },
                    { name: 'insertParagraph', start: [0, 32, 0] }
                ] }
            ], localAction);
            expectActions([{ operations: [{ name: 'insertParagraph', start: [0, 31, 0] }] }], transformedActions);
        });

        it('should handle test 4 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertSlide', start: [5], target: '214' }] }];
            var localAction = [{ operations: [{ name: 'changeLayout', start: [0], target: '123456' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'changeLayout', start: [0], target: '123456' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertSlide', start: [5], target: '214' }] }], transformedActions);
        });

        it('should handle test 5 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertDrawing', start: [0, 5] }] }];
            var localAction = [{ operations: [{ name: 'changeLayout', start: [0], target: '123456' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'changeLayout', start: [0], target: '123456' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertDrawing', start: [0, 5] }] }], transformedActions);
        });

        it('should handle test 6 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertParagraph', start: [0, 5, 0] }] }];
            var localAction = [{ operations: [{ name: 'changeLayout', start: [0], target: '123456' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'changeLayout', start: [0], target: '123456' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertParagraph', start: [0, 5, 0] }] }], transformedActions);
        });

        it('should handle test 7 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }] }];
            var localAction = [{ operations: [{ name: 'changeLayout', start: [0], target: '123456' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'changeLayout', start: [0], target: '123456' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }] }], transformedActions);
        });

        it('should handle test 8 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertSlide', start: [5], target: '214' }] }];
            var localAction = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }] }], localAction);
            expectActions([{ operations: [{ name: 'insertSlide', start: [5], target: '214' }] }], transformedActions);
        });

        it('should handle test 9 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertDrawing', start: [0, 5] }] }];
            var localAction = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }] }], localAction);
            expectActions([{ operations: [{ name: 'insertDrawing', start: [0, 5] }] }], transformedActions);
        });

        it('should handle test 10 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertParagraph', start: [0, 5, 0] }] }];
            var localAction = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }] }], localAction);
            expectActions([{ operations: [{ name: 'insertParagraph', start: [0, 5, 0] }] }], transformedActions);
        });

        it('should handle test 11 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }] }];
            var localAction = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }] }], localAction);
            expectActions([{ operations: [{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }] }], transformedActions);
        });

        it('should handle test 12 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertSlide', start: [5], target: '214' }] }];
            var localAction = [{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertSlide', start: [5], target: '214' }] }], transformedActions);
        });

        it('should handle test 13 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertDrawing', start: [0, 5] }] }];
            var localAction = [{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertDrawing', start: [0, 5] }] }], transformedActions);
        });

        it('should handle test 14 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertParagraph', start: [0, 5, 0] }] }];
            var localAction = [{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertParagraph', start: [0, 5, 0] }] }], transformedActions);
        });

        it('should handle test 15 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }] }];
            var localAction = [{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }] }], transformedActions);
        });

        it('should handle test 16 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertSlide', start: [5], target: '214' }] }];
            var localAction = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertSlide', start: [5], target: '214' }] }], transformedActions);
        });

        it('should handle test 17 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertDrawing', start: [0, 5] }] }];
            var localAction = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertDrawing', start: [0, 5] }] }], transformedActions);
        });

        it('should handle test 18 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertParagraph', start: [0, 5, 0] }] }];
            var localAction = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertParagraph', start: [0, 5, 0] }] }], transformedActions);
        });

        it('should handle test 19 correctly', function () {

            var externalAction = [{ operations: [{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }] }];
            var localAction = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2' }] }];
            var transformedActions = testRunner.transformActions(externalAction, localAction);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2' }] }], localAction);
            expectActions([{ operations: [{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }] }], transformedActions);
        });

    });

    describe.skip('method transformActions() handles group operations and inserting text', function () {

        it('should calculate valid insertText operation, if the ungrouped drawing is not grouped again', function () {

            var externalActions = [
                { operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [3, 1, 2, 0, 0], text: 'aaa', opl: 1, osn: 1 }] }
            ];
            var localActions = [
                { operations: [{ name: 'group', start: [3, 1], drawings: [1, 5, 6], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [3, 1, 1, 0, 0], text: 'bbb', opl: 1, osn: 1 }] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [] },
                { operations: [{ name: 'insertText', start: [3, 3, 0, 0], text: 'bbb', opl: 1, osn: 1 }] } // after 1, 2 and 3 are grouped, 4 becomes 2 and 5 becomes 3
            ], localActions);
            expectActions([
                { operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 5, 6], opl: 1, osn: 1 }, { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [3, 1, 2, 0, 0], text: 'aaa', opl: 1, osn: 1 }] }
            ], transformedActions);
        });

        it('should calculate valid insertText operation, if the ungrouped drawing is grouped again', function () {

            var externalActions = [
                { operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [3, 1, 2, 0, 0], text: 'aaa', opl: 1, osn: 1 }] }
            ];
            var localActions = [
                { operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 6], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [3, 1, 1, 0, 0], text: 'bbb', opl: 1, osn: 1 }] }
            ];
            var transformedActions = testRunner.transformActions(externalActions, localActions);
            expectActions([
                { operations: [] },
                { operations: [{ name: 'insertText', start: [3, 1, 2, 0, 3], text: 'bbb', opl: 1, osn: 1 }] } // after 1, 2 and 3 are grouped, 3 becomes 3,1,2 (and inserting behind 'aaa')
            ], localActions);
            expectActions([
                { operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 6], opl: 1, osn: 1 }, { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] },
                { operations: [{ name: 'insertText', start: [3, 1, 2, 0, 0], text: 'aaa', opl: 1, osn: 1 }] }
            ], transformedActions);
        });
    });

    describe('method transformPosition', function () {

        let pos, localActions;

        it('should handle correctly local position after external move operation', function () {

            pos = [1, 1];
            localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [1, 1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2]);

            pos = [0, 1];
            localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [1, 1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 1]);

            pos = [1, 0];
            localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [1, 1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 0]);

            pos = [1, 3];
            localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [1, 1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1]);

            pos = [1, 4];
            localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [1, 1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 4]);

            pos = [2, 1];
            localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [1, 1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 1]);

            pos = [2, 0];
            localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [1, 1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 0]);

            pos = [1, 3, 4, 8];
            localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [1, 1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 4, 8]);

            pos = [0, 3];
            localActions = [{ operations: [{ name: 'move', start: [0, 3], end: [0, 3], to: [0, 1], target: '123456', opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 3]);
        });

        it('should handle correctly local position after external moveSlide operation downwards', function () {

            pos = [1, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 2]);

            pos = [1, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 2]);

            pos = [1, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 2]);

            pos = [0, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3, 2]);

            pos = [2, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2]);

            pos = [2, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2]);

            pos = [2, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2]);
        });

        it('should handle correctly local position after external moveSlide operation upwards', function () {

            pos = [1, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2]);

            pos = [1, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2]);

            pos = [1, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 2]);

            pos = [0, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2]);

            pos = [0, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2]);

            pos = [0, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2]);

            pos = [2, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 2]);

            pos = [2, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 2]);

            pos = [2, 2];
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([3, 2]);
        });

        it('should handle correctly local position after external group operation', function () {

            pos = [1, 0];
            localActions = [{ operations: [{ name: 'group', start: [1, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 0]);

            pos = [2, 0];
            localActions = [{ operations: [{ name: 'group', start: [1, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([2, 0]);

            pos = [1, 1];
            localActions = [{ operations: [{ name: 'group', start: [1, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 0]);

            pos = [1, 2];
            localActions = [{ operations: [{ name: 'group', start: [1, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 1]);

            pos = [1, 2, 4, 2];
            localActions = [{ operations: [{ name: 'group', start: [1, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 1, 4, 2]);

            pos = [1, 4, 4, 2];
            localActions = [{ operations: [{ name: 'group', start: [1, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2, 4, 2]);

            pos = [1, 4, 4, 2];
            localActions = [{ operations: [{ name: 'group', start: [1, 1], drawings: [1, 2, 3, 5, 7, 8], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 2, 4, 2]);

            pos = [1, 4, 4, 2];
            localActions = [{ operations: [{ name: 'group', start: [1, 1], drawings: [1, 2, 3, 4, 5, 7, 8], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 1, 3, 4, 2]);

            pos = [0, 0];
            localActions = [{ operations: [{ name: 'group', start: [1, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 0]);

            pos = [1, 6];
            localActions = [{ operations: [{ name: 'group', start: [1, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([1, 4]);

            pos = [0, 3];
            localActions = [{ operations: [{ name: 'group', start: [1, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            pos = otManager.transformPosition(pos, localActions);
            expect(pos).toEqual([0, 3]);
        });
    });
});
