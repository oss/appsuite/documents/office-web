/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ============================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        localOperation = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    // the test runner
    var TestRunner = Utils.TestRunner;
    var testRunner = new TestRunner(otManagerPresentation);

    describe('method transformOperation', function () {

        // setAttributes and local group operation (handleGroupSetAttrs)
        it('should calculate valid transformed setAttributes operation after local group operation', function () {

            oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2]);
            expect(transformedOps[0].end).toEqual([3, 2]);

            oneOperation = { name: 'setAttributes', start: [3], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);
            expect(transformedOps[0].end).toEqual([3]);

            oneOperation = { name: 'setAttributes', start: [2], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2]);
            expect(transformedOps[0].end).toEqual([2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 1]);
            expect(transformedOps[0].end).toEqual([3, 2, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 3]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 1]);
            expect(transformedOps[0].end).toEqual([3, 3, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 3, 5, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 1]);
            expect(transformedOps[0].end).toEqual([3, 3, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 3, 4, 5, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 2, 1]);
            expect(transformedOps[0].end).toEqual([3, 1, 2, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 4, 5, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 1, 1]);
            expect(transformedOps[0].end).toEqual([3, 1, 1, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 0, 1], end: [3, 0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 4, 5, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 0, 1]);
            expect(transformedOps[0].end).toEqual([3, 0, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 4, 5, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 1]);
            expect(transformedOps[0].end).toEqual([3, 4, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 4, 5, 7], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 4, 5, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 1]);
            expect(transformedOps[0].end).toEqual([3, 4, 2]);

            oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 4, 5, 7], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 4, 5, 7]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 1, 1]);
            expect(transformedOps[0].end).toEqual([3, 1, 1, 2]);
        });

        // group and local setAttributes operation (handleGroupSetAttrs)
        it('should calculate valid transformed group operation after local setAttributes operation', function () {

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3]);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 3, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 3, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 1], end: [3, 1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 0, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 0, 4]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 1], end: [3, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 0, 1]);
            expect(localActions[0].operations[0].end).toEqual([3, 1, 0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 0, 1]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3, 4]);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3]);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 3]);
        });

        // updateField and local group (handleGroupSetAttrs)
        it('should calculate valid transformed updateField operation after local group operation', function () {

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 1, 1, 1, 1], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 4, 1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' }], transformedOps);

            oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 1, 1, 1], representation: 'abc' };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 1, 0, 1, 1], representation: 'abc' }], transformedOps);
        });

        // updateField and external group (handleGroupSetAttrs)
        it('should calculate valid transformed updateField operation after external group operation', function () {

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 1, 1, 1, 1], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 4, 1, 1], representation: 'abc' }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 1, 1, 1], representation: 'abc' }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 1, 0, 1, 1], representation: 'abc' }] }], localActions);
            expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes for slide and local group operation (handleGroupSetAttrs)
        it('should not modify setAttributes operation for slides and local group operation', function () {

            oneOperation = { name: 'setAttributes', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [4], opl: 1, osn: 1 }], transformedOps);

        });

        // setAttributes for slide and local group operation (handleGroupSetAttrs)
        it('should not modify setAttributes operation for slides and external group operation', function () {

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes for drawing and local group operation (handleGroupSetAttrs)
        it('should calculate valid transformed setAttributes operation for drawings after local group operation', function () {

            oneOperation = { name: 'setAttributes', start: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [2, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 2], opl: 1, osn: 1 };
            localOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'setAttributes', start: [3, 2], opl: 1, osn: 1 };
            localOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'setAttributes', start: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3, 4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [4, 2], opl: 1, osn: 1 }], transformedOps);

        });

        // setAttributes for drawing and external group operation (handleGroupSetAttrs)
        it('should calculate valid transformed setAttributes operation for drawings after external group operation', function () {

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [2, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localOperation = { name: 'setAttributes', start: [3, 2], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 };
            localOperation = { name: 'setAttributes', start: [3, 2], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'group', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [4, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [4, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes for drawing and local group operation (handleGroupSetAttrs)
        it('should calculate valid transformed setAttributes operation for locally grouped drawing', function () {

            oneOperation = { name: 'setAttributes', start: [3, 1], opl: 1, osn: 1 };
            localOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'setAttributes', start: [3, 1], opl: 1, osn: 1 };
            localOperation = { name: 'group', start: [3, 1], drawings: [1, 3, 7], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'setAttributes', start: [3, 3], opl: 1, osn: 1 };
            localOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'group', start: [0, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'group', start: [0, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes for drawing and external group operation (handleGroupSetAttrs)
        it('should calculate valid transformed setAttributes operation for externally grouped drawing', function () {

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localOperation = { name: 'setAttributes', start: [3, 1], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 3, 7], opl: 1, osn: 1 };
            localOperation = { name: 'setAttributes', start: [3, 1], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localOperation = { name: 'setAttributes', start: [3, 3], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'setAttributes', start: [0, 1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [0, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'group', start: [0, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [0, 1], target: '123', opl: 1, osn: 1 }], transformedOps);
        });
    });

});
