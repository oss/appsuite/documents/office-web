/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        localOperation = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    // the test runner
    var TestRunner = Utils.TestRunner;
    var testRunner = new TestRunner(otManagerPresentation);

    // pptx: changeLayout operation (handleChangeLayoutDeleteTargetSlide)
    // pptx: changeMaster operation (handleChangeLayoutDeleteTargetSlide)

    // changeLayout: { start: [2], target: "new_layout_id" }
    // changeMaster: { start: [2], target: "new_master_id" }

    // -> the 'deleteTargetSlide' operation conflicts with changeLayout/changeMaster operation,
    //    if the new assigned layout/master slide is removed.
    //
    // delete operation and deleteTargetSlide operation:
    //  { name: "delete", start: [0], target: "2147483761" }
    //  { name: "deleteTargetSlide", id: "2147483761", index: 2, parenttarget: "2147483756" }
    //

    describe('method transformOperation', function () {

        it('should calculate valid transformed changeLayout operation after local deleteTargetSlide operation of a different layout slide', function () {

            oneOperation = { name: 'changeLayout', start: [1], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeLayout', start: [1], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed changeLayout operation after local deleteTargetSlide operation of master slide', function () {

            oneOperation = { name: 'changeLayout', start: [1], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeLayout', start: [1], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should reload document if the layout slide for the changed document slide was locally removed', function () {

            oneOperation = { name: 'changeLayout', start: [1], target: 'layout_1', opl: 1, osn: 1 };
            localOperation = { name: 'deleteTargetSlide', id: 'layout_1', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });

        it('should calculate valid transformed changeMaster operation after local deleteTargetSlide operation of a different layout slide', function () {

            oneOperation = { name: 'changeMaster', start: [1], target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeMaster', start: [1], target: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed changeMaster operation after local deleteTargetSlide operation of master slide', function () {

            oneOperation = { name: 'changeMaster', start: [1], target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeMaster', start: [1], target: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should reload document if the master slide for the changed document slide was locally removed', function () {

            oneOperation = { name: 'changeMaster', start: [1], target: 'master_1', opl: 1, osn: 1 };
            localOperation = { name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });
    });

});
