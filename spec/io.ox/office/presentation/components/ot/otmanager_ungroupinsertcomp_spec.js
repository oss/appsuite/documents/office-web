/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertParagraph and local ungroup operation (handleUngroupInsertComp)
        // { name: ungroup, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed insertParagraph operation after local ungroup operation', function () {

            oneOperation = { name: 'insertParagraph', start: [3, 1, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 1, 2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 3, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 6, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 9, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 9, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 9, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 6, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 8, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [4, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [4, 1, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [2, 1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [2, 1, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 2, 1], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 2, 1], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertParagraph', start: [3, 2, 1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertParagraph', start: [3, 4, 1], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        // ungroup and local insertParagraph operation (handleUngroupInsertComp)
        // { name: ungroup, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed ungroup operation after local insertParagraph operation', function () {

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [3, 3, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [3, 5, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [3, 6, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertParagraph', start: [3, 0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
        });

        // insertSlide and local ungroup operation (handleUngroupInsertComp)
        // { name: ungroup, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed insertSlide operation after local ungroup operation', function () {

            oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [4, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [4, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }], transformedOps);
        });

        // ungroup and local insertSlide operation (handleUngroupInsertComp)
        // { name: ungroup, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed ungroup operation after local insertSlide operation', function () {

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', target: '123', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [4, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [4, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
        });
    });

});
