/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // delete and local moveSlide operation (handleMoveSlideDelete)
        // { name: moveSlide, start: [2], end: [4] }
        it('should calculate valid transformed delete operation after local moveSlide operation, if content on a slide is removed', function () {

            oneOperation = { name: 'delete', start: [3, 2, 0, 1], end: [3, 2, 0, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4, 2, 0, 1], end: [4, 2, 0, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [5, 2], end: [5, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [5, 2], end: [5, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4, 2], end: [4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 2], end: [1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0, 2], end: [0, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2, 2], end: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2, 2], end: [2, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0, 2], end: [0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0, 2], end: [0, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 2], end: [1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4, 2], end: [4, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2, 2], end: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 2], end: [1, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2, 2], end: [2, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4, 2], end: [4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [5, 2], end: [5, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [5, 2], end: [5, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0, 2], end: [0, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0, 2], end: [0, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1, 2], end: [1, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2, 2], end: [2, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2, 2], end: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4, 2], end: [4, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4, 2], end: [4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 2], end: [1, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [5, 2], end: [5, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [5, 2], end: [5, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4, 2], end: [4, 2], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4, 2], end: [4, 2], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4, 2], end: [4, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [1], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [1], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4, 2], end: [4, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4, 2], end: [4, 2], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [1], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [1], target: '234567', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4, 2], end: [4, 2], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4, 2], end: [4, 2], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [1], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [1], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 2], end: [1, 2], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed delete operation after local moveSlide operation, if a slide is removed', function () {

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [6], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [5], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [6], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [5], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [6], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [5], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [6], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [5], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [6], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [5], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [6], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [6], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [6], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [6], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [6], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [6], end: [3], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [6], end: [3], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [6], end: [3], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [6], end: [3], target: '234567', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [6], end: [3], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [5], end: [3], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [5], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        // moveSlide and local delete operation (handleMoveDelete)
        // { name: moveSlide, start: [2], end: [4] }
        it('should calculate valid transformed moveSlide operation after local delete operation, if content on a slide is removed', function () {

            oneOperation = { name: 'moveSlide', start: [1], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 4], end: [1, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [6, 4], end: [6, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [5], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [5], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [5], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [5], end: [6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [5], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [5], end: [2], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveSlide operation after local delete operation, if a slide is removed', function () {

            oneOperation = { name: 'moveSlide', start: [1], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [0], end: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [6], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [5], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [5], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [5], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [5], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [5], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [6], end: [1], opl: 1, osn: 1 }], transformedOps);
        });
    });
});
