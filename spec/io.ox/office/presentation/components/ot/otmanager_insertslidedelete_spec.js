/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class OTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // delete and local insertSlide (handleInsertParaDelete)
        it('should calculate valid transformed delete operation after local insertSlide operation', function () {

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10] };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2, 10], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], target: '123456' };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 10], target: '123456' }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0], target: '123456' }; // deleting a layout slide
            localActions = [{ operations: [{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [0], target: '123456' }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10, 2, 3] };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [2, 10, 2, 3] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [1, 14] };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [2, 10], end: [2, 14] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3] }; // deleting a slide
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [4] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0] };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [0] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [2] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1] }], transformedOps);

            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }; // not supported inside one operation
            localActions = [{ operations: [{ name: 'insertSlide', start: [5], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }], transformedOps);
        });

        // delete and external insertSlide (handleInsertParaDelete)
        it('should calculate valid transformed delete operation after external insertSlide operation', function () {

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 10], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 10], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 10], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 10], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 10], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 10], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);
        });

        // delete of layout slide and local insertSlide (handleInsertParaDelete)
        it('should calculate valid transformed delete of layout slide operation after external insertSlide operation', function () {

            oneOperation = { name: 'delete', start: [0], target: '456', opl: 1, osn: 1 }; // not supported inside one operation
            localActions = [{ operations: [{ name: 'insertSlide', start: [5], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [5], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: '456', opl: 1, osn: 1 }], transformedOps);

            // the conflict of insertSlide and deleting the corresponding target slide is handled with deleteTargetSlide operation!
            oneOperation = { name: 'delete', start: [0], target: '123', opl: 1, osn: 1 }; // not supported inside one operation
            localActions = [{ operations: [{ name: 'insertSlide', start: [5], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [5], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: '123', opl: 1, osn: 1 }], transformedOps);
        });
    });
});
