/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ============================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    // Tests, that at least null is specified as OT handler -> leading to a noop-handler.

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // insertLayoutSlide and deleteColumns
        it('should not transform insertLayoutSlide operation after local deleteColumns operation on master slide', function () {
            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform deleteColumns operation on master slide operation after local insertLayoutSlide operation', function () {
            oneOperation = { name: 'deleteColumns', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // moveLayoutSlide and deleteColumns
        it('should not transform moveLayoutSlide operation after local deleteColumns operation on master slide', function () {
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform deleteColumns operation on master slide operation after local moveLayoutSlide operation', function () {
            oneOperation = { name: 'deleteColumns', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // insertLayoutSlide and insertCells
        it('should not transform insertLayoutSlide operation after local insertCells operation on master slide', function () {
            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [0, 2, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform insertCells operation on master slide operation after local insertLayoutSlide operation', function () {
            oneOperation = { name: 'insertCells', start: [0, 2, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2, 2]);
        });

        // moveLayoutSlide and insertCells
        it('should not transform moveLayoutSlide operation after local insertCells operation on master slide', function () {
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertCells', start: [0, 2, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform insertCells operation on master slide operation after local moveLayoutSlide operation', function () {
            oneOperation = { name: 'insertCells', start: [0, 2, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2, 2]);
        });

        // insertLayoutSlide and insertChar
        it('should not transform insertLayoutSlide operation after local insertText operation on master slide', function () {
            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 2, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform insertText operation on master slide operation after local insertLayoutSlide operation', function () {
            oneOperation = { name: 'insertText', start: [0, 2, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2, 2]);
        });

        // moveLayoutSlide and insertChar
        it('should not transform moveLayoutSlide operation after local insertText operation on master slide', function () {
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [0, 2, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform insertText operation on master slide operation after local moveLayoutSlide operation', function () {
            oneOperation = { name: 'insertText', start: [0, 2, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2, 2]);
        });

        // insertLayoutSlide and insertColumn
        it('should not transform insertLayoutSlide operation after local insertColumn operation on master slide', function () {
            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [0, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform insertColumn operation on master slide operation after local insertLayoutSlide operation', function () {
            oneOperation = { name: 'insertColumn', start: [0, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2]);
        });

        // moveLayoutSlide and insertColumn
        it('should not transform moveLayoutSlide operation after local insertColumn operation on master slide', function () {
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertColumn', start: [0, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform insertColumn operation on master slide operation after local moveLayoutSlide operation', function () {
            oneOperation = { name: 'insertColumn', start: [0, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2]);
        });

        // insertLayoutSlide and insertComp
        it('should not transform insertLayoutSlide operation after local insertParagraph operation on master slide', function () {
            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform insertParagraph operation on master slide operation after local insertLayoutSlide operation', function () {
            oneOperation = { name: 'insertParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // moveLayoutSlide and insertComp
        it('should not transform moveLayoutSlide operation after local insertParagraph operation on master slide', function () {
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform insertParagraph operation on master slide operation after local moveLayoutSlide operation', function () {
            oneOperation = { name: 'insertParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // insertLayoutSlide and insertRows
        it('should not transform insertLayoutSlide operation after local insertRows operation on master slide', function () {
            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform insertRows operation on master slide operation after local insertLayoutSlide operation', function () {
            oneOperation = { name: 'insertRows', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // moveLayoutSlide and insertRows
        it('should not transform moveLayoutSlide operation after local insertRows operation on master slide', function () {
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform insertRows operation on master slide operation after local moveLayoutSlide operation', function () {
            oneOperation = { name: 'insertRows', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // insertLayoutSlide and mergeComp
        it('should not transform insertLayoutSlide operation after local mergeParagraph operation on master slide', function () {
            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform mergeParagraph operation on master slide operation after local insertLayoutSlide operation', function () {
            oneOperation = { name: 'mergeParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // moveLayoutSlide and mergeComp
        it('should not transform moveLayoutSlide operation after local mergeParagraph operation on master slide', function () {
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform mergeParagraph operation on master slide operation after local moveLayoutSlide operation', function () {
            oneOperation = { name: 'mergeParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // insertLayoutSlide and move
        it('should not transform insertLayoutSlide operation after local move operation on master slide', function () {
            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [0, 2], to: [0, 0], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]);
            expect(localActions[0].operations[0].to).toEqual([0, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform move operation on master slide after local insertLayoutSlide operation', function () {
            oneOperation = { name: 'move', start: [0, 2], to: [0, 0], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2]);
            expect(transformedOps[0].to).toEqual([0, 0]);
        });

        // moveLayoutSlide and move
        it('should not transform moveLayoutSlide operation after local move operation on master slide', function () {
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [0, 2], to: [0, 0], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform move operation on master slide operation after local moveLayoutSlide operation', function () {
            oneOperation = { name: 'move', start: [0, 2], to: [0, 0], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2]);
        });

        // insertLayoutSlide and setAttributes
        it('should not transform insertLayoutSlide operation after local setAttributes operation on master slide', function () {
            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform setAttributes operation on master slide operation after local insertLayoutSlide operation', function () {
            oneOperation = { name: 'setAttributes', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // moveLayoutSlide and setAttributes
        it('should not transform moveLayoutSlide operation after local setAttributes operation on master slide', function () {
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform setAttributes operation on master slide operation after local moveLayoutSlide operation', function () {
            oneOperation = { name: 'setAttributes', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // insertLayoutSlide and splitComp
        it('should not transform insertLayoutSlide operation after local splitParagraph operation on master slide', function () {
            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform splitParagraph operation on master slide operation after local insertLayoutSlide operation', function () {
            oneOperation = { name: 'splitParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // moveLayoutSlide and splitComp
        it('should not transform moveLayoutSlide operation after local splitParagraph operation on master slide', function () {
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform splitParagraph operation on master slide operation after local moveLayoutSlide operation', function () {
            oneOperation = { name: 'splitParagraph', start: [0, 2, 2], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2, 2]);
        });

        // insertLayoutSlide and group
        it('should not transform insertLayoutSlide operation after local group operation on master slide', function () {
            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [0, 2], drawings: [2, 3, 4], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform group operation on master slide operation after local insertLayoutSlide operation', function () {
            oneOperation = { name: 'group', start: [0, 2], drawings: [2, 3, 4], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2]);
        });

        // moveLayoutSlide and group
        it('should not transform moveLayoutSlide operation after local group operation on master slide', function () {
            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [0, 2], drawings: [2, 3, 4], target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 2]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toBe(1);
        });

        it('should not transform group operation on master slide operation after local moveLayoutSlide operation', function () {
            oneOperation = { name: 'group', start: [0, 2], drawings: [2, 3, 4], target: 'master_123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_456789', target: 'master_123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toBe(1);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 2]);
        });
    });

});
