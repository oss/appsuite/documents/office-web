/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class OTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertText and local moveSlide operation (handleMoveSlideGeneric)
        it('should calculate valid transformed insertText operation after local moveSlide operation', function () {

            oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [2, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [2, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [2, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);
        });

        // insertText and external moveSlide operation (handleMoveSlideGeneric)
        it('should calculate valid transformed insertText operation after external moveSlide operation', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [2, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [2, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
        });

        // move and local moveSlide operation (handleMoveSlideGeneric)
        it('should calculate valid transformed move operation after local moveSlide operation', function () {

            oneOperation = { name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [0, 0], to: [0, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 0], to: [2, 2], opl: 1, osn: 1 }], transformedOps);
        });

        // move and external moveSlide operation (handleMoveSlideGeneric)
        it('should calculate valid transformed move operation after external moveSlide operation', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [0, 0], to: [0, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [2, 0], to: [2, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes and local moveSlide operation (handleMoveSlideGeneric)
        it('should calculate valid transformed setAttributes operation after local moveSlide operation', function () {

            oneOperation = { name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [0, 0], end: [0, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [2, 0], end: [2, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [1], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [1, 1], end: [1, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [0, 1], end: [0, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [1, 1], end: [1, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [0, 1], end: [0, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'setAttributes', start: [0, 1], end: [0, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'setAttributes', start: [1, 1], end: [1, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);
        });

        // setAttributes and external moveSlide operation (handleMoveSlideGeneric)
        it('should calculate valid transformed setAttributes operation after external moveSlide operation', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [0, 0], end: [0, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [2, 0], end: [2, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [1], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1], end: [1, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1], end: [0, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1], end: [1, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1], end: [0, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1], end: [0, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'setAttributes', start: [1, 1], end: [1, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }], transformedOps);
        });

        // ungroup and local moveSlide operation (handleMoveSlideGeneric)
        it('should calculate valid transformed ungroup operation after local moveSlide operation', function () {

            oneOperation = { name: 'ungroup', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [0, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [2, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [0, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [1, 1], opl: 1, osn: 1 }], transformedOps);
        });

        // // ungroup and external moveSlide operation (handleMoveSlideGeneric)
        it('should calculate valid transformed ungroup operation after external moveSlide operation', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [0, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [1, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }], transformedOps);
        });

        // handling moveSlide and comment operations
        // { name: 'insertComment', start: [0, 1], text: '2222', date: '2020-01-07T09:37:43Z', pos: [529, 529], author: 'def', initials: 'd', providerId: 'ox', userId: '14', parent: 0, is_new_comment: true }
        // { name: 'changeComment', start: [0, 1], text: '3333', parent: 0 }
        // { name: 'deleteComment', start: [0, 1], is_delete_as_thread: true }

        it('should calculate valid transformed insertComment operation after external moveSlide operation', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed changeComment operation after external moveSlide operation', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [2, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [2, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed deleteComment operation after external moveSlide operation', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertComment operation after internal moveSlide operation', function () {

            oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed changeComment operation after internal moveSlide operation', function () {

            oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [2, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [2, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed deleteComment operation after internal moveSlide operation', function () {

            oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);
        });

        // handling moveSlide and changeLayout or changeMaster operations (changeMaster is used in ODP)
        // { name: 'changeLayout', start: [2], target: 'layout_id' }
        // { name: 'changeMaster', start: [2], target: 'master_id' }

        it('should calculate valid transformed changeLayout operation after external moveSlide operation', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeLayout', start: [2], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeLayout', start: [2], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed changeMaster operation after external moveSlide operation (only ODP)', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeMaster', start: [2], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeMaster', start: [2], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed changeLayout operation after internal moveSlide operation', function () {

            oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeLayout', start: [2], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeLayout', start: [2], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed changeMaster operation after internal moveSlide operation (only ODP)', function () {

            oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeMaster', start: [2], target: 'master_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeMaster', start: [2], target: 'master_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }], transformedOps);
        });
    });
});
