/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // generic insert operation (into a drawing) and local move operation (handleMoveGeneric) in OX Presentation
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 6] }
        it('should calculate valid transformed insertRows operation after local move operation', function () {

            oneOperation = { name: 'insertRows', start: [3, 4, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 0]);

            oneOperation = { name: 'insertRows', start: [3, 2, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 0]);

            oneOperation = { name: 'insertRows', start: [3, 1, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 0]);

            oneOperation = { name: 'insertRows', start: [3, 5, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 0]);

            oneOperation = { name: 'insertRows', start: [3, 4, 0], count: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 5]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 0]);

        });

        // deleteColumns operation and local move operation (handleMoveGeneric) in OX Presentation
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 6] }
        it('should calculate valid transformed deleteColumns operation after local move operation', function () {

            oneOperation = { name: 'deleteColumns', start: [3, 4], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3]);

            oneOperation = { name: 'deleteColumns', start: [3, 2], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5]);

            oneOperation = { name: 'deleteColumns', start: [3, 1], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);

            oneOperation = { name: 'deleteColumns', start: [3, 5], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5]);

            oneOperation = { name: 'deleteColumns', start: [3, 4], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 5]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5]);

        });

        // insertColumn operation and local move operation (handleMoveGeneric) in OX Presentation
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 6] }
        it('should calculate valid transformed insertColumn operation after local move operation', function () {

            oneOperation = { name: 'insertColumn', start: [3, 4], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3]);

            oneOperation = { name: 'insertColumn', start: [3, 2], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5]);

            oneOperation = { name: 'insertColumn', start: [3, 1], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);

            oneOperation = { name: 'insertColumn', start: [3, 5], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5]);

            oneOperation = { name: 'insertColumn', start: [3, 4], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 5]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5]);
        });

        // mergeParagraph operation and local move operation (handleMoveGeneric) in OX Presentation
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 6] }
        it('should calculate valid transformed mergeParagraph operation after local move operation', function () {

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 0]);

            oneOperation = { name: 'mergeParagraph', start: [3, 2, 0], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 0]);

            oneOperation = { name: 'mergeParagraph', start: [3, 1, 0], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 0]);

            oneOperation = { name: 'mergeParagraph', start: [3, 5, 0], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 0]);

            oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 5]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 0]);
        });

        // move operation and local mergeParagraph operation (handleMoveGeneric) in OX Presentation
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 6] }
        it('should calculate valid transformed move operation after local mergeParagraph operation', function () {

            oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 3, 0]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2]);
            expect(transformedOps[0].end).toEqual([3, 2]);
            expect(transformedOps[0].to).toEqual([3, 5]);

            oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 2, 0], paralength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 5, 0]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2]);
            expect(transformedOps[0].end).toEqual([3, 2]);
            expect(transformedOps[0].to).toEqual([3, 5]);

            oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 0], paralength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1, 0]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2]);
            expect(transformedOps[0].end).toEqual([3, 2]);
            expect(transformedOps[0].to).toEqual([3, 5]);

            oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 5, 0]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2]);
            expect(transformedOps[0].end).toEqual([3, 2]);
            expect(transformedOps[0].to).toEqual([3, 5]);

            oneOperation = { name: 'move', start: [3, 5], end: [3, 5], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 5, 0]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5]);
            expect(transformedOps[0].end).toEqual([3, 5]);
            expect(transformedOps[0].to).toEqual([3, 2]);
        });

    });
});
