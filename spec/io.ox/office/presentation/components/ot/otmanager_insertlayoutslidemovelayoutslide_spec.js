/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertLayoutSlide: { start: 1, target: "2147483648", id: "2147483660", attrs: { slide: { type: "titleOnly" }}}
        // moveLayoutSlide: { id: "2147483660", target: "2147483648", oldtarget: "2147483648", oldindex: 1, start: 0 }

        it('should calculate valid transformed insertLayoutSlide operation after local moveLayoutSlide operation and completely different targets', function () {

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertLayoutSlide operation after local moveLayoutSlide operation and move within same master slide', function () {

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 5, target: '123456', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertLayoutSlide operation after local moveLayoutSlide operation and move away from master slide', function () {

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 2, oldtarget: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 3, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 2, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 0, target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertLayoutSlide operation after local moveLayoutSlide operation and move to master slide', function () {

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertLayoutSlide operation after external moveLayoutSlide operation and completely different targets', function () {

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertLayoutSlide operation after external moveLayoutSlide operation and move within same master slide', function () {

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 5, target: '123456', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertLayoutSlide operation after external moveLayoutSlide operation and move away from master slide', function () {

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 2, oldtarget: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 3, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 2, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 0, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertLayoutSlide operation after external moveLayoutSlide operation and move to master slide', function () {

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }], transformedOps);
        });
    });

});
