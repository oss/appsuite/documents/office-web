/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { Color } from '@/io.ox/office/editframework/utils/color';
import * as DOM from '@/io.ox/office/textframework/utils/dom';

import { createPresentationApp } from '~/presentation/apphelper';

// constants ==================================================================

const layoutId_1 = 'layout1';
const layoutId_2 = 'layout2';
const masterId_1 = 'master1';
const slide_1_id = 'slide_1'; // the ID of the first slide in document view
const text_default = 'Hello world!';
const text_para1_drawing1 = 'Hello paragraph 1 in drawing 1';
const text_para1_drawing2 = 'Hello paragraph 1 in drawing 2 of phType content body';
const text_para1_drawing3 = 'Hello paragraph 1 in drawing 3 of phType text body';
const text_layout1_drawing1 = 'Mastertitelformat bearbeiten';
const text_layout1_drawing2 = 'Master-Untertitelformat bearbeiten';

// tests ======================================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        selection = null,

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' },
                    defaultTextListStyles: {
                        l1: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 0 } },
                        l2: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 1270 } },
                        l3: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 2540 } }
                    }
                }
            },
            { name: 'insertMasterSlide', id: masterId_1 },

            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: 1905, top: 5918, width: 21590, height: 4083 } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: text_layout1_drawing1 },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: 3810, top: 10795, width: 17780, height: 4868 } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_1, text: text_layout1_drawing2 },

            { name: 'insertLayoutSlide', id: layoutId_2, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_2, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: 1905, top: 5918, width: 21590, height: 4083 } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_2 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_2, text: text_layout1_drawing1 },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_2, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: 3810, top: 10795, width: 17780, height: 4868 } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_2 },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_2, text: text_layout1_drawing2 },

            { name: 'insertSlide', start: [0], target: layoutId_1 },
            { name: 'insertDrawing', start: [0, 0], type: 'shape' },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertText', start: [0, 0, 0, 0], text: text_default },
            { name: 'insertDrawing', start: [0, 1], type: 'shape' },
            { name: 'insertParagraph', start: [0, 1, 0] },
            { name: 'insertText', start: [0, 1, 0, 0], text: text_default },
            { name: 'insertDrawing', start: [0, 2], type: 'shape' },
            { name: 'insertParagraph', start: [0, 2, 0] },
            { name: 'insertText', start: [0, 2, 0, 0], text: text_default },
            { name: 'insertDrawing', start: [0, 3], type: 'shape' },
            { name: 'insertParagraph', start: [0, 3, 0] },
            { name: 'insertText', start: [0, 3, 0, 0], text: text_default },
            { name: 'insertDrawing', start: [0, 4], type: 'shape' },
            { name: 'insertParagraph', start: [0, 4, 0] },
            { name: 'insertText', start: [0, 4, 0, 0], text: text_default },

            { name: 'insertSlide', start: [1], target: layoutId_1 },
            { name: 'insertDrawing', start: [1, 0], type: 'shape' },
            { name: 'insertParagraph', start: [1, 0, 0] },
            { name: 'insertDrawing', start: [1, 1], type: 'shape' },
            { name: 'insertParagraph', start: [1, 1, 0] },
            { name: 'insertDrawing', start: [1, 2], type: 'shape' },
            { name: 'insertParagraph', start: [1, 2, 0] },
            { name: 'insertDrawing', start: [1, 3], type: 'shape' },
            { name: 'insertParagraph', start: [1, 3, 0] },

            { name: 'insertSlide', start: [2], target: layoutId_1 },
            { name: 'insertDrawing', start: [2, 0], type: 'shape' },
            { name: 'insertParagraph', start: [2, 0, 0] },
            { name: 'insertText', start: [2, 0, 0, 0], text: text_para1_drawing1 },
            { name: 'insertDrawing', start: [2, 1], type: 'shape' },
            { name: 'insertParagraph', start: [2, 1, 0] },
            { name: 'insertText', start: [2, 1, 0, 0], text: text_para1_drawing2 },
            { name: 'insertDrawing', start: [2, 2], type: 'shape' },
            { name: 'insertParagraph', start: [2, 2, 0] },
            { name: 'insertText', start: [2, 2, 0, 0], text: text_para1_drawing3 },
            { name: 'insertDrawing', start: [2, 3], type: 'shape' },
            { name: 'insertDrawing', start: [2, 4], type: 'shape' },
            { name: 'insertDrawing', start: [2, 5], type: 'shape' },

            { name: 'insertSlide', start: [3], target: layoutId_1 },
            { name: 'insertDrawing', start: [3, 0], type: 'shape' },
            { name: 'insertParagraph', start: [3, 0, 0] },
            { name: 'insertDrawing', start: [3, 1], type: 'shape' },
            { name: 'insertParagraph', start: [3, 1, 0] },
            { name: 'insertDrawing', start: [3, 2], type: 'shape' },
            { name: 'insertParagraph', start: [3, 2, 0] },
            { name: 'insertDrawing', start: [3, 3], type: 'shape' },
            { name: 'insertParagraph', start: [3, 3, 0] }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        selection = model.getSelection();
    });

    function expectSlideSelection(expected) {
        expect(selection.isSlideSelection()).toBeTrue();
        expect(selection.isDrawingSelection()).toBeFalse();
        expect(selection.isMultiSelection()).toBeFalse();
        expect(selection.getStartPosition()).toEqual([expected, 0]);
        expect(selection.getEndPosition()).toEqual([expected, 0]);
        return model.getActiveSlideId();
    }

    function expectDrawingSelection(...expected) {
        expect(selection.isSlideSelection()).toBeFalse();
        expect(selection.isDrawingSelection()).toBeTrue();
        expect(selection.isMultiSelection()).toBe(expected.length > 1);
        const positions = selection.getAllLogicalPositions();
        expect(positions).toHaveLength(expected.length);
        positions.forEach((pos, idx) => {
            expect(pos.startPosition).toEqual(expected[idx]);
            const endPos = expected[idx].slice();
            endPos[endPos.length - 1] += 1;
            expect(pos.endPosition).toEqual(endPos);
        });
        return model.getActiveSlideId();
    }

    function expectTextSelection(start, end) {
        expect(selection.isSlideSelection()).toBeFalse();
        expect(selection.isDrawingSelection()).toBeFalse();
        expect(selection.isMultiSelection()).toBeFalse();
        expect(selection.getStartPosition()).toEqual(start);
        expect(selection.getEndPosition()).toEqual(end ?? start);
        return model.getActiveSlideId();
    }

    function expectSlideDrawings(expected) {
        const activeSlide = model.getSlideById(model.getActiveSlideId());
        const drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
        expect(drawings).toHaveLength(expected);
        return drawings;
    }

    // public methods -----------------------------------------------------

    describe('method applyExternalOperations', function () {

        it('should exist', function () {
            expect(model).toRespondTo('applyExternalOperations');
        });

        it('should have valid prepared first slide', function () {
            const drawings = expectSlideDrawings(5);
            expect($(drawings[0]).prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect($(drawings[1]).prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(1);
            expect($(drawings[2]).prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);
            expect($(drawings[3]).prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);
            expect($(drawings[4]).prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(4);
        });

        it('should set correct selection after local setAttributes operation and external move of drawing on same slide', async function () {

            selection.setTextSelection([0, 3, 0, 0], [0, 3, 0, 5]); // selecting world 'Hello'
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectTextSelection([0, 3, 0, 0], [0, 3, 0, 5]);

            // applying local operation (synchronously)
            model.setAttribute('character', 'bold', true);

            selection.setTextSelection([0, 3], [0, 4]);
            expectDrawingSelection([0, 3]);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'move', start: [0, 1], to: [0, 4] }
            ]);

            expectDrawingSelection([0, 2]);
            expectSlideDrawings(5);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectTextSelection([0, 2, 0, 0], [0, 2, 0, 5]);
        });

        it('should set correct selection after local setAttributes operation and external move backwards of drawing on same slide', async function () {

            selection.setTextSelection([0, 2, 0, 0], [0, 2, 0, 5]);
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectTextSelection([0, 2, 0, 0], [0, 2, 0, 5]);

            // applying local operation (synchronously)
            model.setAttribute('character', 'bold', true);

            selection.setTextSelection([0, 2], [0, 3]);
            expectDrawingSelection([0, 2]);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'move', start: [0, 4], to: [0, 1] }
            ]);

            expectDrawingSelection([0, 3]);
            expectSlideDrawings(5);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectTextSelection([0, 3, 0, 0], [0, 3, 0, 5]);
        });

        it('should set correct selection after local setAttributes operation and external move slide operation', async function () {

            selection.setTextSelection([0, 3, 0, 0], [0, 3, 0, 5]); // selecting world 'Hello'
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectTextSelection([0, 3, 0, 0], [0, 3, 0, 5]);

            // applying local operation (synchronously)
            model.setAttribute('character', 'bold', true);

            selection.setTextSelection([0, 3], [0, 4]);
            expectDrawingSelection([0, 3]);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'moveSlide', start: [0], end: [2] }
            ]);

            expectDrawingSelection([2, 3]);
            expectSlideDrawings(5);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectTextSelection([2, 3, 0, 0], [2, 3, 0, 5]);
        });

        it('should set correct selection after local setAttributes operation and external undo of move slide operation', async function () {

            selection.setTextSelection([2, 3, 0, 0], [2, 3, 0, 5]); // selecting world 'Hello'
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectTextSelection([2, 3, 0, 0], [2, 3, 0, 5]);

            // applying local operation (synchronously)
            model.setAttribute('character', 'bold', true);

            selection.setTextSelection([2, 3], [2, 4]);
            expectDrawingSelection([2, 3]);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'moveSlide', start: [2], end: [0] }
            ]);

            expectDrawingSelection([0, 3]);
            expectSlideDrawings(5);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectTextSelection([0, 3, 0, 0], [0, 3, 0, 5]);
        });

        it('should set correct multi selection after local setAttributes operation and external move slide operation', async function () {

            selection.setMultiDrawingSelectionByPosition([[0, 1], [0, 2], [0, 4]]);
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectDrawingSelection([0, 1], [0, 2], [0, 4]);

            // applying local operation (synchronously)
            model.setDrawingFillColor(Color.YELLOW);

            selection.setSlideSelection();
            expectSlideSelection(0);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'moveSlide', start: [0], end: [2] }
            ]);

            expectSlideSelection(2);
            expectSlideDrawings(5);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectDrawingSelection([2, 1], [2, 2], [2, 4]);
        });

        it('should set correct multi selection after local setAttributes operation and external undo of move slide operation', async function () {

            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectDrawingSelection([2, 1], [2, 2], [2, 4]);

            // applying local operation (synchronously)
            model.setDrawingFillColor(Color.BLUE);

            selection.setSlideSelection();
            expectSlideSelection(2);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'moveSlide', start: [2], end: [0] }
            ]);

            expectSlideSelection(0);
            expectSlideDrawings(5);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectDrawingSelection([0, 1], [0, 2], [0, 4]);
        });

        it('should set correct multi selection after local setAttributes operation and external move operation', async function () {

            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectDrawingSelection([0, 1], [0, 2], [0, 4]);

            // applying local operation (synchronously)
            model.setDrawingFillColor(Color.BLUE);

            selection.setSlideSelection();
            expectSlideSelection(0);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'move', start: [0, 4], to: [0, 1] }
            ]);

            expectSlideSelection(0);
            expectSlideDrawings(5);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectDrawingSelection([0, 1], [0, 2], [0, 3]);
        });

        it('should set correct multi selection after local setAttributes operation and external undo of move operation', async function () {

            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectDrawingSelection([0, 1], [0, 2], [0, 3]);

            // applying local operation (synchronously)
            model.setDrawingFillColor(Color.BLUE);

            selection.setSlideSelection();
            expectSlideSelection(0);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'move', start: [0, 1], to: [0, 4] }
            ]);

            expectSlideSelection(0);
            expectSlideDrawings(5);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectDrawingSelection([0, 1], [0, 2], [0, 4]);
        });

        it('should set correct multi selection after local setAttributes operation and external deleting of one drawing', async function () {

            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectDrawingSelection([0, 1], [0, 2], [0, 4]);

            // applying local operation (synchronously)
            model.setDrawingFillColor(Color.YELLOW);

            selection.setSlideSelection();
            expectSlideSelection(0);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'delete', start: [0, 2], end: [0, 2] }
            ]);

            expectSlideSelection(0);
            expectSlideDrawings(4);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectDrawingSelection([0, 1], [0, 3]);
        });

        it('should set correct multi selection after local setAttributes operation and external inserting of one drawing', async function () {

            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectDrawingSelection([0, 1], [0, 3]);

            // applying local operation (synchronously)
            model.setDrawingFillColor(Color.BLUE);

            selection.setSlideSelection();
            expectSlideSelection(0);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [0, 2], type: 'shape' },
                { name: 'insertParagraph', start: [0, 2, 0] },
                { name: 'insertText', start: [0, 2, 0, 0], text: text_default }
            ]);

            expectSlideSelection(0);
            expectSlideDrawings(5);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectDrawingSelection([0, 1], [0, 4]);
        });

        it('should switch from multi selection to single drawing selection after local setAttributes operation and external deleting of one drawing', async function () {

            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectDrawingSelection([0, 1], [0, 4]);

            // applying local operation (synchronously)
            model.setDrawingFillColor(Color.YELLOW);

            selection.setSlideSelection();
            expectSlideSelection(0);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'delete', start: [0, 1], end: [0, 1] }
            ]);

            expectSlideSelection(0);
            expectSlideDrawings(4);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectDrawingSelection([0, 3]);
        });

        it('should set correct single drawing selection after local setAttributes operation and external inserting of one drawing', async function () {

            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectDrawingSelection([0, 3]);

            // applying local operation (synchronously)
            model.setDrawingFillColor(Color.BLUE);

            selection.setSlideSelection();
            expectSlideSelection(0);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [0, 1], type: 'shape' },
                { name: 'insertParagraph', start: [0, 1, 0] },
                { name: 'insertText', start: [0, 1, 0, 0], text: text_default }
            ]);

            expectSlideSelection(0);
            expectSlideDrawings(5);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectDrawingSelection([0, 4]);
        });

        it('should set correct multi selection after local setAttributes operation and external move, moveSlide and delete operations', async function () {

            selection.setMultiDrawingSelectionByPosition([[0, 1], [0, 2], [0, 4]]);
            expectDrawingSelection([0, 1], [0, 2], [0, 4]);

            // applying local operation (synchronously)
            model.setDrawingFillColor(Color.YELLOW);

            selection.setSlideSelection();
            expectSlideSelection(0);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'moveSlide', start: [2], end: [0] },    // slide 0 -> 1
                { name: 'moveSlide', start: [2], end: [0] },    // slide 1 -> 2
                { name: 'move', start: [2, 3], to: [2, 0] },    // new drawing selection on slide 2: 2, 3, 4
                { name: 'delete', start: [2, 3], end: [2, 3] }, // new drawing selection on slide 2: 2, 3
                { name: 'delete', start: [0] }                  // slide 2 -> 1
            ]);

            expectSlideSelection(1);
            expectSlideDrawings(4);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectDrawingSelection([1, 2], [1, 3]);
        });

        it('should set correct multi selection after local setAttributes operation and external undo of previous move, moveSlide and delete operations', async function () {

            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectDrawingSelection([1, 2], [1, 3]);

            // applying local operation (synchronously)
            model.setDrawingFillColor(Color.BLUE);

            selection.setSlideSelection();
            expectSlideSelection(1);

            // applying external operation
            await model.applyExternalOperations([
                { name: 'insertSlide', start: [0], target: layoutId_1 },          // slide 1 -> 2
                { name: 'insertDrawing', start: [0, 0], type: 'shape' },
                { name: 'insertParagraph', start: [0, 0, 0] },
                { name: 'insertText', start: [0, 0, 0, 0], text: text_default },
                { name: 'insertDrawing', start: [0, 1], type: 'shape' },
                { name: 'insertParagraph', start: [0, 1, 0] },
                { name: 'insertText', start: [0, 1, 0, 0], text: text_default },
                { name: 'insertDrawing', start: [0, 2], type: 'shape' },
                { name: 'insertParagraph', start: [0, 2, 0] },
                { name: 'insertText', start: [0, 2, 0, 0], text: text_default },
                { name: 'insertDrawing', start: [0, 3], type: 'shape' },
                { name: 'insertParagraph', start: [0, 3, 0] },
                { name: 'insertText', start: [0, 3, 0, 0], text: text_default },
                { name: 'insertDrawing', start: [0, 4], type: 'shape' },
                { name: 'insertParagraph', start: [0, 4, 0] },
                { name: 'insertText', start: [0, 4, 0, 0], text: text_default },

                { name: 'insertDrawing', start: [2, 3], type: 'shape' },          // new drawing selection on slide 2: 2, 4
                { name: 'insertParagraph', start: [2, 3, 0] },
                { name: 'insertText', start: [2, 3, 0, 0], text: text_default },

                { name: 'move', start: [2, 0], to: [2, 3] },                      // new drawing selection on slide 2: 1, 4

                { name: 'moveSlide', start: [0], end: [2] },                      // slide 2 -> 1
                { name: 'moveSlide', start: [0], end: [2] }                       // slide 1 -> 0
            ]);

            expectSlideSelection(0);
            expectSlideDrawings(5);

            // undoing the local operation
            await model.getUndoManager().undo();

            // checking the selection, must be set by selectionState object
            expectDrawingSelection([0, 1], [0, 4]);
        });
    });
});
