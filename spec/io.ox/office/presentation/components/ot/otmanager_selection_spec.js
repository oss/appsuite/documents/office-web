/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { Color } from '@/io.ox/office/editframework/utils/color';
import * as DOM from '@/io.ox/office/textframework/utils/dom';

import { waitForEvent } from '~/asynchelper';
import { createPresentationApp } from '~/presentation/apphelper';

// constants ==================================================================

const layoutId_1 = 'layout1';
const layoutId_2 = 'layout2';
const masterId_1 = 'master1';
const slide_1_id = 'slide_1'; // the ID of the first slide in document view
const slide_2_id = 'slide_2'; // the ID of the second slide in document view
const text_para1_drawing1 = 'Hello paragraph 1 in drawing 1';
const text_para1_drawing2 = 'Hello paragraph 1 in drawing 2 of phType content body';
const text_para1_drawing3 = 'Hello paragraph 1 in drawing 3 of phType text body';
const text_layout1_drawing1 = 'Mastertitelformat bearbeiten';
const text_layout1_drawing2 = 'Master-Untertitelformat bearbeiten';

// tests ======================================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        selection = null,

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' },
                    defaultTextListStyles: {
                        l1: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 0 } },
                        l2: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 1270 } },
                        l3: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 2540 } }
                    }
                }
            },
            { name: 'insertMasterSlide', id: masterId_1 },

            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: 1905, top: 5918, width: 21590, height: 4083 } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: text_layout1_drawing1 },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: 3810, top: 10795, width: 17780, height: 4868 } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_1, text: text_layout1_drawing2 },

            { name: 'insertLayoutSlide', id: layoutId_2, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_2, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: 1905, top: 5918, width: 21590, height: 4083 } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_2 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_2, text: text_layout1_drawing1 },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_2, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: 3810, top: 10795, width: 17780, height: 4868 } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_2 },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_2, text: text_layout1_drawing2 },

            { name: 'insertSlide', start: [0], target: layoutId_1 },
            { name: 'insertDrawing', start: [0, 0], type: 'shape' },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertDrawing', start: [0, 1], type: 'shape' },
            { name: 'insertParagraph', start: [0, 1, 0] },
            { name: 'insertDrawing', start: [0, 2], type: 'shape' },
            { name: 'insertParagraph', start: [0, 2, 0] },
            { name: 'insertDrawing', start: [0, 3], type: 'shape' },
            { name: 'insertParagraph', start: [0, 3, 0] },
            { name: 'insertDrawing', start: [0, 4], type: 'shape' },
            { name: 'insertParagraph', start: [0, 4, 0] },

            { name: 'insertSlide', start: [1], target: layoutId_1 },
            { name: 'insertDrawing', start: [1, 0], type: 'shape' },
            { name: 'insertParagraph', start: [1, 0, 0] },
            { name: 'insertDrawing', start: [1, 1], type: 'shape' },
            { name: 'insertParagraph', start: [1, 1, 0] },
            { name: 'insertDrawing', start: [1, 2], type: 'shape' },
            { name: 'insertParagraph', start: [1, 2, 0] },
            { name: 'insertDrawing', start: [1, 3], type: 'shape' },
            { name: 'insertParagraph', start: [1, 3, 0] },

            { name: 'insertSlide', start: [2], target: layoutId_1 },
            { name: 'insertDrawing', start: [2, 0], type: 'shape' },
            { name: 'insertParagraph', start: [2, 0, 0] },
            { name: 'insertText', start: [2, 0, 0, 0], text: text_para1_drawing1 },
            { name: 'insertDrawing', start: [2, 1], type: 'shape' },
            { name: 'insertParagraph', start: [2, 1, 0] },
            { name: 'insertText', start: [2, 1, 0, 0], text: text_para1_drawing2 },
            { name: 'insertDrawing', start: [2, 2], type: 'shape' },
            { name: 'insertParagraph', start: [2, 2, 0] },
            { name: 'insertText', start: [2, 2, 0, 0], text: text_para1_drawing3 },
            { name: 'insertDrawing', start: [2, 3], type: 'shape' },
            { name: 'insertDrawing', start: [2, 4], type: 'shape' },
            { name: 'insertDrawing', start: [2, 5], type: 'shape' },

            { name: 'insertSlide', start: [3], target: layoutId_1 },
            { name: 'insertDrawing', start: [3, 0], type: 'shape' },
            { name: 'insertParagraph', start: [3, 0, 0] },
            { name: 'insertDrawing', start: [3, 1], type: 'shape' },
            { name: 'insertParagraph', start: [3, 1, 0] },
            { name: 'insertDrawing', start: [3, 2], type: 'shape' },
            { name: 'insertParagraph', start: [3, 2, 0] },
            { name: 'insertDrawing', start: [3, 3], type: 'shape' },
            { name: 'insertParagraph', start: [3, 3, 0] }

        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        selection = model.getSelection();
    });

    function expectSlideSelection(expected) {
        expect(selection.isSlideSelection()).toBeTrue();
        expect(selection.isDrawingSelection()).toBeFalse();
        expect(selection.isMultiSelection()).toBeFalse();
        expect(selection.getStartPosition()).toEqual([expected, 0]);
        expect(selection.getEndPosition()).toEqual([expected, 0]);
        return model.getActiveSlideId();
    }

    function expectDrawingSelection(...expected) {
        expect(selection.isSlideSelection()).toBeFalse();
        expect(selection.isDrawingSelection()).toBeTrue();
        expect(selection.isMultiSelection()).toBe(expected.length > 1);
        const positions = selection.getAllLogicalPositions();
        expect(positions).toHaveLength(expected.length);
        positions.forEach((pos, idx) => {
            expect(pos.startPosition).toEqual(expected[idx]);
            const endPos = expected[idx].slice();
            endPos[endPos.length - 1] += 1;
            expect(pos.endPosition).toEqual(endPos);
        });
        return model.getActiveSlideId();
    }

    function expectTextSelection(start, end) {
        expect(selection.isSlideSelection()).toBeFalse();
        expect(selection.isDrawingSelection()).toBeFalse();
        expect(selection.isMultiSelection()).toBeFalse();
        expect(selection.getStartPosition()).toEqual(start);
        expect(selection.getEndPosition()).toEqual(end ?? start);
        return model.getActiveSlideId();
    }

    function expectSlideDrawingsAbs(expected) {
        const activeSlide = model.getSlideById(model.getActiveSlideId());
        const drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
        expect(drawings).toHaveLength(expected);
        return drawings;
    }

    function expectSlideDrawingsAll(expected) {
        const activeSlide = model.getSlideById(model.getActiveSlideId());
        const drawings = activeSlide.children(DOM.DRAWING_NODE_SELECTOR);
        expect(drawings).toHaveLength(expected);
        return drawings;
    }

    // public methods -----------------------------------------------------

    describe('method applyExternalOperations', function () {

        it('should exist', function () {
            expect(model).toRespondTo('applyExternalOperations');
        });

        it('should have valid prepared first slide', function () {
            const drawings = expectSlideDrawingsAbs(5);
            expect($(drawings[0]).prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect($(drawings[1]).prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(1);
            expect($(drawings[2]).prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);
            expect($(drawings[3]).prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);
            expect($(drawings[4]).prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(4);
        });

        it('should update text selection after removing previous drawing on same slide', async function () {

            selection.setTextSelection([0, 3, 0, 0]);  // setting the cursor into the first drawing on the first slide
            expect(model.getActiveSlideId()).toBe(slide_1_id);
            expectTextSelection([0, 3, 0, 0]);

            await model.applyExternalOperations([
                { name: 'delete', start: [0, 1], end: [0, 1] }
            ]);

            expectTextSelection([0, 2, 0, 0]);
            expectSlideDrawingsAbs(4);
        });

        it('should update text selection after inserting a previous drawing on same slide', async function () {

            selection.setTextSelection([0, 2, 0, 0]);  // setting the cursor into the first drawing on the first slide
            expectTextSelection([0, 2, 0, 0]);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [0, 1], type: 'shape' },
                { name: 'insertParagraph', start: [0, 1, 0] }
            ]);

            expectTextSelection([0, 3, 0, 0]);
            expectSlideDrawingsAbs(5);
        });

        it('should change to the slide with the specified ID', async function () {

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.setActiveSlideId(slide_2_id); // activating the third slide
            });
            expect(arg).toBe(slide_2_id);

            expect(model.getActiveSlideIndex()).toBe(1); // asynchronous slide formatting has to be finished
            expect(model.getActiveSlideId()).toBe(slide_2_id); // asynchronous slide formatting has to be finished
            expectSlideDrawingsAbs(4);
        });

        it('should not update text selection after removing previous drawing on a different slide', async function () {

            selection.setTextSelection([1, 3, 0, 0]);
            expect(model.getActiveSlideId()).toBe(slide_2_id);
            expectTextSelection([1, 3, 0, 0]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'delete', start: [0, 1], end: [0, 1] }
            ]);

            expectTextSelection([1, 3, 0, 0]);
            expectSlideDrawingsAbs(4);
        });

        it('should not update text selection after inserting a previous drawing on a different slide', async function () {

            selection.setTextSelection([1, 3, 0, 0]);  // setting the cursor into the first drawing on the first slide
            expectTextSelection([1, 3, 0, 0]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [0, 1], type: 'shape' },
                { name: 'insertParagraph', start: [0, 1, 0] }
            ]);

            expectTextSelection([1, 3, 0, 0]);
            expectSlideDrawingsAbs(4);
        });

        it('should update drawing selection after removing previous drawing on same slide', async function () {

            selection.setTextSelection([1, 3], [1, 4]);
            expectDrawingSelection([1, 3]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'delete', start: [1, 1], end: [1, 1] }
            ]);

            expectDrawingSelection([1, 2]);
            expectSlideDrawingsAbs(3);
        });

        it('should update drawing selection after inserting a previous drawing on same slide', async function () {

            selection.setTextSelection([1, 2], [1, 3]);
            expectDrawingSelection([1, 2]);
            expectSlideDrawingsAbs(3);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [1, 1], type: 'shape' },
                { name: 'insertParagraph', start: [1, 1, 0] }
            ]);

            expectDrawingSelection([1, 3]);
            expectSlideDrawingsAbs(4);
        });

        it('should not update drawing selection after removing a following drawing on same slide', async function () {

            selection.setTextSelection([1, 1], [1, 2]);
            expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'delete', start: [1, 3], end: [1, 3] }
            ]);

            expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(3);
        });

        it('should not update drawing selection after inserting a following drawing on same slide', async function () {

            selection.setTextSelection([1, 1], [1, 2]);
            expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(3);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [1, 3], type: 'shape' },
                { name: 'insertParagraph', start: [1, 3, 0] }
            ]);

            expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(4);
        });

        it('should change to slide selection if the selected drawing is removed', async function () {

            selection.setTextSelection([1, 1], [1, 2]);
            expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'delete', start: [1, 1] }
            ]);

            expectSlideSelection(1);
            expectSlideDrawingsAbs(3);
        });

        it('should not change the slide selection after inserting a drawing on the active slide', async function () {

            expectSlideSelection(1);
            expectSlideDrawingsAbs(3);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [1, 1], type: 'shape' },
                { name: 'insertParagraph', start: [1, 1, 0] }
            ]);

            expectSlideSelection(1);
            expectSlideDrawingsAbs(4);
        });

        it('should change the selection, if a previous slide is removed', async function () {

            selection.setTextSelection([1, 1], [1, 2]);
            expect(model.getActiveSlideId()).toBe(slide_2_id);
            expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'delete', start: [0] }
            ]);

            expect(model.getActiveSlideId()).toBe(slide_2_id);
            expectDrawingSelection([0, 1]);
            expectSlideDrawingsAbs(4);
        });

        it('should change the selection, if a new slide is inserted before the currently active slide', async function () {

            expect(model.getActiveSlideId()).toBe(slide_2_id);
            expectDrawingSelection([0, 1]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'insertSlide', start: [0], target: layoutId_1 },
                { name: 'insertDrawing', start: [0, 0], type: 'shape' },
                { name: 'insertParagraph', start: [0, 0, 0] },
                { name: 'insertDrawing', start: [0, 1], type: 'shape' },
                { name: 'insertParagraph', start: [0, 1, 0] },
                { name: 'insertDrawing', start: [0, 2], type: 'shape' },
                { name: 'insertParagraph', start: [0, 2, 0] },
                { name: 'insertDrawing', start: [0, 3], type: 'shape' },
                { name: 'insertParagraph', start: [0, 3, 0] },
                { name: 'insertDrawing', start: [0, 4], type: 'shape' },
                { name: 'insertParagraph', start: [0, 4, 0] }
            ]);

            expect(model.getActiveSlideId()).toBe(slide_2_id);
            expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(4);
        });

        it('should not change the selection, if a following slide is removed', async function () {

            selection.setTextSelection([1, 1], [1, 2]);
            expect(model.getActiveSlideId()).toBe(slide_2_id);
            expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'delete', start: [2] }
            ]);

            expect(model.getActiveSlideId()).toBe(slide_2_id);
            expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(4);
        });

        it('should not change the selection, if a new slide is inserted behind the currently active slide', async function () {

            expect(model.getActiveSlideId()).toBe(slide_2_id);
            expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'insertSlide', start: [2], target: layoutId_1 },
                { name: 'insertDrawing', start: [2, 0], type: 'shape' },
                { name: 'insertParagraph', start: [2, 0, 0] },
                { name: 'insertText', start: [2, 0, 0, 0], text: text_para1_drawing1 },
                { name: 'insertDrawing', start: [2, 1], type: 'shape' },
                { name: 'insertParagraph', start: [2, 1, 0] },
                { name: 'insertText', start: [2, 1, 0, 0], text: text_para1_drawing2 },
                { name: 'insertDrawing', start: [2, 2], type: 'shape' },
                { name: 'insertParagraph', start: [2, 2, 0] },
                { name: 'insertText', start: [2, 2, 0, 0], text: text_para1_drawing3 },
                { name: 'insertDrawing', start: [2, 3], type: 'shape' },
                { name: 'insertDrawing', start: [2, 4], type: 'shape' },
                { name: 'insertDrawing', start: [2, 5], type: 'shape' }
            ]);

            expect(model.getActiveSlideId()).toBe(slide_2_id);
            expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(4);
        });

        it('should change to following slide, if the active slide is removed', async function () {

            selection.setTextSelection([1, 1], [1, 2]);
            const oldSlideId = expectDrawingSelection([1, 1]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'delete', start: [1] }
            ]);

            const newSlideId = expectSlideSelection(1);
            expect(newSlideId).not.toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update the slide selection, if a slide is restored before the currently active slide', async function () {

            const oldSlideId = expectSlideSelection(1);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'insertSlide', start: [1], target: layoutId_1 },
                { name: 'insertDrawing', start: [1, 0], type: 'shape' },
                { name: 'insertParagraph', start: [1, 0, 0] },
                { name: 'insertDrawing', start: [1, 1], type: 'shape' },
                { name: 'insertParagraph', start: [1, 1, 0] },
                { name: 'insertDrawing', start: [1, 2], type: 'shape' },
                { name: 'insertParagraph', start: [1, 2, 0] },
                { name: 'insertDrawing', start: [1, 3], type: 'shape' },
                { name: 'insertParagraph', start: [1, 3, 0] }
            ]);

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should change to the last document slide', async function () {

            const oldSlideId = model.getActiveSlideId();

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.changeToLastSlideInView(); // activating the last slide
            });

            expect(arg).not.toBe(slide_2_id);
            expect(model.getActiveSlideIndex()).toBe(3); // asynchronous slide formatting has to be finished

            const newSlideId = model.getActiveSlideId();
            expect(newSlideId).not.toBe(oldSlideId);
            expectSlideDrawingsAbs(4);
        });

        it('should change to previous slide, if the active slide is removed and there is no following slide', async function () {

            selection.setTextSelection([3, 1], [3, 2]);
            const oldSlideId = expectDrawingSelection([3, 1]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'delete', start: [3] }
            ]);

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).not.toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should not update the slide selection, if a slide is restored behind the currently active slide', async function () {

            const oldSlideId = expectSlideSelection(2);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'insertSlide', start: [3], target: layoutId_1 },
                { name: 'insertDrawing', start: [3, 0], type: 'shape' },
                { name: 'insertParagraph', start: [3, 0, 0] },
                { name: 'insertDrawing', start: [3, 1], type: 'shape' },
                { name: 'insertParagraph', start: [3, 1, 0] },
                { name: 'insertDrawing', start: [3, 2], type: 'shape' },
                { name: 'insertParagraph', start: [3, 2, 0] },
                { name: 'insertDrawing', start: [3, 3], type: 'shape' },
                { name: 'insertParagraph', start: [3, 3, 0] }
            ]);

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update the slide selection, if a slide is move from behind to before the active slide', async function () {

            selection.setTextSelection([2, 2], [2, 3]);
            const oldSlideId = expectDrawingSelection([2, 2]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'moveSlide', start: [3], end: [2] }
            ]);

            const newSlideId = expectDrawingSelection([3, 2]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update the slide selection, if a slide is move from before to behind the active slide', async function () {

            const oldSlideId = expectDrawingSelection([3, 2]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'moveSlide', start: [2], end: [3] }
            ]);

            const newSlideId = expectDrawingSelection([2, 2]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update the drawing selection, if drawings before the selected drawing are grouped', async function () {

            const oldSlideId = expectDrawingSelection([2, 2]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'group', start: [2, 0], drawings: [0, 1, 3, 4] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAll(3); // 'absolute is not set yet at group
        });

        it('should update the drawing selection, if a drawing before the selected drawing is ungrouped', async function () {

            const oldSlideId = expectDrawingSelection([2, 1]);
            expectSlideDrawingsAll(3);

            await model.applyExternalOperations([
                { name: 'ungroup', start: [2, 0], drawings: [0, 1, 3, 4] }
            ]);

            const newSlideId = expectDrawingSelection([2, 2]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update the drawing selection, if the selected drawings itself is grouped', async function () {

            const oldSlideId = expectDrawingSelection([2, 2]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'group', start: [2, 0], drawings: [0, 1, 2, 3] }
            ]);

            const newSlideId = expectSlideSelection(2); // group behaviour
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAll(3); // 'absolute is not set yet at group
        });

        it('should update the drawing selection, if the selected drawings itself is ungrouped', async function () {

            selection.setTextSelection([2, 0], [2, 1]); // selecting the grouped drawing
            const oldSlideId = expectDrawingSelection([2, 0]);
            expectSlideDrawingsAll(3);

            await model.applyExternalOperations([
                { name: 'ungroup', start: [2, 0], drawings: [0, 1, 2, 3] }
            ]);

            const newSlideId = expectSlideSelection(2); // ungroup behaviour
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAll(6);
        });

        it('should update the text selection, if drawings before the selected drawing are grouped', async function () {

            selection.setTextSelection([2, 2, 0, 0], [2, 2, 0, 0]);
            const oldSlideId = expectTextSelection([2, 2, 0, 0], [2, 2, 0, 0]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'group', start: [2, 0], drawings: [0, 1, 3, 4] }
            ]);

            const newSlideId = expectTextSelection([2, 1, 0, 0], [2, 1, 0, 0]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAll(3);
        });

        it('should update the text selection, if a drawing before the selected drawing is ungrouped', async function () {

            const oldSlideId = expectTextSelection([2, 1, 0, 0], [2, 1, 0, 0]);
            expectSlideDrawingsAll(3);

            await model.applyExternalOperations([
                { name: 'ungroup', start: [2, 0], drawings: [0, 1, 3, 4] }
            ]);

            const newSlideId = expectTextSelection([2, 2, 0, 0], [2, 2, 0, 0]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update the text selection, if the selected drawing itself is grouped', async function () {

            selection.setTextSelection([2, 2, 0, 0], [2, 2, 0, 3]);
            const oldSlideId = expectTextSelection([2, 2, 0, 0], [2, 2, 0, 3]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'group', start: [2, 0], drawings: [0, 1, 2, 3] }
            ]);

            const newSlideId = expectSlideSelection(2); // group behaviour
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAll(3); // 'absolute is not set yet at group
        });

        it('should update the text selection, if the selected drawing itself is ungrouped', async function () {

            selection.setTextSelection([2, 0, 2, 0, 0], [2, 0, 2, 0, 3]);
            const oldSlideId = expectTextSelection([2, 0, 2, 0, 0], [2, 0, 2, 0, 3]);
            expectSlideDrawingsAll(3);

            await model.applyExternalOperations([
                { name: 'ungroup', start: [2, 0], drawings: [0, 1, 2, 3] }
            ]);

            const newSlideId = expectSlideSelection(2); // ungroup behaviour
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        // starting multi selection tests
        it('should update the multi selection, if a drawing before the selected drawings is removed', async function () {

            selection.setMultiDrawingSelectionByPosition([[2, 1], [2, 2], [2, 4]]);
            const oldSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'delete', start: [2, 0] }
            ]);

            const newSlideId = expectDrawingSelection([2, 0], [2, 1], [2, 3]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(5);
        });

        it('should update the multi selection, if a drawing is inserted before the selected drawings', async function () {

            const oldSlideId = expectDrawingSelection([2, 0], [2, 1], [2, 3]);
            expectSlideDrawingsAbs(5);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [2, 0], type: 'shape' },
                { name: 'insertParagraph', start: [2, 0, 0] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should not update the multi selection, if a drawing behind the selected drawings is removed', async function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'delete', start: [2, 5] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(5);
        });

        it('should not update the multi selection, if a drawing is inserted behind the selected drawings', async function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expectSlideDrawingsAbs(5);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [2, 5], type: 'shape' },
                { name: 'insertParagraph', start: [2, 5, 0] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update parts of the multi selection, if a drawing between the selected drawings is removed', async function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'delete', start: [2, 3] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 3]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(5);
        });

        it('should update parts of the multi selection, if a drawing is inserted between the selected drawings', async function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 3]);
            expectSlideDrawingsAbs(5);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [2, 3], type: 'shape' },
                { name: 'insertParagraph', start: [2, 3, 0] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should decrease the multi selection, if one of the selected drawings is removed', async function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'delete', start: [2, 2] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 3]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(5);
        });

        it('should not expand but update parts of the multi selection, if a drawing is inserted between the selected drawings', async function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 3]);
            expectSlideDrawingsAbs(5);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [2, 2], type: 'shape' },
                { name: 'insertParagraph', start: [2, 2, 0] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 4]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should switch to single drawing selection, if one of two selected drawings is removed', async function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 4]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'delete', start: [2, 1] }
            ]);

            const newSlideId = expectDrawingSelection([2, 3]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(5);
        });

        it('should not switch to multi selection, if a previously removed drawing is restored again', async function () {

            const oldSlideId = expectDrawingSelection([2, 3]);
            expectSlideDrawingsAbs(5);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [2, 1], type: 'shape' },
                { name: 'insertParagraph', start: [2, 1, 0] }
            ]);

            const newSlideId = expectDrawingSelection([2, 4]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update multi drawing selection correctly, if more than one drawing is removed', async function () {

            selection.setMultiDrawingSelectionByPosition([[2, 1], [2, 2], [2, 4]]);
            const oldSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'delete', start: [2, 3] },  // backwards!
                { name: 'delete', start: [2, 1] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 2]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(4);
        });

        it('should switch to slide selection, if all selected drawings are removed', async function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 2]);
            expectSlideDrawingsAbs(4);

            await model.applyExternalOperations([
                { name: 'delete', start: [2, 3] },  // backwards!
                { name: 'delete', start: [2, 2] },
                { name: 'delete', start: [2, 1] }
            ]);

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(1);
        });

        it('should keep to slide selection, if all drawings are restored', async function () {

            const oldSlideId = expectSlideSelection(2);
            expectSlideDrawingsAbs(1);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [2, 0], type: 'shape' }, { name: 'insertParagraph', start: [2, 0, 0] },
                { name: 'insertDrawing', start: [2, 1], type: 'shape' }, { name: 'insertParagraph', start: [2, 1, 0] },
                { name: 'insertDrawing', start: [2, 2], type: 'shape' }, { name: 'insertParagraph', start: [2, 2, 0] },
                { name: 'insertDrawing', start: [2, 3], type: 'shape' }, { name: 'insertParagraph', start: [2, 3, 0] },
                { name: 'insertDrawing', start: [2, 4], type: 'shape' }, { name: 'insertParagraph', start: [2, 4, 0] }
            ]);

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update multi drawing selection, if a previous slide is deleted', async function () {

            selection.setMultiDrawingSelectionByPosition([[2, 1], [2, 2], [2, 4]]);
            const oldSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'delete', start: [1] }
            ]);

            const newSlideId = expectDrawingSelection([1, 1], [1, 2], [1, 4]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update multi drawing selection, if a slide is inserted before', async function () {

            const oldSlideId = expectDrawingSelection([1, 1], [1, 2], [1, 4]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'insertSlide', start: [1], target: layoutId_1 },
                { name: 'insertDrawing', start: [1, 0], type: 'shape' },
                { name: 'insertParagraph', start: [1, 0, 0] },
                { name: 'insertDrawing', start: [1, 1], type: 'shape' },
                { name: 'insertParagraph', start: [1, 1, 0] },
                { name: 'insertDrawing', start: [1, 2], type: 'shape' },
                { name: 'insertParagraph', start: [1, 2, 0] },
                { name: 'insertDrawing', start: [1, 3], type: 'shape' },
                { name: 'insertParagraph', start: [1, 3, 0] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update multi drawing selection, if a slide is moved from behind to before the current slide', async function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'moveSlide', start: [3], end: [2] }
            ]);

            const newSlideId = expectDrawingSelection([3, 1], [3, 2], [3, 4]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update multi drawing selection, if a slide is moved from before to behind the current slide', async function () {

            const oldSlideId = expectDrawingSelection([3, 1], [3, 2], [3, 4]);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'moveSlide', start: [2], end: [3] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        // handling slide selections
        it('should switch from multi drawing selection to slide selection', function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 4]);

            // setting slide selection
            selection.setSlideSelection();

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).toBe(oldSlideId);
        });

        it('should update slide selection, if a previous slide is deleted', async function () {

            const oldSlideId = expectSlideSelection(2);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'delete', start: [1] }
            ]);

            const newSlideId = expectSlideSelection(1);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update slide selection, if a slide is inserted before', async function () {

            const oldSlideId = expectSlideSelection(1);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'insertSlide', start: [1], target: layoutId_1 },
                { name: 'insertDrawing', start: [1, 0], type: 'shape' },
                { name: 'insertParagraph', start: [1, 0, 0] },
                { name: 'insertDrawing', start: [1, 1], type: 'shape' },
                { name: 'insertParagraph', start: [1, 1, 0] },
                { name: 'insertDrawing', start: [1, 2], type: 'shape' },
                { name: 'insertParagraph', start: [1, 2, 0] },
                { name: 'insertDrawing', start: [1, 3], type: 'shape' },
                { name: 'insertParagraph', start: [1, 3, 0] }
            ]);

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update slide selection, if a slide is moved from behind to before the selected slide', async function () {

            const oldSlideId = expectSlideSelection(2);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'moveSlide', start: [3], end: [2] }
            ]);

            const newSlideId = expectSlideSelection(3);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should update slide selection, if a slide is moved from before to behind the selected slide', async function () {

            const oldSlideId = expectSlideSelection(3);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'moveSlide', start: [2], end: [3] }
            ]);

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        it('should not update slide selection, if a drawing is removed on the selected slide', async function () {

            const oldSlideId = expectSlideSelection(2);
            expectSlideDrawingsAbs(6);

            await model.applyExternalOperations([
                { name: 'delete', start: [2, 0] }
            ]);

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(5);
        });

        it('should not update slide selection, if a drawing is inserted on the selected slide', async function () {

            const oldSlideId = expectSlideSelection(2);
            expectSlideDrawingsAbs(5);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [2, 0] },
                { name: 'insertParagraph', start: [2, 0, 0] }
            ]);

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(6);
        });

        // layout slide handling
        it('should switch to the layout slide of the currently selected document slide', async function () {

            expect(model.isMasterView()).toBeFalse();
            expect(model.getActiveSlideIndex()).toBe(2);
            expectSlideSelection(2);

            // switching to master view and activating layout slide of current document slide
            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.selectSlideView({ showMaster: true });
            });
            expect(arg).toBe(layoutId_1);

            expect(model.getActiveSlideIndex()).toBe(1); // the second slide in the container of the master and layout slides
            expect(model.getActiveSlideId()).toBe(layoutId_1);
            expect(model.getActiveTarget()).toBe(layoutId_1);
            expectSlideSelection(0);
            expectSlideDrawingsAbs(2);
        });

        it('should switch to a text selection on the layout slide', function () {

            const oldSlideId = expectSlideSelection(0);
            expect(model.isMasterView()).toBeTrue();
            expect(model.getActiveSlideIndex()).toBe(1);

            // setting the text selection
            selection.setTextSelection([0, 1, 0, 0], [0, 1, 0, 3]);

            expect(model.getActiveSlideIndex()).toBe(1);
            expect(model.getActiveSlideId()).toBe(layoutId_1);
            expect(model.getActiveTarget()).toBe(layoutId_1);

            const newSlideId = expectTextSelection([0, 1, 0, 0], [0, 1, 0, 3]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(2);
        });

        it('should expand the text selection, after receiving external insertText operation', async function () {

            const oldSlideId = expectTextSelection([0, 1, 0, 0], [0, 1, 0, 3]);
            expectSlideDrawingsAbs(2);

            await model.applyExternalOperations([
                { name: 'insertText', start: [0, 1, 0, 2], text: '123', target: layoutId_1 }
            ]);

            const newSlideId = expectTextSelection([0, 1, 0, 0], [0, 1, 0, 6]);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(2);
        });

        it('should switch to slide selection, after receiving external delete operation for the drawing containing the text selection', async function () {

            const oldSlideId = expectTextSelection([0, 1, 0, 0], [0, 1, 0, 6]);
            expectSlideDrawingsAbs(2);

            await model.applyExternalOperations([
                { name: 'delete', start: [0, 1], target: layoutId_1 }
            ]);

            const newSlideId = expectSlideSelection(0);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(1);
        });

        it('should keep the slide selection, after external insertDrawing operation for the selected slide', async function () {

            const oldSlideId = expectSlideSelection(0);
            expectSlideDrawingsAbs(1);

            await model.applyExternalOperations([
                { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: 3810, top: 10795, width: 17780, height: 4868 } } },
                { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1 },
                { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_1, text: text_layout1_drawing2 }
            ]);

            const newSlideId = expectSlideSelection(0);
            expect(newSlideId).toBe(oldSlideId);
            expectSlideDrawingsAbs(2);
        });

        it('should change to the last layout slide', async function () {

            const oldSlideId = expectSlideSelection(0);
            expect(oldSlideId).toBe(layoutId_1);
            expect(model.getActiveSlideIndex()).toBe(1);

            // activating the last slide in master view
            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.changeToLastSlideInView();
            });

            expect(arg).toBe(layoutId_2);
            expect(model.getActiveSlideIndex()).toBe(2);

            const newSlideId = expectSlideSelection(0);
            expect(newSlideId).toBe(layoutId_2);
            expectSlideDrawingsAbs(2);
        });

        it('should switch to a drawing selection on the layout slide', function () {

            const oldSlideId = expectSlideSelection(0);
            expect(model.isMasterView()).toBeTrue();
            expect(model.getActiveSlideIndex()).toBe(2);
            expect(oldSlideId).toBe(layoutId_2);
            expect(model.getActiveTarget()).toBe(layoutId_2);

            // setting the drawing selection
            selection.setTextSelection([0, 1], [0, 2]);

            const newSlideId = expectDrawingSelection([0, 1]);
            expect(model.getActiveSlideIndex()).toBe(2);
            expect(newSlideId).toBe(layoutId_2);
            expect(model.getActiveTarget()).toBe(layoutId_2);
            expectSlideDrawingsAbs(2);
        });

        it('should switch to master slide if the layout slide with the current selection is removed', async function () {

            const oldSlideId = expectDrawingSelection([0, 1]);
            expect(model.isMasterView()).toBeTrue();
            expect(model.getActiveSlideIndex()).toBe(2);
            expect(oldSlideId).toBe(layoutId_2);
            expect(model.getActiveTarget()).toBe(layoutId_2);

            await model.applyExternalOperations([
                { name: 'delete', start: [0], target: layoutId_2 }
            ]);

            const newSlideId = expectSlideSelection(0);
            expect(newSlideId).toBe(masterId_1);
            expect(model.getActiveTarget()).toBe(masterId_1);
            expect(model.getActiveSlideIndex()).toBe(0);
            expectSlideDrawingsAbs(0);
        });

        it('should change to the first layout slide', async function () {

            const oldSlideId = expectSlideSelection(0);
            expect(oldSlideId).toBe(masterId_1);
            expect(model.getActiveSlideIndex()).toBe(0);

            // activating the last slide in master view
            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.setActiveSlideId(layoutId_1);
            });

            expect(arg).toBe(layoutId_1);
            expect(model.getActiveSlideIndex()).toBe(1);

            const newSlideId = expectSlideSelection(0);
            expect(newSlideId).toBe(layoutId_1);
            expectSlideDrawingsAbs(2);
        });

        it('should keep the slide selection, after inserting another layout slide before the selected layout slide', async function () {

            const oldSlideId = expectSlideSelection(0);
            expect(oldSlideId).toBe(layoutId_1);
            expect(model.getActiveSlideIndex()).toBe(1);
            expectSlideDrawingsAbs(2);

            await model.applyExternalOperations([
                { name: 'insertLayoutSlide', start: 0, id: layoutId_2, target: masterId_1 }, // includes start: 0
                { name: 'insertDrawing', start: [0, 0], target: layoutId_2, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: 1905, top: 5918, width: 21590, height: 4083 } } },
                { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_2 },
                { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_2, text: text_layout1_drawing1 },
                { name: 'insertDrawing', start: [0, 1], target: layoutId_2, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: 3810, top: 10795, width: 17780, height: 4868 } } },
                { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_2 },
                { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_2, text: text_layout1_drawing2 }
            ]);

            const newSlideId = expectSlideSelection(0);
            expect(newSlideId).toBe(oldSlideId);
            expect(model.getActiveSlideIndex()).toBe(2); // the index is increased
            expectSlideDrawingsAbs(2);
        });

        it('should keep the slide selection, after moving another layout slide from before to behind the selected layout slide', async function () {

            const oldSlideId = expectSlideSelection(0);
            expect(oldSlideId).toBe(layoutId_1);
            expect(model.getActiveSlideIndex()).toBe(2);
            expectSlideDrawingsAbs(2);

            // moving layout slide with id 'layoutId_2' from first position (oldindex: 0) below its master to the second position (start: 1)
            await model.applyExternalOperations([
                { name: 'moveLayoutSlide', start: 1, oldindex: 0, id: layoutId_2, target: masterId_1, oldtarget: masterId_1 }
            ]);

            const newSlideId = expectSlideSelection(0);
            expect(newSlideId).toBe(oldSlideId);
            expect(model.getActiveSlideIndex()).toBe(1); // the index is decreased, 'layoutId_1' is below its master now
            expectSlideDrawingsAbs(2);
        });

        // switching back to third document slide
        it('should switch to the last used document slide, when closing layout slide view', async function () {

            const oldSlideId = expectSlideSelection(0);
            expect(model.isMasterView()).toBeTrue();
            expect(model.getActiveSlideIndex()).toBe(1);

            // switching to document slide view and activating the third slide (this was active before switching to layout mode)
            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.selectSlideView();
            });

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).not.toBe(oldSlideId);
            expect(arg).toBe(newSlideId);
            expect(model.getActiveSlideIndex()).toBe(2); // the third slide in the container of the document slides
            expect(model.isMasterView()).toBeFalse();
            expectSlideDrawingsAbs(6);
        });

        it('should change to a multi drawing selection and select all drawings on the slide', function () {

            const oldSlideId = expectSlideSelection(2);
            expect(model.getActiveSlideIndex()).toBe(2);

            // selecting all drawings on the slide
            selection.selectAllDrawingsOnSlide();

            const newSlideId = expectDrawingSelection([2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5]);
            expect(newSlideId).toBe(oldSlideId);
            expect(model.getActiveSlideIndex()).toBe(2);
            expectSlideDrawingsAbs(6);
        });

        it('should not change the multi selection, if a drawing is moved, and keep selection sorted', async function () {

            const oldSlideId = expectDrawingSelection([2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5]);
            expect(model.getActiveSlideIndex()).toBe(2);

            await model.applyExternalOperations([
                { name: 'move', start: [2, 4], to: [2, 0] }
            ]);

            const newSlideId = expectDrawingSelection([2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5]);
            expect(newSlideId).toBe(oldSlideId);
            expect(model.getActiveSlideIndex()).toBe(2);
            expectSlideDrawingsAbs(6);
        });

        it('should update the multi selection, if one of the selected drawings is moved, and keep selection sorted', async function () {

            selection.setMultiDrawingSelectionByPosition([[2, 0], [2, 2], [2, 5]]);
            const oldSlideId = expectDrawingSelection([2, 0], [2, 2], [2, 5]);
            expect(model.getActiveSlideIndex()).toBe(2);

            await model.applyExternalOperations([
                { name: 'move', start: [2, 0], to: [2, 4] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 4], [2, 5]);
            expect(newSlideId).toBe(oldSlideId);
            expect(model.getActiveSlideIndex()).toBe(2);
            expectSlideDrawingsAbs(6);
        });

        it('should update the multi selection, if none of the selected drawings is moved, and keep selection sorted', async function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 4], [2, 5]);
            expect(model.getActiveSlideIndex()).toBe(2);

            await model.applyExternalOperations([
                { name: 'move', start: [2, 3], to: [2, 1] }
            ]);

            const newSlideId = expectDrawingSelection([2, 2], [2, 4], [2, 5]);
            expect(newSlideId).toBe(oldSlideId);
            expect(model.getActiveSlideIndex()).toBe(2);
            expectSlideDrawingsAbs(6);
        });

        it('should update the multi selection, if none of the selected drawings is grouped', async function () {

            const oldSlideId = expectDrawingSelection([2, 2], [2, 4], [2, 5]);
            expect(model.getActiveSlideIndex()).toBe(2);

            await model.applyExternalOperations([
                { name: 'group', start: [2, 0], drawings: [0, 1, 3] }
            ]);

            const newSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 3]);
            expect(newSlideId).toBe(oldSlideId);
            expect(model.getActiveSlideIndex()).toBe(2);
            expectSlideDrawingsAll(4);
        });

        it('should update the multi selection, if none of the selected drawings is ungrouped', async function () {

            const oldSlideId = expectDrawingSelection([2, 1], [2, 2], [2, 3]);
            expect(model.getActiveSlideIndex()).toBe(2);

            await model.applyExternalOperations([
                { name: 'ungroup', start: [2, 0], drawings: [0, 1, 3] } // external user used undo
            ]);

            const newSlideId = expectDrawingSelection([2, 2], [2, 4], [2, 5]);
            expect(newSlideId).toBe(oldSlideId);
            expect(model.getActiveSlideIndex()).toBe(2);
            expectSlideDrawingsAll(6);
        });

        it('should switch to slide selection, if one of the selected drawings is grouped', async function () {

            const oldSlideId = expectDrawingSelection([2, 2], [2, 4], [2, 5]);
            expect(model.getActiveSlideIndex()).toBe(2);

            await model.applyExternalOperations([
                { name: 'group', start: [2, 0], drawings: [0, 1, 4] }
            ]);

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).toBe(oldSlideId);
            expect(model.getActiveSlideIndex()).toBe(2);
            expectSlideDrawingsAll(4);
        });

        it('should switch to slide selection, if one of the selected drawings is ungrouped', async function () {

            const drawings = expectSlideDrawingsAll(4);
            selection.setMultiDrawingSelection([drawings[0], drawings[2]]);

            const oldSlideId = expectDrawingSelection([2, 0], [2, 2]);
            expect(model.getActiveSlideIndex()).toBe(2);

            await model.applyExternalOperations([
                { name: 'ungroup', start: [2, 0], drawings: [0, 1, 4] } // external user used undo
            ]);

            const newSlideId = expectSlideSelection(2);
            expect(newSlideId).toBe(oldSlideId);
            expect(model.getActiveSlideIndex()).toBe(2);
            expectSlideDrawingsAll(6);
        });
    });
});
