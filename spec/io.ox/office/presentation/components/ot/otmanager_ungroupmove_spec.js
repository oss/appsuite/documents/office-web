/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // move and local ungroup operation (handleUngroupMove) in OX Presentation
        // { name: ungroup, start: [2, 4], drawings: [4, 5, 7, 8] }
        // { name: move, start: [2, 2], to: [2, 6] }
        it('should calculate valid transformed move operation after local ungroup operation if the move starts in front of the ungroup position', function () {

            oneOperation = { name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 2], to: [3, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 2], to: [3, 9], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], to: [3, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 2], to: [3, 10], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 2], to: [3, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], to: [3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 2], to: [3, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 8], opl: 1, osn: 1 }] }], localActions); // found a gap
            expectOp([{ name: 'move', start: [3, 2], to: [3, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 2], to: [3, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 7], opl: 1, osn: 1 }] }], localActions); // found a gap
            expectOp([{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 2], to: [3, 8], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed move operation after local ungroup operation if the move starts behind the ungroup position', function () {

            oneOperation = { name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 8], to: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 8], to: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 9], to: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 8], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 10], to: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 7], to: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 5], to: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 7], to: [3, 4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 8], to: [3, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 7], to: [3, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 7, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 7, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [3, 5], to: [3, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 5], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions); // !
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [3, 7], to: [3, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 7], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions); // !
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [3, 7], to: [3, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 8], to: [3, 7], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions); // !
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [3, 8], to: [3, 9], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions); // !
            expectOp([{ name: 'move', start: [3, 10], to: [3, 11], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 9], to: [3, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions); // !
            expectOp([{ name: 'move', start: [3, 11], to: [3, 10], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 8], opl: 1, osn: 1 }] }], localActions); // found a gap!
            expectOp([{ name: 'move', start: [3, 7], to: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 8], to: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 6], to: [3, 2], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 6], to: [3, 2], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 6], to: [3, 2], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 8], to: [3, 2], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed move operation after local ungroup operation if the ungrouped drawing is moved', function () {

            oneOperation = { name: 'move', start: [3, 3], to: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 3], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [3, 3], to: [3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 1], to: [3, 3], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 3], target: '123456', opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        // move and external ungroup operation (handleUngroupMove) in OX Presentation
        // { name: ungroup, start: [2, 4], drawings: [4, 5, 7, 8] }
        // { name: move, start: [2, 2], to: [2, 6] }
        it('should calculate valid transformed move operation after external ungroup operation if the move starts in front of the ungroup position', function () {

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 3], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 9], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 10], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 8], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed move operation after external ungroup operation if the move starts behind the ungroup position', function () {

            oneOperation = { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 8], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 8], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 9], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 8], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 10], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 8], to: [3, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 8], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 7, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 7, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'move', start: [3, 7], to: [3, 5], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'move', start: [3, 5], to: [3, 7], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'move', start: [3, 8], to: [3, 7], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 8], to: [3, 9], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 10], to: [3, 11], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 9], to: [3, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 11], to: [3, 10], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 8], to: [3, 2], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 5], drawings: [5, 6, 7], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed move operation after external ungroup operation if the ungrouped drawing is moved', function () {

            oneOperation = { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], to: [3, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'move', start: [3, 6], to: [3, 3], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], to: [3, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'move', start: [3, 1], to: [3, 3], opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'move', start: [3, 6], to: [3, 3], target: '123456', opl: 1, osn: 1 }, { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

    });

});
