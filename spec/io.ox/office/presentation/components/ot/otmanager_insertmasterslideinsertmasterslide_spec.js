/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        localOperation = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    // the test runner
    var TestRunner = Utils.TestRunner;
    var testRunner = new TestRunner(otManagerPresentation);

    describe('method transformOperation', function () {

        // insertMasterSlide and local insertMasterSlide operation (handleInsertMasterSlideInsertMasterSlide)
        // insertMasterSlide: { id: "2147483658", start: 2, attrs: { slide: { type: "cust" } } }
        it('should calculate valid transformed insertMasterSlide operation after local insertMasterSlide operation (pptx)', function () {

            oneOperation = { name: 'insertMasterSlide', start: 1, id: '456789', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', start: 1, id: '345678', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', start: 1, id: '345678', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', start: 2, id: '456789', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertMasterSlide', start: 1, id: '456789', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', start: 2, id: '345678', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', start: 3, id: '345678', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', start: 1, id: '456789', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertMasterSlide', start: 2, id: '456789', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', start: 1, id: '345678', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', start: 1, id: '345678', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', start: 3, id: '456789', opl: 1, osn: 1 }], transformedOps);

        });

        // testing reload
        it('should reload document, if inserted master slide have the same ID', function () {

            oneOperation = { name: 'insertMasterSlide', start: 1, id: '456789', opl: 1, osn: 1 };
            localOperation = { name: 'insertMasterSlide', start: 2, id: '456789', opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertMasterSlide', start: 1, id: '456789', opl: 1, osn: 1 };
            localOperation = { name: 'insertMasterSlide', start: 1, id: '456789', opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });

        // insertMasterSlide and local insertMasterSlide operation (handleInsertMasterSlideInsertMasterSlide)
        // insertMasterSlide: { id: "2147483658", attrs: { slide: { type: "cust" } } }
        it('should calculate valid transformed insertMasterSlide operation after local insertMasterSlide operation (odp)', function () {

            oneOperation = { name: 'insertMasterSlide', id: '456789', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', id: '345678', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', id: '345678', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', id: '456789', opl: 1, osn: 1 }], transformedOps);
        });

        // testing reload
        it('should reload document, if inserted master slide have the same ID (odp)', function () {

            oneOperation = { name: 'insertMasterSlide', id: '456789', opl: 1, osn: 1 };
            localOperation = { name: 'insertMasterSlide', id: '456789', opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });

    });
});
