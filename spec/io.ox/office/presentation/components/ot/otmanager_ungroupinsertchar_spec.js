/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // insertText and local ungroup operation (handleUngroupInsertChar) in OX Presentation
        // { name: ungroup, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed insertText operation after local ungroup operation if insertChar position is longer than ungroup position', function () {

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 6, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 0, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 0, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 9, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 9, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 9, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 6, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 8, 9, 10], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 8, 9, 10], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 6, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 2, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [2, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [2, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 9, 0, 0], text: 'ooo', target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 1, 3, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 1, 2, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 1, 1, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 1, 0, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 1, 0, 0], text: 'ooo', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertText', start: [3, 1, 0, 2, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertText', start: [3, 1, 2, 2], text: 'ooo', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertDrawing operation after local ungroup operation if insertDrawing position has same length as the ungroup position', function () {

            oneOperation = { name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertDrawing', start: [3, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertDrawing', start: [3, 9], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertDrawing', start: [3, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertDrawing', start: [3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 2], drawings: [2, 3, 4, 6, 7, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertDrawing', start: [3, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertDrawing', start: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertDrawing', start: [3, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertDrawing', start: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertDrawing', start: [2, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertDrawing', start: [4, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertDrawing', start: [4, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertDrawing', start: [3, 10], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertDrawing', start: [3, 4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertDrawing', start: [3, 4], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertDrawing', start: [3, 4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6, 7], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6, 7], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertDrawing', start: [3, 10], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed ungroup operation after local insertDrawing operation if insertDrawing position has same length as the ungroup position', function () {

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertDrawing', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertDrawing', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 2], drawings: [2, 3, 4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertDrawing', start: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertDrawing', start: [3, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertDrawing', start: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 7], opl: 1, osn: 1 }], transformedOps);
        });

    });

});
