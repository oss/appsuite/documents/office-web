/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    // moveLayoutSlide and local deleteTargetSlide operation (handleMoveLayoutSlideDeleteTargetSlide)
    // moveLayoutSlide: { id: "layout_1", start: 1, target: "master_1", oldtarget: "master_2", oldindex: 3 } }
    // "deleteTargetSlide": { id: "layout_1", index: 2, parenttarget: "master_1" } // deleting layout slide
    // "deleteTargetSlide": { id: "master_1", index: 2 }                           // deleting master slide
    //
    // delete operation and deleteTargetSlide operation might be generated:
    //  { name: "delete", start: [0], target: "2147483761" }
    //  { name: "deleteTargetSlide", id: "2147483761", index: 2, parenttarget: "2147483756" }

    describe('method transformOperation', function () {

        // local deleteTargetSlide and external moveLayoutSlide operation

        // removal of layout slide
        it('should calculate valid transformed moveLayoutSlide operation after local deleteTargetSlide operation of a different layout slide and move in same master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 1, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 5, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local deleteTargetSlide operation of a different layout slide and move in one different master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

        });

        it('should calculate valid transformed moveLayoutSlide operation after local deleteTargetSlide operation of a different layout slide and move to a different master slide', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 1, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local deleteTargetSlide operation of a different layout slide and move from a different master slide', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local deleteTargetSlide operation of the same layout slide and move in same master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', index: 1, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', index: 1, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local deleteTargetSlide operation of the same layout slide and move to another master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', index: 1, parenttarget: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', index: 1, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_1', index: 3, parenttarget: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        // removal of master slide
        it('should calculate valid transformed moveLayoutSlide operation after local deleteTargetSlide operation of a master slide and move in same master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local deleteTargetSlide operation of a different master slide and move in same master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local deleteTargetSlide operation of a master slide and move into the deleted master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'layout_1' }, { name: 'deleteTargetSlide', id: 'layout_1', index: 3, parenttarget: 'master_2', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after local deleteTargetSlide operation of a master slide and move from the deleted master', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }, { name: 'delete', start: [0], target: 'layout_1' }, { name: 'deleteTargetSlide', id: 'layout_1', index: 1, parenttarget: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        // delete operation is not influenced by moveLayoutSlide operation
        it('should not transform moveLayoutSlide operation after local delete operation', function () {

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
        });

        // local moveLayoutSlide and external deleteTargetSlide operation

        // removal of layout slide
        it('should calculate valid transformed moveLayoutSlide operation after external deleteTargetSlide operation of a different layout slide and move in same master', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 1, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 5, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after external deleteTargetSlide operation of a different layout slide and move in one different master', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }], transformedOps);

        });

        it('should calculate valid transformed moveLayoutSlide operation after external deleteTargetSlide operation of a different layout slide and move to a different master slide', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 1, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after external deleteTargetSlide operation of a different layout slide and move from a different master slide', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after external deleteTargetSlide operation of the same layout slide and move in same master', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_1', index: 1, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', index: 1, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_1', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after external deleteTargetSlide operation of the same layout slide and move to another master', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_1', index: 1, parenttarget: 'master_2', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_1', index: 1, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_1', index: 3, parenttarget: 'master_2', opl: 1, osn: 1 }], transformedOps);
        });

        // removal of master slide
        it('should calculate valid transformed moveLayoutSlide operation after external deleteTargetSlide operation of a master slide and move in same master', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after external deleteTargetSlide operation of a different master slide and move in same master', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after external deleteTargetSlide operation of a master slide and move into the deleted master', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_1' }, { name: 'deleteTargetSlide', id: 'layout_1', index: 3, parenttarget: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed moveLayoutSlide operation after external deleteTargetSlide operation of a master slide and move from the deleted master', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 }, { name: 'delete', start: [0], target: 'layout_1' }, { name: 'deleteTargetSlide', id: 'layout_1', index: 1, parenttarget: 'master_2', opl: 1, osn: 1 }], transformedOps);
        });

        // delete operation is not influenced by moveLayoutSlide operation
        it('should NOT transform moveLayoutSlide operation after external delete operation', function () {

            oneOperation = { name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'layout_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });
    });

});
