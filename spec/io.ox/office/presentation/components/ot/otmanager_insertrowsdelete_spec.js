/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';


// class OTManager ================================================

describe('TextFramework class OTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null,

        // shortcuts for the utils functions
        expectAction = Utils.expectAction,
        expectOp = Utils.expectOp,

        localOperation = null,

        // the test runner
        TestRunner = Utils.TestRunner,
        testRunner = new TestRunner(otManagerPresentation);

    describe('method transformOperation', function () {

        // insertRows and local delete (handleInsertRowsDelete)
        it('should calculate valid transformed insertRows operation after local delete operation in OX Presentation', function () {

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [1, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 1], end: [1, 11], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 1], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 1], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0, 1], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 1, 4, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], end: [2, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 4, 5], referenceRow: 4, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4], end: [3, 8], opl: 1, osn: 1 }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 4], end: [3, 8], opl: 1, osn: 1 }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 4, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2, 2, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2, 2, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        // insertCells and local delete (handleInsertRowsDelete)
        it('should calculate valid transformed insertCells operation after local delete operation in OX Presentation and forcing reload', function () {

            oneOperation = { name: 'insertCells', start: [2, 1, 1], count: 1, opl: 1, osn: 1 };
            localOperation = { name: 'delete', start: [1], end: [2], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 1], count: 3 };
            localOperation = { name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
            localOperation = { name: 'delete', start: [3, 1, 1, 3, 1], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 8] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1, 0], end: [1, 1, 8] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 2], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 4, 1, 2], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 3], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], end: [2, 1, 1, 2, 2] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 4], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 2], end: [2, 1, 1, 2, 1, 6] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [3] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 5] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 4, 5, 2], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
            localOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2, 2, 2], count: 3 }; // in a text frame
            localOperation = { name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2, 2, 2], count: 3 }; // in a text frame
            localOperation = { name: 'delete', start: [1], end: [2], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });
    });

});
