/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        localOperation = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    // the test runner
    var TestRunner = Utils.TestRunner;
    var testRunner = new TestRunner(otManagerPresentation);

    describe('method transformOperation', function () {

        // { name: ungroup, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed delete operation after local ungroup operation, if delete is inside a drawing', function () {

            oneOperation = { name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 8, 0, 0], end: [3, 8, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 1, 3, 0, 0], end: [3, 1, 3, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 1, 1, 0, 0], end: [3, 1, 1, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 1, 0, 0], end: [3, 1, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 1, 0, 0], end: [3, 1, 0, 5], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 8, 0, 0], end: [3, 8, 0, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed delete operation after local ungroup operation, if a slide is deleted', function () {

            oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [2, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed delete operation after local ungroup operation, if a drawing is deleted, that is not affected by grouping', function () {

            oneOperation = { name: 'delete', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2, 1], end: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2, 1], end: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [4, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [4, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 0], end: [3, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 0], drawings: [0, 1, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 0], end: [3, 0], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 6], end: [3, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 8], end: [3, 8], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 6], end: [3, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 8], end: [3, 8], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed delete operation after local group operation, if the grouped drawing is deleted', function () {

            oneOperation = { name: 'delete', start: [3, 1], end: [3, 1], opl: 1, osn: 1 };
            localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'delete', start: [3, 1], opl: 1, osn: 1 };
            localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 };
            localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });

        it('should calculate valid transformed delete operation after external ungroup operation, if delete is inside a drawing', function () {

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 3, 0, 0], end: [3, 1, 3, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 0, 0], end: [3, 1, 1, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 1, 0, 0], end: [3, 1, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed delete operation after external ungroup operation, if a slide is deleted', function () {

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [2, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        it('should calculate valid transformed delete operation after external ungroup operation, if a drawing is deleted, that is not affected by ungrouping', function () {

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 1], end: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 1], end: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [4, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [4, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 0], end: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 0], end: [3, 0], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 0], drawings: [0, 1, 2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 6], end: [3, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 8], end: [3, 8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 6], end: [3, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 8], end: [3, 8], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed delete operation after local external operation, if a drawing is deleted, that is affected by grouping', function () {

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localOperation = { name: 'delete', start: [3, 1], end: [3, 1], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localOperation = { name: 'delete', start: [3, 1], opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
            localOperation = { name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });
    });
});
