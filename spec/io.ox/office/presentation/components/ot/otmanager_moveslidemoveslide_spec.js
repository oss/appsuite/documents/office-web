/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class OTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    // local helper function for moveSlide tests
    function moveSlideMoveSlideTest(localStart, localEnd, extStart, extEnd, newLocalStart, newLocalEnd, newExtStart, newExtEnd) {

        var extOperation = { name: 'moveSlide', start: [extStart], end: [extEnd] };
        var localOperation = { name: 'moveSlide', start: [localStart], end: [localEnd] };
        var localAction = { operations: [localOperation] };

        var localTransformedOps = otManagerPresentation.transformOperation(extOperation, [localAction]);

        if ((newLocalStart === null) || (newLocalEnd === null)) {
            expect(localAction.operations).toHaveLength(0);
        } else {
            expect(localOperation.start[0]).toBe(newLocalStart);
            expect(localOperation.end[0]).toBe(newLocalEnd);
        }

        if ((newExtStart === null) || (newExtEnd === null)) {
            expect(localTransformedOps).toHaveLength(0);
        } else {
            expect(localTransformedOps[0].start[0]).toBe(newExtStart);
            expect(localTransformedOps[0].end[0]).toBe(newExtEnd);
        }
    }

    describe('method transformOperation', function () {

        // moveSlide and local moveSlide operation, if different slides are moved (handleMoveSlideMoveSlide)
        it('should calculate valid transformed moveSlide operation after local moveSlide operation', function () {

            oneOperation = { name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }], transformedOps);

            // new move process
            oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [5], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [5], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [5], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [2], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [5], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [5], end: [3], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [6], end: [4], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [6], end: [4], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '234567', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [6], end: [4], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [6], end: [4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '123456', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [5], end: [3], target: '123456', opl: 1, osn: 1 }], transformedOps);
        });

        // moveSlide and local moveSlide operation, if the same slide is moved (handleMoveSlideMoveSlide)
        it('should calculate valid transformed moveSlide operation after local moveSlide operation, if the same slide is moved', function () {

            oneOperation = { name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [8], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [1], target: '234567', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);
        });

        // moveSlide and local moveSlide operation resulting in an operation with same source and target (handleMoveSlideMoveSlide)
        it('should remove the local operation if the source and the target are equal', function () {
            oneOperation = { name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);
        });

        // using the moveSheetmoveSheet example
        it('should handle correctly the same start position', function () {

            // 53
            oneOperation = { name: 'moveSlide', start: [1], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // 54
            oneOperation = { name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // 55
            oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            // 56
            oneOperation = { name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // 57
            oneOperation = { name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [5], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // 53_, 5
            oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // 54_, 30
            oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // 55
            oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            // 56_, 80
            oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // 57_, 105
            oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [5], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // 29
            oneOperation = { name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);

            // 31
            oneOperation = { name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // 32
            oneOperation = { name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [5], end: [2], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);
        });

        // using the moveSheetmoveSheet example
        it('should handle correctly different start positions', function () {

            // 58
            oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            // 33
            oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [] }], localActions);
            expectOp([], transformedOps);
        });

        // using the moveSheetmoveSheet example
        it('should handle correctly the full stack of move positions', function () {
            moveSlideMoveSlideTest(1, 1, 1, 1, null, null, null, null);
            moveSlideMoveSlideTest(1, 1, 1, 2, 2, 1, null, null);
            moveSlideMoveSlideTest(1, 1, 1, 3, 3, 1, null, null);
            moveSlideMoveSlideTest(1, 1, 1, 4, 4, 1, null, null);
            moveSlideMoveSlideTest(1, 1, 1, 5, 5, 1, null, null);
            moveSlideMoveSlideTest(1, 1, 2, 1, null, null, 2, 1);
            moveSlideMoveSlideTest(1, 1, 2, 2, null, null, null, null);
            moveSlideMoveSlideTest(1, 1, 2, 3, null, null, 2, 3);
            moveSlideMoveSlideTest(1, 1, 2, 4, null, null, 2, 4);
            moveSlideMoveSlideTest(1, 1, 2, 5, null, null, 2, 5);
            moveSlideMoveSlideTest(1, 1, 3, 1, null, null, 3, 1);
            moveSlideMoveSlideTest(1, 1, 3, 2, null, null, 3, 2);
            moveSlideMoveSlideTest(1, 1, 3, 3, null, null, null, null);
            moveSlideMoveSlideTest(1, 1, 3, 4, null, null, 3, 4);
            moveSlideMoveSlideTest(1, 1, 3, 5, null, null, 3, 5);
            moveSlideMoveSlideTest(1, 1, 4, 1, null, null, 4, 1);
            moveSlideMoveSlideTest(1, 1, 4, 2, null, null, 4, 2);
            moveSlideMoveSlideTest(1, 1, 4, 3, null, null, 4, 3);
            moveSlideMoveSlideTest(1, 1, 4, 4, null, null, null, null);
            moveSlideMoveSlideTest(1, 1, 4, 5, null, null, 4, 5);
            moveSlideMoveSlideTest(1, 1, 5, 1, null, null, 5, 1);
            moveSlideMoveSlideTest(1, 1, 5, 2, null, null, 5, 2);
            moveSlideMoveSlideTest(1, 1, 5, 3, null, null, 5, 3);
            moveSlideMoveSlideTest(1, 1, 5, 4, null, null, 5, 4);
            moveSlideMoveSlideTest(1, 1, 5, 5, null, null, null, null);

            moveSlideMoveSlideTest(1, 2, 1, 1, 1, 2, null, null);
            moveSlideMoveSlideTest(1, 2, 1, 2, null, null, null, null);
            moveSlideMoveSlideTest(1, 2, 1, 3, 3, 2, null, null);
            moveSlideMoveSlideTest(1, 2, 1, 4, 4, 2, null, null);
            moveSlideMoveSlideTest(1, 2, 1, 5, 5, 2, null, null);
            moveSlideMoveSlideTest(1, 2, 2, 1, null, null, null, null);
            moveSlideMoveSlideTest(1, 2, 2, 2, 1, 2, null, null);
            moveSlideMoveSlideTest(1, 2, 2, 3, null, null, 1, 3);
            moveSlideMoveSlideTest(1, 2, 2, 4, null, null, 1, 4);
            moveSlideMoveSlideTest(1, 2, 2, 5, null, null, 1, 5);
            moveSlideMoveSlideTest(1, 2, 3, 1, 2, 3, 3, 1);
            moveSlideMoveSlideTest(1, 2, 3, 2, 1, 3, 3, 1);
            moveSlideMoveSlideTest(1, 2, 3, 3, 1, 2, null, null);
            moveSlideMoveSlideTest(1, 2, 3, 4, 1, 2, 3, 4);
            moveSlideMoveSlideTest(1, 2, 3, 5, 1, 2, 3, 5);
            moveSlideMoveSlideTest(1, 2, 4, 1, 2, 3, 4, 1);
            moveSlideMoveSlideTest(1, 2, 4, 2, 1, 3, 4, 1);
            moveSlideMoveSlideTest(1, 2, 4, 3, 1, 2, 4, 3);
            moveSlideMoveSlideTest(1, 2, 4, 4, 1, 2, null, null);
            moveSlideMoveSlideTest(1, 2, 4, 5, 1, 2, 4, 5);
            moveSlideMoveSlideTest(1, 2, 5, 1, 2, 3, 5, 1);
            moveSlideMoveSlideTest(1, 2, 5, 2, 1, 3, 5, 1);
            moveSlideMoveSlideTest(1, 2, 5, 3, 1, 2, 5, 3);
            moveSlideMoveSlideTest(1, 2, 5, 4, 1, 2, 5, 4);
            moveSlideMoveSlideTest(1, 2, 5, 5, 1, 2, null, null);

            moveSlideMoveSlideTest(1, 3, 1, 1, 1, 3, null, null);
            moveSlideMoveSlideTest(1, 3, 1, 2, 2, 3, null, null);
            moveSlideMoveSlideTest(1, 3, 1, 3, null, null, null, null);
            moveSlideMoveSlideTest(1, 3, 1, 4, 4, 3, null, null);
            moveSlideMoveSlideTest(1, 3, 1, 5, 5, 3, null, null);
            moveSlideMoveSlideTest(1, 3, 2, 1, 2, 3, null, null);
            moveSlideMoveSlideTest(1, 3, 2, 2, 1, 3, null, null);
            moveSlideMoveSlideTest(1, 3, 2, 3, 1, 2, 1, 3);
            moveSlideMoveSlideTest(1, 3, 2, 4, 1, 2, 1, 4);
            moveSlideMoveSlideTest(1, 3, 2, 5, 1, 2, 1, 5);
            moveSlideMoveSlideTest(1, 3, 3, 1, 2, 3, 2, 1);
            moveSlideMoveSlideTest(1, 3, 3, 2, 1, 3, 2, 1);
            moveSlideMoveSlideTest(1, 3, 3, 3, 1, 3, null, null);
            moveSlideMoveSlideTest(1, 3, 3, 4, 1, 2, 2, 4);
            moveSlideMoveSlideTest(1, 3, 3, 5, 1, 2, 2, 5);
            moveSlideMoveSlideTest(1, 3, 4, 1, 2, 4, 4, 1);
            moveSlideMoveSlideTest(1, 3, 4, 2, 1, 4, 4, 1);
            moveSlideMoveSlideTest(1, 3, 4, 3, 1, 4, 4, 2);
            moveSlideMoveSlideTest(1, 3, 4, 4, 1, 3, null, null);
            moveSlideMoveSlideTest(1, 3, 4, 5, 1, 3, 4, 5);
            moveSlideMoveSlideTest(1, 3, 5, 1, 2, 4, 5, 1);
            moveSlideMoveSlideTest(1, 3, 5, 2, 1, 4, 5, 1);
            moveSlideMoveSlideTest(1, 3, 5, 3, 1, 4, 5, 2);
            moveSlideMoveSlideTest(1, 3, 5, 4, 1, 3, 5, 4);
            moveSlideMoveSlideTest(1, 3, 5, 5, 1, 3, null, null);

            moveSlideMoveSlideTest(1, 4, 1, 1, 1, 4, null, null);
            moveSlideMoveSlideTest(1, 4, 1, 2, 2, 4, null, null);
            moveSlideMoveSlideTest(1, 4, 1, 3, 3, 4, null, null);
            moveSlideMoveSlideTest(1, 4, 1, 4, null, null, null, null);
            moveSlideMoveSlideTest(1, 4, 1, 5, 5, 4, null, null);
            moveSlideMoveSlideTest(1, 4, 2, 1, 2, 4, null, null);
            moveSlideMoveSlideTest(1, 4, 2, 2, 1, 4, null, null);
            moveSlideMoveSlideTest(1, 4, 2, 3, 1, 4, 1, 2);
            moveSlideMoveSlideTest(1, 4, 2, 4, 1, 3, 1, 4);
            moveSlideMoveSlideTest(1, 4, 2, 5, 1, 3, 1, 5);
            moveSlideMoveSlideTest(1, 4, 3, 1, 2, 4, 2, 1);
            moveSlideMoveSlideTest(1, 4, 3, 2, 1, 4, 2, 1);
            moveSlideMoveSlideTest(1, 4, 3, 3, 1, 4, null, null);
            moveSlideMoveSlideTest(1, 4, 3, 4, 1, 3, 2, 4);
            moveSlideMoveSlideTest(1, 4, 3, 5, 1, 3, 2, 5);
            moveSlideMoveSlideTest(1, 4, 4, 1, 2, 4, 3, 1);
            moveSlideMoveSlideTest(1, 4, 4, 2, 1, 4, 3, 1);
            moveSlideMoveSlideTest(1, 4, 4, 3, 1, 4, 3, 2);
            moveSlideMoveSlideTest(1, 4, 4, 4, 1, 4, null, null);
            moveSlideMoveSlideTest(1, 4, 4, 5, 1, 3, 3, 5);
            moveSlideMoveSlideTest(1, 4, 5, 1, 2, 5, 5, 1);
            moveSlideMoveSlideTest(1, 4, 5, 2, 1, 5, 5, 1);
            moveSlideMoveSlideTest(1, 4, 5, 3, 1, 5, 5, 2);
            moveSlideMoveSlideTest(1, 4, 5, 4, 1, 5, 5, 3);
            moveSlideMoveSlideTest(1, 4, 5, 5, 1, 4, null, null);

            moveSlideMoveSlideTest(1, 5, 1, 1, 1, 5, null, null);
            moveSlideMoveSlideTest(1, 5, 1, 2, 2, 5, null, null);
            moveSlideMoveSlideTest(1, 5, 1, 3, 3, 5, null, null);
            moveSlideMoveSlideTest(1, 5, 1, 4, 4, 5, null, null);
            moveSlideMoveSlideTest(1, 5, 1, 5, null, null, null, null);
            moveSlideMoveSlideTest(1, 5, 2, 1, 2, 5, null, null);
            moveSlideMoveSlideTest(1, 5, 2, 2, 1, 5, null, null);
            moveSlideMoveSlideTest(1, 5, 2, 3, 1, 5, 1, 2);
            moveSlideMoveSlideTest(1, 5, 2, 4, 1, 5, 1, 3);
            moveSlideMoveSlideTest(1, 5, 2, 5, 1, 4, 1, 5);
            moveSlideMoveSlideTest(1, 5, 3, 1, 2, 5, 2, 1);
            moveSlideMoveSlideTest(1, 5, 3, 2, 1, 5, 2, 1);
            moveSlideMoveSlideTest(1, 5, 3, 3, 1, 5, null, null);
            moveSlideMoveSlideTest(1, 5, 3, 4, 1, 5, 2, 3);
            moveSlideMoveSlideTest(1, 5, 3, 5, 1, 4, 2, 5);
            moveSlideMoveSlideTest(1, 5, 4, 1, 2, 5, 3, 1);
            moveSlideMoveSlideTest(1, 5, 4, 2, 1, 5, 3, 1);
            moveSlideMoveSlideTest(1, 5, 4, 3, 1, 5, 3, 2);
            moveSlideMoveSlideTest(1, 5, 4, 4, 1, 5, null, null);
            moveSlideMoveSlideTest(1, 5, 4, 5, 1, 4, 3, 5);
            moveSlideMoveSlideTest(1, 5, 5, 1, 2, 5, 4, 1);
            moveSlideMoveSlideTest(1, 5, 5, 2, 1, 5, 4, 1);
            moveSlideMoveSlideTest(1, 5, 5, 3, 1, 5, 4, 2);
            moveSlideMoveSlideTest(1, 5, 5, 4, 1, 5, 4, 3);
            moveSlideMoveSlideTest(1, 5, 5, 5, 1, 5, null, null);

            moveSlideMoveSlideTest(2, 1, 1, 1, 2, 1, null, null);
            moveSlideMoveSlideTest(2, 1, 1, 2, null, null, null, null);
            moveSlideMoveSlideTest(2, 1, 1, 3, null, null, 2, 3);
            moveSlideMoveSlideTest(2, 1, 1, 4, null, null, 2, 4);
            moveSlideMoveSlideTest(2, 1, 1, 5, null, null, 2, 5);
            moveSlideMoveSlideTest(2, 1, 2, 1, null, null, null, null);
            moveSlideMoveSlideTest(2, 1, 2, 2, 2, 1, null, null);
            moveSlideMoveSlideTest(2, 1, 2, 3, 3, 1, null, null);
            moveSlideMoveSlideTest(2, 1, 2, 4, 4, 1, null, null);
            moveSlideMoveSlideTest(2, 1, 2, 5, 5, 1, null, null);
            moveSlideMoveSlideTest(2, 1, 3, 1, 3, 2, 3, 1);
            moveSlideMoveSlideTest(2, 1, 3, 2, 3, 1, null, null);
            moveSlideMoveSlideTest(2, 1, 3, 3, 2, 1, null, null);
            moveSlideMoveSlideTest(2, 1, 3, 4, 2, 1, 3, 4);
            moveSlideMoveSlideTest(2, 1, 3, 5, 2, 1, 3, 5);
            moveSlideMoveSlideTest(2, 1, 4, 1, 3, 2, 4, 1);
            moveSlideMoveSlideTest(2, 1, 4, 2, 3, 1, 4, 3);
            moveSlideMoveSlideTest(2, 1, 4, 3, 2, 1, 4, 3);
            moveSlideMoveSlideTest(2, 1, 4, 4, 2, 1, null, null);
            moveSlideMoveSlideTest(2, 1, 4, 5, 2, 1, 4, 5);
            moveSlideMoveSlideTest(2, 1, 5, 1, 3, 2, 5, 1);
            moveSlideMoveSlideTest(2, 1, 5, 2, 3, 1, 5, 3);
            moveSlideMoveSlideTest(2, 1, 5, 3, 2, 1, 5, 3);
            moveSlideMoveSlideTest(2, 1, 5, 4, 2, 1, 5, 4);
            moveSlideMoveSlideTest(2, 1, 5, 5, 2, 1, null, null);

            moveSlideMoveSlideTest(2, 2, 1, 1, null, null, null, null);
            moveSlideMoveSlideTest(2, 2, 1, 2, null, null, 1, 2);
            moveSlideMoveSlideTest(2, 2, 1, 3, null, null, 1, 3);
            moveSlideMoveSlideTest(2, 2, 1, 4, null, null, 1, 4);
            moveSlideMoveSlideTest(2, 2, 1, 5, null, null, 1, 5);
            moveSlideMoveSlideTest(2, 2, 2, 1, 1, 2, null, null);
            moveSlideMoveSlideTest(2, 2, 2, 2, null, null, null, null);
            moveSlideMoveSlideTest(2, 2, 2, 3, 3, 2, null, null);
            moveSlideMoveSlideTest(2, 2, 2, 4, 4, 2, null, null);
            moveSlideMoveSlideTest(2, 2, 2, 5, 5, 2, null, null);
            moveSlideMoveSlideTest(2, 2, 3, 1, null, null, 3, 1);
            moveSlideMoveSlideTest(2, 2, 3, 2, null, null, 3, 2);
            moveSlideMoveSlideTest(2, 2, 3, 3, null, null, null, null);
            moveSlideMoveSlideTest(2, 2, 3, 4, null, null, 3, 4);
            moveSlideMoveSlideTest(2, 2, 3, 5, null, null, 3, 5);
            moveSlideMoveSlideTest(2, 2, 4, 1, null, null, 4, 1);
            moveSlideMoveSlideTest(2, 2, 4, 2, null, null, 4, 2);
            moveSlideMoveSlideTest(2, 2, 4, 3, null, null, 4, 3);
            moveSlideMoveSlideTest(2, 2, 4, 4, null, null, null, null);
            moveSlideMoveSlideTest(2, 2, 4, 5, null, null, 4, 5);
            moveSlideMoveSlideTest(2, 2, 5, 1, null, null, 5, 1);
            moveSlideMoveSlideTest(2, 2, 5, 2, null, null, 5, 2);
            moveSlideMoveSlideTest(2, 2, 5, 3, null, null, 5, 3);
            moveSlideMoveSlideTest(2, 2, 5, 4, null, null, 5, 4);
            moveSlideMoveSlideTest(2, 2, 5, 5, null, null, null, null);

            moveSlideMoveSlideTest(2, 3, 1, 1, 2, 3, null, null);
            moveSlideMoveSlideTest(2, 3, 1, 2, 1, 3, null, null);
            moveSlideMoveSlideTest(2, 3, 1, 3, 1, 2, 1, 3);
            moveSlideMoveSlideTest(2, 3, 1, 4, 1, 2, 1, 4);
            moveSlideMoveSlideTest(2, 3, 1, 5, 1, 2, 1, 5);
            moveSlideMoveSlideTest(2, 3, 2, 1, 1, 3, null, null);
            moveSlideMoveSlideTest(2, 3, 2, 2, 2, 3, null, null);
            moveSlideMoveSlideTest(2, 3, 2, 3, null, null, null, null);
            moveSlideMoveSlideTest(2, 3, 2, 4, 4, 3, null, null);
            moveSlideMoveSlideTest(2, 3, 2, 5, 5, 3, null, null);
            moveSlideMoveSlideTest(2, 3, 3, 1, null, null, 2, 1);
            moveSlideMoveSlideTest(2, 3, 3, 2, null, null, null, null);
            moveSlideMoveSlideTest(2, 3, 3, 3, 2, 3, null, null);
            moveSlideMoveSlideTest(2, 3, 3, 4, null, null, 2, 4);
            moveSlideMoveSlideTest(2, 3, 3, 5, null, null, 2, 5);
            moveSlideMoveSlideTest(2, 3, 4, 1, 3, 4, 4, 1);
            moveSlideMoveSlideTest(2, 3, 4, 2, 3, 4, 4, 2);
            moveSlideMoveSlideTest(2, 3, 4, 3, 2, 4, 4, 2);
            moveSlideMoveSlideTest(2, 3, 4, 4, 2, 3, null, null);
            moveSlideMoveSlideTest(2, 3, 4, 5, 2, 3, 4, 5);
            moveSlideMoveSlideTest(2, 3, 5, 1, 3, 4, 5, 1);
            moveSlideMoveSlideTest(2, 3, 5, 2, 3, 4, 5, 2);
            moveSlideMoveSlideTest(2, 3, 5, 3, 2, 4, 5, 2);
            moveSlideMoveSlideTest(2, 3, 5, 4, 2, 3, 5, 4);
            moveSlideMoveSlideTest(2, 3, 5, 5, 2, 3, null, null);

            moveSlideMoveSlideTest(3, 4, 1, 5, 2, 3, 1, 5);
        });
    });
});
