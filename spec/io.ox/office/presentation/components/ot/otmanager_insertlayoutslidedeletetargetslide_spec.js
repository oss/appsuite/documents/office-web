/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        localOperation = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    // the test runner
    var TestRunner = Utils.TestRunner;
    var testRunner = new TestRunner(otManagerPresentation);

    // insertLayoutSlide and local deleteTargetSlide operation (handleInsertLayoutSlideDeleteTargetSlide)
    // insertLayoutSlide: { start: 1, id: "2147483658", target: "2147483648", attrs: { slide: { type: "cust" } } }
    // -> the 'deleteTargetSlide' operation conflicts only with insertLayoutSlide operation, if another layoutSlide
    //    is deleted (then the insert position for the layout slide might have changed). Or if the corresponding masterSlide
    //    is deleted. In this case the insertLayoutSlide can be removed (a reload is required!).
    //
    // delete operation and deleteTargetSlide operation:
    //  { name: "delete", start: [0], target: "2147483761" }
    //  { name: "deleteTargetSlide", id: "2147483761", index: 2, parenttarget: "2147483756" }
    //

    describe('method transformOperation', function () {

        it('should calculate valid transformed insertLayoutSlide operation after local deleteTargetSlide operation of layout slide', function () {

            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertLayoutSlide operation after local deleteTargetSlide operation of master slide', function () {

            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should reload document if the master slide for the new inserted layout slide was locally removed', function () {

            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localOperation = { name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });

        it('should calculate valid transformed insertLayoutSlide operation after external deleteTargetSlide operation of layout slide', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertLayoutSlide operation after external deleteTargetSlide operation of master slide', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }], transformedOps);
        });

        it('should reload document if the master slide for the new locally inserted layout slide was removed', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 };
            localOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            testRunner.expectBidiError(oneOperation, localOperation);
        });

        it('should NOT change insertLayoutSlide operation after local delete operation of layout slide', function () {

            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should NOT change insertLayoutSlide operation after local delete operation of master slide', function () {

            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should NOT change insertLayoutSlide operation after external delete operation of layout slide', function () {

            oneOperation = { name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }], transformedOps);
        });

        it('should NOT change insertLayoutSlide operation after external delete operation of master slide', function () {

            oneOperation = { name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

    });

});
