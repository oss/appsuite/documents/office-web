/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // insertText and local move operation (handleMoveInsertChar) in OX Presentation
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 6] }
        it('should calculate valid transformed insertText operation after local move operation if insertChar position is longer than move position', function () {

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 4]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 5]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 5]);
            expect(localActions[0].operations[0].to).toEqual([3, 4]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 5, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 5]);
            expect(localActions[0].operations[0].end).toEqual([3, 5]);
            expect(localActions[0].operations[0].to).toEqual([3, 8]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 4]);
            expect(localActions[0].operations[0].end).toEqual([3, 4]);
            expect(localActions[0].operations[0].to).toEqual([3, 8]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 8, 0, 0]);

            oneOperation = { name: 'insertText', start: [0, 4, 0, 0], target: '123456', text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [0, 4], end: [0, 4], to: [0, 8], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([0, 4]);
            expect(localActions[0].operations[0].end).toEqual([0, 4]);
            expect(localActions[0].operations[0].to).toEqual([0, 8]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 4, 0, 0]);

            oneOperation = { name: 'insertText', start: [0, 4, 0, 0], target: '123456', text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [0, 4], end: [0, 4], to: [0, 8], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([0, 4]);
            expect(localActions[0].operations[0].end).toEqual([0, 4]);
            expect(localActions[0].operations[0].to).toEqual([0, 8]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 4, 0, 0]);

            oneOperation = { name: 'insertText', start: [0, 4, 0, 0], target: '123456', text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [0, 4], end: [0, 4], to: [0, 8], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([0, 4]);
            expect(localActions[0].operations[0].end).toEqual([0, 4]);
            expect(localActions[0].operations[0].to).toEqual([0, 8]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0, 8, 0, 0]);
        });

        it('should calculate valid transformed insertDrawing operation after local move operation if insertChar position has same length as the move position', function () {

            oneOperation = { name: 'insertDrawing', start: [3, 6], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 6]);

            oneOperation = { name: 'insertDrawing', start: [3, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);

            oneOperation = { name: 'insertDrawing', start: [2, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 1]);

            oneOperation = { name: 'insertDrawing', start: [3, 1], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], target: ' 123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);

            oneOperation = { name: 'insertDrawing', start: [3, 1], text: 'ooo', target: '234567', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], target: ' 123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);

            oneOperation = { name: 'insertDrawing', start: [3, 1], text: 'ooo', target: ' 123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], target: ' 123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 3]);
            expect(localActions[0].operations[0].end).toEqual([3, 3]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);

            oneOperation = { name: 'insertDrawing', start: [3, 3], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 6]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2]);

            oneOperation = { name: 'insertDrawing', start: [3, 3], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4]);

            oneOperation = { name: 'insertDrawing', start: [3, 3], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toBeUndefined();
            expect(localActions[0].operations[0].to).toEqual([3, 2]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4]);
        });
    });

});
