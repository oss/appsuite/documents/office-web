/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class OTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // deleteColumns and local insertSlide (handleInsertCompDeleteColumns)
        it('should calculate valid transformed deleteColumns operation after local insertSlide operation', function () {

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1, 1] };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteColumns', start: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [1, 1], target: '123456' };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteColumns', opl: 1, osn: 1, start: [1, 1], target: '123456' }], transformedOps);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [0, 1] };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteColumns', start: [0, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteColumns', opl: 1, osn: 1, start: [5, 1] };
            localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteColumns', start: [6, 1], opl: 1, osn: 1 }], transformedOps);
        });

        // deleteColumns and external insertSlide (handleInsertCompDeleteColumns)
        it('should calculate valid transformed deleteColumns operation after external insertSlide operation', function () {

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 1], opl: 1, osn: 1, target: '123456' }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [1, 1], opl: 1, osn: 1, target: '123456' }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [0, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [0, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteColumns', start: [5, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteColumns', start: [6, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);
        });

    });
});
