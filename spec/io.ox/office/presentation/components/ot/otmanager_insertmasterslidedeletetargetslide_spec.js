/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    // insertMasterSlide and local deleteTargetSlide operation (handleInsertMasterSlideDeleteTargetSlide)
    // pptx:
    // insertMasterSlide: { start: 1, id: "2147483658", attrs: { slide: { type: "cust" } } }
    // deleteTargetSlide: { id: "2147483761", index: 2 }
    // In odp there are no 'start' or 'index' numbers, because the master (layout) slide are not sorted.
    //
    // delete operation and deleteTargetSlide operation:
    //  { name: "delete", start: [0], target: "2147483761" }
    //  { name: "deleteTargetSlide", id: "2147483761", index: 2 }
    //

    describe('method transformOperation', function () {

        it('should calculate valid transformed insertMasterSlide operation after local deleteTargetSlide operation of layout slide', function () {

            oneOperation = { name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertMasterSlide operation after local deleteTargetSlide operation of master slide', function () {

            oneOperation = { name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertMasterSlide', start: 2, id: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 3, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', start: 2, id: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertMasterSlide', start: 3, id: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', start: 2, id: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertMasterSlide operation after external deleteTargetSlide operation of layout slide', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }], transformedOps);
        });

        it('should calculate valid transformed insertMasterSlide operation after external deleteTargetSlide operation of master slide', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'master_2', index: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', start: 2, id: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', start: 2, id: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'master_2', index: 3, opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', start: 3, id: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', start: 2, id: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }], transformedOps);
        });

        it('should NOT change insertMasterSlide operation after local delete operation of layout slide', function () {

            oneOperation = { name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should NOT change insertMasterSlide operation after local delete operation of master slide', function () {

            oneOperation = { name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [1], target: 'master_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1], target: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2], target: 'master_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2], target: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        it('should NOT change insertMasterSlide operation after external delete operation of layout slide', function () {

            oneOperation = { name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 }], transformedOps);
        });

        it('should NOT change insertMasterSlide operation after external delete operation of master slide', function () {

            oneOperation = { name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [1], target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [1], target: 'master_1', opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'delete', start: [2], target: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', start: 1, id: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'delete', start: [2], target: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        // ODP
        it('should NOT change insertMasterSlide operation after local deleteTargetSlide operation of master slide (ODP)', function () {

            oneOperation = { name: 'insertMasterSlide', id: 'master_1', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertMasterSlide', id: 'master_1', opl: 1, osn: 1 }], transformedOps);
        });

        // ODP
        it('should NOT change insertMasterSlide operation after external deleteTargetSlide operation of master slide (ODP)', function () {

            oneOperation = { name: 'deleteTargetSlide', id: 'master_2', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertMasterSlide', id: 'master_1', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertMasterSlide', id: 'master_1', opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'deleteTargetSlide', id: 'master_2', opl: 1, osn: 1 }], transformedOps);
        });
    });

});
