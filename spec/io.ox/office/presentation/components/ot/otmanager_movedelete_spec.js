/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as Utils from '~/othelper';
import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManagerPresentation = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    // shortcuts for the utils functions
    var expectAction = Utils.expectAction,
        expectOp = Utils.expectOp;

    describe('method transformOperation', function () {

        // delete and local move operation (handleMoveDelete)
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 1] }
        it('should calculate valid transformed delete operation after local move operation', function () {

            oneOperation = { name: 'delete', start: [3, 2, 0, 1], end: [3, 2, 0, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 6]);
            expect(localActions[0].operations[0].end).toEqual([3, 6]);
            expect(localActions[0].operations[0].to).toEqual([3, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 3, 0, 1]);
            expect(transformedOps[0].end).toEqual([3, 3, 0, 6]);

            oneOperation = { name: 'delete', start: [2, 2], end: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 6], end: [2, 6], to: [2, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 5]);
            expect(localActions[0].operations[0].end).toEqual([2, 5]);
            expect(localActions[0].operations[0].to).toEqual([2, 6]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 2]);
            expect(transformedOps[0].end).toEqual([2, 2]);

            oneOperation = { name: 'delete', start: [2, 2], end: [2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 6], end: [2, 6], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 5]);
            expect(localActions[0].operations[0].end).toEqual([2, 5]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 3]);
            expect(transformedOps[0].end).toEqual([2, 3]);

            oneOperation = { name: 'delete', start: [2, 5], end: [2, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 5]);
            expect(transformedOps[0].end).toEqual([2, 5]);

            oneOperation = { name: 'delete', start: [2, 1], end: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 3]);
            expect(localActions[0].operations[0].end).toEqual([2, 3]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 2]);
            expect(transformedOps[0].end).toEqual([2, 2]);

            oneOperation = { name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 1]);
            expect(transformedOps[0].end).toEqual([2, 1]);

            oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 4]);
            expect(localActions[0].operations[0].end).toEqual([1, 4]);
            expect(localActions[0].operations[0].to).toEqual([1, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([1]);

            oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2]);

            oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2]);

            oneOperation = { name: 'delete', start: [0], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [0, 4], end: [0, 4], to: [0, 1], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([0, 4]);
            expect(localActions[0].operations[0].end).toEqual([0, 4]);
            expect(localActions[0].operations[0].to).toEqual([0, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0]);

            oneOperation = { name: 'delete', start: [0], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [0, 4], end: [0, 4], to: [0, 1], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(0);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([0]);

            oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3]);

            oneOperation = { name: 'delete', start: [2, 4, 1], end: [2, 4, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 1, 1]);
            expect(transformedOps[0].end).toEqual([2, 1, 3]);

            oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 1]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 4, 1]);
            expect(transformedOps[0].end).toEqual([2, 4, 3]);

            oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 4]);
            expect(localActions[0].operations[0].end).toEqual([2, 4]);
            expect(localActions[0].operations[0].to).toEqual([2, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 3, 1]);
            expect(transformedOps[0].end).toEqual([2, 3, 3]);

            oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 0]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 3, 1]);
            expect(transformedOps[0].end).toEqual([2, 3, 3]);

            oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 2, 1]);
            expect(transformedOps[0].end).toEqual([2, 2, 3]);

            oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].end).toEqual([3, 2]);
            expect(localActions[0].operations[0].to).toEqual([3, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 3, 1]);
            expect(transformedOps[0].end).toEqual([2, 3, 3]);

            oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [1, 2], end: [1, 2], to: [1, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([1, 2]);
            expect(localActions[0].operations[0].end).toEqual([1, 2]);
            expect(localActions[0].operations[0].to).toEqual([1, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 3, 1]);
            expect(transformedOps[0].end).toEqual([2, 3, 3]);

            oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 3, 1]);
            expect(transformedOps[0].end).toEqual([2, 3, 3]);

            oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], target: '234567', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 3, 1]);
            expect(transformedOps[0].end).toEqual([2, 3, 3]);

            oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).toEqual([2, 2]);
            expect(localActions[0].operations[0].end).toEqual([2, 2]);
            expect(localActions[0].operations[0].to).toEqual([2, 5]);
            expect(localActions[0].operations).toHaveLength(1);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 2, 1]);
            expect(transformedOps[0].end).toEqual([2, 2, 3]);
        });

        // move and local delete operation (handleMoveDelete)
        // { name: move, start: [2, 4], end: [2, 4], to: [2, 1] }
        it('should calculate valid transformed move operation after local delete operation', function () {

            oneOperation = { name: 'move', start: [2, 6], end: [2, 6], to: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 5], end: [2, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 1], end: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([], transformedOps);

            oneOperation = { name: 'move', start: [2, 6], end: [2, 6], to: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 5, 1, 1], end: [2, 5, 1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 6], end: [2, 6], to: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 3], end: [2, 3], to: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 1], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 3], end: [2, 3], to: [2, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 3, 1, 1], end: [2, 3, 1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 6], opl: 1, osn: 1 }], transformedOps);

            oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [2, 1, 1, 1], end: [2, 1, 1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }], transformedOps);
        });
    });
});
