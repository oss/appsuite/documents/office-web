/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import PresentationOTManager from '@/io.ox/office/presentation/components/ot/otmanager';

// class PresentationOTManager ====================================================

describe('Presentation class PresentationOTManager', function () {

    // private helpers ----------------------------------------------------

    var
        otManager = new PresentationOTManager(),
        localActions = null,
        oneOperation = null,
        transformedOps = null;

    describe('method transformOperation', function () {

        // insertText and local group operation (handleGroupInsertChar) in OX Presentation
        // { name: group, start: [2, 4], drawings: [4, 5, 7, 8] }
        it('should calculate valid transformed insertText operation after local group operation if insertText position is longer than group position', function () {

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3, 5, 6, 7]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 0, 0]);

            oneOperation = { name: 'insertText', start: [2, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3, 5, 6, 7]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 4, 0, 0]);

            oneOperation = { name: 'insertText', start: [4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3, 5, 6, 7]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([4, 4, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3, 5, 6, 7]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3, 5, 6, 7]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 0, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 0, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3, 4]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 3, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 3, 4]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 2, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 4]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 1, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 1, 0, 0], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 4]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 0, 0, 0]);

            oneOperation = { name: 'insertText', start: [3, 1, 2, 2], text: 'ooo', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 4]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1, 0, 2, 2]);
        });

        it('should calculate valid transformed insertDrawing operation after local group operation if insertDrawing position has same length as the group position', function () {

            oneOperation = { name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2]);

            oneOperation = { name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3, 6, 7, 8]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2]);

            oneOperation = { name: 'insertDrawing', start: [3, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(localActions[0].operations[0].drawings).toEqual([2, 3, 4, 6, 7, 8]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);

            oneOperation = { name: 'insertDrawing', start: [3, 6], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4]);

            oneOperation = { name: 'insertDrawing', start: [2, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([2, 0]);

            oneOperation = { name: 'insertDrawing', start: [4, 0], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([4, 0]);

            oneOperation = { name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3, 5, 6, 7, 8]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2]);

            oneOperation = { name: 'insertDrawing', start: [3, 4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6, 7], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3, 4, 5, 6, 7]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 4]);

            oneOperation = { name: 'insertDrawing', start: [3, 4], target: '123456', opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3, 4, 5, 6, 7], target: '123456', opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 1]);
            expect(localActions[0].operations[0].drawings).toEqual([1, 2, 3, 5, 6, 7, 8]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2]);
        });

        it('should calculate valid transformed group operation after local insertDrawing operation if insertDrawing position has same length as the group position', function () {

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertDrawing', start: [3, 0], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 0]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 2]);
            expect(transformedOps[0].drawings).toEqual([2, 3, 4]);

            oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertDrawing', start: [3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations).toHaveLength(1);
            expect(localActions[0].operations[0].start).toEqual([3, 2]);
            expect(transformedOps).toHaveLength(1);
            expect(transformedOps[0].start).toEqual([3, 1]);
            expect(transformedOps[0].drawings).toEqual([1, 2, 4]);
        });

    });

});
