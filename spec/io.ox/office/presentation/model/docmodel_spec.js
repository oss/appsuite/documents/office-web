/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from '$/underscore';
import $ from '$/jquery';

import { opRgbColor, Color } from '@/io.ox/office/editframework/utils/color';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Position from '@/io.ox/office/textframework/utils/position';
import TextBaseModel from '@/io.ox/office/textframework/model/editor';
import PresentationModel from '@/io.ox/office/presentation/model/docmodel';

import { createPresentationApp } from '~/presentation/apphelper';

// class PresentationModel ================================================

describe('Presentation class PresentationModel', function () {

    // private helpers ----------------------------------------------------

    var slideDefaultName = 'slide_1',
        masterId_1 = 'master1',
        masterId_2 = 'master2',
        masterId_3 = 'master3',
        layoutId_1 = 'layout1',
        layoutId_2 = 'layout2',
        layoutId_3 = 'layout3',
        layoutId_4 = 'layout4',
        layoutId_5 = 'layout5',
        layoutId_6 = 'layout6',
        backgroundColor = opRgbColor('FFCC00'),
        secondDrawingText = 'Hello Second Drawing',
        pasteText = '123',
        clipboardOperations = [{ name: 'insertText', start: [0, 0], text: pasteText }],
        pasteDrawingText1 = 'Hello Second Drawing123',
        pasteDrawingText2 = 'Hello 123Second Drawing123';

    // the operations to be applied by the document model
    var OPERATIONS = [
        { name: 'setDocumentAttributes', attrs: { page: { width: 33866, height: 19050, orientation: 'landscape' } } },
        { name: 'insertMasterSlide', id: masterId_1 },
        { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1, attrs: { slide: { name: 'Titleslide', type: 'title' } } },
        { name: 'insertDrawing', type: 'shape', start: [0, 0], target: layoutId_1, presentation: { phType: 'ctrTitle' }, drawing: { noGroup: true, name: 'LayoutTitle 1' } },
        { name: 'insertLayoutSlide', id: layoutId_2, target: masterId_1, attrs: { slide: { name: 'NextTitleslide', type: 'twoObj' } } },
        { name: 'insertDrawing', type: 'shape', start: [0, 0], target: layoutId_2, presentation: { phType: 'subTitle' }, drawing: { noGroup: true, name: 'SubTitle 1' } },
        { name: 'insertMasterSlide', id: masterId_2 },
        { name: 'insertLayoutSlide', id: layoutId_3, target: masterId_2 },
        { name: 'insertLayoutSlide', id: layoutId_4, target: masterId_2 },
        { name: 'insertLayoutSlide', id: layoutId_5, target: masterId_2 },
        { name: 'insertSlide', start: [0], target: layoutId_1, attrs: { fill: { type: 'solid', color: Color.RED } } },
        { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1' } } },
        { name: 'insertParagraph', start: [0, 0, 0] },
        { name: 'insertText', text: 'Hello World', start: [0, 0, 0, 0] },
        { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', noGroup: true } } },
        { name: 'insertParagraph', start: [0, 1, 0] },
        { name: 'insertText', text: secondDrawingText, start: [0, 1, 0, 0] }
    ];

    var model = null;
    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        model.getClipboardOperations = function () { return clipboardOperations; };
        model.hasInternalClipboard = function () { return true; };
        model.isClipboardOriginText = function () { return true; };
        model.checkSetClipboardPasteInProgress = function () { return false; };
    });

    // existence check ----------------------------------------------------

    it('should be subclass of TextBaseModel', function () {
        expect(PresentationModel).toBeSubClassOf(TextBaseModel);
    });

    // public methods -----------------------------------------------------

    describe('method isStandardSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isStandardSlideId');
        });
        it('should return whether the specified ID is the ID of a normal (standard) slide', function () {
            expect(model.isStandardSlideId(layoutId_1)).toBeFalse();
            expect(model.isStandardSlideId(masterId_1)).toBeFalse();
            expect(model.isStandardSlideId(slideDefaultName)).toBeTrue();
        });
    });

    describe('method isLayoutSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isLayoutSlideId');
        });
        it('should return whether the specified ID is the ID of a layout slide', function () {
            expect(model.isLayoutSlideId(layoutId_1)).toBeTrue();
            expect(model.isLayoutSlideId(masterId_1)).toBeFalse();
            expect(model.isLayoutSlideId(slideDefaultName)).toBeFalse();
        });
    });

    describe('method isMasterSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isMasterSlideId');
        });
        it('should return whether the specified ID is the ID of a master slide', function () {
            expect(model.isMasterSlideId(layoutId_1)).toBeFalse();
            expect(model.isMasterSlideId(masterId_1)).toBeTrue();
            expect(model.isMasterSlideId(slideDefaultName)).toBeFalse();
        });
    });

    describe('method getStandardSlideCount', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getStandardSlideCount');
        });
        it('should return the number of normal (standard) slides', function () {
            expect(model.getStandardSlideCount()).toBe(1);
        });
    });

    describe('method getMasterSlideCount', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getMasterSlideCount');
        });
        it('should return the number of master and layout slides', function () {
            expect(model.getMasterSlideCount()).toBe(7);
        });
    });

    describe('method getNumberOfMasterSlides', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getNumberOfMasterSlides');
        });
        it('should return the number of master, not layout slides', function () {
            expect(model.getNumberOfMasterSlides()).toBe(2);
        });
    });

    describe('method getNumberOfLayoutSlidesOfSpecifiedMaster', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getNumberOfLayoutSlidesOfSpecifiedMaster');
        });
        it('should return the number of layout slides for a specified master', function () {
            expect(model.getNumberOfLayoutSlidesOfSpecifiedMaster(masterId_1)).toBe(2);
            expect(model.getNumberOfLayoutSlidesOfSpecifiedMaster(masterId_2)).toBe(3);
            expect(model.getNumberOfLayoutSlidesOfSpecifiedMaster(masterId_3)).toBe(-1);
            expect(model.getNumberOfLayoutSlidesOfSpecifiedMaster()).toBe(-1);
            expect(model.getNumberOfLayoutSlidesOfSpecifiedMaster(null)).toBe(-1);
        });
    });

    describe('method getMaxLayoutSlideNumber', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getMaxLayoutSlideNumber');
        });
        it('should return the maximum number of layout slides for any master slide', function () {
            expect(model.getMaxLayoutSlideNumber()).toBe(3);
        });
    });

    describe('method getFirstStandardSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getFirstStandardSlideId');
        });
        it('should return the ID of the first document slide', function () {
            expect(model.getFirstStandardSlideId()).toBe(slideDefaultName);
        });
    });

    describe('method isUsedLayoutSlide', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isUsedLayoutSlide');
        });
        it('should return whether the specified ID belongs to a layout slide that is used by at least one document slide', function () {
            expect(model.isUsedLayoutSlide(masterId_1)).toBeFalse();
            expect(model.isUsedLayoutSlide(masterId_2)).toBeFalse();
            expect(model.isUsedLayoutSlide(layoutId_1)).toBeTrue();
            expect(model.isUsedLayoutSlide(layoutId_2)).toBeFalse();
            expect(model.isUsedLayoutSlide(layoutId_3)).toBeFalse();
            expect(model.isUsedLayoutSlide(layoutId_4)).toBeFalse();
            expect(model.isUsedLayoutSlide(layoutId_5)).toBeFalse();
            expect(model.isUsedLayoutSlide(slideDefaultName)).toBeFalse();
        });
    });

    describe('method isUsedMasterSlideByLayoutSlide', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isUsedMasterSlideByLayoutSlide');
        });
        it('should return whether the specified slide ID belongs to a master slide that is used by at least one layout slide', function () {
            expect(model.isUsedMasterSlideByLayoutSlide(masterId_1)).toBeTrue();
            expect(model.isUsedMasterSlideByLayoutSlide(masterId_2)).toBeTrue();
            expect(model.isUsedMasterSlideByLayoutSlide(masterId_3)).toBeFalse();
            expect(model.isUsedMasterSlideByLayoutSlide(layoutId_1)).toBeFalse();
            expect(model.isUsedMasterSlideByLayoutSlide(layoutId_2)).toBeFalse();
            expect(model.isUsedMasterSlideByLayoutSlide(layoutId_3)).toBeFalse();
            expect(model.isUsedMasterSlideByLayoutSlide(layoutId_4)).toBeFalse();
            expect(model.isUsedMasterSlideByLayoutSlide(layoutId_5)).toBeFalse();
            expect(model.isUsedMasterSlideByLayoutSlide(slideDefaultName)).toBeFalse();
            expect(model.isUsedMasterSlideByLayoutSlide()).toBeFalse();
            expect(model.isUsedMasterSlideByLayoutSlide(null)).toBeFalse();
        });
    });

    describe('method isUsedMasterSlideByStandardSlide', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isUsedMasterSlideByStandardSlide');
        });
        it('should return whether the specified slide ID belongs to a master slide that is used by at least one document slide', function () {
            expect(model.isUsedMasterSlideByStandardSlide(masterId_1)).toBeTrue();
            expect(model.isUsedMasterSlideByStandardSlide(masterId_2)).toBeFalse();
            expect(model.isUsedMasterSlideByStandardSlide(masterId_3)).toBeFalse();
            expect(model.isUsedMasterSlideByStandardSlide(layoutId_1)).toBeFalse();
            expect(model.isUsedMasterSlideByStandardSlide(layoutId_2)).toBeFalse();
            expect(model.isUsedMasterSlideByStandardSlide(layoutId_3)).toBeFalse();
            expect(model.isUsedMasterSlideByStandardSlide(layoutId_4)).toBeFalse();
            expect(model.isUsedMasterSlideByStandardSlide(layoutId_5)).toBeFalse();
            expect(model.isUsedMasterSlideByStandardSlide(slideDefaultName)).toBeFalse();
            expect(model.isUsedMasterSlideByStandardSlide()).toBeFalse();
            expect(model.isUsedMasterSlideByStandardSlide(null)).toBeFalse();
        });
    });

    describe('method isUsedMasterOrLayoutSlide', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isUsedMasterOrLayoutSlide');
        });
        it('should return whether the specified slide ID belongs to a master or layout slide that is used by at least one document slide', function () {
            expect(model.isUsedMasterOrLayoutSlide(masterId_1)).toBeTrue();
            expect(model.isUsedMasterOrLayoutSlide(masterId_2)).toBeFalse();
            expect(model.isUsedMasterOrLayoutSlide(masterId_3)).toBeFalse();
            expect(model.isUsedMasterOrLayoutSlide(layoutId_1)).toBeTrue();
            expect(model.isUsedMasterOrLayoutSlide(layoutId_2)).toBeFalse();
            expect(model.isUsedMasterOrLayoutSlide(layoutId_3)).toBeFalse();
            expect(model.isUsedMasterOrLayoutSlide(layoutId_4)).toBeFalse();
            expect(model.isUsedMasterOrLayoutSlide(layoutId_5)).toBeFalse();
            expect(model.isUsedMasterOrLayoutSlide(slideDefaultName)).toBeFalse();
            expect(model.isUsedMasterOrLayoutSlide()).toBeFalse();
            expect(model.isUsedMasterOrLayoutSlide(null)).toBeFalse();
        });
    });

    describe('method getLayoutSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getLayoutSlideId');
        });
        it('should return the layout slide ID for a document slide specified by its ID', function () {
            expect(model.getLayoutSlideId(masterId_1)).toBeNull();
            expect(model.getLayoutSlideId(masterId_2)).toBeNull();
            expect(model.getLayoutSlideId(masterId_3)).toBeNull();
            expect(model.getLayoutSlideId(layoutId_1)).toBeNull();
            expect(model.getLayoutSlideId(layoutId_2)).toBeNull();
            expect(model.getLayoutSlideId(layoutId_3)).toBeNull();
            expect(model.getLayoutSlideId(layoutId_4)).toBeNull();
            expect(model.getLayoutSlideId(layoutId_5)).toBeNull();
            expect(model.getLayoutSlideId(slideDefaultName)).toBe(layoutId_1);
            expect(model.getLayoutSlideId()).toBeNull();
            expect(model.getLayoutSlideId(null)).toBeNull();
        });
    });

    describe('method getMasterSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getMasterSlideId');
        });
        it('should return the master slide ID for a layout slide specified by its ID', function () {
            expect(model.getMasterSlideId(masterId_1)).toBeNull();
            expect(model.getMasterSlideId(masterId_2)).toBeNull();
            expect(model.getMasterSlideId(masterId_3)).toBeNull();
            expect(model.getMasterSlideId(layoutId_1)).toBe(masterId_1);
            expect(model.getMasterSlideId(layoutId_2)).toBe(masterId_1);
            expect(model.getMasterSlideId(layoutId_3)).toBe(masterId_2);
            expect(model.getMasterSlideId(layoutId_4)).toBe(masterId_2);
            expect(model.getMasterSlideId(layoutId_5)).toBe(masterId_2);
            expect(model.getMasterSlideId(slideDefaultName)).toBeNull();
            expect(model.getMasterSlideId()).toBeNull();
            expect(model.getMasterSlideId(null)).toBeNull();
        });
    });

    describe('method getLayoutOrMasterSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getLayoutOrMasterSlideId');
        });
        it('should return the master slide ID for a layout slide specified by its ID or the layout slide ID for a document slide specified by its ID', function () {
            expect(model.getLayoutOrMasterSlideId(masterId_1)).toBeNull();
            expect(model.getLayoutOrMasterSlideId(masterId_2)).toBeNull();
            expect(model.getLayoutOrMasterSlideId(masterId_3)).toBeNull();
            expect(model.getLayoutOrMasterSlideId(layoutId_1)).toBe(masterId_1);
            expect(model.getLayoutOrMasterSlideId(layoutId_2)).toBe(masterId_1);
            expect(model.getLayoutOrMasterSlideId(layoutId_3)).toBe(masterId_2);
            expect(model.getLayoutOrMasterSlideId(layoutId_4)).toBe(masterId_2);
            expect(model.getLayoutOrMasterSlideId(layoutId_5)).toBe(masterId_2);
            expect(model.getLayoutOrMasterSlideId(slideDefaultName)).toBe(layoutId_1);
            expect(model.getLayoutOrMasterSlideId()).toBeNull();
            expect(model.getLayoutOrMasterSlideId(null)).toBeNull();
        });
    });

    describe('method getCorrespondingMasterSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getCorrespondingMasterSlideId');
        });
        it('should return the master slide ID that belongs to a specified slide ID. Specified can be any kind of slide ID', function () {
            expect(model.getCorrespondingMasterSlideId(masterId_1)).toBe(masterId_1);
            expect(model.getCorrespondingMasterSlideId(masterId_2)).toBe(masterId_2);
            expect(model.getCorrespondingMasterSlideId(masterId_3)).toBeNull();
            expect(model.getCorrespondingMasterSlideId(layoutId_1)).toBe(masterId_1);
            expect(model.getCorrespondingMasterSlideId(layoutId_2)).toBe(masterId_1);
            expect(model.getCorrespondingMasterSlideId(layoutId_3)).toBe(masterId_2);
            expect(model.getCorrespondingMasterSlideId(layoutId_4)).toBe(masterId_2);
            expect(model.getCorrespondingMasterSlideId(layoutId_5)).toBe(masterId_2);
            expect(model.getCorrespondingMasterSlideId(slideDefaultName)).toBe(masterId_1);
            expect(model.getCorrespondingMasterSlideId()).toBeNull();
            expect(model.getCorrespondingMasterSlideId(null)).toBeNull();
        });
    });

    describe('method getLayoutConnection', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getLayoutConnection');
        });
        it('should return the connection object between the document slides and the layout slides', function () {
            expect(model.getLayoutConnection()).toEqual({ slide_1: layoutId_1 });
        });
    });

    describe('method getLayoutMasterConnection', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getLayoutMasterConnection');
        });
        it('should return the connection object between the layout slides and the master slides', function () {
            expect(model.getLayoutMasterConnection()).toEqual({ layout1: masterId_1, layout2: masterId_1, layout3: masterId_2, layout4: masterId_2, layout5: masterId_2 });
        });
    });

    describe('method getSlideIdSet', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getSlideIdSet');
        });
        it('should return the complete set of slide IDs concerning document slide ID, layout slide ID and master slide ID', function () {
            expect(model.getSlideIdSet(masterId_1)).toEqual({ id: null, layoutId: null, masterId: masterId_1 });
            expect(model.getSlideIdSet(masterId_2)).toEqual({ id: null, layoutId: null, masterId: masterId_2 });
            expect(model.getSlideIdSet(masterId_3)).toEqual({ id: null, layoutId: null, masterId: null });
            expect(model.getSlideIdSet(layoutId_1)).toEqual({ id: null, layoutId: layoutId_1, masterId: masterId_1 });
            expect(model.getSlideIdSet(layoutId_2)).toEqual({ id: null, layoutId: layoutId_2, masterId: masterId_1 });
            expect(model.getSlideIdSet(layoutId_3)).toEqual({ id: null, layoutId: layoutId_3, masterId: masterId_2 });
            expect(model.getSlideIdSet(layoutId_4)).toEqual({ id: null, layoutId: layoutId_4, masterId: masterId_2 });
            expect(model.getSlideIdSet(layoutId_5)).toEqual({ id: null, layoutId: layoutId_5, masterId: masterId_2 });
            expect(model.getSlideIdSet(layoutId_6)).toEqual({ id: null, layoutId: null, masterId: null });
            expect(model.getSlideIdSet(slideDefaultName)).toEqual({ id: slideDefaultName, layoutId: layoutId_1, masterId: masterId_1 });
            expect(model.getSlideIdSet('slide_2')).toEqual({ id: null, layoutId: null, masterId: null });
        });
    });

    describe('method getTargetChain', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getTargetChain');
        });
        it('should return the complete set of slide IDs concerning document slide ID, layout slide ID and master slide ID', function () {
            expect(model.getTargetChain(masterId_1)).toEqual([masterId_1]);
            expect(model.getTargetChain(masterId_2)).toEqual([masterId_2]);
            expect(model.getTargetChain(masterId_3)).toEqual([]);
            expect(model.getTargetChain(layoutId_1)).toEqual([layoutId_1, masterId_1]);
            expect(model.getTargetChain(layoutId_2)).toEqual([layoutId_2, masterId_1]);
            expect(model.getTargetChain(layoutId_3)).toEqual([layoutId_3, masterId_2]);
            expect(model.getTargetChain(layoutId_4)).toEqual([layoutId_4, masterId_2]);
            expect(model.getTargetChain(layoutId_5)).toEqual([layoutId_5, masterId_2]);
            expect(model.getTargetChain(layoutId_6)).toEqual([]);
            expect(model.getTargetChain(slideDefaultName)).toEqual(["", layoutId_1, masterId_1]);
            expect(model.getTargetChain('slide_2')).toEqual([]);
            expect(model.getTargetChain(slideDefaultName, true)).toEqual([slideDefaultName, layoutId_1, masterId_1]);
            expect(model.getTargetChain('slide_2', true)).toEqual([]);
        });
    });

    describe('method isFirstSlideInContainer', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isFirstSlideInContainer');
        });
        it('should return whether a specified slide ID belongs to the first slide in its container', function () {
            expect(model.isFirstSlideInContainer(slideDefaultName)).toBeTrue();
            expect(model.isFirstSlideInContainer('slide_2')).toBeFalse();
            expect(model.isFirstSlideInContainer(masterId_1)).toBeTrue();
            expect(model.isFirstSlideInContainer(masterId_2)).toBeFalse();
            expect(model.isFirstSlideInContainer(masterId_3)).toBeFalse();
            expect(model.isFirstSlideInContainer(layoutId_1)).toBeFalse();
            expect(model.isFirstSlideInContainer(layoutId_2)).toBeFalse();
            expect(model.isFirstSlideInContainer(layoutId_3)).toBeFalse();
            expect(model.isFirstSlideInContainer(layoutId_4)).toBeFalse();
            expect(model.isFirstSlideInContainer(layoutId_5)).toBeFalse();
            expect(model.isFirstSlideInContainer(layoutId_6)).toBeFalse();
            expect(model.isFirstSlideInContainer()).toBeFalse();
            expect(model.isFirstSlideInContainer(null)).toBeFalse();
        });
    });

    describe('method isLastSlideInContainer', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isLastSlideInContainer');
        });
        it('should return whether a specified slide ID belongs to the first slide in its container', function () {
            expect(model.isLastSlideInContainer(slideDefaultName)).toBeTrue();
            expect(model.isLastSlideInContainer('slide_2')).toBeFalse();
            expect(model.isLastSlideInContainer(masterId_1)).toBeFalse();
            expect(model.isLastSlideInContainer(masterId_2)).toBeFalse();
            expect(model.isLastSlideInContainer(masterId_3)).toBeFalse();
            expect(model.isLastSlideInContainer(layoutId_1)).toBeFalse();
            expect(model.isLastSlideInContainer(layoutId_2)).toBeFalse();
            expect(model.isLastSlideInContainer(layoutId_3)).toBeFalse();
            expect(model.isLastSlideInContainer(layoutId_4)).toBeFalse();
            expect(model.isLastSlideInContainer(layoutId_5)).toBeTrue();
            expect(model.isLastSlideInContainer(layoutId_6)).toBeFalse();
            expect(model.isLastSlideInContainer()).toBeFalse();
            expect(model.isLastSlideInContainer(null)).toBeFalse();
        });
    });

    describe('method isOnlySlideInView', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isLastSlideInContainer');
        });
        it('should return whether a specified slide ID belongs to a slide that is the only slide in its container', function () {
            expect(model.isOnlySlideInView(slideDefaultName)).toBeTrue();
            expect(model.isOnlySlideInView('slide_2')).toBeFalse();
            expect(model.isOnlySlideInView(masterId_1)).toBeFalse();
            expect(model.isOnlySlideInView(masterId_2)).toBeFalse();
            expect(model.isOnlySlideInView(masterId_3)).toBeFalse();
            expect(model.isOnlySlideInView(layoutId_1)).toBeFalse();
            expect(model.isOnlySlideInView(layoutId_2)).toBeFalse();
            expect(model.isOnlySlideInView(layoutId_3)).toBeFalse();
            expect(model.isOnlySlideInView(layoutId_4)).toBeFalse();
            expect(model.isOnlySlideInView(layoutId_5)).toBeFalse();
            expect(model.isOnlySlideInView(layoutId_6)).toBeFalse();
            expect(model.isOnlySlideInView()).toBeTrue(); // if not specified, that active slide is used
            expect(model.isOnlySlideInView(null)).toBeTrue();
        });
    });

    describe('method getSortedSlideIndexById', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getSortedSlideIndexById');
        });
        it('should return the sorted index for specified slide ID', function () {
            expect(model.getSortedSlideIndexById(slideDefaultName)).toBe(0);
            expect(model.getSortedSlideIndexById('slide_2')).toBe(-1);
            expect(model.getSortedSlideIndexById(masterId_1)).toBe(0);
            expect(model.getSortedSlideIndexById(masterId_2)).toBe(3);
            expect(model.getSortedSlideIndexById(masterId_3)).toBe(-1);
            expect(model.getSortedSlideIndexById(layoutId_1)).toBe(1);
            expect(model.getSortedSlideIndexById(layoutId_2)).toBe(2);
            expect(model.getSortedSlideIndexById(layoutId_3)).toBe(4);
            expect(model.getSortedSlideIndexById(layoutId_4)).toBe(5);
            expect(model.getSortedSlideIndexById(layoutId_5)).toBe(6);
            expect(model.getSortedSlideIndexById(layoutId_6)).toBe(-1);
            expect(model.getSortedSlideIndexById()).toBe(0); // if not specified, that active slide is used
            expect(model.getSortedSlideIndexById(null)).toBe(0);
        });
    });

    describe('method getSlideIdByPosition', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getSlideIdByPosition');
        });
        it('should return the slide ID corresponding to a logical position', function () {
            expect(model.getSlideIdByPosition(0)).toBe(slideDefaultName);
            expect(model.getSlideIdByPosition([0])).toBe(slideDefaultName);
            expect(model.getSlideIdByPosition([0, 0, 2, 4])).toBe(slideDefaultName);
            expect(model.getSlideIdByPosition(1)).toBeNull();
            expect(model.getSlideIdByPosition([1])).toBeNull();
            expect(model.getSlideIdByPosition()).toBeNull();
            expect(model.getSlideIdByPosition(null)).toBeNull();
        });
    });

    describe('method getSlidePositionById', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getSlidePositionById');
        });
        it('should return the logical position of a slide specified by its slide ID', function () {
            expect(model.getSlidePositionById(slideDefaultName)).toEqual([0]);
            expect(model.getSlidePositionById('slide_2')).toBeNull();
            expect(model.getSlidePositionById(masterId_1)).toEqual([0]);
            expect(model.getSlidePositionById(masterId_2)).toEqual([0]);
            expect(model.getSlidePositionById(masterId_3)).toBeNull();
            expect(model.getSlidePositionById(layoutId_1)).toEqual([0]);
            expect(model.getSlidePositionById(layoutId_2)).toEqual([0]);
            expect(model.getSlidePositionById(layoutId_3)).toEqual([0]);
            expect(model.getSlidePositionById(layoutId_4)).toEqual([0]);
            expect(model.getSlidePositionById(layoutId_5)).toEqual([0]);
            expect(model.getSlidePositionById(layoutId_6)).toBeNull();
            expect(model.getSlidePositionById()).toBeNull();
            expect(model.getSlidePositionById(null)).toBeNull();
        });
    });

    describe('method getDocumentSlideIndexById', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getDocumentSlideIndexById');
        });
        it('should return the logical position of a slide specified by its slide ID', function () {
            expect(model.getDocumentSlideIndexById(slideDefaultName)).toBe(0);
            expect(model.getDocumentSlideIndexById('slide_2')).toBe(0);
            expect(model.getDocumentSlideIndexById(masterId_1)).toBe(0);
            expect(model.getDocumentSlideIndexById(masterId_2)).toBe(0);
            expect(model.getDocumentSlideIndexById(masterId_3)).toBe(0);
            expect(model.getDocumentSlideIndexById(layoutId_1)).toBe(0);
            expect(model.getDocumentSlideIndexById(layoutId_2)).toBe(0);
            expect(model.getDocumentSlideIndexById(layoutId_3)).toBe(0);
            expect(model.getDocumentSlideIndexById(layoutId_4)).toBe(0);
            expect(model.getDocumentSlideIndexById(layoutId_5)).toBe(0);
            expect(model.getDocumentSlideIndexById(layoutId_6)).toBe(0);
            expect(model.getDocumentSlideIndexById()).toBe(0);
            expect(model.getDocumentSlideIndexById(null)).toBe(0);
        });
    });

    describe('method getSlideNumByID', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getSlideNumByID');
        });
        it('should return a 1-based number for a specified document slide', function () {
            expect(model.getSlideNumByID(slideDefaultName)).toBe(1);
            expect(model.getSlideNumByID('slide_2')).toBeNull();
            expect(model.getSlideNumByID(masterId_1)).toBeNull();
            expect(model.getSlideNumByID()).toBeNull();
            expect(model.getSlideNumByID(null)).toBeNull();
        });
    });

    describe('method isMultiMasterSlideDocument', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isMultiMasterSlideDocument');
        });
        it('should return a 1-based number for a specified document slide', function () {
            expect(model.isMultiMasterSlideDocument()).toBeTrue();
        });
    });

    describe('method getAllMasterSlideIndices', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getAllMasterSlideIndices');
        });
        it('should return an info object with information about the master slides in the document', function () {
            expect(model.getAllMasterSlideIndices()).toEqual({ master1: 0, master2: 3, order: [0, 3] });
        });
    });

    describe('method getLastPositionInModelBehindMaster', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getLastPositionInModelBehindMaster');
        });
        it('should return the next valid position behind a specified master slide to insert a layout slide', function () {
            expect(model.getLastPositionInModelBehindMaster(masterId_1)).toBe(3);
            expect(model.getLastPositionInModelBehindMaster(masterId_2)).toBe(-1);
            expect(model.getLastPositionInModelBehindMaster(masterId_3)).toBe(-1);
            expect(model.getLastPositionInModelBehindMaster(layoutId_3)).toBe(-1);
            expect(model.getLastPositionInModelBehindMaster(slideDefaultName)).toBe(-1);
            expect(model.getLastPositionInModelBehindMaster()).toBe(-1);
            expect(model.getLastPositionInModelBehindMaster(null)).toBe(-1);
        });
    });

    describe('method isMasterView', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isMasterView');
        });
        it('should return whether the master/layout view is activated', function () {
            expect(model.isMasterView()).toBeFalse();
        });
    });

    describe('method getActiveSlideId', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getActiveSlideId');
        });
        it('should return the ID of the active slide', function () {
            expect(model.getActiveSlideId()).toBe(slideDefaultName);
        });
    });

    describe('method getSlideById', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getSlideById');
        });
        it('should return a slide', function () {
            expect(model.getSlideById('notExistent')).toBeNull();
            expect(model.getSlideById(slideDefaultName)).toBeInstanceOf($);
            expect(model.getSlideById(slideDefaultName).hasClass('slide')).toBeTrue();
        });
    });

    describe('method isStandardSlideCount', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isStandardSlideCount');
        });
        it('should return whether the specified number is the correct number of document slides', function () {
            expect(model.isStandardSlideCount(0)).toBeFalse();
            expect(model.isStandardSlideCount(1)).toBeTrue();
            expect(model.isStandardSlideCount(2)).toBeFalse();
            expect(model.isStandardSlideCount(3)).toBeFalse();
            expect(model.isStandardSlideCount()).toBeFalse();
            expect(model.isStandardSlideCount(null)).toBeFalse();
        });
    });

    describe('method getActiveSlide', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getActiveSlide');
        });
        it('should return a slide', function () {
            expect(model.getActiveSlide()).toBeInstanceOf($);
            expect(model.getActiveSlide().hasClass('slide')).toBeTrue();
        });
    });

    describe('method getLayoutOrMasterSlide', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getLayoutOrMasterSlide');
        });
        it('should return a layout or master slide, if it exists', function () {

            const masterSlide_1 = model.getLayoutOrMasterSlide(masterId_1);
            expect(masterSlide_1).toBeInstanceOf($);
            expect(masterSlide_1.hasClass('slide')).toBeTrue();

            const layoutSlide_4 = model.getLayoutOrMasterSlide(layoutId_4);
            expect(layoutSlide_4).toBeInstanceOf($);
            expect(layoutSlide_4.hasClass('slide')).toBeTrue();

            const documentSlide_1 = model.getLayoutOrMasterSlide(slideDefaultName);
            expect(documentSlide_1).toBeNull();

            const masterSlide_3 = model.getLayoutOrMasterSlide(masterId_3);
            expect(masterSlide_3).toBeNull();

            const layoutSlide_6 = model.getLayoutOrMasterSlide(layoutId_6);
            expect(layoutSlide_6).toBeNull();
        });
    });

    describe('method getSlideIdForNode', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getSlideIdForNode');
        });
        it('should return the ID for a given slide', function () {
            expect(model.getSlideIdForNode(model.getActiveSlide())).toBe(slideDefaultName);
            expect(model.getSlideIdForNode(null)).toBeNull();
            expect(model.getSlideIdForNode(model.getLayoutOrMasterSlide(layoutId_2))).toBe(layoutId_2);
            expect(model.getSlideIdForNode(model.getLayoutOrMasterSlide(layoutId_6))).toBeNull();
            expect(model.getSlideIdForNode(model.getLayoutOrMasterSlide(masterId_2))).toBe(masterId_2);
            expect(model.getSlideIdForNode(model.getLayoutOrMasterSlide(masterId_3))).toBeNull();
        });
    });

    describe('method getAllSlidesOfActiveView', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getAllSlidesOfActiveView');
        });
        it('should return the collection of slides of the active view', function () {
            const allSlides = model.getAllSlidesOfActiveView();
            const allKeys = _.keys(allSlides);
            expect(allKeys).toHaveLength(1);
            expect(allKeys[0]).toBe(slideDefaultName);
            expect(model.getSlideIdForNode(allSlides[slideDefaultName])).toBe(slideDefaultName);
        });
    });

    describe('method getActiveSlidePosition', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getActiveSlidePosition');
        });
        it('should return the logical position of the active slide', function () {
            expect(model.getActiveSlidePosition()).toBeArray();
            expect(model.getActiveSlidePosition()).toHaveLength(1);
            expect(model.getActiveSlidePosition()[0]).toBe(0);
        });
    });

    describe('method getLayoutIdFromType', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getLayoutIdFromType');
        });
        it('should return the layout ID for a specified drawing type', function () {
            expect(model.getLayoutIdFromType('title')).toBe(layoutId_1);
            expect(model.getLayoutIdFromType('twoObj')).toBe(layoutId_2);
            expect(model.getLayoutIdFromType('notExisting')).toBe(layoutId_1);
            expect(model.getLayoutIdFromType()).toBe(layoutId_1);
        });
    });

    describe('method resetBackground', function () {
        it('should exist', function () {
            expect(model).toRespondTo('resetBackground');
        });
        it('should remove the slide background', function () {
            expect(model.getSlideById(slideDefaultName).data('attributes').fill.type).toBe("solid");
            expect(model.getSlideById(slideDefaultName).css('background-color')).toMatch(/./);
            model.resetBackground();
            expect(model.getSlideById(slideDefaultName).data('attributes').fill).toBeUndefined();
            expect(model.getSlideById(slideDefaultName).css('background-color')).toBe("");
        });
    });

    describe('method setBackgroundColor', function () {
        it('should exist', function () {
            expect(model).toRespondTo('setBackgroundColor');
        });
        it('should set the slide background color', function () {
            expect(model.getSlideById(slideDefaultName).data('attributes').fill).toBeUndefined();
            expect(model.getSlideById(slideDefaultName).css('background-color')).toBe("");
            model.setBackgroundColor(backgroundColor);
            expect(model.getSlideById(slideDefaultName).data('attributes').fill.type).toBe("solid");
            expect(model.getSlideById(slideDefaultName).css('background-color')).toMatch(/./);
        });
    });

    describe('method getAllFamilySlideAttributes', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getAllFamilySlideAttributes');
        });
        it('should get the slide attributes without specifying an ID', function () {
            expect(model.getAllFamilySlideAttributes().fill.type).toBe("solid");
        });
        it('should get the slide attributes with specifying an ID', function () {
            expect(model.getAllFamilySlideAttributes(slideDefaultName).fill.type).toBe("solid");
        });
    });

    describe('method getVerticalAlignmentMode', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getVerticalAlignmentMode');
        });
        it('should get the vertical alignment mode of the text inside the selected drawing', function () {
            model.getSelection().setTextSelection([0, 1], [0, 2]);
            expect(model.getSelection().getSelectedDrawing()).toHaveLength(1);
            expect(model.getSelection().getSelectedDrawing().text()).toBe(secondDrawingText);
            expect(_.last(model.getSelection().getStartPosition())).toBe(1);
            expect(_.last(model.getSelection().getEndPosition())).toBe(2);
            expect(model.getVerticalAlignmentMode()).toBe("top");
        });
    });

    describe('method setVerticalAlignmentMode', function () {
        it('should exist', function () {
            expect(model).toRespondTo('setVerticalAlignmentMode');
        });
        it('should set the vertical alignment mode of the text inside the selected drawing', function () {
            model.getSelection().setTextSelection([0, 1], [0, 2]);
            expect(model.getSelection().getSelectedDrawing().text()).toBe(secondDrawingText);
            expect(_.last(model.getSelection().getStartPosition())).toBe(1);
            expect(_.last(model.getSelection().getEndPosition())).toBe(2);
            model.setVerticalAlignmentMode('centered');
            expect(model.getVerticalAlignmentMode()).toBe("centered");
            model.setVerticalAlignmentMode('bottom');
            expect(model.getVerticalAlignmentMode()).toBe("bottom");
            model.setVerticalAlignmentMode('top');
            expect(model.getVerticalAlignmentMode()).toBe("top");
        });
    });

    describe('method pasteInternalClipboard', function () {
        var // a drawing node
            drawing = null;

        it('should exist', function () {
            expect(model).toRespondTo('pasteInternalClipboard');
        });
        it('should paste some text into the text frame that is selected', async function () {
            model.getSelection().setTextSelection([0, 1], [0, 2]);
            drawing = model.getSelection().getSelectedDrawing();
            expect(drawing.find(DOM.PARAGRAPH_NODE_SELECTOR).first().text()).toBe(secondDrawingText);
            await model.pasteInternalClipboard();
            model.getSelection().setTextSelection([0, 1], [0, 2]);
            expect(drawing.find(DOM.PARAGRAPH_NODE_SELECTOR).first().text()).toBe(pasteDrawingText1);
        });
        it('should paste some text into the text frame that contains the selection', async function () {
            model.getSelection().setTextSelection([0, 1, 0, 6]);
            await model.pasteInternalClipboard();
            expect(drawing.find(DOM.PARAGRAPH_NODE_SELECTOR).first().text()).toBe(pasteDrawingText2);
        });
        it('should paste some text into a new created text frame, if the browser selection is in the clipboard', async function () {
            var currentSlide = model.getSlideById(model.getActiveSlideId());
            expect(currentSlide.children('div.drawing')).toHaveLength(2);
            var followingDrawings = drawing.nextAll('div.drawing').length;
            expect(drawing.nextAll('div.drawing')).toHaveLength(followingDrawings);
            model.getSelection().setTextSelection([0, 0]);
            await model.pasteInternalClipboard();
            expect(drawing.nextAll('div.drawing')).toHaveLength(followingDrawings + 1);
            expect(drawing.nextAll('div.drawing').text()).toBe(pasteText);
            expect(currentSlide.children('div.drawing')).toHaveLength(3);
        });
    });

    describe('method getDefaultTextForTextFrame', function () {

        var // the affected drawing, paragraph and text span nodes
            drawing = null, paragraph = null,
            // the length of the default text
            defaultTextLength = 0;

        it('should exist', function () {
            expect(model).toRespondTo('getDefaultTextForTextFrame');
        });
        it('should return some text for the place holder drawing', function () {
            model.getSelection().setTextSelection([0, 1, 0, 0]); // setting cursor into drawing
            drawing = model.getSelection().getAnyDrawingSelection();
            const defaultText = model.getDefaultTextForTextFrame(drawing);
            expect(defaultText).toBeString();
            defaultTextLength = defaultText.length;
            expect(defaultTextLength).toBeGreaterThan(0);
        });
        it('should insert the default template text into an empty text frame', async function () {
            model.getSelection().setTextSelection([0, 1, 0, 0]); // setting cursor into drawing
            model.getSelection().selectAll(); //

            drawing = model.getSelection().getAnyDrawingSelection();
            paragraph = drawing.find('div.p');

            expect(drawing).toHaveLength(1);
            expect(paragraph).toHaveLength(1);
            expect(_.last(model.getSelection().getStartPosition())).toBe(0);
            expect(_.last(model.getSelection().getEndPosition())).toBeGreaterThan(0);

            await model.deleteSelected({ deleteKey: true });

            expect(_.last(model.getSelection().getStartPosition())).toBe(0);
            expect(_.last(model.getSelection().getEndPosition())).toBe(0);

            // paragraph has no 'official' length
            expect(Position.getParagraphNodeLength(paragraph)).toBe(0);
            // and there is really no content inside the one and only text span
            let textspan = paragraph.children('span');
            expect(textspan).toHaveLength(1);
            expect(textspan.text()).toHaveLength(0);
            expect(DOM.isTextFrameTemplateTextSpan(textspan)).toBeFalse();

            // selecting the drawing, no longer the text itself
            model.getSelection().setTextSelection([0, 1], [0, 2]);

            textspan = paragraph.children('span');
            expect(textspan).toHaveLength(1);
            expect(textspan.text()).toHaveLength(defaultTextLength);
            expect(DOM.isTextFrameTemplateTextSpan(textspan)).toBeTrue();

            // but paragraph has still no 'official' length
            expect(Position.getParagraphNodeLength(paragraph)).toBe(0);

            // selecting again the text in the (empty) text frame
            model.getSelection().setTextSelection([0, 1, 0, 0]);

            textspan = paragraph.children('span');
            expect(textspan).toHaveLength(1);
            expect(textspan.text()).toHaveLength(0);
            expect(DOM.isTextFrameTemplateTextSpan(textspan)).toBeFalse();

            // setting the cursor into the other drawing on this slide
            model.getSelection().setTextSelection([0, 0, 0, 0]);

            textspan = paragraph.children('span');
            expect(textspan).toHaveLength(1);
            expect(textspan.text()).toHaveLength(defaultTextLength);
            expect(DOM.isTextFrameTemplateTextSpan(textspan)).toBeTrue();
        });
    });

    describe('method isAtLeastOneNotGroupableDrawingInSelection', function () {
        it('should exist', function () {
            expect(model).toRespondTo('isAtLeastOneNotGroupableDrawingInSelection');
        });
        it('should return that there is no not groupable drawing in selection', function () {
            expect(model.isAtLeastOneNotGroupableDrawingInSelection()).toBeFalse();
        });
        it('should return that there is no groupable drawing in selection, when only one drawing is selected', function () {
            model.getSelection().setTextSelection([0, 0], [0, 1]);
            expect(model.getSelection().isDrawingSelection()).toBeTrue();
            expect(model.isAtLeastOneNotGroupableDrawingInSelection()).toBeFalse();
        });
        it('should return that there is a not groupable drawing in selection, when both drawings are selected', function () {
            model.getSelection().selectAllDrawingsOnSlide();
            expect(model.getSelection().isMultiSelection()).toBeTrue();
            expect(model.getSelection().isDrawingSelection()).toBeTrue();
            expect(model.isAtLeastOneNotGroupableDrawingInSelection()).toBeTrue();
        });
    });

});

// ========================================================================

describe('Presentation class PresentationModel II', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        layoutId_1 = 'layout1',
        layoutId_2 = 'layout2',
        layoutId_3 = 'layout3',
        layoutId_4 = 'layout4',

        masterId_1 = 'master1',
        masterId_2 = 'master2',
        masterId_3 = 'master3',
        //activeSlide = null,

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' }
                }
            },
            { name: 'insertMasterSlide', id: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_2, target: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_3, target: masterId_1 },

            { name: 'insertMasterSlide', id: masterId_2 },
            { name: 'insertLayoutSlide', id: layoutId_4, target: masterId_2 },

            { name: 'insertMasterSlide', id: masterId_3 },

            { name: 'insertSlide', start: [0], target: layoutId_1 },
            { name: 'insertSlide', start: [1], target: layoutId_1 }

        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // ========================================================================

    describe('method getNextValidSlideIndex', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getNextValidSlideIndex');
        });
        it('should return the next valid slide according to the parameters', function () {

            ////// Master //////
            // next valid (master) upwards -> -1 because there is none before
            expect(model.getNextValidSlideIndex(masterId_1, { upwards: true, masterIsValidSlide: true })).toBe(-1);
            expect(model.getNextValidSlideIndex(masterId_1, { upwards: false, masterIsValidSlide: true })).toBe(4);

            expect(model.getNextValidSlideIndex(masterId_2, { upwards: true, masterIsValidSlide: true })).toBe(0);
            expect(model.getNextValidSlideIndex(masterId_2, { upwards: false, masterIsValidSlide: true })).toBe(6);

            expect(model.getNextValidSlideIndex(masterId_3, { upwards: true, masterIsValidSlide: true })).toBe(4);
            // next valid (master) downwards -> -> -1 because there is not a next master
            expect(model.getNextValidSlideIndex(masterId_3, { upwards: false, masterIsValidSlide: true })).toBe(-1);

            ////// Layout //////
            // next valid (layout) upwards -> -1 because there is no layout slide before
            expect(model.getNextValidSlideIndex(layoutId_1, { upwards: true, masterIsValidSlide: false })).toBe(-1);
            expect(model.getNextValidSlideIndex(layoutId_1, { upwards: false, masterIsValidSlide: false })).toBe(2);

            expect(model.getNextValidSlideIndex(layoutId_4, { upwards: true, masterIsValidSlide: false })).toBe(3);
            // next valid (layout) upwards -> -1 because there is not a next layout slide
            expect(model.getNextValidSlideIndex(layoutId_4, { upwards: false, masterIsValidSlide: false })).toBe(-1);

            ////// Standard //////
            expect(model.getNextValidSlideIndex('slide_1', { upwards: true, masterIsValidSlide: false })).toBe(-1);
            expect(model.getNextValidSlideIndex('slide_1', { upwards: false, masterIsValidSlide: false })).toBe(1);

            expect(model.getNextValidSlideIndex('slide_2', { upwards: true, masterIsValidSlide: false })).toBe(0);
            expect(model.getNextValidSlideIndex('slide_2', { upwards: false, masterIsValidSlide: false })).toBe(-1);
        });
    });
});
