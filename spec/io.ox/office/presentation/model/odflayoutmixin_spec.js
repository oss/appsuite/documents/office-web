/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { KeyCode } from '@/io.ox/office/tk/dom';
import { Color } from '@/io.ox/office/editframework/utils/color';
import { getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Position from '@/io.ox/office/textframework/utils/position';
import OdfLayoutMixin from '@/io.ox/office/presentation/model/odflayoutmixin';
import { getPlaceHolderDrawingType, isEmptyPlaceHolderDrawing } from '@/io.ox/office/presentation/utils/placeholderutils';

import { waitForEvent } from '~/asynchelper';
import { createPresentationApp } from '~/presentation/apphelper';

// mix-in class OdfLayoutMixin ============================================

describe('Presentation mix-in class OdfLayoutMixin', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        selection = null,
        masterId_1 = 'master1',
        slideDefaultName = 'slide_1',
        secondSlideDefaultName = 'slide_2',
        activeSlideId = null,
        activeSlide = null,
        drawing1Attrs = null,
        drawing2Attrs = null,
        drawing3Attrs = null,
        defaultTitleTop = 800,
        defaultBodyTop = 5000,
        loadTitleTop = 900,
        loadBodyTop = 5100,
        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { marginRight: 0, width: 28000, marginBottom: 0, marginTop: 0, marginLeft: 0, height: 21000 }
                }
            },
            {
                name: 'insertMasterSlide', id: masterId_1, attrs: {
                    listStyles: {
                        title: {
                            l1: { paragraph: { bulletFont: { followText: false, name: 'StarSymbol' }, alignment: 'center', indentLeft: 600 }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 44, fontSizeAsian: 44, boldAsian: false, fontSizeComplex: 44, italicAsian: false } }
                        },
                        body: {
                            l1: { paragraph: { indentFirstLine: -600, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 500 }, bullet: { character: '-', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 500 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 32, fontSizeAsian: 32, boldAsian: false, fontSizeComplex: 32, italicAsian: false } },
                            l2: { paragraph: { indentFirstLine: 600, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l3: { paragraph: { indentFirstLine: 1200, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l4: { paragraph: { indentFirstLine: 1800, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l5: { paragraph: { indentFirstLine: 2400, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l6: { paragraph: { indentFirstLine: 3000, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l7: { paragraph: { indentFirstLine: 3600, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l8: { paragraph: { indentFirstLine: 4200, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } },
                            l9: { paragraph: { indentFirstLine: 4800, bulletFont: { followText: false, name: 'Arial' }, bulletSize: { type: 'followText' }, spacingBefore: { type: 'fixed', value: 400 }, bullet: { character: '\u2022', type: 'character' }, indentLeft: 900, spacingAfter: { type: 'fixed', value: 400 } }, character: { color: Color.AUTO, italicComplex: false, underline: false, strike: 'none', fontNameComplex: 'Lucida Sans', bold: false, italic: false, caps: 'none', fillColor: Color.AUTO, fontName: 'Liberation Sans', boldComplex: false, fontNameAsian: 'Microsoft YaHei', fontSize: 28, fontSizeAsian: 28, boldAsian: false, fontSizeComplex: 28, italicAsian: false } }
                        }
                    },
                    fill: { type: 'solid' }
                }
            },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { phType: 'title' }, drawing: { top: defaultTitleTop, left: 1400, width: 25199, height: 3506 } }, target: masterId_1 },
            { name: 'insertParagraph', start: [0, 0, 0], target: masterId_1, attrs: { paragraph: { bullet: { type: 'none' } } } },
            { name: 'insertText', start: [0, 0, 0, 0], target: masterId_1, text: 'Click to edit Master title style' },
            { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { presentation: { phType: 'body' }, drawing: { top: defaultBodyTop, left: 1400, width: 25199, height: 12179 } }, target: masterId_1 },
            { name: 'insertParagraph', start: [0, 1, 0], target: masterId_1, attrs: { paragraph: { level: 0 } } },
            { name: 'insertText', start: [0, 1, 0, 0], target: masterId_1, text: 'Edit Master text styles' },
            { name: 'insertParagraph', start: [0, 1, 1], target: masterId_1, attrs: { paragraph: { level: 1 } } },
            { name: 'insertText', start: [0, 1, 1, 0], target: masterId_1, text: 'Second level' },
            { name: 'insertSlide', start: [0], target: masterId_1, attrs: { slide: { isSlideNum: false, isDate: false, isFooter: false } } },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { userTransformed: true, phType: 'title' }, drawing: { top: loadTitleTop, left: 1400, width: 25199, height: 3506 } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertText', start: [0, 0, 0, 0], text: 'Title' },
            { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { presentation: { phType: 'body' }, drawing: { top: loadBodyTop, left: 1400, width: 25199, height: 12179 } } },
            { name: 'insertParagraph', start: [0, 1, 0], attrs: { paragraph: { level: 0, listStyleId: 'L1' } } },
            { name: 'insertText', start: [0, 1, 0, 0], text: 'Item' }
        ];

    createPresentationApp('odf', OPERATIONS).done(function (app) {
        model = app.getModel();
        selection = model.getSelection();
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(OdfLayoutMixin).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('method changeODFLayout', function () {

        it('should exist', function () {
            expect(model).toRespondTo('changeODFLayout');
        });

        it('should have the first slide activated', function () {
            activeSlideId = model.getActiveSlideId();
            expect(activeSlideId).toBe(slideDefaultName);
        });

        it('should find two drawings on the active slide', function () {
            activeSlide = model.getSlideById(activeSlideId);
            expect(activeSlide).toHaveLength(1);
            const drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(2);
        });

        it('should find the shifted drawings on the active slide after loading', function () {
            const drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(getPlaceHolderDrawingType(drawings[0])).toBe("title");
            expect(getPlaceHolderDrawingType(drawings[1])).toBe("body");

            drawing1Attrs = getExplicitAttributes(drawings[0], 'drawing', true);
            drawing2Attrs = getExplicitAttributes(drawings[1], 'drawing', true);

            expect(drawing1Attrs.top).toBe(loadTitleTop);
            expect(drawing2Attrs.top).toBe(loadBodyTop);
        });

        it('should move the drawings to its default positions after loading, if the layout is not changed', function () {
            const drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);

            model.changeODFLayout('layout_3', activeSlideId); // switching to 'Title and Content'

            drawing1Attrs = getExplicitAttributes(drawings[0], 'drawing', true);
            drawing2Attrs = getExplicitAttributes(drawings[1], 'drawing', true);

            expect(drawing1Attrs.top).toBe(defaultTitleTop);
            expect(drawing2Attrs.top).toBe(defaultBodyTop);
        });

        it('should move the drawings to its default positions after moving, if the layout is not changed', function () {
            const drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);

            // selecting all drawings
            selection.selectAllDrawingsOnSlide();
            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(2);

            // ... and pressing key down
            model.handleDrawingOperations({ keyCode: KeyCode.DOWN_ARROW, shiftKey: false, altKey: false });

            drawing1Attrs = getExplicitAttributeSet(drawings[0], true);
            drawing2Attrs = getExplicitAttributeSet(drawings[1], true);

            expect(drawing1Attrs.drawing.top).toBeGreaterThan(defaultTitleTop);
            expect(drawing2Attrs.drawing.top).toBeGreaterThan(defaultBodyTop);

            // userTransformed must be 'true' nach move operation
            expect(drawing1Attrs.presentation.userTransformed).toBeTrue();
            expect(drawing2Attrs.presentation.userTransformed).toBeTrue();

            model.changeODFLayout('layout_3', activeSlideId); // switching to 'Title and Content'

            drawing1Attrs = getExplicitAttributeSet(drawings[0], true);
            drawing2Attrs = getExplicitAttributeSet(drawings[1], true);

            expect(drawing1Attrs.drawing.top).toBe(defaultTitleTop);
            expect(drawing2Attrs.drawing.top).toBe(defaultBodyTop);

            // userTransformed must not be set after reset to default values
            expect(drawing1Attrs.presentation.userTransformed).toBeUndefined();
            expect(drawing2Attrs.presentation.userTransformed).toBeUndefined();
        });

        it('should add additional place holders if specified by the layout', function () {

            model.changeODFLayout('layout_4', activeSlideId); // switching to 'Title and 2 Contents'

            const drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(3);
        });

        it('should add empty place holders if specified by the layout', function () {
            const drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(isEmptyPlaceHolderDrawing(drawings[0])).toBeFalse();
            expect(isEmptyPlaceHolderDrawing(drawings[1])).toBeFalse();
            expect(isEmptyPlaceHolderDrawing(drawings[2])).toBeTrue(); // new place holder
        });

        it('should restore missing place holder drawings after deletion', async function () {

            let drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            var oxoPos = Position.getOxoPosition(model.getNode(), drawings[2], true);
            expect(oxoPos).toHaveLength(2);
            expect(oxoPos[0]).toBe(0);
            expect(oxoPos[1]).toBe(2);

            selection.setTextSelection(oxoPos, Position.increaseLastIndex(oxoPos)); // selecting the empty place holder drawing
            expect(selection.isDrawingSelection()).toBeTrue();

            // deleting all selected drawings with 'delete' or 'backspace'
            await model.deleteSelected({ deleteKey: true });

            drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(2); // only two remaining drawings

            model.changeODFLayout('layout_4', activeSlideId); // switching to 'Title and 2 Contents'

            drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(3);

            expect(isEmptyPlaceHolderDrawing(drawings[0])).toBeFalse();
            expect(isEmptyPlaceHolderDrawing(drawings[1])).toBeFalse();
            expect(isEmptyPlaceHolderDrawing(drawings[2])).toBeTrue(); // new empty place holder
        });

        it('should increase the number of place holder drawings if required and possible', function () {

            model.changeODFLayout('layout_12', activeSlideId); // switching to 'Title and 6 Contents'

            const drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(7);
        });

        it('should decrease the number of place holder drawings if required and possible', function () {

            model.changeODFLayout('layout_4', activeSlideId); // switching to 'Title and 2 Contents'

            let drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(3);

            model.changeODFLayout('layout_5', activeSlideId); // switching to 'Title only'

            drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(2); // still two drawings, because both have content

            // checking userTransformed
            drawing1Attrs = getExplicitAttributeSet(drawings[0], true);
            drawing2Attrs = getExplicitAttributeSet(drawings[1], true);

            // userTransformed must be set to place holder that is not specified on the layout slide
            expect(drawing1Attrs.presentation.userTransformed).toBeUndefined(); // the title
            expect(drawing2Attrs.presentation.userTransformed).toBeTrue(); // the body is not specified for this layout

            model.changeODFLayout('layout_4', activeSlideId); // switching to 'Title and 2 Contents'

            drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(3);

            expect(isEmptyPlaceHolderDrawing(drawings[0])).toBeFalse();
            expect(isEmptyPlaceHolderDrawing(drawings[1])).toBeFalse();
            expect(isEmptyPlaceHolderDrawing(drawings[2])).toBeTrue(); // new empty place holder

            // userTransformed must not be set after reset to default values
            drawing1Attrs = getExplicitAttributeSet(drawings[0], true);
            drawing2Attrs = getExplicitAttributeSet(drawings[1], true);
            drawing3Attrs = getExplicitAttributeSet(drawings[2], true);

            expect(drawing1Attrs.presentation.userTransformed).toBeUndefined();
            expect(drawing2Attrs.presentation.userTransformed).toBeUndefined();
            expect(drawing3Attrs.presentation.userTransformed).toBeUndefined();
        });

        it('should generate the correct undo operations', async function () {

            let drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(3);

            model.changeODFLayout('layout_12', activeSlideId); // switching to 'Title and 6 Contents'

            drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(7);

            // restoring the drawings again
            await model.getUndoManager().undo();
            drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(3);
        });

        it('should insert a new slide with the specified layout ID', async function () {

            expect(model.getStandardSlideCount()).toBe(1);

            await waitForEvent(model, 'change:activeslide:done', () => {
                model.insertSlide('layout_11'); // inserting new slide with layout 'Title, 4 Contents'
                expect(model.getStandardSlideCount()).toBe(2);
            });

            expect(model.getActiveSlideIndex()).toBe(1); // asynchronous slide formatting has to be finished

            activeSlideId = model.getActiveSlideId();
            expect(activeSlideId).toBe(secondSlideDefaultName);

            activeSlide = model.getSlideById(activeSlideId);
            expect(activeSlide).toHaveLength(1);

            const drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(5);
        });
    });
});
