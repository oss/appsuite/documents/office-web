/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Color } from '@/io.ox/office/editframework/utils/color';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import SlideOperationMixin from '@/io.ox/office/presentation/model/slideoperationmixin';

import { waitForEvent } from '~/asynchelper';
import { createPresentationApp } from '~/presentation/apphelper';

// mix-in class SlideOperationMixin =======================================

describe('Presentation mix-in class SlideOperationMixin', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        layoutId_1 = 'layout1',
        layoutId_2 = 'layout2',
        masterId_1 = 'master1',

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' },
                    defaultTextListStyles: {
                        l1: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 0 } },
                        l2: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 1270 } },
                        l3: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 2540 } }
                    }
                }
            },
            {
                name: 'insertMasterSlide', id: masterId_1, attrs: {
                    listStyles: {
                        title: {
                            l1: {
                                character: { fontSize: 44, fontName: '+mj-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'percent', value: 0 }, defaultTabSize: 2540, bullet: { type: 'none' }, alignment: 'left' }
                            }
                        },
                        body: {
                            l1: {
                                character: { fontSize: 28, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 35 }, defaultTabSize: 2540, bullet: { type: 'character', character: '-' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 635, indentFirstLine: -634 }
                            },
                            body: {
                                l1: {
                                    character: { fontSize: 28, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 35 }, defaultTabSize: 2540, bullet: { type: 'character', character: '-' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 635, indentFirstLine: -634 }
                                },
                                l2: {
                                    character: { fontSize: 24, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 1905, indentFirstLine: -634 }
                                },
                                l3: {
                                    character: { fontSize: 20, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 3175, indentFirstLine: -634 }
                                },
                                l4: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 4445, indentFirstLine: -634 }
                                },
                                l5: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 5715, indentFirstLine: -634 }
                                },
                                l6: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 6985, indentFirstLine: -634 }
                                },
                                l7: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 8255, indentFirstLine: -634 }
                                },
                                l8: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 9525, indentFirstLine: -634 }
                                },
                                l9: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { lineHeight: { type: 'percent', value: 90 }, spacingBefore: { type: 'fixed', value: 17 }, defaultTabSize: 2540, bullet: { type: 'character', character: '\u2022' }, bulletFont: { followText: false, name: 'Arial' }, alignment: 'left', indentLeft: 10795, indentFirstLine: -634 }
                                }
                            },
                            other: {
                                l1: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 0 }
                                },
                                l2: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 1270 }
                                },
                                l3: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 2540 }
                                },
                                l4: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 3810 }
                                },
                                l5: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 5080 }
                                },
                                l6: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 6350 }
                                },
                                l7: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 7620 }
                                },
                                l8: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 8890 }
                                },
                                l9: {
                                    character: { fontSize: 18, fontName: '+mn-lt', type: 'solid', color: Color.TEXT1 },
                                    paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 10160 }
                                }
                            }
                        }
                    }
                }
            },

            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: 1905, top: 5918, width: 21590, height: 4083 } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: 'Mastertitelformat bearbeiten' },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: 3810, top: 10795, width: 17780, height: 4868 } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_1, text: 'Master-Untertitelformat bearbeiten' },

            { name: 'insertLayoutSlide', id: layoutId_2, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_2, type: 'shape', attrs: { presentation: { phType: 'title' }, drawing: { name: 'Titel 1' } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_2 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_2, text: 'Mastertitelformat bearbeiten' },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_2, type: 'shape', attrs: { presentation: { phIndex: 1 }, drawing: { name: 'Drawing 2', left: 1270, top: 4445, width: 11218, height: 12572 } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_2, attrs: { paragraph: { level: 0 } } },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_2, text: 'Mastertextformat bearbeiten' },
            { name: 'insertDrawing', start: [0, 2], target: layoutId_2, type: 'shape', attrs: { presentation: { phIndex: 2 }, drawing: { name: 'Drawing 3', left: 12912, top: 4445, width: 11218, height: 12572 } } },
            { name: 'insertParagraph', start: [0, 2, 0], target: layoutId_2, attrs: { paragraph: { level: 0 } } },
            { name: 'insertText', start: [0, 2, 0, 0], target: layoutId_2, text: 'Mastertextformat bearbeiten' },

            { name: 'insertSlide', start: [0], target: layoutId_1 },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1' } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2' } } },
            { name: 'insertParagraph', start: [0, 1, 0] }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(SlideOperationMixin).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('method changeLayout', function () {
        it('should exist', function () {
            expect(model).toRespondTo('changeLayout');
        });

        it('should remove all empty place holder drawings when switching layout', function () {

            model.getSelection().setTextSelection([0, 0, 0, 0]);  // selecting the first slide

            const activeSlide = model.getSlideById(model.getActiveSlideId());
            expect(activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);

            model.changeLayout(layoutId_2); // switching to the second layout
            expect(activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);

            model.changeLayout(layoutId_1); // switching back to first layout
            expect(activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);
        });

        it('should not remove place holder drawings with content when switching slide layout', function () {

            model.getSelection().setTextSelection([0, 0, 0, 0]);  // selecting the first slide

            const activeSlide = model.getSlideById(model.getActiveSlideId());
            expect(activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);

            model.changeLayout(layoutId_2); // switching to the second layout
            expect(activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);

            model.insertText('Title', [0, 0, 0, 0]);
            model.insertText('Body 1', [0, 1, 0, 0]);
            model.insertText('Body 2', [0, 2, 0, 0]);

            model.changeLayout(layoutId_1); // switching back to first layout
            expect(activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3); // all three drawings are still there

            expect(model.drawingStyles.getAllUnassignedPhDrawings(model.getActiveSlideId())).toHaveLength(1); // one unassigned drawing
        });

    });

    describe('method insertSlide', function () {
        it('should exist', function () {
            expect(model).toRespondTo('insertSlide');
        });
        it('should exist getStandardSlideCount', function () {
            expect(model).toRespondTo('getStandardSlideCount');
        });
        it('should exist getActiveSlideIndex', function () {
            expect(model).toRespondTo('getActiveSlideIndex');
        });
        it('should return the number of normal (standard) slides', function () {
            expect(model.getStandardSlideCount()).toBe(1);
        });
        it('should return the index of the active slide', function () {
            expect(model.getActiveSlideIndex()).toBe(0);
        });
        it('should insert a new standard slide behind the active slide', async function () {

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.insertSlide(layoutId_1); // inserting a new standard slide with specified layout ID behind the active slide
                expect(model.getStandardSlideCount()).toBe(2);
            });

            expect(arg).toBe("slide_2");
            expect(model.getActiveSlideIndex()).toBe(1); // asynchronous slide formatting has to be finished
            expect(model.getActiveSlideId()).toBe("slide_2");
        });
    });

    describe('method deleteSlide', function () {
        it('should exist', function () {
            expect(model).toRespondTo('deleteSlide');
        });
        it('should delete the active slide', async function () {

            expect(model.getActiveSlideIndex()).toBe(1);
            expect(model.getActiveSlideId()).toBe("slide_2");

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.deleteSlide();
                expect(model.getStandardSlideCount()).toBe(1);
            });

            expect(arg).toBe("slide_1");
            expect(model.getActiveSlideIndex()).toBe(0); // asynchronous slide formatting has to be finished
            expect(model.getActiveSlideId()).toBe("slide_1");
        });
    });

    // testing undo of insertSlide and deleteSlide
    describe('method undo', function () {

        it('should undo the insertSlide operation', async function () {

            expect(model.getActiveSlideId()).toBe("slide_1");

            const [arg1] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.insertSlide(layoutId_1); // inserting a new slide behind the active slide
                expect(model.getStandardSlideCount()).toBe(2); // slide count is increased synchronously
            });

            expect(arg1).toBe("slide_3");
            expect(model.getActiveSlideIndex()).toBe(1); // asynchronous slide formatting has to be finished
            expect(model.getActiveSlideId()).toBe("slide_3");

            const [arg2] = await waitForEvent(model, 'change:activeslide:done', () => model.getUndoManager().undo());

            expect(arg2).toBe("slide_1");
            expect(model.getActiveSlideIndex()).toBe(0);
            expect(model.getStandardSlideCount()).toBe(1);
            expect(model.getActiveSlideId()).toBe("slide_1");
        });

        it('should insert a new slide for following delete and undo tests', async function () {

            expect(model.getActiveSlideId()).toBe("slide_1");
            expect(model.getStandardSlideCount()).toBe(1);

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.insertSlide(layoutId_1); // inserting a new slide behind the active slide
                expect(model.getStandardSlideCount()).toBe(2);
            });

            expect(arg).toBe("slide_4");
            expect(model.getActiveSlideIndex()).toBe(1); // asynchronous slide formatting has to be finished
            expect(model.getActiveSlideId()).toBe("slide_4");
        });

        it('should delete the new inserted slide from previous test', async function () {

            expect(model.getStandardSlideCount()).toBe(2);
            expect(model.getActiveSlideId()).toBe("slide_4");

            // registering the handler before calling 'deleteSlide', because the new activated
            // slide ('slide_1') is already formatted and therefore the event 'change:activeslide:done'
            // will be triggered synchronously in this case.
            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.deleteSlide();
                expect(model.getStandardSlideCount()).toBe(1);
            });

            expect(arg).toBe("slide_1");
            expect(model.getActiveSlideIndex()).toBe(0); // asynchronous slide formatting has to be finished
            expect(model.getActiveSlideId()).toBe("slide_1");
        });

        it('should undo the deletion of the slide from the previous test', async function () {

            expect(model.getActiveSlideId()).toBe("slide_1");

            // restoring the drawings again
            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => model.getUndoManager().undo());

            expect(arg).toBe("slide_5");
            expect(model.getActiveSlideIndex()).toBe(1);
            expect(model.getStandardSlideCount()).toBe(2);
            expect(model.getActiveSlideId()).toBe("slide_5");
        });
    });
});

// ========================================================================

describe('Presentation mix-in class SlideOperationMixin II', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        layoutId_1 = 'layout1',
        layoutId_2 = 'layout2',
        layoutId_3 = 'layout3',
        layoutId_4 = 'layout4',
        layoutId_5 = 'layout5',
        layoutId_6 = 'layout6',
        layoutId_7 = 'layout7',
        layoutId_8 = 'layout8',

        masterId_1 = 'master1',
        masterId_2 = 'master2',
        masterId_3 = 'master3',
        //activeSlide = null,

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' }
                }
            },
            { name: 'insertMasterSlide', id: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_2, target: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_3, target: masterId_1 },

            { name: 'insertMasterSlide', id: masterId_2 },
            { name: 'insertLayoutSlide', id: layoutId_4, target: masterId_2 },

            { name: 'insertMasterSlide', id: masterId_3 },
            { name: 'insertLayoutSlide', id: layoutId_5, target: masterId_3 },
            { name: 'insertLayoutSlide', id: layoutId_6, target: masterId_3 },
            { name: 'insertLayoutSlide', id: layoutId_7, target: masterId_3 },
            { name: 'insertLayoutSlide', id: layoutId_8, target: masterId_3 },

            { name: 'insertSlide', start: [0], target: layoutId_1 },
            { name: 'insertSlide', start: [1], target: layoutId_1 },
            { name: 'insertSlide', start: [2], target: layoutId_1 },
            { name: 'insertSlide', start: [3], target: layoutId_1 },
            { name: 'insertSlide', start: [4], target: layoutId_1 },
            { name: 'insertSlide', start: [5], target: layoutId_1 },
            { name: 'insertSlide', start: [6], target: layoutId_1 },
            { name: 'insertSlide', start: [7], target: layoutId_1 },
            { name: 'insertSlide', start: [8], target: layoutId_1 },
            { name: 'insertSlide', start: [9], target: layoutId_1 },
            { name: 'insertSlide', start: [10], target: layoutId_1 },
            { name: 'insertSlide', start: [11], target: layoutId_1 }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        app.isOperationsBlockActive = function () {};
        model = app.getModel();
    });

    ///////////////////////////////////////////////////////////////////
    ///////////////////// tests for normal slides /////////////////////
    ///////////////////////////////////////////////////////////////////
    describe('method moveMultipleSlides', function () {
        it('should exist', function () {
            expect(model).toRespondTo('moveMultipleSlides');
        });

        it('slides should be in normal order from slide_1 to slide_12', function () {

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });

        it('continuous multi selection: should move slide 0, 1 to position 3 and with 1, 2 to 0 back to normal order', function () {

            model.moveMultipleSlides([0, 1], 3);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.moveMultipleSlides([1, 2], 0);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });

        it('continuous multi selection: move first 4 slides to the end and back', function () {

            model.moveMultipleSlides([0, 1, 2, 3], 12);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_12");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_4");

            model.moveMultipleSlides([8, 9, 10, 11], 0);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });

        // before to after on continuous blocks
        it('continuous multi selection: moving a slide after the last or before the first slide should not change the slide order', function () {

            model.moveMultipleSlides([1, 2, 3], 4);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.moveMultipleSlides([1, 2, 3], 1);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });

        it('continuous multi selection: moving a slide between continuous selected slides should not change the slide order', function () {

            model.moveMultipleSlides([1, 2, 3], 2);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.moveMultipleSlides([1, 2, 3], 3);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });

        // EOF before to after on continuous blocks

        ////////////////////// NOT continuous multi selection //////////////////////
        it('NOT continuous multi selection: moving slide 1, 5, 2 to position 1 (before the first selected slide)', function () {

            model.moveMultipleSlides([1, 5, 3], 1);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.moveMultipleSlides([2], 5);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.moveMultipleSlides([2], 6);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });

        it('NOT continuous multi selection: moving slide 1, 5, 2 to position 2 (after the first selected slide)', function () {

            model.moveMultipleSlides([1, 5, 3], 2);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.moveMultipleSlides([2], 5);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.moveMultipleSlides([2], 6);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });

        it('NOT continuous multi selection: moving slide 1, 5, 2 to position 3 (before the selected middle slide)', function () {

            model.moveMultipleSlides([1, 5, 3], 3);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.moveMultipleSlides([1, 3], 4);
            model.moveMultipleSlides([4], 6);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });

        it('NOT continuous multi selection: moving slide 1, 5, 2 to position 4 (after the selected middle slide)', function () {

            model.moveMultipleSlides([1, 5, 3], 4);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.moveMultipleSlides([1, 3], 4);
            model.moveMultipleSlides([4], 6);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });

        it('NOT continuous multi selection: moving slide 1, 5, 2 to position 5 (before the last selected slide)', function () {

            model.moveMultipleSlides([1, 5, 3], 5);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.moveMultipleSlides([3], 1);
            model.moveMultipleSlides([4], 3);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });

        it('NOT continuous multi selection: moving slide 1, 5, 2 to position 6 (after the last selected slide)', function () {

            model.moveMultipleSlides([1, 5, 3], 6);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.moveMultipleSlides([3], 1);
            model.moveMultipleSlides([4], 3);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });
    });

    describe('method moveSlectedSlidesOneStep', function () {
        it('should exist', function () {
            expect(model).toRespondTo('moveSlectedSlidesOneStep');
        });

        it('moving slides down and upwards one step', function () {

            // one step downwards
            model.moveSlectedSlidesOneStep({ downwards: true }, [0, 1, 2, 3]);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            // one step upwards
            model.moveSlectedSlidesOneStep({ downwards: false }, [1, 2, 3, 4]);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            // not continuous selection shoud do nothing - MUST not change the slide order
            model.moveSlectedSlidesOneStep({ downwards: true }, [0, 1, 3, 4]);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");
        });
    });

    ///////////////////////////////////////////////////////////////////
    ////////////////// tests for master/layout slides /////////////////
    ///////////////////////////////////////////////////////////////////
    describe('method moveMultipleSlides #2', function () {

        it('checking if master view order is ok', function () {
            model.selectSlideView({ showMaster: true });

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");
        });

        it('moving layout slides directly AFTER a master slide & also at last position (edge case) & also between two continuous masters (edge case)', function () {

            // moving a slide upwards from a different master after master1
            model.moveMultipleSlides([10], 1);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout8"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout7");

            // moving a slide downwards from a different master after master3
            model.moveMultipleSlides([1], 8);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout8"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout7");

            // (edge case) moving a slide upwards to the last position
            model.moveMultipleSlides([7], 11);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8"); //

            // moving a slide from the same master upwards after master1
            model.moveMultipleSlides([2], 1);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8"); //

            ////// move multiple slides /////

            //  (edge case) moving continuous and not continuous selected slides at last position from a different master when the last slide is a master slide
            // 1) prepare the test: make the last slide a master slide
            model.moveMultipleSlides([7, 8, 9, 10], 6);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout5"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout6"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout7"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout8"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("master3");
            // 2) moving continuous and not continuous selected slides downwards from two different masters after the last slide
            model.moveMultipleSlides([2, 6, 7, 8, 9], 11);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8"); //

            // moving continuous and not continuous selected slides up and downwards from two different masters before master2
            model.moveMultipleSlides([1, 2, 7, 8, 9], 4);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout5"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout6"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout7"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");

            // (edge case) testing if one can insert slides between two continuous masters (masters1 and master2),
            // also reset the slide order back to initial order for the next test
            model.moveMultipleSlides([9], 1);
            model.moveMultipleSlides([3, 4], 2);
            model.moveMultipleSlides([5, 6, 7], 10);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");
        });

        it('moving layout slides in the same master', function () {

            // inserting a singe slide in the same master before it's position - order MUST no change!
            model.moveMultipleSlides([1, 2], 1);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");

            // inserting a singe slide in the same master after it's position - order MUST no change!
            model.moveMultipleSlides([1, 2], 2);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");

            // inserting multiple selected slides in between a continuous selection - order MUST no change!
            model.moveMultipleSlides([7, 8, 9, 10], 8);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");

            // inserting a continuous selection in the same master before the first slide in the selection - order MUST no change!
            model.moveMultipleSlides([7, 8, 9, 10], 7);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");

            // inserting a continuous selection in the same master after the last selected slide  - order MUST no change!
            model.moveMultipleSlides([7, 8, 9, 10], 11);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");

            // moving a continuous selection downwards in the same master
            model.moveMultipleSlides([7, 8], 10);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");

            // moving a continuous selection upwards in the same master
            model.moveMultipleSlides([8, 9], 7);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");

            // moving a not continuous selection in the same master and insert it before the first selected slide
            model.moveMultipleSlides([8, 10], 7);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout7");

            // moving a not continuous selection in the same master and insert it after the last selected slide
            model.moveMultipleSlides([7, 9], 11);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout5");

            // moving a not continuous selection in the same master and insert it between both selected slides (so we have up and downwards movement)
            model.moveMultipleSlides([7, 10], 9);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout6");

            // moving a not continuous selection in the same master and insert it between both selected slides (so we have up and downwards movement)
            // 1) prepare test that we have more slides
            model.moveMultipleSlides([1, 2], 7);
            // 2) the test
            model.moveMultipleSlides([5, 6, 8, 10], 8);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout5");

            // reset the slide order back to initial order for the next test
            model.moveMultipleSlides([6, 7], 1);
            model.moveMultipleSlides([10], 7);
            model.moveMultipleSlides([10], 8);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");
        });

        it('moving layout slides to a different master', function () {

            // (edge case) moving a same index (2) in a master to the same index (2) in a different master
            model.moveMultipleSlides([2], 8);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");

            // moving slides from two masters to a different master
            model.moveMultipleSlides([4, 7, 6, 8, 10], 2);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout7");

            // moving a slide upwards over a empty master section
            model.moveMultipleSlides([10], 8);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("master3");

            // moving a slide downwards over a empty master section
            model.moveMultipleSlides([6], 11);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");

            // moving slides from different masters up and downwards in an empty master section (between master2 and master3)
            model.moveMultipleSlides([5, 7, 2, 3, 10], 9);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("master3");

            // moving slides from different masters up and downwards and also from the same master
            // 1) prepare
            model.moveMultipleSlides([7, 8, 9], 11);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");
            // 2) the test
            model.moveMultipleSlides([1, 2, 5, 9, 10], 6);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout6");

            // inserting slides in between a selection, which contains continuous and non continuous parts
            model.moveMultipleSlides([3, 4, 5, 7, 10], 5);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("master3");

            // reset the slide order back to initial order for the next test
            model.moveMultipleSlides([3, 4], 1);
            model.moveMultipleSlides([9], 11);
            model.moveMultipleSlides([6], 11);
            model.moveMultipleSlides([6, 7], 10);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8");
        });

        it('moving layout slides directly BEFORE a master slide & before the first master slide (edge case)', function () {

            // (edge case) trying to move slides before the first master slide - order MUST no change!
            model.moveMultipleSlides([1, 2], 0);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout8"); //

            // move a slide from a different master upwards before master2
            model.moveMultipleSlides([10], 4);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout8"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout7");

            // move a slide from the same master downwards before master2
            model.moveMultipleSlides([1], 5);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout1"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout7");

            // move a slide from a different master downwards before master3
            model.moveMultipleSlides([1], 7);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout2"); //
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout7");

            ////// move multiple slides /////

            // moving multiple selected slides from the same and a different master up and downwards before master3
            model.moveMultipleSlides([1, 2, 9], 7);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout7");

            // moving multiple continuous selected slides from a different master upwards before master2
            model.moveMultipleSlides([3, 4, 5, 6, 7], 2);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("layout3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("layout5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("layout7");
        });
    });

    describe('method deleteMultipleSlides', function () {
        it('should exist', function () {
            expect(model).toRespondTo('deleteMultipleSlides');
        });

        it('should delete multiple layout slides', function () {

            model.deleteMultipleSlides([2, 3, 4, 10]);
            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("master1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("layout1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("layout8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("layout6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("master2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("master3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("layout5");
            expect(model.getMasterSlideCount()).toBe(7);
            expect(model.getActiveSlideIndex()).toBe(0);
        });

        it('should delete multiple normal slides', function () {

            model.selectSlideView({ showMaster: false });

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_1");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(10)).toBe("slide_11");
            expect(model.getIdOfSlideOfActiveViewAtIndex(11)).toBe("slide_12");

            model.deleteMultipleSlides([0, 11]);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_4");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_6");
            expect(model.getIdOfSlideOfActiveViewAtIndex(5)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(6)).toBe("slide_8");
            expect(model.getIdOfSlideOfActiveViewAtIndex(7)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(8)).toBe("slide_10");
            expect(model.getIdOfSlideOfActiveViewAtIndex(9)).toBe("slide_11");
            expect(model.getStandardSlideCount()).toBe(10);
            expect(model.getActiveSlideIndex()).toBe(0);

            model.deleteMultipleSlides([2, 3, 4, 6, 9]);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_9");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_10");
            expect(model.getStandardSlideCount()).toBe(5);
            expect(model.getActiveSlideIndex()).toBe(2);

            model.deleteMultipleSlides([4]);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_9");
            expect(model.getStandardSlideCount()).toBe(4);
            expect(model.getActiveSlideIndex()).toBe(3);

            model.deleteMultipleSlides([0]);

            expect(model.getIdOfSlideOfActiveViewAtIndex(0)).toBe("slide_3");
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_7");
            expect(model.getIdOfSlideOfActiveViewAtIndex(2)).toBe("slide_9");
            expect(model.getStandardSlideCount()).toBe(3);
        });
    });
});

// ========================================================================

describe('Presentation mix-in class SlideOperationMixin III', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        layoutId_1 = 'layout1',
        layoutId_2 = 'layout2',
        layoutId_3 = 'layout3',
        layoutId_4 = 'layout4',
        layoutId_5 = 'layout5',
        layoutId_6 = 'layout6',
        layoutId_7 = 'layout7',
        layoutId_8 = 'layout8',

        masterId_1 = 'master1',
        masterId_2 = 'master2',
        masterId_3 = 'master3',

        phType = 'title',
        dummyText = 'Hello',

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' }
                }
            },
            { name: 'insertMasterSlide', id: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_2, target: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_3, target: masterId_1 },

            { name: 'insertMasterSlide', id: masterId_2 },
            { name: 'insertLayoutSlide', id: layoutId_4, target: masterId_2 },

            { name: 'insertMasterSlide', id: masterId_3 },
            { name: 'insertLayoutSlide', id: layoutId_5, target: masterId_3 },
            { name: 'insertLayoutSlide', id: layoutId_6, target: masterId_3 },
            { name: 'insertLayoutSlide', id: layoutId_7, target: masterId_3 },
            { name: 'insertLayoutSlide', id: layoutId_8, target: masterId_3 },

            { name: 'insertSlide', start: [0], target: layoutId_3 },
            { name: 'insertDrawing', type: 'shape', start: [0, 0], attrs: { presentation: { phType }, drawing: { name: 'Titel 1', noGroup: true } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertText', start: [0, 0, 0, 0], text: dummyText },
            { name: 'insertSlide', start: [1], target: layoutId_1 },
            { name: 'insertSlide', start: [2], target: layoutId_1 },
            { name: 'insertSlide', start: [3], target: layoutId_1 }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        app.isOperationsBlockActive = function () {};
        app.getView().getSlidePane().setSlidePaneSelection = function () {};
        model = app.getModel();
    });

    describe('method duplicateSlides', function () {
        it('should exist', function () {
            expect(model).toRespondTo('duplicateSlides');
        });

        it('should have duplicate slides resolved successfully', async function () {
            model.selectSlideView({ showMaster: false });
            await model.duplicateSlides([0, 2]);
        });

        it('should duplicate 2 selected slides', function () {
            expect(model.getIdOfSlideOfActiveViewAtIndex(1)).toBe("slide_2");
            expect(model.getIdOfSlideOfActiveViewAtIndex(3)).toBe("slide_5");
            expect(model.getIdOfSlideOfActiveViewAtIndex(4)).toBe("slide_6");
            expect(model.getActiveViewCount()).toBe(6);
        });

        it('should duplicate selected slide', function () {
            expect(model.getActiveViewCount()).toBe(6);
        });

        it('should have duplicate slide resolved successfully', async function () {
            model.selectSlideView({ showMaster: false });
            await model.duplicateSlides([0]);
        });

        it('should duplicate selected slide #2', function () {
            expect(model.getActiveViewCount()).toBe(7);
        });

        it('should have same layout for duplicated slides', function () {
            var parentId1 = model.getParentSlideId('slide_1');
            var parentId2 = model.getParentSlideId('slide_7');

            expect(parentId1).toBe(parentId2);
            expect(parentId1).toBe(layoutId_3);
        });

        it('should have same content as original slide', function () {
            var titlePlaceholderDrawing1 = model.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector('slide_1', phType);
            var titlePlaceholderDrawing2 = model.drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector('slide_7', phType);

            expect(titlePlaceholderDrawing1).toHaveLength(1);
            expect(titlePlaceholderDrawing2).toHaveLength(1);
            expect(titlePlaceholderDrawing1.text()).toBe(titlePlaceholderDrawing2.text());
            expect(titlePlaceholderDrawing1.text()).toBe(dummyText);
        });
    });
});
