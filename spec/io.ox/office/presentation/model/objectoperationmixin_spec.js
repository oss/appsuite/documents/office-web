/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from '$/underscore';
import $ from '$/jquery';

import { convertHmmToLength, getDomNode } from '@/io.ox/office/tk/utils';
import { Rectangle } from '@/io.ox/office/tk/dom';

import ObjectOperationMixin from '@/io.ox/office/presentation/model/objectoperationmixin';
import { CANVAS_EXPANSION_SELECTOR, getCanvasNode } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getHorizontalCellLength, isHorizontallyMergedCell } from '@/io.ox/office/textframework/components/table/table';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import { getElementStyleId, getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { transformOpColor, Color } from '@/io.ox/office/editframework/utils/color';
import { PLACEHOLDER_TEXT_CONTENT } from '@/io.ox/office/presentation/utils/placeholderutils';

import { waitForEvent } from '~/asynchelper';
import { createPresentationApp } from '~/presentation/apphelper';

// mix-in class ObjectOperationMixin ======================================

describe('Presentation mix-in class ObjectOperationMixin', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        view = null,
        selection = null,
        drawingStyles = null,
        layoutId_1 = 'layout1',
        masterId_1 = 'master1',
        activeSlide = null,
        drawing1 = null,
        drawing2 = null,
        drawing3 = null,
        drawing4 = null,
        drawing5 = null,
        operationCounter = 0,
        slide_1_id = 'slide_1', // the ID of the first slide in document view
        slide_3_id = 'slide_3', // the ID of the third slide in document view
        text_para1_drawing1 = 'Hello paragraph 1 in drawing 1',
        text_para1_drawing2 = 'Hello paragraph 1 in drawing 2 of phType content body',
        text_para1_drawing3 = 'Hello paragraph 1 in drawing 3 of phType text body',

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' },
                    defaultTextListStyles: {
                        l1: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 0 } },
                        l2: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 1270 } },
                        l3: { character: { fontSize: 18, fontName: '+mn-lt', color: Color.TEXT1 }, paragraph: { defaultTabSize: 2540, alignment: 'left', indentLeft: 2540 } }
                    }
                }
            },
            { name: 'insertMasterSlide', id: masterId_1 },

            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: 1905, top: 5918, width: 21590, height: 4083 } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: 'Mastertitelformat bearbeiten' },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: 3810, top: 10795, width: 17780, height: 4868 } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_1, text: 'Master-Untertitelformat bearbeiten' },

            { name: 'insertSlide', start: [0], target: layoutId_1 },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow left', left: 5027, top: 6725, width: 9419, height: 6509 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.RED } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow up', left: 4027, top: 5725, width: 9419, height: 6509 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.GREEN } } },
            { name: 'insertParagraph', start: [0, 1, 0] },
            { name: 'insertDrawing', start: [0, 2], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow right', left: 6027, top: 6725, width: 9419, height: 6509 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.BLUE } } },
            { name: 'insertParagraph', start: [0, 2, 0] },
            { name: 'insertDrawing', start: [0, 3], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow down', left: 4027, top: 7725, width: 9419, height: 6509 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.VIOLET } } },
            { name: 'insertParagraph', start: [0, 3, 0] },
            { name: 'insertDrawing', start: [0, 4], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Math Multiply', left: 5027, top: 3725, width: 4419, height: 2509 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.YELLOW } } },
            { name: 'insertParagraph', start: [0, 4, 0] },

            { name: 'insertSlide', start: [1], target: layoutId_1 },
            { name: 'insertDrawing', start: [1, 0], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow left', left: 5027, top: 6725, width: 9419, height: 6000 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.RED } } },
            { name: 'insertParagraph', start: [1, 0, 0] },
            { name: 'insertDrawing', start: [1, 1], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow up', left: 4027, top: 5000, width: 9419, height: 6000 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.GREEN } } },
            { name: 'insertParagraph', start: [1, 1, 0] },
            { name: 'insertDrawing', start: [1, 2], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow right', left: 6027, top: 10000, width: 9419, height: 5000 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.BLUE } } },
            { name: 'insertParagraph', start: [1, 2, 0] },
            { name: 'insertDrawing', start: [1, 3], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow down', left: 4027, top: 7725, width: 9419, height: 6509 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.VIOLET } } },
            { name: 'insertParagraph', start: [1, 3, 0] },

            { name: 'insertSlide', start: [2], target: layoutId_1 },
            { name: 'insertDrawing', start: [2, 0], type: 'shape', attrs: { presentation: { phType: 'ctrTitle', phIndex: 10 }, drawing: { name: 'Titel 1', left: 1000, top: 2000, width: 4000, height: 2000 } } },
            { name: 'insertParagraph', start: [2, 0, 0] },
            { name: 'insertText', start: [2, 0, 0, 0], text: text_para1_drawing1 },
            { name: 'insertDrawing', start: [2, 1], type: 'shape', attrs: { presentation: { phIndex: 1 }, drawing: { name: 'Content body', left: 1000, top: 5000, width: 5000, height: 3000 } } },
            { name: 'insertParagraph', start: [2, 1, 0] },
            { name: 'insertText', start: [2, 1, 0, 0], text: text_para1_drawing2 },
            { name: 'insertDrawing', start: [2, 2], type: 'shape', attrs: { presentation: { phType: 'body', phIndex: 2 }, drawing: { name: 'Text body', left: 1000, top: 9000, width: 5000, height: 5000 } } },
            { name: 'insertParagraph', start: [2, 2, 0] },
            { name: 'insertText', start: [2, 2, 0, 0], text: text_para1_drawing3 },
            { name: 'insertDrawing', start: [2, 3], type: 'shape', attrs: { presentation: { phType: 'pic', phIndex: 11 }, drawing: { name: 'Picture place holder', left: 8000, top: 5000, width: 5000, height: 4000 } } },
            { name: 'insertDrawing', start: [2, 4], type: 'shape', attrs: { presentation: { phType: 'tbl', phIndex: 12 }, drawing: { name: 'Table place holder', left: 8000, top: 10000, width: 5000, height: 4000 } } },

            { name: 'insertSlide', start: [3], target: layoutId_1 },
            { name: 'insertDrawing', start: [3, 0], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow left', left: 1027, top: 6725, width: 9419, height: 6000 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.RED } } },
            { name: 'insertParagraph', start: [3, 0, 0] },
            { name: 'insertDrawing', start: [3, 1], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow up', left: 4027, top: 5000, width: 9419, height: 6000 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.GREEN } } },
            { name: 'insertParagraph', start: [3, 1, 0] },
            { name: 'insertDrawing', start: [3, 2], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow right', left: 6027, top: 10000, width: 9419, height: 5000 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.BLUE } } },
            { name: 'insertParagraph', start: [3, 2, 0] },
            { name: 'insertDrawing', start: [3, 3], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: 'Arrow down', left: 3027, top: 7725, width: 9419, height: 6000 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'solid', color: Color.VIOLET } } },
            { name: 'insertParagraph', start: [3, 3, 0] }

        ];

    function getDrawingFromSelection(drawingSelection) {
        var drawingNode = selection.getDrawingNodeFromMultiSelection(drawingSelection);
        return getExplicitAttributes(drawingNode, 'drawing');
    }

    function getAllSelectedDrawings() {
        return selection.getSelectedDrawingsInfoObject();
    }

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        view = app.getView();
        selection = model.getSelection();
        drawingStyles = model.drawingStyles;
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(ObjectOperationMixin).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('method changeDrawingOrder', function () {

        it('should exist', function () {
            expect(model).toRespondTo('changeDrawingOrder');
        });

        // Check: Starting with cursor selection, single drawing selection and multi drawing selection
        // Check: One step forwards or backwards or completely to foreground or background
        // Check: Keeping the selection after applying the operation
        // Check: Generating only the required operations
        // Check: Undo and redo

        it('should bring the drawing that contains a text selection one step to the top', function () {

            selection.setTextSelection([0, 0, 0, 0]);  // setting the cursor into the first drawing on the first slide

            activeSlide = model.getSlideById(model.getActiveSlideId());

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide

            expect(allDrawings).toHaveLength(5);

            // assigning drawings
            drawing1 = $(allDrawings[0]);
            drawing2 = $(allDrawings[1]);
            drawing3 = $(allDrawings[2]);
            drawing4 = $(allDrawings[3]);
            drawing5 = $(allDrawings[4]);

            // checking order of drawings
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect(drawing2.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(1);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);
            expect(drawing4.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);
            expect(drawing5.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(4);

            operationCounter = model.changeDrawingOrder('forward');

            expect(operationCounter).toBe(1);

            // checking move of drawing
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(1);
            expect(drawing2.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);
            expect(drawing4.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);
            expect(drawing5.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(4);

            // checking merge of text spans
            expect(drawing1.prevAll()).toHaveLength(3); // drawing and two text spans

            // checking the new selection after moving the drawing
            expect(model.getSelection().getStartPosition()).toEqual([0, 1]);
            expect(model.getSelection().getEndPosition()).toEqual([0, 2]);

        });

        it('should bring the selected drawing one step to the top', function () {

            // expecting selection [0,1] to [0,2]

            operationCounter = model.changeDrawingOrder('forward');
            expect(operationCounter).toBe(1);

            // checking move of drawing
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);

            // checking merge of text spans
            expect(drawing1.prevAll()).toHaveLength(5); // two drawings and three text spans

            // checking the new selection after moving the drawing
            expect(model.getSelection().getStartPosition()).toEqual([0, 2]);
            expect(model.getSelection().getEndPosition()).toEqual([0, 3]);

        });

        it('should bring the selected drawing to the top', function () {

            // expecting selection [0,2] to [0,3]

            operationCounter = model.changeDrawingOrder('front');
            expect(operationCounter).toBe(1);

            // checking move of drawing
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(4);

            // checking merge of text spans
            expect(drawing1.prevAll()).toHaveLength(9); // four drawings and five text spans
            expect(drawing1.nextAll()).toHaveLength(2); // only one text span and the br element

            // checking the new selection after moving the drawing
            expect(model.getSelection().getStartPosition()).toEqual([0, 4]);
            expect(model.getSelection().getEndPosition()).toEqual([0, 5]);

        });

        it('should not create any operation to bring the top drawing further to the top', function () {

            // expecting selection [0,4] to [0,5]

            operationCounter = model.changeDrawingOrder('front');
            expect(operationCounter).toBe(0);

            operationCounter = model.changeDrawingOrder('forward');
            expect(operationCounter).toBe(0);

        });

        it('should bring the selected drawing to the back', function () {

            operationCounter = model.changeDrawingOrder('back');
            expect(operationCounter).toBe(1);

            // checking move of drawing
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);

            // checking merge of text spans
            expect(drawing1.prevAll()).toHaveLength(1); // only one text span
            expect(drawing1.nextAll()).toHaveLength(10); // four drawings and five text spans and br element

            // checking the new selection after moving the drawing
            expect(model.getSelection().getStartPosition()).toEqual([0, 0]);
            expect(model.getSelection().getEndPosition()).toEqual([0, 1]);

        });

        it('should not create any operation to bring the background drawing further to the background', function () {

            operationCounter = model.changeDrawingOrder('back');
            expect(operationCounter).toBe(0);

            operationCounter = model.changeDrawingOrder('backward');
            expect(operationCounter).toBe(0);
        });

        it('should undo the operation that brought the upmost drawing to the back', async function () {

            // check that drawing is still a position [0,0]
            expect(model.getSelection().getStartPosition()).toEqual([0, 0]);
            expect(model.getSelection().getEndPosition()).toEqual([0, 1]);
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);

            // bringing drawing1 to the front again
            await model.getUndoManager().undo();

            // checking move of drawing
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(4);

            // checking merge of text spans
            expect(drawing1.prevAll()).toHaveLength(9); // four drawings and five text spans
            expect(drawing1.nextAll()).toHaveLength(2); // only one text span and the br element

            // checking the new selection after moving the drawing
            expect(model.getSelection().getStartPosition()).toEqual([0, 4]);
            expect(model.getSelection().getEndPosition()).toEqual([0, 5]);
        });

        it('should redo the operation that brought the background drawing to the top', async function () {

            // check that drawing is still a position [0,0]
            expect(model.getSelection().getStartPosition()).toEqual([0, 4]);
            expect(model.getSelection().getEndPosition()).toEqual([0, 5]);
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(4);

            // bringing drawing1 to the front again
            await model.getUndoManager().redo();

            // checking move of drawing
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);

            // checking merge of text spans
            expect(drawing1.prevAll()).toHaveLength(1); // only one text span

            // checking the new selection after moving the drawing
            expect(model.getSelection().getStartPosition()).toEqual([0, 0]);
            expect(model.getSelection().getEndPosition()).toEqual([0, 1]);
        });

        // starting with multi selection, the drawings are in the correct order from drawing1 to drawing5

        it('should bring the selected drawings one step to the top with two operations', function () {

            // checking order of drawings
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect(drawing2.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(1);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);
            expect(drawing4.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);
            expect(drawing5.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(4);

            expect(selection.isMultiSelection()).toBeFalse();

            selection.setMultiDrawingSelectionByPosition([[0, 0], [0, 2]]);  // selecting drawing1 and drawing3

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(2);
            expect(selection.getListOfPositions()).toBe("(0,0),(0,2)");

            operationCounter = model.changeDrawingOrder('forward');

            expect(operationCounter).toBe(2);

            // checking move of drawing
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(1);
            expect(drawing2.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);
            expect(drawing4.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);
            expect(drawing5.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(4);

            // checking the new selection after moving the drawing
            expect(selection.getListOfPositions()).toBe("(0,1),(0,3)");
        });

        it('should undo the operations that brought the two drawing one step to the front', async function () {

            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(1);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);

            // bringing drawing1 and drawing 3 one step backwards
            await model.getUndoManager().undo();

            // checking move of drawing
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);

            // checking the new selection after moving the drawing
            expect(selection.getListOfPositions()).toBe("(0,0),(0,2)");
        });

        it('should generate only one operation, if one of the selected drawings is already in the background', function () {

            expect(selection.getListOfPositions()).toBe("(0,0),(0,2)");

            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);

            operationCounter = model.changeDrawingOrder('backward');

            expect(operationCounter).toBe(1);

            // checking move of drawing
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect(drawing2.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(1);
            expect(drawing4.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);
            expect(drawing5.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(4);

            // checking the new selection after moving the drawing
            expect(selection.getListOfPositions()).toBe("(0,0),(0,1)");
        });

        it('should undo the operations that brought the one drawing one step to the back', async function () {

            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(1);

            // bringing drawing1 and drawing 3 one step backwards
            await model.getUndoManager().undo();

            // checking move of drawing
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);

            // checking the new selection after moving the drawing
            // expect(selection.getListOfPositions()).toBe("(0,0),(0,2)");
        });

        it('should bring both selected drawings to the top', function () {

            selection.setMultiDrawingSelectionByPosition([[0, 0], [0, 2], [0, 3]]);  // selecting drawing1, drawing3 and drawing4

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(3);
            expect(selection.getListOfPositions()).toBe("(0,0),(0,2),(0,3)");

            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);
            expect(drawing4.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);

            operationCounter = model.changeDrawingOrder('front');

            expect(operationCounter).toBe(3);

            // checking move of drawing
            expect(drawing1.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);
            expect(drawing2.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(0);
            expect(drawing3.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(3);
            expect(drawing4.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(4);
            expect(drawing5.prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(1);

            // checking the new selection after moving the drawing
            expect(selection.getListOfPositions()).toBe("(0,2),(0,3),(0,4)");
        });

        it('should not create any operation, if all drawings are selected', function () {

            selection.setTextSelection([0, 0, 0, 0]); // setting the cursor into the first drawing on the first slide
            selection.selectAllDrawingsOnSlide();

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(5);
            expect(selection.getListOfPositions()).toBe("(0,0),(0,1),(0,2),(0,3),(0,4)");

            operationCounter = model.changeDrawingOrder('front');
            expect(operationCounter).toBe(0);

            operationCounter = model.changeDrawingOrder('forward');
            expect(operationCounter).toBe(0);

            operationCounter = model.changeDrawingOrder('back');
            expect(operationCounter).toBe(0);

            operationCounter = model.changeDrawingOrder('backward');
            expect(operationCounter).toBe(0);

            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(5);
            expect(selection.getListOfPositions()).toBe("(0,0),(0,1),(0,2),(0,3),(0,4)");
        });

        it('should not create move operations by changing the alignment', function () {
            // Select the text of the first drawing
            selection.setTextSelection([1, 0, 0, 0]);

            // align the drawing to left
            operationCounter = model.changeAlignment('left');
            expect(operationCounter).toBeTrue();

            var allDrawingSelections = getAllSelectedDrawings();

            expect(allDrawingSelections).toHaveLength(1);
            // drawing left position must be on the left side on the page with left 0
            expect(getDrawingFromSelection(allDrawingSelections[0]).left).toBe(0);

            // Select the first and second drawing
            selection.setMultiDrawingSelectionByPosition([[1, 0], [1, 1]]);

            // algin the drawings to 'center'
            operationCounter = model.changeAlignment('center');

            allDrawingSelections = getAllSelectedDrawings();

            expect(allDrawingSelections).toHaveLength(2);

            // check the new left position
            expect(getDrawingFromSelection(allDrawingSelections[0]).left).toBe(2014);

            expect(getDrawingFromSelection(allDrawingSelections[1]).left).toBe(2014);

            // algin the objects to top
            operationCounter = model.changeAlignment('top');

            expect(allDrawingSelections).toHaveLength(2);
            // both drawings must have the same top position, it is the top position of the second drawing
            expect(getDrawingFromSelection(allDrawingSelections[0]).top).toBe(5000);
            expect(getDrawingFromSelection(allDrawingSelections[1]).top).toBe(5000);

            // Select three drawing
            selection.setMultiDrawingSelectionByPosition([[1, 0], [1, 1], [1, 2]]);

            // algin the drawings to 'center'
            operationCounter = model.changeAlignment('middle');

            allDrawingSelections = getAllSelectedDrawings();

            expect(allDrawingSelections).toHaveLength(3);
            //check the new top positions
            expect(getDrawingFromSelection(allDrawingSelections[0]).top).toBe(7000);
            expect(getDrawingFromSelection(allDrawingSelections[1]).top).toBe(7000);
            expect(getDrawingFromSelection(allDrawingSelections[2]).top).toBe(7500);

            // Select the last drawing
            selection.setMultiDrawingSelectionByPosition([[1, 3]]);

            // algin the drawings to 'center'
            operationCounter = model.changeAlignment('right');

            allDrawingSelections = getAllSelectedDrawings();

            expect(allDrawingSelections).toHaveLength(1);
            //check the new top positions
            expect(getDrawingFromSelection(allDrawingSelections[0]).left).toBe(24447);

            // Select the first and last drawing
            selection.setMultiDrawingSelectionByPosition([[1, 0], [1, 3]]);

            // algin the drawings to 'center'
            operationCounter = model.changeAlignment('bottom');

            allDrawingSelections = getAllSelectedDrawings();

            expect(allDrawingSelections).toHaveLength(2);
            //check the new top positions
            expect(getDrawingFromSelection(allDrawingSelections[0]).top).toBe(8234);
            expect(getDrawingFromSelection(allDrawingSelections[1]).top).toBe(7725);
        });

    });

    describe('method distributeDrawings', function () {

        it('should exist', function () {
            expect(model).toRespondTo('distributeDrawings');
        });

        it('should distribute objects properly', function () {
            // Select the text of the first drawing
            selection.setTextSelection([3, 0, 0, 0]);

            // Select the first and second drawing
            selection.setMultiDrawingSelectionByPosition([[3, 0], [3, 1], [3, 2], [3, 3]]);

            // align the drawing to left
            operationCounter = model.distributeDrawings('horzSlide');
            expect(operationCounter).toBeTrue();

            var allDrawingSelections = getAllSelectedDrawings();

            expect(allDrawingSelections).toHaveLength(4);
            // drawing left position must be on the left side on the page with left 0
            expect(getDrawingFromSelection(allDrawingSelections[0]).left).toBe(0);

            // drawing must be on right of page with right edge aligned to page right edge
            expect(getDrawingFromSelection(allDrawingSelections[2]).left).toBe(33866 - 9419); // left = page width - last drawing width

            // align the drawing to left
            operationCounter = model.distributeDrawings('horzDist');
            expect(operationCounter).toBeTrue();

            allDrawingSelections = getAllSelectedDrawings();

            expect(allDrawingSelections).toHaveLength(4);
            // drawing left position must be on the left side on the page with left 0
            expect(getDrawingFromSelection(allDrawingSelections[0]).left).toBe(0);

            // drawing must be on right of page with right edge aligned to page right edge
            expect(getDrawingFromSelection(allDrawingSelections[2]).left).toBe(33866 - 9419); // left = page width - last drawing width

            // align the drawing to left
            operationCounter = model.distributeDrawings('vertSlide');
            expect(operationCounter).toBeTrue();

            allDrawingSelections = getAllSelectedDrawings();

            expect(allDrawingSelections).toHaveLength(4);
            // drawing top position must be on the top side on the page with top 0
            expect(getDrawingFromSelection(allDrawingSelections[1]).top).toBe(0);

            expect(getDrawingFromSelection(allDrawingSelections[0]).top).toBe(4683);
            expect(getDrawingFromSelection(allDrawingSelections[3]).top).toBe(9367);

            // drawing must be on bottom of page with bottom edge aligned to page bottom edge
            expect(getDrawingFromSelection(allDrawingSelections[2]).top).toBe(19050 - 5000);

            // align the drawing to left
            operationCounter = model.distributeDrawings('vertDist');
            expect(operationCounter).toBeTrue();

            allDrawingSelections = getAllSelectedDrawings();

            expect(allDrawingSelections).toHaveLength(4);
            // drawing top position must be on the top side on the page with top 0
            expect(getDrawingFromSelection(allDrawingSelections[1]).top).toBe(0);

            expect(getDrawingFromSelection(allDrawingSelections[0]).top).toBe(4683);
            expect(getDrawingFromSelection(allDrawingSelections[3]).top).toBe(9367);

            // drawing must be on bottom of page with bottom edge aligned to page bottom edge
            expect(getDrawingFromSelection(allDrawingSelections[2]).top).toBe(19050 - 5000);
        });
    });

    // handling insert of images and tables into place holder drawings
    describe('method setActiveSlideId', function () {

        it('should exist', function () {
            expect(model).toRespondTo('setActiveSlideId');
        });

        it('should change to the slide with the specified ID', async function () {

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.setActiveSlideId(slide_3_id); // activating the third slide
            });

            expect(arg).toBe(slide_3_id);

            expect(model.getActiveSlideIndex()).toBe(2); // asynchronous slide formatting has to be finished
            expect(model.getActiveSlideId()).toBe(slide_3_id); // asynchronous slide formatting has to be finished

            activeSlide = model.getSlideById(slide_3_id);

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide

            expect(allDrawings).toHaveLength(5);

            // assigning drawings
            drawing1 = $(allDrawings[0]);
            drawing2 = $(allDrawings[1]);
            drawing3 = $(allDrawings[2]);
            drawing4 = $(allDrawings[3]);
            drawing5 = $(allDrawings[4]);

            expect(drawing3.text()).toBe(text_para1_drawing3);

            expect(model.isPlaceHolderDrawing(drawing1)).toBeTrue();
            expect(model.isPlaceHolderDrawing(drawing2)).toBeTrue();
            expect(model.isPlaceHolderDrawing(drawing3)).toBeTrue();
            expect(model.isPlaceHolderDrawing(drawing4)).toBeTrue();
            expect(model.isPlaceHolderDrawing(drawing5)).toBeTrue();

            expect(model.isEmptyPlaceHolderDrawing(drawing1)).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(drawing2)).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(drawing3)).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(drawing4)).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(drawing5)).toBeTrue();

            selection.setTextSelection([2, 0, 0, 0]);  // setting the cursor into the first drawing on the third slide
        });
    });

    describe('method handleTemplateImageEvent', function () {

        var pictureButton = null;
        var tableButton = null;
        var tableNode = null;
        var defaultTableRowCount = 2;
        var defaultTableCellCount = 10;

        it('should exist', function () {
            expect(model).toRespondTo('handleTemplateImageEvent');
        });

        it('should find buttons for inserting images and tables into the place holder drawings', function () {

            pictureButton = drawing4.find(DOM.IMAGE_TEMPLATE_BUTTON_SELECTOR);
            expect(pictureButton).toHaveLength(1);

            tableButton = drawing5.find(DOM.TABLE_TEMPLATE_BUTTON_SELECTOR);
            expect(tableButton).toHaveLength(1);
        });

        it('should insert a table using the table button inside the place holder drawing', function () {

            model.handleTemplateImageEvent(tableButton);

            expect(drawing5.parent()).toHaveLength(0); // drawing no longer in DOM

            // refreshing the affected drawing assigning drawings
            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5);
            drawing5 = $(allDrawings[4]);

            expect(model.isEmptyPlaceHolderDrawing(drawing5)).toBeFalse();

            // finding the table inside the drawing
            tableNode = drawing5.find(DOM.TABLE_NODE_SELECTOR);
            expect(tableNode).toHaveLength(1);
            expect(DOM.isTableNodeInTableDrawing(tableNode)).toBeTrue();
        });

        it('should be a table with default number of columns and rows', function () {
            expect(DOM.getTableRows(tableNode)).toHaveLength(defaultTableRowCount);
            expect(DOM.getTableCells(tableNode)).toHaveLength(defaultTableCellCount);
        });

        it('should be an empty table place holder after removing the drawing', async function () {

            selection.setTextSelection([2, 4], [2, 5]);  // selecting the drawing that contains the table
            expect(selection.isDrawingSelection()).toBeTrue();

            const selectedDrawing = selection.getSelectedDrawing();

            expect(model.isPlaceHolderDrawing(selectedDrawing)).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(selectedDrawing)).toBeFalse();
            expect(getDomNode(selectedDrawing)).toBe(getDomNode(drawing5));

            // deleting the table drawing
            await model.deleteSelected({ deleteKey: true });

            // refreshing the affected drawing assigning drawings
            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5);

            expect(model.isEmptyPlaceHolderDrawing(allDrawings[4])).toBeTrue();
        });

        it('should remove an empty place holder drawing', async function () {

            selection.setTextSelection([2, 4], [2, 5]);  // selecting the drawing that contains the table

            expect(selection.isDrawingSelection()).toBeTrue();
            let allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            drawing5 = $(allDrawings[4]);
            expect(getDomNode(selection.getSelectedDrawing())).toBe(getDomNode(drawing5));

            // deleting the table drawing
            await model.deleteSelected({ deleteKey: true });

            // refreshing the affected drawing assigning drawings
            allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(4); // the drawing was really removed

            // restoring the drawings again
            await model.getUndoManager().undo();

            allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5); // the drawing was really removed
            // eslint-disable-next-line require-atomic-updates
            drawing5 = $(allDrawings[4]);

            expect(model.isEmptyPlaceHolderDrawing(drawing5)).toBeTrue();
            tableButton = drawing5.find(DOM.TABLE_TEMPLATE_BUTTON_SELECTOR);
            expect(tableButton).toHaveLength(1);
        });
    });

    describe('method insertImageURL', function () {

        it('should exist', function () {
            expect(model).toRespondTo('insertImageURL');
        });
    });

    describe('method insertTextFrame', function () {

        var // a table node inside a drawing of type 'table'
            tableNode = null;

        it('should exist', function () {
            expect(model).toRespondTo('insertTextFrame');
        });

        it('should contain three empty place holder drawings after deleting drawing 2 completely', async function () {

            selection.setTextSelection([2, 1], [2, 2]);  // selecting the drawing that contains the table

            expect(selection.isDrawingSelection()).toBeTrue();
            expect(getDomNode(selection.getSelectedDrawing())).toBe(getDomNode(drawing2));
            expect(model.isEmptyPlaceHolderDrawing(drawing2)).toBeFalse();

            // deleting the table drawing
            await model.deleteSelected({ deleteKey: true });
            expect(drawing2.parent()).toHaveLength(0); // drawing no longer in DOM

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5); // the drawing was really removed
            drawing2 = $(allDrawings[3]);

            expect(model.isEmptyPlaceHolderDrawing(drawing2)).toBeTrue();
        });

        it('should contain four empty place holder drawings after removing the text in drawing 3', async function () {

            var // the text length
                textLength = text_para1_drawing3.length;

            selection.setTextSelection([2, 2], [2, 3]);  // selecting the drawing that contains the table

            expect(selection.isDrawingSelection()).toBeTrue();
            expect(getDomNode(selection.getSelectedDrawing())).toBe(getDomNode(drawing3));
            expect(model.isEmptyPlaceHolderDrawing(drawing3)).toBeFalse();
            expect(drawing3.text()).toBe(text_para1_drawing3);

            // selecting the complete text in the place holder drawing
            selection.setTextSelection([2, 2, 0, 0], [2, 2, 0, textLength]);

            // deleting the table drawing
            await model.deleteSelected({ deleteKey: true });
            expect(drawing3.parent()).toHaveLength(1); // the drawing is still in the DOM

            // const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            // expect(allDrawings).toHaveLength(5); // the drawing was really removed
            // drawing3 = $(allDrawings[4]);

            expect(model.isEmptyPlaceHolderDrawing(drawing3)).toBeTrue();
        });

        // Inserting three tables using the table button in the top bar, not the place holder image.
        // The selection is inside the title drawing node, that is not affected by the table insertion.
        // Used options for inserting from top bar: { insertTable: true, size: { width: 3, height: 5 } }

        it('should have the selection in the title drawing that is not affected by the table insertion', function () {

            selection.setTextSelection([2, 0, 0, 0]);  // setting the selection in the non empty title drawing

            expect(selection.isDrawingSelection()).toBeFalse();
            expect(getDomNode(selection.getSelectedTextFrameDrawing())).toBe(getDomNode(drawing1));
            expect(model.isEmptyPlaceHolderDrawing(drawing1)).toBeFalse();
            expect(drawing1.text()).toBe(text_para1_drawing1);

            expect(model.isEmptyPlaceHolderDrawing(drawing1)).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(drawing2)).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(drawing3)).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(drawing4)).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(drawing5)).toBeTrue();
        });

        it('should insert the first table into the most specialized place holder first', function () {

            var // the options send to insertTextFrame, if the top bar is used for inserting a table
                options = { insertTable: true, size: { width: 3, height: 5 } };

            // drawing 5 is specialized to tables
            expect(model.isEmptyPlaceHolderDrawing(drawing5)).toBeTrue();

            // inserting the table via top bar
            model.insertTextFrame(options);

            expect(drawing5.parent()).toHaveLength(0); // drawing no longer in DOM

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR); // getting all drawings on slide
            expect(allDrawings).toHaveLength(5);
            drawing5 = $(allDrawings[4]);

            // finding the table inside the drawing
            tableNode = drawing5.find(DOM.TABLE_NODE_SELECTOR);
            expect(tableNode).toHaveLength(1);
            expect(DOM.isTableNodeInTableDrawing(tableNode)).toBeTrue();

            expect(model.isEmptyPlaceHolderDrawing(allDrawings[0])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[1])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[2])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[3])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[4])).toBeFalse();
        });

        it('should insert the second table into the content body place holder', function () {

            var // the options send to insertTextFrame, if the top bar is used for inserting a table
                options = { insertTable: true, size: { width: 3, height: 5 } };

            // drawing 2 is a content body place holder
            expect(model.isEmptyPlaceHolderDrawing(drawing2)).toBeTrue();

            // inserting the table via top bar
            model.insertTextFrame(options);

            // expect(drawing2.parent()).toHaveLength(0); // drawing no longer in DOM

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5);
            drawing2 = $(allDrawings[1]);

            // finding the table inside the drawing
            tableNode = drawing2.find(DOM.TABLE_NODE_SELECTOR);
            expect(tableNode).toHaveLength(1);
            expect(DOM.isTableNodeInTableDrawing(tableNode)).toBeTrue();

            expect(model.isEmptyPlaceHolderDrawing(allDrawings[0])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[1])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[2])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[3])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[4])).toBeFalse();
        });

        it('should insert the third table as new drawing into the slide root, not as place holder', function () {

            // the options send to insertTextFrame, if the top bar is used for inserting a table
            const options = { insertTable: true, size: { width: 3, height: 5 } };

            // there are 5 drawings on the slide
            let allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5);

            // inserting the table via top bar
            model.insertTextFrame(options);

            allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(6); // a new drawing was generated
            const drawing6 = $(allDrawings[1]);

            // finding the table inside the drawing
            tableNode = drawing6.find(DOM.TABLE_NODE_SELECTOR);
            expect(tableNode).toHaveLength(1);
            expect(DOM.isTableNodeInTableDrawing(tableNode)).toBeTrue();

            expect(model.isEmptyPlaceHolderDrawing(allDrawings[0])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[1])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[2])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[3])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[4])).toBeFalse();
            expect(model.isPlaceHolderDrawing(allDrawings[5])).toBeFalse();
        });

        // pressing undo 5 times to delete the three tables and insert the text into the place holder drawings

        it('first undo should remove the table drawing that is no place holder drawing', async function () {

            // restoring the drawings again
            await model.getUndoManager().undo();

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5); // the drawing was removed

            expect(model.isEmptyPlaceHolderDrawing(allDrawings[0])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[1])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[2])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[3])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[4])).toBeFalse();
        });

        it('second undo should convert the table place holder drawing to an empty content body place holder drawing', async function () {

            // restoring the drawings again
            await model.getUndoManager().undo();
            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5); // no drawing was removed

            expect(model.isEmptyPlaceHolderDrawing(allDrawings[0])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[1])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[2])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[3])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[4])).toBeFalse();
        });

        it('third undo should convert the table place holder drawing to an empty table place holder drawing', async function () {

            // restoring the drawings again
            await model.getUndoManager().undo();
            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5); // no drawing was removed

            expect(model.isEmptyPlaceHolderDrawing(allDrawings[0])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[1])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[2])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[3])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[4])).toBeTrue();
        });

        it('fourth undo should fill the text into the text place holder drawing', async function () {

            // restoring the drawings again
            await model.getUndoManager().undo();
            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5);

            expect(model.isEmptyPlaceHolderDrawing(allDrawings[0])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[1])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[2])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[3])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[4])).toBeTrue();

            expect($(allDrawings[2]).text()).toBe(text_para1_drawing3);
        });

        // filling the text into the content body place holder drawing
        it('fifth undo should fill the text into the content body place holder drawing', async function () {

            // restoring the drawings again
            await model.getUndoManager().undo();
            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5);

            expect(model.isEmptyPlaceHolderDrawing(allDrawings[0])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[1])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[2])).toBeFalse();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[3])).toBeTrue();
            expect(model.isEmptyPlaceHolderDrawing(allDrawings[4])).toBeTrue();

            expect($(allDrawings[1]).text()).toBe(text_para1_drawing2);
        });
    });

    // Table modifications and table formatting

    describe('method insertTextFrame with table style', function () {

        // a table node inside a drawing of type 'table'
        var tableNode = null;

        it('should exist', function () {
            expect(model).toRespondTo('insertTextFrame');
        });

        it('should change to the slide with the specified ID', async function () {

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.setActiveSlideId(slide_1_id); // activating the third slide
            });

            expect(arg).toBe(slide_1_id);
            expect(model.getActiveSlideIndex()).toBe(0); // asynchronous slide formatting has to be finished
            expect(model.getActiveSlideId()).toBe(slide_1_id); // asynchronous slide formatting has to be finished

            activeSlide = model.getSlideById(slide_1_id);

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide

            expect(allDrawings).toHaveLength(5);

            // assigning drawings
            drawing1 = $(allDrawings[0]);
            drawing2 = $(allDrawings[1]);
            drawing3 = $(allDrawings[2]);
            drawing4 = $(allDrawings[3]);
            drawing5 = $(allDrawings[4]);

            expect(model.isPlaceHolderDrawing(drawing1)).toBeFalse();
            expect(model.isPlaceHolderDrawing(drawing2)).toBeFalse();
            expect(model.isPlaceHolderDrawing(drawing3)).toBeFalse();
            expect(model.isPlaceHolderDrawing(drawing4)).toBeFalse();
            expect(model.isPlaceHolderDrawing(drawing5)).toBeFalse();

            selection.setTextSelection([0, 0, 0, 0]);  // setting the cursor into the first drawing on the third slide
        });

        it('should insert a table with the default table style', async function () {

            // the options send to insertTextFrame, if the top bar is used for inserting a table
            var options = { insertTable: true, size: { width: 2, height: 3 } };
            // the ID of the default table style, if no other style is defined via operation
            var defaultTableStyleId = '{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}';

            // inserting the table via top bar
            model.insertTextFrame(options);

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR); // getting all drawings on slide
            expect(allDrawings).toHaveLength(6);
            drawing5 = $(allDrawings[5]);

            // finding the table inside the drawing
            tableNode = drawing5.find(DOM.TABLE_NODE_SELECTOR);
            expect(tableNode).toHaveLength(1);
            expect(DOM.isTableNodeInTableDrawing(tableNode)).toBeTrue();
            expect(getElementStyleId(drawing5)).toBe(defaultTableStyleId);

            expect(DOM.getTableRows(drawing5)).toHaveLength(3);
            const allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(6);
            const lastCell = allCells.last();

            const tableFormattingDef = $.Deferred(); // promise that will be resolved after list formatting is completed
            model.on('table:formatting:done', function () {
                if (lastCell.css('background-color')) { tableFormattingDef.resolve(); }
            });

            // checking the table cell background (using the DOM node)

            await tableFormattingDef;
            expect($(allCells[1]).css('background-color')).toBeCssColor('#4f81bd');
            expect($(allCells[3]).css('background-color')).toBeCssColor('#d0d8e7');
            expect(lastCell.css('background-color')).toBeCssColor('#e9ecf4');
        });

        // setting the table grid style to the inserted table

        it('should set the table grid table style', async function () {

            // the ID of the table grid style
            var tableGridStyleId = '{5940675A-B579-460E-94D1-54222C63F5DA}';

            model.setAttributes('table', {  styleId: tableGridStyleId }, {  clear: true });

            // finding the table inside the drawing
            tableNode = drawing5.find(DOM.TABLE_NODE_SELECTOR);
            expect(tableNode).toHaveLength(1);
            expect(DOM.isTableNodeInTableDrawing(tableNode)).toBeTrue();
            expect(getElementStyleId(drawing5)).toBe(tableGridStyleId);

            const allCells = DOM.getTableCells(drawing5);
            const lastCell = allCells.last();

            const tableFormattingDef = $.Deferred();
            model.on('table:formatting:done', function () {
                if (lastCell.css('background-color') === 'transparent') { tableFormattingDef.resolve(); }
            });

            await tableFormattingDef;
            expect($(allCells[1]).css('background-color')).toBe("transparent");
            expect($(allCells[3]).css('background-color')).toBe("transparent");
            expect(lastCell.css('background-color')).toBe("transparent");
        });

        // set a new (complex) table style, before inserting a new row

        it('should set a complex table style to the selected table and format it correctly', async function () {

            // the ID of the table grid style
            var tableGridStyleId = '{F5AB1C69-6EDB-4FF4-983F-18BD219EF322}';

            model.setAttributes('table', {  styleId: tableGridStyleId }, {  clear: true });

            // finding the table inside the drawing
            tableNode = drawing5.find(DOM.TABLE_NODE_SELECTOR);
            expect(tableNode).toHaveLength(1);
            expect(DOM.isTableNodeInTableDrawing(tableNode)).toBeTrue();
            expect(getElementStyleId(drawing5)).toBe(tableGridStyleId);

            const allCells = DOM.getTableCells(drawing5);
            const lastCell = allCells.last();

            const tableFormattingDef = $.Deferred();
            model.on('table:formatting:done', function () {
                if (lastCell.css('background-color') !== 'transparent') { tableFormattingDef.resolve(); }
            });

            await tableFormattingDef;
            expect($(allCells[1]).css('background-color')).toBeCssColor('#9bbb59');
            expect($(allCells[3]).css('background-color')).toBeCssColor('#dee7d1');
            expect(lastCell.css('background-color')).toBeCssColor('#eff3e9');
        });

        // adding a row, so that new formatting is triggered
        it('should insert a row into the table and force an update of table formatting', async function () {

            // setting the cursor into the table
            selection.setTextSelection([0, 5, 0, 0, 0, 0]);  // setting the cursor into the table

            // checking the position
            var enclosingTable = selection.getEnclosingTable();
            var additionallySelectedDrawing = selection.getSelectedTextFrameDrawing();
            tableNode = additionallySelectedDrawing.find(DOM.TABLE_NODE_SELECTOR);

            expect(tableNode).toHaveLength(1);
            expect(enclosingTable).toBe(drawing5[0]);
            expect(enclosingTable).toBe(additionallySelectedDrawing[0]);

            // inserting a row behind the first row
            model.insertRow();

            expect(DOM.getTableRows(enclosingTable)).toHaveLength(4);
            const allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(8);

            const lastCell = allCells.last();
            var insertedCell = $(allCells[3]); // second row
            const oldLastCellColor = insertedCell.css('background-color');

            const tableFormattingDef = $.Deferred();
            model.on('table:formatting:done', function () {
                if (insertedCell.css('background-color') !== oldLastCellColor) { tableFormattingDef.resolve(); }
            });

            await tableFormattingDef;
            expect($(allCells[1]).css('background-color')).toBeCssColor('#9bbb59');
            expect($(allCells[3]).css('background-color')).toBeCssColor('#dee7d1');
            expect($(allCells[5]).css('background-color')).toBeCssColor('#eff3e9');
            expect(lastCell.css('background-color')).toBeCssColor('#dee7d1');
        });

        // undo must remove the inserted row again
        it('should insert a row into the table and force an update of table formatting #2', async function () {

            expect(DOM.getTableRows(drawing5)).toHaveLength(4);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(8);

            // removing the inserted row again
            await model.getUndoManager().undo();
            expect(DOM.getTableRows(drawing5)).toHaveLength(3);  // 3 rows left
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(6);  // 6 cells left
        });

        // adding a column
        it('should insert a column into the table', function () {

            // checking 3 rows and 6 cells before inserting the column
            expect(DOM.getTableRows(drawing5)).toHaveLength(3);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(6);

            // inserting a row behind the first row
            model.insertColumn();

            // checking 3 rows and 9 cells after inserting the column
            expect(DOM.getTableRows(drawing5)).toHaveLength(3);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(9);
        });

        // deleting a column
        it('should delete the selected column(s) in the table', async function () {

            // checking 3 rows and 9 cells before inserting the column
            expect(DOM.getTableRows(drawing5)).toHaveLength(3);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(9);

            // deleting the selected column
            await model.deleteColumns();

            // checking 3 rows and 6 cells after deleting the column
            expect(DOM.getTableRows(drawing5)).toHaveLength(3);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(6);
        });

        // undo must insert the deleted column again
        it('should insert a column via undo', async function () {

            // checking 3 rows and 6 cells before undo
            expect(DOM.getTableRows(drawing5)).toHaveLength(3);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(6);

            // inserting the removed column again
            await model.getUndoManager().undo();
            expect(DOM.getTableRows(drawing5)).toHaveLength(3);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(9);  // 9 cells in table
        });

        // insert a new row
        it('should insert a row into the table', function () {

            // checking 3 rows and 9 cells before inserting the row
            expect(DOM.getTableRows(drawing5)).toHaveLength(3);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(9);

            // inserting a row behind the first row (using convenience function)
            model.insertRow();

            // checking 4 rows and 12 cells after inserting the row
            expect(DOM.getTableRows(drawing5)).toHaveLength(4);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(12);
        });

        // checking horizontally merged table cells

        // merging two cells horizontally (not possible via GUI)
        it('should merge two selected cells horizontally', function () {

            // checking 4 rows and 12 cells before inserting the row
            expect(DOM.getTableRows(drawing5)).toHaveLength(4);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(12);

            // setting selection into two cells
            selection.setTextSelection([0, 5, 1, 0, 0, 0], [0, 5, 1, 1, 0, 0]);  // selecting two cells in the second row

            // merging the two selected cells (not possible via GUI)
            model.mergeCells();

            // checking 4 rows and 11 cells after merging the two selected cells
            expect(DOM.getTableRows(drawing5)).toHaveLength(4);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(11);
        });

        // inserting a new row with horizontally merged cells
        it('should insert a row with horizontally merged cells into the table', function () {

            // setting selection into merged cell
            selection.setTextSelection([0, 5, 1, 0, 0, 0]);  // setting selection in the merged cell

            // checking 4 rows and 11 cells before inserting the row
            expect(DOM.getTableRows(drawing5)).toHaveLength(4);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(11);

            // inserting a row behind the first row (using convenience function)
            model.insertRow();

            // checking 5 rows and 13 cells after inserting the row
            const allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(5);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(13);

            // counting the number of cells in each row
            expect($(allRows[0]).find('td')).toHaveLength(3);
            expect($(allRows[1]).find('td')).toHaveLength(2);
            expect($(allRows[2]).find('td')).toHaveLength(2);
            expect($(allRows[3]).find('td')).toHaveLength(3);
            expect($(allRows[4]).find('td')).toHaveLength(3);

            // checking the length of the first cells in row 2 and 3
            let allCellsInOneRow = $(allRows[1]).find('td'); // second row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();

            allCellsInOneRow = $(allRows[2]).find('td'); // third row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
        });

        // insertColumn, deleteColumn and undo for both

        // inserting a new column if selection is in a horizontally merged cell
        it('should insert a valid column handling horizontally merged cells correctly', function () {

            // setting selection into merged cell
            selection.setTextSelection([0, 5, 1, 0, 0, 0]);  // setting selection in the merged cell

            // checking 5 rows and 13 cells before inserting the column
            expect(DOM.getTableRows(drawing5)).toHaveLength(5);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(13);

            // inserting a column
            model.insertColumn();

            // checking 5 rows and 18 cells after inserting the column
            const allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(5);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(18);

            // counting the number of cells in each row
            expect($(allRows[0]).find('td')).toHaveLength(4);
            expect($(allRows[1]).find('td')).toHaveLength(3);
            expect($(allRows[2]).find('td')).toHaveLength(3);
            expect($(allRows[3]).find('td')).toHaveLength(4);
            expect($(allRows[4]).find('td')).toHaveLength(4);

            // checking the length of the cells in row 2 and 3
            // -> all new inserted cells must have a length of 1
            let allCellsInOneRow = $(allRows[1]).find('td'); // second row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[2])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[2])).toBeFalse();

            allCellsInOneRow = $(allRows[2]).find('td'); // third row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[2])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[2])).toBeFalse();
        });

        // undo must remove the inserted column with merged cells again
        it('should delete a column via undo', async function () {

            // checking 5 rows and 18 cells before undo
            expect(DOM.getTableRows(drawing5)).toHaveLength(5);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(18);

            // inserting the removed column again
            await model.getUndoManager().undo();

            // checking 5 rows and 13 cells after inserting the row
            const allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(5);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(13);

            // counting the number of cells in each row
            expect($(allRows[0]).find('td')).toHaveLength(3);
            expect($(allRows[1]).find('td')).toHaveLength(2);
            expect($(allRows[2]).find('td')).toHaveLength(2);
            expect($(allRows[3]).find('td')).toHaveLength(3);
            expect($(allRows[4]).find('td')).toHaveLength(3);

            // checking the length of the first cells in row 2 and 3
            let allCellsInOneRow = $(allRows[1]).find('td'); // second row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();

            allCellsInOneRow = $(allRows[2]).find('td'); // third row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
        });

        // inserting the column again via redo (for preparing tests of delete column)
        it('should insert a column via redo', async function () {

            // checking 5 rows and 18 cells before undo
            expect(DOM.getTableRows(drawing5)).toHaveLength(5);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(13);

            // inserting the removed column again
            await model.getUndoManager().redo();

            // checking 5 rows and 18 cells after inserting the column
            const allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(5);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(18);

            // counting the number of cells in each row
            expect($(allRows[0]).find('td')).toHaveLength(4);
            expect($(allRows[1]).find('td')).toHaveLength(3);
            expect($(allRows[2]).find('td')).toHaveLength(3);
            expect($(allRows[3]).find('td')).toHaveLength(4);
            expect($(allRows[4]).find('td')).toHaveLength(4);

            // checking the length of the cells in row 2 and 3
            // -> all new inserted cells must have a length of 1
            let allCellsInOneRow = $(allRows[1]).find('td'); // second row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[2])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[2])).toBeFalse();

            allCellsInOneRow = $(allRows[2]).find('td'); // third row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[2])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[2])).toBeFalse();
        });

        // Deleting a column, if table contains horizontally merged cells
        // First row contains 4 cells with length 1
        // Second and third row have 3 cells. The first cell has a lenght of 2.
        // 3 x 4 cells and 2 x 3 cells -> 18 cells

        // Setting selection into first cell of first row -> deleting column
        // -> Merged cells in row 2 and row 3 need to be reduced in length

        it('should delete the first column and reduce the length of horizontally merged cells', async function () {

            // setting selection into first cell of first row -> this cell is not horizontally merged
            selection.setTextSelection([0, 5, 0, 0, 0, 0]);  // setting selection in the not merged cell

            // checking 5 rows and 18 cells before deleting the column
            expect(DOM.getTableRows(drawing5)).toHaveLength(5);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(18);

            // deleting the first row
            await model.deleteColumns();

            // checking 5 rows and 15 cells after deleting the column
            // -> 3 cells were deleted, 2 cells were reduced in length
            const allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(5);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(15);

            // counting the number of cells in each row
            expect($(allRows[0]).find('td')).toHaveLength(3);
            expect($(allRows[1]).find('td')).toHaveLength(3);
            expect($(allRows[2]).find('td')).toHaveLength(3);
            expect($(allRows[3]).find('td')).toHaveLength(3);
            expect($(allRows[4]).find('td')).toHaveLength(3);

            // checking the length of the cells in row 2 and 3
            // -> all cells must have a length of 1
            let allCellsInOneRow = $(allRows[1]).find('td'); // second row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[2])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[2])).toBeFalse();

            allCellsInOneRow = $(allRows[2]).find('td'); // third row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[2])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[2])).toBeFalse();
        });

        // restoring the previous state via undo
        // -> inserting the column again
        it('should insert a column via undo and expand horizontally merged cells again', async function () {

            // checking 5 rows and 18 cells before undo
            expect(DOM.getTableRows(drawing5)).toHaveLength(5);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(15);

            // inserting the removed column again
            await model.getUndoManager().undo();

            // checking 5 rows and 18 cells after inserting the column
            // -> 3 cells were inserted, 2 cells were expanded in length
            const allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(5);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(18);

            // counting the number of cells in each row
            expect($(allRows[0]).find('td')).toHaveLength(4);
            expect($(allRows[1]).find('td')).toHaveLength(3);
            expect($(allRows[2]).find('td')).toHaveLength(3);
            expect($(allRows[3]).find('td')).toHaveLength(4);
            expect($(allRows[4]).find('td')).toHaveLength(4);

            // checking the length of the cells in row 2 and 3
            // -> all new inserted cells must have a length of 1
            let allCellsInOneRow = $(allRows[1]).find('td'); // second row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[2])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[2])).toBeFalse();

            allCellsInOneRow = $(allRows[2]).find('td'); // third row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[2])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[2])).toBeFalse();
        });

        // Deleting three rows using a selection range. Reducing the size of the table
        // is also useful for the following unit tests.
        it('should delete three of five rows using a selection range', async function () {

            // checking 5 rows and 18 cells before deleting the row (4, 3, 3, 4, 4 cells)
            let allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(5);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(18);

            // paragraph formatting is required for setting selection (no content inside paragraph)
            model.implParagraphChangedSync($(allRows[2]).find('div.p'));
            model.implParagraphChangedSync($(allRows[4]).find('div.p'));

            // setting selection into row 3 to row 5
            selection.setTextSelection([0, 5, 2, 0, 0, 0], [0, 5, 4, 0, 0, 0]);

            // deleting the first row
            await model.deleteRows();

            // checking 2 rows and 7 cells after deleting the rows
            allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(2);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(7);

            // counting the number of cells in each row
            expect($(allRows[0]).find('td')).toHaveLength(4);
            expect($(allRows[1]).find('td')).toHaveLength(3);

            // checking the length of the cells in row 2 and 3
            // -> all cells must have a length of 1
            const allCellsInOneRow = $(allRows[1]).find('td'); // second row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[2])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[2])).toBeFalse();
        });

        // Deleting a column, if table contains horizontally merged cells and the
        // selection is inside a horizontally merged cell.
        // First row contains 4 cells with length 1
        // Second row has 3 cells. The first cell has a lenght of 2
        // -> 7 cells

        // Setting selection into first cell of first row -> deleting column

        it('should delete the first column and remove horizontally merged cells completely', async function () {

            // checking 5 rows and 18 cells before deleting the column
            let allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(2);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(7);

            // setting selection into first cell of first row -> this cell is not horizontally merged
            model.implParagraphChangedSync($(allRows[1]).find('div.p'));
            selection.setTextSelection([0, 5, 1, 0, 0, 0]);  // setting selection in the merged cell in the second row

            // deleting the first row
            await model.deleteColumns();

            // checking 5 rows and 15 cells after deleting the column
            // -> 2 cells were deleted in 3 rows, 1 cell was deleted in two rows -> minus 8 cells
            allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(2);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(4);

            // counting the number of cells in each row
            expect($(allRows[0]).find('td')).toHaveLength(2);
            expect($(allRows[1]).find('td')).toHaveLength(2);

            // checking the length of the cells in row 2 and 3
            // -> all cells must have a length of 1
            const allCellsInOneRow = $(allRows[1]).find('td'); // second row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
        });

        // restoring the previous state via undo
        // -> inserting the column again
        it('should insert a column via undo and insert horizontally merged cells again', async function () {

            // checking 2 rows and 4 cells before undo
            let allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(2);
            let allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(4);

            // inserting the removed column again
            await model.getUndoManager().undo();

            // checking 2 rows and 7 cells after inserting the column
            // -> 2 cells with length 1 were inserted, 1 cells with length 2
            allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(2);
            allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(7);

            // counting the number of cells in each row
            expect($(allRows[0]).find('td')).toHaveLength(4);
            expect($(allRows[1]).find('td')).toHaveLength(3);

            // checking the length of the cells in row 2 and 3
            // -> all new inserted cells must have a length of 1
            const allCellsInOneRow = $(allRows[1]).find('td'); // second row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[2])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[2])).toBeFalse();
        });

        // Deleting several columns, so that the complete table is removed.
        // Selection is inside a horizontally merged cell and the following cells in the row.
        // First row contains 4 cells with length 1
        // Second row has 3 cells. The first cell has a lenght of 2.
        // -> 7 cells
        //
        // Setting selection into all three cells of the second row -> deleting column
        // -> The complete table is removed

        it('should delete the table by selecting three cells in a row (one is a horizontally merged cell)', async function () {

            // checking that the table drawing was deleted
            let allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(6);

            // checking 2 rows and 7 cells before deleting the column
            const allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(2);
            const allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(7);

            // setting selection into all three cell of second row
            model.implParagraphChangedSync($(allRows[1]).find('div.p'));
            selection.setTextSelection([0, 5, 1, 0, 0, 0], [0, 5, 1, 2, 0, 0]);  // setting selection in the three cells in the second row

            // deleting the first row
            await model.deleteColumns();
            // checking that the table drawing was deleted
            allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5); // one drawing was removed
        });

        // restoring the previous state via undo
        // -> inserting the table again including the horizontally merged cells
        it('should create the table and the horizontally merged cells via undo', async function () {

            // 5 drawings on the slide
            let allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(5); // one drawing was removed

            // inserting the removed column again
            await model.getUndoManager().undo();

            // 6 drawings on the slide
            allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(6);

            // getting the table drawing
            drawing5 = $(allDrawings[5]);

            // finding the table inside the drawing
            tableNode = drawing5.find(DOM.TABLE_NODE_SELECTOR);
            expect(tableNode).toHaveLength(1);
            expect(DOM.isTableNodeInTableDrawing(tableNode)).toBeTrue();

            // checking 2 rows and 7 cells after inserting the column
            // -> 6 cells with length 1 were inserted, 1 cell with length 2
            const allRows = DOM.getTableRows(drawing5);
            expect(allRows).toHaveLength(2);
            const allCells = DOM.getTableCells(drawing5);
            expect(allCells).toHaveLength(7);

            // counting the number of cells in each row
            expect($(allRows[0]).find('td')).toHaveLength(4);
            expect($(allRows[1]).find('td')).toHaveLength(3);

            // checking the length of the cells in row 2
            const allCellsInOneRow = $(allRows[1]).find('td'); // second row
            expect(getHorizontalCellLength(allCellsInOneRow[0])).toBe(2);
            expect(getHorizontalCellLength(allCellsInOneRow[1])).toBe(1);
            expect(getHorizontalCellLength(allCellsInOneRow[2])).toBe(1);
            expect(isHorizontallyMergedCell(allCellsInOneRow[0])).toBeTrue();
            expect(isHorizontallyMergedCell(allCellsInOneRow[1])).toBeFalse();
            expect(isHorizontallyMergedCell(allCellsInOneRow[2])).toBeFalse();
        });

        // TODO:
        // checking table styles with different themes
    });

    describe('method insertShapePreparation', function () {
        it('should exist', function () {
            expect(model).toRespondTo('insertShapePreparation');
        });

        it('should show guidlines', function () {
            var mouseDownEvent = new $.Event('mousedown', { startX: 0, startY: 0, pageX: 0, pageY: 0 });
            var contentRootNode = view.getContentRootNode();

            model.insertShapePreparation();
            expect(contentRootNode.children('.guide-line')).toHaveLength(2);
            expect(contentRootNode.children('.guide-line.v')).toHaveLength(1);
            expect(contentRootNode.children('.guide-line.h')).toHaveLength(1);

            contentRootNode.trigger(mouseDownEvent);
            expect(contentRootNode.children('.guide-line')).toHaveLength(4);
            expect(contentRootNode.children('.guide-line.v')).toHaveLength(2);
            expect(contentRootNode.children('.guide-line.h')).toHaveLength(2);

            model.insertShapePreparation(); // this cancels selection box and cleans up guidelines
            expect(contentRootNode.children('.guide-line')).toHaveLength(0);
        });

    });

    // handling drawing lines and connectors and arrows

    describe('method insertShape', function () {

        var connector_with_1_arrow = null;
        var connector_with_2_arrows = null;

        it('should exist', function () {
            expect(model).toRespondTo('insertShape');
        });

        // analyzing the current state
        it('should have 6 drawings on the first slide', function () {

            expect(model.getActiveSlideIndex()).toBe(0);
            expect(model.getActiveSlideId()).toBe(slide_1_id);

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide

            expect(allDrawings).toHaveLength(6); // 6 drawings on the slide

        });

        it('should insert one connector without arrow', function () {

            model.insertShape('straightConnector1', new Rectangle(1000, 2000, 4000, 500));

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide

            expect(allDrawings).toHaveLength(7); // one connector line inserted

            connector_with_1_arrow = allDrawings.last();
        });

        it('should insert one connector with arrow', function () {

            model.insertShape('straightConnector1_2', new Rectangle(1000, 4000, 4000, 500));

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide

            expect(allDrawings).toHaveLength(8); // one connector line with arrows inserted

            connector_with_2_arrows = allDrawings.last();
        });

        it('should insert an arrow with increased height for the canvas compared with the connector without arrows', function () {

            var canvasWith1Arrow = getCanvasNode(connector_with_1_arrow);
            var canvasWith2Arrows = getCanvasNode(connector_with_2_arrows);

            expect(canvasWith1Arrow).toHaveLength(1); // one canvas in each drawing
            expect(canvasWith2Arrows).toHaveLength(1); // one canvas in each drawing

            var canvasHeightWith1Arrow = canvasWith1Arrow.height();
            var canvasHeightWith2Arrows = canvasWith2Arrows.height();

            // the height of the canvas with arrow must be increased
            expect(canvasHeightWith1Arrow).toBeGreaterThan(0);
            expect(canvasHeightWith2Arrows).toBeGreaterThan(0);
        });

        // test if shapes are inserted with the correct fill style
        it('should insert a shape with no fill', function () {

            model.insertShape('leftBrace', new Rectangle(1000, 5000, 200, 200));

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide

            expect(allDrawings).toHaveLength(9); // leftBrace inserted

            var leftBrace = allDrawings.last();
            expect(getExplicitAttributes(leftBrace, 'fill', true)).toEqual({ type: 'none' }); // leftBrace inserted with no fill
            expect(getExplicitAttributes(leftBrace, 'drawing', true).name).toBe("leftBrace");
        });

        it('should insert a shape with fill', function () {

            model.insertShape('ellipse', new Rectangle(1000, 6000, 200, 200));

            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide

            expect(allDrawings).toHaveLength(10); // ellipse inserted

            var ellipse = allDrawings.last();
            expect(getExplicitAttributes(ellipse, 'fill', true).type).toBe("solid"); // ellipse inserted with fill
            expect(getExplicitAttributes(ellipse, 'drawing', true).name).toBe("ellipse");
        });
    });

    // handling custom placeholders on layout and master slides

    describe('method insertPlaceholderDrawing', function () {
        var options = { rectangle: { top: 0, left: 0, width: 500, height: 500 } };
        var options1 = { rectangle: { top: 100, left: 100, width: 500, height: 500 } };
        it('should exist', function () {
            expect(model).toRespondTo('insertPlaceholderDrawing');
        });

        it('should insert custom placeholder object', async function () {

            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'subTitle')).toHaveLength(1);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'ctrTitle')).toHaveLength(1);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'body')).toHaveLength(0);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'pic')).toHaveLength(0);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'tbl')).toHaveLength(0);

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.selectSlideView({ showMaster: true });
                model.setActiveSlideId(layoutId_1); // the slide was not yet formatted
            });

            expect(arg).toBe(layoutId_1);
            expect(model.getActiveSlideId()).toBe(layoutId_1); // asynchronous slide formatting has to be finished

            activeSlide = model.getSlideById(model.getActiveSlideId());
            expect(activeSlide).toHaveLength(1);
            expect(activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(2);

            // This test need to be simplified because of timing issues.
            // This slide is not yet formatted and activation seems to be a timing issue.

            model.insertPlaceholderDrawing('table', options);
            model.insertPlaceholderDrawing('picture', options);
            model.insertPlaceholderDrawing('content', options);
            model.insertPlaceholderDrawing('text', options);
            model.insertPlaceholderDrawing('textv', options1);

            expect(activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR)).toHaveLength(7);

            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'subTitle')).toHaveLength(1);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'ctrTitle')).toHaveLength(1);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'tbl')).toHaveLength(1);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'pic')).toHaveLength(1);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'body')).toHaveLength(3);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'body', 5)).toHaveLength(1);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'body', 6)).toHaveLength(1);

            const textvDrawing = drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'body', 6);
            const textvAttrs = getExplicitAttributeSet(textvDrawing, true);
            expect(textvAttrs).toBeObject();
            expect(textvAttrs.shape).toBeObject();
            expect(textvAttrs.shape.vert).toBe("eaVert");
        });

        it('should remove the new inserted empty place holder drawings', async function () {

            selection.setMultiDrawingSelectionByPosition([[0, 2], [0, 3], [0, 4], [0, 5], [0, 6]]);  // selecting the five new inserted place holder drawings

            expect(selection.isDrawingSelection()).toBeTrue();
            expect(selection.isMultiSelection()).toBeTrue();
            expect(selection.getMultiSelectionCount()).toBe(5);
            expect(selection.getListOfPositions()).toBe("(0,2),(0,3),(0,4),(0,5),(0,6)");

            expect(model.getActiveSlideId()).toBe(layoutId_1); // still on slide with ID layout_1

            // deleting the table drawing
            await model.deleteSelected({ deleteKey: true });

            // refreshing the affected drawing assigning drawings
            const allDrawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(allDrawings).toHaveLength(2); // the drawing was really removed

            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'subTitle')).toHaveLength(1);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'ctrTitle')).toHaveLength(1);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'tbl')).toHaveLength(0);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'pic')).toHaveLength(0);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(layoutId_1, 'body')).toHaveLength(0);
        });
    });

    // handling default placeholders on layout and master slides, like title and footer objects

    describe('method togglePlaceholderObjectOnSlide', function () {
        it('should exist', function () {
            expect(model).toRespondTo('togglePlaceholderObjectOnSlide');
        });

        it('should insert default placeholder object', async function () {

            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(masterId_1, 'title')).toHaveLength(0);
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(masterId_1, 'dt')).toHaveLength(0);

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.selectSlideView({ showMaster: true });
                model.setActiveSlideId(masterId_1);
            });

            expect(arg).toBe(masterId_1);
            expect(model.getActiveSlideId()).toBe(masterId_1); // asynchronous slide formatting has to be finished

            model.togglePlaceholderObjectOnSlide('title');
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(masterId_1, 'title')).toHaveLength(1);
            expect(model.hasPlaceholderObjectOnSlide('title')).toBeTrue();
            model.togglePlaceholderObjectOnSlide('title');
            expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(masterId_1, 'title')).toHaveLength(0);
            expect(model.hasPlaceholderObjectOnSlide('title')).toBeFalse();

            model.togglePlaceholderObjectOnSlide('footers');
            expect(model.hasPlaceholderObjectOnSlide('footers')).toBeTrue();
            _.each(model.getPresentationFooterTypes(), function (footerType) {
                expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(masterId_1, footerType)).toHaveLength(1);
            });

            model.togglePlaceholderObjectOnSlide('footers');
            expect(model.hasPlaceholderObjectOnSlide('footers')).toBeFalse();
            _.each(model.getPresentationFooterTypes(), function (footerType) {
                expect(drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(masterId_1, footerType)).toHaveLength(0);
            });

            model.togglePlaceholderObjectOnSlide('body');
            expect(model.hasPlaceholderObjectOnSlide('body')).toBeTrue();
            const bodyDrawing = drawingStyles.getAllPlaceHolderDrawingsOnSlideBySelector(masterId_1, 'body');
            expect(bodyDrawing).toHaveLength(1);
            expect(bodyDrawing.find('.p')).toHaveLength(PLACEHOLDER_TEXT_CONTENT.length);
        });
    });

    // Calculating a default size for a inserted shape, text frame, placeholder.
    describe('method getDefaultRectangleForInsert', function () {
        it('should exist', function () {
            expect(model).toRespondTo('getDefaultRectangleForInsert');
        });
        var defaultShapeSize = 2487;
        var halfDefaultShapeSize = 1244;

        it('should return the default height & width for a textframe', function () {
            var box = model.getDefaultRectangleForInsert({ type: 'textframe' });
            expect(box.width).toBe(10001);
            expect(box.height).toBe(2487);
        });

        it('should return the default height & width for a placeholder', function () {
            var box = model.getDefaultRectangleForInsert({ type: 'placeholder' });
            expect(box.width).toBe(10001);
            expect(box.height).toBe(10001);
        });

        it('should return the default height & width (a square) for a square shape (no format property defined)', function () {
            var box = model.getDefaultRectangleForInsert({ type: 'shape', presetShapeId: 'ellipse' });
            expect(box.width).toBe(defaultShapeSize);
            expect(box.height).toBe(defaultShapeSize);
        });

        it('should return the default height & width (width = height / 2) for a shape with format portrait', function () {
            var box = model.getDefaultRectangleForInsert({ type: 'shape', presetShapeId: 'leftRightArrow' });
            expect(box.width).toBe(defaultShapeSize);
            expect(box.height).toBe(halfDefaultShapeSize);
        });

        it('should return the default height & width (width / 2 = height) for a shape with format landscape', function () {
            var box = model.getDefaultRectangleForInsert({ type: 'shape', presetShapeId: 'upDownArrow' });
            expect(box.width).toBe(halfDefaultShapeSize);
            expect(box.height).toBe(defaultShapeSize);
        });

        it('should return the default height & width (a square) for a shape that has a format which is not supported (e.g. scale-vertical-double)', function () {
            var box = model.getDefaultRectangleForInsert({ type: 'shape', presetShapeId: 'bentConnector3_2' });
            expect(box.width).toBe(defaultShapeSize);
            expect(box.height).toBe(defaultShapeSize);
        });

    });

});

// ========================================================================

describe('Checking the size of the Canvas nodes inside the drawing', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        layoutId_1 = 'layout1',
        masterId_1 = 'master1',
        slide_1_id = 'slide_1', // the ID of the first slide in document view
        slide_2_id = 'slide_2', // the ID of the second slide in document view
        slide_3_id = 'slide_3', // the ID of the third slide in document view
        slide_4_id = 'slide_4', // the ID of the fourth slide in document view
        activeSlideId = null,
        activeSlide = null,
        drawings = null,
        canvasNode = null,
        drawingAttrs = null,
        tolerance = 2,
        canvasWidth = 0,
        canvasHeight = 0,
        drawingWidth = 0,
        drawingHeight = 0,
        drawing1Name = 'drawing1',
        drawing2Name = 'drawing2',
        drawing3Name = 'drawing3',
        drawing4Name = 'drawing4',
        drawing5Name = 'drawing5',
        drawing6Name = 'drawing6',
        drawing7Name = 'drawing7',
        drawing1WidthHmm = 10867,
        drawing1WidthPx = 411,
        canvas1WidthPx = 805,
        drawing1HeightHmm = 6405,
        drawing1HeightPx = 242,
        canvas1HeightPx = 284,
        drawing2WidthPx = 549,
        drawing2HeightPx = 190,
        canvas2WidthPx = drawing2WidthPx + 2, // minimal increased canvas in connector without line ends
        canvas2HeightPx = drawing2HeightPx + 2, // minimal increased canvas in connector without line ends
        drawing3WidthPx = 375,
        drawing3HeightPx = 216,
        canvas3WidthPx = drawing3WidthPx + 26, // more increased canvas because of line ends in connector
        canvas3HeightPx = drawing3HeightPx + 26, // more increased canvas because of line ends in connector
        drawing4WidthPx = 59,
        drawing4HeightPx = 81,
        canvas4WidthPx = drawing4WidthPx + 2, // minimal increased canvas in free from
        canvas4HeightPx = drawing4HeightPx + 2, // minimal increased canvas in free form
        drawing5WidthPx = 382,
        drawing5HeightPx = 222,
        canvas5WidthPx = drawing5WidthPx + 169, // 62, // increased canvas width in shape cloudCallout
        canvas5HeightPx = drawing5HeightPx + 253, // strongly increased canvas height in shape cloudCallout
        drawing6WidthPx = 472,
        drawing6HeightPx = 215,
        canvas6WidthPx = drawing6WidthPx + 2, // minimal increased canvas width in Bezier curve with default adjustment point
        canvas6HeightPx = drawing6HeightPx + 2, // minimal increased canvas height in Bezier curve with default adjustment point
        drawing7WidthPx = 472,
        drawing7HeightPx = 215,
        canvas7WidthPx = drawing7WidthPx + 232, // strongly increased canvas width in Bezier curve with default adjustment point
        canvas7HeightPx = drawing7HeightPx + 2, // minimal increased canvas height in Bezier curve with default adjustment point

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' }
                }
            },
            { name: 'insertMasterSlide', id: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },

            { name: 'insertSlide', start: [0], target: layoutId_1 },
            // connector with adjustment point outside of the drawing frame -> canvas must be much larger than drawing
            { name: 'insertDrawing', start: [0, 0], type: 'connector', attrs: { drawing: { name: drawing1Name, left: 5897, top: 3269, width: drawing1WidthHmm, height: drawing1HeightHmm }, geometry: { presetShape: 'bentConnector3', avList: { adj1: 185569 } }, character: { color: Color.TEXT1 }, line: { type: 'solid', color: Color.ACCENT1, width: 176, headEndType: 'triangle', tailEndType: 'triangle' }, fill: { type: 'none' } } },

            { name: 'insertSlide', start: [1], target: layoutId_1 },
            // connector without arrow line ends
            { name: 'insertDrawing', start: [1, 0], type: 'connector', attrs: { drawing: { name: drawing2Name, left: 4004, top: 3765, width: 14523, height: 5020 }, geometry: { presetShape: 'line', avList: {} }, character: { color: Color.TEXT1 }, line: { type: 'solid', color: Color.ACCENT1, width: 17 }, fill: { type: 'none' } } },
            // connector with arrow line ends
            { name: 'insertDrawing', start: [1, 1], type: 'connector', attrs: { drawing: { name: drawing3Name, left: 2480, top: 7769, width: 9921, height: 5708 }, geometry: { presetShape: 'straightConnector1', avList: {} }, character: { color: Color.TEXT1 }, line: { type: 'solid', color: Color.ACCENT1, width: 17, headEndType: 'triangle', tailEndType: 'triangle' }, fill: { type: 'none' } } },
            // free shape
            { name: 'insertDrawing', start: [1, 2], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: drawing4Name, left: 8845, top: 1403, width: 1554, height: 2155 }, geometry: { avList: {}, gdList: [{ name: 'connsiteX0', op: '*/', p0: '398032', p1: 'w', p2: '559397' }, {  name: 'connsiteY0', op: '*/', p0: '237310', p1: 'h', p2: '775667' }, {  name: 'connsiteX1', op: '*/', p0: '301214', p1: 'w', p2: '559397' }, {  name: 'connsiteY1', op: '*/', p0: '291098', p1: 'h', p2: '775667' }, {  name: 'connsiteX2', op: '*/', p0: '279698', p1: 'w', p2: '559397' }, {  name: 'connsiteY2', op: '*/', p0: '312613', p1: 'h', p2: '775667' }, {  name: 'connsiteX3', op: '*/', p0: '236668', p1: 'w', p2: '559397' }, {  name: 'connsiteY3', op: '*/', p0: '344886', p1: 'h', p2: '775667' }, {  name: 'connsiteX4', op: '*/', p0: '204395', p1: 'w', p2: '559397' }, {  name: 'connsiteY4', op: '*/', p0: '366401', p1: 'h', p2: '775667' }, { name: 'connsiteX5', op: '*/', p0: '129091', p1: 'w', p2: '559397' }, { name: 'connsiteY5', op: '*/', p0: '452463', p1: 'h', p2: '775667' }, {  name: 'connsiteX6', op: '*/', p0: '107576', p1: 'w', p2: '559397' }, {  name: 'connsiteY6', op: '*/', p0: '484736', p1: 'h', p2: '775667' }, {  name: 'connsiteX7', op: '*/', p0: '86061', p1: 'w', p2: '559397' }, {  name: 'connsiteY7', op: '*/', p0: '527766', p1: 'h', p2: '775667' }, {  name: 'connsiteX8', op: '*/', p0: '43030', p1: 'w', p2: '559397' }, { name: 'connsiteY8', op: '*/', p0: '570797', p1: 'h', p2: '775667' }, {  name:  'connsiteX9', op: '*/', p0: '10757', p1: 'w', p2: '559397' }, {  name: 'connsiteY9', op: '*/', p0: '646100', p1: 'h', p2: '775667' }, {  name: 'connsiteX10', op: '*/', p0: '0', p1: 'w', p2: '559397' }, {  name: 'connsiteY10', op: '*/', p0: '689131', p1: 'h', p2: '775667' }, { name: 'connsiteX11', op: '*/', p0: '10757', p1: 'w', p2: '559397' }, { name: 'connsiteY11', op: '*/', p0: '721404', p1: 'h', p2: '775667' }, { name: 'connsiteX12', op: '*/', p0: '75303', p1: 'w', p2: '559397' }, { name: 'connsiteY12', op: '*/', p0: '753677', p1: 'h', p2: '775667' }, { name: 'connsiteX13', op: '*/', p0: '107576', p1: 'w', p2: '559397' }, { name: 'connsiteY13', op: '*/', p0: '775192', p1: 'h', p2: '775667' }, { name: 'connsiteX14', op: '*/', p0: '387275', p1: 'w', p2: '559397' }, { name: 'connsiteY14', op: '*/', p0: '742919', p1: 'h', p2: '775667' }, { name: 'connsiteX15', op: '*/', p0: '419548', p1: 'w', p2: '559397' }, { name: 'connsiteY15', op: '*/', p0: '721404', p1: 'h', p2: '775667' }, { name: 'connsiteX16', op: '*/', p0: '473336', p1: 'w', p2: '559397' }, { name: 'connsiteY16', op: '*/', p0: '667616', p1: 'h', p2: '775667' }, { name: 'connsiteX17', op: '*/', p0: '505609', p1: 'w', p2: '559397' }, { name: 'connsiteY17', op: '*/', p0: '603070', p1: 'h', p2: '775667' }, { name: 'connsiteX18', op: '*/', p0: '516367', p1: 'w', p2: '559397' }, { name: 'connsiteY18', op: '*/', p0: '570797', p1: 'h', p2: '775667' }, { name: 'connsiteX19', op: '*/', p0: '527124', p1: 'w', p2: '559397' }, { name: 'connsiteY19', op: '*/', p0: '495493', p1: 'h', p2: '775667' }, { name: 'connsiteX20', op: '*/', p0: '537882', p1: 'w', p2: '559397' }, { name: 'connsiteY20', op: '*/', p0: '430947', p1: 'h', p2: '775667' }, { name: 'connsiteX21', op: '*/', p0: '559397', p1: 'w', p2: '559397' }, { name: 'connsiteY21', op: '*/', p0: '151248', p1: 'h', p2: '775667' }, { name: 'connsiteX22', op: '*/', p0: '527124', p1: 'w', p2: '559397' }, { name: 'connsiteY22', op: '*/', p0: '32914', p1: 'h', p2: '775667' }, { name: 'connsiteX23', op: '*/', p0: '494851', p1: 'w', p2: '559397' }, { name: 'connsiteY23', op: '*/', p0: '22157', p1: 'h', p2: '775667' }, { name: 'connsiteX24', op: '*/', p0: '473336', p1: 'w', p2: '559397' }, { name: 'connsiteY24', op: '*/', p0: '641', p1: 'h', p2: '775667' }, { name: 'connsiteX25', op: '*/', p0: '322729', p1: 'w', p2: '559397' }, { name: 'connsiteY25', op: '*/', p0: '22157', p1: 'h', p2: '775667' }, { name: 'connsiteX26', op: '*/', p0: '290456', p1: 'w', p2: '559397' }, { name: 'connsiteY26', op: '*/', p0: '32914', p1: 'h', p2: '775667' }, { name: 'connsiteX27', op: '*/', p0: '247425', p1: 'w', p2: '559397' }, { name: 'connsiteY27', op: '*/', p0: '54430', p1: 'h', p2: '775667' }, { name: 'connsiteX28', op: '*/', p0: '204395', p1: 'w', p2: '559397' }, { name: 'connsiteY28', op: '*/', p0: '118976', p1: 'h', p2: '775667' }, { name: 'connsiteX29', op: '*/', p0: '150607', p1: 'w', p2: '559397' }, { name: 'connsiteY29', op: '*/', p0: '162006', p1: 'h', p2: '775667' }], pathList: [{ fillMode: 'norm', width: 559397, height: 775667, commands: [{ c: 'moveTo', x: '398032', y: '237310' }, { c: 'cubicBezierTo', pts: [{ x: '365160', y: '253746' }, { x: '330262', y: '267860' }, { x: '301214', y: '291098' }] }, { c: 'cubicBezierTo', pts: [{ x: '293294', y: '297434' }, { x: '287490', y: '306120' }, { x: '279698', y: '312613' }] }, { c: 'cubicBezierTo', pts: [{ x: '265924', y: '324091' }, { x: '251258', y: '334465' }, { x: '236668', y: '344886' }] }, { c: 'cubicBezierTo', pts: [{ x: '226147', y: '352401' }, { x: '214211', y: '357987' }, { x: '204395', y: '366401' }] }, { c: 'cubicBezierTo', pts: [{ x: '169521', y: '396293' }, { x: '155119', y: '416024' }, { x: '129091', y: '452463' }] }, { c: 'cubicBezierTo', pts: [{ x: '121576', y: '462984' }, { x: '113991', y: '473510' }, { x: '107576', y: '484736' }] }, { c: 'cubicBezierTo', pts: [{ x: '99620', y: '498659' }, { x: '95683', y: '514937' }, { x: '86061', y: '527766' }] }, { c: 'cubicBezierTo', pts: [{ x: '73890', y: '543994' }, { x: '43030', y: '570797' }, { x: '43030', y: '570797' }] }, { c: 'cubicBezierTo', pts: [{ x: '12141', y: '694345' }, { x: '55335', y: '542082' }, { x: '10757', y: '646100' }] }, { c: 'cubicBezierTo', pts: [{ x: '4933', y: '659690' }, { x: '3586', y: '674787' }, { x: '0', y: '689131' }] }, { c: 'cubicBezierTo', pts: [{ x: '3586', y: '699889' }, { x: '3673', y: '712549' }, { x: '10757', y: '721404' }] }, { c: 'cubicBezierTo', pts: [{ x: '31309', y: '747094' }, { x: '49320', y: '740686' }, { x: '75303', y: '753677' }] }, { c: 'cubicBezierTo', pts: [{ x: '86867', y: '759459' }, { x: '96818', y: '768020' }, { x: '107576', y: '775192' }] }, { c: 'cubicBezierTo', pts: [{ x: '118320', y: '774655' }, { x: '326320', y: '783555' }, { x: '387275', y: '742919' }] }, { c: 'cubicBezierTo', pts: [{ x: '398033', y: '735747' }, { x: '409818', y: '729918' }, { x: '419548', y: '721404' }] }, { c: 'cubicBezierTo', pts: [{ x: '438630', y: '704707' }, { x: '473336', y: '667616' }, { x: '473336', y: '667616' }] }, { c: 'cubicBezierTo', pts: [{ x: '500377', y: '586497' }, { x: '463901', y: '686486' }, { x: '505609', y: '603070' }] }, { c: 'cubicBezierTo', pts: [{ x: '510680', y: '592928' }, { x: '512781', y: '581555' }, { x: '516367', y: '570797' }] }, { c: 'cubicBezierTo', pts: [{ x: '519953', y: '545696' }, { x: '523268', y: '520554' }, { x: '527124', y: '495493' }] }, { c: 'cubicBezierTo', pts: [{ x: '530441', y: '473935' }, { x: '535176', y: '452591' }, { x: '537882', y: '430947' }] }, { c: 'cubicBezierTo', pts: [{ x: '550242', y: '332074' }, { x: '552962', y: '254210' }, { x: '559397', y: '151248' }] }, { c: 'cubicBezierTo', pts: [{ x: '555199', y: '117664' }, { x: '561026', y: '60035' }, { x: '527124', y: '32914' }] }, { c: 'cubicBezierTo', pts: [{ x: '518269', y: '25830' }, { x: '505609', y: '25743' }, { x: '494851', y: '22157' }] }, { c: 'cubicBezierTo', pts: [{ x: '487679', y: '14985' }, { x: '483443', y: '1483' }, { x: '473336', y: '641' }] }, { c: 'cubicBezierTo', pts: [{ x: '430191', y: '-2955' }, { x: '368251', y: '9151' }, { x: '322729', y: '22157' }] }, { c: 'cubicBezierTo', pts: [{ x: '311826', y: '25272' }, { x: '300879', y: '28447' }, { x: '290456', y: '32914' }] }, { c: 'cubicBezierTo', pts: [{ x: '275716', y: '39231' }, { x: '261769', y: '47258' }, { x: '247425', y: '54430' }] }, { c: 'cubicBezierTo', pts: [{ x: '227869', y: '93542' }, { x: '233141', y: '94336' }, { x: '204395', y: '118976' }] }, { c: 'cubicBezierTo', pts: [{ x: '186962', y: '133919' }, { x: '150607', y: '162006' }, { x: '150607', y: '162006' }] }] }], textRect: { l: 'l', t: 't', r: 'r', b: 'b' } }, character: { color: Color.LIGHT1 }, line: { type: 'solid', color: transformOpColor(Color.ACCENT1, { shade: 50000 }), width: 35.28 }, fill: { type: 'none', color: Color.ACCENT1 } } },
            { name: 'insertParagraph', start: [1, 2, 0], attrs: { paragraph: { alignment: 'center' }, character: { language: 'de-DE' } } },

            { name: 'insertSlide', start: [2], target: layoutId_1 },
            // a cloud as speech bubble -> canvas must be much larger than drawing (presetShape: 'cloudCallout')
            { name: 'insertDrawing',  start: [2, 0], type: 'shape', attrs: { shape: { anchor: 'centered' }, drawing: { name: drawing5Name, left: 5919, top: 4042, width: 10116, height: 5875 }, geometry: { presetShape: 'cloudCallout', avList: { adj1: -39070, adj2: 126667 } }, character: { color: Color.LIGHT1 }, line: { type: 'solid', color: transformOpColor(Color.ACCENT1, { shade: 50000 }), width: 35 }, fill: { type: 'solid', color: Color.ACCENT1 } } },
            { name: 'insertParagraph',  start: [2, 0, 0], attrs: { paragraph: { alignment: 'center' }, character: { language: 'de-DE' } } },

            { name: 'insertSlide', start: [3], target: layoutId_1 },
            // Bezier curve with default adjustment point
            { name: 'insertDrawing', start: [3, 0], type: 'connector', attrs: { drawing: { name: drawing6Name, left: 6096, top: 2032, width: 12491, height: 5678 }, geometry: { presetShape: 'curvedConnector3', avList: {} }, character: { color: Color.TEXT1 }, line: { type: 'solid', color: Color.ACCENT1, width: 17 }, fill: { type: 'none' } } },
            // Bezier curve with adjustment point outside the drawing
            { name: 'insertDrawing', start: [3, 1], type: 'connector', attrs: { drawing: { name: drawing7Name, left: 6096, top: 9926, width: 12491, height: 5678 }, geometry: { presetShape: 'curvedConnector3', avList: { adj1: 148804 } }, character: { color: Color.TEXT1 }, line: { type: 'solid', color: Color.ACCENT1, width: 17 }, fill: { type: 'none' } } }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        app.isOperationsBlockActive = function () {};
        model = app.getModel();
    });

    ///////////////////////////////////////////////////////////////////
    ///////////////////// tests for normal slides /////////////////////
    ///////////////////////////////////////////////////////////////////

    describe('method getActiveSlideId', function () {

        it('should exist', function () {
            expect(model).toRespondTo('getActiveSlideId');
        });

        it('should have the first slide activated', function () {
            activeSlideId = model.getActiveSlideId();
            expect(activeSlideId).toBe(slide_1_id);
        });

        it('should find one drawing on the active slide', function () {
            activeSlide = model.getSlideById(activeSlideId);
            expect(activeSlide).toHaveLength(1);
            drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(1);
        });

        it('drawing should have one canvas inside', function () {
            canvasNode = getCanvasNode(drawings);
            expect(canvasNode).toHaveLength(1);
        });

        it('drawing should have the correct name', function () {
            drawingAttrs = getExplicitAttributes(drawings, 'drawing', true);
            expect(drawingAttrs.name).toBe(drawing1Name);
        });

        it('drawing should have the specified width in hmm', function () {
            drawingAttrs = getExplicitAttributes(drawings, 'drawing', true);
            expect(drawingAttrs.width).toBe(drawing1WidthHmm);
        });

        it('drawing should have the specified width and height in px', function () {
            drawingAttrs = getExplicitAttributes(drawings, 'drawing', true);
            expect(convertHmmToLength(drawingAttrs.width, 'px', 1)).toBe(drawing1WidthPx);
            expect(convertHmmToLength(drawingAttrs.height, 'px', 1)).toBe(drawing1HeightPx);
        });

        it('drawing should have the specified width and height in px as style attribute', function () {
            drawingWidth = drawings.width();
            drawingHeight = drawings.height();
            drawingAttrs = getExplicitAttributes(drawings, 'drawing', true);
            expect(convertHmmToLength(drawingAttrs.width, 'px', 1)).toBeInInterval(drawingWidth - tolerance, drawingWidth + tolerance);
            expect(convertHmmToLength(drawingAttrs.height, 'px', 1)).toBeInInterval(drawingHeight - tolerance, drawingHeight + tolerance);
        });

        it('should have an increased width of the canvas compared with its drawing', function () {
            canvasWidth = canvasNode.width();
            canvasHeight = canvasNode.height();
            expect(canvasWidth).toBeGreaterThan(drawingWidth);
            expect(canvasHeight).toBeGreaterThan(drawingHeight);
            expect(canvasWidth).toBeInInterval(canvas1WidthPx - tolerance, canvas1WidthPx + tolerance);
            expect(canvasHeight).toBeInInterval(canvas1HeightPx - tolerance, canvas1HeightPx + tolerance);
        });

        it('should change to the slide 2 with the specified ID', async function () {

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.setActiveSlideId(slide_2_id); // activating the second slide
            });

            expect(arg).toBe(slide_2_id);
            expect(model.getActiveSlideIndex()).toBe(1); // asynchronous slide formatting has to be finished
            expect(model.getActiveSlideId()).toBe(slide_2_id); // asynchronous slide formatting has to be finished

            activeSlide = model.getSlideById(slide_2_id);
            drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(drawings).toHaveLength(3);
        });

        it('drawings on slide 2 should have the correct name', function () {
            var drawing2Attrs = getExplicitAttributes(drawings[0], 'drawing', true);
            expect(drawing2Attrs.name).toBe(drawing2Name);
            var drawing3Attrs = getExplicitAttributes(drawings[1], 'drawing', true);
            expect(drawing3Attrs.name).toBe(drawing3Name);
            var drawing4Attrs = getExplicitAttributes(drawings[2], 'drawing', true);
            expect(drawing4Attrs.name).toBe(drawing4Name);
        });

        it('connector without line ends should have only a slightly increased canvas', function () {

            // first drawing on slide 2: connector without arrow line ends
            const drawing = $(drawings[0]);
            canvasNode = getCanvasNode(drawing);
            expect(canvasNode).toHaveLength(1);

            drawingWidth = drawing.width();
            drawingHeight = drawing.height();
            canvasWidth = canvasNode.width();
            canvasHeight = canvasNode.height();

            expect(drawingWidth).toBeInInterval(drawing2WidthPx - tolerance, drawing2WidthPx + tolerance);
            expect(drawingHeight).toBeInInterval(drawing2HeightPx - tolerance, drawing2HeightPx + tolerance);
            expect(canvasWidth).toBeInInterval(canvas2WidthPx - tolerance, canvas2WidthPx + tolerance);
            expect(canvasHeight).toBeInInterval(canvas2HeightPx - tolerance, canvas2HeightPx + tolerance);

        });

        it('connector with line ends should have only an increased canvas', function () {

            // second drawing on slide 2: connector with arrow line ends
            const drawing = $(drawings[1]);
            canvasNode = getCanvasNode(drawing);
            expect(canvasNode).toHaveLength(1);

            drawingWidth = drawing.width();
            drawingHeight = drawing.height();
            canvasWidth = canvasNode.width();
            canvasHeight = canvasNode.height();

            expect(drawingWidth).toBeInInterval(drawing3WidthPx - tolerance, drawing3WidthPx + tolerance);
            expect(drawingHeight).toBeInInterval(drawing3HeightPx - tolerance, drawing3HeightPx + tolerance);
            expect(canvasWidth).toBeInInterval(canvas3WidthPx - tolerance, canvas3WidthPx + tolerance);
            expect(canvasHeight).toBeInInterval(canvas3HeightPx - tolerance, canvas3HeightPx + tolerance);

        });

        it('free form shape without line ends should have only a slightly increased canvas', function () {

            // third drawing on slide 2: free shape
            const drawing = $(drawings[2]);
            canvasNode = getCanvasNode(drawing);
            expect(canvasNode).toHaveLength(1);

            drawingWidth = drawing.width();
            drawingHeight = drawing.height();
            canvasWidth = canvasNode.width();
            canvasHeight = canvasNode.height();

            expect(drawingWidth).toBeInInterval(drawing4WidthPx - tolerance, drawing4WidthPx + tolerance);
            expect(drawingHeight).toBeInInterval(drawing4HeightPx - tolerance, drawing4HeightPx + tolerance);
            expect(canvasWidth).toBeInInterval(canvas4WidthPx - tolerance, canvas4WidthPx + tolerance);
            expect(canvasHeight).toBeInInterval(canvas4HeightPx - tolerance, canvas4HeightPx + tolerance);

        });

        it('should change to the slide 3 with the specified ID', async function () {

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.setActiveSlideId(slide_3_id); // activating the second slide
            });

            expect(arg).toBe(slide_3_id);
            expect(model.getActiveSlideIndex()).toBe(2); // asynchronous slide formatting has to be finished
            expect(model.getActiveSlideId()).toBe(slide_3_id); // asynchronous slide formatting has to be finished

            activeSlide = model.getSlideById(slide_3_id);
            drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(drawings).toHaveLength(1);
        });

        it('drawings on slide 3 should have the correct name', function () {
            var drawing5Attrs = getExplicitAttributes(drawings[0], 'drawing', true);
            expect(drawing5Attrs.name).toBe(drawing5Name);
        });

        it('shape "cloudCallout" should have an increased canvas', function () {

            // first drawing on slide 3: shape "cloudCallout"
            const drawing = $(drawings[0]);
            canvasNode = getCanvasNode(drawing);
            expect(canvasNode).toHaveLength(1);

            drawingWidth = drawing.width();
            drawingHeight = drawing.height();
            canvasWidth = canvasNode.width();
            canvasHeight = canvasNode.height();

            expect(drawingWidth).toBeInInterval(drawing5WidthPx - tolerance, drawing5WidthPx + tolerance);
            expect(drawingHeight).toBeInInterval(drawing5HeightPx - tolerance, drawing5HeightPx + tolerance);
            expect(canvasWidth).toBeInInterval(canvas5WidthPx - tolerance, canvas5WidthPx + tolerance);
            expect(canvasHeight).toBeInInterval(canvas5HeightPx - tolerance, canvas5HeightPx + tolerance);

        });

        it('should change to the slide 4 with the specified ID', async function () {

            const [arg] = await waitForEvent(model, 'change:activeslide:done', () => {
                model.setActiveSlideId(slide_4_id); // activating the second slide
            });

            expect(arg).toBe(slide_4_id);
            expect(model.getActiveSlideIndex()).toBe(3); // asynchronous slide formatting has to be finished
            expect(model.getActiveSlideId()).toBe(slide_4_id); // asynchronous slide formatting has to be finished

            activeSlide = model.getSlideById(slide_4_id);
            drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);  // getting all drawings on slide
            expect(drawings).toHaveLength(2);
        });

        it('drawings on slide 3 should have the correct name #2', function () {
            var drawing6Attrs = getExplicitAttributes(drawings[0], 'drawing', true);
            expect(drawing6Attrs.name).toBe(drawing6Name);
            var drawing7Attrs = getExplicitAttributes(drawings[1], 'drawing', true);
            expect(drawing7Attrs.name).toBe(drawing7Name);
        });

        // Bezier curve with default adjustment point
        it('Bezier curve with default adjustment point should have only a slightly increased canvas', function () {

            // first drawing on slide 2: connector without arrow line ends
            const drawing = $(drawings[0]);
            canvasNode = getCanvasNode(drawing);
            expect(canvasNode).toHaveLength(1);

            drawingWidth = drawing.width();
            drawingHeight = drawing.height();
            canvasWidth = canvasNode.width();
            canvasHeight = canvasNode.height();

            expect(drawingWidth).toBeInInterval(drawing6WidthPx - tolerance, drawing6WidthPx + tolerance);
            expect(drawingHeight).toBeInInterval(drawing6HeightPx - tolerance, drawing6HeightPx + tolerance);
            expect(canvasWidth).toBeInInterval(canvas6WidthPx - tolerance, canvas6WidthPx + tolerance);
            expect(canvasHeight).toBeInInterval(canvas6HeightPx - tolerance, canvas6HeightPx + tolerance);

        });

        // Bezier curve with adjustment point outside the drawing
        it('Bezier curve with adjustment point right of drawing should have a strongly increased canvas width', function () {

            // second drawing on slide 2: connector with arrow line ends
            const drawing = $(drawings[1]);
            canvasNode = getCanvasNode(drawing);
            expect(canvasNode).toHaveLength(1);

            drawingWidth = drawing.width();
            drawingHeight = drawing.height();
            canvasWidth = canvasNode.width();
            canvasHeight = canvasNode.height();

            expect(drawingWidth).toBeInInterval(drawing7WidthPx - tolerance, drawing7WidthPx + tolerance);
            expect(drawingHeight).toBeInInterval(drawing7HeightPx - tolerance, drawing7HeightPx + tolerance);
            expect(canvasWidth).toBeInInterval(canvas7WidthPx - tolerance, canvas7WidthPx + tolerance);
            expect(canvasHeight).toBeInInterval(canvas7HeightPx - tolerance, canvas7HeightPx + tolerance);

        });

    });

});

// ========================================================================

describe('Checking the existence of an expansion element inside a drawing to simplify drawing selection', function () {

    // private helpers ----------------------------------------------------

    var model = null,
        layoutId_1 = 'layout1',
        masterId_1 = 'master1',
        slide_1_id = 'slide_1', // the ID of the first slide in document view
        activeSlideId = null,
        activeSlide = null,
        drawings = null,
        drawing1 = null,
        drawing2 = null,
        drawing3 = null,
        drawing4 = null,
        drawing5 = null,
        canvasNode1 = null,
        canvasNode2 = null,
        canvasNode3 = null,
        canvasNode4 = null,
        canvasNode5 = null,
        expansionNode1 = null,
        expansionNode3 = null,
        expansionNode5 = null,
        drawingAttrs = null,
        drawing1Name = 'drawing1',
        drawing2Name = 'drawing2',
        drawing3Name = 'drawing3',
        drawing4Name = 'drawing4',
        drawing5Name = 'drawing5',
        tolerance = 2,
        drawingWidth = 0,
        drawingHeight = 0,
        drawing1WidthHmm = 2000,
        drawing1HeightHmm = 26,
        drawing2WidthHmm = 2000,
        drawing2HeightHmm = 500,
        drawing3WidthHmm = 1,
        drawing3HeightHmm = 2000,
        drawing4WidthHmm = 3400,
        drawing4HeightHmm = 2600,
        drawing5WidthHmm = 4000,
        drawing5HeightHmm = 2600,

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: {
                    page: { width: 33866, height: 19050, orientation: 'landscape' }
                }
            },
            { name: 'insertMasterSlide', id: masterId_1 },
            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },

            { name: 'insertSlide', start: [0], target: layoutId_1 },

            // horizontal line
            { name: 'insertDrawing', type: 'connector', start: [0, 0], attrs: { drawing: { name: drawing1Name, left: 4882, top: 12522, width: drawing1WidthHmm, height: drawing1HeightHmm }, geometry: { presetShape: 'line' }, character: { color: Color.LIGHT1 }, line: { type: 'solid', color: transformOpColor(Color.ACCENT1, { shade: 50000 }), width: 26 }, fill: { type: 'none' } } },
            // horizontal line with slight downward slope
            { name: 'insertDrawing', type: 'connector', start: [0, 1], attrs: { drawing: { name: drawing2Name, left: 4764, top: 15355, width: drawing2WidthHmm, height: drawing2HeightHmm }, geometry: { presetShape: 'line' }, character: { color: Color.LIGHT1 }, line: { type: 'solid', color: transformOpColor(Color.ACCENT1, { shade: 50000 }), width: 26 }, fill: { type: 'none' } } },
            // vertical line
            { name: 'insertDrawing', type: 'connector', start: [0, 2], attrs: { drawing: { name: drawing3Name, left: 2461, top: 4990, width: drawing3WidthHmm, height: drawing3HeightHmm }, geometry: { presetShape: 'line' }, character: { color: Color.LIGHT1 }, line: { type: 'solid', color: transformOpColor(Color.ACCENT1, { shade: 50000 }), width: 26 }, fill: { type: 'none' } } },
            // connector with canvas node complete inside drawing
            { name: 'insertDrawing', type: 'connector', start: [0, 3], attrs: { drawing: { name: drawing4Name, left: 6564, top: 5666, width: drawing4WidthHmm, height: drawing4HeightHmm }, geometry: { presetShape: 'bentConnector3', avList: { adj1: 25959 } }, character: { color: Color.TEXT1 }, line: { type: 'solid', color: transformOpColor(Color.ACCENT1, { shade: 95000 }, { satMod: 105000 }), width: 176.39, headEndType: 'none', tailEndType: 'none' }, fill: { type: 'none' } } },
            // connector with canvas node, that needs to be larger than its drawing node
            { name: 'insertDrawing', type: 'connector', start: [0, 4], attrs: { drawing: { name: drawing5Name, left: 15420, top: 5713, width: drawing5WidthHmm, height: drawing5HeightHmm }, geometry: { presetShape: 'bentConnector3', avList: { adj1: -23710 } }, character: { color: Color.TEXT1 }, line: { type: 'solid', color: transformOpColor(Color.ACCENT1, { shade: 95000 }, { satMod: 105000 }), width: 176.39, headEndType: 'none', tailEndType: 'none' }, fill: { type: 'none' } } }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        app.isOperationsBlockActive = function () {};
        model = app.getModel();
    });

    ///////////////////////////////////////////////////////////////////
    ///////////////////// tests for normal slides /////////////////////
    ///////////////////////////////////////////////////////////////////

    describe('method getActiveSlideId', function () {

        it('should exist', function () {
            expect(model).toRespondTo('getActiveSlideId');
        });

        it('should have the first slide activated', function () {
            activeSlideId = model.getActiveSlideId();
            expect(activeSlideId).toBe(slide_1_id);
        });

        it('should find 5 drawings on the active slide', function () {
            activeSlide = model.getSlideById(activeSlideId);
            expect(activeSlide).toHaveLength(1);
            drawings = activeSlide.children(DOM.ABSOLUTE_DRAWING_SELECTOR);
            expect(drawings).toHaveLength(5);
            drawing1 = $(drawings[0]);
            drawing2 = $(drawings[1]);
            drawing3 = $(drawings[2]);
            drawing4 = $(drawings[3]);
            drawing5 = $(drawings[4]);
        });

        it('every drawing should have one canvas inside', function () {
            canvasNode1 = getCanvasNode(drawing1);
            expect(canvasNode1).toHaveLength(1);
            canvasNode2 = getCanvasNode(drawing2);
            expect(canvasNode2).toHaveLength(1);
            canvasNode3 = getCanvasNode(drawing3);
            expect(canvasNode3).toHaveLength(1);
            canvasNode4 = getCanvasNode(drawing4);
            expect(canvasNode4).toHaveLength(1);
            canvasNode5 = getCanvasNode(drawing5);
            expect(canvasNode5).toHaveLength(1);
        });

        it('drawings should have the correct names', function () {
            drawingAttrs = getExplicitAttributes(drawing1, 'drawing', true);
            expect(drawingAttrs.name).toBe(drawing1Name);
            drawingAttrs = getExplicitAttributes(drawing2, 'drawing', true);
            expect(drawingAttrs.name).toBe(drawing2Name);
            drawingAttrs = getExplicitAttributes(drawing3, 'drawing', true);
            expect(drawingAttrs.name).toBe(drawing3Name);
            drawingAttrs = getExplicitAttributes(drawing4, 'drawing', true);
            expect(drawingAttrs.name).toBe(drawing4Name);
            drawingAttrs = getExplicitAttributes(drawing5, 'drawing', true);
            expect(drawingAttrs.name).toBe(drawing5Name);
        });

        it('drawings should have the specified width and height in hmm', function () {
            drawingAttrs = getExplicitAttributes(drawing1, 'drawing', true);
            expect(drawingAttrs.width).toBe(drawing1WidthHmm);
            expect(drawingAttrs.height).toBe(drawing1HeightHmm);
            drawingAttrs = getExplicitAttributes(drawing2, 'drawing', true);
            expect(drawingAttrs.width).toBe(drawing2WidthHmm);
            expect(drawingAttrs.height).toBe(drawing2HeightHmm);
            drawingAttrs = getExplicitAttributes(drawing3, 'drawing', true);
            expect(drawingAttrs.width).toBe(drawing3WidthHmm);
            expect(drawingAttrs.height).toBe(drawing3HeightHmm);
            drawingAttrs = getExplicitAttributes(drawing4, 'drawing', true);
            expect(drawingAttrs.width).toBe(drawing4WidthHmm);
            expect(drawingAttrs.height).toBe(drawing4HeightHmm);
            drawingAttrs = getExplicitAttributes(drawing5, 'drawing', true);
            expect(drawingAttrs.width).toBe(drawing5WidthHmm);
            expect(drawingAttrs.height).toBe(drawing5HeightHmm);
        });

        it('drawings should have the specified width and height in px as style attribute', function () {
            drawingWidth = drawing1.width();
            drawingHeight = drawing1.height();
            expect(convertHmmToLength(drawing1WidthHmm, 'px', 1)).toBeInInterval(drawingWidth - tolerance, drawingWidth + tolerance);
            expect(convertHmmToLength(drawing1HeightHmm, 'px', 1)).toBeInInterval(drawingHeight - tolerance, drawingHeight + tolerance);
            drawingWidth = drawing2.width();
            drawingHeight = drawing2.height();
            expect(convertHmmToLength(drawing2WidthHmm, 'px', 1)).toBeInInterval(drawingWidth - tolerance, drawingWidth + tolerance);
            expect(convertHmmToLength(drawing2HeightHmm, 'px', 1)).toBeInInterval(drawingHeight - tolerance, drawingHeight + tolerance);
            drawingWidth = drawing3.width();
            drawingHeight = drawing3.height();
            expect(convertHmmToLength(drawing3WidthHmm, 'px', 1)).toBeInInterval(drawingWidth - tolerance, drawingWidth + tolerance);
            expect(convertHmmToLength(drawing3HeightHmm, 'px', 1)).toBeInInterval(drawingHeight - tolerance, drawingHeight + tolerance);
            drawingWidth = drawing4.width();
            drawingHeight = drawing4.height();
            expect(convertHmmToLength(drawing4WidthHmm, 'px', 1)).toBeInInterval(drawingWidth - tolerance, drawingWidth + tolerance);
            expect(convertHmmToLength(drawing4HeightHmm, 'px', 1)).toBeInInterval(drawingHeight - tolerance, drawingHeight + tolerance);
            drawingWidth = drawing5.width();
            drawingHeight = drawing5.height();
            expect(convertHmmToLength(drawing5WidthHmm, 'px', 1)).toBeInInterval(drawingWidth - tolerance, drawingWidth + tolerance);
            expect(convertHmmToLength(drawing5HeightHmm, 'px', 1)).toBeInInterval(drawingHeight - tolerance, drawingHeight + tolerance);
        });

        it('should have at specified drawings a child node with increase size for easier selection', function () {
            expect(drawing1.children(CANVAS_EXPANSION_SELECTOR)).toHaveLength(1); // expansion node for horizontal line
            expect(drawing2.children(CANVAS_EXPANSION_SELECTOR)).toHaveLength(0); // NO expansion node for horizontal line with slight downward slope
            expect(drawing3.children(CANVAS_EXPANSION_SELECTOR)).toHaveLength(1); // expansion node for vertical line
            expect(drawing4.children(CANVAS_EXPANSION_SELECTOR)).toHaveLength(0); // NO expansion node for connector with canvas fully inside drawing
            expect(drawing5.children(CANVAS_EXPANSION_SELECTOR)).toHaveLength(1); // expansion node for connector with canvas larger than the drawing
            expansionNode1 = drawing1.children(CANVAS_EXPANSION_SELECTOR);
            expansionNode3 = drawing3.children(CANVAS_EXPANSION_SELECTOR);
            expansionNode5 = drawing5.children(CANVAS_EXPANSION_SELECTOR);
        });

        it('should have an increased size for the expansion node compared with its drawing', function () {
            expect(expansionNode1.width()).toBeGreaterThan(drawing1.width());
            expect(expansionNode1.height()).toBeGreaterThan(drawing1.height());
            expect(expansionNode3.width()).toBeGreaterThan(drawing3.width());
            expect(expansionNode3.height()).toBeGreaterThan(drawing3.height());
            expect(expansionNode5.width()).toBeGreaterThan(drawing5.width());
            expect(expansionNode5.height()).toBeLessThanOrEqual(drawing5.height()); // no expansion in vertical direction required!
        });

        it('should have an expansion node shifted to the left and top, if required', function () {
            expect(parseInt(expansionNode1.css('left'), 10)).toBeLessThan(0);
            expect(parseInt(expansionNode1.css('top'), 10)).toBeLessThan(0);
            expect(parseInt(expansionNode3.css('left'), 10)).toBeLessThan(0);
            expect(parseInt(expansionNode3.css('top'), 10)).toBeLessThan(0);
            expect(parseInt(expansionNode5.css('left'), 10)).toBeLessThan(0);
            expect(parseInt(expansionNode5.css('top'), 10)).toBeGreaterThanOrEqual(0); // no expansion in vertical direction required!
        });

        it('should have an small increase of size in every direction for horizontal or vertical line', function () {
            var lineIncrease = 14;
            expect(expansionNode1.width() - drawing1.width()).toBeInInterval(lineIncrease - tolerance, lineIncrease + tolerance);
            expect(expansionNode1.height() - drawing1.height()).toBeInInterval(lineIncrease - tolerance, lineIncrease + tolerance);
            expect(expansionNode3.width() - drawing3.width()).toBeInInterval(lineIncrease - tolerance, lineIncrease + tolerance);
            expect(expansionNode3.height() - drawing3.height()).toBeInInterval(lineIncrease - tolerance, lineIncrease + tolerance);
        });

        it('should have a larger increase of width and no increase of height for connector with canvas partly outside the drawing', function () {
            var horzIncrease = 111;
            var vertIncrease = 0;
            expect(expansionNode5.width() - drawing1.width()).toBeInInterval(horzIncrease - tolerance, horzIncrease + tolerance);
            expect(expansionNode5.height() - drawing5.height()).toBe(vertIncrease); // no expansion in vertical direction required!
        });

    });

});
