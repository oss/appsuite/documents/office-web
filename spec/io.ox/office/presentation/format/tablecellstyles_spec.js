/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';

import { Color } from '@/io.ox/office/editframework/utils/color';
import TableCellStyles from '@/io.ox/office/presentation/format/tablecellstyles';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';
import * as Position from '@/io.ox/office/textframework/utils/position';

import { createPresentationApp } from '~/presentation/apphelper';

// class TableCellStyles ==================================================

describe('Presentation format class TableCellStyles', function () {

    // private helpers ----------------------------------------------------
    var positions    = ['top', 'center', 'bottom'],
        domAttr      = 'verticalalign',
        domClass     = '.cell',
        cellAttr     = 'alignVert',
        attributes   = null,
        selection    = null,
        rootNode     = null,
        domPos       = null,

        // the operations to be applied by the document model
        OPERATIONS  = [
            { name: 'setDocumentAttributes', attrs: { page: { width: 33866, height: 19050, orientation: 'landscape' } } },
            { name: 'insertMasterSlide', id: 1 },
            { name: 'insertLayoutSlide', id: 2, target: 1 },
            { name: 'insertSlide', start: [0], target: 2, attrs: { fill: { type: 'solid', color: Color.RED } } },
            { name: 'insertDrawing', start: [0, 0], type: 'table', attrs: { line: { color: Color.BLACK, style: 'single', width: 26, type: 'none' }, fill: { type: 'none' }, geometry: { presetShape: 'rect', avList: {} }, drawing: { width: 8000, left: 5000, top: 3000, noGroup: true, id: '4000010' }, shape: { autoResizeHeight: true, noAutoResize: false, wordWrap: true }, table: { tableGrid: [1000, 1000, 1000], width: 'auto', exclude: ['bandsVert', 'firstCol', 'lastCol', 'lastRow'] } } },
            { name: 'insertRows', start: [0, 0, 0], attrs: { row: { height: 1500 } } },
            { name: 'insertCells', start: [0, 0, 0, 0], attrs: { cell: { alignVert: positions[0] } } },
            { name: 'insertCells', start: [0, 0, 0, 1], attrs: { cell: { alignVert: positions[1] } } },
            { name: 'insertCells', start: [0, 0, 0, 2], attrs: { cell: { alignVert: positions[2] } } },
            { name: 'insertParagraph', start: [0, 0, 0, 0, 0] },
            { name: 'insertParagraph', start: [0, 0, 0, 1, 0] },
            { name: 'insertParagraph', start: [0, 0, 0, 2, 0] },
            { name: 'insertText', start: [0, 0, 0, 0, 0, 0], text: 'Hello World' },
            { name: 'insertText', start: [0, 0, 0, 1, 0, 0], text: 'Hello World' },
            { name: 'insertText', start: [0, 0, 0, 2, 0, 0], text: 'Hello World' }
        ];

    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        var model = app.getModel();
        selection = model.getSelection();
        rootNode = selection.getRootNode();
    });

    // existence check ----------------------------------------------------
    it('should exist', function () {
        expect(TableCellStyles).toBeFunction();
    });

    // DOC-588 User sees text vertically aligned in table cells
    describe('check if attribute exists', function () {

        it('should have vertical alignment attribute #1', function () {
            domPos = Position.getDOMPosition(rootNode, [0, 0, 0, 0]);
            attributes = getExplicitAttributes(domPos.node, 'cell', true);
            expect(attributes[cellAttr]).toBe(positions[0]);
            expect(attributes[cellAttr]).not.toBe(positions[1]);
            expect(attributes[cellAttr]).not.toBe(positions[2]);
            expect($(domPos.node).find(domClass).attr(domAttr)).toBe(positions[0]);
        });

        it('should have vertical alignment attribute #2', function () {
            domPos = Position.getDOMPosition(rootNode, [0, 0, 0, 1]);
            attributes = getExplicitAttributes(domPos.node, 'cell', true);
            expect(attributes[cellAttr]).toBe(positions[1]);
            expect(attributes[cellAttr]).not.toBe(positions[2]);
            expect(attributes[cellAttr]).not.toBe(positions[0]);
            expect($(domPos.node).find(domClass).attr(domAttr)).toBe(positions[1]);
        });

        it('should have vertical alignment attribute #3', function () {
            domPos = Position.getDOMPosition(rootNode, [0, 0, 0, 2]);
            attributes = getExplicitAttributes(domPos.node, 'cell', true);
            expect(attributes[cellAttr]).not.toBe(positions[0]);
            expect(attributes[cellAttr]).not.toBe(positions[1]);
            expect(attributes[cellAttr]).toBe(positions[2]);
            expect($(domPos.node).find(domClass).attr(domAttr)).toBe(positions[2]);
        });
    });
});
