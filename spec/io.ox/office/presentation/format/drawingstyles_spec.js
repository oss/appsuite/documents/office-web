/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { createPresentationApp } from '~/presentation/apphelper';
import { getDrawingRotationAngle } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { ABSOLUTE_DRAWING_SELECTOR } from '@/io.ox/office/textframework/utils/dom';
import DrawingStyles from '@/io.ox/office/presentation/format/drawingstyles';

// class DrawingStyles ====================================================

describe('Presentation format class DrawingStyles', function () {

    // private helpers ----------------------------------------------------

    var layoutId_1 = 'layout1',
        masterId_1 = 'master1',
        ctrTitleLeft = 1900,
        subTitleLeft = 3800,
        ctrTitleTop = 5900,
        subTitleTop = 10800,
        ctrTitleHeight = 4000,
        subTitleHeight = 5000,
        ctrTitleWidth = 21600,
        subTitleWidth = 17800,
        activeSlide = null,
        drawingsOnSlide = null,

        // the operations to be applied by the document model
        OPERATIONS = [
            {
                name: 'setDocumentAttributes',
                attrs: { page: { width: 33866, height: 19050, orientation: 'landscape' } }
            },
            { name: 'insertMasterSlide', id: masterId_1 },

            { name: 'insertLayoutSlide', id: layoutId_1, target: masterId_1 },
            { name: 'insertDrawing', start: [0, 0], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'ctrTitle' }, drawing: { name: 'Titel 1', left: ctrTitleLeft, top: ctrTitleTop, width: ctrTitleWidth, height: ctrTitleHeight } } },
            { name: 'insertParagraph', start: [0, 0, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 0, 0, 0], target: layoutId_1, text: 'Mastertitelformat bearbeiten' },
            { name: 'insertDrawing', start: [0, 1], target: layoutId_1, type: 'shape', attrs: { presentation: { phType: 'subTitle', phIndex: 1 }, drawing: { name: 'Untertitel 2', left: subTitleLeft, top: subTitleTop, width: subTitleWidth, height: subTitleHeight } } },
            { name: 'insertParagraph', start: [0, 1, 0], target: layoutId_1 },
            { name: 'insertText', start: [0, 1, 0, 0], target: layoutId_1, text: 'Master-Untertitelformat bearbeiten' },

            { name: 'insertSlide', start: [0], target: layoutId_1 },
            { name: 'insertDrawing', start: [0, 0], type: 'shape', attrs: { drawing: { name: 'TextBox 3', left: 2903, top: 2573, width: 3958, height: 1026, rotation: 45 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'none' } } },
            { name: 'insertParagraph', start: [0, 0, 0] },
            { name: 'insertText', start: [0, 0, 0, 0], text: 'test' },
            { name: 'insertDrawing', start: [0, 1], type: 'shape', attrs: { drawing: { name: 'TextBox 4', left: 7553, top: 2573, width: 3958, height: 1026, rotation: 90 }, geometry: { presetShape: 'rect', avList: {} }, fill: { type: 'none' } } },
            { name: 'insertParagraph', start: [0, 1, 0] },
            { name: 'insertText', start: [0, 1, 0, 0], text: 'test2' }
        ];

    var model = null;
    var selection = null;
    createPresentationApp('ooxml', OPERATIONS).done(function (app) {
        model = app.getModel();
        selection = model.getSelection();
    });

    // existence check ----------------------------------------------------

    it('should exist', function () {
        expect(DrawingStyles).toBeFunction();
    });

    // public methods -----------------------------------------------------

    describe('method getDrawingRotationAngle', function () {
        it('should exist', function () {
            expect(getDrawingRotationAngle).toBeFunction();
        });
        it('should return correctly set drawing style rotation', function () {
            selection.setTextSelection([0, 0, 0, 0]);  // selecting the first slide
            activeSlide = model.getSlideById(model.getActiveSlideId());
            drawingsOnSlide = activeSlide.children(ABSOLUTE_DRAWING_SELECTOR);

            expect(getDrawingRotationAngle(model, drawingsOnSlide[0])).toBe(45);
            expect(getDrawingRotationAngle(model, drawingsOnSlide[1])).toBe(90);

        });
    });
});
