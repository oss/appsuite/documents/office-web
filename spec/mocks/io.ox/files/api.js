/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

class Model {
}

Model.prototype.types = {
    image: (/^(gif|bmp|tif?f|jpe?g|gmp|png|psd|heic?f?)$/),
    audio: (/^(aac|mp3|m4a|m4b|ogg|opus|wav)$/),
    video: (/^(avi|m4v|mp4|ogv|ogm|mov|mpeg|webm|wmv)$/),
    vcf:   (/^(vcf)$/),
    doc:   (/^(docx|docm|dotx|dotm|odt|ott|doc|dot|rtf)$/),
    xls:   (/^(csv|xlsx|xlsm|xltx|xltm|xlam|xls|xlt|xla|xlsb|ods|ots)$/),
    ppt:   (/^(pptx|pptm|potx|potm|ppsx|ppsm|ppam|odp|otp|ppt|pot|pps|ppa|odg|otg)$/),
    pdf:   (/^pdf$/),
    zip:   (/^(zip|tar|gz|rar|7z|bz2)$/),
    txt:   (/^(txt|md)$/),
    guard: (/^(grd|grd2|pgp)$/)
};

export default {
    Model,
    getUrl() { return "url"; },
    on() {}
};
