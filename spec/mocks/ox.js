/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// global "ox" replacement ====================================================

/**
 * Replacement for the global object "window.ox" for unit tests.
 */
const ox = {

    // standard properties
    abs: window.location.origin,
    apiRoot: "/api",
    debug: false,
    language: "de_DE",
    locale: "de_DE",
    root: "",
    rampup: {},
    serverConfig: {},

    // event interface
    on() { return this; },
    once() { return this; },
    off() { return this; },
    trigger() { return this; },
    listenTo() { return this; },
    listenToOnce() { return this; },
    stopListening() { return this; },

    // standard contents of the "ox.ui" object
    ui: {
        session: {}
    },

    user: "",
    user_id: 12,

    // additions for debugging
    office: {
        _UNITTEST: true
    }
};

export default ox;
