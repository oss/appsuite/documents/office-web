var process = require('process');
/**
 * Returns the tests for this runner calculated from the runner index and the number of runners.
 * @param {array} testPaths all test files.
 * @returns [ { test: <testfilename_1> }, { test: <testfilename_n> } ]
 */
module.exports = testPaths => {
    // The index of the runner executing the tests, the index starts at 1
    const runnerIdx = process.env.CI_NODE_INDEX - 1;
    // The number of runners that run the tests
    const runnerTotal = process.env.CI_NODE_TOTAL;

    const length = testPaths.length;
    const extra = length % runnerTotal;
    const size = Math.round(length / runnerTotal);
    const start = runnerIdx * size;
    const end = start + size;
    const final = runnerIdx === runnerTotal - 1;
    const extraCount = final ? extra : 0;

    // The sorting of the tests is necessary because Jest returns different orders
    const tests = testPaths.sort().slice(start, end + extraCount);
    const testsJson = [];
    tests.forEach(testFile => {
        testsJson.push({
            test: testFile
        });
    });
    console.log(testsJson.length + " tests for runner " + (runnerIdx + 1) + ". Total tests: " + length);
    return {
        filtered: testsJson
    };
};
