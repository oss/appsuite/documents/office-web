# App Suite Documents

The web frontend parts for the Documents editor applications.

## Project Structure

| Directory | Description | Link |
| - | - | - |
| `/vite` | Plugins for Vite and Rollup. | |
| `/src` | The application code that will run in the browser. | |
| `/spec` | Unit tests (based on Jest, Chai, Sinon). | |
| `/e2e` | End-to-end tests (based on Codecept, Puppeteer, Mocha). | |
| `/perf` | Playground for low-level performance tests of JS engines. | [README](./perf/README.md) |

## Setup

### System Requirements

#### NodeJS

The [NodeJS runtime](https://nodejs.org/) needs to be installed globally. The minimum required version of NodeJS is specified in the field `"engines"` of the file [`/package.json`](./package.json).

#### Corepack

This project uses [Modern Yarn](https://yarnpkg.com/) as package manager which requires to enable [Corepack](https://nodejs.org/api/corepack.html).

Corepack needs to be enabled only once on the system (per OS user). After this has been done, every NodeJS project is able to pick its own package manager type and version.

| Platform | Command |
| -- | -- |
| Linux/Mac | Open a shell and run: `sudo corepack enable` |
|  Windows | Open a shell _with administrator privileges_ and run: `corepack enable` |

### Project Dependencies

After cloning the GIT repository, install project dependencies by calling `yarn` in a command shell (please do NOT use `npm` to install, or manage project dependencies). Dependencies are kept up-to-date on a regular base, so don't forget to run `yarn` after pulling the latest changes from GIT.

### Visual Studio Code

The following plugins are recommended to be installed:

- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) for additional GIT commands in the VS Code user interface.
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) for realtime validation of JavaScript and TypeScript code, as well as JSON and YAML configuration files.
- [Stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint) for realtime validation of CSS, LESS, and SASS code.
- [YAML language server](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml) for realtime validation of YAML files against JSON schemas.

This repository contains an example workspace configuration that enables support for realtime validating of TypeScript code as well as JSON and YAML files with the ESLint plugin. Create a copy of the file `/docs.code-workspace.example` by removing the `.example` extension, and then open the configuration file with VS Code.

VS Code remembers the last opened workspace when closed, and restores it on next start without having to open the workspace configuration file explicitly. This even includes volatile settings such as unsaved files.

## Vite Development Server

### GitLab API Token (_optional_)

To be able to access optional internal packages (e.g. the commercial version of [CanvasJS](https://canvasjs.com/)), a GitLab API token is needed. The API token is expected to be provided in the environment variable `GITLAB_TOKEN`. This can be done in the local environment configuration file `/.env`:

1. Create a GitLab API token with (at least) `read_api` or `read_repository` scope: <https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html>.

1. Add the following contents to the file `/.env` (create the file if missing):

   ```env
   GITLAB_TOKEN=<your-token>
   ```

### Local Documents UI

1. Add a folder `/ssl` with SSL certificates to this project.

1. Default configuration uses a dedicated backend server (see option "SERVER" in `/.env.defaults`). To change the configuration, add the following contents to the file `/.env` (create the file if missing):

   ```env
   SERVER=https://your.backend.server:port
   ```

1. In a new shell, go to this project, and enter `yarn dev` to start the development server for the Documents UI.

### Local Documents UI With Local Core UI

1. Create a clone of the Core Frontend UI project (<https://gitlab.open-xchange.com/frontend/ui>) next to this project.

1. Add a folder `/ssl` with SSL certificates to the Core Frontend UI project, and to this project.

1. Add a file `/.env` to the Core Frontend UI project with the following contents:

   ```env
    SERVER=https://your.backend.server:port
    PORT=3000
    ENABLE_SECURE_PROXY=false
    ```

1. Add a file `/.env` to this project with the following contents:

   ```env
   SERVER=https://your.backend.server:port
   FRONTEND_URIS=https://localhost:3000
   ```

1. In a new shell, go to the Core Frontend UI project, and enter `yarn dev` to start the development server for the Core UI.

1. In a new shell, go to this project, and enter `yarn dev` to start the development server for the Documents UI.

## Unit Tests

Unit tests can be filtered by filename or path fragments, for example:

- `yarn test numberformatter_spec` runs all tests with `numberformatter_spec` in their filepaths.
- `yarn test tk/` runs all tests with `tk/` in their filepaths (i.e. all toolkit tests).

## End-To-End Tests

The default configuration for CodeceptJS in `e2e/.env.defaults` is prepared to run the tests against the CI preview deployment for the GIT main branch.

### Configuration

To execute E2E tests locally or against a deployment for another GIT branch it is required to set a few environments variables.

- Create a configuration file `e2e/.env`. All configuration contained in `e2e/.env.defaults` can be overridden in this file. It is included in `.gitignore` and will not appear as locally modified file.
- Adjust the variable `DEPLOYMENT_DOMAIN` if needed (branch deployments). Most other variable containing remote URLs are built depending on its value.
- Adjust `LAUNCH_URL` if running a local development server.

Consult the descriptions in `e2e/.env.defaults` to lean about other configuration options.

### Running Tests

Run `yarn e2e` to execute all tests, or use the option `--grep` to filter for specific files.

The command will pass all options directly to CodeceptJS. See [Running CodeceptJS Tests](https://codecept.io/basics/#running-tests) for more configuration options.

### Debugging

Test results such as screenshots for failing tests will be stored in the folder `e2e/output`.

Detailed logging of all executed test steps can be enabled with the command line option `--debug`.

Debug settings can be enabled using the environment variables `HEADLESS` and `PAUSE_ON_FAIL`.

See the section `Artifacts` in the console output at the end of each test to find the names of the generated debug files.

### Parallel Execution

The script `yarn e2e:run` can be used for local parallel execution. It passes all options to the helper script `codecept-runner` under the hood. See [@open-xchange/codecept-runner](https://www.npmjs.com/package/@open-xchange/codecept-runner) for details about its configuration.

### E2E Allure Reports

Generated by E2E jobs in [pipeline](https://gitlab.open-xchange.com/documents/office-web/-/pipelines)

#### Develop (main)

- Daily stable E2E tests: 1. smoketest 2. stable (<https://appsuite-7.fspd.cloud.oxoe.io/reports/office-web/main/stable/allure-report/latest/>)
- Daily unstable E2E tests: unstable (<https://appsuite-7.fspd.cloud.oxoe.io/reports/office-web/main/unstable/allure-report/latest/>)
- On Commit:
       - Automatic: @smoketest (<https://appsuite-7.fspd.cloud.oxoe.io/reports/office-web/main/allure-report/latest/>)
       - Changes in folder "e2e" or click on the gear with the title "e2e start nonstable": @unstable and not tagged (new test) (<https://appsuite-7.fspd.cloud.oxoe.io/reports/office-web/main/nonstable/allure-report/latest/>)
       - Click on the gear with the title "e2e start stable": @smoketest and @stable (<https://appsuite-7.fspd.cloud.oxoe.io/reports/office-web/main/stable/allure-report/latest/>)

#### stable-8.0

- Daily stable E2E tests: 1. smoketest 2. stable (<https://appsuite-7.fspd.cloud.oxoe.io/reports/office-web/stable-8-0/stable/allure-report/latest/>)
- Daily unstable E2E tests: unstable (<https://appsuite-7.fspd.cloud.oxoe.io/reports/office-web/stable-8-0/unstable/allure-report/latest/>)
- On Commit: smoketest, not tagged (new test) (<https://appsuite-7.fspd.cloud.oxoe.io/reports/office-web/stable-8-0/allure-report/latest/>)

#### Release 7.10.6

- Daily stable E2E tests: 1. smoketest 2. stable (<https://appsuite-7.fspd.cloud.oxoe.io/reports/office-web/release-7-10-6/stable/allure-report/latest/>)
- Daily unstable E2E tests: unstable (<https://appsuite-7.fspd.cloud.oxoe.io/reports/office-web/release-7-10-6/unstable/allure-report/latest/>)
- On Commit: smoketest, not tagged (new test) (<https://appsuite-7.fspd.cloud.oxoe.io/reports/office-web/release-7-10-6/allure-report/latest/>)

## Upgrade Dependencies

To upgrade the project dependencies, follow these steps:

- Create a feature branch with a dedicated Jira identifier.
- In the root folder, run `yarn upgrade-interactive`. This will start the interactive upgrade tool.
- Select all packages to be upgraded (use cursor keys as described in the tool). Check the comments section in `package.json` for packages that _must not_ be upgraded.
- Press `Enter` to upgrade the selected packages. This will update the files `package.json` and `yarn.lock`.
- Check the repositories of the upgraded packages for change logs or release logs to find breaking changes that need to be handled. Complex changes may be moved to an own Jira story.
- Repeat everything above for the `/e2e` subdirectory.
- _Optional:_ To check project health quickly, run `yarn upgrade-check`. This will run the scripts `lint`, `build`, `test`, and `e2e:smoke` (the latter needs a local development server!).
- Commit and push everything.
- Create a merge request in GitLab.

## Yarn Custom Commands Overview

The following custom commands are available:

| Command | Description |
| - | - |
| `yarn dev` | Starts the development server according to the configuration in `/.env`. |
| `yarn dev:debug` | Starts the development server according to the configuration in `/.env` in NodeJS debugging mode. Useful to debug the development server (Vite/Rollup) itself. |
| `yarn build` | Runs a production build of the project into the output directory `/dist`. |
| `yarn lint` | Runs the TypeScript compiler, ESLint, and StyleLint on all source files. |
| `yarn lint:node` | Runs the TypeScript compiler and ESLint on all source files in the `/vite` and `/perf` directories. |
| `yarn lint:src` | Runs the TypeScript compiler, ESLint, and StyleLint on all source files in the `/src` directory. |
| `yarn lint:spec` | Runs ESLint on all source files in the `/spec` directory. |
| `yarn lint:e2e` | Runs the TypeScript compiler and ESLint on all source files in the `/e2e` directory. |
| `yarn test` | Runs all unit tests in parallel (or serially with switch `-i`). Pass a path fragment to filter by name of test files (e.g. `yarn test baseframework`). |
| `yarn test:watch` | Runs all unit tests in watch mode. Pass a path fragment to filter by name of test files (e.g. `yarn test:watch baseframework`). |
| `yarn test:debug` | Runs all unit tests attached to the NodeJS debugger. |
| `yarn e2e` | Runs all or specific Codecept tests. Use the switch `--grep` to select tests by title. |
| `yarn e2e:run` | Runs E2E tests in parallel. Call `yarn e2e:run --help` for details. |
| `yarn e2e:smoke` | Runs all E2E smoketests in parallel (uses `yarn e2e:run` internally). |
| `yarn e2e:debug` | Runs Codecept tests attached to the NodeJS debugger. |
| `yarn release` | Runs the release tool. Bumps versions of `package.json` and Helm charts, updates `CHANGELOG.md`. |
| `yarn upgrade-interactive` | Runs the interactive upgrade tool for package dependencies. |
| `yarn upgrade-check` | Runs the scripts `lint`, `build`, `test`, and `e2e:smoke` (the latter needs a local development server!). |

## Links

### Source Code

- [Type-safe event handling](./src/io.ox/office/tk/event/README.md)
- [The debug package](./src/io.ox/office/debug/README.md)
- [Spreadsheet function definitions](./src/io.ox/office/spreadsheet/model/formula/funcs/README.md)

### Generated Resources

- [How to add or change PNG or SVG icons](./src/io.ox/office/tk/icons/README.md)
- [How to add or change a language in Spreadsheet formula resources](./src/io.ox/office/spreadsheet/model/formula/resource/README.md)

## Create Stable Branch

Example for 8.1

- Create stable-8.1 branch from main
- Bump the main branch version:

  `.gitlab-ci.yml`:

  ```yaml
  PROJECT_VERSION_PART: "8.1" ==> "8.2"
  ```

   `helm/office-web/Chart.yaml`:

   ```yaml
   version: "2.1.0" ==> "2.2.0"
   appVersion: "8.1.0" ==> "8.2.0"
   ```

   `package.json`:

   ```json
   {
     "version": "8.1.0" ==> "8.2.0"
   }
   ```

- Change the stable-8.1 branch e2e LAUNCH_URL in `.gitlab-ci.yml`

## Run E2E test with E2E image

I use the `latest` E2E image and <https://docsbox-main.sre.cloud.oxoe.io/appsuite> for this description.

### Pull the image

```bash
docker pull registry.gitlab.open-xchange.com/documents/office-web/e2e:latest
```

### Run the image and open the image bash

```bash
docker run --rm -it --entrypoint bash registry.gitlab.open-xchange.com/documents/office-web/e2e:latest
```

#### Run in privileged mode to change system settings

```bash
docker run --privileged --rm -it --entrypoint bash registry.gitlab.open-xchange.com/documents/office-web/e2e:latest
```

### Set environement variables

Copy/Paste the following lines in the image bash and the press return

```bash
export runOnly=true &&
export LAUNCH_URL=https://docsbox-main.sre.cloud.oxoe.io/appsuite/ &&
export PROVISIONING_URL=https://docsbox-main.sre.cloud.oxoe.io/ &&
export CONTEXT_ID=123456 &&
export SMTP_SERVER="preview-app-postfix" &&
export IMAP_SERVER="preview-app-dovecot" &&
export E2E_OUTPUT_FOLDER=output_e2e/ &&
export CHROME_ARGS="--no-sandbox --kiosk-printing --disable-web-security  --ignore-certificate-errors" &&
export CI_NODE_INDEX=1 &&
export CI_NODE_TOTAL=1 &&
export MIN_SUCCESS=1 &&
export MAX_RERUNS=2 &&
export CI_PIPELINE_ID=123456789 &&
export FILTER_SUITE=./output/suites/job$CI_NODE_INDEX.json
```

### Run the @smoke tests

Execute the following line

```bash
yarn e2e-rerun @smoketest
```

### Show debug infos

```bash
export DEBUG="*,-puppeteer:protocol:*,-codeceptjs:*,-mocha:*,-node-soap,-mocha-junit-reporter,-follow-redirects,-puppeteer:browsers:launcher"
```

#### Install nano to edit files

```bash
apt update &&
apt install nano
```

Open codecept.conf.js (```nano codecept.conf.js```) to add ```dumpio: true``` to ```chrome: {}```

##### Nano shortcuts

Save: ```Ctrl+S```
Close: ```Ctrl+X```

### Change the node version

Install the Node version manager

```bash
npm install -g n
```

Change the Node version

```bash
n <Node version>
```

### Exit the bash and retrun to the host os

Execute the following line

```bash
exit
```
