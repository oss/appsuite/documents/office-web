/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import ox from '$/ox';

import { random as randomUUID } from '@/io.ox/office/tk/algorithms/uuid';

import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';
import URLUtils from '@/io.ox/office/rt2/shared/jss_urlutils';
import Validate from '@/io.ox/office/rt2/shared/jss_validate';
import Logger from '@/io.ox/office/rt2/shared/jss_logger';
import HashMap from '@/io.ox/office/rt2/shared/jss_hashmap';
import List from '@/io.ox/office/rt2/shared/jss_list';
import { BaseObject } from '@/io.ox/office/tk/objects';
import RT2Const from '@/io.ox/office/rt2/rt2const';
import RT2InitOptions from '@/io.ox/office/rt2/rt2initoptions';
import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';
import RT2Message from '@/io.ox/office/rt2/shared/rt2message';
import RT2MessageFactory from '@/io.ox/office/rt2/shared/rt2messagefactory';
import RT2MessageGetSet from '@/io.ox/office/rt2/shared/rt2messagegetset';
import RT2Channel from '@/io.ox/office/rt2/rt2channel';
import RT2GUI from '@/io.ox/office/rt2/rt2ui';
import EDocState from '@/io.ox/office/rt2/shared/edocstate';
import ErrorCode from '@/io.ox/office/rt2/shared/errorcode';
import { calcDocUID } from '@/io.ox/office/rt2/rt2utils';
import Rt2LoggerContextMain from '@/io.ox/office/rt2/rt2loggercontext_main';

// constants ==================================================================

const /* long    */ INTERVAL_4_FLUSH_ACK_IN_MS               = 5000;
const /* long    */ TIMEOUT_4_MISSING_ACKS_IN_MS             = 60000;
const /* long    */ TIMEOUT_4_REQUESTING_MSGS_VIA_NACK_AGAIN = 60000;

// class RT2 ==================================================================

/**
 * Main entry point into the new RT2 framework.
 */
export default class RT2 extends BaseObject {

    constructor(aInitOptions) {

        // base constructor
        super();

        // member
        var self = this;

        // ID of this RT2 instance (unique across all : user, clients, documents)
        var /* string */ m_sRT2ClientUID = null;

        // This is "the one and only" unique ID of the document owned by this RT2 instance !
        // It's calculated by this RT2 instance based on driveDocId, folderId, fileId and
        // an optional prefix.
        var /* string */ m_sDocUID = null;

        // This is an optional prefix to be used to calculate the docUID.
        // It can be used by special application which needs a separate
        // READ_ONLY access to a document. E.g. in our case the Presenter
        // app which should present while the same presentation document is
        // opened. Handle with case and IT MUST BE ENSURED THAT ANY OTHER
        // APP CAN ONLY ACCESS THE DOCUMENT READ-ONLY. OTHERWISE DATA-LOSS
        // AND STRANGE EFFECTS CAN OCCURE.
        var /* string */ m_sDocIdPrefix = '';

        // Drive ID of the document, where this RT2 instance is used for.
        // It's NOT meant to be used for any drive access.
        // Don't interpret/parse this ID it's must be treated as an unknown
        // string.
        var /* string */ m_sDriveDocId = null;

        // the folder id where the document is located
        var /* string */ m_sFolderId = null;

        // the drive access file id of the document
        var /* string */ m_sFileId = null;

        /// type of the document (text, spreadsheet, resentation)
        var /* string */ m_sDocumentType = null;

        // corresponding communication channel (RT2Channel or RT2Polling)
        var /* RT2Channel */ m_aComChannel = null;

        // knows our current off/online state
        var /* boolean */ m_bOnline = false;

        // sequence number of last message sent
        var /* long */ m_nSeqNrOut = 0;

        // sequence number of last message received
        var /* long */ m_nSeqNrIn = 0;

        // blocks all messages from the backend - for debug/testing only!
        var /* boolean */ m_bBlockMsg = false;

        // disables the send ack message - for debug/testing only!
        var /* boolean */ m_disableSendAck = false;

        // queue to buffer/sort response messages (based on seq-nr header)
        var /* map< long, RT2Message > */ m_aDocResponseQueue = null;

        // timer for send flush ACKs to server
        var /* Timer */ m_aFlushAckChecker = null;

        // map to store messages which haven't received any ACK from the backend
        var /* map< seqnr, AckCheck > */ m_lAckChecks = null;

        // set with seq numbers to be send with next ACK
        var /* set< seqnr > */ m_aRecievedSeqNumbers = null;

        // set with seq numbers requested via a NACK with time stamp
        var /* map< seqnr, long > */ m_aRequestedSeqNrViaNack = null;

        // used for performance measurement
        //var /* json[] */ m_lNetworkData = null;

        var /* Object */ m_aChatGUI = null;

        var /* Logger */ m_aLog = null;

        var /* number */ lastAckTimeStamp = _.now();

        // public methods -----------------------------------------------------

        /* public string */ this.getClientUID = function () {
            return mem_RT2ClientUID();
        };

        //----------------------------------------------------------------------
        /* public string */ this.getDocUID = function () {
            return mem_DocUID();
        };

        //----------------------------------------------------------------------
        /* public boolean */ this.isConnected = function () {
            return !ObjectUtils.isNull(m_aComChannel) && m_bOnline;
        };

        //----------------------------------------------------------------------
        /* public String */ this.getChannelUID = function () {
            return (!ObjectUtils.isNull(m_aComChannel)) ? m_aComChannel.getChannelUID() : null;
        };

        //----------------------------------------------------------------------
        /* public void */ this.sendRequest = function (/* string */ sMsgId, /* string */ sRequest, /* Dict<object> */ lHeader, /* json */ jBody) {
            log().trace('sendRequest', [sMsgId, sRequest, lHeader, jBody]);

            var /* RT2Message */ aRequest = null;
            try {
                aRequest = impl_createDefaultRequest(sRequest, jBody);
                aRequest.mergeHeader(lHeader);

                // WORKAROUND: To make sure that ACK messages that could be lost due to a
                // non-detected offline scenario, we create an artificial ACK message which
                // includes all seq-nrs that we definitely know. We use the SYNC message as
                // a trigger that the client wants to synchronize after an offline phase.
                // This is a knowledge of an implementation detail and can break if we will
                // change the synchronization code. Nevertheless SYNC is the only way to detect
                // a starting sync and is used by client & server to support the synchronization
                // phase. Therefore this is stable enough to be used as a workaround here.
                var /* string  */ sMsgType = aRequest.getType();
                if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.REQUEST_SYNC)) {
                    impl_resendAcksOnSync();
                }

                // HACK : change it asap
                aRequest.setMessageID(sMsgId);

                impl_sendRequest(aRequest, false);
            } catch (/* Error */ ex) {
                log().ex(ex, [aRequest]);
                self.trigger(RT2Const.EVENT_ERROR, ex);
            }
        };

        //----------------------------------------------------------------------
        function /* void */ impl_resendAcksOnSync() {
            // Create ack message & use new syntax to describe a range: '1-m_nSeqNrIn'
            var jBody = {};
            var /* String */ ackProp = '1-' + m_nSeqNrIn;
            jBody[RT2Protocol.BODYPART_ACKS] = ackProp;

            var /* RT2Message */ aAckRange = impl_createSimpleRequest(RT2Protocol.ACK_SIMPLE, jBody);
            impl_sendRequest(aAckRange, false);

            // We might have to create a second ACK message as there could be messages
            // waiting in the response queue due to a missing seq-nrs. Therefore we have to check
            // the message queue for those messages, too. Otherwise we would get a HANG_UP
            // from the server if we forget to ACK just one message.
            var /* map< long, RT2Message > */ responseQueue = mem_DocResponseQueue();
            var /* [] */ seqNrs = responseQueue.keySet().toArray();

            if (seqNrs.length > 0) {
                var /* [] */ ackArray = _.filter(seqNrs, function (seqnr) {
                    return (seqnr > m_nSeqNrIn);
                });

                if (ackArray.length > 0) {
                    jBody = {};
                    jBody[RT2Protocol.BODYPART_ACKS] = ackArray;
                    var /* RT2Message */ aAckList = impl_createSimpleRequest(RT2Protocol.ACK_SIMPLE, jBody);
                    impl_sendRequest(aAckList, false);
                }
            }
        }

        //----------------------------------------------------------------------
        /* public void */ this.debugTriggerProblem = function (/* string */ sProblem) {
            // this is for testing/debugging purpose only
            switch (sProblem) {
                case 'offline':
                case 'online': {
                    var aSwitchMsg = impl_createDefaultRequest(RT2Protocol.RESPONSE_SWITCH_DOCSTATE, {});
                    var sDocState  = (sProblem === EDocState.STR_ONLINE) ? EDocState.E_ONLINE : EDocState.E_OFFLINE;
                    RT2MessageGetSet.setDocState(aSwitchMsg, sDocState);
                    impl_handleSwitchDocState(aSwitchMsg);
                    break;
                }
                case 'blockmsg': m_bBlockMsg = true; break;
                case 'disablesendack': m_disableSendAck = true; break;
                case 'typeerror': {
                    try {
                        throw new Error('type error trigger by debug function!');
                    } catch (ex) {
                        log().ex(ex);
                        self.trigger(RT2Const.EVENT_ERROR, ex);
                    }
                    break;
                }
                case 'sessioninvalid': {
                    var aResponse = impl_createDefaultRequest(RT2Protocol.BROADCAST_UPDATE, {});
                    var aError    = ErrorCode.create(ErrorCode.CODE_GENERAL_SESSION_INVALID_ERROR, 'GENERAL_SESSION_INVALID_ERROR', '', 2);

                    RT2MessageGetSet.setError(aResponse, aError);
                    impl_handleGenericError(aResponse);
                    break;
                }
            }
        };

        //----------------------------------------------------------------------
        function /* void */ impl_onResponse(/* RT2Message */ aResponse) {
            try {
                if (log().isLogLevel(Logger.LEVEL_TRACE)) {
                    log().trace('impl_onResponse', [aResponse]);
                }

                var /* string  */ sMsgType  = aResponse.getType();
                var /* string  */ sMsgId    = aResponse.getMessageID();

                log().debug('handle msg', [sMsgType, sMsgId]);

                if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.PING)) {
                    // DOCS-3104 - Use ping as an external clock for our ACK
                    // handling to circumvent timer throttling by certain
                    // browsers for background tabs.
                    impl_sendFlushAck();
                    return;
                }

                if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.ACK_SIMPLE)) {
                    // handle ACK response and return - no need for further notification
                    impl_handleAck(aResponse);
                    return;
                }

                if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.NACK_SIMPLE)) {
                    // handle NACK request and return - no need for further notification
                    impl_handleNack(aResponse);
                    return;
                }

                if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.RESPONSE_SWITCH_DOCSTATE)) {
                    if (impl_handleSwitchDocState(aResponse)) {
                        return;
                    }
                }

                // some errors have to be handled here directly
                if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.RESPONSE_GENERIC_ERROR)) {
                    if (impl_handleGenericError(aResponse)) {
                        return;
                    }
                }

                if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.BROADCAST_UPDATE)) {
                    impl_enableChatGUI();
                }

                if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.RESPONSE_LEAVE)) {
                    impl_disableChatGUI();
                }

                impl_processResponse(aResponse);
            } catch (/* Error */ ex) {
                log().ex(ex);
                self.trigger(RT2Const.EVENT_ERROR, ex);
            }
        }

        //----------------------------------------------------------------------
        function /* void */ impl_handleAck(/* RT2Message */ aAck) {
            var /* String */ sMsgId = aAck.getMessageID();

            log().trace('handle ACK ...', [aAck, sMsgId]);

            var /* json                 */ jBody      = aAck.getBody();
            var /* array                */ lAckList   = ObjectUtils.getObjectPartOrDefault(jBody, RT2Protocol.BODYPART_ACKS, null);
            var /* map< int, AckCheck > */ lAckChecks = mem_AckChecks();

            if (ObjectUtils.isNull(lAckList)) {
                log().error('... no ACK data in body', [sMsgId]);
                return;
            }

            log().trace('... ACK messages', [sMsgId, lAckList]);
            var /* long */ nNow = Date.now();
            lAckList.forEach(function (nSeqNr) {
                log().trace('... ACK message for SEQ-NR = ' + nSeqNr, [sMsgId]);
                var /* AckCheck */ aAckCheck = lAckChecks.remove(nSeqNr);

                if (ObjectUtils.isNull(aAckCheck)) {
                    log().error(`... there is no AckCheck item available for SEQ-NR = ${nSeqNr}`, [sMsgId]);
                    return;
                }

                var /* long */ nTime = (nNow - aAckCheck.sentAt);
                if (nTime > TIMEOUT_4_MISSING_ACKS_IN_MS) {
                    log().warn(`... ACK needed much time to be received for SEQ-NR = ${nSeqNr}`, [sMsgId, nTime]);
                }
            });
        }

        //----------------------------------------------------------------------
        function /* void */ impl_handleNack(/* RT2Message */ aNack) {
            var /* String */ sMsgId = aNack.getMessageID();

            log().trace('handle NACK ...', [aNack, sMsgId]);

            var /* json  */ jBody     = aNack.getBody();
            var /* array */ lNackList = ObjectUtils.getObjectPartOrDefault(jBody, RT2Protocol.BODYPART_NACKS, null);

            if (ObjectUtils.isNull(lNackList)) {
                log().error('... no NACK data in body', [sMsgId]);
                return;
            }

            log().trace('... resend requested messages according to NACKs', [sMsgId, lNackList]);

            impl_resendMessages(impl_getMessagesForResend(lNackList));
        }

        //----------------------------------------------------------------------
        function /* private List<RT2Message> */ impl_getMessagesForResend(includeSeqList) {
            var /* map< int, AckCheck > */ lAckChecks  = mem_AckChecks();
            var /* List< RT2Message > */ msgsForResend = new List();
            var /* List< int > */ indexList = ObjectUtils.isNotNull(includeSeqList) ? includeSeqList :  lAckChecks.keySet().toArray();

            indexList.forEach(function (nSeqNr) {
                var /* AckCheck   */ aAckCheck = lAckChecks.get(nSeqNr);
                var /* RT2Message */ msg = ObjectUtils.getObjectPartOrDefault(aAckCheck, 'msg', null);
                if (ObjectUtils.isNull(msg)) {
                    log().error('... no message found for SEQ-NR = ' + nSeqNr);
                    self.trigger(RT2Const.EVENT_ERROR, 'Msg seq-nr cannot be found in backup message map!');
                    return;
                }
                msgsForResend.add(msg);
            });

            return msgsForResend;
        }

        //----------------------------------------------------------------------
        function /* private boolean */ impl_resendMessages(/* RT2Message */ lMessages4Resend) {
            lMessages4Resend.toArray().forEach(function (/* RT2Message */ aMessage4Resend) {
                // send requested msg as stored - don't update seq-nr!
                log().trace('... resend message [' + RT2MessageGetSet.getSeqNumber(aMessage4Resend) + ']');
                impl_sendRequest(aMessage4Resend, true);
            });
        }

        //----------------------------------------------------------------------
        /* private boolean */ function impl_handleSwitchDocState(/* RT2Message */ aResponse) {
            var /* EDocState */ eDocState  = RT2MessageGetSet.getDocState(aResponse);
            var /* boolean   */ bGoOffline = eDocState.equals(EDocState.E_OFFLINE);
            var /* boolean   */ bGoOnline  = eDocState.equals(EDocState.E_ONLINE);

            log().info(`doc state switch to [${eDocState}] ...`);

            // switch internal state and notify our listener
            if (bGoOffline) {
                self.trigger(RT2Const.EVENT_OFFLINE);
                m_bOnline = false;
            } else if (bGoOnline) {
                self.trigger(RT2Const.EVENT_ONLINE);
                m_bOnline = true;
            }

            return /* break further handling */ true;
        }

        //----------------------------------------------------------------------
        /* private boolean */ function impl_handleGenericError(/* RT2Message */ aResponse) {
            var /* ErrorCode */ aError = RT2MessageGetSet.getError(aResponse);

            if (aError.equals(ErrorCode.CODE_GENERAL_SESSION_INVALID_ERROR)) {
                log().error('Session is invalid. Force reconnect.');

                // lost session is more or less a 'final' error
                // No further request will be accepted by the server
                // close connection and stop message processing

                impl_stopScheduledTasks();
                impl_resetACKQueue();
                impl_resetDocResponseQueue();
                impl_closeConnection();
            }

            // don't close connection for 'normal' errors.
            // client wants to trigger 'auto-close' might be ...
            // which isn't possible if we close all before ...

            // client needs to know that error too !
            // might for showing an error box ...
            self.trigger(RT2Const.EVENT_ERROR, aError);

            return /* break further handling */ true;
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_processResponse(/* RT2Message */ aResponse) {
            log().trace('impl_processResponse(): START', [aResponse]);

            var /* boolean */ bHasSeqNr      = aResponse.isSequenceMessage();
            var /* long    */ nActSeqNr      = bHasSeqNr ? RT2MessageGetSet.getSeqNumber(aResponse) : null;
            var /* long    */ nExpectedSeqNr = m_nSeqNrIn + 1;
            var /* boolean */ bOutOfOrder    = nActSeqNr !== nExpectedSeqNr;

            log().trace('... check seq-nr[' + nActSeqNr + '] against last-seq-nr[' + m_nSeqNrIn + ']');

            // just for debug purpose - this state can block all messages from the
            // OX Documents backend
            if (m_bBlockMsg) {
                return;
            }

            // a) response messages without seq-nr needs to be forwarded directly
            //    it's prio is higher then 'normal' messages ;-)
            if (!bHasSeqNr) {
                log().trace('... prio response without seq-nr : forward to derived class');
                impl_notifyResponse(aResponse);
                return;
            }

            // b) we already got N response messages.
            //    Now there is a message with N-x as seq-nr ... but those message was already handled !?
            //    Do we have duplicates in seq-nr ?!
            if (nActSeqNr < nExpectedSeqNr) {
                log().warn('... unexpected response with seq-nr[' + nActSeqNr + '] where expected seq-nr[' + nExpectedSeqNr + '] : will be ignored');
                return;
            }

            // c1) bufer response message
            log().trace('... queue response with seq-nr[' + nActSeqNr + ']');
            mem_DocResponseQueue().put(nActSeqNr, aResponse);

            // c2) store received seq number for lazy flush ack
            mem_RecievedSeqNumbers().add(nActSeqNr);

            // c3) remove possible entry in map storing seq-nrs request by the NACK
            impl_removeMsgFromRequestedNackMap(nActSeqNr);

            // d) Look for responses within buffer, which are ready to be notified.
            //    Can be more than one if a message closes a gap in the queue.
            var /* List< RT2Message > */ lReadyResponses = impl_getReadyDocResponsesOrdered();
            var /* int                */ nReadyResponses = lReadyResponses.size();
            for (var /* int */ nReadyResponse = 0; nReadyResponse < nReadyResponses; ++nReadyResponse) {
                var /* RT2Message */ aReadyResponse = lReadyResponses.get(nReadyResponse);
                Validate.isNotNull(aReadyResponse, 'Ready response is null ?! Whats happen ?');
                var /* long       */ nReadySeqNr    = RT2MessageGetSet.getSeqNumber(aReadyResponse);
                log().trace('... found bufferred response with seq-nr[' + nReadySeqNr + '] : forward to derived class');
                impl_notifyResponse(aReadyResponse);
            }

            // d) a message was out-of-order or a message closed a gap
            //    => check if we need to send a NACK message for further possible gaps
            if (bOutOfOrder || !lReadyResponses.isEmpty()) {
                log().trace('... detected out-of-order SEQ-NR or ready to be sent messages');
                impl_handleMissingSeqNrs();
            }

            log().trace('impl_processResponse(): END', [aResponse]);
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_removeMsgFromRequestedNackMap(/* int */ seqNr) {
            log().trace(`impl_removeMsgFromRequestNackMap seqNr[${seqNr}]`);
            mem_RequestedSeqNumbersViaNack().remove(seqNr);
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_handleMissingSeqNrs() {
            var /* List< int > */ lMissingSeqNrs = impl_getMissingSeqNrs();

            log().trace('impl_handleMissingSeqNrs');

            if (!lMissingSeqNrs.isEmpty()) {
                var /* Map< Integer, Object > */ requestedSeqNrsViaNack = mem_RequestedSeqNumbersViaNack();
                var /* List< int > */ newNackList = requestedSeqNrsViaNack.isEmpty() ? lMissingSeqNrs : impl_filterSeqNrsForNextNACK(lMissingSeqNrs);

                impl_updateRequestedSeqNrsViaNACK(newNackList);
                if (!newNackList.isEmpty()) {
                    impl_sendNackRequest(newNackList);
                }
            }
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_updateRequestedSeqNrsViaNACK(/* List< int > */ lMissingSeqNrs) {
            log().trace('impl_updateRequestedSeqNrsViaNACK');

            if (lMissingSeqNrs.isEmpty()) {
                log().trace('impl_updateRequestedSeqNrsViaNACK - no update needed, the list is empty');
                return;
            }

            var /* Map< Integer, Object > */ requestedSeqNrsViaNack = mem_RequestedSeqNumbersViaNack();
            var /* long */ nNow = Date.now();

            lMissingSeqNrs.toArray().forEach(function (seqNr) {
                requestedSeqNrsViaNack.put(seqNr, { timeStamp: nNow });
            });

            log().trace('impl_updateRequestedSeqNrsViaNACK - requested SEQ-NRS by NACK map updated');
        }

        //----------------------------------------------------------------------
        /* private List< int > */ function impl_filterSeqNrsForNextNACK(/* List< int > */ lMissingSeqNrs) {
            log().trace('impl_filterSeqNrsForNextNACK');

            var /* Map< Integer, Object > */ requestedSeqNrsViaNack = mem_RequestedSeqNumbersViaNack();
            var /* long */ nNow = Date.now();

            /* private boolean */ function impl_timeOutReachedForRequestedSeqNr(/* int */ seqNr) {

                var entry = requestedSeqNrsViaNack.get(seqNr);
                return (entry && _.isNumber(entry.timeStamp) && ((entry.timeStamp + TIMEOUT_4_REQUESTING_MSGS_VIA_NACK_AGAIN) < nNow));
            }

            var /* Map < Integer, Boolean > */ seqNrsForNextNACK = new HashMap();
            var /* List< int > */ seqNrListForNack = new List();

            // collect all seq-nrs which have a time-out due to our stored time stamp
            requestedSeqNrsViaNack.keySet().toArray().forEach(function (seqNr) {
                if (impl_timeOutReachedForRequestedSeqNr(seqNr)) {
                    seqNrsForNextNACK.put(seqNr, true);
                }
            });

            if (!seqNrsForNextNACK.isEmpty()) {
                log().trace('impl_filterSeqNrsForNextNACK - found SEQ-NRS requested by NACK with time-out ', seqNrsForNextNACK.keySet().toString());
            }

            // now we also need to check if new missing seq-nrs haven been provided to us
            if (!lMissingSeqNrs.isEmpty()) {
                lMissingSeqNrs.toArray().forEach(function (newSeqNr) {
                    if (!requestedSeqNrsViaNack.containsKey(newSeqNr)) {
                        seqNrsForNextNACK.put(newSeqNr, true);
                    }
                });
            }

            // put all seq-nrs we need to NACK into the resulting list
            seqNrsForNextNACK.keySet().toArray().forEach(function (seqNr) {
                seqNrListForNack.add(seqNr);
            });

            log().trace('impl_filterSeqNrsForNextNACK - filter found needed SEQ-NRS = ', seqNrListForNack.toString());

            return seqNrListForNack;
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_sendNackRequest(/* List< int > */ lMissingSeqNrs) {
            var /* JSON */ jBody = {};

            log().trace('... requesting the following missing SEQ-NRS : ' + lMissingSeqNrs.toString());

            jBody[RT2Protocol.BODYPART_NACKS] = lMissingSeqNrs.toArray();
            var /* RT2Message */ aNackRequest = impl_createNackRequest(jBody);

            impl_sendRequest(aNackRequest, false);
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_notifyResponse(/* RT2Message */ aResponse) {
            var /* string  */ sMsgType = aResponse.getType();

            if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.BROADCAST_CHAT_MESSAGE)) {
                if (impl_handleChatMessageRemoteClient(aResponse)) {
                    return;
                }
            }

            self.trigger(RT2Const.EVENT_DOC_RESPONSE, aResponse);
        }

        //----------------------------------------------------------------------
        /* private List< RT2Message > */ function impl_getReadyDocResponsesOrdered() {
            var /* List< RT2Message >      */ lReadyResponses = new List();
            var /* map< long, RT2Message > */ aResponseQueue  = mem_DocResponseQueue();
            var /* long                    */ nSeqNrStart     = m_nSeqNrIn;
            var /* long                    */ nSeqNrTry       = nSeqNrStart;
            var /* long                    */ nSeqNrFinal     = nSeqNrStart;

            for (;;) {
                nSeqNrTry++;

                if (!aResponseQueue.containsKey(nSeqNrTry)) {
                    break;
                }

                var /* RT2Message */ aResponse = aResponseQueue.remove(nSeqNrTry);

                nSeqNrFinal = nSeqNrTry;
                lReadyResponses.add(aResponse);
            }

            m_nSeqNrIn = nSeqNrFinal;
            return lReadyResponses;
        }

        //----------------------------------------------------------------------
        /* private List< int > */ function impl_getMissingSeqNrs() {
            log().trace('GET MISSING SEQ-NR : START');
            var /* List< int >            */ lMissingSeqNrs = new List();
            var /* Map< int, RT2Message > */ aResponseQueue = mem_DocResponseQueue();
            var /* int                    */ nSeqNrNext     = m_nSeqNrIn;

            if (aResponseQueue.isEmpty()) {
                log().trace('GET MISSING SEQ-NR : END(1)');
                return lMissingSeqNrs;
            }

            for (;;) {
                nSeqNrNext++;
                if (aResponseQueue.containsKey(nSeqNrNext)) {
                    break;
                }

                lMissingSeqNrs.add(nSeqNrNext);
            }

            log().trace('GET MISSING SEQ-NR : END(2)', lMissingSeqNrs.toString());
            return lMissingSeqNrs;
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_sendRequest(/* RT2Message */ aRequest, /* boolean */ bResend) {
            if (log().isLogLevel(Logger.LEVEL_TRACE)) {
                log().trace('impl_sendRequest', [aRequest]);
            }

            var /* RT2Channel */ aChannel      = mem_ComChannel();
            var /* string     */ sRT2ClientUID = mem_RT2ClientUID();

            if (!bResend) {
                // in case of a resend due to a NACK by the server we MUST NOT do this twice
                impl_defineAndCacheSeqNr4RequestIfNeeded(aRequest);
            }

            RT2Message.cleanHeaderBeforeSendToServer(aRequest);

            aChannel.sendRequest(sRT2ClientUID, aRequest);
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_defineAndCacheSeqNr4RequestIfNeeded(/* RT2Message */ aRequest) {
            if (!aRequest.isSequenceMessage()) {
                return;
            }

            if (RT2MessageGetSet.hasSeqNumber(aRequest)) {
                throw new Error('SEQ-NR handling broken ? Creating new SEQ-NR found an already existing one !');
            }

            var /* long */ nSeqNr = ++m_nSeqNrOut;
            RT2MessageGetSet.setSeqNumber(aRequest, nSeqNr);

            log().trace('waiting for SEQ-NR: ' + nSeqNr);

            var /* Map< int, AckCheck > */ lAckChecks = mem_AckChecks();
            var /* AckCheck             */ aAckCheck  = { msg: aRequest, sentAt: Date.now() };
            lAckChecks.put(nSeqNr, aAckCheck);
        }

        //----------------------------------------------------------------------
        /* private RT2Message */ function impl_createDefaultRequest(/* string */ sType, /* json */ jBody) {
            var /* RT2Message */ aRequest = RT2MessageFactory.newMessage(sType);
            aRequest.setBody(jBody);

            RT2MessageGetSet.setClientUID(aRequest, mem_RT2ClientUID());
            RT2MessageGetSet.setDocUID(aRequest, mem_DocUID());

            if (aRequest.isSessionMessage()) {
                RT2MessageGetSet.setSessionID(aRequest, impl_getCurrentSessionId());
            }

            // server side handling needs those values for these two requests only ....
            // where we can safe some bytes if we hide them on all other requests.
            // those values are cached on server side after open ...
            if ((StringUtils.equals(sType, RT2Protocol.REQUEST_JOIN)) ||
                (StringUtils.equals(sType, RT2Protocol.REQUEST_OPEN_DOC))) {
                RT2MessageGetSet.setDriveDocID(aRequest, m_sDriveDocId);
                RT2MessageGetSet.setFolderID(aRequest, m_sFolderId);
                RT2MessageGetSet.setFileID(aRequest, m_sFileId);
                RT2MessageGetSet.setDocType(aRequest, m_sDocumentType);
                if (sType === RT2Protocol.REQUEST_JOIN) {
                    RT2MessageGetSet.setUserAgent(aRequest, window.navigator.userAgent);
                }
            }

            return aRequest;
        }

        //----------------------------------------------------------------------
        /* private RT2Message */ function impl_createSimpleRequest(/* string */ sType, /* json */ jBody) {
            var /* RT2Message */ aRequest = RT2MessageFactory.newMessage(sType);
            aRequest.setBody(jBody);

            RT2MessageGetSet.setClientUID(aRequest, mem_RT2ClientUID());
            RT2MessageGetSet.setDocUID(aRequest, mem_DocUID());

            if (aRequest.isSessionMessage()) {
                RT2MessageGetSet.setSessionID(aRequest, impl_getCurrentSessionId());
            }

            return aRequest;
        }

        //----------------------------------------------------------------------
        /* private RT2Message */ function impl_createFlushAck(/* json */ jBody) {
            var /* RT2Message */ aRequest = impl_createSimpleRequest(RT2Protocol.ACK_SIMPLE, jBody);
            return aRequest;
        }

        //----------------------------------------------------------------------
        /* private RT2Message */ function impl_createNackRequest(/* json */ jBody) {
            var /* RT2Message */ aRequest = impl_createSimpleRequest(RT2Protocol.NACK_SIMPLE, jBody);
            return aRequest;
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_sendFlushAck() {
            var currentTimeStamp = _.now();
            log().info('impl_sendFlushAck() - last ACK timer called: ', currentTimeStamp - lastAckTimeStamp);
            lastAckTimeStamp = currentTimeStamp;

            if (!m_bOnline) {
                return;
            }

            var /* List< int > */ lReceivedSeqNrs = impl_getAndResetRecievedSeqNumbers();
            if (lReceivedSeqNrs.isEmpty()) {
                return;
            }

            var /* int[] */ aSeqNrArray = lReceivedSeqNrs.toArray();
            var /* JSON  */ aJSONBody   = {};
            aJSONBody[RT2Protocol.BODYPART_ACKS] = aSeqNrArray;

            log().trace('impl_sendFlushAck() - send flush ACK to backend');
            var /* RT2Message  */ aFlushAck = impl_createFlushAck(aJSONBody);

            if (m_disableSendAck) { return; }
            impl_sendRequest(aFlushAck, false);
        }

        //----------------------------------------------------------------------
        /* private List< int > */ function impl_getAndResetRecievedSeqNumbers() {
            var /* List< int > */ lOrg   = mem_RecievedSeqNumbers();
            var /* List< int > */ lClone = new List();
            lClone.addAll(lOrg);
            lOrg.clear();
            return lClone;
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_startFlushAcks() {
            if (!ObjectUtils.isNull(m_aFlushAckChecker)) {
                return;
            }

            log().debug('start flush-acks ...');
            m_aFlushAckChecker = window.setInterval(impl_sendFlushAck, INTERVAL_4_FLUSH_ACK_IN_MS);
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_stopFlushAcks() {
            var /* Timer */ aFlushAckChecker  = m_aFlushAckChecker;
            m_aFlushAckChecker  = null;

            if (ObjectUtils.isNull(aFlushAckChecker)) {
                return;
            }

            log().debug('stop flush-acks ...');
            window.clearInterval(aFlushAckChecker);
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_stopScheduledTasks() {
            impl_stopFlushAcks();
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_resetACKQueue() {
            log().debug('reset ACK buffer ...');
            mem_AckChecks().clear();
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_resetDocResponseQueue() {
            log().debug('reset Doc-Response queue ...');
            mem_DocResponseQueue().clear();
            m_nSeqNrIn = 0;
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_resetSeqNr() {
            log().debug('reset SEQ-NR generator ...');
            m_nSeqNrIn  = 0;
            m_nSeqNrOut = 0;
        }

        //----------------------------------------------------------------------
        /* private string */ function impl_calcDocUID(/* string */ sDocIdPrefix, /* string */ sDriveId) {
            if (!ObjectUtils.isString(sDriveId)) {
                log().error('Mandatory DriveId parameter is not string!');
                throw new Error('DriveDocId parameter to calculate docUID is not a string!');
            }
            return calcDocUID(sDocIdPrefix, sDriveId);
        }

        //----------------------------------------------------------------------
        function /* String */ impl_getCurrentSessionId() {
            Validate.notEmpty(ox.session, 'No session found - but requested.');
            return ox.session;
        }

        //----------------------------------------------------------------------
        /* public void */ this.onChatMessage = function (/* string */ sMessage) {
            log().info(`DBG : got chat message: "${sMessage}"`);

            var /* JSON */ jBody = { message: `"${sMessage}"` };

            self.sendRequest(randomUUID(), RT2Protocol.BROADCAST_CHAT_MESSAGE, null, jBody);
        };

        //----------------------------------------------------------------------
        /* private boolean */ function impl_handleChatMessageRemoteClient(/* RT2Message */ aRT2Message) {
            impl_enableChatGUI(/* force */ true);

            var /* string */ sClientUID = RT2MessageGetSet.getClientUID(aRT2Message);
            var /* JSON   */ aBody      = aRT2Message.getBody();
            var /* string */ sMessage   = ObjectUtils.getObjectPartOrDefault(aBody, 'message', '???');

            m_aChatGUI.addChatMessage(sClientUID, sMessage);

            return true;
        }

        //--------------------------------------------------------------------------
        /* private void */ function impl_disableChatGUI() {
            if (ObjectUtils.isNull(m_aChatGUI)) {
                return;
            }
            m_aChatGUI.hide();
            m_aChatGUI = null;
        }

        //--------------------------------------------------------------------------
        /* private void */ function impl_enableChatGUI(/* boolean */ bForce) {
            if (!ObjectUtils.isNull(m_aChatGUI)) {
                return;
            }

            if (!bForce && !impl_isRT2ChatEnabled()) {
                return;
            }

            try {
                m_aChatGUI = new RT2GUI();
                m_aChatGUI.addChatListener(self);
                m_aChatGUI.show();
            } catch (ex) {
                log().error(ex);
            }
        }

        //----------------------------------------------------------------------
        /* private boolean */ function impl_isRT2ChatEnabled() {
            var /* string */ sURL             = URLUtils.getCurrentURL();
            var /* string */ sRT2ChatGUIParam = URLUtils.getParamValue(sURL, 'rt2-chat');
            return StringUtils.equalsIgnoreCase(sRT2ChatGUIParam, 'true');
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_init4Standard(/* RT2InitOptions */ aInitOptions) {
            m_sDriveDocId   = aInitOptions.getOption(RT2InitOptions.INITOPT_DRIVE_DOC_ID,  null);
            m_sFolderId     = aInitOptions.getOption(RT2InitOptions.INITOPT_FOLDER_ID,     null);
            m_sFileId       = aInitOptions.getOption(RT2InitOptions.INITOPT_FILE_ID,       null);
            m_sDocumentType = aInitOptions.getOption(RT2InitOptions.INITOPT_DOC_TYPE,      null);
            m_sDocIdPrefix  = aInitOptions.getOption(RT2InitOptions.INITOPT_DOC_ID_PREFIX, '');

            Validate.isNotNull(m_sDriveDocId,   `Init option "${RT2InitOptions.INITOPT_DRIVE_DOC_ID}" missing.`);
            Validate.isNotNull(m_sFolderId,     `Init option "${RT2InitOptions.INITOPT_FOLDER_ID}" missing.`);
            Validate.isNotNull(m_sFileId,       `Init option "${RT2InitOptions.INITOPT_FILE_ID}" missing.`);
            Validate.isNotNull(m_sDocumentType, `Init option "${RT2InitOptions.INITOPT_DOC_TYPE}" missing.`);
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_init4ReAttach(/* RT2InitOptions */ aInitOptions) {
            impl_init4Standard(aInitOptions);

            m_sRT2ClientUID = aInitOptions.getOption(RT2InitOptions.INITOPT_CLIENT_UID,     null);
            m_sDocUID       = aInitOptions.getOption(RT2InitOptions.INITOPT_DOC_UID,        null);
            m_nSeqNrOut     = aInitOptions.getOption(RT2InitOptions.INITOPT_SEQNR_REQUEST,  null);
            m_nSeqNrIn      = aInitOptions.getOption(RT2InitOptions.INITOPT_SEQNR_RESPONSE, null);

            Validate.isNotNull(m_sRT2ClientUID, `Init option "${RT2InitOptions.INITOPT_CLIENT_UID}" missing.`);
            Validate.isNotNull(m_sDocUID,       `Init option "${RT2InitOptions.INITOPT_DOC_UID}" missing.`);
            Validate.isNotNull(m_nSeqNrOut,     `Init option "${RT2InitOptions.INITOPT_SEQNR_REQUEST}" missing.`);
            Validate.isNotNull(m_nSeqNrIn,      `Init option "${RT2InitOptions.INITOPT_SEQNR_RESPONSE}" missing.`);

            // for the momentt seq-nr cant be restored ...
            // as reload/reattach isnt a real "attach to existing resource" ...
            // it's close/reopen ... where ne seq-numbers has to be used !
            m_nSeqNrOut = 0;
            m_nSeqNrIn  = 0;
        }

        //----------------------------------------------------------------------
        /* private void */ function impl_closeConnection() {
            if (ObjectUtils.isNull(m_aComChannel)) {
                return;
            }

            m_bOnline = false;
            impl_stopFlushAcks();

            // connection is not opened exclusive for us ...
            // it's shared across all RT2 instances ...
            // Dont close it - dont stop it (btw: there is no API regarding these).
            // But deregister this client from that channel.

            m_aComChannel.unregisterClient(mem_RT2ClientUID());
            m_aComChannel = null;
        }

        //----------------------------------------------------------------------
        /* private RT2Channel */ function mem_ComChannel() {
            if (ObjectUtils.isNull(m_aComChannel)) {
                var /* string     */ sRT2ClientUID = mem_RT2ClientUID();
                var /* RT2Channel */ aChannel      = RT2Channel.get();

                aChannel.registerClient(sRT2ClientUID, self, impl_onResponse);
                m_aComChannel = aChannel;

                impl_startFlushAcks();
                m_bOnline = true;
            }
            return m_aComChannel;
        }

        //----------------------------------------------------------------------
        /* private string */ function mem_RT2ClientUID() {
            if (ObjectUtils.isNull(m_sRT2ClientUID)) {
                m_sRT2ClientUID = randomUUID();
            }
            return m_sRT2ClientUID;
        }

        //----------------------------------------------------------------------
        /* private string */ function mem_DocUID() {
            if (StringUtils.isEmpty(m_sDocUID)) {
                m_sDocUID = impl_calcDocUID(m_sDocIdPrefix, m_sDriveDocId);
            }
            return m_sDocUID;
        }

        //----------------------------------------------------------------------
        /* private map< long, RT2Message > */ function mem_DocResponseQueue() {
            if (ObjectUtils.isNull(m_aDocResponseQueue)) {
                m_aDocResponseQueue = new HashMap();
            }
            return m_aDocResponseQueue;
        }

        //----------------------------------------------------------------------
        /* private Map< Integer, AckCheck > */ function mem_AckChecks() {
            if (ObjectUtils.isNull(m_lAckChecks)) {
                m_lAckChecks = new HashMap();
            }
            return m_lAckChecks;
        }

        //----------------------------------------------------------------------
        /* private List< int > */ function mem_RecievedSeqNumbers() {
            if (ObjectUtils.isNull(m_aRecievedSeqNumbers)) {
                m_aRecievedSeqNumbers = new List();
            }
            return m_aRecievedSeqNumbers;
        }

        //----------------------------------------------------------------------
        /* private Map< Integer, Object > */ function mem_RequestedSeqNumbersViaNack() {
            if (ObjectUtils.isNull(m_aRequestedSeqNrViaNack)) {
                m_aRequestedSeqNrViaNack = new HashMap();
            }
            return m_aRequestedSeqNrViaNack;
        }

        //----------------------------------------------------------------------
        /* private Log */ function log() {
            if (ObjectUtils.isNull(m_aLog)) {
                var /* string */ sRT2ClientUID = mem_RT2ClientUID();
                m_aLog = Logger.create(`RT2-${sRT2ClientUID}`, Rt2LoggerContextMain);
            }
            return m_aLog;
        }

        // initialization -----------------------------------------------------

        log().trace('init', [aInitOptions]);

        if (aInitOptions.isType(RT2InitOptions.INITTYPE_STANDARD)) {
            impl_init4Standard(aInitOptions);
        } else if (aInitOptions.isType(RT2InitOptions.INITTYPE_REATTACH)) {
            impl_init4ReAttach(aInitOptions);
        } else {
            throw new Error(`No support for init-options type "${aInitOptions.getType()}" implemented yet.`);
        }

        log().debug(`... client-uid    = "${mem_RT2ClientUID()}"`);
        log().debug(`... doc-uid       = "${mem_DocUID()}"`);
        log().debug(`... doc-type      = "${m_sDocumentType}"`);
        log().debug(`... doc-id-prefix = "${m_sDocIdPrefix}"`);
        log().debug(`... drive-doc-id  = "${m_sDriveDocId}"`);
        log().debug(`... folder-id     = "${m_sFolderId}"`);
        log().debug(`... file-id       = "${m_sFileId}"`);

        this.registerDestructor(function () {

            log().trace('RT2.dtor() called ...');

            impl_stopScheduledTasks();
            impl_closeConnection();
            impl_resetACKQueue();
            impl_resetDocResponseQueue();
            impl_resetSeqNr();
        });
    }
}
