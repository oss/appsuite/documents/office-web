/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/* global self */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';
import Logger from '@/io.ox/office/rt2/shared/jss_logger';
import RunLoop from '@/io.ox/office/rt2/shared/jss_runloop';
import EDocState from '@/io.ox/office/rt2/shared/edocstate';
import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';
import RT2MessageFactory from '@/io.ox/office/rt2/shared/rt2messagefactory';
import RT2MessageGetSet from '@/io.ox/office/rt2/shared/rt2messagegetset';

//-------------------------------------------------------------------------
var /* json[]   */ m_gBeforeReadyBuffer = [];
var /* function */ m_gChannelFunc       = null;
var global = self;

//----------------------------------------------------------------------
/* class RT2WebSocketChannel
*
* Communication channel based on WebSockets for RT2.
* No protocol handling - message handling (in/out) only.
*/
function RT2WebSocketChannel() {
    //------------------------------------------------------------------
    var /* int    */ LOOP_FREQUENCY            = 1;
    var /* int    */ RECONNECT_FREQUENCY_IN_MS = 10000;
    var /* int    */ OFFLINECOUNTDOWN_IN_MS = 70000;

    var /* string */ EVENT_TYPE_INIT = 'init';
    var /* string */ EVENT_TYPE_SEND = 'send_request';

    //------------------------------------------------------------------

    // the web socket channel
    var /* WebSocket */ m_aConnection = null;

    // an outside configuration containing different values ...
    // as e.g. connection url
    var /* json */ m_jConfig = {};

    // buffer for outgoing requests
    var /* RunLoop */ m_aRunLoop4Requests = null;

    // buffer for incoming responses
    var /* RunLoop */ m_aRunLoop4Responses = null;

    // indicates if our run loops are running
    // (loops for buffering requests or responses)
    var /* boolean */ m_bLoopsAreRunning = false;

    var /* boolean */ m_bIsOnline = false;

    var /* long */ m_nOnlineTime = 0;

    var /* long */ m_nOfflineTime = 0;

    var /* ? */ m_aReconnectTimer = null;

    var /* Logger */ m_aLog = null;

    var /* timer */ m_aOfflineTimer = null;

    var /* function */ noop = function () {};

    //------------------------------------------------------------------
    /* start this channel and return a function to handle incoming events
    */
    /* function */ this.start = function () {
        log().trace('start');
        return impl_handleClientEvent;
    };

    //------------------------------------------------------------------
    /* stop this channel
    */
    /* void */ this.stop = function () {
        log().trace('stop');
        impl_closeConnection();
    };

    //------------------------------------------------------------------
    function impl_releaseSocketRef() {
        var socket = m_aConnection;
        m_aConnection = null;
        if (socket !== null) {
            socket.onopen = noop;
            socket.onclose = noop;
            socket.onmessage = noop;
            socket.onerror = noop;
        }
    }

    //------------------------------------------------------------------
    /* close the WS on timeout
    */
    function impl_closeOnPingTimeout() {
        log().debug('impl_closeOnPingTimeout, close because of ping timeout');
        impl_closeConnection();
    }

    //------------------------------------------------------------------
    /* handle the incoming events from our (JS) clients
    *
    * Such event can be :
    * - an internal event (e.g. for initialization)
    * - an event which has to be send to the server
    *
    * @param jEvent [IN]
    *          the event to be handled
    */
    function /* void */ impl_handleClientEvent(/* MessageObject */ aEvent) {
        log().trace('impl_handleClientEvent', aEvent);
        if (!aEvent?.data) {
            log().warn('got request without data ?!');
            return;
        }

        try {
            var /* json */ jEvent = JSON.parse(aEvent.data);

            if (StringUtils.equals(jEvent.type, EVENT_TYPE_INIT)) {
                impl_init(jEvent.config);
                impl_openConnection();
            } else if (StringUtils.equals(jEvent.type, EVENT_TYPE_SEND)) {
                impl_enqueueRequest(jEvent.request);
            } else {
                throw new Error('unsupported message type : "' + jEvent.type + '"');
            }
        } catch (/* Error */ ex) {
            log().error(ex);
        }
    }

    //------------------------------------------------------------------
    function /* void */ impl_init(/* json */ jConfig) {
        log().trace('impl_init', jConfig);
        m_jConfig = jConfig;

        if (!ObjectUtils.isNull(m_jConfig.log_level)) {
            // set log level from main context
            global.rt2LoggerContextWorker.setLogLevel(m_jConfig.log_level);
        } else {
            log().error('... no log_level provided for worker');
        }
    }

    //------------------------------------------------------------------
    function /* boolean */ impl_handleServerRequest(/* string */ sRequest) {
        log().trace('impl_handleServerRequest', sRequest);
        var m_aConnection = mem_Connection();

        try {
            m_aConnection.send(sRequest);
            return true;
        } catch (/* Error */ ex) {
            log().error(ex);
            return false;
        }
    }

    //------------------------------------------------------------------
    function /* boolean */ impl_handleServerResponse(/* string */ sResponse) {
        log().trace('impl_handleServerResponse', sResponse);
        try {
            global.postMessage(sResponse);
            return true;
        } catch (/* Error */ ex) {
            log().error(ex);
            return false;
        }
    }

    //------------------------------------------------------------------
    /*
        CONNECTING,
        OPEN,
        CLOSING,
        CLOSED
    */
    function /* boolean */ impl_isConnectionOpen() {
        return !ObjectUtils.isNull(m_aConnection) && (m_aConnection.readyState === global.WebSocket.OPEN);
    }

    //------------------------------------------------------------------
    function /* void */ impl_openConnection() {
        log().trace('impl_openConnection');
        mem_Connection();
    }

    //------------------------------------------------------------------
    function impl_closeConnection() {
        log().trace('impl_closeConnection');
        if (!ObjectUtils.isNull(m_aConnection)) {
            m_aConnection.close();
            impl_releaseSocketRef();
            // don't wait for the ws triggered close event when closed by
            // the client (can take a lot of time in chrome + offline)
            impl_onConnectionClose();
        }
    }

    // //------------------------------------------------------------------
    // function /* void */ impl_ensureOpenConnection()
    // {
    //     log().trace('impl_ensureOpenConnection');
    //     if (impl_isConnectionInitiated() === true)
    //         return;
    //     if (impl_isConnectionOpen() === true)
    //         return;

    //     impl_closeConnection();
    //     impl_openConnection();
    // }

    //------------------------------------------------------------------
    function /* void */ impl_onConnectionError(/* WS.Event */ aEvent) {
        log().error('impl_onConnectionError', aEvent);
    }

    //------------------------------------------------------------------
    function /* void */ impl_onConnectionOpen(/* WS.Event */ aEvent) {
        log().trace('[ONOFFLINE] impl_onConnectionOpen', aEvent);

        impl_startOrResetDetectOfflineTimer();
        impl_cancelReconnect();
        impl_switchToOnline();
    }

    //------------------------------------------------------------------
    function /* void */ impl_onConnectionClose(/* WS.Event */ aEvent) {
        log().trace('[ONOFFLINE] impl_onConnectionClose', aEvent ? aEvent.code : 'no aEvent');
        impl_releaseSocketRef();
        impl_clearDetectOfflineTimer();
        impl_switchToOffline();
        impl_triggerReconnectAsync();
    }

    //------------------------------------------------------------------
    function /* void */ impl_switchToOffline() {
        log().trace('[ONOFFLINE] impl_switchToOffline');
        var /* boolean */ bIsOnline = impl_isOnline();
        if (!bIsOnline) {
            log().trace('[ONOFFLINE] ... already offline');
            return;
        }

        log().debug('[ONOFFLINE] ... go offline');
        m_bIsOnline    = false;
        m_nOfflineTime = Date.now();

        impl_stopRunLoops();
        impl_notifyOffline();

        log().debug('[ONOFFLINE] ... was online for ' + impl_getTime4Online() + ' ms');
    }

    //------------------------------------------------------------------
    function /* void */ impl_switchToOnline() {
        log().trace('[ONOFFLINE] impl_switchToOnline');
        var /* boolean */ bIsOnline = impl_isOnline();
        if (bIsOnline) {
            log().trace('[ONOFFLINE] ... already online');
            return;
        }

        log().debug('[ONOFFLINE] ... go online');
        m_bIsOnline   = true;
        m_nOnlineTime = Date.now();

        impl_startRunLoops();
        impl_notifyOnline();

        log().debug('[ONOFFLINE] ... was offline for ' + impl_getTime4Offline() + ' ms');
    }

    //------------------------------------------------------------------
    function /* long */ impl_getTime4Online() {
        var /* long */ nNow      = Date.now();
        var /* long */ nBegin    = m_nOnlineTime;
        var /* long */ nTimeInMS = (nNow - nBegin);
        m_nOnlineTime = 0;
        return nTimeInMS;
    }
    //------------------------------------------------------------------
    function /* void */ impl_startOrResetDetectOfflineTimer() {
        log().trace('impl_startOrResetDetectOfflineTimer');
        impl_clearDetectOfflineTimer();
        m_aOfflineTimer = global.setTimeout(impl_closeOnPingTimeout, OFFLINECOUNTDOWN_IN_MS);
    }

    //------------------------------------------------------------------
    function /* void */ impl_clearDetectOfflineTimer() {
        if (m_aOfflineTimer) {
            log().trace('impl_clearDetectOfflineTimer');
            global.clearTimeout(m_aOfflineTimer);
            m_aOfflineTimer = null;

        }
    }

    //------------------------------------------------------------------
    function /* long */ impl_getTime4Offline() {
        var /* long */ nNow      = Date.now();
        var /* long */ nBegin    = m_nOfflineTime;
        var /* long */ nTimeInMS = (nNow - nBegin);
        m_nOfflineTime = 0;
        return nTimeInMS;
    }

    //------------------------------------------------------------------
    function /* void */ impl_triggerReconnectAsync() {
        // connection will be really open - if onConnectionOpen() is called !
        // We dont have any deferreds here ...
        // nor can we join synchronous for those event call ...
        // Save timer handle instead ...
        // and call impl_cancelReconnect() later
        m_aReconnectTimer = global.setTimeout(impl_tryReconnect, RECONNECT_FREQUENCY_IN_MS);
    }

    //------------------------------------------------------------------
    function /* void */ impl_tryReconnect() {
        // clear timer - must be restarted if connection doesn't work
        global.clearTimeout(m_aReconnectTimer);

        var /* boolean */ bIsConnected = impl_isConnectionOpen();
        if (bIsConnected) {
            m_aReconnectTimer = null;
            return;
        }

        log().debug('[ONOFFLINE] try reconnect ...');
        impl_openConnection();

        impl_triggerReconnectAsync();
    }

    //------------------------------------------------------------------
    function /* void */ impl_cancelReconnect() {
        if (ObjectUtils.isNull(m_aReconnectTimer)) {
            return;
        }

        log().debug('[ONOFFLINE] cancel reconnect ...');
        global.clearTimeout(m_aReconnectTimer);
        m_aReconnectTimer = null;
    }

    //------------------------------------------------------------------
    function /* void */ impl_notifyOffline() {
        var /* RT2Message */ aSwitchStateResponse = RT2MessageFactory.newMessage(RT2Protocol.RESPONSE_SWITCH_DOCSTATE);
        RT2MessageGetSet.setDocState(aSwitchStateResponse, EDocState.E_OFFLINE);
        var /* string     */ sSwitchStateResponse = RT2MessageFactory.toJSONString(aSwitchStateResponse);

        // send response directly - run loops are might stopped already !
        impl_handleServerResponse(sSwitchStateResponse);
    }

    //------------------------------------------------------------------
    function /* void */ impl_notifyOnline() {
        var /* RT2Message */ aSwitchStateResponse = RT2MessageFactory.newMessage(RT2Protocol.RESPONSE_SWITCH_DOCSTATE);
        RT2MessageGetSet.setDocState(aSwitchStateResponse, EDocState.E_ONLINE);
        var /* string     */ sSwitchStateResponse = RT2MessageFactory.toJSONString(aSwitchStateResponse);

        // send response directly - run loops are might stopped already !
        impl_handleServerResponse(sSwitchStateResponse);
    }

    //------------------------------------------------------------------
    function /* boolean */ impl_isOnline() {
        return m_bIsOnline;
    }

    //------------------------------------------------------------------
    function /* boolean */  isPingMessageFastCheck(/* string */ sResponse) {
        var /* JSON */ rt2Msg;

        // ATTENTION: the length check is just a performance optimization.
        // a ping message is smaller than 100 utf-16 chars, therefore
        // we don't check larger messages. must be adapted when the ping messages
        // or message format is modified.
        if (sResponse.length < 100) {
            try {
                rt2Msg = RT2MessageFactory.fromJSONString(sResponse, true);
            } catch (ex) {
                log().error(ex);
            }
        }
        return rt2Msg && RT2MessageGetSet.getType(rt2Msg) === RT2Protocol.PING;
    }

    //------------------------------------------------------------------
    function /* void */ impl_onServerResponse(/* MessageObject */ aEvent) {
        if (!aEvent?.data) {
            return;
        }

        var /* string */ sResponse = aEvent.data;

        // when we get a ping, return a pong, do not enqueue the message
        if (isPingMessageFastCheck(sResponse)) {
            var /* RT2Message */ pongMsg = RT2MessageFactory.newMessage(RT2Protocol.PONG);
            var /* string     */ pongMsgString = RT2MessageFactory.toJSONString(pongMsg);
            impl_handleServerRequest(pongMsgString);

            // the offline detection is currently only reset on ping, not on
            // every received message as you would usually do. we do this out
            // of performance considerations, so far we're not aware about a
            // negative consequence for the user. in contrast to resetting it
            // on every message the user may only be detected a little bit earlier
            // as offline.
            impl_startOrResetDetectOfflineTimer();
        }

        impl_enqueueResponse(sResponse);
    }

    //------------------------------------------------------------------
    function /* void */ impl_startRunLoops() {
        if (m_bLoopsAreRunning) {
            return;
        }

        log().trace('impl_startRunLoops');

        var /* RunLoop */ aLoop4Request  = mem_RunLoop4Request();
        var /* RunLoop */ aLoop4Response = mem_RunLoop4Response();

        aLoop4Request.start();
        aLoop4Response.start();

        m_bLoopsAreRunning = true;
    }

    //------------------------------------------------------------------
    function /* void */ impl_stopRunLoops() {
        if (!m_bLoopsAreRunning) {
            return;
        }

        log().trace('impl_stopRunLoops');

        var /* RunLoop */ aLoop4Request  = mem_RunLoop4Request();
        var /* RunLoop */ aLoop4Response = mem_RunLoop4Response();

        aLoop4Request.stop();
        aLoop4Response.stop();

        m_bLoopsAreRunning = false;
    }

    //------------------------------------------------------------------
    // function /* void */ impl_ensureRunLoops()
    // {
    //     log().trace('impl_ensureRunLoops');
    //     impl_startRunLoops();
    // }

    //------------------------------------------------------------------
    function /* void */ impl_enqueueRequest(/* string */ sRequest) {
        log().trace('impl_enqueueRequest', sRequest);
        var /* RunLoop */ aRunLoop = mem_RunLoop4Request();
        aRunLoop.enqueue(sRequest);
    }

    //------------------------------------------------------------------
    function /* void */ impl_enqueueResponse(/* string */ sResponse) {
        log().trace('impl_enqueueResponse', sResponse);
        var /* RunLoop */ aRunLoop = mem_RunLoop4Response();
        aRunLoop.enqueue(sResponse);
    }

    //------------------------------------------------------------------
    function /* boolean */ impl_handleRunLoopRequest(/* string */ sRequest) {
        if (!impl_isConnectionOpen()) {
            return false;
        }

        var /* boolean */ bHandled = impl_handleServerRequest(sRequest);
        return bHandled;
    }

    //------------------------------------------------------------------
    function /* boolean */ impl_handleRunLoopResponse(/* string */ sResponse) {
        if (!impl_isConnectionOpen()) {
            return false;
        }

        var /* boolean */ bHandled = impl_handleServerResponse(sResponse);
        return bHandled;
    }

    //------------------------------------------------------------------
    function /* WebSocket */ mem_Connection() {
        if (!ObjectUtils.isNull(m_aConnection)) {
            return m_aConnection;
        }

        if (ObjectUtils.isNull(m_jConfig.connection_url)) {
            throw new Error('miss configuration item "connection_url"');
        }

        log().debug('open connection [' + m_jConfig.connection_url + '] ...');

        var /* WebSocket */ aConnection = new global.WebSocket(m_jConfig.connection_url);
        aConnection.onopen    = impl_onConnectionOpen;
        aConnection.onclose   = impl_onConnectionClose;
        aConnection.onmessage = impl_onServerResponse;
        aConnection.onerror   = impl_onConnectionError;

        m_aConnection = aConnection;
        return m_aConnection;
    }

    //------------------------------------------------------------------
    function /* RunLoop */ mem_RunLoop4Request() {
        if (ObjectUtils.isNull(m_aRunLoop4Requests)) {
            var /* RunLoop */ aRunLoop = RunLoop.create(global, impl_handleRunLoopRequest, global.rt2LoggerContextWorker);
            aRunLoop.setFrequency(LOOP_FREQUENCY);
            m_aRunLoop4Requests = aRunLoop;
        }
        return m_aRunLoop4Requests;
    }

    //------------------------------------------------------------------
    function /* RunLoop */ mem_RunLoop4Response() {
        if (ObjectUtils.isNull(m_aRunLoop4Responses)) {
            var /* RunLoop */ aRunLoop = RunLoop.create(global, impl_handleRunLoopResponse, global.rt2LoggerContextWorker);
            aRunLoop.setFrequency(LOOP_FREQUENCY);
            m_aRunLoop4Responses = aRunLoop;
        }
        return m_aRunLoop4Responses;
    }

    //------------------------------------------------------------------
    function /* Logger */ log() {
        if (ObjectUtils.isNull(m_aLog)) {
            m_aLog = Logger.create('RT2WebSocketChannel', global.rt2LoggerContextWorker);
        }
        return m_aLog;
    }

} // class RT2WebSocketChannel


function Rt2LoggerContextWorker(/* int */ logLevel) {
    // always start with error, will be set by main thread later
    var /* int */ m_WorkerLogLevel = logLevel;

    //------------------------------------------------------------------
    /* int */ function getWorkerRT2LogLevel() {
        return m_WorkerLogLevel;
    }

    //------------------------------------------------------------------
    /* void */ function setLogLevel(/* int */ logLevel) {
        m_WorkerLogLevel = logLevel;
    }

    //------------------------------------------------------------------

    return { write: global.sendLogToMainProcess, getLevel: getWorkerRT2LogLevel, setLogLevel };

} // class Rt2LoggerContextWorker

//--------------------------------------------------------------------------
/**
 * Send a log message from the worker context to the app context.
 */
global.sendLogToMainProcess = function (logLevel, sNormOut) {
    global.postMessage({ log: '(ASYNC)' + sNormOut, logLevel });
};

//--------------------------------------------------------------------------
/* webworker postmessage handler has to be registered synchronous during loading this script !
* Otherwise incoming events will be dropped by webworker impl !
*
* ES6-LATER: loading is now synchronous, therefore the buffer is propably not needed anymore
*
* @see RT2WebsocketChannel.start()
*/
global.onmessage = function (/* json */ aEvent) {
    // a) channel ready to work and initial buffer removed ?
    //    -> direct forward to channel impl !
    if ((m_gChannelFunc !== null) && (m_gBeforeReadyBuffer === null)) {
        m_gChannelFunc(aEvent);
        return;
    }

    // b) channel not working yet ...
    //    -> buffer event

    m_gBeforeReadyBuffer.push(aEvent);
};

//----------------------------------------------------------------------
/* create a new channel and starts it ...
*/

global.rt2LoggerContextWorker = new Rt2LoggerContextWorker(Logger.LEVEL_ERROR);

var /* RT2WebSocketChannel */ aChannel = new RT2WebSocketChannel();
var channelFunc = aChannel.start();

for (const aOldEvent of m_gBeforeReadyBuffer) {
    channelFunc(aOldEvent);
}

m_gBeforeReadyBuffer = null;
m_gChannelFunc       = channelFunc;
