/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { random as randomUUID } from '@/io.ox/office/tk/algorithms/uuid';

import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';
import Logger from '@/io.ox/office/rt2/shared/jss_logger';
import HashMap from '@/io.ox/office/rt2/shared/jss_hashmap';
import { BaseObject } from '@/io.ox/office/tk/objects';
import RT2Const from '@/io.ox/office/rt2/rt2const';
import RT2MessageGetSet from '@/io.ox/office/rt2/shared/rt2messagegetset';
import RT2 from '@/io.ox/office/rt2/rt2';
import Rt2LoggerContextMain from '@/io.ox/office/rt2/rt2loggercontext_main';

// class RT2DeferredWrapper ===================================================

export default class RT2DeferredWrapper extends BaseObject {

    constructor(aInitOptions) {

        // base constructor
        super();

        // member
        var self = this;

        // the RT2 instance wrapped by this class
        var /* RT2 */ m_aRT2Inst = null;

        // map msg-id to deferreds for sync/async response handling
        var /* map< string, deferred > */ m_aDeferredRegistry = null;

        var /* Logger */ m_aLog = null;

        // public methods -----------------------------------------------------

        /* public string */ this.getClientUID = function () {
            return m_aRT2Inst.getClientUID();
        };

        //----------------------------------------------------------------------
        /* public string */ this.getDocUID = function () {
            return m_aRT2Inst.getDocUID();
        };

        //----------------------------------------------------------------------
        /* public String */ this.getChannelUID = function () {
            return m_aRT2Inst.getChannelUID();
        };

        //----------------------------------------------------------------------
        /* public boolean */ this.isConnected = function () {
            return m_aRT2Inst.isConnected();
        };

        //----------------------------------------------------------------------
        /* public Deferred */ this.send = function (/* string */ sRequest, /* Dict<object> */ lExternalHeader, /* json */ jExternalBody) {
            log().trace('send');

            var /* deferred               */ aDeferred         = this.createDeferred();
            var /* map< string, deferred> */ aDeferredRegistry = mem_DeferredRegistry();
            var /* string                 */ sMsgId            = impl_newMessageId();
            var /* map< string, object >  */ lHeader           = {};
            var /* json                   */ jBody             = {};

            if (!ObjectUtils.isNull(lExternalHeader)) {
                lHeader = lExternalHeader;
            }
            if (!ObjectUtils.isNull(jExternalBody)) {
                jBody = jExternalBody;
            }

            m_aRT2Inst.sendRequest(sMsgId, sRequest, lHeader, jBody);

            log().trace('send with deferred, msg-id = ' + sMsgId + ' waiting for resolve/reject');
            aDeferredRegistry.put(sMsgId, aDeferred);
            return aDeferred;
        };

        //----------------------------------------------------------------------
        /* public void */ this.debugTriggerProblem = function (/* string */ problem) {
            m_aRT2Inst.debugTriggerProblem(problem);
            return this;
        };

        //----------------------------------------------------------------------
        /* private void */ function impl_onDocResponse(/* RT2Message */ aResponse) {
            if (log().isLogLevel(Logger.LEVEL_TRACE)) {
                log().trace('onDocResponse ()', [aResponse]);
            }

            var /* string                 */ sMsgId            = aResponse.getMessageID();
            var /* json                   */ jRT1Msg           = impl_translateRT2MsgToRT1(aResponse);
            var /* map< string, deferred> */ aDeferredRegistry = mem_DeferredRegistry();
            var /* deferred               */ aDeferred         = aDeferredRegistry.get(sMsgId);

            log().trace('onDocResponse () check for deferred with msg-id = ' + sMsgId);

            if (!ObjectUtils.isNull(aDeferred)) {
                log().trace('... resolve deferred for msg-id ' + sMsgId);
                aDeferred.resolve(jRT1Msg);

                log().trace('... remove deferred from registry');
                aDeferredRegistry.remove(sMsgId);
            } else {
                log().trace('onDocResponse () - no deferred found for msg-id = ' + sMsgId);
            }

            // make sure we only notify if we haven't been disposed
            if (self) {
                log().trace('... forward response msg-id ' + sMsgId + ' to derived class');
                self.trigger(RT2Const.EVENT_DOC_RESPONSE, jRT1Msg);
            }
        }

        //----------------------------------------------------------------------
        function /* json */ impl_translateRT2MsgToRT1(/* RT2Message */ aRT2Msg) {
            var /* json */ jRT1Msg = {};

            jRT1Msg.type   = aRT2Msg.getType();
            jRT1Msg.header = {};
            jRT1Msg.body   = aRT2Msg.getBody();

            impl_translateRT2ErrorToRT1(aRT2Msg, jRT1Msg);
            impl_translateRT2PreviewToRT1(aRT2Msg, jRT1Msg);

            var /* Iterator< string > */ lHeader = aRT2Msg.listHeader().iterator();
            while (lHeader.hasNext()) {
                var /* string */ sHeader = lHeader.next();
                var /* object */ aValue  = aRT2Msg.getHeader(sHeader);
                jRT1Msg.header[sHeader] = aValue;
            }

            log().trace('impl_translateRT2MsgToRT1', jRT1Msg);

            return jRT1Msg;
        }

        //----------------------------------------------------------------------
        // @TODO RUFUS#01a : client should use error header direct
        // @TODO RUFUS#01b : error description should not be part of the error (to much bytes send over the channel ...)
        function /* void */ impl_translateRT2ErrorToRT1(/* RT2Message */ aRT2Msg, /* json */ jRT1Msg) {
            var /* ErrorCode */ aError = RT2MessageGetSet.getError(aRT2Msg);
            jRT1Msg.body.hasErrors = false;

            if (aError.isError()) {
                jRT1Msg.body.hasErrors = true;

                jRT1Msg.body.error = {
                    errorClass:       aError.getErrorClass(),
                    errorCode:        aError.getCode(),
                    error:            aError.getCodeAsStringConstant(),
                    errorDescription: aError.getDescription(),
                    errorValue:       aError.getValue()
                };
            }
        }

        //----------------------------------------------------------------------
        //@TODO RUFUS#01c : client should use header directly to detect preview msgs
        function /* void */ impl_translateRT2PreviewToRT1(/* RT2Message */ aRT2Msg, /* json */ jRT1Msg) {
            var /* boolean */ bIsPreview = RT2MessageGetSet.isPreview(aRT2Msg);
            if (bIsPreview) {
                jRT1Msg.body.preview = bIsPreview;
            }
        }

        //----------------------------------------------------------------------
        /* privae string */ function impl_newMessageId() {
            return randomUUID();
        }

        //----------------------------------------------------------------------
        function /* void */ impl_handleOffline() {
            log().trace('impl_handleOffline');
        }

        //----------------------------------------------------------------------
        function /* void */ impl_handleOnline() {
            log().trace('impl_handleOnline');
        }

        //----------------------------------------------------------------------
        function /* void */ impl_handleError() {
            log().trace('impl_handleError');
            impl_rejectAndCleanAllDeferreds();
        }

        //----------------------------------------------------------------------
        function /* void */ impl_rejectAndCleanAllDeferreds() {
            log().trace('impl_rejectAndCleanAllDeferreds');

            var /* map< String, Deferred > */ aDeferredRegistry   = mem_DeferredRegistry();
            var /* Iterator< String >      */ rDeferredRegistry   = aDeferredRegistry.keySet().iterator();
            m_aDeferredRegistry = null;

            log().trace('... reject all open deferreds');

            while (rDeferredRegistry.hasNext()) {
                try {
                    const sMsgId = rDeferredRegistry.next();
                    var /* Deferred */ aDeferred = aDeferredRegistry.get(sMsgId);
                    log().trace('... reject deferred', aDeferred);
                    aDeferred.reject(null);
                } catch (exIgnore) {
                    log().error(exIgnore);
                }
            }
        }

        //----------------------------------------------------------------------
        function /* map< string, deferred > */ mem_DeferredRegistry() {
            if (ObjectUtils.isNull(m_aDeferredRegistry)) {
                m_aDeferredRegistry = new HashMap();
            }
            return m_aDeferredRegistry;
        }

        //----------------------------------------------------------------------
        function log() {
            if (ObjectUtils.isNull(m_aLog)) {
                m_aLog = Logger.create('RT2DeferredWrapper', Rt2LoggerContextMain);
            }
            return m_aLog;
        }

        // initialization -----------------------------------------------------

        m_aRT2Inst = this.member(new RT2(aInitOptions));

        // define callback for RT2 response messages
        m_aRT2Inst.on(RT2Const.EVENT_DOC_RESPONSE, impl_onDocResponse);

        // listen for other possible RT2 communication events and notify them further
        for (const eventType of RT2Const.EVENT_DEFINITION_LIST) {
            m_aRT2Inst.on(eventType, function (jData) {
                if (StringUtils.equals(eventType, RT2Const.EVENT_OFFLINE)) {
                    impl_handleOffline();
                } else if (StringUtils.equals(eventType, RT2Const.EVENT_ONLINE)) {
                    impl_handleOnline();
                } else if (StringUtils.equals(eventType, RT2Const.EVENT_ERROR)) {
                    impl_handleError();
                }

                log().trace('forward event to derived class ...', [jData]);

                // protected against destroyed instnace due to called
                // handler before.
                if (!ObjectUtils.isNull(self)) {
                    self.trigger(eventType, jData);
                } else {
                    log().warn('event received but own instance has been destroyed!');
                }
            });
        }

        this.registerDestructor(function () {
            log().trace('RT2DeferredWrapper.dtor() called ...');
            impl_rejectAndCleanAllDeferreds();
        });
    }
}
