/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';

//--------------------------------------------------------------------------
// const

//--------------------------------------------------------------------------
/* class RT2InitOptions
    */
function RT2InitOptions() {
    //----------------------------------------------------------------------
    var /* object */ self = this;

    //----------------------------------------------------------------------
    var /* json */ m_jOptions = {};

    //----------------------------------------------------------------------
    /* public void */ this.takeOver = function (/* RT2InitOptions */ aInitOptions) {
        // a) no options at all -> ignore it
        if (ObjectUtils.isNull(aInitOptions)) {
            return;
        }

        // b) not an RT2InitOptions object ! -> ignore it
        if (!ObjectUtils.isFunction(aInitOptions.getOption)) {
            return;
        }

        // c) overwrite exiting options with options defined within given object
        impl_takeOverOptionIfValid(RT2InitOptions.INITOPT_TYPE,           aInitOptions.getOption(RT2InitOptions.INITOPT_TYPE,           undefined));
        impl_takeOverOptionIfValid(RT2InitOptions.INITOPT_CLIENT_UID,     aInitOptions.getOption(RT2InitOptions.INITOPT_CLIENT_UID,     undefined));
        impl_takeOverOptionIfValid(RT2InitOptions.INITOPT_DOC_ID_PREFIX,  aInitOptions.getOption(RT2InitOptions.INITOPT_DOC_ID_PREFIX,  ''));
        impl_takeOverOptionIfValid(RT2InitOptions.INITOPT_DOC_UID,        aInitOptions.getOption(RT2InitOptions.INITOPT_DOC_UID,        undefined));
        impl_takeOverOptionIfValid(RT2InitOptions.INITOPT_DOC_TYPE,       aInitOptions.getOption(RT2InitOptions.INITOPT_DOC_TYPE,       undefined));
        impl_takeOverOptionIfValid(RT2InitOptions.INITOPT_DRIVE_DOC_ID,   aInitOptions.getOption(RT2InitOptions.INITOPT_DRIVE_DOC_ID,   undefined));
        impl_takeOverOptionIfValid(RT2InitOptions.INITOPT_FOLDER_ID,      aInitOptions.getOption(RT2InitOptions.INITOPT_FOLDER_ID,      undefined));
        impl_takeOverOptionIfValid(RT2InitOptions.INITOPT_FILE_ID,        aInitOptions.getOption(RT2InitOptions.INITOPT_FILE_ID,        undefined));
        impl_takeOverOptionIfValid(RT2InitOptions.INITOPT_SEQNR_REQUEST,  aInitOptions.getOption(RT2InitOptions.INITOPT_SEQNR_REQUEST,  undefined));
        impl_takeOverOptionIfValid(RT2InitOptions.INITOPT_SEQNR_RESPONSE, aInitOptions.getOption(RT2InitOptions.INITOPT_SEQNR_RESPONSE, undefined));
    };

    //----------------------------------------------------------------------
    /* public void */ this.setType = function (/* string */ sType) {
        impl_setOption(RT2InitOptions.INITOPT_TYPE, sType);
    };

    //----------------------------------------------------------------------
    /* public string */ this.getType = function () {
        return impl_getOption(RT2InitOptions.INITOPT_TYPE, RT2InitOptions.INITTYPE_STANDARD);
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.isType = function (/* string */ sType) {
        var /* string  */ sCurrentType = self.getType();
        var /* boolean */ bIsType      = StringUtils.equalsIgnoreCase(sCurrentType, sType);
        return bIsType;
    };

    //----------------------------------------------------------------------
    /* public void */ this.setOption = function (/* string */ sOption, /* object */ aValue) {
        impl_setOption(sOption, aValue);
    };

    //----------------------------------------------------------------------
    /* public object */ this.getOption = function (/* string */ sOption, /* object */ aDefault) {
        return impl_getOption(sOption, aDefault);
    };

    //----------------------------------------------------------------------
    /* private */ function /* void */ impl_setOption(/* string */ sOption, /* object */ aValue) {
        var /* json */ jOpts = mem_Options();
        jOpts[sOption] = aValue;
    }

    //----------------------------------------------------------------------
    /* private */ function /* void */ impl_takeOverOptionIfValid(/* string */ sOption, /* object */ aValue) {
        if (aValue !== undefined) { // don't check for null as it is a valid value !
            impl_setOption(sOption, aValue);
        }
    }

    //----------------------------------------------------------------------
    /* private */ function /* object */ impl_getOption(/* string */ sOption, /* object */ aDefault) {
        var /* json   */ jOpts  = mem_Options();
        var /* object */ aValue = ObjectUtils.getObjectPartOrDefault(jOpts, sOption, aDefault);
        return aValue;
    }

    //----------------------------------------------------------------------
    /* private */ function /* json */ mem_Options() {
        if (ObjectUtils.isNull(m_jOptions)) {
            m_jOptions = {};
        }
        return m_jOptions;
    }
} // class RT2InitOptions

/* string */ RT2InitOptions.INITTYPE_STANDARD      = 'standard';
/* string */ RT2InitOptions.INITTYPE_REATTACH      = 're-attach';

/* string */ RT2InitOptions.INITOPT_TYPE           = 'type';
/* string */ RT2InitOptions.INITOPT_CLIENT_UID     = 'client_uid';
/* string */ RT2InitOptions.INITOPT_DOC_UID        = 'doc_uid';
/* string */ RT2InitOptions.INITOPT_DOC_TYPE       = 'doc_type';
/* string */ RT2InitOptions.INITOPT_DOC_ID_PREFIX  = 'doc_id_prefix';
/* string */ RT2InitOptions.INITOPT_DRIVE_DOC_ID   = 'drive_doc_id';
/* string */ RT2InitOptions.INITOPT_FOLDER_ID      = 'folder_id';
/* string */ RT2InitOptions.INITOPT_FILE_ID        = 'file_id';
/* string */ RT2InitOptions.INITOPT_SEQNR_REQUEST  = 'seqnr_request';
/* string */ RT2InitOptions.INITOPT_SEQNR_RESPONSE = 'seqnr_response';

//--------------------------------------------------------------------------
// exports

export default RT2InitOptions;
