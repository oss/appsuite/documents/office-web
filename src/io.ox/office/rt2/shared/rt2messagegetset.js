/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';
import RT2MessageHeaderCheck from '@/io.ox/office/rt2/shared/rt2messageheadercheck';
import EDocState from '@/io.ox/office/rt2/shared/edocstate';
import ErrorCode from '@/io.ox/office/rt2/shared/errorcode';

//--------------------------------------------------------------------------
// const
var NO_DESCRIPTION = '';

//--------------------------------------------------------------------------
// class

function RT2MessageGetSet() {
} // class RT2MessageGetSet

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getType = function (/* RT2Message */ aMessage) {
    return aMessage.getType();
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getMessageID = function (/* RT2Message */ aMessage) {
    return aMessage.getMessageID();
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setMessageID = function (/* RT2Message */ aMessage, /* string */ sMsgID) {
    aMessage.setMessageID(sMsgID);
};

//-------------------------------------------------------------------------NO_ERRORVALUE
/* public static void */ RT2MessageGetSet.newMessageID = function (/* RT2Message */ aMessage) {
    aMessage.newMessageID();
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setSessionID = function (/* RT2Message */ aMessage, /* string */ sSessionID) {
    RT2MessageGetSet.setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_SESSION_ID, sSessionID);
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getSessionID = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_SESSION_ID);
};

//-------------------------------------------------------------------------
/* public static Integer */ RT2MessageGetSet.getSeqNumber = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_SEQ_NR);
};

//-------------------------------------------------------------------------
/* public static boolean */ RT2MessageGetSet.hasSeqNumber = function (/* RT2Message */ aMessage) {
    var /* Integer */ nSeqNrCheck = RT2MessageGetSet.getOptionalHeader(aMessage, RT2Protocol.HEADER_SEQ_NR, null);
    var /* boolean */ bHasSeqNr   = ObjectUtils.isNotNull(nSeqNrCheck);
    return bHasSeqNr;
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setSeqNumber = function (/* RT2Message */ aMessage, /* int */ nNr) {
    RT2MessageGetSet.setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_SEQ_NR, nNr);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setClientUID = function (/* RT2Message */ aMessage, /* string */ sClientUID) {
    RT2MessageGetSet.setMandatoryHeader(aMessage, RT2Protocol.HEADER_CLIENT_UID, sClientUID);
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getClientUID = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getMandatoryHeader(aMessage, RT2Protocol.HEADER_CLIENT_UID);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setOldClientUID = function (/* RT2Message */ aMessage, /* string */ sClientUID) {
    RT2MessageGetSet.setMandatoryHeader(aMessage, RT2Protocol.HEADER_OLD_CLIENT_UID, sClientUID);
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getOldClientUID = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getMandatoryHeader(aMessage, RT2Protocol.HEADER_OLD_CLIENT_UID);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setDriveDocID = function (/* RT2Message */ aMessage, /*  string */ sDriveDocID) {
    RT2MessageGetSet.setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DRIVE_DOC_ID, sDriveDocID);
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getDriveDocID = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DRIVE_DOC_ID);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setFolderID = function (/* RT2Message */ aMessage, /* string */ sFolderID) {
    RT2MessageGetSet.setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_FOLDER_ID, sFolderID);
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getFolderID = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_FOLDER_ID);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setFileID = function (/* RT2Message */ aMessage, /* string */ sFileID) {
    RT2MessageGetSet.setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_FILE_ID, sFileID);
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getFileID = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_FILE_ID);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setDocUID = function (/* RT2Message */ aMessage, /* string */ sDocUID) {
    RT2MessageGetSet.setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DOC_UID, sDocUID);
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getDocUID = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DOC_UID);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setDocType = function (/* RT2Message */ aMessage, /* string */ sDocType) {
    RT2MessageGetSet.setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DOC_TYPE, sDocType);
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getDocType = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DOC_TYPE);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setInternalDocType = function (/* RT2Message */ aMessage, /* int */ nDocType) {
    RT2MessageGetSet.setInternalHeader(aMessage, RT2Protocol.HEADER_INTERNAL_DOCTYPE, nDocType);
};

//-------------------------------------------------------------------------
/* public static Integer */ RT2MessageGetSet.getInternalDocType = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getInternalHeader(aMessage, RT2Protocol.HEADER_INTERNAL_DOCTYPE, null);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setDocState = function (/* RT2Message */ aMessage, /* EDocState */ eState) {
    var /* String */ sState = EDocState.toString(eState);
    RT2MessageGetSet.setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DOC_STATE, sState);
};

//-------------------------------------------------------------------------
/* public static EDocState */ RT2MessageGetSet.getDocState = function (/* RT2Message */ aMessage) {
    var /* String    */ sState = RT2MessageGetSet.getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DOC_STATE);
    var /* EDocState */ eState = EDocState.fromString(sState);
    return eState;
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.markAsPreview = function (/* RT2Message */ aMessage) {
    RT2MessageGetSet.setOptionalHeader(aMessage, RT2Protocol.HEADER_PREVIEW, Boolean.TRUE);
};

//-------------------------------------------------------------------------
/* public static boolean */ RT2MessageGetSet.isPreview = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getOptionalHeader(aMessage, RT2Protocol.HEADER_PREVIEW, Boolean.FALSE);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setFastEmpty = function (/* RT2Message */ aMessage, /* boolean */ bFastEmpty) {
    RT2MessageGetSet.setOptionalHeader(aMessage, RT2Protocol.HEADER_FAST_EMPTY, bFastEmpty);
};

//-------------------------------------------------------------------------
/* public static Boolean */ RT2MessageGetSet.getFastEmpty = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getOptionalHeader(aMessage, RT2Protocol.HEADER_FAST_EMPTY, Boolean.FALSE);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setFastEmptyOSN = function (/* RT2Message */ aMessage, /* int */ nFastEmptyOSN) {
    RT2MessageGetSet.setOptionalHeader(aMessage, RT2Protocol.HEADER_FAST_EMPTY_OSN, nFastEmptyOSN);
};

//-------------------------------------------------------------------------
/* public static Integer */ RT2MessageGetSet.getFastEmptyOSN = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getOptionalHeader(aMessage, RT2Protocol.HEADER_FAST_EMPTY_OSN, -1);
};

//-------------------------------------------------------------------------
/* public static Boolean */ RT2MessageGetSet.getNewDocState = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getOptionalHeader(aMessage, RT2Protocol.HEADER_NEW_DOC, Boolean.FALSE);
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getUserAgent = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getOptionalHeader(aMessage, RT2Protocol.HEADER_USERAGENT, null);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setUserAgent = function (/* RT2Message */ aMessage, /* string */ sJSessionID) {
    RT2MessageGetSet.setOptionalHeader(aMessage, RT2Protocol.HEADER_USERAGENT, sJSessionID);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setAppAction = function (/* RT2Message */ aMessage, /* string */ sAction) {
    RT2MessageGetSet.setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_APP_ACTION, sAction);
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getAppAction = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_APP_ACTION);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setAdminID = function (/* RT2Message */ aMessage, /* string */ sAdminID) {
    RT2MessageGetSet.setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_ADMIN_ID, sAdminID);
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageGetSet.getAdminID = function (/* RT2Message */ aMessage) {
    return RT2MessageGetSet.getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_ADMIN_ID);
};

//-------------------------------------------------------------------------
/* public static void */ RT2MessageGetSet.setError = function (/* RT2Message */ aMessage, /* ErrorCode */ aError) {
    if (ObjectUtils.isNull(aError) || aError.isNoError()) {
        return;
    }

    RT2MessageGetSet.setOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_CLASS, aError.getErrorClass());
    RT2MessageGetSet.setOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_CODE,  aError.getCode());
    RT2MessageGetSet.setOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_NAME,  aError.getCodeAsStringConstant());
};

//-------------------------------------------------------------------------
/* public static ErrorCode */ RT2MessageGetSet.getError = function (/* RT2Message */ aMessage) {
    var /* Integer   */ nErrorClass       = RT2MessageGetSet.getOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_CLASS, ErrorCode.NO_ERROR.getErrorClass());
    var /* Integer   */ nErrorCode        = RT2MessageGetSet.getOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_CODE,  ErrorCode.NO_ERROR.getCode());
    var /* String    */ sErrorName        = RT2MessageGetSet.getOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_NAME,  ErrorCode.NO_ERROR.getCodeAsStringConstant());
    var /* String    */ sErrorValue       = RT2MessageGetSet.getOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_VALUE);
    var /* ErrorCode */ aError            = ErrorCode.create(nErrorCode, sErrorName, NO_DESCRIPTION, nErrorClass, sErrorValue);
    return aError;
};

//-------------------------------------------------------------------------
/* public static boolean */ RT2MessageGetSet.isError = function (/* RT2Message */ aMessage) {
    var /* ErrorCode */ aError = RT2MessageGetSet.getError(aMessage);
    return !ObjectUtils.isNull(aError) && (aError !== ErrorCode.NO_ERROR);
};

//-------------------------------------------------------------------------
/* public static < T > void */ RT2MessageGetSet.setMandatoryHeader = function (/* RT2Message */ aMessage, /* string */ sHeader, /* T */ aValue) {
    var /* int */ nFlag  = RT2Protocol.get().getFlag4Header(sHeader);
    aMessage.setHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY, aValue, nFlag);
};

//-------------------------------------------------------------------------
/* public static < T > T */ RT2MessageGetSet.getMandatoryHeader = function (/* RT2Message */ aMessage, /* string */ sHeader) {
    var /* int */ nFlag  = RT2Protocol.get().getFlag4Header(sHeader);
    var /* T   */ aValue = aMessage.getHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY, nFlag);
    return aValue;
};

//-------------------------------------------------------------------------
/* public static < T > void */ RT2MessageGetSet.setFlagMandatoryHeader = function (/* RT2Message */ aMessage, /* string */ sHeader, /* T */ aValue) {
    var /* int */ nFlag  = RT2Protocol.get().getFlag4Header(sHeader);
    aMessage.setHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, aValue, nFlag);
};

//-------------------------------------------------------------------------
/* public static < T > T */ RT2MessageGetSet.getFlagMandatoryHeader = function (/* RT2Message */ aMessage, /* string */ sHeader) {
    var /* int */ nFlag  = RT2Protocol.get().getFlag4Header(sHeader);
    var /* T   */ aValue = aMessage.getHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, nFlag);
    return aValue;
};

//-------------------------------------------------------------------------
/* public static < T > void */ RT2MessageGetSet.setOptionalHeader = function (/* RT2Message */ aMessage, /* string */ sHeader, /* T */ aValue) {
    aMessage.setHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_OPTIONAL, aValue, 0);
};

//-------------------------------------------------------------------------
/* public static < T > T */ RT2MessageGetSet.getOptionalHeader = function (/* RT2Message */ aMessage, /* string */ sHeader, /* T */ aDefault) {
    var /* T */ aValue = aMessage.getHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_OPTIONAL, 0);
    if (ObjectUtils.isNull(aValue)) {
        aValue = aDefault;
    }
    return aValue;
};

//-------------------------------------------------------------------------
/* public static < T > void */ RT2MessageGetSet.setInternalHeader = function (/* RT2Message */ aMessage, /* string */ sHeader, /* T */ aValue) {
    var /* int */ nFlag  = RT2Protocol.get().getFlag4Header(sHeader);
    aMessage.setHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, aValue, nFlag);
};

//-------------------------------------------------------------------------
/* public static < T > T */ RT2MessageGetSet.getInternalHeader = function (/* RT2Message */ aMessage, /* string */ sHeader, /* T */ aDefault) {
    var /* int */ nFlag  = RT2Protocol.get().getFlag4Header(sHeader);
    var /* T   */ aValue = aMessage.getHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, nFlag);
    if (ObjectUtils.isNull(aValue)) {
        aValue = aDefault;
    }
    return aValue;
};

//--------------------------------------------------------------------------
// exports

export default RT2MessageGetSet;
