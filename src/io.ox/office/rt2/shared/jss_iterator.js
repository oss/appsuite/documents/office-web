/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';

//--------------------------------------------------------------------------
// const

//--------------------------------------------------------------------------
/* class Iterator< T >
    */
function Iterator() {
    //----------------------------------------------------------------------

    var /* T[] */ m_lData = null;

    var /* int */ m_nIterator = 0;

    //----------------------------------------------------------------------
    /* protected void */ this.init = function (/* T[] */ lData) {
        m_lData = lData;
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.hasNext = function () {
        var /* int     */ nSize = mem_Data().length;
        var /* boolean */ bHas  = (m_nIterator < nSize);
        return bHas;
    };

    //----------------------------------------------------------------------
    /* public T */ this.next = function () {
        var /* T[] */ lData = mem_Data();
        var /* T   */ aData = lData[m_nIterator];
        m_nIterator++;
        return aData;
    };

    //----------------------------------------------------------------------
    /* public String */ this.toString = function () {
        var /* T[] */ lItems = mem_Data();
        return `[${lItems.join(', ')}] : iterator = ${m_nIterator}`;
    };

    //----------------------------------------------------------------------
    /* private T[] */ function mem_Data() {
        if (ObjectUtils.isNull(m_lData)) {
            m_lData = [];
        }
        return m_lData;
    }

} // class Iterator

//--------------------------------------------------------------------------
/* public static Iterator< T > */ Iterator.create = function (/* T[] */ lData) {
    var /* Iterator< T > */ aIterator = new Iterator();
    aIterator.init(lData);
    return aIterator;
};

//--------------------------------------------------------------------------
// exports

export default Iterator;
