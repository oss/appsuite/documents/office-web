/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';
import HashMap from '@/io.ox/office/rt2/shared/jss_hashmap';
import RT2MessageFlags from '@/io.ox/office/rt2/shared/rt2messageflags';

//--------------------------------------------------------------------------
// class

function RT2Protocol() {
    //----------------------------------------------------------------------

    var /* map< string, string > */ m_aEncoder     = null;

    var /* map< string, string > */ m_aDecoder     = null;

    var /* map< string, int    > */ m_aType2Flags  = null;

    var /* map< string, int    > */ m_aHeader2Flag = null;

    //----------------------------------------------------------------------
    /* public string */ this.encode = function (/* string */ sKey) {
        var /* string */ sValue = m_aEncoder.get(sKey);

        if (StringUtils.isEmpty(sValue)) {
            throw new Error(`RT2Protocol error: no encoded value for "${sKey}"`);
        }
        return sValue;
    };

    //----------------------------------------------------------------------
    /* public string */ this.decode = function (/* string */ sKey) {
        var /* string */ sValue = m_aDecoder.get(sKey);

        if (StringUtils.isEmpty(sValue)) {
            throw new Error(`RT2Protocol error: no decoded value for "${sKey}"`);
        }
        return sValue;
    };

    //----------------------------------------------------------------------
    /* public int */ this.getFlags4Type = function (/* string */ sType) {
        var /* int */ nFlags = m_aType2Flags.get(sType);

        if (ObjectUtils.isNull(nFlags)) {
            throw new Error(`RT2Protocol error: no flags value for type "${sType}"`);
        }
        return nFlags;
    };

    //-------------------------------------------------------------------------
    /* public int */ this.getFlag4Header = function (/* string*/ sHeader) {
        if (
            (StringUtils.startsWithIgnoreCase(sHeader, RT2Protocol.HEADER_PREFIX_JMS)) ||
            (StringUtils.startsWithIgnoreCase(sHeader, RT2Protocol.HEADER_PREFIX_DEBUG)) ||
            (StringUtils.startsWithIgnoreCase(sHeader, RT2Protocol.HEADER_PREFIX_INTERNAL)) ||
            (StringUtils.equalsIgnoreCase(sHeader, 'breadcrumbId')) // special header added by camel ... we need to ignore here !
        ) {
            return RT2MessageFlags.FLAG_NONE;
        }

        var /* int */ nFlag = m_aHeader2Flag.get(sHeader);

        if (ObjectUtils.isNull(nFlag)) {
            throw new Error(`RT2Protocol error: no flag value for header "${sHeader}"`);
        }
        return nFlag;
    };

    //----------------------------------------------------------------------
    /* public void */ this.init = function () {
        if (
            !ObjectUtils.isNull(m_aEncoder) &&
            !ObjectUtils.isNull(m_aDecoder) &&
            !ObjectUtils.isNull(m_aType2Flags) &&
            !ObjectUtils.isNull(m_aHeader2Flag)
        ) {
            return;
        }

        m_aEncoder     = impl_initEncoder();
        m_aDecoder     = impl_initDecoder();
        m_aType2Flags  = impl_initType2Flags();
        m_aHeader2Flag = impl_initHeader2Flag();
    };

    //----------------------------------------------------------------------
    /* private map< string, string > */ function impl_initEncoder() {
        var /* map< string, string > */ aEncoder = new HashMap();

        aEncoder.put('ping', 'ping');
        aEncoder.put('pong', 'pong');
        aEncoder.put('simple_ack', 'ak0');
        aEncoder.put('error_ack', 'ak1');
        aEncoder.put('simple_nack', 'nk0');
        aEncoder.put('close_doc_route_admintask', 'at0');
        aEncoder.put('close_doc_route_admintask_completed', 'at1');
        aEncoder.put('cleanup_for_crashed_node_admintask', 'at2');
        aEncoder.put('cleanup_for_crashed_node_admintask_completed', 'at3');
        aEncoder.put('join_request', 'rq0');
        aEncoder.put('leave_request', 'rq1');
        aEncoder.put('open_doc_request', 'rq2');
        aEncoder.put('save_doc_request', 'rq3');
        aEncoder.put('close_doc_request', 'rq4');
        aEncoder.put('apply_ops_request', 'rq5');
        aEncoder.put('app_action_request', 'rq6');
        aEncoder.put('switch_docstate_request', 'rq7');
        aEncoder.put('update_slide_request', 'rq8');
        aEncoder.put('reset_request', 'rq9');
        aEncoder.put('sync_request', 'rq10');
        aEncoder.put('editrights_request', 'rq11');
        aEncoder.put('unavailability_request', 'rq12');
        aEncoder.put('abort_open_request', 'rq13');
        aEncoder.put('emergency_leave_request', 'rq14');
        aEncoder.put('sync_stable_request', 'rq15');
        aEncoder.put('close_and_leave_request', 'rq16');
        aEncoder.put('generic_error_response', 'rp0');
        aEncoder.put('join_response', 'rp1');
        aEncoder.put('leave_response', 'rp2');
        aEncoder.put('open_doc_response', 'rp3');
        aEncoder.put('open_doc_chunk_response', 'rp4');
        aEncoder.put('save_doc_response', 'rp5');
        aEncoder.put('close_doc_response', 'rp6');
        aEncoder.put('apply_ops_response', 'rp7');
        aEncoder.put('app_action_response', 'rp8');
        aEncoder.put('switch_docstate_response', 'rp9');
        aEncoder.put('update_slide_response', 'rp10');
        aEncoder.put('reset_response', 'rp11');
        aEncoder.put('sync_response', 'rp12');
        aEncoder.put('editrights_response', 'rp13');
        aEncoder.put('unavailability_response', 'rp14');
        aEncoder.put('abort_open_response', 'rp15');
        aEncoder.put('emergency_leave_response', 'rp16');
        aEncoder.put('sync_stable_response', 'rp17');
        aEncoder.put('close_and_leave_response', 'rp18');
        aEncoder.put('update_broadcast', 'bc0');
        aEncoder.put('update_clients_broadcast', 'bc1');
        aEncoder.put('chat_message_broadcast', 'bc2');
        aEncoder.put('editrequest_state_broadcast', 'bc3');
        aEncoder.put('shutdown_broadcast', 'bc4');
        aEncoder.put('crashed_broadcast', 'bc5');
        aEncoder.put('renamed_reload_broadcast', 'bc6');
        aEncoder.put('hangup_broadcast', 'bc7');
        aEncoder.put('ot_reload_broadcast', 'bc8');
        aEncoder.put('file_moved_reload_broadcast', 'bc9');
        aEncoder.put('msg_id_header', 'h0');
        aEncoder.put('seq_nr_header', 'h1');
        aEncoder.put('client_uid_header', 'h2');
        aEncoder.put('session_id_header', 'h3');
        aEncoder.put('doc_uid_header', 'h4');
        aEncoder.put('drive_doc_id_header', 'h5');
        aEncoder.put('folder_id_header', 'h6');
        aEncoder.put('file_id_header', 'h7');
        aEncoder.put('doc_type_header', 'h8');
        aEncoder.put('doc_state_header', 'h9');
        aEncoder.put('app_action_header', 'h10');
        aEncoder.put('useragent_header', 'h11');
        aEncoder.put('node_uid_header', 'h12');
        aEncoder.put('storage_version_header', 'h13');
        aEncoder.put('storage_osn_header', 'h14');
        aEncoder.put('old_client_uid_header', 'h15');
        aEncoder.put('preview_header', 'h16');
        aEncoder.put('err_class_header', 'h17');
        aEncoder.put('err_code_header', 'h18');
        aEncoder.put('err_name_header', 'h19');
        aEncoder.put('err_desc_header', 'h20');
        aEncoder.put('err_value_header', 'h21');
        aEncoder.put('fast_empty_header', 'h22');
        aEncoder.put('fast_empty_osn_header', 'h23');
        aEncoder.put('active_slide_header', 'h24');
        aEncoder.put('auth_code_header', 'h25');
        aEncoder.put('new_doc_header', 'h26');
        aEncoder.put('auto_close_header', 'h27');
        aEncoder.put('no_restore_header', 'h28');
        aEncoder.put('unavail_time_header', 'h29');
        aEncoder.put('ack_start_header', 'h30');
        aEncoder.put('ack_end_header', 'h31');
        aEncoder.put('nack_start_header', 'h32');
        aEncoder.put('nack_end_header', 'h33');
        aEncoder.put('admin_id_header', 'h34');
        aEncoder.put('admin_hz_member_uuid_header', 'h35');
        aEncoder.put('admin_hz_master_uuid_header', 'h36');
        aEncoder.put('cause_header', 'h37');
        aEncoder.put('origin_hostname_header', 'h38');
        aEncoder.put('advisory_lock_header', 'h39');
        aEncoder.put('internal_chunk_header', 'hi0');
        aEncoder.put('internal_doctype_header', 'hi1');
        aEncoder.put('internal_recipients_header', 'hi2');
        aEncoder.put('internal_client_ep_id_header', 'hi3');
        aEncoder.put('internal_client_ep_uri_header', 'hi4');
        aEncoder.put('internal_time_queued_header', 'hi5');
        aEncoder.put('internal_force_header', 'hi6');
        aEncoder.put('internal_referrer_header', 'hi7');
        aEncoder.put('internal_is_response_header', 'hi8');
        aEncoder.put('internal_is_broadcast_header', 'hi9');
        aEncoder.put('dbg_seqnum_header', 'hd0');
        aEncoder.put('dbg_disable_session_validation_header', 'hd1');
        aEncoder.put('dbg_perf_client_in_header', 'hd2');
        aEncoder.put('dbg_perf_client_out_header', 'hd3');
        aEncoder.put('dbg_perf_server_in_header', 'hd4');
        aEncoder.put('dbg_perf_server_out_header', 'hd5');
        aEncoder.put('dbg_perf_server_load_start_header', 'hd6');
        aEncoder.put('dbg_perf_server_load_end_header', 'hd7');
        aEncoder.put('dbg_perf_server_applyops_start_header', 'hd8');
        aEncoder.put('dbg_perf_server_applyops_end_header', 'hd9');
        aEncoder.put('dbg_perf_server_save_start_header', 'hd10');
        aEncoder.put('dbg_perf_server_save_end_header', 'hd11');
        aEncoder.put('dbg_perf_server_recentlist_start_header', 'hd12');
        aEncoder.put('dbg_perf_server_recentlist_end_header', 'hd13');
        aEncoder.put('dbg_perf_server_sequencer_start_header', 'hd14');
        aEncoder.put('dbg_perf_server_sequencer_end_header', 'hd15');
        aEncoder.put('dbg_perf_server_route_doc_in_header', 'hd16');
        aEncoder.put('dbg_perf_server_route_doc_out_header', 'hd17');
        aEncoder.put('dbg_stat_node_id_header', 'hd18');

        return aEncoder;
    }

    //----------------------------------------------------------------------
    /* private map< string, string > */ function impl_initDecoder() {
        var /* map< string, string > */ aDecoder = new HashMap();

        aDecoder.put('ping', 'ping');
        aDecoder.put('pong', 'pong');
        aDecoder.put('ak0', 'simple_ack');
        aDecoder.put('ak1', 'error_ack');
        aDecoder.put('nk0', 'simple_nack');
        aDecoder.put('at0', 'close_doc_route_admintask');
        aDecoder.put('at1', 'close_doc_route_admintask_completed');
        aDecoder.put('at2', 'cleanup_for_crashed_node_admintask');
        aDecoder.put('at3', 'cleanup_for_crashed_node_admintask_completed');
        aDecoder.put('rq0', 'join_request');
        aDecoder.put('rq1', 'leave_request');
        aDecoder.put('rq2', 'open_doc_request');
        aDecoder.put('rq3', 'save_doc_request');
        aDecoder.put('rq4', 'close_doc_request');
        aDecoder.put('rq5', 'apply_ops_request');
        aDecoder.put('rq6', 'app_action_request');
        aDecoder.put('rq7', 'switch_docstate_request');
        aDecoder.put('rq8', 'update_slide_request');
        aDecoder.put('rq9', 'reset_request');
        aDecoder.put('rq10', 'sync_request');
        aDecoder.put('rq11', 'editrights_request');
        aDecoder.put('rq12', 'unavailability_request');
        aDecoder.put('rq13', 'abort_open_request');
        aDecoder.put('rq14', 'emergency_leave_request');
        aDecoder.put('rq15', 'sync_stable_request');
        aDecoder.put('rq16', 'close_and_leave_request');
        aDecoder.put('rp0', 'generic_error_response');
        aDecoder.put('rp1', 'join_response');
        aDecoder.put('rp2', 'leave_response');
        aDecoder.put('rp3', 'open_doc_response');
        aDecoder.put('rp4', 'open_doc_chunk_response');
        aDecoder.put('rp5', 'save_doc_response');
        aDecoder.put('rp6', 'close_doc_response');
        aDecoder.put('rp7', 'apply_ops_response');
        aDecoder.put('rp8', 'app_action_response');
        aDecoder.put('rp9', 'switch_docstate_response');
        aDecoder.put('rp10', 'update_slide_response');
        aDecoder.put('rp11', 'reset_response');
        aDecoder.put('rp12', 'sync_response');
        aDecoder.put('rp13', 'editrights_response');
        aDecoder.put('rp14', 'unavailability_response');
        aDecoder.put('rp15', 'abort_open_response');
        aDecoder.put('rp16', 'emergency_leave_response');
        aDecoder.put('rp17', 'sync_stable_response');
        aDecoder.put('rp18', 'close_and_leave_response');
        aDecoder.put('bc0', 'update_broadcast');
        aDecoder.put('bc1', 'update_clients_broadcast');
        aDecoder.put('bc2', 'chat_message_broadcast');
        aDecoder.put('bc3', 'editrequest_state_broadcast');
        aDecoder.put('bc4', 'shutdown_broadcast');
        aDecoder.put('bc5', 'crashed_broadcast');
        aDecoder.put('bc6', 'renamed_reload_broadcast');
        aDecoder.put('bc7', 'hangup_broadcast');
        aDecoder.put('bc8', 'ot_reload_broadcast');
        aDecoder.put('bc9', 'file_moved_reload_broadcast');
        aDecoder.put('h0', 'msg_id_header');
        aDecoder.put('h1', 'seq_nr_header');
        aDecoder.put('h2', 'client_uid_header');
        aDecoder.put('h3', 'session_id_header');
        aDecoder.put('h4', 'doc_uid_header');
        aDecoder.put('h5', 'drive_doc_id_header');
        aDecoder.put('h6', 'folder_id_header');
        aDecoder.put('h7', 'file_id_header');
        aDecoder.put('h8', 'doc_type_header');
        aDecoder.put('h9', 'doc_state_header');
        aDecoder.put('h10', 'app_action_header');
        aDecoder.put('h11', 'useragent_header');
        aDecoder.put('h12', 'node_uid_header');
        aDecoder.put('h13', 'storage_version_header');
        aDecoder.put('h14', 'storage_osn_header');
        aDecoder.put('h15', 'old_client_uid_header');
        aDecoder.put('h16', 'preview_header');
        aDecoder.put('h17', 'err_class_header');
        aDecoder.put('h18', 'err_code_header');
        aDecoder.put('h19', 'err_name_header');
        aDecoder.put('h20', 'err_desc_header');
        aDecoder.put('h21', 'err_value_header');
        aDecoder.put('h22', 'fast_empty_header');
        aDecoder.put('h23', 'fast_empty_osn_header');
        aDecoder.put('h24', 'active_slide_header');
        aDecoder.put('h25', 'auth_code_header');
        aDecoder.put('h26', 'new_doc_header');
        aDecoder.put('h27', 'auto_close_header');
        aDecoder.put('h28', 'no_restore_header');
        aDecoder.put('h29', 'unavail_time_header');
        aDecoder.put('h30', 'ack_start_header');
        aDecoder.put('h31', 'ack_end_header');
        aDecoder.put('h32', 'nack_start_header');
        aDecoder.put('h33', 'nack_end_header');
        aDecoder.put('h34', 'admin_id_header');
        aDecoder.put('h35', 'admin_hz_member_uuid_header');
        aDecoder.put('h36', 'admin_hz_master_uuid_header');
        aDecoder.put('h37', 'cause_header');
        aDecoder.put('h38', 'origin_hostname_header');
        aDecoder.put('h39', 'advisory_lock_header');
        aDecoder.put('hi0', 'internal_chunk_header');
        aDecoder.put('hi1', 'internal_doctype_header');
        aDecoder.put('hi2', 'internal_recipients_header');
        aDecoder.put('hi3', 'internal_client_ep_id_header');
        aDecoder.put('hi4', 'internal_client_ep_uri_header');
        aDecoder.put('hi5', 'internal_time_queued_header');
        aDecoder.put('hi6', 'internal_force_header');
        aDecoder.put('hi7', 'internal_referrer_header');
        aDecoder.put('hi8', 'internal_is_response_header');
        aDecoder.put('hi9', 'internal_is_broadcast_header');
        aDecoder.put('hd0', 'dbg_seqnum_header');
        aDecoder.put('hd1', 'dbg_disable_session_validation_header');
        aDecoder.put('hd2', 'dbg_perf_client_in_header');
        aDecoder.put('hd3', 'dbg_perf_client_out_header');
        aDecoder.put('hd4', 'dbg_perf_server_in_header');
        aDecoder.put('hd5', 'dbg_perf_server_out_header');
        aDecoder.put('hd6', 'dbg_perf_server_load_start_header');
        aDecoder.put('hd7', 'dbg_perf_server_load_end_header');
        aDecoder.put('hd8', 'dbg_perf_server_applyops_start_header');
        aDecoder.put('hd9', 'dbg_perf_server_applyops_end_header');
        aDecoder.put('hd10', 'dbg_perf_server_save_start_header');
        aDecoder.put('hd11', 'dbg_perf_server_save_end_header');
        aDecoder.put('hd12', 'dbg_perf_server_recentlist_start_header');
        aDecoder.put('hd13', 'dbg_perf_server_recentlist_end_header');
        aDecoder.put('hd14', 'dbg_perf_server_sequencer_start_header');
        aDecoder.put('hd15', 'dbg_perf_server_sequencer_end_header');
        aDecoder.put('hd16', 'dbg_perf_server_route_doc_in_header');
        aDecoder.put('hd17', 'dbg_perf_server_route_doc_out_header');
        aDecoder.put('hd18', 'dbg_stat_node_id_header');

        return aDecoder;
    }

    //----------------------------------------------------------------------
    /* private map< string, int > */ function impl_initType2Flags() {
        var /* map< string, int > */ aType2Flags = new HashMap();

        var /* int */ FLAGS_4_JOIN_LEAVE    = RT2MessageFlags.FLAG_SESSION_BASED                                          | RT2MessageFlags.FLAG_DOC_BASED;
        var /* int */ FLAGS_4_DOC_OPS       = RT2MessageFlags.FLAG_SESSION_BASED | RT2MessageFlags.FLAG_SEQUENCE_NR_BASED | RT2MessageFlags.FLAG_DOC_BASED;
        var /* int */ FLAGS_4_SYNC_INTERNAL =                                                                               RT2MessageFlags.FLAG_DOC_BASED;
        var /* int */ FLAGS_4_SYNC          = RT2MessageFlags.FLAG_SESSION_BASED                                          | RT2MessageFlags.FLAG_DOC_BASED;
        var /* int */ FLAGS_4_SYNC_SEQ      = RT2MessageFlags.FLAG_SESSION_BASED | RT2MessageFlags.FLAG_SEQUENCE_NR_BASED | RT2MessageFlags.FLAG_DOC_BASED;
        var /* int */ FLAGS_4_ACKS          = RT2MessageFlags.FLAG_SESSION_BASED                                          | RT2MessageFlags.FLAG_DOC_BASED;
        var /* int */ FLAGS_4_ERROR         = RT2MessageFlags.FLAG_ALL;
        var /* int */ FLAGS_4_NONE          = RT2MessageFlags.FLAG_NONE;
        var /* int */ FLAGS_4_BROADCAST     = RT2MessageFlags.FLAG_DOC_BASED;
        aType2Flags.put(RT2Protocol.PING, FLAGS_4_NONE);
        aType2Flags.put(RT2Protocol.PONG, FLAGS_4_NONE);
        aType2Flags.put(RT2Protocol.ACK_SIMPLE, FLAGS_4_ACKS);
        aType2Flags.put(RT2Protocol.ACK_ERROR, FLAGS_4_ACKS);
        aType2Flags.put(RT2Protocol.NACK_SIMPLE, FLAGS_4_ACKS);
        aType2Flags.put(RT2Protocol.ADMIN_TASK_CLOSE_DOC_ROUTE, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.ADMIN_TASK_COMPLETED_CLOSE_DOC_ROUTE, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.ADMIN_TASK_CLEANUP_FOR_CRASHED_NODE, FLAGS_4_NONE);
        aType2Flags.put(RT2Protocol.ADMIN_TASK_COMPLETED_CLEANUP_FOR_CRASHED_NODE, FLAGS_4_NONE);
        aType2Flags.put(RT2Protocol.REQUEST_JOIN, FLAGS_4_JOIN_LEAVE);
        aType2Flags.put(RT2Protocol.REQUEST_LEAVE, FLAGS_4_JOIN_LEAVE);
        aType2Flags.put(RT2Protocol.REQUEST_OPEN_DOC, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.REQUEST_SAVE_DOC, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.REQUEST_CLOSE_DOC, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.REQUEST_APPLY_OPS, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.REQUEST_APP_ACTION, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.REQUEST_SWITCH_DOCSTATE, FLAGS_4_SYNC_INTERNAL);
        aType2Flags.put(RT2Protocol.REQUEST_UPDATE_SLIDE, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.REQUEST_RESET, FLAGS_4_SYNC);
        aType2Flags.put(RT2Protocol.REQUEST_SYNC, FLAGS_4_SYNC);
        aType2Flags.put(RT2Protocol.REQUEST_SYNC_STABLE, FLAGS_4_SYNC_SEQ);
        aType2Flags.put(RT2Protocol.REQUEST_EDITRIGHTS, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.REQUEST_UNAVAILABILITY, FLAGS_4_SYNC_INTERNAL);
        aType2Flags.put(RT2Protocol.REQUEST_ABORT_OPEN, FLAGS_4_SYNC);
        aType2Flags.put(RT2Protocol.REQUEST_EMERGENCY_LEAVE, FLAGS_4_JOIN_LEAVE);
        aType2Flags.put(RT2Protocol.REQUEST_CLOSE_AND_LEAVE, FLAGS_4_SYNC_INTERNAL);
        aType2Flags.put(RT2Protocol.RESPONSE_GENERIC_ERROR, FLAGS_4_ERROR);
        aType2Flags.put(RT2Protocol.RESPONSE_JOIN, FLAGS_4_JOIN_LEAVE);
        aType2Flags.put(RT2Protocol.RESPONSE_LEAVE, FLAGS_4_JOIN_LEAVE);
        aType2Flags.put(RT2Protocol.RESPONSE_OPEN_DOC, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.RESPONSE_OPEN_DOC_CHUNK, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.RESPONSE_SAVE_DOC, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.RESPONSE_CLOSE_DOC, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.RESPONSE_APPLY_OPS, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.RESPONSE_APP_ACTION, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.RESPONSE_SWITCH_DOCSTATE, FLAGS_4_SYNC_INTERNAL);
        aType2Flags.put(RT2Protocol.RESPONSE_UPDATE_SLIDE, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.RESPONSE_RESET, FLAGS_4_SYNC);
        aType2Flags.put(RT2Protocol.RESPONSE_SYNC, FLAGS_4_SYNC_SEQ);
        aType2Flags.put(RT2Protocol.RESPONSE_SYNC_STABLE, FLAGS_4_SYNC_SEQ);
        aType2Flags.put(RT2Protocol.RESPONSE_EDITRIGHTS, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.RESPONSE_UNAVAILABILITY, FLAGS_4_SYNC_INTERNAL);
        aType2Flags.put(RT2Protocol.RESPONSE_ABORT_OPEN, FLAGS_4_SYNC);
        aType2Flags.put(RT2Protocol.RESPONSE_EMERGENCY_LEAVE, FLAGS_4_JOIN_LEAVE);
        aType2Flags.put(RT2Protocol.RESPONSE_CLOSE_AND_LEAVE, FLAGS_4_SYNC_INTERNAL);
        aType2Flags.put(RT2Protocol.BROADCAST_UPDATE, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.BROADCAST_UPDATE_CLIENTS, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.BROADCAST_CHAT_MESSAGE, FLAGS_4_NONE);
        aType2Flags.put(RT2Protocol.BROADCAST_SHUTDOWN, FLAGS_4_SYNC);
        aType2Flags.put(RT2Protocol.BROADCAST_CRASHED, FLAGS_4_SYNC);
        aType2Flags.put(RT2Protocol.BROADCAST_RENAMED_RELOAD, FLAGS_4_DOC_OPS);
        aType2Flags.put(RT2Protocol.BROADCAST_HANGUP, FLAGS_4_BROADCAST);
        aType2Flags.put(RT2Protocol.BROADCAST_OT_RELOAD, FLAGS_4_BROADCAST);
        aType2Flags.put(RT2Protocol.BROADCAST_FILE_MOVED_RELOAD, FLAGS_4_DOC_OPS);

        return aType2Flags;
    }

    //----------------------------------------------------------------------
    /* private map< string, int > */ function impl_initHeader2Flag() {
        var /* map< string, int > */ aHeader2Flag = new HashMap();

        aHeader2Flag.put(RT2Protocol.HEADER_MSG_ID, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_SEQ_NR, RT2MessageFlags.FLAG_SEQUENCE_NR_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_CLIENT_UID, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_SESSION_ID, RT2MessageFlags.FLAG_SESSION_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_DOC_UID, RT2MessageFlags.FLAG_DOC_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_DRIVE_DOC_ID, RT2MessageFlags.FLAG_DOC_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_FOLDER_ID, RT2MessageFlags.FLAG_DOC_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_FILE_ID, RT2MessageFlags.FLAG_DOC_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_DOC_TYPE, RT2MessageFlags.FLAG_DOC_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_DOC_STATE, RT2MessageFlags.FLAG_DOC_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_APP_ACTION, RT2MessageFlags.FLAG_DOC_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_OLD_CLIENT_UID, RT2MessageFlags.FLAG_DOC_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_USERAGENT, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_UNAVAIL_TIME, RT2MessageFlags.FLAG_DOC_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_NODE_UID, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_AUTO_CLOSE, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_NO_RESTORE, RT2MessageFlags.FLAG_DOC_BASED);
        aHeader2Flag.put(RT2Protocol.HEADER_PREVIEW, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_ERROR_CLASS, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_ERROR_CODE, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_ERROR_NAME, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_ERROR_DESCRIPTION, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_ERROR_VALUE, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_FAST_EMPTY, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_FAST_EMPTY_OSN, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_ACTIVE_SLIDE, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_AUTH_CODE, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_NEW_DOC, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_STORAGE_VERSION, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_STORAGE_OSN, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_ACK_START, RT2MessageFlags.FLAG_SIMPLE_MSG);
        aHeader2Flag.put(RT2Protocol.HEADER_ACK_END, RT2MessageFlags.FLAG_SIMPLE_MSG);
        aHeader2Flag.put(RT2Protocol.HEADER_NACK_START, RT2MessageFlags.FLAG_SIMPLE_MSG);
        aHeader2Flag.put(RT2Protocol.HEADER_NACK_END, RT2MessageFlags.FLAG_SIMPLE_MSG);
        aHeader2Flag.put(RT2Protocol.HEADER_ADMIN_ID, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_ADMIN_HZ_MEMBER_UUID, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_ADMIN_HZ_MASTER_UUID, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_CAUSE, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_ORIGIN_HOSTNAME, RT2MessageFlags.FLAG_NONE);
        aHeader2Flag.put(RT2Protocol.HEADER_ADVISORY_LOCK, RT2MessageFlags.FLAG_DOC_BASED);

        return aHeader2Flag;
    }
} // class RT2Protocol

//-------------------------------------------------------------------------
// common const values

/* string */ RT2Protocol.HEADER_PREFIX_JMS               = 'JMS';
/* string */ RT2Protocol.HEADER_PREFIX_INTERNAL          = 'internal';
/* string */ RT2Protocol.HEADER_PREFIX_DEBUG             = 'dbg';
/* string */ RT2Protocol.HEADER_PREFIX_DEBUG_STATISTIC   = 'dbg_stat';
/* string */ RT2Protocol.HEADER_PREFIX_DEBUG_PERFORMANCE = 'dbg_perf';

//-------------------------------------------------------------------------
// const protocol items

/* string */ RT2Protocol.PING = 'ping';
/* string */ RT2Protocol.PONG = 'pong';
/* string */ RT2Protocol.ACK_SIMPLE = 'simple_ack';
/* string */ RT2Protocol.ACK_ERROR = 'error_ack';
/* string */ RT2Protocol.NACK_SIMPLE = 'simple_nack';
/* string */ RT2Protocol.ADMIN_TASK_CLOSE_DOC_ROUTE = 'close_doc_route_admintask';
/* string */ RT2Protocol.ADMIN_TASK_COMPLETED_CLOSE_DOC_ROUTE = 'close_doc_route_admintask_completed';
/* string */ RT2Protocol.ADMIN_TASK_CLEANUP_FOR_CRASHED_NODE = 'cleanup_for_crashed_node_admintask';
/* string */ RT2Protocol.ADMIN_TASK_COMPLETED_CLEANUP_FOR_CRASHED_NODE = 'cleanup_for_crashed_node_admintask_completed';
/* string */ RT2Protocol.REQUEST_JOIN = 'join_request';
/* string */ RT2Protocol.REQUEST_LEAVE = 'leave_request';
/* string */ RT2Protocol.REQUEST_OPEN_DOC = 'open_doc_request';
/* string */ RT2Protocol.REQUEST_SAVE_DOC = 'save_doc_request';
/* string */ RT2Protocol.REQUEST_CLOSE_DOC = 'close_doc_request';
/* string */ RT2Protocol.REQUEST_APPLY_OPS = 'apply_ops_request';
/* string */ RT2Protocol.REQUEST_APP_ACTION = 'app_action_request';
/* string */ RT2Protocol.REQUEST_SWITCH_DOCSTATE = 'switch_docstate_request';
/* string */ RT2Protocol.REQUEST_UPDATE_SLIDE = 'update_slide_request';
/* string */ RT2Protocol.REQUEST_RESET = 'reset_request';
/* string */ RT2Protocol.REQUEST_SYNC = 'sync_request';
/* string */ RT2Protocol.REQUEST_SYNC_STABLE = 'sync_stable_request';
/* string */ RT2Protocol.REQUEST_EDITRIGHTS = 'editrights_request';
/* string */ RT2Protocol.REQUEST_UNAVAILABILITY = 'unavailability_request';
/* string */ RT2Protocol.REQUEST_ABORT_OPEN = 'abort_open_request';
/* string */ RT2Protocol.REQUEST_EMERGENCY_LEAVE = 'emergency_leave_request';
/* string */ RT2Protocol.REQUEST_CLOSE_AND_LEAVE = 'close_and_leave_request';
/* string */ RT2Protocol.RESPONSE_GENERIC_ERROR = 'generic_error_response';
/* string */ RT2Protocol.RESPONSE_JOIN = 'join_response';
/* string */ RT2Protocol.RESPONSE_LEAVE = 'leave_response';
/* string */ RT2Protocol.RESPONSE_OPEN_DOC = 'open_doc_response';
/* string */ RT2Protocol.RESPONSE_OPEN_DOC_CHUNK = 'open_doc_chunk_response';
/* string */ RT2Protocol.RESPONSE_SAVE_DOC = 'save_doc_response';
/* string */ RT2Protocol.RESPONSE_CLOSE_DOC = 'close_doc_response';
/* string */ RT2Protocol.RESPONSE_APPLY_OPS = 'apply_ops_response';
/* string */ RT2Protocol.RESPONSE_APP_ACTION = 'app_action_response';
/* string */ RT2Protocol.RESPONSE_SWITCH_DOCSTATE = 'switch_docstate_response';
/* string */ RT2Protocol.RESPONSE_UPDATE_SLIDE = 'update_slide_response';
/* string */ RT2Protocol.RESPONSE_RESET = 'reset_response';
/* string */ RT2Protocol.RESPONSE_SYNC = 'sync_response';
/* string */ RT2Protocol.RESPONSE_SYNC_STABLE = 'sync_stable_response';
/* string */ RT2Protocol.RESPONSE_EDITRIGHTS = 'editrights_response';
/* string */ RT2Protocol.RESPONSE_UNAVAILABILITY = 'unavailability_response';
/* string */ RT2Protocol.RESPONSE_ABORT_OPEN = 'abort_open_response';
/* string */ RT2Protocol.RESPONSE_EMERGENCY_LEAVE = 'emergency_leave_response';
/* string */ RT2Protocol.RESPONSE_CLOSE_AND_LEAVE = 'close_and_leave_response';
/* string */ RT2Protocol.BROADCAST_UPDATE = 'update_broadcast';
/* string */ RT2Protocol.BROADCAST_UPDATE_CLIENTS = 'update_clients_broadcast';
/* string */ RT2Protocol.BROADCAST_CHAT_MESSAGE = 'chat_message_broadcast';
/* string */ RT2Protocol.BROADCAST_EDITREQUEST_STATE = 'editrequest_state_broadcast';
/* string */ RT2Protocol.BROADCAST_SHUTDOWN = 'shutdown_broadcast';
/* string */ RT2Protocol.BROADCAST_CRASHED = 'crashed_broadcast';
/* string */ RT2Protocol.BROADCAST_RENAMED_RELOAD = 'renamed_reload_broadcast';
/* string */ RT2Protocol.BROADCAST_HANGUP = 'hangup_broadcast';
/* string */ RT2Protocol.BROADCAST_OT_RELOAD = 'ot_reload_broadcast';
/* string */ RT2Protocol.BROADCAST_FILE_MOVED_RELOAD = 'file_moved_reload_broadcast';
/* string */ RT2Protocol.HEADER_MSG_ID = 'msg_id_header';
/* string */ RT2Protocol.HEADER_SEQ_NR = 'seq_nr_header';
/* string */ RT2Protocol.HEADER_CLIENT_UID = 'client_uid_header';
/* string */ RT2Protocol.HEADER_SESSION_ID = 'session_id_header';
/* string */ RT2Protocol.HEADER_DOC_UID = 'doc_uid_header';
/* string */ RT2Protocol.HEADER_DRIVE_DOC_ID = 'drive_doc_id_header';
/* string */ RT2Protocol.HEADER_FOLDER_ID = 'folder_id_header';
/* string */ RT2Protocol.HEADER_FILE_ID = 'file_id_header';
/* string */ RT2Protocol.HEADER_DOC_TYPE = 'doc_type_header';
/* string */ RT2Protocol.HEADER_DOC_STATE = 'doc_state_header';
/* string */ RT2Protocol.HEADER_APP_ACTION = 'app_action_header';
/* string */ RT2Protocol.HEADER_USERAGENT = 'useragent_header';
/* string */ RT2Protocol.HEADER_NODE_UID = 'node_uid_header';
/* string */ RT2Protocol.HEADER_STORAGE_VERSION = 'storage_version_header';
/* string */ RT2Protocol.HEADER_STORAGE_OSN = 'storage_osn_header';
/* string */ RT2Protocol.HEADER_OLD_CLIENT_UID = 'old_client_uid_header';
/* string */ RT2Protocol.HEADER_PREVIEW = 'preview_header';
/* string */ RT2Protocol.HEADER_ERROR_CLASS = 'err_class_header';
/* string */ RT2Protocol.HEADER_ERROR_CODE = 'err_code_header';
/* string */ RT2Protocol.HEADER_ERROR_NAME = 'err_name_header';
/* string */ RT2Protocol.HEADER_ERROR_DESCRIPTION = 'err_desc_header';
/* string */ RT2Protocol.HEADER_ERROR_VALUE = 'err_value_header';
/* string */ RT2Protocol.HEADER_FAST_EMPTY = 'fast_empty_header';
/* string */ RT2Protocol.HEADER_FAST_EMPTY_OSN = 'fast_empty_osn_header';
/* string */ RT2Protocol.HEADER_ACTIVE_SLIDE = 'active_slide_header';
/* string */ RT2Protocol.HEADER_AUTH_CODE = 'auth_code_header';
/* string */ RT2Protocol.HEADER_NEW_DOC = 'new_doc_header';
/* string */ RT2Protocol.HEADER_AUTO_CLOSE = 'auto_close_header';
/* string */ RT2Protocol.HEADER_NO_RESTORE = 'no_restore_header';
/* string */ RT2Protocol.HEADER_UNAVAIL_TIME = 'unavail_time_header';
/* string */ RT2Protocol.HEADER_ACK_START = 'ack_start_header';
/* string */ RT2Protocol.HEADER_ACK_END = 'ack_end_header';
/* string */ RT2Protocol.HEADER_NACK_START = 'nack_start_header';
/* string */ RT2Protocol.HEADER_NACK_END = 'nack_end_header';
/* string */ RT2Protocol.HEADER_ADMIN_ID = 'admin_id_header';
/* string */ RT2Protocol.HEADER_ADMIN_HZ_MEMBER_UUID = 'admin_hz_member_uuid_header';
/* string */ RT2Protocol.HEADER_ADMIN_HZ_MASTER_UUID = 'admin_hz_master_uuid_header';
/* string */ RT2Protocol.HEADER_CAUSE = 'cause_header';
/* string */ RT2Protocol.HEADER_ORIGIN_HOSTNAME = 'origin_hostname_header';
/* string */ RT2Protocol.HEADER_ADVISORY_LOCK = 'advisory_lock_header';
/* string */ RT2Protocol.HEADER_INTERNAL_CHUNK_ID = 'internal_chunk_header';
/* string */ RT2Protocol.HEADER_INTERNAL_DOCTYPE = 'internal_doctype_header';
/* string */ RT2Protocol.HEADER_INTERNAL_RECIPIENTS = 'internal_recipients_header';
/* string */ RT2Protocol.HEADER_INTERNAL_CLIENT_ENDPOINT_ID = 'internal_client_ep_id_header';
/* string */ RT2Protocol.HEADER_INTERNAL_CLIENT_ENDPOINT_URI = 'internal_client_ep_uri_header';
/* string */ RT2Protocol.HEADER_INTERNAL_TIME_QUEUED = 'internal_time_queued_header';
/* string */ RT2Protocol.HEADER_INTERNAL_FORCE = 'internal_force_header';
/* string */ RT2Protocol.HEADER_INTERNAL_REFERRER = 'internal_referrer_header';
/* string */ RT2Protocol.HEADER_INTERNAL_IS_RESPONSE = 'internal_is_response_header';
/* string */ RT2Protocol.HEADER_INTERNAL_IS_BROADCAST = 'internal_is_broadcast_header';
/* string */ RT2Protocol.HEADER_DEBUG_SEQNUM = 'dbg_seqnum_header';
/* string */ RT2Protocol.HEADER_DEBUG_DISABLE_SESSION_VALIDATION = 'dbg_disable_session_validation_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_IN = 'dbg_perf_client_in_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_OUT = 'dbg_perf_client_out_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_IN = 'dbg_perf_server_in_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_OUT = 'dbg_perf_server_out_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_LOAD_START = 'dbg_perf_server_load_start_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_LOAD_END = 'dbg_perf_server_load_end_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_APPLYOPS_START = 'dbg_perf_server_applyops_start_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_APPLYOPS_END = 'dbg_perf_server_applyops_end_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_SAVE_START = 'dbg_perf_server_save_start_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_SAVE_END = 'dbg_perf_server_save_end_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_RECENTLIST_START = 'dbg_perf_server_recentlist_start_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_RECENTLIST_END = 'dbg_perf_server_recentlist_end_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_SEQUENCER_START = 'dbg_perf_server_sequencer_start_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_SEQUENCER_END = 'dbg_perf_server_sequencer_end_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_ROUTE_DOC_IN = 'dbg_perf_server_route_doc_in_header';
/* string */ RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_ROUTE_DOC_OUT = 'dbg_perf_server_route_doc_out_header';
/* string */ RT2Protocol.HEADER_DEBUG_STATISTIC_NODEID = 'dbg_stat_node_id_header';
/* string */ RT2Protocol.BODYPART_ACKS = 'ACKS';
/* string */ RT2Protocol.BODYPART_NACKS = 'NACKS';

//-------------------------------------------------------------------------
var /* static RT2Protocol */ m_gSingleton = null;

//-------------------------------------------------------------------------
/* public static RT2Protocol */ RT2Protocol.get = function () {
    if (ObjectUtils.isNull(m_gSingleton)) {
        var /* RT2Protocl */ aSingleton = new RT2Protocol();
        aSingleton.init();
        m_gSingleton = aSingleton;
    }
    return m_gSingleton;
};

//--------------------------------------------------------------------------
// exports

export default RT2Protocol;
