/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';
import Validate from '@/io.ox/office/rt2/shared/jss_validate';
import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';
import RT2Message from '@/io.ox/office/rt2/shared/rt2message';

//--------------------------------------------------------------------------
// const

//--------------------------------------------------------------------------
// class

function RT2MessageFactory() {
} // class RT2MessageFactory

//-------------------------------------------------------------------------
/* private static void */ function impl_defineMessageFlags(/* RT2Message */ aMessage, /* string */ sType) {
    var /* Integer */ nFlags4Type = RT2Protocol.get().getFlags4Type(sType);
    aMessage.setFlags(nFlags4Type);
}

//-------------------------------------------------------------------------
/* private static void */ function impl_defineDefaultHeader(/* RT2Message */ aMessage) {
    aMessage.newMessageID();
}

//-------------------------------------------------------------------------
/* public static RT2Message */ RT2MessageFactory.newMessage = function (/* string */ sType) {
    Validate.notEmpty(sType, 'Invalid argument "type".');

    var /* RT2Message */ aMessage = new RT2Message();

    impl_defineMessageFlags(aMessage, sType);
    impl_defineDefaultHeader(aMessage, sType);

    // as type is defined as header too ... define after header was handled already ! ;-)
    aMessage.setType(sType);

    return aMessage;
};

//-------------------------------------------------------------------------
/* public static RT2Message */ RT2MessageFactory.cloneMessage = function (/* RT2Message */ aOriginal, /* string */ sCloneType) {
    var /* RT2Message */ aClone = RT2MessageFactory.newMessage(sCloneType);

    RT2Message.copyAllHeader(aOriginal, aClone);
    RT2Message.copyBody(aOriginal, aClone);

    // as type is defined as header too ... define after header was handled already ! ;-)
    aClone.setType(sCloneType);

    // msg flags are bound to message type.
    // Dont copy it - generate it new for new type always !
    impl_defineMessageFlags(aClone, sCloneType);

    return aClone;
};

//-------------------------------------------------------------------------
/* public static RT2Message */ RT2MessageFactory.createResponseFromMessage = function (/* RT2Message */ aMessage, /* string */ sResponseType) {
    var /* RT2Message */ aResponse = RT2MessageFactory.cloneMessage(aMessage, sResponseType);
    // message id of response has to be the same !
    // but body needs to be fresh and new !
    aResponse.setBody(null);
    return aResponse;
};

//-------------------------------------------------------------------------
/* public static RT2Message */ RT2MessageFactory.createRequestFromMessage = function (/* RT2Message */ aMessage, /* string */ sRequestType) {
    var /* RT2Message */ aRequest = RT2MessageFactory.cloneMessage(aMessage, sRequestType);
    // new request needs new message id !
    aRequest.newMessageID();
    // but body needs to be fresh and new !
    aRequest.setBody(null);
    return aRequest;
};

//-------------------------------------------------------------------------
/* public static RT2Message */ RT2MessageFactory.fromJSONString = function (/* string  */ sJSON, /* boolean */ bDecode) {
    // optional parameter needs default !
    if (ObjectUtils.isNull(bDecode)) {
        bDecode = true;
    }

    var /* JSON       */ aJSON  = JSON.parse(sJSON); //?
    var /* RT2Message */ aRTMsg = RT2MessageFactory.fromJSON(aJSON, bDecode);
    return aRTMsg;
};

//-------------------------------------------------------------------------
/* public static RT2Message */ RT2MessageFactory.fromJSON = function (/* JSON */ aJSON, /* boolean */ bDecode) {
    // optional parameter needs default !
    if (ObjectUtils.isNull(bDecode)) {
        bDecode = true;
    }

    var /* String      */ KEY_TYPE   = bDecode ? 't' : 'type';
    var /* String      */ KEY_HEADER = bDecode ? 'h' : 'header';
    var /* String      */ KEY_BODY   = bDecode ? 'b' : 'body';

    var /* RT2Protocol */ aProtocol  = RT2Protocol.get();

    var /* String */ sType = aJSON[KEY_TYPE];
    if (bDecode) {
        sType = aProtocol.decode(sType);
    }

    var /* RT2Message */ aRTMsg = RT2MessageFactory.newMessage(sType);

    var /* JSON */ aHeaderMap = ObjectUtils.getObjectPartOrDefault(aJSON, KEY_HEADER, null);
    for (var /* string */ sHeader in aHeaderMap) {
        var /* string */ sRealHeader = sHeader;
        var /* object */ aValue      = aHeaderMap[sHeader];

        if (bDecode) {
            sRealHeader = aProtocol.decode(sRealHeader);
        }

        aRTMsg.setHeader(sRealHeader, aValue);
    }

    var /* JSON */ aBody = ObjectUtils.getObjectPartOrDefault(aJSON, KEY_BODY, {});
    aRTMsg.setBody(aBody);

    var /* JSON */ aValidation = {};
    if (!aRTMsg.isValid(aValidation)) {
        throw new Error(`Message not valid: ${aValidation}`);
    }

    return aRTMsg;
};

//-------------------------------------------------------------------------
/* public static String */ RT2MessageFactory.toJSONString = function (/* RT2Message */ aRTMsg, /* boolean */ bEncode) {
    // optional parameter needs default !
    if (ObjectUtils.isNull(bEncode)) {
        bEncode = true;
    }

    var /* JSON   */ aJSON = RT2MessageFactory.toJSON(aRTMsg, bEncode);
    var /* String */ sJSON = JSON.stringify(aJSON);
    return sJSON;
};

//-------------------------------------------------------------------------
/* public static JSON */ RT2MessageFactory.toJSON = function (/* RT2Message */ aRTMsg, /* boolean */ bEncode) {
    // optional parameter needs default !
    if (ObjectUtils.isNull(bEncode)) {
        bEncode = true;
    }

    var /* String      */ KEY_TYPE   = bEncode ? 't' : 'type';
    var /* String      */ KEY_HEADER = bEncode ? 'h' : 'header';
    var /* String      */ KEY_BODY   = bEncode ? 'b' : 'body';

    var /* JSONObject  */ aJSON      = {};
    var /* RT2Protocol */ aProtocol  = RT2Protocol.get();

    var /* String */ sType = aRTMsg.getType();
    if (bEncode) {
        sType = aProtocol.encode(sType);
    }
    aJSON[KEY_TYPE] = sType;

    var /* JSON           */ aJSONHeader = {};
    var /* List< String > */ lHeader     = aRTMsg.listHeader();
    var /* int            */ c           = lHeader.size();
    for (var /* int */ i = 0; i < c; ++i) {
        var /* String */ sHeader = lHeader.get(i);

        // filter out 'internal' header so they do not reach the client
        if (StringUtils.startsWith(sHeader, RT2Protocol.HEADER_PREFIX_JMS)) {
            continue;
        }

        if (StringUtils.startsWith(sHeader, RT2Protocol.HEADER_PREFIX_INTERNAL)) {
            continue;
        }

        var /* String */ sRealHeader = sHeader;

        // note: value needs to be retrieved from internal map,
        // before we might encode the header name ...
        // otherwise we will get "null" as value always ...
        // because the might encoded header is not known ;-)
        var /* Object */ aValue = aRTMsg.getHeader(sHeader);

        if (bEncode) {
            sRealHeader = aProtocol.encode(sRealHeader);
        }

        aJSONHeader[sRealHeader] = aValue;
    }

    aJSON[KEY_HEADER] = aJSONHeader;
    aJSON[KEY_BODY]   = aRTMsg.getBody();

    return aJSON;
};

//--------------------------------------------------------------------------
// exports

export default RT2MessageFactory;
