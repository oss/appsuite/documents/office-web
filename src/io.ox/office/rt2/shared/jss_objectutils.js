/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

//--------------------------------------------------------------------------
/* class ObjectUtils
    */
function ObjectUtils() {
    // helper with static functions only
} // class ObjectUtils

//--------------------------------------------------------------------------
/* @return true if the given object (or even array of objects) is null or undefined
    *         false otherwise
    *
    * @param aObject [IN]
    *        the object (or even array of objects) to be checked
    */
/* public static boolean */ ObjectUtils.isNull = function (/* object */ aObject) {
    if ((aObject === undefined) || (aObject === null)) {
        return true;
    }

    if (Array.isArray(aObject)) {
        for (const /* object */ aArrayObject of aObject) {
            if (ObjectUtils.isNull(aArrayObject)) {
                return true;
            }
        }
    }

    return false;
};

//--------------------------------------------------------------------------
/* public static boolean */ ObjectUtils.isNotNull = function (/* object */ aObject) {
    return !ObjectUtils.isNull(aObject);
};

//--------------------------------------------------------------------------
/* public static boolean */ ObjectUtils.isFunction = function (/* function */ aFunction) {
    return Object.prototype.toString.call(aFunction) === '[object Function]';
};

//--------------------------------------------------------------------------
/* public static boolean */ ObjectUtils.isString = function (/* string */ sString) {
    return Object.prototype.toString.call(sString) === '[object String]';
};

//--------------------------------------------------------------------------
/* public static boolean */ ObjectUtils.isObject = function (/* json */ aObject) {
    //Underscore.js 1.9.1 with added braces for the linter...
    var type = typeof aObject;
    return type === 'function' || (type === 'object' && !!aObject);
};

//--------------------------------------------------------------------------
/* public static boolean */ ObjectUtils.isNumber = function (/* number */ aObject) {
    return Object.prototype.toString.call(aObject) === '[object Number]';
};

//--------------------------------------------------------------------------
/* public static boolean */ ObjectUtils.isArray = function (/* array */ aObject) {
    return Object.prototype.toString.call(aObject) === '[object Array]';
};

//--------------------------------------------------------------------------
/* public static boolean */ ObjectUtils.isBoolean = function (/* boolean */ aObject) {
    return Object.prototype.toString.call(aObject) === '[object Boolean]';
};

//--------------------------------------------------------------------------
/* public static object */ ObjectUtils.getObjectPartOrDefault = function (/* object */ aObject, /* string */ sPart, /* object */ aDefault) {
    if (ObjectUtils.isNull(aObject) || ObjectUtils.isNull(aObject[sPart])) {
        return aDefault;
    }

    return aObject[sPart];
};

//--------------------------------------------------------------------------
// exports

export default ObjectUtils;
