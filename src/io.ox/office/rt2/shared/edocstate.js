/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';

//--------------------------------------------------------------------------
// const

//--------------------------------------------------------------------------
// class

function EDocState() {
    //----------------------------------------------------------------------
    // member

    var /* string */ m_sState = null;

    //--------------------------------------------------------------------------
    /* public static void */ this.setStateString = function (/* String */ sState) {
        m_sState = sState;
    };

    //--------------------------------------------------------------------------
    /* public static String */ this.getStateString = function () {
        return m_sState;
    };

    //--------------------------------------------------------------------------
    /* public static boolean */ this.equals = function (/* EDocState | String ! */ aState) {
        var /* String */ sState = null;

        if (ObjectUtils.isString(aState)) {
            sState = aState;
        } else {
            sState = aState.getStateString();
        }

        var /* boolean */ bEquals = StringUtils.equalsIgnoreCase(sState, m_sState);
        return bEquals;
    };

} // class EDocState

//-------------------------------------------------------------------------
/* public static EDocState */ EDocState.create = function (/* string */ sState) {
    var /* EDocState */ eState = new EDocState();
    eState.setStateString(sState);
    return eState;
};

//-------------------------------------------------------------------------
/* public static string */ EDocState.STR_OFFLINE = 'offline';
/* public static string */ EDocState.STR_ONLINE  = 'online';

//-------------------------------------------------------------------------
/* public static string */ EDocState.E_OFFLINE = EDocState.create(EDocState.STR_OFFLINE);
/* public static string */ EDocState.E_ONLINE  = EDocState.create(EDocState.STR_ONLINE);

//-------------------------------------------------------------------------
/* public static String */ EDocState.toString = function (/* EDocState */ eState) {
    if (eState.equals(EDocState.E_OFFLINE)) {
        return EDocState.STR_OFFLINE;
    }

    if (eState.equals(EDocState.E_ONLINE)) {
        return EDocState.STR_ONLINE;
    }

    throw new Error(`No support for "${eState}" implemented yet.`);
};

//-------------------------------------------------------------------------
/* public static EDocState */ EDocState.fromString = function (/* string */ sState) {
    if (StringUtils.equalsIgnoreCase(sState, EDocState.STR_OFFLINE)) {
        return EDocState.E_OFFLINE;
    }

    if (StringUtils.equalsIgnoreCase(sState, EDocState.STR_ONLINE)) {
        return EDocState.E_ONLINE;
    }

    throw new Error(`No support for "${sState}" implemented yet.`);
};

//--------------------------------------------------------------------------
// exports

export default EDocState;
