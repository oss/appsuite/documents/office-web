/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';

//--------------------------------------------------------------------------
/* class UrlUtils
    */
function URLUtils() {
    // helper with static functions only
} // class UrlUtils

//--------------------------------------------------------------------------
/* @return the current URL (if possible within current context)
    *         ... an empty string otherwise
    */
/* string */ URLUtils.getCurrentURL = function () {
    try {
        return window.location.href;
    } catch {
    }

    return '';
};

//--------------------------------------------------------------------------
/* @return the value of the searched URL parameter
    *         or an empty string if such parameter does not exists
    *
    * example:
    *     url = 'http://foo.com/path?p1=true&p2=2&p3=something'
    *     will return ...
    *     'true'      for param p1
    *     '2'         for param p2
    *     'something' for param p3
    *
    * @param sURL [IN]
    *        the URL where the parameter schould be read from
    *
    * @param sParam [IN]
    *        the URL parameter name
    */
/* string */ URLUtils.getParamValue = function (/* string */ sURL, /* string */ sParam) {
    var /* RegExp */ aRegEx  = new RegExp(`${sParam}=(.+?(?=&|$))`, 'g');
    var /* object */ aResult = aRegEx.exec(sURL);

    if (ObjectUtils.isNull(aResult)) {
        return '';
    }

    return aResult[1];
};

//--------------------------------------------------------------------------
/* string */ URLUtils.getProtocol = function (/* string */ sURL) {
    var /* RegExp */ aRegEx  = /^([^:]+):\/\//g;
    var /* object */ aResult = aRegEx.exec(sURL);

    if (ObjectUtils.isNull(aResult)) {
        return '';
    }

    return aResult[1];
};

//--------------------------------------------------------------------------
/* string */ URLUtils.setProtocol = function (/* string */ sURL, /* string */ sProtocol) {
    var /* RegExp */ aRegEx  = /^([^:]+):\/\//g;
    var /* string */ sURLWithoutProtocol = sURL.replace(aRegEx, '');
    var /* string */  sNewURL = sProtocol + '://' + sURLWithoutProtocol;
    return sNewURL;
};

//--------------------------------------------------------------------------
/* string */ URLUtils.setHost = function (/* string */ sURL, /* string */ sHost) {
    var /* RegExp */ aRegEx  = /:\/\/(\d?.)?(.[^/:]+)/gm;
    var /* string */ sNewURL = sURL.replace(aRegEx, `://${sHost}`);
    return sNewURL;
};

//--------------------------------------------------------------------------
/* string */ URLUtils.setPort = function (/* string */ sURL, /* string */ sPort) {
    var /* RegExp */ aRegEx  = /(?::\d{2,5})/gm;
    var /* string */ sNewURL = sURL.replace(aRegEx, `:${sPort}`);
    return sNewURL;
};

//--------------------------------------------------------------------------
// exports

export default URLUtils;
