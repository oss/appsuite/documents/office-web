/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { random as randomUUID } from '@/io.ox/office/tk/algorithms/uuid';

import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';
import List from '@/io.ox/office/rt2/shared/jss_list';
import HashMap from '@/io.ox/office/rt2/shared/jss_hashmap';
import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';
import RT2MessageFlags from '@/io.ox/office/rt2/shared/rt2messageflags';
import RT2MessageHeaderCheck from '@/io.ox/office/rt2/shared/rt2messageheadercheck';

//--------------------------------------------------------------------------
// const

//--------------------------------------------------------------------------
// class

function RT2Message() {
    //----------------------------------------------------------------------
    // member

    var /* object */ self = this;

    /// type of Message
    var /* String */ m_sType = null;

    /// flags of Message
    var /* int */ m_nFlags = 0;

    /// map of header
    var /* map< string, object > */ m_lHeader = null;

    /// the body
    var /* JSON */ m_aBodyJSON = null;

    // var /* Logger */ m_aLog = null;

    //----------------------------------------------------------------------
    /* public boolean */ this.isType = function (/* string */ sType) {
        var /* boolean */ bIsType = StringUtils.equalsIgnoreCase(self.getType(), sType);
        return bIsType;
    };

    //-------------------------------------------------------------------------
    /* public String */ this.getType = function () {
        return m_sType;
    };

    //-------------------------------------------------------------------------
    /* public void */ this.setType = function (/* string */ sType) {
        m_sType = sType;
    };

    //-------------------------------------------------------------------------
    /* public String */ this.getMessageID = function () {
        var /* String */ sMsgId = impl_getHeader(RT2Protocol.HEADER_MSG_ID, null);
        return sMsgId;
    };

    //-------------------------------------------------------------------------
    /* public void */ this.setMessageID = function (/* string */ sMsgId) {
        impl_setHeader(RT2Protocol.HEADER_MSG_ID, sMsgId);
    };

    //-------------------------------------------------------------------------
    /* public void */ this.newMessageID = function () {
        self.setMessageID(randomUUID());
    };

    //-------------------------------------------------------------------------
    /* public void */ this.setFlags = function (/* int */ nFlags) {
        m_nFlags = nFlags;
    };

    //-------------------------------------------------------------------------
    /* public boolean */ this.hasFlags = function (/* int */ nFlags) {
        return (m_nFlags & nFlags) === nFlags;
    };

    //-------------------------------------------------------------------------
    /* public List< String > */ this.listHeader = function () {
        var /* List< String > */ lHeader = mem_Header().keySet();
        return lHeader;
    };

    //-------------------------------------------------------------------------
    /* public boolean */ this.hasHeader = function (/* string */ sHeader) {
        var /* List< String > */ lHeader = self.listHeader();
        var /* boolean        */ bHas    = lHeader.contains(sHeader);
        return bHas;
    };

    //-------------------------------------------------------------------------
    /* public < T > T */ this.getHeader = function (/* string */ sHeader) {
        var /* int */ nFlags = RT2Protocol.get().getFlag4Header(sHeader);
        var /* T   */ aValue = self.getHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, nFlags);
        return aValue;
    };

    //-------------------------------------------------------------------------
    /* public < T > T */ this.getHeader2 = function (/* String */ sHeader, /* int */ nMissCheck, /* int */ nFlag) {
        if (!self.hasFlags(nFlag)) {
            throw new Error(`Message [${self.getType()}] does not support requested flags [${RT2MessageFlags.mapFlagToString(nFlag)}] on getting header [${sHeader}].`);
        }

        var /* T */ aValue = impl_getHeader(sHeader, null);
        if (ObjectUtils.isNotNull(aValue)) {
            return aValue;
        }

        if (nMissCheck === RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY) {
            throw new Error(`Message [${self.getType()}] misses mandatory header [${sHeader}].`);
        }

        if ((nMissCheck === RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY) && (nFlag !== RT2MessageFlags.FLAG_NONE)) {
            throw new Error(`Message [${self.getType()}] misses header [${sHeader}] forced by flags.`);
        }

        return null;
    };

    //-------------------------------------------------------------------------
    /* public < T > void */ this.setHeader = function (/* string */ sHeader, /* T */ aValue) {
        var /* int */ nFlags = RT2Protocol.get().getFlag4Header(sHeader);
        self.setHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, aValue, nFlags);
    };

    //-------------------------------------------------------------------------
    /* public < T > void */ this.setHeader2 = function (/* string */ sHeader, /* int */ nMissCheck, /* T */ aValue, /* int */ nFlag) {
        if (!self.hasFlags(nFlag)) {
            throw new Error(`Message [${self.getType()}] does not support requested flags [${RT2MessageFlags.mapFlagToString(nFlag)}] on setting header [${sHeader}].`);
        }

        if (ObjectUtils.isNotNull(aValue)) {
            impl_setHeader(sHeader, aValue);
            return;
        }

        if (nMissCheck === RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY) {
            throw new Error(`Message [${self.getType()}] misses value for setting mandatory header [${sHeader}].`);
        }

        if ((nMissCheck === RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY) && (nFlag !== RT2MessageFlags.FLAG_NONE)) {
            throw new Error(`Message [${self.getType()}] misses value for setting header [${sHeader}] forced by flags.`);
        }
    };

    //----------------------------------------------------------------------
    /* public void */ this.mergeHeader = function (/* map< string, object > | JSON ! */ lOutsideHeader) {
        var /* Map< String, Object > */ lHeader = HashMap.fromMapOrJSON(lOutsideHeader);
        var /* Iterator< string >    */ rHeader = lHeader.keySet().iterator();
        while (rHeader.hasNext()) {
            var /* String */ sHeader = rHeader.next();
            var /* object */ aValue  = lHeader.get(sHeader);
            self.setHeader(sHeader, aValue);
        }
    };

    //-------------------------------------------------------------------------
    /* public JSON */ this.getBody = function () {
        return impl_getBodyJSON();
    };

    //-------------------------------------------------------------------------
    /* public String */ this.getBodyString = function () {
        return impl_getBodyString();
    };

    //-------------------------------------------------------------------------
    /* public void */ this.setBody = function (/* JSON */ aBody) {
        impl_setBodyJSON(aBody);
    };

    //-------------------------------------------------------------------------
    /* public void */ this.setBodyString = function (/* string */ sBody) {
        var /* string */ sNormBody = StringUtils.defaultString(sBody, '{}');
        var /* JSON   */ aBody     = JSON.parse(sNormBody);
        impl_setBodyJSON(aBody);
    };

    //-------------------------------------------------------------------------
    /* public boolean */ this.isValid = function (/* OUT-PARAM ! JSON */ aReason) {
        var /* string */ sType = self.getType();
        if (StringUtils.isEmpty(sType)) {
            aReason.value = '"type" not defined';
            return false;
        }

        var /* string */ sMsgId = self.getMessageID(self);
        if (StringUtils.isEmpty(sMsgId)) {
            aReason.value = '"msg_id" not defined';
            return false;
        }

        return true;
    };

    //-------------------------------------------------------------------------
    /* public boolean */ this.isSimpleMessage = function () {
        return self.hasFlags(RT2MessageFlags.FLAG_SIMPLE_MSG);
    };

    //-------------------------------------------------------------------------
    /* public boolean */ this.isSessionMessage = function () {
        return self.hasFlags(RT2MessageFlags.FLAG_SESSION_BASED);
    };

    //-------------------------------------------------------------------------
    /* public boolean */ this.isSequenceMessage = function () {
        return self.hasFlags(RT2MessageFlags.FLAG_SEQUENCE_NR_BASED);
    };

    //-------------------------------------------------------------------------
    /* public boolean */ this.isDocMessage = function () {
        return self.hasFlags(RT2MessageFlags.FLAG_DOC_BASED);
    };

    //-------------------------------------------------------------------------
    /* public boolean */ this.isChunkMessage = function () {
        return self.hasFlags(RT2MessageFlags.FLAG_CHUNK_BASED);
    };

    //-------------------------------------------------------------------------
    /* public boolean */ this.isFinalMessage = function () {
        return !self.hasFlags(RT2MessageFlags.FLAG_CHUNK_BASED);
    };

    //-------------------------------------------------------------------------
    /* public String */ this.toString = function () {
        var /* string */ sString = '';

        sString += '{\n';
        sString += '    type  : ';
        sString += self.getType();
        sString += '\n';
        sString += '    header:\n';
        sString += mem_Header().toString();
        sString += '\n';
        sString += '    body  :\n';
        sString += impl_getBodyString();
        sString += '\n';
        sString += '}\n';

        return sString;
    };

    //-------------------------------------------------------------------------
    /* private < T > void */ function impl_setHeader(/* string */ sHeader, /* T */ aValue) {
        var /* Map< String, Object > */ lHeader = mem_Header();
        if (ObjectUtils.isNull(aValue)) {
            lHeader.remove(sHeader);
        } else {
            lHeader.put(sHeader, aValue);
        }
    }

    //-------------------------------------------------------------------------
    /* private < T > T */ function impl_getHeader(/* string */ sHeader, /* T */ aDefault) {
        var /* T */ aValue = mem_Header().get(sHeader);

        if (ObjectUtils.isNull(aValue)) {
            aValue = aDefault;
        }

        return aValue;
    }

    //-------------------------------------------------------------------------
    /* private void */ function impl_setBodyJSON(/* JSON */ aBody) {
        m_aBodyJSON = aBody;
    }

    //-------------------------------------------------------------------------
    /* private JSON */ function impl_getBodyJSON() {
        return mem_BodyJSON();
    }

    //-------------------------------------------------------------------------
    /* private String */ function impl_getBodyString() {
        return JSON.stringify(mem_BodyJSON());
    }

    //----------------------------------------------------------------------
    /* private map< string, object > */ function mem_Header() {
        if (ObjectUtils.isNull(m_lHeader)) {
            m_lHeader = new HashMap();
        }
        return m_lHeader;
    }

    //-------------------------------------------------------------------------
    /* private JSON */ function mem_BodyJSON() {
        if (ObjectUtils.isNull(m_aBodyJSON)) {
            m_aBodyJSON = {};
        }
        return m_aBodyJSON;
    }

} // class RT2Message

//-------------------------------------------------------------------------
/* public static void */ RT2Message.cleanHeaderBeforeSendToClient = function (/* RT2Message */ aMessage) {
    RT2Message.cleanInternalHeader(aMessage);

    aMessage.setHeader('breadcrumbId',                  null); // camel specific header
    aMessage.setHeader(RT2Protocol.HEADER_SESSION_ID,   null);
    aMessage.setHeader(RT2Protocol.HEADER_DOC_STATE,    null);
    aMessage.setHeader(RT2Protocol.HEADER_DRIVE_DOC_ID, null);
    aMessage.setHeader(RT2Protocol.HEADER_FOLDER_ID,    null);
    aMessage.setHeader(RT2Protocol.HEADER_FILE_ID,      null);
};

//-------------------------------------------------------------------------
/* public static void */ RT2Message.cleanHeaderBeforeSendToServer = function (/* RT2Message */ aMessage) {
    RT2Message.cleanInternalHeader(aMessage);
};

//-------------------------------------------------------------------------
/* public static void */ RT2Message.cleanInternalHeader = function (/* RT2Message */ aMessage) {
    aMessage.setHeader(RT2Protocol.HEADER_INTERNAL_RECIPIENTS, null);
};

//-------------------------------------------------------------------------
/* public static void */ RT2Message.copyHeaderWithprefix = function (/* RT2Message */ aFrom, /* RT2Message */ aTo, /* string */ sPrefix) {
    var /* Iterator< String > */ lHeader = aFrom.listHeader().iterator();
    while (lHeader.hasNext()) {
        var /* string */ sHeader = lHeader.next();
        if (!StringUtils.startsWith(sHeader, sPrefix)) {
            continue;
        }

        var /* Object */ aValue = aFrom.getHeader(sHeader);
        aTo.setHeader(sHeader, aValue);
    }
};

//-------------------------------------------------------------------------
/* public static void */ RT2Message.copyAllHeader = function (/* RT2Message */ aFrom, /* RT2Message */ aTo) {
    var /* Iterator< String > */ lHeader = aFrom.listHeader().iterator();
    while (lHeader.hasNext()) {
        var /* string */ sHeader = lHeader.next();
        var /* Object */ aValue  = aFrom.getHeader(sHeader);
        aTo.setHeader(sHeader, aValue);
    }
};

//-------------------------------------------------------------------------
/* public static void */ RT2Message.copySomeHeader = function (/* RT2Message */ aFrom, /* RT2Message */ aTo, /* string... */ lHeader) {
    var /* Iterator< String > */ rHeader = List.fromVarArg(lHeader).iterator();
    while (rHeader.hasNext()) {
        var /* string */ sHeader = rHeader.next();
        var /* Object */ aValue  = aFrom.getHeader(sHeader);
        aTo.setHeader(sHeader, aValue);
    }
};

//-------------------------------------------------------------------------
/* public static void */ RT2Message.copyBody = function (/* RT2Message */ aFrom, /* RT2Message */ aTo) {
    var /* JSON */ aBody = aFrom.getBody();
    aTo.setBody(aBody);
};

//--------------------------------------------------------------------------
// exports

export default RT2Message;
