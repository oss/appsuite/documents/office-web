/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';

//--------------------------------------------------------------------------
/* class StringUtils
    */
function StringUtils() {
    // helper with static functions only
} // class StringUtils

//--------------------------------------------------------------------------
/*  Unicode  helper:
    *  Code taken from io.ox/office/tk/algorithms/unicode, but that can't be imported easily in the web worker.
    */

/**
 * Multiplicator for converting high surrogates to Unicode code point values.
 */
var SURR_DATA_SHIFT = 1024;

/**
 * Bit mask for extracting the data bits from UTF-16 surrogates.
 */
var SURR_DATA_MASK = SURR_DATA_SHIFT - 1;

/**
 * Bit mask for detecting UTF-16 surrogates.
 */
var SURR_MARK_MASK = 0xFFFF ^ SURR_DATA_MASK;

/**
 * Marker bits of a UTF-16 high surrogate.
 */
var HI_SURR_MARK = 0xD800;

/**
 * Marker bits of a UTF-16 low surrogate.
 */
var LO_SURR_MARK = 0xDC00;

function isHighSurrCode(code) {
    return (code & SURR_MARK_MASK) === HI_SURR_MARK; // will not pass for `NaN`
}

function isLowSurrCode(code) {
    return (code & SURR_MARK_MASK) === LO_SURR_MARK; // will not pass for `NaN`
}

function hasSurrogatePair(text, offset) {
    return isHighSurrCode(text.charCodeAt(offset)) && isLowSurrCode(text.charCodeAt(offset + 1));
}

//--------------------------------------------------------------------------
/**
 * @param {object} aObject
 *  the object to be checked
 *
 * @returns {boolean}
 *  true if the given variable is a string, false otherwise
 */
/* boolean */ StringUtils.isString = function (/* object */ aObject) {
    if (ObjectUtils.isNull(aObject)) {
        return false;
    }

    var /* string  */ sType     = (typeof aObject);
    var /* boolean */ bIsString = StringUtils.equals(sType, 'string');
    return bIsString;
};

//--------------------------------------------------------------------------
/**
 * @param {string} sString
 *  the string to be checked
 *
 * @returns {boolean}
 *  true if the given string is null, undefined ir empty false otherwise
 */
/* boolean */ StringUtils.isEmpty = function (/* string */ sString) {
    return ObjectUtils.isNull(sString) || (sString.length === 0);
};

//--------------------------------------------------------------------------
/**
 * @param {string} sStringA
 *  the string A to be checked against string B
 *
 * @param {string} sStringB
 *  the string B to be checked against string A
 *
 * @returns {boolean}
 *  true if both strings has the same value false otherwise
 */
/* boolean */ StringUtils.equals = function (/* string */ sStringA, /* string */ sStringB) {
    if (ObjectUtils.isNull(sStringA) && ObjectUtils.isNull(sStringB)) {
        return true;
    }

    return sStringA === sStringB;
};

//--------------------------------------------------------------------------
/**
 * @param {string} sStringA
 *  the string A to be checked against string B
 *
 * @param {string} sStringB
 *  the string B to be checked against string A
 *
 * @returns {boolean}
 *  true if both strings has the same value false otherwise
 */
/* boolean */ StringUtils.equalsIgnoreCase = function (/* string */ sStringA, /* string */ sStringB) {
    if (ObjectUtils.isNull(sStringA) && ObjectUtils.isNull(sStringB)) {
        return true;
    }

    if (!StringUtils.isEmpty(sStringA) && !StringUtils.isEmpty(sStringB)) {
        var /* string */ sLowerA = StringUtils.toLower(sStringA);
        var /* string */ sLowerB = StringUtils.toLower(sStringB);

        if (sLowerA === sLowerB) {
            return true;
        }
    }

    return false;
};

//--------------------------------------------------------------------------
/**
 * @param {string} sString
 *  the string to be converted into lower case.
 *
 * @returns {string}
 *  the lower case version of given string.
 *
 *  If given string is undefined or null the value will be returned as it is ...
 *  undefined or null.
 */
/* string */ StringUtils.toLower = function (/* string */ sString) {
    if (ObjectUtils.isNull(sString)) {
        return sString;
    }

    return sString.toLowerCase();
};

//--------------------------------------------------------------------------
/**
 * @param {string} sString
 *  the string to be converted into upper case.
 *
 * @returns {string}
 *  the upper case version of given string.
 *
 *  If given string is undefined or null the value will be returned as it is ...
 *  undefined or null.
 */
/* string */ StringUtils.toUpper = function (/* string */ sString) {
    if (ObjectUtils.isNull(sString)) {
        return sString;
    }

    return sString.toUpperCase();
};

//--------------------------------------------------------------------------
/**
 * @param {string} sString
 *  the string to be checked against here
 *
 * @param {string} sStart
 *  the string to be searched within string
 *
 * @returns {boolean}
 *  true if given string starts with the also given value false otherwise
 */
/* boolean */ StringUtils.startsWith = function (/* string */ sString, /* string */ sStart) {
    if (StringUtils.isEmpty(sString) && StringUtils.isEmpty(sStart)) {
        return true;
    }

    if (!StringUtils.isEmpty(sString) && !StringUtils.isEmpty(sStart) && (sString.indexOf(sStart, 0) === 0)) {
        return true;
    }

    return false;
};

//--------------------------------------------------------------------------
/* does the same then startsWith() but ignores letter case ...
    */
/* boolean */ StringUtils.startsWithIgnoreCase = function (/* string */ sString, /* string */ sStart) {
    var /* string  */ sLowerString = StringUtils.toLower(sString);
    var /* string  */ sLowerStart  = StringUtils.toLower(sStart);
    var /* boolean */ bStartsWith  = StringUtils.startsWith(sLowerString, sLowerStart);
    return bStartsWith;
};

//--------------------------------------------------------------------------
/**
 * @returns {number}
 *  length of given string - or 0 for 'null' strings
 */
/* int */ StringUtils.getLength = function (/* string */ sString) {
    if (StringUtils.isEmpty(sString)) {
        return 0;
    }
    return sString.length;
};

//--------------------------------------------------------------------------
/**
 * @param {string} sString
 *  the string where the substring has to be extracted from.
 *
 * @param {string} sSeparator
 *  the separator to be searched within given string.
 *
 * @returns {string}
 *  a substring after last occurrence of given separator.
 */
/* string */ StringUtils.substringAfterLast = function (/* string */ sString, /* string */ sSeparator) {
    if (StringUtils.isEmpty(sString) || StringUtils.isEmpty(sSeparator)) {
        return sString;
    }
    var /* int */ nSeperatorLength = StringUtils.getLength(sSeparator);
    var /* int */ nPos             = sString.lastIndexOf(sSeparator);

    if (nPos < 0) {
        return sString;
    }

    nPos += nSeperatorLength;
    var /* string */ sSubstring = sString.substr(nPos);
    return sSubstring;
};

//--------------------------------------------------------------------------
/**
 * Abbreviates a String using ellipses ('...').
 *  This will e.g. turn "Hello World!" into "Hell..."
 *
 * @param {string} sString
 *  the string to be abbreviated here.
 *
 * @param {number} nMaxWidth
 *  maximum length of the abbreviated string (including the suffice '...')
 */
/* string */ StringUtils.abbreviate = function (/* string */ sString, /* int    */ nMaxWidth) {
    if (StringUtils.isEmpty(sString) || ObjectUtils.isNull(nMaxWidth)) {
        return sString;
    }

    var /* int */ nWidth = StringUtils.getLength(sString);
    if (nWidth <= nMaxWidth) {
        return sString;
    }

    var /* string */ sEllipses       = '...';
    var /* int    */ nEllipsesLength = 3;

    if (nWidth <= nEllipsesLength) {
        return sEllipses;
    }

    var /* int    */ nLastIndex    = nMaxWidth - nEllipsesLength;
    var /* string */ sShortString  = StringUtils.unicodeSafeSplit(sString, nLastIndex);
    var /* string */ sResultString = sShortString + sEllipses;
    return sResultString;
};
//--------------------------------------------------------------------------
/**
 * Returns a substring which ensures no split surrogate pairs at the split
 * point. The substring can be one character shorter than specified in case
 * of a collision.
 *
 * Split surrogate pairs can create encoding errors when
 * parsing the string (e.g.via encodeURIComponent()).
 *
 *  @param sString [IN]
 *         the string to be split
 *
 *  @param nIndex [IN]
 *         the offset where the string will be split.
 *         (note: could be one char less in case of a collision)
 */
/* string */ StringUtils.unicodeSafeSplit = function (/* string */ sString, /* int */ nOffset) {
    // early out
    if (sString.length <= nOffset) { return sString; }

    // when a surrogate is split, go one step backwards to be safe
    var mustShiftOffset = !hasSurrogatePair(sString, nOffset - 1);
    var safeOffset = mustShiftOffset ? nOffset : nOffset - 1;
    if (safeOffset < 0) { safeOffset = 0; }
    return sString.substring(0, safeOffset);
};
//--------------------------------------------------------------------------
/* string */ StringUtils.defaultString = function (/* string */ sString, /* string */ sDefault) {
    var /* String */ sResult = '';

    if (!StringUtils.isEmpty(sString)) {
        sResult = sString;
    } else {
        // undefined means : not given as parameter
        // So it cant be the default.
        // not undefined (but even null) means : given as parameter
        // So it needs to be the default.
        if (sDefault !== undefined) {
            sResult = sDefault;
        }
    }

    return sResult;
};

//--------------------------------------------------------------------------
// exports

export default StringUtils;
