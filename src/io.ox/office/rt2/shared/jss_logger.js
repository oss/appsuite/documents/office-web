/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';
import URLUtils from '@/io.ox/office/rt2/shared/jss_urlutils';

//--------------------------------------------------------------------------
// const
var /* string */ URL_PARAM_LOG_SHORT = 'log-short';

//--------------------------------------------------------------------------
/* class Logger
    */
function Logger() {
    //----------------------------------------------------------------------

    // id for this logger instance (used as prefix for all logging messages)
    var /* string */ m_sId = null;

    // enable/disable short logs
    var /* boolean */ m_bShortLogs = null;

    // depending on the context (worker/window) a different logging function and
    // logLevel getter function must be used
    var rt2LogContext = {};

    //----------------------------------------------------------------------
    /* void */ this.setLogContext = function (/* object */ loggerContext) {
        rt2LogContext.write = loggerContext.write;
        rt2LogContext.getLevel = loggerContext.getLevel;
    };

    /* void */ this.setId = function (/* string */ sId) {
        m_sId = sId;
    };

    //----------------------------------------------------------------------
    /* void */ this.isLogLevel = function (/* int */ nLevel) {
        var /* boolean */ bIsLevel = impl_isLevelOn(nLevel);
        return bIsLevel;
    };

    //----------------------------------------------------------------------
    /* void */ this.error = function (/* string */ sMsg, /* object[] */ lArgs) {
        impl_logWithArgs(Logger.LEVEL_ERROR, sMsg, lArgs);
    };

    //----------------------------------------------------------------------
    /* void */ this.ex = function (/* error */ aError, /* object[] */ lArgs) {
        var /* string */ sMsg = '';

        sMsg += aError.message;

        if (!ObjectUtils.isNull(aError.stack)) {
            sMsg += '\n';
            sMsg += aError.stack;
        }

        impl_logWithArgs(Logger.LEVEL_ERROR, sMsg, lArgs);
    };

    //----------------------------------------------------------------------
    /* void */ this.warn = function (/* string */ sMsg, /* object[] */ lArgs) {
        impl_logWithArgs(Logger.LEVEL_WARN, sMsg, lArgs);
    };

    //----------------------------------------------------------------------
    /* void */ this.info = function (/* string */ sMsg, /* object[] */ lArgs) {
        impl_logWithArgs(Logger.LEVEL_INFO, sMsg, lArgs);
    };

    //----------------------------------------------------------------------
    /* void */ this.debug = function (/* string */ sMsg, /* object[] */ lArgs) {
        impl_logWithArgs(Logger.LEVEL_DEBUG, sMsg, lArgs);
    };

    //----------------------------------------------------------------------
    /* void */ this.trace = function (/* string */ sMsg, /* object[] */ lArgs) {
        impl_logWithArgs(Logger.LEVEL_TRACE, sMsg, lArgs);
    };

    //----------------------------------------------------------------------
    /* void */ function impl_logWithArgs(/* int */ nLevel, /* string */ sMsg, /* object[] */ lArgs) {
        var /* string */ sMsgWithArgs = sMsg;

        if (!ObjectUtils.isNull(lArgs)) {
            sMsgWithArgs += '\n';

            var lLogArgs = lArgs;
            if (lLogArgs.constructor !== Array) {
                lLogArgs = [];
                lLogArgs.push(lArgs);
            }

            var /* int */ c = lLogArgs.length;

            for (let i = 0; i < c; ++i) {
                var /* object */ aArg = lLogArgs[i];
                if (ObjectUtils.isNull(aArg)) {
                    aArg = '<null>';
                }

                sMsgWithArgs += 'arg [';
                sMsgWithArgs += i;
                sMsgWithArgs += '] : ';
                sMsgWithArgs += impl_arg2String(aArg);
                sMsgWithArgs += '\n';
            }
        }

        impl_log(nLevel, sMsgWithArgs);
    }

    //----------------------------------------------------------------------
    /* private String */ function impl_arg2String(/* ? */ aArg) {
        if (ObjectUtils.isFunction(aArg)) {
            return impl_func2String(aArg);
        }
        if (ObjectUtils.isObject(aArg)) {
            return impl_json2String(aArg);
        }
        return aArg.toString();
    }

    //----------------------------------------------------------------------
    /* private String */ function impl_func2String(/* Function aFunc*/) {
        return 'func[TODO implement me]';
    }

    // //----------------------------------------------------------------------
    // /* private String */ function impl_obj2String(/* Object */ aObject)
    // {
    //     var /* String   */ sString = '';
    //     var /* String[] */ lKeys   = Object.keys(aObject);
    //     var /* int      */ c       = lKeys.length;

    //     sString += '{\n';
    //     for (var /* int */ i=0; i<c; ++i)
    //     {
    //         var /* String */ sKey   = lKeys  [i   ];
    //         var /* Object */ aValue = aObject[sKey];
    //         sString += '  ';
    //         sString += sKey;
    //         sString += ' : ';
    //         sString += aValue;
    //         sString += '\n';
    //     }
    //     sString += '}\n';

    //     return sString;
    // }

    //----------------------------------------------------------------------
    /* private String */ function impl_json2String(/* JSON */ aJSON) {
        return JSON.stringify(aJSON);
    }

    //----------------------------------------------------------------------
    /* void */ function impl_log(/* int */ nLevel, /* string */ sMsg) {
        var /* boolean */ bIsLevelOn = impl_isLevelOn(nLevel);
        if (!bIsLevelOn) {
            return;
        }

        var /* string */ sOut = '';

        sOut += '[';
        sOut += m_sId;
        sOut += '] ';
        sOut += '(';
        sOut += impl_getLevelString(nLevel);
        sOut += ') ';
        sOut += impl_getTimstamp();
        sOut += ' : ';
        sOut += sMsg;

        var /* string */ sNormOut = sOut;
        if (impl_isShortLogOn()) {
            sNormOut = StringUtils.abbreviate(sOut, 500);
        }

        rt2LogContext.write(nLevel, sNormOut);
    }

    //----------------------------------------------------------------------
    /* boolean */ function impl_isLevelOn(/* int */ nLevel) {
        var /* boolean */ bIsOn = (nLevel <= rt2LogContext.getLevel());
        return bIsOn;
    }

    //----------------------------------------------------------------------
    /* string */ function impl_getLevelString(/* int */ nLevel) {
        switch (nLevel) {
            case Logger.LEVEL_ERROR:    return 'E';
            case Logger.LEVEL_WARN:     return 'W';
            case Logger.LEVEL_INFO:     return 'I';
            case Logger.LEVEL_DEBUG:    return 'D';
            case Logger.LEVEL_TRACE:    return 'T';
            default:                    throw new Error(`No support for log level "${nLevel}" implemented yet.`);
        }
    }

    //----------------------------------------------------------------------
    /* string */ function impl_getTimstamp() {
        var /* Date */ aNow = new Date();
        return aNow.toISOString();
    }

    //----------------------------------------------------------------------
    /* boolean */ function impl_isShortLogOn() {
        if (ObjectUtils.isNull(m_bShortLogs)) {
            var /* string */ sUrl = URLUtils.getCurrentURL();
            m_bShortLogs = !sUrl.includes(`${URL_PARAM_LOG_SHORT}=false`);
        }

        return m_bShortLogs;
    }
} // class Logger

//--------------------------------------------------------------------------
/* int */ Logger.LEVEL_NONE  = 0;
/* int */ Logger.LEVEL_ERROR = 1;
/* int */ Logger.LEVEL_WARN  = 2;
/* int */ Logger.LEVEL_INFO  = 3;
/* int */ Logger.LEVEL_DEBUG = 4;
/* int */ Logger.LEVEL_TRACE = 5;

//--------------------------------------------------------------------------
/* Logger */ Logger.create = function (/* string */ sId, loggerContext) {
    var /* Logger */ aLogger = new Logger();
    aLogger.setLogContext(loggerContext);
    aLogger.setId(sId);
    return aLogger;
};

//--------------------------------------------------------------------------
// exports

export default Logger;
