/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';


//--------------------------------------------------------------------------
// const

//--------------------------------------------------------------------------
// class

function ErrorCode() {
    //----------------------------------------------------------------------
    // const
    // var /* private static int */ ERRORCLASS_COUNT        = 4;
    // var /* private static int */ ERRORCLASS_NO_ERROR     = 0;
    var /* private static int */ ERRORCLASS_WARNING      = 1;
    var /* private static int */ ERRORCLASS_ERROR        = 2;
    // var /* private static int */ ERRORCLASS_FATAL_ERROR  = 3;

    //----------------------------------------------------------------------
    // member

    var /* int    */ m_code                 = null;
    var /* String */ m_codeAsStringConstant = null;
    var /* String */ m_description          = null;
    var /* String */ m_value                = null;
    var /* int    */ m_errorClass           = null;

    //----------------------------------------------------------------------
    /* public boolean */ this.isError = function () {
        return (m_errorClass >= ERRORCLASS_ERROR);
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.isNoError = function () {
        return (m_errorClass <= ERRORCLASS_WARNING);
    };

    //----------------------------------------------------------------------
    /* public void */ this.setCode = function (/* int */ nCode) {
        m_code = nCode;
    };

    //----------------------------------------------------------------------
    /* public void */ this.setCodeAsStringConstant = function (/* string */ sCodeAsStringConstant) {
        m_codeAsStringConstant = sCodeAsStringConstant;
    };

    //----------------------------------------------------------------------
    /* public void */ this.setDescription = function (/* string */ sDescription) {
        m_description = sDescription;
    };

    //----------------------------------------------------------------------
    /* public void */ this.setValue = function (/* string */ sValue) {
        m_value = sValue;
    };

    //----------------------------------------------------------------------
    /* public void */ this.setErrorClass = function (/* int */ nErrorClass) {
        m_errorClass = nErrorClass;
    };

    //----------------------------------------------------------------------
    /* public int */ this.getCode = function () {
        return m_code;
    };

    //----------------------------------------------------------------------
    /* public String */ this.getCodeAsStringConstant = function () {
        return m_codeAsStringConstant;
    };

    //----------------------------------------------------------------------
    /* public String */ this.getDescription = function () {
        return m_description;
    };

    //----------------------------------------------------------------------
    /* public String */ this.getValue = function () {
        return m_value;
    };

    //----------------------------------------------------------------------
    /* public int */ this.getErrorClass = function () {
        return m_errorClass;
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.equals = function (/* ErrorCode | int ! */ aError) {
        var /* int */ nCode = ObjectUtils.isNumber(aError) ? aError : aError.getCode();
        return nCode === m_code;
    };

} // class ErrorCode

//--------------------------------------------------------------------------
/* public static int */ ErrorCode.CODE_NO_ERROR                                   =  0;
/* public static int */ ErrorCode.CODE_GENERAL_UNKNOWN_ERROR                      =  1;
/* public static int */ ErrorCode.CODE_GENERAL_ARGUMENTS_ERROR                    = 10;
/* public static int */ ErrorCode.CODE_GENERAL_FILE_NOT_FOUND_ERROR               = 11;
/* public static int */ ErrorCode.CODE_GENERAL_QUOTA_REACHED_ERROR                = 12;
/* public static int */ ErrorCode.CODE_GENERAL_PERMISSION_CREATE_MISSING_ERROR    = 13;
/* public static int */ ErrorCode.CODE_GENERAL_PERMISSION_READ_MISSING_ERROR      = 14;
/* public static int */ ErrorCode.CODE_GENERAL_PERMISSION_WRITE_MISSING_ERROR     = 15;
/* public static int */ ErrorCode.CODE_GENERAL_MEMORY_TOO_LOW_ERROR               = 16;
/* public static int */ ErrorCode.CODE_GENERAL_SYSTEM_BUSY_ERROR                  = 17;
/* public static int */ ErrorCode.CODE_GENERAL_FILE_LOCKED_ERROR                  = 18;
/* public static int */ ErrorCode.CODE_GENERAL_FILE_CORRUPT_ERROR                 = 19;
/* public static int */ ErrorCode.CODE_GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR = 20;
/* public static int */ ErrorCode.CODE_GENERAL_BACKGROUNDSAVE_IN_PROGRESS_ERROR   = 21;
/* public static int */ ErrorCode.CODE_GENERAL_FOLDER_NOT_VISIBLE_ERROR           = 22;
/* public static int */ ErrorCode.CODE_GENERAL_FOLDER_NOT_FOUND_ERROR             = 23;
/* public static int */ ErrorCode.CODE_GENERAL_FOLDER_INVALID_ID_ERROR            = 24;
/* public static int */ ErrorCode.CODE_GENERAL_SESSION_INVALID_ERROR              = 25;
/* public static int */ ErrorCode.CODE_LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR     = 403;

//--------------------------------------------------------------------------
/* public static ErrorCode */ ErrorCode.create = function (/*int*/ nCode, /*string*/ sCodeAsString, /*string*/ sDescription, /*int*/ nErrorClass, /*string*/ sValue) {
    var /* ErrorCode */ aError = new ErrorCode();
    aError.setCode(nCode);
    aError.setCodeAsStringConstant(sCodeAsString);
    aError.setDescription(sDescription);
    aError.setErrorClass(nErrorClass);
    aError.setValue(sValue);
    return aError;
};

//----------------------------------------------------------------------
/* public static boolean */ ErrorCode.isErrorCode = function (/* aynthing */ aAny) {
    return (ObjectUtils.isObject(aAny) &&
        ObjectUtils.isFunction(aAny.getCode) &&
        ObjectUtils.isFunction(aAny.getCodeAsStringConstant) &&
        ObjectUtils.isFunction(aAny.getErrorClass));
};

//--------------------------------------------------------------------------
/* public static ErrorCode */ ErrorCode.NO_ERROR = ErrorCode.create(ErrorCode.CODE_NO_ERROR, 'NO_ERROR', 'No error', 0);

//--------------------------------------------------------------------------
// exports

export default ErrorCode;
