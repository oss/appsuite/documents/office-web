/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import Iterator from '@/io.ox/office/rt2/shared/jss_iterator';

//--------------------------------------------------------------------------
/* class List< T >
    */
function List() {
    //----------------------------------------------------------------------
    var /* object */ self = this;

    var /* T[] */ m_aCore = null;

    //----------------------------------------------------------------------
    /* public int */ this.size = function () {
        var /* int */ nSize = mem_Core().length;
        return nSize;
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.isEmpty = function () {
        var /* boolean */ bIsEmpty = (self.size() < 1);
        return bIsEmpty;
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.contains = function (/* T */ aItem) {
        var /* int     */ nIndex    = self.indexOf(aItem);
        var /* boolean */ bContains = (nIndex >= 0);
        return bContains;
    };

    //----------------------------------------------------------------------
    /* public Iterator< T > */ this.iterator = function () {
        var /* Iterator< T > */ aIt = Iterator.create(self.toArray());
        return aIt;
    };

    //----------------------------------------------------------------------
    /* public T[] */ this.toArray = function () {
        return mem_Core();
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.add = function (/* T */ aItem) {
        var /* boolean */ bChanged  = false;
        var /* boolean */ bContains = self.contains(aItem);

        if (!bContains) {
            mem_Core().push(aItem);
            bChanged = true;
        }

        return bChanged;
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.remove = function (/* T */ aItem) {
        var /* boolean */ bChanged = false;
        var /* int     */ nIndex   = self.indexOf(aItem);
        if (nIndex >= 0) {
            mem_Core().splice(nIndex, 1);
            bChanged = true;
        }
        return bChanged;
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.containsAll = function (/* List< T > | T[] ! lItems */) {
        throw new Error('not implemented yet');
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.addAll = function (/* List< T > | T[] ! */ lItems) {
        if (ObjectUtils.isNull(lItems)) {
            return false;
        }
        if (ObjectUtils.isArray(lItems)) {
            return impl_addAllFromArray(lItems);
        }
        return impl_addAllFromList(lItems);
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.removeAll = function (/* List< T > | T[] ! lItems */) {
        throw new Error('not implemented yet');
    };

    //----------------------------------------------------------------------
    /* public void */ this.clear = function () {
        m_aCore = null;
    };

    //----------------------------------------------------------------------
    /* public T */ this.get = function (/* int */ nIndex) {
        var /* T[] */ aCore = mem_Core();
        var /* T   */ aItem = aCore[nIndex];
        return aItem;
    };

    //----------------------------------------------------------------------
    /* public T */ this.set = function (/* int nIndex, T   aItem */) {
        throw new Error('not implemented yet');
    };

    //----------------------------------------------------------------------
    /* public int */ this.indexOf = function (/* T */ aItem) {
        return m_aCore ? m_aCore.indexOf(aItem) : -1;
    };

    //----------------------------------------------------------------------
    /* public int */ this.lastIndexOf = function (/* T aItem */) {
        throw new Error('not implemented yet');
    };

    //----------------------------------------------------------------------
    /* public List< T > */ this.subList = function (/* int nFromIndex, int nToIndex*/) {
        throw new Error('not implemented yet');
    };

    //----------------------------------------------------------------------
    /* public String */ this.toString = function () {
        var /* T[] */ lItems = self.toArray();
        return `[${lItems.join(', ')}]`;
    };

    //----------------------------------------------------------------------
    /* private boolean */ function impl_addAllFromArray(/* T[] */ lItems) {
        var /* boolean */ bChangedByAnyItem = false;
        for (const /* T */ aItem of lItems) {
            var /* boolean */ bChangedByThisItem = self.add(aItem);
            bChangedByAnyItem ||= bChangedByThisItem;
        }
        return bChangedByAnyItem;
    }

    //----------------------------------------------------------------------
    /* private boolean */ function impl_addAllFromList(/* List< T > */ lItems) {
        var /* T[]     */ aArray            = lItems.toArray();
        var /* boolean */ bChangedByAnyItem = impl_addAllFromArray(aArray);
        return bChangedByAnyItem;
    }

    //----------------------------------------------------------------------
    /* private T[] */ function mem_Core() {
        if (!m_aCore) {
            m_aCore = [];
        }
        return m_aCore;
    }

} // class List

//--------------------------------------------------------------------------
/* public static List< T > */ List.fromVarArg = function (/* T... */ aVarArg) {
    var /* List< T > */ aList = new List();

    if (ObjectUtils.isArray(aVarArg)) {
        for (const /* T */ aArrayItem of aVarArg) {
            aList.add(aArrayItem);
        }
    } else {
        aList.add(aVarArg);
    }

    return aList;
};

//--------------------------------------------------------------------------
// exports

export default List;
