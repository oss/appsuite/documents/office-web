/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import List from '@/io.ox/office/rt2/shared/jss_list';

//--------------------------------------------------------------------------
// const

//--------------------------------------------------------------------------
/* class Map< K, V >
    */
function HashMap() {
    //----------------------------------------------------------------------
    var /* object */ self = this;

    var /* JSON */ m_aCore = null;

    //----------------------------------------------------------------------
    /* public int */ this.size = function () {
        var /* List< K > */ lKeys = self.keySet();
        var /* int       */ nSize = lKeys.size();
        return nSize;
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.isEmpty = function () {
        var /* boolean */ bIsEmpty = (self.size() < 1);
        return bIsEmpty;
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.containsKey = function (/* K */ aKey) {
        var /* JSON */ aCore  = mem_Core();
        return Object.prototype.hasOwnProperty.call(aCore, aKey);
    };

    //----------------------------------------------------------------------
    /* public boolean */ this.containsValue = function (/* V */ aValue) {
        var /* List< V > */ lValues   = self.values();
        return lValues.contains(aValue);
    };

    //----------------------------------------------------------------------
    /* public V */ this.get = function (/* K */ aKey) {
        var /* JSON */ aCore  = mem_Core();
        var /* V    */ aValue = aCore[aKey];
        return aValue;
    };

    //----------------------------------------------------------------------
    /* public V */ this.put = function (/* K */ aKey, /* V */ aValue) {
        var /* JSON */ aCore     = mem_Core();
        var /* V    */ aOldValue = aCore[aKey];
        aCore[aKey] = aValue;
        return aOldValue;
    };

    //----------------------------------------------------------------------
    /* public V */ this.remove = function (/* K */ aKey) {
        var /* JSON */ aCore     = mem_Core();
        var /* V    */ aOldValue = aCore[aKey];
        delete aCore[aKey];
        return aOldValue;
    };

    //----------------------------------------------------------------------
    /* public void */ this.putAll = function (/* Map< K, V > aMap */) {
        throw new Error('not implemented yet');
    };

    //----------------------------------------------------------------------
    /* public void */ this.clear = function () {
        m_aCore = null;
    };

    //----------------------------------------------------------------------
    /* public List< K > */ this.keySet = function () {
        var /* JSON      */ aCore     = mem_Core();
        var /* K[]       */ aKeyArray = Object.keys(aCore);
        var /* List< K > */ aKeyList  = new List();

        for (const /* K */ aKey of aKeyArray) {
            aKeyList.add(aKey);
        }

        return aKeyList;
    };

    //----------------------------------------------------------------------
    /* public List< V > */ this.values = function () {
        var /* JSON      */ aCore       = mem_Core();
        var /* K[]       */ aKeyArray   = Object.keys(aCore);
        var /* List< V > */ aValueList  = new List();

        for (const /* K */ aKey of aKeyArray) {
            var /* V */ aValue = aCore[aKey];
            aValueList.add(aValue);
        }

        return aValueList;
    };

    //----------------------------------------------------------------------
    /* public String */ this.toString = function () {
        var /* K[] */ lKeys = Object.keys(mem_Core());
        return `{${lKeys.map(aKey => `${aKey}=${self.get(aKey)}`).join(', ')}}`;
    };

    //----------------------------------------------------------------------
    /* private JSON */ function mem_Core() {
        if (!m_aCore) {
            m_aCore = {};
        }
        return m_aCore;
    }

} // class HashMap

//--------------------------------------------------------------------------
/* public HashMap< K, V > */ HashMap.fromMapOrJSON = function (/* HashMap< K, V > | JSON > */ aSource) {
    var /* HashMap< K, V > */ aMap   = new HashMap();

    if (ObjectUtils.isNotNull(aSource.keySet)) {
        var /* Iterator< K > */ aIt = aSource.keySet().iterator();
        while (aIt.hasNext()) {
            const aKey   = aIt.next();
            const aValue = aSource.get(aKey);
            aMap.put(aKey, aValue);
        }
    } else {
        var /* K[] */ lKeys = Object.keys(aSource);
        for (const aKey of lKeys) {
            const aValue = aSource[aKey];
            aMap.put(aKey, aValue);
        }
    }

    return aMap;
};

//--------------------------------------------------------------------------
// exports

export default HashMap;
