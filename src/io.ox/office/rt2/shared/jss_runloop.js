/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import Validate from '@/io.ox/office/rt2/shared/jss_validate';
import Logger from '@/io.ox/office/rt2/shared/jss_logger';

//--------------------------------------------------------------------------
/* class RunLoop
    */
function RunLoop() {
    //----------------------------------------------------------------------

    // for setInterval we need the global context (window or webworker !)
    var /* object */ m_aGlobalContext = null;

    // handler for loop events
    var /* function */ m_fHandler = null;

    // frequency for polling for events
    var /* int */ m_nFrequency = 1;

    // internal buffer
    var /* object[] */ m_lBuffer = null;

    // startInterval returns handle we can use to stop our loop !
    var /* object */ m_aIntervalHandle = null;

    var /* Logger */ m_aLog = null;

    var m_loggerContext = null;

    //----------------------------------------------------------------------
    /* void */ this.setGlobalContext = function (/* object */ aContext) {
        m_aGlobalContext = aContext;
    };

    //----------------------------------------------------------------------
    /* void */ this.setHandler = function (/* function */ fHandler) {
        m_fHandler = fHandler;
    };

    //----------------------------------------------------------------------
    /* void */ this.setFrequency = function (/* int */ nFrequency) {
        m_nFrequency = nFrequency;
    };

    /* void */ this.setLogContext = function (/* object */ loggerContext) {
        m_loggerContext = loggerContext;
    };

    //----------------------------------------------------------------------
    /* void */ this.start = function () {
        if (!ObjectUtils.isNull(m_aIntervalHandle)) {
            return;
        }

        log().trace('start');

        Validate.isNotNull(m_aGlobalContext, 'No global context defined.');
        Validate.isNotNull(m_fHandler,       'No handler defined.');

        m_aIntervalHandle = m_aGlobalContext.setInterval(impl_handleEvent, m_nFrequency);
    };

    //----------------------------------------------------------------------
    /* void */ this.stop = function () {
        if (ObjectUtils.isNull(m_aIntervalHandle)) {
            return;
        }

        log().trace('stop');

        var /* object */ aIntervalHandle = m_aIntervalHandle;
        m_aIntervalHandle = null;
        m_aGlobalContext.clearInterval(aIntervalHandle);
    };

    //----------------------------------------------------------------------
    /* void */ this.enqueue = function (/* object */ aEvent) {
        log().trace('enqueue', aEvent);
        Validate.isNotNull(aEvent, 'Illegal argument "event".');
        var /* object[] */ aBuffer = mem_Buffer();
        aBuffer.push(aEvent);
    };

    //----------------------------------------------------------------------
    /* int */ this.getQueueSize = function () {
        var /* object[] */ aBuffer = mem_Buffer();
        var /* int      */ nSize   = aBuffer.length;
        return nSize;
    };

    //----------------------------------------------------------------------
    function /* void */ impl_handleEvent() {
        var /* object[] */ aBuffer  = mem_Buffer();
        var /* function */ fHandler = m_fHandler;

        if (aBuffer.length < 1) {
            return;
        }

        log().trace('impl_handleEvent ...');

        if (ObjectUtils.isNull(fHandler)) {
            log().warn('... no handler defined.');
            return;
        }

        try {
            var /* object  */ aEvent = aBuffer[0];
            if (ObjectUtils.isNull(aEvent)) {
                log().warn('... unexpected empty event in buffer');
                return;
            }

            log().trace('... call handler', aEvent);
            var /* boolean */ bOK = fHandler(aEvent);
            if (!bOK) {
                log().error('... handler didnt handle event !');
                return;
            }

            log().trace('ok');
            aBuffer.shift();
        } catch (/* Error */ ex) {
            log().error(ex);
        }
    }

    //----------------------------------------------------------------------
    function /* object[] */ mem_Buffer() {
        if (ObjectUtils.isNull(m_lBuffer)) {
            m_lBuffer = [];
        }
        return m_lBuffer;
    }

    //----------------------------------------------------------------------
    function log() {
        if (ObjectUtils.isNull(m_aLog)) {
            m_aLog = Logger.create('RunLoop', m_loggerContext);
        }
        return m_aLog;
    }

} // class RunLoop

//--------------------------------------------------------------------------
RunLoop.create = function (/* object */ aGlobalContext, /* function */ fHandler, loggerContext) {
    var /* RunLoop */ aRunLoop = new RunLoop();
    aRunLoop.setLogContext(loggerContext);
    aRunLoop.setGlobalContext(aGlobalContext);
    aRunLoop.setHandler(fHandler);
    return aRunLoop;
};

//--------------------------------------------------------------------------
// exports

export default RunLoop;
