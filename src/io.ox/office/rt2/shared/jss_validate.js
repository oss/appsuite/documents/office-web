/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';

//--------------------------------------------------------------------------
/* class Validate
    */
function Validate() {
    // helper with static functions only
} // class Validate

//--------------------------------------------------------------------------
/* void */ Validate.isNotNull = function (/* object */ aObject, /* string */ sMessage) {
    Validate.notNull(aObject, sMessage);
};

//--------------------------------------------------------------------------
/* void */ Validate.notNull = function (/* object */ aObject, /* string */ sMessage) {
    if (ObjectUtils.isNull(aObject)) {
        throw new Error(sMessage);
    }
};

//--------------------------------------------------------------------------
/* void */ Validate.notEmpty = function (/* string */ sString, /* string */ sMessage) {
    if (StringUtils.isEmpty(sString)) {
        throw new Error(sMessage);
    }
};

//--------------------------------------------------------------------------
/* void */ Validate.isTrue = function (/* boolean */ bCheck, /* string  */ sMessage) {
    if (!bCheck) {
        throw new Error(sMessage);
    }
};

//--------------------------------------------------------------------------
/* void */ Validate.isFalse = function (/* boolean */ bCheck, /* string  */ sMessage) {
    if (bCheck) {
        throw new Error(sMessage);
    }
};

//--------------------------------------------------------------------------
// exports

export default Validate;
