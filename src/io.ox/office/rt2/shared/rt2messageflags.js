/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

//--------------------------------------------------------------------------
// const

function RT2MessageFlags() {
} // RT2MessageFlags

//-------------------------------------------------------------------------
/* public static int */ RT2MessageFlags.FLAG_NONE                  =          0;
/* public static int */ RT2MessageFlags.FLAG_ALL                   = 2147483647; // max int
/* public static int */ RT2MessageFlags.FLAG_SIMPLE_MSG            =          1;
/* public static int */ RT2MessageFlags.FLAG_SESSION_BASED         =          2;
/* public static int */ RT2MessageFlags.FLAG_SEQUENCE_NR_BASED     =          4;
/* public static int */ RT2MessageFlags.FLAG_CHUNK_BASED           =          8;
/* public static int */ RT2MessageFlags.FLAG_DOC_BASED             =         16;

//-------------------------------------------------------------------------
/* public static string */ RT2MessageFlags.FLAG_STR_NONE              = 'NONE';
/* public static string */ RT2MessageFlags.FLAG_STR_ALL               = 'ALL';
/* public static string */ RT2MessageFlags.FLAG_STR_SIMPLE_MSG        = 'SIMPLE_MSG';
/* public static string */ RT2MessageFlags.FLAG_STR_SESSION_BASED     = 'SESSION_BASED';
/* public static string */ RT2MessageFlags.FLAG_STR_SEQUENCE_NR_BASED = 'SEQUENCE_NR_BASE';
/* public static string */ RT2MessageFlags.FLAG_STR_CHUNK_BASED       = 'CHUNK_BASED';
/* public static string */ RT2MessageFlags.FLAG_STR_DOC_BASED         = 'DOC_BASED';

//-------------------------------------------------------------------------
/* public static String */ RT2MessageFlags.mapFlagToString = function (/* int */ nFlag) {
    switch (nFlag) {
        case RT2MessageFlags.FLAG_NONE:                 return RT2MessageFlags.FLAG_STR_NONE;
        case RT2MessageFlags.FLAG_ALL:                  return RT2MessageFlags.FLAG_STR_ALL;
        case RT2MessageFlags.FLAG_SIMPLE_MSG:           return RT2MessageFlags.FLAG_STR_SIMPLE_MSG;
        case RT2MessageFlags.FLAG_SESSION_BASED:        return RT2MessageFlags.FLAG_STR_SESSION_BASED;
        case RT2MessageFlags.FLAG_SEQUENCE_NR_BASED:    return RT2MessageFlags.FLAG_STR_SEQUENCE_NR_BASED;
        case RT2MessageFlags.FLAG_CHUNK_BASED:          return RT2MessageFlags.FLAG_STR_CHUNK_BASED;
        case RT2MessageFlags.FLAG_DOC_BASED:            return RT2MessageFlags.FLAG_STR_DOC_BASED;
        default:                                        throw new Error(`no support for flag "${nFlag}" implemented yet.`);
    }
};

//--------------------------------------------------------------------------
// exports

export default RT2MessageFlags;
