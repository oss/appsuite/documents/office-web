/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import ox from '$/ox';

import { random as randomUUID } from '@/io.ox/office/tk/algorithms/uuid';

import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import Validate from '@/io.ox/office/rt2/shared/jss_validate';
import URLUtils from '@/io.ox/office/rt2/shared/jss_urlutils';
import Logger from '@/io.ox/office/rt2/shared/jss_logger';
import HashMap from '@/io.ox/office/rt2/shared/jss_hashmap';
import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';
import RT2MessageFactory from '@/io.ox/office/rt2/shared/rt2messagefactory';
import RT2MessageGetSet from '@/io.ox/office/rt2/shared/rt2messagegetset';
import Rt2LoggerContextMain from '@/io.ox/office/rt2/rt2loggercontext_main';

//--------------------------------------------------------------------------
// const

var /* string */ API_RT2_ROOT              = 'rt2';
var /* string */ API_RT2_WS                = `${API_RT2_ROOT}/v1/default`;

var /* string */ CHANNEL_TYPE_WEBSOCKET    = 'websocket';
var /* string */ CHANNEL_TYPE_POLLING      = 'polling';

// worker.js is the pre-build, distributed version of 'rt2websocketchannel.js'
// with all it's dependencies, see vite.config
var /* string */ SCRIPT_RT2_WSCHANNEL_JS   = 'office/rt2/dist/worker.js';

// var /* string */ SCRIPT_RT2_POLLCHANNEL_JS = 'office/rt2/rt2pollchannel.js';

// var /* string */ URLPARAM_BACKEND_HOST     = 'backend-host';
// var /* string */ URLPARAM_BACKEND_PORT     = 'backend-port';

//--------------------------------------------------------------------------
// class
function RT2Channel() {
    //----------------------------------------------------------------------
    // member

    var self = this;

    // every channel has it's own UID ...
    // used to create connection URL and distinguish between several RT2 channels on server side
    var /* string */ m_sChannelID = null;

    // corresponding communication channel (WebSocket or Poll)
    var /* WebWorker */ m_aChannel = null;

    // register clients with her callbacks for getting events
    var /* map< string, json > */ m_aClientRegistry = null;

    // // map outgoing/incoming messages to clients (key=msgid, value=clientid)
    // var /* map< string, string > */ m_aMessagesToClientsMap = null;

    var /* Logger */ m_aLog = null;

    //-------------------------------------------------------------------------------
    /**
     * register a callback for the client with the given Id
     *
     *  Those callback is used to notify client in case server
     *  response messages occur.
     *
     *  @param sRT2ClientUID [IN]
     *         an (unique!) Id of the client
     *
     *  @param aCallback [IN]
     *         the callback function of those client
     */
    /* public void */ this.registerClient = function (/* string */ sRT2ClientUID, /* object */ aClient, /* function */ aCallback) {
        log().trace('registerClient', [sRT2ClientUID, aClient, aCallback]);
        impl_registerClient(sRT2ClientUID, aClient, aCallback);
    };

    //-------------------------------------------------------------------------------
    /**
     * de-register a callback for the client with the given Id
     *
     *  @param sRT2ClientUID [IN]
     *         an (unique!) Id of the client
     */
    /* public void */ this.unregisterClient = function (/* string */ sRT2ClientUID) {
        log().trace('unregisterClient', [sRT2ClientUID]);
        //impl_unbindAllMessagesOfClient(sRT2ClientUID);
        impl_unregisterClient(sRT2ClientUID);
    };

    //-------------------------------------------------------------------------------
    /* public void */ this.destroy = function () {
        if (m_aChannel) {
            m_aChannel.terminate();
            m_aChannel = null;
            m_aClientRegistry = null;
        }
    };

    //-------------------------------------------------------------------------------
    /* public String */ this.getChannelUID = function () {
        return mem_ChannelID();
    };

    //-------------------------------------------------------------------------------
    /**
     * send the given message to the server ...
     *
     *  @param sRT2ClientUID [IN]
     *         an (unique!) Id of the client sending this message
     *
     *  @param aMessage [IN]
     *         the message to be send to the server
     */
    /* public void */ this.sendRequest = function (/* string */ sRT2ClientUID, /* RT2Message */ aMessage) {
        log().trace('sendRequest', [sRT2ClientUID, aMessage]);
        impl_sendRequest(sRT2ClientUID, aMessage);
    };

    //-------------------------------------------------------------------------------
    /* private void */ function impl_registerClient(/* string */ sRT2ClientUID, /* object */ aClient, /* function */ aCallback) {
        log().trace('impl_registerClient', [sRT2ClientUID, aClient, aCallback]);
        var /* map< string, json > */ aRegistry = mem_ClientRegistry();
        aRegistry.put(sRT2ClientUID, {
            client_id: sRT2ClientUID,
            client:    aClient,
            callback:  aCallback
        });
    }

    //-------------------------------------------------------------------------------
    /* private void */ function impl_unregisterClient(/* string */ sRT2ClientUID) {
        log().trace('impl_unregisterClient', [sRT2ClientUID]);
        var /* map< string, json > */ aRegistry = mem_ClientRegistry();
        aRegistry.remove(sRT2ClientUID);

        // destroy this global channel in case last client gone
        if (aRegistry.isEmpty()) {
            RT2Channel.destroy();
        }
    }

    //----------------------------------------------------------------------
    /* private json */ function impl_getCallbackForClient(/* string */ sRT2ClientUID) {
        log().trace('impl_getCallbackForClient', [sRT2ClientUID]);
        var /* map< string, json > */ aRegistry = mem_ClientRegistry();
        var /* json                */ jEntry    = aRegistry.get(sRT2ClientUID);
        return jEntry;
    }

    //----------------------------------------------------------------------
    /* private void */ function impl_sendRequest(/* string */ sRT2ClientUID, /* RT2Message */ aRequest) {
        log().trace('impl_sendRequest', [sRT2ClientUID, aRequest]);
        var /* json   */ sRequest4Send   = RT2MessageFactory.toJSONString(aRequest);
        var /* json   */ jChannelRequest = { type: 'send_request', request: sRequest4Send };

        var /* string */ sChannelRequest = JSON.stringify(jChannelRequest);
        var /* Worker */ aChannel        = mem_Channel();

        // impl_bindMessageIdToClient(sRT2ClientUID, sMsgId);
        aChannel.postMessage(sChannelRequest);
    }

    //----------------------------------------------------------------------
    // the worker can send log messages or the actual server response
    /* private void */ function impl_onWorkerResponse(/* MessageObject */ aEvent) {
        var isLogMsg = Boolean(aEvent.data.log);
        // forward worker messages to global rt2 logging
        if (isLogMsg) {

            if (aEvent && aEvent.data && aEvent.data.log && _.isNumber(aEvent.data.logLevel)) {
                Rt2LoggerContextMain.write(aEvent.data.logLevel, aEvent.data.log);
            } else {
                log().warn('... Error in Log message structure from the WebWorker');
            }

        // forward server response
        } else {
            impl_onServerResponse(aEvent);
        }

    }

    //----------------------------------------------------------------------
    /**
     * callback from our WebSocket communication channel
     */
    /* private void */ function impl_onServerResponse(/* MessageObject */ aEvent) {
        var /* string */ sResponse = aEvent?.data;

        log().debug('impl_onServerResponse', sResponse || "");

        if (ObjectUtils.isNull(self)) {
            log().warn('... got message from websocket, but instance gone!');
            return;
        }

        if (!sResponse) {
            log().warn('... got server response with no data ?!');
            return;
        }

        try {
            var /* RT2Message */ aResponse     = RT2MessageFactory.fromJSONString(sResponse);
            var /* string     */ sRT2ClientUID = null;

            // tricky : under normal circumstances responses came from server and
            // contain client uid of the original request.
            // But special responses (as e.g. offline/online) are generated
            // on client side directly. They do not contain any client uid !
            // They have to be broadcast to all known clients  ... see below.
            if (aResponse.hasHeader(RT2Protocol.HEADER_CLIENT_UID)) {
                sRT2ClientUID = RT2MessageGetSet.getClientUID(aResponse);
            }

            // a) direct response for one specific client
            if (typeof sRT2ClientUID === "string") {
                impl_notifyClient(sRT2ClientUID, aResponse);
                return;
            }

            // b) (internal) 'broadcast' to all clients !
            //    e.g. offline mode
            var /* map< string, json > */ aRegistry = mem_ClientRegistry();
            var /* Iterator< String >  */ rRegistry = aRegistry.keySet().iterator();
            while (rRegistry.hasNext()) {
                sRT2ClientUID = rRegistry.next();
                RT2MessageGetSet.setClientUID(aResponse, sRT2ClientUID);
                impl_notifyClient(sRT2ClientUID, aResponse);
            }
        } catch (/* Error */ ex) {
            log().error('Exception on handling server response: ' + sResponse);
            log().error(ex);
        }
    }

    //----------------------------------------------------------------------
    /* private void */ function impl_notifyClient(/* string */ sRT2ClientUID, /* RT2Message */ aResponse) {
        log().trace('impl_notifyClient', [sRT2ClientUID, aResponse]);
        try {
            // var /* string   */ sMsgId    = jResponse.header.msg_id;
            var /* json     */ jClient   = impl_getCallbackForClient(sRT2ClientUID);
            var /* object   */ aClient   = ObjectUtils.getObjectPartOrDefault(jClient, 'client', null);
            var /* function */ aCallback = ObjectUtils.getObjectPartOrDefault(jClient, 'callback', null);

            if (ObjectUtils.isNull(aClient) || ObjectUtils.isNull(aCallback)) {
                log().warn(`... callback for "${sRT2ClientUID}" is gone. Drop response message.`);
                return;
            }

            aCallback.call(aClient, aResponse);
        } catch (/* Error */ ex) {
            log().error(ex);
        }
    }

    //----------------------------------------------------------------------
    /**
     * @returns initialized URL for connectin to remote WebSocket API
     */
    /* private void */ function impl_createWsUrl() {
        log().trace('impl_createWsUrl');

        Validate.notEmpty(ox.abs, 'Miss global config "ox.abs".');

        var /* string */ sBaseUrl     = ox.serverConfig.websocketUri ? ox.serverConfig.websocketUri : ox.abs;
        var /* string */ sUrl         = sBaseUrl + ox.root;
        var /* string */ sChannelID   = mem_ChannelID();
        var /* string */ sProtocol    = URLUtils.getProtocol(sBaseUrl);

        // we can have 3 valid cases (DOCS-1433):
        // 1. Protocol is https, use 'wss'.
        // 2. Protocol is http, use 'ws'.
        // 3. Protocol is an empty string (probably because the protocol
        // was not defined in the as-config.yml), use 'ox.abs'
        // as fallback.

        // fallback
        if (sProtocol === '') {
            sProtocol = URLUtils.getProtocol(ox.abs);
        }

        // convert to WS
        if (sProtocol === 'http') {
            sUrl = URLUtils.setProtocol(sUrl, 'ws');
        } else {
            sUrl = URLUtils.setProtocol(sUrl, 'wss');
        }

        if (!sUrl.endsWith('/')) {
            sUrl += '/';
        }

        sUrl += API_RT2_WS;
        sUrl += '/';
        sUrl += sChannelID;

        return sUrl;
    }

    //----------------------------------------------------------------------
    /**
     * @returns {RT2WebSocketChannel}
     *  the initialized communication channel for WebSocket communication
     */
    function /* RT2WebSocketChannel */ impl_createWsChannel() {
        log().trace('impl_createWsChannel');

        Validate.notEmpty(ox.abs, 'Miss global config "ox.abs".');
        Validate.notEmpty(ox.apiRoot, 'Miss global config "ox.apiRoot".');

        //Validate.notEmpty(ox.version, 'Miss global config "ox.version".'); ES6-LATER seems not needed?

        var /* string */ sScriptUrl     = ox.abs + ox.root + '/io.ox/' + SCRIPT_RT2_WSCHANNEL_JS;

        var /* string */ sConnectionUrl = impl_createWsUrl();
        var /* int    */ nLogLevel      = impl_getGlobalRT2LogLevel();
        var /* json   */ jInitMsg       = {
            type: 'init',
            config: { connection_url: sConnectionUrl, log_level: nLogLevel }
        };
        var /* string */ sInitMsg       = JSON.stringify(jInitMsg);

        log().debug(`create new WebSocket communication channel [${sScriptUrl}] ...`);
        var /* Worker */ aWsChannel = new window.Worker(sScriptUrl);
        aWsChannel.addEventListener('message', impl_onWorkerResponse, false);
        aWsChannel.postMessage(sInitMsg);

        return aWsChannel;
    }
    //----------------------------------------------------------------------
    function /* ? */ impl_createPollChannel() {
        throw new Error('Not implemented yet.');
    }

    //----------------------------------------------------------------------
    /**
     * @returns {string} the initialized member m_sChannelID
     */
    function /* string */ mem_ChannelID() {
        if (ObjectUtils.isNull(m_sChannelID)) {
            m_sChannelID = randomUUID();
        }
        return m_sChannelID;
    }

    //----------------------------------------------------------------------
    /**
     * @returns {WebWorker}
     *  the initialized communication channel for remote communication
     */
    function /* WebWorker */ mem_Channel() {
        if (!ObjectUtils.isNull(m_aChannel)) {
            return m_aChannel;
        }

        // TODO read type from config
        var /* string */ sChannelType = CHANNEL_TYPE_WEBSOCKET;

        if (sChannelType === CHANNEL_TYPE_WEBSOCKET) {
            m_aChannel = impl_createWsChannel();
        } else if (sChannelType === CHANNEL_TYPE_POLLING) {
            m_aChannel = impl_createPollChannel();
        } else {
            throw new Error(`Not support for channel type "${sChannelType}" implemented yet.`);
        }

        return m_aChannel;
    }

    //----------------------------------------------------------------------
    function mem_ClientRegistry() {
        if (ObjectUtils.isNull(m_aClientRegistry)) {
            m_aClientRegistry = new HashMap();
        }
        return m_aClientRegistry;
    }

    //----------------------------------------------------------------------
    function /* int */ impl_getGlobalRT2LogLevel() {
        return Rt2LoggerContextMain.getLevel();
    }

    //----------------------------------------------------------------------
    function /* Logger */ log() {
        if (ObjectUtils.isNull(m_aLog)) {
            m_aLog = Logger.create('RT2Channel', Rt2LoggerContextMain);
        }

        return m_aLog;
    }

}  // class RT2Channel

//--------------------------------------------------------------------------
// static vars

var /* RT2Channel */ m_gSingleton = null;

//--------------------------------------------------------------------------
// static functions

//--------------------------------------------------------------------------
/**
 * @returns {RT2Channel}
 *  the single instance of RT2Channel
 */
RT2Channel.get = function () {
    if (m_gSingleton !== null) {
        return m_gSingleton;
    }

    m_gSingleton = new RT2Channel();
    return m_gSingleton;
};

//--------------------------------------------------------------------------
/**
 * Destroys single instance of RT2Channel
 */
RT2Channel.destroy = function () {
    if (m_gSingleton !== null) {
        m_gSingleton.destroy();
        m_gSingleton = null;
    }
};

//--------------------------------------------------------------------------
// exports

export default RT2Channel;
