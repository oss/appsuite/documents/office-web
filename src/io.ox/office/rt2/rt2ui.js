/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import Logger from '@/io.ox/office/rt2/shared/jss_logger';
import Rt2LoggerContextMain from '@/io.ox/office/rt2/rt2loggercontext_main';

//--------------------------------------------------------------------------
// const

//--------------------------------------------------------------------------
/**
 */
function RT2UI() {
    //----------------------------------------------------------------------
    // member

    var /* Node */ m_aGUI = null;

    var /* Node */ m_aChat = null;

    var /* Object */ m_aChatListener = null;

    var /* Logger */ m_aLog = null;

    // base constructor ---------------------------------------------------

    //----------------------------------------------------------------------
    /* public void */ this.show = function () {
        if (!ObjectUtils.isNull(m_aGUI)) {
            return;
        }

        log().info('DBG : enable chat gui ...');

        var /* string */ ID_GUI    = 'RT2CHATGUI';
        var /* string */ ID_INPUT  = 'RT2CHATGUI-INPUT';
        var /* string */ ID_OUTPUT = 'RT2CHATGUI-OUTPUT';

        var sHTML = '<div id="' + ID_GUI + '">';
        sHTML += '<textarea id="' + ID_INPUT + '"  name="chattext" cols="50" rows="2"></textarea>';
        sHTML += '<textarea id="' + ID_OUTPUT + '" readonly></textarea>';
        sHTML += '</div>';

        log().info('DBG : ... create GUI');
        var /* Node */ aGUI = document.createElement('div');
        aGUI.innerHTML = sHTML;
        document.body.appendChild(aGUI);

        var /* node */ aChatGUIRoot = document.getElementById(ID_GUI);
        var /* node */ aChatInput   = document.getElementById(ID_INPUT);
        var /* node */ aChatOutput  = document.getElementById(ID_OUTPUT);

        log().info('DBG : ... style GUI');
        aChatGUIRoot.style.position        = 'fixed';
        aChatGUIRoot.style.left            = '25px';
        aChatGUIRoot.style.top             = '150px';
        aChatGUIRoot.style.width           = '400px';
        aChatGUIRoot.style.height          = '800px';
        aChatGUIRoot.style.display         = 'block';
        aChatGUIRoot.style.backgroundColor = 'lightgray';
        aChatGUIRoot.style.color           = 'black';

        aChatInput.style.position        = 'relative';
        aChatInput.style.left            = '10px';
        aChatInput.style.top             = '10px';
        aChatInput.style.width           = '380px';
        aChatInput.style.height          = '50px';

        aChatOutput.style.position        = 'relative';
        aChatOutput.style.left            = '10px';
        aChatOutput.style.top             = '10px';
        aChatOutput.style.width           = '380px';
        aChatOutput.style.height          = '700px';
        aChatOutput.style.overflow        = 'scroll';
        aChatOutput.style.backgroundColor = 'white';

        if (aChatInput.addEventListener) {
            aChatInput.addEventListener('keyup', function (aEvent) {
                if (aEvent.keyCode === 13) {
                    var /* string */ sInput = aChatInput.value;
                    aChatInput.value = '';
                    impl_addChatMessage('Me', sInput);
                    impl_notifyChatListener(sInput);
                }
            }, false);
        } else {
            log().error('DBG : ... no addEventListener ;-(');
        }

        log().info('DBG : ... show GUI');
        aChatGUIRoot.style.visibility = 'visible';

        m_aGUI  = aGUI;
        m_aChat = aChatOutput;

        log().info('DBG : OK.');
    };

    //----------------------------------------------------------------------
    /* public void */ this.hide = function () {
        if (ObjectUtils.isNull(m_aGUI)) {
            return;
        }
        m_aGUI.parentNode.removeChild(m_aGUI);
        m_aGUI  = null;
        m_aChat = null;
    };

    //----------------------------------------------------------------------
    /* public void */ this.addChatListener = function (/* object */ aListener) {
        m_aChatListener = aListener;
    };

    //----------------------------------------------------------------------
    /* public void */ this.addChatMessage = function (/* string */ sUserID, /* string */ sMessage) {
        impl_addChatMessage(sUserID, sMessage);
    };

    //----------------------------------------------------------------------
    /* public */ function /* void */ impl_addChatMessage(/* string */ sUserID, /* string */ sMessage) {
        if (ObjectUtils.isNull(m_aChat)) {
            return;
        }

        var /* string */ sChat = m_aChat.value;
        sChat += '-----------------------------\n';
        sChat += '[' + sUserID + ']\n';
        sChat += sMessage + '\n';

        m_aChat.value     = sChat;
        m_aChat.scrollTop = m_aChat.scrollHeight;
    }

    //----------------------------------------------------------------------
    /* public */ function /* void */ impl_notifyChatListener(/* string */ sMessage) {
        if (ObjectUtils.isNull(m_aChatListener)) {
            return;
        }

        m_aChatListener.onChatMessage(sMessage);
    }

    //----------------------------------------------------------------------
    function log() {
        if (ObjectUtils.isNull(m_aLog)) {
            m_aLog = Logger.create('RT2UI', Rt2LoggerContextMain);
        }
        return m_aLog;
    }

} // class RT2UI

//--------------------------------------------------------------------------
/* RT2UI */ RT2UI.create = function () {
    var /* RT2UI */ aInst = new RT2UI();
    return aInst;
};

//--------------------------------------------------------------------------
// exports

export default RT2UI;
