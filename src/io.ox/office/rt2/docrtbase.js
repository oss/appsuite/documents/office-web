/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import ox from '$/ox';

import { BaseObject } from '@/io.ox/office/tk/objects';
import * as ClientError from '@/io.ox/office/baseframework/utils/clienterror';
import ObjectUtils from '@/io.ox/office/rt2/shared/jss_objectutils';
import StringUtils from '@/io.ox/office/rt2/shared/jss_stringutils';
import Validate from '@/io.ox/office/rt2/shared/jss_validate';
import Logger from '@/io.ox/office/rt2/shared/jss_logger';
import RT2Const from '@/io.ox/office/rt2/rt2const';
import RT2InitOptions from '@/io.ox/office/rt2/rt2initoptions';
import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';
import RT2DeferredWrapper from '@/io.ox/office/rt2/rt2deferredwrapper';
import Rt2LoggerContextMain from '@/io.ox/office/rt2/rt2loggercontext_main';

//--------------------------------------------------------------------------
/**
 * Represents the connection to the real-time framework, used to send
 * message to and receive message from the server.
 *
 * Triggers the following events:
 * - 'update': Update notifications containing actions created by remote
 *  clients, and state information for the application (read-only, name and
 *  identifier of the current editor, etc.).
 */
export default class DocRTBase extends BaseObject {

    constructor(aInitOptions) {
        super();

        var self = this;

        var /* RT2DeferredWrapper */ m_aRT2Inst = null;

        var /* Logger             */ m_aLog     = null;

        // public methods -----------------------------------------------------

        /* public string */ this.getClientUID = function () {
            return m_aRT2Inst.getClientUID();
        };

        /* public string */ this.getDocUID = function () {
            return m_aRT2Inst.getDocUID();
        };

        /* public String */ this.getChannelUID = function () {
            return m_aRT2Inst.getChannelUID();
        };

        /* public boolean */ this.isConnected = function () {
            return m_aRT2Inst.isConnected();
        };

        /* public Promise */ this.join = function () {
            log().trace('join', []);

            var /* map< string, object > */ lHeader           = {};
            var /* json                  */ jBody             = {};
            var /* Promise               */ aInternalPromise  = this.send(RT2Protocol.REQUEST_JOIN, lHeader, jBody);
            var /* Deferred              */ aOutsideDeferred  = this.createDeferred();

            aInternalPromise.then(
                function (/* json */ jRT1Msg) {
                    aOutsideDeferred.resolve(jRT1Msg.body);
                },
                function (/* json */ jRT1Msg) {
                    var aResult = _.isObject(jRT1Msg) ? jRT1Msg.body : ClientError.ERROR_CONNECTION_RESET_RECEIVED;
                    aOutsideDeferred.reject(aResult);
                }
            );

            return aOutsideDeferred.promise();
        };

        /* public Promise */ this.openDocument = function (/* json */ jOpenData) {
            log().trace('openDocument', [jOpenData]);

            var /* map< string, object > */ lHeader = {};
            var /* json                  */ jBody   = {};

            if (!ObjectUtils.isNull(jOpenData)) {
                lHeader[RT2Protocol.HEADER_FAST_EMPTY]      = jOpenData.fastEmpty;
                lHeader[RT2Protocol.HEADER_FAST_EMPTY_OSN]  = jOpenData.fastEmptyOSN;
                lHeader[RT2Protocol.HEADER_OLD_CLIENT_UID]  = jOpenData.oldClientUID;
                lHeader[RT2Protocol.HEADER_AUTH_CODE]       = jOpenData.auth_code;

                if (_.isString(jOpenData.advisoryLock)) {
                    lHeader[RT2Protocol.HEADER_ADVISORY_LOCK] = jOpenData.advisoryLock;
                }

                var /* string/number */ storageVersion = jOpenData.storageVersion;
                if (_.isNumber(storageVersion) || _.isString(storageVersion)) {
                    lHeader[RT2Protocol.HEADER_STORAGE_VERSION] = String(storageVersion);
                    lHeader[RT2Protocol.HEADER_STORAGE_OSN]     = jOpenData.storageOSN;
                }
            }

            return this.send(RT2Protocol.REQUEST_OPEN_DOC, lHeader, jBody);
        };

        /* public Promise */ this.abortOpen = function () {
            log().trace('abortOpen');

            var /* map< string, object > */ lHeader = {};
            var /* json                  */ jBody   = {};

            return this.send(RT2Protocol.REQUEST_ABORT_OPEN, lHeader, jBody);
        };

        /* public Promise */ this.closeDocument = function (/* json */ jCloseData) {
            log().trace('closeDocument', [jCloseData]);

            var /* Deferred              */ aOutsideDeferred = this.createDeferred();
            var /* map< string, object > */ lHeader = {};
            var /* json                  */ jBody   = jCloseData;

            if (ObjectUtils.isObject(jBody) && ObjectUtils.isBoolean(jCloseData.no_restore)) {
                lHeader[RT2Protocol.HEADER_NO_RESTORE] = true;
                delete jBody.no_restore;
            }

            var /* Promise */ promise = this.send(RT2Protocol.REQUEST_CLOSE_DOC, lHeader, jBody);
            promise.then(function (/* json */ jRT1Msg) {
                aOutsideDeferred.resolve(jRT1Msg.body);
            }, function (/* json */ jRT1Msg) {
                var aResult = _.isObject(jRT1Msg) ? jRT1Msg.body : ClientError.ERROR_CONNECTION_RESET_RECEIVED;
                aOutsideDeferred.reject(aResult);
            });

            return aOutsideDeferred.promise();
        };

        /* public Promise */ this.leave = function (autoClose) {
            log().trace('leave', []);

            var /* map< string, object > */ lHeader = {};
            var /* json                  */ jBody   = {};

            if (autoClose) {
                lHeader[RT2Protocol.HEADER_AUTO_CLOSE] = true;
            }

            return this.send(RT2Protocol.REQUEST_LEAVE, lHeader, jBody);
        };

        /* public Promise */ this.reset = function () {
            log().trace('reset', []);

            var /* map< string, object > */ lHeader = {};
            var /* json                  */ jBody   = {};

            return this.send(RT2Protocol.REQUEST_RESET, lHeader, jBody);
        };

        /* public Promise */ this.sync = function (/* json */ jSyncData) {
            log().trace('sync', [jSyncData]);

            var /* map< string, object > */ lHeader = {};
            var /* json                  */ jBody   = jSyncData;

            return this.send(RT2Protocol.REQUEST_SYNC, lHeader, jBody);
        };

        /* public Promise */ this.syncStable = function (/* json */ jSyncData) {
            log().trace('sync', [jSyncData]);

            var /* map< string, object > */ lHeader = {};
            var /* json                  */ jBody   = jSyncData;

            return this.send(RT2Protocol.REQUEST_SYNC_STABLE, lHeader, jBody);
        };

        /* public Promise */ this.sendAppAction = function (/* string */ sAction, /* json  */ jData) {
            log().trace('sendAppAction', [sAction, jData]);

            Validate.isTrue(!StringUtils.isEmpty(sAction), 'Invalid argument "action".');

            var /* map< string, object > */ lHeader = {};
            var /* json                  */ jBody   = {};

            lHeader[RT2Protocol.HEADER_APP_ACTION] = sAction;

            if (!ObjectUtils.isNull(jData)) {
                jBody = jData;
            }

            return this.send(RT2Protocol.REQUEST_APP_ACTION, lHeader, jBody);
        };

        /* public Promise */ this.send = function (/* string */ sRequest, /* Dict<object> */ lHeader, /* json */ jBody) {
            var /* Deferred */ aDeferred = m_aRT2Inst.send(sRequest, lHeader, jBody);
            var /* Promise  */ aPromise  = aDeferred.promise();
            return aPromise;
        };

        /* public void */ this.debugTriggerProblem = function (/* string */ problem) {
            m_aRT2Inst.debugTriggerProblem(problem);
            return this;
        };

        /* private Logger */ function log() {
            if (ObjectUtils.isNull(m_aLog)) {
                m_aLog = Logger.create('DocRTBase', Rt2LoggerContextMain);
            }
            return m_aLog;
        }

        // initialization -----------------------------------------------------

        m_aRT2Inst = this.member(new RT2DeferredWrapper(aInitOptions));

        // forward RT2 events to own listeners
        m_aRT2Inst.on(RT2Const.EVENT_DOC_RESPONSE, function (/* json */ jRT1Msg) {
            log().trace('forward doc response to derived class ...', [jRT1Msg]);
            self.trigger(RT2Const.EVENT_RECEIVE, jRT1Msg);
        });

        // forward RT2 events regarding communication to possible handlers
        for (const eventType of RT2Const.EVENT_DEFINITION_LIST) {
            m_aRT2Inst.on(eventType, function (jData) {
                log().trace('forward event to derived class ...', [jData]);
                self.trigger(eventType, jData);
            });
        }
    }

    //--------------------------------------------------------------------------

    /**
     * @param {BaseApplication} aApp
     * @param {RT2InitOptions} aOutsideInitOptions
     *
     * @returns {DocRTBase}
     */
    static create(aApp, aOutsideInitOptions) {

        Validate.isNotNull(aApp, 'Invalid argument "app".');
        // init opts are optional !

        var /* RT2InitOptions */ aInitOpts       = new RT2InitOptions();
        var /* FileDescriptor */ aFileDescriptor = aApp.getFileDescriptor();
        var /* String         */ sDocType        = aApp.appType;
        var /* String         */ sContextID      = ObjectUtils.getObjectPartOrDefault(ox,              'context_id',                           null);
        var /* String         */ sFolderID       = ObjectUtils.getObjectPartOrDefault(aFileDescriptor, 'folder_id',                            null);
        var /* String         */ sFileID         = ObjectUtils.getObjectPartOrDefault(aFileDescriptor, 'id',                                   null);
        var /* String         */ sRTSpecialID    = ObjectUtils.getObjectPartOrDefault(aFileDescriptor, 'com.openexchange.realtime.resourceID', null);
        var /* String         */ sDriveDocID     = null;

        Validate.isTrue(!StringUtils.isEmpty(sContextID), 'Miss context id.');
        Validate.isTrue(!StringUtils.isEmpty(sDocType),   'Miss doc type.');
        Validate.isTrue(!StringUtils.isEmpty(sFolderID),  'Miss folder id.');
        Validate.isTrue(!StringUtils.isEmpty(sFileID),    'Miss file id.');

        if (StringUtils.isEmpty(sRTSpecialID)) {
            sDriveDocID = sContextID + '@' + sFileID;
        } else {
            sDriveDocID = sContextID + '@' + sRTSpecialID;
        }

        aInitOpts.setOption(RT2InitOptions.INITOPT_DRIVE_DOC_ID, sDriveDocID);
        aInitOpts.setOption(RT2InitOptions.INITOPT_FOLDER_ID,    sFolderID);
        aInitOpts.setOption(RT2InitOptions.INITOPT_FILE_ID,      sFileID);
        aInitOpts.setOption(RT2InitOptions.INITOPT_DOC_TYPE,     sDocType);

        if (!ObjectUtils.isNull(aOutsideInitOptions)) {
            aInitOpts.takeOver(aOutsideInitOptions);
        }

        return new DocRTBase(aInitOpts);
    }
}
