/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { createLabel } from '@/io.ox/office/tk/dom';
import { SMALL_DEVICE, convertLengthToHmm } from '@/io.ox/office/tk/utils';
import { BaseDialog } from '@/io.ox/office/tk/dialogs';

import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';

import { getParagraphElement } from '@/io.ox/office/textframework/utils/position';
import { SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { LengthField } from '@/io.ox/office/editframework/view/editcontrols';

import '@/io.ox/office/text/view/dialog/paragraphdialog.less';

// class ParagraphDialog ==================================================

/**
 * The paragraph modal dialog.
 * Provides possibility to set paragraph indentation values (left, right, first line, hanging).
 *
 * The changes will be applied to the current document after clicking the 'Ok' button.
 *
 * The dialog itself is shown by calling this.execute. The caller can react on successful
 * finalization of the dialog within an appropriate done-Handler, chained to the
 * this.execute function call.
 *
 * @param {TextView} docView
 *  The view instance containing this editor instance.
 */
class ParagraphDialog extends BaseDialog {

    constructor(docView) {

        // base constructor ---------------------------------------------------
        super({
            title: /*#. Dialog title: Change paragraph settings */ gt('Paragraph'),
            width: SMALL_DEVICE ? 320 : 420,
            blockModelProcessing: true
        });

        var self = this;
        var docModel = docView.docModel;
        var selection = docModel.getSelection();
        var controlRootNode = $('<div class="para-dialog-control">');
        // storage for the spin field values
        var map = {};
        // page specific values in hmm
        var pageWidth = 0;
        var marginLeft = 0;
        var marginRight = 0;
        var minPageWidth = 0;
        var minParaWidthHmm = 500;

        // private methods ----------------------------------------------------

        function maxIndentLeft() {
            return pageWidth - marginLeft - map.indentFirst - marginRight - map.indentRight - minPageWidth;
        }

        function maxIndentRight() {
            return pageWidth - marginRight - marginLeft - map.indentLeft - Math.max(0, map.indentFirst) - minPageWidth;
        }

        function maxIndentFirst() {
            return pageWidth - marginLeft - map.indentLeft - marginRight - map.indentRight - minPageWidth;
        }

        function createSpinField(key, min, maxFn, label, tooltip, className) {

            var spinField = self.member(new LengthField({ tooltip, select: true, min, max: maxFn }));
            const ctrlId = `io-ox-office-control-${spinField.uid}`;
            spinField.$input.attr("id", ctrlId);

            spinField.setValue(map[key]);

            // store the changed spin field values in the map
            spinField.on('group:commit', function (value) {
                map[key] = value;
            });

            self.registerEnterGroup(spinField);

            var labelNode = createLabel({ classes: 'spin-field-label', label, for: ctrlId });
            var containerNode = $('<div class="ind-wrapper ' + className + '">').append(labelNode, spinField.$el);
            controlRootNode.append(containerNode);

            return spinField;
        }

        /**
         * Initialize controls of the dialog.
         */
        function initControls() {
            var startPos = selection.getStartPosition();
            var paraPos = _.initial(startPos);
            var paragraph = getParagraphElement(selection.getRootNode(), paraPos);
            var attrs = paragraph && docModel.paragraphStyles.getElementAttributes(paragraph);
            var paraAttrs = (attrs && attrs.paragraph) || {};
            var explAttrs = getExplicitAttributeSet(paragraph);
            var explParaAttrs = (explAttrs && explAttrs.paragraph) || null;
            var explIndentLeft, explIndentRight, explIndentFirst;
            var listIndentLeft, listIndentRight, listIndentFirst;
            // page attribute values
            var pageAttributes = docModel.pageStyles.getElementAttributes(docModel.getNode());
            minPageWidth = minParaWidthHmm;
            marginLeft = pageAttributes.page.marginLeft;
            marginRight = pageAttributes.page.marginRight;
            pageWidth = pageAttributes.page.width;

            controlRootNode.append($('<h4 class="para-dialog-heading">').text(gt('Indentation')));

            if (explParaAttrs) {
                explIndentLeft = explParaAttrs.indentLeft;
                explIndentRight = explParaAttrs.indentRight;
                explIndentFirst = explParaAttrs.indentFirstLine;
            }
            if (paraAttrs.listStyleId) {
                var listAttrs = docModel.getListCollection().getListLevel(paraAttrs.listStyleId, paraAttrs.listLevel || 0);
                if (listAttrs) {
                    listIndentLeft = listAttrs.indentLeft;
                    listIndentRight = listAttrs.indentRight;
                    listIndentFirst = listAttrs.indentFirstLine;
                }
            }
            // priority order: explicit attribute > list style > paragraph style
            map.indentLeft = _.isFinite(explIndentLeft) ? explIndentLeft : (_.isFinite(listIndentLeft) ? listIndentLeft : paraAttrs.indentLeft) || 0;
            map.indentRight = _.isFinite(explIndentRight) ? explIndentRight : (_.isFinite(listIndentRight) ? listIndentRight : paraAttrs.indentRight) || 0;
            map.indentFirst = _.isFinite(explIndentFirst) ? explIndentFirst : (_.isFinite(listIndentFirst) ? listIndentFirst : paraAttrs.indentFirstLine) || 0;

            // spinner fields
            createSpinField('indentLeft', -marginLeft, maxIndentLeft, gt('Left'), gt('Set left indent'), 'left-ind');
            createSpinField('indentRight', -marginRight, maxIndentRight, gt('Right'), gt('Set right indent'), 'right-ind');
            createSpinField('indentFirst', -marginLeft, maxIndentFirst, gt('First line'), gt('Set paragraph\'s first line indent'), 'first-ind');
        }

        // initialization -----------------------------------------------------

        // initialize the body element of the dialog
        this.$bodyNode
            .addClass('io-ox-office-text-paragraph-dialog')
            .toggleClass('mobile', !!SMALL_DEVICE)
            .append(controlRootNode);

        // create the layout of the dialog
        initControls();

        // handler for the OK button
        this.setOkHandler(function () {
            var generator = docModel.createOperationGenerator();
            var target = docModel.getActiveTarget();
            var zoomFactor = docView.getZoomFactor();

            selection.iterateContentNodes(function (paragraph, position) {
                var opProperties = { attrs: { paragraph: {} } };
                var paraTxtPos = _.clone(position);
                paraTxtPos.push(0);
                docModel.doCheckImplicitParagraph(paraTxtPos); // check if paragraph is implicit

                // Re-adjust ind values if they are overlapped
                var paraWidthHmm = convertLengthToHmm(($(paragraph).parent().width() * zoomFactor), 'px');

                if (paraWidthHmm < minParaWidthHmm + map.indentRight + map.indentLeft + Math.max(0, map.indentFirst)) {
                    if (map.indentLeft > map.indentRight) {
                        map.indentLeft = paraWidthHmm - minParaWidthHmm - Math.max(0, map.indentFirst);
                        map.indentRight = 0;
                    } else {
                        map.indentLeft = 0;
                        map.indentRight = paraWidthHmm - minParaWidthHmm - Math.max(0, map.indentFirst);
                    }
                }

                opProperties.attrs.paragraph.indentRight = map.indentRight;
                opProperties.attrs.paragraph.indentLeft = map.indentLeft;
                opProperties.attrs.paragraph.indentFirstLine = map.indentFirst;
                opProperties.start = position;
                if (target) { opProperties.target = target; }
                if (docModel.getChangeTrack().isActiveChangeTracking()) { opProperties.attrs.changes = { modified: docModel.getChangeTrack().getChangeTrackInfo() }; }
                generator.generateOperation(SET_ATTRIBUTES, opProperties);
            });

            docModel.applyOperations(generator);
            docView.scrollToSelection();
        });
    }
}

// exports ================================================================

export default ParagraphDialog;
