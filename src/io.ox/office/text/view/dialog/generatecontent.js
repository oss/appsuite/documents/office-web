/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import Backbone from "$/backbone";
import ModalDialog from "$/io.ox/backbone/views/modal";
import mini from "$/io.ox/backbone/mini-views";
import Dropdown from "$/io.ox/backbone/mini-views/dropdown";

import { createIcon, externalLink } from "$/io.ox/core/components";
import { getFeedbackUrl } from "$/io.ox/core/feature";
import yell from "$/io.ox/core/yell";

import { generateOXTextPrompt } from "@/io.ox/office/text/components/ai/promptgenerator";
import { MAX_INPUT_LENGTH, USER_INPUT_AVAILABLE_LENGTH, createLanguages, generateActions, generateTranslationInputAsJSON, generateTranslationResponse, transformActions, translateActions, translateFromLanguages } from "@/io.ox/office/text/components/ai/aiservice";

import { executePrompt } from "@/io.ox/office/tk/utils/aiutils";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import '@/io.ox/office/text/view/dialog/generatecontent.less';

// constants ==================================================================

const renderTypes = [
    { label: gt("Report"), value: "report" },
    { label: gt("Story"), value: "story" },
    { label: gt("Article"), value: "article" },
    { label: gt("Essay"), value: "essay" },
    { label: gt("Research paper"), value: "research_paper" },
    { label: gt("Memo"), value: "memo" },
    { label: gt("Letter"), value: "letter" },
    // { label: gt("Presentation"), value: "presentation" },
    { label: gt("Resume"), value: "resume" },
    { label: gt("Instruction"), value: "instruction" },
    { label: gt("Invoice"), value: "invoice" },
    { label: gt("Contract"), value: "contract" },
    // { label: gt("Project plan"), value: "project_plan" },
    { label: gt("Whitepaper"), value: "whitepaper" },
    { label: gt("Newsletter"), value: "newsletter" },
    { label: gt("Travel report"), value: "travel_report" }
];

const selectionTypes = [
    { label: gt("Replace selected text in document"), value: "replace" },
    { label: gt("Add before selected text in document"), value: "insert_before" },
    { label: gt("Add behind selected text in document"), value: "insert_behind" }
];

// private functions ==========================================================

function getPropertyFromAction(actions, value, property) {
    const action = actions.find(act => act.value === value);
    return action ? action[property] : "";
}

/**
 * Getting the default action value that is used as start value in the translation dialog.
 *
 * @param {string} defaultLanguage
 *  A default language for translation, that is specified before the dialog opens.
 *
 * @returns {string}
 *  The value used as start value from the translation actions.
 */
function getDefaultLanguageAction(defaultLanguage) {
    return defaultLanguage ? `translate:${defaultLanguage.toLowerCase()}` : "translate:english";
}

// public functions ===========================================================

export function showModalAIContentDialog({ mode = "write", input = "", selectedParagraphs, defaultTranslationLang, createLang, fromLang, type } = {}) {

    const translationMode = mode === "translate";
    const title = translationMode ? gt("Translate") : gt("Generate or transform content");

    return new Promise(resolve => {

        const model = new Backbone.Model({
            action: translationMode ? getDefaultLanguageAction(defaultTranslationLang) : mode,
            type: type || "letter",
            tone: "neutral",
            mood: "neutral",
            length: "short",
            fromlanguage: fromLang || "Auto",
            createlanguage: createLang || "Auto",
            selection: "replace"
        });

        const modalDialog = new ModalDialog({
            title,
            width: 640,
            autoClose: false,
            autoFocusOnIdle: false,
            help: { base: 'help-documents', target: "ox.documents.user.chap.text.html" }
        })
        .inject({
            renderActionOptions() {

                const actionList = translationMode ? [
                    {
                        label: false,
                        options: translateActions.map(action => { return { label: action.label, value: action.value }; })
                    }
                ] : [
                    {
                        label: gt("Generate"),
                        options: generateActions.map(action => { return { label: action.label, value: action.value }; })
                    },
                    {
                        label: gt("Transform"),
                        options: transformActions.map(action => { return { label: action.label, value: action.value }; })
                    }
                ];

                const id = _.uniqueId("action");
                return $(`<label for="${id}">`).text(translationMode ? gt("Translate to") : gt("Action"))
                    .add(new mini.SelectView({ id, name: "action", model, list: actionList, groups: true }).render().$el);
            },
            renderTranslateFromLanguages() {
                const languageFromList = translationMode ? [
                    {
                        label: false,
                        options: translateFromLanguages.map(action => { return { label: action.label, value: action.value }; })
                    }
                ] : undefined;

                const id = _.uniqueId("fromlanguage");
                return $(`<label for="${id}">`).text(gt("Translate from"))
                    .add(new mini.SelectView({ id, name: "fromlanguage", model, list: languageFromList, groups: true }).render().$el);
            },
            renderCreateLanguages() {
                const createLanguageList = translationMode ? undefined : [
                    {
                        label: false,
                        options: createLanguages.map(action => { return { label: action.label, value: action.value }; })
                    }
                ];

                const id = _.uniqueId("createlanguage");
                return $(`<label for="${id}">`).text(gt("Language"))
                    .add(new mini.SelectView({ id, name: "createlanguage", model, list: createLanguageList, groups: true }).render().$el);
            },
            renderTypeOptions() {

                if (translationMode) { return; }

                const typeList = [
                    {
                        label: false,
                        options: renderTypes
                    }
                ];

                const id = _.uniqueId("select");
                this.$typeDropdown = new mini.SelectView({ id, name: "type", model, list: typeList, groups: true }).render().$el;
                return $(`<label for="${id}">`).text(gt("Document type")).add(this.$typeDropdown);
            },
            renderOptionsDropdown() {

                if (translationMode) { return; }

                const label = createIcon("bi/sliders.svg").addClass("me-8").add($.txt(gt("Options")));
                const dropdown = new Dropdown({ keep: true, caret: true, model, label, buttonToggle: true, alignRight: true })
                    .header(gt("Length"))
                    .option("length", "short", gt("Short"), { radio: true })
                    .option("length", "medium", gt("Medium"), { radio: true })
                    .option("length", "long", gt("Long"), { radio: true })
                    .divider()
                    .header(gt("Tone"))
                    .option("tone", "formal", gt("Formal"), { radio: true })
                    .option("tone", "neutral", gt("Neutral"), { radio: true })
                    .option("tone", "informal", gt("Informal"), { radio: true })
                    .divider()
                    .header(gt("Mood"))
                    .option("mood", "exciting", gt("Exciting"), { radio: true })
                    .option("mood", "friendly", gt("Friendly"), { radio: true })
                    .option("mood", "positive", gt("Positive"), { radio: true })
                    .option("mood", "neutral", gt("Neutral"), { radio: true })
                    .option("mood", "rejecting", gt("Rejecting"), { radio: true })
                    .option("mood", "disappointed", gt("Disappointed"), { radio: true })
                    .render();
                this.$dropdownToggle = dropdown.$("[data-toggle]");
                dropdown.$el.children(".btn").addClass("options-dropdown");
                return $("<label class='invisible'>").text("\u00a0").add(dropdown.$el);
            },
            renderRequestArea() {
                const id = _.uniqueId("text");
                if (!translationMode && input && input.length > MAX_INPUT_LENGTH) { input = input.substring(0, MAX_INPUT_LENGTH); }
                this.$requestAreaLabel = $(`<label for="${id}" class="requestarea-label mb-4">`).text(this.getRequestAreaLabel());
                return $("<div class='row mb-16'>").append(
                    $("<div class='col-md-12 requestarea-info requestarea-missing-content mt-8 mb-4'>").text(gt("Please add keywords or text to fine-tune your content!")),
                    //#. %1$d is the maximum allowed number of characters in an AI request
                    $("<div class='col-md-12 requestarea-info requestarea-extended-content mt-8 mb-4'>").text(gt("Your input is restricted to the maximum length of %1$d characters!", MAX_INPUT_LENGTH)),
                    $("<div class='col-md-12 requestarea-container'>").append(
                        this.$requestAreaLabel,
                        $(`<textarea name="keywords" class="request-area form-control resize-y" rows="5" id="${id}" ${translationMode ? "disabled" : `maxlength=${MAX_INPUT_LENGTH}`}>`).val(input)
                        .on("input", evt => {
                            const inputLength = evt.target.value.length;
                            this.toggleRequestAreaMissingContent(inputLength === 0);
                            this.toggleRequestAreaExtendedContent(inputLength === MAX_INPUT_LENGTH);
                            this.toggleRequestAreaLabel(inputLength > 0 && inputLength < MAX_INPUT_LENGTH);

                            if (inputLength > 0 && inputLength < MAX_INPUT_LENGTH) {
                                if (MAX_INPUT_LENGTH - inputLength <= USER_INPUT_AVAILABLE_LENGTH) { // inform user about last available characters
                                    this.$(".requestarea-label").text(this.getRequestAreaLabel() + ` (${this.getRemainingText(inputLength)})`);
                                } else {
                                    this.$(".requestarea-label").text(this.getRequestAreaLabel());
                                }
                            }

                        })
                    )
                );
            },
            renderResult() {
                this.$content = $("<div class='result-area border rounded p-16 break-words whitespace-pre-line overflow-auto select-text'>")
                    .css("min-height", "136px")
                    .append(this.getSkeletons());
                return $("<div class='row'>").hide().append(
                    $("<div class='col-md-12 generated-result'>").append(
                        $("<div class='mb-4 text-bold'>").text(gt("Generated result")),
                        this.$content
                    )
                );
            },
            renderSelectionOptions() {

                const selectionList = [
                    {
                        label: false,
                        options: selectionTypes
                    }
                ];

                const id = _.uniqueId("select");
                this.$selectionDropdown = new mini.SelectView({ id, name: "selection", model, list: selectionList, groups: true }).render().$el.addClass("document-selection-dropdown");
                return $(`<label for="${id}">`).add(this.$selectionDropdown);
            },
            renderFeedback() {
                const href = getFeedbackUrl("openai");
                return href
                    ? $("<div class='mt-8'>").append(
                        externalLink({ href, text: gt("Your feedback on AI integration ...") })
                    )
                    : $();
            },
            getSkeletons() {
                return ["a", "b", "c", "d", "e"].map(c => $(`<div class="skeleton ${c}">`));
            },
            async generateContent() {
                const userInput = this.getRequestAreaValue();
                if (!userInput) { // inform the user about the required input
                    this.toggleRequestAreaLabel(false);
                    this.toggleRequestAreaMissingContent(true);
                    this.$content.empty(); // required for "regenerate"
                    $("textarea").focus();
                    return;
                } else {
                    this.toggleRequestAreaLabel(true);
                    this.toggleRequestAreaMissingContent(false);
                }
                this.busy();
                this.focusAction("cancel");
                this.$content.empty().append(this.getSkeletons()).closest(".row").show();
                if (this.$selectionDropdown) { this.$selectionDropdown.closest(".row").show(); }

                const { ...options } = model.toJSON();
                options.translationlanguage = getPropertyFromAction(translateActions, options.action, "promptValue");
                if (options.translationlanguage) { options.action = "translate"; }
                const effectiveUserInput = options.action === "translate" ? generateTranslationInputAsJSON(selectedParagraphs) : userInput;
                const prompt = generateOXTextPrompt(effectiveUserInput, { ...options, isSingleParagraphSelection: selectedParagraphs.length === 1 });

                try {
                    const response = await executePrompt(prompt);
                    if (this.disposed) { return; }
                    this.generatedContent = options.action === "translate" ? generateTranslationResponse(response, selectedParagraphs) : response;
                } catch (error) {
                    if (error instanceof Error) { globalLogger.error(error.message); }
                    yell("error", gt("API request failed. The service might be unavailable. Please try again in a few minutes."));
                }
                this.$content.empty().text(this.generatedContent);

                this.idle();
            },
            actionNeedsInput(action) {
                return /^(write|rephrase|translate:\w+|summarize)$/.test(action);
            },
            getRequestAreaLabel() {
                const needsInput = this.actionNeedsInput(model.get("action"));
                return needsInput ? gt("Your input") : gt("Keywords (optional)");
            },
            getRequestAreaValue() {
                return String(this.$("textarea").val()).trim();
            },
            getRemainingText(textLength) {
                //#. %1$d is the remaining number of allowed characters in the text area
                return gt("%1$d characters remaining", MAX_INPUT_LENGTH - textLength);
            },
            toggleRequestAreaLabel(state = true) {
                this.$(".requestarea-label").toggle(state);
            },
            toggleRequestAreaMissingContent(state = false) {
                this.$(".requestarea-missing-content").toggle(state);
            },
            toggleRequestAreaExtendedContent(state = false) {
                this.$(".requestarea-extended-content").toggle(state);
            },
            toggleButtons(initialState = true) {
                this.$footer.find("[data-action='use'], [data-action='regenerate']").toggle(!initialState);
                this.$footer.find("[data-action='generate']").toggle(initialState);
            },
            focusAction(action) {
                this.$footer.find(`[data-action="${action}"]`).focus();
            }
        })
        .addAlternativeButton({ action: "regenerate", label: gt("Regenerate"), icon: "bi/arrow-clockwise.svg" })
        .addCancelButton()
        .addButton({ action: "generate", label: gt("Generate") })
        .addButton({ action: "use", label: gt("Use content") })
        .build(function () {

            const updateDropdownState = () => {
                const typeDisabled = /^(rephrase|summarize)$/.test(model.get("action"));
                const optionsDisabled = /^(summarize)$/.test(model.get("action"));
                this.$typeDropdown.prop("disabled", typeDisabled);
                this.$dropdownToggle.prop("disabled", optionsDisabled);
            };

            this.$header.addClass("flex-row")
                .find("h1").addClass("flex-grow mt-8")
                .before(createIcon("bi/ox-ai.svg").addClass("bi-5xl me-8 text-accent"));


            const topBar = $('<div class="row mb-8">');

            if (translationMode) { topBar.append($('<div class="col-md-4">').append(this.renderTranslateFromLanguages())); } // the "from" language
            const actionClasses = translationMode ? "col-md-4" : "col-md-3";
            topBar.append($('<div>').addClass(actionClasses).append(this.renderActionOptions()));
            if (!translationMode) {
                topBar.append(
                    $('<div class="col-md-3">').append(this.renderTypeOptions()),
                    $('<div class="col-md-3">').append(this.renderCreateLanguages()),
                    $('<div class="col-md-3 text-right">').append(this.renderOptionsDropdown())
                );
            }

            this.$body.css('padding-top', '8px').append(
                topBar,
                this.renderRequestArea(),
                this.renderResult()
            );

            if (input) {
                const bottomBar = $('<div class="row mb-4">').hide().append($('<div class="col-md-6">').append(this.renderSelectionOptions()));
                this.$body.append(bottomBar);
            }

            this.$body.append(
                this.renderFeedback()
            );

            this.$footer.find("[data-action='use'], [data-action='regenerate']").hide();

            if (!translationMode) {
                updateDropdownState(); // initial setting for the dropdowns

                this.listenTo(model, "change:action change:type change:tone change:mood change:length", () => {
                    updateDropdownState();
                });

                // check, if the maximum length is reached, so that the text is shortened
                window.setTimeout(() => {
                    const inputLength = $(".request-area").val().length;
                    this.toggleRequestAreaExtendedContent(inputLength === MAX_INPUT_LENGTH);
                    this.toggleRequestAreaLabel(inputLength < MAX_INPUT_LENGTH);

                    if (inputLength > 0 && inputLength < MAX_INPUT_LENGTH && (MAX_INPUT_LENGTH - inputLength <= USER_INPUT_AVAILABLE_LENGTH)) { // inform user about last available characters
                        this.$(".requestarea-label").text(this.getRequestAreaLabel() + ` (${this.getRemainingText(inputLength)})`);
                    }
                }, 0);
            }

            this.toggleRequestAreaMissingContent();
            this.toggleRequestAreaExtendedContent();
        })
        .on("cancel", () => {
            resolve(false);
        })
        .on("generate", async function () {
            this.toggleRequestAreaExtendedContent(false);
            await this.generateContent();
            if (!this.generatedContent) { return; }
            this.toggleButtons(false);
            this.focusAction("use");
        })
        .on("regenerate", async function () {
            this.toggleRequestAreaExtendedContent(false);
            this.generatedContent = null;
            await this.generateContent();
            if (!this.generatedContent) { return; }
            this.focusAction("use");
        })
        .on("use", function () {
            const { action, createlanguage, fromlanguage, selection, type } = model.toJSON();
            const isTranslation = action.startsWith('translate:');
            const languageId = isTranslation ? getPropertyFromAction(translateActions, action, "langId") : getPropertyFromAction(createLanguages, createlanguage, "langId");
            const deleteSelected = selection === "replace";
            const insert = selection === "insert_behind" ? "behind" : "before";
            resolve({ content: this.generatedContent, action, languageId, fromLanguage: fromlanguage, isTranslation, type, deleteSelected, insert, keepDocStructure: deleteSelected && isTranslation });
            this.close();
        });

        modalDialog.$el.find(".modal-dialog").addClass("documents-ai-dialog"); // unique namespace

        modalDialog.open();
    });
}
