/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import {
    SMALL_DEVICE, MEDIUM_DEVICE, COMPACT_DEVICE, TOUCH_DEVICE, IOS_SAFARI_DEVICE, SCROLLBAR_WIDTH,
    addDeviceMarkers, containsNode, createIcon, parseCssLength, convertLengthToHmm,
    isTextInputElement, getFocus, setFocus, hideFocus
} from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import {
    SOFTKEYBORD_TOGGLE_MODE,
    getBooleanOption, getChildNodePositionInNode, getObjectOption,
    isContextEventTriggeredByKeyboard, isPortrait, isSoftKeyboardOpen, scrollToChildNode,
    scrollToPageRectangle
} from '@/io.ox/office/tk/utils';

import { Color } from '@/io.ox/office/editframework/utils/color';
import { MSEC_PER_DAY, format, getDateFormat, getNoYearFormat, getTimeFormat, getUTCDateAsLocal,
    getWeekDayFormat } from '@/io.ox/office/editframework/utils/dateutils';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';
import { COMBINED_TOOL_PANES, MAX_TABLE_COLUMNS, MAX_TABLE_ROWS, SPELLING_ENABLED } from '@/io.ox/office/textframework/utils/config';
import { getDOMPosition, getOxoPosition, getParagraphElement, getPixelPositionToRootNodeOffset } from '@/io.ox/office/textframework/utils/position';
import { COMMENTFILTERGROUP_SELECTOR, COMMENTMARGIN_CLASS, PARAGRAPH_NODE_SELECTOR,
    POPUP_CONTENT_NODE_SELECTOR, getPageContentNode, isPageNode } from '@/io.ox/office/textframework/utils/dom';
import { ACCEPT_ALL_OPTIONS, ACCEPT_AND_NEXT_OPTIONS, ACCEPT_CURRENT_OPTIONS, CELL_BORDERS_LABEL,
    CHANGETRACKING_HEADER_LABEL, COMMENT_HEADER_LABEL, DELETE_DRAWING_BUTTON_OPTIONS, FONT_HEADER_LABEL,
    FORMAT_HEADER_LABEL, HYPERLINK_BUTTON_OPTIONS, INSERT_COMMENT_OPTIONS, INSERT_HEADER_LABEL, OPTIONS_LABEL,
    PARAGRAPH_LABEL, REJECT_ALL_OPTIONS, REJECT_AND_NEXT_OPTIONS, REJECT_CURRENT_OPTIONS, REVIEW_HEADER_LABEL, SHOW_COLLABORATORS_CHECKBOX_OPTIONS,
    SHOW_COMMENTSPANE_CHECKBOX_OPTIONS, SHOW_TOOLBARS_CHECKBOX_OPTIONS, TABLE_HEADER_LABEL, TEXT_ALIGNMENT_IN_SHAPE_LABEL,
    TEXT_ALIGNMENT_IN_SHAPE_TOOLTIP, OX_AI_OPTIONS_TRANSLATE, ZOOM_SCREEN_WIDTH_LABEL } from '@/io.ox/office/textframework/view/labels';
import TextBaseView from '@/io.ox/office/textframework/view/view';
import { PageSettingsDialog } from '@/io.ox/office/textframework/view/dialogs';
import { UniversalContextMenu } from '@/io.ox/office/textframework/view/popup/universalcontextmenu';
import { AnchoragePicker, BorderFlagsPicker, BorderWidthPicker,
    Button, CheckBox, CommentDisplayModePicker, CompoundSplitButton,
    DrawingArrangementPicker, DrawingPositionPicker, DrawingLineStylePicker, DrawingArrowPresetPicker, DrawingLineColorPicker, DrawingFillColorPicker,
    FillColorPicker, HeaderFooterPicker, ImageCropPosition, ImagePicker, InsertFieldPicker, InsertTextFrameButton, LanguagePicker, ShapeTypePicker,
    OpenAIDialogPicker, TableSizePicker, TableAlignmentPicker, TextBoxAlignmentPicker, TocPicker } from '@/io.ox/office/text/view/controls';
import { FieldFormatPopup } from '@/io.ox/office/text/view/popup/fieldformatpopup';
import { ChangeTrackPopup } from '@/io.ox/office/text/view/popup/changetrackpopup';
import PageIndicator from '@/io.ox/office/text/view/pageindicator';
import { FontColorToolBar, FontFamilyToolBar, FontStyleToolBar, FormatPainterToolBar, ListStyleToolBar, ParagraphAlignmentToolBar,
    ParagraphFillBorderToolBar, ParagraphStyleToolBar, TableStyleToolBar } from '@/io.ox/office/text/view/toolbars';
import { RulerPane } from '@/io.ox/office/text/view/pane/rulerpane';
import { TextCommentsPane } from '@/io.ox/office/text/view/pane/textcommentspane';
import { getTextFrameNode } from '@/io.ox/office/drawinglayer/view/drawingframe';

import '@/io.ox/office/text/view/style.less';

// constants ==================================================================

// predefined zoom factors
const ZOOM_STEPS = [0.35, 0.5, 0.75, 1, 1.5, 2, 3, 4, 6, 8];

// class TextView =============================================================

/**
 * The text editor view.
 *
 * Triggers the events supported by the base class EditView, and the
 * following additional events:
 * - 'change:zoom': When the zoom level for the displayed document was changed.
 * - 'change:pageWidth': When the width of the page or the inner margin of the
 *      page was changed. This happens for example on orientation changes on
 *      small devices.
 *
 * @param {TextApplication} app
 *  The application containing this view instance.
 *
 * @param {TextModel} docModel
 *  The document model created by the passed application.
 */
export default class TextView extends TextBaseView {

    constructor(app, docModel) {

        // base constructor
        super(app, docModel, {
            zoomSteps: ZOOM_STEPS,
            initHandler,
            grabFocusHandler,
            contentScrollable: true,
            contentMargin: 30,
            enablePageSettings: true,
            showOwnCollaboratorColor: true
        });

        var // this reference
            self = this,

            // the page node
            pageNode = null,

            //page content node
            pageContentNode = null,

            /** @type {PageIndicator} */
            pageIndicator = null,

            // scroll position of the application pane
            scrollPosition = { left: 0, top: 0 },

            // outer width of page node, used for zoom calculations
            pageNodeWidth = 0,

            // outer height of page node, used for zoom calculations
            pageNodeHeight = 0,

            // change track pop-up and controls
            changeTrackPopup = this.member(new ChangeTrackPopup(this)),

            // change track pop-up timeout promise
            changeTrackPopupTimer = null,

            // instance of change field format popup
            fieldFormatPopup = this.member(new FieldFormatPopup(this)),

            // button in popup to remove field
            //removeFieldButton = new Button({ label: gt('Remove'), value: 'remove', tooltip: gt('Remove this field from document') }),

            // the top visible paragraph set during fast loading the document
            topVisibleParagraph = null,

            // storage for last know touch position
            // (to open context menu on the correct position)
            touchX = null,
            touchY = null,

            // whether the software keyboard was just opened (by the user)
            keyboardJustOpened = false;

        // TODO: Use the app-content node defined in baseview

        // private methods ----------------------------------------------------

        /**
         * Preprocess scroll events which provoke presentation of the page
         * indicator under certain conditions.
         */
        var preprocessScrollEventsForPageIndicator = (function () {
            /**
             * Time span to store scroll events of (in milliseconds).
             *
             * @type {number}
             */
            var aggregationInterval = 1000;

            /**
             * Ocurrences during the aggregation interval.
             *
             * @type {number}
             */
            var eventThreshold = 2;

            /**
             * Cached events reduced to their time stamps.
             *
             * @type {number[]}
             */
            var events = [];

            /**
             * Is set deferred by updateCurrentPageOnIndicator() below.
             * Necessary for comparison to detect page changes.
             *
             * @type {number}
             */
            var currentPage = 1;

            /**
             * Necessary for comparison to detect page changes.
             *
             * @type {number}
             */
            var previousPage = 1;

            /**
             * This value represents a continuous scrolling movement after the
             * page indicator was made visible.
             *
             * This is necessary to prevent the indicator from disappearing
             * during slow scrolling. If a user is scrolling past a page gap
             * and the indicator is shown, it might disappear otherwise, if the
             * user is scrolling slow enough to not reach the next gap in time.
             *
             * Continuous scrolling can be described as a session, which starts
             * with a scroll event. That session lasts until there has been no
             * further scroll event for more than a specific time. That time is
             * equal to the aggregation interval. It works like a timeout.
             *
             * @type {boolean}
             */
            var continuousScrolling = false;

            /**
             * Precondition handling for indicator presentation.
             *
             * To show the page indicator multiple requirements must be met:
             *
             * - More than a single scroll event in a specific time span
             * - The current page must have actually changed
             * - The user is continuously scrolling after the previously
             *   mentioned requirements were met
             */
            function showPageIndicator(event) {
                var pageChanged = false;

                if (docModel.isDraftMode() || SMALL_DEVICE) {
                    return;
                }

                // Filter out stored events older than aggregation interval.
                events = events.filter(function (candidate) {
                    return candidate > event.timeStamp - aggregationInterval;
                });

                // Scrolling ended in terms of no events during the interval.
                if (events.length < eventThreshold) {
                    continuousScrolling = false;
                }

                // Add the new event to the stored events.
                events.push(event.timeStamp);

                if (currentPage !== previousPage) {
                    previousPage = currentPage;
                    pageChanged = true;
                }

                if (pageChanged && events.length >= eventThreshold) {
                    pageIndicator.show();
                    continuousScrolling = true;
                }

                // The user is still scrolling after the indicator was shown.
                if (continuousScrolling) {
                    pageIndicator.show();
                }
            }

            // Debounced update of current page on indicator.
            function updateCurrentPageOnIndicator() {
                if (docModel.isDraftMode() || SMALL_DEVICE) {
                    return;
                }

                var count = 1;
                var pageBreaks = docModel.getPageLayout().getPageBreaksCollection();
                var contentRootNodeOffset = self.$contentRootNode.offset().top;

                pageBreaks.each(function () {
                    if ($(this).offset().top > contentRootNodeOffset) {
                        return false;
                    }

                    count += 1;
                });

                currentPage = count;
                pageIndicator.setCurrentPage(count);
            }

            return self.debounce(showPageIndicator, updateCurrentPageOnIndicator, { delay: 100, maxDelay: 400 });
        }());

        /**
         * Moves the browser focus to the editor root node.
         *
         * @param {ItemExecuteOptions} [options]
         *  Optional parameters.
         *  @param {Boolean} [options.afterImport=false]
         *     If grab focus handler is called directly after import finished.
         */
        function grabFocusHandler(options) {
            if (TOUCH_DEVICE) {

                var softKeyboardOpen = isSoftKeyboardOpen();
                var sourceType = options?.sourceEvent?.type ?? 'click';

                if (SOFTKEYBORD_TOGGLE_MODE) {
                    if ((sourceType === 'tap' || sourceType === 'click') && (!softKeyboardOpen || docModel.isDrawingSelected()) && self.isSoftkeyboardEditModeOff()) {
                        hideFocus();
                    } else {
                        setFocus(pageNode, { forceEvent: true });

                        // when the user opens the shrunken tab burger menu and the focus is set back to the page the body can scroll
                        $('body').scrollTop(0);

                        docModel.trigger('selection:focusToEditPage');
                    }

                // old normal touch behavior
                } else {
                    if ((sourceType === 'tap' || sourceType === 'click') && (!softKeyboardOpen || docModel.isDrawingSelected())) {
                        hideFocus();
                    } else {
                        setFocus(pageNode, { forceEvent: true });
                    }
                }

            // desktop
            } else {

                // don't set focus after import, if focus is already at the page node, to prevent browser from setting cursor to start position
                // See also #46562
                if (options?.afterImport && getFocus() === pageNode[0]) {
                    return;
                }
                var focusNode = pageNode;
                if (docModel.isHeaderFooterEditState()) { // using first header/footer node and then drawing node, not vice versa (DOCS-2824)
                    focusNode = docModel.getCurrentRootNode();
                } else if (docModel.getSelection().isAdditionalTextframeSelection()) {
                    focusNode = getTextFrameNode(docModel.getSelection().getSelectedTextFrameDrawing()); // using the textframe of the selected drawing (62584)
                    // restoring the browser selection, when the focus is set into a text frame. For different target nodes (header/footer), this is handled by the overwritten
                    // focus method, but for text frames, the focus method is not overwritten. Especially for Safari it is necessary to restore the browser selection, because
                    // it is very often removed by the browser (in each mousedown) and can lead to unwanted reloads.
                    docModel.getSelection().restoreBrowserSelection({ forceWhenReadOnly: true });
                }
                setFocus(focusNode);
            }
        }

        /**
         * Caches the current scroll position of the application pane.
         *
         * @returns {Object}
         *  An object with the properties 'top' and 'left' of the current scroll position.
         */
        function saveScrollPosition() {
            scrollPosition = { top: self.$contentRootNode.scrollTop(), left: self.$contentRootNode.scrollLeft() };
            return scrollPosition;
        }

        /**
         * Restores the scroll position of the application pane according to
         * the cached scroll position.
         *
         * The optional parameter can be used to specify a scrollPosition. If it
         * is not specified, the local available scroll position is used.
         *
         * @param {Object} [scrollPos]
         *  An optional object with the properties 'top' and 'left' specifying
         *  the vertical and the horizontal scroll position.
         */
        function restoreScrollPosition(scrollPos) {
            var localScrollPos = scrollPos ? scrollPos : scrollPosition;
            self.$contentRootNode.scrollTop(localScrollPos.top).scrollLeft(localScrollPos.left);
        }

        /**
         * Scrolls the application pane to the focus position of the text
         * selection.
         */
        function scrollToSelection(options) {

            var // the boundaries of the text cursor
                boundRect = docModel.getSelection().getFocusPositionBoundRect();

            // if there are absolutely positioned drawings, a second try might be required (DOCS-3355)
            if (!boundRect) { boundRect = docModel.getSelection().getFocusPositionBoundRect({ forceTextPosition: true }); }

            // make cursor position visible in application pane
            if (boundRect) { self.scrollToPageRectangle(boundRect, options); }
        }

        /**
         * Close the softkeyboard on touch devices.
         */
        function closeSoftKeyboard() {
            app.getWindowNode().addClass('soft-keyboard-off');
            hideFocus();
        }

        /**
         * Performance: Register in the selection, whether the current operation is an insertText
         * operation. In this case the debounced 'scrollToSelection' function can use the saved
         * DOM node instead of using Position.getDOMPosition in 'getPointForTextPosition'. This
         * makes the calculation of scrolling requirements faster.
         */
        function registerInsertText(options) {
            docModel.getSelection().registerInsertText(options);

            // fixing the problem with an upcoming soft-keyboard
            // -> also update scroll position on Safari device (DOCS-5323)
            if (TOUCH_DEVICE) { scrollToSelection({ regardSoftkeyboard: isSoftKeyboardOpen() || IOS_SAFARI_DEVICE }); }
        }

        /**
         * Custom Change track date formatter.
         *
         * @param {Number} timestamp
         *  timestamp in milliseconds
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.toLocal=true]
         *      timestamp are converted to local time zone per default.
         *
         * @returns {String}
         *  The formatted date string.
         */
        function formatChangeTrackDate(timestamp, options) {

            if (!_.isNumber(timestamp)) { return gt('Unknown'); }

            var now = new Date();
            var date = new Date(timestamp);

            if (!getBooleanOption(options, 'toLocal', true)) {
                now = getUTCDateAsLocal(now);
                date = getUTCDateAsLocal(date);
            }

            // signed difference (in days) of passed date and today
            // (negative: past, zero: today, positive: future)
            var todayTs = new Date(now.getFullYear(), now.getMonth(), now.getDate()).getTime();
            var daysDiff = Math.floor((date.getTime() - todayTs) / MSEC_PER_DAY);

            // show time without date for today
            if (daysDiff === 0) {
                return format(date, getTimeFormat());
            }

            // show full date for all dates in the future
            if (daysDiff > 0) {
                return format(date, getDateFormat());
            }

            // show word "yesterday" for any time in preceding day
            if (daysDiff === -1) { return gt('Yesterday'); }

            // show name of weekday for the previous 6 days
            // (but not for the same weekday one week ago to prevent confusion)
            if (daysDiff >= -6) {
                return format(date, getWeekDayFormat());
            }

            // do not show the current year
            if (date.getFullYear() === now.getFullYear()) {
                return format(date, getNoYearFormat());
            }

            // show full date with year for the far past
            return format(date, getDateFormat());
        }

        /**
         * Initiates and shows change track popup where applicable.
         *
         * @param {Object} options
         *  @param {jQuery Event} options.browserEvent
         *   The original jQuery Event object from a user selection. This object
         *   is used to filter out selections that are triggered by the keyboard.
         */
        function handleChangeTrackPopup(options) {

            // do not show popup for odf files (not supported yet)
            if (app.isODF()) { return; }

            if (IOS_SAFARI_DEVICE) {
                docModel.getChangeTrack().setHighlightToCurrentChangeTrack();
            }

            if (COMBINED_TOOL_PANES) {
                return;
            }

            // make sure old popup (if any) is hidden
            self.hideChangeTrackPopup();

            var // the event of the browser
                browserEvent = getObjectOption(options, 'browserEvent', null),
                // array of event types, that will trigger change track popup
                relevantEvents = ['mousedown', 'mouseup', 'touchstart', 'touchend'];

            // quit early if no listened events are found
            if (!browserEvent || !_.contains(relevantEvents, browserEvent.type) || (browserEvent.type === 'mousedown' && browserEvent.button === 2)) {
                return;
            }

            // clear any existing proprietary change track selection (e.q. table column selections)
            docModel.getChangeTrack().clearChangeTrackSelection();

            changeTrackPopupTimer = self.setTimeout(() => {

                // don't show the change track popup, if format painter is active
                if (docModel.isFormatPainterActive()) { return; }

                // try show changetrack popup directly if check spelling is off
                // TODO: spell checker popups must have priority
                // -> check if spell checker is enabled or popup is not visible
                changeTrackPopup.show();

            }, 1000);
        }

        /**
         * Finding the first paragraph that is inside the visible area
         */
        function setFirstVisibleParagraph() {

            var // all paragraph elements in the document
                allParagraphs = pageNode.find(PARAGRAPH_NODE_SELECTOR),
                // top offset for the scroll node
                topOffset = Math.round(self.$contentRootNode.offset().top);

            // iterating over all collected change tracked nodes
            allParagraphs.get().some(function (paragraph) {
                if (Math.round($(paragraph).offset().top) > topOffset) {
                    topVisibleParagraph = paragraph;
                    return true; // exit the loop early
                }
            });
        }

        /**
         * Returns whether the contextmenu should be displayed for the given event.
         */
        function canShowContextMenu(event) {

            // no contextmenu event will be triggered after right click on comment or comment bubble
            if ($(event.target).closest('.commentlayer').length || $(event.target).closest('.commentbubblelayer').length) { return false; }

            return true;
        }

        /**
         * Handles the 'contextmenu' event for the complete browser.
         */
        function globalContextMenuHandler(event) {
            // check whether to show the ox, native or no context menu
            if (!canShowContextMenu(event)) { return false; }

            // quit if the contextmenu event was triggered on an input-/textfield
            // workaround for android: use native context menu for textareas and input elements.
            // e.g. paste an image URL into the insert image dialog, bug #52920.
            if (isTextInputElement(event.target)) { return true; }

            // check if context menu shall be opened on a popup menu
            const menuClicked = $(event.target).closest(".io-ox-office-main.popup-container.popup-menu").length > 0;
            // check if the event target is inside of one of the allowed containers in application window, or inside a popup menu
            const windowClicked = containsNode(app.getWindowNode(), event.target) && ($(event.target).closest(".commentlayer,.page,.textdrawinglayer").length > 0);
            // was the event triggered by keyboard?
            const isGlobalTrigger = isContextEventTriggeredByKeyboard(event);

            // get out here, if no allowed target was clicked, or the global trigger was fired (via keyboard)
            if (!menuClicked && !windowClicked && !isGlobalTrigger) { return false; }

            // if a drawing is selected
            let triggerNode;
            if (docModel.isDrawingSelected()) {
                if ($(event.target).is('.tracker')) { return false; }
                // get the (first) selected drawing as trigger node
                triggerNode = $(docModel.getSelection().getSelectedDrawing()[0]);
            } else {
                // get the current focus node to trigger on it
                triggerNode = $(event.target);
                if (!menuClicked && isGlobalTrigger && isPageNode(triggerNode)) {
                    triggerNode = $(window.getSelection().focusNode.parentNode); // DOCS-3288
                    event.target = triggerNode; // overwriting target also in the source event
                }
            }

            if (!triggerNode) {
                globalLogger.warn("$badge{TextView} globalContextMenuHandler: no target node found, context menu will not open");
                return;
            }

            // if we have a last known touch position, use it
            if (touchX && touchY) {
                event.pageX = touchX;
                event.pageY = touchY;

            // in case of global trigger (via keyboard), or x/y are both "0"
            // locate a meaningful position
            } else if (isGlobalTrigger || (!event.pageX && !event.pageY)) {
                // find the position of the trigger node
                var anchorPoint = getPixelPositionToRootNodeOffset($('html'), triggerNode);
                // and set pageX/pageY manually
                event.pageX = anchorPoint.x + Math.round(triggerNode.width() / 2);
                event.pageY = anchorPoint.y + Math.round(triggerNode.height() / 2);
            }
            // close eventually open field popup, see #42478
            docModel.getFieldManager().destroyFieldPopup();

            //Bug 48159
            if (_.browser.Firefox && isGlobalTrigger) { event.target = triggerNode; }

            // trigger our own contextmennu event on the found trigger node
            UniversalContextMenu.triggerShowEvent(triggerNode, event);

            return false;
        }

        /**
         * Initialization after construction.
         */
        function initHandler() {

            // deferred and debounced scroll handler, registering insertText operations immediately
            var scrollToSelectionDebounced = self.debounce(registerInsertText, scrollToSelection, { delay: 50, maxDelay: 500 });

            // register the global event handler for 'contextmenu' event
            if (COMPACT_DEVICE) {
                this.listenToDOMWhenVisible($(document.body), 'taphold', globalContextMenuHandler);
                this.listenToDOMWhenVisible($(document.body), 'contextmenu', function (evt) {
                    if (/^(INPUT|TEXTAREA)/.test(evt.target.tagName)) { return true; }
                    evt.preventDefault();
                    return false;
                });
            } else {
                this.listenToDOMWhenVisible($(document.body), 'contextmenu', globalContextMenuHandler);
            }

            // to save the position from 'touchstart', we have to register some global eventhandler
            // we need this position to open the contextmenu on the correct location
            if (IOS_SAFARI_DEVICE || _.browser.Android) {
                this.listenToDOMWhenVisible($(document.body), 'touchstart', function (event) {
                    touchX = event.originalEvent.changedTouches[0].pageX;
                    touchY = event.originalEvent.changedTouches[0].pageY;
                });
                this.listenToDOMWhenVisible($(document.body), 'touchend', function () {
                    touchX = touchY = null;
                });
            }

            // create the additional view panes
            if (!SMALL_DEVICE) {
                self.rulerPane = self.addPane(new RulerPane(self));
            }

            // the page root node
            pageNode = docModel.getNode();

            // insert the editor root node into the application pane
            self.insertContents(pageNode);

            //page content node, used for zooming on small devices
            pageContentNode = getPageContentNode(pageNode);

            // handle scroll position when hiding/showing the entire application
            self.listenTo(app.getWindow(), "beforehide", function () {
                saveScrollPosition();
                self.$appPaneNode.css('opacity', 0);
            });
            self.listenTo(app.getWindow(), "show", function () {
                restoreScrollPosition();
                // restore visibility deferred, needed to restore IE scroll position first
                self.setTimeout(() => self.$appPaneNode.css('opacity', 1), 0);
            });

            // disable drag&drop for the content root node to prevent dropping images which are
            // loaded by the browser
            self.$contentRootNode.on('drop dragstart dragenter dragexit dragover dragleave', false);

            // Registering scroll handler
            self.$contentRootNode.on('scroll', function () {
                // handler for updating change track markers in side bar after scrolling
                // -> the handler contains only functionality, if the document contains change tracks
                docModel.updateChangeTracksDebouncedScroll();
            });

            // handle selection change events
            self.listenTo(docModel, 'selection', function (selection, options) {
                // scroll to text cursor (but not, if only a remote selection was updated (39904))
                if (!getBooleanOption(options, 'updatingRemoteSelection', false) && !getBooleanOption(options, 'noscroll', false)) { scrollToSelectionDebounced(options); }

                var userData = {
                    selections: selection.getRemoteSelectionsObject(),
                    target: docModel.getActiveTarget() || undefined
                };
                if (docModel.isHeaderFooterEditState()) {
                    userData.targetIndex = docModel.getPageLayout().getTargetsIndexInDocument(docModel.getCurrentRootNode(), docModel.getActiveTarget());
                }
                // send user data to server
                app.updateUserData(userData);
                // manage change track popups, but do not update, if 'keepChangeTrackPopup' is set to true
                if (!getBooleanOption(options, 'keepChangeTrackPopup', false)) {
                    handleChangeTrackPopup(options);
                }
                // exit from crop image mode, if currently active
                if (selection.isCropMode()) { docModel.exitCropMode(); }
            });

            // process page break event triggered by the model
            self.listenTo(docModel, 'pageBreak:before', function () {
                // scrolled downwards -> restore scroll position after inserting page breaks
                if (self.$contentRootNode.scrollTop() > 0) { setFirstVisibleParagraph(); }
            });

            // process page break event triggered by the model
            self.listenTo(docModel, 'pageBreak:after', function () {
                var selection = docModel.getSelection();
                if (!selection.isUndefinedSelection() && !selection.isTopPositionSelected()) {
                    // scrolling to an existing selection made by the user.
                    // 'topVisibleParagraph' should be used also in this case (user makes selection and then scrolls away
                    // without making further selection), but scrolling to 'topVisibleParagraph' does not work reliable,
                    // if there is a selection set.
                    scrollToSelection();
                } else if (topVisibleParagraph) {
                    // scrolling to the scroll position set by the user (if it was saved in 'pageBreak:before')
                    self.scrollToChildNode(topVisibleParagraph, { forceToTop: true });
                }
                topVisibleParagraph = null;
            });

            // store the values of page width and height with paddings, after loading is finished
            pageNodeWidth = pageNode.outerWidth();
            pageNodeHeight = pageNode.outerHeight();
            if (SMALL_DEVICE) {
                self.$contentRootNode.addClass('draft-mode');
            }

            if (SOFTKEYBORD_TOGGLE_MODE) {
                // close keyboard to leave edit mode
                self.listenTo(docModel, 'selection:possibleKeyboardClose', function (options) {

                    var forceClose = getBooleanOption(options, 'forceClose', false);
                    //if a pop-up is currently open in the UI, ignore the context menu
                    var popupOpen = $('.io-ox-office-main.popup-container:not(.context-menu)').length > 0;

                    if ((!popupOpen && !self.isSoftkeyboardEditModeOff()) || forceClose) {
                        closeSoftKeyboard();
                    }
                });

                // add button to open the softKeyboard
                var openKeyboardButton = $('<div class = "soft-keyboard-button">').append(
                    createIcon('bi:chevron-up', { style: { display: 'block', color: 'white', height: '14px', marginLeft: '36%', marginTop: '10%' } }),
                    createIcon('bi:keyboard', { style: { color: 'white', fontSize: '28px' } })
                );

                openKeyboardButton.on('touchstart', function (e) {
                    e.preventDefault();

                    // removing an optional drawing selection
                    const selection = docModel.getSelection();
                    if (selection.isDrawingSelection()) {
                        selection.clearDrawingSelection(selection.getSelectedDrawing());
                        selection.setTextSelection(selection.getEndPosition()); // set cursor behind drawing
                    }

                    self.openSoftKeyboard();
                });

                app.getWindowNode().addClass('soft-keyboard-off');
                app.getWindowNode().append(openKeyboardButton);

                // fired when a the menu in web core is closed -> when the top blue toolbar burger menu is closed
                // important to restore state after the global toolbar menu (mail, portal...) was opened
                // important: it must not listen when the app is hidden
                this.listenToDOMWhenVisible($(document), 'hide.bs.dropdown', closeSoftKeyboard);
            }

            // Add the page indicator.
            pageIndicator = self.member(new PageIndicator());

            // Inform the page indicator once the initial formatting completed.
            docModel.on('pageBreak:after pagination:finished', function () {
                pageIndicator.setPageCount(docModel.getPageLayout().getNumberOfDocumentPages());
            });

            self.$contentRootNode.on('scroll', preprocessScrollEventsForPageIndicator);

            // Set up page indicator element in DOM tree.
            app.getWindowNode().append(pageIndicator.getElement());
        }

        /**
         * Handler function for the 'refresh:layout' event.
         */
        this._refreshLayoutHandler = function () {

            var // the page attributes contain the width of the page in hmm
                pageAttributeWidth = docModel.getPageLayout().getPageAttribute('width'),
                // whether the page padding need to be reduced
                reducePagePadding = (COMPACT_DEVICE && pageAttributeWidth > convertLengthToHmm(self.$contentRootNode.width(), 'px')),
                // the remote selection object
                remoteSelection = docModel.getRemoteSelection(),
                // temporary page node width value
                tempPageNodeWidth = pageNode.outerWidth(),
                // whether the applications width has changed
                // -> the value for appWidthChanged must be true, if the width of the page node was modified
                // or if the class 'small-device' was added or removed (41160)
                appWidthChanged = (pageNodeWidth !== tempPageNodeWidth) || (self.$contentRootNode.hasClass('small-device') !== reducePagePadding);

            // check, if there is sufficient space for left and right padding on the page
            self.$contentRootNode.toggleClass('small-device', reducePagePadding);

            // fix for #33829 - if padding is reduced, dependent variables have to be updated
            pageNodeWidth = tempPageNodeWidth;
            pageNodeHeight = pageNode.outerHeight();

            // rotating device screen on tablets needs margin recalculated, except for < 7" devices (there are no margins and page breaks)
            // also changing page properties from user dialog, needs page layout repaint
            if (appWidthChanged && self.isImportFinished()) {
                if (!SMALL_DEVICE) {
                    if (docModel.isHeaderFooterEditState()) {
                        docModel.getPageLayout().leaveHeaderFooterEditMode();
                        docModel.getSelection().setNewRootNode(pageNode); // restore original rootNode
                        docModel.getSelection().setTextSelection(docModel.getSelection().getFirstDocumentPosition());
                    }
                    // INFO: calling insertPageBreaks from model is absolutely necessary!
                    // It is debounced method in model, and direct method in pageLayout.
                    // Debounced method has little start delay, but direct model can be interupted by calls from other places,
                    // which can lead to unpredicted page sizes!
                    docModel.insertPageBreaks();
                    self.recalculateDocumentMargin({ keepScrollPosition: true });
                } else {
                    docModel.trigger('update:absoluteElements');   // updating comments and absolutely positioned drawings (41160)
                }
                if (COMPACT_DEVICE) {
                    if (reducePagePadding) {
                        self.setTimeout(() => self.changeZoom('width'), 0);
                    } else {
                        self.changeZoom('fixed', 1);
                    }
                }
            }

            // updating the change track side bar, if necessary
            docModel.updateChangeTracksDebounced();

            // update dynamic zoom factor
            this.setTimeout(() => this.zoomState.refresh(), 0);

            // clean and redraw remote selections
            remoteSelection.renderCollaborativeSelections();
        };

        function calculateOptimalZoomFactor() {
            if (SMALL_DEVICE || docModel.isDraftMode()) { return 1; }
            const documentWidth = pageNode.outerWidth() + (pageNode.hasClass(COMMENTMARGIN_CLASS) ? pageNode.data(COMMENTMARGIN_CLASS) : 0);
            const parentWidth = self.$appPaneNode.width();
            const appContent = self.$appPaneNode.find('.app-content');
            const outerDocumentMargin = 3 + parseCssLength(appContent, 'marginLeft') + parseCssLength(appContent, 'marginRight'); // "3" because of the rounding issues
            const optimalLevel = (parentWidth * parentWidth) / (documentWidth * (parentWidth + SCROLLBAR_WIDTH + 4 + outerDocumentMargin));
            return optimalLevel;
        }

        /**
         * Applies the effective zoom factor, and updates the view.
         *
         * @param {ZoomStateEvent} event
         *  The event data with new zoom type and factor to be used.
         */
        function zoomChangedHandler(event) {

            self.trigger('change:zoom:before');

            const zoomFactor = event.factor;

            // {clean:true} set when switching draft mode to force running the else-branch
            if ((SMALL_DEVICE || docModel.isDraftMode()) && !event.options?.clean) {

                pageContentNode.css({
                    transform: `scale(${zoomFactor})`,
                    transformOrigin: 'top left',
                    width: ((zoomFactor < 1) && docModel.isDraftMode()) ? "100%" : `${100 / zoomFactor}%`,
                    backgroundColor: '#fff'
                });

            } else {

                pageNode.css({ transform: `scale(${zoomFactor})`, transformOrigin: 'top' });
                self.recalculateDocumentMargin();

                if (zoomFactor < 1) {
                    self.$appContentNode.css('overflow', 'hidden');
                } else {
                    self.$appContentNode.css({ overflow: '', paddingLeft: '', paddingRight: '' });
                }

                // Use zoom as soon as all browsers support it.
                // pageNode.css({ zoom: zoomFactor });

                scrollToSelection();
            }

            self.trigger('change:zoom');
        }

        function setAdditiveMargin() {
            if (IOS_SAFARI_DEVICE) {
                //extra special code for softkeyboard behavior, we nee more space to scroll to the end of the textfile
                var dispHeight = Math.max(window.screen.height, window.screen.width);

                pageNode.css('margin-bottom', (parseCssLength(pageNode, 'marginBottom') + dispHeight / 3) + 'px');

                //workaround for clipped top of the first page (20)
                pageNode.css('margin-top', (parseCssLength(pageNode, 'marginTop') + 20) + 'px');
            }
        }

        // public methods -----------------------------------------------------

        /**
         * Returns the position of the passed node in the entire scroll area of
         * the application pane.
         *
         * @param {HTMLElement|jQuery} node
         *  The DOM element whose dimensions will be calculated. Must be
         *  contained somewhere in the application pane. If this object is a
         *  jQuery collection, uses the first node it contains.
         *
         * @returns {Object}
         *  An object with numeric attributes representing the position and
         *  size of the passed node relative to the entire scroll area of the
         *  application pane. See method Utils.getChildNodePositionInNode() for
         *  details.
         */
        this.getChildNodePositionInNode = function (node) {
            return getChildNodePositionInNode(this.$contentRootNode, node);
        };

        /**
         * Scrolls the application pane to make the passed node visible.
         *
         * @param {HTMLElement|jQuery} node
         *  The DOM element that will be made visible by scrolling the
         *  application pane. Must be contained somewhere in the application
         *  pane. If this object is a jQuery collection, uses the first node it
         *  contains.
         *
         * @param {Object} options
         *  @param {Boolean} [options.forceToTop=false]
         *   Whether the specified node shall be at the top border of the
         *   visible area of the scrolling node.
         *
         * @returns {TextView}
         *  A reference to this instance.
         */
        this.scrollToChildNode = function (node, options) {
            if (getBooleanOption(options, 'regardSoftkeyboard', false)) {
                //fix for Bug 39979 on Ipad text is sometimes under the keyboard
                if (!IOS_SAFARI_DEVICE || !isSoftKeyboardOpen()) { delete options.regardSoftkeyboard; }
            }

            scrollToChildNode(this.$contentRootNode, node, _.extend(options || {}, { padding: 15 }));
            return this;
        };

        /**
         * Scrolls the application pane to make the passed document page
         * rectangle visible.
         *
         * @param {Object} pageRect
         *  The page rectangle that will be made visible by scrolling the
         *  application pane. Must provide the properties 'left', 'top',
         *  'width', and 'height' in pixels. The properties 'left' and 'top'
         *  are interpreted relatively to the entire document page.
         *
         *  @param {Object} options
         *   @param {Boolean} [options.regardSoftkeyboard=false]
         *   Whether the softkeyboard is considered in the calculation for the viewport.
         *
         * @returns {TextView}
         *  A reference to this instance.
         */
        this.scrollToPageRectangle = function (pageRect, options) {

            scrollToPageRectangle(this.$contentRootNode, pageRect, { regardSoftkeyboard: getBooleanOption(options, 'regardSoftkeyboard', false), padding: 15 });
            return this;
        };

        /**
         * Open the softkeyboard on touch devices.
         *
         *  @param {Object} options
         *   @param {Boolean} [options.keepFocus=false]
         *   Whether the focus shall be set into the page node.
         */
        this.openSoftKeyboard = function (options) {

            app.getWindowNode().removeClass('soft-keyboard-off');

            // whether the focus shall not be changed. For example leave it in comment editor.
            var keepFocus = getBooleanOption(options, 'keepFocus', false);

            if (!keepFocus) { setFocus(pageNode); }

            // temporary disabled due to fixing bug 64580
            // // In case the focus fails (possible on mobile browsers), revert the state.
            // if (getFocus() !== pageNode) {
            //     app.getWindowNode().addClass('soft-keyboard-off');
            //     return;
            // }

            $('body').scrollTop(0);
            scrollToSelection({ regardSoftkeyboard: true });

            // setting a marker, so that the keyboard is not closed immediately again in "androidselection" (DOCS-4771)
            keyboardJustOpened = true;
            this.setTimeout(() => { keyboardJustOpened = false; }, 500);

            // Android selection -> check if focus is set into page or for example into comment editor
            if (!keepFocus) { docModel.trigger('selection:focusToEditPage'); }
        };

        /**
         * Whether the software keyboard was just opened by the user
         */
        this.keyboardJustOpened = function () {
            return keyboardJustOpened;
        };

        /**
         * Creates necessary margin values when using transform scale css property
         */
        this.recalculateDocumentMargin = function (options) {
            var
                zoomFactor = this.getZoomFactor(),
                marginAddition = zoomFactor < 0.99 ? 3 : 0,
                recalculatedMargin = (pageNodeWidth * (zoomFactor - 1)) / 2 + marginAddition,
                // whether the (vertical) scroll position needs to be restored
                keepScrollPosition = getBooleanOption(options, 'keepScrollPosition', false),
                // an additional right margin used for comments that might be set at the page node
                commentMargin = pageNode.hasClass(COMMENTMARGIN_CLASS) ? pageNode.data(COMMENTMARGIN_CLASS) * zoomFactor : 0;

            if (zoomFactor !== 1) {
                // #34735 page width and height values have to be updated
                pageNodeWidth = pageNode.outerWidth();
                pageNodeHeight = pageNode.outerHeight();
            }

            if (_.browser.Chrome && (zoomFactor > 1)) {
                pageNode.css({
                    margin: `${marginAddition}px ${recalculatedMargin + commentMargin}px ${30 / zoomFactor}px ${recalculatedMargin}px`
                });
            } else {
                pageNode.css({
                    margin: `${marginAddition}px ${recalculatedMargin + commentMargin}px ${pageNodeHeight * (zoomFactor - 1) + marginAddition}px ${recalculatedMargin}px`
                });
            }

            setAdditiveMargin();

            // in the end scroll to cursor position/selection (but not after event 'refresh:layout', that uses the option 'keepScrollPosition')
            if (!keepScrollPosition) { scrollToSelection(); }
        };

        /**
         * Rejects an attempt to edit the text document (e.g. due to reaching
         * the table size limits).
         *
         * @param {String} cause
         *  The cause of the rejection.
         *
         * @returns {TextView}
         *  A reference to this instance.
         */
        this.rejectEditTextAttempt = function (cause) {

            switch (cause) {
                case 'tablesizerow':
                    this.yell({ type: 'info', message: gt('The table reached its maximum size. You cannot insert further rows.') });
                    break;

                case 'tablesizecolumn':
                    this.yell({ type: 'info', message: gt('The table reached its maximum size. You cannot insert further columns.') });
                    break;
            }

            return this;
        };

        /**
         * Shows the change track popup consisting updated change track data.
         */
        this.showChangeTrackPopup = function (options) {
            if (options?.select) {
                docModel.getChangeTrack().showChangeTrackGroup();
            }
            changeTrackPopup.show();
        };

        this.hideChangeTrackPopup = function () {
            changeTrackPopup.hide();
            changeTrackPopupTimer?.abort();
            changeTrackPopupTimer = null;
        };

        this.changeTrackPopupContains = function (node) {
            return changeTrackPopup.$el.has(node).length > 0;
        };

        /**
         * Custom Change track date formatter. This function makes the private
         * date formatter 'formatChangeTrackDate' publicly available. Therefore
         * it supports the same parameter.
         *
         * @param {Number} timeInMilliSeconds
         *  Timestamp in milliseconds
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.toLocal=true]
         *      Timestamp is converted to local time zone per default.
         *
         * @returns {String}
         *  The formatted date string
         */
        this.getDisplayDateString = function (timeInMilliSeconds, options) {
            return formatChangeTrackDate(timeInMilliSeconds, options);
        };

        this.showPageSettingsDialog = function () {
            return new PageSettingsDialog(this).show();
        };

        /**
         * Removes the drawinganchor marker of all paragraphs.
         */
        this.clearVisibleDrawingAnchor = function () {
            docModel.getCurrentRootNode().find('.visibleAnchor').removeClass('visibleAnchor').find('.anchorIcon').remove();
        };

        /**
         * Sets the marker for the anchor
         *
         * @param {HTMLElement|jQuery} drawing
         *  The DOM element for that the anchor will be made
         *  visible.
         *
         * @param {Object} attributes
         *  A map of attribute value maps (name/value pairs), keyed by
         *  attribute families.
         */
        this.setVisibleDrawingAnchor = function (drawing, attributes) {
            var oxoPosition = null,
                paragraph = null;

            // get out here, no attributes given
            if (_.isEmpty(attributes)) { return; }

            // remove all visible anchors first
            self.clearVisibleDrawingAnchor();

            // inline drawing get no visible anchor
            if (!_.has(attributes, 'drawing') || !_.has(attributes.drawing, 'inline') || attributes.drawing.inline) { return; }

            oxoPosition = getOxoPosition(docModel.getCurrentRootNode(), drawing, 0);
            paragraph = $(getParagraphElement(docModel.getCurrentRootNode(), _.initial(oxoPosition)));

            $(paragraph).addClass('visibleAnchor').prepend(createIcon("png:position-anchor", "anchorIcon helper"));
        };

        /**
         * Public accesor to show the field format popup.
         *
         * @param {Object} fieldConfig
         *  Instruction of the field for which the group with format codes will be updated.
         * @param {jQuery} node
         *  Field node.
         * @param {Object} options
         *  @param {Boolean} options.autoDate
         */
        this.showFieldFormatPopup = function (fieldConfig, node, options) {
            if (fieldConfig && this.isEditable() && !docModel.getSelection().hasRange()) {
                fieldFormatPopup.initialize(fieldConfig, node, options);
                fieldFormatPopup.show();
            }
        };

        /**
         * Public accesor to hide and destroy the field format popup.
         */
        this.hideFieldFormatPopup = function () {
            fieldFormatPopup.hide();
        };

        /**
         * Tries to find bookmark node for anchor set in current selection.
         * If found, sets cursor and jumps to that node.
         */
        this.goToSelectedAnchor = function () {
            var selection = docModel.getSelection();
            var rootNode = docModel.getCurrentRootNode();
            var start = selection.getStartPosition();
            var nodeInfo = getDOMPosition(rootNode, start);
            var node, gotoNode, gotoNodePos, characterAttrs;

            if (nodeInfo && nodeInfo.node) {
                node = nodeInfo.node;
                if (node.nodeType === 3) { node = node.parentNode; }
                characterAttrs = getExplicitAttributes(node, 'character', true);
            }

            if (characterAttrs && characterAttrs.anchor) {
                gotoNode = pageNode.find('div[anchor="' + characterAttrs.anchor + '"]');
                if (gotoNode.length) {
                    gotoNodePos = getOxoPosition(pageNode, gotoNode); // only headings in main document are considered for anchoring, => pageNode is used
                    if (gotoNodePos) {
                        selection.switchToValidRootNode(pageNode);
                        this.$contentRootNode.animate({
                            scrollTop: gotoNode.parent().position().top
                        });
                        selection.setTextSelection(gotoNodePos);
                    }
                }
            }
        };

        /**
         * Returns whether the edit mode for the softkeyboard is on or off.
         * When this mode is on, the softkeyboard should not open when the user
         * taps into the document in most cases.
         *
         * @returns {Boolean}
         */
        this.isSoftkeyboardEditModeOff = function () {
            // when this class exists the keyboard will not be opened in the page
            return app.getWindowNode().hasClass('soft-keyboard-off');
        };

        /**
         * Public accesor to scrollToSelection().
         */
        this.scrollToSelection = function (options) {
            scrollToSelection(options);
        };

        this.getActiveTableFlags = function () {
            var flags = {
                    firstRow:   true,
                    lastRow:    true,
                    firstCol:   true,
                    lastCol:    true,
                    bandsHor:   true,
                    bandsVert:  true
                },
                tableAttrs = getExplicitAttributes(docModel.getSelection().getEnclosingTable(), 'table') || null;

            if (tableAttrs && tableAttrs.exclude && _.isArray(tableAttrs.exclude) && tableAttrs.exclude.length > 0) {
                tableAttrs.exclude.forEach(function (ex) {
                    flags[ex] = false;
                });
            }
            return flags;
        };

        /**
         * Public accessor to save and return the current scroll position of the document.
         *
         * @returns {Object}
         *  An object with the properties 'top' and 'left' of the current scroll position.
         */
        this.saveScrollPosition = function () {
            return saveScrollPosition();
        };

        /**
         * Restores the scroll position of the application pane according to
         * the specified scroll position.
         *
         * @param {Object} scrollPosition
         *  An object with the properties 'top' and 'left' specifying
         *  the vertical and the horizontal scroll position.
         */
        this.restoreScrollPosition = function (scrollPosition) {
            restoreScrollPosition(scrollPosition);
        };

        /**
         * Returns the collection with all comment threads of the document.
         *
         * @returns {CommentCollection|null}
         *  The collection with all comment threads of the document.
         */
        this.getCommentCollection = function () {
            return docModel ? docModel.getCommentCollection() : null;
        };

        /**
         * Updating the comment pane.
         * In reload process the comment nodes also need to be created, like after normal load.
         *
         * Performance: This is only done, if the commentsPane is visible and it is not up-to-date.
         */
        this.updateCommentsIfRequired = function () {
            if (this.commentsPane?.showsComments() || app.isReloading()) {
                this.commentsPane.repaintAllComments();
            }
        };

        /**
         * Creating the instance of the text comments pane, that will handle all comments in
         * OX Text. If this instance already exists, it will be returned without creating a
         * new instance.
         *
         * @param {jQuery} paneRootNode
         *  The jQueryfied root node for the text comments pane.
         *
         * @returns {TextCommentsPane}
         *  The instance of the text comments pane class.
         */
        this.createTextCommentsPane = function (paneRootNode) {
            return this.commentsPane || (this.commentsPane = this.member(new TextCommentsPane(this, paneRootNode)));
        };

        // initialization -----------------------------------------------------

        // create the context menu
        this.member(new UniversalContextMenu(this, { delay: 200 }));

        // initialize zoom handling
        this.zoomState.register("width", calculateOptimalZoomFactor);
        this.zoomState.set((COMPACT_DEVICE && isPortrait()) ? "width" : "fixed", 1, { silent: true });
        this.listenTo(this.zoomState, "change:state", zoomChangedHandler);

        // store the values of page width and height with paddings, after loading is finished
        this.waitForImportSuccess(function () {

            pageNodeWidth = pageNode.outerWidth();
            pageNodeHeight = pageNode.outerHeight();

            if (MEDIUM_DEVICE) {
                self.setTimeout(() => {
                    if (calculateOptimalZoomFactor() < 1) {
                        this.changeZoom("width");
                    }
                }, 0);
            }

            if (IOS_SAFARI_DEVICE) {
                var appContent = pageNode.parent();
                appContent.attr('contenteditable', true);

                var listenerList = docModel.getListenerList();
                pageNode.off(listenerList);
                appContent.on(listenerList);

                //we dont want the focus in the document on the beginning!
                hideFocus();
            }

            // quick fix for bug 40053
            if (_.browser.Safari) { addDeviceMarkers(app.getWindowNode()[0]); }
        });
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ async implInitializeGui(options) {

        // base class initialization
        await super.implInitializeGui(options);

        // event handling
        this.on('refresh:layout', this._refreshLayoutHandler);
    }

    /*protected override*/ implInitToolPanes() {
        super.implInitToolPanes();

        this.registerToolPane("format",      this._initFormatToolPane,      { label: FORMAT_HEADER_LABEL,         visibleKey: "view/toolpane/format/visible",      priority: 2 });
        this.registerToolPane("font",        this._initFontToolPane,        { label: FONT_HEADER_LABEL,           visibleKey: "view/toolpane/font/visible",        priority: 2 });
        this.registerToolPane("paragraph",   this._initParagraphToolPane,   { label: PARAGRAPH_LABEL,             visibleKey: "view/toolpane/paragraph/visible",   priority: 2 });
        this.registerToolPane("insert",      this._initInsertToolPane,      { label: INSERT_HEADER_LABEL,         visibleKey: "view/toolpane/insert/visible",      priority: 2 });
        this.registerToolPane("table",       this._initTableToolPane,       { label: TABLE_HEADER_LABEL,          visibleKey: "view/toolpane/table/visible",       priority: 1 });
        this.registerToolPane("drawing",     this._initDrawingToolPane,     { labelKey: "drawing/type/label",     visibleKey: "view/toolpane/drawing/visible",     priority: 1 });
        this.registerToolPane("review",      this._initReviewToolPane,      { label: REVIEW_HEADER_LABEL,         visibleKey: "view/toolpane/review/visible",      priority: 20, sticky: true });
        this.registerToolPane("spelling",    this._initSpellingToolPane,    { label: REVIEW_HEADER_LABEL,         visibleKey: "view/toolpane/spelling/visible",    priority: 20 });
        this.registerToolPane("changetrack", this._initChangeTrackToolPane, { label: CHANGETRACKING_HEADER_LABEL, visibleKey: "view/toolpane/changetrack/visible", priority: 20, sticky: true });
        this.registerToolPane("comment",     this._initCommentsToolPane,    { label: COMMENT_HEADER_LABEL,        visibleKey: "view/toolpane/comments/visible",    priority: 20 });
    }

    /*protected override*/ implInitViewMenu(viewButton) {
        super.implInitViewMenu(viewButton);

        viewButton.addContainer({ section: "zoom" });
        viewButton.addControl("view/zoom/type", new Button({ label: ZOOM_SCREEN_WIDTH_LABEL, value: "width" }));

        viewButton.addSection("options", OPTIONS_LABEL);
        // note: we do not set aria role to "menuitemcheckbox" or "button" due to Safari just working correctly with "checkox". CheckBox constructor defaults aria role to "checkbox"
        viewButton.addControl("view/toolpane/show",  new CheckBox(SHOW_TOOLBARS_CHECKBOX_OPTIONS), { visibleKey: "view/toolpane/show" });
        viewButton.addControl("view/rulerpane/show", new CheckBox({ label: /*#. check box label: show/hide ruler */ gt("Show ruler"), tooltip: gt("Toggle ruler next to document") }));
        viewButton.addControl("view/comments/show",  new CheckBox(SHOW_COMMENTSPANE_CHECKBOX_OPTIONS));
        viewButton.addControl("document/users",      new CheckBox(SHOW_COLLABORATORS_CHECKBOX_OPTIONS));
    }

    // private methods --------------------------------------------------------

    /*private*/ _initFontToolBars() {
        this.insertToolBar(new FontFamilyToolBar(this));
        this.insertToolBar(new FontStyleToolBar(this));
        this.insertToolBar(new FontColorToolBar(this));
        this.insertToolBar(new FormatPainterToolBar(this));
    }

    /*private*/ _initFormatToolPane() {
        this._initFontToolBars();
        this.insertToolBar(new ParagraphAlignmentToolBar(this));
        this.insertToolBar(new ParagraphFillBorderToolBar(this), { shrinkHide: true });
        this.insertToolBar(new ParagraphStyleToolBar(this),      { shrinkHide: true });
        this.insertToolBar(new ListStyleToolBar(this));
    }

    /*private*/ _initFontToolPane() {
        this._initFontToolBars();
    }

    /*private*/ _initParagraphToolPane() {
        this.insertToolBar(new ParagraphAlignmentToolBar(this));
        this.insertToolBar(new ParagraphFillBorderToolBar(this));
        this.insertToolBar(new ListStyleToolBar(this));
    }

    /*private*/ _initInsertToolPane() {

        const insertToolBar = this.createToolBar();

        insertToolBar.addSection("frames");
        insertToolBar.addControl("table/insert",        new TableSizePicker(this, MAX_TABLE_COLUMNS, MAX_TABLE_ROWS), { visibleKey: "table/insert/available" });
        insertToolBar.addControl("image/insert/dialog", new ImagePicker());
        insertToolBar.addControl("textframe/insert",    new InsertTextFrameButton({ toggle: true }), { visibleKey: "!view/combinedpanes" });
        insertToolBar.addControl("shape/insert",        new ShapeTypePicker(this));
        insertToolBar.addControl("comment/insert",      new Button(INSERT_COMMENT_OPTIONS));

        insertToolBar.addSection("hyperlink");
        insertToolBar.addControl("character/hyperlink/dialog", new Button(HYPERLINK_BUTTON_OPTIONS));
        if (!COMBINED_TOOL_PANES) {
            insertToolBar.addControl("character/insert/tab",         new Button({ icon: "png:insert-tab", label: gt("Tab stop"), tooltip: /*#. insert a horizontal tab stop into the text */ gt("Insert tab stop") }));
            insertToolBar.addControl("character/insert/break",       new Button({ icon: "png:insert-linebreak", label: gt("Line break"),  tooltip: /*#. insert a manual line break into the text */ gt("Insert line break") }));
            insertToolBar.addControl("character/insert/pagebreak",   new Button({ icon: "png:page-break", label: gt("Page break"),  tooltip: /*#. insert a manual page break into the text */ gt("Insert page break") }));
            insertToolBar.addControl("document/insert/headerfooter", new HeaderFooterPicker(this));
            insertToolBar.addControl("document/insertfield",         new InsertFieldPicker(this));
            insertToolBar.addSection("toc");
            insertToolBar.addControl("document/inserttoc", new TocPicker(), { visibleKey: "document/ooxml" });
        }

        if (this.docApp.canUseAI()) {
            insertToolBar.addSection("oxai");
            insertToolBar.addControl("oxai/create", new OpenAIDialogPicker(this));
        }
    }

    /*private*/ _initTableToolPane() {

        const tableToolBar = this.createToolBar();
        tableToolBar.addSection("colrow");
        tableToolBar.addControl("table/insert/row", new Button({ icon: "png:table-insert-row", tooltip: gt("Insert row") }));
        tableToolBar.addControl("table/delete/row", new Button({ icon: "png:table-delete-row", tooltip: gt("Delete selected rows") }));
        tableToolBar.addContainer();
        tableToolBar.addControl("table/insert/column", new Button({ icon: "png:table-insert-column", tooltip: gt("Insert column") }));
        tableToolBar.addControl("table/delete/column", new Button({ icon: "png:table-delete-column", tooltip: gt("Delete selected columns") }));

        if (!COMBINED_TOOL_PANES) {
            tableToolBar.addContainer();
            tableToolBar.addControl("table/split", new Button({ icon: "png:table-split", tooltip: /*#. split current table */ gt("Split table") }));
            tableToolBar.addSection("alignment");
            tableToolBar.addControl("table/alignment/menu", new TableAlignmentPicker(this));
            tableToolBar.addSection("style");
            tableToolBar.addControl("table/fillcolor", new FillColorPicker(this, { icon: "png:table-fill-color", label: null, tooltip: gt("Cell fill color"), splitValue: Color.AUTO }));
            tableToolBar.addContainer();
            tableToolBar.addControl("table/cellborder", new BorderFlagsPicker(this, { tooltip: CELL_BORDERS_LABEL, showInnerH: true, showInnerV: true }));

            this.insertToolBar(new TableStyleToolBar(this), { visibleKey: "document/ooxml" });

            const tableBorderToolBar = this.createToolBar({ classes: "no-separator" });
            tableBorderToolBar.addControl("table/borderwidth", new BorderWidthPicker(this, { tooltip: gt("Cell border width") }));
        }
    }

    /*private*/ _initDrawingToolPane() {

        const drawingToolBar = this.createToolBar();
        drawingToolBar.addControl("shape/insert",   new ShapeTypePicker(this), { visibleKey: "drawing/type/shape" });
        drawingToolBar.addControl("drawing/delete", new Button(DELETE_DRAWING_BUTTON_OPTIONS), { visibleKey: "drawing/delete" });

        if (!COMBINED_TOOL_PANES) {

            const drawingLineToolBar = this.createToolBar({ visibleKey: "drawing/shapes/support/line" });
            drawingLineToolBar.addControl("drawing/border/style", new DrawingLineStylePicker(this, { line: false }), { visibleKey: "drawing/border/style/support" });
            drawingLineToolBar.addControl("drawing/border/style", new DrawingLineStylePicker(this, { line: true }),  { visibleKey: "drawing/line/style/support" });
            drawingLineToolBar.addControl("drawing/border/color", new DrawingLineColorPicker(this, { line: false }), { visibleKey: "drawing/border/style/support" });
            drawingLineToolBar.addControl("drawing/border/color", new DrawingLineColorPicker(this, { line: true }),  { visibleKey: "drawing/line/style/support" });

            const drawingFillToolBar = this.createToolBar({ visibleKey: "drawing/shapes/support/fill", shrinkHide: true });
            drawingFillToolBar.addControl("drawing/fill/color", new DrawingFillColorPicker(this), { visibleKey: "drawing/fill/style/support" });

            const drawingCropToolBar = this.createToolBar({ visibleKey: "drawing/cropselection/visible", shrinkHide: true });
            drawingCropToolBar.addControl("drawing/crop", new ImageCropPosition(this));

            this.insertToolBar(new FormatPainterToolBar(this));

            const drawingLineEndsToolBar = this.createToolBar({ visibleKey: "drawing/shapes/support/lineendings" });
            drawingLineEndsToolBar.addControl("drawing/line/endings", new DrawingArrowPresetPicker());

            const drawingAnchorToolBar = this.createToolBar({ visibleKey: "document/editable/anydrawing" });
            drawingAnchorToolBar.addControl("drawing/anchorTo",          new AnchoragePicker());
            drawingAnchorToolBar.addControl("drawing/position",          new DrawingPositionPicker());
            drawingAnchorToolBar.addControl("drawing/order",             new DrawingArrangementPicker(this, { rotationControls: true }));
            drawingAnchorToolBar.addControl("drawing/verticalalignment", new TextBoxAlignmentPicker(this, { label: TEXT_ALIGNMENT_IN_SHAPE_LABEL, tooltip: TEXT_ALIGNMENT_IN_SHAPE_TOOLTIP }));
            drawingAnchorToolBar.addControl("drawing/textframeautofit",  new CheckBox({ label: gt("Autofit"), boxed: true, tooltip: gt("Turn automatic height resizing of text frames on or off") }));
        }
    }

    /*private*/ _initSpellingToolBar(toolBar) {
        toolBar.addControl("document/onlinespelling", new Button({ icon: "png:online-spelling", tooltip: gt("Check spelling permanently"), toggle: true, dropDownVersion: { label: gt("Check spelling") } }));
        toolBar.addSection("language");
        toolBar.addControl("character/language", new LanguagePicker(this));
    }

    /*private*/ _initChangeTrackToolBar(toolBar) {

        const cBoxToggleCTLabel = COMBINED_TOOL_PANES ?
            /*#. change tracking: switch change tracking on/off */ gt("On") :
            /*#. change tracking: toggle the change tracking feature on or off */ gt("Track changes");

        toolBar.addControl("toggleChangeTracking", new CheckBox(({ label: cBoxToggleCTLabel, tooltip: gt("Turn change tracking on or off"), boxed: true })));

        toolBar.addSection("acceptreject");
        if (!COMBINED_TOOL_PANES) {
            const acceptMenu = toolBar.addControl("acceptMoveSelectedChangeTracking", new CompoundSplitButton(this, {
                icon: "bi:check",
                label: /*#. change tracking function: accept the current document change */ gt("Accept"),
                tooltip: gt("Accept the current change and select the next change")
            }));
            acceptMenu.addControl("acceptMoveSelectedChangeTracking", new Button(ACCEPT_AND_NEXT_OPTIONS));
            acceptMenu.addControl("acceptSelectedChangeTracking",     new Button(ACCEPT_CURRENT_OPTIONS));
            acceptMenu.addControl("acceptChangeTracking",             new Button(ACCEPT_ALL_OPTIONS));

            const rejectMenu = toolBar.addControl("rejectMoveSelectedChangeTracking", new CompoundSplitButton(this, {
                icon: "bi:x",
                label: /*#. change tracking function: reject the current document change */ gt("Reject"),
                tooltip: gt("Reject the current change and select the next change")
            }));
            rejectMenu.addControl("rejectMoveSelectedChangeTracking", new Button(REJECT_AND_NEXT_OPTIONS));
            rejectMenu.addControl("rejectSelectedChangeTracking",     new Button(REJECT_CURRENT_OPTIONS));
            rejectMenu.addControl("rejectChangeTracking",             new Button(REJECT_ALL_OPTIONS));

        } else {
            toolBar.addControl("acceptSelectedChangeTracking", new Button({ icon: "bi:check", label: gt("Accept"), tooltip: gt("Accept the current change") }));
            toolBar.addControl("rejectSelectedChangeTracking", new Button({ icon: "bi:x", label: gt("Reject"), tooltip: gt("Reject the current change") }));
        }

        toolBar.addControl("selectPrevChangeTracking", new Button({ label: /*#. change tracking: select the previous change */ gt("Previous"), icon: "bi:chevron-left", tooltip: gt("Select the previous change in the document") }));
        toolBar.addControl("selectNextChangeTracking", new Button({ label: /*#. change tracking: select the next change */ gt("Next"), icon: "bi:chevron-right", tooltip: gt("Select the next change in the document") }));
    }

    /*private*/ _initCommentsToolBar(toolBar) {
        toolBar.addControl("comment/insert", new Button({ icon: "png:comment-add", label: /*#. insert a comment into the text */ gt("Insert comment"), tooltip: /*#. insert a comment into the text */ gt("Insert comment") }));
        toolBar.addContainer();
        toolBar.addControl("comment/displayModeParent", new CommentDisplayModePicker(this, { smallDevice: COMBINED_TOOL_PANES }));
        toolBar.addContainer();
        toolBar.addControl("comment/prevComment", new Button({ label: /*#. select the previous comment */ gt("Previous"), icon: "png:comment-back", tooltip: gt("Select the previous comment in the document") }));
        toolBar.addControl("comment/nextComment", new Button({ label: /*#. select the next comment */ gt("Next"), icon: "png:comment-next", tooltip: gt("Select the next comment in the document") }));
        toolBar.addControl("comment/deleteAll",   new Button({ icon: "png:comment-remove", label: gt("Delete all"), tooltip: /*#. delete all comments in the text */ gt("Delete all comments") }));
    }

    /*private*/ _initReviewToolPane() {

        const reviewToolBar = this.createToolBar({
            shrinkToMenu: {
                icon: "png:online-spelling",
                itemKey: "document/onlinespelling",
                splitButton: true,
                toggle: true,
                tooltip: gt("Check spelling"),
                //#. tooltip for a drop-down menu with functions for reviewing the document (spelling, change tracking)
                caretTooltip: gt("More review actions")
            }
        });

        // spelling controls into "Review" toolpane
        if (SPELLING_ENABLED) {
            this._initSpellingToolBar(reviewToolBar);
        }

        // AI for translations into "Review" toolpane
        if (this.docApp.canUseAI()) {
            reviewToolBar.addSection("oxai");
            reviewToolBar.addControl("oxai/create/selectionrange", new Button({ value: "translate", ...OX_AI_OPTIONS_TRANSLATE, iconStyle: { width: "28px", height: "28px" } }));
        }

        // change tracking controls into "Review" toolpane
        if (!this.docApp.isODF()) {
            const changeTrackToolBar = this.createToolBar();
            this._initChangeTrackToolBar(changeTrackToolBar);
        }

        // controls for comments into "Review" toolpane
        const commentsToolBar = this.createToolBar();
        this._initCommentsToolBar(commentsToolBar);
    }

    /*private*/ _initSpellingToolPane() {
        const toolBar = this.createToolBar({ visibleKey: "document/spelling/available" });
        this._initSpellingToolBar(toolBar);
    }

    /*private*/ _initChangeTrackToolPane() {
        const toolBar = this.createToolBar();
        this._initChangeTrackToolBar(toolBar);
    }

    /*private*/ _initCommentsToolPane() {

        // comment toolbar shrinks to a popup-menu -> avoid closing the popup, if sub-sub-menu is opened (DOCS-2657)
        // TODO: this should be solved generically, not just for this specific dropdown menu!
        const commentsToolBar = this.createToolBar({ shrinkToMenu: { label: COMMENT_HEADER_LABEL, focusableNodes: node => {
            return (($(node).is(POPUP_CONTENT_NODE_SELECTOR) && $(node).find(COMMENTFILTERGROUP_SELECTOR).length > 0) || ($(node).parent(COMMENTFILTERGROUP_SELECTOR).length > 0)) ? node : null;
        } } });

        this._initCommentsToolBar(commentsToolBar);
    }
}
