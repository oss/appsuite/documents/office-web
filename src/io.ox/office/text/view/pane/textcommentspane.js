/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';
import gt from 'gettext';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { XCommentsPane } from '@/io.ox/office/editframework/view/xcommentspane';
import { convertCssLength, findNextSiblingNode } from '@/io.ox/office/tk/utils';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

// class TextCommentsPane =====================================================

/**
 * The threaded comments pane contains all threaded comments in an OX Text
 * document.
 *
 * @param {DocView} docView
 *  The text view instance.
 *
 * @param {jQuery} commentsRootNode
 *  The jQueryfied instance of the comments root node
 */
export class TextCommentsPane extends XCommentsPane.mixin(ModelObject) {

    // the comment layer object
    #commentLayer = null;
    // assigning the jQueryfied comment layer root node
    #paneRootNode = null;
    // an object with all supported display modes
    #DISPLAY_MODE = null;

    constructor(docView, commentsRootNode) {

        // base constructor ---------------------------------------------------
        super(
            // parameters for the mixin class XCommentsPane
            docView,
            gt('There are no comments in this document.'),

            // parameters for the base class ModelObject
            docView.docModel
        );

        this.#commentLayer = this.docModel.getCommentLayer();
        this.#paneRootNode = this.$el = commentsRootNode;
        this.#DISPLAY_MODE = this.#commentLayer.getSupportedDisplayModes();

        // appending required content to the comment root node after "this.$el" is specified above
        this.appendContentsToNode();

        // Info: This "TextCommentsPane" is a "real" class with private and public methods. Therefore the public
        //       methods are assigned to the prototype object. But the functions in the mixin are part of the
        //       constructor. This means, that public funtions of this class cannot overwrite functions inherited
        //       from the mixin. If this is wanted, the following command is required in this constructor behind
        //       the mixin call:
        //       this.overwriteFun = this.prototype.overwriteFun;

        // initialization -----------------------------------------------------

        // prepare new comment frames for current bubble display mode
        this.listenTo(this, 'insert:frame', commentFrame => {

            const displayMode = this.#commentLayer.getDisplayMode();
            const frameNode = commentFrame.getNode();

            switch (displayMode) {
                case this.#DISPLAY_MODE.NONE:
                    break;
                case this.#DISPLAY_MODE.BUBBLES:
                    this.#commentLayer.activateBubbleMode(frameNode);
                    break;
                case this.#DISPLAY_MODE.SELECTED:
                    this.#commentLayer.highlightSelectedComments(frameNode);
                    break;
                case this.#DISPLAY_MODE.ALL:
                    this.#commentLayer.hightlightAllComments(frameNode);
                    break;
                case this.#DISPLAY_MODE.HIDDEN:
                    break;
                default:
                    globalLogger.warn('$badge{TextCommentsPane} "insert:frame" handler: unknown comment display mode');
            }
        });

        this.listenTo(docView, 'update:comments', this.#updateVerticalPositionInCommentsPane);

        this.listenTo(docView, 'switch:displaymode', this.#switchDisplayMode);

        this.listenTo(docView, 'commentThread:delete', this.#prepareDeleteCommentThreadNode); // allowing apps to deregister handler functions

        this.listenTo(docView, 'comment:visibility:changed', this.#updateHiddenStateInCommentsPane);

        this.listenTo(this.docApp, 'docs:reload:prepare', () => { this.stopListeningTo(this.docApp, 'docs:editmode'); });

        // setting the launch option 'commentsPaneVisible', but only, if it is explicitely specified
        // -> typically the value is undefined, but null is used in unit tests
        if (this.docApp.getLaunchOption('commentsPaneVisible') !== undefined && this.docApp.getLaunchOption('commentsPaneVisible') !== null) {
            this.toggle(!!this.docApp.getLaunchOption('commentsPaneVisible'));
        }
    }

    // public methods -----------------------------------------------------

    /**
     * Getting the comments pane root node.
     *
     * @returns {jQuery}
     *  The comments pane root node.
     */
    getNode() {
        return this.#paneRootNode;
    }

    /**
     * Whether the pane root node is visible in the DOM. This does not mean, that
     * comments are visible. In the buble mode, the pane root node is visible, but
     * comments are displayed.
     *
     * @returns {Boolean}
     *  Whether the pane root node is visible in the DOM.
     */
    isVisible() {
        return this.#paneRootNode.css('display') !== 'none' && this.#paneRootNode.height() > 0;
    }

    /**
     * Whether comment are displayed. This function does NOT check, if there are
     * any comments to display and also NOT, if the comments pane root node is
     * visible in the DOM. Instead it checks the comments display mode. This
     * differentation to the function 'isVisible' is important in the bubbles
     * mode, because comments are shown, but the comments pane is not visible in
     * that mode.
     *
     * @returns {Boolean}
     *  Whether the comments are shown in the document.
     */
    showsComments() {
        return !this.#commentLayer.isHiddenDisplayModeActive();
    }

    /**
     * Switching to the hidden comments display mode, so that no comments are shown.
     *
     * @returns {TextCommentsPane}
     *  A reference to this instance.
     */
    hide() {
        return this.toggle(false);
    }

    /**
     * Switching to the default comments display mode, so that the comments are shown.
     *
     * @returns {TextCommentsPane}
     *  A reference to this instance.
     */
    show() {
        return this.toggle(true);
    }

    /**
     * Changes the visibility of this view pane by toggling between the hidden
     * comment display mode and the default comment display mode.
     *
     * @param {Boolean} [state]
     *  If specified, shows or hides the view pane independently from its
     *  current visibility state. If omitted, toggles the visibility of the
     *  view pane.
     *
     * @returns {TextCommentsPane}
     *  A reference to this instance.
     */
    toggle(state) {

        const isVisible = this.showsComments();

        if (isVisible && (state === false || state === undefined)) {
            this.#commentLayer.switchCommentDisplayMode(this.#commentLayer.getHiddenDisplayMode());
        } else if (!isVisible && (state === true || state === undefined)) {
            this.#commentLayer.switchCommentDisplayMode(this.#commentLayer.getDefaultDisplayMode());
        }

        return this;
    }

    /**
     * If an author filter is active, a marker class needs to be added to the comment
     * threads. This is required for handling the comment 'shrinking' that does not work
     * reliable, if author filters are active. Therefore this 'shrinking' feature is
     * disabled with this marker class.
     *
     * @param {Boolean} state
     *  Whether the class for active author filters shall be added or removed.
     */
    handleActiveAuthorFilter(state) {
        for (const commentFrame of this.commentFrames()) {
            commentFrame.getNode().toggleClass('active-author-filter', state);
        }
    }

    // private methods ----------------------------------------------------

    /**
     * Updating the comments pane. Setting a vertical offset to all threads, so that the
     * thread has the best position relative to the document. The vertical offset of a
     * thread is determined by the position of the corresponding rangeMarker nodes in the
     * document and by the previous comment threads.
     *
     * Info: The comments in the pane are NOT absolutely positioned. This has the advantage,
     *       that during typing a new comment, the following comments are shifted downwards.
     *       And for this reasons, the calculated 'top' values inside this functions, are
     *       distances to the previous comment and NOT to the top of the container node.
     */
    #updateVerticalPositionInCommentsPane() {

        // the vertical offset of the page node in pixel
        const pageNodeOffset = this.docModel.getNode().offset().top;
        // whether the bubble mode is active
        const isBubbleMode = this.#commentLayer.isBubbleMode();
        // the height of the header node
        const paneHeaderHeight = isBubbleMode ? 0 : this.getHeader().outerHeight();
        // the next available vertical pixel position -> used to avoid overlap of comments
        let minVerticalPos = paneHeaderHeight;
        // the vertical distance between two comments in pixel
        const minDistance = 8;
        // the zoom factor
        const zoomFactor = this.docApp.getView().getZoomFactor();
        // the range marker object
        const rangeMarker = this.docModel.getRangeMarker();
        // the previous thread node
        let previousThreadNode = null;
        // the top pixel position of the previous thread node
        let previousTopPosition = 0;
        // the height of the previous thread node
        let previousThreadNodeHeight = 0;
        // a collector for all top positions of the threads
        const allTopPositions = [];
        // the unmodified top pixel position of the previous thread node
        let previousTop = 0;
        // whether a resort of comments is recommended (requires rerendering the comments side pane)
        let resortRecommended = false;
        // a pixel tolerance that avoids resorting of comments
        // -> this is especially important for temporary comments with temporary range markers with different height
        const resortTolerance = 8;
        // the comment collection instance
        const commentCollection = this.getCollection();

        // iterating over all (main) comments in the comments model (not over child comments)
        commentCollection.iterateCommentModels(commentModel => {

            // the ID of the comment, that is also used to get the thread node
            const commentId = commentModel.getId();
            // the comment thread
            const commentThread = this.getThreadedCommentFrameById(commentId);

            if (!commentThread || commentThread.getNode().hasClass('isFilteredThread')) { return; } // this happens in the loading phase

            // whether this is the temporary comment node for new threads without operation
            const isTemporaryComment = commentCollection.isTemporaryCommentId(commentId);

            // for temporary comment nodes the range marker must be inserted and synchronously be removed again
            if (isTemporaryComment) { rangeMarker.insertTemporaryRangeMarker(commentCollection.getTemporaryModel(), { onlyStart: true }); }

            // the comment thread node
            const commentThreadNode = commentThread.getNode();
            // the node that determines the top distance of the comment in the comment layer
            const refNode = rangeMarker.getStartMarker(commentId) ? rangeMarker.getStartMarker(commentId) : $(commentModel.getCommentPlaceHolderNode());

            if (!refNode || !refNode.length) { return; } // DOCS-4261

            // the text span following the start node
            const textSpan = findNextSiblingNode(refNode, 'span');
            // a correction value that handles the font-size
            const correction = textSpan ? convertCssLength($(textSpan).css('font-size'), 'px', 1) - convertCssLength($(refNode).css('font-size'), 'px', 1) : 0;
            // the vertical offset of the comment node relative to the page node
            let topPosition = Math.round((refNode.offset().top - correction - pageNodeOffset) / zoomFactor) - paneHeaderHeight;
            // the top position distance that will be set at the thread node
            let relativeTopPosition = 0;
            // the bubble node in bubble mode belonging to the thread
            const bubbleNode = isBubbleMode ? this.#commentLayer.getBubbleNodeById(commentId) : null;
            // the height of the comment thread node (or bubble node)
            const threadNodeHeight = isBubbleMode ? bubbleNode.height() : commentThreadNode.outerHeight();

            // checking the order of the comments in the side pane (might be caused by comments in header/footer or page aligned drawings)
            if (previousTop > 0 && topPosition < previousTop && (previousTop - topPosition > resortTolerance)) { resortRecommended = true; }
            allTopPositions.push({ id: commentId, top: topPosition });
            previousTop = topPosition; // saving the unmodified value for the next run

            // the temporary node might be positioned at the end of the comments pane
            if (isTemporaryComment) {
                if (previousThreadNode) {
                    previousThreadNode.after(commentThreadNode);
                } else {
                    this.getCommentsContainer().prepend(commentThreadNode); // insert as first child
                }
                rangeMarker.removeTemporaryRangeMarker({ onlyStart: true });
            }

            // workaround for abs pos element, which get the "moz-dragger" but we dont want it
            // if (_.browser.Firefox && convertCssLength(commentLayerNode.css('left'), 'px', 1) > 0) { commentThreadNode.css('left', 0); }

            // modifying the top position for the comment threads
            topPosition = Math.max(topPosition, minVerticalPos);

            // setting minimum value for the following comment
            minVerticalPos = topPosition + threadNodeHeight + minDistance;

            if (isBubbleMode) {
                bubbleNode.css('top', topPosition + 'px'); // setting the vertical offset for the comment bubble node
                if (commentCollection.isSelectedCommentId(commentId)) { commentThreadNode.css('marginTop', topPosition + 'px'); } // DOCS-2650
            } else {
                // comments are NOT absolute positioned, therefore setting the distance to the previous thread
                relativeTopPosition = topPosition - previousTopPosition - previousThreadNodeHeight;

                // setting the vertical offset for the comment thread node
                commentThreadNode.css('margin-top', relativeTopPosition + 'px');
            }

            // saving value for following comment
            previousTopPosition = topPosition;
            previousThreadNodeHeight = threadNodeHeight;
            previousThreadNode = commentThreadNode;

        }, this, { onlyThreads: true });

        if (resortRecommended) { this.#commentLayer.resortComments(allTopPositions); }
    }

    /**
     * Updating the hidden states in the comments pane. This can change due to changes of the
     * comments author filter list.
     */
    #updateHiddenStateInCommentsPane() {
        for (const commentFrame of this.commentFrames()) {
            commentFrame.updateCommentVisibility();
        }
    }

    /**
     * When the comment display mode changes, the comments pane root node
     * gets a new marker class representing the current display mode.
     *
     * @param {String} newMode
     *  The class name at the comment pane root node for the new selected
     *  comments display mode. Allowed values are:
     *  'nonemode', 'bubblesmode', 'selectedmode', 'allmode', 'hiddenmode'
     */
    #updateDisplayModeClass(newMode) {
        const allClassNames = this.#commentLayer.getAllCommentDisplayClassNames();
        this.#paneRootNode.removeClass(allClassNames);
        this.#paneRootNode.addClass(this.#commentLayer.getCommentDisplayClassName(newMode));
    }

    /**
     * A new display mode is active. The handlers at the comment thread may need to be updated.
     *
     * @param {String} displayMode
     *  The new display mode.
     */
    #switchDisplayMode(displayMode) {

        this.#updateDisplayModeClass(displayMode);

        // ... doing mode specific things
        switch (displayMode) {

            // no comment highlighting
            case this.#DISPLAY_MODE.NONE:

                // iterating over all comments in the comments model
                this.getCollection().iterateCommentModels(commentModel => {

                    // the comment thread
                    const commentThread = this.getThreadedCommentFrameById(commentModel.getId());

                    if (!commentThread) { return; } // this happens in the loading phase

                    // the comment thread node
                    const commentThreadNode = commentThread.getNode();

                    // calling the registration function for all required handlers in the comment layer
                    this.#commentLayer.highlightNoComments(commentThreadNode);

                }, this, { onlyThreads: true });

                break;

                // only selected and hovered comment is highlighted
            case this.#DISPLAY_MODE.BUBBLES:

                // iterating over all comments in the comments model
                this.getCollection().iterateCommentModels(commentModel => {

                    // the comment thread
                    const commentThread = this.getThreadedCommentFrameById(commentModel.getId());

                    if (!commentThread) { return; } // this happens in the loading phase

                    // the comment thread node
                    const commentThreadNode = commentThread.getNode();

                    // calling the registration function for all required handlers in the comment layer
                    this.#commentLayer.activateBubbleMode(commentThreadNode);

                }, this, { onlyThreads: true });

                break;

            // only selected and hovered comment is highlighted
            case this.#DISPLAY_MODE.SELECTED:

                // iterating over all comments in the comments model
                this.getCollection().iterateCommentModels(commentModel => {

                    // the comment thread
                    const commentThread = this.getThreadedCommentFrameById(commentModel.getId());

                    if (!commentThread) { return; } // this happens in the loading phase

                    // the comment thread node
                    const commentThreadNode = commentThread.getNode();

                    // calling the registration function for all required handlers in the comment layer
                    this.#commentLayer.highlightSelectedComments(commentThreadNode);

                }, this, { onlyThreads: true });

                break;

            // all comments are highlighted
            case this.#DISPLAY_MODE.ALL:

                // iterating over all comments in the comments model
                this.getCollection().iterateCommentModels(commentModel => {

                    // the comment thread
                    const commentThread = this.getThreadedCommentFrameById(commentModel.getId());

                    if (!commentThread) { return; } // this happens in the loading phase

                    // the comment thread node
                    const commentThreadNode = commentThread.getNode();

                    // calling the registration function for all required handlers in the comment layer
                    this.#commentLayer.hightlightAllComments(commentThreadNode);

                }, this, { onlyThreads: true });

                break;

            // the comment layer is hidden
            case this.#DISPLAY_MODE.HIDDEN:

                // iterating over all comments in the comments model
                this.getCollection().iterateCommentModels(commentModel => {

                    // the comment thread
                    const commentThread = this.getThreadedCommentFrameById(commentModel.getId());

                    if (!commentThread) { return; } // this happens in the loading phase

                    // the comment thread node
                    const commentThreadNode = commentThread.getNode();

                    // calling the registration function for all required handlers in the comment layer
                    this.#commentLayer.highlightNoComments(commentThreadNode);

                }, this, { onlyThreads: true });

                break;

            default:
                globalLogger.warn('TextCommentsPane.updateDisplayMode(): unknown comment display mode');
        }

    }

    /**
     * Preparing a deleted comment thread node after its deletion. Especially the
     * handler functions need to be removed.
     */
    #prepareDeleteCommentThreadNode(commentThreadNode) {

        // the current display mode
        const displayMode = this.#commentLayer.getDisplayMode();

        switch (displayMode) {
            case this.#DISPLAY_MODE.NONE:
                break;
            case this.#DISPLAY_MODE.BUBBLES:
                this.#commentLayer.deactivateAllHandlersAtCommentThread($(commentThreadNode));
                break;
            case this.#DISPLAY_MODE.SELECTED:
                this.#commentLayer.deactivateAllHandlersAtCommentThread($(commentThreadNode));
                break;
            case this.#DISPLAY_MODE.ALL:
                this.#commentLayer.deactivateAllHandlersAtCommentThread($(commentThreadNode));
                break;
            case this.#DISPLAY_MODE.HIDDEN:
                break;
            default:
                globalLogger.warn('TextCommentsPane.#prepareDeleteCommentThreadNode(): unknown comment display mode');
        }
    }
}
