/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { math, ary } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, convertHmmToLength, convertLength, convertLengthToHmm, resolveCssVar } from '@/io.ox/office/tk/dom';
import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { Canvas } from '@/io.ox/office/tk/canvas';
import { setToolTip } from '@/io.ox/office/tk/forms';
import { LOCALE_DATA } from '@/io.ox/office/tk/locale';

import { BasePane } from '@/io.ox/office/baseframework/view/pane/basepane';
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { getParagraphElement } from '@/io.ox/office/textframework/utils/position';
import { ParagraphDialog } from '@/io.ox/office/text/view/dialogs';

import '@/io.ox/office/text/view/pane/rulerpane.less';

// private functions ==========================================================

/**
 * Generates the data URl for an indent dragging control.
 *
 * @returns {string}
 *  The data URL for the indent dragging control.
 */
function createIndentCtrlsImg() {
    const canvas = new Canvas({ location: { width: 22, height: 22 } });
    const dataURL = canvas.renderToDataURL(ctx => {
        ctx.setFillStyle(resolveCssVar("--background"));
        ctx.setLineStyle({ style: resolveCssVar("--text"), width: 2 });
        ctx.drawPolygon([[1, 21], [21, 21], [21, 11], [11, 1], [1, 11]]);
    });
    canvas.destroy();
    return dataURL;
}

// class RulerPane ============================================================

/**
 * Represents the ruler in OX Documents edit applications.
 *
 * @param {EditView} docView
 *  The document view instance containing this ruler pane.
 */
export class RulerPane extends BasePane {

    // the selection object
    #selection;
    // app content root node (also known as scrolling node of the app)
    #scrollingNode = null;
    // page specific values in pixels
    #pageWidth = 0;
    #minPageWidth = 0;
    #innerPageWidth = 0;
    #marginLeft = 0;
    #marginRight = 0;
    #rulerContainer = null;
    #leftSectionNode = null;
    #leftMarkerBox = null;
    #mainSectionNode = null;
    #mainMarkerBox = null;
    #controlsContainer = null;
    #leftIndentCtrl = null;
    #rightIndentCtrl = null;
    #firstIndentCtrl = null;
    // unit based on locale of appsuite
    #localeDocUnit = LOCALE_DATA.unit;
    #quarterUnit;
    // predefined minimum value for paragraph width
    #minParaWidthHmm = 500;
    // saved value of the last paragraph position (used for performance, to compare if paragraph selection has changed)
    #storedParaPos = [];
    #storedTarget = '';
    // global Ruler Pane arguments stored during call of debounced method
    #globalRpRuler = false;
    #globalRpOtions = null;
    // global scroll left value for ruler pane (usually on zoom in)
    #globalLeftScroll = 0;

    constructor(docView) {

        // base constructor
        super(docView, {
            position: 'top',
            classes: 'ruler-pane translucent-shade'
        });

        // properties ---------------------------------------------------------

        this.#selection = this.docModel.getSelection();
        this.#quarterUnit = convertLength(0.25, (this.#localeDocUnit === 'mm' ? 'cm' : this.#localeDocUnit), 'px');

        // initialization -----------------------------------------------------

        // initially not visible ruler pane - shown only after successful import (but preparing space to avoid moving of document)
        const isVisibleRulerPaneAfterLoad = !SMALL_DEVICE && this.docApp.getUserSettingsValue('showRulerPane', true);
        this.toggle(isVisibleRulerPaneAfterLoad);
        this.#togglePaneHandler(isVisibleRulerPaneAfterLoad);

        this.docApp.waitForImportSuccess(() => {

            this.#leftMarkerBox   = $('<div class="marker-box">');
            this.#leftSectionNode = $('<div class="ruler-section left">').append('<div class="overlay-shade">', this.#leftMarkerBox);
            this.#mainMarkerBox   = $('<div class="marker-box">');
            this.#mainSectionNode = $('<div class="ruler-section main">').append('<div class="overlay-white">', '<div class="overlay-shade">', this.#mainMarkerBox);
            this.#rulerContainer  = $('<div class="ruler-container">').append(this.#leftSectionNode, this.#mainSectionNode);

            this.#leftIndentCtrl    = $('<span class="indent-ctrl left-ind-ctrl">');
            this.#rightIndentCtrl   = $('<span class="indent-ctrl right-ind-ctrl">');
            this.#firstIndentCtrl   = $('<span class="indent-ctrl first-ind-ctrl">');

            setToolTip(this.#leftIndentCtrl, gt('Left indent'));
            setToolTip(this.#rightIndentCtrl, gt('Right indent'));
            setToolTip(this.#firstIndentCtrl, gt('First line indent'));

            const indentCtrls = $([this.#firstIndentCtrl[0], this.#leftIndentCtrl[0], this.#rightIndentCtrl[0]]);
            indentCtrls.append(`<img src="${createIndentCtrlsImg()}" width="11" height="11">`);
            this.#controlsContainer = $('<div class="controls-container">').append(indentCtrls);

            // repaint slider handles when UI theme changes (canvas-based bitmaps)
            this.listenToGlobal("change:theme", () => indentCtrls.find("img").attr("src", createIndentCtrlsImg()));

            this.$el.append(this.#rulerContainer, this.#controlsContainer);

            // visibility
            const initVisible = this.docApp.getUserSettingsValue('showRulerPane', true);
            this.toggle(initVisible);
            this.#togglePaneHandler(initVisible);

            this.#scrollingNode = this.docApp.getWindowNode().find('.scrolling');

            // call ruler pane initially
            this.#paneLayoutHandler(true);

            // enable/disable ruler controls and tracking observer according to edit mode
            this.listenTo(this.docApp, 'docs:editmode', this.#updateIndentMarkers);
            this.#updateIndentMarkers();

            // initialize tracking observer for the indentation markers
            const trackingObserver = this.member(new TrackingObserver(this.#createTrackingCallbacks()));
            trackingObserver.observe(this.#leftIndentCtrl[0]);
            trackingObserver.observe(this.#firstIndentCtrl[0]);
            trackingObserver.observe(this.#rightIndentCtrl[0]);

            // event listeners
            this.listenTo(this, 'pane:show', this.#paneShowHandler);
            this.listenTo(this, 'pane:hide', this.#paneHideHandler);
            this.listenTo(this.docView, 'change:zoom', () => this.#paneLayoutHandler(true));
            this.listenTo(this.docModel, 'change:pageSettings ruler:initialize', () => this.#paneLayoutHandler(true));
            this.listenTo(this.docModel, 'pagination:finished', () => this.#paneLayoutHandler(false));
            this.listenTo(this.#selection, 'change', this.#selectionChangeHandler);
            this.listenTo(this.docModel.getCommentLayer(), 'commentlayer:created commentlayer:removed', () => this.#paneLayoutHandler(true));

            this.listenTo(this.$el, 'mouseup', this.#mouseUpHandler);

            this.listenTo(indentCtrls, 'dblclick', () => {
                if (this.docApp.isEditable()) { new ParagraphDialog(this.docView).show(); } // open dialog with paragraph properties
            });

            // scroll ruler with scrolling node horizontally
            this.listenTo(this.#scrollingNode, 'scroll', () => {
                this.#globalLeftScroll = this.#scrollingNode.scrollLeft();
                this.$el.css('left', -this.#globalLeftScroll);
                this.$el.css('width', `calc(100% + ${this.#globalLeftScroll}px)`); // DOCS-4158
            });
        });

    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implRefresh() {
        super.implRefresh();
        this.#paneLayoutHandler(false);
    }

    // private methods ----------------------------------------------------

    /**
     * Creates new positions for firstLine, left and right indent markers in ruler.
     */
    #repositionIndentCtrls() {

        const zoomFactor = this.docView.getZoomFactor();
        const startPos = this.#selection.getStartPosition();
        const paraPos = _.initial(startPos);
        const paragraph = getParagraphElement(this.#selection.getRootNode(), paraPos);
        const attrs = paragraph && this.docModel.paragraphStyles.getElementAttributes(paragraph);
        const paraAttrs = (attrs && attrs.paragraph) || null;
        const explAttrs = getExplicitAttributeSet(paragraph);
        const explParaAttrs = (explAttrs && explAttrs.paragraph) || null;

        let paraOffset = null;
        let indentLeft;
        let indentRight;
        let indentFirstLine;
        let explIndentLeft = null;
        let explIndentRight = null;
        let explIndentFirst = null;
        let listIndentLeft = null;
        let listIndentRight = null;
        let listIndentFirst = null;

        if (paraAttrs) {
            if (explParaAttrs) {
                explIndentLeft = explParaAttrs.indentLeft;
                explIndentRight = explParaAttrs.indentRight;
                explIndentFirst = explParaAttrs.indentFirstLine;
            }
            if (paraAttrs.listStyleId) {
                const listAttrs = this.docModel.getListCollection().getListLevel(paraAttrs.listStyleId, paraAttrs.listLevel || 0);
                if (listAttrs) {
                    listIndentLeft = listAttrs.indentLeft;
                    listIndentRight = listAttrs.indentRight;
                    listIndentFirst = listAttrs.indentFirstLine;

                    // taking care of hard-coded indentLeft and indentFirstLine at a paragraph style that specifies the list style.
                    // Example: Heading 3 specifies the listStyle "L6" and has an own indentLeft value (DOCS-3243).
                    if (attrs.styleId) {

                        // the full set of attributes from the style ID, including inheritance -> only required to get an optional list style
                        const styleAttrsInherited = this.docModel.paragraphStyles.getStyleAttributeSet(attrs.styleId);
                        const styleParaAttrsInherited = (styleAttrsInherited && styleAttrsInherited.paragraph) ? styleAttrsInherited.paragraph : null;

                        // taking care of hard-coded indentLeft and indentFirstLine at a paragraph style that specifies the list style.
                        // Example: Heading 3 specifies the listStyle "L6" and has an own indentLeft value (DOCS-3243).
                        if (styleParaAttrsInherited && styleParaAttrsInherited.listStyleId && styleParaAttrsInherited.listStyleId === paraAttrs.listStyleId) {

                            // the style attributes of the specified style ID, without inheritance
                            const styleAttrs = this.docModel.paragraphStyles.getStyleSheetAttributeMap(attrs.styleId);
                            const styleParaAttrs = (styleAttrs && styleAttrs.paragraph) ? styleAttrs.paragraph : null;

                            if (styleParaAttrs) {
                                listIndentLeft = _.isNumber(styleParaAttrs.indentLeft) ? styleParaAttrs.indentLeft : listIndentLeft;
                                listIndentFirst = _.isNumber(styleParaAttrs.indentFirstLine) ? styleParaAttrs.indentFirstLine : listIndentFirst;
                            }
                        }
                    }
                }
            }
            // priority order: explicit attribute > list style > paragraph style
            indentLeft = _.isFinite(explIndentLeft) ? explIndentLeft : (_.isFinite(listIndentLeft) ? listIndentLeft : paraAttrs.indentLeft) || 0;
            indentRight = _.isFinite(explIndentRight) ? explIndentRight : (_.isFinite(listIndentRight) ? listIndentRight : paraAttrs.indentRight) || 0;
            indentFirstLine = indentLeft + (_.isFinite(explIndentFirst) ? explIndentFirst : (_.isFinite(listIndentFirst) ? listIndentFirst : paraAttrs.indentFirstLine) || 0);
            // converting hmm -> px
            indentLeft = convertHmmToLength(indentLeft, 'px', 0.001) * zoomFactor;
            indentRight = convertHmmToLength(indentRight, 'px', 0.001) * zoomFactor;
            indentFirstLine = convertHmmToLength(indentFirstLine, 'px', 0.001) * zoomFactor;

            if (this.docModel.isPositionInTable()) {
                paraOffset = ($(paragraph).parent().offset().left - $(this.#selection.getEnclosingTable()).offset().left);
                indentLeft += paraOffset;
                indentFirstLine += paraOffset;
                indentRight = this.#innerPageWidth - indentLeft - $(paragraph).width() * zoomFactor;
            } else if (this.#selection.isAdditionalTextframeSelection()) {
                const drawingTextframe = $(this.#selection.getSelectedDrawings()[0]).find('.textframe');
                paraOffset = ($(paragraph).parent().offset().left - drawingTextframe.offset().left);
                indentLeft += paraOffset;
                indentFirstLine += paraOffset;
                indentRight = this.#innerPageWidth - indentLeft - $(paragraph).width() * zoomFactor;
            }

            this.#leftIndentCtrl.css('left', indentLeft);
            this.#rightIndentCtrl.css('left', this.#innerPageWidth - indentRight);
            this.#firstIndentCtrl.css('left', indentFirstLine);
        } else {
            this.#leftIndentCtrl.css('left', 0);
            this.#rightIndentCtrl.css('left', this.#innerPageWidth);
            this.#firstIndentCtrl.css('left', 0);
        }
    }

    /**
     * Direct function call that stores the parameters for the debounced function this.#paintRulerPane.
     *
     * @param {boolean} repaintRuler
     *  Whether to repaint the entire ruler contents.
     *
     * @param {Object} [options]
     *  @param {Number} [options.leftAddition=0]
     *  @param {Number} [options.rightAddition=0]
     */
    #storeRulerPaneData(repaintRuler, options) {
        this.#globalRpRuler ||= repaintRuler;
        this.#globalRpOtions = options;
        this._paintRulerPane(); // direct callback calls debounced callback
    }

    /**
     * Debounced function that paints the ruler pane with the data stored in the direct callback this.#storeRulerPaneData.
     */
    @debounceMethod({ delay: "animationframe" })
    _paintRulerPane() {

        const pageAttributes = this.docModel.pageStyles.getElementAttributes(this.docModel.getNode());
        const zoomFactor = this.docView.getZoomFactor();
        const options = this.#globalRpOtions;
        const preLeftOffset = options?.leftAddition || 0;
        const preRightOffset = options?.rightAddition || 0;

        // re-initialize values because of possible zoom or page settings change
        this.#minPageWidth = convertHmmToLength(this.#minParaWidthHmm, 'px', 0.001) * zoomFactor; // preset min width value 0,5cm
        if (this.docModel.isDraftMode()) {
            const pageNode = this.docModel.getNode();
            const pageNodeOuterWidth = pageNode.outerWidth();
            const pageNodeInnerWidth = pageNode.width();
            this.#marginLeft = (pageNodeOuterWidth - pageNodeInnerWidth) / 2 + preLeftOffset;
            this.#marginRight = (pageNodeOuterWidth - pageNodeInnerWidth) / 2;
            this.#pageWidth = pageNodeOuterWidth;
        } else {
            this.#marginLeft = convertHmmToLength(pageAttributes.page.marginLeft, 'px', 0.001) * zoomFactor + preLeftOffset;
            this.#marginRight = convertHmmToLength(pageAttributes.page.marginRight, 'px', 0.001) * zoomFactor;
            this.#pageWidth = convertHmmToLength(pageAttributes.page.width, 'px', 0.001) * zoomFactor;
        }
        this.#innerPageWidth = this.#pageWidth - this.#marginLeft - this.#marginRight;
        // predefined right offset - tables and drawings
        if (preRightOffset) {
            this.#innerPageWidth = preRightOffset;
            this.#marginRight = this.#pageWidth - this.#marginLeft - this.#innerPageWidth;
        }

        const fillSpace = this.docModel.getNode().offset().left + this.#globalLeftScroll; // page node left offset

        if (this.#globalLeftScroll) { this.$el.css('left', -this.#globalLeftScroll); }
        this.#rulerContainer.css({ left: fillSpace });
        // initial indent ctrls positioning
        this.#controlsContainer.css('left', fillSpace + this.#marginLeft - 5); // minus half width of indent control arrow
        this.#repositionIndentCtrls();

        // for performance reasons call fillRuler methods only when really needed
        if (this.#globalRpRuler || !this.#leftMarkerBox[0].hasChildNodes() || !this.#mainMarkerBox[0].hasChildNodes()) {
            this.#leftSectionNode.css('width', this.#marginLeft);
            this.#mainSectionNode.css('width', this.#innerPageWidth + this.#marginRight);
            this.#mainSectionNode.find('>.overlay-white').css('width', this.#innerPageWidth);
            this.#mainSectionNode.find('>.overlay-shade').css('width', this.#marginRight);

            this.#rulerContainer.toggleClass('small-zoom', zoomFactor < 1 && this.#localeDocUnit !== 'in');

            const generateMarkup = (width, prop) => {
                const size = this.#quarterUnit * zoomFactor; // 1/4 of the unit
                const count = Math.ceil(width / size);
                let markup = "";
                for (let i = 1; i < count; i += 1) {
                    const unit = (i % 2) ? 'quarter' : (i % 4) ? 'half' : 'full';
                    markup += `<div class="unit-marker ${unit}" style="${prop}:${i * size}px"></div>`;
                }
                return markup;
            };

            this.#leftMarkerBox.html(generateMarkup(this.#marginLeft, 'right'));
            this.#mainMarkerBox.html(generateMarkup(this.#pageWidth - this.#marginLeft, 'left'));
        }
        this.#globalRpRuler = false;
        this.#globalRpOtions = null;
    }

    /**
     * Generates specific left and right addition values,
     * when ruler needs to be painted for tables, nested tables or shapes.
     */
    #generateTableDrawingOptions() {

        const zoomFactor = this.docView.getZoomFactor();
        let options = null;
        let leftAddition;
        let rightAddition;

        if (this.docModel.isPositionInTable()) {
            const table = $(this.#selection.getEnclosingTable());
            leftAddition = table.offset().left - this.docModel.getNode().children('.pagecontent').offset().left;
            rightAddition = (leftAddition !== 0) ? table.width() * zoomFactor : 0;

            options = { leftAddition, rightAddition };
        } else if (this.#selection.isAdditionalTextframeSelection()) {
            const drawingTextframe = $(this.#selection.getSelectedDrawings()[0]).find('.textframe');
            leftAddition = drawingTextframe.offset().left - this.docModel.getNode().children('.pagecontent').offset().left;
            rightAddition = drawingTextframe.width() * zoomFactor;

            options = { leftAddition, rightAddition };
        }

        return options;
    }

    // -------------------------------HANDLERS---------------------------------

    #paneLayoutHandler(repaintRuler) {
        if (this.isVisible() && this.#scrollingNode) {
            this.#storeRulerPaneData(repaintRuler, this.#generateTableDrawingOptions());
        }
    }

    #togglePaneHandler(visible) {
        // removing border-top from app-pane node when ruler is visible
        this.docApp.getWindowNode().toggleClass('ruler-active', visible);
    }

    #paneShowHandler() {
        this.#togglePaneHandler(true);
    }

    #paneHideHandler() {
        this.#leftMarkerBox.empty();
        this.#mainMarkerBox.empty();
        this.#togglePaneHandler(false);
    }

    #mouseUpHandler() {
        this.#selection.restoreBrowserSelection();
    }

    #selectionChangeHandler() {
        if (!this.isVisible() || !this.#scrollingNode) { return; }
        const startPos = this.#selection.getStartPosition();
        const paraPos = _.initial(startPos);
        const target = this.docModel.getActiveTarget();

        if (ary.equals(paraPos, this.#storedParaPos) && this.#storedTarget === target) {
            return;
        }

        this.#storedParaPos = paraPos;
        this.#storedTarget = target;

        this.#storeRulerPaneData(true, this.#generateTableDrawingOptions());
    }

    #updateIndentMarkers() {
        const disabled = !this.docApp.isEditable();
        for (const elem of [this.#leftIndentCtrl[0], this.#firstIndentCtrl[0], this.#rightIndentCtrl[0]]) {
            elem.classList.toggle('disabled', disabled);
        }
    }

    /**
     * Creates the set of tracking callbacks for the indent markers on
     * ruler pane.
     */
    #createTrackingCallbacks() {

        let markerNode = null;
        let originalLeftPos = null;
        let complLeftPos = null;
        let createdOp = false;
        let isLeftMarker = false;
        let isRightMarker = false;
        const resizeLine = $('<div>').addClass('resizeline');
        let zoomFactor = null;
        let diffPos = 0;

        const startMoveMarker = record => {
            zoomFactor = this.docView.getZoomFactor();
            const activePageHeight = (this.docModel.getNode().height() + 60) * zoomFactor; // 60 for margins and extra space

            markerNode = $(record.current);
            isRightMarker = markerNode.hasClass('right-ind-ctrl');
            isLeftMarker = markerNode.hasClass('left-ind-ctrl');
            originalLeftPos = parseFloat(markerNode.css('left'));
            complLeftPos = isRightMarker ? Math.max(parseFloat(this.#leftIndentCtrl.css('left')), parseFloat(this.#firstIndentCtrl.css('left'))) : parseFloat(this.#rightIndentCtrl.css('left'));
            diffPos = (isLeftMarker) ? originalLeftPos - parseFloat(this.#firstIndentCtrl.css('left')) : 0;
            if (isLeftMarker && diffPos < 0) { complLeftPos += diffPos; }

            resizeLine.css({ width: '1px', height: activePageHeight, left: (record.start.x + this.#globalLeftScroll), top: '0px', zIndex: '99' });
            this.#scrollingNode.append(resizeLine);
        };

        const updateMoveMarker = record => {
            let updatePos = (originalLeftPos + record.offset.x);
            const minBoundary = isRightMarker ? (this.#minPageWidth + complLeftPos) : ((isLeftMarker && diffPos > 0) ? -this.#marginLeft + diffPos : -this.#marginLeft);
            const maxBoundary = isRightMarker ? (this.#innerPageWidth + this.#marginRight) : (complLeftPos - this.#minPageWidth);

            updatePos = math.ceilp(updatePos, this.#quarterUnit * zoomFactor); // first round up to full quarter unit value
            updatePos = math.clamp(updatePos, minBoundary, maxBoundary); // then check boundaries

            markerNode.css('left', updatePos);
            if (isLeftMarker) { this.#firstIndentCtrl.css('left', updatePos - diffPos); }
            resizeLine.css('left', markerNode.offset().left + this.#globalLeftScroll + 4);
        };

        const endMoveMarker = () => {

            const generator = this.docModel.createOperationGenerator();
            const target = this.docModel.getActiveTarget();
            const paraPos = _.initial(this.#selection.getStartPosition());
            const operationProperties = { attrs: { paragraph: {} } };
            let tableAddition = 0;
            let localInnerWidth = this.#innerPageWidth;
            let rightIndPosHmm;
            let leftIndPosHmm;
            let firstIndPosHmm;

            if (Math.abs(originalLeftPos - parseFloat(markerNode.css('left'))) < 1) { return; } // no ops generation if marker didn't move

            if (this.docModel.isPositionInTable()) {
                const paragraph = getParagraphElement(this.#selection.getRootNode(), paraPos);
                const cellcontent = $(paragraph).parent(); // cellcontent is a reference for indentation
                const paraOffset = (cellcontent.offset().left - $(this.#selection.getEnclosingTable()).offset().left);

                tableAddition = paraOffset;
                localInnerWidth = paraOffset + cellcontent.width() * zoomFactor;
            }

            if (isRightMarker) {
                const rightIndPos = (localInnerWidth - parseFloat(this.#rightIndentCtrl.css('left'))) / zoomFactor;
                rightIndPosHmm = convertLengthToHmm(rightIndPos, 'px');
                operationProperties.attrs.paragraph.indentRight = rightIndPosHmm;
            } else if (isLeftMarker) {
                const leftIndPos = (parseFloat(this.#leftIndentCtrl.css('left')) - tableAddition) / zoomFactor;
                leftIndPosHmm = convertLengthToHmm(leftIndPos, 'px');
                operationProperties.attrs.paragraph.indentLeft = leftIndPosHmm;
            } else { // first indent marker
                const firstIndPos = (parseFloat(this.#firstIndentCtrl.css('left')) - parseFloat(this.#leftIndentCtrl.css('left'))) / zoomFactor;
                firstIndPosHmm = convertLengthToHmm(firstIndPos, 'px');
                operationProperties.attrs.paragraph.indentFirstLine = firstIndPosHmm;
            }

            this.#selection.iterateContentNodes((paragraph, position) => {
                const paraWidthHmm = convertLengthToHmm($(paragraph).parent().width(), 'px');
                const attrs = this.docModel.paragraphStyles.getElementAttributes(paragraph);
                const paraAttrs = (attrs && attrs.paragraph) || null;
                let totalIndentation = 0;

                if (paraAttrs) {
                    const paraTxtPos = _.clone(position);
                    paraTxtPos.push(0);
                    this.docModel.doCheckImplicitParagraph(paraTxtPos); // check if paragraph is implicit

                    const opProperties = _.copy(operationProperties, true);

                    if (isRightMarker) {
                        totalIndentation = rightIndPosHmm + paraAttrs.indentLeft + this.#minParaWidthHmm;
                        if (paraWidthHmm <= totalIndentation) {
                            opProperties.attrs.paragraph.indentRight = rightIndPosHmm - (totalIndentation - paraWidthHmm + this.#minParaWidthHmm);
                        }
                    } else if (isLeftMarker) {
                        totalIndentation = leftIndPosHmm + paraAttrs.indentRight + this.#minParaWidthHmm;
                        if (paraWidthHmm <= totalIndentation) {
                            opProperties.attrs.paragraph.indentLeft = leftIndPosHmm - (totalIndentation - paraWidthHmm + this.#minParaWidthHmm);
                        }
                    } else { // first indent marker
                        totalIndentation = firstIndPosHmm + paraAttrs.indentRight + this.#minParaWidthHmm;
                        if (paraWidthHmm <= totalIndentation) {
                            opProperties.attrs.paragraph.indentFirstLine = firstIndPosHmm - (totalIndentation - paraWidthHmm + this.#minParaWidthHmm);
                        }
                    }

                    opProperties.start = position;
                    if (target) { opProperties.target = target; }
                    if (this.docModel.getChangeTrack().isActiveChangeTracking()) { opProperties.attrs.changes = { modified: this.docModel.getChangeTrack().getChangeTrackInfo() }; }
                    generator.generateOperation(SET_ATTRIBUTES, opProperties);
                }
            });

            this.docModel.applyOperations(generator);
            this.docView.scrollToSelection();
            createdOp = true;
        };

        const finalizeMoveMarker = () => {
            if (!createdOp) { // restore start values for markers if tracking is canceled (no operation created)
                markerNode.css('left', originalLeftPos);
                if (isLeftMarker) { this.#firstIndentCtrl.css('left', originalLeftPos - diffPos); }
            }

            resizeLine.remove();
            // reset values
            createdOp = false;
            markerNode = null;
            originalLeftPos = null;
            complLeftPos = null;
            isLeftMarker = false;
            isRightMarker = false;
            zoomFactor = null;
            diffPos = 0;
        };

        return {
            prepare: event => {
                event.preventDefault(); // prevent focus change
                return this.docApp.isEditable();
            },
            start:   startMoveMarker,
            move:    updateMoveMarker,
            end:     endMoveMarker,
            finally: finalizeMoveMarker
        };
    }

}
