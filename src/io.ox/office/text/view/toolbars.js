/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { ToolBar } from "@/io.ox/office/baseframework/view/pane/toolbar";
import {
    ParagraphAlignmentPicker, ParagraphAlignmentGroup, ParagraphSpacingMenu,
    ParagraphFillColorPicker, ParagraphBorderFlagsPicker, ParagraphStylePicker,
    TableStylePicker
} from "@/io.ox/office/text/view/controls";

// re-exports =================================================================

export * from "@/io.ox/office/textframework/view/toolbars";

// class ParagraphAlignmentToolBar ============================================

/**
 * A tool bar with controls to change text text alignment of paragraphs.
 *
 * @param {TextView} docView
 *  The document view instance containing this tool bar.
 */
export class ParagraphAlignmentToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        // create the controls of this tool bar
        this.addControl("paragraph/alignment", new ParagraphAlignmentGroup({ shrinkVisible: false }));
        this.addControl("paragraph/alignment", new ParagraphAlignmentPicker({ shrinkVisible: true }));
    }
}

// class ParagraphFillBorderToolBar ===========================================

/**
 * A tool bar with controls to change the spcing, fill color, and border
 * style of paragraphs.
 *
 * @param {TextView} docView
 *  The document view instance containing this tool bar.
 */
export class ParagraphFillBorderToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView, {
            shrinkToMenu: { icon: "png:border-h", tooltip: gt("Paragraph formatting") }
        });

        // create the controls of this tool bar
        this.addControl("paragraph/spacing/menu", new ParagraphSpacingMenu(docView));
        this.addContainer();
        this.addControl("paragraph/fillcolor", new ParagraphFillColorPicker(docView));
        this.addContainer();
        this.addControl("paragraph/borders", new ParagraphBorderFlagsPicker(docView));
    }
}

// class ParagraphStyleToolBar ================================================

/**
 * A tool bar with a picker control for the paragraph style sheet.
 *
 * @param {TextView} docView
 *  The document view instance containing this tool bar.
 */
export class ParagraphStyleToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        // create the controls of this tool bar
        this.addControl("paragraph/stylesheet", new ParagraphStylePicker(docView));
    }
}

// class TableStyleToolBar ====================================================

/**
 * A tool bar with a picker control for the table style sheet.
 *
 * @param {TextView} docView
 *  The document view instance containing this tool bar.
 */
export class TableStyleToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        // create the controls of this tool bar
        this.addControl("table/stylesheet", new TableStylePicker(docView, docView.getActiveTableFlags.bind(docView)));
    }
}
