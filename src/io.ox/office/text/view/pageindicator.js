/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';
import gt from 'gettext';

import { EObject } from '@/io.ox/office/tk/objects';

// constants ==============================================================

/**
 * The amount of milliseconds a fade transition should last.
 * Must equal the same time used in the stylesheet!
 */
const transitionDuration = 400;

/**
 * The presentation of the page indicator is subject of a lifecycle with
 * distinct phases. The event driven control of the page indicator is
 * influenced by the current lifecycle phase. In example: when the indicator
 * is fading in, a fade out must not be set up. Unlike during opaque phase.
 */
const LifeCycle = {
    /**
     * Neither visible nor in transition.
     */
    invisible: 'invisible',

    /**
     * Becoming opaque.
     */
    fadingIn: 'fading-in',

    /**
     * Visible but not in transition.
     */
    opaque: 'opaque',

    /**
     * Becoming invisible.
     */
    fadingOut: 'fading-out'
};

Object.freeze(LifeCycle);

// class PageIndicator ====================================================

/**
 * Page indicator controller. Handles content and visibility lifecycle.
 *
 * This class is a loosely coupled child to the text app view.
 * It is independent from its parent implementation and controlled by it.
 */
class PageIndicator extends EObject {

    constructor() {

        super();

        var
            /**
             * @type {PageIndicator}
             */
            self = this,

            /**
             * The container of the actually visible label.
             *
             * @type {jQuery}
             */
            indicatorElement = $('<div class="page-indicator">'),

            /**
             * The label containing the "Page X of Y" text.
             *
             * @type {jQuery}
             */
            labelElement = $('<div class="page-indicator-label">'),

            /**
             * Maintain internal state information for improved performance.
             * This is helpful for event handling and phase progression.
             * Certain transitions are allowed in only specific phases.
             *
             * @type {LifeCycle}
             */
            currentPhase = LifeCycle.invisible,

            /**
             * The current page.
             *
             * @type {number}
             */
            currentPage = 1,

            /**
             * The total number of pages.
             *
             * @type {?number}
             */
            pageCount = null,

            /**
             * The timeout invoked after the opaque phase.
             *
             * @type {?jQuery.Promise}
             */
            fadeOutTimeout = null,

            /**
             * The timeout invoked after the fade out phase.
             *
             * @type {?jQuery.Promise}
             */
            hideTimeout = null;

        // private methods ----------------------------------------------------

        /**
         * Switches to the fading in lifecycle phase.
         */
        function fadeIn() {
            indicatorElement.addClass(LifeCycle.fadingIn);
            currentPhase = LifeCycle.fadingIn;
            self.setTimeout(fasten, transitionDuration);
        }

        /**
         * Switches to the opaque lifecycle phase.
         */
        function fasten() {
            indicatorElement.addClass(LifeCycle.opaque);
            indicatorElement.removeClass(LifeCycle.fadingIn);
            currentPhase = LifeCycle.opaque;
            fadeOutTimeout = self.setTimeout(fadeOut, transitionDuration);
        }

        /**
         * Switches to the fading out lifecycle phase.
         */
        function fadeOut() {
            fadeOutTimeout = null;
            indicatorElement.addClass(LifeCycle.fadingOut);
            indicatorElement.removeClass(LifeCycle.opaque);
            currentPhase = LifeCycle.fadingOut;
            hideTimeout = self.setTimeout(hide, transitionDuration);
        }

        /**
         * Switches to the invisible lifecycle phase.
         */
        function hide() {
            indicatorElement.removeClass(LifeCycle.fadingOut);
            currentPhase = LifeCycle.invisible;
        }

        /**
         * Replaces the inner text of the given element.
         */
        function updateLabel() {
            if (pageCount === null) {
                //#. %1$d is the current page number
                labelElement.text(gt('Page %1$d', currentPage));
            } else {
                //#. %1$d is the current page number
                //#. %2$d is the total page count
                labelElement.text(gt('Page %1$d of %2$d', currentPage, pageCount));
            }
        }

        // public methods -----------------------------------------------------

        /**
         * Retrieves the indicator element to attach to the DOM tree.
         *
         * @returns {jQuery}
         */
        this.getElement = function () {
            return indicatorElement;
        };

        /**
         * @param {number} index A one-based page index.
         */
        this.setCurrentPage = function (index) {
            currentPage = index;
            updateLabel(labelElement);
        };

        /**
         * @param {number} count
         */
        this.setPageCount = function (count) {
            pageCount = count;
            updateLabel(labelElement);
        };

        /**
         * Makes the indicator visible, if not already and prolonges its
         * presentation in case it is in the opaque lifecycle phase.
         */
        this.show = function () {
            if (currentPhase === LifeCycle.invisible) {
                fadeIn();
            } else if (currentPhase === LifeCycle.opaque) {
                // Prolong presentation by replacing existing fade out timeout.
                fadeOutTimeout.abort();
                fadeOutTimeout = self.setTimeout(fadeOut, transitionDuration);
            } else if (currentPhase === LifeCycle.fadingOut) {
                hideTimeout.abort();
                indicatorElement.removeClass(LifeCycle.fadingOut);
                fadeIn();
            }
        };

        // Assemble the view.
        indicatorElement.append(labelElement);
        updateLabel();
    }
}

export default PageIndicator;
