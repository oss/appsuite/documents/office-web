/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { is } from "@/io.ox/office/tk/algorithms";
import { Button, UserPicture } from "@/io.ox/office/tk/controls";
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { isGuest } from "@/io.ox/office/tk/utils/driveutils";

import { CompoundMenu } from "@/io.ox/office/baseframework/view/popup/compoundmenu";
import { CONTACTS_AVAILABLE } from "@/io.ox/office/textframework/utils/config";
import { getDOMPosition } from '@/io.ox/office/textframework/utils/position';

// constants ==================================================================

// labels for different change tracking actions
const CHANGE_TRACK_TYPE_LABELS = {
    //#. change tracking: something has been inserted into the document (text, drawing object, table, ...)
    inserted: gt.pgettext("change-tracking", "Inserted"),
    //#. change tracking: something has been deleted from the document (text, drawing object, table, ...)
    removed:  gt.pgettext("change-tracking", "Deleted"),
    //#. change tracking: something has been modified in the document (formatting, table size, ...)
    modified: gt.pgettext("change-tracking", "Formatted")
};

// labels for different change tracking source node kinds
const CHANGE_TRACK_KIND_LABELS = {
    //#. change tracking: text has been changed/inserted/deleted
    text: gt.pgettext("change-tracking", "Text"),
    //#. change tracking: a paragraph has been changed/inserted/deleted
    paragraph: gt.pgettext("change-tracking", "Paragraph"),
    //#. change tracking: a table has been changed/inserted/deleted
    table: gt.pgettext("change-tracking", "Table"),
    //#. change tracking: a table row has been changed/inserted/deleted
    row: gt.pgettext("change-tracking", "Table row"),
    //#. change tracking: a table cell has been changed/inserted/deleted
    cell: gt.pgettext("change-tracking", "Table cell"),
    //#. change tracking: a text frame has been changed/inserted/deleted
    frame: gt.pgettext("change-tracking", "Text frame"),
    //#. change tracking: a drawing shape (rectangle, arrow, star, ...) has been changed/inserted/deleted
    shape: gt.pgettext("change-tracking", "Shape"),
    //#. change tracking: a drawing object (e.g. image) has been changed/inserted/deleted
    drawing: gt.pgettext("change-tracking", "Drawing")
};

// class ChangeTrackBadge =====================================================

export class ChangeTrackBadge extends UserPicture {

    constructor(docView) {

        // base constructor
        super();

        this.docModel = docView.docModel;
        this.docView = docView;
        this.docApp = docView.docApp;

        // add CSS marker class and ARIA role
        this.$el.addClass("change-track-badge");
        this.$el.attr("role", "dialog");

        // create DOM structure
        this._$userLink = $(CONTACTS_AVAILABLE ? "<a>" : "<div>").css("cursor", "default").addClass("change-track-author").text(gt("Unknown"));
        this._$action = $('<div class="change-track-action">').text(gt("Unknown"));
        const $description = $('<div class="change-track-description">').append(this._$userLink, this._$action);
        this._$date = $('<div class="change-track-date">').text(gt("Unknown"));
        this.$el.append($description, this._$date);
    }

    // public methods ---------------------------------------------------------

    initialize() {

        // get change tracking information from model
        const changeTrackInfo = this.docModel.getChangeTrack().getRelevantTrackingInfo();
        if (!changeTrackInfo) { return false; }

        // GUI label for the action type
        const typeLabel = CHANGE_TRACK_TYPE_LABELS[changeTrackInfo.type];
        if (!typeLabel) {
            globalLogger.error(`$badge{ChangeTrackBadge} initialize: missing label for change tracking type "${changeTrackInfo.type}"`);
            return false;
        }

        // GUI label for the source node kind
        const kindLabel = CHANGE_TRACK_KIND_LABELS[changeTrackInfo.kind];
        if (!kindLabel) {
            globalLogger.error(`$badge{ChangeTrackBadge} initialize: missing label for change tracking source kind "${changeTrackInfo.kind}"`);
            return false;
        }

        // set placeholder picture as default and reset author halo link
        this._$userLink.removeAttr("href data-detail-popup").removeData("internal_userid").removeClass("needs-action person").text("");

        const userId = changeTrackInfo.userId;
        const author = changeTrackInfo.author || gt('Unknown');
        const colorIndex = this.docApp.getAuthorColorIndex(changeTrackInfo.author);

        this._$userLink.attr("data-color-index", colorIndex).text(_.noI18n(author));
        this._$date.text(this.docView.getDisplayDateString(Date.parse(changeTrackInfo.date), { toLocal: false }));
        this._$action.text(_.noI18n(`${typeLabel}: ${kindLabel}`));

        // fetch user photo and enable halo view only if author OX User ID is available
        if (is.number(userId)) {
            this.onFulfilled(this.docApp.getUserInfo(userId), info => {
                const displayName = (info.guest && info.displayName) || info.operationName; // use operationName whenever possible, but not for guests
                this.setValue({ userId, userName: displayName });
                this._$userLink.text(_.noI18n(displayName));

                if (!info.guest && !isGuest() && CONTACTS_AVAILABLE) {
                    this._$userLink.data("internal_userid", userId);
                    this._$userLink.attr({
                        href: "#",
                        "data-detail-popup": "halo"
                    });
                    this._$userLink.addClass("needs-action person");
                }
            });
        } else {
            // reset user photo, otherwise the last visible drawing remains (46284)
            this.setValue({ userId: null, userName: "" });
        }

        return true;
    }
}

// class ChangeTrackPopup =====================================================

export class ChangeTrackPopup extends CompoundMenu {

    constructor(docView) {

        // base constructor
        super(docView, {
            classes: "change-track-popup f6-target",
            anchor: () => this._getChangeTrackAnchor(),
            anchorBox: docView.getContentRootNode(),
            autoFocus: false,
            autoClose: false
        });

        // ARIA settings
        this.$el.attr({ tabindex: "-1", role: "dialog", "aria-label": gt("Change tracking") });

        this.addSection("badge");
        this._userBadge = this.addControl(null, new ChangeTrackBadge(docView));

        this.addSection("buttons", { inline: true });
        this.addControl("acceptSelectedChangeTracking", new Button({ icon: "bi:check", value: "accept", label: gt("Accept"), tooltip: gt("Accept this change in the document") }));
        this.addControl("rejectSelectedChangeTracking", new Button({ icon: "bi:x",     value: "reject", label: gt("Reject"), tooltip: gt("Reject this change in the document") }));
        this.addControl("changetrackPopupShow",         new Button({ icon: "bi:eye",   value: "show",   label: gt("Show"),   tooltip: gt("Show this change in the document") }));
    }

    // public methods ---------------------------------------------------------

    /*override*/ show() {
        if (this._userBadge.initialize()) {
            super.show();
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the anchor position for this menu.
     */
    /*private*/ _getChangeTrackAnchor() {

        if (this.docModel.isDrawingSelected()) {
            return this.docModel.getSelection().getSelectedDrawing();
        }

        const endPoint = getDOMPosition(this.docModel.getCurrentRootNode(), this.docModel.getSelection().getEndPosition());
        return endPoint?.node.parentNode ?? this.docModel.getNode();
    }
}
