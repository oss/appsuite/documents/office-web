/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import "$/io.ox/core/tk/datepicker"; // for side effects only (Bootstrap/JQuery plugin initialization)

import { is } from "@/io.ox/office/tk/algorithms";
import { createSvg } from "@/io.ox/office/tk/dom";
import { Button, CheckBox, RadioGroup, RadioList } from "@/io.ox/office/tk/controls";

import { CompoundMenu } from "@/io.ox/office/baseframework/view/popup/compoundmenu";

// private functions ==========================================================

function createFormatOptions(listControl, fieldConfig) {
    listControl.clearOptions();
    if (is.array(fieldConfig.formats)) {
        for (const entry of fieldConfig.formats) {
            listControl.addOption(entry.option, { label: _.noI18n(entry.value) });
        }
        listControl.show();
    } else {
        listControl.hide();
    }
}

// class FieldFormatPicker ====================================================

/**
 * Inline button group for available text field formats.
 */
class FieldFormatGroup extends RadioGroup {

    // public methods ---------------------------------------------------------

    initialize(fieldConfig) {
        createFormatOptions(this, fieldConfig);
    }
}

// class FieldFormatPicker ====================================================

/**
 * Dropdown list for available text field formats.
 */
class FieldFormatPicker extends RadioList {

    constructor() {
        super({
            anchorBorder: ["right", "left", "bottom", "top"],
            label: gt("Custom")
        });
    }

    // public methods ---------------------------------------------------------

    initialize(fieldConfig) {
        createFormatOptions(this, fieldConfig);
    }
}

// class AutoDateCheckbox =====================================================

/**
 * Checkbox control for auto/fixed date mode.
 */
class AutoDateCheckBox extends CheckBox {
    constructor() {
        super({
            label: gt("Update automatically"),
            tooltip: gt("Update this date field automatically on document load, download and mail"),
            boxed: true
        });
    }
}

// class SetTodayButton =======================================================

/**
 * Push button to set the current date to the datepicker control.
 */
class SetTodayButton extends Button {
    constructor() {
        super({
            classes: "set-today-btn",
            //#. button that will set a textfield to today's date
            label: gt("Set to today"),
            //#. button that will set a textfield to today's date
            tooltip: gt("Set to today's date"),
            textAlign: "center"
        });
    }
}

// class DatePickerToggleButton ===============================================

/**
 * Toggle button for the datepicker control.
 */
class DatePickerToggleButton extends Button {
    constructor() {
        super({
            classes: "toggle-datepicker-btn",
            icon: "bi:calendar",
            //#. button that picks a date from a calendar control
            tooltip: gt("Pick date"),
            toggle: true
        });

        this.$el.find('[data-icon-id]').append(this._textNode = createSvg("text"));
        $(this._textNode).attr({ "text-anchor": "middle", x: 8, y: 13, "font-size": 9 });
    }

    updateDateLabel(date) {
        this._textNode.textContent = date ? _.noI18n(String(date.getDate())) : "";
    }
}

// class FieldFormatPopup =====================================================

export class FieldFormatPopup extends CompoundMenu {

    constructor(docView) {

        // base constructor
        super(docView, {
            classes: "field-format-popup f6-target",
            title: gt("Edit field"),
            anchor: () => this.docModel.getFieldManager().getSelectedFieldNode(),
            anchorBox: docView.getContentRootNode(),
            autoClose: false
        });

        // private properties
        this._currentFieldNode = null;
        this._currentFormat = null;

        // ARIA settings
        this.$el.attr({ tabindex: -1, role: "dialog", "aria-label": gt("Field formatting") });

        this.on("popup:hide", () => {
            this.docModel.getSelection().restoreBrowserSelection();
        });

        // button list and dropdown list for field formats
        this.addSection("format", gt("Select format"));
        this._fieldFormatGroup = this.addControl("document/formatfield", new FieldFormatGroup(docView));
        this._fieldFormatPicker = this.addControl("document/formatfield", new FieldFormatPicker(docView));

        // additional controls for date fields
        this.addSection("date", gt("Change date"));
        this._autoDateCheckbox = this.addControl(null, new AutoDateCheckBox());
        this.addContainer({ inline: true, boxed: true });
        this._setTodayBtn = this.addControl(null, new SetTodayButton(), { expandInline: true });
        this._datePickerBtn = this.addControl(null, new DatePickerToggleButton());
        this.addContainer();
        this.addNodes(this._datePickerNode = $('<div class="date-picker-node">'));

        // configure the datepicker control (JQuery plugin)
        this._datePickerNode.datepicker({
            calendarWeeks: false,
            todayBtn: false,
            showAnim: false
        });

        // update enabled state of other controls depending on auto/fixed date mode
        this._autoDateCheckbox.onChange(state => {
            this._setTodayBtn.enable(!state);
            this._datePickerBtn.enable(!state);
            this._datePickerBtn.setValue(false);
        });

        // trigger event for changing auto/fixed date mode
        this._autoDateCheckbox.on("group:commit", state => {
            this.docView.trigger("fielddatepopup:autoupdate", state);
        });

        // set datepicker control to today's date
        this._setTodayBtn.on("group:commit", () => {
            const date = new Date();
            this._currentFieldNode.data("datepickerValue", date);
            const isoDateTxt = this.docModel.numberFormatter.formatValue("yyyy-mm-dd\\Thh:mm:ss.00", date).text;
            docView.trigger("fielddatepopup:change", { value: null, format: this._currentFormat, standard: isoDateTxt }); // send default - which is current date
        });

        // update visibility of datepicker control when clicking toggle button
        this._datePickerBtn.onChange(state => {
            this._datePickerNode.toggleClass("hidden", !state);
            this.refresh({ immediate: true });
        });

        // handler for selecting a date in the datepicker control
        this._datePickerNode.on("changeDate", () => {
            const date = this._datePickerNode.datepicker("getDate");
            this._currentFieldNode.data("datepickerValue", date);
            const viewDate = this._datePickerNode.data().datepicker.viewDate;
            const fmtDateTxt = this.docModel.numberFormatter.formatValue(this._currentFormat, viewDate).text;
            const isoDateTxt = this.docModel.numberFormatter.formatValue("yyyy-mm-dd\\Thh:mm:ss.00", viewDate).text;
            docView.trigger("fielddatepopup:change", { value: fmtDateTxt, standard: isoDateTxt });
        });
    }

    // public methods ---------------------------------------------------------

    initialize(fieldConfig, fieldNode) {

        // special handling for date fields
        const isDateTime = is.array(fieldConfig.formats) && (/^DATE|^TIME/i).test(fieldConfig.type);

        // initialize field format group/picker
        if (isDateTime) {
            this._fieldFormatPicker.initialize(fieldConfig);
            this._fieldFormatGroup.hide();
        } else {
            this._fieldFormatGroup.initialize(fieldConfig);
            this._fieldFormatPicker.hide();
        }

        // prepare visibility of date controls
        this._autoDateCheckbox.toggle(isDateTime);
        this._setTodayBtn.toggle(isDateTime);
        this._datePickerBtn.toggle(isDateTime);
        this._datePickerBtn.setValue(false); // always hide the datepicker initially

        // nothing more to do for non-date fields
        if (!isDateTime) { return; }

        // initialize properties and options
        this._currentFieldNode = $(fieldNode);
        this._currentFormat = fieldConfig.instruction;
        this._autoDateCheckbox.setValue(!!fieldConfig.autoDate);

        // nothing more to do without a textfield node
        if (!this._currentFieldNode.length) { return; }

        // use cached date from textfield node if available
        const cachedDate = this._currentFieldNode.data("datepickerValue");
        if (cachedDate) {
            this._initDatePicker(cachedDate);
            return;
        }

        // collect text from all <span> elements of the textfield
        let nextNode = $(fieldNode).next();
        let fieldText = "";
        while (nextNode.is("span")) {
            fieldText += nextNode.text();
            nextNode = nextNode.next();
        }

        // initialize the datepicker control with parsed field text
        // TODO: this does not work reliably for all date formats (especially with leading weekday)
        const parseDate = this.docModel.numberFormatter.parseFormattedDate(fieldText);
        parseDate?.setHours(0, 0, 0, 0);
        this._initDatePicker(parseDate);
    }

    // private methods --------------------------------------------------------

    /*private*/ _initDatePicker(date) {
        this._datePickerNode.datepicker("update", date);
        this._datePickerBtn.updateDateLabel(date);
    }
}
