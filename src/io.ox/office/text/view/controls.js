/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { extendOptions } from "@/io.ox/office/tk/utils";
import { Color } from "@/io.ox/office/editframework/utils/color";

import { Button, RadioGroup, CompoundButton, CompoundSplitButton, FillColorPicker,
    BorderFlagsPicker } from "@/io.ox/office/textframework/view/controls";
import { OX_AI_OPTIONS_CREATE, OX_AI_OPTIONS_REPHRASE, OX_AI_OPTIONS_SUMMARIZE,
    OX_AI_OPTIONS_TRANSLATE } from "@/io.ox/office/textframework/view/labels";

// re-exports =================================================================

export * from "@/io.ox/office/textframework/view/controls";

// class ParagraphFillColorPicker =============================================

const PARA_FILL_COLOR_LABEL = gt("Paragraph fill color");

/**
 * A color picker control for the paragraph background color in text documents.
 */
export class ParagraphFillColorPicker extends FillColorPicker {
    constructor(docView, config) {
        super(docView, extendOptions({
            icon: "png:para-fill-color",
            label: null,
            tooltip: PARA_FILL_COLOR_LABEL,
            splitValue: Color.AUTO,
            dropDownVersion: { label: PARA_FILL_COLOR_LABEL }
        }, config));
    }
}

// class ParagraphBorderFlagsPicker ===========================================

const PARA_BORDERS_LABEL = gt("Paragraph borders");

/**
 * A border type control for paragraph borders in text documents.
 */
export class ParagraphBorderFlagsPicker extends BorderFlagsPicker {
    constructor(docView) {
        super(docView, {
            tooltip: PARA_BORDERS_LABEL,
            // ODF does not support inner borders at paragraphs
            showInnerH: !docView.getApp().isODF(),
            dropDownVersion: { label: PARA_BORDERS_LABEL }
        });
    }
}

// class HeaderFooterPicker ===================================================

/**
 * A button with a drop-down menu providing options to insert and remove
 * headers and footers in a text document.
 */
export class HeaderFooterPicker extends CompoundSplitButton {

    constructor(docView) {

        super(docView, {
            icon: "png:header-footer",
            label: /*#. insert header and footer in document */ gt("Header & footer"),
            tooltip: gt("Insert header & footer in document"),
            splitValue: "goto",
            updateCaptionMode: "none"
        });

        const buttonGroup = this.addControl("document/insert/headerfooter", new RadioGroup());
        buttonGroup.addOption("default", { label: /*#. header and footer */ gt("Same across entire document") });
        buttonGroup.addOption("first",   { label: /*#. header and footer */ gt("Different first page") });
        buttonGroup.addOption("evenodd", { label: /*#. header and footer */ gt("Different even & odd pages") });
        buttonGroup.addOption("all",     { label: /*#. header and footer */ gt("Different first, even & odd pages") });

        this.addSection("remove");
        this.addControl("document/insert/headerfooter/remove", new Button({ label: /*#. remove all headers and footers from document */ gt("Remove all headers and footers") }));
    }
}

// class InsertFieldPicker ====================================================

/**
 * Control to pick field from dropdown
 */
export class InsertFieldPicker extends CompoundButton {

    constructor(docView) {

        super(docView, {
            label: /*#. button to insert field in document */ gt("Field"),
            tooltip: gt("Insert field"),
            icon: "png:field"
        });

        this.addControl("document/insertfield", new Button({ value: "page",     label: /*#. insert page number */ gt("Page number") }));
        this.addControl("document/insertfield", new Button({ value: "numpages", label: /*#. insert number of pages in document */ gt("Page count") }));
        this.addControl("document/insertfield", new Button({ value: "date",     label: /*#. insert current date and time */ gt("Date & time") }));
        this.addControl("document/insertfield", new Button({ value: "filename", label: /*#. insert document name */ gt("Document name") }));
        this.addControl("document/insertfield", new Button({ value: "author",   label: /*#. insert author's name */ gt("Author name") }));
    }
}

// class OpenAIDialogPicker ===================================================

/**
 * Control to pick AI dialog from dropdown
 */
export class OpenAIDialogPicker extends CompoundButton {

    constructor(docView) {

        super(docView, {
            // label: /*#. button to use AI in document */ gt("AI"),
            tooltip: gt("Create, transform or translate content with AI"),
            icon: "bi:ox-ai",
            iconStyle: { width: "32px", height: "32px" }
        });

        this.addControl("oxai/create", new Button({ value: "write", ...OX_AI_OPTIONS_CREATE, icon: null }));
        this.addControl("oxai/create/selectionrange", new Button({ value: "rephrase", ...OX_AI_OPTIONS_REPHRASE, icon: null }));
        this.addControl("oxai/create/selectionrange", new Button({ value: "summarize", ...OX_AI_OPTIONS_SUMMARIZE, icon: null }));
        this.addControl("oxai/create/selectionrange", new Button({ value: "translate", ...OX_AI_OPTIONS_TRANSLATE, icon: null }));
    }
}
