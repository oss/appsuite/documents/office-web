/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { getOption, getStringOption } from '@/io.ox/office/tk/utils';
import { CHANGE_STYLESHEET, INSERT_STYLESHEET, DELETE_STYLESHEET, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { getDOMPosition, getOxoPosition, getParagraphLength } from '@/io.ox/office/textframework/utils/position';
import { MARGINAL_NODE_SELECTOR, getClosestMarginalTargetNode, getTargetContainerId,
    isImplicitParagraphNode } from '@/io.ox/office/textframework/utils/dom';
import { ParagraphStyleDialog } from '@/io.ox/office/textframework/view/dialogs';
import { TranslationDictionary } from '@/io.ox/office/baseframework/resource/translationdictionary';
import { setExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';

// private functions ======================================================

function trimAllWhitespace(string) {
    return string.toLowerCase().replace(/\s+/g, '');
}

// mix-in class StylesheetMixin ===========================================

/**
 *
 */
export default function StylesheetMixin(app) {

    var self = this;

    var paraClearAttributes = null;
    var charClearAttributes = null;

    var dictionary;
    var reverseDict;

    // private methods ----------------------------------------------------

    function getParaClearAttributes() {
        if (!paraClearAttributes) {
            paraClearAttributes = self.paragraphStyles.buildNullAttributeSet();
        }
        return paraClearAttributes;
    }

    function getCharClearAttributes() {
        if (!charClearAttributes) {
            charClearAttributes = self.characterStyles.buildNullAttributeSet();
            delete charClearAttributes.character.url;
        }
        return charClearAttributes;
    }

    function getCurrentParaAttributes() {
        var endSpan = getDOMPosition(self.getCurrentRootNode(), self.getSelection().getEndPosition()).node.parentElement;

        var character = self.characterStyles.getElementAttributes(endSpan);
        if (character.character.fillColor && character.character.fillColor.type === 'auto') {
            //workaround for Bug 38656 (libre office cant show strike and underline when character fill color is set)
            delete character.character.fillColor;
        }
        var paragraph =  self.paragraphStyles.getElementAttributes(endSpan.parentElement);

        var baseAttributes = {
            character: character.character,
            paragraph: paragraph.paragraph,
            changes: { inserted: '', modified: '', removed: '' }
        };

        return baseAttributes;
    }

    function generateOverwriteStyleIdOp(generator, paraPos, paraNode, styleId) {

        var paraLength = getParagraphLength(paraNode, paraPos);
        if (paraLength) {
            var startChar = _.clone(paraPos);
            var endChar = _.clone(paraPos);
            startChar.push(0);
            endChar.push(paraLength - 1);

            generator.generateOperation(SET_ATTRIBUTES, {
                attrs: getCharClearAttributes(),
                start: _.copy(startChar),
                end:  _.copy(endChar)
            });
        }

        generator.generateOperation(SET_ATTRIBUTES, {
            attrs: getParaClearAttributes(),
            start:  _.copy(paraPos),
            end:  _.copy(paraPos)
        });

        generator.generateOperation(SET_ATTRIBUTES, {
            attrs: { styleId },
            start:  _.copy(paraPos),
            end:  _.copy(paraPos)
        });
    }

    function getEnglishNameByStyleName(styleName) {
        return reverseDict.translate(styleName);
    }

    function getTranslatedNameByStyleId(styleId) {
        return dictionary.translate(self.paragraphStyles.getName(styleId));
    }

    function getFuzzyStyleId(...args) {

        var names = self.paragraphStyles.getStyleSheetNames();
        var normalIds = _.keys(names);
        var normalNames = _.values(names);
        var trimmedIds = normalIds.map(trimAllWhitespace);
        var trimmedNames = normalNames.map(trimAllWhitespace);

        for (const arg of args) {
            var trimmedArg = trimAllWhitespace(arg);
            var index = trimmedIds.indexOf(trimmedArg);
            if (index >= 0) {
                return normalIds[index];
            }
            index = trimmedNames.indexOf(trimmedArg);
            if (index >= 0) {
                return normalIds[index];
            }
        }

        return undefined;
    }

    function resolveStyleName(styleName) {
        const engNewName = getEnglishNameByStyleName(styleName);
        const styleId = getFuzzyStyleId(styleName, engNewName);
        return styleId && getTranslatedNameByStyleId(styleId);
    }

    // methods ------------------------------------------------------------

    this.showInsertNewStyleDialog = function () {

        // find an unused name for the new paragraph style
        var initialName = null;
        var index = 1;
        do {
            //#. default name of a style sheet with formatting attributes for a text paragraph
            //#. "%1$d" is a serial number to be able to generate a new unused name for the style
            initialName = gt('Style %1$d', index);
            index += 1;
        } while (self.paragraphStyles.containsStyleSheetByName(initialName));

        // formatting attributes from the current selection
        var baseAttributes = getCurrentParaAttributes();

        // create the paragraph node for the example text
        var samplePara = $('<div class="preview-box">');
        setExplicitAttributeSet(samplePara, { paragraph: baseAttributes.paragraph });
        self.paragraphStyles.updateElementFormatting(samplePara, { baseAttributes });

        // create the span node for the example text
        var sampleSpan = $('<span class="preview-span">');
        setExplicitAttributeSet(sampleSpan, { paragraph: baseAttributes.character });
        self.characterStyles.updateElementFormatting(sampleSpan, { baseAttributes });

        // get the sample text from the selection
        var selection = self.getSelection();
        var activeRootNode = selection.getRootNode();
        var startPos = selection.getStartPosition();
        var paraPos = _.initial(startPos);
        var sampleText = $(getDOMPosition(activeRootNode, paraPos).node).text();
        sampleSpan.text((sampleText.length > 3) ? sampleText : initialName);

        // action handler for the OK button
        function actionHandler(styleName) {

            // OT: Taking care of unique IDs for insertStyleSheet operations
            var clientUid = '_' + app.getShortClientId();
            var styleId = styleName + clientUid;
            var selection = self.getSelection();

            var startPos = selection.getStartPosition();
            var paraPos = _.initial(startPos);
            var paraNode = self.getCurrentRootNode();
            var localGenerator = self.createOperationGenerator();

            self.getUndoManager().enterUndoGroup(function () {
                self.doCheckImplicitParagraph(startPos);

                localGenerator.generateOperation(INSERT_STYLESHEET, {
                    attrs: baseAttributes,
                    type: 'paragraph',
                    styleId,
                    styleName,
                    custom: true,
                    uiPriority: 0
                });

                generateOverwriteStyleIdOp(localGenerator, paraPos, paraNode, styleId);
                self.applyOperations(localGenerator);
            });
        }

        // create the dialog
        var dialog = new ParagraphStyleDialog({
            title: gt('Create paragraph style'),
            styleName: initialName,
            styleResolver: resolveStyleName,
            actionHandler
        });

        // append the DOM elements for the sample text
        dialog.$bodyNode.append(samplePara.append(sampleSpan));

        return dialog.show();
    };

    this.showRenameStyleDialog = function (styleId, fieldName) {

        var styleName = self.paragraphStyles.getName(styleId);
        var styleLabel = dictionary.translate(styleName);

        // action handler for the OK button
        function actionHandler(newName) {

            // OT: style sheet may have been deleted by remote editor
            if (!self.paragraphStyles.containsStyleSheet(styleId)) { return; }

            var localGenerator = self.createOperationGenerator();
            localGenerator.generateOperation(CHANGE_STYLESHEET, {
                type: 'paragraph',
                styleId,
                styleName: newName,
                uiPriority: self.paragraphStyles.getUIPriority(styleId),
                hidden: self.paragraphStyles.isHidden(styleId),
                custom: self.paragraphStyles.isCustom(styleId)
            });
            self.applyOperations(localGenerator);
        }

        var dialog = new ParagraphStyleDialog({
            title: gt('Rename paragraph style "%1$s"', styleLabel),
            styleName: fieldName || styleLabel,
            styleResolver(newName) {
                // the own style name is always valid (regardless of character case etc.)
                if ((newName === styleName) || (newName === styleLabel)) { return undefined; }
                return resolveStyleName(newName);
            },
            actionHandler
        });

        // OT: immediately close the dialog when remote editor deletes the style sheet
        dialog.listenTo(self.paragraphStyles, 'delete:stylesheet', function (delStyleId) {
            if (styleId === delStyleId) { dialog.close(); }
        });

        return dialog.show();
    };

    this.changeStylesheet = function (styleId) {
        var baseAttributes = getCurrentParaAttributes();
        var styleName = self.paragraphStyles.getName(styleId);

        var selection = self.getSelection();
        var startPos = selection.getStartPosition();
        var paraPos = _.initial(startPos);
        var paraNode = self.getCurrentRootNode();
        var localGenerator = self.createOperationGenerator();

        self.getUndoManager().enterUndoGroup(function () {

            self.doCheckImplicitParagraph(startPos);

            // register pending style sheet via 'insertStyleSheet' operation before modifying it
            if (self.paragraphStyles.isDirty(styleId)) {
                self.generateInsertStyleOp(localGenerator, 'paragraph', styleId);
            }

            localGenerator.generateOperation(CHANGE_STYLESHEET, {
                attrs: baseAttributes,
                type: 'paragraph',
                styleId,
                styleName,
                uiPriority: self.paragraphStyles.getUIPriority(styleId)
            });

            generateOverwriteStyleIdOp(localGenerator, paraPos, paraNode, styleId);

            self.applyOperations(localGenerator);

        });
    };

    this.deleteStylesheet = function (styleId) {
        var localGenerator = self.createOperationGenerator();
        var defaultParaStyle = self.paragraphStyles.getDefaultStyleId();
        var defaultAtts = { styleId: defaultParaStyle };
        var allP = self.getNode().find('.p').not(MARGINAL_NODE_SELECTOR);
        var marginalP = self.getPageLayout().getHeaderFooterPlaceHolder().find('.p');
        var emptyAttr = {};

        _.each(allP, function (para) {
            para = $(para);
            if (isImplicitParagraphNode(para)) {
                return;
            }
            var data = para.data();
            var attrs = getOption(data, 'attributes', emptyAttr);
            var paraStyleId = getStringOption(attrs, 'styleId', defaultParaStyle);
            if (self.paragraphStyles.isChildOfOther(paraStyleId, styleId)) {
                var paraPos = getOxoPosition(self.getNode(), para);

                localGenerator.generateOperation(SET_ATTRIBUTES, {
                    attrs: defaultAtts,
                    start:  _.copy(paraPos),
                    end:  _.copy(paraPos)
                });
            }
        });

        _.each(marginalP, function (para) {
            para = $(para);
            if (isImplicitParagraphNode(para)) {
                return;
            }
            var data = para.data();
            var attrs = getOption(data, 'attributes', emptyAttr);
            var paraStyleId = getStringOption(attrs, 'styleId', defaultParaStyle);
            var rootNode = getClosestMarginalTargetNode(para);
            if (self.paragraphStyles.isChildOfOther(paraStyleId, styleId) && rootNode.length) {
                var paraPos = getOxoPosition(rootNode, para);

                localGenerator.generateOperation(SET_ATTRIBUTES, {
                    attrs: defaultAtts,
                    start:  _.copy(paraPos),
                    end:  _.copy(paraPos),
                    target: getTargetContainerId(rootNode)
                });
            }
        });

        localGenerator.generateOperation(DELETE_STYLESHEET, {
            type: 'paragraph',
            styleId
        });

        self.applyOperations(localGenerator);
    };

    this.getTranslatedNameByStyleId = getTranslatedNameByStyleId;

    // initialization -----------------------------------------------------

    dictionary = app.resourceManager.getTranslations('paragraphstylenames');

    var reverseMap = {};
    dictionary.forEach(function (translation, key) {
        var revKey = translation.replace('$1', '(\\d+)');
        var revTranslation = key.replace('(\\d+)', '$1');
        reverseMap[revKey] = revTranslation;
        reverseMap[trimAllWhitespace(revKey)] = trimAllWhitespace(revTranslation);
    });
    reverseDict = new TranslationDictionary(reverseMap);

    this.listenTo(self.paragraphStyles, 'change:stylesheet', function (styleId) {
        app.getView().enterBusy({
            warningLabel: gt('Please wait while updating document.')
        });
        var emptyAttr = {};
        var defaultParaStyle = self.paragraphStyles.getDefaultStyleId();
        var allP = self.getNode().find('.p');

        self.iterateArraySliced(allP, function (para) {
            para = $(para);
            var data = para.data();
            var attrs = getOption(data, 'attributes', emptyAttr);
            var paraStyleId = getStringOption(attrs, 'styleId', defaultParaStyle);
            if (self.paragraphStyles.isChildOfOther(paraStyleId, styleId)) {
                self.paragraphStyles.updateElementFormatting(para);
            }
        }).always(function () {
            app.getView().leaveBusy();
            self.insertPageBreaks();
        });
    });
}
