/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { convertHmmToLength, iterateSelectedDescendantNodes } from '@/io.ox/office/tk/utils';
import { NONE } from '@/io.ox/office/editframework/utils/border';
import { StyleCollection } from '@/io.ox/office/editframework/model/stylecollection';
import { setCellBorderAttributes } from '@/io.ox/office/textframework/components/table/table';
import { MERGED_CELL_CLASS, PARAGRAPH_NODE_SELECTOR, TABLE_NODE_SELECTOR, getCellContentNode } from '@/io.ox/office/textframework/utils/dom';
import { Color } from '@/io.ox/office/editframework/utils/color';

// constants ==============================================================

// definitions for table cell attributes
const DEFINITIONS = {

    /**
     * The number of grid columns spanned by the table cell.
     */
    gridSpan: {
        def: 1,
        scope: 'element'
    },

    /**
     * Fill color of the table cell.
     */
    fillColor: {
        def: Color.AUTO
    },

    /**
     * Style, width and color of the left table cell border.
     */
    borderLeft: {
        def: NONE
    },

    /**
     * Style, width and color of the right table cell border.
     */
    borderRight: {
        def: NONE
    },

    /**
     * Style, width and color of the top table cell border.
     */
    borderTop: {
        def: NONE
    },

    /**
     * Style, width and color of the bottom table cell border.
     */
    borderBottom: {
        def: NONE
    },

    /**
     * Inner horizontal table cell borders, used in table style sheets
     * to format inner borders of specific table areas (first/last
     * column, inner vertical bands, ...).
     */
    borderInsideHor: {
        def: NONE,
        scope: 'style'
    },

    /**
     * Inner vertical table cell borders, used in table style sheets to
     * format inner borders of specific table areas (first/last row,
     * inner horizontal bands, ...).
     */
    borderInsideVert: {
        def: NONE,
        scope: 'style'
    },

    /**
     * Top padding of the table cell
     */
    paddingTop: {
        def: 0
    },

    /**
     * Bottom padding of the table cell
     */
    paddingBottom: {
        def: 0
    },

    /**
     * Left padding of the table cell
     */
    paddingLeft: {
        def: 0
    },

    /**
     * Right padding of the table cell
     */
    paddingRight: {
        def: 0
    },

    /**
     * Vertical align of the content of the table cell.
     */
    alignVert: {
        def: 'top'
    },

    /**
     * Vertical merge of table cells.
     */
    mergeVert: {
        def: 'noMerge'
    }
};

// parent families with parent element resolver functions
const PARENT_RESOLVERS = {
    table: cell => { return cell.closest(TABLE_NODE_SELECTOR); }
};

// private functions ======================================================

/**
 * Converts the passed padding from 1/100 of millimeters to pixels.
 *
 * @param {Number} padding
 *  The padding, in 1/100 of millimeters.
 *
 * @returns {Number}
 *  The padding, in pixels.
 */
function getPadding(padding) {
    // minimum 2px, required for cell resizing
    return Math.max(2, convertHmmToLength(padding, 'px', 1));
}

// class TableCellStyles ==================================================

/**
 * Contains the style sheets for table cell formatting attributes. The CSS
 * formatting will be read from and written to `<th>` and `<td>` elements.
 *
 * @param {TextModel} docModel
 *  The text document model containing instance.
 */
class TableCellStyles extends StyleCollection {

    constructor(docModel) {

        // base constructor
        super(docModel, 'cell', {
            styleSheetSupport: false,
            families: ['paragraph', 'character', 'changes'],
            parentResolvers: PARENT_RESOLVERS
        });

        // initialization -----------------------------------------------------

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily('cell', DEFINITIONS);

        // register the formatting handlers for DOM elements
        this.registerFormatHandler(this.#updateTableCellFormatting);
        this.registerPreviewHandler(this.#updatePreviewFormatting);
    }

    // private methods ----------------------------------------------------

    /**
     * Will be called for every table cell for table preview generation.
     * Formats the table cell according to the passed attributes.
     *
     * @param {jQuery} cell
     *  The table cell element whose table attributes need to be formatted, as
     *  jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute value maps (name/value pairs), keyed by
     *  attribute family, containing the effective attribute values merged
     *  from style sheets and explicit attributes.
     *
     * @param {Object} [options]
     *  Optional parameters passed to getCssBorder().
     */
    #updateGenericFormatting(cell, mergedAttributes, options) {

        // the cell attributes
        const cellAttrs = mergedAttributes.cell;

        cell.children('.cell').attr({
            verticalalign: cellAttrs.alignVert
        });

        // collecting odt border data of each cell
        if (options && options.cellBorderCollector) { setCellBorderAttributes(cell, cellAttrs, options.cellBorderCollector); }

        cell.css({
            backgroundColor: this.docModel.getCssColor(cellAttrs.fillColor, 'fill'),
            borderLeft: this.docModel.getCssBorder(cellAttrs.borderLeft, null, options),
            borderRight: this.docModel.getCssBorder(cellAttrs.borderRight, null, options),
            borderTop: this.docModel.getCssBorder(cellAttrs.borderTop, null, options),
            borderBottom: this.docModel.getCssBorder(cellAttrs.borderBottom, null, options)
        });
    }

    /**
     * Handling vertically merged cells, so that they are not visible to the user.
     *
     * @param {jQuery} cell
     *  The table cell element whose table attributes need to be formatted, as
     *  jQuery object.
     *
     * @param {Object} cellAttrs
     *  The 'cell' family attributes of the merged cells.
     */
    #handleVerticallyMergedTableCells(cell, cellAttrs) {

        const cellContentNode = getCellContentNode(cell);
        const mergeVert = cellAttrs.mergeVert;

        if (mergeVert === 'continue') {
            cellContentNode.attr('contenteditable', false);
            cellContentNode.css('visibility', 'hidden');
        // } else {
        //     // TODO: This needs to be updated, if client can modify value of 'mergeVert' property.
        //              This is for example the case after deleting or inserting new rows.
        //     cellContentNode.attr('contenteditable', true);
        //     cellContentNode.css('visibility', '');
        }

        cell.attr('mergeVert', mergeVert); // setting marker attribute at cell
    }

    /**
     * Will be called for every table cell element whose attributes have
     * been changed. Reformats the table cell according to the passed
     * attributes.
     *
     * @param {jQuery} cell
     *  The table cell element whose table attributes have been changed, as
     *  jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute value maps (name/value pairs), keyed by
     *  attribute family, containing the effective attribute values merged
     *  from style sheets and explicit attributes.
     *
     * @param {Boolean} [async=false]
     *  Whether the formatting is done asynchronously.
     *
     * @param {Array} [cellBorderCollector=undefined]
     *  An optional collector for data that can be collected in the
     *  formatting process. These data can be used after formatting
     *  for further settings (performance).
     *  This is for example used to collect cell border attributes
     *  during table formatting (DOCS-2400).
     */
    #updateTableCellFormatting(cell, mergedAttributes, async, cellBorderCollector) {

        // the cell attributes
        const cellAttrs = mergedAttributes.cell;

        // Hiding vertically merged cells to avoid impression of data loss (56291)
        this.#handleVerticallyMergedTableCells(cell, cellAttrs);

        // horizontal span size
        cell.attr('colspan', cellAttrs.gridSpan);
        cell.toggleClass(MERGED_CELL_CLASS, cellAttrs.gridSpan > 1);

        // update CSS attributes
        this.#updateGenericFormatting(cell, mergedAttributes, { cellBorderCollector });
        cell.css({
            paddingTop: getPadding(cellAttrs.paddingTop),
            paddingBottom: getPadding(cellAttrs.paddingBottom),
            paddingLeft: getPadding(cellAttrs.paddingLeft),
            paddingRight: getPadding(cellAttrs.paddingRight)
        });

        // Center cell content vertically. This solution is not dynamic
        // -> changing row height or inserting text destroys center position.
        // const cellContentNode = getCellContentNode(cell);
        // cellContentNode.css('padding-top', cell.height() / 2 - cellContentNode.height() / 2);

        // update all paragraphs in the table cell
        iterateSelectedDescendantNodes(getCellContentNode(cell), PARAGRAPH_NODE_SELECTOR, paragraph => {
            this.docModel.paragraphStyles.updateElementFormatting(paragraph, { baseAttributes: mergedAttributes });
        }, undefined, { children: true });

        // change track attribute handling
        this.docModel.getChangeTrack().updateChangeTrackAttributes(cell, mergedAttributes);
    }

    /**
     * Will be called for table cells used as preview elements in the GUI.
     *
     * @param {jQuery} cell
     *  The preview table cell node, as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A complete attribute set, containing the effective attribute values
     *  merged from style sheets and explicit attributes.
     */
    #updatePreviewFormatting(cell, mergedAttributes) {
        this.#updateGenericFormatting(cell, mergedAttributes, { preview: true });
    }

}

// exports ================================================================

export default TableCellStyles;
