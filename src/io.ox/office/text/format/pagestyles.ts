/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { convertHmmToCssLength } from "@/io.ox/office/tk/dom";
import { StyleCollection } from "@/io.ox/office/editframework/model/stylecollection";

import type { DocAttributePoolMap } from "@/io.ox/office/text/model/docmodel";
import type TextModel from "@/io.ox/office/text/model/docmodel";

// types ======================================================================

export interface PageAttributes {

    /**
     * Total width of a single page, in 1/100 of millimeters.
     */
    width: number;

    /**
     * Total height of a single page, in 1/100 of millimeters.
     */
    height: number;

    /**
     * the actual paper format (portrait or landscape)
     */
    orientation: string;

    /**
     * Margin between left page border and editing area, in 1/100 of
     * millimeters.
     */
    marginLeft: number;

    /**
     * Margin between right page border and editing area, in 1/100 of
     * millimeters.
     */
    marginRight: number;

    /**
     * Margin between top page border and editing area, in 1/100 of
     * millimeters.
     */
    marginTop: number;

    /**
     * Margin between bottom page border and editing area, in 1/100 of
     * millimeters.
     */
    marginBottom: number;

    /**
     * Margin between top page border and top header border, in 1/100 of millimeters
     */
    marginHeader: number;

    /**
     * Margin between bottom page border and bottom footer border, in 1/100 of millimeters
     */
    marginFooter: number;

    /**
     * Whether to use separate header/footer settings for the first page.
     *
     */
    firstPage: boolean;

    /**
     * Whether to use separate header/footer settings for even and odd pages, and mirror margin settings accordingly.
     */
    evenOddPages: boolean;
}

export interface PageAttributeSet {
    page: PageAttributes;
}

// constants ==================================================================

// definitions for page attributes
const PAGE_ATTRIBUTES = {
    width: { def: 21000 },
    height: { def: 29700 },
    orientation: { def: "portrait" },
    marginLeft: { def: 1500 },
    marginRight: { def: 1500 },
    marginTop: { def: 1500 },
    marginBottom: { def: 1500 },
    marginHeader: { def: 0 },
    marginFooter: { def: 0 },
    firstPage: { def: false },
    evenOddPages: { def: false }
};

// class PageStyles ===========================================================

/**
 * Contains the style sheets for page formatting attributes. The CSS
 * formatting will be read from and written to the page container elements.
 *
 * @param docModel
 *  The text document model containing instance.
 */
export default class PageStyleCollection extends StyleCollection<TextModel, DocAttributePoolMap, PageAttributeSet> {

    constructor(docModel: TextModel) {

        // base constructor
        super(docModel, "page");

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily("page", PAGE_ATTRIBUTES);

        // register the formatting handler for DOM elements
        this.registerFormatHandler(this.#updatePageFormatting);
    }

    /**
     * Will be called for every page whose attributes have been changed.
     *
     * @param page
     *  The page container element whose character attributes have been
     *  changed, as jQuery object.
     *
     * @param mergedAttrs
     *  A map of attribute maps (name/value pairs), keyed by attribute family,
     *  containing the effective attribute values merged from style sheets and
     *  explicit attributes.
     */
    #updatePageFormatting(page: JQuery, mergedAttrs: PageAttributeSet): void {

        // the page attributes of the passed attribute map
        const pageAttrs = mergedAttrs.page;
        // effective page width (at least 2cm)
        const pageWidth = Math.max(pageAttrs.width, 2000);
        // effective page height (at least 2cm)
        const pageHeight = Math.max(pageAttrs.height, 2000);
        // left page margin
        let leftMargin = Math.max(pageAttrs.marginLeft, 0);
        // right page margin
        let rightMargin = Math.max(pageAttrs.marginRight, 0);
        // total horizontal margin
        let horizontalMargin = leftMargin + rightMargin;
        // top page margin
        let topMargin = Math.max(pageAttrs.marginTop, 0);
        // bottom page margin
        let bottomMargin = Math.max(pageAttrs.marginBottom, 0);
        // total vertical margin
        let verticalMargin = topMargin + bottomMargin;

        // restrict left/right margin to keep an editing area of at least 1cm
        if ((horizontalMargin > 0) && (pageWidth - horizontalMargin < 1000)) {
            // Change margins according to ratio of original left/right margins
            // (e.g. keep left margin twice as big as right margin, if
            // specified in the original attribute values).
            leftMargin = (pageWidth - 1000) * (leftMargin / horizontalMargin);
            rightMargin = (pageWidth - 1000) * (rightMargin / horizontalMargin);
            horizontalMargin = leftMargin + rightMargin;
        }

        // restrict top/bottom margin to keep an editing area of at least 1cm
        if ((verticalMargin > 0) && (pageHeight - verticalMargin < 1000)) {
            // Change margins according to ratio of original top/bottom margins
            // (e.g. keep top margin twice as big as bottom margin, if
            // specified in the original attribute values).
            topMargin = (pageHeight - 1000) * (topMargin / verticalMargin);
            bottomMargin = (pageHeight - 1000) * (bottomMargin / verticalMargin);
            verticalMargin = topMargin + bottomMargin;
        }

        // Set CSS attributes. Page is in "content-size" mode to be able to
        // override padding on small devices without altering the size of the
        // editing area, thus width/height needs to be set without padding.
        page.css({
            width: convertHmmToCssLength(pageWidth - horizontalMargin, "mm", 0.1),
            // TODO: change to "height" when page layout is supported
            minHeight: convertHmmToCssLength(pageHeight - verticalMargin, "mm", 0.1),
            paddingLeft: convertHmmToCssLength(leftMargin, "mm", 0.1),
            paddingRight: convertHmmToCssLength(rightMargin, "mm", 0.1),
            paddingTop: convertHmmToCssLength(topMargin, "mm", 0.1),
            paddingBottom: convertHmmToCssLength(bottomMargin, "mm", 0.1)
        });
    }

}
