/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { dict } from '@/io.ox/office/tk/algorithms';
import { Canvas } from '@/io.ox/office/tk/canvas';
import { SMALL_DEVICE, convertHmmToCssLength, convertHmmToLength, iterateDescendantNodes } from '@/io.ox/office/tk/utils';

import { Color } from '@/io.ox/office/editframework/utils/color';
import { NO_BORDER, isVisibleBorder, equalBorders } from '@/io.ox/office/editframework/utils/border';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';

import { getDrawingNode, isTextFrameNode } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { LIST_LABEL_NODE_SELECTOR, isCellContentNode, isComplexFieldNode, isHardBreakNode, isIncreasableParagraph,
    isNodeInsideTextFrame, isParagraphNode, isRangeMarkerNode, iterateTextSpans } from '@/io.ox/office/textframework/utils/dom';
import ParagraphStyles from '@/io.ox/office/textframework/format/paragraphstyles';

// constants ==============================================================

// definitions for paragraph attributes
const DEFINITIONS = {

    nextStyleId: {
        def: '',
        scope: 'style'
    },

    listLevel: { def: -1 },

    listStyleId: { def: '' },

    listLabelHidden: { def: false },

    listStartValue: { def: -1 },

    outlineLevel: { def: 9 },

    borderLeft: { def: NO_BORDER },

    borderRight: { def: NO_BORDER },

    borderTop: { def: NO_BORDER },

    borderBottom: { def: NO_BORDER },

    borderInside: { def: NO_BORDER },

    contextualSpacing: { def: false },

    marginTop: { def: 0 },

    marginBottom: { def: 0 },

    pageBreakBefore: { def: false },

    pageBreakAfter: { def: false }
};

// parent families with parent element resolver functions
const PARENT_RESOLVERS = {
    drawing: paragraph => { return isTextFrameNode(paragraph.parent()) ? getDrawingNode(paragraph) : null; }, // (51103, text in shapes)
    cell: paragraph => { return isCellContentNode(paragraph.parent()) ? paragraph.closest('td') : null; }
};

const canvas = new Canvas({ _kind: "singleton" });

const defaultIndentFirstLineValue = 635;

// private static functions ===============================================

/**
 *  Checking if a paragraph has a outer border.
 *
 * @param {Object} paraAttr
 *  The paragraphs attributes.
 *
 * @returns {Boolean}
 *  Whether the paragraph has a border.
 */
function hasOuterBorder(paraAttr) {
    // note: borderInside must not be checked here
    return isVisibleBorder(paraAttr.borderTop) || isVisibleBorder(paraAttr.borderBottom) || isVisibleBorder(paraAttr.borderLeft) || isVisibleBorder(paraAttr.borderRight);
}

/**
 * Returning a padding related to a given 'border' and 'addSpace' value.
 * The result is returned in 'hmm' or in 'px'.
 *
 * @param {Object} border
 *  A border attribute object.
 *
 * @param {Number} addSpace
 *  A border attribute object.
 *
 * @param {Boolean} getValueInPx
 *  Wether the value should be returned in hmm or px.
 *
 * @returns {Number|String}
 *  When 'getValueInPx' is false, it returns a Number in hmm,
 *  when true it returns a String with the value in px.
 */
function getBorderPadding(border, addSpace, getValueInPx) {
    const space = addSpace + ((border && border.space) ? border.space : 0);
    return getValueInPx ? convertHmmToCssLength(space, 'px', 1) : space;
}

/**
 *  Checking if two paragraphs have a merged border.
 *
 * @param {Object} attrs1
 *  The paragraphs attributes.
 *
 *  @param {Object} attrs2
 *  The paragraphs attributes.
 *
 * @returns {Boolean}
 *  Whether the two paragraph have a merged border.
 */
function isMergeBorders(attrs1, attrs2) {
    // note: borderInside must not be compared here
    return dict.equals(attrs1, attrs2, ['listStyleId', 'listLevel', 'indentLeft', 'indentRight', 'indentFirstLine']) &&
        dict.equals(attrs1, attrs2, ['borderLeft', 'borderRight', 'borderTop', 'borderBottom'], equalBorders);
}

/**
 * Check if the given paragraph has a color.
 *
 * @param {Object} paraAttr
 *  The paragraphs attributes.
 *
 * @returns {Boolean}
 *  Returns true if paragraph has a color.
 */
function hasColor(paraAttr) {
    return paraAttr.fillColor.type !== 'auto';
}

/**
 * Utility check if attributes have properties pageBreakBefore or pageBreakAfter
 * set to true.
 *
 * @param {Object} attributes
 *
 * @returns {Boolean}
 *
 */
function isSetPageBreakAttribute(attributes) {
    return attributes.pageBreakBefore === true || attributes.pageBreakAfter === true;
}

// class TextParagraphStyles ==============================================

/**
 * Contains the style sheets for paragraph formatting attributes. The CSS
 * formatting will be read from and written to DOM paragraph elements.
 *
 * @param {TextModel} docModel
 *  The text document model containing instance.
 */
class TextParagraphStyles extends ParagraphStyles {

    constructor(docModel) {

        // base constructor
        super(docModel, {
            families: ['character', 'changes'],
            parentResolvers: PARENT_RESOLVERS
        });

        // initialization -----------------------------------------------------

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily('paragraph', DEFINITIONS);

        // register the formatting handler for DOM elements
        this.registerFormatHandler(this.#updateParagraphFormatting);

        // is triggered after page-break calculation is finished
        this.listenTo(docModel, 'update:borderAndColor', this.#updateParagraphBorderAndColor);
    }

    // public methods -----------------------------------------------------

    /**
     * Making the function hasOuterBorder public.
     * (take a look at the private function for more details).
     */
    hasBorder(paraAttr) {
        return hasOuterBorder(paraAttr);
    }

    /**
     * Updates all passed paragraph nodes in case they have:
     * 1) a paragraph color
     * 2) or a border
     *
     * Otherwise nothing happens. So no unnecessary updates are triggered
     * for paragraphs that don't fall into the mentioned category.
     *
     * When this function is called while undoRedo is running, the nodes are added to a queue
     * and are updated after 'undoRedo' is finished. This is needed, because at undoRedo the
     * node may have old attributes, which would cause a visually wrong formatting.
     *
     * This method is intended to update paragraphs with color or a border in special cases,
     * such as paragraph merge, splitt, delete or insert.
     *
     * @param {jQuery|Array} nodes
     *  A jQuery node or an array with jQuery nodes.
     */
    updateParagraphBorderAndColorNodes(nodes) {

        _.each(nodes, node => {

            // make sure its a jQuery node
            const $node = $(node);

            if ($node.hasClass('borderOrColor') && $node && $node.length > 0 && isParagraphNode($node)) {
                // add to queue while undoRedo is running, else update the paragraph normal
                if (this.docModel.isUndoRedoRunning()) {
                    this.docModel.addParagraphToStore($node);
                } else {
                    this.docModel.implParagraphChanged($node);
                }
            }
        });
    }

    // private methods ----------------------------------------------------

    /**
     * Will be called for every paragraph whose character attributes have
     * been changed.
     *
     * @param {jQuery} paragraph
     *  The paragraph node whose attributes have been changed, as jQuery
     *  object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute
     *  family, containing the effective attribute values merged from style
     *  sheets and explicit attributes.
     */
    #updateParagraphFormatting(paragraph, mergedAttributes) {

        // the paragraph attributes of the passed attribute map
        const paragraphAttributes = mergedAttributes.paragraph;

        let leftMargin = 0;
        let rightMargin = 0;

        let leftMarginTmp = 0;
        let rightMarginTmp = 0;

        const prevParagraph = paragraph.prev();
        const prevAttributes = (prevParagraph.length > 0) ? this.getElementAttributes(prevParagraph) : { paragraph: {} };

        const nextParagraph = paragraph.next();
        const nextAttributes = (nextParagraph.length > 0) ? this.getElementAttributes(nextParagraph) : { paragraph: {} };
        const collectedAttrs =  {};
        const prevCollectedAttrs =  {};
        let textIndent = 0;
        let explicitParaAttributes = null;
        let leftPadding;
        // whether the paragraph is inside a text frame
        const isParagraphInsideTextFrame = isNodeInsideTextFrame(paragraph);

        /**
         * Formatting the border and paragraph color for the current paragraph.
         * This function is just used wrap the related code in this function.
         *
         */
        const formatParagraphMarginAndBorder = () => {

            // flags to indicate if it's allowed to draw a border for the current
            // paragraph, the value is based on the state from the prev/next paragraph
            // (e.g. current paragraph is merged with the next paragraph),
            // important: this is not related to the actual border attributes, but only to the context
            let drawBorderTop = false;
            let drawBorderBottom = false;
            let drawBorderMarginUp = false;
            let drawBorderMarginDown = false;
            let drawInnerBorderBottom = false;

            // flags to indicate if a the paragraph color should be drawn
            // in the margin
            let drawColorMarginUp = false;
            let drawColorMarginDown = false;

            // objects containing all needed informations for the setting borders
            let borderBottomObj = {};
            let borderTopObj = {};
            let borderRightObj = {};
            let borderLeftObj = {};

            // resulting margin height
            let resultingMarginHeight;

            /**
             * Creates a canvas rectangle for the border-image. It has a 20px border
             * at each side (matching the border attributes color) and a fill color inside.
             *
             * @param {Object} borderTopObj
             *  Object containing needed information to set the top border.
             *
             * @param {Object} borderBottomObj
             *  Object containing needed information to set the bottom border.
             *
             * @param {Object} borderLeftObj
             *  Object containing needed information to set the left border.
             *
             * @param {Object} borderRightObj
             *  Object containing needed information to set the right border.
             *
             * @returns {Boolean}
             *  Returns the canvas as a base64 encoded image.
             */
            const createBase64Canvas = (borderTopObj, borderBottomObj, borderLeftObj, borderRightObj) => {

                // initialize the canvas with the passed bitmap size (implicitly clears the canvas)
                canvas.initialize({ width: 60, height: 60 });

                return canvas.renderToDataURL(context => {

                    // Important for IE11: The canvas border painted 20px wide to prevent browser zoom/css scale errors
                    // in IE11 when scaling. When the border width is higher than the 'sliced border' (see 'borderImageSlice'),
                    // it stretches the 'sliced border', the results is a blurred border with 'stretch' or small blurred lines
                    // in the 'fill' color with 'repeat'. With 20px there is enough headroom to prevent this.

                    let borderColorLeft = borderLeftObj.color;
                    let borderColorRight = borderRightObj.color;
                    let borderColorBottom = borderBottomObj.color;
                    let borderColorTop = borderTopObj.color;

                    // DOCS-4751 Firefox renders border image wrong, if there are no borders -> using the fill color as line color
                    if (_.browser.Firefox &&
                        borderLeftObj?.cssBorderStyle === "none" &&
                        borderRightObj?.cssBorderStyle === "none" &&
                        borderBottomObj?.cssBorderStyle === "none" &&
                        borderTopObj?.cssBorderStyle === "none"
                    ) {
                        borderColorLeft = borderColorRight = borderColorBottom = borderColorTop = this.docModel.getCssColor(paragraphAttributes.fillColor);
                    }

                    // left
                    context.setLineStyle({ style: borderColorLeft, width: 40 });
                    context.drawLine(0, 0, 0, 60);

                    // right
                    context.setLineStyle({ style: borderColorRight, width: 40 });
                    context.drawLine(60, 0, 60, 60);

                    // bottom
                    context.setLineStyle({ style: borderColorBottom, width: 40 });
                    context.drawLine(0, 60, 60, 60);

                    // top
                    context.setLineStyle({ style: borderColorTop, width: 40 });
                    context.drawLine(0, 0, 60, 0);

                    // fill color
                    context.setFillStyle(this.docModel.getCssColor(paragraphAttributes.fillColor, 'fill'));
                    context.drawRect(20, 20, 20, 20, 'fill');
                });
            };

            /**
             * Calculates the resulting, visible margin top and down for the current paragraph. It's needed
             * for drawing borders and paragraph color.
             *
             * Note: When a paragraph has a top margin and the previous has a bottom margin,
             * both margins collapse.
             *
             * @param {Boolean} drawMarginUp
             *  Whether the margin should be drawn upwards
             *  (value is based on the state from the prev/next paragraph ).
             *
             * @param {Boolean} drawMarginDown
             *  Whether the margin should be drawn downwards
             *  (value is based on the state from the prev/next paragraph ).
             *
             * @param {Number} prevMarginBottom
             *  Related to the current paragraph, the previous paragraphs margin bottom.
             *
             * @param {Number} marginTop
             *  The margin bottom from a paragraph.
             *
             * @param {Number} marginBottom
             *  The margin bottom from a paragraph.
             *
             * @returns {Object}
             *  Returns an object with the resulting margin up and downwards.
             */
            const calcResultingMarginHeight = (drawMarginUp, drawMarginDown, prevMarginBottom, marginTop, marginBottom) => {

                // convert hmm to px to prevent unpredictable rounding by the browser later
                const prevMarginBottomPx = convertHmmToLength(prevMarginBottom, 'px', 1);
                const marginTopPx = convertHmmToLength(marginTop, 'px', 1);
                const marginBottomPx = convertHmmToLength(marginBottom, 'px', 1);

                // calculates the actual values for the resulting, visible margin up and down
                const marginSpaceUp = drawMarginUp ? ((prevMarginBottomPx > marginTopPx) ? 0 : (Math.abs(prevMarginBottomPx - marginTopPx))) : 0;
                const marginSpaceDown = drawMarginDown ? marginBottomPx : 0;

                return { up: marginSpaceUp, down: marginSpaceDown };
            };

            /**
             * Calculates values for a single given border (e.g. left, right, top, or bottom).
             *  The resulting border is based on:
             *  1) paragraph attributes from the border
             *  2) the flag for this border, if it's allowed to draw it
             *
             * @param {Boolean} drawBorder
             *  If the border is allowed to be drawn or not.
             *  (value is based on the state from the prev/next paragraph, not on the current paragraphg attributes).
             *
             * @param {Object} borderAttr
             *  The attributes for a single border (e.g. left, right, top, or bottom).
             *
             *  @returns {Object}
             *  Returns an object with various values needed to draw the border later.
             */
            const resultingBorder = (drawBorder, borderAttr) => {

                // border attributes
                let borderAttributes;
                // the css borderStyle for the border, ready to use for setting the border style via css
                let cssBorderStyle = 'none';
                // the css BorderImageWidth for the border, a value of 1 makes the border visible, 0 not visible
                let cssBorderImageWidth = 0;
                // the color from the border
                const borderColor = this.docModel.getCssColor(borderAttr.color, 'line');
                // if the border is visible based on its border attributes
                let borderIsVisible = false;

                // when the border should be visible and is visible
                if (drawBorder && isVisibleBorder(borderAttr)) {

                    borderAttributes = this.docModel.getCssBorderAttributes(borderAttr);
                    cssBorderStyle = borderAttributes.width + 'px ' + borderAttributes.style + ' ' + borderAttributes.color;
                    cssBorderImageWidth = (borderAttributes.width > 0) ? 1 : 0;
                    borderIsVisible = true;
                }

                return { cssBorderStyle, cssBorderImageWidth, color: borderColor, borderIsVisible };
            };

            // CHECK: which borders needs to be drawn upwards
            // -> (if the paragraph has borders and merged borders with the previous paragraph)
            if ((hasOuterBorder(paragraphAttributes) && isMergeBorders(paragraphAttributes, prevAttributes.paragraph)) && !isSetPageBreakAttribute(paragraphAttributes)) {
                drawBorderTop = false;
                drawBorderMarginUp = true;
            } else {
                drawBorderTop = true;
                drawBorderMarginUp = false;
            }

            // CHECK: which borders needs to be drawn downwards
            // -> (if the paragraph has borders and merged borders with the next paragraph)
            if ((hasOuterBorder(paragraphAttributes) && isMergeBorders(paragraphAttributes, nextAttributes.paragraph)) && !isSetPageBreakAttribute(nextAttributes.paragraph)) {
                drawBorderBottom = false;
                drawBorderMarginDown = true;
                if (isVisibleBorder(paragraphAttributes.borderInside)) {
                    drawInnerBorderBottom = true;
                }
            } else {
                drawBorderBottom = true;
                drawBorderMarginDown = false;
            }

            // CHECK: if the paragraph margin needs to be colored downwards and if a inner border must be drawn
            if (((!hasOuterBorder(nextAttributes.paragraph) && !hasOuterBorder(paragraphAttributes))) && isMergeBorders(paragraphAttributes, nextAttributes.paragraph) && hasColor(paragraphAttributes) && hasColor(nextAttributes.paragraph)) {
                drawColorMarginDown = true;
                if (isVisibleBorder(paragraphAttributes.borderInside)) {
                    drawInnerBorderBottom = true;
                    drawBorderBottom = false;
                }
            }

            // CHECK: if the paragraph margin needs to be colored upwards
            if (((!hasOuterBorder(prevAttributes.paragraph) && !hasOuterBorder(paragraphAttributes))) && isMergeBorders(paragraphAttributes, prevAttributes.paragraph) && hasColor(paragraphAttributes) && hasColor(prevAttributes.paragraph)) {
                drawColorMarginUp = true;
            }

            // create resulting border attributes based on merged border flags and border attributes
            // when a flag due to merged borders is false, it overrides the border based on attributes
            borderBottomObj = resultingBorder(drawBorderBottom, paragraphAttributes.borderBottom);
            borderTopObj = resultingBorder(drawBorderTop, paragraphAttributes.borderTop);
            borderRightObj = resultingBorder(true, paragraphAttributes.borderRight);
            borderLeftObj = resultingBorder(true, paragraphAttributes.borderLeft);

            // padding
            const paddingTopPx = getBorderPadding(paragraphAttributes.borderTop, 0, true);
            const paddingRightPx = getBorderPadding(paragraphAttributes.borderRight, 0, true);
            let paddingBottomPx = getBorderPadding(paragraphAttributes.borderBottom, 0, true);
            const paddingLeftPx = getBorderPadding(paragraphAttributes.borderLeft, 0, true);

            // special case for bottom/inner border
            if (drawInnerBorderBottom && !drawBorderBottom) {
                borderBottomObj = resultingBorder(drawInnerBorderBottom, paragraphAttributes.borderInside);
                paddingBottomPx = getBorderPadding(paragraphAttributes.borderInside, 0, true);
            }

            // creating the resulting css values for the border-image
            const resultingCssBorderImageWidth = borderTopObj.cssBorderImageWidth + ' ' + borderRightObj.cssBorderImageWidth + ' ' + borderBottomObj.cssBorderImageWidth + ' ' + borderLeftObj.cssBorderImageWidth + ' ';

            // setting css
            _.extend(collectedAttrs, {
                // in some browsers (e.g. Chrome) a border style must be set to display border-images
                borderTop: borderTopObj.cssBorderStyle,
                borderRight: borderRightObj.cssBorderStyle,
                borderLeft: borderLeftObj.cssBorderStyle,
                borderBottom: borderBottomObj.cssBorderStyle,

                // When a border should not be visible, the borderImageWidth must be 0 (border-style: 'none' is ignored in most browsers for border images).
                // Important: Don't use px, just use numbers (numbers represents multiples of the corresponding border-width ). This scales much better with browser zoom.
                borderImageWidth: resultingCssBorderImageWidth,

                paddingTop: paddingTopPx,
                paddingRight: paddingRightPx,
                paddingBottom: paddingBottomPx,
                paddingLeft: paddingLeftPx // note: maybe overwritten later again in list formatting in 'this.#updateParagraphFormatting'

            });

            // add a class to the paragraph when it has a color or a border
            paragraph.toggleClass('borderOrColor', hasColor(paragraphAttributes) || hasOuterBorder(paragraphAttributes));

            // check if the paragraph needs to be formatted with margin color or border
            if (borderTopObj.borderIsVisible || borderBottomObj.borderIsVisible || borderRightObj.borderIsVisible || borderLeftObj.borderIsVisible || drawBorderMarginUp || drawColorMarginUp || drawColorMarginDown || drawBorderMarginDown) {

                // get the resulting, visible margin for this paragraph
                resultingMarginHeight = calcResultingMarginHeight(drawBorderMarginUp || drawColorMarginUp, drawBorderMarginDown || drawColorMarginDown, prevAttributes.paragraph.marginBottom, paragraphAttributes.marginTop, paragraphAttributes.marginBottom);

                // setting css
                _.extend(collectedAttrs, {
                    borderImageSource: 'url("' + createBase64Canvas(borderTopObj, borderBottomObj, borderLeftObj, borderRightObj) + '")',
                    // the used part of the canvas to draw the border,
                    // since the border in the source is 20px, it should be sliced with 20px, but the IE11 needs a special treated here.
                    borderImageSlice: (_.browser.IE === 11) ? '21 fill' : '20 fill',
                    // moving the border-image over the css-box-model, to 'paint' in the margin
                    borderImageOutset: resultingMarginHeight.up + 'px 0px ' + resultingMarginHeight.down + 'px 0px',
                    // Note: iOS and IE11: 'stretch must be use in these browsers to prevent display errors with 'repeat'.
                    // In other browsers 'repeat' can also be used (when there are display errors in a browser try 'repeat' here if it looks better)
                    borderImageRepeat: 'stretch',            //IOS_SAFARI_DEVICE ? 'stretch' : 'repeat',
                    // Needed to fix browser painting problems with chrome/safari, they just need something in the margin top/bottom to repaint this area
                    boxShadow:  (_.browser.Chrome || _.browser.Safari) ? '0px ' + resultingMarginHeight.down + 'px transparent ,  0px ' + (resultingMarginHeight.up * -1) + 'px transparent' : ''

                });

            // ... when border or paragraph color, set default values to override possible old css styles
            } else {

                _.extend(collectedAttrs, {
                    // clear a possible border image
                    borderImage: '',
                    // clear the boxShadow
                    boxShadow: ''
                });
            }
        };

        // handle page break settings
        if (paragraphAttributes.pageBreakBefore && !isParagraphInsideTextFrame) {
            if (paragraph.closest('tr').length > 0) {
                paragraph.parents('table').last().addClass('manual-page-break');
            }
            paragraph.addClass('manual-page-break');
        }
        if (paragraphAttributes.pageBreakAfter && !isParagraphInsideTextFrame) {
            if (paragraph.closest('tr').length > 0) {
                paragraph.parents('table').last().addClass('manual-pb-after');
            }
            paragraph.addClass('manual-pb-after');
        }

        // Always update character formatting of all child nodes which may
        // depend on paragraph settings, e.g. automatic text color which
        // depends on the paragraph fill color. Also visit all helper nodes
        // containing text spans, e.g. numbering labels.
        const { characterStyles } = this.docModel;
        iterateDescendantNodes(paragraph, node => {

            // visiting the span inside a hard break node
            // -> this is necessary for change tracking attributes
            if (isHardBreakNode(node)) {
                characterStyles.updateElementFormatting(node.firstChild);
            }
            if (isComplexFieldNode(node) || isRangeMarkerNode(node)) {
                characterStyles.updateElementFormatting(node);
            }

            iterateTextSpans(node, span => {
                if (this.docModel.hasOoxmlTextCharacterFilterSupport()) {
                    characterStyles.updateElementFormatting(span); // DOCS-4211, no character attributes at paragraph to span in OOXML
                } else {
                    characterStyles.updateElementFormatting(span, { baseAttributes: mergedAttributes });
                }
            });
        }, undefined, { children: true });

        // update border padding
        leftPadding = paragraphAttributes.borderLeft && paragraphAttributes.borderLeft.space ? paragraphAttributes.borderLeft.space : 0;
        leftMarginTmp = -getBorderPadding(paragraphAttributes.borderLeft, 0, false);
        rightMarginTmp = -getBorderPadding(paragraphAttributes.borderRight, 0, false);

        // Fix for BUG #36298:
        // only add the border-margins (left/right) if the paragraph isn't in a textframe
        // or if they were positive. Otherwise the left/right border will be hidden.
        if (!isParagraphInsideTextFrame || (leftMarginTmp > 0 && rightMarginTmp > 0)) {
            leftMargin += leftMarginTmp;
            rightMargin += rightMarginTmp;
        }

        let topMargin = paragraphAttributes.marginTop;
        let bottomMargin = paragraphAttributes.marginBottom;

        // calculate paragraph border and the paragraph color
        formatParagraphMarginAndBorder();

        //calculate list indents
        const listLabel = $(paragraph).children(LIST_LABEL_NODE_SELECTOR);
        const listStyleId = paragraphAttributes.listStyleId;
        let listObject = null;

        if (listStyleId.length) {
            let listLevel = paragraphAttributes.listLevel;
            const lists = this.docModel.getListCollection();

            if (listLevel < 0) {
                // is a numbering level assigned to the current paragraph style?
                listLevel = lists.findIlvl(listStyleId, mergedAttributes.styleId);
            }
            if (listLevel !== -1 && listLevel < 10) {
                const listItemCounter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                explicitParaAttributes = getExplicitAttributes(paragraph, 'paragraph');

                listObject = lists.formatNumber(paragraphAttributes.listStyleId, listLevel, listItemCounter, 1, mergedAttributes.styleId, explicitParaAttributes);

                if (listObject.indent < 0 && listObject.indentLeftFromLevelFormat) { // important for "Heading 1" in DOCS-3378 (e2e test "LIST-TX1")
                    leftMargin += listObject.indent;
                }

                if (listObject.indent > 0) {
                    leftPadding += listObject.indent - listObject.firstLine;
                    leftMargin += listObject.firstLine;
                }

                if (listLabel.length) {
                    const listSpan = listLabel.children('span');
                    if (listObject.fontSize) {
                        listSpan.css('font-size', listObject.fontSize + 'pt');
                    }
                    if (listObject.color) {
                        listSpan.css('color', this.docModel.getCssTextColor(listObject.color, [paragraphAttributes.fillColor, listObject.fillColor]));
                    }
                }
            } else {
                //fix for Bug 37594 some list-helper dont disappear
                listLabel.detach();
            }
        } else {
            //fix for Bug 37594 some list-helper dont disappear
            listLabel.detach();
        }

        // paragraph margin attributes - also applying to paragraphs in a list, if they are defined as explicit attribute
        // -> handle both cases correctly: 40792 and 41118
        if (listStyleId === '') {
            leftMargin += paragraphAttributes.indentLeft ? paragraphAttributes.indentLeft : 0;
            rightMargin += paragraphAttributes.indentRight ? paragraphAttributes.indentRight : 0;
            textIndent = paragraphAttributes.indentFirstLine ? paragraphAttributes.indentFirstLine : 0;
            collectedAttrs.textIndent = convertHmmToLength(textIndent, 'px', 1) + 'px';
        } else if (!listObject || !listObject.attributesHandled) { // -> TODO: Avoiding this "else" -> it is too late to handle explicit attributes at the paragraph
            const isODFListInTextFrame = isParagraphInsideTextFrame && this.docApp.isODF(); // special handling for 65054
            // but explicit attributes need to be handled (40792)
            explicitParaAttributes = explicitParaAttributes || getExplicitAttributes(paragraph, 'paragraph');
            if (_.isNumber(explicitParaAttributes.indentLeft) && !isODFListInTextFrame) { leftMargin += explicitParaAttributes.indentLeft; }
            if (_.isNumber(explicitParaAttributes.indentRight)) { rightMargin += explicitParaAttributes.indentRight; }
            if (_.isNumber(explicitParaAttributes.indentFirstLine) && (explicitParaAttributes.indentFirstLine !== 0 || _.isNumber(explicitParaAttributes.indentLeft))) {
                // first line indent, negative values represent hanging indents
                const lFl = explicitParaAttributes.indentFirstLine ||  0;
                // text indent is left indent minus special indent like first line or hanging
                const indentFirstLineValue = (this.docApp.isODF() && explicitParaAttributes.listLabelHidden) ? 0 : defaultIndentFirstLineValue; // (52818, DOCS-3128)
                textIndent = lFl + indentFirstLineValue;
                collectedAttrs.textIndent = convertHmmToLength(textIndent, 'px', 1) + 'px';
            }

            // explicit attribute for left indent needs to handle the indent of a list (47090)
            if (listObject && listObject.indent && explicitParaAttributes && _.isNumber(explicitParaAttributes.indentLeft) && !isODFListInTextFrame) {
                if (!(this.docApp.isODF() && explicitParaAttributes.listLabelHidden)) { // not for ODF with empty list label (52818)
                    leftMargin -= listObject.indent;
                }
            }
        }

        if (textIndent < 0) {
            leftPadding -= textIndent;
            leftMargin += textIndent;
        }

        _.extend(collectedAttrs, {
            paddingLeft: convertHmmToLength(leftPadding, 'px', 1) + 'px',
            // now set left & right margin: On small devices it must be set to 0 (when smaller than 0),
            // otherwise it may be not visible on small devices because of the draft mode
            marginLeft: (SMALL_DEVICE && leftMargin < 0) ? '0px' : convertHmmToLength(leftMargin, 'px', 1) + 'px',
            marginRight: (SMALL_DEVICE && leftMargin < 0) ? '0px' : convertHmmToLength(rightMargin, 'px', 1) + 'px',
            textIndent: convertHmmToLength(textIndent, 'px', 1) + 'px'

        });

        // Overwrite of margin left for lists in draft mode (conversion from fixed unit mm to %)
        if (SMALL_DEVICE && paragraph.data('draftRatio')) {
            collectedAttrs.marginLeft = (parseInt($(paragraph).css('margin-left'), 10) * paragraph.data('draftRatio')) + '%';
        }

        //no distance between paragraph using the same style if contextualSpacing is set
        const noDistanceToPrev = prevAttributes.paragraph.contextualSpacing && (mergedAttributes.styleId === prevAttributes.styleId);
        const noDistanceToNext = paragraphAttributes.contextualSpacing && (mergedAttributes.styleId === nextAttributes.styleId);

        if (noDistanceToPrev) {
            //remove bottom margin from previous paragraph
            prevCollectedAttrs.marginBottom = 0 + 'px';
            collectedAttrs.paddingTop = 0 + 'px';
            topMargin = 0;
        }
        if (noDistanceToNext) {
            collectedAttrs.paddingBottom = 0 + 'px';
            bottomMargin = 0;
        }
        _.extend(collectedAttrs, {
            marginTop: convertHmmToLength(topMargin, 'px', 1) + 'px',
            marginBottom: convertHmmToLength(bottomMargin, 'px', 1) + 'px'
        });

        // Important: The next and previous paragraphs are not available at document load.
        // As these both are mandatory for formatting paragraph border and color with margins,
        // add these paragraphs to a store for later formatting, when both are accessible.
        if (!this.isImportFinished()) {
            if (hasOuterBorder(paragraphAttributes) || hasColor(paragraphAttributes)) {
                this.docModel.addParagraphToStore(paragraph, mergedAttributes);
            }
        }

        // taking care of implicit paragraph nodes behind tables after loading the document
        // (also handling final empty paragraphs behind tables inside table cell (36537)).
        if (isIncreasableParagraph(paragraph) && (!this.isImportFinished() || !this.docModel.selectionInNode(paragraph))) {
            collectedAttrs.height = 0;
        }

        if (!_.browser.IE) {
            collectedAttrs.width = null;
        }

        // apply collected attributes at the end
        prevParagraph.css(prevCollectedAttrs);
        paragraph.css(collectedAttrs);

        // update the size of all tab stops in this paragraph
        this.implUpdateTabStops(paragraph, mergedAttributes);

        // change track attribute handling
        this.docModel.getChangeTrack().updateChangeTrackAttributes(paragraph, mergedAttributes);
    }

    /**
     * Updates all paragraph nodes that have a color or a border.
     * This method is intended to update all needed paragraphs after page-breaks are applied.
     */
    #updateParagraphBorderAndColor() {

        // must not be called at document loading, since at loading page-breaks
        // are applied before formatting borders/paragraph color
        if (this.isImportFinished()) {

            // find all paragraph nodes that have a color or a border
            // note: find seems to be very fast in all browsers and even in very large documents
            const nodes = this.docModel.getNode().find('.borderOrColor');

            if (nodes.length > 0) { this.docModel.implParagraphChanged(nodes); }
        }
    }
}

// constants --------------------------------------------------------------

/**
 * Default solid border line for paragraphs and tables.
 */
TextParagraphStyles.SINGLE_BORDER = { style: 'single', width: 17, space: 140, color: Color.AUTO };

// exports ================================================================

export default TextParagraphStyles;
