/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { fun } from "@/io.ox/office/tk/algorithms";

import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';

import { DELETE_LIST, INSERT_LIST } from '@/io.ox/office/textframework/utils/operations';
import { formatNumber as formatNumberImport } from '@/io.ox/office/textframework/utils/listutils';
import { findPreviousNode, getBooleanOption } from '@/io.ox/office/textframework/utils/textutils';
import { getParagraphElement } from '@/io.ox/office/textframework/utils/position';
import { PARAGRAPH_NODE_SELECTOR, isEmptyParagraph, getReplacementForBullet } from '@/io.ox/office/textframework/utils/dom';

// constants ==============================================================

// the default bullet list definition with different bullets per list level
const DEFAULT_BULLET_LIST_DEFINITION = {
    listLevel0: { textAlign: 'left', indentLeft: 1270,     numberFormat: 'bullet', listStartValue: 1, fontName: 'Symbol', levelText: '\uf0b7', indentFirstLine: -635 },
    listLevel1: { textAlign: 'left', indentLeft: 2 * 1270, numberFormat: 'bullet', listStartValue: 1, fontName: 'Times New Roman', levelText: '\u25CB', indentFirstLine: -635 },
    listLevel2: { textAlign: 'left', indentLeft: 3 * 1270, numberFormat: 'bullet', listStartValue: 1, fontName: 'Times New Roman', levelText: '\u25A0', indentFirstLine: -635 },
    listLevel3: { textAlign: 'left', indentLeft: 4 * 1270, numberFormat: 'bullet', listStartValue: 1, fontName: 'Symbol', levelText: '\uf0b7', indentFirstLine: -635 },
    listLevel4: { textAlign: 'left', indentLeft: 5 * 1270, numberFormat: 'bullet', listStartValue: 1, fontName: 'Times New Roman', levelText: '\u25CB', indentFirstLine: -635 },
    listLevel5: { textAlign: 'left', indentLeft: 6 * 1270, numberFormat: 'bullet', listStartValue: 1, fontName: 'Times New Roman', levelText: '\u25A0', indentFirstLine: -635 },
    listLevel6: { textAlign: 'left', indentLeft: 7 * 1270, numberFormat: 'bullet', listStartValue: 1, fontName: 'Symbol', levelText: '\uf0b7', indentFirstLine: -635 },
    listLevel7: { textAlign: 'left', indentLeft: 8 * 1270, numberFormat: 'bullet', listStartValue: 1, fontName: 'Times New Roman', levelText: '\u25CB', indentFirstLine: -635 },
    listLevel8: { textAlign: 'left', indentLeft: 9 * 1270, numberFormat: 'bullet', listStartValue: 1, fontName: 'Times New Roman', levelText: '\u25A0', indentFirstLine: -635 }
};

// the default numbered list definition with different number formats per list level (as used in MSO2010)
const DEFAULT_NUMBERING_LIST_DEFINITION = {
    listLevel0: { numberFormat: 'decimal',     listStartValue: 1, indentLeft: 1270,     indentFirstLine: -635, textAlign: 'left',  levelText: '%1.' },
    listLevel1: { numberFormat: 'lowerLetter', listStartValue: 1, indentLeft: 2 * 1270, indentFirstLine: -635, textAlign: 'left',  levelText: '%2.' },
    listLevel2: { numberFormat: 'upperLetter', listStartValue: 1, indentLeft: 3 * 1270, indentFirstLine: -635, textAlign: 'right', levelText: '%3.' },
    listLevel3: { numberFormat: 'lowerRoman',  listStartValue: 1, indentLeft: 4 * 1270, indentFirstLine: -635, textAlign: 'left',  levelText: '%4.' },
    listLevel4: { numberFormat: 'upperRoman',  listStartValue: 1, indentLeft: 5 * 1270, indentFirstLine: -635, textAlign: 'left',  levelText: '%5.' },
    listLevel5: { numberFormat: 'decimal',     listStartValue: 1, indentLeft: 6 * 1270, indentFirstLine: -635, textAlign: 'right', levelText: '%6.' },
    listLevel6: { numberFormat: 'lowerLetter', listStartValue: 1, indentLeft: 7 * 1270, indentFirstLine: -635, textAlign: 'left',  levelText: '%7.' },
    listLevel7: { numberFormat: 'upperLetter', listStartValue: 1, indentLeft: 8 * 1270, indentFirstLine: -635, textAlign: 'left',  levelText: '%8.' },
    listLevel8: { numberFormat: 'lowerRoman',  listStartValue: 1, indentLeft: 9 * 1270, indentFirstLine: -635, textAlign: 'right', levelText: '%9.' }
};

// Some paragraph styles, that can be ignored, if lists numbering needs to be continued.
// Example: 'Heading1' needs to continue 'Heading1', but 'Normal' must not continue a list of 'Heading1'.
//          On the other hand a paragraph without list ('Normal') that gets a list assigned, should continue
//          the numbering of a previous list paragraph ('ListParagraph').
const IGNORABLE_PARA_STYLES_IN_LISTS = {
    Normal: 1,
    Standard: 1,
    ListParagraph: 1
};

// all predefined list styles for the GUI controls, mapped by list identifier
const PREDEFINED_LISTSTYLES = fun.do(() => {

    /**
     * Creates and returns a style definition for a bullet list with
     * the same bullet character in all list levels.
     *
     * @param {string} numberFormat
     *
     * @param {String} listKey
     *  A unique key for the list style.
     *
     * @param {String} listLabel
     *  The button label for the list style used in GUI from controls.
     *
     * @param {Number} sortIndex
     *  The sorting index used in drop-down lists in GUI controls.
     *
     * @param {String} tooltip
     *  A tool tip for the list style used in GUI from controls.
     *
     * @param {Function} levelGenerator
     *  A callback function that generates a style definition for a single
     *  list level. Receives the list level in the first parameter.
     *
     * @returns {Object}
     *  An object with an attribute 'definition' and the string
     *  attributes 'listKey', 'listLabel', and 'tooltip'. The list
     *  definition object containing the attributes 'listLevel0' to
     *  'listLevel8' referring to list style definitions with an
     *  indentation according to the respective list level.
     */
    const createListStyle = (numberFormat, listKey, listLabel, sortIndex, tooltip, levelGenerator) => {
        const definition = {};
        _(9).times(level => {
            definition['listLevel' + level] = _.extend({
                numberFormat,
                listStartValue: 1
            }, levelGenerator(level));
        });
        return { definition, format: numberFormat, listKey, listLabel, sortIndex, tooltip };
    };

    /**
     * Creates and returns a style definition for a bullet list with
     * the same bullet character in all list levels.
     *
     * @param {String} bulletKey
     *  A unique key for the bullet list style.
     *
     * @param {String} bulletCharacter
     *  The bullet character for all list levels.
     *
     * @param {Number} sortIndex
     *  The sorting index used in drop-down lists in GUI controls.
     *
     * @param {String} tooltip
     *  A tool tip for the list style used in GUI from controls.
     *
     * @returns {Object}
     *  An object with an attribute 'definition' and the string
     *  attributes 'listLabel' and 'tooltip'. The list definition
     *  object containing the attributes 'listLevel0' to 'listLevel8'
     *  referring to list style definitions with an indentation
     *  according to the respective list level.
     */
    const createBulletListStyle = (bulletKey, bulletCharacter, sortIndex, tooltip) => {
        return createListStyle('bullet', bulletKey, bulletCharacter, sortIndex, tooltip, level => {
            return {
                indentLeft: (level + 1) * 1270,
                indentFirstLine: -635,
                textAlign: 'left',
                levelText: bulletCharacter,
                fontName: 'Times New Roman'
            };
        });
    };

    /**
     * Creates and returns a style definition for a numbered list with
     * the same numbering style in all list levels.
     *
     * @param {String} numberFormat
     *  One of the predefined number formats 'decimal', 'lowerLetter',
     *  'upperLetter', 'lowerRoman', or 'upperRoman'.
     *
     * @param {String} textBefore
     *  Fixed text inserted before each numeric list label.
     *
     * @param {String} textAfter
     *  Fixed text inserted after each numeric list label.
     *
     * @param {Number} sortIndex
     *  The sorting index used in drop-down lists in GUI controls.
     *
     * @param {String} tooltip
     *  A tool tip for the list style used in GUI from controls.
     *
     * @returns {Object}
     *  An object with an attribute 'definition' and the string
     *  attributes 'listLabel' and 'tooltip'. The list definition
     *  object containing the attributes 'listLevel0' to 'listLevel8'
     *  referring to list style definitions with an indentation
     *  according to the respective list level.
     */
    const createNumberedListStyle = (numberFormat, textBefore, textAfter, sortIndex, tooltip, indentLeftVal, indentFirstLineVal, textAlignVal) => {

        // default value for left indent in 1/100 mm
        const defaultIndentLeftValue = 1270;
        // default value for first line indent in 1/100 mm
        const defaultIndentFirstLineValue = -635;
        // default value for alignment of list numbers
        const defaultTextAlignValue = 'left';
        // value for left indent in 1/100 mm
        const indentLeftValue = indentLeftVal || defaultIndentLeftValue;
        // value for first line indent in 1/100 mm
        const indentFirstLineValue = indentFirstLineVal || defaultIndentFirstLineValue;
        // value for alignment of list numbers (only left supported yet)
        const textAlignValue = textAlignVal || defaultTextAlignValue;
        // unique key for the list style
        const listKey = textBefore + numberFormat + textAfter;
        // GUI label for the list style
        const listLabel = textBefore + formatNumberImport(1, numberFormat) + textAfter;

        return createListStyle(numberFormat, listKey, listLabel, sortIndex, tooltip, level => {
            return {
                indentLeft: (level + 1) * indentLeftValue,
                indentFirstLine: indentFirstLineValue,
                textAlign: textAlignValue,
                levelText: textBefore + '%' + (level + 1) + textAfter
            };
        });
    };

    // generate and return the array with all predefined list styles
    return {
        L20000: createBulletListStyle('filled-circle',       '\u25CF', 11, /*#. symbol in bullet lists (filled) */ gt('Filled circle')),
        L20001: createBulletListStyle('small-filled-circle', '\u2022', 12, /*#. symbol in bullet lists (filled) */ gt('Small filled circle')),
        L20002: createBulletListStyle('circle',              '\u25CB', 13, /*#. symbol in bullet lists (not filled) */ gt('Circle')),
        L20003: createBulletListStyle('small-circle',        '\u25E6', 14, /*#. symbol in bullet lists (not filled) */ gt('Small circle')),
        L20004: createBulletListStyle('filled-square',       '\u25A0', 21, /*#. symbol in bullet lists (filled) */ gt('Filled square')),
        L20005: createBulletListStyle('small-filled-square', '\u25AA', 22, /*#. symbol in bullet lists (filled) */ gt('Small filled square')),
        L20006: createBulletListStyle('square',              '\u25A1', 23, /*#. symbol in bullet lists (not filled) */ gt('Square')),
        L20007: createBulletListStyle('small-square',        '\u25AB', 24, /*#. symbol in bullet lists (not filled) */ gt('Small square')),
        L20008: createBulletListStyle('filled-diamond',      '\u2666', 31, /*#. symbol in bullet lists (filled) */ gt('Filled diamond')),
        L20009: createBulletListStyle('diamond',             '\u25CA', 32, /*#. symbol in bullet lists (not filled) */ gt('Diamond')),
        L20010: createBulletListStyle('filled-triangle',     '\u25BA', 41, /*#. symbol in bullet lists (filled) */ gt('Filled triangle')),
        L20011: createBulletListStyle('chevron',             '>',      42, /*#. symbol in bullet lists */ gt('Chevron')),
        L20012: createBulletListStyle('arrow',               '\u2192', 51, /*#. symbol in bullet lists */ gt('Arrow')),
        L20013: createBulletListStyle('dash',                '\u2013', 52, /*#. symbol in bullet lists */ gt('Dash')),
        L20014: createBulletListStyle('none',                ' ',      99, /*#. bullet lists without symbols */ gt('No symbol')),

        L30000: createNumberedListStyle('decimal',     '',  '',  11, /*#. label style in numbered lists */ gt('Decimal numbers')),
        L30001: createNumberedListStyle('decimal',     '',  '.', 12, /*#. label style in numbered lists */ gt('Decimal numbers')),
        L30002: createNumberedListStyle('decimal',     '',  ')', 13, /*#. label style in numbered lists */ gt('Decimal numbers')),
        L30003: createNumberedListStyle('decimal',     '(', ')', 14, /*#. label style in numbered lists */ gt('Decimal numbers')),
        L30010: createNumberedListStyle('lowerLetter', '',  '',  21, /*#. label style in numbered lists */ gt('Small Latin letters')),
        L30011: createNumberedListStyle('lowerLetter', '',  '.', 22, /*#. label style in numbered lists */ gt('Small Latin letters')),
        L30012: createNumberedListStyle('lowerLetter', '',  ')', 23, /*#. label style in numbered lists */ gt('Small Latin letters')),
        L30013: createNumberedListStyle('lowerLetter', '(', ')', 24, /*#. label style in numbered lists */ gt('Small Latin letters')),
        L30020: createNumberedListStyle('upperLetter', '',  '',  31, /*#. label style in numbered lists */ gt('Capital Latin letters')),
        L30021: createNumberedListStyle('upperLetter', '',  '.', 32, /*#. label style in numbered lists */ gt('Capital Latin letters')),
        L30022: createNumberedListStyle('upperLetter', '',  ')', 33, /*#. label style in numbered lists */ gt('Capital Latin letters')),
        L30023: createNumberedListStyle('upperLetter', '(', ')', 34, /*#. label style in numbered lists */ gt('Capital Latin letters')),
        L30030: createNumberedListStyle('lowerRoman',  '',  '',  41, /*#. label style in numbered lists */ gt('Small Roman numbers')),
        L30031: createNumberedListStyle('lowerRoman',  '',  '.', 42, /*#. label style in numbered lists */ gt('Small Roman numbers')),
        L30032: createNumberedListStyle('lowerRoman',  '',  ')', 43, /*#. label style in numbered lists */ gt('Small Roman numbers')),
        L30033: createNumberedListStyle('lowerRoman',  '(', ')', 44, /*#. label style in numbered lists */ gt('Small Roman numbers')),
        L30040: createNumberedListStyle('upperRoman',  '',  '',  51, /*#. label style in numbered lists */ gt('Capital Roman numbers')),
        L30041: createNumberedListStyle('upperRoman',  '',  '.', 52, /*#. label style in numbered lists */ gt('Capital Roman numbers')),
        L30042: createNumberedListStyle('upperRoman',  '',  ')', 53, /*#. label style in numbered lists */ gt('Capital Roman numbers')),
        L30043: createNumberedListStyle('upperRoman',  '(', ')', 54, /*#. label style in numbered lists */ gt('Capital Roman numbers'), 1905, -1270)
    };

}); // end of PREDEFINED_LISTSTYLES local scope

// static private function ================================================

function isLevelEqual(defaultLevel, compareLevel) {
    const ret = defaultLevel !== undefined && compareLevel !== undefined &&
        defaultLevel.numberFormat === compareLevel.numberFormat &&
        defaultLevel.indentLeft === compareLevel.indentLeft &&
        defaultLevel.indentFirstLine === compareLevel.indentFirstLine &&
        defaultLevel.textAlign === compareLevel.textAlign &&
        defaultLevel.levelText === compareLevel.levelText &&
        defaultLevel.fontName === compareLevel.fontName &&
        defaultLevel.listStartValue === compareLevel.listStartValue;
    return ret;
}

function isDefinitionEqual(defaultDefinition, compareDefinition) {
    return isLevelEqual(defaultDefinition.listLevel0, compareDefinition.listLevel0) &&
        isLevelEqual(defaultDefinition.listLevel1, compareDefinition.listLevel1) &&
        isLevelEqual(defaultDefinition.listLevel2, compareDefinition.listLevel2) &&
        isLevelEqual(defaultDefinition.listLevel3, compareDefinition.listLevel3) &&
        isLevelEqual(defaultDefinition.listLevel4, compareDefinition.listLevel4) &&
        isLevelEqual(defaultDefinition.listLevel5, compareDefinition.listLevel5) &&
        isLevelEqual(defaultDefinition.listLevel6, compareDefinition.listLevel6) &&
        isLevelEqual(defaultDefinition.listLevel7, compareDefinition.listLevel7) &&
        isLevelEqual(defaultDefinition.listLevel8, compareDefinition.listLevel8);
}

function isArrayDefinitionEqual(arrayDefinition, compareDefinition) {
    return isLevelEqual(arrayDefinition.listLevels[0], compareDefinition.listLevel0) &&
        isLevelEqual(arrayDefinition.listLevels[1], compareDefinition.listLevel1) &&
        isLevelEqual(arrayDefinition.listLevels[2], compareDefinition.listLevel2) &&
        isLevelEqual(arrayDefinition.listLevels[3], compareDefinition.listLevel3) &&
        isLevelEqual(arrayDefinition.listLevels[4], compareDefinition.listLevel4) &&
        isLevelEqual(arrayDefinition.listLevels[5], compareDefinition.listLevel5) &&
        isLevelEqual(arrayDefinition.listLevels[6], compareDefinition.listLevel6) &&
        isLevelEqual(arrayDefinition.listLevels[7], compareDefinition.listLevel7) &&
        isLevelEqual(arrayDefinition.listLevels[8], compareDefinition.listLevel8);
}

function isArrayDefinitionEqualAtLevel(arrayDefinition, compareDefinition, oneLevel) {
    const levelKey = 'listLevel' + oneLevel;
    return isLevelEqual(arrayDefinition.listLevels[oneLevel], compareDefinition[levelKey]);
}

// class ListCollection ===================================================

/**
 * Contains the definitions of lists.
 *
 * @param {TextModel} docModel
 *  The document model containing this instance.
 */
class ListCollection extends ModelObject {

    // list definitions
    #lists = [];
    // listIds used in insertList operation
    #insertedListIds = {};
    // saving the base styles id for every list style
    // -> key value pairs: listStyleId: baseStyleId
    #baseStyleIDs = {};
    // saving all list styles, that share the same base style
    // -> key value pairs: baseStyleId: [listStyleId1, listStyleId2, ...]
    #allListStyleIDs = {};
    // defaults
    #defaultNumberingNumId;
    #defaultBulletNumId;
    // whether the document is and ODF file
    #odfFormat = false;

    constructor(docModel) {

        // base constructor
        super(docModel);

        // properties ---------------------------------------------------------

        this.#odfFormat = this.docApp.isODF();
    }

    // methods ------------------------------------------------------------

    /**
     * Adds a new list to this container. An existing list definition
     * with the specified identifier will be replaced.
     *
     * @param {Object} operation
     *  The attributes of the list definition.
     *
     * @returns {ListCollection}
     *  A reference to this instance.
     */
    insertList(operation) {

        const list = {};

        this.#lists.push(list);
        list.listIdentifier = operation.listStyleId;
        list.listUnifiedNumbering = operation.listUnifiedNumbering;
        list.listLevels = [];

        // handling the base style id (required for task 40792)
        if (operation.baseStyleId) {
            // saving base style direct in the list object
            list.baseStyleId = operation.baseStyleId;
            // creating key value pair from list style to base style
            this.#baseStyleIDs[operation.listStyleId] = operation.baseStyleId;
            // creating list with all list styles belonging to one base style
            if (!this.#allListStyleIDs[operation.baseStyleId]) { this.#allListStyleIDs[operation.baseStyleId] = []; }
            this.#allListStyleIDs[operation.baseStyleId].push(operation.listStyleId);
        }

        if (operation.listDefinition) {
            list.listLevels[0] = this.#odfFormat ? (operation.listDefinition.listLevel0 || {}) : operation.listDefinition.listLevel0;
            list.listLevels[1] = this.#odfFormat ? (operation.listDefinition.listLevel1 || {}) : operation.listDefinition.listLevel1;
            list.listLevels[2] = this.#odfFormat ? (operation.listDefinition.listLevel2 || {}) : operation.listDefinition.listLevel2;
            list.listLevels[3] = this.#odfFormat ? (operation.listDefinition.listLevel3 || {}) : operation.listDefinition.listLevel3;
            list.listLevels[4] = this.#odfFormat ? (operation.listDefinition.listLevel4 || {}) : operation.listDefinition.listLevel4;
            list.listLevels[5] = this.#odfFormat ? (operation.listDefinition.listLevel5 || {}) : operation.listDefinition.listLevel5;
            list.listLevels[6] = this.#odfFormat ? (operation.listDefinition.listLevel6 || {}) : operation.listDefinition.listLevel6;
            list.listLevels[7] = this.#odfFormat ? (operation.listDefinition.listLevel7 || {}) : operation.listDefinition.listLevel7;
            list.listLevels[8] = this.#odfFormat ? (operation.listDefinition.listLevel8 || {}) : operation.listDefinition.listLevel8;
            if (operation.listDefinition.listLevel9) {
                list.listLevels[9] = operation.listDefinition.listLevel9;
            }
            if (operation.listDefinition.defaultlist) {
                if (operation.listDefinition.defaultlist === 'bullet') {
                    this.#defaultBulletNumId = operation.listStyleId;
                } else {
                    this.#defaultNumberingNumId = operation.listStyleId;
                }
            } else {
                if (this.#defaultBulletNumId === undefined) {
                    if (isDefinitionEqual(DEFAULT_BULLET_LIST_DEFINITION, operation.listDefinition) === true) {
                        this.#defaultBulletNumId = operation.listStyleId;
                    }
                }
                if (this.#defaultNumberingNumId === undefined) {
                    if (isDefinitionEqual(DEFAULT_NUMBERING_LIST_DEFINITION, operation.listDefinition) === true) {
                        this.#defaultNumberingNumId = operation.listStyleId;
                    }
                }
            }

            //wingdings bullets, had problem in clipborad and now also in the filter!
            //so we use mario clipboard replace utils to fix it!
            //Bug 38545
            list.listLevels.forEach((listDefinition, index) => {
                const levelText = listDefinition.levelText;
                const replacement = _.isString(listDefinition.fontName) ? getReplacementForBullet(listDefinition.fontName, levelText) : null;
                if (replacement) {
                    list.listLevels[index] = {
                        ...listDefinition,
                        levelText: replacement.char,
                        fontName: replacement.font
                    };
                }
            });
        }

        // notify listeners
        this.trigger('insert:list', operation.listStyleId);
        return this;
    }

    /**
     * Remove an existing list
     *
     * @param {String} listIdentifier
     *  The name of of the list to be removed.
     *
     * @param {Boolean} external
     *  The name of of the list to be removed.
     *
     * @returns {ListCollection}
     *  A reference to this instance.
     */
    deleteList(listIdentifier, external) {

        let index = 0;
        // the undo manager
        const undoManager = this.docModel.getUndoManager();

        // generate undo/redo operations
        if (!external && this.docApp.isOTEnabled() && undoManager.isUndoEnabled()) {
            const undoOperation = this.getListOperationFromListStyleId(listIdentifier);
            const redoOperation = { name: DELETE_LIST, listStyleId: listIdentifier };
            undoManager.addUndo(undoOperation, redoOperation);
        }

        for (; index < this.#lists.length; ++index) {
            if (this.#lists[index].listIdentifier === listIdentifier) {
                this.#lists.splice(index, 1);
                if (this.#defaultNumberingNumId === listIdentifier) {
                    this.#defaultNumberingNumId = undefined;
                }
                if (this.#defaultBulletNumId === listIdentifier) {
                    this.#defaultBulletNumId = undefined;
                }
                break;
            }
        }

        // also removing the list id from the already inserted lists
        if (listIdentifier in this.#insertedListIds) {
            delete this.#insertedListIds[listIdentifier];
        }

        // notify listeners
        this.trigger('delete:list', listIdentifier);
        return this;
    }

    /**
     * Gives access to a single list definition.
     *
     * @param {String} name
     *  the name of the list to return.
     */
    getList(name) {
        let index = 0;
        let ret;

        for (; index < this.#lists.length; ++index) {
            if (this.#lists[index].listIdentifier === name) {
                ret = this.#lists[index];
                break;
            }
        }
        return ret;
    }

    /**
     * Gives access to all list definitions.
     */
    getLists() {
        return this.#lists;
    }

    /**
     * Gives access to a single listlevel definition.
     *
     * @param {String} name
     *  The name of the list.
     *
     * @param {Number} level
     *  The list level to be returnd.
     *
     * @returns {Object|Null}
     *  The list level describing object, or Null, if it is not defined.
     */
    getListLevel(name, level) {
        let index = 0;
        let ret = null;
        const length = this.#lists.length;

        for (; index < length; ++index) {
            if (this.#lists[index].listIdentifier === name) {
                ret = this.#lists[index];
                if (ret.listLevels && ret.listLevels[level]) {
                    ret = ret.listLevels[level];
                    break;
                }
                break;
            }
        }

        return ret;
    }

    /**
     * @param {String} type
     *  either bullet or numbering
     * @returns {integer}
     *  the Id of a default bullet or numbered numbering. If this default numbering definition is not available then it will be created
     */
    getDefaultNumId(type) {
        return type === 'bullet' ? this.#defaultBulletNumId : this.#defaultNumberingNumId;
    }

    /**
     * creates a list id not used, yet
     *
     * @returns {String}
     *  new list id
     */
    getFreeListId() {
        let freeId = (this.#lists.length + 1);
        let sFreeId = 'L' + freeId;

        while (this.hasListStyleId(sFreeId)) {
            ++freeId;
            sFreeId = 'L' + freeId;
        }
        return sFreeId;
    }

    /**
     * Returns true if the given listStyleId is in use
     *
     * @param {String} listStyleId
     *  the list style id
     *
     * @returns {Boolean}
     *  return true if the listStyleId is in use
     */
    hasListStyleId(listStyleId) {
        return _.any(this.#lists, element => {
            return (element.listIdentifier === listStyleId);
        });
    }

    /**
     * Returns the default list definition for bullet or numbering lists.
     *
     * @param {String} [type='numbering']
     *  'bullet' or 'character' for bullet lists, 'numbering' for numbered lists.
     *
     * @returns {Object}
     *  The default list definition.
     */
    getDefaultListDefinition(type) {
        let listDefinition = null;

        if (type === 'bullet' || type === 'character') {
            listDefinition = _.copy(DEFAULT_BULLET_LIST_DEFINITION, true);
            //replace ooxml special bullets
            if (this.#odfFormat) {
                listDefinition.listLevel0.levelText = '\u2022';
                listDefinition.listLevel3.levelText = '\u2022';
                listDefinition.listLevel6.levelText = '\u2022';
            }
        } else {
            listDefinition = _.copy(DEFAULT_NUMBERING_LIST_DEFINITION, true);
        }

        return listDefinition;
    }

    /**
     * @param {String} type
     *  either bullet or numbering
     * @param {Object} options
     *  can contain symbol - the bullet symbol
     *              listStartValue - start index of an ordered list
     * @returns {Object}
     *  the operation that creates the requested list
     *
     */
    getDefaultListOperation(type, options) {

        const sFreeId = this.getFreeListId();
        let newOperation = null;
        let target = null; // needed for odt handling (44393)
        let fullListStyleId = sFreeId; // needed for odt handling (44393)

        if (this.docApp.isODF() && this.docModel.isHeaderFooterEditState()) { // odt handling (44393)
            target = this.docModel.getActiveTarget();
            if (target) {
                fullListStyleId = sFreeId + '_' + target;
            }
        }

        newOperation = { name: INSERT_LIST, listStyleId: fullListStyleId };

        if (target) { newOperation.target = target; } // 44393

        newOperation.listDefinition = this.getDefaultListDefinition(type);

        if (type === 'bullet') {
            if (options && options.symbol && options.symbol !== '*') {
                newOperation.listDefinition.listLevel0.levelText = options.symbol;
            } else {
                newOperation.listDefinition.defaultlist = type;
            }
        } else {
            let defaultlist = true;
            if (options) {
                if (options.listStartValue) {
                    newOperation.listDefinition.listLevel0.listStartValue = options.listStartValue;
                    defaultlist = false;
                }
                if (options.numberFormat) {
                    newOperation.listDefinition.listLevel0.numberFormat = options.numberFormat;
                    defaultlist = false;
                }
            }
            if (defaultlist) {
                newOperation.listDefinition.defaultlist = type;
            }
        }
        return newOperation;
    }

    /**
     * Generates an Insert List operation from an array of html list types.
     *
     * @param {Array} htmlTypes
     *  The HTML list types.
     *      The array index represents the list level and the value
     *      the corresponding list type.
     *
     * @returns {Object}
     *  The operation that creates the requested list.
     *
     */
    getListOperationFromHtmlListTypes(htmlTypes) {

        let listLevel;
        let levelCount;
        const sFreeId = this.getFreeListId();
        const newOperation = { name: INSERT_LIST, listStyleId: sFreeId };
        let type;

        if (_.isArray(htmlTypes)) {
            // set list definition for either bullet or numbering
            type = ListCollection.getListTypeFromHtmlListType(htmlTypes[0]);
            newOperation.listDefinition = _.copy((type === 'numbering') ? DEFAULT_NUMBERING_LIST_DEFINITION : DEFAULT_BULLET_LIST_DEFINITION, true);
            if (this.#odfFormat) {
                newOperation.listDefinition.listLevel9 =  { textAlign: 'left', indentLeft: 10 * 1270, indentFirstLine: -635, listStartValue: 1 };
            }
            levelCount = Math.min(htmlTypes.length, _.size(newOperation.listDefinition));

            for (listLevel = 0; listLevel < levelCount; listLevel++) {

                switch (htmlTypes[listLevel]) {

                    case 'decimal':
                    case '1':
                        newOperation.listDefinition['listLevel' + listLevel].numberFormat = 'decimal';
                        newOperation.listDefinition['listLevel' + listLevel].levelText = '%' + (listLevel + 1) + '.';
                        break;

                    case 'decimal-leading-zero':
                        newOperation.listDefinition['listLevel' + listLevel].numberFormat = 'decimal';
                        newOperation.listDefinition['listLevel' + listLevel].levelText = '0%' + (listLevel + 1) + '.';
                        break;

                    case 'A':
                    case 'upper-alpha':
                    case 'upper-latin':
                        newOperation.listDefinition['listLevel' + listLevel].numberFormat = 'upperLetter';
                        newOperation.listDefinition['listLevel' + listLevel].levelText = '%' + (listLevel + 1) + '.';
                        break;

                    case 'a':
                    case 'lower-alpha':
                    case 'lower-latin':
                        newOperation.listDefinition['listLevel' + listLevel].numberFormat = 'lowerLetter';
                        newOperation.listDefinition['listLevel' + listLevel].levelText = '%' + (listLevel + 1) + '.';
                        break;

                    case 'I':
                    case 'upper-roman':
                        newOperation.listDefinition['listLevel' + listLevel].numberFormat = 'upperRoman';
                        newOperation.listDefinition['listLevel' + listLevel].levelText = '%' + (listLevel + 1) + '.';
                        break;

                    case 'i':
                    case 'lower-roman':
                        newOperation.listDefinition['listLevel' + listLevel].numberFormat = 'lowerRoman';
                        newOperation.listDefinition['listLevel' + listLevel].levelText = '%' + (listLevel + 1) + '.';
                        break;

                    case 'disc':
                        newOperation.listDefinition['listLevel' + listLevel].numberFormat = 'bullet';
                        newOperation.listDefinition['listLevel' + listLevel].fontName = 'Symbol';
                        newOperation.listDefinition['listLevel' + listLevel].levelText = '\uf0b7';
                        break;

                    case 'circle':
                        newOperation.listDefinition['listLevel' + listLevel].numberFormat = 'bullet';
                        newOperation.listDefinition['listLevel' + listLevel].fontName = 'Times New Roman';
                        newOperation.listDefinition['listLevel' + listLevel].levelText = '\u25CB';
                        break;

                    case 'square':
                        newOperation.listDefinition['listLevel' + listLevel].numberFormat = 'bullet';
                        newOperation.listDefinition['listLevel' + listLevel].fontName = 'Times New Roman';
                        newOperation.listDefinition['listLevel' + listLevel].levelText = '\u25A0';
                        break;

                    case 'none':
                        newOperation.listDefinition['listLevel' + listLevel].numberFormat = 'bullet';
                        newOperation.listDefinition['listLevel' + listLevel].fontName = 'Times New Roman';
                        newOperation.listDefinition['listLevel' + listLevel].levelText = ' ';
                        break;

                    default:
                        //newOperation.listDefinition['listLevel' + listLevel].numberFormat = 'decimal';
                        //newOperation.listDefinition['listLevel' + listLevel].levelText = '%1.';
                        break;
                }
            }
        }

        return newOperation;
    }

    /**
     * Generates an Insert List operation from a MS list defintion (generated by the clipboard parser).
     *
     * @param {Object} definition
     *          {Object} level
     *              {String} numberFormat
     *                  The number format of the list level
     *              {String} levelText
     *                  The text for the list level
     *              {String} fontFamily
     *                  The level text font
     *              {Number} indentFirstLine
     *                  The first line text indent in 1/100 of millimeters.
     *              {Number} indentLeft
     *                  The left indent in 1/100 of millimeters.
     *              {String} numberPosition
     *                  The number position
     *
     * @returns {Object}
     *  The operation that creates the requested list.
     */
    getListOperationFromListDefinition(definition) {

        const sFreeId = this.getFreeListId();
        const newOperation = { name: INSERT_LIST, listStyleId: sFreeId };
        const type = definition && definition[0] && definition[0].numberFormat;

        // set list definition for either bullet or numbering
        newOperation.listDefinition = _.copy((type === 'bullet') ? DEFAULT_BULLET_LIST_DEFINITION : DEFAULT_NUMBERING_LIST_DEFINITION, true);

        _.each(definition, (listItem, listLevel) => {

            if (listItem.numberFormat) {
                newOperation.listDefinition['listLevel' + listLevel].numberFormat = listItem.numberFormat;
            }
            if (listItem.levelText) {
                newOperation.listDefinition['listLevel' + listLevel].levelText = listItem.levelText;
            }
            if (listItem.fontFamily) {
                newOperation.listDefinition['listLevel' + listLevel].fontName = listItem.fontFamily;
            }
            if (listItem.indentFirstLine) {
                newOperation.listDefinition['listLevel' + listLevel].indentFirstLine = listItem.indentFirstLine;
            }
            if (listItem.indentLeft) {
                newOperation.listDefinition['listLevel' + listLevel].indentLeft = listItem.indentLeft;
            }
            if (listItem.numberPosition) {
                newOperation.listDefinition['listLevel' + listLevel].textAlign = listItem.numberPosition;
            }
        });

        return newOperation;
    }

    /**
     * Generates an Insert List operation from a list style id.
     *
     * @param {String} listStyleId
     *  The list style id.
     *
     * @returns {Object}
     *  The operation that creates the requested list.
     *
     */
    getListOperationFromListStyleId(listStyleId) {

        let newOperation = null;
        const list = this.getList(listStyleId);

        if (list) {

            newOperation = { name: INSERT_LIST, listDefinition: {}, listStyleId: list.listIdentifier };

            _(list.listLevels).each((levelData, id) => {
                newOperation.listDefinition['listLevel' + id] = levelData;
            });

            // also adding the base style id, if available
            if (list.baseStyleId) { newOperation.baseStyleId = list.baseStyleId; }
        }

        return newOperation;
    }

    /**
     * Generating a new list style with new list style ID from an already existing list style.
     *
     * @param {String} listStyleId
     *  The reference list style id of that list style that will be used for the new list style
     *  (with new ID).
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.useBaseStyle=false]
     *      Whether a baseStyleId shall be transferred to the new list style. If this is set to
     *      true, the list counting with the new list style is not (!) modified compared with the
     *      reference style.
     *  @param {Boolean} [options.resetStartValue=false]
     *      Whether the list start value shall be resetted.
     *
     * @returns {Object|Null}
     *  The operation that creates the new list style. Or null, if no existing reference list could
     *  be found.
     */
    getNewListOperationFromListStyleId(listStyleId, options) {

        // the new operation that might be generated
        let newOperation = null;
        // the existing list witht the specified list style ID
        const list = this.getList(listStyleId);

        if (list) {

            newOperation = { name: INSERT_LIST, listDefinition: {}, listStyleId: this.getFreeListId() };

            _(list.listLevels).each((levelData, id) => {
                newOperation.listDefinition['listLevel' + id] = _.copy(levelData, true); // deep copy
            });

            if (list.baseStyleId && getBooleanOption(options, 'useBaseStyle', false)) {
                // also adding the base style id, if available
                newOperation.baseStyleId = list.baseStyleId;
            }

            // setting the start value to 1 in new generated list style
            if (getBooleanOption(options, 'resetStartValue', false)) {
                _.each(newOperation.listDefinition, levelDef => {
                    levelDef.listStartValue = 1;
                });
            }
        }

        return newOperation;
    }

    /**
     * @param {String} listId
     *  selected list style id
     *
     * @param {String} target
     *  An optional target that is used for generating specific list IDs that are
     *  used in lists in ODT headers or footers (44393).
     *
     * @param {String} predefinedStyleId the styleId to get the predefined liststyle
     *
     * @returns {Object|Null}
     *  the operation that creates the requested list
     */
    getSelectedListStyleOperation(listId, target, predefinedStyleId) {

        const fullListId = target ? (listId + '_' + target) : listId;
        let newOperation = { name: INSERT_LIST, listStyleId: fullListId };

        if (target && this.docApp.isODF() && this.docModel.isHeaderFooterEditState()) { newOperation.target = target; } // odt handling (44393)

        if ((predefinedStyleId in PREDEFINED_LISTSTYLES) && !(fullListId in this.#insertedListIds)) {
            newOperation.listDefinition = _.copy(PREDEFINED_LISTSTYLES[predefinedStyleId].definition, true);
            this.#insertedListIds[fullListId] = true;  // avoiding inserting list more than once
        } else {
            newOperation = null;
        }
        return newOperation;
    }

    /**
     * Merge a level of a list definition into an existing listStyle and create an insertList operation
     * (if no such list exists) and return that operation and it's listId
     *
     * @param {String} existingListStyle
     *  selected list style id
     *
     * @param {string} modifyingStyle
     *
     * @param {number} level
     *
     * @returns {Object}
     *  containing the operation that creates the requested list (optional)
     *  and the id of the merged list
     */
    mergeListStyle(existingListStyle, modifyingStyle, level) {

        const operationAndStyle = { listOperation: null, listStyleId: null };
        let sourceLevel = null;
        let sourceListDefinition;

        if ((modifyingStyle in PREDEFINED_LISTSTYLES)) {
            operationAndStyle.listOperation = { name: INSERT_LIST, listStyleId: this.getFreeListId() };
            operationAndStyle.listOperation.listDefinition = _.copy(this.getList(existingListStyle), true);
            sourceListDefinition = PREDEFINED_LISTSTYLES[modifyingStyle].definition;
            _(9).times(l => {
                sourceLevel = l === level ? sourceListDefinition['listLevel' + l] : operationAndStyle.listOperation.listDefinition.listLevels[l];
                operationAndStyle.listOperation.listDefinition['listLevel' + l] = _.copy(sourceLevel, true);

            });

            //search for the new list definition in the existing list styles to prevent creation of identical styles
            operationAndStyle.listStyleId = operationAndStyle.listOperation.listStyleId;
            _.each(this.#lists, (element, key, list) => {
                if (operationAndStyle.listOperation && isArrayDefinitionEqual(element, operationAndStyle.listOperation.listDefinition)) {
                    operationAndStyle.listStyleId = list[key].listIdentifier;
                    operationAndStyle.listOperation = null;
                }
            });
        } else {
            operationAndStyle.listStyleId = existingListStyle;
        }
        return operationAndStyle;
    }

    /**
     * Checks if the given list definition matches an existing on a predefined list style
     * and returns the corresponding list style id, otherwise returns null.
     *
     * @param {Object} listDefinition
     *  the list definition to get the list style id for.
     *
     * @returns {String|null}
     *  the list style id for the given list definition or null.
     */
    getListStyleIdForListDefinition(listDefinition) {

        let listStyleId = null;

        // search for the list definition in the predefined list styles
        for (listStyleId in PREDEFINED_LISTSTYLES) {
            if (isDefinitionEqual(PREDEFINED_LISTSTYLES[listStyleId].definition, listDefinition)) {
                return listStyleId;
            }
        }

        // search for the list definition in the existing list styles
        for (listStyleId = 0; listStyleId < this.#lists.length; listStyleId++) {
            if (isArrayDefinitionEqual(this.#lists[listStyleId], listDefinition)) {
                return this.#lists[listStyleId].listIdentifier;
            }
        }

        return null;
    }

    /**
     * Returns whether the specified list is a bullet list in all levels.
     *
     * @param {String} listStyleId
     *  The identifier of a list.
     *
     * @returns {Boolean}
     *  Whether the specified list identifier points to a list, that contains in all levels
     *  bullets (or the levels are undefined).
     */
    isAllLevelsBulletsList(listStyleId) {

        const currentList = this.getList(listStyleId);

        if (!currentList) { return false; }

        return _.every(_.range(10), level => {
            const listLevelDef = currentList.listLevels[level];
            return (!listLevelDef || ((listLevelDef.numberFormat === 'bullet') || (listLevelDef.numberFormat === 'none')));
        });
    }

    /**
     * Returns whether the specified list is a bullet list.
     *
     * @param {String} listStyleId
     *  The identifier of a list.
     *
     * @param {Number} listLevel
     *  the indent level of the list in the paragraph.
     *
     * @returns {Boolean}
     *  Whether the specified list identifier points to a bullet list.
     */
    isBulletsList(listStyleId, listLevel) {

        const currentList = this.getList(listStyleId);
        const listLevelDef = currentList ? currentList.listLevels[listLevel] : null;

        return listLevelDef && ((listLevelDef.numberFormat === 'bullet') || (listLevelDef.numberFormat === 'none'));
    }

    /**
     * Returns whether the specified list is a numbered list.
     *
     * @param {String} listStyleId
     *  The identifier of a list.
     *
     * @param {Number} listLevel
     *  the indent level of the list in the paragraph.
     *
     * @returns {Boolean}
     *  Whether the specified list identifier points to a numbered list.
     */
    isNumberingList(listStyleId, listLevel) {

        const currentList = this.getList(listStyleId);
        const listLevelDef = currentList ? currentList.listLevels[listLevel] : null;

        return listLevelDef && (listLevelDef.numberFormat !== 'bullet') && (listLevelDef.numberFormat !== 'none');
    }

    /**
     * Generates the numbering Label for the given paragraph
     *
     * @param {String} listStyleId
     *  identifier of the applied numbering definition
     *
     * @param {Number} ilvl
     *  indent level, zero based
     *
     * @param {Array<Number>} levelIndexes
     *  Array of sequential position of the current paragraph, with ilvl+1
     *  elements that determines the sequential position of the current
     *  paragraph within the numbering.
     *
     * @returns {Object}
     *          indent
     *          labelwidth
     *          text
     *          tbd.
     */
    formatNumber(listStyleId, ilvl, levelIndexes, listParagraphIndex, styleId, explicitAttrs) {

        const ret = {};
        const currentList = this.getList(listStyleId);
        if (currentList === undefined) {
            return '?';
        }
        const levelFormat = currentList.listLevels[ilvl];
        if (levelFormat === undefined) {
            return '??';
        }
        const format = this.#formatNumberType(levelIndexes, ilvl, currentList, listParagraphIndex);
        _.extend(ret, format);

        let styleParaIndentLeft = null;
        let styleParaIndentFirstLine = null;

        let attributesHandled = false;
        if (styleId) {

            // the full set of attributes from the style ID, including inheritance -> only required to get an optional list style
            const styleAttrsInherited = this.docModel.paragraphStyles.getStyleAttributeSet(styleId);
            const styleParaAttrsInherited = (styleAttrsInherited && styleAttrsInherited.paragraph) ? styleAttrsInherited.paragraph : null;

            // taking care of hard-coded indentLeft and indentFirstLine at a paragraph style that specifies the list style.
            // Example: Heading 3 specifies the listStyle "L6" and has an own indentLeft value (DOCS-3243).
            if (styleParaAttrsInherited && styleParaAttrsInherited.listStyleId && styleParaAttrsInherited.listStyleId === listStyleId) {

                // the style attributes of the specified style ID, without inheritance
                const styleAttrs = this.docModel.paragraphStyles.getStyleSheetAttributeMap(styleId);
                const styleParaAttrs = (styleAttrs && styleAttrs.paragraph) ? styleAttrs.paragraph : null;

                if (styleParaAttrs) {
                    styleParaIndentLeft = _.isNumber(styleParaAttrs.indentLeft) ? styleParaAttrs.indentLeft : null;
                    styleParaIndentFirstLine = _.isNumber(styleParaAttrs.indentFirstLine) ? styleParaAttrs.indentFirstLine : null;
                    attributesHandled = _.isNumber(styleParaAttrs.indentLeft) || _.isNumber(styleParaAttrs.indentFirstLine);
                }

                // Possible improvement: Explicit attributes should always be evaluated here, not only for paragraph styles with list styles
                // -> handling of text-indent is still missing here
                if (explicitAttrs) {
                    if (_.isNumber(explicitAttrs.indentLeft)) { styleParaIndentLeft = explicitAttrs.indentLeft; }
                    if (_.isNumber(explicitAttrs.indentFirstLine)) { styleParaIndentFirstLine = explicitAttrs.indentFirstLine; }
                    attributesHandled = _.isNumber(explicitAttrs.indentLeft) || _.isNumber(explicitAttrs.indentFirstLine);
                    // required for DOCS-3333 -> no further handling of explicit attributes!
                }
            } else if (explicitAttrs && _.isNumber(explicitAttrs.indentLeft) && _.isNumber(explicitAttrs.indentFirstLine)) {
                if (explicitAttrs.indentLeft === -explicitAttrs.indentFirstLine) { // simple scenario without margin-left
                    if (_.isNumber(explicitAttrs.indentLeft)) { styleParaIndentLeft = explicitAttrs.indentLeft; }
                    if (_.isNumber(explicitAttrs.indentFirstLine)) { styleParaIndentFirstLine = explicitAttrs.indentFirstLine; }
                    attributesHandled = _.isNumber(explicitAttrs.indentLeft) || _.isNumber(explicitAttrs.indentFirstLine);
                    // required for DOCS-3434 / DOCS-3460 -> no further handling of explicit attributes!
                }
            }
        }

        const relevantIndentLeft = _.isNumber(styleParaIndentLeft) ? styleParaIndentLeft : levelFormat.indentLeft;
        const relevantIndentFirstLine = _.isNumber(styleParaIndentFirstLine) ? styleParaIndentFirstLine : levelFormat.indentFirstLine;

        ret.indent = relevantIndentLeft;
        ret.firstLine = relevantIndentFirstLine ? relevantIndentLeft + relevantIndentFirstLine : relevantIndentLeft;

        ret.attributesHandled = attributesHandled; // TODO: This should be the default in the future
        ret.indentLeftFromLevelFormat = !_.isNumber(styleParaIndentLeft);

        return ret;
    }

    findIlvl(listStyleId, paraStyle) {

        const list = this.getList(listStyleId);
        if (list === undefined) {
            return -1;
        }
        let ilvl = 0;
        for (; ilvl < 9; ++ilvl) {
            const levelFormat = list.listLevels[ilvl];
            if (levelFormat.paraStyle === paraStyle) {
                return ilvl;
            }
        }
        return -1;
    }

    findPrevNextStyle(listStyleId, paraStyle, prev) {
        const list = this.getList(listStyleId);
        if (list === undefined) {
            return '';
        }
        let ilvl = 0;
        for (; ilvl < 9; ++ilvl) {
            const levelFormat = list.listLevels[ilvl];
            if (levelFormat.paraStyle === paraStyle) {
                let ret = '';
                if (prev) {
                    if (ilvl > 0 && list.listLevels[ilvl - 1].paraStyle) {
                        ret = list.listLevels[ilvl - 1].paraStyle;
                    }
                } else if (ilvl < 8 && list.listLevels[ilvl + 1].paraStyle) {
                    ret = list.listLevels[ilvl + 1].paraStyle;
                }
                return ret;
            }
        }
        return -1;
    }

    /**
     * Receiving a list of list style ids, that have the same base style id as a specified list
     * style. If there is no base style at all, undefined is returned.
     *
     * @param {String} listStyleId
     *  The list style id for that the list of all list styles with same base style id shall be returned.
     *
     * @returns {String[]|undefined}
     *  Returns an array with all list style IDs, that share the same base style as the specified
     *  list style. Or undefined, if there is no common base style at all.
     */
    getAllListStylesWithSameBaseStyle(listStyleId) {
        return this.#baseStyleIDs[listStyleId] && this.#allListStyleIDs[this.#baseStyleIDs[listStyleId]];
    }

    /**
     * Receiving the base style id for a specified list style id. If there is no base style at all,
     * undefined is returned.
     *
     * @param {String} listStyleId
     *  The list style id for that the base style id shall be returned.
     *
     * @returns {String|undefined}
     *  Returns the base style ID for the specified list style id. Or undefined, if there is no base
     *  style at all.
     */
    getBaseStyleIdFromListStyle(listStyleId) {
        return this.#baseStyleIDs[listStyleId];
    }

    /**
     * Getter for all predefined list styles.
     *
     * @returns {Object}
     *  A map with list style identifiers as keys, and objects as values
     *  containing the attributes 'definition' with the list style definition,
     *  'listlabel' containing a string with the number 1 formatted according
     *  to the list style, and 'tooltip' containing a GUI tool tip string for
     *  the list style.
     */
    getPredefinedListStyles() {
        return PREDEFINED_LISTSTYLES;
    }

    /**
     * Getter for the object containing the ignorable paragraph styles.
     *
     * @returns {Object}
     *  An object, containing as key those paragraph styles, that can be ignored,
     *  if lists numbering needs to be continued.
     */
    getIgnorableParagraphStyles() {
        return IGNORABLE_PARA_STYLES_IN_LISTS;
    }

    /**
     * Get the listId for a liststyle. If the liststyle is used in the previous paragraph, return the previous
     * listId, if the previous not empty paragraph has no liststyle or the given id is in use return null to create a new
     * listId.
     *
     * @param {Object} listDefinition The default list definition.
     * @param {String} id the liststyleId returned if the previous not empty paragraph has not liststyle and if this id is not in use
     * @returns {String} the liststyleId to create a liststyle
     */
    getListId(listDefinition, id) {

        let listId = this.hasListStyleId(id) ? null : id;
        if (listDefinition) {
            const rootNode = this.docModel.getCurrentRootNode();
            const startPosition = this.docModel.getSelection().getStartPosition();
            startPosition.pop();
            const paragraph = getParagraphElement(rootNode, startPosition);
            let prevParagraph = findPreviousNode(rootNode, paragraph, PARAGRAPH_NODE_SELECTOR);
            const paragraphAttrs = this.docModel.paragraphStyles.getElementAttributes(paragraph);
            const paragraphStyle = (paragraphAttrs.styleId && !IGNORABLE_PARA_STYLES_IN_LISTS[paragraphAttrs.styleId]) ? paragraphAttrs.styleId : '';
            while (prevParagraph) {
                if (isEmptyParagraph(prevParagraph)) {
                    prevParagraph = findPreviousNode(rootNode, prevParagraph, PARAGRAPH_NODE_SELECTOR);
                } else {
                    const attrs = this.docModel.paragraphStyles.getElementAttributes(prevParagraph);
                    const prevParaStyle = (attrs.styleId && !IGNORABLE_PARA_STYLES_IN_LISTS[attrs.styleId]) ? attrs.styleId : '';
                    const listStyleList = this.getList(attrs.paragraph.listStyleId);
                    if (attrs.paragraph.listStyleId !== '' && paragraphStyle === prevParaStyle && listStyleList && isArrayDefinitionEqual(listStyleList, listDefinition)) {
                        listId = attrs.paragraph.listStyleId; // continuing the list of the previous paragraph
                    } else {
                        const continuedId = this.#checkPreviousSingleParagraphList(paragraph, paragraphAttrs, listDefinition); // check if a single paragraph list must be continued
                        if (continuedId) { listId = continuedId; } // only change listId, if there is a previous paragraph
                    }
                    prevParagraph = null;
                }
            }
        }

        return listId;
    }

    // private methods ----------------------------------------------------

    /**
     * Creates the label text of the current numbered paragraph.
     * For picture numbered bullet list it returns the URI of the picture
     *
     * @param {Array<Number>} levelIndexes
     *  array of indexes of all numbering levels
     *
     * @param {Number} ilvl
     *  current indentation level
     *
     * @param {Object} listDefinition
     *  properties of the list used
     *
     * @returns {Object}
     *  text element contains label text
     *  imgsrc element contains URI of the picture used
     *  In case imgsrc is set then text will be empty
     */
    #formatNumberType(levelIndexes, ilvl, listDefinition, listParagraphIndex) {

        const ret = {};
        let seqNo = 0;
        const end = (listDefinition.listUnifiedNumbering === true) ? ilvl : 0;
        const topLevelformat = listDefinition.listLevels[ilvl];
        let levelText = topLevelformat.levelText ? topLevelformat.levelText : '';
        let lvl = ilvl;

        ret.labelFollowedBy = listDefinition.listLevels[ilvl].labelFollowedBy;
        ret.tabpos = listDefinition.listLevels[ilvl].tabpos;

        for (; lvl >= end; --lvl) {
            const levelFormat = listDefinition.listLevels[lvl];
            if (listDefinition.listUnifiedNumbering === true) {
                seqNo += listParagraphIndex;
                if (listDefinition.listLevels[0] && listDefinition.listLevels[0].listStartValue !== undefined) {
                    seqNo += listDefinition.listLevels[0].listStartValue - 1;
                }
            } else {
                seqNo = (levelIndexes === undefined) ? 0 :
                    levelIndexes[lvl] + (levelFormat && levelFormat.listStartValue !== undefined ? levelFormat.listStartValue - 1 : 0);
            }
            const levelToken = '%' + (lvl + 1);
            const indexpos = levelText.indexOf(levelToken);
            if (indexpos < 0 && levelFormat.numberFormat !== 'bullet') {
                continue;
            }
            let replacetext = '';
            switch (levelFormat.numberFormat) {
                case 'bullet':
                    if (lvl === ilvl) { // bullets in 'upper' levels are ignored
                        if (levelFormat.levelPicBulletUri) {
                            ret.imgsrc = levelFormat.levelPicBulletUri;
                            ret.imgwidth = levelFormat.width;
                            replacetext = '';
                        } else {
                            const charCode = levelFormat.levelText ? levelFormat.levelText.charCodeAt(0) : -1;
                            if (charCode > 0 && (charCode < 0xE000 || charCode > 0xF8FF)) {
                                replacetext = levelFormat.levelText;
                            } else {
                                replacetext = charCode === 0xf0b7 ? '\u2022' : '\u25cf';
                            }
                        }
                    }
                    break;
                case 'none':
                    replacetext = '';
                    break;
                default:
                    replacetext = formatNumberImport(seqNo, levelFormat.numberFormat);
            }

            if (topLevelformat.levelPicBulletUri) {
                levelText = '';
                break;
            }
            if (topLevelformat.numberFormat === 'bullet') {
                levelText = replacetext;
                break;
            }
            levelText = levelText.replace(levelToken, replacetext);
        }
        ret.color = topLevelformat.color;
        if (!ret.color && topLevelformat.styleId) {
            ret.color = this.docModel.characterStyles.getStyleAttributeSet(topLevelformat.styleId).character.color;
        }
        ret.text = levelText;
        return ret;
    }

    /**
     * Checking whether a list that contains only one paragraph must be continued.
     * This is for example important for headings.
     *
     * A list is continued under two circumstances:
     * 1. The previous paragraph is a list paragraph with the same list style ID.
     * 2. There is one previous paragraphs in the document, that uses the same list
     *    style ID and this paragraph has no direct neighbor that uses the list style
     *    ID, too. Two directly following paragraphs with the same list style ID do
     *    stop the list.
     *
     * Within this function, the rule 2 is checked. Rule 1 was already checked,
     * before this function is called.
     *
     * @param {Node} paragraph
     *  The DOM paragraph node to be checked.
     *
     * @param {Object} paragraphAttrs
     *  The complete set of attributes of the specified paragraph.
     *
     * @param {Object} listDefinition
     *  The default list definition.
     *
     * @returns {String|Null}
     *  The liststyle ID, if the list can be continued. Or null, if the list shall
     *  not be continued.
     */
    #checkPreviousSingleParagraphList(paragraph, paragraphAttrs, listDefinition) {

        // the list style ID
        let listId = null;
        // whether the search for a previous paragraph shall continue
        let continueSearch = true;
        // the attributes of the previous paragraph with the same list style
        let prevParaAttrs = null;
        // the attributes of the previous of the previous paragraph
        let prevPrevAttrs = null;
        // the previous paragraph of the previous paragraph
        let prevPrevParagraph = null;
        // the previous paragraph that has the same list style
        let prevParaWithSameListAttrs = null;
        // the style ID of the specified paragraph
        const paraStyleId = (paragraphAttrs.styleId && !IGNORABLE_PARA_STYLES_IN_LISTS[paragraphAttrs.styleId]) ? paragraphAttrs.styleId : '';
        // the list level of the specified paragraph
        const paraLevel = paragraphAttrs.paragraph.listLevel > 0 ? paragraphAttrs.paragraph.listLevel : 0;

        // helper function to find a previous paragraph in the document using the same list style (and the same paragraph style)
        // and that also uses the same paragraph style. The latter is important, so that heading lists are not automatically
        // continued in non-heading lists.
        const getPreviousParagraphWithSameListAttributes = (oneParagraph, styleId, level) => {

            // check all paragraphs, that already use this list style
            const allPreviousParagraphs = $(oneParagraph).prevAll(PARAGRAPH_NODE_SELECTOR); // already reverse order of paragraphs

            return _.find(allPreviousParagraphs, para => {
                const localAttrs = this.docModel.paragraphStyles.getElementAttributes(para);
                const oneParaStyleId = (localAttrs.styleId && !IGNORABLE_PARA_STYLES_IN_LISTS[localAttrs.styleId]) ? localAttrs.styleId : '';
                const listStyleList = this.getList(localAttrs.paragraph.listStyleId);
                return localAttrs.paragraph.listStyleId !== '' && oneParaStyleId === styleId && listStyleList && isArrayDefinitionEqualAtLevel(listStyleList, listDefinition, level);
            });
        };

        // finding the previous paragraph with the same list style
        prevParaWithSameListAttrs = getPreviousParagraphWithSameListAttributes(paragraph, paraStyleId, paraLevel);

        if (prevParaWithSameListAttrs) {

            prevParaAttrs = this.docModel.paragraphStyles.getElementAttributes(prevParaWithSameListAttrs);

            // finding the previous non empty paragraph of this previous paragraph
            prevPrevParagraph = prevParaWithSameListAttrs.previousSibling;

            while (continueSearch) {
                if (isEmptyParagraph(prevPrevParagraph)) {
                    prevPrevParagraph = prevPrevParagraph.previousSibling;
                } else {
                    continueSearch = false;
                }
            }

            // NOT continuing the list, if this found paragraph uses the same list style (two directly following paragraphs with same list style end the list)
            if (prevPrevParagraph) {
                prevPrevAttrs = this.docModel.paragraphStyles.getElementAttributes(prevPrevParagraph);
                if (prevPrevAttrs.paragraph.listStyleId !== '' && prevParaAttrs.paragraph.listStyleId === prevPrevAttrs.paragraph.listStyleId) {
                    listId = null; // not continuing the list, if there are already two directly following paragraphs with same list style ID before (local list)
                } else {
                    listId = prevParaAttrs.paragraph.listStyleId; // continuing the list of the previous single list paragraph
                }
            } else {
                listId = prevParaAttrs.paragraph.listStyleId; // continuing the list of the previous paragraph (prevPara is first in document)
            }

        } else {
            listId = null; // default: Generating new list ID, not continuing the list
        }

        return listId;
    }

    // static methods ---------------------------------------------------------

    static getListTypeFromHtmlListType(htmlType) {

        switch (htmlType) {
            case 'disc':
            case 'square':
            case 'circle':
            case 'none':
                return 'bullet';

            default:
                return 'numbering';
        }
    }

}

// exports ================================================================

export default ListCollection;
