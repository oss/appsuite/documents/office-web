/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { SMALL_DEVICE, convertCssLength, convertCssLengthToHmm, convertHmmToCssLength,
    convertHmmToLength, convertLengthToHmm, findNextSiblingNode, findPreviousSiblingNode,
    getAllAbsoluteDrawingsOnNode, getAllAbsoluteDrawingsOnPage, getBooleanOption,
    getPageNumber, handleParagraphIndex, iterateSelectedDescendantNodes,
    sortDrawingsOrder } from '@/io.ox/office/textframework/utils/textutils';
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import DrawingStyleCollection from '@/io.ox/office/drawinglayer/model/drawingstylecollection';
import { FORCE_INLINE_CLASS, ROTATED_DRAWING_CLASSNAME, clearSelection, getTextFrameNode, isGroupContentNode,
    isGroupedDrawingFrame, isShapeDrawingFrame, isTextFrameShapeDrawingFrame, updateCssTransform,
    updateFormatting } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { drawDrawingSelection } from '@/io.ox/office/text/components/drawing/drawingresize';
import { COMMENTMARGIN_CLASS, DRAWING_MARGINS_HMM, DRAWING_MARGINS_PIXEL, INCREASED_CONTENT_CLASS, PARAGRAPH_NODE_SELECTOR,
    getMarginalTargetNode, isDrawingLayerNode, isFooterNode, isMarginalNode, isMultiLineTextSpan, isPageContentNode,
    isParagraphNode } from '@/io.ox/office/textframework/utils/dom';
import { getPixelPositionToRootNodeOffset } from '@/io.ox/office/textframework/utils/position';

// constants ==============================================================

const parseInt = window.parseInt;

// definitions for text-specific drawing attributes
const DEFINITIONS = {

    /**
     * Margin from top border of the drawing to text contents, in 1/100
     * of millimeters.
     */
    marginTop: { def: 0 },

    /**
     * Margin from bottom border of the drawing to text contents, in
     * 1/100 of millimeters.
     */
    marginBottom: { def: 0 },

    /**
     * Margin from left border of the drawing to text contents, in 1/100
     * of millimeters.
     */
    marginLeft: { def: 0 },

    /**
     * Margin from right border of the drawing to text contents, in
     * 1/100 of millimeters.
     */
    marginRight: { def: 0 },

    // object anchor --------------------------------------------------

    /**
     * If set to true, the drawing is rendered as inline element ('as
     * character'), otherwise it is anchored relative to another
     * element (page, paragraph, table cell, ...).
     */
    inline: { def: true },

    anchorHorBase: { def: 'margin' },

    anchorHorAlign: { def: 'left' },

    anchorHorOffset: { def: 0 },

    anchorVertBase: { def: 'margin' },

    anchorVertAlign: { def: 'top' },

    anchorVertOffset: { def: 0 },

    anchorLayerOrder: { def: 0 },

    anchorBehindDoc: { def: false },

    /**
     * Specifies how text floats around the drawing.
     * - 'none': Text does not float around the drawing.
     * - 'square': Text floats around the bounding box of the drawing.
     * - 'tight': Text aligns to the left/right outline of the drawing.
     * - 'through': Text aligns to the entire outline of the drawing.
     * - 'topAndBottom': Text floats above and below the drawing only.
     */
    textWrapMode: { def: 'topAndBottom' },

    /**
     * Specifies on which side text floats around the drawing. Effective
     * only if the attribute 'textWrapMode' is either 'square',
     * 'tight', or 'through'.
     * - 'both': Text floats at the left and right side.
     * - 'left': Text floats at the left side of the drawing only.
     * - 'right': Text floats at the right side of the drawing only.
     * - 'largest': Text floats at the larger side of the drawing only.
     */
    textWrapSide: { def: 'both' },

    /**
     * The base for the relative horizontal width and offset of a drawing: margin, page, leftMargin, rightMargin, insideMargin, outsideMargin
     */
    sizeRelHFrom: { def: '' },

    /**
     * The base for the relative vertical height and offset of a drawing: margin, page, topMargin, bottomMargin, insideMargin, outsideMargin
     */
    sizeRelVFrom: { def: '' },

    /**
     * Relative width of a drawing in percent of the specified "sizeRelHFrom", for example the "page". This is a value between 0 and 100000.
     * Only used, if "sizeRelHFrom" is not 'none'.
     */
    pctWidth: { def: 100000 },

    /**
     * Relative height of a drawing in percent of the specified "sizeRelVFrom", for example the "page". This is a value between 0 and 100000.
     * Only used, if "sizeRelVFrom" is not 'none'.
     */
    pctHeight: { def: 100000 },

    /**
     * Relative horizontal offset of a drawing in percent of the specified "sizeRelHFrom", for example the "page"
     * Only used, if "sizeRelHFrom" is not 'none'.
     */
    pctPosHOffset: { def: 'none' },

    /**
     * Relative vertical offset of a drawing in percent of the specified "sizeRelVFrom", for example the "page"
     * Only used, if "sizeRelVFrom" is not 'none'.
     */
    pctPosVOffset: { def: 'none' }
};

// values for the 'textWrapMode' attribute allowing to wrap the text around the drawing
const WRAPPED_TEXT_VALUES = /^(square|tight|through)$/;

// private global functions ===============================================

/**
 * Returns whether the passed 'textWrapMode' attribute allows to wrap the
 * text around the drawing.
 */
function isTextWrapped(textWrapMode) {
    return WRAPPED_TEXT_VALUES.test(textWrapMode);
}

// class TextDrawingStyles ================================================

/**
 * Contains the style sheets for drawing formatting attributes. The CSS
 * formatting will be read from and written to drawing elements of any type.
 *
 * @param {TextModel} docModel
 *  The text document model containing instance.
 */
class TextDrawingStyles extends DrawingStyleCollection {

    constructor(docModel) {

        // base constructor
        super(docModel, { families: ['changes'] });

        // initialization -----------------------------------------------------

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily('drawing', DEFINITIONS);

        // register the formatting handler for DOM elements
        this.registerFormatHandler(this.#updateDrawingFormatting);
    }

    // private methods ----------------------------------------------------

    /**
     * Helper function that calculates the horizontal offset for absolute positioned drawings.
     * This function is not used for in line drawings.
     *
     * @param {Object} pageLayout
     *  The object handling the layout of the pages.
     *
     * @param {Object} drawingAttributes
     *  A map with drawing attributes, as name/value pairs.
     *
     * @param {Boolean} isDrawingInDrawingLayer
     *  Whether the drawing is a node inside the drawing layer. This is the case for all drawings
     *  that are anchored to the page or the paragraph.
     *
     * @param {Number} paraWidth
     *  The width of the paragraph in 1/100 mm.
     *
     * @param {Number} drawingWidth
     *  The width of the drawing in 1/100 mm.
     *
     * @param {jQuery} paragraph
     *  The paragraph node that is the parent of the drawing, as jQuery object.
     *
     * @param {jQuery} pageNode
     *  The page node node as jQuery object.
     *
     * @param {Number} zoomFactor
     *  The current zoom factor in percent.
     *
     * @returns {Number}
     *  The horizontal offset in 1/100mm relative to the page or margin or paragraph
     */
    #calculateHorizontalOffset(pageLayout, drawingAttributes, isDrawingInDrawingLayer, paraWidth, drawingWidth, paragraph, pageNode, zoomFactor) {

        // the width of the page in 1/100 mm (handling of zoom level only for zoomFactor > 1, DOCS-4150)
        let pageWidth = zoomFactor > 1 ? Math.round(convertLengthToHmm(pageNode.outerWidth(true) / zoomFactor, 'px')) : Math.round(convertLengthToHmm(pageNode.outerWidth(true), 'px'));
        // the calculated left offset of the drawing in 1/100 mm
        let leftOffset = 0;
        // the width of the left margin in 1/100 mm
        let leftMarginWidth = pageLayout.getPageAttribute('marginLeft');
        // the width of the left paragraph margin
        let leftParagraphMarginWidth = 0;
        // the width of the right margin in 1/100 mm
        let rightMarginWidth = 0;
        // the left padding of the page
        let pageLeftPadding = 0;
        // an additional right margin might be set for comments at the page node
        const commentMargin = pageNode.hasClass(COMMENTMARGIN_CLASS) ? pageNode.data(COMMENTMARGIN_CLASS) : 0;
        // the left margin of the page
        let cssMarginLeft = 0;
        // the right margin of the page
        let cssMarginRight = 0;

        if (commentMargin) { // the comment pane is visible -> this has an effect on the page width, because a right margin shifts the page to the left
            if (zoomFactor >= 1) {
                pageWidth -= Math.round(convertLengthToHmm(commentMargin, 'px')); // simply reduce width of page by width of comment pane, because all margins are positive
            } else {
                // special handling for negative margin on the left and positive margin on the right (happens at zoomFactor 0.75)
                cssMarginRight = parseFloat(pageNode.css('margin-right'));
                if (cssMarginRight > 0) {
                    cssMarginLeft = parseFloat(pageNode.css('margin-left'));
                    if (cssMarginLeft < 0) { pageWidth -= Math.round(convertLengthToHmm(Math.abs(cssMarginRight + cssMarginLeft), 'px')); }
                }
            }
        }

        // drawing horizontally aligned to column or margin
        if ((drawingAttributes.anchorHorBase === 'column') || (drawingAttributes.anchorHorBase === 'margin') || (drawingAttributes.anchorHorBase === 'rightMargin')) {

            if (isDrawingInDrawingLayer) {  // drawing is located in the drawing layer

                switch (drawingAttributes.anchorHorAlign) {
                    case 'center':
                        rightMarginWidth = pageLayout.getPageAttribute('marginRight');
                        leftOffset = leftMarginWidth + (pageWidth - leftMarginWidth - rightMarginWidth - drawingWidth) / 2;
                        break;
                    case 'right':
                        rightMarginWidth = pageLayout.getPageAttribute('marginRight');
                        leftOffset = pageWidth - rightMarginWidth - drawingWidth;
                        break;
                    case 'left':
                        leftOffset = leftMarginWidth;
                        break;
                    case 'offset':
                        leftOffset = drawingAttributes.anchorHorOffset;
                        // increasing the left offset by the distance between page and paragraph
                        // Info: The paragraph can be located anywhere, for example in a text frame positioned at the page (35146)
                        // TODO: Check for: leftMarginWidth = Math.round(convertLengthToHmm(Position.getPixelPositionToRootNodeOffset(pageNode, paragraph, zoomFactor * 100).x, 'px') / zoomFactor);
                        // if the paragraph has an additional left margin set as css attribute, this need to be handled, too (might come from a style ID (35146)
                        if (paragraph) { leftMarginWidth -= convertCssLengthToHmm(paragraph.css('margin-left')); }
                        leftOffset += leftMarginWidth;
                        if (drawingAttributes.anchorHorBase === 'rightMargin') { leftOffset += paraWidth; } // 64497
                        if (leftOffset + drawingWidth > pageWidth) { leftOffset = pageWidth - drawingWidth; }
                        break;
                    default:
                        leftOffset = 0;
                }

            } else {
                switch (drawingAttributes.anchorHorAlign) {
                    case 'center':
                        leftOffset = (paraWidth - drawingWidth) / 2;
                        break;
                    case 'right':
                        leftOffset = paraWidth - drawingWidth;
                        break;
                    case 'left':
                        leftOffset = drawingAttributes.anchorHorOffset;
                        break;
                    case 'offset':
                        leftOffset = drawingAttributes.anchorHorOffset;
                        if (drawingAttributes.anchorHorBase === 'rightMargin') { leftOffset = paraWidth + drawingAttributes.anchorHorOffset; }
                        break;
                    default:
                        leftOffset = 0;
                }

                // Taking left margin for indent first line into account (58090)
                if (paragraph) {
                    leftParagraphMarginWidth = convertCssLengthToHmm(paragraph.css('margin-left'));
                    if (leftParagraphMarginWidth) { leftOffset -= leftParagraphMarginWidth; }
                }

                if (this.docApp.isODF()) { // In ODT the drawing is shifted into the document, if it is partly outside the page on the right side (61266)
                    pageLeftPadding = convertCssLengthToHmm(pageNode.css('padding-left'));
                    if (drawingWidth <= pageWidth && (pageLeftPadding + leftOffset + drawingWidth > pageWidth) && paragraph && isPageContentNode(paragraph.parent())) {
                        leftOffset -= (pageLeftPadding + leftOffset + drawingWidth - pageWidth); // simply reducing leftOffset by the overlap on the right side
                    }
                }
            }

        // drawing horizontally aligned to page
        } else if (drawingAttributes.anchorHorBase === 'page') {

            if (isDrawingInDrawingLayer) {  // drawing is located in the drawing layer (and not in header or footer)

                switch (drawingAttributes.anchorHorAlign) {
                    case 'center':
                        leftOffset = (pageWidth - drawingWidth) / 2;
                        break;
                    case 'right':
                        leftOffset = pageWidth - drawingWidth;
                        break;
                    case 'offset':
                        leftOffset = drawingAttributes.anchorHorOffset;
                        break;
                    default:
                        leftOffset = 0;
                }

            } else {  // the drawing will be positioned inside its paragraph

                switch (drawingAttributes.anchorHorAlign) {
                    case 'center':
                        leftOffset = (paraWidth - drawingWidth) / 2;
                        break;
                    case 'right':
                        leftOffset = pageWidth - leftMarginWidth - drawingWidth;
                        break;
                    case 'left':
                        leftOffset = -leftMarginWidth;
                        break;
                    case 'offset':
                        // reducing the left offset by the distance between page and paragraph
                        leftOffset = drawingAttributes.anchorHorOffset;
                        if (drawingAttributes.anchorHorOffset + drawingWidth > pageWidth) { leftOffset = pageWidth - drawingWidth; }
                        // decreasing the left offset by the distance between page and paragraph (TODO: zoomFactor?)
                        leftMarginWidth = Math.round(convertCssLengthToHmm(pageNode.css('padding-left')) * zoomFactor);
                        leftOffset -= leftMarginWidth;  // reducing offset by the left page margin
                        break;
                    default:
                        leftOffset = 0;
                }
            }

        // drawing horizontally aligned to character
        } else if (drawingAttributes.anchorHorBase === 'character') {
            // Also 'character' looks better, if it is handled like 'column' (33276). The value for 'anchorHorOffset'
            // is not relative to the character, but to the paragraph.
            switch (drawingAttributes.anchorHorAlign) {
                case 'center':
                    leftOffset = (paraWidth - drawingWidth) / 2;
                    break;
                case 'right':
                    leftOffset = paraWidth - drawingWidth;
                    break;
                case 'offset':
                    leftOffset = drawingAttributes.anchorHorOffset;
                    break;
                default:
                    leftOffset = 0;
            }
        } else {
            // Other yet unsupported anchor bases
            leftOffset = 0;
        }

        return Math.ceil(leftOffset);
    }

    /**
     * Calculating the vertical offset for a drawing that is anchored to the paragraph.
     *
     * @param {Object} drawingAttributes
     *  A map with drawing attributes, as name/value pairs.
     *
     * @param {jQuery} paragraph
     *  The paragraph node that is the parent of the drawing, as jQuery object.
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     *
     * @param {Number} zoomFactor
     *  The current zoom factor in percent.
     *
     * @returns {Number}
     *  The vertical offset in 1/100mm relative to the paragraph.
     */
    #calculateParagraphTopOffset(drawingAttributes, paragraph, drawing, zoomFactor) {

        // the calculated top offset of the drawing (in 1/100 mm relative to the paragraph)
        let topOffset = 0;
        // the neighboring text span behind (preferred) or before the drawing
        let textSpan = findNextSiblingNode(drawing, 'span');
        // a position object with keys x and y in pixel
        let pos = null;

        if (drawingAttributes.anchorVertAlign === 'offset') {

            if (drawingAttributes.anchorVertBase === 'line') {
                topOffset = drawingAttributes.anchorVertOffset < 0 ? 0 : drawingAttributes.anchorVertOffset; // no negative values for 'line' (see Exercise03 document)
            } else {
                topOffset = drawingAttributes.anchorVertOffset; // allowing negative values
            }

        } else if (drawingAttributes.anchorVertBase === 'line') {

            // automatic alignment (top/bottom/center) is not possible for the vertical anchor
            // base 'paragraph', but for 'line'. In this case the drawing is aligned to the upper
            // border of the text line.

            textSpan = findNextSiblingNode(drawing, 'span');

            if (textSpan) {
                // receiving the pixel position of the upper left corner of the text span relative to the paragraph
                pos = getPixelPositionToRootNodeOffset(paragraph, textSpan, zoomFactor * 100);
            } else {
                // trying to get the previous text span, but that might be a multi line span. Therefore the lower right
                // corner need to be used and then the font-size can be added
                textSpan = findPreviousSiblingNode(drawing, 'span');

                if (textSpan) {

                    pos = getPixelPositionToRootNodeOffset(paragraph, textSpan, zoomFactor * 100);

                    if (pos && pos.y && isMultiLineTextSpan(textSpan)) {
                        pos.y += ($(textSpan).height() - convertCssLength($(textSpan).css('font-size'), 'px', 1));
                    }
                }
            }

            // ... and finally the offset can be calculated
            // -> TODO: The space maker node shifts the text span downwards, so that the pos.y is no longer precise.
            if (pos && pos.y) {
                if (drawingAttributes.anchorVertAlign === 'top') {
                    topOffset = convertLengthToHmm(pos.y, 'px', 1);
                } else if (drawingAttributes.anchorVertAlign === 'bottom') {
                    topOffset = convertLengthToHmm(pos.y - drawing.height(), 'px', 1);
                } else {
                    topOffset = convertLengthToHmm(pos.y - 0.5 * drawing.height(), 'px');
                }
            }

            // no support of drawings above the paragraph
            if (topOffset < 0) { topOffset = 0; }
        } else {
            // fallback must be generated, if anchorVertOffset is specified
            if (_.isNumber(drawingAttributes.anchorVertOffset)) {
                topOffset = drawingAttributes.anchorVertOffset;
            }
        }

        return Math.ceil(topOffset);
    }

    /**
     * Helper function to save the drawing attributes for the margin directly at the
     * drawing node. This is useful for performance reasons. It needs to be fast to
     * position drawings absolutely and to find intersecting space maker nodes.
     *
     * Info: As long as it is not possible to modify the drawing margin in OX Text,
     * it is sufficient to set the margin at the node once.
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     *
     * @param {Object} drawingAttributes
     *  A map with drawing attributes, as name/value pairs.
     */
    #saveMarginsAtDrawing(drawing, drawingAttributes) {

        if (!drawing.data(DRAWING_MARGINS_HMM)) {
            drawing.data(DRAWING_MARGINS_HMM, {
                marginTop: drawingAttributes.marginTop,
                marginBottom: drawingAttributes.marginBottom,
                marginLeft: drawingAttributes.marginLeft,
                marginRight: drawingAttributes.marginRight
            });
        }

        if (!drawing.data(DRAWING_MARGINS_PIXEL)) {
            drawing.data(DRAWING_MARGINS_PIXEL, {
                marginTop: convertHmmToLength(drawingAttributes.marginTop, 'px', 1),
                marginBottom: convertHmmToLength(drawingAttributes.marginBottom, 'px', 1),
                marginLeft: convertHmmToLength(drawingAttributes.marginLeft, 'px', 1),
                marginRight: convertHmmToLength(drawingAttributes.marginRight, 'px', 1)
            });
        }
    }

    /**
     * Calculating the vertical offset for a drawing that is anchored to the page or margin.
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     *
     * @param {Object} drawingAttributes
     *  A map with drawing attributes, as name/value pairs.
     *
     * @param {Object} pageLayout
     *  The object handling the layout of the pages.
     *
     * @param {jQuery} pageNode
     *  The root node containing the document model and representation, as jQuery object.
     *
     * @param {Boolean} inFooter
     *  Whether the drawing is located inside the footer node.
     *
     * @returns {Object}
     *  An object with two properties:
     *    topOffset: The vertical offset in 1/100 mm relative to the page or margin.
     *    isBottomOffset: Whether the value of the property 'topOffset' is related to
     *                    the bottom of the page
     *                    -> this is only used in footers to increase the precision,
     *                       because page height is not required if a drawing in the
     *                       footer is aligned to the page.
     */
    #calculatePageTopOffset(drawing, drawingAttributes, pageLayout, pageNode, inFooter) {

        // the calculated top offset of the drawing (in pixel relative to the page)
        let topOffset = 0;
        // whether the calculated top offset is related to the bottom (of the footer)
        let isBottomOffset = false;

        if (drawingAttributes.anchorVertAlign === 'offset') {

            // offset to 'margin' or 'bottomMargin' & 'page' can be negative
            topOffset = drawingAttributes.anchorVertOffset;

            // -> special handling for drawings in footers aligned to the page
            if (inFooter && drawingAttributes.anchorVertBase === 'page') {
                // this is a drawing in the footer
                // -> the offset from the bottom of the page is the height of the page minus
                //    the specified vertical offset minus the height of the drawing.
                topOffset = pageLayout.getPageAttribute('height') - topOffset - convertLengthToHmm(drawing.height(), 'px');
                // the calculated offset relates to the bottom of the page (this works only in footer nodes)
                isBottomOffset = true;
                // no negative top offsets in footer
                if (topOffset < 0) { topOffset = 0; }
            }

            // Info: For the type 'margin' the margin height is not handled here, because it is dynamic.
            // Word uses the height of the header, that is a dynamic value. Therefore only the value
            // of 'anchorVertOffset' is saved at the node. The precise calculation happens inside
            // drawingLayer.calculateVerticalPageOffset().

        } else {
            // automatic alignment (top/bottom/center/...)
            if (drawingAttributes.anchorVertBase === 'page') {
                if (drawingAttributes.anchorVertAlign === 'top') {
                    topOffset = 0;
                } else if (drawingAttributes.anchorVertAlign === 'bottom') {
                    topOffset = pageLayout.getPageAttribute('height') - convertLengthToHmm(drawing.height(), 'px');
                    if (inFooter) {
                        topOffset = 0;
                        isBottomOffset = true;
                    }
                } else if (drawingAttributes.anchorVertAlign === 'center') {
                    topOffset = (pageLayout.getPageAttribute('height') - convertLengthToHmm(drawing.height(), 'px')) / 2;
                } else {
                    topOffset = 0;
                }
            }

            // alignment at 'margin' (top/center/bottom) will be handled dynamically in 'drawingLayer.setAbsoluteDrawingPosition'
            // -> this is necessary, because height of header or footer changes.
        }

        return { topOffset: Math.ceil(topOffset), isBottomOffset };
    }

    /**
     * Setting the maximum width for a drawing. This needs to be adapted, if the drawing gets
     * a new anchor. If it was anchored to page before and is now inline or anchored to paragraph,
     * the size needs to be reduced.
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map with the merged drawing attributes, as name/value pairs.
     *
     * @param {Number} drawingWidth
     *  The width of the drawing in 1/100 mm.
     *
     * @param {Number} borderWidth
     *  The width of the drawing of the border in 1/100 mm.
     *
     * @param {Number} paraWidth
     *  The width of the paragraph in 1/100 mm.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.useParagraphWidth=true]
     *      If set to true, the width of the paragraph is used as maximal width. Otherwise the
     *      width of the page is used.
     *
     * @returns {Number}
     *  The new width of the drawing in 1/100mm
     */
    #setMaximumDrawingWidth(drawing, mergedAttributes, drawingWidth, borderWidth, paraWidth, options) {

        // whether the paragraph or the page width is used
        const useParagraphWidth = getBooleanOption(options, 'useParagraphWidth', true);
        // the maximum allowed width (no limit for drawings that are page aligned (61802))
        const maxWidth = useParagraphWidth ? paraWidth : drawingWidth;
        // the image attributes of the passed attribute map
        const imageAttributes = mergedAttributes.image;
        // the new drawing width, handling width of paragraph or page
        let newDrawingWidth = drawingWidth;

        // reducing width of drawing to width of paragraph (not page!), if no left or right cropping is enabled (Task 30982)
        if ((drawingWidth > maxWidth) && (imageAttributes.cropLeft === 0) && (imageAttributes.cropRight === 0)) {
            newDrawingWidth = maxWidth;
            drawing.width(convertHmmToCssLength(newDrawingWidth - borderWidth, 'px', 1));
        }

        return newDrawingWidth;
    }

    /**
     * Sets the width of the drawing.
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     *
     * @param {Object} attributes
     *  A map with the merged drawing attributes, as name/value pairs.
     */
    #setDrawingWidth(drawing, attributes) {

        if (isGroupedDrawingFrame(drawing)) { return; }

        // bugfix :: BUG#45141 :: https://bugs.open-xchange.com/show_bug.cgi?id=45141 :: Vertical drawing object is shown diagonally
        const width = attributes.drawing.width;
        if (_.isString(width)) {
            const value = parseInt(width, 10);
            const tuple = width.split(value);
            const unit  = (tuple.length === 2) ? $.trim(tuple[1]) : '';

            if (unit === '%') {
                drawing.width([value, unit].join(''));
            }/* else if (unit !== '') {
                element.width(convertLength(value, unit, 'px', 1));
            }*/
        } else {
            drawing.width(convertHmmToLength(width, 'px', 1));
        }
    }

    /**
     * Sets the height of the drawing.
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     *
     * @param {Object} attributes
     *  A map with the merged drawing attributes, as name/value pairs.
     */
    #setDrawingHeight(drawing, attributes) {

        // whether the height of the drawing shall be set
        // -> no setting of height for grouped drawings and for shapes with property autoResizeHeight
        let setHeight = !(isGroupedDrawingFrame(drawing) || attributes.shape.autoResizeHeight);

        // but height must be fix for drawings with autoResizeHeight and vertical text direction (62977)
        if (attributes.shape.vert === 'vert270') { setHeight = true; }

        // in ODT the default style for drawings has the value of the property 'autoResizeHeight' set to true. Therefore an additional check is required (56526).
        if (!setHeight && this.docApp.isODF() && attributes.shape.autoResizeHeight && !isGroupedDrawingFrame(drawing) && !isShapeDrawingFrame(drawing)) { setHeight = true; }

        if (setHeight) {
            drawing.height(convertHmmToLength(attributes.drawing.height, 'px', 1));
        } else if (attributes.shape.autoResizeHeight) {
            drawing.height(''); // if 'autoResizeHeight' is set, an explicit height must be removed
        }
    }

    /**
     * Helper function to update attributes of children of drawings. This is only used for text frames yet.
     *
     * @param {jQuery|Node} drawing
     *  The drawing node as jQuery object, whose children need an update of element formatting.
     */
    #handleChildAttributes(drawing) {

        // the explicit drawing attributes
        const explicitDrawingAttrs = getExplicitAttributeSet(drawing);

        // checking the fill color (task 36385)
        if (explicitDrawingAttrs?.fill) {

            const textFrameNode = getTextFrameNode(drawing);

            iterateSelectedDescendantNodes(textFrameNode, PARAGRAPH_NODE_SELECTOR, paragraph => {
                this.docModel.paragraphStyles.updateElementFormatting(paragraph);
            }, undefined, { children: true });
        }
    }

    /**
     * Handling drawing that are in-line with text. This is especially important, if the drawing
     * was until yet no in-line drawing. In this case some clean up at the node is required.
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map with the merged drawing attributes, as name/value pairs.
     *
     * @param {Number} drawingWidth
     *  The width of the drawing in 1/100 mm.
     *
     * @param {Number} borderWidth
     *  The width of the drawing of the border in 1/100 mm.
     *
     * @param {Number} paraWidth
     *  The width of the paragraph in 1/100 mm.
     *
     * @param {Number} halfBorderWidth
     *  The half width of the drawings border in 1/100 mm.
     *
     * @param {Boolean} forcedInlineMode
     *  Whether the drawing is forced into inline mode (small devices or enabled draft mode).
     */
    #handleInlineDrawing(drawing, mergedAttributes, drawingWidth, borderWidth, paraWidth, halfBorderWidth, forcedInlineMode) {

        // the drawing layer for absolutely positioned drawings
        let drawingLayer = null;

        if (drawing.hasClass('inline') && !forcedInlineMode && !this.docModel.isImportFinished()) {
            // new inserted drawing in the loading phase
            this.#setMaximumDrawingWidth(drawing, mergedAttributes, drawingWidth, borderWidth, paraWidth, { useParagraphWidth: true });
        } else if (!drawing.hasClass('inline') || forcedInlineMode) {

            // switch from floating to inline mode
            drawingLayer = this.docModel.getDrawingLayer();

            // reducing drawing width if required
            this.#setMaximumDrawingWidth(drawing, mergedAttributes, drawingWidth, borderWidth, paraWidth, { useParagraphWidth: true });

            // removing an optional space maker node (for header/footer this must happen, before 'absolute' is removed)
            // -> this is at this position only required for drawings anchored to the paragraph.
            drawingLayer.removeSpaceMakerNode(drawing);

            // removing the drawing from the model containers ('paragraphDrawingCollectorUpdateAlways' and 'paragraphDrawingCollector', 43851)
            drawingLayer.removeDrawingFromModelContainer(drawing);

            // removing an optionally existing z-index, inline drawings are in background
            drawing.css('zIndex', '');
            drawing.removeAttr('layer-order');

            // Word uses fixed predefined margins in in line mode, we too?
            drawing.removeClass('float left right absolute').addClass('inline');
            // ignore other attributes in in line mode

            // whether the drawing needs to be removed from the drawing layer
            if (isDrawingLayerNode(drawing.parent())) {
                drawingLayer.setAbsoluteDrawingPosition(drawing, { doClean: true });
                drawingLayer.shiftDrawingAwayFromDrawingLayer(drawing);

                // the selection needs to be repainted. Otherwise it is possible
                // to move the drawing to the page borders.
                clearSelection(drawing);
                // BUG 36372: Text frame gets blue resize/selection-border wrongly (via undo)
                //  previously check if the drawing was selected before pressing undo
                if (this.docModel.getSelection().isDrawingSelected(drawing)) {
                    drawDrawingSelection(this.docApp, drawing);
                }
            } else {
                // Handling of absolute drawings anchored at paragraph
                drawingLayer.setAbsoluteDrawingParagraphPosition(drawing, { doClean: true });
                // checking paragraph z-index, because the drawing was paragraph aligned before
                if (this.docModel.isIncreasedDocContent()) { handleParagraphIndex(drawing.parent()); }
            }
        }

        // setting margin to in-line drawings. This is necessary, because a thick border drawing with
        // canvas might overlap the surrounding text.
        drawing.css({ margin: convertHmmToCssLength(halfBorderWidth, 'px', 1) });
    }

    /**
     * Handling drawing that are absolute positioned. This are all drawings that are not in-line
     * with text. This is especially important, if the drawing anchor was modified. In this case
     * some clean up at the node is required.
     *
     * @param {jQuery} drawing
     *  The drawing node as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map with the merged drawing attributes, as name/value pairs.
     *
     * @param {Number} drawingWidth
     *  The width of the drawing in 1/100 mm.
     *
     * @param {Number} borderWidth
     *  The width of the drawing of the border in 1/100 mm.
     *
     * @param {Number} paraWidth
     *  The width of the paragraph in 1/100 mm.
     *
     * @param {Boolean} inHeaderFooter
     *  Whether the drawing is located inside a header or footer node.
     *
     * @param {Number} zoomFactor
     *  The current zoom factor in percent.
     */
    #handleAbsoluteDrawing(drawing, mergedAttributes, drawingWidth, borderWidth, paraWidth, inHeaderFooter, zoomFactor) {

        // the page layout object
        const pageLayout = this.docModel.getPageLayout();
        // the page node
        const pageNode = this.docModel.getNode();
        // the drawing layer for absolutely positioned drawings
        const drawingLayer = this.docModel.getDrawingLayer();
        // the paragraph element containing the drawing node
        // -> this can also be a drawing group without paragraphs
        const paragraph = drawing.parent();
        // the drawing attributes of the passed attribute map
        const drawingAttributes = mergedAttributes.drawing;
        // offset from top/left/right margin of paragraph element, in 1/100 mm
        let topOffset = 0;
        let leftOffset = 0;
        // whether the drawing is positioned absolutely to the page and is shifted into the drawing layer
        let isDrawingInDrawingLayer = false;
        // the horizontal and vertical offsets in pixel relative to the page
        let topValue = 0;
        let leftValue = 0;
        // the header or footer root node for the drawing
        let targetRootNode = null;
        // whether the drawing is vertically aligned to the margin
        let isVerticalMarginAligned = false;
        // whether the drawing is vertically aligned to the bottom margin
        let useBottomMargin = false;
        // a helper object containing information about the vertical offset
        let topOffsetObject = null;
        // whether the drawing is inside the footer node
        let isFooterNodeLocal = false;

        // calculate top offset (only if drawing is anchored to paragraph or to line).
        // Anchor to 'line' means the base line of the character position. The drawings with these anchor
        // types are absolutely positioned inside a paragraph.
        if ((drawingAttributes.anchorVertBase === 'paragraph') || (drawingAttributes.anchorVertBase === 'line')) {

            drawing.removeClass('inline float left right').addClass('absolute');  // absolute positioned!

            // whether the drawing needs to be removed from the drawing layer
            // -> for the following calculations, the drawing must be located inside the paragraph
            if (isDrawingLayerNode(paragraph)) {
                drawingLayer.setAbsoluteDrawingPosition(drawing, { doClean: true });
                drawingLayer.shiftDrawingAwayFromDrawingLayer(drawing);
            }

            // calculating the top offset of the drawing in relation to the page or margin
            topOffset = this.#calculateParagraphTopOffset(drawingAttributes, paragraph, drawing, zoomFactor);

            // checking paragraph z-index, because the drawing might have gotten a space maker node
            if (isParagraphNode(paragraph) && this.docModel.isIncreasedDocContent()) { handleParagraphIndex(paragraph); }

            // paragraph aligned drawings that are positioned behind the document must have a negative z-index
            if (drawingAttributes.anchorBehindDoc && (drawing.css('zIndex') === '0' || !drawing.css('zIndex'))) { drawing.css('zIndex', -1); }

        } else if (drawingAttributes.anchorVertBase === 'page' || drawingAttributes.anchorVertBase === 'margin' || drawingAttributes.anchorVertBase === 'bottomMargin' || drawingAttributes.anchorVertBase === 'topMargin') {

            // setting position 'absolute' to all drawings that have a vertical offset corresponding to the page
            isDrawingInDrawingLayer = true;

            // setting the header or footer target node for the drawing
            if (inHeaderFooter) { targetRootNode = getMarginalTargetNode(pageNode, drawing); }

            // if inHeaderFooter is true and targetRootNode is found, it might be better to shift only
            // those drawings into the drawing layer, that are children of the 'div.header-footer-placeholder'.
            // All other drawing layers will be removed soon and replaced by these header and footer
            // templates, that are located inside 'div.header-footer-placeholder'.

            // whether the drawing needs to be moved to the drawing layer
            if (isParagraphNode(drawing.parent())) { drawingLayer.shiftDrawingIntoDrawingLayer(drawing, targetRootNode); }

            drawing.removeClass('inline float left right').addClass('absolute');  // absolute positioned!

            // marking this drawing as vertically aligned to the margin
            if (drawingAttributes.anchorVertBase === 'margin' || drawingAttributes.anchorVertBase === 'bottomMargin') { isVerticalMarginAligned = true; }
            if (drawingAttributes.anchorVertBase === 'bottomMargin') { useBottomMargin = true; }

            if (targetRootNode) { isFooterNodeLocal = isFooterNode(targetRootNode); }

            // calculating the top offset of the drawing in relation to the page or margin
            topOffsetObject = this.#calculatePageTopOffset(drawing, drawingAttributes, pageLayout, pageNode, isFooterNodeLocal);
            topOffset = topOffsetObject.topOffset;

            // checking paragraph z-index, because the drawing might have been paragraph aligned before
            if (isParagraphNode(paragraph) && this.docModel.isIncreasedDocContent()) { handleParagraphIndex(paragraph); }

            // page aligned drawings that are positioned behind the document must have z-index 0
            if (drawingAttributes.anchorBehindDoc) { drawing.css('zIndex', 0); }
        }

        // reducing drawing width if required (without margin)
        drawingWidth = this.#setMaximumDrawingWidth(drawing, mergedAttributes, drawingWidth, borderWidth, paraWidth, { useParagraphWidth: false });

        // calculate left/right offset (required before setting wrap mode)
        leftOffset = this.#calculateHorizontalOffset(pageLayout, drawingAttributes, isDrawingInDrawingLayer, paraWidth, drawingWidth, paragraph, pageNode, zoomFactor);

        leftValue = convertHmmToLength(leftOffset, 'px', 0.01);
        topValue = convertHmmToLength(topOffset, 'px', 0.01);

        // setting calculated values at the drawing element
        if (isDrawingInDrawingLayer) {
            // the topValue is the distance in pixel to the top page border
            drawingLayer.setAbsoluteDrawingPosition(drawing, { leftValue, topValue, newValues: true, isBottomOffset: topOffsetObject.isBottomOffset, targetNode: targetRootNode, isMarginalNode: inHeaderFooter, isFooterNode: isFooterNodeLocal, verticalMargin: (isVerticalMarginAligned ? drawingAttributes.anchorVertAlign : ''), useBottomMargin });

            drawingLayer.updateCropDrawing(drawing, drawingAttributes);
        } else {
            drawingLayer.setAbsoluteDrawingParagraphPosition(drawing, { leftValue, topValue, textWrapMode: drawingAttributes.textWrapMode, textWrapSide: drawingAttributes.textWrapSide, newValues: true, targetNode: targetRootNode });
            // cache for drawings (and paragraphs) to update only selected elements
            drawingLayer.registerUpdateElement(drawing);

            if (inHeaderFooter) {
                drawingLayer.updateCropMarginalParagraphDrawing(drawing, drawingAttributes);
            } else {
                drawingLayer.resetCropDrawing(drawing);
            }
        }

        // removing a margin, no margin at absolutely positioned drawings.
        // A margin must be handled by space maker node. It might have been set at inline drawing before.
        drawing.css('margin', '');

        // saving margins directly at the drawing (for performance reasons)
        this.#saveMarginsAtDrawing(drawing, drawingAttributes);

        this.#handleLayerOrder(drawing, inHeaderFooter, drawingAttributes);
    }

    /**
     * Will be called for every drawing node whose attributes have been changed. Repositions
     * and reformats the drawing according to the passed attributes.
     *
     * @param {jQuery} drawing
     *  The drawing node whose attributes have been changed, as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute family, containing the
     *  effective attribute values merged from style sheets and explicit attributes.
     */
    #updateDrawingFormatting(drawing, mergedAttributes) {

        if (this.docApp.isInQuit()) { return; }

        // the drawing attributes of the passed attribute map
        const drawingAttributes = mergedAttributes.drawing;
        // the current zoom factor
        const zoomFactor = this.docApp.getView().getZoomFactor();
        // the paragraph element containing the drawing node
        // -> this can also be a drawing group without paragraphs
        const paragraph = drawing.parent();
        // total width of the paragraph, in 1/100 mm
        const paraWidth = convertLengthToHmm(paragraph.width(), 'px');
        // the page node
        const pageNode = this.docModel.getNode();
        // whether the drawing is located in header or footer
        const inHeaderFooter = isMarginalNode(drawing) || getMarginalTargetNode(pageNode, drawing).length > 0;
        // the width of the border of the drawing
        const borderWidth = mergedAttributes.line.width;
        // the half width of the border of the drawing
        // half border width goes only for text frames, full width for images; cannot be less than 1px (or 26.4 hmm), to be compatible with canvas border, #36576
        const halfBorderWidth = isTextFrameShapeDrawingFrame(drawing) ? ((0.5 * borderWidth) > 26.5 ? (0.5 * borderWidth) : 26.5) : borderWidth;

        // current drawing width, in 1/100 mm, without margin
        // -> since the border is drawn with a canvas, the drawing width (including border) needs to be adjusted
        // -> the canvas draws half of the border width outside and half of it inside the drawing.
        this.#setDrawingWidth(drawing, mergedAttributes);
        const drawingWidth = convertLengthToHmm(drawing.width(), 'px') + borderWidth;
        // Setting the height of the drawing
        this.#setDrawingHeight(drawing, mergedAttributes);

        // marking drawings with relative size or position
        drawing.toggleClass('relativesize', !!(drawingAttributes.sizeRelHFrom || drawingAttributes.sizeRelVFrom));

        // nothing to do, if paragraph is a groupcontent object
        // -> in this case the group is responsible for the correct attributes
        if (isGroupContentNode(paragraph)) {
            drawing.removeClass('inline float left right').addClass('absolute grouped');  // absolute positioned drawing inside its group!
            // set generic formatting to the drawing frame
            updateFormatting(this.docApp, drawing, mergedAttributes); // TODO: Performance. Better not call updateFormatting here
            // simply do nothing, let the group make further attribute settings
            return;
        }

        // applying rotation
        const degrees = drawingAttributes.rotation || 0;
        updateCssTransform(drawing, degrees);
        drawing.toggleClass(ROTATED_DRAWING_CLASSNAME, degrees !== 0);

        // on small devices and on activated draft mode all drawings must be inline
        const forcedInlineMode = !drawingAttributes.inline && (SMALL_DEVICE || this.docModel.isDraftMode());
        drawing.toggleClass(FORCE_INLINE_CLASS, forcedInlineMode);

        // checking whether the content nodes need to be increased in z-index. This is the case, if there is a drawing with anchorBehindText set to true.
        if (mergedAttributes.drawing.anchorBehindDoc && !this.docModel.isIncreasedDocContent()) {
            this.docModel.getNode().addClass(INCREASED_CONTENT_CLASS); // the page gets the marker class for increased paragraphs or tables.
            // also all paragraphs with absolute positioned drawings might need an increased z-index
            // -> when the document is loaded, this can only happen, if this is offered by the OX Text GUI (currently only in debug mode)
            // -> in the loading phase all "previous" already formatted drawings are affected by this change to the increased content. The following drawing
            //    are not attached to the DOM yet (for performance reasons all top level paragraphs are detached from the DOM before formatting starts).
            //    Therefore searching for all absolute drawings in the loading phase only finds the correct (the affected) drawings (e2e "TEXT_SH-05").
            _.each(this.docModel.getNode().find('.p > .drawing.absolute'), oneDrawing => { handleParagraphIndex($(oneDrawing).parent(), oneDrawing); });
        }

        // setting position to 'inline'
        if (drawingAttributes.inline || forcedInlineMode) {
            this.#handleInlineDrawing(drawing, mergedAttributes, drawingWidth, borderWidth, paraWidth, halfBorderWidth, forcedInlineMode);
        } else {
            this.#handleAbsoluteDrawing(drawing, mergedAttributes, drawingWidth, borderWidth, paraWidth, inHeaderFooter, zoomFactor);
        }

        // change track attribute handling
        this.docModel.getChangeTrack().updateChangeTrackAttributes(drawing, mergedAttributes);

        // set generic formatting to the drawing frame
        updateFormatting(this.docApp, drawing, mergedAttributes);

        // update formatting of all paragraphs inside the text frame content node (36385)
        if (isTextFrameShapeDrawingFrame(drawing)) { this.#handleChildAttributes(drawing); }

        // set visibility of anchor
        if (this.docModel.getSelection().isDrawingSelected(drawing)) {
            this.docApp.getView().setVisibleDrawingAnchor(drawing, mergedAttributes);
        }
    }

    /**
     * Will be called for every absolute position drawing node whose attributes have been changed
     * and detects If "anchorLayerOrder" or "anchorBehindDoc" have changed.
     * If it is changed, z-index for all drawings on current page will be updated.
     *
     * @param {jQuery} drawing
     *  The drawing node whose attributes have been changed, as jQuery object.
     *
     * @param {Boolean} inHeaderFooter
     *  Whether the drawing is located inside a header or footer node.
     *
     * @param {Object} drawingAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute family, containing the
     *  effective attribute values merged from style sheets and explicit attributes.
     */
    #handleLayerOrder(drawing, inHeaderFooter, drawingAttributes) {

        const drawingEl = drawing[0];

        let order = drawingAttributes.anchorLayerOrder;
        if (drawingAttributes.anchorBehindDoc) { order -= 0xFFFFFFFF; } // stay in 32-bit range (61843)

        const layerOrder = drawingEl.getAttribute('layer-order');

        const oldOrder = layerOrder ? parseInt(layerOrder, 10) : 0;
        if (oldOrder !== order || (order === 0 && layerOrder === null)) { // also taking care of 'order === 0' (DOCS-3221)
            drawingEl.setAttribute('layer-order', order);

            // we return here, because after importFinish sorting is called in DrawingLayer on event 'update:absoluteElements'
            if (!this.docModel.getDrawingLayer().isImportFinished()) { return; }

            let startIndex = 0;
            let allDrawings = null;
            if (inHeaderFooter) {
                startIndex = 11;
                allDrawings = getAllAbsoluteDrawingsOnNode(getMarginalTargetNode(this.docModel.getNode(), drawing));
            } else {
                startIndex = 41;
                //begin at 41 because page-break has zIndex 40
                const pageNumber = getPageNumber(drawingEl, this.docModel);
                allDrawings = getAllAbsoluteDrawingsOnPage(this.docModel.getNode(), pageNumber);
            }
            sortDrawingsOrder(allDrawings, startIndex);
        }
    }

    // static methods ---------------------------------------------------------

    /**
     * Returns the drawing position identifier representing the passed drawing
     * attributes.
     *
     * @param {Object} attributes
     *  A map with drawing attributes, as name/value pairs.
     *
     * @returns {String|Null}
     *  The GUI drawing position identifier; or null, if any of the passed
     *  attributes is ambiguous.
     */
    static getPositionFromAttributes(attributes) {

        // inline mode overrules floating attributes
        if (_.isNull(attributes.inline)) { return null; }
        if (attributes.inline === true) { return 'inline'; }

        // filter unsupported position anchor modes
        if (!/^(page|margin|column)$/.test(attributes.anchorHorBase) || !/^(page|margin|paragraph|line)$/.test(attributes.anchorVertBase)) { return null; }

        // filter ambiguous or unsupported horizontal alignment types
        if (!/^(left|center|right)$/.test(attributes.anchorHorAlign)) { return null; }

        // filter ambiguous text wrapping modes
        if (_.isNull(attributes.textWrapMode) || _.isNull(attributes.textWrapSide)) { return null; }

        // build the resulting drawing position
        return attributes.anchorHorAlign + ':' + (isTextWrapped(attributes.textWrapMode) ? attributes.textWrapSide : 'none');
    }

    /**
     * Returns the drawing attributes that are needed to represent the passed
     * GUI drawing position.
     *
     * @param {String} position
     *  The GUI drawing position.
     *
     * @param {Object} [origAttributes]
     *  The original attributes of the target drawing object.
     *
     * @returns {Object}
     *  A map with drawing attributes, as name/value pairs.
     */
    static getAttributesFromPosition(position, origAttributes) {

        // extract alignment and text wrap mode
        const matches = position.match(/^([a-z]+):([a-z]+)$/);
        // resulting drawing attributes
        const drawingAttributes = {};
        // the horizontal anchor for the operation
        let newAnchorHorBase = null;

        // inline flag overrules all other attributes, no need to return any other (unfortunately it is still necessary for undo reasons)
        if (position === 'inline') {
            return {
                inline: true,
                anchorHorBase: null,
                anchorHorAlign: null,
                anchorHorOffset: null,
                anchorVertBase: null,
                anchorVertAlign: null,
                anchorVertOffset: null,
                textWrapMode: null,
                textWrapSide: null
            };
        }

        // check that passed position contains alignment and text wrapping mode
        if (!_.isArray(matches) || (matches.length < 3)) {
            globalLogger.warn('TextDrawingStyles.getAttributesFromPosition(): invalid drawing position: "' + position + '"');
            return {};
        }

        // set horizontal alignment
        newAnchorHorBase = (origAttributes.anchorHorBase === 'page' || origAttributes.anchorHorBase === 'margin') ? origAttributes.anchorHorBase : 'column';
        _(drawingAttributes).extend({ inline: false, anchorHorBase: newAnchorHorBase, anchorHorAlign: matches[1], anchorHorOffset: 0 });

        // set text wrapping mode
        if (matches[2] === 'none') {
            _(drawingAttributes).extend({ textWrapMode: 'topAndBottom', textWrapSide: null });
        } else {
            _(drawingAttributes).extend({ textWrapMode: 'square', textWrapSide: matches[2] });
        }

        return drawingAttributes;
    }

    /**
     * Returns whether the specified attribute set of drawing attributes describe
     * a drawing with no text wrapping around.
     *
     * @param {Object} drawingAttrs
     *  A map with drawing attributes, as name/value pairs.
     *
     * @returns {Boolean}
     *  Whether the specified attribute set of drawing attributes describe
     *  a drawing with no text wrapping.
     */
    static isNoWrapAttributeSet(drawingAttrs) {
        return drawingAttrs && drawingAttrs.textWrapMode === 'none';
    }

}

// exports ================================================================

export default TextDrawingStyles;
