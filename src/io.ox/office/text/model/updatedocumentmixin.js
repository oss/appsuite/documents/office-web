/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import { NODE_SELECTOR, CANVAS_NODE_SELECTOR, checkEmptyTextShape, isDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { CELLCONTENT_NODE_SELECTOR, DRAWINGPLACEHOLDER_NODE_SELECTOR, DRAWING_SPACEMAKER_NODE_SELECTOR,
    PARAGRAPH_NODE_SELECTOR, TABLE_NODE_SELECTOR, getPageContentNode, isExceededSizeTableNode, isImplicitParagraphNode, isParagraphNode,
    isTableNode } from '@/io.ox/office/textframework/utils/dom';
import {  isEmptyParagraph } from '@/io.ox/office/textframework/utils/position';
import { convertHmmToLength, getDefaultPaperSize } from '@/io.ox/office/textframework/utils/textutils';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';

// mix-in class UpdateDocumentMixin =======================================

/**
 * A mix-in class for the 'updateDocumentFormatting' function.
 */
export default function UpdateDocumentMixin(app) {

    // self reference for local functions
    var self = this;

    // private methods ----------------------------------------------------

    /**
     * Helper function to prepare the html content loaded from local storage before formatting
     * the complete document content. This is necessary, if the user gets additional operations
     * sent from the server during loading the document from local storage.
     */
    function prepareLocalStorageContent() {

        // the content node of the page
        var pageContentNode = getPageContentNode(self.getNode());
        // the page layout object
        var pageLayout = self.getPageLayout();

        // removing all existing space maker nodes, because they are not yet connected to their
        // corresponding drawings. Therefore after the full formatting there would be superfluous
        // space maker nodes.
        pageContentNode.find(DRAWING_SPACEMAKER_NODE_SELECTOR).remove();
        pageLayout.getHeaderFooterPlaceHolder().find(DRAWINGPLACEHOLDER_NODE_SELECTOR).remove();
    }

    /**
     * Load performance: Execute post process activities after loading the document from the
     * local storage.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the formatting has been
     *  updated successfully, or rejected when an error has occurred.
     */
    function updateDocumentFormattingStorage() {

        var // the page node
            editdiv = self.getNode(),
            // the page attributes object
            pageAttributes = self.pageStyles.getElementAttributes(editdiv),
            // the page layout object
            pageLayout = self.getPageLayout();

        // initialize default page formatting
        self.pageStyles.updateElementFormatting(editdiv);

        // handling page breaks correctly
        self.setPageAttributes(pageAttributes);
        self.setPageMaxHeight(convertHmmToLength(pageAttributes.page.height - pageAttributes.page.marginTop - pageAttributes.page.marginBottom, 'px', 1));
        self.setPagePaddingLeft(convertHmmToLength(pageAttributes.page.marginLeft, 'px', 1));
        self.setPagePaddingTop(convertHmmToLength(pageAttributes.page.marginTop, 'px', 1));
        self.setPagePaddingBottom(convertHmmToLength(pageAttributes.page.marginBottom, 'px', 1));
        self.setPageWidth(convertHmmToLength(pageAttributes.page.width, 'px', 1));

        // all canvas elements need to be rendered
        if (editdiv.find(CANVAS_NODE_SELECTOR).length > 0) {
            self.trigger('drawingHeight:update', editdiv.find(CANVAS_NODE_SELECTOR));
        }

        if (pageLayout.hasContentHeaderFooterPlaceHolder()) {
            pageLayout.updateStartHeaderFooterStyle();
            pageLayout.headerFooterCollectionUpdate();
        }

        pageLayout.callInitialPageBreaks({ triggerEvent: true }); // necessary always because of fields update. see #42606

        return $.when();
    }

    /**
     * In docx-documents there is no span inside empty paragraphs. Therefore the character attributes
     * are saved at the paragraph. When a document is loaded in OX Text, these character attributes must
     * be added to the span, so that for example the height of the paragraph is shown correctly.
     *
     * Adding the first content into the paragraph, the character attributes at the empty text span are
     * sent to the filter, so that the document is shown correctly (DOCS-4211).
     * See function model.collectCharacterAttributesForInsertOperation().
     *
     * @param {jQuery} paragraph
     *  The paragraph node.
     */
    function transferCharacterAttributesFromParagraphToSpan(paragraph) {
        const characterAttrs = getExplicitAttributes(paragraph, 'character');
        if (characterAttrs && !isImplicitParagraphNode(paragraph) && isEmptyParagraph(paragraph)) {
            // setting the character attributes to the empty text span inside the empty paragraph
            self.characterStyles.setElementAttributes(paragraph.children('span').first(), { character: characterAttrs });
        }
    }

    // public methods -----------------------------------------------------

    /**
     * Updates the formatting of all elements, after the import operations
     * of the document have been applied.
     *
     * @param {boolean} fromStorage
     *  Whether the document has been imported from local storage.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the formatting has been
     *  updated successfully, or rejected when an error has occurred.
     */
    this.updateDocumentFormatting = function (fromStorage) {

        // Load performance: Execute post process activities after loading the document from the local storage.
        if (fromStorage) { return updateDocumentFormattingStorage(); }

        var // the result deferred object
            def = this.createDeferred(),
            // the page node
            editdiv = this.getNode(),
            // the content node of the page
            pageContentNode = getPageContentNode(editdiv),
            // all top-level paragraphs, all tables (also embedded), and all drawings
            formattingNodes = pageContentNode.find('> ' + PARAGRAPH_NODE_SELECTOR + ', ' + NODE_SELECTOR + ', ' + TABLE_NODE_SELECTOR),
            // number of progress ticks for tables (1 tick per paragraph/drawing)
            TABLE_PROGRESS_TICKS = 20,
            // total number of progress ticks
            totalProgressTicks = formattingNodes.length + (TABLE_PROGRESS_TICKS - 1) * formattingNodes.filter(TABLE_NODE_SELECTOR).length,
            // current progress for formatting process
            currentProgressTicks = 0,
            // update progress bar at most 250 times
            progressBlockSize = totalProgressTicks / 250,
            // index of progress block from last progress update
            lastProgressBlock = -1,
            // the page layout object
            pageLayout = this.getPageLayout(),
            // marker for getting nodes for displaying immediately on document loading
            splitPoint,
            // header and footer container nodes needs to be updated also, if there is content inside them
            headerFooterFormattingNodes = pageLayout.getHeaderFooterPlaceHolder().find(PARAGRAPH_NODE_SELECTOR + ', ' + NODE_SELECTOR + ', ' + TABLE_NODE_SELECTOR),
            // fast display first page -> initialPageBreaks is called 2x, abort first promise when second is triggered
            initialPbPromise,
            // an array containing the formatting nodes. This array is required to guarantee, that the order of the formatting nodes is not modified
            formattingNodesArray = null,
            //
            splitListNodes, splitListNodesArray,
            //
            indexSplitPoint,
            // the page attributes object
            pageAttributes = this.pageStyles.getElementAttributes(editdiv);

        // in special cases, this function is called, even though the document was loaded from local storage
        // (additional operations were sent from the server). In this case some cleanup of the loaded strings
        // from local storage is required
        if (this.isLocalStorageImport()) { prepareLocalStorageContent(); }

        // Collect all paragraphs with drawings to recalculate after drawing calculation.
        var paragraphWithDrawingCollector = $();

        this.setPageAttributes(pageAttributes);
        this.setPageMaxHeight(convertHmmToLength(pageAttributes.page.height - pageAttributes.page.marginTop - pageAttributes.page.marginBottom, 'px', 1));
        this.setPagePaddingLeft(convertHmmToLength(pageAttributes.page.marginLeft, 'px', 1));
        this.setPagePaddingTop(convertHmmToLength(pageAttributes.page.marginTop, 'px', 1));
        this.setPagePaddingBottom(convertHmmToLength(pageAttributes.page.marginBottom, 'px', 1));
        this.setPageWidth(convertHmmToLength(pageAttributes.page.width, 'px', 1));

        // #27818 - On loading document with table, table style is missing for proper formatting
        if (app.isODF()) { self.tableStyles.insertMissingTableStyles(); }

        // first 90% for formatting (last 10% for updating lists)
        function updateFormatProgress(progressTicks) {
            var progressBlock = Math.floor(progressTicks / progressBlockSize);
            if (lastProgressBlock < progressBlock) {
                def.notify(0.9 * progressTicks / totalProgressTicks);
                lastProgressBlock = progressBlock;
            }
        }

        // updates the formatting of the passed element, returns a promise
        function updateElementFormatting(element) {

            var // the Promise for asynchronous element formatting
                promise = null,
                // whether the element is a direct child of the page content node
                topLevel = !element.parentNode;

            // convert element to jQuery object
            element = $(element);

            // insert the detached element into the page content node
            // (performance optimization for large documents, see below)
            if (topLevel) { pageContentNode.append(element); }

            // determine the type of the passed element
            if (isParagraphNode(element)) {

                // validate DOM contents AND update formatting of the paragraph
                self.validateParagraphNode(element);
                if (self.hasOoxmlTextCharacterFilterSupport()) { transferCharacterAttributesFromParagraphToSpan(element); }
                self.paragraphStyles.updateElementFormatting(element);
                updateFormatProgress(currentProgressTicks += 1);

                // Bug 56523: Tabs are calculated different dependent from loading process. This is because the
                // width of the drawings with activated fast-load are not calculated before. Done by collecting all
                // paragraphs with drawings to recalculate after drawings calculation
                if (element.children('div.drawing').length > 0) {
                    paragraphWithDrawingCollector = paragraphWithDrawingCollector.add(element);
                }

            } else if (isDrawingFrame(element)) {

                // TODO: validate DOM of paragraphs embedded in text frames
                self.drawingStyles.updateElementFormatting(element);
                updateFormatProgress(currentProgressTicks += 1);

                // adding an implicit paragraph into shapes without paragraph (50644)
                if (!app.isODF()) {
                    if (checkEmptyTextShape(self, element)) { // if new empty textframe is inserted, reformat is required
                        self.drawingStyles.updateElementFormatting(element); // #56145
                    }
                }

                // also updating all paragraphs inside text frames (not searching twice for the drawings)
                self.implParagraphChangedSync($(element).find(PARAGRAPH_NODE_SELECTOR));

                // trigger update of parent paragraph
                self.trigger('paragraphUpdate:after', $(element).closest(PARAGRAPH_NODE_SELECTOR));

            } else if (isTableNode(element) && !isExceededSizeTableNode(element)) {

                // Bug 28409: Validate DOM contents of embedded paragraphs without
                // formatting (done by the following table formatting). Finds and
                // processes all cell paragraphs contained in a top-level table
                // (also from embedded tables).
                if (topLevel) {
                    element.find(CELLCONTENT_NODE_SELECTOR + ' > ' + PARAGRAPH_NODE_SELECTOR).each(function () {
                        self.validateParagraphNode(this);
                    });
                }
                // update table formatting asynchronous to prevent browser alerts
                promise = self.tableStyles.updateElementFormatting(element, { async: true })
                    .progress(function (localProgress) {
                        updateFormatProgress(currentProgressTicks + TABLE_PROGRESS_TICKS * localProgress);
                    })
                    .always(function () {
                        currentProgressTicks += TABLE_PROGRESS_TICKS;
                    });
            }

            return promise;
        }

        // updates the formatting of all lists, forwards the progress, returns a promise
        function updateListFormatting() {
            return self.updateLists(null, { async: true }).progress(function (progress) {
                def.notify(0.9 + 0.1 * progress);  // last 10% for updating lists
            });
        }

        // recalculate paragraphs with drawings
        function updateParagraphsWithDrawing() {
            return self.iterateArraySliced(paragraphWithDrawingCollector, updateElementFormatting);
        }

        /**
         * Helper function to get the split point for the first page of the loaded document. This split
         * point can be used to show the first page very fast, while the following content is still
         * formatted.
         * Info: This must be handled in context with 35671 (very large table) and 61921 (very large
         *       document with small table on first page).
         */
        function getSplitPoint() {

            var splitPt = pageContentNode.children('.splitpoint');

            if (splitPt.length) { return splitPt; }

            var LARGE_FORMAT_COUNT = 1000;
            var LARGE_PARA_COUNT = 200;
            var SPLIT_POINT_PARA = 50;
            var contentNodeChildren = null;

            if (formattingNodes.length > LARGE_FORMAT_COUNT) { // a large document with many formatting nodes (61921)

                contentNodeChildren = pageContentNode.children(PARAGRAPH_NODE_SELECTOR);

                if (contentNodeChildren.length > LARGE_PARA_COUNT) {
                    splitPt = $(contentNodeChildren.get(SPLIT_POINT_PARA)).addClass('splitPoint'); // adding marker for split point at paragraph 50;
                }
            }

            return splitPt.length ? splitPt : $();
        }

        // resetting an existing selection, after all operations are applied
        self.getSelection().resetSelection();
        self.setLastOperationEnd(null);

        // US 74030826 - fetch first part of elements for fast displaying of first page
        splitPoint = getSplitPoint(pageContentNode, formattingNodes);

        if (splitPoint.length > 0) {
            indexSplitPoint = formattingNodes.index(splitPoint);
            splitListNodes = formattingNodes.slice(0, indexSplitPoint);
            formattingNodes = formattingNodes.slice(indexSplitPoint);
        }

        // detach all child nodes of the page content node, this improves
        // performance in very large documents notably
        formattingNodes.filter(function () { return this.parentNode === pageContentNode[0]; }).detach();

        // saving the order of the formatting nodes in an array
        // -> using following add function on detached nodes can lead to unsorted nodes (at least in tests using Jest -> e2e "TEXT_SH-05").
        // It must not happen, that for example drawings are formatted before their parent paragraphs, because the top level paragraphs are
        // not attached to the DOM.
        formattingNodesArray = formattingNodes.toArray();

        // first, update the root page node
        this.pageStyles.updateElementFormatting(editdiv);

        // assign style to header footer placeholder node
        pageLayout.updateStartHeaderFooterStyle();

        if (splitPoint.length > 0) {
            splitListNodesArray = splitListNodes.toArray().concat(headerFooterFormattingNodes.toArray()); // taking care of order of elements
            self.firstPartOfDocumentFormatting(splitListNodesArray);
            initialPbPromise = pageLayout.callInitialPageBreaks();
            app.leaveBusyDuringImport();
        } else {
            // add header and footer to formatting nodes -> header/footer nodes are in the DOM always behind the nodes in the page content.
            formattingNodesArray = formattingNodesArray.concat(headerFooterFormattingNodes.toArray());
        }

        // abort first promise before calling function second time (avoid duplicate calls if first is not finished)
        if (initialPbPromise) { initialPbPromise.abort(); }

        this.iterateArraySliced(formattingNodesArray, updateElementFormatting)
            // .then(updateDrawingGroups)
            .then(updateParagraphsWithDrawing)
            .then(updateListFormatting)
            .then(function () { return pageLayout.callInitialPageBreaks({ triggerEvent: true }); })
            .then(function () { return self.startParagraphMarginColorAndBorderFormating(); }) // must be after page-breaks
            .done(function () { self.dumpProfilingInformation(); def.resolve(); })
            .fail(function () { def.reject(); });

        return def.promise();
    };

    /**
     * Synchronous formatting of first part of document (1-2 pages) for fast display.
     *
     * @param {HTMLElement[]} [splitListNodes]
     *  If exists, collected nodes for synchronous formatting.
     */
    this.firstPartOfDocumentFormatting = function (splitListNodes) {

        var // the nodes to be formatted
            formattingNodes,
            // the page node
            editdiv = self.getNode();

        if (splitListNodes) {
            formattingNodes = $(splitListNodes);
        } else {
            // first, update the root page node
            self.pageStyles.updateElementFormatting(editdiv);
            // assign style to header footer placeholder node
            self.getPageLayout().updateStartHeaderFooterStyle();

            formattingNodes = getPageContentNode(editdiv).find('> ' + PARAGRAPH_NODE_SELECTOR + ', ' + NODE_SELECTOR + ', ' + TABLE_NODE_SELECTOR);
        }

        _.each(formattingNodes, function (element) {

            var // whether the element is a direct child of the page content node
                topLevel = !element.parentNode;

            element = $(element);
            // determine the type of the passed element
            if (isParagraphNode(element)) {
                // validate DOM contents AND update formatting of the paragraph
                self.validateParagraphNode(element);
                self.paragraphStyles.updateElementFormatting(element);
            } else if (isDrawingFrame(element)) {
                // TODO: validate DOM of paragraphs embedded in text frames
                self.drawingStyles.updateElementFormatting(element);
            } else if (isTableNode(element) && !isExceededSizeTableNode(element)) {
                // Bug 28409: Validate DOM contents of embedded paragraphs without
                // formatting (done by the following table formatting). Finds and
                // processes all cell paragraphs contained in a top-level table
                // (also from embedded tables).
                if (topLevel) {
                    element.find(CELLCONTENT_NODE_SELECTOR + ' > ' + PARAGRAPH_NODE_SELECTOR).each(function () {
                        self.validateParagraphNode(this);
                    });
                }
                // update table formatting asynchronous to prevent browser alerts
                self.tableStyles.updateElementFormatting(element);
            }
        });

        // updating the list paragraphs on the first page, too (synchronously)
        self.updateLists(formattingNodes.filter(PARAGRAPH_NODE_SELECTOR), { async: false });
    };

    /**
     * Handler for loading empty document with fast load. It applies html string, then actions,
     * formats document synchronosly, calculates page breaks, and at the end, leaves busy mode.
     *
     * @param {String} markup
     *  The HTML mark-up to be shown as initial document contents.
     *
     * @param {Operation[]} operations
     *  The document operations to be applied to finalize the fast import.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the actions have been applied.
     */
    this.fastEmptyLoadHandler = function (markup, operations) {
        // markup is in form of object, parse and get data from mainDocument
        markup = JSON.parse(markup);
        if (markup.fv) { self.setFastLoadFilterVersion(markup.fv); }
        self.setFullModelNode(markup.mainDocument);

        return self.applyExternalOperations(operations, { expand: true }).done(function () {
            self.firstPartOfDocumentFormatting();
            self.getPageLayout().callInitialPageBreaks({ triggerEvent: true });
        });
    };

    /**
     * Set the default paper format according to UI language if it is a new document
     */
    this.setDefaultPaperFormat = function () {
        var attrs = self.pageStyles.getElementAttributes(self.getNode());
        var promise = null;

        if (attrs.page) {
            var page = _.copy(attrs.page, true),
                width = null,
                height = null,
                deltaWidth = 0,
                deltaHeight = 0,
                maxDelta = 2;

            var paperSize = getDefaultPaperSize();

            if (page.width !== paperSize.width || page.height !== paperSize.height) {
                width = paperSize.width;
                height = paperSize.height;
                deltaWidth = (paperSize.width > page.width) ? (paperSize.width - page.width) : (page.width - paperSize.width);
                deltaHeight = (paperSize.height > page.height) ? (paperSize.height - page.height) : (page.height - paperSize.height);
            }

            if (deltaWidth > maxDelta || deltaHeight > maxDelta) {
                app.stopOperationDistribution(function () {
                    promise = self.setPaperFormat({ width, height, marginLeft: page.marginLeft, marginRight: page.marginRight }, page);
                    self.getUndoManager().clearUndoActions();
                    return promise;
                });
            }
        }

        return promise ? promise : $.when();
    };
}
