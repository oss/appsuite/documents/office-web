/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// ES6-LATER
// define('io.ox/office/text/model/docmodel', [
//     'io.ox/office/tk/dom',
//     'io.ox/office/drawinglayer/view/drawingframe',
//     'io.ox/office/drawinglayer/view/drawinglabels',
//     'io.ox/office/editframework/utils/attributeutils',
//     'io.ox/office/textframework/model/editor',
//     'io.ox/office/text/model/modelattributesmixin',
//     'io.ox/office/text/format/listcollection',
//     'io.ox/office/text/format/stylesheetmixin',
//     'io.ox/office/text/model/pagehandlermixin',
//     'io.ox/office/text/model/updatedocumentmixin',
//     'io.ox/office/text/model/listhandlermixin',
//     'io.ox/office/text/model/updatelistsmixin',
//     'io.ox/office/text/components/comment/commentlayer',
//     'io.ox/office/text/components/drawing/drawingresize',
//     'io.ox/office/textframework/components/field/fieldmanager',
//     'io.ox/office/textframework/model/numberformatter',
//     'io.ox/office/textframework/utils/textutils',
//     'io.ox/office/textframework/utils/dom',
//     'io.ox/office/textframework/utils/position',
//     'io.ox/office/textframework/utils/operations',
//     'io.ox/office/textframework/view/selectionbox',
//     'io.ox/office/drawinglayer/utils/drawingutils'
// ], /**
//     * @param {{ clearBrowserSelection: any; }} dom_
//     * @param {Dict<any>} DrawingFrame
//     * @param {{ isTextShapeId: (arg0: string) => boolean; getPresetShape: (arg0: string) => { type: any; }; isPresetConnectorId: (arg0: string) => boolean; getPresetAspectRatio: (arg0: string) => void; }} DrawingLabels
//     * @param { AttributeUtilsType } AttributeUtils
//     * @param { EditorType } Editor
//     * @param {() => void} ModelAttributesMixin
//     * @param {new(...args: any[]) => {}} ListCollection
//     * @param {(app: any) => void} StylesheetMixin
//     * @param {(app: any) => void} PageHandlerMixin
//     * @param {(app: any) => void} UpdateDocumentMixin
//     * @param {() => void} ListHandlerMixin
//     * @param {(app: any) => void} UpdateListsMixin
//     * @param {Object} CommentLayer
//     * @param {{ drawDrawingSelection: any; updateOverlaySelection: any; }} DrawingResize
//     * @param {new (arg0: any) => void} FieldManager
//     * @param {new (...args: any[]) => {}} NumberFormatter
//     * @param {Dict<any>} Utils
//     * @param {{ serializeElementToJSON: (arg0: any) => void; INCREASED_CONTENT_CLASS: any; APPCONTENT_NODE_SELECTOR: string; getPageContentNode: (arg0: any) => { children: (arg0: any) => { last: () => void; }; }; PARAGRAPH_NODE_SELECTOR: any; APP_CONTENT_ROOT_SELECTOR: string; }} DOM
//     * @param {Dict<any>} Position
//     * @param {{ SET_ATTRIBUTES: any; INSERT_DRAWING: any; PARA_INSERT: any; }} Operations
//     * @param {SelectionBoxType} SelectionBox
//     * @param {Dict<any>} DrawingUtils
//     */
// function (dom_, DrawingFrame, DrawingLabels, AttributeUtils, Editor, ModelAttributesMixin, ListCollection, StylesheetMixin, PageHandlerMixin, UpdateDocumentMixin, ListHandlerMixin, UpdateListsMixin, CommentLayer, DrawingResize, FieldManager, NumberFormatter, Utils, DOM, Position, Operations, SelectionBox, DrawingUtils) {}

import _ from '$/underscore';
import $ from '$/jquery';

import { TOUCH_DEVICE, clearBrowserSelection } from '@/io.ox/office/tk/dom';

import { checkEmptyTextShape, drawPreviewShape, isGroupedDrawingFrame, updateResizersMousePointers } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getPresetAspectRatio, getPresetShape, isPresetConnectorId, isTextShapeId } from '@/io.ox/office/drawinglayer/view/drawinglabels';
import { getExplicitAttributes, getExplicitAttributeSet, insertAttribute } from '@/io.ox/office/editframework/utils/attributeutils';
import Editor from '@/io.ox/office/textframework/model/editor';
import AIService from '@/io.ox/office/text/components/ai/aiservice';
import ModelAttributesMixin from '@/io.ox/office/text/model/modelattributesmixin';
import ListCollection from '@/io.ox/office/text/format/listcollection';
import StylesheetMixin from '@/io.ox/office/text/format/stylesheetmixin';
import PageHandlerMixin from '@/io.ox/office/text/model/pagehandlermixin';
import UpdateDocumentMixin from '@/io.ox/office/text/model/updatedocumentmixin';
import ListHandlerMixin from '@/io.ox/office/text/model/listhandlermixin';
import UpdateListsMixin from '@/io.ox/office/text/model/updatelistsmixin';
import CommentLayer from '@/io.ox/office/text/components/comment/commentlayer';
import { drawDrawingSelection, updateOverlaySelection } from '@/io.ox/office/text/components/drawing/drawingresize';
import FieldManager from '@/io.ox/office/textframework/components/field/fieldmanager';
import { NumberFormatter } from '@/io.ox/office/textframework/model/numberformatter';
import { convertAppContentBoxToPageBox, convertHmmToLength, convertLengthToHmm, getArrayOption, getBooleanOption, getObjectOption } from '@/io.ox/office/textframework/utils/textutils';
import { APPCONTENT_NODE_SELECTOR, APP_CONTENT_ROOT_SELECTOR, INCREASED_CONTENT_CLASS, PARAGRAPH_NODE_SELECTOR, getPageContentNode,
    isImplicitParagraphNode, serializeElementToJSON } from '@/io.ox/office/textframework/utils/dom';
import { appendNewIndex, getDOMPosition, getFirstPositionInParagraph, getLeadingAbsoluteDrawingOrDrawingPlaceHolderCount, getOxoPosition,
    getParagraphElement, getPixelPositionToRootNodeOffset, getPositionFromPagePixelPosition, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import {  COMMENT_CHANGE, COMMENT_INSERT, DELETE, INSERT_DRAWING, PARA_INSERT, RANGE_INSERT, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import SelectionBox from '@/io.ox/office/textframework/view/selectionbox';
import { createDefaultShapeAttributeSet, createDefaultSizeForShapeId, getParagraphAttrsForDefaultShape, transformAttributeSet } from '@/io.ox/office/drawinglayer/utils/drawingutils';

/**
 * @typedef {import("@/io.ox/office/tk/dom").Size} Size;
 *
 * @typedef {import("@/io.ox/office/editframework/app/editapplication").EditApplication} EditApplication;
 *
 * @typedef {typeof import("@/io.ox/office/editframework/utils/attributeutils")} AttributeUtilsType
 *
 * @typedef {typeof import("@/io.ox/office/textframework/model/editor")} EditorType // ES6-LATER .default (?)
 * @typedef {InstanceType<EditorType>} Editor
 *
 * @typedef {typeof import("@/io.ox/office/textframework/view/selectionbox")} SelectionBoxType
 * @typedef {InstanceType<SelectionBoxType>} SelectionBox
 *
 * @typedef {typeof import("@/io.ox/office/text/model/docmodel")} TextModelType;
 * @typedef {InstanceType<TextModelType>} TextModelValue
 *
 * @typedef {import("@/io.ox/office/drawinglayer/utils/drawingutils").TrackingRectangle} TrackingRectangle;
 * @typedef {import("@/io.ox/office/editframework/utils/operations").Operation} Operation;
 */

// class TextModel ========================================================

/**
 * Represents the document model of a text application.
 *
 * @param {EditApplication} app
 *  The text application containing this document model.
 *  TODO: This parameter is the TextApplication, not the EditApplication!
 */
export default class TextModel extends Editor {

    constructor(app) {

        // base constructor ---------------------------------------------------
        super(app, {
            updateSelectionInUndo: true, // undo manager puts selection state into actions
            spellingSupported: true,
            commentLayerClass: CommentLayer
        });

        // self reference for local functions
        /** @type {TextModelValue} self */
        var self = this;
        // the root element for the document contents
        /** @type {JQuery | null} pagediv */
        var pagediv = null;
        // the selection box object
        /** @type {SelectionBox | null} selectionBox */
        var selectionBox = null;
        /** @type {Boolean} checkUnsentAttributes */
        let checkUnsentCharacterAttributes = false;
        /** @type {AIService | null} aiService */
        let aiService = null;

        // mixins -------------------------------------------------------------
        ModelAttributesMixin.call(this);
        StylesheetMixin.call(this, app);
        PageHandlerMixin.call(this, app);
        UpdateDocumentMixin.call(this, app);
        ListHandlerMixin.call(this);
        UpdateListsMixin.call(this, app);

        // private methods ----------------------------------------------------

        /**
         * Listener function for the event 'document:reloaded:after', that is triggered, if the user cancels a long
         * running action.
         *
         * _@param {Object} snapShot
         *  The snapshot object.
         */
        function documentReloadAfterHandler(/*snapShot*/) {
            self.getNode().children('.drawingselection-overlay').empty(); // clearing old selection frames (58570)
        }

        /**
         * Helper function to set the selection after reloading a document.
         * The selection must be specified in the launch options.
         */
        function setSelectionAfterLoad() {
            var selectionOptions = app.getLaunchOption('selection');
            if (selectionOptions) {
                var startPos = selectionOptions.start;
                var endPos = selectionOptions.end ? selectionOptions.end : _.copy(startPos);
                var target = selectionOptions.target;
                var selection = self.getSelection();
                if (target) {
                    var pageLayout = self.getPageLayout();
                    if (pageLayout.isIdOfMarginalNode(target) && app.isEditable()) {
                        // Problems for header/footer:
                        // 1. header/footer edit mode is left during 'changeEditModeHandler' (-> the target is not saved before reload)
                        // 2. header/footer edit mode is left during 'updateDateTimeFieldOnLoad' (-> the edit mode is left after setting here)
                        // 3. The page might be required, so that the identical header/footer is used after reload
                        var localRootNode = self.getRootNode(target); // receiving the header/footer node inside the page
                        pageLayout.enterHeaderFooterEditMode(localRootNode);
                        selection.setNewRootNode(localRootNode);
                    }
                }
                selection.setTextSelection(startPos, endPos);
                // also taking care of a selected comment (it might not exist, when reloading with an unsaved comment (DOCS-2987))
                if (selectionOptions.selectedCommentId && self.getCommentCollection().getById(selectionOptions.selectedCommentId)) { self.getCommentCollection().selectComment(selectionOptions.selectedCommentId); }
            }

            // restoring the zoom
            const zoomType = app.getLaunchOption('zoomType');
            const zoomFactor = app.getLaunchOption('zoomFactor');
            if (zoomType) { app.docView.changeZoom(zoomType, zoomFactor); }

            // restoring scroll position is more precise than 'scrollToSelection' and also works fine for drawings
            var scrollPosition = app.getLaunchOption('scrollPosition');
            if (scrollPosition) { app.docView.restoreScrollPosition(scrollPosition); }
        }

        /**
         * Handler function for the COMMENT_INSERT operation.
         *
         * @param {OperationContext} context
         *  The context object wrapping the document operation.
         *
         * @returns {Boolean}
         *  Whether the comment has been inserted successfully.
         */
        function handleInsertComment(context) {

            // the undo operation
            var undoOperation = null;
            // the undo manager
            var undoManager = self.getUndoManager();
            // whether this is an external operation.
            var external = context.external;

            if (!self.getCommentLayer().insertCommentHandler(context)) {
                return false;
            }

            if (undoManager.isUndoEnabled() && !external) {
                undoManager.enterUndoGroup(function () {
                    undoOperation = { name: DELETE, start: context.getPos('start') };
                    self.extendPropertiesWithTarget(undoOperation, context.optStr('target'));
                    undoManager.addUndo(undoOperation, context.operation);
                });
            }

            return true;
        }

        /**
         * Handler function for the COMMENT_CHANGE operation.
         *
         * @param {OperationContext} context
         *  The context object wrapping the operation.
         *
         * @returns {Boolean}
         *  Whether the comment has been changed successfully.
         */
        function handleChangeComment(context) {

            // the undo operation
            var undoOperation = null;
            // the undo manager
            var undoManager = self.getUndoManager();
            // a helper object for the old text and old mentions of the comment
            var oldState = {};
            // whether this is an external operation.
            var external = context.external;

            if (!self.getCommentLayer().changeCommentHandler(context, oldState)) {
                return false;
            }

            if (undoManager.isUndoEnabled() && !external) {
                undoManager.enterUndoGroup(function () {
                    undoOperation = { name: COMMENT_CHANGE, start: context.getPos('start') };
                    self.extendPropertiesWithTarget(undoOperation, context.optStr('target'));
                    if (oldState.text) { undoOperation.text = oldState.text; }
                    if (oldState.mentions) { undoOperation.mentions = oldState.mentions; }
                    undoManager.addUndo(undoOperation, context.operation);
                });
            }

            return true;
        }

        /**
         * The handler for the "change" event of the selection.
         *
         * @param {Object} [options]
         *  The options that are sent together with the selection 'change' event.
         */
        function selectionChangeHandler(options) {
            const insertOperation = getBooleanOption(options, 'insertOperation', false);
            checkUnsentCharacterAttributes = !insertOperation;
        }

        // protected methods --------------------------------------------------

        /**
         * Returns an arbitrary but unambiguous JSON representation of this
         * document model.
         *
         * @returns {object}
         *  An unambiguous JSON representation of this document model.
         */
        this.serializeToJSON = function () {
            return serializeElementToJSON(self.getNode());
        };

        // public methods -----------------------------------------------------

        /**
         * Returns the current selection state of the document.
         *
         * @returns {object}
         *  The current selection state of this document.
         */
        this.getSelectionState = function () {
            var selection = self.getSelection();
            var startPos = selection.getStartPosition();
            return {
                start: startPos,
                end: selection.getEndPosition(),
                target: selection.getRootNodeTarget(),
                type: selection.getSelectionType(),
                dir: selection.getDirection(),
                isTextCursor: selection.isTextCursor(),
                isMultiSelection: false,
                isSlideSelection: false,
                allPositions: null,
                isImplicitParagraph: selection.isTextCursor() && _.last(startPos) === 0 && isImplicitParagraphNode(getParagraphElement(selection.getRootNode(), _.initial(startPos))),
                selectedCommentId: self.getCommentCollection().getSelectedCommentId()
            };
        };

        /**
         * Changes the selection state of the document.
         *
         * @param {object} _selectionState
         *  The new selection state to be set at this document.
         */
        this.setSelectionState = function (_selectionState) {
            // restoring the selection state needs to be done in undo-redo-after handler
        };

        /**
         * Overwriting the function "checkUnsentCharacterAttributes" from the editModel in OX Text.
         * The value "checkUnsentCharacterAttributes" is set within the handler of the selection
         * "change" event. After every insertText operation it is set to "false". This is a
         * performance issue.
         *
         * @returns {Boolean}
         *  Whether it is necessary to check at the current position the existence of character
         *  attributes that are not already sent to the backend. This is not necessary, after the
         *  insertion of a character, because the paragraph cannot be empty. Only after the operation
         *  "splitParagraph" at the end of a paragraph with hard character attributes at the last
         *  text span, there might be unsent character attributes in the text span in the new empty
         *  paragraph.
         */
        this.checkUnsentCharacterAttributes = function () {
            return checkUnsentCharacterAttributes;
        };

        /**
         * Returning whether the document content (paragraphs and tables) do use an increased z-index.
         * This is the case, if during loading a document, there was at least one drawing with the
         * attribute 'anchorBehindDoc' set to true.
         * Or if one drawing was shifted behind the text in the OX Text GUI. Currently this is only
         * possible in the debug pane.
         *
         * @returns {boolean}
         *  Whether the document content nodes have an increased z-index.
         */
        this.isIncreasedDocContent = function () {
            return self.getNode().hasClass(INCREASED_CONTENT_CLASS);
        };

        /**
         * Getting the default text for an empty text frame drawing.
         * This function is application specific.
         * This is not implemented for OX Text yet.
         *
         * _@param {HTMLElement|jQuery} drawing
         *  The drawing node.
         *
         * _@param {Object} [options]
         *  Some additional options.
         *
         * @returns {String|Null}
         *  The default text for a place holder drawing. Or null, if it cannot be determined.
         */
        this.getDefaultTextForTextFrame = function (/*drawing, options*/) {
            return null;
        };

        /**
         * Handling the property 'autoResizeHeight' of a selected text frame node.
         *
         * @param {boolean} state
         *  Whether the property 'autoResizeHeight' of a selected text frame shall be
         *  enabled or disabled.
         */
        this.handleTextFrameAutoFit = function (state) {

            var selection = self.getSelection();
            // the operations generator
            var generator = self.createOperationGenerator();
            // the options for the setAttributes operation
            var operationOptions = {};
            // a selected text frame node or the text frame containing the selection
            var textFrame = selection.getAnyTextFrameDrawing({ forceTextFrame: true });

            // collecting the attributes for the operation
            operationOptions.attrs =  {};
            operationOptions.attrs.shape = { autoResizeHeight: state, noAutoResize: !state }; // also noAutoResize property has to be set, see #50645

            // if auto fit is disabled, the current height must be set explicitely
            if (!state) {
                if (textFrame && textFrame.length > 0 && !isGroupedDrawingFrame(textFrame)) { // #62516 - for shape inside groups dont set explicit height
                    operationOptions.attrs.drawing = { height: convertLengthToHmm(textFrame.height(), 'px') };
                }
            }

            if (selection.isAdditionalTextframeSelection()) {
                operationOptions.start = getOxoPosition(self.getCurrentRootNode(), textFrame, 0);
            } else {
                operationOptions.start = selection.getStartPosition();
            }

            // generate the 'setAttributes' operation
            // @ts-ignore
            generator.generateOperation(SET_ATTRIBUTES, operationOptions);

            // apply all collected operations
            self.applyOperations(generator);
        };

        /**
         *
         * Inserting a shape with a default size.
         *
         * @param {String} presetShapeId
         *  The identifier for the shape.
         *
         * @returns {JQuery.Promise<any>}
         *  The return value of 'self.insertShape'.
         */
        this.insertShapeWithDefaultBox = function (presetShapeId) {
            return self.insertShape(presetShapeId, createDefaultSizeForShapeId(presetShapeId, TextModel.DEFAULT_SHAPE_SIZE.width, TextModel.DEFAULT_SHAPE_SIZE.height));
        };

        /**
         * Inserting a text frame drawing node of type 'shape' into the document.
         *
         * @param {String} presetShapeId
         *  The string specifying the shape to be inserted.
         *
         * @param {Size | null} size
         *  The string specifying the shape to be inserted.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {boolean} [options.selectDrawingOnly=false]
         *      Whether the shape shall be selected after inserting it. Default is
         *      that the paragraph inside the shape is selected.
         *  @param {Number[]} [options.position=null]
         *      A logical position that at which the shape shall be inserted.
         *  @param {Object} [options.paragraphOffset=null]
         *      An offset object containing the properties 'left' and 'top' that describe
         *      the distance of the shape to a paragraph in hmm.
         *
         * @returns {JQuery.Promise<any>}
         *  A promise that will be resolved if the dialog asking the user, if
         *  he wants to delete the selected content (if any) has been closed
         *  with the default action; or rejected, if the dialog has been
         *  canceled. If no dialog is shown, the promise is resolved
         *  immediately.
         */
        this.insertShape = function (presetShapeId, size, options) {

            // the undo manager returns the return value of the callback function
            return self.getUndoManager().enterUndoGroup(function () {

                var // the selection object
                    selection = self.getSelection();

                // getting a valid position for inserting a shape, if it is inserted via keyboard.
                // Otherwise the position was already determined by the selection box.
                // -> shapes can only be inserted in top-level paragraphs or paragraphs in tables.
                function getValidInsertShapePosition() {

                    var validPos = selection.getStartPosition();
                    var leadingAbsoluteDrawings = 0;
                    var paragraphPoint = null;

                    if (selection.isAdditionalTextframeSelection() || (TOUCH_DEVICE && _.browser.Android && validPos.length > 2)) {
                        validPos = [_.first(validPos)]; // the position of the top level element (table or paragraph)
                        validPos = getFirstPositionInParagraph(self.getCurrentRootNode(), validPos);
                        // inserting behind already existing absolute drawings are place holder drawings in this paragraph
                        paragraphPoint = getDOMPosition(self.getCurrentRootNode(), _.initial(validPos), true);
                        if (paragraphPoint && paragraphPoint.node) {
                            leadingAbsoluteDrawings = getLeadingAbsoluteDrawingOrDrawingPlaceHolderCount(paragraphPoint.node);
                            if (leadingAbsoluteDrawings > 0) { validPos = increaseLastIndex(validPos, leadingAbsoluteDrawings); }
                        }
                    }

                    return validPos;
                }

                // helper function, that generates the operations
                function doInsertShape() {

                    // the operations generator
                    var generator = self.createOperationGenerator();
                    // whether the shape shall be selected after insertion
                    var selectDrawingOnly = getBooleanOption(options, 'selectDrawingOnly', false) || TOUCH_DEVICE;
                    // an optional position for inserting the shape
                    var optPosition = getArrayOption(options, 'position', null);
                    // an offset relative to a paragraph in pixel
                    var paragraphOffset = getObjectOption(options, 'paragraphOffset', null);
                    // the width of the parent paragraph
                    var paraWidth = 0;
                    // the current cursor (start) position or a specified position for inserting the drawing
                    var start = optPosition ? optPosition : getValidInsertShapePosition();
                    // the position of the first paragraph inside the text frame
                    var paraPos = _.clone(start);
                    // the paragraph node, in which the text frame will be inserted
                    var paraNode = null;
                    // target for operation - if exists, it's for ex. header or footer
                    var target = self.getActiveTarget();
                    // the options for the insert drawing operation
                    var operationOptions = {};
                    // the attribute set with default formatting
                    var attrSet = createDefaultShapeAttributeSet(self, presetShapeId, options);
                    // the character attribute set
                    var charAttrs = attrSet.character;
                    // whether the shapes can contain text (in ODF: connectors can contain text too)
                    var hasText = app.isODF() || isTextShapeId(presetShapeId);
                    // the options for the setAttributes operation
                    /** @type { Dict<any> | null} setAttrsOptions */
                    var setAttrsOptions = null;
                    // the drawing attributes for the vertical order
                    var drawingOrderAttrs = null;
                    // the options for the insertParagraph opertion
                    var paraInsertOptions = null;

                    self.doCheckImplicitParagraph(start);
                    paraPos.push(0);

                    // the shape family object for the insertDrawing operation
                    attrSet.shape = { anchor: 'centered', anchorCentered: false, paddingLeft: 100, paddingRight: 100, paddingTop: 100, paddingBottom: 100, wordWrap: true, horzOverflow: 'overflow', vertOverflow: 'overflow' };
                    // the attributes of the drawing family
                    insertAttribute(attrSet, 'drawing', 'name', presetShapeId);

                    if (!size) { size = TextModel.DEFAULT_SHAPE_SIZE; }
                    _.extend(attrSet.drawing, size);
                    attrSet.shape.paddingLeft = attrSet.shape.paddingRight = Math.round(size.width / 10);
                    attrSet.shape.paddingTop = attrSet.shape.paddingBottom = Math.round(size.height / 10);

                    operationOptions.attrs = attrSet;
                    operationOptions.start = _.copy(start);
                    operationOptions.type = getPresetShape(presetShapeId).type;

                    // modifying the attributes, if changeTracking is activated
                    if (self.getChangeTrack().isActiveChangeTracking()) {
                        operationOptions.attrs.changes = { inserted: self.getChangeTrack().getChangeTrackInfo(), removed: null };
                    }

                    // checking the width of the parent paragraph to adapt the shape size
                    paraNode = getParagraphElement(pagediv, _.initial(start));

                    if (paraNode) {
                        paraWidth = convertLengthToHmm($(paraNode).width(), 'px');
                        if (paraWidth && paraWidth < operationOptions.attrs.drawing.width) {
                            operationOptions.attrs.drawing.width = paraWidth;
                            operationOptions.attrs.drawing.height = paraWidth; // keeping the drawing quadratic
                        }
                    }

                    if (paragraphOffset) {
                        // adding attributes to the insertDrawing operation, so that the drawing is absolutely positioned in paragraph
                        _.extend(operationOptions.attrs.drawing, { anchorHorBase: 'column', anchorHorAlign: 'offset', anchorVertBase: 'paragraph', anchorVertAlign: 'offset', inline: false, textWrapMode: 'none' });
                        operationOptions.attrs.drawing.anchorHorOffset = paragraphOffset.left;
                        operationOptions.attrs.drawing.anchorVertOffset = paragraphOffset.top;
                    }

                    // not assigning character attributes to the drawing (51104) -> will be inherited to following text spans
                    if (operationOptions.attrs.character) { delete operationOptions.attrs.character; }

                    // @ts-ignore
                    self.extendPropertiesWithTarget(operationOptions, target);
                    // @ts-ignore
                    generator.generateOperation(INSERT_DRAWING, operationOptions);

                    setAttrsOptions = { attrs: { shape: { anchor: 'centered' } }, start: _.copy(start) };

                    // setting autoResizeHeight explicitly to false in odt files (56351)
                    if (app.isODF()) { setAttrsOptions.attrs.shape.autoResizeHeight = false; }

                    // bringing the shape always to the 'front' (54735)
                    drawingOrderAttrs = self.getDrawingLayer().getDrawingOrderAttributes('front', $(paraNode));
                    if (drawingOrderAttrs && drawingOrderAttrs.anchorBehindDoc) { drawingOrderAttrs = null; } // inserted drawings must be in front of the text
                    if (drawingOrderAttrs) { setAttrsOptions.attrs.drawing = drawingOrderAttrs; }

                    self.extendPropertiesWithTarget(setAttrsOptions, target);
                    generator.generateOperation(SET_ATTRIBUTES, setAttrsOptions);

                    if (hasText) {
                        paraInsertOptions = { start: _.copy(paraPos), attrs: _.extend({ character: charAttrs }, getParagraphAttrsForDefaultShape(self)) };
                        self.extendPropertiesWithTarget(paraInsertOptions, target);
                        generator.generateOperation(PARA_INSERT, paraInsertOptions);
                    }

                    self.applyOperations(generator);

                    if (!hasText || selectDrawingOnly) {
                        self.getSelection().setTextSelection(start, increaseLastIndex(start)); // select the drawing
                    } else {
                        self.getSelection().setTextSelection(appendNewIndex(_.clone(paraPos), 0)); // setting cursor into text frame
                    }

                }

                // Inserting a shape never deletes an existing selection
                // if (selection.hasRange()) {
                //     return self.deleteSelected()
                //         .done(function () {
                //             doInsertShape();
                //         });
                //     }

                doInsertShape();
                return $.when();

            }, this); // enterUndoGroup()
        };

        /**
         * Preparations for inserting a shape. A selection box can be opened
         * so that the user can determine the position of the shape.
         *
         * @param {String} presetShapeId
         *  The identifier for the shape.
         *
         * @returns {boolean}
         *  Whether a selection box could be activated.
         */
        this.insertShapePreparation = function (presetShapeId) {

            var contentRootNode = app.docView.getContentRootNode();
            var contentHeight = contentRootNode.children(APPCONTENT_NODE_SELECTOR).outerHeight() + 100;
            var contentRootOffset = contentRootNode.offset();
            var shapeMode = 'shape';
            var canvasDiv = $('<div style="position:absolute;z-index:99">');
            var vertStartLine = $('<div class="guide-line v"></div>').height(contentHeight);
            var horzStartLine = $('<div class="guide-line h"></div>');
            var vertEndLine = $('<div class="guide-line v"></div>').height(contentHeight);
            var horzEndLine = $('<div class="guide-line h"></div>');
            // special behavior for line/connector shapes
            var twoPointMode = isPresetConnectorId(presetShapeId);
            // custom aspect ratio for special shapes
            var aspectRatio = getPresetAspectRatio(presetShapeId);

            /**
             * @param {JEvent} event
             */
            function initGuidelinesHandler(event) {
                if (!selectionBox) { return; }
                var eventPos = selectionBox.getEventPosition(event, (event.type === 'touchmove') ? 'touch' : 'mouse');
                if (eventPos) {
                    var startX = eventPos.x + contentRootNode.scrollLeft() - contentRootOffset.left;
                    var startY = eventPos.y + contentRootNode.scrollTop() - contentRootOffset.top;
                    horzStartLine.css('top', startY);
                    vertStartLine.css('left', startX);
                }
            }

            function clearGuidelines() {
                contentRootNode.children('.guide-line').remove();
            }

            /**
             * @param {TrackingRectangle} frameRect
             * @returns {{flipH: boolean, flipV:boolean} | null}
             */
            // returns the flipping options for connector/line shapes
            function getFlippingOptions(frameRect) {
                return twoPointMode ? { flipH: frameRect.reverseX, flipV: frameRect.reverseY } : null;
            }

            function clearGuidelinesHandler() {
                clearGuidelines();
            }

            /**
             * @param {TrackingRectangle} frameRect
             */
            function moveHandler(frameRect) {
                canvasDiv.css({ left: frameRect.left, top: frameRect.top });
                horzStartLine.css('top', frameRect.top);
                vertStartLine.css('left', frameRect.left);
                vertEndLine.css('left', frameRect.right());
                horzEndLine.css('top', frameRect.bottom());
                var options = _.extend({ transparent: true }, getFlippingOptions(frameRect));
                drawPreviewShape(app, canvasDiv, frameRect, presetShapeId, options);
                clearBrowserSelection(); // avoiding visible browser selection during inserting shape
            }

            /**
             * @param {TrackingRectangle} frameRect
             */
            function startHandler(frameRect) {
                contentRootNode.append(canvasDiv, vertEndLine, horzEndLine);
                self.stopListeningTo(contentRootNode, 'mousemove touchmove', initGuidelinesHandler);
                moveHandler(frameRect);
            }

            /**
             * @param {TrackingRectangle} frameRect
             */
            function stopHandler(frameRect) {
                // the options for the insertShape operation
                /**
                 * @type {{position: number[], paragraphOffset: {left: number, top: number}, absolute: boolean, flipH: number, flipV: number} | any} insertShapeOptions
                 */
                var insertShapeOptions = getFlippingOptions(frameRect) || {};
                // the current zoom level
                var zoomLevel = app.docView.getZoomFactor() * 100;
                // an object with the paragraph offset
                var paraOffset = null;
                // the page info object returned from the pixel API
                var pageInfo = null;
                // the last paragraph in the document
                var lastParagraph = null;
                // the number of leading absolute positioned drawings in the paragraph
                var leadingAbsoluteDrawings = 0;
                // defaultBox for shapes
                var defaultBox;

                if (!selectionBox) { return; }

                // inserting drawing with default size after click without creating a box
                if (frameRect.area() === 0) {

                    // box.width = convertHmmToLength(TextModel.DEFAULT_SHAPE_SIZE.width, 'px', 1);
                    // box.height = convertHmmToLength(TextModel.DEFAULT_SHAPE_SIZE.height, 'px', 1);
                    defaultBox = createDefaultSizeForShapeId(presetShapeId, convertHmmToLength(TextModel.DEFAULT_SHAPE_SIZE.width, 'px', 1), convertHmmToLength(TextModel.DEFAULT_SHAPE_SIZE.height, 'px', 1));
                    frameRect.width = defaultBox.width;
                    frameRect.height = defaultBox.height;
                }

                // converting the pixel relative to app content root node relative to the page
                frameRect = convertAppContentBoxToPageBox(app, frameRect);

                if (!self.isHeaderFooterEditState()) { // in header/footer edit state the pixel API is not precise enough
                    pageInfo = getPositionFromPagePixelPosition(pagediv, frameRect.left, frameRect.top, zoomLevel, { onlyParagraphPosition: true });

                    if (pageInfo && pageInfo.pos) {
                        leadingAbsoluteDrawings = getLeadingAbsoluteDrawingOrDrawingPlaceHolderCount(pageInfo.point.node);
                        insertShapeOptions.position = appendNewIndex(pageInfo.pos, leadingAbsoluteDrawings);

                        // calculating pixel position relative to the paragraph
                        paraOffset = getPixelPositionToRootNodeOffset(pagediv, pageInfo.point.node, zoomLevel);
                        paraOffset = { left: convertLengthToHmm(frameRect.left - paraOffset.x, 'px'), top: convertLengthToHmm(frameRect.top - paraOffset.y, 'px') };

                        insertShapeOptions.paragraphOffset = paraOffset;
                        insertShapeOptions.absolute = true;
                    } else {
                        // check if the position is below the last paragraph
                        lastParagraph = getPageContentNode(pagediv).children(PARAGRAPH_NODE_SELECTOR).last();
                        paraOffset = getPixelPositionToRootNodeOffset(pagediv, lastParagraph, zoomLevel);
                        if (frameRect.top > paraOffset.y) {
                            leadingAbsoluteDrawings = getLeadingAbsoluteDrawingOrDrawingPlaceHolderCount(lastParagraph);
                            paraOffset = { left: convertLengthToHmm(frameRect.left - paraOffset.x, 'px'), top: convertLengthToHmm(frameRect.top - paraOffset.y, 'px') };
                            insertShapeOptions.paragraphOffset = paraOffset;
                            insertShapeOptions.absolute = true;
                            insertShapeOptions.position = appendNewIndex(getOxoPosition(pagediv, lastParagraph, 0), leadingAbsoluteDrawings);
                        }
                    }
                }

                // setting marker for operations triggered from selection box
                // -> this marker can be used to cancel an active selection box, if there are operations not generated
                //    from the selection box (the user might have clicked on another button in the top bar).
                selectionBox.setSelectionBoxOperation(true);

                // setting the size of the drawing
                var size = { width: convertLengthToHmm(frameRect.width, 'px'), height: convertLengthToHmm(frameRect.height, 'px') };
                // assigning the drawing to the paragraph of the upper left position
                self.insertShape(presetShapeId, size, insertShapeOptions);

                // resetting marker for operations triggered from selection box
                selectionBox.setSelectionBoxOperation(false);
            }

            function finalizeHandler() {
                self.executeDelayed(function () {
                    canvasDiv.remove();
                    clearGuidelines();
                }, 50);
                // reset to the previous selection box mode
                if (selectionBox) { selectionBox.setPreviousMode(); }
            }

            if (!selectionBox) { return false; }

            if (self.isInsertShapeActive()) {
                selectionBox.cancelSelectionBox();
                clearGuidelines();
                return false;
            }

            contentRootNode.append(vertStartLine, horzStartLine);
            self.listenTo(contentRootNode, 'mousemove touchmove', initGuidelinesHandler);

            // always registering the mode for text shape insertion
            selectionBox.registerSelectionBoxMode(shapeMode, {
                contentRootClass: 'textframemode',
                preventScrolling: true,
                leaveModeOnCancel: true,
                excludeFilter: TextModel.EXCLUDE_SELECTION_BOX_FILTER,
                trackingOptions: { twoPointMode, aspectRatio },
                stopHandler,
                startHandler,
                moveHandler,
                finalizeHandler,
                clearGuidelinesHandler
            });

            // activating the mode for text frame insertion
            selectionBox.setActiveMode(shapeMode);

            return true;
        };

        /**
         * Getter for the selection box object, that can be used to create a specified
         * rectangle on the application content root node.
         *
         * @returns {SelectionBox | null}
         *  The selection box object.
         */
        this.getSelectionBox = function () {
            return selectionBox;
        };

        /**
         * Getting the default shape size with the properties 'width' and 'height'.
         *
         * @returns {Object}
         *  An object containing the properties 'width' and 'height' for the default shape size.
         */
        this.getDefaultShapeSize = function () {
            return TextModel.DEFAULT_SHAPE_SIZE;
        };

        /**
         * Whether the 'shape' mode is the active mode.
         *
         * @returns {boolean}
         *  Whether the 'shape' mode is the active mode.
         */
        this.isInsertShapeActive = function () {
            return !!(selectionBox && selectionBox.getActiveMode() === 'shape');
        };

        /**
         * Saving the attributes that are explicitely set at an implicit paragraph. In this case
         * the new paragraph that replaces the implicit paragraph, shall also use this attributes.
         * This is for example the case for paragraphs in empty shapes.
         *
         * @param {JQuery} para
         *  The implicit paragraph that will be replaced via operation.
         *
         * @returns {Dict}
         *  The attributes that are required for the insertParagraph operation.
         */
        this.getReplaceImplicitParagraphAttrs = function (para) {
            return getExplicitAttributeSet(para);
        };

        /**
         * Rotates and/or flips the selected drawing obejcts.
         *
         * @param {Number} angle
         *  The rotation angle to be added to the current rotation angle of the
         *  drawing objects.
         *
         * @param {boolean} flipH
         *  Whether to flip the drawing objects horizontally.
         *
         * @param {boolean} flipV
         *  Whether to flip the drawing objects vertically.
         */
        this.transformDrawings = function (angle, flipH, flipV) {

            // the selection object
            var selection = self.getSelection();
            // if drawing or text inisde is selected
            var isAdditionalTextSel = selection.isAdditionalTextframeSelection();
            // root node
            var rootNode = selection.getRootNode();
            // drawing node
            var drawing = isAdditionalTextSel ? selection.getSelectedTextFrameDrawing() : selection.getSelectedDrawing();

            // create the new drawing attributes
            var drawingAttrs = this.drawingStyles.getElementAttributes(drawing).drawing;
            drawingAttrs = transformAttributeSet(drawingAttrs, angle, flipH, flipV, app.isODF());

            // create the new 'setAttributes' operation
            if (drawingAttrs) {

                // the operations generator
                var generator = self.createOperationGenerator();
                // drawing start position
                var startPos = isAdditionalTextSel ? getOxoPosition(rootNode, drawing, 0) : selection.getStartPosition();
                // the properties for the new setAttributes operation
                var properties = { start: _.copy(startPos), attrs: { drawing: drawingAttrs } };

                if (selection.isCropMode()) { self.exitCropMode(); }

                // create and apply the operations (undo group is created automatically)
                self.extendPropertiesWithTarget(properties, self.getActiveTarget());
                generator.generateOperation(SET_ATTRIBUTES, properties);
                self.applyOperations(generator);

                self.getSelection().setTextSelection(startPos, increaseLastIndex(startPos)); // refreshing selection after drawing is rotated (64767)

                // after operations are applied, update resize mouse pointers
                if ('rotation' in drawingAttrs) {
                    updateResizersMousePointers(drawing, drawingAttrs.rotation);
                }
            }
        };

        /**
         * After undo/redo or pasting internal clipboard is applied, it is necessary that empty shapes,
         * that were not generated inside OX Text, receive an implicit paragraph, so that text can be
         * inserted inside OX Text (53983).
         * After loading the document this is already handled within updateDocumentFormatting (call of
         * checkEmptyTextShape).
         *
         * @param {Operation[]} operations
         *  An array containing the undo or redo operations.
         */
        this.checkInsertedEmptyTextShapes = function (operations) {

            // all insert drawing operations from undo/redo
            var insertDrawingOps = _.filter(operations, function (op) { return op.name === INSERT_DRAWING; });

            _.each(insertDrawingOps, function (op) {
                // the root node for the logical position in the operation
                var rootNode = self.getRootNode(op.target);
                // the drawing nodes and its offset
                var drawingPoint = getDOMPosition(rootNode, op.start, true);
                // whether the drawing node was really modified
                var drawingModified = false;

                if (drawingPoint && drawingPoint.node) {
                    drawingModified = checkEmptyTextShape(self, drawingPoint.node);
                    if (drawingModified) { self.drawingStyles.updateElementFormatting(drawingPoint.node); }
                }
            });

        };

        /**
         * Checking if the undo operations need to be removed after an empty redo without any operation.
         * The undo operations might be part of the external operations (DOCS-2645).
         * In the case of insertComment or insertRange operations that use an ID, it leads to an error, if
         * components with the same ID are inserted again.
         *
         * @param {Action[]} externalActions
         *  The array of external actions.
         *
         * @param {Operation[]} undoOperations
         *  The array containing the undo operations belonging to the 'empty' redo.
         */
        this.checkUndoOperations = function (externalActions, undoOperations) {

            var idCollector = {};

            // checking the validity of the undo actions, after an empty redo caused by OT.
            // -> in this case the operations with ID (insertRange and insertComment) must be called with a new comment Id
            // TODO: It might be checked, if the undoOperations are included in the external operations, too.
            _.each(undoOperations, function (op) {
                var id = null;
                var newId = null;
                if (op.name === COMMENT_INSERT || (op.name === RANGE_INSERT && op.type === 'comment')) {
                    id = op.id;
                    newId = idCollector[id] || self.getCommentLayer().getNextCommentID();
                    idCollector[id] = newId;
                    op.id = newId;
                    if (op.parentId && idCollector[op.parentId]) { op.parentId = idCollector[op.parentId]; }
                }
            });
        };

        /**
         * Check, whether at the current position, a list can be inserted. This is in ODT not always the case (58663, 62540 (excluding comments)).
         *
         * @returns {boolean}
         *   Whether a list can be inserted at the current position.
         */
        this.checkTextListAvailability = function () {
            return !app.isODF() || ((!self.getSelection().isAnyDrawingSelection() || self.isFullOdfTextframeFunctionality()) && !self.isCommentFunctionality());
        };

        /**
         * Toggling the drawing position relative to the text of the selected drawing. The drawing position changes
         * from 'behind the text' to 'before the text' and vice versa.
         */
        this.toggleDrawingToTextMode = function () {

            var selection = self.getSelection();
            // the operations generator
            var generator = self.createOperationGenerator();
            // the options for the setAttributes operation
            var operationOptions = {};
            // a selected drawing or a selected text frame in a text selection
            var drawing = selection.getAnyDrawingSelection();

            if (!drawing || drawing.length === 0) { return; } // nothing to do

            var drawingAttrs = getExplicitAttributes(drawing, 'drawing');

            if (drawingAttrs.textWrapMode !== 'none') { return; } // nothing to do

            // collecting the attributes for the operation
            operationOptions.attrs =  { drawing: { anchorBehindDoc: !drawingAttrs.anchorBehindDoc } };

            // generating setAttributes operation
            if (selection.isDrawingSelection()) {
                operationOptions.start = selection.getStartPosition();
            } else {
                operationOptions.start = getOxoPosition(self.getCurrentRootNode(), drawing, 0);
            }

            // generate the 'setAttributes' operation
            // @ts-ignore
            generator.generateOperation(SET_ATTRIBUTES, operationOptions);

            // apply all collected operations
            self.applyOperations(generator);
        };

        /**
         * Returns whether this document contains a comment thread with unsaved changes.
         *
         * @returns {boolean}
         *  Whether this document contains any comment thread with unsaved changes.
         */
        this.hasUnsavedComments = function () {
            return self.getCommentCollection().hasUnsavedComment();
        };

        /**
         * Discards unsaved changes in all comment threads in this document.
         */
        this.deleteUnsavedComments = function () {
            self.getCommentCollection().deleteUnsavedComment();
        };

        /**
         * Returning the comment collection, that contains all comments in the document.
         *
         * @returns {Object}
         *  A reference to the comment collection object.
         */
        this.getCommentCollection = function () {
            return self.getCommentLayer().getCommentCollection();
        };

        /**
         * Simulating character attributes at paragraphs or drawings (DOCS-4211)
         */
        this.addCharacterAttributes = function (attrs) {

            const selection = self.getSelection();
            const generator = self.createOperationGenerator();
            const operationOptions = {};

            if (selection.isDrawingSelection()) {
                operationOptions.start = selection.getStartPosition();
            } else if (selection.isTextCursor()) {
                operationOptions.start = _.initial(selection.getStartPosition());
                if (isImplicitParagraphNode(getParagraphElement(selection.getRootNode(), operationOptions.start))) {
                    generator.generateOperation(PARA_INSERT, { start: _.copy(operationOptions.start), noUndo: true });
                }

            } else {
                return; // do nothing
            }

            operationOptions.attrs =  { character: attrs };
            generator.generateOperation(SET_ATTRIBUTES, operationOptions);

            // apply all collected operations
            self.applyOperations(generator);
        };

        /**
         * Getter for the AI service.
         *
         * @returns {AIService | null}
         *  The AI service.
         */
        this.getAIService = function () {
            return aiService;
        };

        // Initialization (registering the handler functions for the operations) --------

        this.registerOperationHandler(COMMENT_INSERT, handleInsertComment);
        this.registerOperationHandler(COMMENT_CHANGE, handleChangeComment);

        // initialization -----------------------------------------------------

        pagediv = self.getNode();

        // the AI service
        aiService = new AIService(this);

        // setting the handler function for moving and resizing drawings and for updating the selection in the overlay node
        self.getSelection().setDrawingResizeHandler(drawDrawingSelection);
        self.getSelection().setUpdateOverlaySelectionHandler(updateOverlaySelection);

        // setting handler for handling fields in presentation
        self.setFieldManager(new FieldManager(this));

        // create the number formatter
        self.setNumberFormatter(new NumberFormatter(this));

        // setting the list collection object
        self.setListCollection(new ListCollection(this));

        // setting the handler function for updating lists after the document is imported successfully
        self.waitForImportSuccess(function () {
            if (!self.getSlideTouchMode()) {
                selectionBox = new SelectionBox(app, app.docView.getContentRootNode());
                self.on('operations:before', function (_ops, external) {
                    if (!external && self.isInsertShapeActive() && selectionBox && !selectionBox.isSelectionBoxOperation()) { selectionBox.cancelSelectionBox(); } // for example: The user clicked another button
                });
            }

            // finally setting the selection after a successful import, if specified in reload options
            // -> but in reload case it is necessary to wait for the event 'revealApp' that is fired later.
            if (!app.isReloading()) { setSelectionAfterLoad(); }
        });

        // in the case of a reload, the selection can be restored after event 'revealApp'.
        self.listenTo(app, 'revealApp', setSelectionAfterLoad);

        // registering the handler for document reload after event. This happens, if the user cancels a long running action
        self.on('document:reloaded:after', documentReloadAfterHandler);

        self.getSelection().on('change', selectionChangeHandler);

        // registering process mouse down handler at the page
        if (pagediv) { pagediv.on('mousedown touchstart', self.getPageProcessMouseDownHandler()); }
        if (self.getListenerList()) { self.getListenerList()['mousedown touchstart'] = self.getPageProcessMouseDownHandler(); }
        // complex fields
        if (pagediv) { pagediv.on('mouseenter', '.complex-field', event => self.getFieldManager().createHoverHighlight(event)); }
        if (pagediv) { pagediv.on('mouseleave', '.complex-field', event => self.getFieldManager().removeHoverHighlight(event)); }

        // destroy all class members on destruction
        self.registerDestructor(function () {
            selectionBox?.deRegisterSelectionBox();
        });
    }
}

// constants --------------------------------------------------------------

/**
 * The attributes that are used for new inserted drawings.
 */
TextModel.DEFAULT_SHAPE_SIZE = {
    width: 5000,
    height: 5000
};

/**
 * A CSS filter to specify not allowed event targets for the selection box.
 */
TextModel.EXCLUDE_SELECTION_BOX_FILTER = APPCONTENT_NODE_SELECTOR + ', ' + APP_CONTENT_ROOT_SELECTOR;
