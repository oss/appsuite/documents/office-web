/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { SMALL_DEVICE, TOUCH_DEVICE } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { findAbsoluteDrawingNodeByPixelPosition, getAllAbsoluteDrawingsOnPage,
    getDomNode } from '@/io.ox/office/textframework/utils/textutils';
import { FIELD_NODE_SELECTOR, TAB_NODE_SELECTOR, TABLE_CELLNODE_SELECTOR, Point, Range,
    findDrawingNodeById, getMarginalTargetNode, isContentNode,
    isDrawingSpaceMakerNode, isHeaderOrFooter, isInlineComponentNode, isInsideMarginalContextMenu, isListLabelNode,
    isPlaceholderNode, isTablePageBreakRowNode, isResizeNode } from '@/io.ox/office/textframework/utils/dom';
import { getOxoPosition, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import { TableResizeObserver } from '@/io.ox/office/textframework/components/table/tableresize';
import { NODE_SELECTOR, TEXTFRAME_NODE_SELECTOR, isOnlyBorderMoveableDrawing, isSelected,
    isTextFrameNode } from '@/io.ox/office/drawinglayer/view/drawingframe';

// mix-in class PageHandlerMixin ======================================

/**
 * A mix-in class for the document model class TextModel providing
 * the event handlers for the main slide page used in a text document.
 *
 * @param {Application} app
 *  The application containing this instance.
 */
export default function PageHandlerMixin(app) {

    var // self reference for local functions
        self = this;

    // start to observe table resizer elements, and generate the appropriate operations
    // (needs to be in an `app.onInit` block because class requires access to document view)
    // TODO: move this initialization block somewhere into view code
    app.onInit(() => {
        if (!SMALL_DEVICE) {
            const tableResizeObserver = this.member(new TableResizeObserver(app));
            tableResizeObserver.observe(this.getNode()[0]);
        }
    });

    // private methods ----------------------------------------------------

    /**
     * The handler for the 'mousedown' and the 'touchstart' event in the presentation
     * application.
     *
     * @param {jQuery.Event} event
     *  The jQuery event object.
     */
    function pageProcessMouseDownHandler(event) {

        var // whether the user has privileges to edit the document
            readOnly = !app.isEditable(),
            // jQuerified target of the event
            $eventTarget = $(event.target),
            // mouse click on a table
            tableCell = $eventTarget.closest(TABLE_CELLNODE_SELECTOR),
            tableCellPoint = null,
            // mouse click in the comments pane node
            commentsNode = $eventTarget.closest('.comments-container'),
            // mouse click on a tabulator node
            tabNode = null,
            // mouse click on field node
            field = null,
            // the currently activated node
            activeRootNode = self.getCurrentRootNode(),
            // current page number
            pageNum = null,
            // whether the right mouse-button was clicked, or not
            rightClick = (event.button === 2),
            // the selection object
            selection = self.getSelection(),
            // the page layout object
            pageLayout = self.getPageLayout();

        self.setMouseDownInCommentsPane(!!commentsNode.length); // other listeners can use this value (f.e. mouseup handler)
        if (commentsNode.length) { return; } // DOCS-2664, not selecting content in comments, that are not in edit-mode

        // fix for not clearing anoying selection range in Chrome on click
        if (_.browser.Chrome && selection.hasRange() && !rightClick && !event.shiftKey) {
            window.getSelection().empty();
        }

        // expanding a waiting double click to a triple click
        if (self.getDoubleClickEventWaiting()) {
            self.setTripleClickActive(true);
            event.preventDefault();
            return;
        }

        // do not show the changeTrack-popup, if the right mousebutton was cliked
        // or hide it, if it's already open
        if (rightClick) {
            app.docView.hideChangeTrackPopup();
        }

        // activation mouse down
        self.setActiveMouseDownEvent(true);

        // avoid in FF white background, if user double clicked on textframe node (54187)
        if (_.browser.Firefox && isTextFrameNode($eventTarget) && selection.isAdditionalTextframeSelection()) {
            // check, if this drawing is already selected
            if (getDomNode(selection.getSelectedTextFrameDrawing()) === getDomNode($eventTarget.closest('.drawing'))) {
                $eventTarget.removeAttr('_moz_abspos'); // this attribute was already set by the browser
                event.preventDefault();
                return;
            }
        }

        // also dont do processBrowserEvent if the change track popup is clicked
        if (app.docView.changeTrackPopupContains(event.target)) {
            event.preventDefault();
            return;
        }

        // Fix for 29257 and 29380 and 31623
        if ((_.browser.IE === 11) && ($eventTarget.is('table') ||
            isListLabelNode(event.target) || (event.target.parentNode && isListLabelNode(event.target.parentNode)) ||
            isPlaceholderNode(event.target) || (event.target.parentNode && isPlaceholderNode(event.target.parentNode)) ||
            (event.target.parentNode && event.target.parentNode.parentNode && isPlaceholderNode(event.target.parentNode.parentNode)))) {
            return;
        }

        // Fix for 29409: preventing default to avoid grabbers in table cells or inline components
        if ((_.browser.IE === 11) && (
            $eventTarget.is('div.cell') ||
            isInlineComponentNode(event.target) ||
            (event.target.parentNode && isInlineComponentNode(event.target.parentNode)))
        ) {
            event.preventDefault(); // -> not returning !
        }

        if (!SMALL_DEVICE) { // no page break handling on small devices

            if (self.isHeaderFooterEditState()) {
                if ((_.browser.Firefox || _.browser.IE) && $eventTarget.is('.cover-overlay, .inactive-selection, .page-break')) {
                    event.preventDefault();
                    return; //#37151
                }

                // mouse click in the marginal context menu -> no handling in mouseup handler (DOCS-3423)
                if (isInsideMarginalContextMenu(event.target)) { self.ignoreFollowingMouseUpEventHandler(); }

                let itemNode;
                if ((itemNode = $eventTarget.closest('.marginal-menu-goto')).length) {
                    event.preventDefault();
                    pageNum = pageLayout.getPageNumber(self.getCurrentRootNode());
                    pageLayout.leaveHeaderFooterAndSetCursor(self.getCurrentRootNode());
                    if (itemNode.data('target') === 'header') {
                        pageLayout.jumpToHeaderOnCurrentPage(pageNum);
                    } else {
                        pageLayout.jumpToFooterOnCurrentPage(pageNum);
                    }
                    return;
                }
                if ($eventTarget.closest('.marginal-menu-close').length) {
                    event.preventDefault();
                    pageLayout.leaveHeaderFooterAndSetCursor(self.getCurrentRootNode(), undefined, { asyncCursor: true });
                    activeRootNode = self.getCurrentRootNode();
                    self.updateEditingHeaderFooterDebounced(); // this call is important for repainting layout after leaving h/f edit mode
                    return;
                }
                if ($eventTarget.closest('.marginal-none').length) {
                    event.preventDefault();
                    pageLayout.setHeaderFooterTypeInDoc('none');
                    return;
                }
                if ($eventTarget.closest('.marginal-same').length) {
                    event.preventDefault();
                    pageLayout.setHeaderFooterTypeInDoc('default');
                    return;
                }
                if ($eventTarget.closest('.marginal-first').length) {
                    event.preventDefault();
                    pageLayout.setHeaderFooterTypeInDoc('first');
                    return;
                }
                if ($eventTarget.closest('.marginal-even').length) {
                    event.preventDefault();
                    pageLayout.setHeaderFooterTypeInDoc('evenodd');
                    return;
                }
                if ($eventTarget.closest('.marginal-first-even').length) {
                    event.preventDefault();
                    pageLayout.setHeaderFooterTypeInDoc('all');
                    return;
                }
                if ($eventTarget.closest('.marginal-container').length) {
                    event.preventDefault();
                    return;
                }
                if ($eventTarget.closest('.marginal-menu-type').length) {
                    event.preventDefault();
                    pageLayout.toggleMarginalMenuDropdown(event.target);
                    return;
                }
                if (selection.isAnyDrawingSelection() && $eventTarget.closest('.drawingselection-overlay').length > 0) {
                    // click inside selection overlay node -> simply not handle this event here
                } else if (!isHeaderOrFooter(event.target) && getMarginalTargetNode(self.getNode(), event.target).length === 0) { // Leaving Header-footer edit state
                    pageLayout.leaveHeaderFooterAndSetCursor(self.getParentOfHeaderFooterRootNode().find('.active-selection'), self.getParentOfHeaderFooterRootNode());
                    activeRootNode = self.getCurrentRootNode(); // reset cached node after changing root node
                    self.updateEditingHeaderFooterDebounced(); // this call is important for repainting layout after leaving h/f edit mode
                }
            } else {
                if (_.browser.IE && !rightClick && $eventTarget.is('.inactive-selection, .cover-overlay')) { // #35678 prevent resize rectangles on headers in IE
                    event.preventDefault();
                    selection.updateIESelectionAfterMarginalSelection(activeRootNode, $eventTarget);
                    return;
                }
            }

            if (_.browser.IE && $eventTarget.hasClass('inner-pb-line')) {
                event.preventDefault();
                return;
            }

            if (tableCell.length > 0) {
                if (isTablePageBreakRowNode(tableCell.parent())) {
                    tableCell = tableCell.parent().next().children().eq(tableCell.index());
                    tableCell = tableCell.find('.p').first();
                    if (tableCell.length) { // if clicked on table cell in repeated row, try to find position in a same cell one row behind
                        const startPosition = getOxoPosition(selection.getRootNode(), tableCell, 0);
                        if (startPosition) {
                            startPosition.push(0);
                            selection.setTextSelection(startPosition);
                            event.preventDefault();
                            return;
                        }
                    }
                }
                // saving initial cell as anchor cell for table cell selection
                // -> this has to work also in read-only mode
                tableCellPoint = Point.createPointForNode(tableCell);
                selection.setAnchorCellRange(new Range(tableCellPoint, new Point(tableCellPoint.node, tableCellPoint.node + 1)));
            }
        }

        if (SMALL_DEVICE && self.getActiveTarget()) {
            selection.processBrowserEvent(event);
            return;
        }

        // tabulator handler
        tabNode = $eventTarget.closest(TAB_NODE_SELECTOR);
        if (tabNode.length > 0) {
            // handle in processMouseUp event
            return;
        }

        // checking for a selection on a drawing node. But if this is a text frame drawing node, this is not a drawing
        // selection, if the click happened on the text frame sub element.
        let drawingNode = $eventTarget.closest(NODE_SELECTOR);

        // taking care of drawing that might be located below paragraphs (DOCS-3265)
        if (!drawingNode.length && self.isIncreasedDocContent()) {
            var localDrawingNode = null;
            if (isDrawingSpaceMakerNode($eventTarget)) {
                localDrawingNode = findDrawingNodeById(self.getNode(), $eventTarget.attr('data-drawingID'));
                // TODO: $eventtarget should be inside the text frame -> Pixel API ?
            } else if (isContentNode($eventTarget)) {
                // the event was catch by a paragraph (not a span inside) -> checking, if there is an absolutely positioned drawing behind the paragraph
                localDrawingNode = findAbsoluteDrawingNodeByPixelPosition(getAllAbsoluteDrawingsOnPage(self.getNode(), pageLayout.getPageNumber($eventTarget)), event.clientX, event.clientY);
            }
            if (localDrawingNode && localDrawingNode.length === 1) { drawingNode = localDrawingNode; }
        }

        if (drawingNode.length > 0) {
            var isInsideTextFrame = (TOUCH_DEVICE && !SMALL_DEVICE) ? self.isNodeInsideTextframe($eventTarget) : ($eventTarget.closest(TEXTFRAME_NODE_SELECTOR).length > 0);
            // -> shapes in small devices are not moveable -> simplify text selection by allowing touchevent in textframe node. Otherwise it might be problematic
            //    to get the touchevent into an empty paragraph inside the text frame.
            if (isInsideTextFrame && isOnlyBorderMoveableDrawing(drawingNode)) {
                drawingNode = $(); // for touch devices (not small devices) the event target must be inside a text frame, for other devices it can also be the text frame node.
            }
        }

        // checking for selection on a field
        field = SMALL_DEVICE ? [] : $eventTarget.closest(FIELD_NODE_SELECTOR);

        if (field.length > 0) {
            // prevent default click handling of the browser
            event.preventDefault();

            // set focus to the document container (may be located in GUI edit fields)
            app.getView().grabFocus();

            // select the field
            const startPosition = getOxoPosition(activeRootNode, field[0], 0);
            selection.setTextSelection(startPosition, startPosition, { event });
            return;
        }

        // in read only mode allow text selection only (and drawing selection, task 30041)
        if (readOnly && drawingNode.length === 0) {
            selection.processBrowserEvent(event);
            return;
        }

        if (drawingNode.length > 0) {

            // prevent default click handling of the browser -> this avoids for example scrolling page on touch devices, if drawing shall be moved
            var avoidPreventDefault = app.isTextApp() && SMALL_DEVICE && event.type === 'touchstart'; // 67946 -> no move on small devices in OX Text
            if (!avoidPreventDefault) { event.preventDefault(); }

            // converting selection overlay node to 'real' drawing node in the document (if required)
            if (drawingNode.hasClass('selectiondrawing')) {
                drawingNode = self.getSelection().getDrawingNodeFromDrawingSelection(drawingNode);
                if (!drawingNode) {
                    globalLogger.warn('PageHandlerMixin.pageProcessMouseDownHandler(): Failed to determine drawing node from drawing selection!');
                    return;
                }
            }

            // set focus to the document container (may be located in GUI edit fields);
            // This line needs to be executed after a selection drawing was switched to the 'real' drawingNode,
            // as it repaints selection, and (selection) drawingNode is not pointing to DOM node anymore
            app.getView().grabFocus();

            // do nothing if the drawing is already selected (prevent endless
            // recursion when re-triggering the event, to be able to start
            // moving the drawing immediately, see below)
            // Also create a drawing selection, if there is an additional text frame selection,
            // that also has a text selection. In this case the text selection is removed.
            if (!isSelected(drawingNode) || selection.isAdditionalTextframeSelection()) {

                // select the drawing
                const startPosition = getOxoPosition(activeRootNode, drawingNode[0], 0);
                if (startPosition) {
                    selection.setTextSelection(startPosition, increaseLastIndex(startPosition), { event });
                } else { // TODO: sometimes the position cannot be determined
                    globalLogger.warn('PageHandlerMixin.pageProcessMouseDownHandler(): Failed to determine position of drawing node!');
                }

                // explicitly start movement tracking (current DOM event has already bubbled up to the page element,
                // therefore tracking observer will not see it anymore)
                drawingNode.data("dispatcher")?.process(drawingNode[0], event.originalEvent);
            }

            return; // no browser handling for this event
        }

        // checking for a selection on a resize node
        if (isResizeNode($eventTarget)) {
            return; // no browser handling for this event
        }

        // Avoiding that the Safari Browser expands a right click to a word selection
        if (rightClick && _.browser.Safari) { selection.storeCurrentSelectionState(); }

        // clicked somewhere else: calculate logical selection from browser selection, after browser has processed the mouse event
        selection.processBrowserEvent(event);
    }

    // public methods -----------------------------------------------------

    /**
     * Getting the handler function that is used for process mouse down and touchstart
     * events on the page node.
     *
     * @returns {Function}
     *  The handler function for the 'mousedown' and the 'touchstart' event.
     */
    this.getPageProcessMouseDownHandler = function () {
        return pageProcessMouseDownHandler;
    };
}
