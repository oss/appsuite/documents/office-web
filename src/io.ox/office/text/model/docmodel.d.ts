/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Size } from "@/io.ox/office/tk/algorithms";
import { DocumentAttributes } from "@/io.ox/office/editframework/model/editmodel";
import CharacterStyleCollection from "@/io.ox/office/textframework/format/characterstyles";
import ParagraphStyleCollection from "@/io.ox/office/textframework/format/paragraphstyles";
import TableStyleCollection from "@/io.ox/office/textframework/format/tablestyles";
import TableRowStyleCollection from "@/io.ox/office/textframework/format/tablerowstyles";
import TextBaseModel, { TextBaseAttributePoolMap } from "@/io.ox/office/textframework/model/editor";
import TableCellStyleCollection, { TableCellAttributes } from "@/io.ox/office/text/format/tablecellstyles";
import DrawingStyleCollection from "@/io.ox/office/text/format/drawingstyles";
import PageStyleCollection, { PageAttributes } from "@/io.ox/office/text/format/pagestyles";
import PageHandlerMixin from "@/io.ox/office/text/model/pagehandlermixin";
import SelectionBox from "@/io.ox/office/textframework/view/selectionbox";
import TextApp from "@/io.ox/office/text/app/application";

// types ======================================================================

interface TextDocumentAttributes extends DocumentAttributes {

    /**
     * Default tab width in all paragraphs without own tab stop settings, in
     * 1/100 mm.
     */
    defaultTabStop: number;

    /**
     * Current zoom factor.
     */
    zoom: { type?: string; value?: number };

    changeTracking: boolean;

    usedLanguages: string[];

    usedLanguagesEa: string[];

    usedLanguagesBidi: string[];
}

export interface DocAttributePoolMap extends TextBaseAttributePoolMap {
    document: [TextDocumentAttributes];
    cell: [TableCellAttributes];
    page: [PageAttributes];
}

// class TextModel ============================================================

// declaration merging to add all properties of base interfaces automatically
interface TextModel extends PageHandlerMixin { }

declare class TextModel extends TextBaseModel<DocAttributePoolMap> implements PageHandlerMixin {

    declare readonly docApp: TextApp;

    readonly characterStyles: CharacterStyleCollection<TextModel>;
    readonly paragraphStyles: ParagraphStyleCollection<TextModel>;
    readonly tableStyles: TableStyleCollection<TextModel>;
    readonly tableRowStyles: TableRowStyleCollection<TextModel>;
    readonly tableCellStyles: TableCellStyleCollection;
    readonly drawingStyles: DrawingStyleCollection;
    readonly pageStyles: PageStyleCollection;

    public constructor(docApp: TextApp, options?: object);

    getSelectionState(): object;
    setSelectionState(): void;
    isIncreasedDocContent(): boolean;
    getDefaultTextForTextFrame(): string | null;
    handleTextFrameAutoFit(state: boolean): void;
    insertShapeWithDefaultBox(presetShapeId: string): JPromise;
    insertShape(presetShapeId: string, size: Size | null, options?: object): JPromise;
    insertShapePreparation(presetShapeId: string): boolean;
    getSelectionBox(): SelectionBox | null | undefined;
    getDefaultShapeSize(): object;
    isInsertShapeActive(): boolean;
    getReplaceImplicitParagraphAttrs(para: JQuery): Dict;
    transformDrawings(angle: number, flipH: boolean, flipV: boolean): void;
    checkInsertedEmptyTextShapes(operations: any[]): void;
    checkTextListAvailability(): void;
    toggleDrawingToTextMode(): void;
    isDraftMode(): boolean;
    toggleDraftMode(state: boolean): void;
    isPageBreakMode(): boolean;
    togglePageBreakMode(state: boolean): void;

    protected serializeToJSON(): object;
}

export default TextModel;
