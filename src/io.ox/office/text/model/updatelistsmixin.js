/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { SMALL_DEVICE, convertHmmToLength, convertLengthToHmm, getBooleanOption, getDomNode, getStringOption } from '@/io.ox/office/textframework/utils/textutils';
import { LIST_BORDER_NODE_SELECTOR, LIST_LABEL_NODE_SELECTOR, LIST_PARAGRAPH_ATTRIBUTE, LIST_PARAGRAPH_MARKER, PAGE_BREAK_SELECTOR,
    PARAGRAPH_NODE_IS_FIRST_IN_LIST, PARAGRAPH_NODE_SELECTOR, createListLabelNode, findFirstPortionSpan, hasMSPageHardbreakNode,
    isChangeTrackRemoveNode, isListParagraphNode, isManualPageBreakNode, isPageNode } from '@/io.ox/office/textframework/utils/dom';
import { FILTER_MODULE_NAME } from '@/io.ox/office/tk/utils/io';
import { getExplicitAttributes, getCssTextDecoration } from '@/io.ox/office/editframework/utils/attributeutils';

// mix-in class UpdateListsMixin ======================================

/**
 * A mix-in class for the document model providing the handler functions
 * that keep lists up-to-date.
 *
 * @param {Application} app
 *  The application containing this instance.
 */
export default function UpdateListsMixin(app) {

    var // self reference for local functions
        self = this,
        // a collector for all paragraph nodes that need to be updated
        paragraphs = $(),
        // whether not only the current paragraph needs to be updated
        forcedFullUpdate = false,
        // collects information for debounced list update
        updateListsDebouncedOptions = {};

    // private methods ----------------------------------------------------

    /**
     * Collecting information about the event, that triggered the list update. This update can be significantly
     * accelerated by modifying only paragraphs with specified list IDs.
     *
     * @param {HTMLElement|jQuery} [paragraph]
     *  Optional paragraph or set of paragraphs, that need to be updated. If not specified all paragraphs in the
     *  document will to be updated.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.useSelectedListStyleIDs=false]
     *      If set to true, the update list process can be accelerated by
     *      only modifying paragraphs with correct list style ID set.
     *  @param {String|String[]} [options.listStyleId, '']
     *      The listStyleId of the paragraph.
     *  @param {Number} [options.listStyleLevel = -1]
     *      The listStyleLevel of the paragraph.
     *  @param {Boolean} [options.paraInsert = false]
     *      Whether a paragraph was inserted (or got the list attribute).
     *  @param {Boolean} [options.splitInNumberedList = false]
     *      Whether a paragraph with a numbered was splitted.
     */
    function registerListUpdate(paragraph, options) {

        // Performance: Registering whether only specified style IDs need to be updated in this list update (never overwriting, after 'false' was set).
        // This is the case after splitting of a paragraph or after deleting a paragraph
        if ((updateListsDebouncedOptions.useSelectedListStyleIDs === undefined) || (updateListsDebouncedOptions.useSelectedListStyleIDs === true)) {
            updateListsDebouncedOptions.useSelectedListStyleIDs = getBooleanOption(options, 'useSelectedListStyleIDs', false);

            // checking, whether new paragraphs with list style IDs were inserted.
            // It is sufficient, that only one call of this function sets the value to 'true'.
            if ((updateListsDebouncedOptions.paraInsert === undefined) || (updateListsDebouncedOptions.paraInsert === false)) {
                updateListsDebouncedOptions.paraInsert = getBooleanOption(options, 'paraInsert', false);
            }

            // check, if only operations with 'splitInList' triggered this registerListUpdate. In this case the paragraph is marked that was
            // created by the split. Only this and the following paragraphs need to be updated with new numbers. This new created paragraph
            // has the flag 'splitInList'.
            if ((updateListsDebouncedOptions.splitInNumberedList === undefined) || (updateListsDebouncedOptions.splitInNumberedList === true)) {
                updateListsDebouncedOptions.splitInNumberedList = getBooleanOption(options, 'splitInNumberedList', false);
            }

            // also collecting all the affected listStyleIDs (listStyleLevels are ignored yet)
            if (getBooleanOption(options, 'useSelectedListStyleIDs', false)) {
                updateListsDebouncedOptions.listStyleIds = updateListsDebouncedOptions.listStyleIds || [];
                if (_.isArray(options.listStyleId)) {
                    updateListsDebouncedOptions.listStyleIds = updateListsDebouncedOptions.listStyleIds.concat(options.listStyleId);
                } else {
                    updateListsDebouncedOptions.listStyleIds.push(getStringOption(options, 'listStyleId', ''));
                }
            }
        }

        // whether only the specified paragraph needs to be updated
        // onlySingleParagraph = getBooleanOption(options, 'singleParagraph', false);

        // store the new paragraph in the collection (jQuery keeps the collection unique)
        if (paragraph) {
            paragraphs = paragraphs.add(paragraph);
        } else if (paragraphs.length > 0) {
            forcedFullUpdate = true; // no paragraph specified -> full update is required (even if a paragraph is specified in a later call of this function)
            paragraphs = $(); // empty the list, if it was filled before
        }
    }

    /**
     * The deferred callback for the list update.
     *
     * @returns {jQuery.Promise}
     *  A promise that is resolved, when all paragraphs are updated. Asynchronous updating of
     *  lists happens only during loading the document.
     */
    function execUpdateLists() {

        // a promise that is resolved, when all paragraphs are updated.
        const updateListPromise = self.updateLists((paragraphs.length > 0 && !forcedFullUpdate) ? paragraphs : null);

        paragraphs = $();
        forcedFullUpdate = false;

        return updateListPromise;
    }

    // public methods -----------------------------------------------------

    /**
     * Updates all paragraphs that are part of any bullet or numbering
     * list.
     *
     * @param {jQuery} [paragraphs]
     *  Optional set of paragraphs, that need to be updated. If not specified
     *  all paragraphs in the document need to be updated. This happens after
     *  the document was loaded successfully. In this case the option 'async'
     *  needs to be set to true.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.async=false]
     *      If set to true, all lists are updated asynchronously. This
     *      should happen only once when the document is loaded.
     *
     * @returns {jQuery.Promise}
     *  A promise that is resolved, when all paragraphs are updated.
     */
    this.updateLists = function (paragraphs, options) {

        var listItemCounter = [],
            // a copy of the list item counter for paragraphs on the page node
            pageListItemCounter = null,
            // list item counter for deleted paragraphs (change-track)
            deletedlistItemCounter = [],
            listParagraphIndex = [], // paragraph index in a list
            deletedlistParagraphIndex = [], // paragraph index in a list for deleted paragraphs (change-track)
            // list of all paragraphs in the document
            paragraphNodes = null,
            // the deferred, whose promise will be returned
            def = null,
            // Performance: Whether this list update was triggered by an action with specified style id
            useSelectedListStyleIDs = updateListsDebouncedOptions.useSelectedListStyleIDs,
            // Performance: Whether there are list styles registered for updating
            doNothing = false,
            // Performance: An array with listStyleIds of splitted paragraphs
            allListStyleIds = null,  // Performance: Only for splitted paragraphs
            // Performance: An array with listStyleLevels of splitted paragraphs (same order as in listStyleIds)
            // allListStyleLevels = null;
            // Performance: If only paragraphs with numbered lists were splitted in the registration phase,
            // it is sufficient to update all paragraphs starting from the one paragraph that was marked
            // with 'splitInNumberedList'.
            splitInNumberedList = updateListsDebouncedOptions.splitInNumberedList,
            // whether the splitted paragraph in the numbered list was found
            splittedNumberedListFound = false,
            // whether DOM manipulations can be suppressed. This is the case for numbered
            suppressDomManipulation = false,
            // whether to the deleted paragraph list numbering (change-track)
            showDeletedParagraphNumbering = false,
            // the list root node of the previous paragraph
            lastRootNode = null,
            // the list root node of the current paragraph
            currentRootNode = null,
            // the previous paragraph node
            lastParagraphNode = null,
            // the maximum number of paragraphs that are updated synchronously
            MAX_SYNC_PARA_COUNT = 25,
            // a collector for those list styles, for that the property "startOverride" was already evaluated
            startOverrideApplied = {};

        function updateListInParagraph(para) {

            // always remove an existing label
            var elementAttributes = self.paragraphStyles.getElementAttributes(para),
                paraAttributes = elementAttributes.paragraph,
                listStyleId = paraAttributes.listStyleId,
                listLevel = paraAttributes.listLevel,
                oldLabel = null,
                updateParaTabstops = false,
                removeLabel = false,
                updateList = false,
                // the list collection object
                listCollection = self.getListCollection(),
                // an array with all list style ids, that share the common base (and are counted together (40792))
                listFamily = listStyleId !== '' ? listCollection.getAllListStylesWithSameBaseStyle(listStyleId) : null,
                // using the first list id of a list family as counter (inside listItemCounter and listParagraphIndex)
                listCounterId = (listFamily && listFamily.length > 1) ? listFamily[0] : listStyleId,
                // whether the pararaph is deleted (change-track)
                deletedParagraph = isChangeTrackRemoveNode(para),
                // whether the list has no label
                noListLabel = paraAttributes.listLabelHidden === true,
                // the list style attributes specified by listStyleId and listLevel
                listStyleAttrs = listStyleId !== '' ? listCollection.getListLevel(listStyleId, listLevel || 0) : null,
                // an optional start value specified at a list style, that is not the list style used for counting (DOCS-3463)
                hiddenListStyleStartValue = (listStyleId !== listCounterId && listStyleAttrs && listStyleAttrs.listStartValue) ? listStyleAttrs.listStartValue : -1,
                // evaluating the flag "startOverride" -> if this is the first appearance of this list style in this listLevel, then the specified start value of this list level must be used (DOCS-3466)
                listOverrideStartValue = (listStyleAttrs && listStyleAttrs.listOverrideStartValue && !startOverrideApplied[listStyleId + '_' + listLevel]) ? listStyleAttrs.listOverrideStartValue : -1;

            function updateListItemCounter(itemCounter, paragraphIndex) {

                if (!itemCounter[listCounterId]) {
                    itemCounter[listCounterId] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    paragraphIndex[listCounterId] = 0;
                }

                if (!noListLabel) {
                    itemCounter[listCounterId][listLevel]++;
                    paragraphIndex[listCounterId]++;
                }

                if (paraAttributes.listStartValue >= 0) { // the listStartValue is set as hard attribute at the paragraph, not at the list style
                    itemCounter[listCounterId][listLevel] = paraAttributes.listStartValue;
                } else if (hiddenListStyleStartValue && hiddenListStyleStartValue > itemCounter[listCounterId][listLevel]) {
                    // increasing once the counter for the complete list family, if an increased value is required -> handling like a hard attribute at the paragraph
                    itemCounter[listCounterId][listLevel] = hiddenListStyleStartValue; // DOCS-3463
                } else if (listOverrideStartValue > 0) {
                    itemCounter[listCounterId][listLevel] = listOverrideStartValue; // DOCS-3466
                    startOverrideApplied[listStyleId + '_' + listLevel] = 1; // the new start value must only be applied at first occurence of paragraph with this listStyleId
                }

                // TODO: reset sub-levels depending on their 'levelRestartValue' attribute
                var subLevelIdx = listLevel + 1;
                for (; subLevelIdx < 10; subLevelIdx++) {
                    itemCounter[listCounterId][subLevelIdx] = 0;
                }

                // fix level counts of non-existing upper levels
                subLevelIdx = listLevel - 1;
                for (; subLevelIdx >= 0; subLevelIdx--) {
                    if (itemCounter[listCounterId][subLevelIdx] === 0) {
                        itemCounter[listCounterId][subLevelIdx] = 1;
                    }
                }

            }

            // helper function to avoid that lists leave the region of drawings, headers, comments, ... (54460)
            function handleListRootNode() {

                if (lastParagraphNode && lastParagraphNode.nextSibling === getDomNode(para)) { // checking neighborhood of following paragraphs (avoiding call of closest)
                    currentRootNode = lastRootNode;
                } else {
                    currentRootNode = getDomNode($(para).closest(LIST_BORDER_NODE_SELECTOR)); // finding the border for lists
                }

                if (lastRootNode && currentRootNode !== lastRootNode) {

                    if (isPageNode(lastRootNode)) {
                        pageListItemCounter = _.copy(listItemCounter, true); // saving the list item counter of the page
                    }

                    if (pageListItemCounter && isPageNode(currentRootNode)) {
                        listItemCounter = _.copy(pageListItemCounter, true); // restoring the list item counter of the page (if exists)
                    } else {
                        listItemCounter = []; // resetting the list item counter for the new root node
                    }

                }

                lastRootNode = currentRootNode;
                lastParagraphNode = getDomNode(para);
            }

            // Updating paragraphs, that are no longer part of lists (for example after 'Backspace')
            if ($(para).data('removeLabel')) {
                removeLabel = true;
                $(para).removeData('removeLabel');
            }

            if ($(para).data('updateList')) {
                updateList = true;
                $(para).removeData('updateList');
            }

            if ($(para).data('splitInNumberedList')) {  // might be set at more than one paragraph
                splittedNumberedListFound = true;
                $(para).removeData('splitInNumberedList');
            }

            // Setting the valid root node for the list (before any return can be used)
            handleListRootNode();

            // Performance II: Do nothing, if the list style ID does not fit -> only modify paragraphs with correct list style ID
            // -> could be improved with check of list level (taking care of styles including upper levels)
            if (useSelectedListStyleIDs && !_.contains(allListStyleIds, listStyleId) && (!listFamily || !_.contains(listFamily, listStyleId)) && !removeLabel && !updateList) { return; }

            // Performance III: No DOM manipulation, if the split happened in a numbered list and the marked
            // paragraph (marked with 'splitInNumberedList') was not found yet. Only for following paragraphs
            // the DOM manipulations are necessary. But the number counting is necessary for all paragraphs.
            suppressDomManipulation = (splitInNumberedList && !splittedNumberedListFound);

            if (!suppressDomManipulation) {
                oldLabel = $(para).children(LIST_LABEL_NODE_SELECTOR);
                updateParaTabstops = oldLabel.length > 0;
                oldLabel.remove();
            }

            // Removig the marker for the first paragraph in a list
            $(para).removeClass(PARAGRAPH_NODE_IS_FIRST_IN_LIST);

            // Marking list paragraphs for faster update
            $(para).toggleClass(LIST_PARAGRAPH_MARKER, listStyleId !== '');

            // saving list style ID as attribute (for performance reasons, if list is modified)
            if (listStyleId) {
                $(para).attr(LIST_PARAGRAPH_ATTRIBUTE, listStyleId);
            } else {
                $(para).removeAttr(LIST_PARAGRAPH_ATTRIBUTE);
            }

            if (listStyleId !== '' || listLevel) {

                if (listLevel < 0) {
                    // is a numbering level assigned to the current paragraph style?
                    listLevel = listCollection.findIlvl(listStyleId, elementAttributes.styleId);
                }
                if (listLevel !== -1 && listLevel < 10) {

                    if (showDeletedParagraphNumbering) {
                        updateListItemCounter(deletedlistItemCounter, deletedlistParagraphIndex);
                    }

                    if (!deletedParagraph) {
                        updateListItemCounter(listItemCounter, listParagraphIndex);
                    }

                    // Performance III: After updating the listItemCounter and listParagraphIndex, the following
                    // DOM manipulations can be ignored, if 'suppressDomManipulation' is set to true. This is the
                    // case for numbered lists in which the added paragraph marked with 'splitInNumberedList' was
                    // not found yet.
                    if (suppressDomManipulation) { return; }

                    updateParaTabstops = true;
                    var listItemCounterItem = deletedParagraph ? deletedlistItemCounter[listCounterId] : listItemCounter[listCounterId];
                    var listParagraphIndexItem = deletedParagraph ? deletedlistParagraphIndex[listCounterId] : listParagraphIndex[listCounterId];
                    var listObject = listCollection.formatNumber(listCounterId, listLevel, listItemCounterItem, listParagraphIndexItem, elementAttributes.styleId, getExplicitAttributes(para, 'paragraph'));
                    var tab = !listObject.labelFollowedBy || listObject.labelFollowedBy === 'listtab';

                    if (!tab && (listObject.labelFollowedBy === 'space')) {
                        listObject.text += '\xa0'; //add non breaking space
                    }

                    var numberingElement = createListLabelNode(noListLabel ? '' : listObject.text);

                    // setting marker for first paragraphs in lists
                    $(para).toggleClass(PARAGRAPH_NODE_IS_FIRST_IN_LIST, (listItemCounterItem && listItemCounterItem[listLevel] === 1));

                    var elementCharAttributes = null;
                    if (self.hasOoxmlTextCharacterFilterSupport()) {
                        // OOXML: the character attributes at paragraphs are used for list labels (DOCS-4211)
                        elementCharAttributes = getExplicitAttributes(para, 'character');
                    } else {
                        var span = findFirstPortionSpan(para);
                        elementCharAttributes = self.characterStyles.getElementAttributes(span).character;
                    }

                    var listSpans = numberingElement.children('span');

                    if (deletedParagraph) {
                        if (!showDeletedParagraphNumbering) {
                            listSpans.text('\u2014');
                        }
                        listSpans.attr('data-change-track-removed', true);
                        listSpans.attr('data-change-track-removed-author', $(para).attr('data-change-track-removed-author'));
                    }

                    var paraCharStyles = null;
                    if (listObject.imgsrc && !noListLabel) {
                        var absUrl = app.getServerModuleUrl(FILTER_MODULE_NAME, { action: 'getfile', get_filename: listObject.imgsrc }),
                            imageWidth = convertHmmToLength(listObject.imgwidth, 'pt');
                        if (!imageWidth) {
                            imageWidth = elementCharAttributes.fontSize;
                        }

                        var image = $('<div>', { contenteditable: false })
                            .addClass('drawing')
                            .data('url', listObject.imgsrc)
                            .append($('<div>').addClass('content')
                                .append($('<img>', { src: absUrl }).css('width', imageWidth + 'pt'))
                            );

                        self.characterStyles.updateElementLineHeight(image, paraAttributes.lineHeight, elementCharAttributes);
                        $(image).css('height', elementCharAttributes.fontSize + 'pt');
                        $(image).css('width', elementCharAttributes.fontSize + 'pt');
                        numberingElement.prepend(image);

                    } else if (!app.isODF() && (elementCharAttributes || elementAttributes.styleId)) {
                        // Map certain character styles from the following span to the label ignoring others
                        // which doesn't fit this special use case. We prefer span character attributes over
                        // style character attributes.
                        // Don't map background color and underline as MS Word also don't support them for
                        // list-labels.

                        paraCharStyles = self.paragraphStyles.getStyleAttributeSet(elementAttributes.styleId);

                        if (elementCharAttributes.fontSize || paraCharStyles.character.fontSize) {
                            listSpans.css('font-size', (elementCharAttributes.fontSize || paraCharStyles.character.fontSize) + 'pt');
                        }
                        if (elementCharAttributes.fontName || paraCharStyles.character.fontName) {
                            listSpans.css('font-family', self.getCssFontFamily(elementCharAttributes.fontName || paraCharStyles.character.fontName));
                        }
                        if (elementCharAttributes.bold || paraCharStyles.character.bold) {
                            listSpans.css('font-weight', (elementCharAttributes.bold || paraCharStyles.character.bold) ? 'bold' : 'normal');
                        }
                        if (elementCharAttributes.italic || paraCharStyles.character.italic) {
                            listSpans.css('font-style', (elementCharAttributes.italic || paraCharStyles.character.italic) ? 'italic' : 'normal');
                        }

                        listSpans.css('text-decoration', getCssTextDecoration(elementCharAttributes));

                        if (elementCharAttributes.color || paraCharStyles.character.color) {
                            listSpans.css('color', self.getCssColor((elementCharAttributes.color || paraCharStyles.character.color), 'text'));
                        }
                        if (elementCharAttributes.fillColor || paraCharStyles.character.fillColor) {
                            listSpans.css('background-color', self.getCssColor((elementCharAttributes.fillColor || paraCharStyles.character.fillColor), 'fill'));
                        }

                    } else if (app.isODF() && (elementAttributes.character || elementAttributes.styleId)) {

                        paraCharStyles = self.paragraphStyles.getStyleAttributeSet(elementAttributes.styleId);
                        // Map certain character styles from the paragraph style to the label ignoring others
                        // which doesn't fit this special use case. We prefer paragraph character attributes over
                        // style character attributes.
                        // Don't map background color and underline as MS Word also don't support them for
                        // list-labels.
                        if (elementAttributes.character.fontSize || paraCharStyles.character.fontSize) {
                            listSpans.css('font-size', (elementAttributes.character.fontSize || paraCharStyles.character.fontSize) + 'pt');
                        }
                        if (elementAttributes.character.fontName || paraCharStyles.character.fontName) {
                            listSpans.css('font-family', self.getCssFontFamily(elementAttributes.character.fontName || paraCharStyles.character.fontName));
                        }
                        if (elementAttributes.character.bold || paraCharStyles.character.bold) {
                            listSpans.css('font-weight', (elementAttributes.character.bold || paraCharStyles.character.bold) ? 'bold' : 'normal');
                        }
                        if (elementAttributes.character.italic || paraCharStyles.character.italic) {
                            listSpans.css('font-style', (elementAttributes.character.italic || paraCharStyles.character.italic) ? 'italic' : 'normal');
                        }

                        listSpans.css('text-decoration', getCssTextDecoration(elementAttributes.character));

                        if (elementAttributes.character.color || paraCharStyles.character.color) {
                            listSpans.css('color', self.getCssColor((elementAttributes.character.color || paraCharStyles.character.color), 'text'));
                        }
                        if (paraCharStyles.character.fillColor) {
                            listSpans.css('background-color', self.getCssColor(paraCharStyles.character.fillColor, 'fill'));
                        }
                    }

                    if (listObject.color) {
                        listSpans.css('color', self.getCssTextColor(listObject.color, [paraAttributes.fillColor, listObject.fillColor]));
                    }
                    self.characterStyles.updateElementLineHeight(numberingElement, paraAttributes.lineHeight, elementAttributes.character);
                    var minWidth = 0;
                    var isNegativeIndent = listObject.firstLine < listObject.indent;

                    if (isNegativeIndent) {
                        var labelWidth = listObject.indent - listObject.firstLine;
                        if (tab) {
                            minWidth = labelWidth;
                        }
                        numberingElement.css('margin-left', (-listObject.indent + listObject.firstLine) / 100 + 'mm');
                    } else {
                        numberingElement.css('margin-left', (listObject.firstLine - listObject.indent) / 100 + 'mm');
                    }

                    // also handling a negative text indentation at the paragraph. This need to be added to the min-width of the label (54618)
                    var textIndent = parseInt($(para).css('textIndent'), 10);
                    if (!tab && !noListLabel && textIndent < 0) { minWidth -= convertLengthToHmm(textIndent, 'px'); }

                    numberingElement.css('min-width', minWidth / 100 + 'mm');

                    // #35649 - MS Word pagebreak inside list title not positioned correctly
                    if (isManualPageBreakNode($(para)) && hasMSPageHardbreakNode($(para)) && $(para).find(PAGE_BREAK_SELECTOR).length > 0) {
                        $(para).find(PAGE_BREAK_SELECTOR).first().after(numberingElement);
                    } else {
                        $(para).prepend(numberingElement);
                    }

                    if (tab || noListLabel) {

                        var realNumberingElementPixelWidth = numberingElement[0].offsetWidth;
                        var realWidth = convertLengthToHmm(realNumberingElementPixelWidth, 'px');
                        var realEndPos = listObject.firstLine + realWidth;
                        var realEndPosInPixel = convertHmmToLength(listObject.firstLine, 'px', 1) + realNumberingElementPixelWidth;
                        var listIndentInPixel = convertHmmToLength(listObject.indent, 'px', 1);
                        var defaultTabstop = self.globalConfig.defaultTabStop;
                        var targetPosition = 0;
                        var minWidthValue = 0;

                        if (isNegativeIndent && listIndentInPixel >= realEndPosInPixel) { // comparing pixel to avoid precision error with hmm (DOCS-3242)
                            targetPosition = listObject.indent;
                        } else {
                            var tabstop = null;
                            if (paraAttributes && paraAttributes.tabStops) {
                                tabstop = _.find(paraAttributes.tabStops, function (tab) {
                                    return realEndPos <= tab.pos;
                                });
                            }
                            if (tabstop) {
                                targetPosition = tabstop.pos;
                            } else if (defaultTabstop > 0) {
                                // using factor '0.96' to avoid ugly large list-labels, if they are not really required (40792)
                                // -> factor should be as near to 1 as possible to avoid other negative side effects (44186)
                                targetPosition = (1 + (Math.floor((0.96 * realEndPos) / defaultTabstop))) * defaultTabstop;
                            } else {
                                targetPosition = realEndPos;
                            }
                        }

                        minWidthValue = (targetPosition - listObject.firstLine);
                        minWidthValue = minWidthValue ? minWidthValue : 0;
                        if (textIndent < 0) { minWidthValue -= convertLengthToHmm(textIndent, 'px'); } // (54618)
                        if (noListLabel && app.isODF()) { minWidthValue = 0; } // (52818)
                        if (minWidthValue > 0) { numberingElement.css('min-width', minWidthValue / 100 + 'mm'); }
                    }

                    // in draft mode, margins of list paragraphs have to be converted from mm to %
                    if (SMALL_DEVICE && para.style.marginLeft.slice(-1) !== '%') {
                        var ratio = 100  / (self.getPageWidth() - (2 * self.getPagePaddingLeft()));
                        $(para).css('margin-left', (ratio * parseInt($(para).css('margin-left'), 10)) + '%');
                        $(para).data('draftRatio', ratio);
                    }
                }
            }

            if (updateParaTabstops) {
                self.paragraphStyles.updateTabStops(para);
            }
        }

        // receiving list of all document paragraphs
        paragraphNodes = paragraphs ? paragraphs : self.getCurrentRootNode().find(PARAGRAPH_NODE_SELECTOR);

        // Performance I: Simplified process, if list update was triggered by splitting or deleting of paragraphs
        // Only check those paragraphs, that have a list label node.
        if (useSelectedListStyleIDs) {
            // Reducing the number of paragraphs, that need to be updated. This can happen now before it is checked,
            // if the paragraph has a list style ID set. Affected are only paragraphs, that contain already the
            // list label node (DOM.LIST_LABEL_NODE_SELECTOR), or, if they are newly inserted, they are branded
            // with the 'updateList' or 'removeLabel' (after backspace) data.
            if (updateListsDebouncedOptions.paraInsert) {
                paragraphNodes = paragraphNodes.filter(function () {
                    return isListParagraphNode(this) || $(this).data('updateList');
                });
            } else {
                paragraphNodes = paragraphNodes.filter(function () {
                    return isListParagraphNode(this);
                });
            }

            allListStyleIds = updateListsDebouncedOptions.listStyleIds;

            // Update of bullet lists can be completely ignored, except new paragraphs were inserted
            if (!updateListsDebouncedOptions.paraInsert) {
                allListStyleIds = _.reject(allListStyleIds, function (styleID) {
                    return self.getListCollection().isAllLevelsBulletsList(styleID);
                });
            }

            // If the list is empty, there is nothing to do
            doNothing = _.isEmpty(allListStyleIds);
        }

        if (doNothing) {
            def = $.when();
        } else if (getBooleanOption(options, 'async', false)) {
            def = self.iterateArraySliced(paragraphNodes, updateListInParagraph);
        } else {
            if (paragraphNodes.length < MAX_SYNC_PARA_COUNT) {
                paragraphNodes.each(function () { updateListInParagraph(this); });
                def = $.when();
            } else {
                // large number of paragraphs -> forcing asynchronous list update (61921)
                def = self.iterateArraySliced(paragraphNodes, updateListInParagraph);
            }
        }

        // enabling new registration for debounced list update
        updateListsDebouncedOptions = {};

        return def.promise().always(function () {
            self.trigger('listformatting:done'); // informing the listeners, that the list formatting is done
        });
    };

    // the debounced list handler function
    this.updateListsDebounced = this.debounce(registerListUpdate, execUpdateLists, { delay: "animationframe" });
}
