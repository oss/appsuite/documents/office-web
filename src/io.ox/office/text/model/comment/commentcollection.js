/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import ox from '$/ox';

import { ary } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getBooleanOption } from '@/io.ox/office/tk/utils';
import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import { calculateUserId, OX_PROVIDER_ID } from '@/io.ox/office/editframework/utils/operationutils';
import { CommentSettings, CommentModel } from '@/io.ox/office/text/model/comment/commentmodel';

// class CommentCollection ================================================

export default class CommentCollection extends ModelObject {

    constructor(docApp, docModel) {

        // base constructor
        super(docModel);

        // properties ---------------------------------------------------------

        // the self reference
        var self = this;
        // the document view instance
        var docView = docApp.getView(); // TODO: Too early, docView will be null
        // the object with the unsaved comment data
        var unsavedComment = null;
        // all comment models, sorted by their anchor address (row by row)
        var sortedComments = [];
        // the name of the current user
        var userName = null;
        // the selection state, when a new comment thread is generated
        var savedSelectionState = null;
        // the ID of a selected comment (this comment is not necessarily still selected)
        var selectedCommentId = null;
        // the list of authors
        var commentsAuthorList = [];
        // a reference to the temporary comment model in the collection
        var temporaryModel = null;
        // the ID of a temporary created comment model (there can always only be one)
        var TEMPORARY_COMMENT_ID = 'TEMPORARY_COMMENT_ID';

        // private methods ----------------------------------------------------

        /**
         * Register a comment and remove it from the comment lists.
         *
         * @param {CommentModel} commentModel
         *  The comment model that will be inserted into the sorted collection.
         *
         * @param {Number} index
         *  The position at which the comment model will be inserted into the collection.
         *  If the index is negative or larger than the length of the collection, the
         *  new comment model is inserted at the end of the collector.
         */
        function registerComment(commentModel, index) {

            var length = 0;

            if (index > -1) {
                length = sortedComments.length;
                if (index > length) {
                    globalLogger.error('CommentCollection: Invalid index for inserting comment into collection. Count: ' + length + ' and required index: ' + index);
                } else if (index === length) {
                    sortedComments.push(commentModel);
                } else {
                    sortedComments.splice(index, 0, commentModel);
                }

            } else {
                sortedComments.push(commentModel);
            }

        }

        /**
         * Unregister a comment and remove it from the comment lists.
         *
         * @param {String} id
         *  The ID of the comment to be removed.
         *
         * @returns {CommentModel|null}
         *  The removed comment model or null, if no model was removed.
         */
        function unregisterComment(id) {
            const index = sortedComments.findIndex(commentModel => commentModel.getId() === id);
            return (index < 0) ? null : ary.deleteAt(sortedComments, index);
        }

        /**
         * Keeping the list of comment authors up-to-date, when comments
         * are inserted or removed.
         *
         * @param {CommentModel} commentModel
         *  The inserted or removed comment model.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.register=true]
         *   Whether the comment model is registered or unregistered.
         */
        function updateAuthorList(commentModel, options) {

            if (!commentModel) { return; }

            // the number of comment authors
            var oldNumber = commentsAuthorList.length;
            // the comment author
            var author = commentModel.getAuthor();

            if (!author) { return; }

            if (getBooleanOption(options, 'register', true)) {
                if (_.indexOf(commentsAuthorList, author) < 0) { commentsAuthorList.push(author); }
            } else {
                // generating completely new list after deleting a comment -> there might be other comments from this author
                commentsAuthorList = [];
                self.iterateCommentModels(function (model) {
                    var localAuthor = model.getAuthor();
                    if (localAuthor && _.indexOf(commentsAuthorList, localAuthor) < 0) { commentsAuthorList.push(localAuthor); }
                }, self);
            }

            // inform listeners, that the comment author list is modified
            if (self.isImportFinished() && (oldNumber !== commentsAuthorList.length)) {
                docView.trigger('update:commentauthorlist');
            }
        }

        /**
         * Triggering an event at the view object.
         *
         * @param {jQuery.Event} event
         *  The jQuery event object.
         *
         * @param {*} eventInfo
         *  Some additional information sent with the event.
         */
        function triggerEvent(event, eventInfo) {
            docModel.trigger(event, eventInfo);
        }

        /**
         * Getting the user name for the current user who edits the document. This function
         * returns a promise, that will be resolved with the username.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the user name.
         */
        function getUserName() {

            if (userName !== null) {
                return $.when(userName);
            }
            /**
             *  fixing #Bug 68174 - "Better 'user name' for external invitees / collaborators"
             *  [https://bugs.open-xchange.com/show_bug.cgi?id=68174]
             *
             *  This solutions uses the `info.guest` flag as switch of how to retrieve a user's/client's name.
             *  In order to not end up with a cryptic generated name for an external invitee like 'User 10314',
             *  as it would happen with `info.operationName` one uses `info.displayName` for an identified guest.
             */
            return docApp.getUserInfo(ox.user_id).then(function (info) {
                userName = ((info.guest && info.displayName) || info.operationName);
                return userName;
            });
        }

        /**
         * After selecting a comment thread, a handler must be registered at the content root node
         * to deselect the comment thread, if the user clicks somewhere else.
         * Deselecting a comment happens simply by triggering the selection event 'select:threadedcomment'
         * without any valid comment.
         */
        function removeSelectionHandler(event) {
            if (!$(event.target).hasClass('ignorepageevent') && !docModel.isIgnoreClickEvent() && !$(event.target).closest('.comments-container').length) { // must include '.comments-container' and '.commentEditor' and still be in the DOM
                docModel.trigger('select:threadedcomment', null, removeSelectionHandler);
                // removing the marker for the selected comment
                setSelectedCommentId(null);
            }
        }

        /**
         * Setting the ID of the comment that is selected.
         *
         * This function is the only place where the variable 'selectedCommentId' can be
         * changed, because it compares the new id with the old comment id and informs
         * the commentLayer about the change, if required.
         *
         * @param {String|Null} id
         *  The ID of the selected comment. Or null, if no comment is selected.
         */
        function setSelectedCommentId(id) {
            var oldId = selectedCommentId;
            selectedCommentId = id; // do this only within this function
            if (oldId !== id) { docModel.getCommentLayer().selectedCommentChanged(oldId, id); }
        }

        /**
         * Aftern an undo of an insert of a main comment, it is possible that the children
         * have no valid parent (DOCS-2674). Therefore this functions cleans the parent IDs
         * of the children and makes them to parents.
         *
         * @param {String} id
         *  The ID of the removed comment.
         */
        function makeChildrenToParents(id) {
            self.iterateCommentModels(function (commentModel) {
                if (commentModel.getParentId() === id) { commentModel.setParentId(null); }
            }, self);
        }

        /**
         * After an insertComment operation that was generated for undo/redo, it is possble
         * that parent comments need to be converted back to child comments (DOCS-2684).
         *
         * Info: insertComment operations that have the property 'children' are only
         *       generated in the undo/redo process.
         *
         * @param {String} parentId
         *  The ID of the new parent comment.
         *
         * @param {String[]} allChildren
         *  The IDs of all children.
         */
        function makeParentsToChildren(parentId, allChildren) {
            _.each(allChildren, function (childId) {
                var childModel = self.getById(childId);
                if (childModel && !childModel.isReply()) { childModel.setParentId(parentId); }
            });
        }

        // public methods -----------------------------------------------------

        /**
         * Getting the complete set of all comments in the document.
         *
         * @returns {CommentModel[]}
         *  The sorted array with all comment models.
         */
        this.getCommentModels = function () {
            return sortedComments;
        };

        /**
         * Whether the comment collection is empty.
         *
         * @returns {Boolean}
         *  Whether there are no comment models in the comment collection.
         */
        this.isEmpty = function () {
            return sortedComments.length === 0;
        };

        /**
         * Getting the number of comments in the comment collection.
         *
         * @returns {Number}
         *  The number of comments in the comment collection.
         */
        this.getCommentNumber = function () {
            return sortedComments.length;
        };

        /**
         * Getting the sorted array with all comment models of visible comments.
         *
         * @returns {CommentModel[]}
         *  The sorted array with all comment models of visible comments.
         */
        this.getVisibleCommentModels = function () {
            return _.filter(sortedComments, function (commentModel) { return !commentModel.isHidden(); });
        };

        /**
         * Getting the number of visible comments in the comment collection.
         *
         * @returns {Number}
         *  The number of visible comments in the comment collection.
         */
        this.getVisibleCommentNumber = function () {
            return self.getVisibleCommentModels().length;
        };

        /**
         * Returns the comment specified by its id.
         *
         * @param {String} id
         *  The id of the main comment in the thread.
         *
         * @returns {CommentModel|Null}
         *  The comment model that is addressed by its id. Or null, if it cannot be found.
         */
        this.getById = function (id) {
            var commentModel = _.find(sortedComments, function (oneCommentModel) { return oneCommentModel.getId() === id; });
            return commentModel ? commentModel : null;
        };

        /**
         * Returns the comment specified by its index in the collection.
         *
         * @param {Number} index
         *  The index of the comment model in the collector.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.onlyVisible=false]
         *   Whether the specified index is related to the visible comments collector.
         *
         * @returns {CommentModel|Null}
         *  The comment model that is addressed by its index. Or null, if it cannot be found.
         */
        this.getByIndex = function (index, options) {
            var commentCollector = getBooleanOption(options, 'onlyVisible', false) ? self.getVisibleCommentModels() : sortedComments;
            var commentModel = commentCollector ? commentCollector[index] : null;
            return commentModel ? commentModel : null;
        };

        /**
         * Returns the comment specified by its date.
         *
         * @param {number} date
         *
         * @returns {CommentModel|Null}
         *  The comment model that is addressed by its date. Or null, if it cannot be found.
         */
        this.getByDateId = function (date) {
            var commentModel = _.find(sortedComments, function (oneCommentModel) { return new Date(oneCommentModel.getDate()).getTime() === date; });
            return commentModel ? commentModel : null;
        };

        /**
         * Returns the complete comment thread specified by a comment id.
         *
         * @param {String} parentId
         *  The id of that comment whose thread is searched.
         *
         * @returns {CommentModel[]}
         *  The comment models that are addressed by their parentId together with the parent. An empty array, if no child can be found.
         */
        this.getThreadById = function (parentId) {
            return _.filter(sortedComments, function (oneCommentModel) { return oneCommentModel.getParentId() === parentId || oneCommentModel.getId() === parentId; });
        };

        /**
         * Returns the comment children specified by a comment id.
         *
         * @param {String} parentId
         *  The id of that comment whose children are searched.
         *
         * @returns {CommentModel[]}
         *  The comment models that are addressed by their parentId. An empty array, if no children were found.
         */
        this.getChildrenById = function (parentId) {
            return _.filter(sortedComments, function (oneCommentModel) { return oneCommentModel.getParentId() === parentId; });
        };

        /**
         * Returns the IDs of a comment children specified by a comment id.
         *
         * @param {String} parentId
         *  The id of that comment whose children are searched.
         *
         * @returns {CommentModel[]}
         *  The comment IDs that are addressed by their parentId. An empty array, if no children were found.
         */
        this.getAllChildIds = function (parentId) {
            var allChildrenIDs = [];
            _.each(sortedComments, function (oneCommentModel) { if (oneCommentModel.getParentId() === parentId) { allChildrenIDs.push(oneCommentModel.getId()); } });
            return allChildrenIDs;
        };

        /**
         * Searching the neighbour comment model for another comment model specified by its comment ID.
         *
         * @param {String} commentId
         *  The comment ID of the comment model for that the neighbour is searched.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.next=true]
         *   Whether the next (true) or the previous (false) neighbour is searched.
         *  @param {Boolean} [options.onlyThreads=false]
         *   Whether only comment models are searched, that are no replies to other comments.
         *  @param {Boolean} [options.onlyVisible=false]
         *   Whether hidden comment models are allowed return values.
         *
         * @returns {Object}
         *  An object with two properties:
         *   - 'commentModel', that contains the found commentModel or null as value.
         *   - 'jump', that is true, if a jump to the beginning (if next is true) or the end
         *             (if next is false) of the document was required to find the model.
         */
        this.getNeighbourCommentModelById = function (commentId, options) {

            // the index of the comment model specified by the commentId inside the sorted comments collector
            var startIndex = sortedComments.indexOf(self.getById(commentId));
            // the maximum index allowed
            var maxIndex = self.getCommentNumber() - 1;
            // the index that is currently used for searching
            var searchIndex  = startIndex;
            // the comment model returned by this function
            var commentModel = null;
            // the number of searches in the comments collector
            var currentRuns = 0;
            // the maximum number of searches in the comments collector to avoid endless loops in error case
            var maxRuns = maxIndex + 1;
            // whether the next (true) or the previous (false) neighbour is searched.
            var next = getBooleanOption(options, 'next', true);
            // whether only comment models are searched, that are no replies to other comments.
            var onlyThreads = getBooleanOption(options, 'onlyThreads', false);
            // whether hidden comment models are allowed return values
            var onlyVisible = getBooleanOption(options, 'onlyVisible', false);
            // whether the search jumped to the start (if next is true) or to the end (if next is false)
            var jump = false;

            if (startIndex > -1) {

                while (!commentModel && currentRuns < maxRuns) {

                    currentRuns++;
                    jump = false;

                    searchIndex = next ? searchIndex + 1 : searchIndex - 1;

                    if (searchIndex < 0) {
                        searchIndex = maxIndex;
                        jump = true;
                    } else if (searchIndex > maxIndex) {
                        searchIndex = 0;
                        jump = true;
                    }

                    commentModel = self.getByIndex(searchIndex);

                    // checking, if found comment model is a reply
                    if (commentModel && onlyThreads && commentModel.isReply()) {
                        commentModel = null;
                    }

                    // checking visibility of found comment model (but allowing hidden thread, if at least one child is visible)
                    if (commentModel && onlyVisible && commentModel.isHidden()) {
                        if (!(onlyThreads && !commentModel.isReply() && self.hasVisibleChild(commentModel))) { // check, if there is at least one visible child
                            commentModel = null;
                        }
                    }
                }
            }

            return { commentModel, jump };
        };

        /**
         * Returns the threaded comment models anchored by the parent Id.
         *
         * Info: This is not required functionality by OX Text, but must be implemented because of the API.
         *
         * @param {String} parentId
         *  The id of the main comment in the thread.
         *
         * @param {Boolean} isInReverseOrder
         *  A flag that indicates whether or not reverting the model-order within the list of threaded comment models.
         *
         * @returns {CommentModel[]|Null}
         *  List of threaded comment models that are addressed by a comment thread (be it a sole comment or a parent comment), otherwise null.
         */
        this.getByAddress = function (parentId, isInReverseOrder, ignoreCommentsWithInvalidParent) {

            var commentList = [];
            var isFirstThreadInDocument = sortedComments.length && sortedComments[0].getId() === parentId; // is this the first thread -> take care of children

            commentList = sortedComments.filter(function (commentModel) {
                return commentModel.getParentId() === parentId || (isFirstThreadInDocument && !ignoreCommentsWithInvalidParent && commentModel.hasInvalidParent());
            });

            if (isInReverseOrder) { commentList.reverse(); }

            return commentList.length >= 1 ? commentList : null;
        };

        /**
         * Getting the selection state that was valid, when a new comment thread was started.
         *
         * @returns {Object}
         *  The selection state of this document.
         */
        this.getSavedSelectionState = function () {
            return savedSelectionState;
        };

        /**
         * A very special comment ID, that is only used for new comment threads, for that no
         * operations are generated. This must be handled correctly in the comments pane and
         * the commented range must also be made visible with 'temporary' range marker nodes.
         * The range marker nodes and the comment (all generated without operation) can be
         * connected with this ID.
         *
         * @returns {String}
         *  The ID of the one temporary comment model.
         */
        this.getTemporaryCommentId = function () {
            return TEMPORARY_COMMENT_ID;
        };

        /**
         * Whether the specified id is the id of the temporary comment model.
         *
         * @param {String} id
         *  The comment id to be checked.
         *
         * @returns {Boolean}
         *  Whether the specified id is the id of the temporary comment model.
         */
        this.isTemporaryCommentId = function (id) {
            return id === TEMPORARY_COMMENT_ID;
        };

        /**
         * Callback handler for the document operation 'insertComment'. Creates a new
         * comment model and stores this in the sorted collection.
         *
         * @param {OperationContext} context
         *  A wrapper representing the 'insertComment' operation.
         *
         * @param {jQuery} commentPlaceHolder
         *  The place holder node in the page content, that was generated for the new comment.
         *
         * @param {jQuery} allCommentPlaceHolders
         *  The sorted list of all place holder nodes in the page content. This is required to
         *  find a valid position for the new comment model inside the sorted collection.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.findPosition=false]
         *      If set to true, the position of the place holder node in the container of all
         *      placeholder nodes is searched. This is typically not required in the loading phase,
         *      but it must be done when the document is loaded with fastLoad and there are
         *      additional insertComment operations in the loading phase.
         *
         * @returns {CommentModel}
         *  The new generated comment model.
         */
        this.handleInsertCommentOperation = function (context, commentPlaceHolder, allCommentPlaceHolders, options) {

            // the author ID, that might be specified in the operation to reference the comment author
            var authorId = context.optStr('authorId');
            // the author name
            var author = context.optStr('author', { empty: true });
            // the provider ID for the author
            var providerId = context.optStr('providerId', { empty: true });
            // the user ID for the author
            var userId = context.optStr('userId', { empty: true });
            // the reference to the registered author in the list of authors
            var registeredAuthor = null;
            //  the children of the new inserted comment (DOCS-2684)
            var allChildren = context.optArr('children', null);

            if (authorId) {
                registeredAuthor = docApp.getRegisteredAuthorItemById(authorId);
                author = author || registeredAuthor.name;
                userId = userId ||  registeredAuthor.userId;
                providerId = providerId || registeredAuthor.providerId;

                // id:         authorId, // {String|Number}
                // name:       authorName, // non empty {String}
                // initials:   (getStringOption(authorOptions, 'initials', '').trim() || null), // non empty {String} || {Null}
                // providerId: (getStringOption(authorOptions, 'providerId', '').trim() || null), // non empty {String} || {Null}
                // userId:     (getStringOption(authorOptions, 'userId', '').trim() || null) // non empty {String} || {Null}
            }

            // additional settings for the comment model
            var settings = new CommentSettings(
                context.optStr('id', { empty: true }),
                context.optStr('parentId', { empty: true }),
                author,
                userId,
                providerId,
                context.optStr('date'),
                context.optStr('text', { empty: true }),
                context.optArr('mentions', null, true),
                context.optBool('restorable', true),
                null, // the selection state only needs to be saved for new threads
                context.optStr('target', { empty: true }),
                false, // this is not a new thread generated without operation
                context.optBool('done'),
                commentPlaceHolder
            );

            // the new comment model for the new comment
            var commentModel = new CommentModel(docModel, settings);
            // the position of the new comment model in the sorted collector
            var index = -1;

            // the position in the array is dependent from the position of the placeholder node in the DOM
            // -> how to handle header/footer?
            if (docModel.isImportFinished() || getBooleanOption(options, 'findPosition', false)) {
                // finding the specified placeholder in the list of all placeholders
                index = allCommentPlaceHolders.index(commentPlaceHolder);
            }

            registerComment(commentModel, index);

            // in ODF the order is more difficult, because the main comment is at the beginning of
            // the range, and the children at the end of the range. This makes it possible, that
            // other comments are between parent and children. But this is not allowed in the
            // model container.
            if (docApp.isODF()) { self.sortCommentsInModel(); }

            updateAuthorList(commentModel);

            if (allChildren) { makeParentsToChildren(commentModel.getId(), allChildren); }

            // the following is only required after the document is loaded completely
            if (docModel.isImportFinished()) {
                if (context.optBool('newcomment')) {
                    if (commentModel.isReply()) {
                        triggerEvent('new:threadedcomment:comment', commentModel);
                    } else {
                        triggerEvent('new:threadedcomment:thread', commentModel);
                    }
                    delete context.operation.newcomment;
                } else {
                    triggerEvent('insert:threadedcomment', commentModel);

                    // an undo of a parent comment (DOCS-2674) -> forcing a full update of comments pane
                    if (!commentModel.isReply() && self.hasChild(commentModel)) {
                        triggerEvent('after:move:threadedcomments');
                    }
                }
            }

            return commentModel;
        };

        /**
         * Deleting a comment from the sorted collection. This is done in OX Text
         * with a default 'delete' operation.
         *
         * Removing one comment element from the comment layer node. The parameter
         * is the place holder node in the page content.
         *
         * @param {String} commentId
         *  The id of the comment that will be removed.
         *
         * @returns {CommentModel|null}
         *  The removed comment model or null, if no model was removed.
         */
        this.handleDeleteComment = function (commentId) {
            if (commentId === selectedCommentId) { setSelectedCommentId(null); }
            if (unsavedComment && unsavedComment.model && unsavedComment.model.getId() === commentId) { self.deleteUnsavedComment(); } // DOCS-2667
            var deletedCommentModel = unregisterComment(commentId);
            updateAuthorList(deletedCommentModel, { register: false });
            if (deletedCommentModel && !self.isTemporaryCommentId(commentId)) { triggerEvent('delete:threadedcomment:end', [deletedCommentModel]); }

            // an undo of a parent comment (DOCS-2674) -> forcing a full update of comments pane, so that children are displayed as parents
            if (deletedCommentModel && !deletedCommentModel.isReply() && self.hasChild(deletedCommentModel)) {
                makeChildrenToParents(commentId); // make the children to parents
                docModel.trigger('after:move:threadedcomments'); // update the complete comment pane
            }

            return deletedCommentModel;
        };

        /**
         * Generates the operations, and the undo operations, to delete all
         * cell comment threads (optionally located in specific ranges) from
         * this collection.
         *
         * @param {SheetOperationGenerator} generator
         *  The operations generator to be filled with the operations.
         *
         * @param {RangeSource} [ranges]
         *  The cell ranges containing the cell comments to be deleted. If
         *  omitted, all existing cell comments will be deleted.
         *
         * @returns {JPromise<void>}
         *  A promise that will fulfil when the operations have been generated
         *  successfully, or that will reject on any error.
         */
        this.generateDeleteAllCommentsOperations = function (generator, ranges) {
            // bug 64896: delete in reversed order (uses an array iterator internally)
            var iterator = this.yieldCommentModels({ ranges, reverse: true });
            return this.iterateSliced(iterator, commentModel => {
                this.generateDeleteCommentOperations(generator, commentModel);
            });
        };

        /**
         * Selecting the temporary comment, if it exists.
         */
        this.selectTemporaryComment = function () {
            var model = self.getTemporaryModel();
            if (model) { self.selectComment(model); }
        };

        /**
         * Selecting a specified comment. This means setting a fixed border and
         * expanding the collapsed reply comments.
         *
         * This is handled by adding classes 'selected' and 'show-all'.
         *
         * @param {String|CommentModel} comment
         *  The first commentModel in a threaded comment frame or the comment ID.
         */
        this.selectComment = function (comment) {
            if (comment) {
                if (_.isString(comment)) { comment = self.getById(comment); }
                if (!comment) { return; } // DOCS-2987
                docModel.trigger('select:threadedcomment', comment, removeSelectionHandler);
                // saving the last selected comment id
                setSelectedCommentId(comment.getId());
            }
        };

        /**
         * Deselecting a comment.
         */
        this.deselectComment = function () {
            docModel.trigger('select:threadedcomment');
            setSelectedCommentId(null);
        };

        /**
         * Starting a new comment thread, for example using the 'comment' button. In this
         * scenario, no new operations are generated. But in this scenario it is important
         * to save the selection state and update it with every local and external operation,
         * so that the comment can be shown correctly in the comments pane and that it can
         * be inserted correctly, if the user presses the 'Send' button.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the user name.
         */
        this.startNewThread = function () {

            if (docModel.getCommentLayer().isCommentEditorActive()) {
                // select the comment with the active editor instead of starting another unsaved comment (DOCS-4273)
                docApp.getView().selectEditComment();
                return $.when();
            }

            savedSelectionState = docModel.getSelectionState(); // saving the current selection, so that it can be used to generate operations later

            return getUserName().then(function (userName) {

                var settings = new CommentSettings(self.getTemporaryCommentId(), '', userName, String(calculateUserId(ox.user_id)), OX_PROVIDER_ID, '', '', null, true, savedSelectionState, '', true, false, null);
                var commentModel = new CommentModel(docModel, settings);
                var commentLayer = docModel.getCommentLayer();
                var options = { model: commentModel };

                // deselecting an existing selected thread
                if (selectedCommentId) { self.deselectComment(); }

                triggerEvent('start:threadedcomment', options);

                // inserting the new temporary commment (with ID and start and end position) into the sorted comment model. Then it can be correctly
                // vertically adjusted in the comment pane. The range marker nodes need to be inserted into the DOM without operation every time, when
                // the vertical update is triggered or when the comment selection shall be made visible.

                // saving a reference to the temporary model for faster access
                temporaryModel = commentModel;

                // Adding into temporary comment model into the collection.
                // -> the position in the array is dependent from the position of the placeholder node in the DOM
                // -> how to handle header/footer?
                // finding the temporary inserted placeholder in the list of all placeholders
                var index = commentLayer.getIndexForNewCommentThread(commentModel);

                registerComment(commentModel, index);

                // preparing the first thread in the bubble mode correctly (TODO: There might be an easier solution)
                if (commentLayer.isBubbleModeActivationRequired()) { triggerEvent('new:threadedcomment:thread', commentModel); }

                // check, if a new bubble node must be generated
                commentLayer.createAndRegisterNewBubbleNode(commentModel);

                // forcing vertical update of comments (and inserting range marker nodes temporarely)
                triggerEvent('comments:update:vertical');

                // informing the comment layer about the new thread
                commentLayer.prepareNewThread(commentModel);

                // setting the selection into the new comment
                self.selectComment(temporaryModel);
            });
        };

        /**
         * Removing the temporary generated comment model from the collector of comment models.
         * It was necessary to generate the temporary comment model in the sorted collector,
         * so that the new inserted comment thread (inserted without operation) could be
         * shown at the best position in the comments side pane.
         */
        this.removeTemporaryCommentModel = function () {
            self.handleDeleteComment(self.getTemporaryCommentId());
            if (temporaryModel) { temporaryModel.destroy(); }
            temporaryModel = null; // removing the reference to the temporary model in the collector
        };

        /**
         * Generating the operations for inserting comments. This function is called if a new
         * thread is generated or if a comment reply is generated.
         *
         * This function is called directly from the class 'ThreadedCommentEditor' in
         * the 'editframework'.
         *
         * @param {String|undefined} parentId
         *  The parent ID for the new comment. If this is a new comment thread, this value is undefined.
         *
         * @param {Object} commentInfo
         *  Additional data for the new comment, like 'mentions' and 'text'.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the new commentId.
         */
        this.insertThread = function (parentId, commentInfo) {

            var promise = getUserName();

            if (!parentId) { parentId = null; } // this happens for new threads

            // generate and apply the operations (in the commentLayer class)
            promise = promise.then(function (userName) {
                return docModel.getCommentLayer().insertComment(parentId, userName, commentInfo);
            });

            return promise;
        };

        /**
         * Changing a comment.
         *
         * This function is called directly from the class 'ThreadedCommentEditor' in
         * the 'editframework'.
         *
         * @param {CommentModel} commentModel
         *  The model of the comment that shall be changed.
         *
         * @param {Object} commentInfo
         *  Additional data for the changed comment, like 'mentions' and 'text'.
         */
        this.changeThread = function (commentModel, commentInfo) {
            // handling this change in the commentLayer class.
            docModel.getCommentLayer().changeComment(commentModel, commentInfo);
        };

        /**
         * Unassigning the unsaved comment in the document.
         *
         * @param {Opt<Object>} comment
         *  The object with the valid unsaved comment data. Only with this
         *  valid data, the existing object can be set to null.
         */
        this.setUnsavedComment = function (comment) {
            unsavedComment = comment;
        };

        /**
         * Receiving the unsaved comment data in the document. This
         * is important when the document is closed.
         * Unsaved comment data are possible with a new comment thread,
         * a reply or a change of comment content.
         *
         * @returns {Object}
         *  The object with the unsaved comment.
         */
        this.getUnsavedComment = function () {
            return unsavedComment;
        };

        /**
         * Deleting the unsaved comment in the document.
         */
        this.deleteUnsavedComment = function () {
            unsavedComment = null;
        };

        /**
         * Whether there are unsaved comment data in the document. This
         * is important when the document is closed.
         * Unsaved comment data are possible with a new comment thread,
         * a reply or a change of comment content.
         *
         * @returns {Boolean}
         *  Whether there are unsaved data in comment.
         */
        this.hasUnsavedComment = function () {
            return !!unsavedComment;
        };

        /**
         * Returns whether this collection contains any comments.
         *
         * @returns {Boolean}
         *  Whether this collection contains any comments.
         */
        this.hasComments = function () {
            return sortedComments.length > 0;
        };

        /**
         * TODO
         * This is an important API function. Please do not remove this.
         */
        this.getSelectedThread = function () {
            return null;
        };

        /**
         * Getting the ID of the comment that was selected before. It is not
         * sure that this comment is still selected (or even exists).
         * -> update: This should be sure, because selectedCommentId is updated
         *            if a comment is deselected and if a comment is removed.
         *
         * @returns {String|Null}
         *  The ID of the last selected comment. Or null, if no comment has
         *  been selected before.
         */
        this.getSelectedCommentId = function () {
            return selectedCommentId;
        };

        /**
         * Checking, if the specified comment id belongs to the selected comment.
         *
         * @param {String} id
         *  The ID that will be checked.
         *
         * @returns {Boolean}
         *  Whether the specified id is the id of the selected comment.
         */
        this.isSelectedCommentId = function (id) {
            return selectedCommentId === id;
        };

        /**
         * Getting the id of the selected thread
         *
         * @returns {String|Null}
         *  The ID of the selected thread. Or null, if no comment is selected.
         */
        this.getSelectedThreadId = function () {
            if (!selectedCommentId) { return null; }
            return self.getThreadIdForCommentId(selectedCommentId);
        };

        /**
         * Checking, if the comment thread for a specified comment id is selected.
         * This is the case, if any of the comments in the thread is selected.
         *
         * @param {String} id
         *  The ID that will be checked.
         *
         * @returns {Boolean}
         *  Whether the specified id is the id of a comment inside a selected thread.
         */
        this.isSelectedThreadId = function (id) {
            if (!selectedCommentId) { return false; } // no comment is selected
            return self.getThreadIdForCommentId(id) === self.getSelectedThreadId();
        };

        /**
         * Getting the id of the comment thread (the parent id) for a specified
         * comment id. The the specified id is already the id of a parent comment,
         * the id itself is returned.
         *
         * @param {String} id
         *  The id for that the thread id (the parend id) shall be returned.
         *
         * @returns {String}
         *  The parent id of the specified id. Or the specified id itself, if it
         *  is a parent comment. An empty string is returned, if no commentModel
         *  can be found belonging to the specified id.
         */
        this.getThreadIdForCommentId = function (id) {
            // checking, if the comment has parent
            var commentModel = self.getById(id);
            if (!commentModel) { return ''; }
            // the parent id of the selected comment
            var parentId = commentModel.getParentId();
            // the id of the specified thread
            return parentId ? parentId : id;
        };

        /**
         * Getting the list of all authors that have written comments in
         * the current document.
         *
         * @returns {String[]}
         *  A list of all authors that have written comments in the current
         *  document.
         */
        this.getCommentsAuthorList = function () {
            return commentsAuthorList;
        };

        /**
         * Returning the number of authors of comments in the document.
         *
         * @returns {Number}
         *  The number of authors of comments in the document.
         */
        this.getCommentAuthorCount = function () {
            return commentsAuthorList.length;
        };

        /**
         * Applying an author filter to the comment models. If no filter is
         * specified, all comments are visible.
         *
         * @param {String[]|undefined} authorFilter
         *  A list of those authors, that are not filtered. If not specified,
         *  no comment author is filtered.
         */
        this.applyAuthorFilter = function (authorFilter) {
            self.iterateCommentModels(function (commentModel) {
                var doHideComment = !!(authorFilter && !_.contains(authorFilter, commentModel.getAuthor()));
                commentModel.setHiddenState(doHideComment);
            }, self);
        };

        /**
         * Returns whether a comment is contained in a hidden parent.
         *
         * @param {commentModel} commentModel
         *  The specified comment model.
         *
         * @returns {boolean}
         *  Whether the comment is contained in a hidden parent.
         */
        this.hasHiddenParent = function (commentModel) {
            if (!commentModel.isReply()) { return false; }
            var parentModel = self.getById(commentModel.getParentId());
            return parentModel.isHidden();
        };

        /**
         * Whether the specified comment model is a parent comment, that
         * has at least one reply.
         *
         * @param {commentModel} commentModel
         *  The specified comment model.
         *
         * @returns {Boolean}
         *  Whether the specified comment model is a parent comment, that
         *  has at least one reply.
         */
        this.hasReply = function (commentModel) {
            if (!commentModel) { return false; }
            var commentId = commentModel.getId();
            return !commentModel.isReply() && !!_.find(sortedComments, function (model) { return model.getParentId() === commentId; });
        };

        /**
         * Whether the specified comment model is a comment, that
         * has at least one child.
         *
         * @param {commentModel} commentModel
         *  The specified comment model.
         *
         * @returns {Boolean}
         *  Whether the specified comment model is a comment, that
         *  has at least one child.
         */
        this.hasChild = function (commentModel) {
            if (!commentModel) { return false; }
            var parentId = commentModel.getId();
            return !!_.find(sortedComments, function (model) { return model.getParentId() === parentId; });
        };

        /**
         * Whether the specified comment model is a parent comment, that
         * has at least one hidden child.
         *
         * @param {commentModel} commentModel
         *  The specified comment model.
         *
         * @returns {Boolean}
         *  Whether the specified comment model is a parent comment, that
         *  has at least one hidden child.
         */
        this.hasHiddenChild = function (commentModel) {
            if (!commentModel) { return false; }
            var parentId = commentModel.getId();
            return !commentModel.isReply() && !!_.find(sortedComments, function (model) { return model.isHidden() && model.getParentId() === parentId; });
        };

        /**
         * Whether the specified comment model is a parent comment, that
         * has at least one visible child.
         *
         * @param {commentModel} commentModel
         *  The specified comment model.
         *
         * @returns {Boolean}
         *  Whether the specified comment model is a parent comment, that
         *  has at least one visible child.
         */
        this.hasVisibleChild = function (commentModel) {
            if (!commentModel) { return false; }
            var parentId = commentModel.getId();
            return !commentModel.isReply() && !!_.find(sortedComments, function (model) { return !model.isHidden() && model.getParentId() === parentId; });
        };

        /**
         * Whether the comment specified by its commentModel is visible.
         *
         * @param {commentModel} commentModel
         *  The specified comment model.
         *
         * @returns {Boolean}
         *  Whether the specified comment model describes a visible comment.
         */
        this.isCommentVisible = function (commentModel) {
            return !commentModel.isHidden();
        };

        /**
         * Whether there is a selected comment and this selected comment is hidden.
         *
         * @returns {Boolean}
         *  If a comment is selected and this selected comment is hidden (for example
         *  after applying an author filter), this function returns true. Otherwise
         *  false.
         */
        this.isSelectedCommentHidden = function () {
            var selectedId = self.getSelectedCommentId();
            return selectedId ? self.getById(selectedId).isHidden() : false;
        };

        /**
         * Whether there is at least one marginal comment in the collection (only ODT).
         *
         * @returns {Boolean}
         *  Whether there is at least one marginal comment in the collection (only ODT).
         */
        this.containsMarginalComments = function () {
            return !!_.find(sortedComments, function (model) { return model.isMarginal(); });
        };

        /**
         * Whether there is a temporary model, generated without operations by a new thread,
         * in the sorted comment collector.
         *
         * @returns {Boolean}
         *  Whether the comment collector contains the temporary comment model.
         */
        this.containsTemporaryModel = function () {
            return !!temporaryModel;
        };

        /**
         * Whether there is only a temporary model, generated without operations by a new thread,
         * in the sorted comment collector.
         *
         * @returns {Boolean}
         *  Whether the comment collector contains only the temporary comment model.
         */
        this.containsOnlyTemporaryModel = function () {
            return sortedComments.length === 1 && self.isTemporaryCommentId(sortedComments[0].getId());
        };

        /**
         * Getting the reference to the temporary comment model, generated without operations
         * by a new thread, in the sorted comment collector.
         *
         * @returns {CommentModel|Null}
         *  The reference to the temporary comment model in the sorted comment collector.
         *  If it does not exist, null is returned.
         */
        this.getTemporaryModel = function () {
            return temporaryModel;
        };

        /**
         * Creating a clone of the array of sorted comments and returning the clone.
         *
         * @returns {CommentModel[]}
         *  The clone array of all comment models.
         */
        this.clone = function () {

            var newSortedCommentsArray = [];

            sortedComments.forEach(function (commentModel) {
                var newCommentModel = commentModel.clone(docModel);
                newSortedCommentsArray.push(newCommentModel);
            });

            return newSortedCommentsArray;
        };

        /**
         * Check, that every loaded child comment has a parent. If this is not the case, the
         * child becomes a parent. This increases the document resilience (DOCS-3259).
         */
        this.checkParentValidity = function () {

            _.each(sortedComments, function (model) {
                if (model.isReply()) {
                    var parent = self.getById(model.getParentId());
                    if (!parent) { model.setParentId(null); } // making child to parent
                }
            });
        };

        /**
         * Sorting the comments in the comment model. This is especially important for ODT, because
         * the main comment is positioned at the range start and the children are positioned at the
         * range end. Therefore it can easily happen, that other comment are between the main comment
         * and its children. But in this case the side pane would have problems.
         *
         * This sorting is also important after loading the document with fast load. In this case it
         * might happen, that a child comment is inserted into the model before its parent (DOCS-2648).
         */
        this.sortCommentsInModel = function () {

            // the new collector for the sorted comments
            var newSortedComments = [];
            // the child comments container for the thread
            var children = null;

            _.each(sortedComments, function (model) {
                if (!model.isReply()) {
                    newSortedComments.push(model);
                    children = self.getChildrenById(model.getId());
                    if (children && children.length) { newSortedComments = newSortedComments.concat(children); }
                }
            });

            if (sortedComments.length !== newSortedComments.length) { globalLogger.error('CommentCollection.sortCommentsInModel(): Wrong number of comments in the collection after sorting!'); }

            // better: Check, if the order has changed before assigning array
            sortedComments = newSortedComments;
        };

        /**
         * During the update of the vertical comment positions, it is checked if the order is still
         * valid. Typically this is always fine, because the 'sortedComments' always contain a valid
         * order that is generated by using the placeHolderCollection from the commentLayer
         * ('commentPlaceHolders'). But this is not reliable, if comments are located in header/footer
         * or in absolute positioned drawings.
         *
         * Therefore an additional process in 'updateVerticalPositionInCommentsPane' measures the
         * height of all comments relative to the page. If there is a wrong comment order, this
         * function will be called with a collector of all comment IDs and top positions. Therefore
         * this function can resort the collector of all comments and render a refresh of the comments
         * side pane.
         *
         * A good example are comments in footers. If a page break is inserted into a page, that shifts
         * a comment from page 1 to page 2, it must be shifted below the comment in the footer. And if
         * the page break is removed again, the page comment must be shifted above the footer comment
         * again.
         *
         * @param {Object[]} collector
         *  A collector that contains for all main comments an object with two properties:
         *   - id : the id of the main comment
         *     top: the top position (in pixel) of that comment relative to the page
         */
        this.resortCommentsInCollection = function (collector) {

            // the sorted top positions (relative to the page) of all main comments
            var newOrder = _.sortBy(collector, 'top');
            // the new collector for the sorted comments
            var newSortedComments = [];
            // the child comments container for the thread
            var children = null;

            _.each(newOrder, function (orderInfo) {
                var model = self.getById(orderInfo.id);
                if (!model.isReply()) {
                    newSortedComments.push(model);
                    children = self.getChildrenById(model.getId());
                    if (children && children.length) { newSortedComments = newSortedComments.concat(children); }
                }
            });

            if (sortedComments.length !== newSortedComments.length) { globalLogger.error('CommentCollection.resortCommentsInCollection(): Wrong number of comments in the collection after resorting!'); }

            sortedComments = newSortedComments;
        };

        /**
         * Applying a complete collection of comment models. This is for example required,
         * when a snapshot is applied.
         *
         * @param {CommentModel[]} newCommentCollection
         *  The collection of comment models that will replace the old collection.
         */
        this.applyCommentCollection = function (newCommentCollection) {
            sortedComments = newCommentCollection;
        };

        /**
         * Calls the passed iterator function for the sorted comment models of
         * this collection.
         *
         * @param {Function} iterator
         *  The iterator function that will be called for every comment model.
         *  It receives the following parameters:
         *      (1) {CommentModel} the comment model itself.
         *
         * @param {Object} [context]
         *  If specified, the iterator will be called with this context (the
         *  symbol 'this' will be bound to the context inside the iterator
         *  function).
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.onlyThreads=false]
         *      If set to true, only the first comments of a thread are iterated.
         *      Default is false, so that over all comment models is iterated.
         */
        this.iterateCommentModels = function (iterator, context, options) {
            // whether only threads comment shall be iterated or also replys.
            var onlyThreads = getBooleanOption(options, 'onlyThreads', false);
            sortedComments.forEach(function (commentModel) {
                if (commentModel.isReply() && onlyThreads) { return; }
                iterator.call(context, commentModel);
            });
        };

        // destroy all class members
        this.registerDestructor(function () {
            sortedComments.forEach(commentModel => commentModel.destroy());
        });
    }
}
