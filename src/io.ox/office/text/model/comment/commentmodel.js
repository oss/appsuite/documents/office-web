/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import { resolveUserId, OX_PROVIDER_ID } from '@/io.ox/office/editframework/utils/operationutils';
import { PAGE_CHILDREN_CONTENT_SELECTOR } from '@/io.ox/office/textframework/utils/dom';

/**
 * All additional settings of a comment in OX Text.
 */
export class CommentSettings {

    constructor(id, parentId, author, authorId, authorProvider, date, text, mentions, restorable, selectionState, target, isNewThread, done, commentPlaceHolder) {
        this.id = id;
        this.parentId = parentId || '';
        this.author = author || '';
        this.authorId = authorId || '';
        this.authorProvider = authorProvider;
        this.date = date || '';
        this.text = text || '';
        this.mentions = mentions;
        this.restorable = restorable;
        this.selectionState = selectionState;
        this.target = target || '';
        this.isNewThread = isNewThread;
        this.done = done || null;
        this.commentPlaceHolder = $(commentPlaceHolder);
    }

    /**
     * Returns a JSON object representation of the object.
     *
     * @param {boolean} addIndex if true add the index and the parentIndex to the JSON
     *
     * @returns {Object} the settings in a JSONObject
     */
    toJSON(addIndex) {

        var json = {
            text: this.text,
            id: this.id,
            parentId: this.parentId,
            mentions: this.mentions,
            selectionState: this.selectionState,
            target: this.target,
            isNewThread: this.isNewThread,
            restorable: this.restorable,
            date: this.date,
            author: this.author,
            authorId: this.authorId,
            authorProvider: this.authorProvider
        };

        if (!this.parent && this.done) {
            json.done = this.done;
        }

        if (addIndex) {
            json.index = this.index;

            if (this.parent) {
                json.parent = this.parent;
            }
        }

        return json;
    }

    clone() {
        var settingsClone = new CommentSettings(this.id, this.parentId, this.author, this.authorId, this.authorProvider, this.date, this.text, this.mentions, this.restorable, this.selectionState, this.target, this.isNewThread, this.done);
        settingsClone.commentPlaceHolder = this.commentPlaceHolder;
        return settingsClone;
    }
}

export class CommentModel extends ModelObject {

    constructor(docModel, initSettings) {

        // base constructor
        super(docModel);

        // properties ---------------------------------------------------------

        // the self reference
        var self = this;
        // making a local clone of the specified settings
        var settings = initSettings.clone();
        // parsing the specified comment date
        var parsedDate = Date.parse(settings.date);
        // the comment date as number
        var longDate = _.isFinite(parsedDate) ? parsedDate : 0;
        // whether this comment is visible, taking care of an optional author filter
        var hiddenState = false;
        // preparing the authorId with '::' syntax, that is used, if the author is also used in a mention
        var userIdArray = settings.authorId.split('::');
        // the user ID, that can only be evaluated, if the providerId is the own OX_PROVIDER_ID
        var userId = userIdArray.length > 1 ? userIdArray[1] : settings.authorId;
        // the authors email, that can be evaluated from the authors list in setDocumentAttributes operation
        var authorEmail = userIdArray.length > 1 ? userIdArray[0] : null;

        // public methods -----------------------------------------------------

        this.getId = function () {
            return settings.id;
        };

        this.getParentId = function () {
            return settings.parentId;
        };

        /**
         * In ODF the parent ID is not sent from the filter. Therefore a process after loading the
         * document analyzes the DOM to detect the comment structure and assigns parentIDs.
         *
         * @param {String|null} id
         *  The parentID for this comment. If null is sent, the parent is removed (DOCS-2674).
         */
        this.setParentId = function (id) {
            settings.parentId = id;
        };

        // TODO Is this required?
        this.getParent = function () {
            return settings.parent;
        };

        // TODO Is this required?
        this.setParent = function (parent) {
            settings.parent = parent;
        };

        this.getText = function () {
            return settings.text;
        };

        this.setText = function (text) {
            settings.text = text;
        };

        this.getMentions = function () {
            return settings.mentions;
        };

        this.setMentions = function (mentions) {
            settings.mentions = mentions;
        };

        this.getDate = function () {
            return settings.date;
        };

        this.getDateAsNumber = function () {
            return longDate;
        };

        this.getAuthor = function () {
            return settings.author;
        };

        this.getAuthorProvider = function () {
            return settings.authorProvider;
        };

        /**
         * The user ID (!) for the author, as it is used in operations.
         * This is NOT the property 'authorId' that is used in operations to reference
         * an author that is specified in the authors list in the setDocumentAttributes
         * operation.
         */
        this.getAuthorId = function () {
            return userId;
        };

        /**
         * Getter for the authors email address. This is only set, if the user ID
         * is sent from the filter in the format 'email::userid::name'
         */
        this.getAuthorEmail = function () {
            return authorEmail;
        };

        /**
         * Calculating the app suite user id from the authorId. This value is used in the
         * class 'ThreadedCommentElement' to show the author and picture informations.
         *
         * @returns {Number}
         *  The internal user id of the author. If it cannot be determined, '-1' is returned.
         */
        this.getInternalUserId = function () {
            var defaultId = -1;
            if (settings.authorProvider !== OX_PROVIDER_ID) { return defaultId; }
            var numberValue = parseInt(userId, 10);
            return _.isFinite(numberValue) ? resolveUserId(numberValue) : defaultId;
        };

        this.isDone = function () {
            return settings.done ? settings.done : false;
        };

        this.equals = function (commentModel) {
            return commentModel ? commentModel.getId() === settings.id : false;
        };

        this.isReply = function () {
            return !!settings.parentId;
        };

        this.isRestorable = function () {
            return settings.restorable;
        };

        this.setRestorable = function (value) {
            settings.restorable = value;
        };

        this.isHidden = function () {
            return hiddenState;
        };

        this.setHiddenState = function (value) {
            hiddenState = value;
        };

        /**
         * This is a marker for comment models, that are generated with new threads and
         * that are not yet sent to the filter. It is important to handle this models in
         * the (sorted) comment collection, so that is can be shown correctly in the
         * comments pane.
         */
        this.isNewThread = function () {
            return settings.isNewThread;
        };

        /**
         * The selection state must be saved and updated in comment models, that are generated
         * with new threads and that are not yet sent to the filter. It is important to handle
         * this models in the (sorted) comment collection, so that is can be shown correctly in
         * the comments pane. The selection state must be updated with every local and external
         * operation in the document, because there are no rangemarker in the main document,
         * that belong to this comment.
         */
        this.getSelectionState = function () {
            return settings.selectionState;
        };

        /**
         * Returning the comment place holder node for this comment.
         *
         * @returns {jQuery|Null}
         *  The jQueryfied place holder node belonging to this comment. If it cannot be determined
         *  (what is a failure), null is returned.
         */
        this.getCommentPlaceHolderNode = function () {

            if (!self.isMarginal()) { return settings.commentPlaceHolder; }

            // check, if the place holder is still inside the first header or the last footer or the page content node
            if (settings.commentPlaceHolder.closest(PAGE_CHILDREN_CONTENT_SELECTOR).length > 0) { return settings.commentPlaceHolder; }

            // the marginal root node of the comment place holder node is no longer in the DOM -> find the new placeholder in the DOM
            var marginalRoot = docModel.getRootNode(self.getTarget());

            if (marginalRoot && marginalRoot.length > 0) {

                var selectorString = '.commentplaceholder[data-container-id=' + self.getId() + ']';
                var foundNode = marginalRoot.find(selectorString);

                if (foundNode && foundNode.length > 0) {
                    self.setCommentPlaceHolderNode(foundNode); // updating the place holder node
                    return settings.commentPlaceHolder;
                } else {
                    globalLogger.warn('Warning: Failed to find marginal comment place holder node with ID: ' + self.getId() + ' in the target: ' + self.getTarget());
                }
            } else {
                globalLogger.warn('Warning: Failed to find marginal node with target: ' + self.getTarget());
            }

            return null;
        };

        this.setCommentPlaceHolderNode = function (placeHolderNode) {
            settings.commentPlaceHolder = $(placeHolderNode);
        };

        this.clone = function (targetModel) {
            return new CommentModel(targetModel, settings.clone());
        };

        // TODO: This must be set, if the parentId does not exist or the comment with
        //       the specified parentId also has a parentId.
        this.hasInvalidParent = function () {
            return false;
        };

        this.getJSONSettings = function () {

            var json = {};
            if (settings.id) { json.id = settings.id; }
            if (settings.parentId) { json.parentId = settings.parentId; }
            if (settings.text) { json.text = settings.text; }
            if (settings.mentions) { json.mentions = settings.mentions; }
            if (settings.author) { json.author = settings.author; }
            if (settings.authorId) { json.authorId = settings.authorId; }
            if (settings.authorProvider) { json.authorProvider = settings.authorProvider; }
            if (settings.date) { json.date = settings.date; }

            if (!this.isReply() && settings.done) {
                json.done = settings.done;
            }

            // var attrs = this.getMergedAttributeSet(true);
            // if (attrs.drawing) {
            //     json.attrs = { drawing: attrs.drawing };
            // }

            return json;
        };

        /**
         * It's only needed to create the fallback Notes drawing attributes.
         */
        this.forEachChildModel = function () {
        };

        /**
         * It's only needed to create the fallback Notes drawing attributes.
         */
        this.getType = function () {
            return 'note';
        };

        /**
         * It's only needed to create the fallback Notes drawing attributes.
         */
        this.getParentModel = function () {
            return null;
        };

        /**
         * Return the zIndex for the drawing frame.
         * It's only needed to create the fallback Notes drawing attributes.
         */
        this.getIndex = function () {
            return 1;
        };

        /**
         * Whether this comment is located inside a header or footer.
         *
         * @returns {Boolean}
         *  Whether this comment is located inside a header or footer.
         */
        this.isMarginal = function () {
            return !!settings.target;
        };

        /**
         * Getting the target of this comment. The value is only NOT the empty
         * string, if the comment is located inside a header or footer.
         *
         * @returns {String}
         *  The target of the header/footer in which this comment is located. Or
         *  an empty string, if the comment is inside the main document.
         */
        this.getTarget = function () {
            return settings.target;
        };

        /**
         * TODO
         * The usage of 'anchor' is not clear in OX Text and OX Presentation. In 'ThreadedCommentFrame.js' the child comments are
         * collected with
         * var commentModels = documentCollection.getByAddress(threadComment.getAnchor());
         * where the threadComment is the main comment.
         * In OX Text this can be handle simply by using the comment 'id' and the 'parentId'.
         */
        this.getAnchor = function () {
            return settings.parentId ? settings.parentId : settings.id;
        };

        // TODO
        this.getAnchorString = function () { // satisfy the APIs of both `ThreadedCommentCollection` and `CommentsPane`
            return settings.parentId; // empty string for all non-replies, not used in OX Text
        };

        // this.getAnchorRectangle = function () {
        //     return sheetModel.getAnchorRectangle(settings.anchor);
        // };
    }
}
