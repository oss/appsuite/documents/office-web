/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import CharacterStyles from '@/io.ox/office/textframework/format/characterstyles';
import TableStyles from '@/io.ox/office/textframework/format/tablestyles';
import TableRowStyles from '@/io.ox/office/textframework/format/tablerowstyles';

import ParagraphStyles from '@/io.ox/office/text/format/paragraphstyles';
import DrawingStyles from '@/io.ox/office/text/format/drawingstyles';
import TableCellStyles from '@/io.ox/office/text/format/tablecellstyles';
import PageStyles from '@/io.ox/office/text/format/pagestyles';

// definitions for global document attributes -----------------------------

const DOCUMENT_ATTRIBUTES = {
    defaultTabStop:     { def: 1270 }, // 1/2 inch
    zoom:               { def: { value: 100 } }, // 100%
    changeTracking:     { def: false },
    usedLanguages:      { def: [] },
    usedLanguagesEa:    { def: [] },
    usedLanguagesBidi:  { def: [] }
};

// definitions for change tracking attributes
const CHANGES_ATTRIBUTES = {
    inserted: { def: '', scope: 'element' },
    removed:  { def: '', scope: 'element' },
    modified: { def: '', scope: 'element' }
};

// mix-in class ModelAttributesMixin ======================================

/**
 * A mix-in class for the document model class TextModel providing the
 * style sheet containers for all attribute families used in a text
 * document.
 */
export default function ModelAttributesMixin() {

    // formatting attributes registration
    this.defAttrPool.registerAttrFamily("document", DOCUMENT_ATTRIBUTES);
    this.defAttrPool.registerAttrFamily("changes",  CHANGES_ATTRIBUTES);

    // style sheet collections
    this.characterStyles = this.addStyleCollection(new CharacterStyles(this, { families: ['changes'] }));
    this.paragraphStyles = this.addStyleCollection(new ParagraphStyles(this));
    this.tableStyles = this.addStyleCollection(new TableStyles(this));
    this.tableRowStyles = this.addStyleCollection(new TableRowStyles(this));
    this.tableCellStyles = this.addStyleCollection(new TableCellStyles(this));
    this.drawingStyles = this.addStyleCollection(new DrawingStyles(this));
    this.pageStyles = this.addStyleCollection(new PageStyles(this));
}
