/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import _ from '$/underscore';
import $ from '$/jquery';

import { equalPositions } from '@/io.ox/office/editframework/utils/operations';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';

import { findPreviousNode, getBooleanOption, getDomNode } from '@/io.ox/office/textframework/utils/textutils';
import { DRAWING_NODE_SELECTOR, LIST_BORDER_NODE_SELECTOR, LIST_PARAGRAPH_ATTRIBUTE, LIST_PARAGRAPH_MARKER,
    PARAGRAPH_NODE_SELECTOR, getNodeSelection, isEmptyListParagraph, isFirstParagraphInList,
    isImplicitParagraphNode, isMarginalNode, isPageNode, isParagraphNode } from '@/io.ox/office/textframework/utils/dom';
import { appendNewIndex, getLastNodeFromPositionByNodeName, getLeadingTabsInParagraph, getOxoPosition,
    getParagraphElement, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import { DELETE, INSERT_STYLESHEET, PARA_INSERT, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';

// mix-in class ListHandlerMixin ======================================

/**
 * A mix-in class for the document model class providing functionality
 * for the list handling used in a text document.
 */
export default function ListHandlerMixin() {

    var // self reference for local functions
        self = this;

    // private methods ----------------------------------------------------

    /**
     * Private helper function that generates 'setAttributes' operations with
     * assigned listStyleId and listLevel. Setting styleId to list-paragraph
     * only if current styleId is standard.
     *
     * @param {String} listStyleId
     *  The id of the list style.
     *
     * @param {Number} listLevel
     *  The list level.
     */
    function setListStyle(listStyleId, listLevel) {

        var attrs = self.getAttributes('paragraph'),
            newAttrs = { paragraph: { listStyleId, listLevel } };

        //TODO: workaround for broken paint update
        if (_.isUndefined(listLevel) && attrs.paragraph) {
            newAttrs.paragraph.listLevel = attrs.paragraph.listLevel;
        }

        // assigning list style id to paragraph, if it has the default style id (40787)
        if (attrs.styleId === self.getDefaultUIParagraphStylesheet()) {
            // checking, if the paragraph has a style id set
            newAttrs.styleId = self.getDefaultUIParagraphListStylesheet();
        }

        self.setAttributes('paragraph', newAttrs);

        // Bug 57688: Add Operation for last empty paragraph in selection
        var endPosition = self.getSelection().getEndPosition();
        if (self.getSelection().hasRange() && _.last(endPosition) === 0) {
            endPosition.pop();
            var paraNode = getParagraphElement(self.getCurrentRootNode(), endPosition);
            if (isEmptyListParagraph(paraNode)) {
                self.applyOperations({ name: SET_ATTRIBUTES, start:  _.copy(endPosition), attrs: newAttrs });
            }
        }
    }

    /**
     * Helper function to find a previous paragraph in the document using a numbering list style.
     *
     * @param {Node|jQuery} rootNode
     *  The DOM root node for the specific paragraph node.
     *
     * @param {HTMLElement|jQuery} oneParagraph
     *  The paragraph node, for that a previous paragraph with a list style ID is searched.
     *
     * @param {String} paragraphStyle
     *  The style ID of the paragraph, for that a previous paragraph with a list style ID is searched.
     *  Info: Also checking equal paragraph style, so that for example heading paragraphs do not disturb normal paragraphs and vice versa.
     *
     * @returns {HTMLElement|undefined}
     *  A previous list style paragraph node or 'undefined', if no previous list style paragraph
     *  could be found.
     */
    function getPreviousParagraphWithNumberingListAttributes(rootNode, oneParagraph, paragraphStyle) {

        // the locally used root node
        var localRootNode = self.getSelection().isAdditionalTextframeSelection() ? $(oneParagraph).closest(DRAWING_NODE_SELECTOR) : rootNode;
        // all previous paragraph nodes
        var allPreviousParagraphs = getNodeSelection(localRootNode, PARAGRAPH_NODE_SELECTOR, oneParagraph, { followingNodes: false, includeReference: false });
        // whether the local root node is a drawing (-> never mix lists inside and outside of text frames)
        var rootIsDrawing = self.getSelection().isAdditionalTextframeSelection() || self.getCommentLayer().isCommentId(self.getActiveTarget());
        // whether the specified paragraph is inside header or footer
        var isMarginalParagraph = isMarginalNode(oneParagraph);
        // the list collection object
        var listCollection = self.getListCollection();
        // an object with ignorable list styles
        var ignorableParagraphStyles = listCollection.getIgnorableParagraphStyles();
        // the normalized paragraph style
        var localParagraphStyle = (paragraphStyle && !ignorableParagraphStyles[paragraphStyle]) ? paragraphStyle : '';
        // the position of the searched previous list paragraph
        var lastIndex = _.findLastIndex(allPreviousParagraphs, function (para) {
            var attrs = self.paragraphStyles.getElementAttributes(para);
            var listStyleId = attrs.paragraph.listStyleId;
            var listLevel = attrs.paragraph.listLevel || 0;
            var paraStyle = (attrs.styleId && !ignorableParagraphStyles[attrs.styleId]) ? attrs.styleId : '';

            if (!rootIsDrawing && $(para).closest(DRAWING_NODE_SELECTOR).length > 0) { return; } // ignoring previous paragraphs in drawings/comments
            if (!isMarginalParagraph && isMarginalNode(para)) { return; } // ignoring previous paragraphs in header/footer
            return listStyleId !== '' && paraStyle === localParagraphStyle && listCollection.isNumberingList(listStyleId, listLevel);
        });

        return (lastIndex > -1) ? allPreviousParagraphs.get(lastIndex) : $();
    }

    /**
     * Helper function to set a new paragraph list style ID to all paragraphs with a specified 'old' list style ID.
     *
     * @param {Node|jQuery} rootNode
     *  The DOM root node, that is used to calculate the logical positions for the paragraphs.
     *
     * @param {HTMLElement|jQuery} oneParagraph
     *  The first paragraph node in the document, that gets the new list style ID assigned. Additionally all following
     *  paragraphs with the same list style ID will get the new list style ID assigned.
     *
     * @param {String} newListStyleId
     *  The new list style ID for the affected paragraphs.
     *
     * @param {String} oldListStyleId
     *  The old list style ID. All paragraphs following the specified paragraph that have this 'old' list style ID
     *  and the specified paragraph itself, need to get the new list style ID.
     *
     * @param {Number} listLevel
     *  The list level of the affected paragraphs. Only for paragraphs with the same or higher listlevels an operation
     *  is generated, but never for a paragraph with lower list level.
     *  Info: Also modifying the list style ID of paragraphs with higher list label, looks better, if the user reduces
     *        the list level later. (But maybe it is more consistent, if only paragraphs with the same list level are
     *        modified.)
     *
     * @param {Object} generator
     *  An operations generator object to collect all operations.
     */
    function collectListStyleOpsToFollowingParagraphs(rootNode, oneParagraph, newListStyleId, oldListStyleId, listLevel, generator) {

        // target for operation
        var target = self.getActiveTarget();
        // the change track manager
        var changeTrack = self.getChangeTrack();
        // the old paragraph attributes
        var oldAttrs = null;
        // the operation attributes
        var operationAttrs = { attrs: { paragraph: { listStyleId: newListStyleId } } };
        // the locally used root node
        var localRootNode = self.getSelection().isAdditionalTextframeSelection() ? $(oneParagraph).closest(DRAWING_NODE_SELECTOR) : rootNode;
        // whether the page is the current root node -> in this case the following paragraph needs to be filtered more
        var isPageRootNode = isPageNode(localRootNode);
        // the page node as DOM node
        var pageNode = isPageRootNode ? getDomNode(localRootNode) : null;
        // the last paragraph that was handled (performance)
        var lastParaHandled = null;
        // collecting all paragraphs behind the specified paragraph (including the specified reference paragraph)
        var allFollowingParagraphs = getNodeSelection(localRootNode, PARAGRAPH_NODE_SELECTOR, oneParagraph, { followingNodes: true, includeReference: true });

        // generating the operation for one paragraph, if required
        function handleParagraphNode(paragraph) {
            var paraAttributes = null;
            var isNeighborNode = isPageRootNode ? (lastParaHandled && lastParaHandled.nextSibling === getDomNode(paragraph)) : true; // try to avoid usage of closest
            // checking for lists in the page (not in comment, header, drawing, ...)
            if (isPageRootNode && !isNeighborNode && getDomNode($(paragraph).closest(LIST_BORDER_NODE_SELECTOR)) !== pageNode) { return; }

            paraAttributes = self.paragraphStyles.getElementAttributes(paragraph).paragraph;
            if (paraAttributes && paraAttributes.listStyleId === oldListStyleId && paraAttributes.listLevel >= listLevel && !isImplicitParagraphNode(paragraph)) {
                operationAttrs = { attrs: { paragraph: { listStyleId: newListStyleId } } };
                if (target) { operationAttrs.target = target; }

                if (changeTrack.isActiveChangeTracking()) {
                    oldAttrs = changeTrack.getOldNodeAttributes(oneParagraph);
                    oldAttrs = _.extend(oldAttrs, changeTrack.getChangeTrackInfo());
                    operationAttrs.attrs.changes = { modified: oldAttrs };
                }

                operationAttrs.start = getOxoPosition(rootNode, paragraph);
                generator.generateOperation(SET_ATTRIBUTES, operationAttrs);

                lastParaHandled = getDomNode(paragraph); // saving for performace reasons
            }
        }

        _.each(allFollowingParagraphs, function (paragraphNode) {
            handleParagraphNode(paragraphNode);
        });
    }

    /**
     * Helper function to generate setAttributes operations and save them in an operations generator.
     *
     * @param {OperationGenerator} generator
     *  The operations generator
     *
     * @param {Node|jQuery} rootNode
     *  The DOM root node, that is used to calculate the logical positions for the paragraphs.
     *
     * @param {String} target
     *  The currently active target for the logical position.
     *
     * @param {HTMLElement|jQuery} oneParagraph
     *  The paragraph node for that the operation needs to be generated.
     *
     * @param {Object} paraAttrs
     *  The full stack of paragraph attributes.
     *
     * @param {String} listStyleId
     *  The id of the list style.
     *
     * @param {Number} listLevel
     *  The list level.
     *
     * @param {Number[]} [position]
     *  The logical position of the paragraph. Can be used for performance reasons.
     */
    function makeListStyleOperation(generator, rootNode, target, oneParagraph, paraAttrs, listStyleId, listLevel, position) {

        // the new paragraph attributes
        var newAttrs = { paragraph: { listStyleId, listLevel } };
        // the set of operation attributes
        var operationAttrs = { attrs: newAttrs };
        // the change track manager
        var changeTrack = self.getChangeTrack();
        // the old paragraph attributes
        var oldAttrs = null;

        // transferring list level, if not specified
        if (!_.isNumber(listLevel) && paraAttrs) { newAttrs.paragraph.listLevel = paraAttrs.listLevel; }

        // assigning list style id to paragraph, if it has the default style id (40787)
        if (paraAttrs.styleId === self.getDefaultUIParagraphStylesheet()) { newAttrs.styleId = self.getDefaultUIParagraphListStylesheet(); }

        if (target) { operationAttrs.target = target; }

        // generating a new paragraph, if it is implicit
        if (isImplicitParagraphNode(oneParagraph)) { generator.generateOperation(PARA_INSERT, { start: position ? _.copy(position) : getOxoPosition(rootNode, oneParagraph) }); }

        if (changeTrack.isActiveChangeTracking()) {
            oldAttrs = changeTrack.getOldNodeAttributes(oneParagraph);
            oldAttrs = _.extend(oldAttrs, changeTrack.getChangeTrackInfo());
            newAttrs.changes = { modified: oldAttrs };
        }

        operationAttrs.start = position ? _.copy(position) : getOxoPosition(rootNode, oneParagraph);
        generator.generateOperation(SET_ATTRIBUTES, operationAttrs);
    }

    /**
     * In ODF format the list start value must be set as hard attribute to the paragraph (54447).
     *
     * @param {OperationGenerator} generator
     *  The operations generator
     *
     * @param {HTMLElement|jQuery} paragraph
     *  The paragraph node that gets the list start value assigned.
     *
     * @param {Number[]} pos
     *  The logical position of the paragraph.
     *
     * @param {Number|Null} listStartValue
     *  The list start valuethe affected paragraphs. Only for paragraphs with the same or higher listlevels an operation
     */
    function handleListStartValue(generator, paragraph, pos, listStartValue) {

        // target for operation
        var target = self.getActiveTarget();
        // the change track manager
        var changeTrack = self.getChangeTrack();
        // the set of operation attributes
        var operationAttrs = { attrs: { paragraph: { listStartValue } } };
        // the old paragraph attributes
        var oldAttrs = null;

        if (target) { operationAttrs.target = target; }

        if (changeTrack.isActiveChangeTracking()) {
            oldAttrs = changeTrack.getOldNodeAttributes(paragraph);
            oldAttrs = _.extend(oldAttrs, changeTrack.getChangeTrackInfo());
            operationAttrs.attrs.changes = { modified: oldAttrs };
        }

        operationAttrs.start = _.copy(pos);
        generator.generateOperation(SET_ATTRIBUTES, operationAttrs);
    }

    /**
     * Keeping an optionally existing list level, so that the new assigned list style does not change the indentation
     * of the paragraph (57707). But if the paragraph was no list paragraph before it might have a value for the listLevel
     * of '-1'. This happens when the list style was removed from a paragraph. In this case the specified default list
     * level must be used.
     *
     * @param {HTMLElement|jQuery} paragraph
     *  The paragraph node for that the list level need to be determined.
     *
     * @param {Number} defaultLevel
     *  The default level that can be used, if no listLevel is specified at the paragraph or if it is '-1'.
     *
     * @returns {Number}
     *  The new list level for the specified paragraph.
     */
    function getValidParagraphListLevel(paragraph, defaultLevel) {

        // the list level for the specified paragraph
        var listLevel = defaultLevel;
        // the explicit attributes of family 'paragraph' of the specified paragraph
        var paraAttrs = getExplicitAttributes(paragraph, 'paragraph');

        if (paraAttrs && _.isNumber(paraAttrs.listLevel) && paraAttrs.listLevel > -1) { listLevel = paraAttrs.listLevel; }

        return listLevel;
    }

    // public methods -----------------------------------------------------

    /**
     * Creates a default list either with decimal numbers or bullets
     *
     * @param {String} type
     *  The list type: 'numbering' or 'bullet'
     */
    this.createDefaultList = function (type) {
        var defListStyleId = self.getListCollection().getDefaultNumId(type);

        if (defListStyleId) {
            var listId = self.getListCollection().getListId(self.getListCollection().getDefaultListDefinition(type), defListStyleId);
            if (listId) {
                defListStyleId = listId;
            } else {
                defListStyleId = undefined;
            }
        }

        self.getUndoManager().enterUndoGroup(function () {

            var selection = self.getSelection();
            var start = selection.getStartPosition();
            var rootNode = self.getCurrentRootNode();
            var para = null;
            var listLevel = 0;

            if (defListStyleId === undefined) {
                var listOperation = self.getListCollection().getDefaultListOperation(type);
                this.applyOperations(listOperation);
                defListStyleId = listOperation.listStyleId;
            }

            // the list level might be specified by the number of leading tabs in the paragraph (55908)
            start.pop();
            para = getParagraphElement(rootNode, start);
            listLevel = para ? getLeadingTabsInParagraph(para) : 0;

            setListStyle(defListStyleId, listLevel);

            if (listLevel) {
                // generate delete operation, if leading tabs need to be removed
                this.applyOperations({ name: DELETE, start: appendNewIndex(start), end: appendNewIndex(start, listLevel - 1) });
                selection.setTextSelection(appendNewIndex(start)); // immediately fixing position
            }

        }, this);
    };

    /**
     * Creates a non-default list either with decimal numbers or bullets that
     * are available at the GUI.
     *
     * @param {String} listStyleId
     *  The id of the list style.
     *
     * @param {Number} listLevel
     *  The list level.
     */
    this.createSelectedListStyle = function (listStyleId, listLevel) {

        // the operations generator
        var generator = null;
        // the selection object
        var selection = self.getSelection();
        // the root node of the current selection
        var rootNode = self.getCurrentRootNode();
        // the locally used root node
        var localRootNode = null;
        // the page DOM node
        var localPageNode = null;
        // the list operation object
        var listOperation;
        // an object containing the operation that creates the requested list (optional) and the id of the merged list
        var listOperationAndStyle;
        // the logical start position
        var _start = selection.getStartPosition();
        // the active target node
        var target = self.getActiveTarget();
        // a modifier for list style in ODF header/footer nodes
        var odftarget = null;
        // the paragraph node at start position
        var para;
        // the paragraph attributes of the paragraph at start position
        var paraAttributes;
        // the list style ID
        var fullListStyleId = listStyleId;
        // a selector to find all affected paragraphs
        var paraSelector = null;
        // a collector for all affected paragraphs
        var allParagraphs = null;
        // the style Id to get the predefined liststyle
        var predefinedStyleId = null;
        // the number of leading tabs in the selected paragraph
        var numberOfLeadingTabs = 0;
        // the list ID
        var listId = null;

        _start.pop();
        para = getParagraphElement(rootNode, _start);
        paraAttributes = self.paragraphStyles.getElementAttributes(para).paragraph;

        if (!selection.hasRange() && paraAttributes.listStyleId !== '') {
            // merge level from new listlevel with used list level and apply that new list to all consecutive paragraphs using the old list
            if (listStyleId !== paraAttributes.listStyleId) {

                generator = self.createOperationGenerator();

                listOperationAndStyle = self.getListCollection().mergeListStyle(paraAttributes.listStyleId, listStyleId, paraAttributes.listLevel);

                if (listOperationAndStyle.listOperation) {
                    generator.appendOperations(listOperationAndStyle.listOperation); // first operation is the insertListStyle operation
                }

                // finding the best valid local root node (reducing area to drawing)
                localRootNode = self.getSelection().isAdditionalTextframeSelection() ? $(para).closest(DRAWING_NODE_SELECTOR) : rootNode;

                // finding all affected paragraphs in the document, not only the neighboring paragraphs
                paraSelector = 'div.p.' + LIST_PARAGRAPH_MARKER + '[' + LIST_PARAGRAPH_ATTRIBUTE + '="' + paraAttributes.listStyleId + '"]';
                allParagraphs = getNodeSelection(localRootNode, paraSelector);

                // Removing invalid paragraphs, if the page is the root node (54460). Then paragraphs in drawings, comments, ... must not be modified
                if (isPageNode(localRootNode)) {
                    localPageNode = getDomNode(localRootNode);
                    allParagraphs = _.filter(allParagraphs, function (para) {
                        return getDomNode($(para).closest(LIST_BORDER_NODE_SELECTOR)) === localPageNode;
                    });
                }

                _.each(allParagraphs, function (paragraph) {
                    var paraAttrs = self.paragraphStyles.getElementAttributes(paragraph).paragraph;
                    makeListStyleOperation(generator, rootNode, target, paragraph, paraAttrs, listOperationAndStyle.listStyleId);
                });
            }

        } else {

            generator = self.createOperationGenerator();

            predefinedStyleId = listStyleId;

            if ((self.getApp().isODF() && self.isHeaderFooterEditState())) { // odt handling (44393)
                if (target) {
                    fullListStyleId = listStyleId + '_' + target;
                    odftarget = target;
                }
            } else {

                listId = self.getListCollection().getListId(self.getListCollection().getPredefinedListStyles()[listStyleId].definition, listStyleId);
                if (listId) {
                    listStyleId = listId;
                    fullListStyleId = listStyleId;
                } else {
                    fullListStyleId = self.getListCollection().getFreeListId();
                    listStyleId = fullListStyleId;
                }
            }

            listOperation = self.getListCollection().getSelectedListStyleOperation(listStyleId, odftarget, predefinedStyleId);

            if (listOperation) {
                generator.appendOperations(listOperation); // first operation is the insertListStyle operation
            }

            listLevel = Math.max(0, listLevel || 0);  // setting listLevel for paragraphs, that were not in lists before

            // count the number of leading tabs at the beginning of the paragraph (55908)
            if (listLevel === 0) {
                numberOfLeadingTabs = getLeadingTabsInParagraph(para);
                if (numberOfLeadingTabs) {
                    listLevel = numberOfLeadingTabs;
                    // generate delete operation for the tabs
                    generator.generateOperation(DELETE, { start: appendNewIndex(_start), end: appendNewIndex(_start, numberOfLeadingTabs - 1) });
                    selection.setTextSelection(appendNewIndex(_start)); // immediately fixing position (62405)
                }
            }

            // iterating over all paragraphs in the selection
            var elementsToApply = [];
            self.getSelection().iterateContentNodes(function (paragraph, position) {
                elementsToApply.push({ paragraph, position });
            });

            // Bug 57688: this is necessary because iterateContentNodes does not return empty last paragraph
            // TODO: improve iterateContentNodes and remove this code block again (also for bold, italic...)
            var endPosition = selection.getEndPosition();
            if (_.last(endPosition) === 0) {
                endPosition.pop();
                var paraNode = getParagraphElement(self.getCurrentRootNode(), endPosition);
                if (paraNode && paraNode !== _.last(elementsToApply).paragraph) {
                    elementsToApply.push({ paragraph: paraNode, position: endPosition });
                }
            }

            _.each(elementsToApply, function (element) {
                var localListLevel = getValidParagraphListLevel(element.paragraph, listLevel); // 57707
                makeListStyleOperation(generator, rootNode, target, element.paragraph, paraAttributes, fullListStyleId, localListLevel, element.position);
            });
        }

        // apply all collected operations
        if (generator) { self.applyOperations(generator); }
    };

    /**
     * Creates a new list after pressing 'Enter'. In this case the list auto detection found a valid structure
     * for a new list style.
     *
     * @param {String} type
     *  The list type: 'numbering' or 'bullet'
     *
     * @param {Object} options
     *  The detected options that are used to create the new list style.
     */
    this.createList = function (type, options) {

        var defListStyleId = (!options || (!options.symbol && !options.listStartValue)) ? self.getListCollection().getDefaultNumId(type) : undefined,
            // the attributes object for the operation
            allAttrs = null,
            // the old paragraph attributes
            oldAttrs = null,
            // the paragraph dom node
            paraNode = null,
            // the operation object
            newOperation = null;

        if (defListStyleId === undefined) {
            var listOperation = self.getListCollection().getDefaultListOperation(type, options);
            self.applyOperations(listOperation);
            defListStyleId = listOperation.listStyleId;
        }
        if (options && options.startPosition) {
            var start = _.clone(options.startPosition),
                listParaStyleId = this.getDefaultUIParagraphListStylesheet(),
                insertStyleOperation = null,
                paragraphStyles = self.paragraphStyles;

            start.pop();
            // register pending style sheet via 'insertStyleSheet' operation
            if (_.isString(listParaStyleId) && paragraphStyles.isDirty(listParaStyleId)) {

                var styleSheetAttributes = {
                    attrs: paragraphStyles.getStyleSheetAttributeMap(listParaStyleId),
                    type: 'paragraph',
                    styleId: listParaStyleId,
                    styleName: paragraphStyles.getName(listParaStyleId),
                    parent: paragraphStyles.getParentId(listParaStyleId),
                    uiPriority: paragraphStyles.getUIPriority(listParaStyleId)
                };

                // parent is an optional value, should not be send as 'null'
                if (styleSheetAttributes.parent === null || styleSheetAttributes.parent === '') { delete styleSheetAttributes.parent; }

                insertStyleOperation = _.extend({ name: INSERT_STYLESHEET }, styleSheetAttributes);
                self.applyOperations(insertStyleOperation);

                // remove the dirty flag
                paragraphStyles.setDirty(listParaStyleId, false);
            }

            // the attributes object for the operation
            allAttrs = { styleId: listParaStyleId, paragraph: { listStyleId: defListStyleId, listLevel: 0 } };

            // handling change track informations for set attributes operations (36259)
            if (self.getChangeTrack().isActiveChangeTracking()) {
                // Creating one setAttribute operation for each span inside the paragraph, because of old attributes!
                oldAttrs = {};
                paraNode = getParagraphElement(self.getNode(), start);

                if (isParagraphNode(paraNode)) {
                    oldAttrs = self.getChangeTrack().getOldNodeAttributes(paraNode);
                }

                // adding the old attributes, author and date for change tracking
                if (oldAttrs) {
                    oldAttrs = _.extend(oldAttrs, self.getChangeTrack().getChangeTrackInfo());
                    allAttrs.changes = { modified: oldAttrs };
                }
            }

            newOperation = {
                name: SET_ATTRIBUTES,
                attrs: allAttrs,
                start:  _.copy(start)
            };
            self.applyOperations(newOperation);
            newOperation = _.copy(newOperation);
            newOperation.start = increaseLastIndex(newOperation.start);
            self.applyOperations(newOperation);
        } else {
            self.setAttributes('paragraph', { styleId: this.getDefaultUIParagraphListStylesheet(), paragraph: { listStyleId: defListStyleId, listLevel: 0 } });
        }

    };

    /**
     * Removes bullet/numbered list formatting from the selected
     * paragraphs.
     */
    this.removeListAttributes = function () {

        var paragraph = getLastNodeFromPositionByNodeName(self.getCurrentRootNode(), self.getSelection().getStartPosition(), PARAGRAPH_NODE_SELECTOR),
            prevPara = findPreviousNode(self.getCurrentRootNode(), paragraph, PARAGRAPH_NODE_SELECTOR),
            attrs = self.getAttributes('paragraph'),
            newAttrs = { paragraph: { listStyleId: null, listLevel: -1 } };

        if (!attrs.styleId || attrs.styleId === self.getDefaultUIParagraphListStylesheet()) {
            //set list style only, if there is no special para style chosen
            newAttrs.styleId = self.getDefaultUIParagraphStylesheet();
            if (attrs.paragraph && _.isNumber(attrs.paragraph.indentFirstLine) && attrs.paragraph.indentFirstLine < 0) { newAttrs.paragraph.indentFirstLine = 0; } // DOCS-2278
        }

        self.setAttributes('paragraph', newAttrs);
        if (prevPara) {
            self.paragraphStyles.updateElementFormatting(prevPara);
        }
    };

    /**
     * Returns all predefined bullet list styles.
     *
     * @returns {Object}
     *  A map with list style identifiers as keys, and objects as values
     *  containing the attributes 'definition' with the list style definition,
     *  'listLabel' containing the bullet text, and 'tooltip' containing a GUI
     *  tool tip string for the list style.
     */
    this.getPredefinedBulletListStyles = function () {

        var // the resulting bullet list styles, mapped by list identifier
            bulletListStyles = {},
            // all predefined list styles
            allPredefinedListStyles = self.getListCollection().getPredefinedListStyles();

        _(allPredefinedListStyles).each(function (listStyle, listStyleId) {
            if (listStyle.definition.listLevel0.numberFormat === 'bullet') {
                bulletListStyles[listStyleId] = listStyle;
            }
        });

        return bulletListStyles;
    };

    /**
     * Returns all predefined numbered list styles.
     *
     * @returns {Object}
     *  A map with list style identifiers as keys, and objects as values
     *  containing the attributes 'definition' with the list style definition,
     *  'listlabel' containing a string with the number 1 formatted according
     *  to the list style, and 'tooltip' containing a GUI tool tip string for
     *  the list style.
     */
    this.getPredefinedNumberedListStyles = function () {

        var // the resulting numbered list styles, mapped by list identifier
            numberedListStyles = {},
            // all predefined list styles
            allPredefinedListStyles = self.getListCollection().getPredefinedListStyles();

        _(allPredefinedListStyles).each(function (listStyle, listStyleId) {
            if (listStyle.definition.listLevel0.numberFormat !== 'bullet') {
                numberedListStyles[listStyleId] = listStyle;
            }
        });

        return numberedListStyles;
    };

    /**
     * Check, whether the specified paragraph attribute set specifies a paragraph with
     * defined bullet type. The bullet can be a character, a number or a bitmap.
     *
     * @param {Object} paragraphAttrs
     *  The paragraph attribute set.
     *
     * @returns {Boolean}
     *  Whether the specified paragraph attribute set specifies a paragraph with defined
     *  list level.
     */
    this.isListParagraph = function (paragraphAttrs) {
        return paragraphAttrs && _.isNumber(paragraphAttrs.listLevel) && paragraphAttrs.listLevel > -1;
    };

    /**
     * Check, whether at a specified position the list auto detection shall be executed.
     *
     * @returns {Boolean}
     *  Whether the list auto detection can be executed at the current position.
     */
    this.isAutoDetectionPosition = function () {
        // in the text application the list detection must not happen in ODT files in shapes and frames and comments.
        return self.checkTextListAvailability();
    };

    /**
     * Check, whether a specified string at a specified position in a paragraph with a specified
     * text length can be used to create a list automatically.
     *
     * @param {Number[]} position
     *  The logical position.
     *
     * @param {Number} paragraphLength
     *  The length of the text in the paragraph.
     *
     * @param {String} paraText
     *  The text in the paragraph.
     *
     * @returns {Boolean}
     *  Whether the specified string can be used to create a list automatically.
     */
    this.isListAutoDetectionString = function (position, paragraphLength, paraText) {

        return (_.last(position) > (paraText.indexOf('. ') + 1)) && (
            ((paraText.indexOf('. ') >= 0) && (paragraphLength > 3) && (paraText.indexOf('. ') < (paraText.length - 2))) ||
            ((paraText.indexOf(' ') >= 0) && (paragraphLength > 2) && (paraText.indexOf('* ') === 0 || paraText.indexOf('- ') === 0))
        );
    };

    /**
     * Returns whether the passed node is a list paragraph element that is the first paragraph
     * in its list and in its level. For every list level, one paragraph has the marker class.
     *
     * Info: This is a convenience function at the list handler, that directly uses the DOM function.
     *
     * @param {Node|jQuery} [paragraph]
     *  The DOM node to be checked. If this object is a jQuery collection, uses the first DOM node
     *  it contains. If missing or null, returns false.
     *  If the paragraph is not specified, the paragraph at the logical start position is used.
     *
     * @returns {Boolean}
     *  Whether the passed node is a list paragraph element, that is the first paragraph in its
     *  list.
     */
    this.isFirstParagraphInList = function (paragraph) {

        // the logical start position of the selection
        var startPos = null;

        if (!paragraph) {
            startPos = self.getSelection().getStartPosition();
            startPos.pop();
            paragraph = getParagraphElement(self.getCurrentRootNode(), startPos);
        }

        return paragraph ? isFirstParagraphInList(paragraph) : false;
    };

    /**
     * Returns whether the paragraph with the specified paragraph attributes uses an increased list
     * start value. This means that the paragraph has a list style ID and a list level assigned. If the
     * property 'listStartValue' for the specified list style ID and list level is greater than 1, this
     * function returns true.
     *
     * @param {Object} attrs
     *  The paragraph attribute set.
     *
     * @returns {Boolean}
     *  Whether the paragraph with the specified paragraph attributes uses an increased list
     *  start value.
     */
    this.isIncreasedStartValueInList = function (attrs) {
        var listLevelDef = self.getListCollection().getListLevel(attrs.listStyleId, attrs.listLevel || 0);
        var listStartValue = 1;

        if (listLevelDef && _.isNumber(listLevelDef.listStartValue)) {
            listStartValue = listLevelDef.listStartValue;
        }

        return listStartValue > 1;
    };

    /**
     * Setting a numbering list start value for a selected paragraph. Supported values are:
     * - the first value for the numbered list of the first selected paragraph (1., a., i. ...)
     * - continuing the list after a previous found numbered list paragraph.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.continue=false]
     *      Whether the numbered list of the first selected paragraph shall be continued from a
     *      previous numbered list paragraph or if it shall start with its first value.
     *      Default is: Start with first value.
     *
     * @returns {Number}
     *  The number of generated operations.
     */
    this.setListStartValue = function (options) {

        // whether a list shall be continued or started with first value
        var continueList = getBooleanOption(options, 'continue', false);
        // the list collection object
        var listCollection = self.getListCollection();
        // the current root node of the selection
        var rootNode = self.getCurrentRootNode();
        // the logical start position of the selection
        var startPos = self.getSelection().getStartPosition();
        // the first paragraph of the selection
        var paragraph = null;
        // the complete set of attributes of the first selected paragraph
        var allAttributes = null;
        // the paragraph attributes of the first selected paragraph
        var paraAttributes = null;
        // the paragraph style of the first selected paragraph
        var paragraphStyle = null;
        // the list level of the first selected paragraph
        var listLevel = 0;
        // the list style ID of the first selected paragraph
        var listStyleId = null;
        // a previous paragraph node before the first selected paragraph
        var prevParagraph = null;
        // the attributes of the previous paragraph
        var prevAttributes = null;
        // the list style ID of the previous paragraph
        var prevListStyleId = null;
        // an optionally generated new list operation
        var listOperation = null;
        // the operations generator
        var generator = null;

        // all selected paragraphs must have the same list style and the same list level (TODO: Check this for enabling in context menu)
        // -> in this function only the first paragraph needs to be evaluated

        startPos.pop();
        paragraph = getParagraphElement(rootNode, startPos);

        if (paragraph) {

            allAttributes = self.paragraphStyles.getElementAttributes(paragraph);
            paraAttributes = allAttributes.paragraph;
            paragraphStyle = allAttributes.styleId || '';

            if (paraAttributes && paraAttributes.listStyleId) {

                listStyleId = paraAttributes.listStyleId;
                listLevel = paraAttributes.listLevel || 0;

                if (listCollection.isNumberingList(listStyleId, listLevel)) {

                    // check for current list style, and for the list style of the previous list paragraph

                    if (continueList) {

                        // finding the list style of the previous list paragraph
                        prevParagraph = getPreviousParagraphWithNumberingListAttributes(rootNode, paragraph, paragraphStyle);

                        if (prevParagraph) {
                            prevAttributes = self.paragraphStyles.getElementAttributes(prevParagraph).paragraph;
                            prevListStyleId = prevAttributes.listStyleId;

                            if (prevListStyleId && prevListStyleId !== listStyleId) { // only do something, if the list style changes
                                generator = self.createOperationGenerator();
                                // setting the prev list style ID to this paragraph and the following paragraphs with the same list style ID
                                // -> these paragraphs can be anywhere in the document
                                collectListStyleOpsToFollowingParagraphs(rootNode, paragraph, prevListStyleId, listStyleId, listLevel, generator);
                            }
                        }

                        // Removing an existing list start value in ODF (54447)
                        if (self.getApp().isODF() && paraAttributes && _.isNumber(paraAttributes.listStartValue)) {
                            generator = generator || self.createOperationGenerator();
                            handleListStartValue(generator, paragraph, startPos, null);
                        }

                    } else {

                        // Check, if this is not the first paragraph of the list
                        // -> additionally it is possible to reset the list start value of the first paragraph in list
                        if (!isFirstParagraphInList(paragraph) || self.isIncreasedStartValueInList(paraAttributes)) {

                            // -> setting a new list style to this and the following paragraphs with the same list style ID

                            // generating a new list style operation, but avoiding to transfer the base style. Otherwise the
                            // counting in updateLists is not correct
                            listOperation = listCollection.getNewListOperationFromListStyleId(listStyleId, { useBaseStyle: false, resetStartValue: true });

                            if (listOperation) {
                                generator = self.createOperationGenerator();
                                generator.appendOperations(listOperation); // first operation is the insertListStyle operation

                                // setting the prev list style ID to this paragraph and the following paragraphs with the same list style ID
                                // -> these paragraphs can be anywhere in the document
                                collectListStyleOpsToFollowingParagraphs(rootNode, paragraph, listOperation.listStyleId, listStyleId, listLevel, generator);

                                // ODF reqires that the list start value ('1' in this case) is explicitely assigned to the paragraph (54447)
                                if (self.getApp().isODF()) { handleListStartValue(generator, paragraph, startPos, 1); }
                            }
                        }
                    }
                }
            }
        }

        // apply all collected operations
        if (generator) { self.applyOperations(generator); }

        // returning the number of generated operations
        return generator ? generator.getOperationCount() : 0;
    };

    /**
     * After changing the list level, it might be necessary to update the paragraph style of the paragraph
     * in the current selection. This happens only for selections that are inside one paragraph (DOCS-3243).
     *
     * @param {Number} listLevel
     *  The new list level of the paragraph.
     *
     * @returns {Boolean}
     *  Whether the operation was generated within this function.
     */
    this.setParagraphStyleForListLevel = function (listLevel) {

        // whether the operations are generated in this function
        var generatedOperation = false;
        // the operations generator
        var generator = null;
        // the current root node
        var rootNode = self.getCurrentRootNode();
        // the selection object
        var selection = self.getSelection();
        // the logical start position
        var startPos = selection.getStartPosition();
        // the logical end position
        var endPos = selection.getEndPosition();
        // whether the selection is inside one paragraph
        var singleParagraph = equalPositions(startPos, endPos, startPos.length - 1);

        if (singleParagraph) {

            startPos.pop();
            var paragraph = getParagraphElement(rootNode, startPos);
            var paragraphAttrs = self.paragraphStyles.getElementAttributes(paragraph);

            if (paragraphAttrs && paragraphAttrs.paragraph && paragraphAttrs.paragraph.listStyleId) {

                var listStyleId = paragraphAttrs.paragraph.listStyleId;
                var listStyleList = self.getListCollection().getList(listStyleId);

                if (listStyleList && listStyleList.listLevels && listStyleList.listLevels[listLevel] && listStyleList.listLevels[listLevel].paraStyle) {
                    var newParagraphStyleId = listStyleList.listLevels[listLevel].paraStyle;
                    generator = self.createOperationGenerator();
                    generator.generateOperation(SET_ATTRIBUTES, { start:  _.copy(startPos), attrs: { styleId: newParagraphStyleId, paragraph: { listLevel } } });
                    self.applyOperations(generator);
                    generatedOperation = true;
                }
            }
        }

        return generatedOperation;
    };
}
