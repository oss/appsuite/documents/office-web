/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { json } from '@/io.ox/office/tk/algorithms';

import { equalPositions, comparePositions } from '@/io.ox/office/editframework/utils/operations';
import { isOperationRemoved, setOperationRemoved } from '@/io.ox/office/editframework/utils/otutils';

import { OT_DELETE_TARGET_ALIAS, OT_INSERT_CHAR_ALIAS, OT_INSERT_COMP_ALIAS, OT_MERGE_COMP_ALIAS,
    OT_SPLIT_COMP_ALIAS, getResultObject, ancestorRemoved, calculateDeleteArray,
    TextBaseOTManager } from '@/io.ox/office/textframework/model/otmanager';

import * as Op from '@/io.ox/office/textframework/utils/operations';

// constants ==============================================================

/**
 * The names of all additional text operations that delete a target object
 * (an object addressed with the `target` property). Will be aliased to the
 * synthetic operation name `OT_DELETE_TARGET_ALIAS`.
 */
const DELETE_TARGET_OPS = [
    Op.DELETE_HEADER_FOOTER
];

/**
 * The names of all additional text operations that insert content into a
 * paragraph. Will be aliased to the synthetic operation name
 * `OT_INSERT_CHAR_ALIAS`.
 */
const INSERT_CHAR_OPS = [
    Op.INSERT_DRAWING,
    Op.COMMENT_INSERT,
    Op.COMPLEXFIELD_INSERT
];

// class TextOTManager ====================================================

export default class TextOTManager extends TextBaseOTManager {

    constructor() {

        // base constructor
        super();

        // initialization
        this.registerAliasName(OT_DELETE_TARGET_ALIAS, DELETE_TARGET_OPS);
        this.registerAliasName(OT_INSERT_CHAR_ALIAS, INSERT_CHAR_OPS);

        // registering OX Text specific operations to be ignored
        // this.registerIgnoreNames();

        this.registerTransformations({
            changeComment: {
                changeComment: this.handleChangeCommentChangeComment,
                delete: this.handleSetAttrsDelete,
                deleteColumns: this.handleSetAttrsDeleteColumns,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertCells: this.handleSetAttrsInsertRows,
                insertChar: this.handleSetAttrsInsertChar,
                insertColumn: this.handleSetAttrsInsertColumn,
                insertComp: this.handleSetAttrsInsertComp,
                insertHeaderFooter: null,
                insertListStyle: null,
                insertRows: this.handleSetAttrsInsertRows,
                mergeComp: this.handleSetAttrsMergeComp,
                move: this.handleSetAttrsMove,
                position: null,
                setAttributes: null,
                splitParagraph: this.handleSetAttrsSplitPara,
                splitTable: this.handleSetAttrsSplitTable,
                updateComplexField: null,
                updateField: null
            },
            delete: {
                changeComment: this.handleSetAttrsDelete,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                move: this.handleMoveDelete,
                updateComplexField: this.handleSetAttrsDelete
            },
            deleteColumns: {
                changeComment: this.handleSetAttrsDeleteColumns,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                move: this.handleMoveDeleteColumns,
                updateComplexField: this.handleSetAttrsDeleteColumns
            },
            deleteHeaderFooter: { // handled by preprocessor for targets
                changeComment: null,
                delete: null,
                deleteColumns: null,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComp: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                insertRows: null,
                mergeComp: null,
                move: null,
                position: null,
                setAttributes: null,
                splitComp: null,
                updateComplexField: null,
                updateField: null
            },
            deleteListStyle: {
                changeComment: null,
                delete: null,
                deleteColumns: null,
                deleteHeaderFooter: null,
                deleteListStyle: this.handleDeleteListStyleDeleteListStyle,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComp: null,
                insertHeaderFooter: null,
                insertListStyle: this.handleInsertListStyleDeleteListStyle,
                insertRows: null,
                mergeComp: null,
                move: null,
                position: null,
                setAttributes: null,
                splitComp: null,
                updateComplexField: null,
                updateField: null
            },
            insertCells: {
                changeComment: this.handleSetAttrsInsertRows,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                move: this.handleMoveInsertRows,
                updateComplexField: this.handleSetAttrsInsertRows
            },
            insertChar: {
                changeComment: this.handleSetAttrsInsertChar,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                move: this.handleMoveInsertChar,
                updateComplexField: this.handleSetAttrsInsertChar
            },
            insertColumn: {
                changeComment: this.handleSetAttrsInsertColumn,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                move: this.handleMoveInsertColumn,
                updateComplexField: this.handleSetAttrsInsertColumn
            },
            insertComp: {
                changeComment: this.handleSetAttrsInsertComp,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                move: this.handleMoveInsertComp,
                updateComplexField: this.handleSetAttrsInsertComp
            },
            insertHeaderFooter: {
                changeComment: null,
                delete: null,
                deleteColumns: null,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComp: null,
                insertHeaderFooter: this.handleInsertHeaderInsertHeader,
                insertListStyle: null,
                insertRows: null,
                mergeComp: null,
                move: null,
                position: null,
                setAttributes: null,
                splitComp: null,
                updateComplexField: null,
                updateField: null
            },
            insertListStyle: {
                changeComment: null,
                delete: null,
                deleteColumns: null,
                deleteHeaderFooter: null,
                deleteListStyle: this.handleInsertListStyleDeleteListStyle,
                insertCells: null,
                insertChar: null,
                insertColumn: null,
                insertComp: null,
                insertHeaderFooter: null,
                insertListStyle: this.handleInsertListStyleInsertListStyle,
                insertRows: null,
                mergeComp: null,
                move: null,
                position: null,
                setAttributes: null,
                splitComp: null,
                updateComplexField: null,
                updateField: null
            },
            insertRows: {
                changeComment: this.handleSetAttrsInsertRows,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                move: this.handleMoveInsertRows,
                updateComplexField: this.handleSetAttrsInsertRows
            },
            mergeComp: {
                changeComment: this.handleSetAttrsMergeComp,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                move: this.handleMergeCompMove,
                updateComplexField: this.handleSetAttrsMergeComp
            },
            move: {
                changeComment: this.handleSetAttrsMove,
                delete: this.handleMoveDelete,
                deleteColumns: this.handleMoveDeleteColumns,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertCells: this.handleMoveInsertRows,
                insertChar: this.handleMoveInsertChar,
                insertColumn: this.handleMoveInsertColumn,
                insertComp: this.handleMoveInsertComp,
                insertHeaderFooter: null,
                insertListStyle: null,
                insertRows: this.handleMoveInsertRows,
                mergeComp: this.handleMergeCompMove,
                move: this.handleMoveMove,
                position: this.handleMoveExtPosition,
                setAttributes: this.handleSetAttrsMove,
                splitComp: this.handleSplitCompMove,
                updateComplexField: this.handleSetAttrsMove,
                updateField: this.handleSetAttrsMove
            },
            setAttributes: {
                changeComment: null,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                move: this.handleSetAttrsMove,
                updateComplexField: null
            },
            splitComp: {
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                move: this.handleSplitCompMove
            },
            splitParagraph: {
                changeComment: this.handleSetAttrsSplitPara,
                deleteHeaderFooter: null,
                updateComplexField: this.handleSetAttrsSplitPara
            },
            splitTable: {
                changeComment: this.handleSetAttrsSplitTable,
                deleteHeaderFooter: null,
                updateComplexField: this.handleSetAttrsSplitTable
            },
            updateComplexField: {
                changeComment: null,
                delete: this.handleSetAttrsDelete,
                deleteColumns: this.handleSetAttrsDeleteColumns,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertCells: this.handleSetAttrsInsertRows,
                insertChar: this.handleSetAttrsInsertChar,
                insertColumn: this.handleSetAttrsInsertColumn,
                insertComp: this.handleSetAttrsInsertComp,
                insertHeaderFooter: null,
                insertListStyle: null,
                insertRows: this.handleSetAttrsInsertRows,
                mergeComp: this.handleSetAttrsMergeComp,
                move: this.handleSetAttrsMove,
                position: null,
                setAttributes: null,
                splitParagraph: this.handleSetAttrsSplitPara,
                splitTable: this.handleSetAttrsSplitTable,
                updateComplexField: this.handleUpdateComplexFieldUpdateComplexField,
                updateField: null
            },
            updateField: {
                changeComment: null,
                deleteHeaderFooter: null,
                deleteListStyle: null,
                insertHeaderFooter: null,
                insertListStyle: null,
                move: this.handleSetAttrsMove,
                updateComplexField: null
            }
        });
    }

    // handler functions for two operations -------------------------------

    /**
     * Handling the specified position with the move operation.
     * Only moving of drawings is currently supported. The source paragraph and the destination paragraph are affected.
     *
     * move { start: [0, 0], end: [0, 0], to: [1, 0] }
     *
     * Only positions inside the same (paragraphs) are affected, if a drawing is moved.
     */
    handleMoveExtPosition(moveOp, posOp) {

        // the length of the start position of the move operation
        const moveStartLength = moveOp.start.length;
        // the length of the external position
        const posLength = posOp.start.length;

        if (posLength >= moveStartLength && equalPositions(moveOp.start, posOp.start, moveStartLength)) { // check, if the element containing the selection is moved
            posOp.start = moveOp.to.concat(posOp.start.slice(moveStartLength)); // using 'move.to' instead of 'move.start' at current position
        } else {
            this.handleDeleteExtPosition(moveOp, posOp);
            if (!isOperationRemoved(posOp)) {
                // the 'to' position is the 'start' position for inserting element
                this.handleInsertCharExtPosition({ start: moveOp.to }, posOp);
            }
        }
    }

    /**
     * Handling two move operations.
     */
    handleMoveMove(localOp, extOp) {

        // the copy of the local operation
        const origLocalOp = json.deepClone(localOp);
        // the collector for the logical positions of the local move operation
        const localAllPos = [localOp.start, localOp.end, localOp.to];
        // the collector for the logical positions of the external move operation
        const extAllPos = [extOp.start, extOp.end, extOp.to];

        if (equalPositions(localOp.start, extOp.start) && equalPositions(localOp.end, extOp.end)) {
            if (equalPositions(localOp.to, extOp.to)) {
                // setting marker at both operations
                setOperationRemoved(localOp); // setting marker at internal operation!
                setOperationRemoved(extOp); // setting marker at external operation
            } else {
                // different destination for the same drawing -> setting the new source to local operation that will be sent to the server
                // Example: local: move [1,5] -> [2,0], extern: move [1,5] -> [3,0] -> ignore extern and locally new source: move [3,0] to [2,0]
                setOperationRemoved(extOp); // setting marker at external operation
                localOp.start = json.deepClone(extOp.to);
                localOp.end = json.deepClone(extOp.to);
            }
        } else {
            // the external move operation influences the local move operation
            localAllPos.forEach((localMovePos, index) => {
                const posOp = { name: Op.POSITION, start: localMovePos };
                this.handleDeleteExtPosition({ name: Op.DELETE, start: json.deepClone(extOp.start) }, posOp);
                if (!isOperationRemoved(posOp)) {
                    this.handleInsertCharExtPosition({ name: Op.INSERT_DRAWING, start: json.deepClone(extOp.to) }, posOp);
                }
                // TODO: what happens with deleted position (set properties in localOp to undefined?)
                // TODO: localMovePos has been modified in-place, no need to assign back to localOp
                if (index === 0) { localOp.start = localMovePos; }
                if (index === 1) { localOp.end = localMovePos; }
                if (index === 2) { localOp.to = localMovePos; }
            }, this);

            // and the internal move operation influences the external move operation (using the original local move operation)
            extAllPos.forEach((extMovePos, index) => {
                const posOp = { name: Op.POSITION, start: extMovePos };
                this.handleDeleteExtPosition({ name: Op.DELETE, start: json.deepClone(origLocalOp.start) }, posOp);
                if (!isOperationRemoved(posOp)) {
                    this.handleInsertCharExtPosition({ name: Op.INSERT_DRAWING, start: json.deepClone(origLocalOp.to) }, posOp);
                }
                // TODO: what happens with deleted position (set properties in extOp to undefined?)
                // TODO: extMovePos has been modified in-place, no need to assign back to extOp
                if (index === 0) { extOp.start = extMovePos; }
                if (index === 1) { extOp.end = extMovePos; }
                if (index === 2) { extOp.to = extMovePos; }
            }, this);
        }
    }

    /**
     * Handling of two insertHeaderFooter operations.
     * They do only conflict, if the same type of header and footer was inserted. And in this case the operations can simply be ignored.
     *
     * insertHeaderFooter: { id: "OX_rId100", type: "header_default" }
     * After deleting and inserting again:
     * insertHeaderFooter: { id: "OX_rId101", type: "header_default" }
     *
     * Same type, but different id is possible.
     */
    handleInsertHeaderInsertHeader(localOp, extOp) {

        // a new operation that might be required
        let newOperation = null;
        // the container for new external operations executed before the current external operation
        const externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        const externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        let localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        const localOpsAfter = null;

        if (localOp.type === extOp.type) {

            if (localOp.id === extOp.id) {
                // same ID and same type
                setOperationRemoved(localOp); // setting marker at internal operation
                setOperationRemoved(extOp); // setting marker at external operation
            } else {
                // same type, but different ID
                newOperation = json.deepClone(extOp);
                newOperation.name = Op.DELETE_HEADER_FOOTER;
                delete newOperation.type;

                localOpsBefore = [newOperation]; // inserting delete operation that will be sent to the server

                setOperationRemoved(extOp); // setting marker at external operation
            }
        } else if (localOp.id === extOp.id) {
            // same ID but different type
            newOperation = json.deepClone(extOp);
            newOperation.name = Op.DELETE_HEADER_FOOTER;
            delete newOperation.type;

            localOpsBefore = [newOperation]; // inserting delete operation that will be sent to the server

            setOperationRemoved(extOp); // setting marker at external operation
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling two operations of type insertListStyle.
     *
     * { name: 'insertListStyle', opl: 1, osn: 1, listStyleId: 'L1', listDefinition: { ... } }
     *
     * These two operations influence each other only, if they have the listStyleId.
     */
    handleInsertListStyleInsertListStyle(localOp, extOp) {

        if (localOp.listStyleId === extOp.listStyleId) {
            // not applying external operation (this must be handled differently on server side)
            setOperationRemoved(extOp);
        }
    }

    /**
     * Handling two operations of type deleteListStyle.
     *
     * { name: 'deleteListStyle', opl: 1, osn: 1, listStyleId: 'L1' }
     *
     * These two operations influence each other only, if they have the same listStyleId.
     */
    handleDeleteListStyleDeleteListStyle(localOp, extOp) {

        if (localOp.listStyleId === extOp.listStyleId) {
            setOperationRemoved(localOp);
            setOperationRemoved(extOp);
        }
    }

    /**
     * Handling a deleteListStyle operation and an insertListStyleOperation.
     *
     * { name: 'deleteListStyle', opl: 1, osn: 1, listStyleId: 'L1' }
     * { name: 'insertListStyle', opl: 1, osn: 1, listStyleId: 'L1', listDefinition: { ... } }
     *
     * These two operations influence each other only, if they have the same listStyleId.
     */
    handleInsertListStyleDeleteListStyle(localOp, extOp) {

        if (localOp.listStyleId === extOp.listStyleId) {
            // removing the deleteListStyle operation to avoid superfluous problems, when
            // paragraphs get assigned a new listStyle in setAttributes operation after
            // the insertListStyle operation.
            if (localOp.name === Op.DELETE_LIST) {
                setOperationRemoved(localOp);
            } else {
                setOperationRemoved(extOp);
            }
        }
    }

    /**
     * Handling of move operation with an insertChar operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], to: [3, 1, 1, 2, 0] }
     */
    handleMoveInsertChar(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_CHAR_ALIAS) ? localOp : extOp;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the default length of the insertChar operation
        const insertCharLength = insertOp.start.length;
        // the length of the inserted text/char
        let length = 1;
        // the collector for the logical positions of the move operation
        const allPos = [moveOp.start, moveOp.end, moveOp.to];
        // the length of the start position of the move operation
        const moveStartLength = moveOp.start.length;
        // a clone of the move operation (required to calculate changes of insert operation)
        const origMoveOp = json.deepClone(moveOp);

        // insertChar influences the move operation
        allPos.forEach((movePos, index) => {
            if (insertCharLength <= movePos.length && equalPositions(insertOp.start, movePos, insertCharLength - 1)) {
                textIndex = insertCharLength - 1;
                if (insertOp.start[textIndex] <= movePos[textIndex]) { // insert shifts the move operation
                    if (insertOp.name === Op.TEXT_INSERT) { length = insertOp.text.length; }
                    movePos[textIndex] += length; // the start/end/to position of the move operation needs to be increased
                    if (index === 0) { moveOp.start = movePos; }
                    if (index === 1) { moveOp.end = movePos; }
                    if (index === 2) { moveOp.to = movePos; }
                } else {

                }
            }
        });

        // the move operation can also influence the insertChar operation
        if (insertCharLength > moveStartLength && equalPositions(origMoveOp.start, insertOp.start, moveStartLength)) { // check, if the element containing the selection is moved
            insertOp.start = origMoveOp.to.concat(insertOp.start.slice(moveStartLength)); // using 'move.to' instead of 'move.start' at current position
        } else {
            this.handleInsertCharDelete({ name: Op.DELETE, start: json.deepClone(origMoveOp.start) }, insertOp);
            if (insertOp.start) { this.handleInsertCharInsertChar({ name: Op.INSERT_DRAWING, start: json.deepClone(origMoveOp.to) }, insertOp); } // the 'to' position is the 'start' position for inserting element
            // -> Info: On server side the two operations for handleInsertCharInsertChar are in other order
            // This is relevant, if the 'to' position of the move operation is identical with the insert position of the insert operation
        }
    }

    /**
     * Handling of move operation with an insertRows/insertCells operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], end: [3, 1, 1, 2, 0] }
     */
    handleMoveInsertRows(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the insert operation
        const insertOp = (localOp.name === Op.ROWS_INSERT || localOp.name === Op.CELLS_INSERT) ? localOp : extOp;
        // the default length of the insertRows/insertCells operation
        const insertLength = insertOp.start.length;
        // the collector for the logical positions of the move operation
        const allPos = [moveOp.start, moveOp.end, moveOp.to];
        // the length of the start position of the move operation
        const moveStartLength = moveOp.start.length;
        // the index of the row/cell inside the logical position
        const rowIndex = insertLength - 1;
        // the number of inserted rows/cells
        const rowCount = insertOp.count || 1;
        // a clone of the move operation (required to calculate changes of insert operation)
        const origMoveOp = json.deepClone(moveOp);

        // insertRows/insertCells influences the move operation
        allPos.forEach((movePos, index) => {
            if (comparePositions(insertOp.start, movePos) <= 0) {
                // the table must be the parent for both positions to influence the external position
                if ((movePos.length >= insertLength && equalPositions(movePos, insertOp.start, insertLength - 1))) {
                    if (movePos[rowIndex] >= insertOp.start[rowIndex]) { // position inside a following row/cell
                        movePos[rowIndex] += rowCount;
                        if (index === 0) { moveOp.start = movePos; }
                        if (index === 1) { moveOp.end = movePos; }
                        if (index === 2) { moveOp.to = movePos; }
                    }
                }
            }
        });

        // the move operation can also influence the insertRows/insertCells operation
        if (insertLength > moveStartLength && equalPositions(origMoveOp.start, insertOp.start, moveStartLength)) { // check, if the element containing the selection is moved
            insertOp.start = origMoveOp.to.concat(insertOp.start.slice(moveStartLength)); // using 'move.to' instead of 'move.start' at current position
        } else {
            this.handleInsertRowsDelete({ name: Op.DELETE, start: json.deepClone(origMoveOp.start) }, insertOp);
            if (insertOp.start) { this.handleInsertRowsInsertChar({ name: Op.INSERT_DRAWING, start: json.deepClone(origMoveOp.to) }, insertOp); } // the 'to' position is the 'start' position for inserting element
        }
    }

    /**
     * Handling of move operation with an insertColumn operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], end: [3, 1, 1, 2, 0] }
     * { name: insertColumn, start: [2], tableGrid: [374,374,374], gridPosition: 1, insertMode: 'before' }
     */
    handleMoveInsertColumn(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the insert operation
        const insertOp = (localOp.name === Op.COLUMN_INSERT) ? localOp : extOp;
        // the default length of the insertRows/insertCells operation
        const insertLength = insertOp.start.length;
        // the collector for the logical positions of the move operation
        const allPos = [moveOp.start, moveOp.end, moveOp.to];
        // the length of the start position of the move operation
        const moveStartLength = moveOp.start.length;
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the column position inside the logical position
        let columnIndex = 0;
        // the specification, where the column is inserted
        const mode = insertOp.insertMode || 'behind';
        // a clone of the move operation (required to calculate changes of insert operation)
        const origMoveOp = json.deepClone(moveOp);

        // insertRows/insertCells influences the move operation
        allPos.forEach((movePos, index) => {
            if (comparePositions(insertOp.start, movePos) <= 0) {
                if ((movePos.length > insertLength + 1 && equalPositions(movePos, insertOp.start, insertLength))) {
                    tableIndex = insertLength - 1;
                    columnIndex = tableIndex + 2;
                    if ((movePos[columnIndex] > insertOp.gridPosition) || (mode === 'before' && movePos[columnIndex] === insertOp.gridPosition)) {
                        movePos[columnIndex]++;
                        if (index === 0) { moveOp.start = movePos; }
                        if (index === 1) { moveOp.end = movePos; }
                        if (index === 2) { moveOp.to = movePos; }
                    }
                }
            }
        });

        // the move operation can also influence the insertRows/insertCells operation
        if (insertLength > moveStartLength && equalPositions(origMoveOp.start, insertOp.start, moveStartLength)) { // check, if the element containing the selection is moved
            insertOp.start = origMoveOp.to.concat(insertOp.start.slice(moveStartLength)); // using 'move.to' instead of 'move.start' at current position
        } else {
            this.handleInsertColumnDelete({ name: Op.DELETE, start: json.deepClone(origMoveOp.start) }, insertOp);
            if (insertOp.start) { this.handleInsertColumnInsertChar({ name: Op.INSERT_DRAWING, start: json.deepClone(origMoveOp.to) }, insertOp); } // the 'to' position is the 'start' position for inserting element
        }
    }

    /**
     * Handling of move operation with a deleteColumns operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], end: [3, 1, 1, 2, 0] }
     * { name: deleteColumns, start: [2], startGrid: 2, endGrid: 3 }
     */
    handleMoveDeleteColumns(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the insert operation
        const deleteOp = (localOp.name === Op.COLUMNS_DELETE) ? localOp : extOp;
        // the default length of the insertRows/insertCells operation
        const deleteLength = deleteOp.start.length;
        // the collector for the logical positions of the move operation
        const allPos = [moveOp.start, moveOp.end, moveOp.to];
        // the length of the start position of the move operation
        const moveStartLength = moveOp.start.length;
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the column position inside the logical position
        let columnIndex = 0;
        // the number of deleted columns
        const deleteColumns = deleteOp.endGrid - deleteOp.startGrid + 1;
        // a clone of the move operation (required to calculate changes of insert operation)
        const origMoveOp = json.deepClone(moveOp);
        // whether the source of the move is removed by the delete operation
        let moveStartDeleted = false;
        // whether the destination of the move is removed by the delete operation
        let moveEndDeleted = false;
        // a new operation that might be required
        let newOperation = null;
        // the container for new external operations executed before the current external operation
        const externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        let externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        const localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        let localOpsAfter = null;

        // deleteColumns influence the move operation
        allPos.forEach((movePos, index) => {
            if (comparePositions(deleteOp.start, movePos) <= 0) {
                if ((movePos.length > deleteLength + 1 && equalPositions(movePos, deleteOp.start, deleteLength))) {
                    tableIndex = deleteLength - 1;
                    columnIndex = tableIndex + 2;
                    if (movePos[columnIndex] >= deleteOp.startGrid) {
                        if (movePos[columnIndex] > deleteOp.endGrid) {
                            movePos[columnIndex] -= deleteColumns;
                            if (index === 0) { moveOp.start = movePos; }
                            if (index === 1) { moveOp.end = movePos; }
                            if (index === 2) { moveOp.to = movePos; }
                        } else {
                            // generating a delete operation for the target of the move operation (if this is not also removed)
                            if (index === 2) {
                                moveEndDeleted = true;
                            } else {
                                moveStartDeleted = true;
                            }
                        }
                    }
                }
            }
        });

        if (moveStartDeleted && moveEndDeleted) {
            // the move operation cannot be applied, because its column is removed
            setOperationRemoved(moveOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
        } else if (moveStartDeleted || moveEndDeleted) {
            setOperationRemoved(moveOp); // moveOp cannot be executed anymore and will be replaced by a delete operation

            // extern: move [3, 0, 3, 1, 0] to [2, 0], local: deleteColumn [3], startGrid: 3
            // sending additional delete at [2, 0] to the server, locally is the element at [3, 0, 3, 1, 0] already removed
            newOperation = json.deepClone(deleteOp);
            newOperation.name = Op.DELETE;
            delete newOperation.startGrid;
            delete newOperation.endGrid;
            newOperation.start = moveStartDeleted ? json.deepClone(moveOp.to) : json.deepClone(moveOp.start);
            newOperation.end = moveStartDeleted ? json.deepClone(moveOp.to) : json.deepClone(moveOp.start);
            if ((moveOp === extOp && moveStartDeleted) || (moveOp === localOp && moveEndDeleted)) {
                localOpsAfter = [newOperation]; // inserting delete operation that will be sent to the server (and ignore move operation)
            } else if ((moveOp === localOp && moveStartDeleted) || (moveOp === extOp && moveEndDeleted)) {
                externalOpsAfter = [newOperation]; // inserting delete operation that need to be executed locally (and ignore move operation)
            }
        }

        if (!isOperationRemoved(moveOp)) {
            // the move operation can also influence the deleteColumns operation
            if (deleteLength > moveStartLength && equalPositions(origMoveOp.start, deleteOp.start, moveStartLength)) { // check, if the element containing the selection is moved
                deleteOp.start = origMoveOp.to.concat(deleteOp.start.slice(moveStartLength)); // using 'move.to' instead of 'move.start' at current position
            } else {
                this.handleDeleteColumnsDelete({ name: Op.DELETE, start: json.deepClone(origMoveOp.start) }, deleteOp);
                if (deleteOp.start) { this.handleDeleteColumnsInsertChar({ name: Op.INSERT_DRAWING, start: json.deepClone(origMoveOp.to) }, deleteOp); } // the 'to' position is the 'start' position for inserting element
            }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling of move operation with a delete operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], end: [3, 1, 1, 2, 0] }
     */
    handleMoveDelete(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the insert operation
        const deleteOp = (localOp.name === Op.DELETE) ? localOp : extOp;
        // the collector for the logical positions of the move operation
        const allPos = [moveOp.start, moveOp.end, moveOp.to];
        // the length of the start position of the move operation
        const moveStartLength = moveOp.start.length;
        // a clone of the move operation (required to calculate changes of insert operation)
        const origMoveOp = json.deepClone(moveOp);
        // whether the source of the move is removed by the delete operation
        let moveStartDeleted = false;
        // whether the destination of the move is removed by the delete operation
        let moveEndDeleted = false;
        // a new operation that might be required
        let newOperation = null;
        // a new operation that might be required
        let moveToPositionHandled = false;
        // whether the delete operation has a range
        const deleteHasRange = deleteOp.start && deleteOp.end && !equalPositions(deleteOp.start, deleteOp.end);
        // the container for new external operations executed before the current external operation
        const externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        let externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        const localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        let localOpsAfter = null;

        // the move operation might influence the delete operation
        const modifyDeleteOperation = () => {
            const deleteLength = deleteOp.start.length;
            if (deleteLength > moveStartLength && equalPositions(origMoveOp.start, deleteOp.start, moveStartLength)) { // check, if the element containing the selection is moved
                deleteOp.start = origMoveOp.to.concat(deleteOp.start.slice(moveStartLength)); // using 'move.to' instead of 'move.start' at current position
                if (deleteOp.end) { deleteOp.end = origMoveOp.to.concat(deleteOp.end.slice(moveStartLength)); }
                moveToPositionHandled = true;
            } else {
                this.handleDeleteDelete({ name: Op.DELETE, start: json.deepClone(origMoveOp.start) }, deleteOp);
                if (deleteOp.start) { this.handleInsertCharDelete({ name: Op.INSERT_DRAWING, start: json.deepClone(origMoveOp.to) }, deleteOp); } // the 'to' position is the 'start' position for inserting element
            }
        };

        allPos.forEach((movePos, index) => {
            // Case 1: position is before the start position (or equal to the start position) -> moveOp shifts both delete positions.
            // Case 2: position is between start and end position -> the moveOp needs to be removed (and maybe replaced by a delete operation).
            // Case 3: position is after the end position -> delete shifts the moveOp position.

            // whether the moved element is explicitely deleted (the start position is identical with the delete position)
            const deletedElement = index < 2 && !deleteHasRange && equalPositions(deleteOp.start, movePos);

            if (comparePositions(movePos, deleteOp.start) < 0 || (!deletedElement && equalPositions(movePos, deleteOp.start))) {
                // 1. the specified move position is before the delete start position (or it is the delete start position in a delete range)
                //    -> not calling this function twice for moveOp.start and moveOp.end because modifyDeleteOperation handles the complete operation
                if (index !== 1 && !moveToPositionHandled) { modifyDeleteOperation(); }
            } else if (deletedElement || ancestorRemoved(deleteOp, { start: movePos }) || (deleteOp.end && comparePositions(movePos, deleteOp.end) <= 0)) {
                // 2. the move position is inside the deletion range
                if (deleteOp.end && movePos.length === deleteOp.end.length) {
                    if (index === 2) { deleteOp.end[deleteOp.end.length - 1]++; } // increasing last position, if drawing was moved into range
                    if (index === 0) { deleteOp.end[deleteOp.end.length - 1]--; } // decreasing last position, if drawing was moved out of range range
                }
                setOperationRemoved(moveOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                // generating a delete operation for the target of the move operation (if this is not also removed)
                if (index === 2) {
                    moveEndDeleted = true;
                } else {
                    moveStartDeleted = true;
                }
            } else {
                // 3. the move position is behind the delete end position -> its position must be modified.
                const endPosition = deleteOp.end ? deleteOp.end : deleteOp.start;
                const deleteArray = calculateDeleteArray(deleteOp.start, endPosition, movePos);
                // calculating the new values for the external position
                for (let idx = 0; idx < movePos.length; idx += 1) { movePos[idx] -= deleteArray[idx]; }
            }
        }, this);

        if (moveStartDeleted && moveEndDeleted) {
            // the move operation cannot be applied, because its column is removed
            setOperationRemoved(moveOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
        } else if (moveStartDeleted || moveEndDeleted) {
            setOperationRemoved(moveOp); // moveOp cannot be executed anymore and will be replaced by a delete operation

            // extern: move [3, 0, 3, 1, 0] to [2, 0], local: deleteColumn [3], startGrid: 3
            // sending additional delete at [2, 0] to the server, locally is the element at [3, 0, 3, 1, 0] already removed
            newOperation = json.deepClone(deleteOp);
            newOperation.name = Op.DELETE;
            newOperation.start = moveStartDeleted ? json.deepClone(moveOp.to) : json.deepClone(moveOp.start);
            newOperation.end = moveStartDeleted ? json.deepClone(moveOp.to) : json.deepClone(moveOp.start);

            // the deleteOp can be ignored, too, if only the moved drawing was deleted -> there will be a new delete operation at the modified position
            if (moveStartDeleted && !deleteHasRange && equalPositions(deleteOp.start, moveOp.start)) { setOperationRemoved(deleteOp); }

            if ((moveOp === extOp && moveStartDeleted) || (moveOp === localOp && moveEndDeleted)) {
                localOpsAfter = [newOperation]; // inserting delete operation that will be sent to the server (and ignore move operation)
            } else if ((moveOp === localOp && moveStartDeleted) || (moveOp === extOp && moveEndDeleted)) {
                externalOpsAfter = [newOperation]; // inserting delete operation that need to be executed locally (and ignore move operation)
            }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling of move operation with an insertParagraph/insertTable operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], end: [3, 1, 1, 2, 0] }
     */
    handleMoveInsertComp(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_COMP_ALIAS) ? localOp : extOp;
        // the default length of the insert operation
        const insertLength = insertOp.start.length;
        // the collector for the logical positions of the move operation
        const allPos = [moveOp.start, moveOp.end, moveOp.to];
        // the length of the start position of the move operation
        const moveStartLength = moveOp.start.length;
        // the index of the paragraph/table inside the logical position
        const paraIndex = insertLength - 1;
        // a clone of the move operation (required to calculate changes of insert operation)
        const origMoveOp = json.deepClone(moveOp);

        // insertChar influences the move operation
        allPos.forEach((movePos, index) => {
            if (comparePositions(insertOp.start, movePos) <= 0) {
                // the table must be the parent for both positions to influence the external position
                if ((movePos.length >= insertLength && equalPositions(movePos, insertOp.start, insertLength - 1))) {
                    if (movePos[paraIndex] >= insertOp.start[paraIndex]) { // position inside a following paragraph/table
                        movePos[paraIndex]++;
                        if (index === 0) { moveOp.start = movePos; }
                        if (index === 1) { moveOp.end = movePos; }
                        if (index === 2) { moveOp.to = movePos; }
                    }
                }
            }
        });

        // the move operation can also influence the insert operation
        if (insertLength > moveStartLength && equalPositions(origMoveOp.start, insertOp.start, moveStartLength)) { // check, if the element containing the selection is moved
            insertOp.start = origMoveOp.to.concat(insertOp.start.slice(moveStartLength)); // using 'move.to' instead of 'move.start' at current position
        } else {
            this.handleInsertCompDelete({ name: Op.DELETE, start: json.deepClone(origMoveOp.start) }, insertOp);
            if (insertOp.start) { this.handleInsertCompInsertChar({ name: Op.INSERT_DRAWING, start: json.deepClone(origMoveOp.to) }, insertOp); } // the 'to' position is the 'start' position for inserting element
        }
    }

    /**
     * Handling of move operation with a mergeParagraph/mergeTable operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], end: [3, 1, 1, 2, 0] }
     */
    handleMergeCompMove(localOp, extOp) {

        // the insert operation
        const mergeOp = this.hasAliasName(localOp, OT_MERGE_COMP_ALIAS) ? localOp : extOp;
        // the move operation
        const moveOp = (mergeOp === localOp) ? extOp : localOp;
        // the default length of the merge operation
        const mergeLength = mergeOp.start.length;
        // the collector for the logical positions of the move operation
        const allPos = [moveOp.start, moveOp.end, moveOp.to];
        // the length of the start position of the move operation
        const moveStartLength = moveOp.start.length;
        // the index of the paragraph/table inside the logical position
        let paraIndex = 0;
        // the index of the text/row inside the logical position
        let textIndex = 0;
        // whether this is a merge of a paragraph (and not a table)
        const isParaMerge = mergeOp.name === Op.PARA_MERGE;
        // the name of the property containing the length of the leading paragraph/table
        const lengthProperty = isParaMerge ? 'paralength' : 'rowcount';
        // a clone of the move operation (required to calculate changes of merge operation)
        const origMoveOp = json.deepClone(moveOp);
        // a result object from the handler with additional required operations
        let result;

        // merge influences the move operation
        allPos.forEach((movePos, index) => {
            if (comparePositions(mergeOp.start, movePos) <= 0) {
                if ((movePos.length >= mergeLength && equalPositions(movePos, mergeOp.start, mergeLength - 1))) {
                    paraIndex = mergeOp.start.length - 1;
                    textIndex = paraIndex + 1;
                    if (mergeOp.start[paraIndex] < (movePos[paraIndex] - 1)) {
                        movePos[paraIndex]--; // decreasing position of move operation
                        if (index === 0) { moveOp.start = movePos; }
                        if (index === 1) { moveOp.end = movePos; }
                        if (index === 2) { moveOp.to = movePos; }
                    } else if (mergeOp.start[paraIndex] === (movePos[paraIndex] - 1)) {
                        movePos[paraIndex]--;
                        movePos[textIndex] += (mergeOp[lengthProperty] ? mergeOp[lengthProperty] : 0);
                        if (index === 0) { moveOp.start = movePos; }
                        if (index === 1) { moveOp.end = movePos; }
                        if (index === 2) { moveOp.to = movePos; }
                    }
                }
            }
        });

        // the move operation can also influence the merge operation (using the original move operation)
        if (mergeLength > moveStartLength && equalPositions(origMoveOp.start, mergeOp.start, moveStartLength)) { // check, if the element containing the selection is moved
            mergeOp.start = origMoveOp.to.concat(mergeOp.start.slice(moveStartLength)); // using 'move.to' instead of 'move.start' at current position
        } else {
            result = this.handleMergeCompDelete({ name: Op.DELETE, start: json.deepClone(origMoveOp.start) }, mergeOp);
            if (mergeOp.start) { this.handleMergeCompInsertChar({ name: Op.INSERT_DRAWING, start: json.deepClone(origMoveOp.to) }, mergeOp); } // the 'to' position is the 'start' position for inserting element
        }

        return result;
    }

    /**
     * Handling of move operation with a splitParagraph/splitTable operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], end: [3, 1, 1, 2, 0] }
     */
    handleSplitCompMove(localOp, extOp) {

        // the insert operation
        const splitOp = this.hasAliasName(localOp, OT_SPLIT_COMP_ALIAS) ? localOp : extOp;
        // the move operation
        const moveOp = (splitOp === localOp) ? extOp : localOp;
        // the default length of the split operation
        const splitLength = splitOp.start.length;
        // the collector for the logical positions of the move operation
        const allPos = [moveOp.start, moveOp.end, moveOp.to];
        // the length of the start position of the move operation
        const moveStartLength = moveOp.start.length;
        // the index of the paragraph/table inside the logical position
        let paraIndex = 0;
        // the index of the text/row inside the logical position
        let textIndex = 0;
        // whether this is a merge of a paragraph (and not a table)
        const isParaSplit = splitOp.name === Op.PARA_SPLIT;
        // a clone of the move operation (required to calculate changes of merge operation)
        const origMoveOp = json.deepClone(moveOp);

        // split influences the move operation
        allPos.forEach((movePos, index) => {
            if (comparePositions(splitOp.start, movePos) <= 0) {
                if ((movePos.length >= splitLength && equalPositions(movePos, splitOp.start, splitLength - 2))) {
                    textIndex = splitLength - 1;
                    paraIndex = textIndex - 1;
                    if (splitOp.start[paraIndex] < movePos[paraIndex]) {
                        movePos[paraIndex]++; // increasing position of insert operation
                        if (index === 0) { moveOp.start = movePos; }
                        if (index === 1) { moveOp.end = movePos; }
                        if (index === 2) { moveOp.to = movePos; }
                    } else if (splitOp.start[paraIndex] === movePos[paraIndex]) {
                        if (splitOp.start[textIndex] <= movePos[textIndex]) {
                            movePos[paraIndex]++;
                            movePos[textIndex] -= splitOp.start[textIndex];
                            if (index === 0) { moveOp.start = movePos; }
                            if (index === 1) { moveOp.end = movePos; }
                            if (index === 2) { moveOp.to = movePos; }
                        }
                    }
                }
            }
        });

        // the move operation can also influence the split operation (using the original move operation)
        if (splitLength > moveStartLength && equalPositions(origMoveOp.start, splitOp.start, moveStartLength)) { // check, if the element containing the selection is moved
            splitOp.start = origMoveOp.to.concat(splitOp.start.slice(moveStartLength)); // using 'move.to' instead of 'move.start' at current position
        } else {
            this.handleSplitCompDelete({ name: Op.DELETE, start: json.deepClone(origMoveOp.start) }, splitOp);
            if (splitOp.start) {
                if (isParaSplit) {
                    this.handleSplitParaInsertChar({ name: Op.INSERT_DRAWING, start: json.deepClone(origMoveOp.to) }, splitOp);
                } else {
                    this.handleSplitTableInsertChar({ name: Op.INSERT_DRAWING, start: json.deepClone(origMoveOp.to) }, splitOp);
                }
            }
        }
    }

    /**
     * Handling of move operation with a setAttributes operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], end: [3, 1, 1, 2, 0] }
     */
    handleSetAttrsMove(localOp, extOp) {

        // the move operation
        const moveOp = localOp.name === Op.MOVE ? localOp : extOp;
        // the setAttributes operation
        const attrsOp = moveOp === extOp ? localOp : extOp;
        // the collector for the logical positions of the attributes operation
        const allPos = attrsOp.end ? [attrsOp.start, attrsOp.end] : [attrsOp.start];
        // the length of the start position of the move operation
        const moveStartLength = moveOp.start.length;
        // whether the move operation moves the element that contains the attributes
        const movedAttributes = attrsOp.start.length >= moveStartLength && equalPositions(moveOp.start, attrsOp.start, moveStartLength);

        // TODO: Is it possible, that only the start or the end position of the setAttributes operation is moved with the drawing?
        // TODO: Is it important (and requires a new operation) if a drawing is moved away from an attributed range?
        //       -> Example: If a paragraph gets attribute 'centered', the paragraphs in a text frame in this paragraph do not get
        //                   the attribute centered. Therefore an additional setAttributes for a drawing that was moved away from
        //                   this paragraph would not be required. But are there other examples?

        // the move operation can influence the setAttributes operation, not vice versa
        if (movedAttributes) {
            allPos.forEach((attrsPos, index) => {
                const property = index === 0 ? 'start' : 'end';
                attrsOp[property] = moveOp.to.concat(attrsPos.slice(moveStartLength)); // using 'move.to' instead of 'move.start' at current position
            });
        } else {
            this.handleSetAttrsDelete({ name: Op.DELETE, start: json.deepClone(moveOp.start) }, attrsOp);
            if (attrsOp.start) { this.handleSetAttrsInsertChar({ name: Op.INSERT_DRAWING, start: json.deepClone(moveOp.to) }, attrsOp); }
        }
    }

    /**
     * Handling two operations of type updateComplexField.
     *
     * { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }
     *
     * These two operations influence each other only, if they have the same start position.
     */
    handleUpdateComplexFieldUpdateComplexField(localOp, extOp) {

        if (equalPositions(localOp.start, extOp.start)) {

            if (localOp.instruction === extOp.instruction) {
                // if the instructions are the same, both operations can be ignored
                setOperationRemoved(extOp);
                setOperationRemoved(localOp);
            } else {
                // different instructions: not applying external operation (this must be handled differently on server side)
                setOperationRemoved(extOp);
            }
        }
    }

    /**
     * Handling two operations of type changeComment.
     *
     * { name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Modified text' }
     *
     * These two operations influence each other only, if they have the same start position.
     */
    handleChangeCommentChangeComment(localOp, extOp) {

        if (equalPositions(localOp.start, extOp.start)) {
            // not applying external operation (this must be handled differently on server side)
            setOperationRemoved(extOp);
        }
    }

}
