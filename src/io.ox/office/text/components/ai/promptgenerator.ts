/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary } from "@/io.ox/office/tk/algorithms";
import type { Action, TextLengthProperties, TextType, TextTone, TextMood, TranslationLanguage, TranslationFromLanguage, CreateLanguage } from "@/io.ox/office/text/components/ai/aiservice";

// types ======================================================================

/**
 * The length specifications for one document type.
 */
type OneDocTypeLengths = Record<TextLengthProperties, number>;

/**
 * The length specification for all supported document types.
 */
type AllDocTypeLengths = Record<TextType, OneDocTypeLengths>;

/**
 * The options to create the prompt that are selected by the user in the AI dialog.
 */
interface PromptOptions {

    /**
     * The action selected by the user in the AI dialog.
     */
    action: Action;

    /**
     * The document type selected by the user in the AI dialog.
     */
    type: TextType;

    /**
     * The tone selected by the user in the AI dialog.
     */
    tone: TextTone;

    /**
     * The mood selected by the user in the AI dialog.
     */
    mood: TextMood;

    /**
     * The text length selected by the user in the AI dialog.
     */
    length: TextLengthProperties;

    /**
     * The language for new created content selected by the user in the AI dialog.
     */
    createlanguage: CreateLanguage | "";

    /**
     * The language to translate "from" selected by the user in the AI dialog.
     */
    fromlanguage: TranslationFromLanguage | "";

    /**
     * The language to translate "to" selected by the user in the AI dialog.
     */
    translationlanguage: TranslationLanguage | "";

    /**
     * Whether the current selection is inside a single paragraph.
     */
    isSingleParagraphSelection?: boolean;
}

// constants ==================================================================

/**
 * The length specifications for all document types.
 */
const DOCTYPE_LENGTH: AllDocTypeLengths = {
    report: { short: 300, medium: 1000, long: 1000 },
    story: { short: 300, medium: 1000, long: 1000 },
    article: { short: 300, medium: 800, long: 800 },
    essay: { short: 200, medium: 500, long: 500 },
    research_paper: { short: 300, medium: 800, long: 800 },
    memo: { short: 100, medium: 200, long: 200 },
    letter: { short: 300, medium: 600, long: 600 },
    presentation: { short: 300, medium: 600, long: 600 },
    resume: { short: 300, medium: 600, long: 600 },
    instruction: { short: 300, medium: 600, long: 600 },
    invoice: { short: 200, medium: 500, long: 500 },
    contract: { short: 300, medium: 600, long: 600 },
    project_plan: { short: 300, medium: 600, long: 600 },
    whitepaper: { short: 300, medium: 600, long: 600 },
    newsletter: { short: 300, medium: 600, long: 600 },
    travel_report: { short: 300, medium: 1000, long: 1000 },
};

// public functions ===========================================================

/**
 * Generates one or more prompts for the AI request(s) in OX Text.
 *
 * @param userInput
 *  The content of the input field provided by the user or in the case
 *  of translations, a stringified JSON object or an array of stringified
 *  JSON objects, that contain the text to be translated.
 *
 * @param options
 *  The options selected by the user in the AI dialog.
 *
 * @returns
 *  The prompt (or the array of prompts) that can be used for the query
 *  to the AI service.
 */
export function generateOXTextPrompt(userInput: string | string[], options: PromptOptions): string | string[] {

    const switchAction = options.action;
    const allPrompts: string[] = [];

    ary.wrap(userInput).forEach(input => {

        if (!input?.length) {
            return;
        }

        let prompt;

        switch (switchAction) {
            case "write": {
                prompt = `Write a ${options.type} based on the following text fragments ${evaluateCreateLanguage(options)}: "${input}". ${evaluateOptions(options)}`;
                break;
            }
            case "rephrase": {
                prompt = `Rephrase the following text ${evaluateCreateLanguage(options)}: "${input}". ${evaluateOptions(options)}`;
                break;
            }
            case "summarize": {
                prompt = `Summarize the following text ${evaluateCreateLanguage(options)}: "${input}"`;
                break;
            }
            case "translate": {
                if (options?.isSingleParagraphSelection) {
                    prompt = `Translate the text "${input}" ${evaluateFromLanguage(options)} to the language ${options.translationlanguage}. If you cannot translate the text return it and do not add any comment to the translation.`;
                } else {
                    prompt = `Translate the text in the values of the JSON object ${input} ${evaluateFromLanguage(options)} to the language ${options.translationlanguage}. If you cannot translate the text do not modify the value and do not add any comment to the translation. If you did not make any change, only return the original JSON object.`;
                }
                break;
            }
        }

        allPrompts.push(prompt);
    });

    return (allPrompts.length > 1) ? allPrompts : allPrompts[0];
}

// private functions ==========================================================

/**
 * Specify the length for the generated content.
 */
function evaluateLength(documentType: TextType, userSpecifiedLength: TextLengthProperties): string {

    const textLengths = DOCTYPE_LENGTH[documentType];
    const length = textLengths[userSpecifiedLength]; // the value for long is not evaluated currently
    let promptLengthText = "";

    switch (userSpecifiedLength) {

        case "short":
        case "medium": {
            promptLengthText = ` a length of approximately ${length} words`;
            break;
        }
        case "long": {
            promptLengthText = ` make a long and comprehensive text with at least ${length} words`;
            break;
        }

    }

    return promptLengthText;
}

/**
 * Setting the language of the content that shall be translated (the "fromLanguage"). If not explicitely specified by
 * the user, "Auto" is used, so that the AI detects the language of the user input.
 */
function evaluateFromLanguage(options: PromptOptions): string {
    return options?.fromlanguage !== "Auto" ? `from the language ${options.fromlanguage}` : "";
}

/**
 * Setting the language of the content that shall be generated ("create", "rephrase" or "summarize"). If not explicitely
 * specified by the user, "Auto" is used, so that the AI uses the language of the user input.
 */
function evaluateCreateLanguage(options: PromptOptions): string {
    return options?.createlanguage !== "Auto" ? `in the language ${options.createlanguage}` : "in the same language";
}

/**
 * Get the prompt part for the options.
 */
function evaluateOptions(options: PromptOptions): string {
    return `Use a ${options.tone} tone, a ${options.mood} mood and ${evaluateLength(options.type, options.length)}.`;
}
