/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ox from "$/ox";
import yell from "$/io.ox/core/yell";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import type { OperationGenerator } from "@/io.ox/office/editframework/model/operation/generator";
import type { Position } from "@/io.ox/office/editframework/utils/operations";
import type { CharacterAttributes } from "@/io.ox/office/editframework/utils/attributeutils";

import type { SelectedParagraphsInfo } from "@/io.ox/office/textframework/selection/selection";
import { isEmptyParagraph } from "@/io.ox/office/textframework/utils/dom";
import {  PARA_SPLIT, SET_ATTRIBUTES, TEXT_INSERT, type DeleteOperation, type InsertTextOperation, type SetAttributesOperation } from "@/io.ox/office/textframework/utils/operations";

import { appendNewIndex, decreaseLastIndex, getCharacterAtPosition, getParagraphElement, increaseLastIndex } from "@/io.ox/office/textframework/utils/position";
import Snapshot from "@/io.ox/office/textframework/utils/snapshot";
import type TextBaseModel from "@/io.ox/office/textframework/model/editor";

import type { AIDialogMode, AIResultType } from "@/io.ox/office/text/view/dialog/generatecontent";

import { ary, json } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import gt from "gettext";

// types ======================================================================

/**
 * The user specified length settings for the generated content.
 */
export type TextLengthProperties = "short" | "medium" | "long";

/**
 * The supported languages for the AI translation.
 */
export type TranslationLanguage = "English" | "Dutch" | "French" | "German" | "Italian" | "Portuguese" | "Spanish";

/**
 * The supported language codes for the AI translation.
 */
export type TranslationLanguageCode = "en-US" | "nl-NL" | "fr-FR" | "de-DE" | "it-IT" | "pt-BR" | "es-ES";

/**
 * The supported languages for the AI translation.
 */
export type LanguageCode = "en" | "nl" | "fr" | "de" | "it" | "pt" | "es";

/**
 * The supported language IDs for the created content of the AI (actions "write", "rephrase" or "summarize").
 */
export type CreateLanguageID = "" | TranslationLanguageCode;

/**
 * The supported "from" languages for the AI translation.
 */
export type TranslationFromLanguage = "Auto" | TranslationLanguage;

/**
 * The supported languages for the actions "write", "rephrase" or "summarize".
 */
export type CreateLanguage = "Auto" | TranslationLanguage;

/**
 * The supported actions.
 */
export type Action = "write" | "rephrase" | "summarize" | "translate";

/**
 * The supported document types.
 */
export type TextType = "report" | "story" | "article" | "essay" | "research_paper" | "memo" | "letter" | "presentation" | "resume" | "instruction" | "invoice" | "contract" | "project_plan" | "whitepaper" | "newsletter" | "travel_report";

/**
 * The supported tones for the generated content.
 */
export type TextTone = "formal" | "neutral" | "informal";

/**
 * The supported moods for the generated content.
 */
export type TextMood = "exciting" | "friendly" | "positive" | "neutral" | "rejecting" | "disappointed";

/**
 * The information object for the supported translation languages.
 */
interface TranslationInfo {
    value: `translate:${Lowercase<TranslationLanguage>}`;
    label: string;
    langId: TranslationLanguageCode;
    promptValue: TranslationLanguage;
}

/**
 * The information object for the supported translation languages.
 */
interface TranslationFromInfo {
    value: TranslationFromLanguage;
    label: string;
    promptValue: TranslationFromLanguage;
}

/**
 * The information object for the supported languages to "create", "rephrase"
 * or "summarize" content.
 */
interface CreateLanguageInfo {
    value: CreateLanguage;
    label: string;
    langId: CreateLanguageID;
    promptValue: CreateLanguage;
}

/**
 * The return type for the generated insertText operations.
 */
interface InsertOpsRT {

    /**
     * The generator containing all insertText and splitParagraph operations.
     */
    generator: OperationGenerator;

    /**
     * The start position of the inserted content.
     */
    startPos: Position;

    /**
     * The end position of the inserted content.
     */
    endPos: Position;
}

/**
 * The options for opening the AI dialog.
 */
interface OpenDialogOptions {

    /**
     * The mode, in which the AI dialog shall be opened.
     */
    mode: AIDialogMode;
}

// constants ==================================================================

/**
 * Label for automatic detection of the base language by the AI.
 */
//#. Short version inside a selection list for "Automatic detection" of the language
const AUTO_DETECTION_LABEL = gt("Auto");

const ENGLISH_LANGUAGE_NAMES: Record<LanguageCode, TranslationLanguage> = {
    en: "English",
    es: "Spanish",
    de: "German",
    fr: "French",
    it: "Italian",
    nl: "Dutch",
    pt: "Portuguese",
};

export const translateActions: TranslationInfo[] = [
    { value: "translate:english", label: gt("English"), langId: "en-US", promptValue: "English" },
    { value: "translate:dutch", label: gt("Dutch"), langId: "nl-NL", promptValue: "Dutch" },
    { value: "translate:french", label: gt("French"), langId: "fr-FR", promptValue: "French" },
    { value: "translate:german", label: gt("German"), langId: "de-DE", promptValue: "German" },
    { value: "translate:italian", label: gt("Italian"), langId: "it-IT", promptValue: "Italian" },
    { value: "translate:portuguese", label: gt("Portuguese"), langId: "pt-BR", promptValue: "Portuguese" },
    { value: "translate:spanish", label: gt("Spanish"), langId: "es-ES", promptValue: "Spanish" }
];

export const translateFromLanguages: TranslationFromInfo[] = [
    { value: "Auto", label: AUTO_DETECTION_LABEL, promptValue: "Auto" },
    { value: "English", label: gt("English"), promptValue: "English" },
    { value: "Dutch", label: gt("Dutch"), promptValue: "Dutch" },
    { value: "French", label: gt("French"), promptValue: "French" },
    { value: "German", label: gt("German"), promptValue: "German" },
    { value: "Italian", label: gt("Italian"), promptValue: "Italian" },
    { value: "Portuguese", label: gt("Portuguese"), promptValue: "Portuguese" },
    { value: "Spanish", label: gt("Spanish"), promptValue: "Spanish" }
];

export const createLanguages: CreateLanguageInfo[] = [
    { value: "Auto", label: AUTO_DETECTION_LABEL, langId: "", promptValue: "Auto" },
    { value: "English", label: gt("English"), langId: "en-US", promptValue: "English" },
    { value: "Dutch", label: gt("Dutch"), langId: "nl-NL", promptValue: "Dutch" },
    { value: "French", label: gt("French"), langId: "fr-FR", promptValue: "French" },
    { value: "German", label: gt("German"), langId: "de-DE", promptValue: "German" },
    { value: "Italian", label: gt("Italian"), langId: "it-IT", promptValue: "Italian" },
    { value: "Portuguese", label: gt("Portuguese"), langId: "pt-BR", promptValue: "Portuguese" },
    { value: "Spanish", label: gt("Spanish"), langId: "es-ES", promptValue: "Spanish" }
];

export const generateActions = [
    { value: "write", label: gt("Write about"), langId: "", promptValue: "" }
];

export const transformActions = [
    { value: "rephrase", label: gt("Rephrase"), langId: "", promptValue: "" },
    { value: "summarize", label: gt("Summarize"), langId: "", promptValue: "" }
];

/**
 * A regular expression that checks, whether the content of a paragraph
 * cannot be translated.
 */
const TRANSLATION_CHECK = /^\s*$/;

/**
 * The maximum length for a text to be translated within one prompt.
 */
const MAX_TEXT_LENGTH = 600;

/**
 * The maximum input length for the actions "write", "rephrase" and "summarize".
 * This must be limited, because the request contains only one single prompt.
 */
export const MAX_INPUT_LENGTH = 4000;

/**
 * The threshold of remaining characters, so that the user will be informed in
 * the GUI.
 */
export const USER_INPUT_AVAILABLE_LENGTH = 100;

// public functions ===========================================================

/**
 * Helper function to generate the response for requests with multiple
 * prompts (translations).
 *
 * @param responses
 *  The container with all responses created by the AI.
 *
 * @param selectedParagraphs
 *  The container with the info about all selected paragraphs.
 *
 * @returns
 *  The single response string generated from all translated and not
 *  translated parts to be shown in the dialog's response window.
 */
export function generateTranslationResponse(responses: string | string[], selectedParagraphs: Opt<SelectedParagraphsInfo[]>): string {

    if (!selectedParagraphs) { return ""; }

    const allParaStrings: string[] = [];
    const aiResponses = ary.wrap(responses);
    const allAIJSONResponses: Opt<Dict<string>> = {};

    if (selectedParagraphs.length === 1) {
        const key = generateTranslationKey(selectedParagraphs[0].position);
        allAIJSONResponses[key] = aiResponses[0];
    } else {
        aiResponses.forEach(oneResponse => {
            let aiJSONResponse: Dict<string>;
            try {
                aiJSONResponse = json.tryParse(oneResponse) as Dict<string>;
                if (aiJSONResponse) { for (const key in aiJSONResponse) { allAIJSONResponses[key] = aiJSONResponse[key]; } }
            } catch (err) {
                globalLogger.exception(err, "when evaluating the JSON response of the AI service.");
            }
        });
    }

    selectedParagraphs.forEach(paraInfo => {

        if (paraInfo.isTranslatable) {
            const key = generateTranslationKey(paraInfo.position);
            const translationExists = key in allAIJSONResponses;
            if (translationExists) {
                let singleAIResponse = allAIJSONResponses[key];
                // 1. step: Removing leading line breaks
                singleAIResponse = singleAIResponse?.replace(/^\n+/, "");
                // 2. step: Removing content after additional line breaks at end of string -> might be a comment. Example: '{ "content": "\n\n;\n\nNo translation needed; \";\" is a punctuation mark in English."}'
                // singleAIResponse = singleAIResponse?.replace(/\n+[^\n]+$/, "");
                // 3. There should be no further "\n". But replace them with " " to keep the document structure by not increasing the number of paragraphs
                singleAIResponse = singleAIResponse?.replaceAll("\n", " ");
                paraInfo.translation = singleAIResponse;
            } else {
                paraInfo.translation = paraInfo.selectedText; // should never happen
            }
        } else {
            paraInfo.translation = paraInfo.selectedText;
        }

        allParaStrings.push(paraInfo.translation);
    });

    return allParaStrings.join("\n");
}

/**
 * Collect all the paragraph content (as strings) that can be translated. This is for example not the case
 * for empty strings or strings that contain only whitespaces.
 *
 * @param selectedParagraphs
 *  The info object for the selected paragraphs. This is used for translation, that
 *  is done for each paragraph. In this scenario the user cannot modify the content
 *  in the AI dialog.
 *
 * @returns
 *  An array with all content strings of the current selection.
 */
export function generateTranslationInput(selectedParagraphs: Opt<SelectedParagraphsInfo[]>): string | string[] {
    const translatableStrings: string[] = [];
    selectedParagraphs?.forEach(paraInfo => { if (paraInfo.isTranslatable) { translatableStrings.push(paraInfo.selectedText); } });
    return translatableStrings;
}

/**
 * Generate a JSON object as string, that contains the paragraph contents, that
 * shall be translated, as values.
 *
 * @param selectedParagraphs
 *  The info object for the selected paragraphs. This is used for translation, that
 *  is done for each paragraph. In this scenario the user cannot modify the content
 *  in the AI dialog.
 *
 * @returns
 *  The generated JSON object as string or as array of strings.
 */
export function generateTranslationInputAsJSON(selectedParagraphs: Opt<SelectedParagraphsInfo[]>): string | string[] {

    const allJsonStrings = [];
    let length = 0;

    // short cut for single paragraph selections
    if (selectedParagraphs?.length === 1) {
        const paraInfo = selectedParagraphs[0];
        return paraInfo.isTranslatable ? paraInfo.selectedText.replaceAll("\"", "") : "";
    }

    let jsonString = "{";

    selectedParagraphs?.forEach((paraInfo, index) => {
        if (paraInfo.isTranslatable) {
            let isLast = selectedParagraphs.length === index + 1;
            const key = generateTranslationKey(paraInfo.position);
            const value = paraInfo.selectedText.replaceAll("\"", ""); // quotation marks disturb the JSON structure
            jsonString = `${jsonString}"${key}":"${value}"`;

            length += value.length;

            if (length > MAX_TEXT_LENGTH) {
                isLast = true;
                length = 0;
                allJsonStrings.push(`${jsonString}}`);
                jsonString = "{";
            }

            if (!isLast) { jsonString = `${jsonString},`; }
        }
    });

    jsonString = `${jsonString}}`;

    if (jsonString.length > 2) { allJsonStrings.push(jsonString); } // jsonString is longer than "{}"

    return allJsonStrings.length > 1 ? allJsonStrings : allJsonStrings[0];
}

// private functions ==========================================================

/**
 * Create a paragraph specific key for the JSON object for the translation.
 *
 * @param paragraphPosition
 *  The logical position of a paragraph.
 *
 * @returns
 *  A string that is used as key in the JSON object for the translation.
 */
function generateTranslationKey(paragraphPosition: Position): string {
    return `_para_${paragraphPosition}`;
}

// class AIService ============================================================

/**
 * An instance of this class represents the AI service in the edited document.
 *
 * @param {TextModel} docModel
 *  The text model instance.
 */
class AIService extends ModelObject<TextBaseModel> {

    // constructor(docModel) {
    //     // base constructor ------------------------------------------------
    //     super(docModel);
    // }

    /**
     * The last used base language for translation.
     */
    #lastFromLanguage: Opt<TranslationLanguage>;

    /**
     * The last used target language for translation.
     */
    #lastToLanguage: Opt<TranslationLanguage>;

    /**
     * The last used language of created content.
     */
    #lastCreateLanguage: Opt<TranslationLanguage>;

    /**
     * The last used document type.
     */
    #lastDocumentType: Opt<TextType>;

    // public methods ---------------------------------------------------------

    /**
     * Opens the modal dialog to generate AI content.
     *
     * @param [options]
     *  An optional dialog mode.
     *
     * @returns
     *  A promise, that fulfils, when the AI dialog is closed and the generated
     *  content is inserted into the document.
     */
    async openContentGeneratorDialog(options: OpenDialogOptions = { mode: "write" }): Promise<void> {

        const { selectedText, selectedParagraphs, allLanguagesInSelection } = this.docModel.getSelection().getStructuredSelectedText();
        let defaultTranslationLang: TranslationLanguage = "English";
        if (options?.mode === "translate") {
            this.#checkTranslatableParagraphs(selectedParagraphs);
            const uiLanguage = ox.language;
            const docLanguage = this.docModel.defAttrPool.getDefaultValue("character", "language");
            defaultTranslationLang = this.#lastToLanguage || this.#getDefaultTranslationLanguage(uiLanguage, docLanguage, allLanguagesInSelection);
        }
        const { getConsent } = await import("$/pe/openai/consent");
        if (!(await getConsent())) { return; }
        const appWindow = this.docApp.getWindow();
        appWindow?.busy();
        try {
            const { showModalAIContentDialog } = await import("@/io.ox/office/text/view/dialog/generatecontent");
            const result = await showModalAIContentDialog({ mode: options.mode, input: selectedText, selectedParagraphs, defaultTranslationLang, type: this.#lastDocumentType, fromLang: this.#lastFromLanguage, createLang: this.#lastCreateLanguage });
            if (result) {
                this.#lastDocumentType = result.type;
                this.#lastFromLanguage = result.isTranslation ? result.fromLanguage : this.#lastFromLanguage;
                this.#lastToLanguage = result.isTranslation ? this.#getEnglishLanguageNameFromIsoCode(result.languageId) : this.#lastToLanguage;
                this.#lastCreateLanguage = result.isTranslation ? this.#lastCreateLanguage : this.#getEnglishLanguageNameFromIsoCode(result.languageId);
                await this.#insertAIContentIntoDocument(result, selectedParagraphs);
            }
        } catch (error) {
            if (error !== "abort") { yell("error", this.#getErrorMessage(error as Error)); }
        } finally {
            appWindow?.idle();
        }
    }

    // private methods ----------------------------------------------------

    /**
     * Generated an error message shown to the user, when the AI request fails.
     *
     * @param error
     *  An error thrown when the modal AI content dialog is open.
     *
     * @returns
     *  The error message shown to the user, when the AI request fails.
     */
    #getErrorMessage(error: Error): string {
        if (error && error.message === "OpenAI Moderation API: Inappropriate content detected.") {
            return gt("Inappropriate content detected. Please try again with different input.");
        }

        return gt("API request failed. The service might be unavailable. Please try again in a few minutes.");
    }

    /**
     * Check for each paragraph in the selection, whether it can be translated.
     *
     * @param selectedParagraphs
     *  The info object for all selected paragraphs.
     *
     * @returns
     *  The updated info object for all selected paragraphs.
     */
    #checkTranslatableParagraphs(selectedParagraphs: SelectedParagraphsInfo[]): SelectedParagraphsInfo[]  {

        selectedParagraphs.forEach(paraInfo => {
            paraInfo.isTranslatable = !!paraInfo.selectedText && !TRANSLATION_CHECK.test(paraInfo.selectedText);
        });

        return selectedParagraphs;
    }

    /**
     * Determine the target language for the translation via AI.
     *
     * @param uiLanguage
     *  The language of the appsuite.
     *
     * @param docLanguage
     *  The document language.
     *
     * @param _selectionLanguages
     *  The list of languages of the text in the current selection.
     *
     * @returns
     *  The calculated translation language.
     */
    #getDefaultTranslationLanguage(uiLanguage: string, docLanguage: string, _selectionLanguages: string[]): TranslationLanguage {
        return this.#getEnglishLanguageNameFromIsoCode(uiLanguage) || this.#getEnglishLanguageNameFromIsoCode(docLanguage) || "English";
    }

    /**
     * Determine the English language string from an iso code, using only the first two characters.
     *
     * @param isoCode
     *  The iso code of a language.
     *
     * @returns
     *  The English language string.
     */
    #getEnglishLanguageNameFromIsoCode(isoCode: string): Opt<TranslationLanguage> {
        return (isoCode.substring(0, 2) in ENGLISH_LANGUAGE_NAMES) ? ENGLISH_LANGUAGE_NAMES[isoCode?.substring(0, 2) as LanguageCode] : undefined;
    }

    /**
     * Converts the AI generated content that is passed as string to
     * operations that can be used to modify the document.
     *
     * @param result
     *  An object containing the generated content as string (with new lines
     *  marked with "\n") and additional user settings from the AI dialog.
     *
     * @param useNewParagraph
     *  When the selected content was not removed or when there was no
     *  selected range, is looks better, when the inserted content is in
     *  its own paragraph.
     *
     * @returns
     *  An object, containing an operation generator instance that contains
     *  all generated operations and the start and the end position of the
     *  inserted content.
     */
    #createInsertOperations(result: AIResultType, useNewParagraph: boolean, selectedParagraphs: SelectedParagraphsInfo[]): InsertOpsRT {

        const { content: resultString, languageId: contentLanguage, insert, keepDocStructure } = result;
        const generator = this.docModel.createOperationGenerator();
        const ct = this.docModel.getChangeTrack();
        const insertedChangeTrackAttrs = ct.isActiveChangeTracking() ? { changes: { inserted: ct.getChangeTrackInfo(), removed: null } } : undefined;
        const modifiedChangeTrackAttrs = ct.isActiveChangeTracking() ? { changes: { modified: ct.getChangeTrackInfo(), removed: null } } : undefined;
        let startPos: Position;
        let newContentStartPos: Position;
        let newContentEndPos: Position;

        if (keepDocStructure) { // keeping the document structure (only for translation with delete selection) -> insert at specified positions

            startPos = this.docModel.getSelection().getStartPosition();
            newContentStartPos = [...startPos];

            selectedParagraphs.forEach(paragraphInfo => {

                const paracontent = paragraphInfo.translation!;
                const paraContentLength = paracontent.length;
                startPos = paragraphInfo.position;
                newContentEndPos = [...startPos];

                if (paraContentLength > 0) {

                    startPos.push(paragraphInfo.startOffset || 0);

                    const insertOp: Partial<InsertTextOperation> = { start: [...startPos], text: paracontent };
                    if (ct.isActiveChangeTracking()) { insertOp.attrs = insertedChangeTrackAttrs; }
                    if (contentLanguage) {
                        insertOp.attrs = insertOp.attrs ?? {};
                        insertOp.attrs.character = insertOp.attrs.character ?? {};
                        insertOp.attrs.character.language = contentLanguage;
                    }
                    generator.generateOperation(TEXT_INSERT, insertOp);
                    newContentEndPos = increaseLastIndex(startPos, paraContentLength);

                } else {

                    // setting language attribute at empty paragraph
                    const setAttrsOp: Partial<SetAttributesOperation> = { start: [...startPos] };
                    if (ct.isActiveChangeTracking()) { setAttrsOp.attrs = modifiedChangeTrackAttrs; }
                    if (contentLanguage) {
                        setAttrsOp.attrs = setAttrsOp.attrs ?? {};
                        setAttrsOp.attrs.character = setAttrsOp.attrs.character || {};
                        (setAttrsOp.attrs.character as Partial<CharacterAttributes>).language = contentLanguage;
                    }

                    generator.generateOperation(SET_ATTRIBUTES, setAttrsOp);
                }
            });

        } else { // not keeping the document structure -> insert content in one block with splitParagraph operations

            const allParagraphs = resultString.split("\n");
            startPos = insert === "behind" ? this.docModel.getSelection().getEndPosition() : this.docModel.getSelection().getStartPosition();
            let startParaPos = startPos.slice(0, startPos.length - 1) as Position;
            let additionalCharacterAttrs: Partial<CharacterAttributes> | null = null;
            newContentStartPos = [...startPos];
            newContentEndPos = [...startPos];

            const generateParaSplitOperation = (): void => {
                if (keepDocStructure) { return; } // rely on existing paragraphs -> do not modify document structure
                generator.generateOperation(PARA_SPLIT, { start: [...startPos] });
                if (ct.isActiveChangeTracking()) { generator.generateOperation(SET_ATTRIBUTES, { start: [...startParaPos], attrs: insertedChangeTrackAttrs }); }
                setPositionToStartOfNextParagraph();
                newContentEndPos = [...startPos];
            };

            const setPositionToStartOfNextParagraph = (): void => {
                startParaPos = increaseLastIndex(startParaPos);
                startPos = [...startParaPos];
                startPos.push(0);
            };

            if (useNewParagraph && !isEmptyParagraph(getParagraphElement(this.docModel.getCurrentRootNode(), startPos.slice(0, -1) as Position))) {
                // saving the explicit character attributes of last text span
                additionalCharacterAttrs = this.docModel.getCharacterAttributesFromFinalSpan(this.docModel.getCurrentRootNode(), [...startPos]);
                // and generate the first splitParagraph operation
                generateParaSplitOperation();
            }

            allParagraphs.forEach((paracontent, index) => {

                if (index === 0) { this.docModel.doCheckImplicitParagraph(startPos); }

                const paraContentLength = paracontent.length;
                const isLast = index === allParagraphs.length - 1;
                if (paraContentLength > 0) {
                    const insertOp: Partial<InsertTextOperation> = { start: [...startPos], text: paracontent };
                    if (ct.isActiveChangeTracking()) { insertOp.attrs = insertedChangeTrackAttrs; }
                    if (additionalCharacterAttrs) {
                        insertOp.attrs = insertOp.attrs ?? {};
                        insertOp.attrs.character = additionalCharacterAttrs;
                    }
                    if (contentLanguage) {
                        insertOp.attrs = insertOp.attrs ?? {};
                        insertOp.attrs.character = insertOp.attrs.character ?? {};
                        insertOp.attrs.character.language = contentLanguage;
                    }
                    generator.generateOperation(TEXT_INSERT, insertOp);
                    newContentEndPos = increaseLastIndex(startPos, paraContentLength);
                }
                if (!isLast || useNewParagraph) {
                    startPos = increaseLastIndex(startPos, paraContentLength);
                    generateParaSplitOperation();
                }
            });
        }

        return { generator, startPos: newContentStartPos!, endPos: newContentEndPos! };
    }

    /**
     * When selected content is replaced with the new generated content,
     * it looks better, when the generated content uses at least one leading
     * or trailing space of the current selection.
     * Example: Double click a word, so that the selection has a trailing space.
     *          When the word is translated, the new content should reuse this
     *          space.
     *
     * @param result
     *  An object containing the generated content as string, that might be
     *  modified within this function.
     */
    #keepLeadingAndTrailingSpaces(result: AIResultType): void {

        // the selection object
        const selection = this.docModel.getSelection();

        // handle leading and trailing spaces (for example, when only when word is selected)
        const firstChar = getCharacterAtPosition(this.docModel.getCurrentRootNode(), selection.getStartPosition());
        if (firstChar === " ") { result.content = " " + result.content; } // using a leading space also in the new content

        if (ary.last(selection.getEndPosition())! > 0) {
            const lastChar = getCharacterAtPosition(this.docModel.getCurrentRootNode(), decreaseLastIndex(selection.getEndPosition()));
            if (lastChar === " ") { result.content += " "; } // using a trailing space also in the new content
        }
    }

    /**
     * When the selection contains drawings and the document structure shall not be
     * changed (only text is translated), existing drawings must not be deleted.
     * Therefore it is necessary, that not the complete paragraph content is deleted,
     * but only the text around the drawings.
     *
     * @param deleteGenerator
     *  The operation generator that contains the calculated delete operations to
     *  remove the complete paragraph content.
     *
     * @param selectedParagraphs
     *  A helper object with the information about the paragraphs (and drawings) in
     *  the current selection.
     */
    #keepDrawingsInParagraphs(deleteGenerator: OperationGenerator, selectedParagraphs: SelectedParagraphsInfo[]): void {

        // do nothing, if there are no drawings in the selection
        const noDrawingInSelection = selectedParagraphs.every(paraInfo => !paraInfo.drawings);
        if (noDrawingInSelection) { return; }

        const newDeleteOps: DeleteOperation[] = [];
        const oldDeleteOps = deleteGenerator.getOperations() as DeleteOperation[];

        oldDeleteOps.forEach(oldDeleteOp => {

            const oldDeleteParaPos = ary.initial(oldDeleteOp.start) as Position;
            const selectionInfo = ary.findValue(selectedParagraphs, paraInfo => ary.equals(paraInfo.position, oldDeleteParaPos));

            if (selectionInfo) {
                if (selectionInfo.drawings) {
                    let deleteEndPos = ary.last(oldDeleteOp.end)!;
                    const deleteStartPos = ary.last(oldDeleteOp.start)!;
                    // splitting the delete operation from back to end around the postions of the drawings
                    selectionInfo.drawings.reverse().forEach(drawingPos => {
                        // delete content behind the drawing to the end of paragraph or to the next drawing
                        const newDeleteOp = json.deepClone(oldDeleteOp);
                        if (deleteEndPos > drawingPos) {
                            newDeleteOp.start = appendNewIndex(oldDeleteParaPos, drawingPos + 1);
                            newDeleteOp.end = appendNewIndex(oldDeleteParaPos, deleteEndPos);
                            newDeleteOps.push(newDeleteOp);
                        }
                        deleteEndPos = drawingPos - 1;
                    });
                    // and finally deleting the text in front of the first drawing
                    if (deleteEndPos > deleteStartPos) {
                        const newDeleteOp = json.deepClone(oldDeleteOp);
                        newDeleteOp.start = appendNewIndex(oldDeleteParaPos, deleteStartPos);
                        newDeleteOp.end = appendNewIndex(oldDeleteParaPos, deleteEndPos);
                        newDeleteOps.push(newDeleteOp);
                    }
                } else {
                    newDeleteOps.push(oldDeleteOp);
                }
            } else {
                newDeleteOps.push(oldDeleteOp);
            }

        });

        // replacing the delete operations in the operations generator
        deleteGenerator.clearOperations().insertOperations(newDeleteOps);
    }

    /**
     * Optionally removing selected content in the text document and inserting
     * the AI generated content into the document.
     *
     * @param result
     *  An object containing the generated content as string (with new lines
     *  marked with "\n") and additional user settings from the AI dialog.
     *
     * @returns
     *  A promise that will resolve, when all operations are applied to the
     *  document. When the user presses "Cancel", the promise will be aborted.
     */
    async #insertAIContentIntoDocument(result: AIResultType, selectedParagraphs: SelectedParagraphsInfo[]): Promise<void> {

        if (!result?.content) { return; } // nothing to do

        // the selection object
        const selection = this.docModel.getSelection();
        // the generator for the delete operations
        let deleteGenerator = null;
        // whether the user must be asked before deleting content
        let askUser = false;
        // whether the inserted content shall get its own paragraphs
        const useNewParagraph = !selection.hasRange() || !result.deleteSelected;
        // whether only paragraph content must not be removed (to keep the document structure)
        const keepParagraphs = result.keepDocStructure;

        // create delete operations, if required
        if (result.deleteSelected && selection.hasRange()) {
            const deleteResult = await this.docModel.deleteSelected({ onlyCreateOperations: true, keepParagraphs });
            deleteGenerator = deleteResult.generator;
            askUser = deleteResult.askUser;
            if (keepParagraphs) { this.#keepDrawingsInParagraphs(deleteGenerator, selectedParagraphs); }
            this.#keepLeadingAndTrailingSpaces(result);
        }

        // create insert operations
        const { generator: insertOperationsGenerator, startPos, endPos } = this.#createInsertOperations(result, useNewParagraph, selectedParagraphs);
        if (deleteGenerator) { insertOperationsGenerator.prependOperations(deleteGenerator.getOperations()); }

        if (askUser) {
            try {
                await this.docModel.askUserHandler(askUser, false);
            } catch {
                return; // user rejects removal of content
            }
        }

        // apply the operations asynchronously
        this.docModel.setBlockKeyboardEvent(true);
        const snapshot = new Snapshot(this.docModel);

        // the returned promise is required for the progress bar and the cancelhandler
        // await this.docModel.applyOperationsAsync(insertOperationsGenerator);

        // this.docApp.getView().leaveBusy();
        // if (snapshot) { snapshot.destroy(); }
        // this.docModel.setBlockKeyboardEvent(false);

        const operationsDef = this.docModel.applyOperationsAsync(insertOperationsGenerator);

        // update the progress bar according to progress of the operations promise
        const progressCallback = (progress: number): void => { this.docApp.docView.updateBusyProgress(progress); };

        // clean up handler after all operations are applied or cancelling by the user
        const cleanUpHandler = (): void => {
            this.docApp.docView.leaveBusy();
            if (snapshot) { snapshot.destroy(); }
            this.docModel.setBlockKeyboardEvent(false);
        };

        // register progress bar and cleanup
        operationsDef.progress(progressCallback);
        operationsDef.always(cleanUpHandler);

        // enter busy mode and register cancel handler
        this.docApp.docView.enterBusy({
            cancelHandler: () => {
                if (operationsDef?.abort) {
                    snapshot.apply(); // restoring the old state
                    this.docApp.enterBlockOperationsMode(() => { operationsDef.abort(); }); // block sending of operations
                }
            },
            delay: 1000,
            warningLabel: /*#. shown while applying AI content operations */ gt("Inserting AI content will take some time, please wait...")
        });

        await operationsDef;

        this.setTimeout(() => this.docModel.getSelection().setTextSelection(startPos, endPos), 100); // set selection deferred (TODO)
    }
}

// export =================================================================

export default AIService;
