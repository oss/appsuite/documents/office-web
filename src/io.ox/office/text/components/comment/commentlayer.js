/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';

import { ary } from '@/io.ox/office/tk/algorithms';
import { TOUCH_DEVICE, parseCssLength, convertCssLength, getFocus, setFocus, createIcon, getSchemeColorCount } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { getMSFormatIsoDateString } from '@/io.ox/office/editframework/utils/dateutils';
import { comparePositions } from '@/io.ox/office/editframework/utils/operations';
import { calculateUserId, OX_PROVIDER_ID } from '@/io.ox/office/editframework/utils/operationutils';
import { OperationContext } from '@/io.ox/office/editframework/model/operation/context';

import { getBooleanOption, getDomNode, getIntegerOption, getStringOption, textLogger } from '@/io.ox/office/textframework/utils/textutils';
import { COMMENTBUBBLENODE_CLASS, COMMENTBUBBLELAYER_CLASS, COMMENTBUBBLENODE_SELECTOR, COMMENTLAYER_CLASS, COMMENTMARGIN_CLASS,
    COMMENTPLACEHOLDER_CLASS, COMMENTPLACEHOLDER_NODE_SELECTOR, RANGEMARKER_STARTTYPE_SELECTOR, getClosestMarginalTargetNode,
    getContentChildrenOfPage, getPageContentNode, getRangeMarkerId, getRangeMarkerType, getTargetContainerId, getTextDrawingLayerNode,
    isCommentPlaceHolderNode, isEmptySpan, isFooterWrapper, isInsideHeaderFooterTemplateNode, isParagraphNode, isRangeMarkerEndNode,
    isRangeMarkerStartNode, setTargetContainerId } from '@/io.ox/office/textframework/utils/dom';
import { getDOMPosition, getOxoPosition, getParagraphElement, getParagraphNodeLength, getPixelPositionToRootNodeOffset,
    hasSameParentComponent, hasSameParentComponentAtSpecificIndex, increaseLastIndex, isNodePositionInsideRange,
    isValidElementPosition } from '@/io.ox/office/textframework/utils/position';
import { COMMENT_CHANGE, COMMENT_INSERT, DELETE, INSERT_DRAWING, RANGE_INSERT, TEXT_INSERT } from '@/io.ox/office/textframework/utils/operations';

import CommentCollection from '@/io.ox/office/text/model/comment/commentcollection';

// class CommentLayer =========================================================

/**
 * An instance of this class represents the model for all comments in the
 * edited OX Text document.
 *
 * Triggers the following events:
 * - 'commentlayer:created': When the comment layer on the right side of the
 *      document becomes visible.
 * - 'commentlayer:removed': When the comment layer on the right side of the
 *      document is no longer visible.
 * - 'update:commentauthorlist': When the list of author comments is modified.
 *
 * @param {TextModel} docModel
 *  The text model instance.
 */
export default class CommentLayer extends ModelObject {

    // a list of all comments (jQuery), the model
    #comments = [];
    // whether there is an active filter list for the authors
    #isActiveAuthorFilterList = false;
    // a list of authors that can be used for filtering
    #authorFilter = null;
    // a sorted list of all comment place holders (jQuery), from start of document to end of document
    #commentPlaceHolders = $();
    // a place holder ID for absolute positioned drawings
    #placeHolderCommentIDs = new Set();
    // a marker class to identify the comment connection lines
    #commentLineClass = 'isCommentConnectionLine';
    // a selector to identify the comment connection lines
    #commentLineSelector = '.isCommentConnectionLine';
    // the undo manager
    #undoManager = null;
    // the models page layout object
    #pageLayout = null;
    // whether a target comment was inserted in undo/redo operation
    #undoRedoWithTargetComments = false;
    // the layer node for all comments
    #commentLayerNode = null;
    // the layer node for all comment bubbles
    #bubbleLayerNode = null;
    // the children of the page that contain can contain comment place holder nodes as sorted jQuery list
    // -> in OOXML this is only the page content node, in ODF also the first header and last footer
    #contentChildren = null;
    // the comment default width in pixel
    #commentDefaultWidth = 300;
    // the current (start) mode for showing comments
    #displayMode = TOUCH_DEVICE ? CommentLayer.DISPLAY_MODE.BUBBLES : CommentLayer.DISPLAY_MODE.SELECTED;
    // the default mode for highlighting comments (this can be modified dependent from resolution)
    #defaultDisplayMode = TOUCH_DEVICE ? CommentLayer.DISPLAY_MODE.BUBBLES : CommentLayer.DISPLAY_MODE.SELECTED;
    // the display mode that was active, before the reload was triggered (in reload error case, it is always hidden)
    #beforeReloadDisplayMode = null;
    // the comment state when a zoom level change is triggered
    #zoomChangeMode = null;
    // whether the bubble mode is activated
    #isBubbleMode = false;
    // a collector for the bubble nodes
    #bubbleNodeCollector = null;
    // a collector for all insertComment operations that are sent additionally to the fast load string
    #fastLoadInsertCommentOperations = {};
    // a collector for all changeComment operations that are sent additionally to the fast load string
    #fastLoadChangeCommentOperations = {};
    // whether the comment model was filled within fastload process (for debug reasons)
    #commentModelFilled = false;
    // the ID of a comment in edit mode. Or null, if no comment is in edit mode
    #commentEditorActive = null;
    // the comment fastload string received from the backend (for debug reasons)
    #fastLoadString = null;
    // the comment collection object
    #commentCollection = null;
    // the height of a comment editor in the comments pane
    #previousHeight = 0;

    constructor(docModel) {

        // base constructor
        super(docModel);

        // initialization -----------------------------------------------------

        this.docApp.onInit(() => {

            // setting the global undo manager
            this.#undoManager = this.docModel.getUndoManager();

            // setting the global page layout object
            this.#pageLayout = this.docModel.getPageLayout();

            // registering handler for preparations after successful document loading
            this.waitForImportSuccess(this.#importSuccessHandler);

            // registering handler for loading document with fastload (early for drawings in comments, first run for ODF (49142))
            this.docModel.one('fastload:done', () => { this.#updateModel({ fastLoad: true }); });

            // registering handler for first inserting of page breaks
            this.docModel.one('pageBreak:after', () => { this.#updateAfterPageBreak(); });

            // in fast load the model can be filled, after the author list is registered in setDocumentAttributes operation
            // -> this must happen before the importSuccessHandler, because otherwise following insertComment, changeComment
            //    and deleteComment operations fail.
            this.docModel.one('authorList:update', () => { this.#fillModelAfterFastLoad(); });

            // update comments when document edit mode changes
            // this.listenTo(this.docApp, 'docs:editmode', updateEditMode);

            // updating the model after the document was reloaded (after cancelling long running actions)
            this.listenTo(this.docModel, 'document:reloaded', this.#handleDocumentReload);

            // register handler for document modifications (triggered very often)
            this.listenTo(this.docModel, 'update:absoluteElements removed:marginal', this.#updateCommentsLayer);

            // register handler for changes of page settings
            this.listenTo(this.docModel, 'change:pageSettings', () => this.#updateHorizontalCommentLayerPosition());

            // register handler for tasks, when the header or footer are no longer active and all marginal nodes are exchanged
            this.listenTo(this.#pageLayout, 'updateMarginal:leave', this.#additionalUpdatePlaceHolderCollection);

            // register handler for tasks after undo operations
            this.listenTo(this.#undoManager, 'undo:after redo:after', this.#handleUndoAfter);

            // listening the creation or removal of comment layer node
            this.on('commentlayer:created', options => this.#updatePagePosition(true, options));
            this.on('commentlayer:removed', options => this.#updatePagePosition(false, options));

            // register handler for active changes in the comment pane (opening and closing the editor, ...)
            this.listenTo(this.docModel, 'comments:update:vertical', () => this.#updateCommentsAndRanges());

            // register handler to react to creation of comment editors (new, edit, reply)
            this.listenTo(this.docApp.docView, 'commenteditor:created', this.#commentEditorCreatedHandler);

            // register handler to react to closing of comment editors (new, edit, reply)
            this.listenTo(this.docApp.docView, 'commenteditor:closed', this.#commentEditorClosedHandler);

            // register handler to react to changes of the info box that informs the user
            this.listenTo(this.docApp.docView, 'comment:infobox:changed', this.#infoboxChangedHandler);

            // register handler to react to changes of the info box that informs the user
            this.listenTo(this.docApp, 'docs:reload:prepare', this.#prepareReloadHandler);

            // register handler to react to changes of the zoom level
            this.listenTo(this.docApp.docView, 'change:zoom:before', this.#changeZoomBeforeHandler);

            // register handler to react to changes of the zoom level
            this.listenTo(this.docApp.docView, 'change:zoom', this.#changeZoomHandler);

            // generating the comment collection instance that collects all comment models
            this.#commentCollection = this.member(new CommentCollection(this.docApp, this.docModel));
        });
    }

    // public methods -----------------------------------------------------

    /**
     * Whether the document contains comments in the document.
     *
     * @returns {Boolean}
     *  Whether the document contains at least one comment.
     */
    isEmpty() {
        return this.#commentCollection.isEmpty();
    }

    /**
     * Whether a specified ID belongs to an existing comment model in the
     * comment collection.
     *
     * @param {String} id
     *  The id string.
     *
     * @returns {Boolean}
     *  Whether the specified ID string is a valid comment id.
     */
    isCommentId(id) {
        return !!this.#commentCollection.getById(id);
    }

    /**
     * Provides the number of comments in the comment collection.
     *
     * @returns {Number}
     *  The number of comments in the comment collection.
     */
    getCommentNumber() {
        return this.#commentCollection.getCommentNumber();
    }

    /**
     * Returning the comment collection, that contains all comments in the document.
     *
     * @returns {Object}
     *  A reference to the comment collection object.
     */
    getCommentCollection() {
        return this.#commentCollection;
    }

    /**
     * Whether the comment editor is active.
     *
     * @returns {Boolean}
     *  Whether the comment editor is active.
     */
    isCommentEditorActive() {
        return this.#commentEditorActive !== null;
    }

    /**
     * Returning the ID of the comment that has an active
     * editor. Or null, of no comment is in edit mode.
     *
     * @returns {String | null}
     *  The ID of the comment with the active editor.
     */
    getIdOfActiveCommentEditor() {
        return this.#commentEditorActive;
    }

    /**
     * Returns the active comment display mode.
     *
     * @returns {String}
     *  The currently active comment display mode.
     */
    getDisplayMode() {
        return this.#displayMode;
    }

    /**
     * Returns the default comment display mode.
     *
     * @returns {String}
     *  The default comment display mode.
     */
    getDefaultDisplayMode() {
        return this.#defaultDisplayMode;
    }

    /**
     * Returns the display mode that was active, before a reload was triggered.
     *
     * @returns {String}
     *  The display mode that was active, before a reload was triggered.
     */
    getBeforeReloadDisplayMode() {
        return this.#beforeReloadDisplayMode;
    }

    /**
     * Whether the bubble mode is active.
     *
     * @returns {Boolean}
     *  Whether the bubble mode is active.
     */
    isBubbleMode() {
        return this.#isBubbleMode;
    }

    /**
     * Returns whether the hidden comment display mode is active.
     *
     * @returns {Boolean}
     *  Whether the hidden comment display mode is active.
     */
    isHiddenDisplayModeActive() {
        return this.#displayMode === CommentLayer.DISPLAY_MODE.HIDDEN;
    }

    /**
     * Returns the hidden comment display mode.
     *
     * @returns {String}
     *  The hidden comment display mode.
     */
    getHiddenDisplayMode() {
        return CommentLayer.DISPLAY_MODE.HIDDEN;
    }

    /**
     * Returns the object with all supported display modes.
     *
     * @returns {Object}
     *  An object with all supported display modes as keys.
     */
    getSupportedDisplayModes() {
        return CommentLayer.DISPLAY_MODE;
    }

    /**
     * Getting a bubble node for a specified comment ID. If
     * it does not exist, 'undefined' is returned.
     *
     * @param {String} commentId
     *  The ID of a comment (thread).
     *
     * @returns {jQuery}
     *  The jQuerified bubble node with the specified ID, or
     *  undefined, if it does not exist.
     */
    getBubbleNodeById(commentId) {
        return this.#bubbleNodeCollector[commentId];
    }

    /**
     * Getting the class name that must be assigned to the comments pane
     * root node for each specific display mode.
     *
     * @param {String} localDisplayMode
     *  The specified display mode.
     *
     * @returns {String}
     *  The classname that will be set for the specified display mode.
     */
    getCommentDisplayClassName(localDisplayMode) {
        return CommentLayer.DISPLAY_MODE_CLASSNAMES[localDisplayMode];
    }

    /**
     * Getting a string that consists of all available class names for
     * the different display modes at the comments pane root node.
     *
     * @returns {String}
     *  The string that consists of all available class names for the
     *  different display modes.
     */
    getAllCommentDisplayClassNames() {
        return CommentLayer.ALL_DISPLAY_MODE_CLASSNAMES;
    }

    /**
     * Returning a list of all authors of comments, whose comments are visible.
     * This list contains all authors, if no filter is active. If a filter is
     * active, then this filter represents the list of comment authors.
     *
     * @returns {String[]}
     *  The list of all comment authors in the document that are not filtered.
     */
    getListOfUnfilteredAuthors() {
        return this.#authorFilter ? this.#authorFilter : this.#commentCollection.getCommentsAuthorList();
    }

    /**
     * Returning the number of authors of comments in the document.
     *
     * @returns {Number}
     *  The number of authors of comments in the document.
     */
    getCommentAuthorCount() {
        return this.#commentCollection.getCommentAuthorCount();
    }

    /**
     * Setting a new filter for the comment authors.
     *
     * @param {String[]} nameList
     *  The list of those authors, whose comments shall be displayed.
     */
    setAuthorFilter(nameList) {

        // the number of comment authors in the document
        const maxNumber = this.#commentCollection.getCommentsAuthorList().length;
        // the old state of active author
        const oldState = this.#isActiveAuthorFilterList;

        if (nameList && nameList.length < maxNumber) {
            this.#isActiveAuthorFilterList = true;
            this.#authorFilter = nameList;
        } else {
            this.#isActiveAuthorFilterList = false;
            this.#authorFilter = null;
        }

        // Setting a marker class, to avoid shrinking of comments, if a filter is active (but only, if the state has changed)
        if (oldState !== this.#isActiveAuthorFilterList) {
            this.docApp.docView.commentsPane.handleActiveAuthorFilter(this.#isActiveAuthorFilterList);
        }

        // applying the new filter
        this.#applyAuthorFilter();
    }

    /**
     * Removing an optionally set author filter.
     */
    removeAuthorFilter() {
        if (this.#isActiveAuthorFilterList) { this.setAuthorFilter(); }
    }

    /**
     * Provides a unique ID for the comment in the comment layer and
     * its placeholder element and the comment ranges.
     * Also handling 'broken' range markers, that can exist without
     * an existing comment. In this case the id needs to be further
     * increased.
     *
     * Caused by OT the comment ID has to contain a client specific marker,
     * so that two 'insertComment' operations cannot collide. The specified
     * syntax required by the filter is the following:
     *
     * 'cmt' + <4 digit client id marker + 4 digit unique identifier>
     *
     * @returns {String}
     *  A unique id.
     */
    getNextCommentID() {

        let commentId = this.#getNextPlaceHolderCommentID(); // the next free comment id
        const rangeMarker = this.docModel.getRangeMarker(); // the range marker object

        // checking, if there also exist no range markers with this id
        while (rangeMarker.isUsedRangeMarkerId(commentId)) {
            commentId = this.#getNextPlaceHolderCommentID();
        }
        return commentId;
    }

    /**
     * Returning the current author of the application as string
     * with full display name.
     *
     * @returns {String}
     *  The author string for the comment meta info object.
     */
    getCommentAuthor() {
        return this.docApp.getClientOperationName();
    }

    /**
     * Returning the user id of the current author of the application.
     * The value for the userId is already transformed so that it can be
     * used in the operation.
     *
     * @returns {String}
     *  The userId of the author for the comment meta info object.
     */
    getCommentAuthorUid() {
        const uid = calculateUserId(ox.user_id);
        return uid ? uid.toString() : '';
    }

    /**
     * Returning the current date as string in ISO date format.
     * This function calls getMSFormatIsoDateString() to simulate
     * the MS behavior, that the local time is send a UTC time. The
     * ending 'Z' in the string represents UTC time, but MS always
     * uses local time with ending 'Z'.
     *
     * @returns {String}
     *  The date string for the change track info object.
     */
    getCommentDate() {
        return getMSFormatIsoDateString();
    }

    /**
     * Receiving the comment layer node. If it does not exist, it is created. The node is
     * searched as child of div.page. It can happen, that the global commentLayerNode is
     * not defined, for example after loading document from local storage.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.visible=true]
     *      If set to true, the comment layer will be created with visibility value set
     *      to 'hidden'. This is useful for loading from local storage or using fast load,
     *      because then the comments will displayed unordered, until the document is
     *      loaded completely.
     *
     * @returns {jQuery}
     *  The comment layer node
     */
    getOrCreateCommentLayerNode(options) {

        // the comment layer node
        let commentLayer = this.#commentLayerNode;
        // the bubble layer node
        let bubbleLayer = null;
        // the page content node
        let pageContentNode = null;
        // whether the commentlayer shall be visible
        const visible = getBooleanOption(options, 'visible', true);

        // is there already a comment layer node
        if (commentLayer && commentLayer.length > 0) { return commentLayer; }

        // create a new comment layer node
        pageContentNode = getPageContentNode(this.docModel.getNode());

        commentLayer = $(`<div class="${COMMENTLAYER_CLASS} translucent-constant">`);

        bubbleLayer = $(`<div class="${COMMENTBUBBLELAYER_CLASS}">`);

        // workaround for abs pos element, which get the "moz-dragger" but we dont want it
        commentLayer.attr('contenteditable', 'false');

        // comment layer will not be made visible, if specified in options
        if (!visible) { commentLayer.css('visibility', 'hidden'); }

        // Adding the text drawing layer behind an optional footer wrapper
        if (isFooterWrapper(pageContentNode.next())) { pageContentNode = pageContentNode.next(); }

        // inserting the comment layer node in the page
        pageContentNode.after(commentLayer);

        // inserting the comment bubble layer node in the page
        commentLayer.after(bubbleLayer);

        // saving global variable (this function might be triggered during fast load or local storage load)
        this.#commentLayerNode = commentLayer;

        // saving also the global bubble layer node
        this.#bubbleLayerNode = bubbleLayer;

        // adding additional classes to the comment layer to be compatible with comments pane in OX Spreadsheet and OX Presentation
        commentLayer.addClass('view-pane comments-pane');

        // adding the marker class required for the current display mode
        commentLayer.addClass(this.getCommentDisplayClassName(this.#displayMode));

        // setting the left distance in the div.page node
        this.#updateHorizontalCommentLayerPosition(this.#commentLayerNode);

        return commentLayer;
    }

    /**
     * Handler for insertComment operations.
     *
     * @param {OperationContext} context
     *  A wrapper representing the 'insertComment' operation.
     *
     * @returns {Boolean}
     *  Whether the comment has been inserted successfully.
     */
    insertCommentHandler(context) {

        // the logical start position for the new comment
        const start = context.getPos('start');
        // the unique id of the comment
        const id = context.getStr('id');
        // the author of the comment -> the author name can be an empty string (DOCS-2646)
        let author = context.optStr('author', { empty: true });
        // the author ID of the author of the comment
        const authorId = context.optStr('authorId');
        // the optional target string for the comment (comments in header/footer)
        const target = context.optStr('target');
        // the place holder element for the drawing in the page content
        const commentPlaceHolder = $('<div>', { contenteditable: false }).addClass('inline ' + COMMENTPLACEHOLDER_CLASS);
        // an object with all range end markers in the document
        const endMarker = this.docModel.getRangeMarker().getEndMarker(id);
        // the text span for inserting the comment place holder node
        const insertSpan = this.docModel.prepareTextSpanForInsertion(start, null, target);
        // whether this document is completely loaded
        const isImportFinished = this.isImportFinished();
        // the author might be registered in the author list
        let registeredAuthor = null;
        // whether it is necessary to search for the position of the new inserted comment
        let findPosition = false;
        // the new inserted comment model
        let commentModel = null;
        // whether there is already a comment with the same ID in the collection (DOCS-2670)
        let commentIdExists = false;
        // the attributes of the operation (only reqired for OOXML in context of DOCS-4507)
        const attrs = context.optDict('attrs');

        if (!insertSpan) { return false; }

        // taking care of a specified author list
        if (!author && authorId) {
            registeredAuthor = this.docApp.getRegisteredAuthorItemById(authorId);
            author = registeredAuthor.name;
        }

        // checking, if there is already a comment with the same ID (54744, DOCS-2666)
        if (this.#commentCollection.getById(id)) {
            commentIdExists = true;
            globalLogger.warn('WARNING: Comment with ID "' + id + '" already exists!'); // TODO: This is more likely an error (DOCS-2670)
        }

        // inserting the place holder node into the DOM
        if (endMarker) {
            if (getDomNode($(insertSpan).prev()) === getDomNode(endMarker)) { // the found span follows the saved end rangemarker (this is the required structure)
                if (isEmptySpan(insertSpan)) { $(insertSpan).remove(); }
                commentPlaceHolder.insertAfter(endMarker); // no empty span between end marker and comment placeholder node
            } else {
                // problematic case (63780): The found span does not follow the end rangemarker (how was the document generated?)
                commentPlaceHolder.insertAfter(insertSpan); // inserting the comment NOT behind the specified end marker, but at its valid position
                globalLogger.warn('insertCommentHandler(): Failed to insert comment node behind its specified end rangemarker!');
            }
        } else {
            commentPlaceHolder.insertAfter(insertSpan);
        }

        // assigning the target id to the comment place holder node
        setTargetContainerId(commentPlaceHolder, id);

        // in docx files the character attributes must be assigned to the range marker nodes (DOCS-4507)
        if (attrs && attrs.character && this.docModel.hasOoxmlTextCharacterFilterSupport()) {
            const startMarker = this.docModel.getRangeMarker().getStartMarker(id);
            if (startMarker && this.#isEmptyCommentRangeInOoxml(startMarker, endMarker)) { this.docModel.characterStyles.setElementAttributes(startMarker, attrs); }
            this.docModel.characterStyles.setElementAttributes(endMarker, attrs);
        }

        // restoring the sorted list of all place holders (but not in the loading phase)
        // -> this must be done before the new comment is inserted into the comment collection to find
        //    the correct position in the sorted comment collection.
        // -> during fast load import this must be done, when an additional insertComment is sent
        //    after the fast load string is attached.
        if (isImportFinished || this.docModel.isFastLoadImport()) {
            this.#updatePlaceHolderCollection();
            findPosition = true;
        }

        // additional operations after fast load
        // -> this will be handled in function 'this.#fillModelAfterFastLoad'
        // -> but the placeholder must be inserted, so that it can be found in 'this.#fillModelAfterFastLoad' and the operation context must be saved
        if (this.docModel.isFastLoadImport() && !isImportFinished && !this.#commentModelFilled) {
            this.#fastLoadInsertCommentOperations[id] = context;
            return true;
        }

        // adding the comment into the commentModel and commentCollection classes
        if (!commentIdExists) { commentModel = this.#commentCollection.handleInsertCommentOperation(context, commentPlaceHolder, this.#commentPlaceHolders, { findPosition }); }

        if (target && this.#pageLayout.isIdOfMarginalNode(target) && this.docModel.isUndoRedoRunning()) {
            this.docModel.updateEditingHeaderFooterDebounced(this.docModel.getRootNode(target)); // #53839
        }

        if (isImportFinished && this.docApp.isODF() && target && this.docApp.isOTEnabled()) { // check for OT is required for 68184
            if (isInsideHeaderFooterTemplateNode(this.docModel.getNode(), insertSpan)) {
                // refreshing header/footer in document before updating placeholder collection (66562)
                this.#pageLayout.replaceAllTypesOfHeaderFooters();
            } else {
                // refreshing the placeholder, because this happens otherwise only, when leaving header/footer edit mode (but it might not be active)
                this.#pageLayout.updateEditingHeaderFooter({ givenNode: getClosestMarginalTargetNode(insertSpan) });
            }
        }

        // handling the global counter
        this.#updatePlaceHolderCommentID(id);

        // switching to the default display mode, if 'none' is currently active
        if (this.#displayMode === CommentLayer.DISPLAY_MODE.HIDDEN) { this.switchCommentDisplayMode(this.#defaultDisplayMode); }

        // updating the comments side pane, setting the vertical offsets
        if (isImportFinished) { this.#updateCommentsAndRangesInAllMode(); }

        // activating the author, if he was filtered out
        // -> but any other new comment must be visible, if a filter is active
        if (this.#isActiveAuthorFilterList) { this.#updateAuthorFilter(author, { addAuthor: true, forceUpdate: commentModel && commentModel.isReply() && this.#commentCollection.hasHiddenParent(commentModel) }); }

        // special handling, if the bubble mode is active
        if (this.#isBubbleMode && isImportFinished) {
            if (!context.external && commentModel && !commentModel.isReply()) {
                this.#commentCollection.selectComment(commentModel); // selecting the comment
                this.#showCommentThreadInBubbleMode(commentModel); // and showing the comment thread next to the bubble
            }

            // updating the bubble node after the comment was inserted might be required
            if (commentModel && commentModel.getParentId()) { this.#updateBubbleNode(commentModel); }
        }

        // setting a marker, that comments in header/footer are inserted during undo/redo
        if (this.docModel.isUndoRedoRunning() && target) { this.#undoRedoWithTargetComments = true; }

        return true;
    }

    /**
     * New comment processing in OX Text. This function is called for new comment threads
     * and for a reply to an existing comment thread.
     *
     * This function generates the required operations for inserting new comments. This
     * operations are different for ODT and OOXML and also for inserting a comment into
     * an existing comment thread or for starting a new comment thread.
     *
     * In ODT it is possible to insert comments in header and footer. This is not possible
     * in OOXML
     *
     * @param {String|Null} parentId
     *  The unique comment ID of the parent comment.
     *
     * @param {String} commentAuthor
     *  The author of the comment.
     *
     * @param {Object} commentInfo
     *  Additional data for the new comment, like 'mentions' and 'text'.
     *
     * @returns {String | void} // TODO: check-void-return
     *  returns the new commentId
     */
    insertComment(parentId, commentAuthor, commentInfo) {

        // the operations generator
        const generator = this.docModel.createOperationGenerator();
        // whether this is a new thread or a reply into an existing thread
        const isNewThread = !parentId || parentId === this.#commentCollection.getTemporaryCommentId();
        // the selection state that was saved, when the new comment was generated
        const selectionState = isNewThread ? (this.#commentCollection.getSavedSelectionState() ? this.#commentCollection.getSavedSelectionState() : this.docModel.getSelectionState())  : null;
        // the logical start position of the selection
        let start = selectionState ? selectionState.start : null;
        // the logical end position of the selection
        let end = selectionState ? selectionState.end : null;
        // whether start and end position are in the same paragraph
        let sameParent = false;
        // the new id of the comment
        const commentId = this.getNextCommentID();
        // the logical position for the start range
        let startRangePos = null;
        // the logical position for the end range
        let endRangePos = null;
        // the logical position for the comment
        let commentPos = null;
        // the authors user id
        const commentUserId = this.getCommentAuthorUid();
        // the date of comment creation
        const commentDate = this.getCommentDate();
        // the text in the comment
        const text = commentInfo.text;
        // the mentions in the comment
        const mentions =  commentInfo.mentions;
        // all comment models in the thread
        let commentThread = null;
        // the last comment in the thread
        let lastCommentInThread = null;
        // the id of the last comment node in the thread
        let lastCommentId = null;
        // the range markers object
        const rangeMarker = isNewThread ? null : this.docModel.getRangeMarker();
        // the range start marker node
        let startMarkerNode = null;
        // first start marker node of the comment thread
        let firstStartMarkerNode = null;
        // the range end marker node
        let endMarkerNode = null;
        // the target node for the start marker and end marker
        let rootNode = null;
        // the logical range start marker position
        let startMarkerPos = null;
        // the logical position of the first range start marker in the thread
        let firstStartMarkerPos = null;
        // the logical range end marker position
        let endMarkerPos = null;
        // the place holder node of the comment
        let placeHolderNode = null;
        // the target in which the comment is located (only set for header and footer)
        let target = '';
        // whether the start position is valid
        let validStart = true;
        // whether the end position is valid
        let validEnd = true;
        // the ID of the complete comment thread
        let threadId;
        // the target might be active, if the comment is inserted
        const blockerTarget = this.docModel.getOperationTargetBlocker();
        // the settings for the new created operation
        let newOperation = null;
        // an optional start position after the comment is inserted
        let selectionStartPos = null;
        // an optional end position after the comment is inserted
        let selectionEndPos = null;
        // the settings for the new created insert comment operation
        const insertCommentOperation = { id: commentId, author: commentAuthor, userId: commentUserId, providerId: OX_PROVIDER_ID, date: commentDate, text };

        if (isNewThread && parentId === this.#commentCollection.getTemporaryCommentId()) { parentId = 0; }

        // optionally adding parentId and mentions to the settings for the insertComment operation
        if (parentId) { insertCommentOperation.parentId = parentId; }
        if (mentions) { insertCommentOperation.mentions = this.#reduceMentionContentForOperation(mentions); }

        if (isNewThread) {

            // checking that 'start' and 'end' positions are still valid. They are transformed via OT before -> reducing risk of invalid operations
            target = selectionState ? selectionState.target : null;

            validStart = start && isValidElementPosition(this.docModel.getRootNode(target), start, { allowFinalPosition: true });
            validEnd = end && isValidElementPosition(this.docModel.getRootNode(target), end, { allowFinalPosition: true });

            if (!validStart && !validEnd) { return; } // nothing to do -> improve handling

            if (!validStart) { start = _.copy(end); }
            if (!validEnd) { end = _.copy(start); }

            // checking, that start and end of selection are in non-implicit paragraphs
            this.docModel.doCheckImplicitParagraph(start);
            sameParent = hasSameParentComponent(start, end);
            if (!sameParent) { this.docModel.doCheckImplicitParagraph(end); }

            startRangePos = start;
            endRangePos = sameParent ? increaseLastIndex(end) : end;
            commentPos = this.docApp.isODF() ? start : increaseLastIndex(endRangePos);

            if (this.docApp.isODF()) { // starting new comment thread in ODT

                // A. inserting the comment itself and the end range marker
                // B. in ODF comment node and marker must have the same parent (no comment from paragraph into table)

                insertCommentOperation.start = commentPos;
                generator.generateOperation(COMMENT_INSERT, insertCommentOperation);

                if (!_.isEqual(start, end)) {
                    if (selectionState.type !== 'cell' && !hasSameParentComponent(startRangePos, endRangePos, 2)) {
                        // Limit the end range position, if the paragraphs do not have the same parent (using number 2)
                        endRangePos = this.#getBestODFRangeEndPosition(startRangePos);
                        // if the new endRangePos is in the same paragraph as the comment node, it need to be increased by 1
                        if (_.isEqual(_.initial(commentPos), _.initial(endRangePos))) { endRangePos = increaseLastIndex(endRangePos); }
                    }

                    newOperation = { start: _.copy(endRangePos), position: 'end', type: 'comment', id: commentId, target: blockerTarget };
                    generator.generateOperation(RANGE_INSERT, newOperation);
                }

                selectionStartPos = commentPos;
                selectionEndPos = endRangePos;

            } else { // starting new comment thread in OOXML

                // inserting two comment ranges and the comment itself
                // no target for all operations required
                // -> inserting comment is only possible in main document
                // -> rangeMarker for comments are never change tracked!
                newOperation = { start: _.copy(startRangePos), position: 'start', type: 'comment', id: commentId, target: blockerTarget };
                generator.generateOperation(RANGE_INSERT, newOperation);

                newOperation = { start: _.copy(endRangePos), position: 'end', type: 'comment', id: commentId, target: blockerTarget };
                generator.generateOperation(RANGE_INSERT, newOperation);

                insertCommentOperation.start = commentPos;
                insertCommentOperation.target = blockerTarget;

                // adding character attributes for the first character in the paragraph (DOCS-4211)
                // -> the filter can save this attributes only at the comment, not at the comment rangemarkers in ooxml format
                const additionalCharacterAttrs = this.docModel.collectCharacterAttributesForInsertOperation(this.docModel.getCurrentRootNode(target), start);

                if (additionalCharacterAttrs) {
                    insertCommentOperation.attrs = {};
                    insertCommentOperation.attrs.character = additionalCharacterAttrs;
                }

                generator.generateOperation(COMMENT_INSERT, insertCommentOperation);

                selectionStartPos = startRangePos;
                selectionEndPos = commentPos;
            }

        } else { // this is not a new thread but a reply

            // calculating the position of the start marker node

            commentThread = this.#commentCollection.getThreadById(parentId);
            lastCommentInThread = commentThread && commentThread.length ? _.last(commentThread) : null;
            if (lastCommentInThread) { lastCommentId = lastCommentInThread.getId(); }
            if (lastCommentId) { startMarkerNode = rangeMarker.getStartMarker(lastCommentId); }
            if (lastCommentId) { endMarkerNode = rangeMarker.getEndMarker(lastCommentId); }

            target = this.#commentCollection.getById(parentId).getTarget();
            rootNode = this.docModel.getRootNode(target);

            if (this.docApp.isODF()) { // reply in ODT

                const isFirstChild = commentThread && commentThread.length === 1;

                // B. in ODF only the comment node needs to be inserted, directly behind the end range node
                //    of the parent comment or directly behind the previous child comment

                // the logical position for the comment
                if (isFirstChild) {
                    if (!endMarkerNode && lastCommentInThread) { endMarkerNode = lastCommentInThread.getCommentPlaceHolderNode(); } // this might happen after copy/paste (DOCS-2653)
                    if (!endMarkerNode) {
                        globalLogger.error('CommentLayer.insertComment(): Failed to determine end marker node when inserting comment!');
                        return;
                    }
                    endMarkerPos = getOxoPosition(rootNode, endMarkerNode);
                    commentPos = increaseLastIndex(endMarkerPos); // the first child comment -> first position behind the end range marker
                } else {
                    placeHolderNode = lastCommentInThread ? lastCommentInThread.getCommentPlaceHolderNode() : null; // -> not first child -> position behind preceeding comment
                    if (!placeHolderNode) {
                        globalLogger.error('CommentLayer.insertComment(): Failed to determine placeholder node when inserting comment!');
                        return;
                    }
                    commentPos = increaseLastIndex(getOxoPosition(rootNode, placeHolderNode));
                }

                insertCommentOperation.start = commentPos;
                if (target) {
                    insertCommentOperation.target = target;
                } else {
                    insertCommentOperation.target = blockerTarget;
                }
                generator.generateOperation(COMMENT_INSERT, insertCommentOperation);

                selectionStartPos = commentPos;
                selectionEndPos = commentPos;

            } else { // reply in OOXML

                startMarkerPos = startMarkerNode ? getOxoPosition(rootNode, startMarkerNode) : null;
                endMarkerPos = endMarkerNode ? getOxoPosition(rootNode, endMarkerNode) : null;

                if (startMarkerPos && endMarkerPos) {

                    startRangePos = increaseLastIndex(startMarkerPos);

                    // the new end positions for the end range marker and the comment node itself are dependent
                    // from a check, if the start range marker is inside the same paragraph
                    if (_.isEqual(_.initial(startMarkerPos), _.initial(endMarkerPos))) {
                        endRangePos = increaseLastIndex(endMarkerPos, 3); // increasing position by 3: one for startmarker, one for endmarker, one for comment
                    } else {
                        endRangePos = increaseLastIndex(endMarkerPos, 2); // increasing position by 2: one for endmarker, one for comment
                    }

                    commentPos = increaseLastIndex(endRangePos);

                    // no target for all operations required, because inserting comment is only possible in main document

                    newOperation = { start: _.copy(startRangePos), position: 'start', type: 'comment', id: commentId, target: blockerTarget };
                    generator.generateOperation(RANGE_INSERT, newOperation);

                    newOperation = { start: _.copy(endRangePos), position: 'end', type: 'comment', id: commentId, target: blockerTarget };
                    generator.generateOperation(RANGE_INSERT, newOperation);

                    insertCommentOperation.start = commentPos;
                    insertCommentOperation.target = blockerTarget;
                    generator.generateOperation(COMMENT_INSERT, insertCommentOperation);

                    // calculating the logical positions for the selection
                    threadId = this.#commentCollection.getThreadIdForCommentId(parentId);
                    if (threadId) { firstStartMarkerNode = rangeMarker.getStartMarker(threadId); }
                    if (firstStartMarkerNode) { firstStartMarkerPos = getOxoPosition(rootNode, firstStartMarkerNode); }

                    selectionStartPos = firstStartMarkerPos ? firstStartMarkerPos : startMarkerPos;
                    selectionEndPos = commentPos;

                } else {

                    // start or end range node are missing (this might happen after copy/paste) -> insert only a new comment without range
                    placeHolderNode = lastCommentInThread.getCommentPlaceHolderNode(); // -> using the position behind the preceeding comment
                    commentPos = increaseLastIndex(getOxoPosition(rootNode, placeHolderNode));

                    insertCommentOperation.start = commentPos;
                    insertCommentOperation.target = blockerTarget;
                    generator.generateOperation(COMMENT_INSERT, insertCommentOperation);

                    selectionStartPos = commentPos;
                    selectionEndPos = commentPos;
                }
            }

        }

        // inform the users, who have been mentioned in a comment
        if (mentions) { this.docApp.sendCommentNotification(mentions, text, commentId); }

        // apply all collected operations
        this.docModel.applyOperations(generator);

        // Setting the new selection (with comment and rangemarker nodes)
        if (selectionStartPos && selectionEndPos) { this.docModel.getSelection().setTextSelection(selectionStartPos, selectionEndPos); }

        // // only in text: scroll the comment in the viewport with an open softkeyboard
        // if (!this.docModel.useSlideMode() && SOFTKEYBORD_TOGGLE_MODE && IOS_SAFARI_DEVICE) { this.docApp.docView.scrollToSelection({ regardSoftkeyboard: true }); }
        return commentId;
    }

    /**
     * Checking, whether inside a specified node between optionally specified start and end offsets,
     * a comment place holder node is located, that contains unrestorable content.
     *
     * @param {Node} searchNode
     *  Node, in which comment place holder nodes are searched or a single place holder node.
     *
     * @param {Number} [startOffset]
     *  Start offset of given node. Must be specified together with 'endOffset'.
     *
     * @param {Number} [endOffset]
     *  End offset of given node. Must be specified together with 'startOffset'.
     *
     * @returns {Boolean}
     *  Whether inside the specified search node is a comment place holder node of a comment
     *  that contains unrestorable content.
     */
    checkUnrestorableCommentNode(searchNode, startOffset, endOffset) {

        // whether inside the search node is a comment place holder node of a comment that contains unrestorable content
        let containsUnrestorableContent = false;
        // the container of all place holder nodes
        const placeHolderNodes = isCommentPlaceHolderNode(searchNode) ? $(searchNode) : $(searchNode).find(COMMENTPLACEHOLDER_NODE_SELECTOR);

        _.each(placeHolderNodes, placeHolderNode => {

            // the ID of the removed comment
            const commentId = getTargetContainerId(placeHolderNode);
            // the comment model for the specified ID
            const commentModel = this.#commentCollection.getById(commentId);
            // whether a range is specified
            const isRangeSpecified = _.isNumber(startOffset) && _.isNumber(endOffset);
            // the logical comment position
            const commentPos = isRangeSpecified ? getOxoPosition(this.docModel.getRootNode(commentModel.getTarget()), placeHolderNode, 0) : null;
            // whether the comment is inside the specified range
            const isInsideRange = isRangeSpecified ? isNodePositionInsideRange(commentPos, startOffset, endOffset) : true;

            if (!isInsideRange) { return false; }

            if (!commentModel.isRestorable()) { containsUnrestorableContent = true; }

            // also checking the children (in ODT they might not be part of the selection)
            if (!containsUnrestorableContent && !commentModel.isReply()) {
                const allComments = this.#commentCollection.getThreadById(commentId);
                allComments.forEach(oneComment => { if (!oneComment.isRestorable()) { containsUnrestorableContent = true; } });
            }
        });

        return containsUnrestorableContent;
    }

    /**
     * New comment process for deleting comments or complete comment threads.
     *
     * This function generates all the operations to delete the comment placeholder nodes and the
     * corresponding rangeMarker nodes.
     *
     * @param {CommentModel} commentModel
     *  The comment model of that comment, that shall be deleted.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.all=false]
     *      If set to true, all comments in the document are removed.
     */
    deleteComment(commentModel, options) {

        // the operations generator
        const generator = this.docModel.createOperationGenerator();
        // the range markers object
        const rangeMarker = this.docModel.getRangeMarker();
        // whether all comments shall be removed
        const removeAll = getBooleanOption(options, 'all', false);
        // whether the specified node is the first node in a thread
        const isParentComment = removeAll ? false : !commentModel.isReply();
        // a collector for all comment models, that need to be deleted
        const allCommentModels = removeAll ? this.#commentCollection.getCommentModels() : (isParentComment ? this.#commentCollection.getThreadById(commentModel.getId()) : [commentModel]);
        // whether the user needs to be asked, because filtered comments are removed, too
        const askUser = isParentComment && this.#isActiveAuthorFilterList && this.#commentCollection.hasHiddenChild(commentModel);
        // the place holder position of the comment node
        let placeHolderPosition = null;
        // the target node of the saved place holder node
        let placeHolderRootNode = null;
        // the target of a saved place holder position
        let placeHolderTarget = '';
        // a collector for all positions (and targets) to be deleted
        const allPositions = {};
        // the number of previous removed elements in the paragraph containing the place holder node for the active comment
        let correction = 0;
        // caching the current target node (might be the deleted comment, but can also be any other target)
        const targetCache = this.#cacheTargetNode();
        // whether at least one comment cannot be restored
        let notRestorable = false;
        // the resulting promise
        let promise = null;

        // iterating over all comment models that will be removed and generate the delete operations
        // -> it is important, that the comment models are sorted in the container 'allCommentModels',
        //    because the delete operations are generated to delete from back to front.
        //    The correct sorting is possible, because the container for the comment models in the
        //    commentCollection is already sorted.

        if (!allCommentModels) { return; }

        /**
         * Collecting all positions that need to be removed, if one comment is removed.
         */
        const collectAllPositions = oneCommentModel => {

            // the id of the comment node
            const localCommentId = oneCommentModel.getId();
            // the place holder node for the comment
            const placeHolderNode = oneCommentModel.getCommentPlaceHolderNode();
            // the target in which the comment is located (only set for header and footer)
            const target = oneCommentModel.getTarget();
            // the root node of the comment place holder (this can be a marginal node)
            const rootNode = this.docModel.getRootNode(target);
            // the logical position of the place holder node (only searching in main document, ignoring target)
            const commentPosition = getOxoPosition(rootNode, placeHolderNode, 0);
            // the range start marker node
            const startMarkerNode = rangeMarker.getStartMarker(localCommentId);
            // the range end marker node
            const endMarkerNode = rangeMarker.getEndMarker(localCommentId);
            // the logical range start marker position
            const startMarkerPos = startMarkerNode ? getOxoPosition(rootNode, startMarkerNode) : null;
            // the logical range end marker position
            const endMarkerPos = endMarkerNode ? getOxoPosition(rootNode, endMarkerNode) : null;

            // saving the place holder for later setting of cursor
            if (!placeHolderPosition) {
                placeHolderPosition = _.clone(commentPosition);
                if (this.docApp.isODF()) {
                    placeHolderTarget = target;
                    placeHolderRootNode = rootNode;
                }
            }

            if (!allPositions[target]) { allPositions[target] = []; } // also using empty string as key
            allPositions[target].push(commentPosition);
            // deleting the comment and also the start range marker and the end range marker, if they exist
            if (endMarkerPos) { allPositions[target].push(endMarkerPos); }
            if (startMarkerPos) { allPositions[target].push(startMarkerPos); }
        };

        /**
         * counting the removed elements inside the paragraph that contains the place holder node for
         * an active comment node. This is necessary for setting the cursor to the position of the place
         * holder node after removal.
         */
        const countPreviousRemovedNodesInParagraph = (placeHolderPosition, allPositions) => {

            // the number of removed nodes in the paragraph before the place holder position
            let previousNodes = 0;
            // the logical position of the paragraph
            const paraPos = _.initial(placeHolderPosition);
            // the text position of the place holder node inside its paragraph
            const textPos = _.last(placeHolderPosition);

            _.each(allPositions, onePos => {
                if (_.isEqual(paraPos, _.initial(onePos)) && _.last(onePos) < textPos) {
                    previousNodes++;
                }
            });

            return previousNodes;
        };

        /**
         * Generating and applying the operations for deleting the comment place holder nodes and the range marker nodes.
         */
        const doDeleteComments = () => {

            // a collector for all affected targets
            const allDeletedTargets = [];

            _.each(allCommentModels, commentModel => {
                if (this.#commentCollection.isTemporaryCommentId(commentModel.getId())) { return; }
                collectAllPositions(commentModel);
                allDeletedTargets.push(commentModel.getId());
            });

            if (allDeletedTargets.length === 0) {
                if (removeAll && this.#commentCollection.containsTemporaryModel()) { this.#deleteTemporaryComment(); } // deleting also the temporary comment node
                return; // no operations required
            }

            // sorting and generating the 'delete' operations: comments and ranges might be in arbitrary order
            _.each(allPositions, (allPosInOneTarget, oneTarget) => {
                // sorting the positions for one specific target
                allPosInOneTarget.sort(comparePositions);
                // generating the delete operations
                if (!oneTarget) { oneTarget = this.docModel.getOperationTargetBlocker(); } // DOCS-2668, the header/footer can be active, if 'delete all' is used
                _.each(allPosInOneTarget, onePos => { generator.generateOperation(DELETE, { start: _.copy(onePos), target: oneTarget }); });
            });

            // reverting operation order -> delete from back to front
            generator.reverseOperations();

            // sending delete operation for the comment
            this.docModel.applyOperations(generator);

            // deleting also the temporary comment node
            if (removeAll && this.#commentCollection.containsTemporaryModel()) { this.#deleteTemporaryComment(); }

            // Setting cursor to the position of the placeholder (39945). It might be necessary to decrease the last index, because some nodes in the paragraph were removed.
            if (this.docApp.isODF() && placeHolderTarget) {  // the deleted comment is inside header or footer
                this.#pageLayout.enterHeaderFooterEditMode(placeHolderRootNode);
                this.docModel.getSelection().setNewRootNode(placeHolderRootNode);
                this.docModel.setActiveTarget(placeHolderTarget);
            }

            correction = countPreviousRemovedNodesInParagraph(placeHolderPosition, allPositions['']); // not required in marginal node
            if (correction > 0) {
                placeHolderPosition[placeHolderPosition.length - 1] -= correction;
                if (this.docApp.isODF() && _.last(placeHolderPosition) < 0) { placeHolderPosition[placeHolderPosition.length - 1] = 0; } // TODO handle positions in header/footer
            }
            this.docModel.getSelection().setTextSelection(placeHolderPosition);

            // activating changeTracking again, because this comment node is no longer active
            if (targetCache.activeTarget) { this.docModel.getChangeTrack().setChangeTrackSupported(true); }
        };

        // asking the user, if some filtered comments will be removed, too.
        // Or if the user want to delete all comments.
        if (askUser || removeAll) {
            // choose the appropriate dialog text for 'askUser' or 'removeAll'
            const msg = askUser ? gt('At least one filtered comment will be removed, too. Do you want to continue?') : gt('All comments in the document will be removed. Do you want to continue?');
            promise = this.docModel.showDeleteWarningDialog(gt('Delete comments'), msg);
            promise.fail(() => { this.docApp.docView.grabFocus(); }); // return focus to editor after 'No' button
        } else {
            promise = $.when();
        }

        // check, if there is at least one not restorable comment in the collection of the deleted commentModels
        notRestorable = !!_.find(allCommentModels, commentModel => { return !commentModel.isRestorable(); });

        // delete contents on resolved promise
        return promise.then(() => { return this.docModel.askUserHandler(notRestorable, false); }).then(doDeleteComments);
    }

    /**
     * Removing one comment element from the comment layer node. The parameter
     * is the place holder node in the page content.
     *
     * @param {jQuery} placeHolderNode
     *  The place holder node in the page content
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.keepDrawingLayer=false]
     *      If set to true, the comment in the comment layer and in the comment model
     *      corresponding to the comment place holder is NOT removed. This is for
     *      example necessary after a split of paragraph. The default is 'false' so
     *      that place holder node and the comment in comment layer and the comment
     *      model are removed together.
     */
    removeFromCommentLayer(placeHolderNode, options) {

        // the ID of the removed comment
        const commentId = getTargetContainerId(placeHolderNode);
        // whether the comments in the comment layer shall not be removed -> this is necessary after splitting a paragraph
        const keepDrawingLayer = getBooleanOption(options, 'keepDrawingLayer', false);
        // whether the comment layer node was visible before removing the comment
        let visibleBefore = true;
        // the old width of the comment layer
        let oldWidth = 0;
        // whether the comment is in header or footer
        const isMarginalComment = false;
        // the comment model of the deleted comment
        let commentModel = null;

        // deleting the placeholder node
        $(placeHolderNode).remove();

        // additional operations after fast load
        // -> this will be handled in function 'this.#fillModelAfterFastLoad'
        // -> but the placeholder must be deleted, so that it cannot be found in 'this.#fillModelAfterFastLoad'
        if (this.docModel.isFastLoadImport() && !this.docModel.isImportFinished() && !this.#commentModelFilled)  { return; }

        if (!keepDrawingLayer) {

            // TODO:
            // - handling of marginal comments

            visibleBefore = this.#isCommentLayerVisible();
            oldWidth = visibleBefore ? this.#commentLayerNode.width() : 0;

            // removing content from the commentModel and commentCollection classes
            commentModel = this.#commentCollection.handleDeleteComment(commentId);

            // removing the marker for the comment in edit mode
            if (this.#commentEditorActive === commentId) { this.#commentEditorActive = null; }

            // deleting the highlighting of the range and the connection line
            this.docModel.getRangeMarker().removeHighLightRange(commentId);

            if (commentModel) {
                // removing a visible bubble node in bubble mode
                this.#removeCommentBubble(commentModel);

                // updating a buble node in bubble mode
                if (commentModel.getParentId()) { this.#updateBubbleNode(commentModel); }

                // destroy the comment model object
                commentModel.destroy();
            }

            // removing comments layer, if it is empty now
            this.#hideCommentLayerIfEmpty();

            // inform others, that a visible comment layer was removed
            if (visibleBefore && this.isEmpty()) { this.trigger('commentlayer:removed', { width: oldWidth }); }

            if (this.#isActiveAuthorFilterList) { this.#applyAuthorFilter(); } // DOCS-2870

            // new ordering of comments in side pane
            if (!this.isEmpty()) {
                if (this.docModel.isUndoRedoRunning()) { // DOCS-2639
                    this.#undoManager.one('undo:after redo:after', () => { this.#updateCommentsAndRanges({ ignoreMissing: isMarginalComment }); }); // Info: options are not evaluated now
                } else {
                    this.#updateCommentsAndRanges({ ignoreMissing: isMarginalComment }); // Info: options are not evaluated now
                }
            }
        }
    }

    /**
     * Removing all comments from the comment layer, that have place holder nodes
     * located inside the specified node. The node can be a paragraph that will be
     * removed. In this case it is necessary, that the comments in the comment layer
     * are removed, too.
     * Info: The ranges corresponding to the comments are not handled inside this
     * function.
     *
     * @param {HTMLElement|jQuery} node
     *  The node, for which all comments in the comment layer will be removed. This
     *  assumes that the node contains comment place holder nodes. Only the comments
     *  in the comment layer will be removed. This function does not take care of the
     *  comment place holder nodes itself.
     */
    removeAllInsertedCommentsFromCommentLayer(node) {

        // the collection of all comment place holder nodes
        let allPlaceHolders = $(node).find(COMMENTPLACEHOLDER_NODE_SELECTOR);
        // whether the comment layer node is visible before removal
        let visibleBefore = false;
        // the old width of the comment layer node
        let oldWidth = 0;

        // starting to remove the comments, if there are place holder nodes
        if (allPlaceHolders.length > 0) {

            // check if the comment layer is visible
            visibleBefore = this.#isCommentLayerVisible();
            oldWidth = visibleBefore ? this.#commentLayerNode.width() : 0;

            // reverting the order, so that children are removed before the main comment in the thread is removed
            if (allPlaceHolders.length > 1) { allPlaceHolders = allPlaceHolders.toArray().reverse(); }

            _.each(allPlaceHolders, placeHolderNode => {
                this.removeFromCommentLayer(placeHolderNode);
            });

            // removing comments layer, if it is empty now
            this.#hideCommentLayerIfEmpty();

            // inform others, that a visible comment layer was removed
            if (visibleBefore && this.#comments.length === 0) {
                this.trigger('commentlayer:removed', { width: oldWidth });
            }

            // ...  and restoring the sorted list of all place holders
            this.#updatePlaceHolderCollection();
        }
    }

    /**
     * Generating the function to change the content of a comment.
     * This is supported by the new OX Text model using mentions.
     *
     * @param {CommentModel} commentModel
     *  The comment model of that comment, that shall be deleted.
     *
     * @param {Object} commentInfo
     *  Additional data for the new comment, like 'mentions' and 'text'.
     */
    changeComment(commentModel, commentInfo) {

        // the operations generator
        const generator = this.docModel.createOperationGenerator();
        // the place holder node of the comment
        const placeHolderNode = commentModel.getCommentPlaceHolderNode();
        // the target in which the comment is located (only set for header and footer)
        let target = commentModel.getTarget();
        // the root node of the comment place holder (this can be a marginal node)
        const rootNode = this.docModel.getRootNode(target);
        // the logical position of the place holder node
        const commentPosition = getOxoPosition(rootNode, placeHolderNode, 0);
        // the options for the changeComment operation
        const operationOptions = { start: _.copy(commentPosition) };

        if (commentInfo.text && _.isString(commentInfo.text)) { operationOptions.text = commentInfo.text; }
        if (commentInfo.mentions) { operationOptions.mentions = this.#reduceMentionContentForOperation(commentInfo.mentions); }
        if (!target) { target = this.docModel.getOperationTargetBlocker(); } // the header/footer might be active
        if (target) { operationOptions.target = target; }

        generator.generateOperation(COMMENT_CHANGE, operationOptions);

        this.docModel.applyOperations(generator);

        // TODO: Setting the selection
    }

    /**
     * Handler function for the 'changeComment' operation.
     *
     * @param {OperationContext} context
     *  A wrapper representing the `changeDrawing` operation.
     *
     * @param {Object} oldState
     *  A helper object for the properties 'text' and 'mentions'. This object is used to
     *  generate the undo operation.
     *
     * @returns {Boolean}
     *  Whether the comment has been changed successfully.
     */
    changeCommentHandler(context, oldState) {

        // the position of the comment placeholder node
        const position = context.getPos('start');
        // the target of the comment placeholder node
        const target = context.optStr('target');
        // the root node of the comment placeholder node
        const rootNode = this.docModel.getRootNode(target);
        // the node at the specified position
        const nodeInfo = getDOMPosition(rootNode, position, true);

        if (!nodeInfo || !isCommentPlaceHolderNode(nodeInfo.node)) {
            globalLogger.error('CommentLayer.changeCommentHandler: Failed to find comment placeholder node at position: ' + JSON.stringify(position));
        }

        // the comment placeholder node in the document
        const placeHolderNode = nodeInfo.node;
        // the ID of the comment node
        const id = getTargetContainerId(placeHolderNode);

        // additional operations after fast load
        // -> this will be handled in function 'this.#fillModelAfterFastLoad'
        // -> but the ID is now required to update the correct comment model
        if (this.docModel.isFastLoadImport() && !this.docModel.isImportFinished() && !this.#commentModelFilled) {
            this.#fastLoadChangeCommentOperations[id] = context;
            return true;
        }

        // the comment model for the specified ID
        const commentModel = this.#commentCollection.getById(id);
        // the old text of the comment
        const oldText = commentModel.getText();
        // the old mentions in the comment
        const oldMentions = commentModel.getMentions();
        // the new text in the comment
        const newText = context.optStr('text');
        // the new mentions in the comment
        const newMentions = context.optArr('mentions');

        if (_.isString(newText)) { commentModel.setText(newText); }

        // docs-3902, a changeComment operation without mentions needs to remove old mentions
        commentModel.setMentions(newMentions);

        // this comment no longer needs to be marked with 'restorable' flag set to 'false'
        if (!commentModel.isRestorable()) { commentModel.setRestorable(true); }

        // saving the old value for the undo operation
        if (_.isString(oldText)) { oldState.text = oldText; }
        if (oldMentions) { oldState.mentions = oldMentions; }

        this.#updateCommentsAndRangesInAllMode();

        // informing the view to update
        this.docApp.getModel().trigger('change:threadedcomment', commentModel);
        return true;
    }

    /**
     * An existing connection line between comment and a highlighted comment context needs
     * to be updated, if for example an external operation, triggers a repaint of the range
     * markers. In this case it is necessary, that also the connection line is repainted.
     * This function is therefore triggered from the range marker class.
     *
     * @param {String} id
     *  The unique id of the comment node (and range marker). This can be used to identify the
     *  corresponding comment node.
     *
     * @param {Object} startPos
     *  An object containing the properties x and y. These represent the pixel position of
     *  the upper left corner of the range, that will be connected to the comment. The pixel
     *  positions are received using the jQuery 'offset' function and relative to the page.
     *
     * @param {jQuery} allOverlayChildren
     *  A collection with all children of the range overlay node. This is only required for
     *  restoring the connection line nodes with the correct class names.
     *
     * @param {Boolean} [alreadyInserted=false]
     *  Whether the temporary comment range marker nodes were already inserted by the
     *  calling function(s).
     */
    updateCommentLineConnection(id, startPos, allOverlayChildren, alreadyInserted) {

        // the threaded comment object that is stored in the comments pane
        const threadedComment = this.#getThreadFromId(id);
        // the thread comment node
        const threadCommentNode = threadedComment ? threadedComment.getNode() : null;
        // a string containing all classes of the connection lines
        let allClassString = '';

        if (threadCommentNode) {
            // the classes need to be restored
            if (allOverlayChildren && allOverlayChildren.length > 0) {

                // reverse order -> click nodes behind hover nodes
                ary.some(allOverlayChildren.get(), child => {
                    if (getRangeMarkerId(child) === id && $(child).is(this.#commentLineSelector)) {
                        allClassString = $(child).attr('class'); // creating string with all classes
                        this.#drawCommentLineConnection(threadCommentNode, startPos, id, allClassString, alreadyInserted);
                        return true; // exit the loop early
                    }
                }, { reverse: true });
            }
        }
    }

    /**
     * After splitting a paragraph with comment place holder in the new paragraph (that
     * was cloned before), it is necessary that the comments in the comment model
     * update their references to the place holder comment in the new paragraph.
     *
     * @param {Node|jQuery} paragraph
     *  The paragraph node.
     */
    repairLinksToPageContent(paragraph) {

        // setting the search function for comment place holders
        const searchFunction = isParagraphNode(paragraph) ? 'children' : 'find';

        _.each($(paragraph)[searchFunction](COMMENTPLACEHOLDER_NODE_SELECTOR), placeHolder => {
            const commentId = getTargetContainerId(placeHolder);
            const commentModel = this.#commentCollection.getById(commentId);
            if (commentModel) { commentModel.setCommentPlaceHolderNode(placeHolder); } // updating the reference to the place holder node in the comment model
        });

        // restoring the sorted list of all place holders
        this.#updatePlaceHolderCollection();
    }

    /**
     * For zoom levels smaller than 100, the this.docApp content node (the parent of the page) gets the
     * property 'overflow' set to hidden. Without an additional padding, the comment will become
     * invisible. Therefore it is necessary to check for an additional padding, if the zoom
     * level is changed.
     *
     * @returns {String}
     *  The width of the additional padding, that is necessary, so that the comment stay visible.
     */
    getAppContentPaddingWidth() {
        return this.#comments.length > 0 ? this.#commentDefaultWidth + 'px' : '0px';
    }

    /**
     * When the document is saved in the local storage, it is necessary to remove all classes that
     * handle a temporary highlighting of a comment node. This is not neccessary for the visible
     * comment range or the line connection to the comment, because these nodes are in an overlay
     * node, that is not stored in the local storage.
     */
    clearCommentThreadHighlighting() {

        // helper function for iteration
        const removeColorClasses = commentThread => {
            // removing all classes 'comment-thread-hovered-author-X'
            for (let index = 1, count = getSchemeColorCount(); index <= count; index += 1) {
                $(commentThread).removeClass(`comment-thread-hovered-author-${index} comment-author-${index}`);
            }
        };

        // only necessary, if a comment layer node exists
        if (this.#commentLayerNode) {
            // iterating over all comments
            _.each(this.#commentLayerNode.children(), oneCommentThread => {
                removeColorClasses(oneCommentThread);
                // removing also highlight classes at comment nodes
                $(oneCommentThread).find('.commentbuttonactive').removeClass('commentbuttonactive');
            });
        }
    }

    /**
     * When pasting content, that contains comments, it is necessary, that the comments get a new
     * unique target (and all operations for content inside the comment).
     *
     * @param {Object[]} operations
     *  The collection with all paste operations.
     */
    handlePasteOperationTarget(operations) {

        // a container for all comment ids
        const allTargets = [];
        // a container for all parentIds
        const allParentIds = {};
        // a container for all range marker ids of comments
        const allRangeMarkerTargets = {};

        // replacing the comment ids
        const replaceTarget = (ops, target1, target2) => {

            _.each(ops, op => {
                if (op) {
                    if (op.id === target1) { op.id = target2; }
                    if (op.target === target1) { op.target = target2; }
                    // also exchanging the parentId might be required (DOCS-1575)
                    if (op.name === COMMENT_INSERT && op.parentId && op.parentId === target1 && allParentIds[target1] && allParentIds[target1] !== 1) {
                        op.parentId = allParentIds[target1];
                    }
                }
            });
        };

        // collecting all 'id's of insertComment operations and assign new target values
        _.each(operations, operation => {
            if (operation && operation.name === COMMENT_INSERT) {
                allTargets.push(operation.id);
                // also collecting the parent id is required
                if (operation.parentId) { allParentIds[operation.parentId] = 1; }
            } else if (operation && operation.name === RANGE_INSERT && operation.type === 'comment') {
                allRangeMarkerTargets[operation.id] = 1;
            }
        });

        // iterating over all registered comment ids
        if (allTargets.length > 0) {
            _.each(allTargets, oldTarget => {
                const newTarget = this.getNextCommentID();
                if (allParentIds[oldTarget]) { allParentIds[oldTarget] = newTarget; }
                // iterating over all operations, so that also the range markers will be updated
                replaceTarget(operations, oldTarget, newTarget);
                delete allRangeMarkerTargets[oldTarget];
            });
        }

        // are there still unmodified range marker targets (maybe only the start marker was pasted)
        _.each(_.keys(allRangeMarkerTargets), oldTarget => {
            const newTarget = this.getNextCommentID();
            // iterating over all operations
            replaceTarget(operations, oldTarget, newTarget);
        });

    }

    /**
     * Typically comments can only be pasted into the main document. In this case the
     * comment node together with its range node(s) and the content inside the comment
     * will be pasted. But if the paste target is not the main document, but for example
     * a header or footer or another comment, some information need to be removed. In this
     * case the comment information will not be pasted.
     *
     * This means that the operations that have the target property set to a comment id
     * will be removed. Additionally the insertComment and insertRange operations that
     * have the specified id(s) need to be replaced by insertText operations of length 1.
     * This is the secure version, that avoids to remove insertComment or insertRange,
     * because otherwise the logical positions of all following operations need to be
     * recalculated. This functionality is based on 40089.
     *
     * Info: The inserted array of operations will be returned modified from this function.
     *
     * @param {Object[]} operations
     *  A list of all paste operations
     *
     * @param {Boolean} isPasteToSameDocumentType
     *  Whether the pasting happens into the same document type. For example from
     *  odt to odt or from docx to docx.
     *
     * @param {OperationGenerator} deleteReplacementsGenerator
     *  a generator that holds all delete operations that finally remove the spaces
     *  added by replacement operations.
     *
     * @returns {Object[]}
     *  A (modified) list of all paste operations
     */
    handleCommentOperationsForPasting(operations, isPasteToSameDocumentType, deleteReplacementsGenerator) {

        // a list of all comment IDs
        const allIDs = this.#collectAllCommentIDsFromOperations(operations);
        // the new inserted text instead of comment node or range node
        const text = ' ';
        // the collector for the new operations
        const newOps = {};

        // iterating over all collected comment IDs
        if (allIDs && allIDs.length > 0) {

            // removing all operations that have the specified ids as target property
            // -> this might happen for old comments pasted from older OX Text versions
            _.each(allIDs, id => {
                operations = _.reject(operations, operation => {
                    return (operation.target && operation.target === id);
                });
            });

            // ... collecting the insertComment and insertRange operations with the specified ids
            _.each(allIDs, id => {
                _.each(operations, (operation, index) => {
                    if ((operation.name === COMMENT_INSERT || operation.name === RANGE_INSERT) && operation.id === id) {
                        const newOp = { name: TEXT_INSERT, text, start: _.clone(operation.start) };
                        const newDeleteOp = { start: _.clone(operation.start) };

                        if (operation.target) {
                            newOp.target = operation.target;
                            newDeleteOp.target = operation.target;
                        }

                        newOps[index] = newOp;

                        // and adding the delete operations in sorted reversed order
                        // -> but not using delete operations, if the document type is modified (54362)
                        if (isPasteToSameDocumentType) {
                            deleteReplacementsGenerator.generateOperation(DELETE, newDeleteOp);
                        }
                    }
                });

            });

            // ... and finally replacing the operations in the array
            _.each(newOps, (newOp, index) => {
                operations[index] = newOp;
            });
        }

        return operations;
    }

    /**
     * Check, whether the content specified by the paste operations can be pasted into the text document,
     * dependant on the document format (OOXML or ODF).
     *
     * @param {Object[]} operations
     *  A list of all paste operations
     *
     * @returns {Boolean}
     *  Whether the comments can be pasted into the document
     */
    checkTextCommentContent(operations) {
        return this.docApp.isODF() ? this.checkODTCommentContent(operations) : this.checkDOCXCommentContent(operations);
    }

    /**
     * Check, whether the content specified by the paste operations can be pasted into an ODT document.
     * This is for example not possible if there are old style operations in the operations list, that
     * use a comment ID as target. If this is the case, the comment will be removed from paste
     * operations completely.
     *
     * @param {Object[]} operations
     *  A list of all paste operations
     *
     * @returns {Boolean}
     *  Whether the comments can be pasted into the document
     */
    checkODTCommentContent(operations) {

        //  whether the operations contain valid content for ODT comments
        let isValidContent = true;

        // checking, if there are operations, that have the comment ID as target (old style comments in OX Text)
        if (this.#hasOldStyleCommentOperation(operations)) { isValidContent = false; }

        return isValidContent;
    }

    /**
     * Check, whether the content specified by the paste operations can be pasted into a DOCX document.
     * This is for example not possible, if there are comments inside shapes (54127). Such a document could
     * be created in another application. If this is the case, the comment will be removed from paste
     * operations completely.
     *
     * @param {Object[]} operations
     *  A list of all paste operations
     *
     * @returns {Boolean}
     *  Whether the comments can be pasted into the document
     */
    checkDOCXCommentContent(operations) {

        //  whether the operations contain valid content for ODT comments
        let isValidContent = true;
        // a list of all comment IDs
        const allDrawingPositions = this.#collectAllDrawingPositionsFromOperations(operations);

        // checking, if the comment is inserted into a drawing -> this is not allowed in DOCX files
        if (allDrawingPositions && allDrawingPositions.length > 0) {
            _.each(operations, operation => {
                if (operation.name === COMMENT_INSERT && _.find(allDrawingPositions, oneDrawingPos => { return hasSameParentComponentAtSpecificIndex(operation.start, oneDrawingPos, oneDrawingPos.length); })) {
                    isValidContent = false;
                }
            });
        }

        // checking, if there are operations, that have the comment ID as target (old style comments in OX Text)
        if (isValidContent && this.#hasOldStyleCommentOperation(operations)) { isValidContent = false; }

        return isValidContent;
    }

    /**
     * Check, whether start range marker AND end range marker are inserted for a comment in OOXML.
     * If only one of these range marker nodes is pasted, the other one is removed from the paste
     * operations.
     *
     * @param {Object[]} operations
     *  A list of all paste operations
     *
     * @param {Boolean} isPasteToSameDocumentType
     *  Whether the pasting happens into the same document type. For example from
     *  odt to odt or from docx to docx.
     *
     * @param {OperationGenerator} deleteReplacementsGenerator
     *  a generator that holds all delete operations that finally remove the spaces
     *  added by replacement operations.
     *
     * @returns {Object[]}
     *  A (modified) list of all paste operations
     */
    handleMissingRangeMarkersForPasting(operations, isPasteToSameDocumentType, deleteReplacementsGenerator) {

        // the new inserted text instead of the range node
        const text = ' ';
        // the counter object for the range marker nodes
        const rangeCounter = {};
        // the collector for the new operations
        const newOps = {};

        // ... collecting the insertRange operations with the specified ids -> there must be one for the
        // start range marker and one for the end range marker.
        _.each(operations, operation => {
            if (operation.name === RANGE_INSERT && operation.type === 'comment') {
                if (rangeCounter[operation.id] === 1) {
                    rangeCounter[operation.id] = 2;
                } else {
                    rangeCounter[operation.id] = 1;
                }
            }
        });

        // removing all insertRange operations that insert only start or only end marker node
        _.each(operations, (operation, index) => {
            if ((operation.name === RANGE_INSERT) && rangeCounter[operation.id] === 1) {
                const newOp = { name: TEXT_INSERT, text, start: _.clone(operation.start) };
                const newDeleteOp = { start: _.clone(operation.start) };

                if (operation.target) {
                    newOp.target = operation.target;
                    newDeleteOp.target = operation.target;
                }

                newOps[index] = newOp;

                // and adding the delete operations in sorted reversed order
                // -> but not using delete operations, if the document type is modified (54362)
                if (isPasteToSameDocumentType) {
                    deleteReplacementsGenerator.generateOperation(DELETE, newDeleteOp);
                }
            }
        });

        // ... and finally replacing the operations in the array
        _.each(newOps, (newOp, index) => {
            operations[index] = newOp;
        });

        return operations;
    }

    /**
     * Switching the display type for the comments. Allowed parameters for the
     * specified mode are the values defined in 'CommentLayer.DISPLAY_MODE'.
     *
     * @param {String} mode
     *  The mode that defines the highlighting and the visibility of the comments.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.forceSwitch=false]
     *      If set to true, the display mode of the comment layer is set, even if
     *      it is not modified. This is especially required, when a new comment
     *      layer is created, after a previous comment layer was removed with
     *      deleting the last comment.
     */
    switchCommentDisplayMode(mode, options) {

        // whether the comment layer is visible before the switch
        let isVisibleBefore = false;
        // whether the comment layer is visible after the switch
        let isVisibleAfter = false;
        // whether it is necessary to force a switch of the view mode
        const forceSwitch = getBooleanOption(options, 'forceSwitch', false);
        // the ID of a selected comment
        const selectedCommentId = this.#commentCollection.getSelectedCommentId();
        // the width of a visible comment layer node
        let oldWidth = 0;

        if (this.#displayMode === mode && !forceSwitch) { return; } // nothing to do

        // registering, if commentlayer is visible before switch
        isVisibleBefore = this.#isCommentLayerVisible();

        // deselecting an optionally selected comment -> will be restored later
        if (selectedCommentId) { this.#commentCollection.deselectComment(); }

        // saving the width of the comment layer, if it is visible
        oldWidth = this.#commentLayerNode && this.#commentLayerNode.width ? this.#commentLayerNode.width() : 0;

        // leaving bubble mode, if it is currently active
        // -> this can be used to clean up bubbles and connections between threads and bubbles
        if (this.#displayMode === CommentLayer.DISPLAY_MODE.BUBBLES) { this.#leaveBubbleMode(); }
        if (mode === CommentLayer.DISPLAY_MODE.BUBBLES && this.#displayMode !== CommentLayer.DISPLAY_MODE.BUBBLES) { this.#enterBubbleMode(); } // DOCS-2936

        // setting the new display mode
        this.#displayMode = mode;

        // informing the text comments pane about the new display mode
        this.docApp.docView.trigger('switch:displaymode', this.#displayMode);

        // if a author filter is active, it need to be applied
        // -> in bubble mode not all bubbles will be visible
        // -> if comment layer is visible, empty threads are not displayed
        if (this.#isActiveAuthorFilterList) { this.#applyAuthorFilter(); }

        // updating of horizontal and vertical positions in comment layer required
        this.#updateCommentsLayer();

        // some additional tasks that are required after switching the display mode
        this.#afterSwitchDisplayModeTasks(selectedCommentId, mode);

        // after setting display mode, it can be checked if the comment layer is visible after switch
        isVisibleAfter = this.#isCommentLayerVisible();

        // ... inform others about changed visibility of comment layer
        if (isVisibleBefore && !isVisibleAfter) {
            this.trigger('commentlayer:removed', { width: oldWidth });
            this.docApp.getController().update(); // if this was triggered by the closer in the comments pane, a controller update is required, to show the new state
        } else if (isVisibleAfter && !isVisibleBefore) {
            this.trigger('commentlayer:created', { width: this.#commentLayerNode.width() });
        }
    }

    /**
     * Selecting and activating the next or the previous comment from the comment layer.
     * If there is no comment, nothing happens.
     * If there is only one comment and this is not active, it will be activated.
     * If there is only one comment and this is already active, nothing happens.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.next=true]
     *      If set to true, the following comment will be activated, otherwise the previous
     *      comment.
     *      If set to true and no comment is active, the first comment will be activated.
     *      Otherwise the last comment will be activated.
     */
    selectNextComment(options) {

        // whether the next or the previous comment shall be selected
        const next = getBooleanOption(options, 'next', true);
        // the comment model to be selected
        let commentModel = null;
        // a comment info object as returned by 'this.#commentCollection.getNeighbourCommentModelById'.
        // -> it contains the properties 'commentModel' for the model and 'jump', if a jump in the
        //    document was required to find the comment.
        let commentInfo = null;
        // the number of (visible) comments in the comments pane
        const visibleCommentsCount = this.#isActiveAuthorFilterList ? this.#commentCollection.getVisibleCommentNumber() : this.#commentCollection.getCommentNumber();

        // nothing to do (no visible comments)
        if (visibleCommentsCount === 0) { return; }

        if (visibleCommentsCount === 1) {
            commentModel = this.#isActiveAuthorFilterList ? this.#commentCollection.getVisibleCommentModels()[0] : this.#commentCollection.getByIndex(0);
        } else {

            // using the ordered list of comment models in the comment collector object.
            // Is currently a comment selected? Then select next or previous (not filtered) comment thread.
            // Is no comment selected? Then select first or last (not filtered) comment thread.

            // is there already a selected comment node?
            const selectedCommentId = this.#commentCollection.getSelectedCommentId();

            // is the selection still valid? -> this check should no longer be required.
            // if (selectedCommentId) {
            //     // the threaded comment object that is stored in the comments pane
            //     selectedCommentThread = this.#getThreadFromId(selectedCommentId);
            //     // this might no longer be a selected comment thread
            //     if (!selectedCommentThread.getNode().hasClass('selected')) { selectedCommentId = null; }
            // }

            if (selectedCommentId) {
                commentInfo = this.#commentCollection.getNeighbourCommentModelById(selectedCommentId, { next, onlyThreads: true, onlyVisible: true });
                commentModel = commentInfo.commentModel;
            } else {
                commentModel = this.#commentCollection.getByIndex(next ? 0 : (visibleCommentsCount - 1), { onlyVisible: true });
            }

            if (selectedCommentId && commentModel && commentInfo.jump) {
                // inform the user about the jump in the comments pane
                this.docApp.docView.yell({
                    type: 'info',
                    message: (next ? gt('The search continues from the beginning of this document.') : gt('The search continues from the end of this document.'))
                });
            }
        }

        if (commentModel) {

            // switching to the default display mode, if 'none' is currently active
            if (this.#displayMode === CommentLayer.DISPLAY_MODE.HIDDEN) {
                this.switchCommentDisplayMode(this.#defaultDisplayMode);
            }

            // selecting the comment
            this.#commentCollection.selectComment(commentModel);

            // activating the comment thread in bubble mode
            if (this.#isBubbleMode) { this.#showCommentThreadInBubbleMode(commentModel); }
        }

    }

    /**
     * Debug: Whether the comment model was filled within refreshCommentLayer in fastLoad process (67242).
     */
    isCommentModelFilled() {
        return this.#commentModelFilled;
    }

    /**
     * Debug: Saving the html string sent from the server in fastLoad process (67242).
     */
    saveCommentHtmlString(htmlString) {
        this.#fastLoadString = htmlString;
    }

    /**
     * Enabling the bubble mode for the comment layer. The comment layer itself is no longer visible,
     * but the bubble layer is filled with comment bubbles. Those bubbles have a hover handler to show
     * the comment range and a click handler to show the comment thread.
     *
     * @param {JQuery} commentThread
     *  The comment thread node in the comment layer.
     */
    activateBubbleMode(commentThread) {

        // the id of the main comment in the thread
        const commentId = commentThread.attr('data-comment-id');
        // the comment model of the specified ID
        const commentModel = this.#commentCollection.getById(commentId);
        // whether the specified comment contains at least one reply
        const hasReply = this.#commentCollection.hasReply(commentModel);
        // the author of the main comment
        const commentAuthor = commentModel.getAuthor();
        // the bubble node for each comment thread
        let bubbleNode = null;

        // preparing the bubble mode
        this.#enterBubbleMode();

        // activating the commplete border around the thread node
        commentThread.removeClass('highlightallcomments');

        // making each comment thread invisible (so it can be made visible individually)
        this.#hideCommentThread(commentThread);

        // deactivating handlers at comment threads
        this.#deactivateAllHandlersAtCommentThread(commentThread);

        // removing an optionally existing bubble node with the same comment ID
        if (this.#bubbleNodeCollector[commentId]) { this.#removeBubbleNode(this.#bubbleNodeCollector[commentId], commentId); }

        // receiving a bubble node
        bubbleNode = this.#createBubbleNode(commentId, { multiComment: hasReply, colorClassName: this.#getCommentColorClassName(commentAuthor) });

        // taking care of the visibility of the comment thread
        bubbleNode.toggleClass('isFilteredThread', commentThread.hasClass('isFilteredThread'));

        // appending bubble node into bubble layer
        this.#bubbleLayerNode.append(bubbleNode);

        // collecting the bubble nodes
        this.#bubbleNodeCollector[commentId] = bubbleNode;
    }

    /**
     * Callback function that is called with every thread node.
     *
     * @param {Query} commentThread
     *  The comment thread node in the comment layer.
     */
    hightlightAllComments(commentThread) {

        // activating the commplete border around the thread node
        commentThread.addClass('highlightallcomments');

        // deactivating all handlers to avoid multiple registrations
        this.#deactivateAllHandlersAtCommentThread(commentThread);

        // drawing the 'hover'-highlight for each comment thread without hover event
        // -> but not if the thread is not visible because of filters
        if (!commentThread.hasClass('isFilteredThread')) { this.#drawCommentRangeHover(commentThread); }
    }

    /**
     * Disabling the highlighting for the comments. The ranges are not displayed and the
     * connection lines are not drawn from the ranges to the comments. If the option
     * 'hidden' is set to true, the comment layer becomes invisible.
     *
     * @param {jQuery} commentThread
     *  The comment thread node in the comment layer.
     */
    highlightNoComments(commentThread) {

        // activating the commplete border around the thread node
        commentThread.removeClass('highlightallcomments');

        // deactivating all handlers at comment thread
        this.#deactivateAllHandlersAtCommentThread(commentThread);

        // remove hover highlighting at comment thread without hover event
        this.#removeCommentRangeHover(commentThread);
    }

    /**
     * Enable the highlighting for the selected and the hovered comments. The ranges are displayed
     * and the connection lines are drawn from the ranges to the comments only for those specific
     * comments.
     *
     * @param {jQuery} commentThread
     *  The comment thread node in the comment layer.
     */
    highlightSelectedComments(commentThread) {

        // activating the commplete border around the thread node
        commentThread.removeClass('highlightallcomments');

        // deactivating all handlers to avoid multiple registrations
        this.#deactivateAllHandlersAtCommentThread(commentThread);

        // remove hover highlighting at comment thread without hover event
        this.#removeCommentRangeHover(commentThread);

        // activating handler for hover over comment threads
        $(commentThread).hover(this.#drawCommentRangeHoverHandler, this.#removeCommentRangeHoverHandler);
    }

    /**
     * Deactivating all handlers for one specified comment thread.
     *
     * @param {jQuery} commentThread
     *  The comment thread node in the comment layer, whose handlers will
     *  be deactivated.
     */
    deactivateAllHandlersAtCommentThread(commentThread) {
        this.#deactivateAllHandlersAtCommentThread(commentThread);
    }

    /**
     * Handling for temporary inserted comment models, that are generated for new
     * created comment threads.
     *
     * @returns {Number}
     *  The position of the temporary range marker in the collection of all
     *  range marker nodes.
     */
    getIndexForNewCommentThread() {

        // the index of the new inserted range marker node
        let index = 0;
        // whether the index was found
        let foundIndex = false;
        // a collector for the found comment IDs
        const commentIDs = [];

        // inserting a temporary range marker at exactly the start position
        this.docModel.getRangeMarker().insertTemporaryRangeMarker(this.#commentCollection.getTemporaryModel(), { onlyStart: true });

        // the children of the page node, in which comment place holders can be located
        const localcontentChildren = (this.docApp.isODF() ? getContentChildrenOfPage(this.docModel.getNode()) : getPageContentNode(this.docModel.getNode()));
        // the collector for rangemaker start nodes and comment place holder nodes
        const startCollector = localcontentChildren.find(RANGEMARKER_STARTTYPE_SELECTOR + ', ' + COMMENTPLACEHOLDER_NODE_SELECTOR);

        // finding the new generated range marker in the jQuery collector to determine its position
        _.each(startCollector, node => {
            if (foundIndex) { return; }
            let currentID = null;
            if (isCommentPlaceHolderNode(node)) {
                currentID = getTargetContainerId(node);
                if (!_.contains(commentIDs, currentID)) {
                    commentIDs.push(currentID);
                    index++;
                }
            } else if (isRangeMarkerStartNode(node) && getRangeMarkerType(node) === 'comment') {
                currentID = getRangeMarkerId(node);
                if (!_.contains(commentIDs, currentID)) {
                    if (this.#commentCollection.isTemporaryCommentId(currentID)) {
                        foundIndex = true;
                    } else {
                        commentIDs.push(currentID);
                        index++;
                    }
                }
            }
        });

        // deleting the range marker again (without operation)
        // -> full delete handling is required (instead of node.remove()) to handle range marker model
        //    correctly and to merge text spans again
        this.docModel.getRangeMarker().removeTemporaryRangeMarker({ onlyStart: true });

        return foundIndex ? index : -1;
    }

    /**
     * Additional todos after creating a new comment thread with a temporary comment model.
     *
     * @param {CommentModel} commentModel
     *  The (temporary) comment model of the new generated thread.
     */
    prepareNewThread(commentModel) {

        // the ID of the new comment (should always be the temporary comment id)
        const id = commentModel.getId();
        // the threaded comment object that is stored in the comments pane
        const threadedComment = this.#getThreadFromId(id);
        // the thread comment node
        const threadCommentNode = threadedComment ? threadedComment.getNode() : null;

        if (threadCommentNode) { threadCommentNode.hover(this.#drawCommentRangeHoverHandler, this.#removeCommentRangeHoverHandler); }

        // adapting position in bubble mode
        if (this.#isBubbleMode) { this.#showCommentThreadInBubbleMode(commentModel); }
    }

    /**
     * Creating a bubble node for a specified comment (model) and register it in the
     * collector for all bubble nodes.
     *
     * @param {CommentMode} commentModel
     *  The comment model of the comment that gets a new bubble node.
     */
    createAndRegisterNewBubbleNode(commentModel) {

        if (!this.#isBubbleMode) { return; }

        // the comment id
        const id = commentModel.getId();
        // the bubble node belonging to the thread node
        let bubbleNode = null;

        if (this.#bubbleNodeCollector[id]) { this.#bubbleNodeCollector[id].remove(); } // removing an older version (should not happen)

        bubbleNode = this.#createBubbleNode(id, { multiComment: false, colorClassName: this.#getCommentColorClassName(commentModel.getAuthor()) });
        this.#bubbleLayerNode.append(bubbleNode); // appending bubble node into bubble layer
        this.#bubbleNodeCollector[id] = bubbleNode; // collecting the bubble nodes
    }

    /**
     * Check, whether the bubble mode needs to be activated. This is the case, if the current display mode is 'buubles'
     * and a temporary comment is inserted.
     *
     * @returns {Boolean}
     *  Whether there is exactly one comment in the document and the current status is 'bubbles'.
     */
    isBubbleModeActivationRequired() {
        return this.getCommentNumber() === 1 && this.getDisplayMode() === CommentLayer.DISPLAY_MODE.BUBBLES;
    }

    /**
     * Handler for a change of the selected comment
     *
     * @param {String|Null} oldId
     *  The id of the previous selected comment.
     *
     * @param {String|Null} newId
     *  The id of the new selected comment.
     */
    selectedCommentChanged(oldId, newId) {

        // the ID of the threaded comment
        let threadedCommentId = null;
        // the threaded comment object that is stored in the comments pane
        let threadedComment = null;
        // the thread comment node
        let threadCommentNode = null;

        // Informing listeners, that the comment with the ID 'oldId' is no longer selected
        // -> closing optionally an open selection box after pressing 'Next', 'Prev', ...
        if (oldId) { this.docApp.docView.trigger('comment:selection:lost', oldId); }

        // After a selection change of comments it is possible, that shrinked comments
        // are shown again or visible comments are shrinked. Therefore it is necessary
        // to update the range marker lines. But only, if all lines are visible.
        if (CommentLayer.ALWAYS_VISIBLE_COMMENT_LINES_MODE[this.#displayMode]) {
            if (this.#commentCollection.isTemporaryCommentId(oldId) || this.#commentCollection.isTemporaryCommentId(newId)) {
                this.executeDelayed(() => {
                    const focusNode = getFocus(); // storing the existing focus node
                    this.docModel.trigger('update:absoluteElements'); // a new comment was added or removed -> full update
                    // restoring the previous focus (it is lost after asynchronous udpate after inserting new comment)
                    if (focusNode !== getFocus()) { setFocus(focusNode); }
                }, 200);
            } else {
                this.docModel.trigger('update:rangeMarkers'); // updating only the visible ranges
                // -> not triggering 'update:absoluteElements' because this would also affect the comment nodes
            }
        } else {
            // Removing the comment line connection after deselecting a comment.
            // -> this is not necessary, if all ranges and comment lines are visible
            if (oldId) {
                threadedCommentId = this.#commentCollection.getThreadIdForCommentId(oldId);
                threadedComment = this.#getThreadFromId(threadedCommentId);
                threadCommentNode = threadedComment ? threadedComment.getNode() : null;
                this.#removeCommentRangeHover(threadCommentNode);
            }
        }

        // Drawing the comment line connection after selecting a comment. For selected comments the range and
        // the line should always be visible.
        // -> this is not necessary, if all ranges and comment lines are visible
        if (newId && !this.isHiddenDisplayModeActive()) {
            threadedCommentId = this.#commentCollection.getThreadIdForCommentId(newId);
            threadedComment = this.#getThreadFromId(threadedCommentId);
            threadCommentNode = threadedComment ? threadedComment.getNode() : null;
            this.#drawCommentRangeHover(threadCommentNode);
        }

        // Adding a extra class to the selected comment, if all comments are highlighted (DOCS-2654)
        if (CommentLayer.ALWAYS_VISIBLE_COMMENT_LINES_MODE[this.#displayMode]) { this.#setMarkerAtSelectedCommentLine(newId); }
    }

    /**
     * The update of the vertical positions in the comments side pane revealed, that the comment order is
     * not perfect. There are mixed vertical top positions, so that a new sorting of the comments collection
     * and a rerendering of the comments pane is recommended.
     *
     * @param {Object[]} collector
     *  A collector that contains for all main comments an object with two properties:
     *   - id : the id of the main comment
     *     top: the top position (in pixel) of that comment relative to the page
     */
    resortComments(collector) {

        // resorting the comments in the comment collection
        this.#commentCollection.resortCommentsInCollection(collector);
        // adding comments into the comments pane
        this.docApp.docView.updateCommentsIfRequired();
        // updating vertical distances of comments
        this._updateComments();
    }

    /**
     * Debug: Logging the model state (67242).
     *
     * @param {String} target
     *  The target, that enforced this comment debugging.
     */
    logModelState(target) {
        globalLogger.info('Operation target: ' + target);
        globalLogger.info('Comment was filled in fastLoad process: ' + this.#commentModelFilled);
        globalLogger.info('Number of comments in comment model: ' + this.getCommentNumber());
        this.#commentCollection.iterateCommentModels(commentModel => { globalLogger.info('Comment ID in model: ' + commentModel.getId()); });
        globalLogger.info('Fast load string received from backend: ' + this.#fastLoadString);
    }

    // private methods ----------------------------------------------------

    /**
     * Refreshing the links between a comment model and its place holder node.
     *
     * @param {CommentModel} commentModel
     *  The commentModel object that needs a refreshed comment place holder node.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.fastLoad=false]
     *      If set to true, this function was triggered by the fastload:done event (important for ODF files).
     *  @param {Boolean} [options.importSuccess=false]
     *      If set to true, this function was triggered by the import success handler function.
     */
    #refreshLinkForComment(commentModel, options) {

        // the comment id
        const commentID = commentModel.getId();
        // the selector string for the search for place holder nodes and space maker nodes
        const selectorString = '[data-container-id=' + commentID + ']';
        // finding the comment place holder nodes, that are located inside the page content node
        let foundNodes = null;
        // wheter this function was triggered by fast load event of an ODF file
        const odfFastLoad = this.docApp.isODF() && getBooleanOption(options, 'fastLoad', false);

        // the children of the page node that can contain comment place holder nodes
        this.#contentChildren = this.#contentChildren || (this.docApp.isODF() ? getContentChildrenOfPage(this.docModel.getNode()) : getPageContentNode(this.docModel.getNode()));

        // finding the place holder and optionally the space maker node, that are located inside the page content node
        foundNodes = $(this.#contentChildren).find(selectorString);

        // updating the value for the global comment id, so that new comments in the comment layer get an increased number.
        // this.#updatePlaceHolderCommentID(commentID);

        if (odfFastLoad) { this.#contentChildren = null; } // need to be restored later in importSuccess

        if (foundNodes.length > 0) {
            commentModel.setCommentPlaceHolderNode(foundNodes);
        } else {
            if (!odfFastLoad) {
                globalLogger.error('CommentLayer.refreshLinkForComment(): failed to find place holder node with commentID: ' + commentID);
            }
        }
    }

    /**
     * Refreshing the list of comment place holder nodes, that always need to have the correct order
     * of comments in the document. This order is needed for the correct ordering of comments in the
     * side pane.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.emptyModel=false]
     *    If set to true, this comment model has not been filled yet. This is important for ODT files
     *    with comments in headers and footers that were loaded with fast load.
     */
    #updatePlaceHolderCollection(options) {

        // the collector for all start ranges and all comments
        let startCollector = null;
        // a collector for all saved comment IDs
        const commentIDs = [];
        // the current ID
        let currentID = 0;
        // a comment placeholder node
        let placeHolderNode = null;
        // a collector for the comment place holders
        let placeHolderCollector = [];
        // selector string to find a comment placeholder node
        let selectorString = null;

        // the children of the page node, in which comment place holders can be located
        this.#contentChildren = this.#contentChildren || (this.docApp.isODF() ? getContentChildrenOfPage(this.docModel.getNode()) : getPageContentNode(this.docModel.getNode()));

        // restoring the sorted list of all place holders
        // -> after inserting comment, deleting comment, splitting paragraph or loading document from local storage)

        if (this.docApp.isODF()) {
            // inside the document, the order is determined by the position of the place holder node, that always marks the beginning of the comment
            startCollector = this.#contentChildren.find(COMMENTPLACEHOLDER_NODE_SELECTOR);

            if (this.#commentCollection.containsMarginalComments() || getBooleanOption(options, 'emptyModel', false)) {

                // in header and footer the place holder can occur more often
                // -> they need to be filtered

                _.each(startCollector, node => {
                    currentID = getTargetContainerId(node);
                    if (!_.contains(commentIDs, currentID)) {
                        placeHolderCollector.push(node);
                        commentIDs.push(currentID);
                    }
                });
            } else {
                placeHolderCollector = startCollector; // no comments in header or footer
            }

            // improving the position of comments inside text frames that are in the text drawing layer of the main document
            if (getTextDrawingLayerNode(this.docModel.getNode()).find(COMMENTPLACEHOLDER_NODE_SELECTOR).length > 0) {
                // these pageComments place holder nodes are at the end of the place holder collector
                // -> iterating over all place holder nodes and resorting them
                placeHolderCollector = _.sortBy(placeHolderCollector, placeHolder => {
                    return $(placeHolder).offset().top;
                });
            }

        } else {
            // inside the document, the order of the comments is determined by the start of the comment.
            // In OOXML this can be the start range node or the place holder node itself, if the comment
            // has no start range node.
            startCollector = this.#contentChildren.find(RANGEMARKER_STARTTYPE_SELECTOR + ', ' + COMMENTPLACEHOLDER_NODE_SELECTOR);

            _.each(startCollector, node => {

                if (isCommentPlaceHolderNode(node)) {

                    currentID = getTargetContainerId(node);

                    if (!_.contains(commentIDs, currentID)) {
                        placeHolderCollector.push(node);
                        commentIDs.push(currentID);
                    }

                } else if (isRangeMarkerStartNode(node) && getRangeMarkerType(node) === 'comment') {

                    currentID = getRangeMarkerId(node);

                    if (!_.contains(commentIDs, currentID)) {

                        selectorString = '.commentplaceholder[data-container-id="' + currentID + '"]';
                        placeHolderNode = startCollector.filter(selectorString); // TODO: Better .find (but it fails)

                        if (placeHolderNode.length) {
                            placeHolderCollector.push(getDomNode(placeHolderNode));
                            commentIDs.push(currentID);
                        } else {
                            globalLogger.warn('CommentLayer.updatePlaceHolderCollection(): Failed to find comment model with ID: ' + currentID);
                        }
                    }
                }
            });

        }

        // setting collection for global container
        this.#commentPlaceHolders = $(placeHolderCollector);
    }

    /**
     * Helper function to delete a temporary comment node, if the user selected
     * 'Delete all'.
     *
     * Triggering an event, so that the comment editor calls the 'close' handler.
     */
    #deleteTemporaryComment() {
        this.docApp.docView.trigger('comment:delete:all');
    }

    /**
     * The ID of a comment thread. This is the ID of the first comment in the thread.
     * The threads are collected in the comments pane of OX Text.
     *
     * @param {String} commentId
     *  The ID of a comment thread.
     */
    #getThreadFromId(commentId) {
        return this.docApp.docView.commentsPane.getThreadedCommentFrameById(commentId);
    }

    /**
     * Helper function to keep the global placeHolderCommentID up-to-date. After inserting
     * a new comment or after loading from local storage, this number needs to be updated.
     * Then a new comment can be inserted from the client with a valid and unique id.
     *
     * @param {String} id
     * The id of the comment node. The format of the id is a three bytes prefix "cmt"
     * followed by hex32 value e.g: "cmtFF00FF00"
     */
    #updatePlaceHolderCommentID(id) {
        this.#placeHolderCommentIDs.add(parseInt(id.substr(3), 16));
    }

    /**
     * Helper function to return the next unique comment id (in this format: "cmtFF00FF00"), the unique
     * id is also added to the placeHolderCommentIds
     */
    #getNextPlaceHolderCommentID() {
        let clientWithRandom = Math.trunc((parseInt(this.docApp.getShortClientId(), 16) << 16) | (Math.random() * 65536));
        while (this.#placeHolderCommentIDs.has(clientWithRandom)) {
            clientWithRandom++;
        }
        this.#placeHolderCommentIDs.add(clientWithRandom);
        return "cmt" + (clientWithRandom + Math.pow(16, 8)).toString(16).slice(-8).toUpperCase();
    }

    /**
     * Caching the currently active target, so that it can be restored later.
     *
     * @returns {Object}
     *  An object with the property 'activeTarget' for the currently active target string
     *  and the property 'rootNode' for the currently used active root node.
     */
    #cacheTargetNode() {
        return { activeTarget: this.docModel.getActiveTarget(), rootNode: this.docModel.getSelection().getRootNode() };
    }

    /**
     * Setting the left offset of the comment layer node. This needs to be done, after creating
     * a new comment layer node, or after the page width has changed. This happens for example
     * after change of page orientation, or on small devices.
     *
     * @param {HTMLElement|jQuery} [node]
     *  The node, that represents the comment layer of the document. This can be used as
     *  parameter, if it is new created. Otherwise the globally saved 'this.#commentLayerNode'
     *  is used.
     */
    #updateHorizontalCommentLayerPosition(node) {

        // the container node for the comments. This can come in as parameter (after creating
        // a new container node, or the global 'this.#commentLayerNode' is used.
        const layerNode = node || this.#commentLayerNode;
        // the new calculated pixel position for the left offset
        let leftPos = 0;
        // the right padding of the page node
        let rightPadding = 0;
        // the horizontal offset for the bubble layer node relative to the right border of the page
        let bubbleLayerOffset = 0;

        if (layerNode) {

            leftPos = Math.round(this.docModel.getNode().innerWidth());

            // the left position ignores the margin, so that the right margin can be used for
            // centering the page including the comment layer

            $(layerNode).css('left', (leftPos + 1) + 'px');

            // also adapting the bubble layer node
            if (this.#bubbleLayerNode) {

                // receiving the width of the right margin
                rightPadding = convertCssLength(this.docModel.getNode().css('padding-right'), 'px', 1);

                if (rightPadding > CommentLayer.BUBBLE_LAYER_WIDTH) {
                    bubbleLayerOffset = Math.round((rightPadding + CommentLayer.BUBBLE_LAYER_WIDTH) / 2);
                } else {
                    // bubble must be complete on the page
                    bubbleLayerOffset = CommentLayer.BUBBLE_LAYER_WIDTH + 1;
                }

                $(this.#bubbleLayerNode).css('left', (leftPos - bubbleLayerOffset) + 'px');
            }
        }
    }

    /**
     * Updating the comments layer once after the page breaks are all inserted.
     */
    #updateAfterPageBreak() {

        // first header and last footer are now available (those can contain comment place holder in ODF)
        if (this.docApp.isODF()) {
            // the children of the page node, in which comment place holders can be located
            this.#contentChildren = getContentChildrenOfPage(this.docModel.getNode());
        }

        // updating the comments layer for ODF and OOXML
        this.#updateCommentsLayer();
    }

    /**
     * Updating the complete comment layer. This includes the positioning of the comment container
     * node and the positioning of all comments inside the comment container.
     */
    #updateCommentsLayer() {
        if (this.isImportFinished() && !this.isEmpty() && !this.docApp.docView.destroyed) {
            this.#additionalUpdatePlaceHolderCollection(); // refreshing marginal place holders before calling update comments (only odf)
            this.#updateHorizontalCommentLayerPosition();
            this._updateComments();
        }
    }

    /**
     * Updating the comments in vertical order using 'this._updateComments' after activating or deactivating
     * a comment node. This can cause a change of height of the comment. Typically an updateComments
     * is sufficient for the new vertical ordering of the comments inside the comment layer. But in the
     * 'all' mode the comment selections and the comment lines need to be updated, too.
     */
    #updateCommentsAndRangesInAllMode() {

        if (CommentLayer.ALWAYS_VISIBLE_COMMENT_LINES_MODE[this.#displayMode]) {
            const overlayNode = this.docModel.getRangeMarker().getRangeOverlayNode();
            if (overlayNode && overlayNode.children().length > 0) {
                this.docModel.trigger('update:absoluteElements'); // updating the comments vertically and also the visible ranges
            } else {
                this._updateComments(); // only updating the vertical comment positions
            }
        } else {
            this._updateComments(); // only updating the vertical comment positions
        }

    }

    /**
     * Updating the comments in vertical order using 'this._updateComments'. But in some special cases it it
     * necessary to trigger 'update:absoluteElements' that additionally repaints the comment ranges. This
     * function should only be used rarely, because 'update:absoluteElements' it typically triggered in
     * the model, not here.
     *
     * Info: options are not evaluated now. This might change in the future.
     * @param {Object} [_options]
     *  Optional parameters:
     *  @param {Boolean} [_options.ignoreMissing=false]
     *      Whether it is allowed, that the place holder list contains (invalid) place holder nodes from
     *      non-first marginal nodes.
     */
    #updateCommentsAndRanges(_options) {

        if (CommentLayer.VISIBLE_COMMENT_THREAD_MODES[this.#displayMode]) {
            const overlayNode = this.docModel.getRangeMarker().getRangeOverlayNode();
            if (overlayNode && overlayNode.children().length > 0) {
                this.docModel.trigger('update:absoluteElements');  // updating the comments vertically and also the visible ranges
            } else {
                this._updateComments(); // only updating the vertical comment positions
            }
        } else {
            this._updateComments(); // only updating the vertical comment positions
        }
    }

    /**
     * A handler function triggered when the comment editor is created.
     *
     * @param {CommentEditor} editor
     *  The comment editor instance.
     *
     * @param {CommentModel} commentModel
     *  The comment model for that the comment editor is created.
     */
    #commentEditorCreatedHandler(editor, commentModel, commentContentEditableElement) {
        // setting global marker that the comment editor is active
        this.#commentEditorActive = commentModel.getId();
        // starting to measure the height of the comment editor
        this.#registerCommentHeightHandler(editor);
        // setting the selection to the comment (thread), if it is not already selected
        if (this.#commentCollection.getSelectedCommentId() !== commentModel.getId()) { this.#commentCollection.selectComment(commentModel); }

        // opening the soft keyboard and setting focus into the editor
        if (TOUCH_DEVICE) {
            this.executeDelayed(() => {
                this.docApp.docView.openSoftKeyboard({ keepFocus: true });
                this.executeDelayed(() => { commentContentEditableElement.focus(); }, 500);
            }, 500);
        }
    }

    /**
     * Handler function for the event 'commenteditor:closed'. This event is triggered, if a
     * comment editor was closed. This can happen in the three cases:
     *
     *  - new comment
     *  - edit comment
     *  - reply to comment
     *
     * . This event is triggered in both cases:
     *
     *  - the user closes the comment editor with the close button.
     *  - the user presses the 'Send' button and the operations for a new comment thread are
     *    generated.
     *
     * If this is a new comment, in both cases the temporay comment model, that was generated
     * to show the comment at the correct position in the comments pane, must be removed again.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.isNewThread=false]
     *      Whether the editor was used in a new thread.
     *  @param {String} [options.reason=null]
     *      The reason for closing the editor. Supported values are
     *       - 'save', the user pressed the 'send' button
     *       - 'cancel', the user pressed the 'cancel' button
     *       - 'deleteAll', ' the user pressed the 'Delete all comments' button
     */
    #commentEditorClosedHandler(options) {

        let reason = null;
        const selection = this.docModel.getSelection();

        this.#commentEditorActive = null;
        selection.restoreBrowserSelection(); // avoiding reload by accident (DOCS-2981)

        if (getBooleanOption(options, 'isNewThread', false)) {
            this.#removeCommentBubble(this.#commentCollection.getTemporaryModel());
            this.#commentCollection.removeTemporaryCommentModel(); // no longer required
            reason = getStringOption(options, 'reason', null);
            if (reason === 'cancel' || reason === 'deleteAll') {
                this._updateComments();
                // removing comments layer, if it is empty now (this might be the case after closing a temporary comment)
                this.#hideCommentLayerIfEmpty();
            }
        }
    }

    /**
     * Registering a handler to measure the height of the comment editor. If the height changes,
     * the range markers can be updated. This is only required, if all range markers are visible.
     *
     * @param {CommentEditor} editor
     *  The comment editor instance.
     */
    #registerCommentHeightHandler(editor) {

        // starting a function to measure the height of the node of the new threaded comment frame and trigger an event, if the height has changed.
        const heightTimer = this.setInterval(() => {
            if (!editor || editor.destroyed) {
                heightTimer.abort();
                return;
            }
            const currentHeight = editor.getCommentTextFrame().height();
            if (currentHeight !== this.#previousHeight) {
                if (this.#previousHeight) {
                    if (CommentLayer.ALWAYS_VISIBLE_COMMENT_LINES_MODE[this.#displayMode]) {
                        this.docModel.trigger('update:rangeMarkers'); // updating only the visible ranges
                    }
                }
                this.#previousHeight = currentHeight;
            }
        }, { delay: 1000, interval: 800 });
    }

    /**
     * Handler for saving the display mode at event 'docs:reload:prepare'. If this is called several
     * times before the reload, only the first value is saved. In error case the dispay mode is set
     * to 'hidden', but this should not be stored.
     */
    #prepareReloadHandler() {
        // saving the current display mode, before it is changed to 'hidden' from setInternalError
        // -> accepting only the first call of this handler function
        this.#beforeReloadDisplayMode = this.#beforeReloadDisplayMode || this.#displayMode;
    }

    /**
     * Handler for the event 'change:zoom:before'.
     */
    #changeZoomBeforeHandler() {
        // because of the margins in OX Text in different zoom levels and the margin for the comments
        // pane, the latter should be closed before changing the zoom level (DOCS-4148).
        if (CommentLayer.VISIBLE_COMMENT_THREAD_MODES[this.#displayMode]) {
            this.#zoomChangeMode = this.#displayMode; // saving the current mode
            this.switchCommentDisplayMode(CommentLayer.DISPLAY_MODE.HIDDEN);
        }
    }

    /**
     * Handler for the event 'change:zoom'.
     */
    #changeZoomHandler() {
        // restoring an optionally saved 'mode'. This was saved before the zoom change (DOCS-4148).
        if (this.#zoomChangeMode) { this.switchCommentDisplayMode(this.#zoomChangeMode); }
        this.#zoomChangeMode = null;
    }

    /**
     * An info box was created in a comment to inform the user about problems, when he changes the
     * document. If all comment lines are visible, they need to be restored.
     *
     * @param {CommentModel} commentModel
     *  The comment model for that the info box about complex content is created.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.created=false]
     *      Whether the infobox was created or not.
     */
    #infoboxChangedHandler(commentModel, options) {

        if (getBooleanOption(options, 'created', false)) {
            // setting the selection to the comment (thread), if it is not already selected
            if (this.#commentCollection.getSelectedCommentId() !== commentModel.getId()) { this.#commentCollection.selectComment(commentModel); }
        }

        if (CommentLayer.ALWAYS_VISIBLE_COMMENT_LINES_MODE[this.#displayMode]) {
            this.docModel.trigger('update:rangeMarkers'); // updating only the visible ranges
        }
    }

    /**
     * Special function for updating the place holder collection and the comment threading more often,
     * if the document contains comments in header or footer. This requires a more often update of
     * the place holder collection, because they are more often exchanged, if they are located inside
     * header or footer.
     *
     * Info: This function is typically called before the vertical update of comments is done in
     *       function 'this._updateComments'.
     * Info: Minimize usage of this function.
     */
    #additionalUpdatePlaceHolderCollection() {

        // Updating place holder collection can be expensive
        // -> reducing calls of updatePlaceHolderCollection()
        // -> keeping track of marginal comments
        if (this.docApp.isODF() && this.#commentCollection.containsMarginalComments()) {
            this.#updatePlaceHolderCollection();
        }
    }

    /**
     * Updating the vertical position of the comment nodes in the comment layer.
     * This function is called very often and is therefore performance critical.
     */
    @textLogger.profileMethod("CommentLayer.updateComments()")
    _updateComments() {

        // Performance: Only display the comments in the visible region of the document
        // -> handling like for change track markers
        // Performance: Keeping sorted order of comment place holders, that is used
        // for display order of comments in the side pane
        this.docApp.docView.trigger('update:comments');
    }

    /**
     * Drawing a connection line between the comment node and the upper left corner of the
     * range to that the comment belongs.
     *
     * @param {HTMLElement|jQuery} commentThreadNode
     *  The comment thread node, that will be connected to its range.
     *
     * @param {Object} startPos
     *  An object containing the properties x and y. These represent the pixel position of
     *  the upper left corner of the range, that will be connected to the comment. The pixel
     *  positions are received using the jQuery 'offset' function and relative to the page.
     *
     * @param {String} id
     *  The id of the comment node.
     *
     * @param {String} highlightClassNames
     *  One or more space-separated classes to be added to the class attribute of the
     *  generated 'div'-elements, that are used to draw the connection line.
     *
     * @param {Boolean} [alreadyInserted=false]
     *  Whether the temporary comment range marker nodes were already inserted by the
     *  calling function(s).
     */
    #drawCommentLineConnection(commentThreadNode, startPos, id, highlightClassNames, alreadyInserted) {

        // whether this comment id is of the temporary comment model
        const isTemporaryComment = this.#commentCollection.isTemporaryCommentId(id);

        // for temporary comment nodes the range marker must be inserted and synchronously be removed again
        if (isTemporaryComment && !alreadyInserted) { this.docModel.getRangeMarker().insertTemporaryRangeMarker(this.#commentCollection.getTemporaryModel()); }

        // drawing the line into the range marker overlay
        const overlayNode = this.docModel.getRangeMarker().getRangeOverlayNode();
        // an id that is optionally registered at the overlayNode, because this is the only visible in active editing
        const visibleId = overlayNode.attr('data-range-id');
        // whether the generated comment line connection must be hidden
        const doHide = visibleId && id !== visibleId;
        // the classes that will be assigned to the lines
        const lineBaseClasses = doHide ? this.#commentLineClass + ' hideline' : this.#commentLineClass;
        // the horizontal pixel position, where the vertical line will be drawn
        let breakPosX = 0;
        // the current zoom
        const zoomFactor = this.docApp.docView.getZoomFactor() * 100;
        // the destination position at the comment node
        const destPos = getPixelPositionToRootNodeOffset(this.docModel.getNode(), commentThreadNode, zoomFactor);
        // whether one line is sufficient to connect comment and range
        const oneLine = Math.abs(startPos.y - destPos.y) < 1;
        // the classes for the new highlight elements (adding marker for connection line)
        const classNames = highlightClassNames ? highlightClassNames + ' ' + lineBaseClasses : lineBaseClasses;
        // the first vertical line, that will always be drawn
        const line1 = $('<div>').addClass(classNames).attr('data-range-id', id);
        // optional second and third line
        let line2 = null;
        let line3 = null;
        // the right padding of the page in pixel
        let rightPadding = 0;
        // a minimum thickness, that is used for zoom factors smaller than 100 (overwrites css)
        const minThickNess = zoomFactor < 100 ? Math.ceil(100 / zoomFactor) : 1;
        // a selector to find existing lines
        const localCommentLineSelector =  '.' + this.#commentLineClass + '[data-range-id=' + id + ']';
        // existing lines for this ID
        const existingLines = overlayNode.children(localCommentLineSelector);
        // a small correction of the width of the lines caused by rounded corners
        const rounderCornerAddon = 5;

        // removing existing lines
        if (existingLines.length) { overlayNode.children(localCommentLineSelector).remove(); }

        if (oneLine) {
            line1.css({ top: startPos.y, left: startPos.x, width: destPos.x - startPos.x + rounderCornerAddon });
            if (zoomFactor < 100) { line1.css({ height: minThickNess }); }
            overlayNode.append(line1);
        } else {
            rightPadding = parseCssLength(this.docModel.getNode(), 'paddingRight');
            breakPosX = overlayNode.width() - Math.round(rightPadding / 2);

            line2 = $('<div>').addClass(classNames).attr('data-range-id', id);
            line3 = $('<div>').addClass(classNames).attr('data-range-id', id);

            line1.css({ top: startPos.y, left: startPos.x, width: breakPosX - startPos.x });
            line2.css({ top: Math.min(startPos.y, destPos.y), left: breakPosX, height: Math.abs(startPos.y - destPos.y) });
            line3.css({ top: destPos.y, left: breakPosX, width: destPos.x - breakPosX + rounderCornerAddon });

            if (zoomFactor < 100) {
                line1.css({ height: minThickNess });
                line2.css({ width: minThickNess });
                line3.css({ height: minThickNess });
            }

            overlayNode.append(line1, line2, line3);
        }

        if (isTemporaryComment && !alreadyInserted) { this.docModel.getRangeMarker().removeTemporaryRangeMarker(); }
    }

    /**
     * Adding an extra marker class to the selected comment lines (DOCS-2654). Or removing
     * the extra marker class, if no comment is selected anymore (DOCS-3373).
     *
     * @param {String|null} commentId
     *  The ID of the selected comment. Or null, if no comment is selected anymore.
     */
    #setMarkerAtSelectedCommentLine(commentId) {

        // drawing the line into the range marker overlay
        const overlayNode = this.docModel.getRangeMarker().getRangeOverlayNode();
        // a selector to find existing lines
        const allLineSelector =  '.' + this.#commentLineClass;
        // a selector to find existing lines
        const localCommentLineSelector = commentId ?  '.' + this.#commentLineClass + '[data-range-id=' + commentId + ']' : null;

        // removing 'selected' marker from all lines (also if no new comment is selected, DOCS-3373)
        overlayNode.children(allLineSelector).removeClass('selected');
        // setting the class 'selected' for the specified comment id
        if (localCommentLineSelector) {
            overlayNode.children(localCommentLineSelector).addClass('selected');
        }
    }

    /**
     * Visualizing the selection range of a comment thread. This handler is assigned
     * to the comment thread node. If bubble mode is active, 'this' is the currently
     * hovered bubble node.
     *
     * @param {HTMLElement|jQuery} targetNode
     *  The current target of the event.
     *
     * @param {jQuery.Event} [event]
     *  The jQuery event object. This is only specified, if this function
     *  was triggered by hovering.
     */
    #drawCommentRangeHover(targetNode, event) {

        // the id of the main comment in the thread
        const commentId = $(targetNode).attr('data-comment-id');
        // the comment model of the specified ID
        const commentModel = this.#commentCollection.getById(commentId);

        if (!commentModel) { return; } // increasing resilience, but this should never happen

        // the author of the main comment
        const commentAuthor = commentModel.getAuthor();
        // the author specific color class name
        const colorClassName = this.#getCommentColorClassName(commentAuthor);
        // the range marker handler
        const rangeMarker = this.docModel.getRangeMarker();
        // the position of the upper left corner of the highlighted range
        let startPos = null;
        // whether this comment id is of the temporary comment model
        const isTemporaryComment = this.#commentCollection.isTemporaryCommentId(commentId);
        // whether the temporary range marker nodes are already inserted
        const alreadyInserted = true;

        // this function was called by a real hover event -> do not draw an additional range on selected comments
        if (event && this.#commentCollection.isSelectedCommentId(commentId)) { return; }

        // for temporary comment nodes the range marker must be inserted and synchronously be removed again
        if (isTemporaryComment) { rangeMarker.insertTemporaryRangeMarker(this.#commentCollection.getTemporaryModel()); }

        // highlighting the complete comment thread
        $(targetNode).addClass(this.#getHoveredCommentThreadColorClassName(commentAuthor));

        // highlighting the range with the specific id, but not if this is
        // the active target (cursor is positioned inside the comment).
        if (commentId && this.docModel.getActiveTarget() !== commentId) {

            startPos = rangeMarker.highLightRange(commentId, 'visualizedcommenthover fillcolor ' + colorClassName);

            // drawing a line connection between comment and range (not necessary for bubble mode)
            if (startPos && !this.#isBubbleMode) { this.#drawCommentLineConnection(targetNode, startPos, commentId, 'highlightcommentlinehover fillcolor ' + colorClassName, alreadyInserted); }
        }

        if (isTemporaryComment) { rangeMarker.removeTemporaryRangeMarker(); }
    }

    /**
     *  Removing the visualization of one range, if the visualization was
     *  triggered by hover.
     *
     * @param {HTMLElement|jQuery} targetNode
     *  The current target of the event.
     *
     * @param {jQuery.Event} [event]
     *  The jQuery event object. This is only specified, if this function
     *  was triggered by hovering.
     */
    #removeCommentRangeHover(targetNode, event) {

        // the id of the main comment in the thread
        const commentId = $(targetNode).attr('data-comment-id');

        if (!commentId) { return; } // DOCS-2639

        // the comment model of the specified ID
        const commentModel = this.#commentCollection.getById(commentId);

        if (!commentModel) { return; } // happens, if there are multiple comments with same ID in (broken) document

        // the author of the main comment
        const commentAuthor = commentModel.getAuthor();

        // this function was called by a real hover event -> do not remove selection on selected comments
        if (event && this.#commentCollection.isSelectedThreadId(commentId)) { return; }

        // removing the hover-highlighting of the complete comment thread
        $(targetNode).removeClass(this.#getHoveredCommentThreadColorClassName(commentAuthor));

        // removing the highlighting of a highlighted range
        this.docModel.getRangeMarker().removeHighLightRange(commentId);
    }

    /**
     * In ODF documents, that comment start node (the comment node itself) and the
     * comment end node (the range marker) must have the same parent. Therefore
     * the current selection needs to be adapted, so that the end position is
     * inside a paragraph, that has the same parent as the start paragraph node.
     *
     * @param {Number[]} startPos
     *  The logical start position for the new comment.
     *
     * @returns {Number[]}
     *  The logical end position for the new comment.
     */
    #getBestODFRangeEndPosition(startPos) {

        // the best end position for the current selection
        let endPos = null;
        // the currently active root node of the document
        const rootNode = this.docModel.getCurrentRootNode();
        // the paragraph node containing the start position
        const startParagraph = getDomNode(getParagraphElement(rootNode, _.initial(startPos)));
        // the end paragraph
        let endParagraph = null;

        if (startParagraph) {

            endParagraph = startParagraph;

            // iterating over all following paragraphs, not traversing into table
            while (endParagraph && endParagraph.nextSibling && isParagraphNode(endParagraph.nextSibling)) {
                endParagraph = endParagraph.nextSibling;
            }

            endPos = getOxoPosition(rootNode, endParagraph);
            endPos.push(getParagraphNodeLength(endParagraph));  // setting to last position inside this paragraph
        }

        if (!endPos) {
            endPos = increaseLastIndex(startPos);
        }

        return endPos;
    }

    /**
     * Determining the class name for the color of the author for a specified comment
     * node. This class name is used in comment nodes (color of the name) and in the
     * comment thread (color of left border).
     *
     * @param {String} [author]
     *  Optionally the comment author.
     *
     * @returns {String|null}
     *  The class name for the color of the author of the specified comment node.
     */
    #getCommentColorClassName(author) {
        const colorIndex = this.docApp.getAuthorColorIndex(author);
        return _.isNumber(colorIndex) ? ('comment-author-' + colorIndex) : null;
    }

    /**
     * Determining the class name for the color of the author to highlight the hovered
     * comment thread.
     *
     * @param {String} [author]
     *  Optionally the comment author.
     *
     * @returns {String|null}
     *  The hovered comment thread class name for the color of the author of the
     *  specified comment node.
     */
    #getHoveredCommentThreadColorClassName(author) {
        const colorIndex = this.docApp.getAuthorColorIndex(author);
        return _.isNumber(colorIndex) ? ('comment-thread-hovered-author-' + colorIndex) : null;
    }

    /**
     * Removing empty text spans between the comment placeholder node and a preceding
     * range end marker node (only OOXML).
     *
     * @param {CommentModel} commentModel
     *  The comment node in the comment layer.
     */
    #removePreviousEmptyTextSpan(commentModel) {

        // the place holder node for the comment
        const placeHolderNode = commentModel.getCommentPlaceHolderNode();
        // the node preceding the comment place holder node
        const precedingNode = placeHolderNode && placeHolderNode.prev();

        if (precedingNode.length > 0 && precedingNode.prev().length > 0 && isEmptySpan(precedingNode) && isRangeMarkerEndNode(precedingNode.prev())) {
            precedingNode.remove();
        }
    }

    /**
     * Searching for a parent comment in ODT after loading the document.
     * Finding a valid range marker end node, that can be the end range marker
     * for a comment node. That comment node is the parent comment, if between
     * the end range marker and the specified comment placeholder node are only
     * empty text spans or other comment placeholder nodes (from other replies).
     *
     * This function is used to find a parent for a comment, if the comment has
     * a range and therefore a range marker end node.
     *
     * @param {HTMLElement|jQuery} startNode
     *  The comment placeholder node for that the previous nodes are investigated.
     *
     * @returns {HTMLElement|null}
     *  The found end range marker node, or null, if it could not be found.
     */
    #findPreviousRangeMarkerEndNode(startNode) {

        // whether the iteration shall be continued
        let doContinue = true;
        // whether the found node is a range end marker node of a comment
        let isCommentRangeEndMarker = false;
        // the non-jQueryfied start node
        let node = getDomNode(startNode);
        // the id of the range marker node
        let id = null;

        while (doContinue && node.previousSibling) {

            node = node.previousSibling;

            if (!isEmptySpan(node) && !isCommentPlaceHolderNode(node)) {
                doContinue = false;
                if (isRangeMarkerEndNode(node)) {
                    id = getRangeMarkerId(node);
                    isCommentRangeEndMarker = this.isCommentId(id);
                }
            }
        }

        return isCommentRangeEndMarker ? node : null;
    }

    /**
     * Searching for a parent comment in ODT after loading the document.
     *
     * This function is used to find a parent for a comment, if the comment has
     * no range and therefore no range marker end node. In this case the comment
     * nodes are directly following each other and the leftmost placeholder is
     * the parent. Only empty textspans are allowed between the comment placeholder
     * nodes.
     *
     * @param {HTMLElement|jQuery} startNode
     *  The comment placeholder node for that the previous nodes are investigated.
     *
     * @returns {HTMLElement|null}
     *  The found parent placeholder node, or null, if it could not be found.
     */
    #findPreviousPlaceHolderNode(startNode) {

        // whether the iteration shall be continued
        let doContinue = true;
        // whether the found node is a comment placeholder node
        let foundPlaceHolderNode = null;
        // the non-jQueryfied start node
        let node = getDomNode(startNode);

        while (doContinue && node.previousSibling) {

            node = node.previousSibling;

            if (isEmptySpan(node) || isCommentPlaceHolderNode(node)) {
                doContinue = true;
                if (isCommentPlaceHolderNode(node)) {
                    foundPlaceHolderNode = node; // taking the leftmost comment place holder to find the parent of all children
                }
            } else {
                doContinue = false;
            }
        }

        return foundPlaceHolderNode ? foundPlaceHolderNode : null;
    }

    /**
     * After loading is finished, push collected comment authors list to global document authors list,
     * and after that, for each comment that is loaded with document, assign appropriate color for author.
     *
     * @param {Boolean} usedLocalStorage
     *  Whether the document was loaded from the local storage.
     *
     * @param {Boolean} usedFastLoad
     *  Whether the document was loaded with fast load.
     */
    #finalizeComments(usedLocalStorage, usedFastLoad) {

        // the range marker manager
        const rangeMarker = this.docModel.getRangeMarker();

        this.#commentCollection.iterateCommentModels(commentModel => {
            // removing empty text spans between comment node and range marker end node, if fast load was used
            // -> this might be superfluous with new comments in OX Text
            if (usedFastLoad && !this.docApp.isODF()) { this.#removePreviousEmptyTextSpan(commentModel); }

            // trying to find a parent structure in ODF
            if (this.docApp.isODF()) {
                if (!commentModel.isReply() && !this.#commentCollection.hasChild(commentModel) && !rangeMarker.getEndMarker(commentModel.getId())) { // no end marker exists for this comment
                    const placeHolderNode = commentModel.getCommentPlaceHolderNode();
                    const endRangeMarkerNode = this.#findPreviousRangeMarkerEndNode(placeHolderNode); // looking into the DOM for previous nodes
                    if (endRangeMarkerNode) { commentModel.setParentId(getRangeMarkerId(endRangeMarkerNode)); }
                    // it is also possible, that there is no end range marker node at the parent, because there is no selection range (DOCS-2665)
                    if (!endRangeMarkerNode) {
                        const previousPlaceHolderNode = this.#findPreviousPlaceHolderNode(placeHolderNode); // looking into the DOM for previous nodes that are comment placeholders
                        if (previousPlaceHolderNode) {
                            const previousPlaceHolderId = getTargetContainerId(previousPlaceHolderNode);
                            if (!rangeMarker.getEndMarker(previousPlaceHolderId)) { // there must not be an end rangemarker for the found parent
                                const previousPlaceHolderModel = this.#commentCollection.getById(previousPlaceHolderId);
                                if (previousPlaceHolderModel && !previousPlaceHolderModel.isReply()) { commentModel.setParentId(previousPlaceHolderId); }
                            }
                        }
                    }
                }
            }
        });

        // in ODF it is now important to put children behind their parents
        if (this.docApp.isODF()) { this.#commentCollection.sortCommentsInModel(); }
    }

    /**
     * After loading the document with fast load or local storage, it is necessary
     * to fill the comments model.
     * This function must be called in fast load process, after all fast load strings
     * have been applied to the DOM, but before the operations are handled (38562).
     * This is important for drawings inside comments, because insertDrawing and
     * setAttributes operations for drawings are still sent from the server, even
     * if fast load is used.
     * In odt documents on the other hand, comment can be included in header and
     * footer. But in odt drawings are not allowed in comments. Therefore it is
     * important that the comments model is filled, after the headers and footers
     * are created. So even when fast load is used, 'this.#updateModel' must not be
     * triggered by 'fastload:done', because this would be too early.
     *
     * So this function is called differently corresponding to the loading process
     * and the file format:
     *
     * 1. Loading with operations: This function is not called.
     * 2. Fast load (ODT): Late, in importSuccessHandler
     * 3. Fast load (OOXML): Very early, after 'fastload:done' (38562)
     * 4. Local storage: Late, in importSuccessHandler
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.fastLoad=false]
     *      If set to true, this function was triggered by the fastload:done event.
     *  @param {Boolean} [options.importSuccess=false]
     *      If set to true, this function was triggered by the import success handler function.
     */
    #updateModel(options) {
        // refreshCommentLayer(options);
        // refreshing also the range marker model, if it was not done before (but forcing it after 'importSuccess' for ODF)
        this.docModel.getRangeMarker().refreshModel((this.docApp.isODF() && getBooleanOption(options, 'importSuccess', false)) ? undefined : { onlyOnce: true });
    }

    /**
     * After load with fast load the comments model needs to be filled. The data for the comments
     * are saved in the placeholder node.
     *
     * In fast load the model can only be filled, after the author list is registered in setDocumentAttributes
     * operation. Otherwise the authors cannot be correctly assigned from the authorId.
     *
     * This filling of the model must happen earlier than the 'importSuccessHandler', because 'insertComment',
     * 'changeComment' or 'deleteComment' operations fail, if these operations directly follow the insertion
     * of the fastLoad string. In this case, the 'importSuccessHandler' did not run. Therefore this function
     * is called, after the setDocumentAttributes operation set the author list. Then the comments can be
     * inserted into the model successfully. And the 'setDocumentAttributes' operation with the author list
     * must be sent from filter before the comment specific operations.
     *
     * @param {jQuery.Event} event
     *  The 'click' event in the comment bubble node.
     *
     * @param {Object[]} authors
     *  The array of registered authors as sent in the setDocumentAttributes operation.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.updatePlaceHolders=true]
     *      Whether an update of the place holder collection is required.
     */
    #fillModelAfterFastLoad(event, authors, options) {

        if (!this.docModel.isFastLoadImport() || this.#commentModelFilled) { return; }

        if (getBooleanOption(options, 'updatePlaceHolders', true)) { this.#updatePlaceHolderCollection(); }

        _.each(this.#commentPlaceHolders, commentPlaceHolder => {

            // the id of the comment
            const commentId = getTargetContainerId(commentPlaceHolder);
            // the operation context for a simulated insertComment operation (the context might be saved in a global collector)
            let operationContext = this.#fastLoadInsertCommentOperations[commentId] ? this.#fastLoadInsertCommentOperations[commentId] : null;
            // the data that are saved at the comment placeholder node
            let commentData = null;
            // the simulated operation
            let operation = null;
            // the comment model for the specified id
            let commentModel = null;
            // the changed text of a changeComment operation
            let changedText = null;
            // the changed mentions of a changeComment operation
            let changedMentions = null;

            if (!operationContext) {

                commentData = $(commentPlaceHolder).data(); // the operation data are stored at the place holder node
                operation = { name: COMMENT_INSERT };

                for (const key in commentData) {
                    if (_.isString(commentData[key])) {
                        if (commentData[key]) { operation[key] = commentData[key]; } // no empty strings
                    } else {
                        operation[key] = commentData[key];
                    }
                }

                if (!operation.id) {
                    operation.id = operation.containerId;
                    delete operation.containerId;
                }

                operationContext = new OperationContext(this.docModel, operation, false);
            }

            // adding the new comment into the commentModel and commentCollection classes
            // -> for this step the authors in the author list have to be registered.
            this.#commentCollection.handleInsertCommentOperation(operationContext, commentPlaceHolder, this.#commentPlaceHolders);

            // handling the global counter
            this.#updatePlaceHolderCommentID(commentId);

            // deleting the operation context from the collector
            if (this.#fastLoadInsertCommentOperations[commentId]) { delete this.#fastLoadInsertCommentOperations[commentId]; }

            // there might also be a changeComment operation sent after the fast load string
            if (this.#fastLoadChangeCommentOperations[commentId]) {
                commentModel = this.#commentCollection.getById(commentId);
                changedText = this.#fastLoadChangeCommentOperations[commentId].optStr('text');
                changedMentions = this.#fastLoadChangeCommentOperations[commentId].optArr('mentions');
                if (_.isString(changedText)) { commentModel.setText(changedText); }
                if (changedMentions) { commentModel.setMentions(changedMentions); }
                delete this.#fastLoadChangeCommentOperations[commentId];
            }
        });

        // checking that every comment has a valid parent -> make children with invalid parent to parent (DOCS-3259)
        this.#commentCollection.checkParentValidity();

        // checking that the order is valid and all child comments are inserted after their parents (DOCS-2648)
        this.#commentCollection.sortCommentsInModel();

        this.#commentModelFilled = true; // setting marker for debugging reasons
    }

    /**
     * The click handler for the comment bubble nodes in bubble mode. The 'click' event cannot be used here,
     * because the node is inside the div page node. Therefore the mouse handlers (for example mousedown) will
     * also be executed (mousedown comes before 'click'). This leads to unwanted effects.
     *
     * @param {jQuery.Event} event
     *  The 'click' event in the comment bubble node.
     */
    #bubbleNodeClickCallback(event) {

        // the clicked bubble node
        const bubbleNode = $(event.currentTarget);
        // the comment ID
        const commentId = bubbleNode.attr('data-comment-id');
        // the thread belonging to the bubble node
        const thread = this.#getThreadFromId(commentId);
        // the thread node belonging to the bubble node
        const threadNode = thread.getNode();

        if (threadNode) {
            // setting position for the thread node
            this.#setCommentThreadNextToBubble(bubbleNode, threadNode);
            // selecting the comment and making thread node visible ...
            this.#commentCollection.selectComment(this.#commentCollection.getById(commentId));
        }

        // setting a marker that the mousedown happened on a comment bubble node
        this.docModel.setMouseDownInCommentsBubbleNode(true); // DOCS-3101

        // no handling of this event in page node (has negative side effects)
        // -> also closing another comment must be handled in this handler
        event.preventDefault();
        event.stopPropagation();
    }

    /**
     * A handler for stopping the event propagation. This is useful in bubble mode,
     * because a handler at the content root node takes care of closing the selected
     * comment threads. But this handler must not be called, if the click happened
     * on a bubble node or a comment thread node. Therefore it is necessary, that
     * the event stops its propagation to the content root node.
     *
     * @param {jQuery.Event} event
     *  The jQueryfied event.
     */
    #stopPropagationCallback(event) {
        event.stopPropagation();
    }

    /**
     * Helper function to make a comment thread visible next to its corresponding
     * bubble node. The comment thread is specified by one of its comments.
     *
     * @param {CommentModel} commentModel
     *  The comment model, whose comment thread will be made visible next to
     *  its bubble node.
     */
    #showCommentThreadInBubbleMode(commentModel) {

        // the ID of the selected comment (thread)
        const commentId = commentModel.getId(); // the thread node of the specified comment node
        // the thread belonging to the bubble node
        const thread = this.#getThreadFromId(commentId);

        if (!thread) { return; } // increasing resilience

        // the thread node belonging to the bubble node
        const threadNode = thread.getNode();
        // the bubble node belonging to the thread node
        const bubbleNode = this.#bubbleNodeCollector[commentId];

        // if (threadNode && threadNode.length > 0 && !threadNode.hasClass('activatedBubble')) {
        if (threadNode && threadNode.length > 0 && bubbleNode) {
            this.#setCommentThreadNextToBubble(bubbleNode, threadNode); // setting position for the thread node
        }
    }

    /**
     * Handler function for "this.#bubbleNodeClickCallback" to be able to be used in ".on" and ".off", so
     * that the handler can be registered and deregistered.
     */
    #bubbleNodeClickHandler = evt => { this.#bubbleNodeClickCallback(evt); };

    /**
     * Handler function for "this.#stopPropagationCallback", so that the handler can be registered and deregistered.
     */
    #stopPropagationHandler = evt => { this.#stopPropagationCallback(evt); };

    /**
     * Handler function for "this.#drawCommentRangeHover", so that the handler can be registered and deregistered.
     */
    #drawCommentRangeHoverHandler = evt => { this.#drawCommentRangeHover(evt.currentTarget, evt); };

    /**
     * Handler function for "this.#removeCommentRangeHover", so that the handler can be registered and deregistered.
     */
    #removeCommentRangeHoverHandler = evt => { this.#removeCommentRangeHover(evt.currentTarget, evt); };

    /**
     * Activating the handlers at a specified comment bubble node.
     *
     * @param {HTMLElement|jQuery} bubbleNode
     *  The comment bubble node, whose handlers for hover and 'click' will be
     *  be activated.
     */
    #activateHandlersAtBubbleNode(bubbleNode) {

        // the bubble node jQueryfied
        const $bubbleNode = $(bubbleNode);

        $bubbleNode.hover(this.#drawCommentRangeHoverHandler, this.#removeCommentRangeHoverHandler);
        $bubbleNode.on('mousedown touchstart', this.#bubbleNodeClickHandler);
        $bubbleNode.on('click', this.#stopPropagationHandler);
    }

    /**
     * Deactivating all handlers for one specified comment thread.
     *
     * @param {HTMLElement|jQuery} commentThread
     *  The comment thread node in the comment layer, whose handlers will
     *  be deactivated.
     */
    #deactivateAllHandlersAtCommentThread(commentThread) {

        // the comment thread node jQueryfied
        const $commentThread = $(commentThread);

        // deactivating handler for hover over comment threads
        $commentThread.off('mouseenter', this.#drawCommentRangeHoverHandler);
        $commentThread.off('mouseleave', this.#removeCommentRangeHoverHandler);
    }

    /**
     * Deactivating the handlers at a specified comment bubble node.
     *
     * @param {jQuery} bubbleNode
     *  The comment bubble node, whose handlers for hover and 'click' will be
     *  be deactivated.
     */
    #deactivateAllHandlersAtBubbleNode(bubbleNode) {

        // deactivating handler for hover over bubble nodes
        bubbleNode.off('mouseenter', this.#drawCommentRangeHoverHandler);
        bubbleNode.off('mouseleave', this.#removeCommentRangeHoverHandler);

        bubbleNode.off('mousedown touchstart', this.#bubbleNodeClickHandler);
        bubbleNode.off('click', this.#stopPropagationHandler);
    }

    /**
     * Setting the pixel position of a comment thread in bubble mode. Standard behavior is,
     * that the comment thread is placed directly next to the bubble. If there is not
     * enough space, the comment is displayed left of the bubble.
     *
     * @param {HTMLElement|jQuery} bubbleNode
     *  The comment bubble node.
     *
     * @param {HTMLElement|jQuery} commentThread
     *  The comment thread node that will be positioned next to the specified comment bubble
     *  node.
     */
    #setCommentThreadNextToBubble(bubbleNode, commentThread) {

        // the jQueryfied bubble node
        const $bubbleNode = $(bubbleNode);
        // the jQuerified comment thread node
        const $commentThread = $(commentThread);
        // the left pixel position of the comment layer
        const commentLayerLeft = convertCssLength(this.#commentLayerNode.css('left'), 'px', 1);
        // the left pixel position of the bubble layer
        const bubbleLayerLeft = convertCssLength(this.#bubbleLayerNode.css('left'), 'px', 1);
        // calculating left offset of the comment thread node in the comment layer
        let left = bubbleLayerLeft - commentLayerLeft + $bubbleNode.width() + 2;
        // the top offset of the bubble node
        let top = convertCssLength($bubbleNode.css('top'), 'px', 1);
        // the available width inside the application content root node
        const availableWidth = this.docApp.docView.$contentRootNode.width();
        // the page offset in the application content root node
        const pageOffset = this.docModel.getNode().offset().left;
        // the most right pixel postion of the comment thread node, if it is positioned right of the bubble
        const rightPos = pageOffset + convertCssLength(this.#commentLayerNode.css('left'), 'px', 1) + this.#commentLayerNode.width();

        // it might be necessary to move the content thread left of the bubble node
        if (rightPos > availableWidth) {
            // showing comment thread below the bubble node as right as possible
            top += $bubbleNode.height();
            left -= (rightPos - availableWidth + 2);
        }

        // setting position of thread node
        $commentThread.css({ left, marginTop: top });
    }

    /**
     * Creating a bubble node that is displayed for a comment thread in the comments bubble mode.
     *
     * @param {String} commentId
     *  The id of the comment that will be assigned to the bubble.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.multiComment=false]
     *      Whether the comment thread contains more than one comment.
     *  @param {String} [options.colorClassName='']
     *      The class name that is used to set the author specific color for the bubble node.
     *
     * @returns {jQuery}
     *  A new created bubble node for a comment thread. The required handlers are already
     *  registered.
     */
    #createBubbleNode(commentId, options) {

        // the new bubble node
        const bubbleNode = $('<div>').addClass(COMMENTBUBBLENODE_CLASS);
        // whether the corresponding comment thread has more than one child
        const isMultiComment = getBooleanOption(options, 'multiComment', false);
        // an optional class name for the color of the bubble
        const colorClassName = getStringOption(options, 'colorClassName', '');
        // the link with icon in the bubble node
        const linkNode = $('<a tabindex="0">').append(createIcon(isMultiComment ? 'svg:comment-reply-o' : 'svg:comment-o', 24));

        if (colorClassName) {
            // setting the color to the icon in the bubble node
            linkNode.children('svg').addClass(colorClassName);
            // also setting the color for the background color of the bubble node (for hover and activating)
            if (colorClassName) { bubbleNode.addClass(colorClassName); }
        }

        // appending the icon node
        bubbleNode.append(linkNode);

        // setting marker to recognize that more than one comment is in the thread
        if (isMultiComment) { bubbleNode.addClass(CommentLayer.MULTI_COMMENT_THREAD); }

        // assigning the hover handler and the click to the bubble node
        this.#activateHandlersAtBubbleNode(bubbleNode);

        // saving the thread ID at the comment bubble
        bubbleNode.attr('data-comment-id', commentId);

        return bubbleNode;
    }

    /**
     * Updating the icon inside the bubble node of a specified comment thread. If the thread
     * contains more than one comment another icon is displayed compared to a thread with only
     * one comment.
     *
     * @param {CommentModel} commentModel
     *  The comment model for that the bubble needs to be updated after inserting or removing
     *  a comment.
     *
     * @returns {jQuery | void} // TODO: check-void-return
     *  The updated bubble node.
     */
    #updateBubbleNode(commentModel) {

        if (!this.#isBubbleMode) { return; }

        // the id of the changed (inserted or removed) comment model
        const commentId = commentModel.getId();
        // the thread id of the comment
        const threadId = commentModel.getParentId() ? commentModel.getParentId() : commentId;
        // the model of the main comment in the thread
        const mainCommentModel = this.#commentCollection.getById(threadId);
        // whether there is at least one reply for the thread specified
        const isMultiCommentThread = mainCommentModel && this.#commentCollection.hasReply(mainCommentModel);
        // the bubble node belonging to the specified comment thread
        const bubbleNode = this.#bubbleNodeCollector[threadId];
        // the color for the author of the first comment in the comment thread
        let colorClassName = null;
        // whether a change of the icon is required
        let changeRequired = false;

        // switching the icon at the bubble node
        const setBubbleIcon = iconId => {
            const linkNode = $('<a tabindex="0">').append(createIcon(iconId, 24));
            linkNode.children('svg').addClass(colorClassName);
            bubbleNode.children('a').remove().end().append(linkNode);
        };

        if (bubbleNode) {

            changeRequired = (isMultiCommentThread && !bubbleNode.hasClass(CommentLayer.MULTI_COMMENT_THREAD)) || (!isMultiCommentThread && bubbleNode.hasClass(CommentLayer.MULTI_COMMENT_THREAD));

            if (changeRequired) {
                colorClassName = this.#getCommentColorClassName(commentModel.getAuthor()); // the author specific color class name

                if (isMultiCommentThread) {
                    setBubbleIcon('svg:comment-reply-o');
                    bubbleNode.addClass(CommentLayer.MULTI_COMMENT_THREAD);
                } else {
                    setBubbleIcon('svg:comment-o');
                    bubbleNode.removeClass(CommentLayer.MULTI_COMMENT_THREAD);
                }
            }
        }

        return bubbleNode;
    }

    /**
     * Removing the bubble node for a specified comment (model) and deregister it in the
     * collector for all bubble nodes.
     *
     * @param {CommentMode} commentModel
     *  The comment model of the comment that gets a new bubble node.
     */
    #removeCommentBubble(commentModel) {

        if (!this.#isBubbleMode || commentModel.isReply()) { return; } // nothing to do

        const commentId = commentModel.getId();
        const bubbleNode = this.#bubbleNodeCollector[commentId];

        if (bubbleNode) { this.#removeBubbleNode(bubbleNode, commentId); } // also deregisters the node from the collector
    }

    /**
     * Deleting a specified bubble node of a comment thread.
     *
     * @param {jQuery} bubbleNode
     *  The comment bubble node, that will be removed from the DOM, after the data
     *  object is cleaned and the handlers are removed.
     *
     * @param {String} commentId
     *  The ID of the comment (thread).
     */
    #removeBubbleNode(bubbleNode, commentId) {

        // removing the click and hover handlers
        this.#deactivateAllHandlersAtBubbleNode(bubbleNode);

        // ... and finally remove the node itself
        bubbleNode.remove();

        // deleting bubble node from the collector
        delete this.#bubbleNodeCollector[commentId];
    }

    /**
     * Deactivating the comment bubble mode. This function can be used for clean up of the
     * bubbles and the bubble layer.
     */
    #leaveBubbleMode() {

        // all bubble nodes inside the bubble node layer
        const allBubbles = this.#bubbleLayerNode && this.#bubbleLayerNode.children(COMMENTBUBBLENODE_SELECTOR);

        _.each(allBubbles, bubbleNode => {

            // the comment ID
            const commentId = $(bubbleNode).attr('data-comment-id');
            // the thread belonging to the bubble node
            const thread = this.#getThreadFromId(commentId);
            // the thread node belonging to the bubble node
            const threadNode = thread ? thread.getNode() : null;

            // removing modified left and top alignment at thread that is only valid in bubble mode
            if (threadNode) { $(threadNode).css({ left: '', top: '' }); }

            this.#removeBubbleNode($(bubbleNode), commentId);
        });

        // clearing the collector for the bubble nodes
        this.#bubbleNodeCollector = null;

        // setting global marker for bubble mode
        this.#isBubbleMode = false;
    }

    /**
     * Activating the comment bubble mode. This function can be used for preparations up of the
     * bubbles and the bubble layer.
     */
    #enterBubbleMode() {

        // generating the collector for the bubble nodes
        if (!this.#bubbleNodeCollector) { this.#bubbleNodeCollector = {}; }

        // setting global marker for bubble mode
        this.#isBubbleMode = true;
    }

    /**
     * Making a comment thread invisible and also removing its visible range.
     */
    #hideCommentThread(commentThread) {
        // remove hover highlighting at comment thread without hover event
        this.#removeCommentRangeHover(commentThread);
    }

    /**
     * Updating the list of authors that are not filtered. This is especially useful, if an author,
     * whose comments are not visible because of an active filter, inserts a new comment.
     *
     * @param {String} author
     *  The name of the author, who shall be added to or removed from the list of not filtered authors.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.addAuthor=false]
     *      If set to true, the author will be added to the list of not filtered authors. Otherwise
     *      the author will be removed from this list.
     *  @param {Boolean} [options.forceUpdate=false]
     *      If set to true, the function setAuthorFilter will be called, even if no change in author
     *      list happened. In this case the new filter will be applied to the comments pane.
     */
    #updateAuthorFilter(author, options) {

        // whether the specified author shall be removed or added to the author filter list
        const addAuthor = getBooleanOption(options, 'addAuthor', false);
        // whether the author list was modified
        let modified = false;

        if (addAuthor) {
            if (!_.contains(this.#authorFilter, author)) {
                this.#authorFilter.push(author);
                modified = true;
            }
        } else {
            if (_.contains(this.#authorFilter, author)) {
                this.#authorFilter = _.without(this.#authorFilter, author);
                modified = true;
            }
        }

        if (modified || getBooleanOption(options, 'forceUpdate', false)) {
            // setting new list of not filtered authors
            this.setAuthorFilter(this.#authorFilter);
        }
    }

    /**
     * After deleting comment(s) it might be necessary to remove the complete
     * comment layer node.
     */
    #hideCommentLayerIfEmpty() {
        if (this.isEmpty()) { this.switchCommentDisplayMode(CommentLayer.DISPLAY_MODE.HIDDEN); }
    }

    /**
     * Handler for the events 'commentlayer:created' and 'commentlayer:removed'. In this
     * function all necessary work can be done, that is needed, if the comment layer on
     * the right side of the document is created or is removed.
     * This includes for example setting a right margin to the page node, so that it is
     * always centered.
     *
     * @param {boolean} created
     *  Whether the comment layer has been created (`true`) or removed (`false`).
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Number} [options.width=0]
     *      The width of the comment layer, that will be inserted or was removed (in pixel).
     */
    #updatePagePosition(created, options) {

        // the width of the created or removed comment layer
        const width = getIntegerOption(options, 'width', 0);
        // the page node
        let pageNode = null;
        // the right margin in pixel of the page node
        let marginWidth = 0;
        // whether a new value for the margin was calculated
        let marginModified = false;

        if (width > 0) {

            pageNode = this.docModel.getNode();

            // receiving the old value of the width of the right margin
            marginWidth = parseCssLength(pageNode, 'marginRight');

            // expanding or shrinking the right margin of the page node
            if (created && !pageNode.hasClass(COMMENTMARGIN_CLASS)) {
                marginWidth += width;
                pageNode.addClass(COMMENTMARGIN_CLASS);
                pageNode.data(COMMENTMARGIN_CLASS, width);  // using class name as key to save the width
                marginModified = true;
            } else if (!created && pageNode.hasClass(COMMENTMARGIN_CLASS)) {
                marginWidth -= width;
                pageNode.removeClass(COMMENTMARGIN_CLASS);
                pageNode.removeData(COMMENTMARGIN_CLASS);
                marginModified = true;
            }

            if (marginModified) {
                pageNode.css('margin-right', marginWidth);
                // and updating the horizontal position of the comment layer node
                this.#updateHorizontalCommentLayerPosition();
            }

            this.docApp.docView.recalculateDocumentMargin();
        }
    }

    /**
     * Refreshing the range markers for the comments and the connection lines
     * between comment frame and comment range. This is especially required, after
     * a change of the author filter, because it might be necessary to remove
     * existing lines or to add further lines. This does not happen by calling
     * the function 'updateCommentLineConnection', because it only updates existing
     * lines.
     *
     * This function iterates over all comment threads, removes an existing
     * highlighting and adds restores it for all visible comment frames.
     */
    #refreshAllRangeMarkers() {
        // update the range marker connection lines is not sufficient, because existing lines must be
        // removed or new lines must be generated -> iterating over threads and refreshing the
        for (const commentFrame of this.docApp.docView.commentsPane.commentFrames()) {
            const threadNode = commentFrame.getNode();
            this.#removeCommentRangeHover(threadNode);
            if (!threadNode.hasClass('isFilteredThread')) { this.#drawCommentRangeHover(threadNode); }
        }
    }

    /**
     * After applying or removing an author filter, the visibility of the
     * bubble nodes must be updated.
     */
    #updateBubbleVisibility() {

        for (const commentFrame of this.docApp.docView.commentsPane.commentFrames()) {
            // the id of the main comment in the thread
            const commentId = commentFrame.getThreadComment().getId();
            // the corresponding bubble node
            const bubbleNode = this.getBubbleNodeById(commentId);

            bubbleNode?.toggleClass('isFilteredThread', commentFrame.getNode().hasClass('isFilteredThread'));
        }
    }

    /**
     * After a modification of the filter for the author of the comments, it
     * is necessary that the new filter is applied.
     * Comments are made visible using the css style display directly at the
     * comment nodes. If all comments inside a comment thread are made invisible,
     * the comment thread also needs display 'none'. In case of bubble mode, the
     * bubble then also needs display 'none'.
     *
     * Info: This settings must be set in any display mode, even if the complete
     * comment layer is hidden. This has the advantage, that the comment nodes
     * itself are up to date, when the display mode is switched.
     */
    #applyAuthorFilter() {

        // updating all comment models in the comment collection
        this.#commentCollection.applyAuthorFilter(this.#authorFilter);

        // updating the visibility of the comment threads and comments
        this.docApp.docView.trigger('comment:visibility:changed');

        // updating the visibility of all bubbles
        if (this.#isBubbleMode) { this.#updateBubbleVisibility(); }

        // updating the vertical position of the comments and optionally the visible ranges

        if (this.#displayMode === CommentLayer.DISPLAY_MODE.ALL) {
            this._updateComments();
            // removing all and adding all required rangemarkers, not only updating existing
            this.#refreshAllRangeMarkers();
        } else {
            this.#updateCommentsAndRanges();
        }

        // deselecting an optionally selected comment
        if (this.#commentCollection.isSelectedCommentHidden()) { this.#commentCollection.deselectComment(); }
    }

    /**
     * Returns whether there is a visible comment layer right of the page node. This means
     * a comment layer in the 'classic' view. If this is visible, the centering of the page
     * needs to be readjusted.
     *
     * @returns {Boolean}
     *  Whether the comment layer right of the page is visible.
     */
    #isCommentLayerVisible() {
        return this.docApp.docView.commentsPane ? this.docApp.docView.commentsPane.isVisible() : false;
    }

    /**
     * Helper function to collect all IDs from the insertComment operations from a
     * specified list of operations.
     *
     * @param {Object[]} operations
     *  A list of all paste operations.
     *
     * @returns {String[]}
     *  A list of all comment IDs.
     */
    #collectAllCommentIDsFromOperations(operations) {

        const allInsertCommentOps = _.filter(operations, operation => {
            return operation.name && operation.name === COMMENT_INSERT;
        });

        return _.map(allInsertCommentOps, operation => {
            return operation.id;
        });
    }

    /**
     * Helper function to collect all logical positions of drawings from a
     * specified list of operations.
     *
     * @param {Object[]} operations
     *  A list of all paste operations.
     *
     * @returns {String[]}
     *  A list of all logical positions of the inserted drawings.
     */
    #collectAllDrawingPositionsFromOperations(operations) {

        const allInsertDrawingOps = _.filter(operations, operation => {
            return operation.name && operation.name === INSERT_DRAWING;
        });

        return _.map(allInsertDrawingOps, operation => {
            return _.clone(operation.start);
        });
    }

    /**
     * Checking, whether there is no content between the start marker node and the
     * end marker node in an OOXML comment.
     *
     * @param {jQuery} startMarker
     *  The start marker node of the comment.
     *
     * @param {jQuery} endMarker
     *  The end marker node of the comment.
     *
     * @returns {Boolean}
     *  Whether there is only an empty text span between the two range marker nodes.
     */
    #isEmptyCommentRangeInOoxml(startMarker, endMarker) {
        const firstSpan = startMarker.next();
        return firstSpan && isEmptySpan(firstSpan) && getDomNode(firstSpan.next()) === getDomNode(endMarker);
    }

    /**
     * Selecting a comment after loading a document using the "comment_id"
     * launch option that is part of the document URL.
     */
    #setDeepLink() {

        // an optional comment ID (date ID) for deep linking comments in OX Text
        const commentId = this.docApp.getLaunchOption('comment_id'); // checking for a deep link information the URL
        // the target model of the selected comment
        let targetModel = null;

        if (commentId) {
            targetModel = this.#commentCollection.getById(commentId);

            if (targetModel) {
                this.executeDelayed(() => { // delayed to avoid conflict with other selection settings
                    this.#commentCollection.selectComment(targetModel);
                }, 500);
            }
        }
    }

    /**
     * Checking, if in the list of specified operations (that will be pasted) are old style comments,
     * that use a target in the operation to write content into the comment. This can happen, when the
     * operations were created in an older version of OX Text.
     *
     * @param {Object[]} operations
     *  A list of all (paste) operations.
     *
     * @returns {Boolean}
     *  Whether there is at least one operation with a comment target in the operations list.
     *
     */
    #hasOldStyleCommentOperation(operations) {

        // whether there are operations for old style comments in the operations list
        let hasOldStyleComment = false;
        // a list of all comment IDs
        const allCommentIDs = this.#collectAllCommentIDsFromOperations(operations);

        // checking, if there is an old style comment operation (for example copied from an older version)
        if (allCommentIDs && allCommentIDs.length > 0) {
            _.each(operations, operation => {
                if (operation.target && _.contains(allCommentIDs, operation.target)) { hasOldStyleComment = true; }
            });
        }

        return hasOldStyleComment;
    }

    /**
     * Reducing the content that is listed in the mentions object, when it is sent via operations.
     * This function removes some properties, that are used, when mentions are generated. There are
     * some propierties in the object that are required especially for the permission handling.
     * But these not documentend properties should not be sent to remote clients.
     *
     * @param {Object[]} mentions
     *  The array with the new mentions in insertComment or changeComment operation.
     *
     * @returns {Object[]}
     *  A new array with cloned mentions, that have less properties.
     */
    #reduceMentionContentForOperation(mentions) {

        if (!mentions || !mentions.length) { return mentions; }

        const reducedMentions = [];

        mentions.forEach(mention => {

            // the new mention with less content
            const reducedMention = _.copy(mention, true);
            // the properties that will be removed from the mentions
            const props = ['id', 'contact_id', 'contact_folder'];

            props.forEach(prop => { delete reducedMention[prop]; });

            reducedMentions.push(reducedMention);
        });

        return reducedMentions;
    }

    /**
     * Additional tasks that are required after changing the comment display mode. This
     * are tasks to restore range markers or taking care of selected comments.
     *
     * @param {String} selectedCommentId
     *  The ID of an optionally selected comment.
     *
     * @param {String} newMode
     *  The new display mode.
     */
    #afterSwitchDisplayModeTasks(selectedCommentId, newMode) {

        // the selected comment model
        let selectedCommentModel = null;

        if (CommentLayer.ALWAYS_VISIBLE_COMMENT_LINES_MODE[newMode]) {
            this.docModel.trigger('update:rangeMarkers');
        }

        if (selectedCommentId) {
            selectedCommentModel = this.#commentCollection.getById(selectedCommentId);
            this.#commentCollection.selectComment(selectedCommentModel);
            if (this.#isBubbleMode) { this.#showCommentThreadInBubbleMode(selectedCommentModel); }
        } else if (this.#commentCollection.containsTemporaryModel()) {
            this.#commentCollection.selectTemporaryComment();
            if (this.#isBubbleMode) { this.#showCommentThreadInBubbleMode(this.#commentCollection.getTemporaryModel()); }
        }
    }

    /**
     * Comment specific handling for 'operations:after' handler.
     */
    #operationsAfterHandler(operations) {

        if (!this.#commentCollection.containsTemporaryModel()) { return; } // fast exit, nothing to do

        // the temporary comment model
        const temporaryModel = this.#commentCollection.getTemporaryModel();
        // the saved selection, that need to be updated
        const selectionState = temporaryModel.getSelectionState();

        if (selectionState && operations) { this.trigger('transform:selectionstate', selectionState, [{ operations }]); }
    }

    /**
     * Comment specific handling after an undo operation.
     */
    #handleUndoAfter() {
        if (this.#undoRedoWithTargetComments) {
            this.#undoRedoWithTargetComments = false;
            this.#updateCommentsLayer(); // one additional update is required (DOCS-3116)
        }
    }

    /**
     * Comment specific handling after reloading the document (for example after
     * canceling a long running action).
     *
     * @param {Snapshot} state
     *  The snapshot object describing the saved document state.
     */
    #handleDocumentReload(state) {

        if (!this.docApp.isTextApp()) { return; } // comments not supported for OX Presentation yet
        // the page node
        const pageNode = this.docModel.getNode();
        // refreshing the children of the page node that can contain comment place holder nodes
        this.#contentChildren = this.docApp.isODF() ? getContentChildrenOfPage(pageNode) : getPageContentNode(pageNode);
        // updating the comment place holder nodes in the comment collection
        this.#commentCollection.iterateCommentModels(commentModel => { this.#refreshLinkForComment(commentModel); });
        // refreshing the range marker model (still required?)
        this.#updateModel();
        // switching once to a visible comment display mode, even if it was hidden before
        this.switchCommentDisplayMode(this.getDefaultDisplayMode(), { forceSwitch: true });
        // adding comments into the comments pane
        this.docApp.docView.updateCommentsIfRequired();
        // updating vertical distances of comments
        this._updateComments();
        // reactivating the display mode that was active, when the snapshot was created
        this.switchCommentDisplayMode(state.getCommentDisplayMode(), { forceSwitch: true });
        // updating the horizontal comment layer position
        this.#updateHorizontalCommentLayerPosition();
    }

    /**
     * Function that is executed after the 'import success' event of the application.
     */
    #importSuccessHandler() {

        // generating the text comments pane at the view
        this.docApp.docView.createTextCommentsPane(this.getOrCreateCommentLayerNode({ visible: false }));
        // updating the sorted order of comment place holders after the document is loaded
        this.#updatePlaceHolderCollection({ emptyModel: true });
        // filling the model after fast load import is done (if not already done with event 'authorList:update')
        if (this.docModel.isFastLoadImport() && !this.#commentModelFilled) { this.#fillModelAfterFastLoad(null, null, { updatePlaceHolders: false }); }
        // checking that every comment has a valid parent -> make children with invalid parent to parent (DOCS-3259)
        if (!this.docModel.isLocalStorageImport() && !this.docModel.isFastLoadImport()) { this.#commentCollection.checkParentValidity(); }
        // updating the comments model after load from local storage or (but only for ODF) after load with fast load
        if (this.docModel.isLocalStorageImport() || (this.docApp.isODF() && this.docModel.isFastLoadImport())) { this.#updateModel({ importSuccess: true }); }
        // removing empty spans, checking parent structure in ODF
        this.#finalizeComments(this.docModel.isLocalStorageImport(), this.docModel.isFastLoadImport());
        // making the comment layer visible (can be invisible until yet)
        if (this.#commentLayerNode) { this.#commentLayerNode.css('visibility', ''); }
        // adding comments into the comments pane
        this.docApp.docView.updateCommentsIfRequired();
        // updating vertical distances of comments -> this is also sync and must be called after the comment generation
        this._updateComments();
        // checking if a deep link to a comment is specified
        this.#setDeepLink();
        // hiding the comment layer, if there are no comments
        if (this.isEmpty()) { this.switchCommentDisplayMode(CommentLayer.DISPLAY_MODE.HIDDEN); }
        // inform listeners, that a comment layer was created, if there are comments in the document
        if (this.#isCommentLayerVisible()) { this.trigger('commentlayer:created', { width: this.#commentLayerNode ? this.#commentLayerNode.width() : 0 }); }
        // switching to the display mode that was active before reloading the document
        if (this.docApp.isReloading() && this.docApp.getLaunchOption('commentDisplayMode')) { this.switchCommentDisplayMode(this.docApp.getLaunchOption('commentDisplayMode')); }
        // triggering an update of the author list of all comments
        this.trigger('update:commentauthorlist');
        // register handler for 'operations:after' to update positions temporary comment model
        this.listenTo(this.docModel, 'operations:after', this.#operationsAfterHandler);
    }
}

// constants --------------------------------------------------------------

/**
 * Display types for the comments:
 *     'ALL':      All ranges of the comments are always visible
 *     'SELECTED': Only the selected and the hovered comment range are visible
 *     'BUBBLES':  Shows small bubbles on right document margin instead of comment layer
 *     'NONE':     No ranges of any comment is visible
 *     'HIDDEN':   The comments layer is not visible
 */
CommentLayer.DISPLAY_MODE = {
    HIDDEN: 'hidden',
    NONE: 'none',
    BUBBLES: 'bubbles',
    SELECTED: 'selected',
    ALL: 'all'
};

/**
 * The class names that must be set at the comments pane root node dependent from the
 * current display mode.
 */
CommentLayer.ALL_DISPLAY_MODE_CLASSNAMES = 'nonemode bubblesmode selectedmode allmode hiddenmode';

/**
 * An object that contains the class names (as value) that must be set at the comments pane root node
 * dependent from the current display mode (as key).
 */
CommentLayer.DISPLAY_MODE_CLASSNAMES = {
    hidden: 'hiddenmode',
    none: 'nonemode',
    bubbles: 'bubblesmode',
    selected: 'selectedmode',
    all: 'allmode'
};

/**
 * A collector containing only those display modes, in which the comment thread are visible.
 * This is not the case for 'hidden', where no comments are visible and for 'bubbles', where
 * bubbles represent the comment thread.
 */
CommentLayer.VISIBLE_COMMENT_THREAD_MODES = {
    none: 1,
    selected: 1,
    all: 1
};

/**
 * A collector containing only those display modes, in which the comment lines that connect
 * the comments and the comment selections are always visible.
 */
CommentLayer.ALWAYS_VISIBLE_COMMENT_LINES_MODE = {
    all: 1
};

/**
 * Marker class to differentiate comment thread bubbles with only one comment from those
 * with more than one comment.
 */
CommentLayer.MULTI_COMMENT_THREAD = 'multicomment';

/**
 * The width of the bubble layer node in pixel.
 */
CommentLayer.BUBBLE_LAYER_WIDTH = 25;

/**
 * The maximum height of the comment content node in pixel, if the comment thread is not active.
 * This value is also needs to be adapted in less file as 'max-height' for class 'inactivethread'.
 */
CommentLayer.COMMENT_CONTENT_MAXHEIGHT = 36;

/**
 * The class name used to marked comment threads that are not active.
 */
CommentLayer.INACTIVE_THREAD_CLASS = 'inactivethread';
