/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { math } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, TOUCH_DEVICE, containsNode, Rectangle } from '@/io.ox/office/tk/dom';
import { TrackingDispatcher } from '@/io.ox/office/tk/tracking';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { MIN_MOVE_DURATION, SOFTKEYBORD_TOGGLE_MODE, convertHmmToLength, convertLengthToHmm,
    findNextNode, findPreviousNode, getBooleanOption, getDomNode } from '@/io.ox/office/tk/utils';
import { AUTORESIZEHEIGHT_SELECTOR, FRAME_WITH_TEMP_BORDER2, GROUPCONTENT_SELECTOR, NODE_SELECTOR, ROTATE_ANGLE_HINT_SELECTOR,
    ROTATING_STATE_CLASSNAME, addRotationHint, clearAdjPoints, clearSelection, drawSelection, getDrawingRotationAngle,
    getDrawingType, getGeometryValue, getNormalizedMoveCoordinates, getNormalizedResizeDeltas, getPointPixelPosition,
    getPositionDiffAfterResize, getPxPosFromGeo, getResizerHandleType, getTextFrameNode, isActiveCropping, isAdjustmentHandle,
    isAutoResizeHeightDrawingFrame, isConnectorDrawingFrame, isFlippedHorz, isFlippedVert, isFullOdfTextframeNode,
    isGroupDrawingFrame, isMinHeightDrawingFrame, isRotateHandle, isTextFrameShapeDrawingFrame, isTwoPointShape, normalizeMoveOffset,
    removeRotateAngleHint, scaleAdjustmentValue, toggleTracking, updateCssTransform, updateResizersMousePointers } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { getCropHandleType, getDrawingMoveCropOp, getDrawingResizeCropOp, createCropMoveCallbacks, createCropResizeCallbacks, refreshCropFrame } from '@/io.ox/office/textframework/components/drawing/imagecropframe';
import { COMMENTMARGIN_CLASS, PARAGRAPH_NODE_SELECTOR, TABLE_CELLNODE_SELECTOR, TABLE_NODE_SELECTOR, getCellContentNode,
    getDrawingPlaceHolderNode, getPageContentNode, isAbsoluteParagraphDrawing, isDrawingLayerNode, isExceededSizeTableNode,
    isFooterNode, isHardBreakNode, isHeaderOrFooter, isImplicitParagraphNode, isInlineDrawingNode, isManualPageBreakNode,
    isMarginalNode, isNodeInsideTextFrame, isTableCellNode } from '@/io.ox/office/textframework/utils/dom';
import { getLeadingFloatingDrawingCount, getOxoPosition, getParagraphElement, getParagraphNodeLength,
    getPixelPositionOfSelectionInSelectionLayer, getUnrotatedPixelPositionToRoot, getVerticalPagePixelPosition,
    increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import { INSERT_DRAWING, MOVE, PARA_INSERT, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';

// public functions ===========================================================

/**
 * Shortcut for updating the size and position of the selection in the selection
 * overlay node without repainting the complete drawing selection. This affects
 * all drawings, that have a selection inside the drawing overlay node. If this
 * selection gets a new size and position, this function can be used to update
 * this values.
 *
 * @param {TextApplication} app
 *  The application instance containing the drawing.
 *
 * @param {HTMLElement|jQuery} drawing
 *  The drawing node, whose selection need to be updated. As DOM node or jQuery object.
 */
export function updateOverlaySelection(app, drawing) {

    var // the pixel position of the selection in the selection overlay node relative to the page
        pos = null,
        // the text model
        model = app.getModel(),
        // the root node of the editor DOM, or if inside header/footer, one of their root nodes
        rootNode = model.getCurrentRootNode();

    if (drawing.data('selection')) {
        const zoomFactor = app.docView.getZoomFactor() * 100;
        pos = getPixelPositionOfSelectionInSelectionLayer(rootNode, model.getNode(), drawing, zoomFactor);
        drawing.data('selection').css({ transform: drawing.css('transform'), left: pos.x, top: pos.y, width: drawing.width(), height: drawing.height() });
    }
}

/**
 * Draws a selection box for the specified drawing node and registers
 * mouse handlers for moving and resizing.
 *
 * @param {TextApplication} app
 *  The application instance containing the drawing.
 *
 * @param {HTMLElement|jQuery} drawingNode
 *  The drawing node to be selected, as DOM node or jQuery object.
 */
export function drawDrawingSelection(app, drawingNode) {

    var // the text editor
        editor = app.getModel(),
        // the root node of the editor DOM, or if inside header/footer, one of their root nodes
        rootNode = editor.getCurrentRootNode(),
        // the node containing the scroll bar
        scrollNode = app.getView().getContentRootNode(),
        // whether this is an additinal text frame selection
        isTextSelection = editor.getSelection().isAdditionalTextframeSelection(),
        // object containing information whether the selected drawing is movable, resizable, rotatable or adjustable
        options = SMALL_DEVICE ? { movable: false, resizable: !isTextSelection, rotatable: !isTextSelection, adjustable: !isTextSelection } : { movable: true, resizable: true, rotatable: true, adjustable: true },  // -> parameter?
        // whether the drawing is located in the drawing layer
        isDrawingLayerNodeLocal = isDrawingLayerNode($(drawingNode).parent()),
        // whether the drawing is anchored to the paragraph
        isAbsoluteParagraphNode = !isDrawingLayerNodeLocal && isAbsoluteParagraphDrawing(drawingNode),
        // whether the selected drawing is of type 'connector'
        isConnector = isConnectorDrawingFrame(drawingNode),
        // whether the drawing selection shows only two points, like for lines and arrows
        isTwoPointSelection = isTwoPointShape(drawingNode),
        // the container element used to visualize the selection
        selectionBox = null,
        // the container element used to visualize the movement and resizing
        moveBox = null,
        //zoom factor in floating point notation
        zoomFactor,
        // target ID of currently active root node, if existing
        target = editor.getActiveTarget(),
        // the empty drawing node that contains the selection node
        selectionDrawing = null,
        // whether this drawing is an inline drawing (when it is selected)
        startInlineDrawing = isInlineDrawingNode(drawingNode),
        // some additional properties for generated operations
        operationProperties = {};

    // common deinitialization after move or resize tracking
    function leaveTracking() {
        moveBox.css({ left: '', top: '', right: '', bottom: '', width: '', height: '' });
        editor.getSelection().setActiveTracking(false); // removing the tracking marker at the selection
    }

    function extendAndGenerateOp(generator, drawingNode, operationProperties, target) {
        editor.extendPropertiesWithTarget(operationProperties, target);
        generator.generateOperation(SET_ATTRIBUTES, operationProperties);
    }

    function forceRepaintDrawingSelection() {
        clearSelection(drawingNode);
        drawDrawingSelection(app, drawingNode);
    }

    /**
     * Creates callback handlers for tracking notifications while moving a drawing.
     */
    function createDrawingMoveCallbacks() {

        var // size of the drawing
            drawingWidth = 0, drawingHeight = 0,
            // the current position change (in px)
            shiftX = 0, shiftY = 0,
            // the current scroll position
            scrollX = 0, scrollY = 0,
            // the start scroll position
            startScrollX = 0, startScrollY = 0,
            // left/top distance from drawing to event point (in px)
            leftDrawingDistance = 0, topDrawingDistance = 0,
            // the page node
            pageNode = editor.getNode(),
            // the width of the page node, taking care of the comment margin
            effectivePageNodeWidth = Math.round(pageNode.outerWidth(true) - (pageNode.hasClass(COMMENTMARGIN_CLASS) ? pageNode.data(COMMENTMARGIN_CLASS) : 0)),
            // the reference node for the drawing: the page node for page aligned drawings or the pagecontent node for paragraph aligned drawings
            referenceNode = isDrawingLayerNodeLocal ? pageNode : getPageContentNode(pageNode),
            // the width of the page node or the page content node
            referenceNodeWidth = 0,
            // the height of the page content node
            referenceNodeHeight = 0,
            // whether the mouse was really moved (Fix for 28633)
            mouseMoved = false,
            // the left offset of the page content node -> the drawing must be right of this position
            minLeftPosition = 0,
            // the left offset of the page content node plus its width -> the drawing must be left of this position
            maxRightPosition = 0,
            // the top position of the page content, not the page itself
            minTopPosition = 0,
            // adding the height of the page content (can be only one paragraph) to the top position of the page content
            maxBottomPosition = 0,
            // whether the move of the drawing can be change tracked
            // -> MS Word supports only moving of inline drawings (from inline to inline)
            // -> We do not support 'inline to inline' (in the moment), so that all move operations will not be change tracked yet.
            doChangeTrackMove = editor.getChangeTrack().ctSupportsDrawingMove(),
            // whether the moveBox was already checked
            moveBoxCheckDone = false;

        function prepareMoveDrawing(event) {
            // handling for text frame drawings, if the click happened inside the internal text frame element
            // -> but not doing so, if the drawing is located inside a comment node (53081)
            var selection = editor.getSelection();
            var isInsideTextFrame = TOUCH_DEVICE ? editor.isNodeInsideTextframe(event.target) : editor.isTextframeOrInsideTextframe(event.target);
            if (!isInsideTextFrame && (!selection.isSelectedDrawingNode(drawingNode) || selection.isAdditionalTextframeSelection())) { return false; } // the event will be triggered again
            return !isInsideTextFrame || editor.isCommentFunctionality();
        }

        // initialing the moving of the drawing according to the passed tracking event
        function startMoveDrawing(record) {

            zoomFactor = app.getView().getZoomFactor();

            // storing old height and width of drawing
            drawingWidth = drawingNode.width();
            drawingHeight = drawingNode.height();

            // updating whether the drawing is located in the drawing layer
            isDrawingLayerNodeLocal = isDrawingLayerNode(drawingNode.parent());

            effectivePageNodeWidth = Math.round(pageNode.outerWidth(true) - (pageNode.hasClass(COMMENTMARGIN_CLASS) ? pageNode.data(COMMENTMARGIN_CLASS) : 0));
            referenceNodeWidth = isDrawingLayerNodeLocal ? effectivePageNodeWidth : Math.round(referenceNode.width());
            referenceNodeHeight = Math.round(referenceNode.outerHeight(true));

            leftDrawingDistance = Math.round(record.point.x - drawingNode.offset().left);
            topDrawingDistance = Math.round(record.point.y - drawingNode.offset().top);

            startScrollX = scrollNode.scrollLeft();
            startScrollY = scrollNode.scrollTop();

            // the left offset of the page content node -> the drawing must be right of this position
            minLeftPosition = Math.round(referenceNode.offset().left);
            // the top position of the page content, not the page itself
            minTopPosition = editor.isHeaderFooterEditState() ? Math.round(rootNode.offset().top)  : Math.round(referenceNode.offset().top);

            if (isDrawingLayerNodeLocal) {
                // Forcing the drawing to stay on the page
                // the left offset of the page content node plus its width -> the drawing must be left of this position
                maxRightPosition = minLeftPosition + referenceNodeWidth - drawingWidth * zoomFactor;
                // adding the height of the page content (can be only one paragraph) to the top position of the page content
                maxBottomPosition = minTopPosition + (editor.isHeaderFooterEditState() ? (isDrawingLayerNodeLocal ? rootNode.outerHeight(true) : rootNode.height()) :  referenceNodeHeight) - drawingHeight * zoomFactor;
            } else {
                // the left offset of the page content node plus its width -> the drawing must be left of this position
                maxRightPosition = minLeftPosition + (referenceNodeWidth * zoomFactor);
                // but (paragraph aligned) drawings must always stay completely on the page
                const maxRightForPage = Math.round(pageNode.offset().left) + effectivePageNodeWidth - drawingWidth * zoomFactor;
                if (maxRightForPage < maxRightPosition) { maxRightPosition = maxRightForPage; }
                // adding the height of the page content (can be only one paragraph) to the top position of the page content
                maxBottomPosition = minTopPosition + ((editor.isHeaderFooterEditState() ? (isDrawingLayerNodeLocal ? rootNode.outerHeight(true) : rootNode.height()) :  referenceNodeHeight) * zoomFactor);
            }
        }

        // updating the drawing position according to the passed tracking event
        function updateMove(record) {
            var flipH, flipV, rotation, normalizedCoords;

            // clicking on an unselected drawing might lead to a moveBox, that is not inside the DOM
            if (!moveBoxCheckDone) {
                if (moveBox && moveBox.length > 0 && containsNode(pageNode, moveBox)) {
                    moveBoxCheckDone = true; // checking only once
                } else if (!startInlineDrawing && drawingNode.data('selection')) {
                    moveBox = drawingNode.data('selection').find('.tracker'); // reset the move box
                }
            }

            // make move box visible when tracking position has moved
            toggleTracking(drawingNode, true);
            if (drawingNode.data('selection')) { toggleTracking(drawingNode.data('selection'), true); }

            // reading scrollPosition again. Maybe it was not updated or not updated completely.
            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;

            shiftX = (record.offset.x + scrollX) / zoomFactor;
            shiftY = (record.offset.y + scrollY) / zoomFactor;

            // only move the moveBox, if the mouse was really moved (Fix for 28633)
            mouseMoved = (shiftX !== 0) || (shiftY !== 0);

            if (mouseMoved) {

                if (record.point.x - leftDrawingDistance < minLeftPosition) { shiftX += (minLeftPosition - record.point.x + leftDrawingDistance) / zoomFactor; }
                if (record.point.x - leftDrawingDistance > maxRightPosition) { shiftX += (maxRightPosition - record.point.x + leftDrawingDistance) / zoomFactor; }

                // the upper left corner of the drawing always has to be inside the page content node -> reduce or increase shiftX and shiftY, if necessary
                if (record.point.y - topDrawingDistance < minTopPosition) { shiftY += (minTopPosition - record.point.y + topDrawingDistance) / zoomFactor; }
                if (record.point.y - topDrawingDistance > maxBottomPosition) { shiftY += (maxBottomPosition - record.point.y + topDrawingDistance) / zoomFactor; }

                if ((shiftX !== 0) || (shiftY !== 0)) {
                    if (record.modifiers.shiftKey && !isActiveCropping(drawingNode)) {
                        if (Math.abs(shiftX) > Math.abs(shiftY)) {
                            shiftY = 0;
                        } else {
                            shiftX = 0;
                        }
                    }

                    flipH = isFlippedHorz(drawingNode);
                    flipV = isFlippedVert(drawingNode);
                    rotation = getDrawingRotationAngle(editor, drawingNode);
                    normalizedCoords = getNormalizedMoveCoordinates(shiftX, shiftY, rotation, flipH, flipV);
                    moveBox.css({ left: normalizedCoords.x, top: normalizedCoords.y, width: drawingWidth, height: drawingHeight });
                }
            }
        }

        // updates scroll position according to the passed tracking event
        function updateMoveScroll(record) {

            // update scrollPosition with suggestion from event
            if (record.scroll.x) {
                scrollNode.scrollLeft(scrollNode.scrollLeft() + record.scroll.x);
            }

            if (record.scroll.y) {
                scrollNode.scrollTop(scrollNode.scrollTop() + record.scroll.y);
            }

            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;
        }

        // handling the drawing position, when moving is stopped according to the passed tracking event
        function stopMoveDrawing(record) {

            // mouse up handler
            var generator = editor.createOperationGenerator(),
                // the horizontal and vertical move of the drawing in 1/100 mm
                moveX = 0, moveY = 0,
                // the logical position where the drawing is located before it needs to be moved
                updatePosition = null,
                // the attributes of the drawing node
                drawingNodeAttrs = null,
                // the anchorHorOffset and anchorVertOffset properties of the drawing
                anchorHorOffset = 0, anchorVertOffset = 0,
                // the anchorHorBase and anchorVertBase properties of the drawing
                anchorHorBase = 0, anchorVertBase = 0,
                // the anchorHorAlign an anchorHorAlign properties of the drawing
                anchorHorAlign = 0, anchorVertAlign = 0,
                // the saved anchorHorOffset and anchorVertOffset properties of the drawing before move
                oldAnchorHorOffset = 0, oldAnchorVertOffset = 0,
                // paragraph node required for checking implicit paragraphs
                localParagraph = null,
                // logical paragraph position required for checking implicit paragraphs
                localPosition = null,
                // the logical destination for moved images
                destPosition = null,
                // current drawing width and height, in 1/100 mm
                drawingWidth = 0, drawingHeight = 0,
                // the paragraph element containing the drawing node
                paragraph = null,
                // total width of the paragraph, in 1/100 mm
                paraWidth = 0,
                // is it necessary to move the image?
                moveImage = false,
                // whether the drawing was moved or not
                imageMoved = false,
                // whether the image was moved downwards caused by the resize operation
                moveDownwards = false,
                // position of the mouse up event shifted into the document borders
                trimmedPosition = null,
                // the current position sent by the event (in px)
                currentX = 0, currentY = 0,
                // the position of the top left corner of the drawing
                drawingLeftX = 0, drawingTopY = 0,
                // attributes object for operations
                allDrawingAttrs = null,
                // the old drawing attributes
                oldDrawingAttrs = null,
                // value of margin top of drawing node
                drawingMarginTop = 0,
                // maximum allowed height of page without page margins, in 1/100 mm
                pageMaxHeight = editor.getPageLayout().getDefPageActiveHeight(),
                // the start position of the drawing before it was moved relative to the page in pixel
                startPosition = null,
                // the maximum vertical offset for drawings at pages (page height minus drawing height)
                maxVertPageOffset = 0,
                // the page layout manager
                pageLayout = null,
                // the page number
                pageNumber = 1,
                // the active root node, might be header or footer
                activeRootNode = editor.getCurrentRootNode(),
                // whether the active root node is a header or footer node
                isHeaderOrFooterLocal = isHeaderOrFooter(activeRootNode),
                // an optional footer offset
                footerOffset = 0,
                // whether position relative to margin shall be calculated instead of page
                keepMarginSettings = false,
                // offset of the drawing against rotation angle
                normalizedOffset = null,
                // the minimum time for a valid move operation (not checked in unit tests)
                minMoveTime = editor.ignoreMinMoveTime() ? -1 : MIN_MOVE_DURATION,
                // checking, if this is a wanted move operation or just triggered by selecting the drawing
                validOperation = minMoveTime <= record.duration;

            function adaptPositionIntoDocument(posX, posY) {

                // taking care of leftDrawingDistance and topDrawingDistance, so that the upper left corner is inside the pageContent.
                if (posX < (minLeftPosition + leftDrawingDistance) / zoomFactor) { posX = (minLeftPosition + leftDrawingDistance) / zoomFactor; }
                if (posX > (maxRightPosition + leftDrawingDistance) / zoomFactor) { posX = (maxRightPosition + leftDrawingDistance) / zoomFactor; }
                if (posY < (minTopPosition + topDrawingDistance) / zoomFactor) { posY = (minTopPosition + topDrawingDistance) / zoomFactor; }
                if (posY > (maxBottomPosition + topDrawingDistance) / zoomFactor) { posY = (maxBottomPosition + topDrawingDistance) / zoomFactor; }

                return { posX, posY };
            }

            // check, whether a specified node contains the pixel position defined by posX and posY
            function isPositionInsideNode(node, posX, posY/*, reverse*/) {

                function isInside(node, x, y) {

                    return ((Math.round(node.offset().left / zoomFactor) <= x) &&
                        (x <= Math.round((node.offset().left / zoomFactor) + node.outerWidth())) &&
                        (Math.round(node.offset().top  / zoomFactor) <= y) &&
                        (y <= Math.round((node.offset().top / zoomFactor) + node.outerHeight())));

                }

                if (!(node instanceof $)) { node = $(node); }

                return isInside(node, posX, posY);
            }

            function iterateSelectorNodes(topNode, currentNode, posX, posY, selector, skipSelector, options) {

                var selectorNode = null,
                    reverse = getBooleanOption(options, 'reverse', false);

                while (currentNode) {

                    if (isPositionInsideNode(currentNode, posX, posY, reverse)) {
                        selectorNode = currentNode;
                        break;
                    }

                    if (reverse) {
                        currentNode = findPreviousNode(topNode, currentNode, selector, skipSelector);
                    } else {
                        currentNode = findNextNode(topNode, currentNode, selector, skipSelector);
                    }
                }

                return selectorNode;
            }

            function getParagraphAtPosition(topNode, startNode, shiftX, shiftY, posX, posY) {

                var searchPrevious = true,
                    searchFollowing = true,
                    paragraph = null,
                    tableCell = null,
                    startIsMarginal = isMarginalNode(startNode),
                    endIsMarginal = false;

                if ((shiftX > 0) && (shiftY > 0)) { searchPrevious = false; }
                if ((shiftX < 0) && (shiftY < 0)) { searchFollowing = false; }

                if (searchFollowing) {
                    paragraph = iterateSelectorNodes(topNode, getDomNode(startNode), posX, posY, PARAGRAPH_NODE_SELECTOR, NODE_SELECTOR, { reverse: false });
                }

                searchPrevious = !paragraph;

                if (searchPrevious) {
                    paragraph = iterateSelectorNodes(topNode, getDomNode(startNode), posX, posY, PARAGRAPH_NODE_SELECTOR, NODE_SELECTOR, { reverse: true });
                }

                // maybe the paragraph is in a table cell with a cell neighbor that is much higher -> use last paragraph in this cell
                if (!paragraph) {
                    tableCell = iterateSelectorNodes(topNode, getDomNode(startNode), posX, posY, TABLE_CELLNODE_SELECTOR, NODE_SELECTOR, { reverse: false });

                    if (!tableCell) {
                        tableCell = iterateSelectorNodes(topNode, getDomNode(startNode), posX, posY, TABLE_CELLNODE_SELECTOR, NODE_SELECTOR, { reverse: true });
                    }

                    if (tableCell && isTableCellNode(tableCell) && !isExceededSizeTableNode($(tableCell).closest(TABLE_NODE_SELECTOR))) { // check if table is exceeded size for #33275
                        var cellContentNode = getCellContentNode(tableCell);
                        if (cellContentNode && cellContentNode.length > 0) { // no cell content node in page-break rows
                            paragraph = cellContentNode[0].lastChild;  // the last paragraph of the cell content
                        }
                    }
                }

                if (paragraph) {
                    paragraph = $(paragraph);
                    endIsMarginal = isMarginalNode(paragraph);
                    if (startIsMarginal !== endIsMarginal) { paragraph = null; } // found another paragraph, but that is not valid (57596)
                }

                return paragraph;
            }

            // helper function to receive a reasonable value for inserting
            // a moved drawing with activated change tracking.
            function getDrawingInsertPosition() {

                var // the best possible position to insert the drawing
                    insertPosition = _.clone(updatePosition),  // shifting drawing inside the same paragraph
                    // a local helper node
                    localDrawingNode = drawingNode;

                // handling drawings in drawing layer
                if (isDrawingLayerNode($(localDrawingNode).parent())) {
                    localDrawingNode = $(getDrawingPlaceHolderNode(localDrawingNode));
                    if (!paragraph) { paragraph = localDrawingNode.parent(); }
                }

                insertPosition[insertPosition.length - 1] = getLeadingFloatingDrawingCount(paragraph, anchorVertOffset, localDrawingNode); //#33245 passing optional drawing node, to exclude itself from count

                return insertPosition;
            }

            // begin of stopMoveDrawing

            if (!mouseMoved || !validOperation) {
                return;
            }

            zoomFactor = app.getView().getZoomFactor();

            // setting position of tracking notification
            currentX = record.point.x / zoomFactor;
            currentY = record.point.y / zoomFactor;

            // scrolling to the correct position -> absolutely necessary!
            scrollNode.scrollLeft(scrollX + startScrollX).scrollTop(scrollY + startScrollY);

            // shifting currentX and currentY to position inside the document
            trimmedPosition = adaptPositionIntoDocument(currentX, currentY);
            currentX = trimmedPosition.posX;
            currentY = trimmedPosition.posY;

            // calculating the real shift of the drawing
            shiftX = currentX - ((record.start.x - scrollX) / zoomFactor);
            shiftY = currentY - ((record.start.y - scrollY) / zoomFactor);

            // top left corner of the drawing
            normalizedOffset = normalizeMoveOffset(editor, drawingNode, moveBox);
            drawingLeftX = normalizedOffset.left / zoomFactor;
            drawingTopY = normalizedOffset.top / zoomFactor;

            updatePosition = getOxoPosition(rootNode, drawingNode, 0);

            if ((shiftX !== 0) || (shiftY !== 0)) {
                if (record.modifiers.shiftKey) {
                    if (Math.abs(shiftX) > Math.abs(shiftY)) {
                        shiftY = 0;
                    } else {
                        shiftX = 0;
                    }
                }

                drawingNodeAttrs = editor.drawingStyles.getElementAttributes(drawingNode).drawing;

                if (isDrawingLayerNodeLocal) {  // the drawing is anchored to page or margin

                    activeRootNode = editor.getCurrentRootNode();

                    isHeaderOrFooterLocal = isHeaderOrFooter(activeRootNode);

                    startPosition = getUnrotatedPixelPositionToRoot(activeRootNode, drawingNode, getDrawingRotationAngle(editor, drawingNode), zoomFactor);

                    // checking the page number of the drawing
                    pageLayout = app.getModel().getPageLayout();

                    maxVertPageOffset = convertHmmToLength(pageLayout.getPageAttribute('height'), 'px', 0.001) - drawingNode.height();

                    // checking the page number of the drawing
                    pageNumber = isHeaderOrFooterLocal ? 1 : pageLayout.getPageNumber(getDrawingPlaceHolderNode(drawingNode));

                    if (pageNumber > 1) { startPosition.y -= getVerticalPagePixelPosition(pageNode, pageLayout, pageNumber, zoomFactor * 100); }

                    anchorHorOffset = startPosition.x + shiftX;
                    anchorVertOffset = startPosition.y + shiftY;

                    if (isFooterNode(activeRootNode)) {
                        // if this is a footer, the height between page start and footer need to be added
                        footerOffset = convertHmmToLength(pageLayout.getPageAttribute('height'), 'px', 1) - Math.round($(activeRootNode).outerHeight(true));
                        anchorVertOffset += footerOffset;
                    }

                    if (anchorVertOffset < 0) {
                        anchorVertOffset = 0; // forcing on the same page, no shift to page above
                    } else if (anchorVertOffset > maxVertPageOffset) {
                        anchorVertOffset = maxVertPageOffset; // forcing on the same page, no shift to page below
                    }

                    // converting anchorHorOffset and anchorVertOffset to 1/100 mm
                    anchorHorOffset = convertLengthToHmm(anchorHorOffset, 'px');
                    anchorVertOffset = convertLengthToHmm(anchorVertOffset, 'px');

                    keepMarginSettings = (drawingNodeAttrs.anchorHorBase === 'margin' && drawingNodeAttrs.anchorVertBase === 'margin');

                    if (keepMarginSettings) {
                        anchorHorOffset -= pageLayout.getPageAttribute('marginLeft');
                        anchorVertOffset -= pageLayout.getPageAttribute('marginTop');
                    }

                    anchorHorAlign = 'offset';
                    anchorHorBase = keepMarginSettings ? 'margin' : 'page';

                    anchorVertAlign = 'offset';
                    anchorVertBase = keepMarginSettings ? 'margin' : 'page';

                } else {   // the drawing is anchored to paragraph or in-line with text

                    paragraph = drawingNode.parent();
                    // converting to 1/100 mm
                    moveX = convertLengthToHmm(shiftX, 'px');
                    moveY = convertLengthToHmm(shiftY, 'px');
                    drawingWidth = convertLengthToHmm(drawingNode.width(), 'px');
                    drawingHeight = convertLengthToHmm(drawingNode.height(), 'px');
                    paraWidth = convertLengthToHmm(paragraph.width(), 'px');
                    // evaluating attributes
                    oldAnchorHorOffset = drawingNodeAttrs.anchorHorOffset;
                    oldAnchorVertOffset = drawingNodeAttrs.anchorVertOffset ? drawingNodeAttrs.anchorVertOffset : 0;
                    anchorHorBase = drawingNodeAttrs.anchorHorBase;
                    anchorVertBase = drawingNodeAttrs.anchorVertBase;
                    anchorHorAlign = drawingNodeAttrs.anchorHorAlign;
                    anchorVertAlign = drawingNodeAttrs.anchorVertAlign;
                    drawingMarginTop = drawingNodeAttrs.marginTop;

                    if ((oldAnchorHorOffset === undefined) || (oldAnchorHorOffset === 0)) {
                        // anchorHorOffset has to be calculated corresponding to the left paragraph border
                        if (anchorHorAlign === 'right') {
                            oldAnchorHorOffset = paraWidth - drawingWidth;
                        } else if (anchorHorAlign === 'center') {
                            oldAnchorHorOffset = Math.round((paraWidth - drawingWidth) / 2);
                        } else {
                            oldAnchorHorOffset = 0;
                        }
                    }

                    anchorHorOffset = oldAnchorHorOffset;
                    anchorVertOffset = oldAnchorVertOffset;

                    // checking position of mouse up event
                    // -> is the top-left corner of the drawing still in the same paragraph?
                    if (isPositionInsideNode(paragraph, drawingLeftX, drawingTopY)) {   // -> new position is in the same paragraph (before the drawing is shifted!)

                        if (moveX !== 0) {

                            // moving an inline drawing needs correction of old horizontal offset
                            if (isInlineDrawingNode(drawingNode)) { oldAnchorHorOffset = convertLengthToHmm((drawingNode.offset().left - drawingNode.closest('div.p').offset().left) / zoomFactor, 'px'); }

                            anchorHorOffset = oldAnchorHorOffset + moveX;
                            anchorHorAlign = 'offset';
                            anchorHorBase = 'column';
                            if (anchorHorOffset < 0) {
                                anchorHorOffset = 0;
                            } else if (anchorHorOffset > (paraWidth - drawingWidth)) {
                                anchorHorOffset = paraWidth - drawingWidth;
                            }
                        }

                        if (moveY !== 0) {
                            anchorVertAlign = 'offset';
                            anchorVertBase = 'paragraph';

                            anchorVertOffset = convertLengthToHmm((drawingTopY - (paragraph.offset().top / zoomFactor)), 'px');
                            // anchorVertOffset always has to be >= 0, not leaving the paragraph ('< 0' should never happen here)
                            if (anchorVertOffset < 0) { anchorVertOffset = 0; }

                            // correction for anchorVertOffset, sum of anchorVertOffset and height of drawing (including top margin) cannot be greater than maximum page size
                            if (anchorVertOffset > pageMaxHeight - drawingHeight - drawingMarginTop) {

                                // this might be caused by a page break node inside the drawing before the drawing
                                // -> in this case only the distance to the page break node is needed
                                if (isManualPageBreakNode(paragraph)) {
                                    var allPageBreaks = drawingNode.prevAll('.page-break');
                                    if (allPageBreaks.length > 0) {
                                        var prevPageBreak = allPageBreaks.first();
                                        if (isHardBreakNode(prevPageBreak.prev())) { // is this a hard break node before the page break?
                                            // -> setting vertical offset relative to the page break node (see also 56266)
                                            anchorVertOffset = convertLengthToHmm(drawingTopY - (prevPageBreak.offset().top / zoomFactor) - prevPageBreak.height(), 'px');
                                        }
                                    }
                                }

                                // correction still required
                                if (anchorVertOffset > pageMaxHeight - drawingHeight - drawingMarginTop) {
                                    anchorVertOffset = pageMaxHeight - drawingHeight - drawingMarginTop;
                                }
                            }
                        }

                    } else { // -> new position is in another paragraph or behind the last paragraph (before the drawing is moved!)

                        // paragraph has to be determined from the coordinates of the top left corner of the drawing
                        // -> moving operation for the drawing is always required
                        paragraph = getParagraphAtPosition(rootNode, paragraph, shiftX, shiftY, drawingLeftX, drawingTopY);

                        if (paragraph) {

                            // -> the paragraph must not be the paragraph inside a text frame (not moving inside text frames)
                            if (isNodeInsideTextFrame(paragraph)) {
                                paragraph = null;
                            } else {
                                moveImage = true;
                            }

                        } else {

                            // No paragraph found: Do not call set Attributes and not moveImage
                            moveImage = false;

                            // taking care of the offset of an inline drawing inside its paragraph (54767)
                            if (isInlineDrawingNode(drawingNode)) {
                                oldAnchorHorOffset = convertLengthToHmm((drawingNode.offset().left - drawingNode.closest('div.p').offset().left) / zoomFactor, 'px');
                                oldAnchorVertOffset = convertLengthToHmm((drawingNode.offset().top - drawingNode.closest('div.p').offset().top) / zoomFactor, 'px');
                            }

                            anchorHorOffset = oldAnchorHorOffset + moveX;
                            anchorVertOffset = oldAnchorVertOffset + moveY;

                            anchorVertAlign = 'offset';
                            anchorVertBase = 'paragraph';
                            anchorHorAlign = 'offset';
                            anchorHorBase = 'column';
                        }

                        if (moveImage) {

                            // whether the drawing is a text frame -> it must not be moved into another text frame -> simply do nothing
                            if (isTextFrameShapeDrawingFrame(drawingNode) && isNodeInsideTextFrame(paragraph)) { return; }

                            // updating horizontal and vertical positions
                            anchorVertAlign = 'offset';
                            anchorVertBase = 'paragraph';
                            anchorVertOffset = convertLengthToHmm((drawingTopY - (paragraph.offset().top / zoomFactor)), 'px');

                            anchorHorAlign = 'offset';
                            anchorHorBase = 'column';
                            anchorHorOffset = convertLengthToHmm((drawingLeftX - (paragraph.offset().left / zoomFactor)), 'px');

                            destPosition = getOxoPosition(rootNode, paragraph, 0);

                            // moving behind already existing drawings
                            destPosition.push(getLeadingFloatingDrawingCount(paragraph, drawingTopY - (paragraph.offset().top / zoomFactor)));
                        }
                    }
                }

                // Generating operations. If change tracking is activated, different operations need
                // to be created, because the drawing at the old position stays on its position but
                // needs to be marked as 'deleted'. At the new position a new drawing needs to be
                // inserted, that is marked as inserted.
                // If change tracking is NOT active, it is first necessary to switch from in-line mode
                // to floated mode. Then a move operation might be necessary and finally a further
                // setAttributes operation, that sets the attributes for the position of the drawing
                // at its new location.

                // 1. Switching from in-line to floated mode, if required, using default values for floated mode
                // -> in the case of activated change tracking this operation is not required. Instead it is
                // necessary to mark the drawing as 'deleted'.
                if ((anchorHorOffset !== oldAnchorHorOffset) || (anchorVertOffset !== oldAnchorVertOffset)) {

                    if (editor.getChangeTrack().isActiveChangeTracking() && doChangeTrackMove && !editor.getChangeTrack().isInsertNodeByCurrentAuthor(drawingNode)) {
                        // if change tracking is active, the old drawing position must be marked as 'deleted' ...
                        // -> this is necessary for in-line and non-in-line drawings
                        operationProperties = {
                            attrs: { changes: { removed: editor.getChangeTrack().getChangeTrackInfo() } },
                            start: _.clone(updatePosition)
                        };
                        editor.extendPropertiesWithTarget(operationProperties, target);
                        generator.generateOperation(SET_ATTRIBUTES, operationProperties);
                    } else if (isInlineDrawingNode(drawingNode)) {
                        // switching from in-line to floated mode (vertically aligned at paragraph)

                        var drawingAttrs = { inline: false, anchorHorBase: 'column', anchorVertBase: 'paragraph', anchorHorAlign: 'right', anchorHorOffset: 0, textWrapMode: 'topAndBottom', textWrapSide: null };

                        // getting the operation attributes for 'anchorBehindDoc' and 'anchorLayerOrder', so that the drawing is positioned at the top (54735)
                        // -> but this must never lead to a drawing behind the text (DOCS-3270)
                        var orderdrawingAttrs = editor.getDrawingLayer().getDrawingOrderAttributes('front', drawingNode);
                        if (orderdrawingAttrs && !orderdrawingAttrs.anchorBehindDoc) { _.extend(drawingAttrs, orderdrawingAttrs); }

                        operationProperties = {
                            attrs: { drawing: drawingAttrs },
                            start: _.clone(updatePosition)
                        };
                        editor.extendPropertiesWithTarget(operationProperties, target);
                        generator.generateOperation(SET_ATTRIBUTES, operationProperties);
                    }
                }

                // 2. Moving the image, if this is necessary
                // -> In case of activated change tracking, this is only necessary, if the drawing was inserted
                //    before by the current author. Never moving drawings located in the drawing layer node.
                if (moveImage && !isDrawingLayerNodeLocal && !_.isEqual(updatePosition, destPosition)) {

                    // moving the drawing, if change tracking is NOT active or it is active
                    // and the current author has inserted the drawing before
                    if (!editor.getChangeTrack().isActiveChangeTracking() || !doChangeTrackMove || (editor.getChangeTrack().isActiveChangeTracking() && editor.getChangeTrack().isInsertNodeByCurrentAuthor(drawingNode))) {

                        // check, if destPosition is located in a non-implicit paragraph -> otherwise create paragraph
                        // similar to editor.handleImplicitParagraph
                        localPosition = _.clone(destPosition);

                        if (localPosition.pop() === 0) {  // is this an empty paragraph?
                            localParagraph = getParagraphElement(rootNode, localPosition);
                            if ((isImplicitParagraphNode(localParagraph)) && (getParagraphNodeLength(localParagraph) === 0)) {
                                // removing implicit paragraph node
                                $(localParagraph).remove();
                                // creating new paragraph explicitly
                                operationProperties = { start: _.clone(localPosition) };
                                editor.extendPropertiesWithTarget(operationProperties, target);
                                generator.generateOperation(PARA_INSERT, operationProperties);
                            }
                        }

                        // move operation
                        operationProperties = {
                            start: _.clone(updatePosition),
                            end: _.clone(updatePosition),
                            to: _.clone(destPosition)
                        };
                        editor.extendPropertiesWithTarget(operationProperties, target);
                        generator.generateOperation(MOVE, operationProperties);

                        updatePosition = _.clone(destPosition); // for setting attributes required
                        if (moveDownwards) {
                            updatePosition[updatePosition.length - 1] -= 1;  // moving from [0,0] to [0,2], so that it ends at [0,1]
                        }

                        imageMoved = true;
                    }
                }

                // 3. Setting attributes to the drawing at its new location (for the correct positioning)
                if ((anchorHorOffset !== oldAnchorHorOffset) || (anchorVertOffset !== oldAnchorVertOffset)) {

                    // Inserting a new drawing if change tracking is active and the drawing was not inserted
                    // by the current author before. This is always necessary, if the offset was modified. Independent
                    // whether the drawing was moved before or not.
                    if (editor.getChangeTrack().isActiveChangeTracking() && doChangeTrackMove && !editor.getChangeTrack().isInsertNodeByCurrentAuthor(drawingNode)) {

                        // If change tracking is active and the drawing was not inserted by the current author before, then:
                        // - the drawing needs to be inserted at its new position with an insertDrawing operation.
                        // - if a move operation was required (without change tracking) the new logical position
                        //   is already determined and saved in 'destPosition'.
                        // - if no move operation was required (moving inside the same paragraph), the destPosition needs
                        //   to be determined now using helper function 'getDrawingInsertPosition'.
                        // TODO: Calling getDrawingInsertPosition() is not working for drawings in the drawing layer.
                        // -> But this is no problem, because change tracking does not support move of drawings, that
                        //    are not in-line (behavior of MS Office)
                        if (!destPosition) { destPosition = getDrawingInsertPosition(); }

                        // check, if destPosition is located in a non-implicit paragraph -> otherwise create paragraph
                        // similar to editor.handleImplicitParagraph
                        localPosition = _.clone(destPosition);

                        if (localPosition.pop() === 0) {  // is this an empty paragraph?
                            localParagraph = getParagraphElement(rootNode, localPosition);
                            if ((isImplicitParagraphNode(localParagraph)) && (getParagraphNodeLength(localParagraph) === 0)) {
                                // removing implicit paragraph node
                                $(localParagraph).remove();
                                // creating new paragraph explicitly
                                operationProperties = { start: _.clone(localPosition) };
                                editor.extendPropertiesWithTarget(operationProperties, target);
                                generator.generateOperation(PARA_INSERT, operationProperties);
                            }
                        }

                        oldDrawingAttrs = getExplicitAttributeSet(drawingNode);
                        allDrawingAttrs = oldDrawingAttrs;

                        // setting default floated value for the inserted drawing
                        if (isInlineDrawingNode(drawingNode)) {
                            allDrawingAttrs.drawing.inline = false;  // the drawing is never in-line in new position
                            allDrawingAttrs.drawing.textWrapMode = 'topAndBottom';
                            allDrawingAttrs.drawing.textWrapSide = null;
                        }

                        allDrawingAttrs.changes = { inserted: editor.getChangeTrack().getChangeTrackInfo(), removed: null };

                        operationProperties = {
                            start: _.clone(destPosition),
                            type: getDrawingType(drawingNode),
                            attrs: allDrawingAttrs
                        };
                        editor.extendPropertiesWithTarget(operationProperties, target);
                        generator.generateOperation(INSERT_DRAWING, operationProperties);

                        updatePosition = _.clone(destPosition); // for setting attributes required
                    }

                    // and finally setting the new position attributes via a setAttributes operation
                    operationProperties = {
                        attrs: { drawing: { anchorHorOffset, anchorVertOffset, anchorHorAlign, anchorVertAlign, anchorHorBase, anchorVertBase } },
                        start: _.clone(updatePosition)
                    };

                    if (isActiveCropping(drawingNode)) {
                        var cropOps = getDrawingMoveCropOp(drawingNode, getExplicitAttributes(drawingNode, 'drawing'), operationProperties.attrs.drawing);
                        if (cropOps && !_.isEmpty(cropOps)) {
                            operationProperties.attrs.image = cropOps;
                        }
                    }
                    editor.extendPropertiesWithTarget(operationProperties, target);
                    generator.generateOperation(SET_ATTRIBUTES, operationProperties);

                }
            }

            // apply the operations (undo group is created automatically)
            editor.applyOperations(generator);

            if (imageMoved) {
                // set new text selection after moving the drawing, this will repaint the selection
                editor.getSelection().setTextSelection(updatePosition, increaseLastIndex(updatePosition));
                // special handling for text frames, which need, that the frame is repainted after move (Webkit and IE)
                // -> otherwise setting a selection in the text frame will fail
                if (isTextFrameShapeDrawingFrame(drawingNode) && (_.browser.WebKit || _.browser.IE)) {
                    app.getModel().executeDelayed(function () {
                        clearSelection(drawingNode);
                        drawDrawingSelection(app, drawingNode);
                    });
                }
            }
            if (isMarginalNode(drawingNode)) {
                app.getModel().getDrawingLayer().updateCropMarginalParagraphDrawing(drawingNode);
            }

            // if the image was moved from inline to paragraph aligned it is necessary to repaint the selection,
            // because it needs to move to the selection into the selection layer.
            if (startInlineDrawing) {
                // no check of inline drawing required, because after move the drawing is no longer inline.
                // This behavior might change in the future.
                clearSelection(drawingNode);
                drawDrawingSelection(app, drawingNode);
            }

            if (isActiveCropping(drawingNode)) { refreshCropFrame(drawingNode); }
        }

        // finalizes the move tracking
        function finalizeMoveDrawing() {
            leaveTracking();

            // update an existing selection in the selection overlay node (49975)
            updateOverlaySelection(app, drawingNode);

            // Resetting variables for new mouseup events without mousemove
            shiftX = shiftY = scrollX = scrollY = 0;

            // switching from text to drawing selection
            if (isTextSelection) { editor.getSelection().switchAdditionalDrawingToDrawingSelection(); }
        }

        return {
            prepare: prepareMoveDrawing,
            start: startMoveDrawing,
            move: updateMove,
            scroll: updateMoveScroll,
            end: stopMoveDrawing,
            finally: finalizeMoveDrawing
        };
    }

    /**
     * Creates the callbacks for tracking notifications while resizing a drawing.
     */
    function createDrawingResizeCallbacks() {

        var // original size of the drawing
            oldWidth = 0, oldHeight = 0,
            // the size of the resized drawing (in px)
            finalWidth = 0, finalHeight = 0,
            // the current scroll position
            scrollX = 0, scrollY = 0,
            // the initial scroll position
            startScrollX = 0, startScrollY = 0,
            // whether resizing is available in horizontal/vertical direction
            useX = false, useY = false, topResize = false, leftResize = false,
            // correction factor for resizing to the left/top
            scaleX = 0, scaleY = 0,
            // the maximum width of the drawing on the page or in a table cell
            maxDrawingWidth = null,
            // the maximum height of the drawing (sometimes same scaling as width)
            // -> the drawing must not be higher than the page
            maxDrawingHeight = null;

        function prepareResizeDrawing(event) {
            return !!getResizerHandleType(event.target) && !getCropHandleType(event.target);
        }

        // initializes resizing the drawing according to the passed tracking event
        function startResizeDrawing(record) {

            var pos = $(record.target).attr('data-pos'),
                // the maximum height of a drawing related to its width
                newHeightToWidth = 0,
                // attributes at page
                pageAttributes = 0,
                // the paragraph element containing the drawing node
                paragraph = null;

            // updating whether the drawing is located in the drawing layer
            isDrawingLayerNodeLocal = isDrawingLayerNode(drawingNode.parent());

            zoomFactor = app.getView().getZoomFactor();

            // storing old height and width of drawing
            oldWidth = finalWidth = drawingNode.width();
            oldHeight = finalHeight = drawingNode.height();

            // collecting information about the handle node
            useX = /[lr]/.test(pos);
            useY = /[tb]/.test(pos);
            topResize = /[t]/.test(pos);
            leftResize = /[l]/.test(pos);

            if (leftResize) {
                scaleX = -1;
                moveBox.css({ left: 'auto', right: 0 });
            } else {
                scaleX = 1;
                moveBox.css({ left: 0, right: 'auto' });
            }
            if (isFlippedHorz(drawingNode)) { scaleX *= -1; }

            if (topResize) {
                scaleY = -1;
                moveBox.css({ top: 'auto', bottom: 0 });
            } else {
                scaleY = 1;
                moveBox.css({ top: 0, bottom: 'auto' });
            }
            if (isFlippedVert(drawingNode)) { scaleY *= -1; }

            startScrollX = scrollNode.scrollLeft();
            startScrollY = scrollNode.scrollTop();

            // reduce the width of the drawing, if the width is bigger than the width of
            // the page plus the page's right margin or the width of the paragraph in a table cell
            paragraph = drawingNode.parent();
            maxDrawingWidth = paragraph.width();  // using only the paragraph width, no padding or margin (task 28867)

            pageAttributes = editor.pageStyles.getElementAttributes(editor.getNode());
            // reading page attributes, they are always available -> no need to check existence
            maxDrawingHeight = pageAttributes.page.height - pageAttributes.page.marginTop - pageAttributes.page.marginBottom;
            maxDrawingHeight = convertHmmToLength(maxDrawingHeight, 'px', 1);

            // in the case of non-deforming resize, there is also a maximum value for the height (not for lines and arrows (DOCS-5073))
            if (!isTwoPointSelection && useX && useY) {
                newHeightToWidth = oldHeight * maxDrawingWidth / oldWidth;
                if ((maxDrawingHeight) && (maxDrawingHeight < newHeightToWidth)) {
                    // reduction of maxDrawingWidth is required
                    maxDrawingWidth *= (maxDrawingHeight / newHeightToWidth);
                } else {
                    maxDrawingHeight = newHeightToWidth;
                }
            }
        }

        // updates scroll position according to the passed tracking event
        function updateResizeScroll(record) {

            // update scrollPosition with suggestion from event
            scrollNode
                .scrollLeft(scrollNode.scrollLeft() + record.scroll.x)
                .scrollTop(scrollNode.scrollTop() + record.scroll.y);

            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;
        }

        // updates resizing the drawing according to the passed tracking event
        function updateResize(record) {

            var deltaX = record.offset.x / zoomFactor + scrollX;
            var deltaY = record.offset.y / zoomFactor + scrollY;
            // the scaling factor for the width
            var scaleWidth = 1;
            // the scaling factor for the height
            var scaleHeight = 1;
            // normalize and scale deltas of the dimensions
            var normalizedDeltas = getNormalizedResizeDeltas(deltaX, deltaY, useX, useY, scaleX, scaleY, getDrawingRotationAngle(editor, drawingNode));
            var sumWidth = oldWidth + normalizedDeltas.x;
            var sumHeight = oldHeight + normalizedDeltas.y;
            var signWidth = sumWidth > 0 ? 1 : -1;
            var signHeight = sumHeight > 0 ? 1 : -1;
            var flipH = sumWidth < 0;
            var flipV = sumHeight < 0;
            var transformProp = 'scaleX(' + (flipH ? -1 : 1) + ') scaleY(' + (flipV ? -1 : 1) + ')';
            var topPos, bottomPos, leftPos, rightPos;

            // The maximum value for the width is 'maxDrawingWidth' -> avoid endless scrolling
            // There is no limit for the height (only in non-deforming resizing).
            if (sumWidth >= maxDrawingWidth) {
                sumWidth = maxDrawingWidth;
            }

            if (sumHeight >= maxDrawingHeight) {
                sumHeight = maxDrawingHeight;
            }

            // use the same scaling factor for vertical and horizontal resizing, if both are enabled (and this is not a connector)
            // -> the larger number wins
            if (useX && useY && !isConnector && !isActiveCropping(drawingNode)) {
                scaleWidth = Math.abs(sumWidth / oldWidth); // scale shouldn't be negative
                scaleHeight = Math.abs(sumHeight / oldHeight);

                if (scaleWidth > scaleHeight) {
                    sumHeight = scaleWidth * oldHeight * signHeight; // return sign of current width/height after scale
                } else {
                    sumWidth = scaleHeight * oldWidth * signWidth;
                }
            }
            topPos = topResize ? 'auto' : Math.min(0, sumHeight);
            bottomPos = topResize ? Math.min(0, sumHeight) : 'auto';
            leftPos = leftResize ? 'auto' : Math.min(0, sumWidth);
            rightPos = leftResize ? Math.min(0, sumWidth) : 'auto';

            // update drawing size
            finalWidth = Math.abs(sumWidth);
            finalHeight = Math.abs(sumHeight);

            moveBox.css({ width: finalWidth, height: finalHeight, top: topPos, left: leftPos, bottom: bottomPos, right: rightPos, transform: transformProp });
            moveBox.data({ flipH, flipV });

            // make move box visible when tracking position has moved
            toggleTracking(drawingNode, true);
            if (drawingNode.data('selection')) { toggleTracking(drawingNode.data('selection'), true); }
        }

        // resizes the drawing according to the passed tracking event
        function stopResizeDrawing() {

            var generator = editor.createOperationGenerator(),
                // all attributes for the operation
                allAttrs = {},
                // drawing attributes for the operation
                attributes = {},
                // the final scaling of the drawing width and height
                finalWidthScaling = 1,
                // the paragraph containing the resized drawing
                paragraph = drawingNode.parent(),
                // the logical position where the drawing is located before it needs to be moved
                updatePosition = getOxoPosition(rootNode, drawingNode, 0),
                // the attributes of the drawing node
                drawingNodeAttrs = editor.drawingStyles.getElementAttributes(drawingNode).drawing,
                // the saved anchorVertOffset and anchorHorOffset property of the drawing before resize
                oldAnchorVertOffset = 0, oldAnchorHorOffset = 0,
                // the old drawing attributes
                oldAttrs = null,
                // the start position of the drawing before it was moved relative to the page in pixel
                startPosition = null,
                // the page layout manager
                pageLayout = null,
                // the affected drawings with auto resize functionality
                allAutoResizeNodes = null,
                // the page number
                pageNumber = 1,
                // the active root node, this can be a header or footer
                activeRootNode = null,
                // an optional offset of the footer node in the page
                footerOffset = 0,
                // whether position relative to margin shall be calculated instead of page
                keepMarginSettings = false,
                // whether drawing is alredy horizontally or vertically flipped by operation
                isFlipH = isFlippedHorz(drawingNode),
                isFlipV = isFlippedVert(drawingNode),
                // whether drawing is in process of being flipped on resizing
                isCurrentlyFlippedH = moveBox.data('flipH'),
                isCurrentlyFlippedV = moveBox.data('flipV'),
                rotationAngle = getDrawingRotationAngle(editor, drawingNode),
                positionDiff = getPositionDiffAfterResize(drawingNode, moveBox, rotationAngle, isFlipH, isFlipV),
                leftDiff = positionDiff.leftDiff / zoomFactor,
                topDiff = positionDiff.topDiff / zoomFactor,
                bottomDiff = positionDiff.bottomDiff / zoomFactor,
                leftResizeNormalized = leftDiff !== 0,
                topResizeNormalized = topDiff !== 0,
                bottomResizeNormalized = !topResizeNormalized && bottomDiff !== 0;

            // Calculating new width and height

            // calculate width
            if (useX && (finalWidth > 0) && (finalWidth !== oldWidth)) {
                attributes.width = convertLengthToHmm(finalWidth, 'px');

                if (finalWidth > maxDrawingWidth) {
                    finalWidthScaling = maxDrawingWidth / finalWidth;
                    attributes.width = convertLengthToHmm(maxDrawingWidth, 'px');
                }
            }

            // calculate height
            if (useY && (finalHeight > 0) && (finalHeight !== oldHeight)) {
                if (finalWidthScaling !== 1) { finalHeight *= finalWidthScaling; }
                attributes.height = convertLengthToHmm(finalHeight, 'px');
            }

            // simplified handling for drawings in drawing layer
            if (isDrawingLayerNodeLocal) {
                keepMarginSettings = (drawingNodeAttrs.anchorHorBase === 'margin' && drawingNodeAttrs.anchorVertBase === 'margin');
                activeRootNode = editor.getCurrentRootNode();
                zoomFactor = app.getView().getZoomFactor() * 100;

                // is it necessary to update the upper left corner of the drawing?
                //startPosition = Position.getPixelPositionToRootNodeOffset(activeRootNode, drawingNode, zoomFactor);
                startPosition = getUnrotatedPixelPositionToRoot(activeRootNode, drawingNode, rotationAngle, zoomFactor / 100);
                pageLayout = app.getModel().getPageLayout();

                // checking the page number of the drawing
                pageNumber = isHeaderOrFooter(activeRootNode) ? 1 : pageLayout.getPageNumber(getDrawingPlaceHolderNode(drawingNode));
                if (pageNumber > 1) { startPosition.y -= getVerticalPagePixelPosition(editor.getNode(), pageLayout, pageNumber, zoomFactor); }

                if (topResizeNormalized) {
                    // comparing final height and old height
                    attributes.anchorVertOffset = Math.max(convertLengthToHmm(startPosition.y, 'px') - convertLengthToHmm(topDiff, 'px'), 0);
                } else {
                    attributes.anchorVertOffset = convertLengthToHmm(startPosition.y, 'px');
                }

                if (isFooterNode(activeRootNode)) {
                    // if this is a footer, the height between page start and footer need to be added
                    footerOffset = pageLayout.getPageAttribute('height') - convertLengthToHmm($(activeRootNode).outerHeight(true), 'px');
                    attributes.anchorVertOffset += footerOffset;
                }

                if (leftResizeNormalized) {
                    // comparing final width and old width
                    attributes.anchorHorOffset = Math.max(convertLengthToHmm(startPosition.x, 'px') - convertLengthToHmm(leftDiff, 'px'), 0);
                }

                // using margin instead of page, if drawing was anchored to margin before
                if (keepMarginSettings) {
                    if (leftResizeNormalized) { attributes.anchorHorOffset -= pageLayout.getPageAttribute('marginLeft'); }
                    attributes.anchorVertOffset -= pageLayout.getPageAttribute('marginTop');
                }

                // always sending this? Or only, if it was modified? Avoid sending horizontal information, if it was not modified (43996)
                if (leftResizeNormalized) {
                    attributes.anchorHorAlign = 'offset';
                    attributes.anchorHorBase = keepMarginSettings ? 'margin' : 'page';
                }

                attributes.anchorVertAlign = 'offset';
                attributes.anchorVertBase = keepMarginSettings ? 'margin' : 'page';

            } else if (isAbsoluteParagraphNode) {

                if (!_.isEmpty(attributes)) { // not setting only 'anchorHorOffset' and 'anchorVertOffset' for resize operation

                    // if the drawing was resized to the left or to the top, it might be necessary to update the values
                    // for anchorHorOffset and anchorVertOffset
                    if (topResizeNormalized || bottomResizeNormalized) {
                        oldAnchorVertOffset = drawingNodeAttrs.anchorVertOffset || 0;

                        if ((oldAnchorVertOffset >= 0 && topResizeNormalized) || (oldAnchorVertOffset < 0 && bottomResizeNormalized)) {
                            attributes.anchorVertOffset = oldAnchorVertOffset - convertLengthToHmm(topDiff, 'px');

                            // simplification -> not allowing to resize drawing above paragraph borders
                            if (topResizeNormalized && attributes.anchorVertOffset < 0) {
                                attributes.anchorVertOffset = 0;
                            } else if (bottomResizeNormalized && attributes.anchorVertOffset >= 0) {
                                attributes.anchorVertOffset = -1;
                            }
                        }
                    }

                    // the drawing was resized in left direction
                    if (leftResizeNormalized) {
                        oldAnchorHorOffset = drawingNodeAttrs.anchorHorOffset || 0;

                        attributes.anchorHorOffset = oldAnchorHorOffset - convertLengthToHmm(leftDiff, 'px');
                        if (attributes.anchorHorOffset < 0) {  attributes.anchorHorOffset = 0; }
                    }
                }
            }

            // Generating the setAttribute operation, if necessary
            if (!_.isEmpty(attributes)) {

                // check for relative values for width, height and offset -> removing relative attributes
                if (drawingNodeAttrs.sizeRelHFrom && _.isNumber(attributes.width)) { attributes.sizeRelHFrom = null; }
                if (drawingNodeAttrs.sizeRelVFrom && _.isNumber(attributes.height)) { attributes.sizeRelVFrom = null; }

                allAttrs = { drawing: attributes };

                if (isCurrentlyFlippedH) { allAttrs.drawing.flipH = !isFlipH; }
                if (isCurrentlyFlippedV) {
                    if (app.isODF()) {
                        allAttrs.drawing.flipH = !_.isUndefined(allAttrs.drawing.flipH) ? allAttrs.drawing.flipH : !isFlipH;
                        allAttrs.drawing.rotation = math.modulo((getDrawingRotationAngle(editor, drawingNode) || 0) + 180, 360);
                    } else {
                        allAttrs.drawing.flipV = !isFlipV;
                    }
                }

                // after resizing an automatic resized text frame in vertical direction by the user,
                // the automatic resize will be removed
                if (_.isNumber(attributes.height)) {
                    if (isAutoResizeHeightDrawingFrame(drawingNode)) {
                        allAttrs.shape = { autoResizeHeight: false };
                    } else if (isGroupDrawingFrame(drawingNode)) {
                        // disable autoResizeHeight for all children inside a resized group
                        allAutoResizeNodes = drawingNode.find(AUTORESIZEHEIGHT_SELECTOR);
                        if (allAutoResizeNodes.length > 0) {
                            _.each(allAutoResizeNodes, function (resizeNode) {
                                var localDrawingNode = $(resizeNode).parent(),
                                    localDrawingPos = getOxoPosition(rootNode, localDrawingNode, 0);

                                generator.generateOperation(SET_ATTRIBUTES, {
                                    start: _.copy(localDrawingPos),
                                    attrs: { shape: { autoResizeHeight: false } }
                                });

                            });
                        }
                    }

                    // setting a minimum height at frames (ODF only), 55236
                    if (app.isODF() && isMinHeightDrawingFrame(drawingNode)) { attributes.minFrameHeight = attributes.height; }
                }

                // Resize of drawing is not supported yet in OOXML file format
                // -> this code needs to be activated, if OOXML supports change tracking of resizing of drawings
                // -> in this case ctSupportsDrawingResizing() has to return 'true'
                if (editor.getChangeTrack().isActiveChangeTracking() && editor.getChangeTrack().ctSupportsDrawingResizing()) {
                    // Expanding operation for change tracking with old explicit attributes
                    oldAttrs = editor.getChangeTrack().getOldNodeAttributes(drawingNode);
                    // adding the old attributes, author and date for change tracking
                    if (oldAttrs) {
                        oldAttrs = _.extend(oldAttrs, editor.getChangeTrack().getChangeTrackInfo());
                        allAttrs.changes = { modified: oldAttrs };
                    }
                }

                operationProperties = {
                    start: _.clone(updatePosition),
                    attrs: allAttrs
                };

                if (isActiveCropping(drawingNode)) {
                    var cropOps = getDrawingResizeCropOp(drawingNode, getExplicitAttributes(drawingNode, 'drawing'), operationProperties.attrs.drawing, leftResize, topResize);
                    if (cropOps && !_.isEmpty(cropOps)) {
                        operationProperties.attrs.image = cropOps;
                    }
                }

                editor.extendPropertiesWithTarget(operationProperties, target);
                generator.generateOperation(SET_ATTRIBUTES, operationProperties);

                clearAdjPoints(drawingNode);
            }

            // Applying the operations (undo group is created automatically)
            editor.applyOperations(generator);

            // Triggering text frame border to be repainted
            if (paragraph) { app.getModel().trigger('paragraphUpdate:after', paragraph); }

            // if drawing is inside marginal node, check cropping of drawing
            if (isMarginalNode(drawingNode)) {
                app.getModel().getDrawingLayer().updateCropMarginalParagraphDrawing(drawingNode);
            }

            if (isActiveCropping(drawingNode) && !_.isEmpty(attributes)) { refreshCropFrame(drawingNode); }
        }

        // finalizes the resize tracking
        function finalizeResizeDrawing() {
            leaveTracking();

            // update an existing selection in the selection overlay node (49975)
            updateOverlaySelection(app, drawingNode);

            app.getView().scrollToChildNode(drawingNode);

            // switching from text to drawing selection
            if (isTextSelection) { editor.getSelection().switchAdditionalDrawingToDrawingSelection(); }
        }

        return {
            prepare: prepareResizeDrawing,
            start: startResizeDrawing,
            move: updateResize,
            scroll: updateResizeScroll,
            end: stopResizeDrawing,
            finally: finalizeResizeDrawing
        };
    }

    function createDrawingRotateCallbacks() {
        // dimensions of the drawing
        var drawingWidth = 0, drawingHeight = 0;
        // position of the center rotation of the drawingWidth
        var centerX, centerY;
        // the current scroll position
        var scrollX = 0, scrollY = 0;
        // the initial scroll position
        var startScrollX = 0, startScrollY = 0;
        // starting angle before rotation
        var startAngle = 0;
        // final angle on finish of the rotation
        var rotationAngle = null;
        // flag for marking that the operation is generated
        var generatedOp = false;

        function prepareRotateDrawing(event) {
            return isRotateHandle(event.target);
        }

        function startRotateDrawing() {
            var drawingOffset;
            var attrs = getExplicitAttributeSet(drawingNode);
            var hasNoFillNoLine = attrs && (!attrs.fill || attrs.fill.type === 'none') && attrs.line && attrs.line.type === 'none';

            zoomFactor = app.getView().getZoomFactor();
            startAngle = getDrawingRotationAngle(editor, drawingNode);
            if (_.isNumber(startAngle)) {
                $(drawingNode).css('transform', ''); // reset to def to get values
                drawingOffset = drawingNode.offset();
                updateCssTransform(drawingNode, startAngle);
            } else {
                startAngle = 0;
                drawingOffset = drawingNode.offset();
            }

            drawingHeight = drawingNode.height() * zoomFactor;
            drawingWidth = drawingNode.width() * zoomFactor;
            centerX = drawingOffset.left + (drawingWidth / 2);
            centerY = drawingOffset.top + (drawingHeight / 2);

            drawingNode.addClass(ROTATING_STATE_CLASSNAME);
            if (drawingNode.data('selection')) { drawingNode.data('selection').addClass(ROTATING_STATE_CLASSNAME); }

            if (hasNoFillNoLine) { drawingNode.addClass(FRAME_WITH_TEMP_BORDER2); }

            if (editor.getSelection().isAdditionalTextframeSelection()) {
                getTextFrameNode(drawingNode, { deep: true }).attr('contenteditable', false);
            }
        }

        function updateRotate(record) {
            var radians = Math.atan2(record.point.x + scrollX - centerX, record.point.y + scrollY - centerY);
            rotationAngle = Math.round(180 - radians * 180 / Math.PI);

            if (record.modifiers.shiftKey) {
                rotationAngle = math.roundp(rotationAngle, 15); // make 15 deg step with shift key
            }
            if (isFlippedVert(drawingNode)) {
                rotationAngle += 180;
            }
            rotationAngle = math.modulo(rotationAngle, 360);
            var drawingSelectionNode = drawingNode.data('selection') || null;

            updateCssTransform(drawingNode, rotationAngle);
            if (drawingSelectionNode) { updateCssTransform(drawingSelectionNode, rotationAngle); }
            var flipH = isFlippedHorz(drawingNode);
            var flipV = isFlippedVert(drawingNode);
            var rotationHintNode = drawingSelectionNode ? drawingSelectionNode : drawingNode;
            addRotationHint(rotationHintNode.find(ROTATE_ANGLE_HINT_SELECTOR), rotationAngle, flipH, flipV);
        }

        function updateRotateScroll(record) {
            // update scrollPosition with suggestion from event
            scrollNode
                .scrollLeft(scrollNode.scrollLeft() + record.scroll.x)
                .scrollTop(scrollNode.scrollTop() + record.scroll.y);

            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;
        }

        function stopRotateDrawing() {
            var generator = editor.createOperationGenerator();
            var operationProperties;

            if (_.isNumber(rotationAngle)) {
                // normalize angle between 0 and 359 deg
                rotationAngle = math.modulo(rotationAngle, 360);

                operationProperties = {
                    start: getOxoPosition(rootNode, drawingNode, 0),
                    attrs: { drawing: { rotation: rotationAngle } }
                };

                extendAndGenerateOp(generator, drawingNode, operationProperties, target);

                // Applying the operations (undo group is created automatically)
                editor.applyOperations(generator);
                generatedOp = true;
            }
        }

        function finalizeRotateDrawing() {
            drawingNode.removeClass(ROTATING_STATE_CLASSNAME + ' ' + FRAME_WITH_TEMP_BORDER2);
            if (drawingNode.data('selection')) { drawingNode.data('selection').removeClass(ROTATING_STATE_CLASSNAME); }
            removeRotateAngleHint(drawingNode);

            if (!generatedOp) { // reset to start angle if tracking was canceled
                updateCssTransform(drawingNode, startAngle);
            } else {
                updateResizersMousePointers(drawingNode, getDrawingRotationAngle(editor, drawingNode));
            }

            if (editor.getSelection().isAdditionalTextframeSelection()) {
                getTextFrameNode(drawingNode, { deep: true }).attr('contenteditable', true);
                editor.getSelection().restoreBrowserSelection();
            }
            leaveTracking();

            // update an existing selection in the selection overlay node (49975)
            updateOverlaySelection(app, drawingNode);

            generatedOp = false;

            // switching from text to drawing selection
            if (isTextSelection) { editor.getSelection().switchAdditionalDrawingToDrawingSelection(); }
        }

        return {
            prepare: prepareRotateDrawing,
            start: startRotateDrawing,
            move: updateRotate,
            scroll: updateRotateScroll,
            end: stopRotateDrawing,
            finally: finalizeRotateDrawing
        };
    }

    /**
     * Creates the callbacks for moving adjustment handle of the shape.
     */
    function createDrawingAdjustCallbacks() {
        var scrollX = 0, scrollY = 0;
        var deltaX, deltaY, normDeltas;
        // the start scroll position
        var startScrollX = 0, startScrollY = 0;
        var adjHandlerNode, adjPos;
        var cxnObject, ahList, avList, gdList;
        var moveX = false, moveY = false, isPolar = false;
        var xDiffpx, yDiffpx, minMaxRdiff;
        var drawingRotation, scaleX, scaleY, snapRect;
        var minXValue, maxXValue, minYValue, maxYValue;
        var minXpos, maxXpos, minYpos, maxYpos, minAng, maxAng;
        var minR, maxR, minRPolar, maxRPolar, minRposPx, maxRposPx;
        var absMinMaxSumX, absMinMaxSumY, absMinMaxSumR;
        var actXpos, actYpos, currentPolar, currentAng, currentRadius;

        function prepareAdjust(event) {
            return isAdjustmentHandle(event.target);
        }

        function startAdjust(record) {
            adjHandlerNode = $(record.target);
            zoomFactor = app.getView().getZoomFactor();
            startScrollX = scrollNode.scrollLeft();
            startScrollY = scrollNode.scrollTop();
            drawingRotation = getDrawingRotationAngle(editor, drawingNode);
            scaleX = isFlippedHorz(drawingNode) ? -1 : 1;
            scaleY = isFlippedVert(drawingNode) ? -1 : 1;
            cxnObject = adjHandlerNode.data('cxnObj');
            avList = cxnObject.avList;
            gdList = cxnObject.gdList;
            ahList = cxnObject.ah;
            isPolar = ahList.type === 'polar';
            moveX = !_.isUndefined(ahList.minX);
            moveY = !_.isUndefined(ahList.minY);
            snapRect = new Rectangle(0, 0, drawingNode.width(), drawingNode.height());
            adjPos = getPointPixelPosition(ahList, snapRect, avList, gdList);

            if (isPolar) {
                currentPolar = snapRect.pointToPolar(adjPos);
                if (!_.isUndefined(ahList.minAng) || !_.isUndefined(ahList.maxAng)) {
                    minAng = getGeometryValue(ahList.minAng, snapRect, avList, gdList) / 60000;
                    maxAng = getGeometryValue(ahList.maxAng, snapRect, avList, gdList) / 60000;
                    currentAng = Math.round(currentPolar.a * 180 / Math.PI);
                    if (currentAng < 0) { currentAng += 360; }
                    currentAng = math.clamp(currentAng, minAng, maxAng);
                }
                if (!_.isUndefined(ahList.minR) || !_.isUndefined(ahList.maxR)) {
                    currentRadius = currentPolar.r;
                    minR = getGeometryValue(ahList.minR, snapRect, avList, gdList);
                    minRposPx = getPxPosFromGeo(minR, snapRect, avList, ahList, gdList, ahList.gdRefR);
                    minRPolar = snapRect.pointToPolar(minRposPx).r;

                    maxR = getGeometryValue(ahList.maxR, snapRect, avList, gdList);
                    maxRposPx = getPxPosFromGeo(maxR, snapRect, avList, ahList, gdList, ahList.gdRefR);
                    maxRPolar = snapRect.pointToPolar(maxRposPx).r;

                    absMinMaxSumR = Math.abs(maxR - minR);
                    minMaxRdiff = Math.abs(maxRPolar - minRPolar);
                }
            }

            if (moveX) {
                minXValue = getGeometryValue(ahList.minX, snapRect, avList, gdList);
                minXpos = getPxPosFromGeo(minXValue, snapRect, avList, ahList, gdList, ahList.gdRefX);

                maxXValue = getGeometryValue(ahList.maxX, snapRect, avList, gdList);
                maxXpos = getPxPosFromGeo(maxXValue, snapRect, avList, ahList, gdList, ahList.gdRefX);

                xDiffpx = Math.abs(maxXpos.x - minXpos.x);
                absMinMaxSumX = Math.abs(maxXValue - minXValue);
            }
            if (moveY) {
                minYValue = getGeometryValue(ahList.minY, snapRect, avList, gdList);
                minYpos = getPxPosFromGeo(minYValue, snapRect, avList, ahList, gdList, ahList.gdRefY);

                maxYValue = getGeometryValue(ahList.maxY, snapRect, avList, gdList);
                maxYpos = getPxPosFromGeo(maxYValue, snapRect, avList, ahList, gdList, ahList.gdRefY);

                yDiffpx = Math.abs(maxYpos.y - minYpos.y);
                absMinMaxSumY = Math.abs(maxYValue - minYValue);
            }
        }

        function updateAdjust(record) {
            var allowX = isPolar || moveX;
            var allowY = isPolar || moveY;
            var modAttrs = { avList: _.copy(avList) };

            deltaX = record.offset.x / zoomFactor + scrollX;
            deltaY = record.offset.y / zoomFactor + scrollY;
            normDeltas = getNormalizedResizeDeltas(deltaX, deltaY, allowX, allowY, scaleX, scaleY, drawingRotation);
            actXpos = normDeltas.x + adjPos.x;
            actYpos = normDeltas.y + adjPos.y;
            if (moveX) {
                actXpos = math.clamp(actXpos, Math.min(maxXpos.x, minXpos.x), Math.max(maxXpos.x, minXpos.x));
                if (_.isFinite(actXpos)) { modAttrs.avList[ahList.gdRefX] = scaleAdjustmentValue(actXpos, minXpos.x, maxXpos.x, minXValue, xDiffpx, absMinMaxSumX); }
            }
            if (moveY) {
                actYpos = math.clamp(actYpos, Math.min(maxYpos.y, minYpos.y), Math.max(maxYpos.y, minYpos.y));
                if (_.isFinite(actYpos)) { modAttrs.avList[ahList.gdRefY] = scaleAdjustmentValue(actYpos, minYpos.y, maxYpos.y, minYValue, yDiffpx, absMinMaxSumY); }

            }
            if (isPolar) {
                currentPolar = snapRect.pointToPolar({ x: actXpos, y: actYpos });
                if (!_.isUndefined(minAng) || !_.isUndefined(maxAng)) {
                    currentAng = Math.round(currentPolar.a * 180 / Math.PI);
                    if (currentAng < 0) { currentAng += 360; }
                    currentAng = math.clamp(currentAng, minAng, maxAng);
                }
                if (!_.isUndefined(minR) || !_.isUndefined(maxR)) {
                    currentRadius = currentPolar.r;
                    currentRadius = math.clamp(currentRadius, Math.min(minRPolar, maxRPolar), Math.max(minRPolar, maxRPolar));
                }
                if (!_.isUndefined(ahList.gdRefAng)) {
                    modAttrs.avList[ahList.gdRefAng] = currentAng * 60000;
                }
                if (!_.isUndefined(ahList.gdRefR)) {
                    modAttrs.avList[ahList.gdRefR] = scaleAdjustmentValue(currentRadius, minRPolar, maxRPolar, minR, minMaxRdiff, absMinMaxSumR, true);
                }
            }

            adjHandlerNode.css({ left: actXpos - 5, top: actYpos - 5 });
        }

        function updateAdjustScroll(record) {
            scrollNode
                .scrollLeft(scrollNode.scrollLeft() + record.scroll.x)
                .scrollTop(scrollNode.scrollTop() + record.scroll.y);

            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;
        }

        function stopAdjust() {
            var generator = editor.createOperationGenerator();
            var operationProperties = { attrs: { geometry: { avList: _.copy(avList) } }, start: getOxoPosition(rootNode, drawingNode, 0) };
            var opCreated = false;
            var scaledValue = null;

            if (moveX && _.isFinite(actXpos)) {
                scaledValue = scaleAdjustmentValue(actXpos, minXpos.x, maxXpos.x, minXValue, xDiffpx, absMinMaxSumX);
                if (_.isFinite(scaledValue)) {
                    operationProperties.attrs.geometry.avList[ahList.gdRefX] = scaledValue;
                    opCreated = true;
                }
            }
            if (moveY && _.isFinite(actYpos)) {
                scaledValue = scaleAdjustmentValue(actYpos, minYpos.y, maxYpos.y, minYValue, yDiffpx, absMinMaxSumY);
                if (_.isFinite(scaledValue)) {
                    operationProperties.attrs.geometry.avList[ahList.gdRefY] = scaledValue;
                    opCreated = true;
                }
            }
            if (isPolar) {
                if (!_.isUndefined(ahList.gdRefAng) && _.isFinite(currentAng)) {
                    operationProperties.attrs.geometry.avList[ahList.gdRefAng] = currentAng * 60000;
                    opCreated = true;
                }
                if (!_.isUndefined(ahList.gdRefR)) {
                    scaledValue = scaleAdjustmentValue(currentRadius, minRPolar, maxRPolar, minR, minMaxRdiff, absMinMaxSumR, true);
                    if (_.isFinite(scaledValue)) {
                        operationProperties.attrs.geometry.avList[ahList.gdRefR] = scaledValue;
                        opCreated = true;
                    }
                }
            }
            if (opCreated) {
                extendAndGenerateOp(generator, drawingNode, operationProperties, target);

                editor.applyOperations(generator);
                if (drawingNode && drawingNode.length) {
                    clearSelection(drawingNode);
                    editor.getSelection().drawDrawingSelection(drawingNode);
                }
            }
        }

        function finalizeAdjust() {
            leaveTracking();

            // Resetting variables for new mouseup events without mousemove
            scrollX = scrollY = 0;

            // switching from text to drawing selection
            if (isTextSelection) { editor.getSelection().switchAdditionalDrawingToDrawingSelection(); }
        }

        return {
            prepare: prepareAdjust,
            start: startAdjust,
            move: updateAdjust,
            scroll: updateAdjustScroll,
            end() { stopAdjust(); finalizeAdjust(); },
            cancel() { finalizeAdjust(); forceRepaintDrawingSelection(); }
        };
    }

    // shifting the selection into an overlay node
    function shiftSelectionToOverlayNode() {

        var // the overlay node that contains the selection drawings
            overlayNode = editor.getNode().children('.drawingselection-overlay'),
            // the empty drawing node that contains the selection
            selDrawing =  $('<div>').addClass('drawing selectiondrawing').css({ position: 'absolute' }),
            // a helper string for class names
            classesString = drawingNode.get(0).className;

        if (classesString) { selDrawing.addClass(classesString); }  // transferring drawing class names to the selection drawing node
        drawingNode.data('selection', selDrawing); // saving the drawing selection node at the drawing
        selectionBox.css('position', 'static');
        updateOverlaySelection(app, drawingNode);
        selDrawing.append(selectionBox); // shifting the selection to the new empty drawing node
        overlayNode.append(selDrawing);

        return selDrawing;
    }

    // starting code of static method drawDrawingSelection()

    // on small devices the keyboard should be closed
    if (SOFTKEYBORD_TOGGLE_MODE) { editor.trigger('selection:possibleKeyboardClose'); }

    drawingNode = $(drawingNode);
    if (drawingNode.length !== 1) {
        globalLogger.error('drawDrawingSelection(): single drawing node expected');
        drawingNode = drawingNode.first();
    }

    if (drawingNode.hasClass('horizontal-line')) {
        options.movable = false;
    } else if (drawingNode.hasClass('grouped')) {
        // options.movable = options.resizable = false;
        drawingNode = drawingNode.closest(GROUPCONTENT_SELECTOR).parent();
    }

    if (!app.isEditable() || (!editor.isCommentFunctionality() && editor.isTextframeOrInsideTextframe(drawingNode))) {
        options.movable = options.resizable = options.rotatable = options.adjustable = false;
        if (app.isEditable() && getDrawingType(drawingNode) === 'image') { options.resizable = options.rotatable = true; } // DOCS-3271: At least images are resizable and rotatable
    }

    if (app.isODF() && isFullOdfTextframeNode(drawingNode)) { // default text frames are not rotatable
        options.rotatable = false;
    }

    if (editor.isDraftMode()) { options.movable = false; }

    var currentZoomFactor       = app.getView().getZoomFactor();
    options.zoomValue           = 100 * currentZoomFactor;
    options.scaleHandles        = 1 / currentZoomFactor;
    options.rotation            = getDrawingRotationAngle(editor, drawingNode) || 0;
    options.isMultiSelection    = false; // TODO: change when multiselection is enabled

    var isCropMode = editor.getSelection().isCropMode();
    if (isCropMode) { editor.exitCropMode(); }

    // removing an existing selection (possible for example in additional text frame selections)
    if (drawingNode.data('selection')) { clearSelection(drawingNode); }

    // the container element used to visualize the selection
    selectionBox = drawSelection(drawingNode, options);

    // shifting the selection into an overlay node (not inline drawings, not in draft-mode and not on small devices)
    if (!isInlineDrawingNode(drawingNode) && !editor.isDraftMode() && !SMALL_DEVICE) { selectionDrawing = shiftSelectionToOverlayNode(); }

    if (isCropMode) { editor.handleDrawingCrop(true); }

    // set visible drawing anchor
    app.getView().setVisibleDrawingAnchor(drawingNode, getExplicitAttributeSet(drawingNode));

    // initialize move tracking
    moveBox = selectionBox.children('.tracker');

    if (!drawingNode.data("dispatcher") && (options.movable || options.resizeable || options.rotatable || options.adjustable)) {

        // create a tracking dispatcher for the selection elements
        const dispatcher = new TrackingDispatcher({
            before() {
                // setting a tracking marker at the selection
                editor.getSelection().setActiveTracking(true);
                // blocking external operations, to avoid conflicts with operations inside the drawing like insertText
                editor.setDrawingChangeOTHandling(true);
            },
            after() {
                // remove tracking marker at the selection
                editor.getSelection().setActiveTracking(false);
                // no longer blocking external operations
                editor.setDrawingChangeOTHandling(false);
                // always remove the tracking overlay box
                toggleTracking(drawingNode, false);
                if (selectionDrawing) { toggleTracking(selectionDrawing, false); }
            }
        });

        // resize drawing object
        if (options.resizable) {
            dispatcher.register("crop-resize", createCropResizeCallbacks(app, drawingNode));
            dispatcher.register("drawing-resize", createDrawingResizeCallbacks());
        }

        // rotate drawing object
        if (options.rotatable) {
            dispatcher.register("drawing-rotate", createDrawingRotateCallbacks());
        }

        // adjustment handles of shapes
        if (options.adjustable && !options.isMultiSelection) {
            dispatcher.register("shape-adjust", createDrawingAdjustCallbacks());
        }

        // move drawing object (fallback if no other receiver matches)
        if (options.movable) {
            dispatcher.register("crop-move", createCropMoveCallbacks(app, drawingNode));
            dispatcher.register("drawing-move", createDrawingMoveCallbacks());
        }

        // standard configuration options for all oberved elements
        const TRACKING_CONFIG = {
            stopPropagation: true,
            trackModifiers: true,
            scrollDirection: "all",
            scrollBoundary: app.docView.$contentRootNode[0],
            scrollMinSpeed: 10,
            scrollMaxSpeed: 250,
            scrollAccelBand: [-30, 30]
        };

        // observe the drawing node, and its overlay clone
        dispatcher.observe(drawingNode[0], TRACKING_CONFIG);
        if (selectionDrawing) { dispatcher.observe(selectionDrawing[0], TRACKING_CONFIG); }

        // this hack is needed to be able to access the dispatcher in external generic code
        // ("clearSelection" in drawinglayer/view/drawingframe.js)
        drawingNode.data("dispatcher", dispatcher);
    }
}
