/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { EditMVCFactory } from "@/io.ox/office/editframework/app/mvcfactory";

import { TextResourceManager } from "@/io.ox/office/text/resource/resourcemanager";
import TextOTManager from "@/io.ox/office/text/components/ot/otmanager";
import TextModel from "@/io.ox/office/text/model/docmodel";
import TextView from "@/io.ox/office/text/view/view";
import TextController from "@/io.ox/office/text/app/controller";
import type TextApp from "@/io.ox/office/text/app/application";

// class TextMVCFactory =======================================================

/**
 * The MVC factory for text application instances.
 */
class TextMVCFactory extends EditMVCFactory<TextApp> {

    public constructor() {
        super();
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates and returns a new resource manager for a text document.
     */
    createResourceManager(docApp: TextApp): TextResourceManager {
        return new TextResourceManager(docApp);
    }

    /**
     * Creates and returns a new text document model.
     */
    createModel(docApp: TextApp): TextModel {
        return new TextModel(docApp);
    }

    /**
     * Creates and returns a new text document view.
     */
    createView(docApp: TextApp, docModel: TextModel): TextView {
        return new TextView(docApp, docModel);
    }

    /**
     * Creates and returns a new text document controller.
     */
    createController(docApp: TextApp, docModel: TextModel, docView: TextView): TextController {
        return new TextController(docApp, docModel, docView);
    }

    /**
     * Creates and returns a new OT manager for a text document.
     */
    createOTManager(docModel: TextModel): TextOTManager {
        return new TextOTManager(docModel);
    }
}

// exports ====================================================================

export default new TextMVCFactory();
