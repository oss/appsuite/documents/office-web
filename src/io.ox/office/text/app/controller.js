/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { fun } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, TOUCH_DEVICE } from '@/io.ox/office/tk/dom';
import { getBooleanOption, getStringOption } from '@/io.ox/office/tk/utils';

import { Color } from '@/io.ox/office/editframework/utils/color';
import { EditController } from '@/io.ox/office/editframework/app/editcontroller';

import { hasSupportedProtocol, isValidURL } from '@/io.ox/office/editframework/utils/hyperlinkutils';
import { getBorderAttributes, getBorderFlags } from '@/io.ox/office/editframework/utils/mixedborder';

import { getPresetBorder } from '@/io.ox/office/drawinglayer/utils/drawingutils';
import { getDrawingTypeLabel, isPresetConnectorId } from '@/io.ox/office/drawinglayer/view/drawinglabels';
import { isFullOdfTextframeNode, isUnsupportedDrawing } from '@/io.ox/office/drawinglayer/view/drawingframe';

import { MAX_TABLE_CELLS, SPELLING_ENABLED } from '@/io.ox/office/textframework/utils/config';
import { DEFAULT_LIST_STYLE_ID } from '@/io.ox/office/textframework/utils/listutils';
import { getOxoPosition, getWordSelection, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import TableStyles from '@/io.ox/office/textframework/format/tablestyles';

import ParagraphStyles from '@/io.ox/office/text/format/paragraphstyles';
import DrawingStyles from '@/io.ox/office/text/format/drawingstyles';
import { ImageCropDialog, ParagraphDialog } from '@/io.ox/office/text/view/dialogs';

// class TextController ===================================================

/**
 * The controller of a OX Text application.
 */
class TextController extends EditController {

    constructor(app, docModel, docView) {

        // base constructor
        super(app, docModel, docView);

        // self reference
        var self = this;

        // protected methods --------------------------------------------------

        /**
         * Executes a "Search" command.
         */
        this.executeSearch = function (command, query, config) {
            return docModel.getSearchHandler().executeSearch(command, query, config);
        };

        /**
         * Executes a "Replace" command.
         */
        this.executeReplace = function (command, query, replace, config) {
            return docModel.getSearchHandler().executeReplace(command, query, replace, config);
        };

        // private methods ----------------------------------------------------

        /**
         * Item getter function for a list style identifier. Will be called in
         * the context of a controller item instance.
         *
         * @param {String} listType
         *  The type of the list styles: either 'bullet' or 'numbering'.
         *
         * @param {Object} attributes
         *  The merged attribute set of the selected paragraphs.
         *
         * @returns {String|Null}
         *  If unambiguous, the list style identifier contained in the passed
         *  paragraph attributes, if the type of that list matches the list
         *  type specified in the item user data ('bullets' or 'numbering'),
         *  otherwise an empty string. If ambiguous, returns the value null.
         */
        function getListStyleId(listType, attributes) {

            var listStyleId = attributes.listStyleId,
                listLevel = attributes.listLevel,
                isListType = false,
                // the list style ID with added target (only ODT)
                shortListStyleId = listStyleId, // 44393, special behavior for list in header/footer in ODT
                // the target of the header or footer
                target = null,
                // the sub string position inside the list style ID
                index = 0;

            // special check for lists in ODT header or footer
            if (listStyleId && app.isODF() && docModel.isHeaderFooterEditState()) {
                target = docModel.getActiveTarget();
                if (target) {
                    target = '_' + target;
                    index = listStyleId.indexOf(target);
                    if (index > 0) {
                        shortListStyleId = listStyleId.substring(0, index);
                    }
                }
            }

            // ambiguous attributes
            if (_.isNull(listStyleId) || _.isNull(listLevel)) { return null; }

            if (listLevel > -1) {
                switch (listType) {
                    case 'bullet':
                        isListType = docModel.getListCollection().isBulletsList(listStyleId, listLevel);
                        break;
                    case 'numbering':
                        isListType = docModel.getListCollection().isNumberingList(listStyleId, listLevel);
                        break;
                }

                // check, if the ID belongs to a predefined list style (so that a nice selection can be done, if this is no predefined list style (58368))
                if (isListType && !docModel.getListCollection().getPredefinedListStyles()[listStyleId]) {
                    // -> if this is not a predefined list style, it is required to find that list style, that matches the list item at the corresponding level
                    var predefinedListStyles = (listType === 'bullet') ? docModel.getPredefinedBulletListStyles() : docModel.getPredefinedNumberedListStyles();
                    var compareLevel = docModel.getListCollection().getListLevel(listStyleId, listLevel);
                    var key = 'listLevel' + listLevel; // searching that list style, that has the same list definition at the specified level.

                    _.find(predefinedListStyles, function (oneListStyle, id) {
                        var searchLevel = oneListStyle.definition[key];
                        var found = (searchLevel?.numberFormat === compareLevel.numberFormat) && (searchLevel?.levelText === compareLevel.levelText);
                        if (found) { shortListStyleId = id; } // highlighting the best list icon
                        return found;
                    });
                }
            }

            // return empty string, if list type in attributes does not match list type in item user data
            return isListType ? shortListStyleId : '';
        }

        /**
         * Item setter function for a list style identifier. Will be called in
         * the context of a controller item instance.
         *
         * @param {String} listType
         *  The type of the list styles: either 'bullet' or 'numbering'.
         *
         * @param {String} listStyleId
         *  The new list style identifier. The value of the special constant
         *  DEFAULT_LIST_STYLE_ID can be used to toggle the default
         *  list style.
         */
        function setListStyleId(listType, listStyleId) {

            // simulate toggle behavior for default list style
            if (listStyleId === DEFAULT_LIST_STYLE_ID) {
                if (this.value === '') {
                    docModel.createDefaultList(listType);
                } else {
                    docModel.removeListAttributes();
                }
            } else {
                // list level may be null, will fall back to level 0 then...
                docModel.createSelectedListStyle(listStyleId, self.getItemValue('paragraph/list/indent'));
            }
        }

        // item registration --------------------------------------------------

        // register item definitions
        this.registerItems({

            // view -------------------------------------------------------

            'view/rulerpane/show': {
                parent: 'document/editable',
                get() { return !!docView.rulerPane?.isVisible(); },
                set(state) {
                    app.setUserSettingsValue('showRulerPane', state); // save in background
                    docView.rulerPane?.toggle(state);
                }
            },

            'view/toolpane/format/visible': {
                parent: ['document/editable/text', '!view/combinedpanes']
            },

            'view/toolpane/font/visible': {
                parent: ['document/editable/text', 'view/combinedpanes']
            },

            'view/toolpane/paragraph/visible': {
                parent: ['document/editable/text', 'view/combinedpanes']
            },

            'view/toolpane/insert/visible': {
                parent: 'document/editable/text'
            },

            'view/toolpane/table/visible': {
                parent: 'document/editable/table'
            },

            'view/toolpane/drawing/visible': {
                parent: 'document/editable/anydrawing'
            },

            'view/toolpane/review/visible': {
                parent: ['app/valid', '!view/combinedpanes']
            },

            'view/toolpane/spelling/visible': {
                parent: ['document/editable', 'view/combinedpanes']
            },

            'view/toolpane/changetrack/visible': {
                parent: ['app/valid', 'document/ooxml', 'view/combinedpanes']
            },

            'view/toolpane/comments/visible': {
                parent: ['app/valid', 'view/combinedpanes']
            },

            'document/text': {
                parent: 'app/valid',
                enable() { return docModel.isTextSelected() || docModel.getSelection().isAnyTextFrameDrawingSelection(); }
            },

            'document/editable/text': {
                parent: 'document/editable',
                enable() { return docModel.isTextSelected() || docModel.getSelection().isAnyTextFrameDrawingSelection(); }
            },

            'document/editable/text/notextframe': {
                parent: 'document/editable/text',
                enable() {
                    return !docModel.getSelection().isAdditionalTextframeSelection();
                }
            },

            'document/editable/text/nodrawing': {
                parent: 'document/editable/text',
                enable() {
                    return !docModel.getSelection().isAnyDrawingSelection();
                }
            },

            'document/selectall': {
                // enabled in read-only mode
                set() { docModel.selectAll(); },
                // restrict keyboard shortcut to application pane
                shortcut: { keyCode: 'A', ctrlOrMeta: true, scope: '.app-pane' },
                preserveFocus: true // do not return focus to document
            },

            'document/selectDocStartPos': {
                // enabled in read-only mode
                set() { docModel.getSelection().setTextSelection(docModel.getSelection().getFirstDocumentPosition()); },
                // restrict keyboard shortcut to application pane
                shortcut: { keyCode: 'HOME', ctrlOrMeta: true, scope: '.app-pane' },
                preserveFocus: true // do not return focus to document
            },

            'document/selectDocStartPosWithShift': {
                // enabled in read-only mode
                set() { docModel.getSelection().setTextSelection(docModel.getSelection().getFirstDocumentPosition(), docModel.getSelection().getEndPosition()); },
                // restrict keyboard shortcut to application pane
                shortcut: { keyCode: 'HOME', ctrlOrMeta: true, shift: true, scope: '.app-pane' },
                preserveFocus: true // do not return focus to document
            },

            'document/selectDocEndPos': {
                // enabled in read-only mode
                set() { docModel.getSelection().setTextSelection(docModel.getSelection().getLastDocumentPosition()); },
                // restrict keyboard shortcut to application pane
                shortcut: { keyCode: 'END', ctrlOrMeta: true, scope: '.app-pane' },
                preserveFocus: true // do not return focus to document
            },

            'document/selectDocEndPosWithShift': {
                // enabled in read-only mode
                set() { docModel.getSelection().setTextSelection(docModel.getSelection().getStartPosition(), docModel.getSelection().getLastDocumentPosition()); },
                // restrict keyboard shortcut to application pane
                shortcut: { keyCode: 'END', ctrlOrMeta: true, shift: true, scope: '.app-pane' },
                preserveFocus: true // do not return focus to document
            },

            'document/selectDrawing': {
                set(options) {
                    docModel.getSelection().selectNextDrawing({ backwards: getBooleanOption(options, 'backwards', false) });
                },
                shortcut: { keyCode: 'F4', shift: true }
            },
            'document/setCursorIntoTextframe': {
                set() {
                    docModel.getSelection().setSelectionIntoTextframe();
                },
                shortcut: { keyCode: 'F2' }
            },
            'document/noDrawingSelected': {
                parent: 'document/editable',
                enable() { return !docModel.isDrawingSelected(); }
            },

            // document contents
            'document/cut': {
                parent: ['document/copy', 'document/editable'],
                set() { docModel.cut(); }
            },
            'document/copy': {
                // enabled in read-only mode
                enable() { return docModel.hasSelectedRange(); },
                set() { docModel.copy(); }
            },
            'document/paste': {
                parent: 'document/editable',
                enable() {
                    // DOCS-1116: disable on spellerror without selection range
                    return docModel.hasSelectedRange() || !docModel.getSpellChecker().getSpellErrorWord().spellError;
                },
                set() { return docView.showClipboardNoticeDialog(); }
            },
            'document/paste/internal': {
                parent: 'document/editable',
                enable() { return docModel.hasInternalClipboard(); },
                set() { docModel.pasteInternalClipboard(); }
            },

            // spelling

            'document/spelling/available': {
                parent: 'document/editable',
                enable() { return SPELLING_ENABLED; }
            },
            'document/onlinespelling': {
                parent: 'document/spelling/available',
                get() { return docModel.getSpellChecker().isOnlineSpelling(); },
                set(state) { docModel.getSpellChecker().setOnlineSpelling(state); }
            },
            'document/spelling/replace': {
                parent: 'document/editable',
                set(replacement) {
                    var selection = docModel.getSelection(),
                        startPosition = selection.getStartPosition(),
                        wordPos = getWordSelection(selection.getEnclosingParagraph(), startPosition[startPosition.length - 1]);

                    docModel.getSpellChecker().replaceWord(startPosition, wordPos, replacement);
                }
            },
            'document/spelling/ignoreword': {
                parent: 'document/editable',
                set(options) {
                    return docModel.getSpellChecker().addWordToIgnoreSpellcheck(options);
                }
            },
            'document/spelling/userdictionary': {
                parent: 'document/editable',
                set(options) {
                    return docModel.getSpellChecker().addWordToIgnoreSpellcheck(options);
                }
            },
            // Page settings

            'document/pagesettings': {
                parent: 'document/editable',
                set() { return docView.showPageSettingsDialog(); }
            },

            // paragraphs

            'paragraph/textonly/supported': {
                enable() { return !docModel.useSlideMode(); }
            },

            'paragraph/group': {
                parent: 'document/editable/text',
                get() { return docModel.getAttributes('paragraph', { maxIterations: 10 }) || {}; }
            },
            'paragraph/stylesheet': {
                parent: 'paragraph/group',
                get(paragraph) { return paragraph.styleId; },
                set(styleId) {
                    // TODO: hacked code for function "create new paragraph stylesheet"
                    if (styleId === '$cmd$/paragraph/createstylesheet') {
                        return docModel.showInsertNewStyleDialog();
                    }
                    docModel.setAttributes('paragraph', { styleId }, { clear: true });
                }
            },
            'paragraph/createstylesheet': {
                parent: ['paragraph/group', 'character/group'],
                /*
                enable: function (paragraphHolder, characterHolder) {
                    var character = characterHolder.character;
                    var paragraph = paragraphHolder.character;
                    for (var key in character) {
                        if (_.isNull(character[key])) {return false;}
                    }
                    if (_.isEqual(paragraph, character)) {
                        return false;
                    }
                    return true;
                },
                */
                set() { return docModel.showInsertNewStyleDialog(); }
            },
            'paragraph/renamestylesheet': {
                set(styleId) { return docModel.showRenameStyleDialog(styleId); }
            },
            'paragraph/changestylesheet': {
                parent: ['paragraph/group', 'character/group', 'paragraph/textonly/supported'],
                set(styleId) { return docModel.changeStylesheet(styleId); }
            },
            'paragraph/deletestylesheet': {
                parent: ['paragraph/group', 'character/group', 'paragraph/textonly/supported'],
                set(styleId) { return docModel.deleteStylesheet(styleId); }
            },
            'paragraph/attributes': {
                parent: 'paragraph/group',
                get(paragraph) { return paragraph.paragraph || {}; }
            },
            'paragraph/attrs/dialog': {
                parent: ['document/editable', 'paragraph/group'],
                set() { return new ParagraphDialog(docView).show(); }
            },
            'paragraph/alignment': {
                parent: ['paragraph/attributes', 'insert/comment/supported/odf'],
                get(attributes) { return attributes.alignment; },
                set(alignment) { docModel.setAttribute('paragraph', 'alignment', alignment); }
                // removed for the 7.2.0 due to clashes with browser shortcuts
                /*shortcut: [
                    { keyCode: 'L', ctrlOrMeta: true, value: 'left' },
                    { keyCode: 'R', ctrlOrMeta: true, value: 'right' },
                    { keyCode: 'E', ctrlOrMeta: true, value: 'center' },
                    { keyCode: 'J', ctrlOrMeta: true, value: 'justify' }
                ]*/
            },
            'paragraph/spacing/menu': {
                parent: ['paragraph/attributes']
            },
            'paragraph/lineheight': {
                parent: ['paragraph/attributes', 'insert/comment/supported/odf'],
                get(attributes) { return attributes.lineHeight; },
                set(lineHeight) { docModel.setAttribute('paragraph', 'lineHeight', lineHeight); }
                // removed for the 7.2.0 due to clashes with browser shortcuts
                /*shortcut: [
                    { keyCode: '1', ctrlOrMeta: true, value: LineHeight.SINGLE },
                    { keyCode: '5', ctrlOrMeta: true, value: LineHeight.ONE_HALF },
                    { keyCode: '2', ctrlOrMeta: true, value: LineHeight.DOUBLE }
                ]*/
            },
            'paragraph/spacing': {
                parent: ['paragraph/attributes', 'insert/comment/supported/odf'],
                get(attributes) { return docModel.getParagraphSpacing(attributes); },
                set(multiplier) { return docModel.setParagraphSpacing(multiplier); }
            },
            'paragraph/fillcolor': {
                parent: ['paragraph/attributes', 'insert/comment/odf/supported'],
                get(attributes) { return attributes.fillColor; },
                set(color) { docModel.setAttribute('paragraph', 'fillColor', color); }
            },
            'paragraph/borders': {
                parent: 'paragraph/attributes',
                get(attributes) { return getBorderFlags(attributes, { paragraph: true }); },
                set(borderFlags) {
                    var borderAttrs = getBorderAttributes(borderFlags, this.parentValue, ParagraphStyles.SINGLE_BORDER, { paragraph: true, clearNone: true });
                    docModel.setAttributes('paragraph', { paragraph: borderAttrs });
                }
            },

            // toggle default bullet list, or select bullet type
            'paragraph/list/bullet': {
                parent: ['paragraph/attributes', 'insert/listindrawing/odf/supported', 'insert/comment/odf/supported'],
                get: _.partial(getListStyleId, 'bullet'),
                set: _.partial(setListStyleId, 'bullet')
            },
            // toggle default numbered list, or select numbering type
            'paragraph/list/numbered': {
                parent: ['paragraph/attributes', 'insert/listindrawing/odf/supported', 'insert/comment/odf/supported'],
                get: _.partial(getListStyleId, 'numbering'),
                set: _.partial(setListStyleId, 'numbering')
            },

            // parent for controller items only enabled inside lists
            'paragraph/list/enabled': {
                parent: 'paragraph/attributes',
                enable() {
                    var indent = this.value.listLevel;
                    return _.isNumber(indent) && (indent >= 0) && (indent <= 8);
                }
            },
            // change list level
            'paragraph/list/indent': {
                parent: 'paragraph/list/enabled',
                get(attributes) { return attributes.listLevel; },
                set(indent) { docModel.setAttribute('paragraph', 'listLevel', indent); }
            },
            // increase list level by one
            'paragraph/list/incindent': {
                parent: 'paragraph/list/indent',
                enable() { return this.value < 8; },
                set() {
                    var indent = this.value;
                    if (indent < 8) {
                        var opGenerated = docModel.setParagraphStyleForListLevel(indent + 1);
                        if (!opGenerated) { docModel.setAttribute('paragraph', 'listLevel', indent + 1); }
                    }
                }
            },
            // decrease list level by one
            'paragraph/list/decindent': {
                parent: 'paragraph/list/indent',
                enable() { return this.value > 0; },
                set() {
                    var indent = this.value;
                    if (indent > 0) {
                        var opGenerated = docModel.setParagraphStyleForListLevel(indent - 1);
                        if (!opGenerated) { docModel.setAttribute('paragraph', 'listLevel', indent - 1); }
                    }
                }
            },
            // starting the numbering list in a selected paragraph with first value
            'paragraph/list/startListWithOne': {
                parent: 'paragraph/attributes',
                enable(attrs) { return attrs.listStyleId !== '' && docModel.getListCollection().isNumberingList(attrs.listStyleId, attrs.listLevel) && (!docModel.isFirstParagraphInList() || docModel.isIncreasedStartValueInList(attrs)); },
                set() { docModel.setListStartValue({ continue: false }); }
            },
            // continuing the numbering list in a selected paragraph after a previous numbering list
            'paragraph/list/continueList': {
                parent: 'paragraph/attributes',
                enable(attrs) { return attrs.listStyleId !== '' && docModel.getListCollection().isNumberingList(attrs.listStyleId, attrs.listLevel) && docModel.isFirstParagraphInList(); },
                set() { docModel.setListStartValue({ continue: true }); }
            },

            // characters

            'character/group': {
                parent: 'document/editable/text',
                get() { return docModel.getAttributes('character', { maxIterations: 10 }) || {}; }
            },
            'character/text/group': {
                parent: 'document/text',
                get() { return docModel.getAttributes('character', { maxIterations: 10 }) || {}; }
            },
            'character/stylesheet': {
                parent: 'character/group',
                get(character) { return character.styleId; },
                set(styleId) { docModel.setAttributes('character', { styleId }, { clear: true }); }
            },
            'character/attributes': {
                parent: 'character/group',
                get(character) { return character.character || {}; }
            },
            'character/text/attributes': {
                parent: 'character/text/group',
                get(character) { return character.character || {}; }
            },
            'character/fontname': {
                parent: 'character/attributes',
                get(attributes) { return attributes.fontName; },
                set(fontName) { docModel.setAttribute('character', 'fontName', fontName); }
            },
            'character/fontsize': {
                parent: ['character/attributes', 'insert/comment/supported/odf'],
                get(attributes) { return attributes.fontSize; },
                set(fontSize) { docModel.setAttribute('character', 'fontSize', fontSize); }
            },
            'character/bold': {
                parent: 'character/attributes',
                get(attributes) { return attributes.bold; },
                set(state) { docModel.setAttribute('character', 'bold', state); },
                shortcut: { keyCode: 'B', ctrlOrMeta: true, value: fun.not, scope: '.app-pane' }
            },
            'character/italic': {
                parent: 'character/attributes',
                get(attributes) { return attributes.italic; },
                set(state) { docModel.setAttribute('character', 'italic', state); },
                shortcut: { keyCode: 'I', ctrlOrMeta: true, value: fun.not, scope: '.app-pane' }
            },
            'character/underline': {
                parent: 'character/attributes',
                get(attributes) { return attributes.underline; },
                set(state) { docModel.setAttribute('character', 'underline', state); },
                shortcut: { keyCode: 'U', ctrlOrMeta: true, value: fun.not, scope: '.app-pane' }
            },
            'character/strike': {
                parent: 'character/attributes',
                get(attributes) { return _.isString(attributes.strike) ? (attributes.strike !== 'none') : null; },
                set(state) { docModel.setAttribute('character', 'strike', state ? 'single' : 'none'); }
            },
            'character/vertalign': {
                parent: 'character/attributes',
                get(attributes) { return attributes.vertAlign; },
                set(align) { docModel.setAttribute('character', 'vertAlign', align); }
            },
            'character/color': {
                parent: ['character/attributes', 'insert/comment/odf/supported'],
                get(attributes) { return attributes.color; },
                set(color) { docModel.setAttribute('character', 'color', color); }
            },
            'character/fillcolor': {
                parent: ['character/attributes', 'insert/comment/odf/supported'],
                get(attributes) { return attributes.fillColor; },
                set(color) { docModel.setAttribute('character', 'fillColor', color); }
            },
            'character/language': {
                parent: 'character/attributes',
                get(attributes) { return attributes.language; },
                set(language) { docModel.setAttribute('character', 'language', language); }
            },
            'character/hyperlink': {
                parent: 'character/attributes',
                enable() { return docModel.getSelection().isSelectionInsideOneParagraph(); },
                get(attributes) { return attributes.url; }
                // no direct setter (see item 'character/hyperlink/dialog')
            },
            'character/hyperlink/dialog': {
                parent: 'character/hyperlink',
                set() { return docModel.insertHyperlinkDialog(); }
                // removed for the 7.2.0 due to clashes with browser shortcuts
                //shortcut: { keyCode: 'K', ctrlOrMeta: true }
            },
            'character/hyperlink/remove': {
                parent: 'character/hyperlink',
                set() { return docModel.removeHyperlink(); }
            },

            'character/hyperlink/valid': {
                parent: 'character/text/attributes',  // not using 'character/attributes' -> must also work in read-only mode
                enable(attrs) { return !!attrs.url && hasSupportedProtocol(attrs.url) && isValidURL(attrs.url); }
            },

            'character/reset': {
                parent: 'document/editable/text',
                set() { docModel.resetAttributes(); },
                shortcut: { keyCode: 'SPACE', ctrl: true, scope: '.app-pane' }
            },

            'character/insert/break': {
                parent: 'document/editable/text',
                set() { return docModel.insertHardBreak(); }
            },
            'character/insert/tab': {
                parent: 'document/editable/text',
                set() { return docModel.insertTab(); }
            },
            'character/insert/pagebreak': {
                parent: ['document/editable/text/nodrawing', 'insert/comment/supported'],
                enable() { return !docModel.isHeaderFooterEditState(); },
                set() { return docModel.insertManualPageBreak(); }
            },

            // header-footer
            'document/insert/headerfooter': {
                parent: ['document/editable', 'insert/comment/supported'],
                get() { return docModel.getPageLayout().getHeaderFooterTypeInDoc(); },
                set(value) { docModel.getPageLayout().setHeaderFooterTypeInDoc(value); }
            },

            // header-footer remove all
            'document/insert/headerfooter/remove': {
                parent: ['document/editable', 'insert/comment/supported'],
                enable() { return docModel.getPageLayout().isHeaderFooterActivated(); },
                set() { docModel.getPageLayout().setHeaderFooterTypeInDoc('none'); }
            },

            // reduced odf text frame functionality
            'insert/textframe/supported': {
                enable() { return !docModel.isReducedOdfTextframeFunctionality(); }
            },

            // reduced odf image insertion text frame functionality
            'insert/imageintextframe/supported': {
                enable() { return !app.isODF() || !docModel.getSelection().isAdditionalTextframeSelection(); }
            },

            // reduced odf comment functionality
            'insert/comment/odf/supported': {
                enable() { return !docModel.isOdfCommentFunctionality(); }
            },

            'insert/comment/notactive': {
                enable() { return !docModel.getCommentLayer().isCommentEditorActive(); }
            },

            'insert/comment/supported': {
                enable() { return !docModel.isCommentFunctionality(); }
            },

            'insert/comment/supported/odf': {
                enable() { return app.isODF() || !docModel.isCommentFunctionality(); }
            },

            'insert/listindrawing/odf/supported': {
                enable() { return docModel.checkTextListAvailability(); }
            },

            'insert/margin/supported': {
                enable() { return !docModel.isHeaderFooterEditState(); }
            },

            'insert/margin/supported/odf': {
                enable() { return app.isODF() || !docModel.isHeaderFooterEditState(); }
            },

            'insert/shape/supported': {
                enable() { return docModel.isInsertShapePosition(); }
            },

            // tables

            'table/group': {
                parent: 'document/editable/text',
                get() { return docModel.getAttributes('table') || {}; }
            },
            'table/insert/available/drawing': {
                enable() { return !docModel.getSelection().isDrawingSelection(); } // enable or disable, but not changing visibility (66666)
            },
            'table/insert/available': {
                enable() { return MAX_TABLE_CELLS > 0; }
            },
            'table/insert': {
                parent: ['document/editable/text', 'table/insert/available', 'table/insert/available/drawing', 'insert/textframe/supported', 'insert/comment/odf/supported'],
                set(size) { docModel.insertTable(size); },
                preserveFocus: true // do not return focus to document
            },

            'document/editable/table': {
                parent: 'table/group',
                enable() { return docModel.isPositionInTable(); }
            },
            'table/delete': {
                parent: 'document/editable/table',
                set() { return docModel.deleteTable(); }
            },
            'table/insert/row': {
                parent: 'document/editable/table',
                set() { docModel.insertRow(); }
            },
            'table/insert/column': {
                parent: 'document/editable/table',
                set() { docModel.insertColumn(); }
            },
            'table/delete/row': {
                parent: 'document/editable/table',
                set() { return docModel.deleteRows(); }
            },
            'table/delete/column': {
                parent: 'document/editable/table',
                set() { return docModel.deleteColumns(); }
            },

            'table/stylesheet': {
                parent: 'document/editable/table',
                get(table) { return table.styleId; },
                set(styleId) { docModel.setAttributes('table', { styleId }, { clear: true }); }
            },
            'table/attributes': {
                parent: 'document/editable/table',
                get(table) { return table.table || {}; }
            },
            'table/cellattributes': {
                parent: 'document/editable/table',
                get() {
                    return docModel.getAttributes('cell').cell || {};
                }
            },
            'table/cellborder': {
                parent: 'table/attributes',
                get(attributes) { return getBorderFlags(attributes); },
                set(borderFlags) {
                    var attrSet = this.parentValue;
                    var borderAttrs = getBorderAttributes(borderFlags, attrSet, docModel.getDefaultBorderStyle(attrSet, ParagraphStyles.SINGLE_BORDER), { fullBorderSet: true });
                    docModel.setAttributes('table', { table: borderAttrs }, { cellSpecificTableAttribute: true });
                }
            },
            'table/borderwidth': {
                parent: 'table/cellattributes',
                get(attributes) {
                    return TableStyles.getBorderStyleFromAttributes(attributes);
                },
                set(borderWidth) {
                    docModel.setAttributes('table', {
                        table: TableStyles.getAttributesFromBorderStyle(borderWidth, docModel.getAttributes('table').table)
                    }, { onlyVisibleBorders: true });
                }
            },
            'table/fillcolor': {
                parent: 'table/cellattributes',
                get(attributes) { return attributes.fillColor; },
                set(color) { docModel.setAttribute('cell', 'fillColor', color); }
            },
            'table/split': {
                parent: 'document/editable/table',
                set() { docModel.splitTable(); }
            },
            'table/alignment/menu': {
                parent: 'table/cellattributes',
                get: _.noop // do not store formatting attributes in "data-state" attribute of controls
            },
            'table/verticalalignment': {
                parent: ['table/cellattributes'],
                get(attributes) { return attributes.alignVert === 'center' ? 'centered' : attributes.alignVert; },
                set(verticalalignment) { docModel.setAttribute('cell', 'alignVert', verticalalignment === 'centered' ? 'center' : verticalalignment); }
            },

            // drawing

            'document/editable/drawing': {
                parent: 'document/editable',
                enable() { return docModel.isDrawingSelected(); }
            },
            'document/editable/anydrawing': {
                parent: 'document/editable',
                enable() { return docModel.getSelection().isAnyDrawingSelection(); }
            },
            'document/editable/onlysupporteddrawings': {
                parent: 'document/editable',
                enable() { return docModel.getSelection().areOnlySupportedDrawingsSelected(); }
            },
            'drawing/shapes/support/fill': {
                parent: 'document/editable/onlysupporteddrawings',
                enable() {
                    var allSelectedTypes = docModel.getSelection().getSelectedDrawingsTypes();
                    return allSelectedTypes.has('shape') || allSelectedTypes.has('group');
                }
            },
            'drawing/shapes/support/line': {
                parent: 'document/editable/onlysupporteddrawings',
                enable() {
                    var allSelectedTypes = docModel.getSelection().getSelectedDrawingsTypes();
                    return allSelectedTypes.has('shape') || allSelectedTypes.has('group') || allSelectedTypes.has('image') || allSelectedTypes.has('connector');
                }
            },
            'drawing/shapes/support/lineendings': {
                parent: ['drawing/attributes/full', 'document/editable/onlysupporteddrawings'],
                enable(attrs) {
                    return (attrs && attrs.geometry) ? isPresetConnectorId(attrs.geometry.presetShape) : false;
                }
            },

            'drawing/attributes/full': {
                parent: 'document/editable/anydrawing',
                get() { return docModel.getAttributes('drawing') || {}; }
            },

            'drawing/attributes': {
                parent: 'drawing/attributes/full',
                get(attributes) { return attributes.drawing || {}; }
            },
            'drawing/group/attributes': {
                parent: 'document/editable/anydrawing',
                get() { return docModel.getAttributes('drawing', { groupOnly: true }).drawing || {}; }
            },

            'drawing/border/style/support': {
                parent: 'drawing/attributes/full',
                enable(attributes) { return !((attributes && attributes.geometry) ? isPresetConnectorId(attributes.geometry.presetShape) : false); }
            },

            'drawing/line/style/support': {
                parent: 'drawing/attributes/full',
                enable(attributes) { return (attributes && attributes.geometry) ? isPresetConnectorId(attributes.geometry.presetShape) : false; }
            },

            'drawing/delete': {
                parent: 'document/editable/anydrawing',
                set() {
                    var selection = docModel.getSelection();
                    if (docModel.isDrawingSelected()) {
                        return docModel.deleteSelected();

                    } else if (selection.isAdditionalTextframeSelection()) {
                        var start = getOxoPosition(docModel.getCurrentRootNode(), selection.getSelectedTextFrameDrawing(), 0),
                            end = increaseLastIndex(start);

                        selection.setTextSelection(start, end);
                        return docModel.deleteSelected();
                    }
                }
            },

            'drawing/nowrap': {
                parent: ['drawing/attributes/full', 'document/editable/drawing'],
                enable(attributes) { return !DrawingStyles.isNoWrapAttributeSet(attributes.drawing); },
                set() { docModel.setAttributes('drawing', { drawing: DrawingStyles.getAttributesFromPosition('none') }); }
            },

            'drawing/anchorTo': {
                parent: 'drawing/group/attributes',
                enable() {
                    var selection = docModel.getSelection();
                    return !selection.isDrawingInTextFrameSelection();

                    // TODO: Disabled, OpenOffice moves the selected Drawing out of its TextFrame!
                    // return !selection.isDrawingInTextFrameSelection() || selection.isDrawingInODFTextFrameSelection();
                },
                get(attributes) { return (attributes.inline) ? 'inline' : attributes.anchorVertBase; },
                set(value) { docModel.anchorDrawingTo(value); }
            },
            'drawing/position': {
                parent: 'drawing/group/attributes',
                enable(attributes) { return (!attributes.inline); },
                get(attributes) { return DrawingStyles.getPositionFromAttributes(attributes); },
                set(position) {
                    docModel.setAttributes('drawing', {
                        drawing: DrawingStyles.getAttributesFromPosition(position, docModel.getAttributes('drawing').drawing)
                    }, {
                        ctSupportsDrawingPosition: docModel.getChangeTrack().ctSupportsDrawingPosition()
                    });
                }
            },
            'drawing/verticalalignment': {
                enable() { return docModel.getSelection().isAnyTextFrameSelection({ allDrawingTypesAllowed: true }); },
                get() { return docModel.getVerticalAlignmentMode(); },
                set(mode) { docModel.setVerticalAlignmentMode(mode); }
            },

            // parent item for drawing objects that can be flipped and rotated
            'drawing/operation/transform': {
                parent: 'drawing/position'
            },

            // (stateless command) flips the selected drawing objects horizontally
            'drawing/transform/fliph': {
                parent: 'drawing/operation/transform',
                set() { docModel.transformDrawings(0, true, false); }
            },

            // (stateless command) flips the selected drawing objects vertically
            'drawing/transform/flipv': {
                parent: 'drawing/operation/transform',
                set() { docModel.transformDrawings(0, false, true); }
            },

            // (stateless command) rotates the selected drawing objects by the passed angle
            'drawing/transform/rotate': {
                parent: 'drawing/operation/transform',
                set(angle) { docModel.transformDrawings(angle, false, false); }
            },

            // a single image drawing is selected
            'drawing/cropselection/visible': {
                parent: 'document/editable/drawing',
                enable() {
                    var selection = docModel.getSelection();
                    var isSupportedDrawing = !isUnsupportedDrawing(selection.getSelectedDrawing());
                    var isForbidenDrawingInDrawing = !docModel.isCommentFunctionality() && docModel.isTextframeOrInsideTextframe(selection.getSelectedDrawing());
                    return isSupportedDrawing && !isForbidenDrawingInDrawing && selection.isCropDrawingSelection() && selection.isOnlyImageSelection();
                }
            },

            'drawing/cropselection': {
                parent: 'drawing/cropselection/visible',
                enable() {
                    return !docModel.getSelection().isMultiSelection(); // in multi image selections the button is visible, but disabled
                }
            },

            // drawing z-order

            'drawing/order': {
                parent: 'drawing/position',
                set(orderType) { docModel.getDrawingLayer().updateDrawingOrder(orderType); }
            },

            'drawing/type/shape': {
                parent: 'document/editable',
                enable() {
                    return (docModel.getSelection().getClosestSelectedDrawingType() === 'shape' || docModel.getSelection().getClosestSelectedDrawingType() === 'connector');
                }
            },

            'drawing/type/label': {
                parent: 'document/editable/drawing',
                get() { return getDrawingTypeLabel(docModel.getSelection().getClosestSelectedDrawingType()); }
            },

            'drawing/textframeautofit': {
                enable() { return docModel.getSelection().isAnyTextFrameSelection({ allDrawingTypesAllowed: true }); },
                get() { return docModel.isAutoResizableTextFrame(); },
                set(state) { docModel.handleTextFrameAutoFit(state); }
            },

            'drawing/crop': {
                parent: 'drawing/cropselection',
                get() { return docModel.getSelection().isCropMode(); },
                set(state) { docModel.handleDrawingCrop(state); }
            },

            'drawing/cropposition/fill': {
                parent: 'drawing/crop',
                set() { return docModel.cropFillFitSpace('fill'); }
            },

            'drawing/cropposition/fit': {
                parent: 'drawing/crop',
                set() { return docModel.cropFillFitSpace('fit'); }
            },

            'drawing/cropposition/dialog': {
                parent: 'drawing/crop',
                set() { return new ImageCropDialog(docView).show(); }
            },

            // drawing fill formatting

            'drawing/fill/style/support': {
                parent: 'drawing/attributes/full',
                enable(attributes) { return !((attributes && attributes.geometry) ? isPresetConnectorId(attributes.geometry.presetShape) : false); }
            },

            'drawing/fill/attributes': {
                parent: 'drawing/attributes/full',
                get(attributes) { return attributes.fill || {}; }
            },

            'drawing/fill/color': {
                parent: 'drawing/fill/attributes',
                get(attributes) { return attributes.color; },
                set(color) { docModel.setDrawingFillColor(color); }
            },

            // drawing line formatting

            'drawing/line/attributes': {
                parent: 'drawing/attributes/full',
                get(attributes) { return attributes.line || {}; }
            },

            'drawing/line/endings': {
                parent: ['drawing/line/attributes', 'drawing/shapes/support/lineendings'],
                get(lineAttrs) { return lineAttrs.headEndType + ':' + lineAttrs.tailEndType; },
                set(endings) { docModel.setLineEnds(endings); }
            },

            'drawing/border/style': {
                parent: 'drawing/line/attributes',
                get(attributes) { return getPresetBorder(attributes); },
                set(border) { docModel.setDrawingBorder(border); }
            },

            'drawing/border/color': {
                parent: 'drawing/line/attributes',
                get(attributes) { return (attributes.type !== 'none') ? attributes.color : Color.AUTO; },
                set(color) { docModel.setDrawingBorderColor(color); }
            },

            // text frames

            'textframe/insert': {
                parent: ['document/editable/text/notextframe', 'insert/comment/supported'],
                set() { docModel.insertTextFrame(); }
            },

            // AI DOCS-5132

            'oxai/create': {
                parent: 'document/editable',
                set(mode) { docModel.getAIService().openContentGeneratorDialog({ mode }); }
            },

            'oxai/create/selectionrange': {
                parent: 'document/editable',
                enable() { return docModel.getSelection().hasRange(); },
                set(mode) { docModel.getAIService().openContentGeneratorDialog({ mode }); }
            },

            // comments

            'comment/insert': {
                parent: ['document/editable/text/notextframe', 'insert/comment/supported', 'document/noDrawingSelected', 'insert/margin/supported'],
                set() {
                    docView.processUnsavedComment().then(function () { docModel.getCommentCollection().startNewThread(); });
                }
            },

            'comment/displayModeParent': {
                enable() { return !docModel.getCommentLayer().isEmpty(); }
            },

            'comment/displayMode': {
                get() { return docModel.getCommentLayer().getDisplayMode(); },
                set(mode) { docModel.getCommentLayer().switchCommentDisplayMode(mode); }
            },

            'comment/authorFilter': {
                get() { return docModel.getCommentLayer().getListOfUnfilteredAuthors(); },
                set(authorList) { docModel.getCommentLayer().setAuthorFilter(authorList); }
            },

            'comment/nextComment': {
                // enabled in read-only mode
                enable() { return !docModel.getCommentLayer().isEmpty(); },
                set() { docModel.getCommentLayer().selectNextComment({ next: true }); }
            },
            'comment/prevComment': {
                // enabled in read-only mode
                enable() { return !docModel.getCommentLayer().isEmpty(); },
                set() { docModel.getCommentLayer().selectNextComment({ next: false }); }
            },

            'comment/deleteAll': {
                parent: ['document/editable'],
                enable() { return !docModel.getCommentLayer().isEmpty(); },
                set() { docModel.getCommentLayer().deleteComment(null, { all: true }); }
            },

            'threadedcomment/delete/reply': {
                parent: 'document/editable',
                set(commentModel) { docModel.getCommentLayer().deleteComment(commentModel); }
            },

            'view/comments/show': {
                parent: 'document/editable',
                enable() { return !SMALL_DEVICE; },
                get() { return !docModel.getCommentLayer().isHiddenDisplayModeActive(); },
                set() { docView.commentsPane.toggle(); }
            },

            // show image drive, local or url dialog
            'image/insert/dialog': {
                parent: ['document/editable/text', 'insert/imageintextframe/supported', 'insert/comment/odf/supported'],
                set(sourceType) { return docView.insertImageWithDialog(sourceType); }
            },

            'shape/insert': {
                parent: ['document/editable', 'insert/shape/supported'],
                get() { return docModel.isInsertShapeActive(); },
                set(presetShape, inputOptions) {
                    if (getStringOption(inputOptions, 'sourceType') === 'keyboard') {
                        return docModel.insertShapeWithDefaultBox(presetShape);
                    } else if (TOUCH_DEVICE) {
                        return docModel.insertShapeWithDefaultBox(presetShape);
                    } else {
                        return docModel.insertShapePreparation(presetShape);
                    }
                }
            },

            // change tracking

            checkCurrentChangeTracking: {
                parent: 'document/editable',
                enable() { return docModel.getChangeTrack().isOnChangeTrackNode(); }
            },
            toggleChangeTracking: {
                parent: 'document/editable',
                get() { return docModel.getChangeTrack().isActiveChangeTrackingInDocAttributes(); },
                set(state) { docModel.getChangeTrack().setActiveChangeTracking(state); }
            },
            acceptChangeTracking: {
                parent: 'document/editable',
                set() {
                    docModel.getChangeTrack().resolveChangeTracking(null, { accepted: true, all: true });
                    docView.hideChangeTrackPopup();
                }
            },
            rejectChangeTracking: {
                parent: 'document/editable',
                set() {
                    docModel.getChangeTrack().resolveChangeTracking(null, { accepted: false, all: true });
                    docView.hideChangeTrackPopup();
                }
            },
            acceptSelectedChangeTracking: {
                parent: 'checkCurrentChangeTracking',
                set() {
                    docModel.getChangeTrack().resolveChangeTracking(null, { accepted: true, all: false });
                    docView.hideChangeTrackPopup();
                    docModel.setValidSelectionIfRequired();
                }
            },
            acceptMoveSelectedChangeTracking: {
                parent: 'document/editable',
                set() {
                    docModel.getChangeTrack().resolveChangeTracking(null, { accepted: true, all: false, refreshNodes: true, reuseSameNode: true });
                    docView.hideChangeTrackPopup();
                    docModel.getChangeTrack().getNextChangeTrack({ reuseSameNode: true });
                }
            },
            rejectSelectedChangeTracking: {
                parent: 'checkCurrentChangeTracking',
                set() {
                    docModel.getChangeTrack().resolveChangeTracking(null, { accepted: false, all: false });
                    docView.hideChangeTrackPopup();
                    docModel.setValidSelectionIfRequired();
                }
            },
            rejectMoveSelectedChangeTracking: {
                parent: 'document/editable',
                set() {
                    docModel.getChangeTrack().resolveChangeTracking(null, { accepted: false, all: false, refreshNodes: true, reuseSameNode: true });
                    docView.hideChangeTrackPopup();
                    docModel.getChangeTrack().getNextChangeTrack({ reuseSameNode: true });
                }
            },
            selectNextChangeTracking: {
                parent: 'app/valid',
                set() { docModel.getChangeTrack().getNextChangeTrack({ next: true }); }
            },
            selectPrevChangeTracking: {
                parent: 'app/valid',
                set() { docModel.getChangeTrack().getNextChangeTrack({ next: false }); }
            },
            changetrackPopupShow: {
                parent: 'app/valid',
                set() { docView.showChangeTrackPopup({ select: true }); }
            },

            // debug
            'debug/deleteHeaderFooter': {
                parent: 'document/editable',
                enable() { return docModel.isHeaderFooterEditState(); },
                set() { docModel.getPageLayout().removeActiveHeaderFooter(); }
            },
            'debug/toggleDrawingToText': {
                set() { docModel.toggleDrawingToTextMode(); }
            },

            // fields
            updateField: {
                parent: 'document/editable',
                set() { docModel.getFieldManager().updateHighlightedField(); }
            },
            updateAllFields: {
                parent: 'document/editable',
                set() { docModel.getFieldManager().updateAllFields(); }
            },
            removeField: {
                parent: 'document/editable',
                set() { docModel.getFieldManager().removeField(); }
            },
            editField: {
                parent: 'document/editable',
                set() { docModel.getFieldManager().editFieldFromContextMenu(); }
            },

            'document/insertfield': {
                parent: ['document/editable/text', 'insert/comment/odf/supported'],
                enable() {
                    // odf supports inserting fields only in frames, not in shapes and textframes
                    var selection = docModel.getSelection();
                    return !app.isODF() || (!selection.isAdditionalTextframeSelection() || isFullOdfTextframeNode(selection.getSelectedTextFrameDrawing()));
                },
                set(value) { docModel.getFieldManager().dispatchInsertField(value, 'default'); }
            },

            'document/formatfield': {
                parent: 'document/editable',
                get() { return docModel.getFieldManager().getSelectedFieldFormat(); },
                set(value) { docModel.getFieldManager().updateFieldFormatting(value); }
            },

            'document/inserttoc': {
                parent: ['document/editable/text/notextframe', 'insert/comment/supported', 'insert/margin/supported'],
                enable() { return !app.isODF(); },
                set(value) { docModel.getFieldManager().dispatchInsertToc(value); }
            },

            goToAnchor: {
                parent: 'app/valid',
                set() { docView.goToSelectedAnchor(); }
            },

            // android-entry for selection which cant be deleted by softkeyboard
            'document/editable/selection': {
                parent: 'document/editable',
                enable() {
                    var selection = docModel.getSelection();
                    return !_.isEqual(selection.getStartPosition(), selection.getEndPosition());
                }
            },
            'selection/delete': {
                parent: 'document/editable/selection',
                set() { return docModel.deleteSelected(); }
            }
        });

        // initialization -----------------------------------------------------

        if (_.browser.Android) {
            this.update = _.wrap(this.update, function (updateFunc) {
                if (docModel.isAndroidTyping()) {
                    return self.executeDelayed(self.update, 1000);
                } else {
                    return updateFunc.call(self);
                }
            });
        }

        // update GUI after changed selection
        this.updateOnEvent(docModel, 'selection');
    }
}

// exports ================================================================

export default TextController;
