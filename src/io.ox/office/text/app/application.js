/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import ConverterUtils from '$/io.ox/core/tk/doc-converter-utils';
import Inline from '$/io.ox/mail/compose/inline-images';

import { str } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getFileModelFromDescriptor, getStandardTrashFolderId } from '@/io.ox/office/tk/utils/driveutils';
import { FlushReason } from '@/io.ox/office/baseframework/utils/apputils';
import ErrorCode from '@/io.ox/office/baseframework/utils/errorcode';
import { SAVEAS, TEXT } from '@/io.ox/office/baseframework/utils/errorcontext';
import { getMessageData } from '@/io.ox/office/baseframework/utils/errormessages';
import { getElementStyleId } from '@/io.ox/office/editframework/utils/attributeutils';
import { EditApplication } from '@/io.ox/office/editframework/app/editapplication';
import { getPageContentNode } from '@/io.ox/office/textframework/utils/dom';
import Export from '@/io.ox/office/textframework/utils/export';
import { CONVERTER_AVAILABLE, SEND_AS_HTML_MAX_PAGE_COUNT } from '@/io.ox/office/textframework/utils/config';

import mvcFactory from '@/io.ox/office/text/app/mvcfactory';

// constants ==================================================================

var characterCountPerLine = 80;
var textFragmentMaxLength = characterCountPerLine;

// private static
function recursivelyReduceTextFragmentFromJQuerySiblings(node, collector) {
    var text = str.trimAll([

            collector.text,
            str.trimAndCleanNPC(node.text()).split(/\s+/).join(' ')        // - due to firefox that, with `innerText` ...
            //str.trimAndCleanNPC(node[0].innerText).split(/\s+/).join(' ')  //   ... delivers [undefined] for empty text nodes.

        ].join(' ')),

        diff = (collector.maxLength - text.length);

    if (diff <= 0) {
        text = text.substring(0, (text.length + diff));

        if (diff < 0) {
            text = text.replace((/\s+[^\s]*$/), '');
        }
    } else if (node.next()[0]) {
        collector.text = text;

        text = recursivelyReduceTextFragmentFromJQuerySiblings(node.next(), collector);
    }
    return text;
}

function reduceTextFragmentFromDOM(node, maxLength) {
    return recursivelyReduceTextFragmentFromJQuerySiblings(node.children().eq(0), {
        text: '',
        maxLength
    });
}

function predictivelyGetTitleTextFromDOM(node) {
    var styleIdRegXFilterList = [

            (/^(?:Title|Heading[0-6]*)$/i),
            (/^(?:ResumeStyle|ResumeDescription)[0-6]*$/i),
            (/^Normal[0-6]*$/i)
        ],
        createStyleIdFilter = function (regX) {
            return function (node) {
                return regX.test(getElementStyleId(node));
            };
        },
        regX,

        firstLevelNodeList = node.children().toArray().filter(function (node/*, idx list*/) {
            return node.classList.contains('p');
        }),
        nodeList = [],

        text = '';

    while ((regX = styleIdRegXFilterList.shift()) && !nodeList.length) {

        nodeList = firstLevelNodeList.filter(createStyleIdFilter(regX));
        //console.log('predictivelyGetTitleTextFromDOM :: nodeList.length : ', nodeList.length);
    }

    ((nodeList.length && nodeList) || firstLevelNodeList).some(function (node/*, idx, list*/) {
        text = str.trimAll([

            text,
            str.trimAndCleanNPC($(node).text()).split(/\s+/).join(' ') // - due to firefox that, with `innerText` ...
            //str.trimAndCleanNPC(node.innerText).split(/\s+/).join(' ') //   ... delivers [undefined] for empty text nodes.

        ].join(' '));

        return (text !== '');                           // targets the first non empty paragraph.
        //return (text.length >= textFragmentMaxLength);  // targets all paragraphs till maxLength condition is fulfilled.
    });

    if (text.length > textFragmentMaxLength) {
        text = text.substring(0, (textFragmentMaxLength + 1)).replace((/\s+[^\s]*$/), '');
    }
    //console.log('predictivelyGetTitleTextFromDOM :: before leeave - text : ', text);

    return text;
}

function extractTitleTextFromDOM(node) {
    var contentNode = getPageContentNode(node).eq(0),

        title = (
            predictivelyGetTitleTextFromDOM(contentNode) ||
            reduceTextFragmentFromDOM(contentNode, textFragmentMaxLength)
        );

    return title;
}

// // bugfix: Bug #50399 - "Sending shapes as email content doesn't make sense."
// function hasMainlyNonConvertibleContent(node) {
//     var isMainlyNonConvertibleContent = false,
//
//         contentNode = getPageContentNode(node).eq(0),
//
//         textContent = contentNode.text(),
//         imageCollection = contentNode.find('img'),
//         canvasCollection = contentNode.find('canvas');
//
//     if (textContent.length < (characterCountPerLine / 2)) {
//
//         isMainlyNonConvertibleContent = ((imageCollection.length === 0) && (canvasCollection.length >= 1));
//
//     } else if (textContent.length < characterCountPerLine) {
//
//         isMainlyNonConvertibleContent = ((imageCollection.length === 0) && (canvasCollection.length >= 2));
//     }
//     isMainlyNonConvertibleContent = (isMainlyNonConvertibleContent && (imageCollection.length < 2));
//     isMainlyNonConvertibleContent = (isMainlyNonConvertibleContent || (canvasCollection.length > 2));
//
//     return isMainlyNonConvertibleContent;
// }
//
// bugfix: Bug #58402 - "When sending content as mail, shapes are not included / visible."
// change to ... >> The alert will show up as soon as 'non convertible content' is in the document. <<
//
// bugfix Bug #58768 - "Misleading alert for empty 'document content as email"
// Follow up for Bug #58402.
//
function getContentConversionMeasures(node) {
    var
        contentNode = getPageContentNode(node).eq(0),

        textContent = contentNode.text(),
        imageCollection = contentNode.find('img'),
        canvasCollection = contentNode.find('canvas'),

        containsNeitherImageNorText = (
            (imageCollection.length === 0)
            && (
                (textContent.length === 0)
                || (/^[\s\t\n]+$/).test(textContent)
            )
        ),
        containsShapes = (canvasCollection.length >= 1);

    return {
        isEmpty: (!containsShapes && containsNeitherImageNorText),
        isNotConvertible: (containsShapes || containsNeitherImageNorText)
    };
}

// class TextApp ==============================================================

/**
 * The OX Text application.
 */
export default class TextApp extends EditApplication {

    /**
     * @param {CoreApp} coreApp
     *  The core application instance.
     */
    constructor(coreApp) {

        super(coreApp, mvcFactory, {
            applyActionsDetached: true,
            mergeCachedOperationsHandler: mergeCachedOperations,
            sendActionsDelay: 1000,
            isMultiSelectionApp: false,
            localStorageApp: true,
            requiredStorageVersion: 3,
            supportedStorageVersion: 3,
            supportsOnlineSync: true,
            otConfigKey: 'text/concurrentEditing'
        });

        // self reference
        var self = this;

        // private methods ----------------------------------------------------

        /**
         * Merge cached operations buffer, with current action buffer, before registering.
         *
         * @param {Array} actionBuffer
         *  Current array of operations.
         *
         * @param {Array} cacheBuffer
         *  Cached array of operations, e.g operations for complex fields update
         *  on document open are cached until document is modified.
         *
         * @returns {Array}
         *
         */
        function mergeCachedOperations(actionBuffer, cacheBuffer) {
            return cacheBuffer.concat(actionBuffer);
        }

        /**
         * Deferred helper that sanitizes base64 encoded image urls
         * within the passed chunk of stringified HTML markup.
         *
         * Any matching image gets converted into a file and uploaded
         * via a special ox service. After uploading a physical URL gets
         * retrieved that replaces the former base64 encoded image source.
         *
         * @param {string} htmlFragmentString
         *  Any chunk of stringified HTML markup.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the file upload could be
         *  managed successfully, or rejected otherwise.
         *  If resolved without failure it returns the promised
         *  and sanitized HTML fragment in its stringified form.
         */
        function sanitizeBase64ImageUrlsDeferred(htmlFragmentString) {
            var
                InlineApi     = Inline.api,

                $htmlFragment = $(['<div>', htmlFragmentString, '</div>'].join('')),
                imageList     = $htmlFragment.find('img'),

                promise = self.iterateArraySliced(imageList, function (img/*, idx, list*/) {
                    var src = img.src;

                    //if ((idx + 2) >= list.length) {     // forced rejection for development and testing purposes.
                    //    return $.Deferred().reject();
                    //}
                    if (/^data:/.test(src)) {
                        var byteString = window.atob(src.split(',')[1]),
                            mimeString = src.split(',')[0].split(':')[1].split(';')[0],
                            buffer = new ArrayBuffer(byteString.length),
                            intArray = new Uint8Array(buffer);

                        for (var i = 0; i < byteString.length; i++) {
                            intArray[i] = byteString.charCodeAt(i);
                        }
                        return InlineApi.inlineImage({ file: new Blob([intArray], { type: mimeString }) }).done(function success(response) {

                            img.src = InlineApi.getInsertedImageUrl(response);
                        });
                    }
                });

            return promise.then(function () {

                return $htmlFragment.html();

            }, function () {

                return { text: gt('Document images could not be processed. Send as mail is not possible.') };
            });
        }

        /**
         *
         */
        function createPdfMailAttachment() {
            var fileName = self.getFullFileName();
            var len      = fileName.length;
            var idx      = fileName.lastIndexOf('.');

            fileName = fileName.substring(0, ((idx >= 0) ? idx : len)) + '.pdf';

            var
                fileDescriptor  = self.getFileDescriptor(),

                deferred = getFileModelFromDescriptor(fileDescriptor).then(function (fileModel) {
                    return ConverterUtils.sendConverterRequest(fileModel, {
                        documentformat: 'pdf',
                        saveas_filename: fileName,
                        saveas_folder_id: getStandardTrashFolderId()
                    });
                });

            // intercept data flow via a new deferred object
            // in order to support this special failure case ...

            deferred.then(function (data) {

                if ('cause' in data) {

                    // ... an own error switch that passes its data
                    // into the generic deferred failure handling.
                    return new $.Deferred().reject(data);
                }
            });

            deferred.fail(function (error) {

                var // the message data retrieved from getMessageData
                    messageData = null;

                // make sure we always have a valid error object
                error = ErrorCode.getErrorCode(error);

                error.setErrorContext(SAVEAS);
                //error.setErrorContext(GENERAL);

                messageData = getMessageData(error, { fullFileName: self.getFullFileName() });
                globalLogger.warn('TextApplication :: createPdfMailAttachment :: Error code "' + error.getCodeAsConstant() + '"');

                // Use docView.yell to show message - this is only a temporary error and
                // cannot be set via setInternalError()
                self.getView().yell({ type: messageData.type, headline: messageData.headline, message: messageData.message });
            });

            return deferred.promise();
        }

        /**
         * Does select all of an current document's content,
         * converts that via clipboard functionality into a
         * stringified HTML markup and returns the latter.
         *
         * _Attention:_ This locally declared method will be registered as a
         * converter to its base application. Nevertheless it always does work
         * within the current document's `this` context.
         *
         * @returns {jQuery.Promise}
         *  If resolved without failure it returns the promised
         *  representative of the current model that features
         *  the stringified HTML markup equivalent of all of
         *  a document's `content` together with a `title`.
         */
        function createInlineHtmlDocument() {

            var htmlFragmentString,

                editor    = self.getModel(),
                selection = editor.getSelection(),

                rootNode  = selection.getRootNode(),
                titleText = extractTitleTextFromDOM(rootNode),

                // // bugfix: Bug #50399 - "Sending shapes as email content doesn't make sense."
                // isMainlyNonConvertibleContent = hasMainlyNonConvertibleContent(rootNode),

                // bugfix: Bug #58402 - "When sending content as mail, shapes are not included / visible."
                // change to ... >> The alert will show up as soon as 'non convertible content' is in the document. <<
                //
                // bugfix Bug #58768 - "Misleading alert for empty 'document content as email"
                // Follow up for Bug #58402.
                //
                conversionMeasures = getContentConversionMeasures(rootNode),

                // the existing selection
                startPosition = selection.getStartPosition(), endPosition = selection.getEndPosition();

            selection.selectAll();
            if (!self.isEditable()) {
                selection.restoreBrowserSelection({ forceWhenReadOnly: true });
            }
            htmlFragmentString = Export.getHTMLFromSelection(editor/*, { isEncodeBase64: false }*/);

            selection.setTextSelection(startPosition, endPosition); // restoring the existing selection
            if (!self.isEditable()) {
                selection.restoreBrowserSelection({ forceWhenReadOnly: true });
            }
            htmlFragmentString = htmlFragmentString.replace((/\s+'=""\s+/g), ' '); // fixing some html syntax errors.

            return sanitizeBase64ImageUrlsDeferred(htmlFragmentString).then(function (htmlFragmentString) {
                return {
                    // isMainlyNonConvertibleContent: isMainlyNonConvertibleContent,
                    isNotConvertible: conversionMeasures.isNotConvertible,
                    isEmpty: conversionMeasures.isEmpty,

                    // isEmpty: (!containsShapes && containsNeitherImageNorText),
                    // isNotConvertible: (containsShapes || containsNeitherImageNorText)

                    title:   titleText,
                    content: htmlFragmentString
                };
            });
        }

        // public methods -----------------------------------------------------

        /**
         * Text specific guard method that decides whether an application is able
         * of sending its document as mail - in this case as inline-html - or not.
         *
         * @returns {Boolean}
         * Returns `true` if the document's overall page count is in between 1 and
         * a maximum value that is maintained as office text property and provided
         * by `TextConfig.SEND_AS_HTML_MAX_PAGE_COUNT`. Otherwise returns `false`.
         */
        this.canSendMail = function () {
            var pageCount = self.docModel.getPageLayout().getNumberOfDocumentPages();

            return (
                (pageCount >= 1) &&
                (pageCount <= SEND_AS_HTML_MAX_PAGE_COUNT)
            );
        };

        // application runtime ------------------------------------------------

        // register inline HTML converter, e.g. for sending as email
        this.registerConverter("inlineHtml", createInlineHtmlDocument);

        // register PFD attachment converter, e.g. for sending as email
        if (CONVERTER_AVAILABLE) {
            this.registerConverter("pdfAttachment", createPdfMailAttachment);
        }

        // bug 55071: for newly generated docs, generate locale based page settings;
        // but for "edit as new" documents don't rewrite properties from original file
        if ((this.launchConfig.action === 'new') && _.isEmpty(this.launchConfig.templateFile)) {
            this.one('docs:state:initial', () => {
                this.docModel.setDefaultPaperFormat();
            });
        }
    }

    // protected methods ------------------------------------------------------

    /**
     * Post-processes the document contents and formatting, after all its
     * import operations have been applied.
     *
     * @param {boolean} fromStorage
     *  Whether the document has been imported from local storage.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the document has been post-processed
     *  successfully, or reject when an error has occurred.
     */
    /*protected override*/ implPostProcessImport(fromStorage) {
        return this.docModel.updateDocumentFormatting(fromStorage);
    }

    /**
     * Handler for fast load of empty documents.
     *
     * @param {string} markup
     *  The HTML markup to be shown as initial document contents.
     *
     * @param {Operations[]} operations
     *  The document operations to be applied to finalize the fast import.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the operations have been applied.
     */
    /*protected override*/ implFastEmptyLoad(markup, operations) {
        return this.docModel.fastEmptyLoadHandler(markup, operations);
    }

    /**
     * Will be called by base class if importing the document failed.
     *
     * @param {ErrorCode} error
     *  The parsed error response data.
     */
    /*protected override*/ implImportFailed(error) {

        //  DOCS-733: Improve OOM Error Message in Client
        switch (error.getCodeAsConstant()) {
            case "LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR":
                this.setInternalError(error, TEXT, null, { showMessage: false });
                break;
        }
    }

    /**
     * Optimizes the operation actions before sending them to the server.
     *
     * @param {OperationAction[]} actions
     *  An array with operation actions.
     *
     * @returns {OperationAction[]}
     *  The optimized array with operation actions.
     */
    /*protected override*/ implOptimizeOperations(actions) {
        return this.docModel.optimizeActions(actions);
    }

    /**
     * Preprocessing before the document will be flushed for downloading,
     * printing, sending as mail, or closing.
     *
     * @param {FlushReason} reason
     *  The origin of the flush request.
     */
    /*protected override*/ implPrepareFlushDocument(reason) {
        if ((reason === FlushReason.DOWNLOAD) || (reason === FlushReason.EMAIL)) {
            this.docModel.getFieldManager().prepareDateTimeFieldsDownload();
        }
    }

    /**
     * Trigger flushing of buffered operations on losing edit rights.
     */
    /*protected override*/ implPrepareLoseEditRights() {
        this.docModel.trigger('cacheBuffer:flush');
    }

    /**
     * Returns additional launch options for reloading this document.
     */
    /*protected override*/ implPrepareReloadDocument() {
        return this.docModel.prepareReloadDocument();
    }
}
