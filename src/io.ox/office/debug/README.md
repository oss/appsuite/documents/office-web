# The Debug Package

This directory contains source code for development builds of OX Documents. All code contained in this directory will NOT be packed into the released product, but into a special debug package that can optionally be installed. This has a few technical implications for importing modules from this package (see chapter _Module Imports_ below).

## Package Structure

The debug package contains a directory "common" that contains shared debug code for all applications, and another directory for each application containing special debug code.

The main entry points of the debug package are the following three functions exported from `io.ox/office/debug/common/main`:

- The function `initializeApp()` performs early application initialization, e.g. registering controller items. It will be called from the EditApplication's method `implInitializeApp()` after the construction and initialization of the application, model, view, and controller, just before the import process starts.

- The function `initializeView()` performs global initialization of the document view (e.g., it registers the toolpane with the tab identifier "debug"). It will be called from the EditView's method `implInitializeDebug()` after regular view initialization.

- The function `initializeToolPane()` adds toolbars and form controls to the debug toolpane. It will be called from the toolpane manager when the debug toolpane will be rendered for the first time.

All three functions try to load and execute the same function from the `main` module of the respective application directory, e.g. from `io.ox/office/debug/spreadsheet/main` when loading a spreadsheet document. These functions will receive a parameter `debugContext` containing additional objects created in the common main module, e.g. the operation replay worker and the UI test runner (created in `initializeApp()`), the operations pane and the clipboard pane (created in `initializeView()`), and the common debug toolbar (created in `initializeToolPane()`).

## Debug Flags and Console Loggers

### Local Storage and URL Anchor

Debug user settings will be saved in a protected item in the local storage. This item will not be deleted when App Suite cleans up the storage on logout or on UI version updates.

By convention, the configuration keys of common debug settings start with "office:" (for example "office:enable-debug", "office:log-focus"), and the configuration keys of application-specific debug settings start with the application name (for example "spreadsheet:debug-formulas", "spreadsheet:log-renderer"). The configuration keys of console loggers start with "log-" (e.g. "office:log-focus").

All debug user settings can be overridden by inserting the configuration keys in the URL anchor part, for example `https://localhost:8337/#.....&text:enable-something=true`.

### Debug Flags

The common debug toolbar contains a drop-down menu with all debug flags, accessible as property `debugToolBar.optionsPicker`. The current state of debug flags can be accessed with the function `getDebugFlag()` exported from the module `io.ox/office/tk/config`.

To create and use a new debug flag, the following steps are needed:

- Create a new item in the "Options" drop-down menu. Common items can be created in the class constructor of `DebugOptionsPicker`, application-specific items can be created in the function `initializeToolPane()`.

  Example:

  ```JS
  function initializeToolPane(docView, debugContext) {
      const { debugToolBar } = debugContext;
      debugToolBar.optionsPicker.registerOption("text:enable-something", "Useful tooltip");
  }
  ```

- Use the debug flag somewhere in the Documents code by passing its configuration key to `getDebugFlag()`.

  Example:

  ```JS
  if (Config.getDebugFlag("text:enable-something")) {
      ...
  }
  ```

### Console Loggers

The common debug toolbar contains a drop-down menu allowing to change the log level of all console loggers, accessible as property `debugToolBar.loggersPicker`.

To create and use a new console loggers, the following steps are needed:

- Create a new item in the "Loggers" drop-down menu. Common loggers can be registered in the class constructor of `DebugLoggersPicker`, application-specific loggers can be registered in the function `initializeToolPane()`.

  Example:

  ```JS
  function initializeToolPane(docView, debugContext) {
      const { debugToolBar } = debugContext;
      debugToolBar.loggersPicker.registerLogger("text:log-something", "Useful tooltip");
  }
  ```

- Create a logger bound to the configuration key somewhere in the Documents code.

  Example:

  ```JS
  const logger = new Logger("text:log-something", { tag: "ABC" });
  ...
  logger.info("Something interesting.");
  ```

It is possible to create multiple loggers that are all bound to the same configuration key.

## Module Imports

It should be sufficient to add new debug code into the entry functions described above. However, the technical details for allowed and forbidden imports regarding the debug package will be described in this chapter.

### Forbidden Static Imports

Source files from this directory _MUST NOT_ be imported statically (with `import` statement) from production source files. Doing so would throw exceptions when trying to load the non-existant sources in a release build, effectively making the entire product unusable.

WRONG:

```JS
import { NormalStuff } from "@/io.ox/office/tk/stuff";
import { DebugStuff } from "@/io.ox/office/debug/stuff";  // <=== fails in release build!!
```

However, it is perfectly valid that debug source files import other debug source files (as well as any other production source files).

### Allowed Dynamic Imports

The only way to import debug source files from production source files is to use a dynamic import, i.e. the `import()` runtime function which allows to individually handle import errors. Importing optional source files must be wrapped in a `try`/`catch` block, e.g.:

```JS
try {
    const { DebugStuff } = await import("@/io.ox/office/debug/stuff");
    ...
} catch { }
```

However, all debug code should try use the existing registration entry points described above. Make use of event handlers and other callback mechanisms provided by the application to add more debug code.

### Linter Support

The custom ESLint rule `env-project/no-invalid-modules` implemented in the package `@open-xchange/linter-presets` will take care of the aforementioned rules, i.e. it will fail when trying to statically import debug source files from production source files.
