/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { Color } from '@/io.ox/office/editframework/utils/color';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';
import * as Op from '@/io.ox/office/textframework/utils/operations';
import { getDOMPosition, getParagraphLength } from '@/io.ox/office/textframework/utils/position';

// constants ==================================================================

const LOREM_IPSUM_STRING = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';

const COPY_PASTE_TEMPLATE_OPERATIONS = [
    { name: Op.PARA_INSERT, start: [0] },
    { name: Op.TEXT_INSERT, start: [0, 0], text: '0 ' + LOREM_IPSUM_STRING },
    { name: Op.SET_ATTRIBUTES, start: [0, 2], end: [0, 592], attrs: { character: { bold: true } } },
    { name: Op.PARA_INSERT, start: [1] },
    { name: Op.TEXT_INSERT, start: [1, 0], text: '1 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [1, 2], end: [1, 592], attrs: { character: { italic: true } } },
    { name: Op.PARA_INSERT, start: [2] },
    { name: Op.TEXT_INSERT, start: [2, 0], text: '2 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [2, 2], end: [2, 592], attrs: { character: { underline: true } } },
    { name: Op.PARA_INSERT, start: [3] },
    { name: Op.TEXT_INSERT, start: [3, 0], text: '3 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [3, 2], end: [3, 592], attrs: { character: { color: Color.RED } } },
    { name: Op.PARA_INSERT, start: [4] },
    { name: Op.TEXT_INSERT, start: [4, 0], text: '4 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [4, 2], end: [4, 592], attrs: { character: { fontsize: 14 } } },
    { name: Op.PARA_INSERT, start: [5] },
    { name: Op.TEXT_INSERT, start: [5, 0], text: '5 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [5, 2], end: [5, 592], attrs: { character: { fillColor: Color.RED } } },
    { name: Op.PARA_INSERT, start: [6] },
    { name: Op.TEXT_INSERT, start: [6, 0], text: '6 ' + LOREM_IPSUM_STRING },
    { name: Op.SET_ATTRIBUTES, start: [6, 0], end: [6, 592], attrs: { character: { vertAlign: 'sub' } } },
    { name: Op.PARA_INSERT, start: [7] },
    { name: Op.TEXT_INSERT, start: [7, 0], text: '7 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [7, 0], end: [7, 592], attrs: { character: { vertAlign: 'super' } } },
    { name: Op.PARA_INSERT, start: [8] },
    { name: Op.TEXT_INSERT, start: [8, 0], text: '8 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [8, 0], end: [8, 592], attrs: { character: { strike: 'single' } } },
    { name: Op.PARA_INSERT, start: [9] },
    { name: Op.TEXT_INSERT, start: [9, 0], text: '9 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [9], attrs: { paragraph: { lineHeight: { type: 'percent', value: 150 } } } },
    { name: Op.PARA_INSERT, start: [10] },
    { name: Op.TEXT_INSERT, start: [10, 0], text: '10 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [10], attrs: { paragraph: { alignment: 'right' } } },
    { name: Op.PARA_INSERT, start: [11] },
    { name: Op.TEXT_INSERT, start: [11, 0], text: '11 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [11], attrs: { paragraph: { alignment: 'justify' } } },
    { name: Op.PARA_INSERT, start: [12] },
    { name: Op.TEXT_INSERT, start: [12, 0], text: '12 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [12], attrs: { paragraph: { alignment: 'center' } } },
    { name: Op.PARA_INSERT, start: [13] },
    { name: Op.TEXT_INSERT, start: [13, 0], text: '13 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [13], attrs: { paragraph: { lineHeight: { type: 'percent', value: 100 } } } },
    { name: Op.PARA_INSERT, start: [14] },
    { name: Op.TEXT_INSERT, start: [14, 0], text: '14 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [14], attrs: { paragraph: { fillColor: Color.GREEN } } },
    { name: Op.PARA_INSERT, start: [15] },
    { name: Op.TEXT_INSERT, start: [15, 0], text: '15 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [15], attrs: { paragraph: { lineHeight: { type: 'percent', value: 200 } } } },
    { name: Op.PARA_INSERT, start: [16] },
    { name: Op.TEXT_INSERT, start: [16, 0], text: '16 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [16], attrs: { paragraph: { marginTop: 0, marginBottom: 704 } } },
    { name: Op.PARA_INSERT, start: [17] },
    { name: Op.TEXT_INSERT, start: [17, 0], text: '17 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [17], attrs: { paragraph: { marginTop: 0, marginBottom: 0 } } },
    { name: Op.PARA_INSERT, start: [18] },
    { name: Op.TEXT_INSERT, start: [18, 0], text: '18 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [18], attrs: { paragraph: { fillColor: Color.RED } } },
    { name: Op.PARA_INSERT, start: [19] },
    { name: Op.TEXT_INSERT, start: [19, 0], text: '19 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [19, 0], end: [19, 592], attrs: { character: { fillColor: Color.GREEN } } },
    { name: Op.PARA_INSERT, start: [20] },
    { name: Op.TEXT_INSERT, start: [20, 0], text: '20 ' + LOREM_IPSUM_STRING  },
    { name: Op.SET_ATTRIBUTES, start: [20, 0], end: [20, 592], attrs: { character: { fillColor: Color.VIOLET } } }
];

// exports ================================================================

/**
 * Definitions for the runtime tests of OX Text.
 */
export default {

    // single client, simple operation test, no commands
    TEST_01: {
        description: 'Simple single client test',
        tooltip: 'A simple single client test',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABC' },
                        { name: Op.TEXT_INSERT, start: [0, 3], text: 'DEF' },
                        { name: Op.TEXT_INSERT, start: [0, 6], text: 'G' }
                    ] },
                    { selection: { start: [0, 1], end: [0, 3] } },
                    { keyboardevents: '2*SHIFT+RIGHT_ARROW' }

                ]
            }
        },
        commands: null,
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 1], end: [0, 5] },
                expectedTextContent: 'ABCDEFG'
            }
        }
    },

    // single client, with all available commands. This test is the command reference test.
    TEST_02: {
        description: 'The command reference test',
        tooltip: 'A single client test demonstrating the possible commands',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABC' },
                        { name: Op.TEXT_INSERT, start: [0, 3], text: 'DEF' }
                    ] },
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200, // currently only used for keyboard events
                    commandDelay: 50,
                    arbitraryTestStartDelayMax: 2000 // the maximum time for starting the test delayed (can be used to start test differently on different clients)
                },
                orders: [
                    { keyboardevents: [
                        '1 2 3 4 5 6 7 8 9 0 a SHIFT+a',
                        'ENTER', 'b', 'c', 'd', 'ENTER', 'e', 'f', 'g',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'UP_ARROW', 'DOWN_ARROW', 'UP_ARROW', 'DOWN_ARROW', 'UP_ARROW', 'DOWN_ARROW', 'RIGHT_ARROW', 'RIGHT_ARROW', 'RIGHT_ARROW'
                    ] },
                    { selection: { start: [0, 3], end: [0, 5] } },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } }, // Info: Only functions at model allowed yet
                    { timeout: 1000 }, // specifying a timeout
                    { operations: [ // Info: In this tests it is always better to call the function that generates the operation, not the operation directly.
                        { name: Op.PARA_INSERT, start: [1] },
                        { name: Op.TEXT_INSERT, start: [1, 0], text: 'ABCD' },
                        { name: Op.SET_ATTRIBUTES, start: [1, 1], end: [1, 2], attrs: { character: { bold: true } } }
                    ] },
                    { selection: { start: [0, 7], end: [0, 7] } },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '3*DOWN_ARROW 2*RIGHT_ARROW ENTER' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/stylesheet' } },
                    { timeout: 500 },
                    { keyboardevents: '4*DOWN_ARROW 2*RIGHT_ARROW ENTER' },
                    { timeout: 100 },
                    { keyboardevents: 'a b c' },
                    { activatetoolbar: 'format' },
                    { selection: { start: [1, 0, 0, 0, 0], end: [1, 0, 0, 0, 3] } },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { timeout: 1000 }, // specifying a timeout
                    { buttonclick: { dataKey: 'view/character/format/menu' } }, // opens sub menu
                    { timeout: 1000 }, // specifying a timeout
                    { buttonclick: { selector: '.popup-container .group[data-key="character/vertalign"] > a[data-value="super"]' } },
                    { timeout: 1000 }, // specifying a timeout
                    { selection: { start: [1, 1, 0, 0, 0] } },
                    { keyboardevents: [
                        'SHIFT+q q SPACE 0 1 2 SPACE 3 4 5 SPACE 6 7 8 SPACE 9',
                        'SHIFT+a SHIFT+b SPACE SHIFT+c SHIFT+d SHIFT+e',
                        'ENTER SHIFT+f SHIFT+g SHIFT+h SHIFT+i',
                        '3*BACKSPACE l m SPACE n o'
                    ] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { keyboardevents: 'a b c SPACE d e f SPACE g h i' },
                    { buttonclick: 'undo' },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'PAGE' } } }, // leaving header/footer edit mode
                    { timeout: 1000 }, // specifying a timeout
                    { selection: { start: [0, 3], end: [0, 3] } },
                    { keyboardevents: 'SPACE SHIFT+m a i n SPACE SHIFT+r e l o a d' },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
                    { keyboardevents: '1 2 3 4 5 6 ENTER 7 8 9 0 SPACE SHIFT+f r i d a y SPACE' },
                    // { activatetoolbar: 'insert' },
                    // { selection: { start: [0, 1], end: [0, 5] } },
                    // { buttonclick: { dataKey: 'comment/insert' } },
                    // { timeout: 1000 }, // specifying a timeout
                    // { keyboardevents: 'i n SPACE t h e SPACE c o m m e n t' }
                    { triggerevent: { type: 'nodeClick', definition: { target: 'PAGE' } } },
                    { selection: { start: [0, 3], end: [0, 3] } },
                    { keyboardevents: 'ENTER o u t SPACE o f SPACE c o m m e n t ENTER 1 2 3 ENTER' },
                    { activatetoolbar: 'format' },
                    { selection: { start: [1, 0], end: [1, 14] } },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'character/color' } },
                    { selection: { start: [2, 1], end: [2, 1] } },
                    { buttonclick: { dataKey: 'character/color', index: 1 } }, // using the second button (the arrow on the split button)
                    { timeout: 500 },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="green"]' } },
                    { timeout: 500 },
                    { selection: { start: [7, 0], end: [7, 0] } },
                    { keyboardevents: 'SPACE g r e e n SPACE' },
                    { selection: { start: [7, 2], end: [7, 2] } },
                    { buttonclick: { dataKey: 'character/underline' } }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_02A: {
        description: 'Demo test for mouse selections in page, header and comment',
        tooltip: 'Demo test for mouse selections to switch between different targets',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'Hallo Welt.' }
                    ] }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200, // currently only used for keyboard events
                    commandDelay: 15,
                    arbitraryTestStartDelayMax: 2000 // the maximum time for starting the test delayed (can be used to start test differently on different clients)
                },
                orders: [
                    { keyboardevents: ['1', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9', '0', 'ENTER', 'a', 'b', 'c', 'd', 'e'] },
                    // clicking behind the '7'
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .p:nth-of-type(2)', textposition: 'middle', forceTestSelection: true  } } },
                    // { keyboardevents: [{ key: 'RIGHT_ARROW', shift: true }, { key: 'RIGHT_ARROW', shift: true }] },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { dataKey: 'comment/insert' } },
                    { timeout: 500 },
                    { keyboardevents: ['i', 'n', 'SPACE', 't', 'h', 'e', 'ENTER ', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER', '1', '2', '3'] },
                    { timeout: 500 },
                    { buttonclick: { selector: '.app-content > .page > .commentlayer .commentEditor .btn-default' } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .p:nth-of-type(1)', textposition: 'middle', forceTestSelection: true  } } }, // leaving comment
                    { timeout: 1000 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .commentlayer .commentthread:nth-of-type(1) > .drawing:nth-of-type(1) .p:nth-of-type(2)', textposition: 'middle', forceTestSelection: true  } } }, // activating comment
                    { keyboardevents: ['RIGHT_ARROW', 'RIGHT_ARROW', 'SPACE', 'n', 'e', 'w'] },
                    { timeout: 1000 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .p:nth-of-type(1)', textposition: 'middle', forceTestSelection: true  } } }, // leaving comment
                    { keyboardevents: ['SPACE', 'm', 'a', 'i', 'n', 'SPACE'] },
                    { timeout: 1000 },
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating the header
                    { keyboardevents: ['i', 'n', 'ENTER', 't', 'h', 'e', 'ENTER', 'h', 'e', 'a', 'd', 'e', 'r'] },
                    { timeout: 1000 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .p:nth-of-type(3)', textposition: 'middle', forceTestSelection: true  } } }, // leaving header
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating the header
                    { timeout: 500 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .header-wrapper > .active-selection > .marginalcontent > .p:nth-of-type(2)', textposition: 'end', forceTestSelection: true  } } }, // setting cursor in header
                    { keyboardevents: ['SPACE', 'n', 'e', 'w'] },
                    { timeout: 1000 },
                    { triggerevent: { type: 'nodeClick', definition: { selector: '.app-content > .page > .pagecontent > .p:nth-of-type(2)', textposition: 'start', forceTestSelection: true  } } },
                    { keyboardevents: ['m', 'a', 'i', 'n', 'SPACE'] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // single client, with all available commands. This test is the command reference test.
    TEST_02B: {
        description: 'A demo test for assertions',
        tooltip: 'A single client test demonstrating assertions',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] }
                    ] },
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200, // currently only used for keyboard events
                    commandDelay: 50,
                    arbitraryTestStartDelayMax: 2000 // the maximum time for starting the test delayed (can be used to start test differently on different clients)
                },
                orders: [
                    { assert: { id: 10, expect: { type: 'selection', start: [0, 0], end: [0, 0] }, text: 'Unexpected selection.' } },
                    { keyboardevents: 'a b c' },
                    { assert: { id: 20, expect: { type: 'selection', start: [0, 3], end: [0, 3] }, text: 'Unexpected selection.' } },
                    { assert: { id: 30, expect: { type: 'text', position: [0], content: 'abc' }, text: 'Unexpected text content.' } },
                    { assert: { id: 40, expect: { type: 'function', context: 'model', function: [{ name: 'getNode' }, { name: 'children', param: '.pagecontent' }, { name: 'children', param: '.p' }, { name: 'length', isProperty: true }], result: 1 }, text: 'Unexpected paragraph count.' } },
                    { assert: { id: 50, expect: { type: 'function', context: 'model', function: [{ name: 'getNode' }, { name: 'children', param: '.pagecontent' }, { name: 'children', param: '.p' }, { name: 'first' }, { name: 'text' }], result: 'abc' }, text: 'Unexpected text content.' } },
                    { assert: { id: 60, expect: { type: 'function', context: 'Position', function: [{ name: 'getParagraphLength', param: ['CURRENT_ROOT_NODE', [0, 3]] }], result: 3 }, text: 'Unexpected paragraph length.' } },
                    { assert: { id: 70, expect: { type: 'call', function(app) {
                        return getParagraphLength(app.getModel().getCurrentRootNode(), app.getModel().getSelection().getStartPosition());
                    }, result: 3 }, text: 'Unexpected paragraph length.' } },
                    { keyboardevents: '3*SHIFT+LEFT_ARROW' },
                    { assert: { id: 75, expect: { type: 'selection', start: [0, 0], end: [0, 3] }, text: 'Unexpected selection.' } },
                    { assert: { id: 80, expect: { type: 'call', function(app) {
                        var domPos = getDOMPosition(app.getModel().getCurrentRootNode(), app.getModel().getSelection().getStartPosition(), true);
                        var explicitAttrs = getExplicitAttributes(domPos.node, 'character');
                        return explicitAttrs.bold;
                    }, result: undefined }, text: 'Unexpected explicit attribute value for "bold" .' } },
                    { functioncall: { type: 'call', function(app) {
                        app.getModel().setAttribute('character', 'bold', true); // only do this in preparatation phase -> in test phase use 'buttonclick'
                    } } },
                    { assert: { id: 90, expect: { type: 'call', function(app) {
                        var domPos = getDOMPosition(app.getModel().getCurrentRootNode(), app.getModel().getSelection().getStartPosition(), true);
                        var explicitAttrs = getExplicitAttributes(domPos.node, 'character');
                        return explicitAttrs.bold;
                    }, result: true }, text: 'Unexpected explicit attribute value for "bold" .' } }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // single client, with all available commands. This test is the command reference test.
    TEST_02C: {
        description: 'A demo test for assertions with reload',
        tooltip: 'A single client test demonstrating assertions with reload',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] }
                    ] },
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200, // currently only used for keyboard events
                    commandDelay: 50,
                    arbitraryTestStartDelayMax: 2000 // the maximum time for starting the test delayed (can be used to start test differently on different clients)
                },
                orders: [
                    { assert: { id: 10, expect: { type: 'selection', start: [0, 0], end: [0, 0] }, text: 'Unexpected selection.' } },
                    { keyboardevents: 'a b c' },
                    { assert: { id: 20, expect: { type: 'selection', start: [0, 3], end: [0, 3] }, text: 'Unexpected selection.' } },
                    { assert: { id: 30, expect: { type: 'text', position: [0], content: 'abc' }, text: 'Unexpected text content.' } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
                    { assert: { id: 40, expect: { type: 'selection', start: [0, 3], end: [0, 3] }, text: 'Unexpected selection.' } },
                    { keyboardevents: 'SHIFT+d SHIFT+e Shift+f' },
                    { assert: { id: 50, expect: { type: 'text', position: [0], content: 'abcDEF' }, text: 'Unexpected text content.' } }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // two clients, with commands, adding content into different paragraphs
    TEST_03: {
        description: 'Content in different paragraphs',
        tooltip: 'A two clients test with text input in different paragraphs',
        clients: 2,
        autotest: true,
        repeat: 1,
        repeatDelay: 2000,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABCDEF' },
                        { name: Op.PARA_INSERT, start: [1] },
                        { name: Op.TEXT_INSERT, start: [1, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [1, 1], end: [1, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1500,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: ['1', '2', '3', '4', '5', '6', '7', '8'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: ['1', '2', '3', '4', '5', '6', '7', '8'] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 13], end: [0, 13] },
                expectedTextContent: 'ABCDE12345678FM12345678NOPQR'
            },
            CLIENT_2: {
                expectedSelection: { start: [1, 9], end: [1, 9] },
                expectedTextContent: 'ABCDE12345678FM12345678NOPQR'
            }
        }
    },

    // two clients, with commands, adding content into the same paragraph
    TEST_04: {
        description: 'Content in same paragraph',
        tooltip: 'A two clients test with text input into the same paragraphs',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 100
                },
                orders: [
                    { keyboardevents: ['1', '2', '3', '4', '5', '6', '7', '8'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 100
                },
                orders: [
                    { keyboardevents: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 21], end: [0, 21] },
                expectedTextContent: 'MabcdefghNOPQ12345678R'
            },
            CLIENT_2: {
                expectedSelection: { start: [0, 9], end: [0, 9] },
                expectedTextContent: 'MabcdefghNOPQ12345678R'
            }
        }
    },

    // two clients, with commands, adding content into the same paragraph, faster than test 4
    TEST_05: {
        description: 'Content in same paragraph',
        tooltip: 'A two clients test with text input into the same paragraphs, faster than TEST_04',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: ['1', '2', '3', '4', '5', '6', '7', '8'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 800,
                    commandDelay: 70
                },
                orders: [
                    { keyboardevents: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 21], end: [0, 21] },
                expectedTextContent: 'MabcdefghNOPQ12345678R'
            },
            CLIENT_2: {
                expectedSelection: { start: [0, 9], end: [0, 9] },
                expectedTextContent: 'MabcdefghNOPQ12345678R'
            }
        }
    },

    // two clients, with commands, adding content into the same paragraph like test 5.
    // But additionally the 'ENTER' key is pressed.
    TEST_06: {
        description: 'Content in same paragraph and ENTER',
        tooltip: 'A two clients test with text input into the same paragraph and using ENTER',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 20
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 900,
                    commandDelay: 30
                },
                orders: [
                    { keyboardevents: [
                        'ENTER', 'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'g', 'h', 'i', 'j'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [9, 4], end: [9, 4] },
                expectedTextContent: 'MabcdefghijabcdefghijabcdefghijNOPQ012345678901234567890123456789R'
            },
            CLIENT_2: {
                expectedSelection: { start: [6, 7], end: [6, 7] },
                expectedTextContent: 'MabcdefghijabcdefghijabcdefghijNOPQ012345678901234567890123456789R'
            }
        }
    },

    // two clients, with commands, adding content into the same paragraph like test 5.
    // 'ENTER' key and 'PageUp' keys are pressed.
    TEST_07: {
        description: 'Content in same paragraph and ENTER and PageUP',
        tooltip: 'A two clients test with text input into the same paragraph and using ENTER and PageUP',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 120
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9', 'PAGE_UP',
                        '0', '1', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 900,
                    commandDelay: 130
                },
                orders: [
                    { keyboardevents: [
                        'ENTER', 'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'g', 'h', 'i', 'j'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [2, 4], end: [2, 4] },
                expectedTextContent: '01234567890123456789MabcdefghijabcdefghijabcdefghijNOPQ0123456789R'
            },
            CLIENT_2: {
                expectedSelection: { start: [8, 7], end: [8, 7] },
                expectedTextContent: '01234567890123456789MabcdefghijabcdefghijabcdefghijNOPQ0123456789R'
            }
        }
    },

    // two clients, with commands, adding content into the same paragraph like test 5.
    // 'ENTER' key is pressed and the selection is changed by one client.
    TEST_08: {
        description: 'Content in paragraph and ENTER and selection change (test is repeated)',
        tooltip: 'A two clients test with text input into the same paragraph and using ENTER and changing the selection (this test is repeated several times)',
        clients: 2,
        autotest: true,
        repeat: 1,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 20
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9', 'PAGE_UP',
                        '0', '1', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9'
                    ] },
                    { selection: { start: [4, 0] } },
                    { keyboardevents: ['ENTER', 'ENTER', '0', '1', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 900,
                    commandDelay: 30
                },
                orders: [
                    { keyboardevents: ['ENTER', 'a', 'b', 'c', 'ENTER', 'd', 'e', 'f'] },
                    { selection: { start: [3, 0] } },
                    { keyboardevents: [
                        'ENTER', 'ENTER', 'g', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'g', 'h', 'i', 'j'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [7, 4], end: [7, 4] },
                expectedTextContent: '0123456789Mabcdef0123456789ghijabcdefghijabcdefghijNOPQ0123456789R',
                expectedParagraphCount: 14
            },
            CLIENT_2: {
                expectedSelection: { start: [12, 7], end: [12, 7] },
                expectedTextContent: '0123456789Mabcdef0123456789ghijabcdefghijabcdefghijNOPQ0123456789R',
                expectedParagraphCount: 14
            }
        }
    },

    // two clients, testing OT for combination of insertText and splitParagraph operations
    TEST_09: {
        description: 'insertText and splitParagraph',
        tooltip: 'A two clients test for insertText and splitParagraph operations',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABCDEF' }
                    ] },
                    { selection: { start: [0, 2], end: [0, 2] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 100
                },
                orders: [
                    { keyboardevents: ['ENTER'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 100
                },
                orders: [
                    { keyboardevents: ['a'] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [1, 0], end: [1, 0] },
                expectedTextContent: 'ABCDEaF'
            },
            CLIENT_2: {
                expectedSelection: { start: [1, 4], end: [1, 4] },
                expectedTextContent: 'ABCDEaF'
            }
        }
    },

    // two clients, with commands.
    // This test is used to find recursive calls. Might be insertText after running splitParagraph.
    TEST_10: {
        description: 'Finding recursive calls with insertText and splitParagraph',
        tooltip: 'A two clients test for insertText and splitParagraph operations to find recursive calls',
        clients: 2,
        autotest: true,
        avoidAdditionalPreparation: true, // spellchecking is not relevant in this test
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 15,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { activatetoolbar: 'format' }, // only to leave "debug" in automated tests
                    { keyboardevents: [
                        '0', '1', '2', 'ENTER', '3', '4', '5', 'ENTER', '6', '7', '8', 'ENTER', '9',
                        '0', '1', '2', 'ENTER', '3', '4', '5', 'ENTER', '6', '7', '8', 'ENTER', '9', 'ENTER', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 10,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        'ENTER', 'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'ENTER', 'ENTER', 'g', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'ENTER', 'd', 'e', 'ENTER', 'f', 'g', 'ENTER', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'ENTER', 'g', 'h', 'i', 'j'
                    ] }
                ]
            }
        },
        result: {
        }
    },

    // two clients, with commands.
    // This test is used to find recursive calls. Might be insertText after running splitParagraph.
    // This test is slower than test 10
    TEST_11: {
        description: 'insertText and splitParagraph',
        tooltip: 'A two clients test for insertText and splitParagraph operations (slower than TEST_10)',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', 'ENTER', '3', '4', '5', 'ENTER', '6', '7', '8', 'ENTER', '9',
                        '0', '1', '2', 'ENTER', '3', '4', '5', 'ENTER', '6', '7', '8', 'ENTER', '9', 'ENTER', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', '4', '5', 'ENTER', '6', '7', '8', '9'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 120,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        'ENTER', 'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'ENTER', 'ENTER', 'g', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'ENTER', 'd', 'e', 'ENTER', 'f', 'g', 'ENTER', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'ENTER', 'd', 'e', 'f', 'ENTER', 'g', 'h', 'i', 'j'
                    ] }
                ]
            }
        },
        result: {
        }
    },

    // single client. Testing insertTab, insertDrawing, insertTable and insertRows.
    TEST_12: {
        description: 'insertTab, insertDrawing, insertTable and insertRows',
        tooltip: 'A single client test for insertTab, insertDrawing, insertTable and insertRows',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABC' },
                        { name: Op.TEXT_INSERT, start: [0, 3], text: 'DEF' }
                    ] },
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                orders: [
                    { keyboardevents: ['1', 'TAB', '2', 'TAB', '3', 'TAB', '4', '5', 'TAB', '6', '7', '8'] },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { dataKey: 'textframe/insert' } },
                    { keyboardevents: ['a', 'b', 'c'] },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 100 },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 100 },
                        'TAB',
                        { keyCode: 66, charCode: 101 },
                        { keyCode: 67, charCode: 102 }
                    ] },
                    { activatetoolbar: 'table' },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 103 },
                        { keyCode: 66, charCode: 104 },
                        'TAB',
                        { keyCode: 67, charCode: 105 }
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 13, 1, 2, 1, 0, 1], end: [0, 13, 1, 2, 1, 0, 1] },
                expectedTextContent: 'A1       2         3         45       678defghiBCDEFdefghi'
            }
        }
    },

    // two clients. Testing insertTab, insertDrawing, insertTable and insertRows.
    TEST_13: {
        description: 'insertTab, insertDrawing, insertTable and insertRows',
        tooltip: 'A two clients test for insertTab, insertDrawing, insertTable and insertRows',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABC' },
                        { name: Op.TEXT_INSERT, start: [0, 3], text: 'DEF' }
                    ] },
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 4], end: [0, 4] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500
                },
                orders: [
                    { keyboardevents: ['1', 'TAB', '2', 'TAB', '3', 'TAB', '4', '5', 'TAB', '6', '7', '8'] },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { dataKey: 'textframe/insert' } },
                    { keyboardevents: ['a', 'b', 'c'] },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW 2*DOWN_ARROW ENTER' },
                    { timeout: 100 },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 100 },
                        { keyCode: 66, charCode: 101 },
                        { keyCode: 67, charCode: 102 },
                        'TAB',
                        { keyCode: 68, charCode: 103 },
                        { keyCode: 69, charCode: 104 }
                    ] },
                    { activatetoolbar: 'table' },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 103 },
                        { keyCode: 66, charCode: 104 },
                        'TAB',
                        { keyCode: 67, charCode: 105 }
                    ] }
                ]
            },
            CLIENT_2: {
                orders: [
                    { keyboardevents: ['1', 'TAB', '2', 'TAB', '3', 'TAB', '4', '5', 'TAB', '6', '7', '8'] },
                    { activatetoolbar: 'insert' },
                    { buttonclick: { dataKey: 'textframe/insert' } },
                    { keyboardevents: ['a', 'b', 'c'] },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: 'RIGHT_ARROW DOWN_ARROW ENTER' },
                    { timeout: 100 },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 100 },
                        { keyCode: 66, charCode: 101 },
                        { keyCode: 67, charCode: 102 },
                        'TAB',
                        { keyCode: 68, charCode: 103 },
                        { keyCode: 69, charCode: 104 }
                    ] },
                    { activatetoolbar: 'table' },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { keyboardevents: [ // these keyboard events are inside the table without setting the selecion explicitely
                        { keyCode: 65, charCode: 103 },
                        { keyCode: 66, charCode: 104 },
                        'TAB',
                        { keyCode: 67, charCode: 105 }
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // two clients. Testing setAttributes in combination with splitParagraph.
    TEST_14: {
        description: 'setAttributes and splitParagraph',
        tooltip: 'A two clients test for setAttributes and splitParagraph',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' }
                    ] },
                    { selection: { start: [0, 18], end: [0, 23] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 20], end: [0, 20] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 10,
                    commandDelay: 50
                },
                orders: [
                    { selection: { start: [0, 18], end: [0, 22] } },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } },
                    { selection: { start: [0, 12], end: [0, 16] } },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'italic', true] } },
                    { selection: { start: [0, 6], end: [0, 10] } },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'bold', true] } },
                    { selection: { start: [0, 1], end: [0, 4] } },
                    { functioncall: { functionname: 'setAttribute', params: ['character', 'italic', true] } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 10,
                    commandDelay: 20
                },
                orders: [
                    { selection: { start: [0, 20], end: [0, 20] } },
                    { keyboardevents: ['ENTER'] },
                    { selection: { start: [0, 14], end: [0, 14] } },
                    { keyboardevents: ['ENTER'] },
                    { selection: { start: [0, 8], end: [0, 8] } },
                    { keyboardevents: ['ENTER'] },
                    { selection: { start: [0, 3], end: [0, 3] } },
                    { keyboardevents: ['ENTER'] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // many clients, with commands.
    // Inserting much text into one single paragraph
    TEST_15: {
        description: 'insertText into single paragraph',
        tooltip: 'A two clients test for inserting content into one paragraph',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 20,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', 'PAGE_UP', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'PAGE_UP', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', 'PAGE_UP', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', 'PAGE_UP', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', 'PAGE_UP', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'PAGE_UP', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', 'PAGE_UP', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'PAGE_UP', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'PAGE_UP', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'PAGE_UP', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'SPACE'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 50,
                    commandDelay: 30
                    // arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } },
                    { keyboardevents: [
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'PAGE_UP', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'PAGE_UP', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE',
                        'A', 'B', 'C', 'D', 'PAGE_UP', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'PAGE_UP', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'PAGE_UP', 'F', 'G', 'H', 'I', 'J'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedParagraphCount: 23
            },
            CLIENT_2: {
                // expectedParagraphCount: 23
            }
        }
    },

    TEST_15A: {
        description: 'insertText into single paragraph',
        tooltip: 'Several clients inserting text content into one paragraph',
        clients: 5,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: '+' }
                    ] },
                    { selection: { start: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                orders: [] // watcher
            },
            CLIENT_2: {
                orders: [
                    { keyboardevents: ['a b c d e f g h i j k l m'] }
                ]
            },
            CLIENT_3: {
                orders: [
                    { keyboardevents: ['n o p q r s t u v w x y z'] }
                ]
            },
            CLIENT_4: {
                orders: [
                    { keyboardevents: ['SHIFT+a SHIFT+b SHIFT+c SHIFT+d SHIFT+e SHIFT+f SHIFT+g SHIFT+h SHIFT+i SHIFT+j SHIFT+k SHIFT+l SHIFT+m'] }
                ]
            },
            CLIENT_5: {
                orders: [
                    { keyboardevents: ['1 2 3 4 5 6 7 8 9 0'] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // single client, can be used as partner working at the same document (very long)
    TEST_16: {
        description: 'Single user partner test (1400 OPs)',
        tooltip: 'A single client insert content into a document for a long time',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABCDEF' },
                        { name: Op.PARA_INSERT, start: [1] },
                        { name: Op.TEXT_INSERT, start: [1, 0], text: 'GHIJKL' },
                        { name: Op.PARA_INSERT, start: [2] },
                        { name: Op.TEXT_INSERT, start: [2, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', '4', 'ENTER', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', '4', 'ENTER', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', '4', 'ENTER', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', '4', 'ENTER', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', '4', 'ENTER', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'ENTER',
                        '0', '1', '2', '3', '4', 'ENTER', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedParagraphCount: 23
            }
        }
    },

    // single client, can be used as partner working at the same document
    TEST_17: {
        description: 'Single user partner test (200 OPs, 30 sec.)',
        tooltip: 'A single client insert content into a document for half a minute',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'Automated Test:' }
                    ] },
                    { selection: { start: [0, 15], end: [0, 15] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 90,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        'ENTER SHIFT+B u t SPACE SHIFT+I SPACE m u s t SPACE e x p l a i n SPACE t o SPACE y o u SPACE h o w SPACE a l l SPACE t h i s SPACE m i s t a k e n SPACE i d e a SPACE',
                        'o f ENTER ENTER d e n o u n c i n g SPACE p l e a s u r e SPACE a n d SPACE p r a i s i n g SPACE p a i n SPACE w a s SPACE b o r n EMTER ENTER a n d SPACE SHIFT+I SPACE w i l l SPACE',
                        'g i v e SPACE y o u SPACE a SPACE c o m p l e t e SPACE a c c o u n t SPACE o f SPACE t h e SPACE s y s t e m SPACE a n d SPACE e x p o u n d SPACE t h e SPACE',
                        'a c t u a l SPACE t e a c h i n g s SPACE o f SPACE t h e ENTER ENTER g r e a t SPACE e x p l o r e r SPACE o f SPACE t h e SPACE t r u t h SPACE t h e ENTER ENTER',
                        'm a s t e r SPACE b u i l d e r SPACE o f SPACE h u m a n SPACE h a p p i n e s s ENTER ENTER SHIFT+T e s t SPACE d o n e'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedParagraphCount: 23
            }
        }
    },

    // single client, can be used as partner working at the same document
    TEST_17A: {
        description: 'Single user partner test (200 OPs, 45 sec.)',
        tooltip: 'A single client insert content into a document for a long time',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABCDEF' },
                        { name: Op.PARA_INSERT, start: [1] },
                        { name: Op.TEXT_INSERT, start: [1, 0], text: 'GHIJKL' },
                        { name: Op.PARA_INSERT, start: [2] },
                        { name: Op.TEXT_INSERT, start: [2, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedParagraphCount: 23
            }
        }
    },

    // single client, can be used as partner working at the same document
    TEST_17B: {
        description: 'Single user partner test with ENTER (scroll test)',
        tooltip: 'A single client inserts many returns, so that the document scrolls permanently',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABCDEF' },
                        { name: Op.PARA_INSERT, start: [1] },
                        { name: Op.TEXT_INSERT, start: [1, 0], text: 'GHIJKL' },
                        { name: Op.PARA_INSERT, start: [2] },
                        { name: Op.TEXT_INSERT, start: [2, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER',
                        '0', '1', 'ENTER', '2', '3', 'ENTER', '4', '5', 'ENTER', '6', '7', 'ENTER', '8', '9', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
            }
        }
    },

    // two clients.
    // Testing splitParagraph, mergeParagraph and inserting text.
    TEST_18: {
        description: 'splitParagraph, mergeParagraph and inserting text',
        tooltip: 'A two clients test for insert content and split/merge/delete paragraph',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' }
                    ] },
                    { selection: { start: [0, 18], end: [0, 23] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 20], end: [0, 20] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 2000, // more time for preparation of client 2
                    commandDelay: 20
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE', '0', '1',
                        'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE',
                        'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9',
                        'SPACE', '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER',
                        '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE',
                        '7', '8', '9', 'SPACE', '0', '1', '2', 'ENTER', '3', '4', '5', '6', 'BACKSPACE',
                        'ENTER', '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE',
                        '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE',
                        '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE',
                        '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE',
                        '7', '8', '9', 'SPACE', '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE',
                        '7', 'ENTER', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: [
                        '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedSelection: { start: [0, 13, 1, 2, 1, 0, 1], end: [0, 13, 1, 2, 1, 0, 1] },
                // expectedTextContent: ''
            }
        }
    },

    // Testing splitParagraph, mergeParagraph and inserting text.
    TEST_18A: {
        description: 'splitParagraph, mergeParagraph and inserting text from three clients',
        tooltip: 'A three clients test for insert content and split/merge/delete paragraph',
        clients: 3,
        autotest: true,
        idleTime: 10000,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' }
                    ] },
                    { selection: { start: [0, 18], end: [0, 23] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 20], end: [0, 20] } }
                ]
            },
            CLIENT_3: {
                orders: [
                    { selection: { start: [0, 22], end: [0, 22] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 0,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 0,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // Testing splitParagraph, mergeParagraph and inserting text.
    TEST_18B: {
        description: 'splitParagraph, mergeParagraph and inserting text from three clients (faster than 18A)',
        tooltip: 'A three clients test for insert content and split/merge/delete paragraph',
        clients: 3,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' }
                    ] },
                    { selection: { start: [0, 18], end: [0, 23] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 20], end: [0, 20] } }
                ]
            },
            CLIENT_3: {
                orders: [
                    { selection: { start: [0, 22], end: [0, 22] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 10,
                    commandDelay: 20
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '0', '1', '2', '3', '4', '5', '6', '7'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 10,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: [
                        '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '8',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 10,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: [
                        '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9', '9',
                        'ENTER', 'ENTER', 'ENTER', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_19: {
        description: 'Multi user undo/redo test for splitParagraph',
        tooltip: 'A multi client test demonstrating the undo/redo functionality for the splitParagraph operation',
        clients: 2,
        autotest: true,
        idleTime: 10000,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'Hello World' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200, // currently only used for keyboard events
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: ['ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 250, // currently only used for keyboard events
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: ['ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
            },
            CLIENT_2: {
            }
        }
    },

    TEST_19A: {
        description: 'Multi user undo/redo test for splitParagraph (short version)',
        tooltip: 'A multi client test demonstrating the undo/redo functionality for the splitParagraph operation (short version)',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'Hello World' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200, // currently only used for keyboard events
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: ['ENTER', 'ENTER', 'ENTER'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 250, // currently only used for keyboard events
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: ['ENTER', 'ENTER', 'ENTER'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedSelection: { start: [0, 5], end: [0, 5] },
                expectedTextContent: 'Hello World',
                expectedParagraphCount: 1
            },
            CLIENT_2: {
                // expectedSelection: { start: [0, 0], end: [0, 0] },
                expectedTextContent: 'Hello World',
                expectedParagraphCount: 1
            }
        }
    },

    TEST_19B: {
        description: 'Multi user undo/redo test for splitParagraph and cut in UndoActions',
        tooltip: 'A multi client test demonstrating the undo/redo functionality for the splitParagraph operation and cutting the UndoActions by a local insertText operation',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'Hello World' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200, // currently only used for keyboard events
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: ['ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['1'] }, // cutting the undo stack, disabling redo (6 undos remain)
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' }, // 6 redos available
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['2'] }, // cutting the undo stack, disabling redo (5 undos remain)
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 250, // currently only used for keyboard events
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: ['ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['3'] }, // cutting the undo stack, disabling redo (4 undos remain)
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['4'] }, // cutting the undo stack, disabling redo (3 undos remain)
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedSelection: { start: [0, 5], end: [0, 5] },
                expectedTextContent: 'Hello World',
                expectedParagraphCount: 1
            },
            CLIENT_2: {
                // expectedSelection: { start: [0, 0], end: [0, 0] },
                expectedTextContent: 'Hello World',
                expectedParagraphCount: 1
            }
        }
    },

    TEST_20: {
        description: 'Multi user undo/redo test for splitParagraph',
        tooltip: 'A multi client test demonstrating the undo/redo functionality for the splitParagraph operation with arbitrary undo/redo order',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'Hello World' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1200,
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: ['ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 250, // currently only used for keyboard events
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: ['ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER', 'ENTER'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedSelection: { start: [0, 5], end: [0, 5] },
                expectedTextContent: 'Hello World',
                expectedParagraphCount: 1
            },
            CLIENT_2: {
                // expectedSelection: { start: [0, 0], end: [0, 0] },
                expectedTextContent: 'Hello World',
                expectedParagraphCount: 1
            }
        }
    },

    // Inserting much text into one single paragraph and using undo and redo(Info: undo requires spaces because of grouping of operations)
    TEST_21: {
        description: 'insertText into single paragraph together with undo and redo',
        tooltip: 'A two clients test for inserting content into one paragraph and using undo and redo',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1500,
                    commandDelay: 20,
                    arbitraryTestStartDelayMax: 1500
                },
                orders: [
                    { keyboardevents: ['0', '1', '2', 'SPACE', '3', '4', '5', '6', 'SPACE', '7', '8', '9', '0'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['1', '2', 'SPACE', '3', '4'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['5', '6', 'SPACE', '7', '8', '9', '0'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['1', '2', 'SPACE', '3', '4', '5', '6', '7', '8', 'PAGE_UP', '9', '0', '1', '2', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['6', '7', '8', '9', '0', 'SPACE', '1', '2', '3', '4', '5', 'SPACE', '6', 'PAGE_UP', '7', '8', '9', '0', '1', '2', '3'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['4', '5', '6', 'SPACE', '7', '8', '9', '0', '1', '2', '3', 'SPACE', '4', '5', '6', '7'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['8', '9', '0', '1', '2', 'SPACE', '3', '4', '5', '6', '7', 'SPACE', '8', '9', '0'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['1', '2', '3', '4', '5', '6', 'SPACE', '7', '8', '9', '0', 'SPACE', '1', '2', '3', '4', 'SPACE', '5', '6', '7', 'PAGE_UP', '8', '9', '0', '1', '2', '3'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['4', '5', '6', '7', 'SPACE', '8', '9', '0', '1', '2', '3', 'SPACE', '4', '5', '6', '7', '8', '9'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 250,
                    commandDelay: 30,
                    arbitraryTestStartDelayMax: 250
                },
                orders: [
                    { keyboardevents: ['A', 'B', 'C', 'SPACE', 'D', 'E', 'F', 'G', 'H', 'SPACE', 'I', 'J', 'A', 'B', 'C'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['D', 'E', 'F', 'G', 'SPACE', 'H', 'I', 'J', 'PAGE_UP', 'A', 'B', 'C', 'D', 'SPACE', 'E', 'F'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['G', 'H', 'I', 'J', 'A', 'SPACE', 'B', 'C', 'D', 'E', 'F', 'G', 'SPACE', 'H', 'I', 'J', 'A', 'B', 'SPACE', 'C', 'D', 'E', 'F', 'G'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['H', 'I', 'PAGE_UP', 'J', 'A', 'B', 'C', 'SPACE', 'D', 'E', 'F', 'G', 'H', 'I', 'SPACE', 'J', 'A', 'B', 'C', 'D', 'E', 'F', 'G'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['H', 'I', 'J', 'A', 'B', 'C', 'D', 'SPACE', 'E', 'F', 'G', 'H', 'I', 'SPACE', 'J', 'A', 'B', 'C', 'D', 'PAGE_UP', 'E', 'F', 'G', 'H'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['I', 'J', 'A', 'B', 'C', 'D', 'E', 'SPACE', 'F', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'SPACE', 'F', 'G', 'H', 'I'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['J', 'A', 'B', 'SPACE', 'C', 'D', 'E', 'F', 'SPACE', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'SPACE', 'F', 'G', 'H', 'I'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' },
                    { keyboardevents: ['J', 'A', 'B', 'SPACE', 'C', 'D', 'E', 'F', 'SPACE', 'G', 'H', 'I', 'J', 'SPACE', 'A', 'B', 'C', 'D', 'E', 'SPACE', 'F', 'G', 'H', 'I'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { buttonclick: 'redo' }
                ]
            }
        },
        result: {
        }
    },

    // Testing the grouping of insertText operations for undo and redo
    TEST_22: {
        description: 'insertText into single paragraph and pressing undo must delete all the text',
        tooltip: 'A single client test for inserting text and using undo to delete all inserted text',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'test' }
                    ] },
                    { selection: { start: [0, 4], end: [0, 4] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: ['SPACE', '0', '1', '2', '3', '4'] },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedTextContent: 'test ', // not modified
                expectedParagraphCount: 1
            }
        }
    },

    // Testing the grouping of insertText operations for undo and redo with several words
    TEST_23: {
        description: 'insertText into single paragraph and pressing undo must delete the last word',
        tooltip: 'A single client test for inserting text and using undo to delete the last word of the inserted text',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'test' }
                    ] },
                    { selection: { start: [0, 2], end: [0, 2] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '3', '4', '5', '6', 'SPACE'] },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedTextContent: 'te 012 st', // second word '3456' removed
                expectedParagraphCount: 1
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    // Test with 3 different client definitions
    TEST_24: {
        description: 'insertText into single paragraph from three clients',
        tooltip: 'A three client test for inserting text and using undo to delete the last word of the inserted text',
        clients: 3,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'test' }
                    ] },
                    { selection: { start: [0, 4], end: [0, 4] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 2], end: [0, 2] } }
                ]
            },
            CLIENT_3: {
                orders: [
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: ['SPACE', '6', '7', '8', 'SPACE', '6', '7', '8'] },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    // Test with 3 different client definitions
    TEST_25: {
        description: 'insertText into three paragraphs from three clients',
        tooltip: 'A three client test for inserting text and using undo to delete the last word of the inserted text',
        clients: 3,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'test A' },
                        { name: Op.PARA_INSERT, start: [1] },
                        { name: Op.TEXT_INSERT, start: [1, 0], text: 'test B' },
                        { name: Op.PARA_INSERT, start: [2] },
                        { name: Op.TEXT_INSERT, start: [2, 0], text: 'test C' }
                    ] },
                    { selection: { start: [0, 4], end: [0, 4] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [1, 2], end: [1, 2] } }
                ]
            },
            CLIENT_3: {
                orders: [
                    { selection: { start: [2, 0], end: [2, 2] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 10,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 10,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: ['SPACE', '6', '7', '8', 'SPACE', '6', '7', '8'] },
                    { buttonclick: 'undo' }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    // Testing splitParagraph, mergeParagraph and inserting text.
    TEST_26: {
        description: 'splitParagraph, mergeParagraph and inserting text for three clients',
        tooltip: 'A three clients test for insert content and split/merge/delete paragraph',
        clients: 3,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABCDEF' },
                        { name: Op.PARA_INSERT, start: [1] },
                        { name: Op.TEXT_INSERT, start: [1, 0], text: 'GHIJKL' },
                        { name: Op.PARA_INSERT, start: [2] },
                        { name: Op.TEXT_INSERT, start: [2, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [2, 2], end: [2, 2] } },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { keyboardevents: ['8'] }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [1, 2], end: [1, 2] } },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { keyboardevents: ['7'] }
                ]
            },
            CLIENT_3: {
                orders: [
                    { selection: { start: [0, 2], end: [0, 2] } },
                    { buttonclick: { dataKey: 'character/underline' } },
                    { keyboardevents: ['6'] }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 20
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', 'ENTER', '3', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', '9', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 10,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', 'ENTER', '3', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', '9', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 10,
                    commandDelay: 10
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'ENTER', '2', '3', 'ENTER', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', 'ENTER', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', 'ENTER', '3', '4', '5', '6', 'BACKSPACE', 'ENTER', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', '8', '9', 'SPACE',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', '7', 'ENTER', '8', '9', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // expectedTextContent: ''
            }
        }
    },

    TEST_27: {
        description: 'insertText into header',
        tooltip: 'A two clients test for inserting text into the header',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'test A' },
                        { name: Op.PARA_INSERT, start: [1] },
                        { name: Op.TEXT_INSERT, start: [1, 0], text: 'test B' }
                    ] },
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { timeout: 1000 }, // enabling insertion of header an paragraph
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 0], target: 'ACTIVE_TARGET', text: 'Text in header' } // inserting text into header
                    ] },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'PAGE' } } },
                    { timeout: 500 }, // enabling insertion of header an paragraph
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } },
                    { selection: { start: [0, 4], end: [0, 4] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } },
                    { selection: { start: [0, 2], end: [0, 2] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1500,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2', 'SPACE'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { buttonclick: 'undo' },
                    { triggerevent: { type: 'nodeClick', definition: { target: 'PAGE' } } },
                    { selection: { start: [0, 2], end: [0, 2] } },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2', 'SPACE'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { selection: { start: [0, 4], end: [0, 4] } },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2', 'SPACE'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } }, // leaving header/footer edit mode
                    { selection: { start: [0, 2], end: [0, 2] } },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { buttonclick: 'undo' },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2', 'SPACE'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 500,
                    commandDelay: 60
                },
                orders: [
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } }, // leaving header/footer edit mode
                    { selection: { start: [0, 3], end: [0, 3] } },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { selection: { start: [0, 3], end: [0, 3] } },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } }, // leaving header/footer edit mode
                    { selection: { start: [0, 3], end: [0, 3] } },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { buttonclick: 'undo' },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    TEST_28: {
        description: 'insertText into header',
        tooltip: 'A two clients test for inserting text into the header',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'test A' },
                        { name: Op.PARA_INSERT, start: [1] },
                        { name: Op.TEXT_INSERT, start: [1, 0], text: 'test B' }
                    ] },
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { timeout: 1000 }, // enabling insertion of header an paragraph
                    { operations: [
                        { name: Op.TEXT_INSERT, start: [0, 0], target: 'ACTIVE_TARGET', text: 'Text in header' } // inserting text into header
                    ] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } }, // leaving header/footer edit mode
                    { timeout: 500 }, // enabling insertion of header an paragraph
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } },
                    { timeout: 500 },
                    { selection: { start: [0, 10], end: [0, 10] } }
                ]

            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } },
                    { selection: { start: [0, 2], end: [0, 2] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2', 'SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } }, // leaving header/footer edit mode
                    { selection: { start: [0, 2], end: [0, 2] } },
                    { keyboardevents: ['SPACE', '0', '1', '2', 'SPACE', '0', '1', '2', 'SPACE', '0', '1', '2', 'SPACE', '0', '1', '2'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 60
                },
                orders: [
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5', 'SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } }, // leaving header/footer edit mode
                    { selection: { start: [0, 3], end: [0, 3] } },
                    { keyboardevents: ['SPACE', '3', '4', '5', 'SPACE', '3', '4', '5', 'SPACE', '3', '4', '5', 'SPACE', '3', '4', '5'] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    TEST_29: {
        description: 'insertText into header',
        tooltip: 'A two clients test for inserting text into the header',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'test A' },
                        { name: Op.PARA_INSERT, start: [1] },
                        { name: Op.TEXT_INSERT, start: [1, 0], text: 'test B' }
                    ] },
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { timeout: 1000 }, // enabling insertion of header an paragraph
                    { operations: [{ name: Op.TEXT_INSERT, start: [0, 0], target: 'ACTIVE_TARGET', text: 'Text in header' }] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } } // leaving header/footer edit mode
                ]
            },
            CLIENT_2: {
                orders: [
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } },
                    { selection: { start: [0, 2], end: [0, 2] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 50
                },
                orders: [
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { selection: { start: [0, 10], end: [0, 10] } },
                    { keyboardevents: [
                        'q', { key: 'q', shift: true }, 'SPACE', '0', '1', '2', 'SPACE', '3', '4', '5', 'SPACE', '6', '7', '8', 'SPACE', '9',
                        { key: 'a', shift: true }, { key: 'b', shift: true }, 'SPACE', { key: 'c', shift: true }, { key: 'd', shift: true }, { key: 'e', shift: true }
                    ] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } }, // leaving header/footer edit mode
                    { selection: { start: [0, 2], end: [0, 2] } },
                    { keyboardevents: ['0'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 60
                },
                orders: [
                    { keyboardevents: ['z'] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } }, // leaving header/footer edit mode
                    { selection: { start: [0, 3], end: [0, 3] } },
                    { keyboardevents: ['z'] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    // Inserting text, splitting and merging paragraphs
    TEST_30: {
        description: 'Inserting text, splitting and merging paragraphs',
        tooltip: 'A three clients test, no reload',
        clients: 3,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            },
            CLIENT_3: {
                orders: [
                    { selection: { start: [0, 3], end: [0, 3] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', 'ENTER', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', 'ENTER', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', 'ENTER', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'g', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'ENTER', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'ENTER', 'ENTER', 'j',
                        'a', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'ENTER', 'ENTER', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'
                    ] }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true }, 'ENTER',
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, 'ENTER', { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, 'ENTER', { key: 'J', shift: true }, 'BACKSPACE', 'BACKSPACE', 'BACKSPACE',
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, 'BACKSPACE', 'BACKSPACE', { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, 'ENTER', { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, 'BACKSPACE', 'BACKSPACE', { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, 'ENTER', 'BACKSPACE', { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true }
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
            },
            CLIENT_2: {
            }
        }
    },

    // 3 specified clients, client 1 reloads the document at the end of the test.
    // Inserting text, splitting and merging paragraphs
    TEST_31: {
        description: 'Inserting text, splitting and merging paragraphs and document reload at end of test',
        tooltip: 'A three clients test, client 1 reloads the document at the end of the test',
        clients: 3,
        autotest: false, // reload
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            },
            CLIENT_3: {
                orders: [
                    { selection: { start: [0, 3], end: [0, 3] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', 'ENTER', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', 'ENTER', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9'
                    ] },
                    // reloading the document at the end of the test; waiting with reload to make sure other clients have finished the test
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'g', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'ENTER', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'ENTER', 'ENTER', 'j',
                        'a', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'ENTER', 'ENTER', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'ENTER', 'ENTER', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'
                    ] }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true }, 'ENTER',
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, 'ENTER', { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, 'ENTER', { key: 'J', shift: true }, 'BACKSPACE', 'BACKSPACE', 'BACKSPACE',
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, 'BACKSPACE', 'BACKSPACE', { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, 'ENTER', { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, 'BACKSPACE', 'BACKSPACE', { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, 'ENTER', 'BACKSPACE', { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true }
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
            },
            CLIENT_2: {
            }
        }
    },

    // 3 specified clients, client 1 reloads the document in the middle of the test.
    // Inserting text, splitting and merging paragraphs
    TEST_32: {
        description: 'Inserting text, splitting and merging paragraphs and document reload in the middle',
        tooltip: 'A three clients test, client 1 reloads the document in the middle of the test',
        clients: 3,
        autotest: false, // reload
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            },
            CLIENT_3: {
                orders: [
                    { selection: { start: [0, 3], end: [0, 3] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', 'ENTER', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', 'ENTER', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', 'ENTER', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'g', 'h', 'i', 'j', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'ENTER', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'ENTER', 'ENTER', 'j',
                        'a', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'ENTER', 'ENTER', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'
                    ] }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true }, 'ENTER',
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, 'ENTER', { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, 'ENTER', { key: 'J', shift: true }, 'BACKSPACE', 'BACKSPACE', 'BACKSPACE',
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, 'BACKSPACE', 'BACKSPACE', { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, 'ENTER', { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, 'BACKSPACE', 'BACKSPACE', { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, 'ENTER', 'BACKSPACE', { key: 'B', shift: true }, { key: 'C', shift: true }, { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true },
                        { key: 'A', shift: true }, { key: 'B', shift: true }, { key: 'C', shift: true }, 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', { key: 'D', shift: true }, { key: 'E', shift: true },
                        { key: 'F', shift: true }, { key: 'G', shift: true }, { key: 'H', shift: true }, { key: 'I', shift: true }, { key: 'J', shift: true }
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
            },
            CLIENT_2: {
            }
        }
    },

    // 2 specified clients, client 1 reloads the document three times in the middle of the test.
    // Inserting text, splitting and merging paragraphs
    TEST_33: {
        description: 'Inserting text, splitting and merging paragraphs and 3 document reloads in the middle',
        tooltip: 'A two clients test, client 1 reloads the document in the middle of the test three times',
        clients: 2,
        autotest: false, // reload
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', 'ENTER', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
                    { keyboardevents: [
                        '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', 'ENTER', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '9', '0'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'G', 'H', 'I', 'J', 'ENTER',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'ENTER', 'ENTER',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'ENTER', 'ENTER', 'J',
                        'A', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'ENTER', 'ENTER', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'ENTER', 'ENTER',
                        'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'ENTER', 'ENTER',
                        'A', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'ENTER', 'ENTER', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'ENTER', 'ENTER',
                        'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
            },
            CLIENT_2: {
            }
        }
    },

    // 2 specified clients, all clients reload the document in the middle of the test several times.
    // Inserting text, splitting and merging paragraphs
    TEST_34: {
        description: 'Inserting text, splitting and merging paragraphs and several document reloads of all clients',
        tooltip: 'A two clients test, all clients reload the document in the middle of the test several times',
        clients: 2,
        autotest: false, // reload
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 1], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', 'ENTER', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', '9',
                        '0', '1', '2', '3'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } },
                    { keyboardevents: [
                        '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', 'ENTER', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE',
                        '3', '4', '5', '6', '7', '8', 'ENTER', '9', '0', '1', '2'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } },
                    { keyboardevents: [
                        '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', 'ENTER', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE',
                        '3', '4', '5', '6', '7', '8', 'ENTER', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', '8', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'ENTER', '7', 'ENTER', '8', '9',
                        '0', '1', '2', '3', '4', '5', '6', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 950,
                    commandDelay: 100,
                    arbitraryTestStartDelayMax: 500
                },
                orders: [
                    { keyboardevents: [
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'G', 'H', 'I', 'J',
                        'ENTER', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'ENTER', 'ENTER',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } },
                    { keyboardevents: [
                        'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'ENTER', 'ENTER', 'J',
                        'A', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'BACKSPACE', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'ENTER', 'ENTER', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } },
                    { keyboardevents: [
                        'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'ENTER', 'ENTER',
                        'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
            },
            CLIENT_2: {
            }
        }
    },

    // Testing long running process like pasting for several clients
    TEST_35: {
        description: 'Copy paste of a long document together with an active client',
        tooltip: 'Testing long running process like pasting in combination with an active client',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: COPY_PASTE_TEMPLATE_OPERATIONS },
                    // { keyboardevents: [
                    //     { key: 'A', ctrlKey: true }, // selecting the complete content
                    //     { key: 'C', ctrlKey: true } // copying content into clipboard
                    // ] },
                    { selection: { start: [1, 0], end: [1, 0] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [4, 0], end: [4, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] }, // selecting the complete content
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 3000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 3000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 3000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } }
                    // { keyboardevents: [
                    //     { key: 'V', ctrlKey: true }, // pasting the complete content
                    // ] },
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: [
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    // Testing long running process like pasting from several clients
    TEST_36: {
        description: 'Copy paste of a long document from several clients',
        tooltip: 'Testing long running process like pasting from several clients (a very long document is generated)',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: COPY_PASTE_TEMPLATE_OPERATIONS },
                    // { keyboardevents: [
                    //     { key: 'A', ctrlKey: true }, // selecting the complete content
                    //     { key: 'C', ctrlKey: true } // copying content into clipboard
                    // ] },
                    { selection: { start: [1, 0], end: [1, 0] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [4, 0], end: [4, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] }, // selecting the complete content
                    // { functioncall: { functionname: 'selectAll' } }, // alternative way for selecting the complete content
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 5000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 5000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } }
                    // { keyboardevents: [
                    //     { key: 'V', ctrlKey: true }, // pasting the complete content
                    // ] },
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [4, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[4, 0]] } },
                    { timeout: 5000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [4, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[4, 0]] } },
                    { timeout: 5000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [4, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[4, 0]] } }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    // Testing long running asynchronous process like accepting change tracks with an active client
    TEST_37: {
        description: 'Accepting a lot of change tracks while another client is active (simplified)',
        tooltip: 'Testing generating and applying of operations with change track while a second client modifies the document',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: COPY_PASTE_TEMPLATE_OPERATIONS },
                    { selection: { start: [1, 0], end: [1, 0] } },
                    { activatetoolbar: 'review' },
                    { buttonclick: { dataKey: 'toggleChangeTracking' } },
                    { timeout: 1000 }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [4, 0], end: [4, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] }, // selecting the complete content
                    // { functioncall: { functionname: 'selectAll' } }, // alternative way for selecting the complete content
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 3000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 3000 },
                    { buttonclick: { dataKey: 'toggleChangeTracking' } },
                    { timeout: 1000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] }, // selecting the complete content before accepting it
                    { buttonclick: { dataKey: 'acceptMoveSelectedChangeTracking', event: 'changetrack:resolve:after' } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0], end: [1, 0] } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: [
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    // Testing long running asynchronous process like accepting change tracks with an active client
    TEST_37A: {
        description: 'Accepting a lot of change tracks while another client is active',
        tooltip: 'Testing generating and applying of operations with change track while a second client modifies the document',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: COPY_PASTE_TEMPLATE_OPERATIONS },
                    { selection: { start: [1, 0], end: [1, 0] } },
                    { activatetoolbar: 'review' },
                    { buttonclick: { dataKey: 'toggleChangeTracking' } },
                    { timeout: 1000 }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [4, 0], end: [4, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] }, // selecting the complete content
                    // { functioncall: { functionname: 'selectAll' } }, // alternative way for selecting the complete content
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 3000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 3000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 3000 },
                    // { activatetoolbar: 'review' },
                    { buttonclick: { dataKey: 'toggleChangeTracking' } },
                    { timeout: 1000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] }, // selecting the complete content before accepting it
                    { buttonclick: { dataKey: 'acceptMoveSelectedChangeTracking', event: 'changetrack:resolve:after' } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0], end: [1, 0] } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: [
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    // Testing long running asynchronous process like rejecting change tracks with an active client
    TEST_38: {
        description: 'Rejecting a lot of change tracks while another client is active',
        tooltip: 'Testing generating and applying of operations with change track while a second client modifies the document',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: COPY_PASTE_TEMPLATE_OPERATIONS },
                    { selection: { start: [1, 0], end: [1, 0] } },
                    { activatetoolbar: 'review' },
                    { buttonclick: { dataKey: 'toggleChangeTracking' } },
                    { timeout: 1000 }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [4, 0], end: [4, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] }, // selecting the complete content
                    // { functioncall: { functionname: 'selectAll' } }, // alternative way for selecting the complete content
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 3000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 3000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] },
                    { functioncall: { functionname: 'copy', params: [null, { forceOperationGeneration: true }] } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0] } },
                    { functioncall: { functionname: 'pasteInternalClipboard', params: [[1, 0]] } },
                    { timeout: 3000 },
                    // { activatetoolbar: 'review' },
                    { buttonclick: { dataKey: 'toggleChangeTracking' } },
                    { timeout: 1000 },
                    { keyboardevents: [{ key: 'A', ctrlKey: true }] }, // selecting the complete content before accepting it
                    { buttonclick: { dataKey: 'rejectMoveSelectedChangeTracking', event: 'changetrack:resolve:after' } },
                    { timeout: 1000 },
                    { selection: { start: [1, 0], end: [1, 0] } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: [
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER',
                        '0 1 ENTER 2 3 ENTER 4 5 ENTER 6 7 ENTER 8 9 ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                // check: undo button is enabled (already because of the preparation operations)
            }
        }
    },

    // OT session bug: loading document when one client has NO pagenumber in header and types content into header.
    TEST_39: {
        description: 'no complex field in header and reloading document',
        tooltip: 'Loading a document when one client has no complex field in header and types content into header',
        clients: 2,
        autotest: false, // reload
        preparation: {
            CLIENT_1: {}
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { timeout: 1000 },
                    { keyboardevents: [
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d',
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d',
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d',
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d',
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d',
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d',
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d',
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d',
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d',
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d',
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d',
                        'SPACE n o SPACE c o m p l e x SPACE f i e l d'
                    ] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } } // leaving header/footer edit mode
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 200
                },
                orders: [
                    { timeout: 1000 },
                    { keyboardevents: ['a', 'b', 'ENTER', 'c'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
                    { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
                    { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
                    { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
                    { keyboardevents: ['m', 'n', 'ENTER', 'o'] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // OT session bug: loading document when one client has pagenumber in header and types content into header.
    TEST_39A: {
        description: 'pagenumber field in header and reloading document',
        tooltip: 'Loading a document when one client has pagenumber field in header and types content into header',
        clients: 2,
        autotest: false, // reload
        preparation: {
            CLIENT_1: {}
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { timeout: 1000 },
                    { functioncall: { functionname: 'dispatchInsertField', object: 'fieldmanager', params: ['page', 'default'] } },
                    { timeout: 500 },
                    { selection: { start: [0, 4], end: [0, 4] } },
                    { keyboardevents: [
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd',
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd',
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd',
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd',
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd',
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd',
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd',
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd',
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd',
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd',
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd',
                        'SPACE', 'c', 'o', 'm', 'p', 'l', 'e', 'x', 'SPACE', 'f', 'i', 'e', 'l', 'd'
                    ] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } } // leaving header/footer edit mode
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 200
                },
                orders: [
                    { timeout: 1000 },
                    { keyboardevents: ['a', 'b', 'ENTER', 'c'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
                    { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
                    { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
                    { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
                    { keyboardevents: ['m', 'n', 'ENTER', 'o'] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // OT session bug: loading document when one client has the author name in header and types content into header.
    TEST_39B: {
        description: 'author field in header and reloading document',
        tooltip: 'Loading a document when one client has author field in header and types content into header',
        clients: 2,
        autotest: false, // reload
        preparation: {
            CLIENT_1: {}
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { timeout: 1000 },
                    { functioncall: { functionname: 'dispatchInsertField', object: 'fieldmanager', params: ['author', 'default'] } },
                    { timeout: 500 },
                    { keyboardevents: [
                        'END', // setting cursor behind author name
                        'SPACE', 'a', 'u', 't', 'h', 'o', 'r', 'SPACE', 'n', 'a', 'm', 'e',
                        'SPACE', 'a', 'u', 't', 'h', 'o', 'r', 'SPACE', 'n', 'a', 'm', 'e',
                        'SPACE', 'a', 'u', 't', 'h', 'o', 'r', 'SPACE', 'n', 'a', 'm', 'e',
                        'SPACE', 'a', 'u', 't', 'h', 'o', 'r', 'SPACE', 'n', 'a', 'm', 'e',
                        'SPACE', 'a', 'u', 't', 'h', 'o', 'r', 'SPACE', 'n', 'a', 'm', 'e',
                        'SPACE', 'a', 'u', 't', 'h', 'o', 'r', 'SPACE', 'n', 'a', 'm', 'e',
                        'SPACE', 'a', 'u', 't', 'h', 'o', 'r', 'SPACE', 'n', 'a', 'm', 'e',
                        'SPACE', 'a', 'u', 't', 'h', 'o', 'r', 'SPACE', 'n', 'a', 'm', 'e',
                        'SPACE', 'a', 'u', 't', 'h', 'o', 'r', 'SPACE', 'n', 'a', 'm', 'e',
                        'SPACE', 'a', 'u', 't', 'h', 'o', 'r', 'SPACE', 'n', 'a', 'm', 'e'
                    ] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } } // leaving header/footer edit mode
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 200
                },
                orders: [
                    { timeout: 1000 },
                    { keyboardevents: ['a', 'b', 'ENTER', 'c'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
                    { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
                    { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
                    { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
                    { keyboardevents: ['m', 'n', 'ENTER', 'o'] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // OT session bug: loading document when one client has the date/time field in header and types content into header.
    TEST_39C: {
        description: 'date/time field in header and reloading document',
        tooltip: 'Loading a document when one client has date/time field in header and types content into header',
        clients: 2,
        autotest: false, // reload
        preparation: {
            CLIENT_1: {}
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { timeout: 1000 },
                    { functioncall: { functionname: 'dispatchInsertField', object: 'fieldmanager', params: ['date', 'default'] } },
                    { timeout: 500 },
                    { keyboardevents: [
                        'END', // setting cursor behind author name
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e'
                    ] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } } // leaving header/footer edit mode
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 200
                },
                orders: [
                    { timeout: 1000 },
                    { keyboardevents: ['a', 'b', 'ENTER', 'c'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
                    { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
                    { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
                    { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
                    { keyboardevents: ['m', 'n', 'ENTER', 'o'] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // OT session bug: loading document when one client has the date/time field in header and types content into header.
    TEST_39D: {
        description: 'date/time field with seconds in header and reloading document',
        tooltip: 'Loading a document when one client has date/time field with seconds in header and types content into header',
        clients: 2,
        autotest: false, // reload
        preparation: {
            CLIENT_1: {}
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 200
                },
                orders: [
                    { triggerevent: { type: 'dblclick', definition: { target: 'FIRST_HEADER' } } }, // activating header/footer edit mode
                    { timeout: 1000 },
                    { functioncall: { functionname: 'dispatchInsertField', object: 'fieldmanager', params: ['date', 'default'] } },
                    { timeout: 500 },
                    { selection: { start: [0, 3], end: [0, 3] } }, // setting selection inside field
                    { functioncall: { functionname: 'updateFieldFormatting', object: 'fieldmanager', params: ['MM/DD/YYYY hh:mm:ss'] } },
                    { timeout: 500 },
                    { keyboardevents: [
                        'END', // setting cursor behind author name
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e',
                        'SPACE', 'd', 'a', 't', 'e', 'SPACE', 't', 'i', 'm', 'e'
                    ] },
                    { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } } // leaving header/footer edit mode
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 200
                },
                orders: [
                    { timeout: 1000 },
                    { keyboardevents: ['a', 'b', 'ENTER', 'c'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
                    { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
                    { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
                    { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
                    { keyboardevents: ['m', 'n', 'ENTER', 'o'] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // OT session bug: loading document when one client is editing in the comment.
    // TEST_40: {
    //     description: 'editing a comment while reloading document',
    //     tooltip: 'Loading a document when one client is editing a comment',
    //     clients: 2,
    //     autotest: false, // reload
    //     preparation: {
    //         CLIENT_1: {
    //             orders: [
    //                 { activatetoolbar: 'insert' },
    //                 { selection: { start: [0, 1], end: [0, 5] } },
    //                 { buttonclick: { dataKey: 'comment/insert' } },
    //                 { timeout: 1000 } // specifying a timeout
    //             ]
    //         }
    //     },
    //     commands: {
    //         CLIENT_1: {
    //             timers: {
    //                 initialDelay: 1000,
    //                 commandDelay: 200
    //             },
    //             orders: [
    //                 { keyboardevents: [
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER'
    //                 ] },
    //                 { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } } // leaving comment
    //             ]
    //         },
    //         CLIENT_2: {
    //             timers: {
    //                 initialDelay: 2000,
    //                 commandDelay: 200
    //             },
    //             orders: [
    //                 { timeout: 1000 },
    //                 { keyboardevents: ['a', 'b', 'ENTER', 'c'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
    //                 { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
    //                 { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
    //                 { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
    //                 { keyboardevents: ['m', 'n', 'ENTER', 'o'] }
    //             ]
    //         }
    //     },
    //     result: {
    //         CLIENT_1: {}
    //     }
    // },

    // OT session bug: loading document when one client is editing in the comment.
    // TEST_41: {
    //     description: 'editing a comment while reloading document, very long version',
    //     tooltip: 'Loading a document when one client is editing a comment (many times)',
    //     clients: 2,
    //     autotest: false, // reload
    //     preparation: {
    //         CLIENT_1: {
    //             orders: [
    //                 { activatetoolbar: 'insert' },
    //                 { selection: { start: [0, 1], end: [0, 5] } },
    //                 { buttonclick: { dataKey: 'comment/insert' } },
    //                 { timeout: 1000 } // specifying a timeout
    //             ]
    //         }
    //     },
    //     commands: {
    //         CLIENT_1: {
    //             timers: {
    //                 initialDelay: 1000,
    //                 commandDelay: 200
    //             },
    //             orders: [
    //                 { keyboardevents: [
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER',
    //                     'i', 'n', 'SPACE', 'c', 'o', 'm', 'm', 'e', 'n', 't', 'ENTER'
    //                 ] },
    //                 { triggerevent: { type: ['mousedown', 'mouseup'], definition: { target: 'PAGE' } } } // leaving comment
    //             ]
    //         },
    //         CLIENT_2: {
    //             timers: {
    //                 initialDelay: 2000,
    //                 commandDelay: 200
    //             },
    //             orders: [
    //                 { timeout: 1000 },
    //                 { keyboardevents: ['a', 'b', 'ENTER', 'c'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
    //                 { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
    //                 { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
    //                 { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
    //                 { keyboardevents: ['m', 'n', 'ENTER', 'o'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
    //                 { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
    //                 { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
    //                 { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
    //                 { keyboardevents: ['m', 'n', 'ENTER', 'o'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
    //                 { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
    //                 { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
    //                 { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
    //                 { keyboardevents: ['m', 'n', 'ENTER', 'o'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
    //                 { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
    //                 { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
    //                 { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
    //                 { keyboardevents: ['m', 'n', 'ENTER', 'o'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
    //                 { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
    //                 { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
    //                 { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
    //                 { keyboardevents: ['m', 'n', 'ENTER', 'o'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
    //                 { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
    //                 { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
    //                 { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
    //                 { keyboardevents: ['m', 'n', 'ENTER', 'o'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
    //                 { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
    //                 { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
    //                 { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
    //                 { keyboardevents: ['m', 'n', 'ENTER', 'o'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
    //                 { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
    //                 { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
    //                 { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
    //                 { keyboardevents: ['m', 'n', 'ENTER', 'o'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
    //                 { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
    //                 { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
    //                 { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
    //                 { keyboardevents: ['m', 'n', 'ENTER', 'o'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_1' } }, // reloading the document
    //                 { keyboardevents: ['d', 'e', 'ENTER', 'f'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_2' } }, // reloading the document
    //                 { keyboardevents: ['g', 'h', 'ENTER', 'i'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_3' } }, // reloading the document
    //                 { keyboardevents: ['j', 'k', 'ENTER', 'l'] },
    //                 { functioncall: { functionname: 'reloadDocument', object: 'app', marker: 'RELOAD_4' } }, // reloading the document
    //                 { keyboardevents: ['m', 'n', 'ENTER', 'o'] }
    //             ]
    //         }
    //     },
    //     result: {
    //         CLIENT_1: {}
    //     }
    // },

    // single client, inserting text into a textframe
    TEST_42: {
        description: 'inserting text into a textframe',
        tooltip: 'A single client that inserts permanently text into an existing textframe',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { activatetoolbar: 'insert' },
                    { buttonclick: { dataKey: 'textframe/insert' } },
                    { timeout: 500 }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // single client, inserting text into a textframe
    TEST_43: {
        description: 'inserting text into an existing textframe and splitting it into several paragraphs',
        tooltip: 'A single client that inserts permanently text into an existing textframe and then generates many splitParagraph operations',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { activatetoolbar: 'insert' },
                    { buttonclick: { dataKey: 'textframe/insert' } },
                    { timeout: 500 }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW', 'LEFT_ARROW',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER',
                        'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER', 'RIGHT_ARROW', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // single client, inserting text into a shape
    TEST_44: {
        description: 'inserting text into a shape',
        tooltip: 'A single client that inserts permanently text into an existing shape',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { keyboardevents: ['ESCAPE', { key: 'a', ctrl: true }, 'DELETE'] }, // selecting all content and deleting it
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.1, startY: 0.1, stepX: 0.1, stepY: 0.05, stepCount: 8 } } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/textframeautofit' } } // setting autofit feature
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // Two clients, inserting text into a shape while resizing
    TEST_45: {
        description: 'inserting text into a textframe that is resized',
        tooltip: 'Two clients resizing a text frame and inserting text content into the text frame',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { keyboardevents: ['ESCAPE', { key: 'a', ctrl: true }, 'DELETE'] }, // selecting all content and deleting it
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'textframe/insert' } }, // inserting a shape with the GUI
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 500 },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="green"]' } }, // setting new background color
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/anchorTo' } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="paragraph"]' } },
                    { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    // { keyboardevents: [{ key: 'RIGHT_ARROW', shift: true }] } // selecting the drawing
                    { selection: { start: [0, 0], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 40
                },
                orders: [
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0, stepX: 0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: -0.05, startY: 0, stepX: -0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0, stepX: 0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: -0.05, startY: 0, stepX: -0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0, stepX: 0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // Two clients, changing position of a textframe with splitParagraph and mergeParagraph while resizing
    TEST_46: {
        description: 'splitting and merging paragraphs in front of a textframe that is resized',
        tooltip: 'Two clients resizing a text frame while in front of the textframe paragraphs are splitted and merged',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { keyboardevents: ['ESCAPE', { key: 'a', ctrl: true }, 'DELETE'] }, // selecting all content and deleting it
                    { timeout: 100 },
                    { keyboardevents: ['ENTER', 'ENTER'] },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'textframe/insert' } }, // inserting a shape with the GUI
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 500 },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="red"]' } }, // setting new background color
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/anchorTo' } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="paragraph"]' } },
                    { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    // { keyboardevents: [{ key: 'RIGHT_ARROW', shift: true }] } // selecting the drawing
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 40
                },
                orders: [
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0, stepX: 0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: -0.05, startY: 0, stepX: -0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0, stepX: 0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: -0.05, startY: 0, stepX: -0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0, stepX: 0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: -0.05, startY: 0, stepX: -0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: 0.05, startY: 0, stepX: 0.05, stepY: 0, stepCount: 14 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'resize', definition: { selector: '.selection > .resizers > [data-pos="r"]', startX: -0.05, startY: 0, stepX: -0.05, stepY: 0, stepCount: 14 } } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: [
                        'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE',
                        'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE',
                        'ENTER', 'ENTER', 'BACKSPACE', 'ENTER', 'ENTER', 'BACKSPACE', 'ENTER', 'ENTER', 'BACKSPACE', 'ENTER', 'ENTER', 'BACKSPACE',
                        'ENTER', 'BACKSPACE', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // Two clients, inserting text into a shape while rotating
    TEST_47: {
        description: 'inserting text into a textframe that is rotated',
        tooltip: 'Two clients rotating a text frame and inserting text content into the text frame',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { keyboardevents: ['ESCAPE', { key: 'a', ctrl: true }, 'DELETE'] }, // selecting all content and deleting it
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'textframe/insert' } }, // inserting a shape with the GUI
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 500 },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="green"]' } }, // setting new background color
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/anchorTo' } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="paragraph"]' } },
                    { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    // { keyboardevents: [{ key: 'RIGHT_ARROW', shift: true }] } // selecting the drawing
                    { selection: { start: [0, 0], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 40
                },
                orders: [
                    { timeout: 500 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: 0.15, startY: 0.10, stepX: 0.05, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: -0.05, startY: -0.01, stepX: -0.02, stepY: -0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: 0.15, startY: 0.10, stepX: 0.05, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: -0.05, startY: -0.01, stepX: -0.02, stepY: -0.01, stepCount: 10 } } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // Two clients, splitting and merging paragraphs in front of a shape while rotating
    TEST_48: {
        description: 'splitting and merging paragraphs in front of a textframe that is rotated',
        tooltip: 'Two clients rotating a text frame and splitting and merging paragraphs in front of the text frame',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { keyboardevents: ['ESCAPE', { key: 'a', ctrl: true }, 'DELETE'] }, // selecting all content and deleting it
                    { timeout: 100 },
                    { keyboardevents: ['ENTER', 'ENTER'] },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'textframe/insert' } }, // inserting a shape with the GUI
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 500 },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="red"]' } }, // setting new background color
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/anchorTo' } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="paragraph"]' } },
                    { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 40
                },
                orders: [
                    { timeout: 500 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: 0.15, startY: 0.10, stepX: 0.05, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: -0.05, startY: -0.01, stepX: -0.02, stepY: -0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: 0.15, startY: 0.10, stepX: 0.05, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 500 },
                    { triggerevent: { type: 'rotate', definition: { selector: '.selection > .rotate-handle', startX: -0.05, startY: -0.01, stepX: -0.02, stepY: -0.01, stepCount: 10 } } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: [
                        'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE',
                        'ENTER', 'ENTER', 'BACKSPACE', 'ENTER', 'ENTER', 'BACKSPACE', 'ENTER', 'ENTER', 'BACKSPACE', 'ENTER', 'ENTER', 'BACKSPACE',
                        'ENTER', 'BACKSPACE', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // Two clients, inserting text into a shape while moving
    TEST_49: {
        description: 'inserting text into a textframe that is moved',
        tooltip: 'Two clients moving a text frame and inserting text content into the text frame',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { keyboardevents: ['ESCAPE', { key: 'a', ctrl: true }, 'DELETE'] }, // selecting all content and deleting it
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'textframe/insert' } }, // inserting a shape with the GUI
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 500 },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="green"]' } }, // setting new background color
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/anchorTo' } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="paragraph"]' } },
                    { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    // { keyboardevents: [{ key: 'RIGHT_ARROW', shift: true }] } // selecting the drawing
                    { selection: { start: [0, 0], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 40
                },
                orders: [
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .p > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.01, stepCount: 11 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .p > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.01, stepCount: 11 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .p > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.01, stepCount: 11 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .p > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.01, stepCount: 11 } } },
                    { timeout: 1000 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    // Two clients, splitting and merging paragraphs in front of a shape while moving
    TEST_50: {
        description: 'splitting and merging paragraphs in front of a textframe that is moved',
        tooltip: 'Two clients moving a text frame and splitting and merging paragraphs in front of the text frame',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { keyboardevents: ['ESCAPE', { key: 'a', ctrl: true }, 'DELETE'] }, // selecting all content and deleting it
                    { timeout: 100 },
                    { keyboardevents: ['ENTER', 'ENTER'] },
                    { activatetoolbar: 'insert' },
                    { timeout: 200 },
                    { buttonclick: { dataKey: 'textframe/insert' } }, // inserting a shape with the GUI
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/fill/color', index: 1 } }, // activating background color GUI
                    { timeout: 500 },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="red"]' } }, // setting new background color
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/anchorTo' } },
                    { timeout: 500 },
                    { buttonclick: { selector: 'a[data-value="paragraph"]' } },
                    { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 40
                },
                orders: [
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .p > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.01, stepCount: 11 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .p > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.01, stepCount: 11 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .p > .drawing.selected > .textframecontent', startX: 0.05, startY: 0.05, stepX: 0.05, stepY: 0.01, stepCount: 11 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'move', definition: { selector: '.app-content > .page > .pagecontent > .p > .drawing.selected > .textframecontent', startX: -0.05, startY: -0.05, stepX: -0.05, stepY: -0.01, stepCount: 11 } } },
                    { timeout: 1000 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: [
                        'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'ENTER', 'BACKSPACE',
                        'ENTER', 'ENTER', 'BACKSPACE', 'ENTER', 'ENTER', 'BACKSPACE', 'ENTER', 'ENTER', 'BACKSPACE', 'ENTER', 'ENTER', 'BACKSPACE',
                        'ENTER', 'BACKSPACE', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'BACKSPACE', 'ENTER', 'BACKSPACE', 'BACKSPACE'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_51: {
        description: 'inserting text into a shape that is adjusted',
        tooltip: 'Two clients adjusting a shape and inserting text content into the shape',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                orders: [
                    { keyboardevents: ['ESCAPE', { key: 'a', ctrl: true }, 'DELETE'] }, // selecting all content and deleting it
                    { timeout: 100 },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'shape/insert' } }, // inserting a shape with the GUI
                    { timeout: 1000 },
                    { buttonclick: { selector: 'a[data-value="roundRect"]' } },
                    { timeout: 500 },
                    { triggerevent: { type: 'selectionBox', definition: { mode: 'shape', startX: 0.1, startY: 0.1, stepX: 0.1, stepY: 0.05, stepCount: 8 } } },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'drawing/textframeautofit' } }, // setting autofit feature
                    { timeout: 500 }
                ]
            },
            CLIENT_2: {
                orders: [
                    // { keyboardevents: [{ key: 'RIGHT_ARROW', shift: true }] } // selecting the drawing
                    { selection: { start: [0, 0], end: [0, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 50
                },
                orders: [
                    { timeout: 1000 },
                    { triggerevent: { type: 'adjust', definition: { selector: '.selection > .adj-points-container > .adj-handle', startX: 0.15, startY: 0.10, stepX: 0.05, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'adjust', definition: { selector: '.selection > .adj-points-container > .adj-handle', startX: -0.05, startY: -0.01, stepX: -0.02, stepY: -0.01, stepCount: 10 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'adjust', definition: { selector: '.selection > .adj-points-container > .adj-handle', startX: 0.15, startY: 0.10, stepX: 0.05, stepY: 0.01, stepCount: 10 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'adjust', definition: { selector: '.selection > .adj-points-container > .adj-handle', startX: -0.05, startY: -0.01, stepX: -0.02, stepY: -0.01, stepCount: 10 } } },
                    { timeout: 1000 },
                    { triggerevent: { type: 'adjust', definition: { selector: '.selection > .adj-points-container > .adj-handle', startX: 0.15, startY: 0.10, stepX: 0.05, stepY: 0.01, stepCount: 10 } } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 200,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: [
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ENTER'
                    ] }
                ]
            }
        },
        result: {
            CLIENT_1: {}
        }
    },

    TEST_52: {
        description: 'Testing insertColumn and deleteColumns together with their undo operations agains a client that inserts and deletes text into the same table (DOCS-3114)',
        tooltip: 'A two clients test for inserting and deleting columns in a table in that another client adds and removes text (DOCS-3114)',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 20
                },
                orders: [
                    { keyboardevents: 'CTRL+a DELETE' }, // twice 'delete' to remove regenerated placeholder drawings, too
                    { timeout: 100 },
                    { keyboardevents: ['SHIFT+h e l l o SPACE SHIFT+W o r l d'] },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '7*RIGHT_ARROW 7*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: [
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW',
                        'SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+W o r l d TAB SHIFT+h e l l o SPACE SHIFT+w o r l d 3*SHIFT+TAB DOWN_ARROW'
                    ] },
                    { timeout: 100 },
                    { keyboardevents: '6*UP_ARROW' }
                ]
            },
            CLIENT_2: {
                orders: [
                    { keyboardevents: '3*DOWN_ARROW TAB' }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    testStartDelay: 3000, // give other client time for preparation
                    initialDelay: 100,
                    commandDelay: 30
                },
                orders: [
                    { timeout: 500 }, // let second user insert a row
                    { keyboardevents: ['5*TAB SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1'] }, // going to right part of the table for editing
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 2'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 2'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 2'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 3'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 3'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 3'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 4'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 4'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 4'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 5'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 5'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 5'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 6'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 6'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 6'] }, { timeout: 300 },
                    { keyboardevents: ['1*RIGHT_ARROW 5*UP_ARROW'] },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 7'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 7'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 7'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 8'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 8'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 8'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 9'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 9'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 9'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1 0'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1 0'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1 0'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1 1'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1 1'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1 1'] }, { timeout: 300 },
                    { keyboardevents: ['DOWN_ARROW SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1 2'] },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1 2'] }, { timeout: 300 },
                    { buttonclick: 'undo' }, { buttonclick: 'undo' }, { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: ['SHIFT+H e l l o SPACE SHIFT+T e s t SPACE 1 2'] }, { timeout: 300 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 20
                },
                orders: [
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 1' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 2' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 3' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 4' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 5' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 6' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 7' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 8' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 9' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 1 0' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 1 1' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 1 2' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 1 3' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 1 4' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/insert/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 1 5' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { keyboardevents: 'SPACE 1 6' },
                    { activatetoolbar: 'table' }, { timeout: 100 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 },
                    { buttonclick: 'redo' }, { timeout: 300 },
                    { buttonclick: 'undo' }, { timeout: 300 }
                ]
            }
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    },

    // modifying a table, so that cells are removed. In OX Text this is allowed. In OX Presentation there is
    // test TEST_18H, in which a reload is triggered, when table cells are not restored.
    TEST_53: {
        description: 'Testing deleteColumn together with their undo operations against a client that deletes rows in the same table',
        tooltip: 'A two clients test for deleting columns and rows together with their undo operations in one table',
        clients: 2,
        autotest: true,
        preparation: {
            CLIENT_1: {
                timers: {
                    commandDelay: 20
                },
                orders: [
                    { keyboardevents: 'CTRL+a DELETE' }, // twice 'delete' to remove regenerated placeholder drawings, too
                    { timeout: 100 },
                    { keyboardevents: ['SHIFT+h e l l o SPACE SHIFT+W o r l d'] },
                    { activatetoolbar: 'insert' },
                    { timeout: 500 },
                    { buttonclick: { dataKey: 'table/insert' } }, // inserting a table via keyboard (the recommended way)
                    { timeout: 100 },
                    { keyboardevents: '5*RIGHT_ARROW 5*DOWN_ARROW ENTER' },
                    { timeout: 500 },
                    { keyboardevents: [
                        '1 TAB 2 TAB 3 TAB SHIFT+h e l l o SPACE 4 TAB 5 TAB 6 TAB',
                        '7 TAB 8 TAB 9 TAB SHIFT+h e l l o SPACE 1 0 TAB 1 1 TAB 1 2 TAB',
                        '1 3 TAB 1 4 TAB 1 5 TAB SHIFT+h e l l o SPACE  1 6 TAB 1 7 TAB 1 8 TAB',
                        'SHIFT+h e l l o SPACE 1 9 TAB SHIFT+h e l l o SPACE 2 0 TAB SHIFT+h e l l o SPACE 2 1 TAB SHIFT+h e l l o SPACE  2 2 TAB SHIFT+h e l l o SPACE 2 3 TAB SHIFT+h e l l o SPACE 2 4 TAB',
                        '2 5 TAB 2 6 TAB 2 7 TAB SHIFT+h e l l o SPACE  2 8 TAB 2 9 TAB 3 0 TAB',
                        '3 1 TAB 3 2 TAB 3 3 TAB SHIFT+h e l l o SPACE  3 4 TAB 3 5 TAB 3 6',
                        '2*UP_ARROW 5*SHIFT+TAB' // deleting the 4th of the 6 rows
                    ] }
                ]
            },
            CLIENT_2: {
                orders: [
                    { keyboardevents: '2*DOWN_ARROW 3*TAB a' } // deleting the 4th of the 6 columns
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    testStartDelay: 2500, // give other client time for preparation
                    initialDelay: 100,
                    commandDelay: 30
                },
                orders: [
                    { timeout: 500 }, // let second user insert a row
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/row' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 100,
                    commandDelay: 20
                },
                orders: [
                    { activatetoolbar: 'table' }, { timeout: 300 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 },
                    { buttonclick: { dataKey: 'table/delete/column' } }, { timeout: 1500 },
                    { buttonclick: 'undo' }, { timeout: 1500 }
                ]
            },
            CLIENT_3: {} // a watcher
        },
        result: {
            CLIENT_1: {},
            CLIENT_2: {}
        }
    }
};
