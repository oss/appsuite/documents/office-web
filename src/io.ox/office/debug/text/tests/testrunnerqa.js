/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import * as Op from '@/io.ox/office/textframework/utils/operations';

// exports ================================================================

/**
 * Definitions for the runtime tests of OX Text (QA).
 */
export default {

    // single client, simple operation test, no commands
    TEST_0001_QA: {
        description: 'QA: Simple single client test',
        tooltip: 'QA: A simple single client test',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABC' },
                        { name: Op.TEXT_INSERT, start: [0, 3], text: 'DEF' },
                        { name: Op.TEXT_INSERT, start: [0, 6], text: 'G' }
                    ] },
                    { selection: { start: [0, 1], end: [0, 3] } }
                ]
            }
        },
        commands: null,
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 1], end: [0, 3] },
                expectedTextContent: 'ABCDEFG'
            }
        }
    },
    TEST_0001_QA_FAIL: {
        description: 'QA: Simple single client test FAIL',
        tooltip: 'QA: A simple single client test content FAIL',
        clients: 1,
        autotest: false,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABC' },
                        { name: Op.TEXT_INSERT, start: [0, 3], text: 'G' }
                    ] },
                    { selection: { start: [0, 1], end: [0, 3] } }
                ]
            }
        },
        commands: null,
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 1], end: [0, 3] },
                expectedTextContent: 'ABCDEFG'
            }
        }
    },
    // TestRail test #
    TEST_0002_QA_TestRail_C7924: {
        description: 'TestRail C7924 Font attribute bold preselect',
        tooltip: 'C7924 Font attribute bold preselect',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    // TestRail test #
    TEST_0003_QA_TestRail_C7925: {
        description: 'TestRail C7925 Font attribute bold select',
        tooltip: 'C7925 Font attribute bold select',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { activatetoolbar: 'format' },
                    { selection: { start: [0, 6], end: [0, 11] } },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: { time: 1000 } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' }
                ]
            }
        }
    },
    // TestRail test #3
    TEST_0004_QA_TestRail_C7926: {
        description: 'TestRail C7926 Font attribute italic preselect',
        tooltip: 'C7926 Font attribute italic preselect',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    // TestRail test #4
    TEST_0005_QA_TestRail_C7927: {
        description: 'TestRail C7927 Font attribute italic select',
        tooltip: 'C7927 Font attribute italic select',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { activatetoolbar: 'format' },
                    { selection: { start: [0, 6], end: [0, 11] } },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { timeout: { time: 1000 } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' }
                ]
            }
        }
    },
    // TestRail test #5
    TEST_0006_QA_TestRail_C7928: {
        description: 'TestRail C7928 Font attribute underline preselect',
        tooltip: 'C7928 Font attribute underline preselect',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { buttonclick: { dataKey: 'character/underline' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    // TestRail test #6
    TEST_0007_QA_TestRail_C7929: {
        description: 'TestRail C7929 Font attribute underline select',
        tooltip: 'C7929 Font attribute underline select',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { activatetoolbar: 'format' },
                    { selection: { start: [0, 6], end: [0, 11] } },
                    { buttonclick: { dataKey: 'character/underline' } },
                    { timeout: { time: 1000 } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' }
                ]
            }
        }
    },
    // TestRail test #7
    TEST_0008_QA_TestRail_C7930: {
        description: 'TestRail C7930 Font attribute strike through preselect',
        tooltip: 'C7930 Font attribute strike through preselect',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/strike"] > a' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    // TestRail test #8
    TEST_0009_QA_TestRail_C7931: {
        description: 'TestRail C7931 Font attribute strike through select',
        tooltip: 'C7931 Font attribute strike through select',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { activatetoolbar: 'format' },
                    { selection: { start: [0, 6], end: [0, 11] } },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/strike"] > a' } },
                    { timeout: { time: 500 } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' }
                ]
            }
        }
    },
    // TestRail test #9
    TEST_0010_QA_TestRail_C7932: {
        description: 'TestRail C7932 Font attribute subscrip preselect',
        tooltip: 'C7932 Font attribute subscrip preselect',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/vertalign"] > a[data-value="sub"]' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    // TestRail test #10
    TEST_0011_QA_TestRail_C7933: {
        description: 'TestRail C7933 Font attribute subscript select',
        tooltip: 'C7933 Font attribute subscript select',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { activatetoolbar: 'format' },
                    { selection: { start: [0, 6], end: [0, 11] } },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/vertalign"] > a[data-value="sub"]' } },
                    { timeout: { time: 500 } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' }
                ]
            }
        }
    },
    // TestRail test #11
    TEST_0012_QA_TestRail_C7934: {
        description: 'TestRail C7934 Font attribute superscrip preselect',
        tooltip: 'C7934 Font attribute superscrip preselect',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40

                },
                orders: [
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/vertalign"] > a[data-value="super"]' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    // TestRail test #12
    TEST_0013_QA_TestRail_C7935: {
        description: 'TestRail C795 Font attribute superscript select',
        tooltip: 'C7935 Font attribute superscript select',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { activatetoolbar: 'format' },
                    { selection: { start: [0, 6], end: [0, 11] } },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/vertalign"] > a[data-value="super"]' } },
                    { timeout: { time: 500 } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' }
                ]
            }
        }
    },
    // combining several font styles
    TEST_0014_QA: {
        description: 'bold, italic, underlined, strike through, sub script, super script',
        tooltip: 'bold, italic, underlined, strike through, sub script, super script',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { selection: { start: [1, 17], end: [1, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { selection: { start: [3, 6], end: [3, 11] } },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { timeout: { time: 500 } },
                    { selection: { start: [3, 17], end: [3, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'character/underline' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { selection: { start: [5, 17], end: [5, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'character/underline' } },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/strike"] > a' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { keyboardevents: ['ENTER'] },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/strike"] > a' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { selection: { start: [8, 6], end: [8, 11] } },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/vertalign"] > a[data-value="sub"]' } },
                    { timeout: { time: 500 } },
                    { selection: { start: [9, 17], end: [9, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { selection: { start: [10, 6], end: [10, 11] } },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/vertalign"] > a[data-value="super"]' } },
                    { timeout: { time: 500 } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { selection: { start: [11, 17], end: [11, 17] } },
                    { keyboardevents: ['ENTER'] }
                ]
            }
        }
    },
    //Text in textframe
    TEST_0015_QA_TestRail_C12240: {
        description: 'TestRail C12240 Insert text in Textframe',
        tooltip: 'TestRail C12240 Insert Text in Textframe',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'insert' },
                    { buttonclick: { dataKey: 'textframe/insert' } },
                    { timeout: { time: 500 } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' }
                ]
            }
        }
    },
    //Bold text in textframe
    TEST_0016_QA_TestRail_C17473: {
        description: 'TestRailC17473 Insert bold text in Textframe',
        tooltip: 'TestRailC17473 Insert bold text in Textframe',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'insert' },
                    { buttonclick: { dataKey: 'textframe/insert' } },
                    { timeout: { time: 500 } },
                    { activatetoolbar: 'format' },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: { time: 500 } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'ENTER', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' }
                ]
            }
        }
    },
    // TestRail test #
    TEST_0017_QA_TestRail_C293121: {
        description: 'TestRailC293121 Font attribute reset',
        tooltip: 'C293121 Font attribute reset',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'ENTER'
                    ] },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { timeout: { time: 200 } },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'ENTER'
                    ] },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { timeout: { time: 200 } },
                    { buttonclick: { dataKey: 'character/underline' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'ENTER'
                    ] },
                    { buttonclick: { dataKey: 'character/underline' } },
                    { timeout: { time: 200 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 300 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/strike"] > a' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'ENTER'
                    ] },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 300 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/strike"] > a' } },
                    { timeout: { time: 300 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 300 } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'ENTER'
                    ] },
                    { selection: { start: [4, 0], end: [4, 5] } },
                    { timeout: { time: 300 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 300 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/vertalign"] > a[data-value="sub"]' } },
                    { timeout: { time: 300 } },
                    { selection: { start: [4, 11], end: [4, 11] } },
                    { keyboardevents: ['ENTER'] }, //new line
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'ENTER' //insert text
                    ] },
                    { selection: { start: [5, 0], end: [5, 5] } }, //select text
                    { timeout: { time: 300 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { timeout: { time: 300 } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/vertalign"] > a[data-value="super"]' } },
                    { timeout: { time: 300 } },
                    { selection: { start: [5, 11], end: [5, 11] } },
                    { keyboardevents: ['ENTER'] }, //new line
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'ENTER' //insert text
                    ] },
                    { selection: { start: [6, 0], end: [6, 5] } }, //select text
                    { timeout: { time: 300 } },
                    { buttonclick: { dataKey: 'character/fillcolor' } }, // yellow fill preselect
                    { timeout: { time: 300 } },
                    { selection: { start: [6, 11], end: [6, 11] } },
                    { keyboardevents: ['ENTER'] }, //new line
                    { timeout: { time: 300 } },
                    { buttonclick: { dataKey: 'character/color' } }, //red text color
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'ENTER' //insert text
                    ] },
                    { selection: { start: [0, 0], end: [0, 0] } },
                    { timeout: { time: 2000 } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { activatetoolbar: 'format' },
                    { selection: { start: [0, 0], end: [7, 11] } },
                    { timeout: { time: 1000 } },
                    { buttonclick: { dataKey: 'view/character/format/menu' } },
                    { buttonclick: { selector: '.popup-container .group[data-key="character/reset"] > a' } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { selection: { start: [0, 0], end: [0, 0] } }
                ]
            }
        }
    },
    //Text in table
    TEST_0018_QA: {
        description: 'Insert text in a table',
        tooltip: 'Insert text in a table',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'insert' },
                    { functioncall: { functionname: 'insertTable', params: [{ width: 2, height: 3 }] } },
                    { timeout: { time: 500 } },
                    { keyboardevents: [
                        'l', 'o', 'r', 'e', 'm', 'TAB', 'i', 'p', 's', 'u', 'm'
                    ] },
                    { selection: { start: [0, 0, 1, 0, 0], end: [0, 0, 1, 0, 5] } },
                    { activatetoolbar: 'format' },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { selection: { start: [0, 0, 1, 0, 5], end: [0, 0, 1, 0, 5] } },
                    { keyboardevents: [
                        'TAB', 'l', 'o', 'r', 'e', 'm', 'TAB', 'i', 'p', 's', 'u', 'm'
                    ] },
                    { selection: { start: [0, 1, 0, 0, 0], end: [0, 1, 0, 0, 5] } },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { selection: { start: [0, 0, 1, 0, 0], end: [0, 0, 1, 0, 5] } },
                    { activatetoolbar: 'table' },
                    { buttonclick: { dataKey: 'table/insert/row' } },
                    { timeout: { time: 500 } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' }
                ]
            }
        }
    },
    TEST_0019_QA: {
        description: 'TestRail C7936 Font attribute Red color preselect',
        tooltip: 'C7936 Font attribute Red preselect preselect',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'character/color' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { selection: { start: [1, 17], end: [1, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    TEST_0020_QA: {
        description: 'TestRail C7937 Font attribute Red color select',
        tooltip: 'C7937 Font attribute Red preselect select',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { selection: { start: [1, 6], end: [1, 11] } },
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'character/color' } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { selection: { start: [1, 17], end: [1, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    TEST_0021_QA: {
        description: 'TestRail C7936 Font attribute colorpicker Accent 1 preselect',
        tooltip: 'C7936 Font attribute Accent 1 preselect',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'character/color' } },
                    { buttonclick: { dataKey: 'character/color', index: 1 } }, // using the second button (the arrow on the split button)
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="accent1"]' } },
                    { timeout: { time: 500 } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { selection: { start: [0, 17], end: [0, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] }
                ]
            }
        }
    },
    TEST_0022_QA: {
        description: 'TestRail C7937 Font attribute colorpicker Accent 2 select',
        tooltip: 'C7937 Font attribute colorpicker Accent 2 select',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { selection: { start: [1, 6], end: [1, 11] } },
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'character/color' } },
                    { buttonclick: { dataKey: 'character/color', index: 1 } }, // using the second button (the arrow on the split button)
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="accent2"]' } },
                    { timeout: { time: 500 } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { selection: { start: [1, 17], end: [1, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    TEST_0023_QA: {
        description: 'TestRail C7938 Font background color Yellow preselect',
        tooltip: 'C7938 Font background color Yellow preselect',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'character/fillcolor' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { selection: { start: [1, 17], end: [1, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    TEST_0024_QA: {
        description: 'TestRail C7939 Font background color Yellow color select',
        tooltip: 'C7939 Font background color Yellow preselect select',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { selection: { start: [1, 6], end: [1, 11] } },
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'character/fillcolor' } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { selection: { start: [1, 17], end: [1, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    TEST_0025_QA: {
        description: 'TestRail C7938 Font background colorpicker Accent 3 preselect',
        tooltip: 'C7938 Font background color Accent 3 preselect',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'character/fillcolor' } },
                    { buttonclick: { dataKey: 'character/fillcolor', index: 1 } }, // using the second button (the arrow on the split button)
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="accent3-darker50"]' } },
                    { timeout: { time: 500 } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { selection: { start: [1, 17], end: [1, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    TEST_0026_QA: {
        description: 'TestRail C7939 Font background color Accent 3 color select',
        tooltip: 'C7939 Font background color Accent preselect select',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { selection: { start: [1, 6], end: [1, 11] } },
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'character/fillcolor' } },
                    { buttonclick: { dataKey: 'character/fillcolor', index: 1 } }, // using the second button (the arrow on the split button)
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="accent3-darker50"]' } },
                    { timeout: { time: 500 } },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { selection: { start: [1, 17], end: [1, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    TEST_0027_QA: {
        description: 'TestRail C7938 Font background color Accent4 darker 25% preselect',
        tooltip: 'C7938 Font background color preselect',
        clients: 1,
        autotest: false,
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 400,
                    commandDelay: 40
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { timeout: { time: 500 } },
                    { buttonclick: { dataKey: 'character/fillcolor' } },
                    { buttonclick: { dataKey: 'character/fillcolor', index: 1 } }, // using the second button (the arrow on the split button)
                    { timeout: { time: 500 } },
                    { buttonclick: { selector: '.popup-container a.btn[data-value="accent4-darker25"]' } },
                    { timeout: { time: 500 } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { functioncall: { functionname: 'reloadDocument', object: 'app' }, marker: 'RELOAD_1' },
                    { selection: { start: [1, 17], end: [1, 17] } },
                    { keyboardevents: ['ENTER'] },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] }
                ]
            }
        }
    },
    TEST_00XX_QA_BETA: {
        description: 'Font format with 3 clients',
        tooltip: 'Font format with 3 clients',
        clients: 3,
        autotest: false,
        preparation: {
            CLIENT_1: {}
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 100
                },
                orders: [
                    { activatetoolbar: 'format' },
                    { buttonclick: { dataKey: 'character/bold' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { selection: { start: [1, 17], end: [1, 17] } },
                    { buttonclick: { dataKey: 'character/bold' } }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 100
                },
                orders: [
                    { buttonclick: { dataKey: 'character/underline' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r'
                    ] },
                    { buttonclick: { dataKey: 'character/underline' } }
                ]
            },
            CLIENT_3: {
                timers: {
                    initialDelay: 2000,
                    commandDelay: 100
                },
                orders: [
                    { buttonclick: { dataKey: 'character/italic' } },
                    { keyboardevents: ['l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER', 'l', 'o', 'r', 'e', 'm', 'SPACE', 'i', 'p', 's', 'u', 'm', 'SPACE',
                        'd', 'o', 'l', 'o', 'r', 'ENTER'
                    ] },
                    { buttonclick: { dataKey: 'character/italic' } },
                    { timeout: { time: 500 } }
                ]
            }
        }
    }
};
