/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import * as Op from '@/io.ox/office/textframework/utils/operations';

// exports ================================================================

/**
 * Definitions for the runtime tests of OX Text (QA2).
 */
export default {

    // two clients, adding content into different paragraphs
    TEST_0001_QA2: {
        description: 'Content in different paragraphs',
        tooltip: 'A two clients test with text input in different paragraphs',
        clients: 2,
        autotest: false,
        repeat: 1,
        repeatDelay: 2000,
        preparation: {
            CLIENT_1: {
                orders: [
                    { operations: [
                        { name: Op.PARA_INSERT, start: [0] },
                        { name: Op.TEXT_INSERT, start: [0, 0], text: 'ABCDEF' },
                        { name: Op.PARA_INSERT, start: [1] },
                        { name: Op.TEXT_INSERT, start: [1, 0], text: 'MNOPQR' }
                    ] },
                    { selection: { start: [0, 5], end: [0, 5] } }
                ]
            },
            CLIENT_2: {
                orders: [
                    { selection: { start: [1, 1], end: [1, 1] } }
                ]
            }
        },
        commands: {
            CLIENT_1: {
                timers: {
                    initialDelay: 1500,
                    commandDelay: 200
                },
                orders: [
                    { keyboardevents: ['1', '2', '3', '4', '5', '6', '7', '8'] }
                ]
            },
            CLIENT_2: {
                timers: {
                    initialDelay: 1000,
                    commandDelay: 500
                },
                orders: [
                    { keyboardevents: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'] }
                ]
            }
        },
        result: {
            CLIENT_1: {
                expectedSelection: { start: [0, 13], end: [0, 13] },
                expectedTextContent: 'ABCDE12345678FMabcdefghNOPQR'
            },
            CLIENT_2: {
                expectedSelection: { start: [1, 9], end: [1, 9] },
                expectedTextContent: 'ABCDE12345678FMabcdefghNOPQR'
            }
        }
    }

};
