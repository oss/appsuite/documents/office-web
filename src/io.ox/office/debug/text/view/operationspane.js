/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";

import { getExplicitAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import { getDOMPosition } from "@/io.ox/office/textframework/utils/position";

// public functions =======================================================

/**
 * Registers all event handling and logging for the debug operations pane.
 */
export function initializeOperationsPane(docView, operationsPane) {

    // shortcuts to properties
    const docModel = docView.docModel;
    const selection = docModel.getSelection();
    const inspector = operationsPane.inspector;

    // create the output nodes in the operations pane
    operationsPane.addDebugInfoHeader("selection", "Current Selection");
    operationsPane.addDebugInfoNode("selection", "type", "Type of the selection");
    operationsPane.addDebugInfoNode("selection", "start", "Start position");
    operationsPane.addDebugInfoNode("selection", "end", "End position");
    operationsPane.addDebugInfoNode("selection", "target", "Target position");
    operationsPane.addDebugInfoNode("selection", "dir", "Direction");
    operationsPane.addDebugInfoNode("selection", "attrs1", "Explicit attributes of cursor position");
    operationsPane.addDebugInfoNode("selection", "attrs2", "Explicit attributes (parent level)");
    operationsPane.addDebugInfoNode("selection", "style", "Paragraph style");

    function debugAttrs(position, ignoreDepth, target) {
        position = _.initial(position, ignoreDepth);
        const domPos = position?.length ? getDOMPosition(docModel.getCurrentRootNode(), position, true) : null;
        const contents = domPos?.node ? inspector.createAnchor(getExplicitAttributeSet(domPos.node)) : null;
        operationsPane.setDebugInfo("selection", target, contents);
    }

    function debugStyle(family, target) {
        const styleId = docModel.getAttributes(family).styleId;
        if (styleId) {
            const attrSet = docModel.getStyleCollection(family).getStyleAttributeSet(styleId);
            operationsPane.setDebugInfo("selection", target, `family=${family}`, inspector.createAnchor(attrSet, { label: `id=${styleId}` }));
        } else {
            operationsPane.setDebugInfo("selection", target, "");
        }
    }

    // log all selection events, and new formatting after operations
    operationsPane.listenTo(docModel, "selection operations:success", operationsPane.debounce(() => {

        operationsPane.setDebugInfo("selection", "type", selection.getSelectionType());
        operationsPane.setDebugInfo("selection", "start", selection.getStartPosition().join(", "));
        operationsPane.setDebugInfo("selection", "end", selection.getEndPosition().join(", "));
        operationsPane.setDebugInfo("selection", "target", selection.getRootNodeTarget());
        operationsPane.setDebugInfo("selection", "dir", selection.getDirection());

        debugAttrs(selection.getStartPosition(), 0, "attrs1");
        debugAttrs(selection.getStartPosition(), 1, "attrs2");
        debugStyle("paragraph", "style");

    }, { delay: 200 }));
}
