/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { SMALL_DEVICE } from "@/io.ox/office/tk/dom";

import type TextView from "@/io.ox/office/text/view/view";
import type TextApp from "@/io.ox/office/text/app/application";

import { ClipboardToolBar } from "@/io.ox/office/debug/common/view/toolbars";
import type { AppDebugContext } from "@/io.ox/office/debug/common/app/debugcontext";

import { initializeOperationsPane } from "@/io.ox/office/debug/text/view/operationspane";

import testDefinitions from "@/io.ox/office/debug/text/tests/testrunner";
import testDefinitionsqa from "@/io.ox/office/debug/text/tests/testrunnerqa";
import testDefinitionsqa2 from "@/io.ox/office/debug/text/tests/testrunnerqa2";

// public functions ===========================================================

/**
 * Entry point for debug initialization of a text application.
 *
 * @param docApp
 *  The application instance to be extended with debug functionality.
 *
 * @param debugContext
 *  The storage for additional debug data per application.
 */
export function initializeApp(docApp: TextApp, debugContext: AppDebugContext): void {

    // the application components
    const { docModel, docController } = docApp;

    docController.registerItem<boolean>("debug/option/pagebreaks", {
        enable() { return !SMALL_DEVICE && !docModel.isDraftMode(); },
        get() { return docModel.isPageBreakMode(); },
        set(state) { docModel.togglePageBreakMode(state); }
    });

    docController.registerItem<boolean>("debug/option/draftmode", {
        parent: "debug/enabled",
        enable() { return !SMALL_DEVICE; },
        get() { return docModel.isDraftMode(); },
        set(state) { docModel.toggleDraftMode(state); }
    });

    // register definitions for text UI tests
    debugContext.testRunner.registerDefinitions(testDefinitions);
    debugContext.testRunner.registerDefinitions(testDefinitionsqa);
    debugContext.testRunner.registerDefinitions(testDefinitionsqa2);
}

/**
 * Entry point for debug initialization of a text view.
 *
 * @param docView
 *  The view instance to be extended with debug functionality.
 *
 * @param debugContext
 *  The storage for additional debug data per application.
 */
export function initializeView(docView: TextView, debugContext: AppDebugContext): void {

    // initialize the operations pane
    initializeOperationsPane(docView, debugContext.operationsPane);
}

/**
 * Entry point for initialization of the debug toolbar in the text view.
 *
 * @param docView
 *  The view instance to be extended with debug functionality.
 *
 * @param debugContext
 *  The storage for additional debug data per application.
 */
export function initializeToolPane(docView: TextView, debugContext: AppDebugContext): void {

    const { debugToolBar } = debugContext;

    debugToolBar.optionsPicker.addSection("import", _.noI18n("Import settings"));
    debugToolBar.optionsPicker.createCheckBox("debug/option/localstorage", { icon: "bi:truck", label: "Use local storage" });
    debugToolBar.optionsPicker.createCheckBox("debug/option/fastload",     { icon: "bi:bicycle", label: "Use fast load" });

    debugToolBar.optionsPicker.addSection("text", _.noI18n("Text settings"));
    debugToolBar.optionsPicker.createCheckBox("debug/option/pagebreaks",   { label: "Show page breaks" });
    debugToolBar.optionsPicker.createCheckBox("debug/option/draftmode",    { label: "View in draft mode" });

    debugToolBar.loggersPicker.addSection("text", _.noI18n("Text loggers"));
    debugToolBar.loggersPicker.registerLogger("text:log-pagebreaks", "Logger for the page-break engine.");

    // extra toolbars
    docView.insertToolBar(new ClipboardToolBar(docView));
}
