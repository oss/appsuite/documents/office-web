/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import type PresentationView from "@/io.ox/office/presentation/view/view";
import type PresentationApp from "@/io.ox/office/presentation/app/application";

import { ClipboardToolBar } from "@/io.ox/office/debug/common/view/toolbars";
import type { AppDebugContext } from "@/io.ox/office/debug/common/app/debugcontext";

import { initializeOperationsPane } from "@/io.ox/office/debug/presentation/view/operationspane";

import testDefinitions from "@/io.ox/office/debug/presentation/tests/testrunner";
import testDefinitions2 from "@/io.ox/office/debug/presentation/tests/testrunner2";
import testDefinitionsqa from "@/io.ox/office/debug/presentation/tests/testrunnerqa";
import testDefinitionsqa2 from "@/io.ox/office/debug/presentation/tests/testrunnerqa2";

// public functions ===========================================================

/**
 * Entry point for debug initialization of a presentation application.
 *
 * @param _docApp
 *  The application instance to be extended with debug functionality.
 *
 * @param debugContext
 *  The storage for additional debug data per application.
 */
export function initializeApp(_docApp: PresentationApp, debugContext: AppDebugContext): void {

    // register definitions for presentation UI tests
    debugContext.testRunner.registerDefinitions(testDefinitions);
    debugContext.testRunner.registerDefinitions(testDefinitions2);
    debugContext.testRunner.registerDefinitions(testDefinitionsqa);
    debugContext.testRunner.registerDefinitions(testDefinitionsqa2);
}

/**
 * Entry point for debug initialization of a presentation view.
 *
 * @param docView
 *  The view instance to be extended with debug functionality.
 *
 * @param debugContext
 *  The storage for additional debug data per application.
 */
export function initializeView(docView: PresentationView, debugContext: AppDebugContext): void {

    // initialize the operations pane
    initializeOperationsPane(docView, debugContext.operationsPane);
}

/**
 * Entry point for initialization of the debug toolbar in the presentation
 * view.
 *
 * @param docView
 *  The view instance to be extended with debug functionality.
 *
 * @param debugContext
 *  The storage for additional debug data per application.
 */
export function initializeToolPane(docView: PresentationView, debugContext: AppDebugContext): void {

    const { debugToolBar } = debugContext;

    debugToolBar.optionsPicker.addSection("import", _.noI18n("Import settings"));
    debugToolBar.optionsPicker.createCheckBox("debug/option/localstorage", { icon: "bi:truck", label: "Use local storage" });
    debugToolBar.optionsPicker.createCheckBox("debug/option/fastload",     { icon: "bi:bicycle", label: "Use fast load" });

    // extra toolbars
    docView.insertToolBar(new ClipboardToolBar(docView));
}
